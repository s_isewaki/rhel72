{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}研修・講義</title>
</head>
<body onresize="resized()">
<!-- ///study_menu_user  -->







<script type="text/javascript">
    var is_emp_proxy = "{$is_emp_proxy}";
    var target_emp_id = "{$target_emp_id}";
    var notify_list_count = {$notif_list_count};
    var cur_page_num = {$page_num};
    var cur_per_page_size = {$per_page_size};
    var total_count = {$total_count};
    var last_page = {$last_page};

    {literal}
    function paging(mode) {
        var page_num = parseInt($("#page_num").val());
        var new_page_num = parseInt($("#new_page_num").val());
        if (last_page < 1) last_page = 1;
        if (!page_num || isNaN(page_num)) page_num = 1;
        if (mode=="next") $("#page_num").val(++cur_page_num);
        if (mode=="prev") $("#page_num").val(--cur_page_num);
        if (mode=="top") $("#page_num").val(1);
        if (mode=="last") $("#page_num").val(last_page);
        if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
        if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
        if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
        if (mode) document.mainform.submit();
    }

    //講座作成・編集画面を開く
    function open_contents(contents_id) {
        var url = "study_contents.php?session="+session+"&event=ev_edit_contents&preview_flg=t&contents_id="+contents_id;
        window_open(url, "contents_edit", 900, 550);
    }
    // テスト実施
    function execute_test(contents_id) {
        var url = "study_test_user.php?session="+session+"&btn_event=be_test_execute&contents_id="+contents_id;
        window_open(url, "test_execute", 800, 600);
    }
    // テスト結果確認
    function show_user_answer(test_id) {
        var url = "study_test_user.php?session="+session+"&btn_event=be_show_test_user_result&contents_id="+test_id+(is_emp_proxy ? "&target_emp_id="+target_emp_id : "");
        window_open(url, "test_result", 1000, 600);
    }
    // アンケート入力
    function input_questionnaire(contents_id) {
        var url = "study_questionnaire.php?session="+session+"&btn_event=be_show_questionnaire_user&contents_id="+contents_id+(is_emp_proxy ? "&target_emp_id="+target_emp_id : "");
        window_open(url, "test_impression", 800, 580);
    }
    //集合研修作成画面を開く
    function exec_assembly_user(contents_id) {
        var url = "study_assembly.php?session="+session+ "&contents_id="+contents_id+"&event=ev_show_assembly_user";
        window_open(url, "assembly_user_list", 900, 450);
    }
    function setFolderIds(ids) {
        ee("folder_selected_ids").value = ids;
        document.mainform.submit();
    }
    $(document).ready(function() {
        if (notify_list_count && ee("notify_new_icon")) {
            ee("notify_new_icon").style.display = "";
        }
        tryFocus("keyword");
    });
    {/literal}
</script>

<iframe frameborder="0" name="download_frame" style="position:absolute; left:0; top:0; width:0; height:0"></iframe>

<form name="mainform" method="POST">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="event" id="event" value="" />
    <input type="hidden" name="folder_selected_ids" id="folder_selected_ids" value="{$folder_selected_ids}" />


{php} if (!$this->_tpl_vars['is_popup']) { {/php}
    {* 研修講義ヘッダー *}
    {php} $current_screen = "user@study_menu_user"; require_once("view/contents_header.tpl"); {/php}
    <div>
{php} } else { {/php}
    <div id="title_area"><table cellspacing="0" cellpadding="0"><tr><th><nobr>研修・講義 </nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="{$emp_lt_nm|escape} {$emp_ft_nm|escape}" /></td>
        <td><a onclick="window.close();"></a></td></tr></table>
    </div>
    <div id="title_margin">&nbsp;</div>
    <div class="usual_box">
{php} } {/php}



    <div class="findbox" style="padding:5px;">


        <div style="padding-bottom:5px">
        <table cellspacing="0" cellpadding="0" class="rgroup">
            <tr>
                <td><span>講座</span></td>
                <td>
                    <div id="ddl_folder_container"></div>
                    <script type="text/javascript">
                        folderChanged(0, 0);
                        {php}
                            $ids = explode("\t", @$this->_tpl_vars["folder_selected_ids"]);
                            for ($idx=1; $idx<=count($ids); $idx++) {
                                if ($ids[$idx-1]) echo 'folderChanged('.$idx.','.$ids[$idx-1].');';
                            }
                        {/php}
                    </script>
                </td>
            </tr>
        </table>
        </div>


        <table cellspacing="0" cellpadding="0" class="rgroup"><tr>

        <td>
            <table cellspacing="0" cellpadding="0" class="rgroup" width="100%"><tr>

                <th navititle="すべて表示" navi="実施期間外の研修講義、入力を完了した研修講義も表示します。<br>実施期間が終了していなければ、再入力が可能です。<br>実施期間が終了した場合は、行に「歴」が表示されます。"><nobr><input type="checkbox" id="disp_out_of_date" name="disp_out_of_date" value="1"{if $disp_out_of_date eq "1"} checked{/if} onclick="document.mainform.submit()"></nobr></th>

                <td navititle="すべて表示" navi="実施期間外の研修講義、入力を完了した研修講義も表示します。<br>実施期間が終了していなければ、再入力が可能です。<br>実施期間が終了した場合は、行に「歴」が表示されます。"><nobr><label for="disp_out_of_date">すべて表示</label></nobr></td>

             <td style="padding-left:30px"><nobr>年度</nobr></td>
             <td><nobr><input type="text" class="text imeoff" name="bunrui_year" style="width:50px" maxlength="4" value="{$bunrui_year|escape}" onkeydown="if (isEnterKey(this,event)) document.mainform.submit();" /></nobr></td>
             <td><nobr><span style="padding-left:20px">検索ワード</span></nobr></td>
             <td navititle="検索ワード" navi="研修講義の「タイトル」「内容」「検索キーワード」を部分一致検索します。<br>複数入力する場合はスペースで区切ってください。（AND検索） "><nobr><input type="text" class="text" name="keyword" id="keyword" style="width:300px" maxlength="50" value="{$keyword|escape}" onkeydown="if (isEnterKey(this,event)) document.mainform.submit();" /></nobr></td>
             <td><nobr><button type="button" class="btn-main" onclick="document.mainform.submit()">検索</button></nobr></td>

            </tr></table>

        </td>

        </tr></table>
    </div>

    <div style="padding:5px 0">
        <table cellspacing="0" cellpadding="0" class="rgroup"><tr>


        <td style="letter-spacing:0; width:80px;"><nobr>
            <span style="padding-right:20px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b>{$total_count}</b></span>件</span></nobr>
        </td>

        <td style="letter-spacing:0; width:80px;"><nobr>
            <span style="font-size:12px">表示件数</span>
            <select name="per_page_size" onchange="$('#page_num').val(1); document.mainform.submit()">
                {php}
                    $per_page_size = (int)$this->_tpl_vars["per_page_size"];
                    $ary = array(5,10,20,30,40,50,100,200,300,500,-1);
                    foreach ($ary as $v) {
                        echo '<option value="'.$v.'"    '.($v==$per_page_size?" selected":"").'>'.($v==-1?"全件":$v."件").'</option>';
                    }
                {/php}
            </select></nobr>
        </td>


        <td style="font-size:12px"><nobr>
            {php}
                $page_num = $this->_tpl_vars["page_num"];
                $last_page = $this->_tpl_vars["last_page"];
                $dis = ($page_num<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" />
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('top')"{php}echo $dis{/php}>≪</button>
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('prev')"{php}echo $dis{/php}>＜</button>
            <input type="text" class="text imeoff" maxlength="4" style="width:40px" id="new_page_num" value="{$page_num}"
            ><span style="font-size:12px; letter-spacing:0; padding-left:1px" class="imeoff">ページ</span>
            {php}
                $dis = ($last_page<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:38px" onclick="paging('move')"{php}echo $dis{/php}>移動</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('next')"{php}echo $dis{/php}>＞</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('last')"{php}echo $dis{/php}>≫</button></nobr>
        </td>
        </tr></table>
    </div>




    <table cellspacing="0" cellpadding="0" class="list">
        <tr>
            <th width="16" class="border0r">&nbsp;</th>
            <th class="border0l"
            navititle="一覧の表示順" navi="一覧は以下（１）⇒（２）で表示されています。<br>１）実施期限が本日以降の「研修・講義」について、実施期限が迫っているもの順（＝本日に近い順）<br>２）実施期限が昨日以前の「研修・講義」（「歴」マーク）について、実施期限が最近であるもの順（＝本日に近い順）"
            ><nobr>研修・講義</nobr></th>
            <th width="34"><nobr>年度</nobr></th>
            <th width="120"><nobr>集合研修申込</nobr></th>
            <th width="80"><nobr>集合研修実績</nobr></th>
            <th width="120"><nobr>アンケート</nobr></th>
            <th width="120"><nobr>テスト</nobr></th>
        </tr>


        {foreach from=$contents_list item=item}
        <tr class="color_hover{if $item.is_out_of_date eq "1"} is_history{/if}" style="height:46px">


            {* 履歴マーク *}
            {if $item.is_out_of_date eq "1"}
            <td class="center border0r is_history">歴</td>
            {else}
            <td class="border0r">&nbsp;</td>
            {/if}



            {* 研修・講義サマリ *}
            <td class="border0l" style="padding-left:4px;">
                <div style="color:#00aa00">{$item.folder_disp}<a href="javascript:void(0)" onclick="setFolderIds('{$item.folder_ids}'); return false;" class="green" navititle="講座で絞込" navi="クリックすると、この講座で条件を絞り込み、再検索を行います。">[絞込]</a></div>


                <div style="padding-left:10px"><a href="javascript:void(0)" onclick="open_contents({$item.id})" navititle="研修・講義タイトル" navi="クリックすると、この研修講義の概要を確認できます。<br>教材・テキストがある場合は、ここから取得できます。<br>（{php}
                $itm = $this->_tpl_vars['item'];
                if ($itm["max_end_date"]) echo "実施期限：".conv_ymdhm_to_slash_ymd($itm['max_end_date']);
                {/php}）">{$item.title|escape}</a></div>
            </td>



            {* 年度 *}
            <td class="center">{$item.bunrui_year}</td>



             {* 集合研修申込 *}
            <td class="center">
            {php}
                $itm = $this->_tpl_vars['item'];
                $is_emp_proxy = $this->_tpl_vars['is_emp_proxy'];
                if (!isset($itm["policy_info"]["assembly"])) {
                    echo '<span class="gray" navi="集合研修の実施はありません">―</span>'; // 対象外の場合
                } else {
                    // 期間開始後の場合
                    if ($itm["assembly_start_date"] && $itm["assembly_start_date"] <= date("YmdHi") && $itm["assembly_count"] > 0) {
                        // 期間中は編集可能。期間終了になれば入力終了（履歴化・リードオンリーとなる）。
                        $is_in_kikan = ($itm["assembly_end_date"] >= date("YmdHi"));
                        if ($is_in_kikan && !$is_emp_proxy) echo '<a href="javascript:void(0)" onclick="exec_assembly_user('.$itm["id"].')">';
                        if ($itm['assembly_status']=="1") echo '出席<br/><span class="fontsize12">'.date("Y/m/d",strtotime($itm['assembly_date']))."</span>";
                        else if ($itm['assembly_status']=="2") echo "欠席";
                        else if ($is_emp_proxy) echo '未登録';
                        else if ($is_in_kikan) echo "申し込む";
                        else echo '<span class="gray">受付終了</span>';
                        if ($is_in_kikan && !$is_emp_proxy) echo '</a>';
                        if ($itm['assembly_status']!="1" && $itm['assembly_status']!="2") {
                            echo '<br />';
                            echo '<span class="mini_prefix_green">期限&nbsp;<span class="gray">';
                            echo conv_ymdhm_to_slash_ymd($itm["assembly_end_date"]) . '</span></span>';
                        }
                    }
                }
            {/php}
            </td>



             {* 集合研修実績。実績は管理者が登録。この状態を表示する。 *}
            <td class="center">
            {php}
                $itm = $this->_tpl_vars['item'];
                if (!isset($itm["policy_info"]["assembly"])) {
                    echo '<span class="gray" navi="集合研修の実施はありません">―</span>'; // 対象外の場合
                } else {
                    if ($itm["assembly_actual_status"]=="1") echo '出席済<br/><span class="fontsize12">'.conv_ymd($itm["assembly_actual_date"]).'</span>';
                    else echo '<span class="gray">(実績なし)</span>';
                }
            {/php}
            </td>



            {* アンケート *}
            <td class="center">
            {php}
                $itm = $this->_tpl_vars['item'];
                $is_in_kikan = ($itm["questionnaire_end_date"] >= date("YmdHi"));
                $questionnaire_status =  ((int)$itm["questionnaire_status"]);
                $is_emp_proxy = $this->_tpl_vars['is_emp_proxy'];
                $policy_for_questionnaire = $itm["policy_for_questionnaire"]; // "A+"集合研修出席で表示／"A-"集合研修欠席で表示、の意味
                $is_extra_hide = ($policy_for_questionnaire=="A+" && $itm["assembly_actual_status"]!="1");
                $is_extra_show = ($policy_for_questionnaire=="A-" && $itm["assembly_actual_status"]=="1");

                if (!isset($itm["policy_info"]["questionnaire"])) {
                    echo '<span class="gray" navi="アンケートの実施はありません">―</span>'; // 対象外の場合
                } else {
                    // 期間開始後の場合
                    if ($itm["questionnaire_start_date"] && $itm["questionnaire_start_date"] <= date("YmdHi") && $itm["questionnaire_count"] > 0) {
                        if ($is_extra_hide) {
                            echo '<span class="gray" navi="集合研修出席後にアンケートをお願いいたします。<br>欠席者は、回答いただく必要はありません。">※</span>'; // 対象外の場合
                        } else if ($is_extra_show) {
                            echo '<span class="gray" navi="集合研修出席者は、回答いただく必要はありません。">※</span>'; // 対象外の場合
                        } else {
                            // 期間中は編集可能。期間終了になれば入力終了（履歴化・リードオンリーとなる）。
                            if (!$is_emp_proxy || $questionnaire_status) {
                                echo '<a href="javascript:void(0)" onclick="input_questionnaire('.$itm["id"].')"';
                                if ($questionnaire_status || !$is_in_kikan) echo ' class="custom_visited"';
                                echo '>';
                            }

                            if ($questionnaire_status) echo '回答済';
                            else if ($is_emp_proxy) echo '未回答';
                            else if ($is_in_kikan) echo "回答する";
                            else echo '期間終了';

                            if (!$is_emp_proxy || $questionnaire_status) echo '</a>';
                            echo '<br />';
                            echo '<span class="mini_prefix_green">期限&nbsp;<span class="gray">';
                            echo conv_ymdhm_to_slash_ymd($itm["questionnaire_end_date"]) . '</span></span>';
                        }
                    }
                    // 未来に実施する場合
                    else if ($itm["questionnaire_start_date"] > date("YmdHi") && $itm["questionnaire_count"] > 0) {
						echo '<span class="gray">(実施予定)</span>';
					}
                }
            {/php}
            </td>



            {* テスト実施 *}
            <td class="center">
            {php}
                $itm = $this->_tpl_vars['item'];
                $is_in_kikan = ($itm["test_end_date"] >= date("YmdHi"));
                $is_emp_proxy = $this->_tpl_vars['is_emp_proxy'];
                $percentage = (int)$itm["percentage"];
                $border_score = (int)$itm["border_score"];
                $test_status = (int)$itm["test_status"];
                $test_date = $itm["test_date"];
                $policy_for_test = $itm["policy_for_test"]; // "A+"集合研修出席で表示／"A-"集合研修欠席で表示、の意味
                $is_extra_hide = ($policy_for_test=="A+" && $itm["assembly_actual_status"]!="1");
                $is_extra_show = ($policy_for_test=="A-" && $itm["assembly_actual_status"]=="1");
                if (!isset($itm["policy_info"]["test"])) {
                    echo '<span class="gray" navi="テストの実施はありません">―</span>'; // 対象外の場合
                } else {
                    // 期間内の場合
                    if ($itm["test_start_date"] && $itm["test_start_date"] <= date("YmdHi") && $itm["test_count"] > 0) {
                        if ($is_extra_hide) {
                            echo '<span class="gray" navi="集合研修出席後にテストをお願いいたします。">※</span>'; // 対象外の場合
                        } else if ($is_extra_show) {
                            echo '<span class="gray" navi="集合研修出席者は、テストを実施いただく必要はありません。">※</span>'; // 対象外の場合
                        } else {
                            // 期間中は編集可能。期間終了になれば入力終了（履歴化・リードオンリーとなる）。
                            if (!$is_emp_proxy) {
                                echo '<a href="javascript:void(0)" onclick="execute_test('.$itm["test_id"].')"';
                                if ($test_status==2 || !$is_in_kikan) {
                                    echo ' class="custom_visited"';
                                }
                                if ($test_status) {
                                    echo ' navititle="再度テストを実施する" navi="期限：'.conv_ymdhm_to_slash_ymd($itm["test_end_date"]);
                                    echo '<br>最終正答率'.$percentage."％ / ".($border_score ? "正答".$border_score.'％以上で合格"' : '実施のみで合格"');
                                }
                                echo '>';
                            }

                            if ($test_status==1) echo "実施中";
                            else if ($test_status==2) echo '実施済（合格）';
                            else if ($is_emp_proxy) echo '未実施';
                            else if ($is_in_kikan) echo "受ける";
                            else echo '期間終了';

                            if (!$is_emp_proxy) echo '</a>';
                            if ($test_status) {
                                echo '<br />';
                                echo '<a href="javascript:void(0)" onclick="show_user_answer('.$itm["test_id"].')"';
                                echo ' class="custom_visited fontsize12" navi="解答・正答・解説を確認できます">';
                                echo '前回&nbsp;'. date("Y/m/d", strtotime($itm["test_date"]))."</a>";
                            }  else {
                                echo '<br />';
                                echo '<span class="mini_prefix_green">期限&nbsp;<span class="gray">';
                                echo conv_ymdhm_to_slash_ymd($itm["test_end_date"]) . '</span></span>';
                            }
                        }
                    }
                    // 未来に実施する場合
                    else if ($itm["test_start_date"] > date("YmdHi") && $itm["test_count"] > 0) {
						echo '<span class="gray">(実施予定)</span>';
					}
                }
            {/php}
            </td>


        </tr>
        {/foreach}
    </table>


    {if not $contents_list|@count}
    <div style="padding:10px 5px" class="gray2">該当データは存在しません。</div>
    {/if}

</div>
</form>

</body>
</html>
