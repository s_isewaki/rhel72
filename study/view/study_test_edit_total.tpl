{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}問題別正答率表</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_test_edit  -->




<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>問題別正答率表</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents_title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>



<script type='text/javascript'>
function downloadFile(btn_event){literal}{{/literal}
{php}
    global $session;
    echo 'var session="' . $session . '";'."\n";
    echo 'var contents_id = ' . $_REQUEST['contents_id'] . ";\n";
    echo 'var contents_title = "' . str_replace('"','\"',$this->_tpl_vars["contents_title"]) . '";';
{/php}
{literal}
    var ext = (btn_event=="be_test_csv" ? ".csv" : ".xls");
    document.getElementById("iframe_download").src ="csv_download/"+session+"/"+contents_id+"/"+btn_event+"/"+encodeURIComponent(contents_title) + ext;
}{/literal}
</script>

<iframe id="iframe_download" src="../img/spacer.gif" style="position:absolute; width:100px; height:100px; left:-200px" tabIndex="-1"></iframe>
<div class="usual_box">
<form name="sub_main" method="get" target="_self">
    <div style="text-align:right; padding-bottom:4px">
        <button type="button" class="btn-main" name="btn_event" value="be_test_csv" onclick="downloadFile('be_test_csv')">テストデータ出力(CSV)</button>
        <button type="button" class="btn-main" name="btn_event" value="be_test_excel" onclick="downloadFile('be_test_excel')">テスト結果出力(EXCEL)</button>
    </div>

    <table cellspacing="0" cellpadding="0" class="list fixed letterspace2">
        <tr>
            <th width="8%" rowspan="2"></th>
            <th width="23%" colspan="2">１回目</th>
            <th width="23%" colspan="2">２回目</th>
            <th width="23%" colspan="2">３回目</th>
            <th width="23%" colspan="2">最終回</th>
        </tr>
        <tr>
            <th width="16%" class="fontsize12"><nobr>合格/実施者数</nobr></th>
            <th width="7%"><nobr>正答率</nobr></th>
            <th width="16%" class="fontsize12"><nobr>合格/実施者数</nobr></th>
            <th width="7%"><nobr>正答率</nobr></th>
            <th width="16%" class="fontsize12"><nobr>合格/実施者数</nobr></th>
            <th width="7%"><nobr>正答率</nobr></th>
            <th width="16%" class="fontsize12"><nobr>合格/実施者数</nobr></th>
            <th width="7%"><nobr>正答率</nobr></th>
        </tr>
        {php}
            $percents = $this->_tpl_vars["percents"];
            $bunbun = $this->_tpl_vars["bunbun"];
            for ($no=1; $no<=count($percents); $no++) {
                $caption = "問題".$no;
                if ($no==count($percents)) $caption = "平均";
        {/php}
        <tr>
            <td width="20%" class="center"><nobr>{php} echo $caption {/php}</nobr></td>
            <td width="15%" class="center">{php} echo $bunbun[$no][1]{/php}</td><td width="8%" class="right"><b>{php} echo $percents[$no][1]{/php}</b></td>
            <td width="15%" class="center">{php} echo $bunbun[$no][2]{/php}</td><td width="8%" class="right"><b>{php} echo $percents[$no][2]{/php}</b></td>
            <td width="15%" class="center">{php} echo $bunbun[$no][3]{/php}</td><td width="8%" class="right"><b>{php} echo $percents[$no][3]{/php}</b></td>
            <td width="15%" class="center">{php} echo $bunbun[$no][4]{/php}</td><td width="8%" class="right"><b>{php} echo $percents[$no][4]{/php}</b></td>
        </tr>
        {php} } {/php}
    </table>
</form>
</div>
</body>
</html>
