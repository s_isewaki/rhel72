<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<style type="text/css">
    .percent { mso-style-parent:style0; mso-number-format:"0\.0%";  }
</style>
</head>
<body>
    <!-- ///study_contents_syozokubetu_excel  -->
    <table id="emp_list_header" cellspacing="0" cellpadding="0" border="1px solid #000000">
        <tr>
          <th style="background-color:#ffe7d2"><nobr><?=h(GG_CLASS_NM)?></nobr></th>
          <th style="background-color:#ffe7d2"><nobr><?=h(GG_ATRB_NM)?></nobr></th>
          <th style="background-color:#ffe7d2"><nobr><?=h(GG_DEPT_NM)?></nobr></th>
          <? foreach ($make_fields as $mfield) { ?>
              <? if ($mfield["disp"]!="yes") continue; ?>
              <th style="background-color:#ffe7d2"><nobr><?=h($mfield["jp"])?></nobr></th>
          <? } ?>
        </tr>

        <? // class別ループ ---------------------------------------------- ?>
        <? foreach ($data_hier as $class_id => $data_hier2) { ?>
            <? $class_counter = 0; ?>
            <? $class_row_count = $section_row_count[$class_id]; ?>
            <? // atrb別ループ ---------------------------------------------- ?>
            <? foreach ($data_hier2 as $atrb_id => $data_hier3) { ?>
                <? $attr_counter = 0; ?>
                <? $atrb_row_count = $section_row_count[$class_id."_".$atrb_id]; ?>


                <? // dept別ループ ---------------------------------------------- ?>
                <? foreach ($data_hier3 as $dept_id => $data_hier4) { ?>
                    <? $class_counter++; ?>
                    <? $attr_counter++; ?>
                    <? $data_row = $data_list[$class_id."_".$atrb_id."_".$dept_id]; ?>
                    <? $section_name = $section_names[$class_id."_".$atrb_id."_".$dept_id]; ?>
                    <tr>
                    <? if ($class_counter==1) { ?>
                    <th rowspan="<?=$class_row_count?>" style="background-color:#ffe7d2"><nobr><?=h($section_name["class_nm"])?></nobr></th>
                    <? } ?>
                    <? if ($attr_counter==1) { ?>
                    <th rowspan="<?=$atrb_row_count?>" style="background-color:#ffe7d2"><nobr><?=h($section_name["atrb_nm"])?></nobr></th>
                    <? } ?>
                    <th style="background-color:#ffe7d2"><nobr><?=h($section_name["dept_nm"])?></nobr></th>
                    <? foreach ($make_fields as $mfield) { ?>
                        <? if ($mfield["disp"]!="yes") continue; ?>
                        <td class="percent" align="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
                    <? } ?>
                    </tr>
                <? } ?>

                <? // atrb別小計（dept合計）?>
                <? $data_row = $data_list[$class_id."_".$atrb_id]; ?>
                <? $section_name = $section_names[$class_id."_".$atrb_id]; ?>
                <tr>
                <th style="background-color:#ffe7d2"><nobr>小計</nobr></th>
                <? foreach ($make_fields as $mfield) { ?>
                    <? if ($mfield["disp"]!="yes") continue; ?>
                    <td class="percent" align="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
                <? } ?>
                </tr>
            <? } ?>

            <? // ---------------------------------------------- class別小計（atrb合計）?>
            <? $data_row = $data_list[$class_id]; ?>
            <? $section_name = $section_names[$class_id]; ?>
            <tr>
            <th style="background-color:#ffe7d2" colspan="2"><nobr>小計</nobr></th>
            <? foreach ($make_fields as $mfield) { ?>
                <? if ($mfield["disp"]!="yes") continue; ?>
                <td class="percent" align="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
            <? } ?>
            </tr>
        <? } ?>

        <? // ---------------------------------------------- 全体合計（class合計）?>
        <? $data_row = $data_list["last_total"]; ?>
        <tr>
        <th style="background-color:#ffe7d2" colspan="3"><nobr>合計</nobr></th>
        <? foreach ($make_fields as $mfield) { ?>
            <? if ($mfield["disp"]!="yes") continue; ?>
            <td class="percent" align="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
        <? } ?>
        </tr>

    </table>
</body>
</html>

