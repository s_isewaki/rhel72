{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}研修・講義管理</title>
    <link rel="stylesheet" type="text/css" href="css/jquery.treeview.css" />
    <script type="text/javascript" src="js/jquery.treeview.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_main  -->




<script type="text/javascript">
    var init_viewing = 1;
    var notify_list_count = {$notif_list_count};
    {literal}

    function paging(mode) {
        var cur_page_num = ee("cur_page_num2").value;
        var cur_per_page_size = ee("per_page_size2").value;
        var total_count = ee("total_count2").value;
        var last_page = ee("last_page2").value;
        var page_num = parseInt($("#page_num").val());
        var new_page_num = parseInt($("#new_page_num2").val());
        if (last_page < 1) last_page = 1;
        if (!page_num || isNaN(page_num)) page_num = 1;
        if (mode=="next") $("#page_num").val(++cur_page_num);
        if (mode=="prev") $("#page_num").val(--cur_page_num);
        if (mode=="top") $("#page_num").val(1);
        if (mode=="last") $("#page_num").val(last_page);
        if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
        if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
        if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
        if (mode) getListContentsViaAjax();
    }
    // ディレクトリ移動
    function change_directory(action, target) {
        if (!init_viewing) {
            if (ee("page_num")) ee("page_num").value = "1";
        }
        var folder_id = "";
        if ("down" == action) folder_id = target.attr("folder_id"); // 一覧の行から取得
        else if ("up" == action) folder_id = $(".selected_fileitem").attr("folder_id"); // ツリーから取得
        else return;
        $("#folder_tree_folder_" + folder_id).trigger("click");
        up_control(folder_id);
    }

    // 上の階層へボタンの制御
    function up_control(folder_id) {
        if (!init_viewing) {
            if (ee("page_num")) ee("page_num").value = "1";
        }
        if ($("#folder_tree_folder_" + folder_id).attr("parent_id") == "") {
            $("#up_btn").attr("disabled", "disabled").addClass("btn-off");
        } else {
            $("#up_btn").removeAttr("disabled").removeClass("btn-off");
        }
    }

    // ツリーをクリック
    function tree_click($target) {
        $("[name=selected_folder_id]").val($target.attr("folder_id")); // 講座のIDをhiddenに保持
        $("[name=parent_folder_id]").val($target.attr("parent_id")); // 講座の親IDをhiddenに保持
        $(".selected_fileitem").removeClass("selected_fileitem").addClass("unselected_fileitem");
        $target.removeClass("unselected_fileitem").addClass("selected_fileitem");
        up_control($target.attr("folder_id"));
        if (!init_viewing) {
            if (ee("page_num")) ee("page_num").value = "1";
        }
        getListContentsViaAjax();
    }

    // 研修・講義タイトル テーブルソート（プルダウン型）
    function change_sort2(sort_string) {
        if (ee("page_num")) ee("page_num").value = "1";
        ee("sort_string2").value = sort_string;
        getListContentsViaAjax();
    }
    function change_ext_joken(ext_joken) {
        ee("ext_joken").value = ext_joken;
        getListContentsViaAjax();
    }
    // Ajax送信処理
    function getListContentsViaAjax() {
        if (ee("cur_page_num2")) ee("cur_page_num").value = ee("cur_page_num2").value;
        if (ee("cur_per_page_size2")) ee("cur_per_page_size").value = ee("cur_per_page_size2").value;
        if (ee("new_page_num2")) ee("new_page_num").value = ee("new_page_num2").value;
        if (ee("per_page_size2")) ee("per_page_size").value = getSelectionValue(ee("per_page_size2"));
        if (ee("last_page2")) ee("last_page").value = ee("last_page2").value;
        if (ee("total_count2")) ee("total_count").value = ee("total_count2").value;
        if (ee("sort_string2")) ee("sort_string").value = ee("sort_string2").value;
        var page_num = (ee("page_num") ? ee("page_num").value : 1);
        var new_page_num = (ee("new_page_num") ? ee("new_page_num").value : 1);
        var per_page_size = (ee("per_page_size") ? ee("per_page_size").value : 10);
        var sendData = {
            session:session,
            page_num:page_num,
            ext_joken:ee("ext_joken").value,
            new_page_num:new_page_num,
            per_page_size:per_page_size,
            event:"ev_tree_click", // 講座クリックと同一イベントとして処理
            selected_folder_id:$(".selected_fileitem").attr("folder_id"),
            sort_string:ee("sort_string").value,
            disp_out_of_date:"1"
        };
        $.ajax({
            type:"GET", url:"study_main.php?t="+(new Date()), data:sendData, async:false,
            before:function(xhr) {
                xhr.overrideMimeType("text/html;charset=EUC-JP");
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                var msg = "システムエラーが発生しました。\n管理者にお問い合わせください。";
                if (errorThrown != null) msg += "\n error=" + errorThrown;
                if (textStatus != null) msg += "\n status" + textStatus;
                alert(msg);
            },
            success:function(data, dataType) {
                ee("contents_wrapper").innerHTML = data;
            }
        });
    }
















    // 講座編集
    function edit_folder(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if ($(".selected_fileitem").size() > 0) open_folder_edit("ev_edit_folder"); // ツリーで選択されてなければ何もしない
    }
    function create_folder(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        open_folder_edit("ev_create_folder"); // ツリーで選択されてなければ何もしない
    }

    // 講座作成・編集画面を開く
    function open_folder_edit(event) {
        var selected_folder_id = $(".selected_fileitem").attr("folder_id");
        var parent_folder_id = $(".selected_fileitem").attr("parent_id");
        var url = "study_folder.php?session="+session+"&event="+event+"&call_from=study_main";
        if ($(".selected_fileitem").size() > 0) {
            url += "&selected_folder_id="+selected_folder_id+"&parent_folder_id="+parent_folder_id+"&current_folder_id="+selected_folder_id;
        }
        window_open(url, "folder_edit", 600, 380);
    }
    // 講義内検索を開く
    function search_contents() {
        var url  = "study_main.php?session="+session+"&event=ev_contents_search";
        window_open(url, "contents_search", 800, 500);
    }
    function open_contents_create(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        open_contents_edit('ev_create_contents', '');
    }
    // 講座作成・編集画面などを開く
    function open_contents_edit(event, contents_id) {
        var url="study_contents.php?session="+session+"&event="+event+"&save_folder_id="+$(".selected_fileitem").attr("folder_id")+"&contents_id="+contents_id;
        window_open(url, "contents_edit2", 900, 600);
    }
    // 受講状況画面を開く
    function show_contents_summary(contents_id) {
        var url = "study_contents.php?session="+session+"&btn_event=be_show_contents_summary&contents_id="+contents_id;
        window_open(url, "wn_contents_summary", 700, 550);
    }
    // テスト作成・編集画面を開く
    function show_test_edit(contents_id) {
        var url = "study_test_edit.php?session="+session+"&btn_event=be_show_from_menu&contents_id="+contents_id;
        window_open(url, "wn_test_edit", 1000, 650);
    }
    // 問題別正答率(％)表を開く
    function show_test_edit_total(contents_id) {
        var url = "study_test_edit.php?session="+session+"&btn_event=be_show_test_edit_total&contents_id="+contents_id;
        window_open(url, "wn_test_edit_total", 900, 650);
    }
    // 教材・テキストダウンロード
    function show_download_dialog(contents_id) {
        var url = "study_contents.php?session="+session+"&btn_event=be_show_download_dialog&contents_id="+contents_id;
        window_open(url, "download_dialog", 800, 500);
    }
    // アンケート結果表示
    function result_questionnaire(contents_id) {
        var url = "study_questionnaire.php?session="+session+"&btn_event=be_result_questionnaire&contents_id="+contents_id;
        window_open(url, "result_impression", 900, 650);
    }
    //受講者設定画面を開く
    function setting_student(contents_id, assembly_actual_status) {
        if (!assembly_actual_status) assembly_actual_status = "";
        var url = "study_student.php?session="+session+ "&contents_id="+contents_id+"&assembly_actual_status="+assembly_actual_status;
        window_open(url, "setting_student", 1000, 650);
    }
    //集合研修作成画面を開く
    function create_assembly(contents_id) {
        var url = "study_assembly.php?session="+session+ "&contents_id="+contents_id;
        window_open(url, "student_list", 1000, 650);
    }
    //お知らせ作成画面を開く
    function notice_history(contents_id) {
        var url = "study_notif_admin.php?session="+session+ "&contents_id="+contents_id;
        window_open(url, "notice_history", 1000, 620);
    }
    //アンケート作成画面を開く
    function create_questionnaire(contents_id) {
        var url = "study_questionnaire.php?session="+session+"&contents_id="+contents_id;
        window_open(url, "questionnaire", 900, 650);
    }

    //受講一覧を出力する
    function export_csv() {
        $("#export_csv_btn").val("export_csv");
        document.mainform.event.value ="export_csv";
        document.mainform.submit();
    }
    // body_onload
    $(document).ready(function() {
        $("a" + "[type=tree]" + "[id^=folder_tree]").click(function() { tree_click($(this)); }); // 講座選択のイベントハンドラ（ツリー)
        // 上へボタン 現在選択中のツリーをクリックする
        $("#up_btn").click(function() {
            var parent_id = $(".selected_fileitem").attr("parent_id");
            if (parent_id != "") $("#folder_tree_folder_" + parent_id).trigger("click");
        });

        $("#folder_tree").treeview({ collapsed:false, animated:"fast" }); // ツリー初期表示

        // 選択講座がなかったら
        // ルート講座に対してクリックイベントを発行
        if ($(".selected_fileitem").size() == 0) {
            $("a" + "[root=true]").each(function() { $(this).trigger("click"); return false; });
        } else {
            $(".selected_fileitem").trigger("click"); // 選択講座に対してクリックイベント発行
        }
        if (notify_list_count && ee("notify_new_icon")) {
            ee("notify_new_icon").style.display = "";
        }
        init_viewing = 0;
    });
    {/literal}
</script>






{* 研修・講義ヘッダー *}
{php} $current_screen = "user@study_main"; require_once("view/contents_header.tpl"); {/php}



<form name="mainform" method="POST" action="study_main.php?t={php} echo date("YmdHis"); {/php}" onsubmit="alert('ok');">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="event" id="event" value="" />
    <input type="hidden" name="selected_folder_id" value="" />
    <input type="hidden" name="parent_folder_id" value="" />
    <input type="hidden" name="contents_id" />
    <input type="hidden" id="page_num" name="page_num" value="{php} echo h(@$_REQUEST["page_num"]);{/php}" />
    <input type="hidden" id="new_page_num" name="new_page_num" value="{php} echo h(@$_REQUEST["new_page_num"]);{/php}" />
    <input type="hidden" id="per_page_size" name="per_page_size" value="{php} echo h(@$_REQUEST["per_page_size"]);{/php}" />
    <input type="hidden" id="cur_page_num" name="cur_page_num" value="{php} echo h(@$_REQUEST["cur_page_num"]);{/php}" />
    <input type="hidden" id="cur_per_page_size" name="cur_per_page_size" value="{php} echo h(@$_REQUEST["cur_per_page_size"]);{/php}" />
    <input type="hidden" id="total_count" name="total_count" value="{php} echo h(@$_REQUEST["total_count"]);{/php}" />
    <input type="hidden" id="last_page" name="last_page" value="{php} echo h(@$_REQUEST["last_page"]);{/php}" />
    <input type="hidden" id="sort_string" name="sort_string" value="{php} echo h(@$_REQUEST["sort_string"]);{/php}" />
    <input type="hidden" id="ext_joken" name="ext_joken" value="{php} echo h(@$_REQUEST["ext_joken"]);{/php}" />
</form>

    <div style="padding:5px 0">
    <table style="width:100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="vertical-align:top; width:226px;">
    {if $disp_auth}
                <div style="height:28px">
                    {* ボタン *}
                    <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
                    <td style="padding-left:0; width:220px"><nobr>
                        <button type="button" class="btn-main" style="width:64px" id="up_btn">上階層へ</button><!--
                        --><button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" style="width:74px" id="create_folder_btn" onclick="create_folder(this)">講座登録</button><!--
                        --><button type="button" class="btn-main{if not $edit_auth || $folder_list|@count == 0} btn-off{/if}" style="width:74px" id="update_folder_btn" onclick="edit_folder(this)">講座修正</button>
                        </nobr>
                    </td>
                    </tr></table>
                </div>

    {if $folder_list|@count != 0}
                <div style="background-color:#ffe7d2; border:1px solid #f2995e; padding:4px; text-align:center; height:18px">講座</div>
                <div style="background-color:#ffffff; overflow-x:hidden; overflow-y:scroll; border:1px solid #bea89c; border-top:0; padding-bottom:10px">
                    {tree folder_list=$folder_list tree_id="folder_tree" selected_folder_id=$selected_folder_id event="" }
                </div>
    {/if}
    {/if}
            </td>
            <td style="vertical-align:top">
                {* 研修・講義一覧 ここに admin_menu_table.tplが来る *}
                <div id="contents_wrapper"></div>
            </td>
        </tr>
    </table>
    </div>

    {if !$disp_auth}
    <div class="warningbox">講座への教育担当者権限、または教育責任者権限がありません。</div>
    {/if}


<form name="dummyform" id="dummyform" method="POST" target="dummy_iframe">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
</form>
<iframe frameborder="0" name="dummy_iframe" id="dummy_iframe" style="position:absolute; left:0; top:0; width:0; height:0"></iframe>

</body>
</html>