<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>受講者管理</title>
</head>
<body onresize="resized()">
<!-- ///study_student_list  -->




<script type="text/javascript">
    var contents_id = "<?=$contents_id?>";
    function paging(mode) {
        var cur_page_num = <?=$page_num?>;
        var cur_per_page_size = <?=$per_page_size?>;
        var total_count = <?=(int)@$total_count?>;
        var page_num = parseInt($("#page_num").val());
        var new_page_num = parseInt($("#new_page_num").val());
        var last_page = <?=$last_page?>;
        if (last_page < 1) last_page = 1;
        if (!page_num || isNaN(page_num)) page_num = 1;
        if (mode=="next") $("#page_num").val(++cur_page_num);
        if (mode=="prev") $("#page_num").val(--cur_page_num);
        if (mode=="top") $("#page_num").val(1);
        if (mode=="last") $("#page_num").val(last_page);
        if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
        if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
        if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
        if (mode) document.form_sub.submit();
    }

    // テスト結果(リードオンリー)
    function show_user_answer(emp_id) {
        var url = "study_test_user.php?session="+session+"&btn_event=be_show_test_user_result&contents_id="+contents_id+"&target_emp_id="+emp_id;
        window_open(url, "test_result", 1000, 600);
    }
    // アンケート入力（リードオンリー）
    function show_questionnaire(emp_id) {
        var url = "study_questionnaire.php?session="+session+"&btn_event=be_show_questionnaire_user&contents_id="+contents_id+"&target_emp_id="+emp_id;
        window_open(url, "test_impression", 800, 580);
    }
    // 集合研修 実績 インポート ダイアログを開く
    function show_assembly_import(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url = "study_assembly.php?session=" + session + "&event=ev_show_assembly_import&contents_id="+contents_id;
        window_open(url, "show_assembly_import", 700, 420);
    }
    // 受講者CSVインポートダイアログを開く
    function import_student(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url = "study_student.php?session=" + session + "&btn_event=be_show_student_import_csv&contents_id=" + contents_id;
        window_open(url, "emp_import", 700, 400);
    }
    function export_student(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url = "study_student.php?session="+session + "&btn_event=be_show_student_export_csv&contents_id=" + contents_id;
        window_open(url, "emp_export", 600, 200);
    }
    // 職員選択画面を開く
    function open_emp_select(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url = "study_student.php?btn_event=be_show_student_selector&session="+session+"&contents_id="+contents_id+"&mode="+$("[name=hidden_mode]").val()+
        "&js_callback=exec_add_employee";
        window_open(url, "emp_select", 800, 580);
    }
    // 集合研修申込者名簿EXCEL出力
    function export_excel(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url = "study_student.php?session="+session+"&btn_event=be_get_application_apply_count&contents_id="+contents_id+"&t="+(new Date());
        $.ajax({
          url:url, type:'GET', async:false, success:function(msg) {
            var ret = msg.split("\t");
            if (ret[0]!="ok") { alert(msg); return; }
            if (!ret[1] || ret[1]=="0") return alert("集合研修への申込者は存在しません。");
            document.form_excel.submit();
          }
        });
    }
    function exec_add_employee(emp_id_list) {
        var url = "study_student.php?session="+session+"&contents_id="+contents_id+"&t="+(new Date());
        var data = "btn_event=be_add_employee&emp_id_comma_list="+emp_id_list.join(",");
        $.ajax({
          url:url, type:'POST', async:false, data:data, success:function(msg) {
            if (msg!="ok") { alert(msg); return; }
            document.form_sub.result.value = "success";
            document.form_sub.submit();
          }
        });
    }
    //出欠登録画面を開く
    function show_actual_entry(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url = "study_assembly.php?session="+session+"&event=ev_show_assembly_actual_entry";
        window_open(url, "", 1010, 660);
    }
    function get_actual_entry_contents_info() {
        return {contents_id:"<?=$contents_id?>","contents_title":"<?=h($contents["title"])?>"};
    }


    // 受講者の削除
    function delete_student(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if ($("input:checkbox[name='emp_id']:checked").size() == 0) {
            alert("削除対象の受講者チェックボックスが選択されていません。");
            return;
        }
        var ary = document.getElementsByName("emp_id");
        var emp_id_list = [];
        for (var idx=0; idx<ary.length; idx++) {
            var elem = ary[idx];
            if (elem.checked) emp_id_list.push(ary[idx].value);
        }
        if (!confirm("チェックを行った受講者を削除します。\nひとつでも実施済みである受講者は、削除されません。\n\n削除してよろしいですか?")) return;

        var url = "study_student.php?session="+session+"&contents_id="+contents_id+"&t="+(new Date());
        var data = "btn_event=be_del_employee&emp_id_comma_list="+emp_id_list.join(",");
        $.ajax({
          url:url, type:'POST', async:false, data:data, success:function(msg) {
            if (msg!="ok") { alert(msg); return; }
            document.form_sub.result.value = "success";
            document.form_sub.submit();
          }
        });
    }


    // 受講者のステータス変更
    function student_change(btn, btn_event, new_status) {
        var status_param = "";
        if (btn.className.indexOf("btn-off")>=0) return;
        if ($("input:checkbox[name='emp_id']:checked").size() == 0) {
            alert("対象受講者チェックボックスが選択されていません。");
            return;
        }
        var ary = document.getElementsByName("emp_id");
        var emp_id_list = [];
        for (var idx=0; idx<ary.length; idx++) {
            var elem = ary[idx];
            if (elem.checked) emp_id_list.push(ary[idx].value);
        }
        if (btn_event=="be_change_assembly_actual_status") {
            if (new_status=="" && !confirm("チェックを行った受講者の出欠実績をクリアします。よろしいですか?")) return;
            var msg = "チェックを行った受講者の出欠実績を、欠席状態にします。\n\n登録してよろしいですか?";
            if (new_status=="2" && !confirm(msg)) return;
            status_param = "&assembly_actual_status="+new_status;
        }
        
        
        if (btn_event=="be_change_assembly_status" && new_status=="2" ) {
           
            status_param = "&assembly_status="+new_status;
        }
        
        
        if (btn_event=="be_change_questionnaire_status" ) {
            if (!confirm("チェックを行った受講者がアンケート回答済みである場合、\nアンケートを抹消し、未回答の状態に変更します。よろしいですか?")) return;
        }

        if (btn_event=="be_clear_test_result") {
            if (!confirm("チェックを行った受講者がテスト受講済みである場合、\nテスト結果とテスト履歴を抹消し、未受講の状態に変更します。よろしいですか?")) return;
        }

        ee("emp_id_comma_list").value = emp_id_list.join(",");

        if (btn_event=="be_change_assembly_actual_status" && new_status=="1") {
            var url = "study_assembly.php?session=" + session + "&event=ev_show_assembly_date&contents_id="+contents_id;
            window_open(url, "show_assembly_date", 650, 400);
            return;
        }

        var url = "study_student.php?session="+session+"&contents_id="+contents_id+"&t="+(new Date());
        var data = "btn_event="+btn_event+"&emp_id_comma_list="+emp_id_list.join(",")+status_param;
        $.ajax({
          url:url, type:'POST', async:false, data:data, success:function(msg) {
            if (msg!="ok") { alert(msg); return; }
            document.form_sub.result.value = "success";
            document.form_sub.submit();
          }
        });
    }



    $(document).ready(function() {
        <? if ($result=="success") { ?>
        if (opener && opener.document) {
            if (opener.document.mainform) parentRefreshRequest("mainform");
            if (opener.document.form_main) parentRefreshRequest("form_main"); // 集合研修画面
            else if (opener.document.sub_main) parentRefreshRequest("sub_main");
            grandParentRefreshRequest("mainform");
            setTimeout("window.focus()", 500);
        }
        <? } ?>
        setComboColor(ee("assembly_date_no"));
        setComboColor(ee("assembly_actual_status"));
        setComboColor(ee("questionnaire_status"));
        setComboColor(ee("test_status"));
        setComboColor(ee("total_status"));
    });

    // 職員のチェックボックスを制御
    function allCheckEmp(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var caption = $("#all_check_emp").html();
        var ary = document.getElementsByName("emp_id");
        var bool = (caption == "選択");
        for (var idx=0; idx<ary.length; idx++) ary[idx].checked = bool;
        $("#all_check_emp").html(bool?"解除":"選択");
    }
    function tryExportCsv(id_type) {
        document.form_export_csv.id_type.value = id_type;
        document.form_export_csv.emp_class.value = getSelectionValue(document.form_sub.emp_class);
        document.form_export_csv.emp_attr.value = getSelectionValue(document.form_sub.emp_attr);
        document.form_export_csv.emp_dept.value = getSelectionValue(document.form_sub.emp_dept);
        document.form_export_csv.emp_room.value = getSelectionValue(document.form_sub.emp_room);
        document.form_export_csv.emp_personal_id.value = document.form_sub.emp_personal_id.value;
        document.form_export_csv.emp_name.value = document.form_sub.emp_name.value;
        document.form_export_csv.emp_job.value = getSelectionValue(document.form_sub.emp_job);
        document.form_export_csv.emp_status.value = getSelectionValue(document.form_sub.emp_status);
        
        <? if ($policy_info["assembly"]) { ?>
        	document.form_export_csv.no.value = getSelectionValue(document.form_sub.no);
        	document.form_export_csv.assembly_actual_status.value = getSelectionValue(document.form_sub.assembly_actual_status);
        <? } ?>
        
        <? if ($policy_info["questionnaire"]){ ?>
        	document.form_export_csv.questionnaire_status.value = getSelectionValue(document.form_sub.questionnaire_status);
        <? } ?>
        
        <? if ($policy_info["test"]){ ?>
        	document.form_export_csv.test_status.value = getSelectionValue(document.form_sub.test_status);
        <? } ?>
        
        document.form_export_csv.total_status.value = getSelectionValue(document.form_sub.total_status);
        document.form_export_csv.submit();
    }
</script>



<iframe frameborder="0" name="download_frame" style="position:absolute; left:0; top:0; width:0; height:0"></iframe>


<form name="form_excel" action="" method="post">
    <input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
    <input type="hidden" name="btn_event" value="be_export_excel" />
    <input type="hidden" name="contents_id" value="<?=$contents_id?>" />
</form>




<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>受講者管理</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="<?=h($contents["title"])?>" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





<form name="form_sub" action="" method="post" target="_self">
    <input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
    <input type="hidden" name="contents_id" value="<?=$contents_id?>" />
    <input type="hidden" name="event" value="ev_search" />
    <input type="hidden" name="result" value="" />
    <input type="hidden" name="btn_event" value="" />
    <input type="hidden" name="emp_id_comma_list" id="emp_id_comma_list" value="" />


    <? if (!$edit_auth) { ?>
        <div style="padding:5px 5px 0 5px"><div class="warningbox">権限がありません。登録に関連する操作は行えません。</div></div>
    <? } ?>





    <div class="findbox" style="background-color:#ffffff"><nobr>
        <? $btnOff = (!$edit_auth ? " btn-off":"");?>
        <button type="button" class="btn-main<?=$btnOff?>" id="select_student" onclick="open_emp_select(this);">受講者追加</button>
        <button type="button"  style="width:170px" class="btn-main<?=$btnOff?>" onclick="import_student(this)">受講者CSVインポート</button>

        <? $btnOff = (!$edit_auth || !$contents["student_count"] ? " btn-off":"");?>
        <button type="button"  style="width:170px" class="btn-main<?=$btnOff?>" onclick="export_student(this)"
        navititle="受講者CSVエクスポート" navi="現在の検索条件にヒットした受講者をCSV出力します。"
        >受講者CSVエクスポート</button>

        <? $visi = ($policy_info["assembly"] ? "" : " visibility_hidden");?>
        <? $btnOff = (!$edit_auth || !$contents["student_count"] ? " btn-off":"");?>
        <button type="button"  style="width:170px" class="btn-main<?=$btnOff.$visi?>" onclick="export_excel(this)" navititle="集合研修申込者名簿出力" navi="集合研修へ申込登録を行った受講者の一覧をExcel出力します。">集合研修申込者名簿出力</button>
        <button type="button" style="width:170px" class="btn-main<?=$btnOff.$visi?>" onclick="show_assembly_import(this)" navititle="集合研修実績インポート"            navi="集合研修を受講した職員IDのリストを取り込み、受講済みに変更します。">集合研修実績インポート</button>
        <button type="button" style="width:130px" class="btn-main<?=$btnOff.$visi?>" onclick="show_actual_entry(this)" navititle="集合研修実績受付"
        navi="画面で指定した職員IDを、集合研修受講済みに変更します。受講日は本日になります。">集合研修実績受付</button>
    </nobr></div>

    <!-- 検索欄開始 -->
    <div class="findbox">
        <table cellspacing="0" cellpadding="0" class="rgroup">
            <tr>
                <td><nobr><?=h(GG_CLASS_NM)?>
                    <select name="emp_class" id="emp_class" onchange="ddlClassChanged(this)">
                        <option value="">全て</option>
                        <?foreach ($class_list as $class_item) {?>
                            <? $selected = ($class_item["class_id"]==$emp_class?" selected":"")?>
                            <option value="<?=$class_item["class_id"]?>"<?=$selected?>><?=h($class_item["class_nm"])?></option>
                        <?}?>
                    </select>
                    <span style="padding-left:20px"><?=h(GG_ATRB_NM)?></span>
                    <select name="emp_attr" id="emp_attr" onchange="ddlAttrChanged(this)">
                        <option class_id="" value="">全て</option>
                        <?foreach ($attr_list as $attr_item) {?>
                            <? $selected = ($attr_item["atrb_id"]==$emp_attr?" selected":"")?>
                            <option value="<?=$attr_item["atrb_id"]?>"<?=$selected?>><?=h($attr_item["atrb_nm"])?></option>
                        <?}?>
                    </select>
                    <span style="padding-left:20px"><?=h(GG_DEPT_NM)?></span>
                    <select name="emp_dept" id="emp_dept" onchange="ddlDeptChanged(this)" style="width:200px">
                        <option attr_id="" value="">全て</option>
                        <?foreach ($dept_list as $dept_item) { ?>
                            <? $selected = ($dept_item["dept_id"]==$emp_dept?" selected":"")?>
                            <option value="<?=$dept_item["dept_id"]?>"<?=$selected?>><?=h($dept_item["dept_nm"])?></option>
                        <?}?>
                    </select>
                    <span style="<?=(GG_ROOM_NM?"":"display:none")?>">
                    <span style="padding-left:20px"><?=h(GG_ROOM_NM)?></span>
                    <select name="emp_room" id="emp_room">
                        <option room_id="" value="">全て</option>
                        <?foreach ($room_list as $room_item) { ?>
                            <? $selected = ($room_item["room_id"]==$emp_room?" selected":"")?>
                            <option value="<?=$room_item["room_id"]?>"<?=$selected?>><?=h($room_item["room_nm"])?></option>
                        <?}?>
                    </select>
                    </span>

                    <script type="text/javascript">
                    ddlClassChanged(ee("emp_class"));
                    if (ee("emp_attr")) setSelection(ee("emp_attr"), "<?=$emp_attr?>");
                    if (ee("emp_attr")) ddlAttrChanged(ee("emp_attr"));
                    if (ee("emp_dept")) setSelection(ee("emp_dept"), "<?=$emp_dept?>");
                    if (ee("emp_dept")) ddlDeptChanged(ee("emp_dept"));
                    if (ee("emp_room")) setSelection(ee("emp_room"), "<?=$emp_room?>");
                    </script>
                    </nobr>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%; margin-top:5px">
            <tr>
                <td><nobr>
                職員ID
                <input type="text" class="text imeoff" size="15"  maxlength="12" name="emp_personal_id" value="<?=h($emp_personal_id)?>" />

                <span style="padding-left:20px">職員名</span>
                <input type="text" class="text" size="20" maxlength="30" name="emp_name" value="<?=h($emp_name)?>" style="width:100px" />

                <span style="padding-left:20px">職種</span>
                <select name="emp_job" style="width:160px">
                    <option value="">全て</option>
                    <?foreach ($job_list as $job_item) {?>
                        <? $selected = ($job_item["job_id"]==$emp_job?" selected":"")?>
                        <option value="<?=$job_item["job_id"]?>"<?=$selected?>><?=h($job_item["job_nm"])?></option>
                    <?}?>
                </select>

                <span style="padding-left:20px">役職</span>
                <select name="emp_status" style="width:110px">
                    <option value="">全て</option>
                    <?foreach ($status_list as $status_item) {?>
                        <? $selected = ($status_item["st_id"]==$emp_status?" selected":"")?>
                        <option value="<?=$status_item["st_id"]?>"<?=$selected?>><?=h($status_item["st_nm"])?></option>
                    <?}?>
                </select>
                    <span style="padding-left:20px">退職/停止</span><select name="emp_expire_type" navititle="退職/停止" navi="退職済職員：本日付またはそれ以前に退職済である職員のみを抽出します。<br>利用停止職員：現時点で利用停止設定が行われている職員のみを抽出します。<br>利用権限の無い職員：このシステムの閲覧権限/利用権限の無い職員のみを抽出します。">
                        <option></option>
                        <option value="is_emp_retire" <?=($emp_expire_type=="is_emp_retire"?" selected":"")?>>退職済職員</option>
                        <option value="is_emp_del_flg" <?=($emp_expire_type=="is_emp_del_flg"?" selected":"")?>>利用停止職員</option>
                        <option value="is_not_emp_manabu_flg" <?=($emp_expire_type=="is_not_emp_manabu_flg"?" selected":"")?>>利用権限の無い職員</option>
                    </select></nobr>

                </td>
            </tr>
        </table>




        <div style="padding-top:10px">
        <table cellspacing="0" cellpadding="0" style="width:100%"><tr><td>
            <table cellspacing="0" cellpadding="0">
            <tr>
                <? if ($policy_info["assembly"]){ ?><td><span class="fontsize11 brown">集合研修予定</span></td><? } ?>
                <? if ($policy_info["assembly"]){ ?><td><span class="fontsize11 brown">集合研修実績</span></td><? } ?>
                <? if ($policy_info["questionnaire"]){ ?><td><span class="fontsize11 brown">ｱﾝｹｰﾄ</span></td><? } ?>
                <? if ($policy_info["test"]){ ?><td><span class="fontsize11 brown">ﾃｽﾄ</span></td><? } ?>
                <td><span class="fontsize11 brown">総合</span></td>
            </tr>
            <tr>
            <? if ($policy_info["assembly"]){ ?>
            <td style="padding-right:20px">
            <select name="no" id="assembly_date_no" style="width:350px" onchange="setComboColor(this)">
                <option class="normal" value="">全て</option>
                <option class="strong" value="-1"<?=($no=="-1"?" selected":"")?>>未登録</option>
                <option class="strong" value="-3"<?=($no=="-3"?" selected":"")?>>出席</option>
                <? foreach ($asm_list as $idx => $row) { ?>
                    <option class="strong" value="<?=$row["no"]?>" <?=($row["no"]==$no?" selected":"")?>>出席：<?
                        echo date("Y年m月d日",strtotime($row["date"]));
                        $dispTime = "";
                        if (strlen($row["start_time"])==4) $dispTime .= substr($row["start_time"],0,2).":".substr($row["start_time"],2,2);
                        if (strlen($row["start_time"])==4 && strlen($row["end_time"])==4) $dispTime .= "〜";
                        if (strlen($row["end_time"])==4) $dispTime .= substr($row["end_time"],0,2).":".substr($row["end_time"],2,2);
                        if ($dispTime) echo "　" . h($dispTime);
                        echo "　".h($row["place"]);
                    ?></option>
                <? } ?>
                <option class="strong" value="-2"<?=($no=="-2"?" selected":"")?>>欠席</option>
            </select>
            </td>
            <? } ?>

            <? if ($policy_info["assembly"]){ ?>
            <td style="padding-right:20px">
            <select name="assembly_actual_status" id="assembly_actual_status" onchange="setComboColor(this)">
                <option class="normal" value="">全て</option>
                <option class="strong" value="1"<?=($assembly_actual_status=="1"?" selected":"")?>>出席</option>
                <? foreach ($assembly_actual_date_list as $row) {?>
                <?     $ymd = $row["assembly_actual_date"]; ?>
                <?     $ymdjp = date("Y年m月d日",strtotime($ymd)); ?>
                <option class="strong" value="1_<?=$ymd?>"<?=($assembly_actual_status=="1_".$ymd?" selected":"")?>>出席：<?=$ymdjp?></option>
                <? } ?>
                <!--option class="strong" value="2"<?=($assembly_actual_status=="2"?" selected":"")?>>欠席</option-->
                <!--option class="strong" value="-1"<?=($assembly_actual_status=="-1"?" selected":"")?>>未登録</option-->
                <option class="strong" value="-3"<?=($assembly_actual_status=="-3"?" selected":"")?>>出席以外</option>
            </select>
            </td>
            <? } ?>

            <? if ($policy_info["questionnaire"]){ ?>
            <td style="padding-right:20px">
            <select name="questionnaire_status" id="questionnaire_status" onchange="setComboColor(this)">
                <option class="normal" value="">全て</option>
                <option class="strong" value="1"<?=($questionnaire_status=="1"?" selected":"")?>>回答済</option>
                <option class="strong" value="0"<?=($questionnaire_status=="0"?" selected":"")?>>未回答</option>
            </select>
            </td>
            <? } ?>

            <? if ($policy_info["test"]){ ?>
            <td style="padding-right:20px"><select name="test_status" id="test_status" onchange="setComboColor(this)">
                <option class="normal" value="">全て</option>
                <option class="strong" value="2"<?=($test_status=="2"?" selected":"")?>>合格</option>
                <option class="strong" value="-2"<?=($test_status=="-2"?" selected":"")?>>合格以外</option>
                <option class="strong" value="1"<?=($test_status=="1"?" selected":"")?>>実施中</option>
                <option class="strong" value="-1"<?=($test_status=="-1"?" selected":"")?>>未実施</option>
            </select></td>
            <? } ?>

            <td><select name="total_status" id="total_status" onchange="setComboColor(this)">
                <option class="normal" value="">全て</option>
                <option class="strong" value="1"<?=($total_status=="1"?" selected":"")?>>受講済</option>
                <option class="strong" value="0"<?=($total_status=="0"?" selected":"")?>>未修</option>
            </select></td>

            </tr></table>
        </td>
        <td style="text-align:right; padding-right:5px">
            <br/>
            <button type="button" class="btn-main" id="search_student" onclick="document.form_sub.submit();">検索</button></span>
        </td>
        </tr></table>
        </div>
        </td></tr></table>
    </div>
    <!-- 検索欄終了 -->


    <div class="usual_box" style="padding-bottom:0">
        <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
            <td style="letter-spacing:0"><nobr>
                <? if ($contents["student_count"]) {?>
                <span style="padding-right:0px; font-size:11px;">受講者<span style="font-size:16px; color:#dd0000; padding:0 2px"><b><?=$contents["student_count"]?></b></span>人／</span>
                <? } ?>
                <span style="padding-right:20px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b><?=$total_count?></b></span>人</span>
                <span style="font-size:12px">表示件数</span>
                <select name="per_page_size" onchange="$('#page_num').val(1); document.form_sub.submit()">
                    <? $ary = array(5,10,20,30,40,50,100,200,300,500,-1); ?>
                    <? foreach ($ary as $v) {?><option value="<?=$v?>"<?=($v==$per_page_size?" selected":"")?>><?=($v==-1?"全件":$v."件")?></option><?}?>
                </select>
            </td>
            <td style="text-align:right; font-size:12px"><nobr>
                <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" />
                <?$dis = ($page_num<=1?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('top')"<?=$dis?>>≪</button>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('prev')"<?=$dis?>>＜</button>
                <input type="text" class="text imeoff" maxlength="4" style="width:40px" id="new_page_num" value="<?=$page_num?>"
                ><span style="font-size:12px; letter-spacing:0; padding-left:1px">ページ</span>
                <?$dis = ($last_page<=1?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:38px" onclick="paging('move')"<?=$dis?>>移動</button>
                <?$dis = ($page_num>=$last_page?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('next')"<?=$dis?>>＞</button>
                <?$dis = ($page_num>=$last_page?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('last')"<?=$dis?>>≫</button></nobr>
            </td>
        </tr></table>
    </div>






    <div class="usual_box">
        <table cellspacing="0" cellpadding="0" class="list">
            <? $rowspan = ($policy_info["assembly"] ? ' rowspan="2"':'');?>
            <tr navi="並び順：受講者氏名昇順">
                <th<?=$rowspan?> width="60"><button type="button" class="btn-sel<?=(!$edit_auth || !$total_count ?" btn-off":"")?>" id="all_check_emp" onclick="allCheckEmp(this)">選択</button></th>
                <th<?=$rowspan?> width="120">職員ID</th>
                <th<?=$rowspan?> width="150">受講者氏名</th>
                <th<?=$rowspan?> width="280">所属</th>
                <th<?=$rowspan?> width="80">職種</th>
                <? if ($policy_info["assembly"]){ ?>
                <th colspan="2" width="72">集合研修</th>
                <? } ?>
                <? if ($policy_info["questionnaire"]){ ?>
                <th<?=$rowspan?> width="50">ｱﾝｹｰﾄ</th>
                <? } ?>
                <? if ($policy_info["test"]){ ?>
                <th<?=$rowspan?> width="42">ﾃｽﾄ</th>
                <? } ?>
                <th<?=$rowspan?> width="auto">総合</th>
            </tr>
            <? if ($policy_info["assembly"]){ ?>
            <tr>
                <th width="36">予定</th>
                <th width="36">実績</th>
            </tr>
            <? } ?>
        </table>
        <div class="scroll_div" style="border-bottom:1px solid #dddddd; background-color:#f5f5f5;" heightMargin="36">
        <? if ($total_count > 0) {?>
            <table id="student_update_list" cellspacing="0" cellpadding="0" class="list">

<?foreach ($student_list as $item) { ?>
<? $syozoku = h($item["class_nm"]);
if (@$item["atrb_nm"]) $syozoku .= " ＞" . h($item["atrb_nm"]);
if (@$item["dept_nm"]) $syozoku .= " ＞" . h($item["dept_nm"]);
if (@$item["room_nm"]) $syozoku .= " ＞" . h($item["room_nm"]);



//---------------------------------
// 集合研修申込について
//---------------------------------
$tmp = $item["assembly_status"];

$asm_last_date = end($asm_list);//最後の開催日
$tmp_asm_date = $item["assembly_status_date"];//登録日

$asm_status = '<span class="gray">未</span>';
if ($tmp=="1") $asm_status = '出席';
if ($tmp=="2"){
    if($tmp_asm_date>$asm_last_date['date']){
        $asm_status = '<span class="gray">欠席*</span>';
    }else{
        $asm_status = '<span class="gray">欠席</span>';
    }
}

if (!$policy_info["assembly"]) $asm_status = '<span class="gray">―</span>';

$mosikomi_navi = "";
if ($item["assembly_mosikomi_date_jp"]) {
    $mosikomi_navi = ' navi="申込内容：'.$item["assembly_mosikomi_date_jp"].' ' . h(h($item["assembly_mosikomi_place"])).'"';
}


//---------------------------------
// 集合研修実績について
//---------------------------------
$asm_exist = 0;
$asm_actual_status = '<span class="gray">未</span>';
if ($item["assembly_actual_status"]=="1" && $item["assembly_actual_status_late"]=="1") { $asm_exist = 1; $asm_actual_status = '出席*'; }
else if ($item["assembly_actual_status"]=="1") { $asm_exist = 1; $asm_actual_status = '出席'; }


if ($item["assembly_actual_status"]=="2") { $asm_exist = 2; $asm_actual_status = '欠席'; }
if (!$policy_info["assembly"]) { $asm_exist = 0; $asm_actual_status = '<span class="gray">―</span>'; }


//---------------------------------
// アンケートについて
//---------------------------------
$que_exist = 0;
$questionnaire_status = '<span class="gray">未</span>';
if ($contents["policy_for_questionnaire"]==="A-" && $asm_exist==1) $questionnaire_status = '<span class="gray">不要</span>'; // エーマイナスなら
if ($contents["policy_for_questionnaire"]==="A+" && $asm_exist==2) $questionnaire_status = '<span class="gray">不要</span>'; // エープラスなら

if ($item["questionnaire_status"]) {
    $que_exist = 1;
    $questionnaire_status = '<a href="javascript:void(0)" onclick="show_questionnaire(\''.$item["emp_id"].'\')">済</a>';
}
if (!$policy_info["questionnaire"]) { $que_exist = 0; $questionnaire_status = '<span class="gray">―</span>'; }


//---------------------------------
// テストについて
//---------------------------------
$tes_exist = 0;
$test_status = '<span class="gray">未</span>';
if ($contents["policy_for_test"]==="A-" && $asm_exist) $test_status = '<span class="gray">不要</span>'; // エーマイナスなら
if ($item["test_status"]=="1") {
    $tes_exist = 1;
    $test_status = '<a href="javascript:void(0)" onclick="show_user_answer(\''.$item["emp_id"].'\')">実施中</a>';
}
if ($item["test_status"]=="2") {
    $tes_exist = 1;
    $test_status = '<a href="javascript:void(0)" onclick="show_user_answer(\''.$item["emp_id"].'\')">合格</a>';
}
if (!$policy_info["test"]) { $tes_exist = 0; $test_status = '<span class="gray">―</span>'; }


//---------------------------------
// 総合
//---------------------------------
$total_status = '<span class="gray">未</span>';
if ($item["total_status"]=="1") {
    $total_status = '<span class="sumi">済</span>';
}

?>



<tr>
<td width="60" class="center" id="<?=$item["emp_id"]?>"><input name="emp_id" type="checkBox" value="<?=$item["emp_id"]?>" /></td>
<td width="120"><?=$item["emp_personal_id"]?></td>
<td width="150"><div style="width:150px; overflow:hidden"><?=($item["emp_manabu_flg"]!="t" ? '<span class="no_auth" navi="システムの閲覧権限/利用権限がありません">[権なし]</span>&nbsp;' : '')?><?=h($item["last_name"])?>&nbsp;<?=h($item["first_name"])?></div></td>
<td width="280"><div style="width:280px; overflow:hidden"><?=$syozoku?></div></td>
<td width="80"><div style="width:80px; overflow:hidden"><?=h($item["job_nm"])?></div></td>
<? if ($policy_info["assembly"]){ ?>
<td width="36" class="center"<?=$mosikomi_navi?>><?=$asm_status?></td>
<td width="36" class="center"<?=($item["assembly_actual_date_jp"] ? ' navi="実績日：'.$item["assembly_actual_date_jp"].'"':"")?>><?=$asm_actual_status?></td>
<? } ?>
<? if ($policy_info["questionnaire"]){ ?>
<td width="50" class="center"><?=$questionnaire_status?></td>
<? } ?>
<? if ($policy_info["test"]){ ?>
<td width="42" class="center"><?=$test_status?></td>
<? } ?>
<td width="auto" class="center"><?=$total_status?></td>
</tr>
<?}?>


        </table>
    <? } ?>
    <? if (!$contents["student_count"]) {?>
    <div style="padding:10px 5px; line-height:1.5" class="gray2">受講者が登録されていません。<br/>「受講者追加」または「受講者CSVインポート」ボタンより、受講者を登録してください。</div>
    <? } else if ($total_count == 0) {?>
    <div style="padding:10px 5px" class="gray2">該当データは存在しません。</div>
    <? } ?>

        </div>


    </div>


</form>









<div style="position:absolute; bottom:-40px; width:100%">
<div style="padding:5px;">
    <table cellspacing="0" cellpadding="0" class="rgroup"><tr>

    <? $btnoff = (!$edit_auth || !count($student_list)) ? " btn-off" : ""; ?>
    <td><nobr>
        <button type="button" class="btn-main<?=$btnoff?>" onclick="delete_student(this, 'delete')"
            navititle="受講者の削除"
            navi="チェック対象を受講者対象外とします。<br>受講済または受講中の受講者について、削除はできません。">受講者削除</button>
    </nobr></td>


    <? $btnoff = (!$edit_auth || !count($student_list)) ? " btn-off" : ""; ?>
    
    
    <td style="padding-left:20px"<? if (!$policy_info["assembly"]) { ?> class="visibility_hidden"<? } ?>><nobr>
        <button type="button" class="btn-main<?=$btnoff?>" onclick="student_change(this, 'be_change_assembly_status', '2')"
            navititle="集合研修予定欠席"
            navi="チェック対象の集合研修予定を欠席に変更します。">集合研修予定欠席</button>
        
        <button type="button" class="btn-main<?=$btnoff?>" onclick="student_change(this, 'be_change_assembly_status', '')"
            navititle="集合研修予定クリア"
            navi="チェック対象の集合研修予定済をクリアします。">集合研修予定クリア</button>
        
        <button type="button" class="btn-main<?=$btnoff?>" onclick="student_change(this, 'be_change_assembly_actual_status', '1')"
            navititle="集合研修出席"
            navi="チェック対象の集合研修実績を出席に変更します。">集合研修出席</button>
        
        <button type="button" class="btn-main<?=$btnoff?>" onclick="student_change(this, 'be_change_assembly_actual_status', '2')"
            navititle="集合研修欠席"
            navi="チェック対象の集合研修実績を欠席に変更します。">集合研修欠席</button>

        <button type="button" class="btn-main<?=$btnoff?>" onclick="student_change(this, 'be_change_assembly_actual_status', '')"
            navititle="集合研修ｸﾘｱ"
            navi="チェック対象の集合研修出席済をクリアします。">集合研修ｸﾘｱ</button>
    </nobr></td>


    <td style="padding-left:20px"<? if (!$policy_info["questionnaire"]) { ?> class="visibility_hidden"<? } ?>><nobr>
        <button type="button" class="btn-main<?=$btnoff?>" onclick="student_change(this, 'be_change_questionnaire_status', '')"
            navititle="アンケート回答削除"
            navi="チェック対象の回答済みアンケートを削除します。">アンケート回答削除</button>
    </nobr></td>

    <td <? if (!$policy_info["test"]) { ?> class="visibility_hidden"<? } ?>><nobr>
        <button type="button" class="btn-main<?=$btnoff?>" onclick="student_change(this, 'be_clear_test_result', '')"
            navititle="テスト解答削除"
            navi="チェック対象の解答済みテスト結果および履歴を削除し、テスト未実施状態にします。">テスト解答削除</button>
    </nobr></td>


    </tr></table>

</div>
</div>



<form name="form_export_csv" action="" method="post">
    <input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
    <input type="hidden" name="btn_event" value="be_exec_student_export_csv" />
    <input type="hidden" name="contents_id" value="<?=$contents_id?>" />
    <input type="hidden" name="id_type" />
    <input type="hidden" name="emp_class" />
    <input type="hidden" name="emp_attr" />
    <input type="hidden" name="emp_dept" />
    <input type="hidden" name="emp_room" />
    <input type="hidden" name="emp_personal_id" />
    <input type="hidden" name="emp_name" />
    <input type="hidden" name="emp_job" />
    <input type="hidden" name="emp_status" />
    <input type="hidden" name="no" />
    <input type="hidden" name="assembly_actual_status" />
    <input type="hidden" name="questionnaire_status" />
    <input type="hidden" name="test_status" />
    <input type="hidden" name="total_status" />
</form>




</body>
</html>
