{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}アンケート項目編集</title>
</head>
<body onresize="resized()">
<!-- ///study_questionnaire_edit  -->



<script type="text/javascript">
    var item_no = "{$item_no}";
    var mode = "{$mode}";
    {literal}
    function selected_choice_count() {
        var choice_count = $("#choice_cnt").val();
        for (var idx = 1; idx <= 9; idx++) {
            ee("tr_item"+idx).style.display = (idx<=choice_count ? "" : "none");
        }
    }

    function selected_type() {
        var num = $("#question_type").val();
        ee("select1").style.display = (num=="3" ? "none" : "");
        ee("select2").style.display = (num=="3" ? "none" : "");
        selected_choice_count();
    }

    function create_item(mode){
        var question_type = $("#question_type").val(); //種類
        if (question_type == "3") choice_cnt = 0;

        //質問文の項目チェック
        var question = $("#question").val(); //質問
        if (jQuery.trim(question) == "") {
            alert("質問文が入力されていません。");
            return;
        }

        //選択肢の項目チェック
        var choice_cnt = $("#choice_cnt").val();//選択数
        var answers = [];
        if (question_type != "3") {
            for (var idx=1; idx<=choice_cnt; idx++) {
                var sentakusi = $("#choice_item"+idx).val();
                if (jQuery.trim(sentakusi) == "") { alert("選択肢の項目が入力されていません。"); return; }
                answers.push(sentakusi);
            }
        } else {
            choice_cnt = 0;
        }

        //回答必須
        var required = "0";
        if (ee("required").checked) required = "1";
        window.opener.edit_question_item_from_sub_window(item_no, mode, question, choice_cnt, question_type, required, answers);
        window.close();
    }

    function clear_item() {
        $("#question").val("");                 //質問文
        $("#required").attr("checked", false);  //回答必須
        for (var idx=1; idx<=10; idx++) ee("choice_item"+idx).value ="";
    }
    $(document).ready(function() {
        if (mode!="1") {
            selected_type();
        }
        if (mode=="1") {
            if (window.opener && window.opener.mainform && item_no) {
                $("#question").val(window.opener.mainform["question"+item_no].value);                 //質問文
                $("#question_type").val(int(window.opener.mainform["question_type"+item_no].value));             //種類
                ee("required").checked = (int(window.opener.mainform["required"+item_no].value) ? true : false); //回答必須
                $("#choice_cnt").val(int(window.opener.mainform["choice_count"+item_no].value));                //選択肢
            }
            selected_type();
            if ($("#question_type").val()!="3") {
                var choice_cnt = int($("#choice_cnt").val());
                for (var idx=1; idx<=9; idx++) {
                    ee("choice_item"+idx).value = (idx<=choice_cnt ? window.opener.mainform["answers"+item_no+"_"+idx].value : "");
                    ee("tr_item"+idx).style.display = (idx<=choice_cnt ? "" : "none");
                }
            }
        }
    });
    {/literal}
</script>






<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>アンケート項目編集</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





<div class="usual_box">

    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="70">質問文</th>
            <td><textarea id="question" cols=75 rows=3></textarea></td>
        </tr>

        <tr>
            <th><nobr>質問種類</nobr></th>
            <td>
                <select id="question_type" onChange="selected_type()">
                    <option value="1">単数選択</option>
                    <option value="2">複数選択</option>
                    <option value="3">文章入力</option>
                </select>
            </td>
        </tr>

        <tr>
            <th>回答必須</th>
            <td>
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                    <th><input type="checkbox" id="required" value="1" /></th>
                    <td><label for="required">はい</label></td>
                </tr></table>
            </td>
        </tr>


        <tr id="select1" style="display:none;">
            <th>選択肢数</th>
            <td>
                <select id="choice_cnt" onchange="selected_choice_count()">
                    {section name=choice start=2 loop=10 step=1}
                        <option value="{$smarty.section.choice.index}">{$smarty.section.choice.index}</option>
                    {/section}
                </select>
            </td>
        </tr>

        <tr id="select2" style="display:none;">
            <th>選択肢</th>
            <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr id="tr_item1" style="display:none"><td><input type="text" class="text" id="choice_item1" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item2" style="display:none"><td><input type="text" class="text" id="choice_item2" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item3" style="display:none"><td><input type="text" class="text" id="choice_item3" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item4" style="display:none"><td><input type="text" class="text" id="choice_item4" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item5" style="display:none"><td><input type="text" class="text" id="choice_item5" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item6" style="display:none"><td><input type="text" class="text" id="choice_item6" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item7" style="display:none"><td><input type="text" class="text" id="choice_item7" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item8" style="display:none"><td><input type="text" class="text" id="choice_item8" size="90" style="ime-mode:active;"/></td></tr>
                    <tr id="tr_item9" style="display:none"><td><input type="text" class="text" id="choice_item9" size="90" style="ime-mode:active;"/></td></tr>
                </table>
            </td>
        </tr>
    </table>




    <div style="text-align:right; padding:14px 0px 10px 0px;">
        <button type="button" class="btn-main" onclick="clear_item()">クリア</button>
        {if $mode eq 1}
            <button type="button" class="btn-main" onclick="create_item(1)">更新</button>
        {else}
            <button type="button" class="btn-main" onclick="create_item(0)">作成</button>
        {/if}
    </div>



</div>
</body>
</html>
