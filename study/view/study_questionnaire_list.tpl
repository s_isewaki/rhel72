{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}アンケート編集</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_questionnaire_list  -->






<script type="text/javascript">
    var contents_id = "{$contents_id}";
    var question_seq = {$questionnaire|@count};
    var result = "{$result}";
    {literal}
    //アンケート登録
    function entry_questionnaire(btn){
        if (btn.className.indexOf("btn-off")>=0) return;
        var question_info = new Array();
        var info = new Array();
        var span_info = new Array();
        if (!convertAndCheckDateTime()) return false;

        $("[id^=content]").each(function(i){
            var str = $(this).attr("id");
            var index = str.slice(7);

            var question = htmlEscape(document.mainform["question"+index].value); // 質問文
            var question_type = int(document.mainform["question_type"+index].value); // 質問タイプ
            var required = int(document.mainform["required"+index].value); // 回答必須
            var choice_count = int(document.mainform["choice_count"+index].value); // 選択肢数


           
            //選択肢
            var answers = [];
            for (var idx = 1; idx <= choice_count; idx++) {
                var ans = document.mainform["answers"+index+"_"+idx];
                var v = ans ? ans.value : "";
                answers.push(htmlEscape(v.replace(/\,/g, "，"))); // 選択肢数
            }
            var question_val =
                '<input type="hidden" name="question_info['+i+'][0]" value="'+question+'">'+
                '<input type="hidden" name="question_info['+i+'][1]" value="'+question_type+'">'+
                '<input type="hidden" name="question_info['+i+'][2]" value="'+required+'">'+
                '<input type="hidden" name="question_info['+i+'][3]" value="'+choice_count+'">'+
                '<input type="hidden" name="question_info['+i+'][4]" value="' + answers.join(",") + '">';
            $("#all_content").append(question_val);
        
        });
        document.mainform.submit();
    }

    //アンケート編集
    function edit_question(item_no) {
        select_color(item_no);
        var url = "study_questionnaire.php?session="+session+"&btn_event=be_edit_questionnaire&mode=1&contents_id="+contents_id+"&item_no="+item_no;
        window_open(url, "edit_questionnaire", 800, 420);
    }

    //アンケート削除
    function delete_question(item_no) {
        var txt=$('#p' + item_no).text();
        select_color(item_no);
        if (all_content.rows.length < 2) { alert("アンケート作成に一つ以上の項目が必要です。\n最後の項目は削除できません。"); return false; }
        if (!confirm("「" + txt + "」\n\nこの項目を削除しますか？")) return false;
        $("#content" + item_no).remove();
    }

    //アンケート追加
    function add_question(item_no) {
        select_color(item_no);
        var url = "study_questionnaire.php?session="+session+"&btn_event=be_add_questionnaire&mode=0&contents_id="+contents_id+"&item_no="+item_no;
        window_open(url, "edit_questionnaire", 800, 420);
    }

    //アンケート移動
    function move_question(no, upOrDown){
        var names = [];
        var div = ee("content"+no);
        for (var idx =0; idx<div.parentNode.childNodes.length; idx++) {
            var elem = div.parentNode.childNodes[idx];
            if (elem.tagName!="TR") continue;
            names.push(elem.id);
        }

        for (var nidx=0; nidx<names.length; nidx++) {
            if (names[nidx]=="content"+no) {
                if (upOrDown=="up" && nidx>0) { div.parentNode.insertBefore(div, ee(names[nidx-1])); break; }
                if (upOrDown=="down" && nidx<names.length-1) { div.parentNode.insertBefore(ee(names[nidx+1]), div); break; }
            }
        }
        select_color(no);
    }

    //アンケート項目編集/追加の処理
    function edit_question_item_from_sub_window(item_no, mode, question, choice_cnt, question_type, required, answers) {
        var no = item_no;
        if (mode!=1) {
            question_seq++;
            no = question_seq;
        }
        var answer_html = [];
        if (question_type=="3") {
            answer_html.push('<tr><td><textarea cols="60" rows="4" readonly class="gray">（ここに回答者が回答します）</textarea></td></tr>');
        } else {
            for (i = 1; i <= answers.length; i++) {
                answer_html.push('<tr>');
                if (question_type=="1") answer_html.push('<th><input type="radio" name="survey'+no+'" id="survey_'+no+'_'+i+'"></th>');
                if (question_type=="2") answer_html.push('<th><input type="checkbox" id="survey'+no+'_'+i+'"></th>');
                answer_html.push('<td><label for="survey_'+no+'_'+i+'"><span>'+htmlEscape(answers[i-1])+'</span></label></td></tr>');
                answer_html.push('<input type="hidden" name="answers'+no+'_'+i+'" id="answers'+no+'_'+i+'" value="'+htmlEscape(answers[i-1])+'" />');
            }
        }
        var tr_question_html =
            '<p id="p' + no + '" class="test_question">' + htmlEscape(question).replace(/\n/g, '<br>') + '</p>'+
            '<div class="test_answer1"><div class="test_answer2" id="divans'+no+'">'+
            '<table cellspacing="0" cellpadding="0" class="rgroup">'+
            answer_html.join("")+
            '</table>'+
            '</div></div>' +
            '<input type="hidden" name="choice_count'+no+'" value="'+int(choice_cnt)+'" />'+
            '<input type="hidden" name="question_type'+no+'" value="'+int(question_type)+'" />'+
            '<input type="hidden" name="required'+no+'" value="'+int(required)+'" />'+
            '<input type="hidden" name="question'+no+'" value="'+htmlEscape(question)+'" />'+
            '<h3 class="test_editor" id="hid' + no + '">' +
                '<a id="et' + no +'" href="javascript:void(0);"'+
                ' onclick="edit_question('+no+'); return false;">編集</a> | ' +
                '<a id="de' + no +'" href="javascript:void(0);" onclick="delete_question('+no+'); return false;">削除</a> | ' +
                '<a id="ad' + no +'" href="javascript:void(0);" onclick="add_question('+no+'); return false;">追加</a> | ' +
                '<a id="up' + no +'" href="javascript:void(0);" onclick="move_question('+no+',\'up\'); return false;">▲上へ</a> | ' +
                '<a id="dn' + no +'" href="javascript:void(0);" onclick="move_question('+no+',\'down\'); return false;">▼下へ</a>' +
                (required=="1" ? ' | <span style="color:#ff0000"> ※(回答必須)</span>' : '') +
            '</h3>';

        var p_index = $("#content" + no).index();
        var p = $(".position").eq(p_index).offset().top;
        if (mode == 1){
//            ee("content"+no).innerHTML = "<td>"+tr_question_html+"</td>";  
            $("#content" + no).empty();
            $("#content" + no).append("<td>"+tr_question_html+"</td>");
            select_color(no);
        }else{
            $('<tr id="content' + no + '" class="position"><td>' +tr_question_html+'</td></tr>').insertAfter("#content" + item_no);
            select_color(no);
        }
    }

    //選択色変更
    function select_color(no) {
        $("[id^=p]").each(function(i){ $(this).removeClass("questionnaire_pickup"); });
        $("h3").each(function(i){ $(this).removeClass("questionnaire_pickup"); });
        $(".test_answer2").each(function(i){ $(this).removeClass("questionnaire_pickup"); });
        $("#p" + no).addClass("questionnaire_pickup");
        $("#hid" + no).addClass("questionnaire_pickup");
        $("#divans"+no).addClass("questionnaire_pickup");
    }

    $(document).ready(function() {
        if (result=="success") {
            parentRefreshRequest("mainform");
            window.close();
        }
        // 日付にカレンダー適用
        $("#start_day").datepicker( { dateFormat: 'yy年mm月dd日' });
        $("#end_day").datepicker( { dateFormat: 'yy年mm月dd日' });
    });

    {/literal}
</script>






<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>アンケート編集</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents.title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





{if not $edit_auth}
        <div style="padding:5px"><div class="warningbox">
            権限がありません。閲覧のみ可能です。
        </div></div>
{else}
    {if $contents.questionnaire_answer_count}
        <div style="padding:5px"><div class="warningbox">
            アンケートに回答されました。編集はできません。
        </div></div>
    {/if}
{/if}




<div class="usual_box" style="width:873px">
<form name="mainform" action="" method="post">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="btn_event" value="be_create_questionnaire" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />
    <input type="hidden" name="start_date" value="" />
    <input type="hidden" name="end_date" value="" />


    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th style="width:90px;"><nobr>回答期間</nobr></th>
            <td style="padding-left:4px"><nobr>
                <span class="list_color">開始日時</span>
                <input name="start_day" id="start_day" type="text" class="text" size="18" readonly value="{$array_start[0]}" style="width:120px" />
                <select name="st_hour">
                    {section name=st_item start=0 loop=24 step=1}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $array_start[1]}
                            <option value="{$smarty.section.st_item.index}" selected>{$smarty.section.st_item.index}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index}">{$smarty.section.st_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="st_minute">
                    {section name=st_item start=0 loop=60 step=5}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $array_start[2]}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}" selected>{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}">{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分</span>



                <span class="list_color" style="padding-left:20px">終了日時</span>
                <input name="end_day" id="end_day" type="text" class="text" size="18" readonly value="{$array_end[0]}" style="width:120px" />
                <select name="ed_hour">
                    {section name=ed_item start=0 loop=24 step=1}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $array_end[1]}
                            <option value="{$smarty.section.ed_item.index}" selected>{$smarty.section.ed_item.index}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index}">{$smarty.section.ed_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="ed_minute">
                    {section name=ed_item start=0 loop=60 step=5}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $array_end[2]}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}" selected>{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}">{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分</span></nobr>
            </td>
        </tr>
    </table>






    <div style="padding-top:10px">
    <div class="scroll_div" style="overflow-y:scroll; overflow-x:hidden; border:1px solid #dddddd; width:872px" heightMargin="30">
    <div class="usual_box">
        <table id="all_content" width="100%" border="0" cellspacing="0" cellpadding="0">
            {foreach from=$questionnaire key=keys item=items }
                <tr id="content{$keys}" class="position">
                    <td>
                        <input type="hidden" name="choice_count{$keys}" value="{$items.choice_cnt}" />
                        <input type="hidden" name="question_type{$keys}" value="{$items.question_type}" />
                        <input type="hidden" name="required{$keys}" value="{$items.required}" />
                        <input type="hidden" name="question{$keys}"
                            value="{php} echo h($this->_tpl_vars['items']["question"]);{/php}" />

                        <p id="p{$keys}" class="test_question">
                                                {php}
                                                    $question_br = str_replace("\n", "<br>",h($this->_tpl_vars['items']["question"]));
                                                    $question_convert = str_replace(" ", "&nbsp;",$question_br);
                                                    echo $question_convert;
                                                {/php}</p>
                        <div class="test_answer1"><div class="test_answer2" id="divans{$keys}">
                        {foreach from=$items.choice_item key=sub_key item=answer}
                            <div><table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                                {if $items.question_type eq 1}
                                    <th><input type="radio"  name="survey{$keys}" id="survey{$keys}_{$sub_key+1}" value=""></th>
                                    <td><label for="survey{$keys}_{$sub_key+1}"><span name="span{$keys}_{$sub_key+1}" class="input_subguide">{$answer|escape}</span></label></td>
                                {elseif $items.question_type eq 2}
                                    <th><input type="checkbox"  name="survey{$keys}_{$sub_key+1}" id="survey{$keys}_{$sub_key+1}" value=""></th>
                                    <td><label for="survey{$keys}_{$sub_key+1}"><span name="span{$keys}_{$sub_key+1}" class="input_subguide">{$answer|escape}</span></label></td>
                                {else}
                                    <th><textarea cols=60 rows=4 readonly class="gray">（ここに回答者が回答します）</textarea></th>
                                {/if}
                            </tr></table></div>
                            <input type="hidden" name="answers{$keys}_{$sub_key+1}" value="{php} echo h($this->_tpl_vars['answer']);{/php}" />
                        {/foreach}
                        </div></div>

{if $contents.questionnaire_answer_count eq 0 && $edit_auth}
                        <h3 id="hid{$keys}" class="test_editor">
                                <a id="et{$keys}" href="javascript:void(0);"
                                    onclick="edit_question({$keys}); return false;">編集</a> |
                                <a id="de{$keys}" href="javascript:void(0);" onclick="delete_question({$keys}); return false;">削除</a> |
                                <a id="ad{$keys}" href="javascript:void(0);" onclick="add_question({$keys}); return false;">追加</a> |
                                <a id="up{$keys}" href="javascript:void(0);" onclick="move_question({$keys},'up'); return false;">▲上へ</a> |
                                <a id="dn{$keys}" href="javascript:void(0);" onclick="move_question({$keys},'down'); return false;">▼下へ</a>
                                {if $items.required eq "1"}
                                     |<span style="color:#ff0000"> ※(回答必須)</span>
                                {/if}
                        </h3>
{/if}
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>
    </div>
    </div>



    <div style="text-align:right; padding-top:5px">
        <button type="button" class="btn-main{if $contents.questionnaire_answer_count || not $edit_auth} btn-off{/if}" name="entry" onclick="entry_questionnaire(this);">{$mode}</button>
    </div>



</form>
</div>
</body>
</html>