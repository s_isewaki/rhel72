<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>所属別受講状況表</title>
</head>
<body onresize="resized()">
<!-- ///study_contents_syozokubetu  -->


<script type="text/javascript">
    function exec_download_shozokubetu_excel() {
        location.href = "study_contents.php?session=<?=GG_SESSION_ID?>&contents_id=<?=$contents_id?>&btn_event=be_download_syozokubetu";
    }
</script>

<div id="title_area"><table cellspacing="0" cellpadding="0"><tr><th><nobr>所属別受講状況表</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td><td><a onclick="window.close();"></a></td></tr></table></div>
<div id="title_margin">&nbsp;</div>




<div class="usual_box">


    <div style="text-align:right; padding-bottom:5px">
        <button type="button" class="btn-main" onclick="exec_download_shozokubetu_excel();">EXCEL出力</button>
    </div>



    <table id="emp_list_header" cellspacing="0" cellpadding="0" class="list">
        <tr>
          <th><nobr><?=h(GG_CLASS_NM)?></nobr></th>
          <th><nobr><?=h(GG_ATRB_NM)?></nobr></th>
          <th><nobr><?=h(GG_DEPT_NM)?></nobr></th>
          <? foreach ($make_fields as $mfield) { ?>
          <?     if ($mfield["disp"]!="yes") continue; ?>
              <th><nobr><?=h($mfield["jp"])?></nobr></th>
          <? } ?>
        </tr>

        <? // class別ループ ---------------------------------------------- ?>
        <? foreach ($data_hier as $class_id => $data_hier2) { ?>
            <? $class_counter = 0; ?>
            <? $class_row_count = $section_row_count[$class_id]; ?>
            <? // atrb別ループ ---------------------------------------------- ?>
            <? foreach ($data_hier2 as $atrb_id => $data_hier3) { ?>
                <? $attr_counter = 0; ?>
                <? $atrb_row_count = $section_row_count[$class_id."_".$atrb_id]; ?>


                <? // dept別ループ ---------------------------------------------- ?>
                <? foreach ($data_hier3 as $dept_id => $data_hier4) { ?>
                    <? $class_counter++; ?>
                    <? $attr_counter++; ?>
                    <? $data_row = $data_list[$class_id."_".$atrb_id."_".$dept_id]; ?>
                    <? $section_name = $section_names[$class_id."_".$atrb_id."_".$dept_id]; ?>
                    <tr>
                    <? if ($class_counter==1) { ?>
                    <th rowspan="<?=$class_row_count?>" class="pale"><nobr><?=h($section_name["class_nm"])?></nobr></th>
                    <? } ?>
                    <? if ($attr_counter==1) { ?>
                    <th rowspan="<?=$atrb_row_count?>" class="pale"><nobr><?=h($section_name["atrb_nm"])?></nobr></th>
                    <? } ?>
                    <th class="pale"><nobr><?=h($section_name["dept_nm"])?></nobr></th>
                    <? foreach ($make_fields as $mfield) { ?>
                        <? if ($mfield["disp"]!="yes") continue; ?>
                        <td class="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
                    <? } ?>
                    </tr>
                <? } ?>

                <? // atrb別小計（dept合計）?>
                <? $data_row = $data_list[$class_id."_".$atrb_id]; ?>
                <? $section_name = $section_names[$class_id."_".$atrb_id]; ?>
                <tr>
                <th class="pale"><nobr>小計</nobr></th>
                <? foreach ($make_fields as $mfield) { ?>
                    <? if ($mfield["disp"]!="yes") continue; ?>
                    <td class="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
                <? } ?>
                </tr>
            <? } ?>

            <? // ---------------------------------------------- class別小計（atrb合計）?>
            <? $data_row = $data_list[$class_id]; ?>
            <? $section_name = $section_names[$class_id]; ?>
            <tr>
            <th class="pale" colspan="2"><nobr>小計</nobr></th>
            <? foreach ($make_fields as $mfield) { ?>
                <? if ($mfield["disp"]!="yes") continue; ?>
                <td class="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
            <? } ?>
            </tr>
        <? } ?>

        <? // ---------------------------------------------- 全体合計（class合計）?>
        <? $data_row = $data_list["last_total"]; ?>
        <tr>
        <th class="pale" colspan="3"><nobr>合計</nobr></th>
        <? foreach ($make_fields as $mfield) { ?>
            <? if ($mfield["disp"]!="yes") continue; ?>
            <td class="right"><?=$data_row[$mfield["field"]].$mfield["postfix"]?></td>
        <? } ?>
        </tr>

    </table>




</div>

</body>
</html>