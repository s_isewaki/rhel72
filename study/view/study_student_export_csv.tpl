{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}受講者CSVエクスポート</title>
    <script type="text/javascript">
        {literal}
        function tryExportCsv() {
            if (opener && opener!=window && opener.tryExportCsv) {
                opener.tryExportCsv((ee("emp1").checked ? "personal" : "login"));
                window.close();
            }
        }
        {/literal}
    </script>
</head>
<body onresize="resized()">
<!-- ///study_student_export_csv  -->




<div id="title_area"><table cellspacing="0" cellpadding="0"><tr><th><nobr>受講者CSVエクスポート</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td><td><a onclick="window.close();"></a></td></tr></table></div>
<div id="title_margin">&nbsp;</div>


<div class="usual_box">
    <form name="mainform" action="" method="post">
        <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
        <input type="hidden" name="btn_event" value="be_exec_student_export_csv" />
        <input type="hidden" name="contents_id" value="{$contents_id}" />
        <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th>区分</th>
            <td>
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <th><input type="radio" name="id_type" id="emp1" checked></th><td><label for="emp1">職員ID</label></td>
                <th><input type="radio" name="id_type" id="emp2"></th><td><label for="emp2">ログインID</label></td>
                </tr></table>
            </td>
        </table>
    </form>

    <div style="text-align:right; padding-top:5px">
        <button type="button" class="btn-main" onclick="tryExportCsv();">確定</button>
    </div>
</div>
</body>
</html>