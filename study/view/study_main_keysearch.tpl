{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}講座内検索</title>
    {literal}
    <style type="text/css"> html { overflow-y:scroll; } </style>
    {/literal}
</head>
<body onresize="resized()">
<!-- ///study_menu_admin_keysearch  -->





<script type="text/javascript">
    {literal}
    $(document).ready(function() {
        $("a[type=contents]").click(function() {
            // 講座IDを取得
            var folder_id = $(this).attr("folder_id");

            // 親画面のツリーをクリックする
            var tree = window.opener.$("a[type=tree]");
            if (tree != null) { // ツリーがなかった場合に備えて
                window.opener.$("a[type=tree][folder_id=" + folder_id + "]").trigger("click");
            }
            window.close();
        });
        tryFocus("keyword");
    });
    {/literal}
</script>





<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>講座内検索</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





<div class="usual_box">

    <form name="sub_main" action="" method="post" target="_self">
        <input type="hidden" name="event" value="ev_contents_search" />
        <input type="hidden" name="btn_event" value="be_search" />
        <span class="brown2">実施年度</span>
        <input type="text" class="text" size="50" name="bunrui_year" value="{$bunrui_year|escape}" onkeyup="if(event.keyCode==13) document.sub_main.submit()" style="width:50px" class="numeric" />
        <span class="brown2" style="padding-left:20px">検索ワード</span>
        <input type="text" class="text" size="60" id="keyword" name="keyword" value="{$keyword|escape}" onkeyup="if(event.keyCode==13) document.sub_main.submit()" style="width:400px" />
        <button class="btn-main" id="search" onclick="document.sub_main.submit()">検索</button>
        <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    </form>

    <div style="line-height:18px; padding:2px 0 5px 230px; border:0" class="fontsize12 gray2">
        研修講義の「タイトル」「内容」「検索キーワード」を部分一致検索します。<br/>
        複数入力する場合はスペースで区切ってください。（AND検索）
    </div>


    {if $btn_event eq "be_search"}
        <table id="contents_list" cellspacing="0" cellpadding="2" class="list">
            <tr>
                <th class="left" width="350">研修・講義</th>
                <th width="40">年</th>
                <th class="left">講座</th>
            </tr>
            {foreach from=$contents_list item=item }
            <tr>
                <td>
                {$item.title|escape}
                </td>
                <td class="center">{$item.bunrui_year|escape}</td>
                <td style="padding:0 0 0 4px;">
                    <a type="contents" contents_id="{$item.id}" class="block" folder_id="{$item.folder_id}" href="javascript:void(0)"
                        style="background:url(../img/icon/folder.gif) no-repeat 0 4px; padding-left:20px; line-height:20px;"
                        >{$item.folder_joint_name|escape}
                    </a>
                </td>
            </tr>
            {/foreach}
        </table>

        {if $contents_list|@count eq 0}
            <div style="padding:10px 5px" class="gray2">該当データは存在しません。</div>
        {/if}
    {/if}
</div>



</body>
</html>
