{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}{$title}</title>
    <script type="text/javascript" src="js/jquery.treeview.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.treeview.css" />
</head>
<body onresize="resized()">
<!-- ///study_folder  -->






<script type="text/javascript">
    var target_label = "{$target_label}";
    {literal}

    function pickup_folder() {
        var folder_id = $("[name=target_folder_id]").val();
        var folder_name = $("[name=target_folder_name]").val();
        if (!folder_id) return alert("講座を指定してください。");
        {/literal}{if $js_callback}
        if (opener && opener.{$js_callback}) opener.{$js_callback}(folder_id, folder_name);
        window.close();
        {/if}
        {literal}
    }

    // 講座選択のイベントハンドラ
    function tree_click(target) {
        $(".selected_fileitem").removeClass("selected_fileitem").addClass("unselected_fileitem");
        target.removeClass("unselected_fileitem").addClass("selected_fileitem");
        $("[name=target_folder_id]").val(target.attr("folder_id"));
        $("[name=target_folder_name]").val(target.html());
        $("[name=parent_folder_id]").val(target.attr("parent_id"));
    }

    function getByte(string){
        if(string.length == 0) return 0;
        var count = 0;
        for(var i = 0; i <string.length; i++){
            if( escape(string.charAt(i)).length  < 4 ) count = count + 1;
            else count = count + 2;
        }
        return count;
    }
    // 実行
    function execute(btn, btn_event) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var folder_name = $("[name=folder_name]").val(); // 講座名
        if (folder_name == "") { alert("講座名が空です。"); $("[name=folder_name]").focus(); return; }
        // 最上位講座が指定されていなければターゲットを取得する
        if (btn_event!="be_edit_folder") {
	        if ($("input[name=select_folder_name]:checked").val() != "top") {
	            if (!$("[name=target_folder_id]").val()) {
	                alert(target_label+"を指定してください。"); return;
	            }
	        }
	    }

        // バイト数チェック(EUC-JP最大で60byte)
        if (getByte(folder_name) > 60) { alert("講座名は全角30文字まで入力可能です。"); tryFocus("folder_name"); return; }

        // 最上位講座が指定されていたらターゲットを空にする
        if ($("input[name=select_folder_name]:checked").val() == "top") {
            $("[name=target_folder_id]").val("");
            $("[name=parent_folder_id]").val("");
        }
        document.sub_main.btn_event.value = btn_event;
        document.sub_main.submit();
    }

    function exec_delete(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if(confirm("削除します。よろしいですか？")) { $("[name=btn_event]").val("be_delete_folder"); document.sub_main.submit(); }
    }


    function exec_move(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        $("[name=btn_event]").val("be_move_folder");
        // 子の講座へは移動できない(普通のディレクトリシステムと同じ仕様)
        var current_folder_id = $("[name=current_folder_id]").val(); // 編集対象の講座
        var target_folder_id = $("[name=target_folder_id]").val(); // 移動先

        // 最上位講座が指定されていたらターゲットを空にする
        if ($("input[name=select_folder_name]:checked").val() == "top") {
            // root講座を最上位に移動する場合はエラー
            if ($("#folder_tree_folder_" + current_folder_id).attr("parent_id") == "") { alert("送り側と受け側の講座が同じです。"); return; }
            $("[name=target_folder_id]").val("");
            $("[name=parent_folder_id]").val("");
        } else {
            if ($(".selected_fileitem").size() == 0) { alert("移動先が選択されていません"); return; }
            if (current_folder_id == target_folder_id) { alert("送り側と受け側の講座が同じです。"); return; }
            if ($("[name=target_folder_id]").val() == $("[name=current_parent_id]").val()) { alert("移動先を指定してください。"); return; }
            var chkFunc = function(current, target) {
                var selector = $("#folder_tree_folder_" + target);
                if (selector.size() > 0) {
                    var parent_id = selector.attr("parent_id");
                    if (parent_id == current) return false;
                    else return chkFunc(current, parent_id);
                } else {
                    return true;
                }
            };

            if (!chkFunc(current_folder_id, target_folder_id)) { alert("子講座へは移動できません。"); return; }
        }

        document.sub_main.submit();
    }
    // body_onload
    $(document).ready(function() {

        // 講座選択のイベントハンドラ
        $("a" + "[type=tree]" + "[id^=folder_tree]").click(function() { tree_click($(this)); });

        $("#top").click(function() { if ($("#specified").attr("disabled") != true) $("[name=tree_toggle]").toggle(); });

        $("#specified").click(function() { $("[name=tree_toggle]").toggle(); });

        // イベントの判定
        var selected_folder_id = $("[name=selected_folder_id]").val();
        var current_parent_id = $("[name=current_parent_id]").val();

        var event = $("[name=event]").val();

        if (event == "ev_create_folder") {
            if (selected_folder_id == "") { // これはまだ講座が存在しない場合にしか行われない処理
                $("#specified").attr("disabled", "disabled").addClass("btn-off"); // 他の講座指定を非活性
                $("#top").trigger("click"); // 他の講座指定のradioを選択
            } else {
                $("#specified").trigger("click"); // 他の講座指定のradioを選択
                $("#folder_tree_folder_" + selected_folder_id).trigger("click"); // 作成先のツリーを選択
            }
        } else if (event == "ev_pickup_folder") {
            $("#specified").trigger("click"); // 他の講座指定のradioを選択
            if (selected_folder_id) $("#folder_tree_folder_" + selected_folder_id).trigger("click"); // 現在の親講座を選択
        } else {
            $("#specified").trigger("click"); // 他の講座指定のradioを選択
            $("#folder_tree_folder_" + current_parent_id).trigger("click"); // 現在の親講座を選択
        }
        $("#folder_tree").treeview({collapsed:false, animated:"fast" }); // ツリー初期表示
        if ("success" == $("[name=result]").val()) { parentRefreshRequest("mainform"); window.close(); }
    });
    {/literal}
</script>





<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>{$title}</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>






<div class="usual_box">
<form name="sub_main" action="" method="post" target="_self">
    <input type="hidden" name="target_folder_id" value="" />
    <input type="hidden" name="target_folder_name" value="" />
    <input type="hidden" name="btn_event" value="" />
    <input type="hidden" name="call_from" value="{$call_from}" />
    <input type="hidden" name="event" value="{$event}" />
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="selected_folder_id" value="{$selected_folder_id}" />
    <input type="hidden" name="parent_folder_id" value="{$parent_folder_id}" />
    <input type="hidden" name="current_folder_id" value="{$current_folder_id}" />
    <input type="hidden" name="current_parent_id" value="{$current_parent_id}" />
    <input type="hidden" name="folder_joint_name" value="{$folder_joint_name|escape}" />
    <input type="hidden" name="current_folder_name" value="{$current_folder_name|escape}" />
    <input type="hidden" name="result" value="{$result}" />

    {if not $edit_auth}{if $event eq "ev_create_folder"}
    <div style="padding-bottom:5px"><div class="warningbox">講座の作成権限がありません。</div></div>
    {/if}{/if}
    {if not $edit_auth}{if $event eq "ev_edit_folder"}
    <div style="padding-bottom:5px"><div class="warningbox">選択講座への編集権限がありません。</div></div>
    {/if}{/if}

    <table cellspacing="0" cellpadding="0" class="inputlist">
        {* 編集モードなら現在の講座を表示 *}
        {if "ev_edit_folder" eq $event }
        <tr>
            <th width="100">現在の講座</td>
            <td>{$folder_joint_name|escape}</td>
        </tr>
        {/if}
        <tr>
            <th width="100">{$target_label}を指定</th>
            <td>
                <div {if not $is_topnode_radio_selectable} style="display:none"{/if}>
                    <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                    <th><input id="top" type="radio" name="select_folder_name" size="50" value="top"></th>
                    <td><label for="top">最上位講座を指定</label></td>
                {if $folder_list|@count ne 0}
                    <th><input id="specified" type="radio" name="select_folder_name" size="50" value="specified"></th>
                    <td><label for="specified">他の講座を指定</label></td>
                {/if}
                    </tr></table>
                </div>

                {if $folder_list|@count ne 0}
                <div style="padding:5px">
                    <div name="tree_toggle" style="position:relative; height:190px; width:400px; background-color:#ffffff; border:#aaaaaa solid 1px; overflow:scroll; display:none;">
                        {tree folder_list=$folder_list tree_id="folder_tree" selected_folder_id=$selected_folder_id event=$event }
                    </div>
                    <div name="tree_toggle">
                    </div>
                </div>
                {/if}
            </td>
        </tr>
        <tr {if $event eq "ev_pickup_folder"} style="display:none"{/if}>
            <th>講座名</th>
            <td>
                <input type="text" class="text" id="folder_name" name="folder_name" size="55" style="ime-mode:active;" value="{$folder_name|escape}" maxlength="60">
                <span class="gray">*60字</span>
            </td>
        </tr>
    </table>





    <div style="text-align:right; padding:5px 0">
        {if $event eq "ev_pickup_folder"}
            {if $call_from eq "study_edu_tantou"}
            <div style="padding:0px 0; text-align:left" class="gray">
            指定講座より配下の講座／研修講義へのアクセス権限を付与します。
            </div>
            {/if}
            <button type="button" class="btn-main" onclick="pickup_folder()">選択</button>
        {else}
            {if $event eq "ev_create_folder"}
                <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" id="exec_btn" onclick="execute(this, 'be_create_folder')">作成</button>
            {else}
                <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" id="exec_btn" onclick="execute(this, 'be_edit_folder')">名称変更</button>
            {/if}
            {* 編集モードなら削除ボタンも表示 *}
            {if "ev_edit_folder" eq $event }
            <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" onclick="exec_move(this)">移動</button>
            <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" onclick="exec_delete(this)">削除</button>
            {/if}
        {/if}
    </div>



</form>
</div>


</body>
</html>
