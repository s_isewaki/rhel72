{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}集合研修申込管理</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_assembly_list  -->





<script type="text/javascript">
    var result = "{$result}";
    var contents_id = "{$contents_id}";
    {literal}
    $(document).ready(function() {
        if (result=="success") parentRefreshRequest("mainform");

        $("a" + "[event=ev_date_edit]").click(function() { date_edit($(this)); }); // 開催日編集画面を開く
        // 選択ボタン制御
        var cnt = document.getElementsByName("dates[]").length;
        if (ee("all_check_day")) ee("all_check_day").disabled = (cnt==0);

        $("#start_day").datepicker( { dateFormat:"yy年mm月dd日", buttonImageOnly:true }); // 開催開始日にカレンダー適用
        $("#end_day").datepicker( { dateFormat:"yy年mm月dd日", buttonImageOnly:true }); // 開催終了日編集にカレンダー適用
    });
    //開催日を削除
    function delete_assembly(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if ($("input:checkbox[name='dates[]']:checked").size() == 0) { alert("削除対象の開催日が選択されていません。"); return; }
        if (!confirm("開催日・開催場所登録を削除します。\nユーザの出席申込みがある場合は、未申込状態になります。\n\n削除してよろしいですか？")) return;
        $("[name=event]").val("ev_exec_delete_assembly");
        document.sub_main.submit();
    }
    //受付期間更新
    function update_deadline(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if (!convertAndCheckDateTime()) return false;
        if (!confirm("更新します。よろしいですか？")) return;
        $("[name=event]").val("ev_exec_update_deadline");
        document.form_main.submit();
    }
    //集合研修作成画面を開く
    function add_date(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url = "study_assembly.php?session="+session+"&contents_id="+contents_id+"&event=ev_show_assembly_kaisaibi";
        window_open(url, "date_edit", 560, 330);
    }
    //集合研修編集画面を開く
    function date_edit(target) {
        var no  = target.attr("no");
        var url = "study_assembly.php?session="+session+"&contents_id=" + contents_id + "&no=" + no +  "&event=ev_show_assembly_kaisaibi";
        window_open(url, "date_edit", 560, 330);
    }
    //出欠登録画面を開く
    function show_student_list(no, assembly_actual_status) {
        var url = "study_student.php?session="+session+"&contents_id="+
        contents_id + "&no=" + no +  "&assembly_actual_status="+assembly_actual_status;
        window_open(url, "setting_student3", 1000, 650);
    }
    function allCheckDay(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var $btn = $("#all_check_day").html();
        if ($btn == "選択") {
            $("#all_check_day").html("解除");
            $("input:checkbox[name^=dates]").prop({'checked':'checked'});
        } else {
            $("#all_check_day").html("選択");
            $("input:checkbox[name^=dates]").prop({'checked':false});
        }
    }
    {/literal}
</script>


<div id="title_area"><table cellspacing="0" cellpadding="0"><tr><th><nobr>集合研修申込管理</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents.title|escape}" /></td><td><a onclick="window.close();"></a></td></tr></table></div>
<div id="title_margin">&nbsp;</div>


{if not $edit_auth}
<div style="padding:5px 5px 0 5px"><div class="warningbox">権限がありません。参照のみ可能です。</div></div>
{/if}





<div style="padding:5px"><div class="section_title">■ 申込受付期間</div></div>



<div class="usual_box">
<form name="form_main" action="" method="post">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />
    <input type="hidden" name="start_date" value="" />
    <input type="hidden" name="end_date" value="" />
    <input type="hidden" name="result" value="{$result}" />
    <input type="hidden" name="event" value="" />
    <input type="button" id="update_student" value="更新" style="display:none" />

    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th style="width:70px"><nobr>受付期間</nobr></th>
            <td><nobr>
                <div style="padding:2px">
                <span class="brown">開始日時</span>
                <input type="text" class="text" size="18" name="start_day" id="start_day" readonly value="{$array_start[0]}" style="width:120px" />
                <select name="st_hour">
                    {section name=st_item start=0 loop=24 step=1}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $array_start[1]}
                            <option value="{$smarty.section.st_item.index}" selected>{$smarty.section.st_item.index}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index}">{$smarty.section.st_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="st_minute">
                    {section name=st_item start=0 loop=60 step=5}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $array_start[2]}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}" selected>{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}">{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分</span>
                &nbsp;&nbsp;&nbsp;
                <span class="brown">終了日時</span>
                <input name="end_day" id="end_day" type="text" class="text" size="18" readonly value="{$array_end[0]}" style="width:120px" />
                <select name="ed_hour">
                    {section name=ed_item start=0 loop=24 step=1}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $array_end[1]}
                            <option value="{$smarty.section.ed_item.index}" selected>{$smarty.section.ed_item.index}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index}">{$smarty.section.ed_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="ed_minute">
                    {section name=ed_item start=0 loop=60 step=5}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $array_end[2]}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}" selected>{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}">{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分</span>
                </div>
            </nobr></td>
        </tr>
        <tr>
            <th>備考</th>
            <td><input type="text" class="text" size="100" maxlength="200" name="notes" value="{$notes|escape}" /></td>
        </tr>
    </table>
    <div style="padding:10px 0; text-align:right">
        <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" onclick="update_deadline(this)">更新</button>
    </div>
</form>
</div>








<div style="padding:5px"><div class="section_title">■ 開催日・開催場所</div></div>
<div class="usual_box">
    <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
    <td>
        <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" onclick="add_date(this)">追加</button>
        <button type="button" class="btn-main{if $asm_list|@count eq 0 || (not $edit_auth)} btn-off{/if}" onclick="delete_assembly(this)">削除</button>
    </td>
    <td class="right">
        <span style="padding-left:20px">受講者：<a href="javascript:void(0)" onclick="show_student_list('', '')">{$student_count}人</a></span>
        <span style="padding-left:20px">欠席申込：<a href="javascript:void(0)" onclick="show_student_list('-2', '')">{$kesseki}人</a></span>
        <span style="padding-left:20px">申込未登録：<a href="javascript:void(0)" onclick="show_student_list('-1', '')">{$mitouroku}人</a></span>
        <span style="padding-left:20px">集合研修済：<a href="javascript:void(0)" onclick="show_student_list('', '1')">{$contents.assembly_actual_count}人</a></span>
    </td>
    </tr></table>
</div>






<div class="usual_box">
<form name="sub_main" action="" method="post">
    <input type="hidden" name="event" value="" />
    <table cellspacing="0" cellpadding="0" class="list">
        <tr>
            <th width="60"><nobr><button type="button" class="btn-sel{if $asm_list|@count eq 0 || (not $edit_auth)} btn-off{/if}" id="all_check_day" onclick="allCheckDay(this)">選択</button></nobr></th>
            <th width="140"><nobr>開催日</nobr></th>
            <th width="110"><nobr>時間</nobr></th>
            <th width="auto"><nobr>場所</nobr></th>
            <th width="100"><nobr>講師</nobr></th>
            <th width="50"><nobr>定員</nobr></th>
            <th width="60"><nobr>出席申込</nobr></th>
        </tr>

        {foreach from=$asm_list item=item}
            <tr>
                <td class="center"><input name="dates[]" type="checkBox" value="{$item.no}" /></td>
                <td class="center"><a event="ev_date_edit" no="{$item.no}" href="javascript:void(0)">{$item.date|escape}</a></td>
                <td class="center">{$item.start_time|escape}〜{$item.end_time|escape}</td>
                <td>{$item.place|escape}</td>
                <td>{$item.lecturer|escape}</td>
                <td class="center"
                {if $item.persons_limit eq "0"} navi="定員ゼロの場合、ユーザは申込みが行えなくなります。" style="color:#ff0000; font-weight:bold"{/if}
                >{if $item.persons_limit ne ''}{$item.persons_limit|escape}人{else}(なし){/if}</td>
                <td class="center"><a href="javascript:void(0)" onclick="show_student_list({$item.no}, '')">{$item.syusseki|escape}人</a></td>
            </tr>
        {/foreach}
    </table>
</form>

{if $asm_list|@count eq 0}<div style="padding:10px 5px" class="gray2">開催情報は登録されていません。</div>{/if}

</div>



<iframe frameborder="0" name="download_frame" style="position:absolute; left:0; top:0; width:0; height:0"></iframe>



</body>
</html>
