<!-- ///contents_header  -->
<? //========================================================================== ?>
<? // 管理画面⇔ユーザ画面のリンクを表示する。                                  ?>
<? // 管理者の場合は管理メニューへのリンクを表示。                              ?>
<? // ユーザの場合は管理メニューへの遷移がないため、このスペーシングだけ行う。  ?>
<? //========================================================================== ?>
<?
    // お知らせ横のnewアイコン。display:noneで追加。実際に表示させるのは各画面のJavaScript。
    $notify_new_icon = '<img id="notify_new_icon" src="img/notify_new.gif" alt="" style="display:none; vertical-align:top; padding:6px 0 0 5px" />';

    $menues = array();
    //require(dirname(dirname(dirname(__FILE__))) . '/conf/conf.inf');
    if (GG_ADMIN_FLG && substr($current_screen,0,5)=="admin") {
        $menues["admin@study_folder_admin"] = array("url" => "study_folder_admin.php", "name" => "講座管理");
        $menues["admin@study_edu_admins"] = array("url" => "study_edu_admins.php", "name" => "教育責任者");
        $menues["admin@study_edu_tantou"] = array("url" => "study_edu_tantou.php", "name" => "教育担当者");
        if(GG_BARITESS_NENSUU_OPT_FLAG){
            $menues["admin@study_edu_nensuu"] = array("url" => "study_edu_nensuu.php", "name" => "職員経験年数");
        }
    } else {
        $menues["user@study_notif_user"] = array("url" => "study_notif_user.php", "name" => "お知らせ".$notify_new_icon);
        $menues["user@study_menu_user"] = array("url" => "study_menu_user.php",  "name" => "研修・講義");
        if (GG_STUDY_ADMIN_FLG || GG_STUDY_TANTOU_FLG) {
           $menues["user@study_main"] = array("url" => "study_main.php", "name" => "研修・講義管理");
           $menues["user@study_emp_search"] = array("url" => "study_emp_search.php", "name" => "職員検索");
        }
    }
?>
<table width="100%" cellspacing="0" cellpadding="0" id="tblLogoMenu">
    <tr>
        <td style="width:234px"><nobr>
        <a href="study_menu_user.php?session=<?=GG_SESSION_ID?>" style="letter-spacing:2px">
            <img src="../img/icon/b45.gif?v=1" width="32" height="32" border="0" alt="バリテス"><img src="./img/baritess.gif" width="199" height="32" border="0" alt="バリテス" style="padding-left:8px"></nobr></a></td>
        <? if (GG_ADMIN_FLG) {?>
        <td style="padding-right:6px; text-align:right">
            <? if (substr($current_screen,0,5)=="admin") { ?>
                <a href="study_menu_user.php?session=<?=GG_SESSION_ID?>" style="font-size:16px; font-weight:bold">ユーザ画面へ</a>
            <? } else { ?>
                <a href="study_folder_admin.php?session=<?=GG_SESSION_ID?>" style="font-size:16px; font-weight:bold">管理画面へ</a>
            <? } ?>
        </td>
        <? } ?>
    </tr>
</table>

<? //========================================================================== ?>
<? // タブメニューを表示する。                                                  ?>
<? // @param $menu_array 表示メニューの連想配列                                 ?>
<? // @param $event イベント指定(連想配列のキー)                                ?>
<? //========================================================================== ?>
<div id="divMainLinkWrapper">
<div id="divMainLinkWrapper2">
<table cellspacing="0" cellpadding="0" id="tblMainLink">
    <tr>
        <? foreach ($menues as $menu_name => $menu_info) { ?>
        <?     if ($menu_name==$current_screen) { ?>
                <td><nobr><?=$menu_info["name"]?></nobr></td>
            <? } else { ?>
                <td class="link"><a href="<?=$menu_info["url"]?>?session=<?=GG_SESSION_ID?>"><nobr><?=$menu_info["name"]?></nobr></a></td>
            <? } ?>
        <? } ?>
        <td class="after_margin">&nbsp;</td>
    </tr>
</table>
</div>
</div>

