{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}教育責任者</title>
</head>
<body onresize="resized()">
<!-- ///study_edu_admins  -->




<script type="text/javascript">
    {literal}
    function showHistory(emp_id) {
        var url  = "study_menu_user.php?session="+session+"&is_popup=1&emp_id=" + emp_id;
        window_open(url, "emp_history_view", 1000, 600);
    }

    // 職員選択画面を開く
    function open_emp_select() {
        var url = "study_student.php?btn_event=be_show_student_selector&session="+session+"&js_callback=add_edu_admins";
        window_open(url, "emp_select", 800, 580);
    }

    // 職員のチェックボックスを制御
    function allCheckEmp(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var caption = $("#all_check_emp").html();
        var ary = document.getElementsByName("emp_id");
        var bool = (caption == "選択");
        for (var idx=0; idx<ary.length; idx++) ary[idx].checked = bool;
        $("#all_check_emp").html(bool?"解除":"選択");
    }

    function add_edu_admins(emp_id_list) {
        document.mainform.emp_id_comma_list.value = emp_id_list.join(",");
        document.mainform.btn_event.value = "be_add_employee";
        document.mainform.submit();
    }

    function delete_edu_admins(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if ($("input:checkbox[name='emp_id']:checked").size() == 0) {
            alert("削除対象の職員チェックボックスが選択されていません。");
            return;
        }
        var ary = document.getElementsByName("emp_id");
        var emp_id_list = [];
        for (var idx=0; idx<ary.length; idx++) {
            var elem = ary[idx];
            if (elem.checked) emp_id_list.push(ary[idx].value);
        }
        if (!confirm("チェックを行った職員を削除します。よろしいですか?")) return;

        ee("emp_id_comma_list").value = emp_id_list.join(",");
        document.mainform.btn_event.value = "be_del_employee";
        document.mainform.submit();
    }
    {/literal}
</script>





{* コンテンツヘッダー *}
{php} $current_screen = "admin@study_edu_admins"; require_once("view/contents_header.tpl"); {/php}





<form name="mainform" method="POST">
<input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
<input type="hidden" name="event" id="event" value="" />
<input type="hidden" name="btn_event" value="" />
<input type="hidden" name="emp_id_comma_list" id="emp_id_comma_list" value="" />
<div style="padding-top:5px">

    <div>
        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:400px"><tr>
            <td><button type="button" class="btn-main" id="select_student" onclick="open_emp_select();">追加</button></td>
            <td class="right" style="padding-right:0"><button type="button" class="btn-main"
                id="select_student" onclick="delete_edu_admins(this);">削除</button></td>
        </tr></table>
    </div>

    <div style="padding-top:5px">

        <table cellspacing="0" cellpadding="0" class="list" style="width:400px">
            <tr>
                <th width="60"><button type="button" class="btn-sel{if $emp_list_count eq 0} btn-off{/if}" id="all_check_emp"
                    onclick="allCheckEmp(this)">選択</button></th>
                <th width="130">責任者職員ID</th>
                <th>責任者職員氏名</th>
            </tr>
        </table>

        {if isset($emp_list)}
        <div style="width:400px">
            <table cellspacing="0" cellpadding="0" class="list" style="width:400px">
                {foreach from=$emp_list item=item}
                <tr>
                    <td width="60" class="center"><input name="emp_id" type="checkBox" value="{$item.emp_id}" /></td>
                    <td width="130">{$item.emp_personal_id}</td>
                    <td>{$item.emp_lt_nm|escape}&nbsp;{$item.emp_ft_nm|escape}</td>
                </tr>
                {/foreach}
            </table>
        </div>
        {/if}

    </div>

</div>
</form>
</body>
</html>
