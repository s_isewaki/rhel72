<!-- /// study_main_admin_right_part  -->

    <input type="hidden" id="cur_page_num2" value="<?=$page_num?>" />
    <input type="hidden" id="cur_per_page_size2" value="<?=$per_page_size?>" />
    <input type="hidden" id="total_count2" value="<?=$total_count?>" />
    <input type="hidden" id="last_page2" value="<?=$last_page?>" />
    <input type="hidden" id="sort_string2" value="<?=$sort_string?>" />


    <?if ($right_part_call_from!="study_folder_admin"){?>
    <div style="height:28px">
        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
        <td><nobr>
            <button type="button" class="btn-main<?if (!$edit_auth){?> btn-off<?}?>" id="create_contents_btn1" onclick="open_contents_create(this)" style="width:134px">研修・講義新規登録</button>
            </nobr>
        </td>

        
        <td style="text-align:right; padding-right:0"><nobr>
            <button type="button" class="btn-main" id="search_contents_btn" onclick="search_contents()" style="width:90px">講座内検索</button>
            <button type="submit" class="btn-main<?if (!$edit_auth){?> btn-off<?}?>" name="export_csv_btn" value="export_csv_btn" onclick="export_csv()" value="export_csv" style="width:100px">集合研修一覧出力</button>
            </nobr>
        </td>
        
        
        </tr></table>
    </div>
    <? } ?>


    <div style="height:28px">
        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:auto"><tr>

<?if ($right_part_call_from=="study_folder_admin"){?>
    <td style="color:#de5836"><nobr>≪研修・講義≫</nobr></td>
<? } ?>
        <td style="padding-left:5px">
            <select onchange="change_ext_joken(this.value)" navititle="" navi="未修：未修のみを表示<br>公開中：公開中の研修・講義のみを表示">
                <option value=""></option>
                <option value="misyu_only"<?=($ext_joken=="misyu_only"?" selected":"")?>>未修</option>
                <option value="pub_only"<?=($ext_joken=="pub_only"?" selected":"")?>>公開中</option>
            </select>
        </td>

        <td>
            <select navititle="並べ替え" navi="&nbsp;" onchange="change_sort2(this.value)">
            <option value="create_date-desc" style="font-size:12px" navi="登録日時 降順（新しい順）で並べ替え"<?=($sort_string=="create_date-desc"?" selected":"")?>>▼登録日</option>
            <option value="create_date-asc" style="font-size:12px" navi="登録日時 昇順（古い順）で並べ替え"<?=($sort_string=="create_date-asc"?" selected":"")?>>▲登録日</option>
            <option value="title-desc" navi="研修・講義タイトル 降順で並べ替え"<?=($sort_string=="title-desc"?" selected":"")?>>▼ﾀｲﾄﾙ</option>
            <option value="title-asc" navi="研修・講義タイトル 昇順で並べ替え"<?=($sort_string=="title-asc"?" selected":"")?>>▲ﾀｲﾄﾙ</option>
            </select>
        </td>

        <td style="letter-spacing:0; width:80px; padding-left:2px"><nobr>
            <span style="padding-right:4px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b><?=$total_count?></b></span>件</span>
        </td>

    <?if ($right_part_call_from=="study_folder_admin"){?>
    </tr></table>
    </div>
    <div style="height:28px">
    <table cellspacing="0" cellpadding="0" class="rgroup" style="width:auto"><tr>
    <? } ?>



        <td><nobr>
            <span style="font-size:12px; padding-left:1px">表示件数</span><!--
            --><select name="per_page_size2" id="per_page_size2" onchange="$('#page_num').val(1); getListContentsViaAjax()">
                <? $ary = array(5,10,20,30,40,50,100,200,300,500,-1); ?>
                <? foreach ($ary as $v) {?><option value="<?=$v?>"<?=($v==$per_page_size?" selected":"")?>><?=($v==-1?"全件":$v."件")?></option><?}?>
            </select></nobr>
        </td>

        <td style="font-size:12px"><nobr>
            <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" /><!--
            <?$dis = ($page_num<=1?" disabled":""); ?>
            --><button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('top')"<?=$dis?>>≪</button><!--
            --><button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('prev')"<?=$dis?>>＜</button><!--
            --><input type="text" class="text imeoff" maxlength="3" style="width:26px" id="new_page_num2" value="<?=$page_num?>"
            ><span style="font-size:12px; letter-spacing:0; padding:0 3px 0 1px">ページ</span><!--
            <?$dis = ($last_page<=1?" disabled":""); ?>
            --><button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:38px" onclick="paging('move')"<?=$dis?>>移動</button><!--
            <?$dis = ($page_num>=$last_page?" disabled":""); ?>
            --><button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('next')"<?=$dis?>>＞</button><!--
            <?$dis = ($page_num>=$last_page?" disabled":""); ?>
            --><button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('last')"<?=$dis?>>≫</button></nobr>
        </td>
        </tr>
        </table>
    </div>







<table cellspacing="0" cellpadding="0" class="list padding_vertical0">
    <tr>
        <th class="left" width="auto">
            研修講義ﾀｲﾄﾙ
            <!-- table cellspacing="0" cellpadding="0" style="width:100%"><tr>
            <td style="border:0; padding:0; width:auto">
                <div style="position:relative"><nobr>
                    <? if ($first_sort=="title") { ?>
                    <img src="../img/color_5.gif" style="position:absolute; left:5px; top:1px; width:7px; height:2px" alt="" />
                    <? } ?>
                    <a href="javascript:void(0)" onclick="change_sort('title','<?=(strpos($sort_string,'title-desc')===FALSE?"desc":"asc")?>')"
                    ><img src="../img/<?=(strpos($sort_string,'title-desc')===FALSE?"up":"down")?>.gif" style="padding:0"
                        alt="" width="17" height="17" style="vertical-align: middle;">研修・講義</a>
                </nobr></div>
            </td>
            <td style="border:0; padding:0 0 0 10px; width:3%">
                <div style="position:relative"><nobr>
                    <? if ($first_sort=="create_date") { ?>
                    <img src="../img/color_5.gif" style="position:absolute; left:5px; top:1px; width:7px; height:2px" alt="" />
                    <? } ?>
                    <a href="javascript:void(0)" onclick="change_sort('create_date','<?=(strpos($sort_string,'create_date-desc')===FALSE?"desc":"asc")?>')"
                    ><img src="../img/<?=(strpos($sort_string,'create_date-desc')===FALSE?"up":"down")?>.gif" style="padding:0"
                        alt="" width="17" height="17" style="vertical-align: middle;">登録日</a>
                </nobr></div>
            </td>
            </tr></table -->

        </th>
        <?if ($right_part_call_from!="study_folder_admin"){?>
            <th width="32">年度</th>
            <th width="100"><nobr>教材・ﾃｷｽﾄ</nobr></th>
            <th width="44"><nobr>受講者</nobr></th>
            <th style="width:80px" colspan="2" class="fontsize12"><nobr>集合研修</nobr></th>
            <th style="width:80px" colspan="2"><nobr>ｱﾝｹｰﾄ</nobr></th>
            <th style="width:80px" colspan="2"><nobr>ﾃｽﾄ</nobr></th>
            <th width="50" class="fontsize12"><nobr>お知らせ</nobr></th>
            <th width="50"><nobr>状況</nobr></th>
        <? } ?>
    </tr>

    <?if ($right_part_call_from!="study_folder_admin"){?>
    <?// 講座配下に講座がある場合 ?>
    <? foreach ($folder_list as $folder) { ?>
    <tr>
        <td style="padding:0">
            <a folder_id="<?=$folder["id"]?>" class="block" href="javascript:void(0)" onclick="change_directory('down', $(this))"
            navititle="講座" navi="（登録：<?=$folder["create_date_disp"]?>）"
            ><img src="../img/icon/folder.gif" alt="" width="17" height="17" style="vertical-align: middle;"
            ><?=h($folder["name"])?></a>
        </td>
        <td class="center"><span class="gray">―</span></td>
        <td class="center"><span class="gray">―</span></td>
        <td class="center"><span class="gray">―</span></td>
        <td class="center border0r" width="36"><span class="gray">―</span></td>
        <td class="center border0l" width="44"><span class="gray">―</span></td>
        <td class="center border0r" width="36"><span class="gray">―</span></td>
        <td class="center border0l" width="44"><span class="gray">―</span></td>
        <td class="center border0r" width="36"><span class="gray">―</span></td>
        <td class="center border0l" width="44"><span class="gray">―</span></td>
        <td class="center"><span class="gray">―</span></td>
        <td class="center"><span class="gray">―</span></td>
    </tr>
    <? } ?>
    <? } ?>

    <? // コンテンツを選択した場合 ?>
    <? $now = date("YmdHi"); ?>
    <? foreach ($contents_list as $item) { ?>
    <tr>
        <?if (!$item["student_count"]){?>
            <td style="padding:0"><a class="block" href="javascript:void(0)" onclick="open_contents_edit('ev_edit_contents', <?=$item["id"]?>);" navititle="公開されていません" navi="受講者を１人以上指定してください。<br>（登録：<?=$item["create_date_disp"]?>）"><?=h($item["title"])?></a></td>
        <?}else if ($item["is_out_of_date"]){?>
            <td style="padding:0"><a class="block" style="padding-right:0" href="javascript:void(0)" onclick="open_contents_edit('ev_edit_contents', <?=$item["id"]?>);" navititle="公開されていません" navi="本日現時刻において、この研修・講義は公開されていません。<br>集合研修の「申込み受付期間」／ｱﾝｹｰﾄの「開始・終了日時」／ﾃｽﾄの「開始・終了日時」<br>について、いずれも期間外か、その他内容が有効ではありません。<br>（登録：<?=$item["create_date_disp"]?>）"><?=h($item["title"])?></a></td>
        <?} else { ?>
            <td class="publishing_bk" style="padding:0" navititle="公開中" navi="本日現時刻で、公開されています。<br>（登録：<?=$item["create_date_disp"]?>）">
                <table cellspacing="0" cellpadding="0" style="width:100%" class="rgroup"><tr>
                <td width="17" style="padding:0 4px; border:0"><span class="publishing_mark">公</span></td>
                <td style="padding:0; line-height:22px; border:0"><a class="block" href="javascript:void(0)" onclick="open_contents_edit('ev_edit_contents', <?=$item["id"]?>);"><?=h($item["title"])?></a></td></tr></table>
            </td>
        <?}?>


<?if ($right_part_call_from!="study_folder_admin"){?>

        <?// 年度 ?>
        <td class="center"><?=h($item['bunrui_year'])?></td>




        <?// 教材・ﾃｷｽﾄ ?>
        <td class="center">
        <? $cnt = count($item['files_info']); ?>
        <? if ($cnt > 0) { ?>
            <a class="block" href="javascript:void(0)" onclick="show_download_dialog(<?=$item["id"]?>)"><nobr>閲覧<?if($cnt>1) echo "（x".$cnt."）";?></nobr></a>
        <? } ?>
        </td>



        <?// 受講者 ?>
        <?  $err = "";
            $navi = "";
            if (!$item["student_count"]) {
                $err=" tri_r"; $navi='navi="受講者を指定してください。"';
            }
        ?>
        <td class="center padding0<?=$err?>"<?=$navi?>>
            <a class="block" href="javascript:void(0)" onclick="setting_student(<?=$item["id"]?>)"><nobr><?=(int)$item['student_count']?></nobr></a>
        </td>



        <?// 集合研修 ?>
        <?  $err = "";
            $navi = 'navi="集合研修開催：'.((int)$item['assembly_count']).'回"';
            if ($item["assembly_start_date"] > $now || $now > $item["assembly_end_date"]) {
                $err=" tri_g"; $navi='navi="本日現時点では、期間外につき公開されていません。"';
            }
            if (!$item["assembly_start_date"] || !$item["assembly_end_date"]) {
                $err=" tri_r"; $navi='navi="集合研修申込期間が未指定です。公開されません。"';
            }
            if (!$item["assembly_count"]) {
                $err=" tri_r"; $navi='navi="集合研修開催日が作成されていないため、公開されません。"';
            }
        ?>
        <? if ($item["policy_info"]["assembly"]) { ?>
            <td class="center border0r padding0<?=$err?>"<?=$navi?> width="36">
                <a class="block" onclick="create_assembly(<?=$item["id"]?>)" href="javascript:void(0)"<?=$navi?>><nobr><?=(int)$item['assembly_count']?>回</nobr></a>
            </td>

            <?
                $mcnt = (int)@$item['assembly_actual_count'];
                $disp = '<div class="gray" navi="集合研修出席者は存在しません。">0</div>';
                if ($mcnt>0) {
                    $disp =
                    '<a class="block" href="javascript:void(0)" onclick="setting_student('.$item["id"].', 1)"'.
                    ' navi="集合研修出席者：'.$mcnt.'人">'.$mcnt.'</a>';
                }
            ?>
            <td class="center border0l" width="44"><nobr><?=$disp?></nobr></td>
        <? } else { ?>
            <td class="center border0r" width="36"><div class="gray" navi="集合研修は実施しない設定になっています。">―</div></td>
            <td class="center border0l" width="44"></td>
        <? } ?>





        <?// アンケート ?>
        <?  $err = "";
            $navi = "";
            if ($item["questionnaire_start_date"] > $now || $now > $item["questionnaire_end_date"]) {
                $err=" tri_g"; $navi='navi="本日現時点では、期間外につき公開されていません。"';
            }
            if (!$item["questionnaire_start_date"] || !$item["questionnaire_end_date"]) {
                $err=" tri_r"; $navi='navi="アンケート期間が未指定です。公開されません。"';
            }
            if (!$item["questionnaire_count"]) {
                $err=" tri_r"; $navi='navi="アンケートが作成されていないため、公開されません。"';
            }
        ?>
        <? if ($item["policy_info"]["questionnaire"]) { ?>
            <td class="center border0r padding0<?=$err?>"<?=$navi?> width="36">
                <a class="block" onclick="create_questionnaire(<?=$item["id"]?>)" href="javascript:void(0)"><nobr><?=(int)$item['questionnaire_count']?>題</nobr></a>
            </td>

            <?
                $disp = '<div class="gray" navi="先にｱﾝｹｰﾄを編集してください。">―</div>';
                if ((int)$item['questionnaire_count']) {
                    $mcnt = (int)@$item['questionnaire_answer_count'];
                    $disp = '<div class="gray" navi="ｱﾝｹｰﾄ回答者は存在しません。">0</div>';
                    if ($mcnt>0) {
                        $disp ='<a navi="ｱﾝｹｰﾄ回答者：'.$mcnt.'人" class="block" onclick="result_questionnaire('.$item["id"].')" href="javascript:void(0)">'.$mcnt.'</a>';
                    }
                }
            ?>
            <td class="center border0l" width="44"><nobr><?=$disp?></nobr></td>
        <? } else { ?>
            <td class="center border0r" width="36"><div class="gray" navi="ｱﾝｹｰﾄは実施しない設定になっています。">―</div></td>
            <td class="center border0l" width="44"></td>
        <? } ?>



        <?// テスト ?>
        <?  $err = "";
            $navi = "";
            if ($item["test_start_date"] > $now || $now > $item["test_end_date"]) {
                $err=" tri_g"; $navi='navi="本日現時点では、期間外につき公開されていません。"';
            }
            if (!$item["test_start_date"] || !$item["test_end_date"]) {
                $err=" tri_r"; $navi='navi="テスト期間が未指定です。公開されません。"';
            }
            if (!$item["test_count"]) {
                $err=" tri_r"; $navi='navi="テストが作成されていないため、公開されません。"';
            }
        ?>
        <? if ($item["policy_info"]["test"]) { ?>
            <td class="center border0r padding0<?=$err?>"<?=$navi?> width="36">
                <a class="block" onclick="show_test_edit(<?=$item["id"]?>)" href="javascript:void(0)"><nobr><?=(int)$item['test_count']?>問</nobr></a>
            </td>

            <?
                $disp = '<div class="gray" navi="先にテストを編集してください。">―</div>';
                if ((int)$item['test_count']) {
                    $mcnt = (int)@$item['test_ok_count'] + (int)@$item['test_ng_count'];
                    $disp = '<div class="gray" navi="テスト実施者は存在しません。">0</div>';
                    if ($mcnt>0) $disp = '<a navi="合格：'.((int)@$item['test_ok_count']).'／実施中：'.((int)@$item['test_ng_count']).'" class="block" onclick="show_test_edit_total('.$item["id"].')" href="javascript:void(0)">'.$mcnt.'</a>';
                }
            ?>
            <td class="center border0l" width="44"><nobr><?=$disp?></nobr></td>
        <? } else { ?>
            <td class="center border0r" width="36"><div class="gray" navi="ﾃｽﾄは実施しない設定になっています。">―</div></td>
            <td class="center border0l" width="44"></td>
        <? } ?>
        </td>


        <?//お知らせ?>
        <td class="center"><a class="block" onclick="notice_history('<?=$item["id"]?>')"
        navi="お知らせ作成数：<?=(int)$item['notif_count']?>件"href="javascript:void(0)"><?=(int)$item['notif_count']?>件</a></td>


        <?// 状況 ?>
        <td class="center padding0">
            <? if ((int)$item["contents_total_status"] && $item["student_count"]) { ?>
            <a class="block sumi" href="javascript:void(0)" onclick="show_contents_summary(<?=$item["id"]?>);"><nobr>受講済</nobr></a>
            <? } else { ?>
            <a class="block" href="javascript:void(0)" onclick="show_contents_summary(<?=$item["id"]?>);"><nobr>未修</nobr></a>
            <? } ?>
        </td>
<? } ?>
    </tr>
    <? } ?>
</table>

    <?if ($right_part_call_from=="study_folder_admin"){?>
    <? if (!count($contents_list)) { ?>
    <div style="padding:10px; background-color:#f5f5f5" class="gray">該当データは存在しません</div>
    <? } ?>
    <? } ?>
