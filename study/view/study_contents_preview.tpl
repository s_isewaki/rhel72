<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>研修・講義</title>
    <script type="text/javascript" src="js/jquery.treeview.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.treeview.css" />
    <style type="text/css">
        html { overflow-y:scroll; }
    </style>
</head>
<body onresize="resized()">
<!-- ///study_contents  -->




<iframe frameborder="0" name="download_iframe" id="download_iframe" style="position:absolute; left:0; top:0; width:0; height:0"></iframe>



<script type="text/javascript">
    var contents_id = "<?=$contents["id"]?>";
    var movie_dialog = 0;
    function show_study_contents_movie(file_path, movie_url) {
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.indexOf("msie 6.")!=-1 || ua.indexOf("msie 7.")!=-1 || ua.indexOf("msie 8.")!=-1 || ua.indexOf("safari")!=-1) { // IE6 or IE7 or IE8 or safari
            window.open(movie_url);
            return;
        }
        var url = "study_contents.php?session=<?=GG_SESSION_ID?>&btn_event=be_show_study_contents_movie"+
        "&contents_id=<?=$contents["id"]?>&file_path="+encodeURIComponent(file_path);
        movie_dialog = window.open(url, "", "width:1000,height=650"+dlgResizableOption);
    }
    function tryMovieDownload(url, isForward) {
        if (isForward) window.open(url);
        else ee("download_iframe").src = url;
        try { movie_dialog.close(); } catch(ex){};
    }
</script>



<div id="title_area"><table cellspacing="0" cellpadding="0"><tr><th><nobr>研修・講義</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="<?=h($contents["title"])?>" /></td><td><a onclick="window.close();"></a></td></tr></table></div>
<div id="title_margin">&nbsp;</div>
<div class="usual_box">


    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="120px"><nobr>研修講義タイトル</nobr></th>
            <td><?=h($contents["title"])?></td>
        </tr>
        <tr>
            <th>実施内容</th>
            <td>
                <?=$contents["policy_jp1"]?>
                <? if ($contents["policy_for_questionnaire"]=="A+"){?><br><span class="red">※ｱﾝｹｰﾄは集合研修出席者のみ実施</span><?}?>
                <? if ($contents["policy_for_questionnaire"]=="A-"){?><br><span class="red">※ｱﾝｹｰﾄは集合研修の非出席者のみ実施</span><?}?>
                <? if ($contents["policy_for_test"]=="A+"){?><br><span class="red">※ﾃｽﾄは集合研修出席者のみ実施</span><?}?>
                <? if ($contents["policy_for_test"]=="A-"){?><br><span class="red">※ﾃｽﾄは集合研修の非出席者のみ実施</span><?}?>
            </td>
        </tr>
        <tr>
            <th>内容</th>
            <td><?=str_replace("\n", "<br />", h($contents["summary"]))?></td>
        </tr>
        <tr>
            <th>検索キーワード</th>
            <td><?=str_replace("\n", "<br />", h($contents["keyword"]))?></td>
        </tr>
    </table>









<div style="padding-top:20px; padding-bottom:5px">≪教材・テキスト≫</div>


<div style="padding:5px; display:none" id="file_exist_message">
    <div class="beige">ファイル数：&nbsp;<?=count($contents["files_info"])?></div>
</div>

<div style="padding:10px; display:none; background-color:#f5f5f5" class="gray2" id="file_not_exist_message">
    教材・テキストはありません。
</div>





<div class="usual_box">
    <?
        $file_exist = 0;
        foreach ($contents["files_info"] as $finfo) {
            $fp = $finfo["file_path"];
            $fn = $finfo["file_name"];
            $furl = $finfo["file_url"];
            $ft = $finfo["file_type"];
            $fno = $finfo["file_no"];
            $movie_url = $finfo["movie_url"];
            $browser_url = $finfo["browser_url"];
            $download_url = $finfo["download_url"];
            $capacity = $finfo["file_capacity_mb"];
            if ($capacity!="") $capacity .= "MB";
            else $capacity = '<span class="gray">―</span>';
            if ($fn=="") $fn = ($furl ? $furl : $fp);
            $file_exist = 1;
    ?>
        <table cellspacing="0" cellpadding="0" class="file_list" style="width:100%"><tr>
        <? if ($fn=="") { ?>
        <td width="auto" class="left" style="color:#ff0000">（※このファイルは現在、取得できません）</td>
        <? } else { ?>
            <td width="auto" class="left">
            <?
            if ($ft=="movie") {
                echo '<a href="'.$download_url.'" onclick="show_study_contents_movie(\''.$fp.'\', \''.$movie_url.'\'); return false;" navi="クリックすると、教材を新しいブラウザ別画面で開きます。">'.h($fn)."</a>";
            } else if ($furl) {
                echo '<a href="'.$furl.'" target="_blank" navi="クリックすると、教材を新しいブラウザ画面で開きます。">'.h($fn)."</a>";
            } else if ($ft=="browser") {
                if (substr($fp,0,strlen(GG_BASE_FOLDER))==GG_BASE_FOLDER) $fp = substr($fp, strlen(GG_BASE_FOLDER)+1);
                echo '<a href="'.$browser_url.'" target="_blank" navi="クリックすると、教材を新しいブラウザ画面で開きます。">'.h($fn)."</a>";
            } else {
                echo '<a target="download_iframe" navi="クリックするとダウンロードを開始します。"';
                echo ' href="tmp/contents/download/'.GG_SESSION_ID."/".h(basename($fp))."/".h($fn).'" target="download_iframe">'.h($fn)."</a>";
            }
            ?>
            </td>
        <? } ?>
        <th width="50"><nobr><span class="list_color">サイズ</span></nobr></th>
        <th width="70"><nobr><?=$capacity?></nobr></th>
        <th width="80" class="left"><nobr><?
            if($ft=="movie") echo "(動画形式)";
        ?></nobr></th>
        </tr>
        </table>
    <? } ?>
</div>
<script type="text/javascript">
<? if ($file_exist) { ?>
    ee("file_exist_message").style.display = "";
<? } else { ?>
    ee("file_not_exist_message").style.display = "";
<? } ?>
</script>


</div>
</body>
</html>