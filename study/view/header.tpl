<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1,requiresActiveX=true" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Content-Type" content="text/html;charset=EUC-JP" />
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/common.js"></script>
    <script type="text/javascript">
        var session = "<?=GG_SESSION_ID?>";
        var mm_disp_auth = "<?if (@$this && @$this->_tpl_vars["disp_auth"]) echo $this->_tpl_vars["disp_auth"]; else echo @$disp_auth ?>";
        var mm_edit_auth = "<?if (@$this && @$this->_tpl_vars["edit_auth"]) echo $this->_tpl_vars["edit_auth"]; else echo @$edit_auth ?>";


<? //---------------------------------------------------------------?>
<? // 所属プルダウン選択肢、ちょくちょく利用するのでここで出力      ?>
<? // 選択肢リストは$smarty変数にある場合もある。                   ?>
<? //---------------------------------------------------------------?>
<? if (!@$attr_list && @$this && is_array(@$this->_tpl_vars["attr_list"])) $attr_list = $this->_tpl_vars["attr_list"]; ?>
<? if (!@$dept_list && @$this && is_array(@$this->_tpl_vars["dept_list"])) $dept_list = $this->_tpl_vars["dept_list"]; ?>
<? if (!@$room_list && @$this && is_array(@$this->_tpl_vars["room_list"])) $room_list = $this->_tpl_vars["room_list"]; ?>
<? if (!@$folder_list && @$this && is_array(@$this->_tpl_vars["folder_list"])) $folder_list = $this->_tpl_vars["folder_list"]; ?>
var attr_list = [];
var dept_list = [];
var room_list = [];
var folder_list = [];
<? if (@$attr_list) { ?>
    <? foreach ($attr_list as $item) { ?>
        attr_list.push("<?=$item["class_id"]."\t".$item["atrb_id"]."\t".h($item["atrb_nm"])?>");
    <? } ?>
<? } ?>
<? if (@$dept_list) { ?>
    <? foreach ($dept_list as $item) { ?>
        dept_list.push("<?=$item["class_id"]."\t".$item["atrb_id"]."\t".$item["dept_id"]."\t".h($item["dept_nm"])?>");
    <? } ?>
<? } ?>
<? if (@$room_list) { ?>
    <? foreach ($room_list as $item) { ?>
        room_list.push("<?=$item["class_id"]."\t".$item["atrb_id"]."\t".$item["dept_id"]."\t".$item["room_id"]."\t".h($item["room_nm"])?>");
    <? } ?>
<? } ?>
<? if (@$folder_list) { ?>
    <? foreach ($folder_list as $item) { ?>
        folder_list.push("<?=((int)$item["id"])."\t".((int)$item["parent_id"])."\t".h($item["name"])?>");
    <? } ?>


var folder_dropdown_blank_string = "全て";
function folderChanged(hier, folderId, doSubmit) {
    if (hier>0) setSelection(ee("ddl_folder"+hier), folderId);
    var prefix = "ddl_folder";
    var childHier = hier;
    for(;;) {
        childHier++;
        var child = ee(prefix+childHier);
        if (!child) break;
        child.parentNode.removeChild(child);
    }
    var ids = [];
    for (var idx=1; idx<=hier; idx++) {
        var ddl = ee("ddl_folder"+idx);
        if (ddl.options[ddl.selectedIndex].value) ids.push(ddl.options[ddl.selectedIndex].value);
    }
    var childIds = [];
    if (folderId!=="") {
        childHier = int(hier)+1;
        var html = [];
        var cap = (hier==0 ? folder_dropdown_blank_string : "全て");
        html.push('<select id="ddl_folder'+childHier+'" onchange="folderChanged('+childHier+', this.value, 1)">');
        html.push('<option value="">'+cap+'</option>');
        for(var idx=0; idx<folder_list.length; idx++) {
            var params = folder_list[idx].split("\t");
            if (params[1]==folderId) html.push('<option value="'+params[0]+'">'+params[2]+'</option>');
        }
        html.push('</select>');
        $("#ddl_folder_container").append(html.join(""));
    }
    ee("folder_selected_ids").value = ids.join("\t");
    if (doSubmit) document.mainform.submit();
}
<? } ?>

    </script>
