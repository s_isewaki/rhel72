{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}集合研修 受講実績の登録</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_assembly_date  -->




<script type="text/javascript">
    var result = "{$result}";
    {literal}
    $(document).ready(function() {
        $("#import").click(function() {
            var ymd = ee("assembly_application_day").value;
            if (ymd=="") { alert("日付を指定してください。"); return ""; }
            ymd = getYYYYMMDDFromJpYMD(ymd);
            if (ymd=="") { alert("日付を正しく指定してください。"); return ""; }
            ee("assembly_application_date").value = ymd;
            document.mainform.submit();
        });

        if (result=="success") {
            window.opener.document.form_sub.submit();
            grandParentRefreshRequest("mainform");
            window.close();
        }
        $("#assembly_application_day").datepicker( { dateFormat:"yy年mm月dd日", buttonImageOnly:true }); // 開催開始日にカレンダー適用
        ee("emp_id_comma_list").value = window.opener.document.getElementById("emp_id_comma_list").value;
    });
    {/literal}
</script>




<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>集合研修 受講実績の登録</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>



<div class="usual_box">
    <div style="padding:5px">
        呼び出し元画面でチェックを行った職員を受講済みにします。
    </div>
    <form name="mainform" action="" method="post" enctype="multipart/form-data">
        <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
        <input type="hidden" name="btn_event" value="be_exec_assembly_date" />
        <input type="hidden" name="event" value="ev_show_assembly_date" />
        <input type="hidden" name="emp_id_comma_list" id="emp_id_comma_list" />
        <input type="hidden" name="contents_id" value="{$contents_id}" />
        <input type="hidden" name="assembly_application_date" id="assembly_application_date" value="" />

        <table cellspacing="0" cellpadding="0" class="inputlist">
            <tr>
                <th width="90"><nobr>受講日</nobr></th>
                <td><input type="text" class="text" size="18" name="assembly_application_day" id="assembly_application_day" readonly value="{$assembly_application_date}" style="width:120px" />
            {php} if (count($this->_tpl_vars["asm_list"])) { {/php}
                <a href="javascript:void(0)" onclick="ee('divAssemblyKouho').style.display=(ee('divAssemblyKouho').style.display=='none'?'':'none');">集合研修予定から日付選択</a>
            {php} } {/php}
            <div style="padding:5px 0 5px 10px; display:none" id="divAssemblyKouho">
            {php} foreach ($this->_tpl_vars["asm_list"] as $row) { {/php}
            <a href="javascript:void(0)" onclick="ee('assembly_application_day').value='{php} echo $row["ymd_jp"]; {/php}'; ee('divAssemblyKouho').style.display='none';">◆{php} echo $row["link_caption"]; {/php}</a><br>
            {php} } {/php}
            </div>
            </td>
            </tr>
        </table>
        <div style="text-align:right; padding:5px 0 16px 0">
            <button type="button" class="btn-main" id="import">受講実績の登録</button>
        </div>
    </form>
</div>
</body>
</html>
