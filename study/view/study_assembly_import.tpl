{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}集合研修実績インポート</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_assembly_import  -->





<script type="text/javascript">
    var result = "{$result}";
    var message = "{$message}";
    {literal}
    function try_import() {
        if ($("[name=csv]").val() == "") { alert("ファイルが選択されていません。"); return; }
        var ymd = ee("assembly_application_day").value;
        if (ymd=="") { alert("日付を指定してください。"); return ""; }
        ymd = getYYYYMMDDFromJpYMD(ymd);
        if (ymd=="") { alert("日付を正しく指定してください。"); return ""; }
        ee("assembly_application_date").value = ymd;
        document.mainform.submit();
    }
    $(document).ready(function() {
        $("#assembly_application_day").datepicker( { dateFormat:"yy年mm月dd日", buttonImageOnly:true }); // 開催開始日にカレンダー適用

        if (result=="error") { alert(message); return; }
        if (result=="success") { window.opener.document.form_sub.submit(); window.close(); }
    });
    {/literal}
</script>





<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>集合研修実績インポート</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>






<div class="usual_box">
<form name="mainform" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="btn_event" value="be_exec_assembly_import" />
    <input type="hidden" name="event" value="ev_show_assembly_import" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />
    <input type="hidden" name="assembly_application_date" id="assembly_application_date" value="" />

    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="90"><nobr>CSVファイル</nobr></th>
            <td><input type="file" name="csv" /></td>
        </tr>
        <tr>
            <th width="90"><nobr>受講日</nobr></th>
            <td><input type="text" class="text" size="18" name="assembly_application_day" id="assembly_application_day" readonly value="{$assembly_application_day}" style="width:120px" />
            {php} if (count($this->_tpl_vars["asm_list"])) { {/php}
                <a href="javascript:void(0)" onclick="ee('divAssemblyKouho').style.display=(ee('divAssemblyKouho').style.display=='none'?'':'none');">集合研修予定から日付選択</a>
            {php} } {/php}
            <div style="padding:5px 0 5px 10px; display:none" id="divAssemblyKouho">
            {php} foreach ($this->_tpl_vars["asm_list"] as $row) { {/php}
            <a href="javascript:void(0)" onclick="ee('assembly_application_day').value='{php} echo $row["ymd_jp"]; {/php}'; ee('divAssemblyKouho').style.display='none';">◆{php} echo $row["link_caption"]; {/php}</a><br>
            {php} } {/php}
            </div>
            </td>
        </tr>
        <tr>
            <th>ID区分</th>
            <td>
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <th><input type="radio" name="id_type" value="personal" id="personal"{if $id_type ne "login"} checked{/if}></th><td><label for="personal">職員ID</label></td>
                <th><input type="radio" name="id_type" value="login" id="login"{if $id_type eq "login"} checked{/if}></th><td><label for="login">ログインID</label></td>
                <th><input type="radio" name="id_type" value="idm" id="login"{if $id_type eq "idm"} checked{/if}></th><td><label for="IDm">ICカード(IDm)</label></td>
                </tr></table>
            </td>
        </tr>
    </table>
    <div style="text-align:right; padding:5px 0 16px 0">
        <button type="button" class="btn-main" onclick="try_import()">登録</button>
    </div>

    <div style="background-color:#f5f5f5; padding:8px">
        <div style="padding-bottom:8px"><b>CSVのフォーマット</b></div>
        <div style="line-height:1.3">
            １行につき１つの職員IDを指定してください。<br/>
            例)<br>
                &nbsp;&nbsp;0000000001<br>
                &nbsp;&nbsp;0000000002<br>
                &nbsp;&nbsp;.......<br>
            <div style="padding-top:6px">
            ※&nbsp;取り込み時は、最初のカンマまでを職員IDと認識し、カンマ以降は無視します。<br>
            以下は正しく取り込むことができます。<br/>
            例)<br>
                &nbsp;&nbsp;0000000001,山田　太郎<br>
                &nbsp;&nbsp;0000000002,鈴木　花子<br>
                &nbsp;&nbsp;.......
            </div>
        </div>
    </div>
</form>
</div>
</body>
</html>
