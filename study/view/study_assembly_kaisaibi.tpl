{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}集合研修 開催情報</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_assembly_date_edit  -->




<script type="text/javascript">
    var result = "{$result}";
    {literal}
    function exe_date_submit(btn){
        if (btn.className.indexOf("btn-off")>=0) return;
        var tmp_date = $.trim($("[name=date]").val());
        if (tmp_date == "") { alert("開催日を入力してください。"); return false; }
        $("[name=event]").val('ev_exec_create_date');
        document.assembly_date.submit();
    }

    $(document).ready(function() {
        if (result == "success") {
            parentRefreshRequest("sub_main");
            grandParentRefreshRequest("mainform");
            window.close();
        }
        $("#date").datepicker( { dateFormat: "yy年mm月dd日", buttonImageOnly:true }); // 開催日編集にカレンダー適用
    });
    {/literal}
</script>


<iframe frameborder="0" name="download_frame" style="position:absolute; left:0; top:0; width:0; height:0"></iframe>



<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>集合研修　開催情報{$mode}</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /><td>
    <a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>






<div class="usual_box">
<form name="assembly_date"  action=""  method="post">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />
    <input type="hidden" name="no" value="{$no}" />
    <input type="hidden" name="event" value="" />

    {if not $edit_auth}
        <div style="padding-bottom:5px"><div class="warningbox">権限がありません。参照のみ可能です。</div></div>
    {/if}

    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th style="width:80px">開催日</th>
            <td><input type="text" class="text" size="20" name="date" id="date" readonly value="{$date|escape}" /></td>
        </tr>
        <tr>
            <th>開催時刻</th>
            <td>
                <select name="st_hour">
                    {section name=st_item start=0 loop=24 step=1}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $start_time[0]}
                            <option value="{$smarty.section.st_item.index}" selected>{$smarty.section.st_item.index}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index}">{$smarty.section.st_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="st_minute">
                    {section name=st_item start=0 loop=60 step=5}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $start_time[1]}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}" selected>{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}">{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分&nbsp;&nbsp;〜&nbsp;</span>
                <select name="ed_hour">
                    {section name=ed_item start=0 loop=24 step=1}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $end_time[0]}
                            <option value="{$smarty.section.ed_item.index}" selected>{$smarty.section.ed_item.index}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index}">{$smarty.section.ed_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="ed_minute">
                    {section name=ed_item start=0 loop=60 step=5}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $end_time[1]}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}" selected>{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}">{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分</span>
            </td>
        </tr>
        <tr>
            <th>開催場所</th>
            <td><input type="text" class="text" size="50" name="place" value="{$place|escape}" /></td>
        </tr>
        <tr>
            <th>講師</th>
            <td><input type="text" class="text" size="20" name="lecturer" value="{$lecturer|escape}" /></td>
        </tr>
        <tr>
            <th>定員</th>
            <td>
                <input type="text" class="text numeric" size="5" name="persons_limit" value="{$persons_limit|escape}" />
                <span class="input_subguide">人を超えたら受付不可とする</span>
            </td>
        </tr>
    </table>

    <div style="text-align:right; padding-top:12px">
        <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" id="update" onclick="exe_date_submit(this)">{$mode}</button>
    </div>

</form>
</div>





</body>
</html>