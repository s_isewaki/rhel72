<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<style type="text/css">
{literal}
.percent {
    mso-style-parent:style0;
    mso-number-format:"0\.0%";
    border:1.0pt solid black;
    white-space:normal;
}
.general {
    border:1px solid #000;
}
.text_center {
    border:1px solid #000;
    mso-number-format:"\@";
    text-align:center;
}
{/literal}
</style>
</head>
    <body>
        <!-- ///study_questionnaire_excel  -->

        <table cellspacing="0" cellpadding="0" style="border:1px solid #000000">
            <tr>
                <th class="general">問題別正答率表</td>
                <td class="general">{$contents_title|escape}</td>
            </tr>
        </table>
        <br><br>

        <table cellspacing="0" cellpadding="0" style="border-collapse:collapse">
            <tr>
                <th class="general" rowspan="2"></th>
                <th class="general" colspan="2">1回目</th>
                <th class="general" colspan="2">2回目</th>
                <th class="general" colspan="2">3回目</th>
                <th class="general" colspan="2">最終回</th>
            </tr>
            <tr>
                <th class="general">合格/実施者数</th>
                <th class="general">正答率</th>
                <th class="general">合格/実施者数</th>
                <th class="general">正答率</th>
                <th class="general">合格/実施者数</th>
                <th class="general">正答率</th>
                <th class="general">合格/実施者数</th>
                <th class="general">正答率</th>
            </tr>

            {php}
            $percents = $this->_tpl_vars["percents"];
            $bunbun = $this->_tpl_vars["bunbun"];
            for ($no=1; $no<=count($percents); $no++) {
                $caption = "問題".$no;
                if ($no==count($percents)) $caption = "平均";
            {/php}

            <tr>
                <td width="20%" class="general"><nobr>{php} echo $caption {/php}</nobr></td>
                <td class="text_center">{php} echo $bunbun[$no][1]{/php}</td><td class="general"><b>{php} echo $percents[$no][1]{/php}</b></td>
                <td class="text_center">{php} echo $bunbun[$no][2]{/php}</td><td class="general"><b>{php} echo $percents[$no][2]{/php}</b></td>
                <td class="text_center">{php} echo $bunbun[$no][3]{/php}</td><td class="general"><b>{php} echo $percents[$no][3]{/php}</b></td>
                <td class="text_center">{php} echo $bunbun[$no][4]{/php}</td><td class="general"><b>{php} echo $percents[$no][4]{/php}</b></td>
            </tr>
            
            {php} } {/php}
        </table>
    </body>
</html>

