<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>テスト編集</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_test_edit  -->




<script type="text/javascript">
    var test_executing_count = <?=(int)@$test_executing_count?>;
    var is_no_auth = <?=($edit_auth ? 0 : 1)?>;
    var test_count  = <?=$test_count?>;
    var result = "<?=$result?>";
    var contents_id = "<?=$contents_id?>";
    var sequence_item_count = <?=$test_count?>;
    var item_id_comma_list = "";
    var import_status = "<?=$import_status?>";
    var seqToNameIndex = {};

    // テスト項目を追加する
    function add_item() {
        // 現在の項目数＋1
        sequence_item_count++;
        $("#test_start_tag").append(
            '<div index="'+sequence_item_count+'" id="div' + sequence_item_count + '">'+
            $("#copy_question_div").html().replace(/__item_no__/g, sequence_item_count)+
            "</div>"
        );
        updatePrepare();
        var scrollHeight = ee("scroll_div").scrollHeight - 400;
        $("#scroll_div").animate({"scrollTop":scrollHeight});
    }

    // テスト項目を削除する
    function delete_item(target_id) {
        if (!confirm("この問題を削除します。よろしいですか？")) return;
        $("#div" + target_id).remove(); // 対象のdivを削除
        updatePrepare();
    }

    // 選択肢プルダウンのイベント
    function selected_choice_count(seq, cnt) {
        for (var idx=1; idx<=10; idx++) ee("choice_tr_"+seq+"_"+idx).style.display = (idx<=cnt ? "" : "none");
    }

    function change_limit_time_btn() {
        if (ee("rlt_y").checked) $("#limit_time").removeClass("text_readonly");
        else $("#limit_time").addClass("text_readonly").val("");
    }

    // body_onload
    $(document).ready(function() {
        set_auto_session_update();  // 20141203TN
        sequence_item_count = test_count;
        if (result=="success") {
            parentRefreshRequest("mainform");
            window.close();
        }

        // 日付にカレンダー適用
        $("#start_day").datepicker( { dateFormat:'yy年mm月dd日' });
        $("#end_day").datepicker( { dateFormat:'yy年mm月dd日' });

        // テスト項目が0件の場合はテンプレートから1件追加
        if (!test_count) add_item();
        updatePrepare();

        if (is_no_auth || test_executing_count || !test_count) $("#btn_delete").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth || test_executing_count) $("#btn_save").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth) $("#btn_export_question").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth) $("#btn_preview").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth || test_executing_count) $("#btn_import").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth || test_executing_count) $("#btn_add_item").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth || test_executing_count || !test_count) $("[name=btn_various]").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth || test_executing_count) $("[name=btn_imageedit]").attr("disabled", "disabled").addClass("btn-off");
        if (is_no_auth || !test_executing_count) $("#btn-endtime-save").attr("disabled", "disabled").addClass("btn-off");
 

        if (import_status) {
            if (import_status=="ok") alert("問題インポートを行いました。\n\n「テスト保存」操作を行うまでは、保存されません。\n");
            if (import_status!="ok") alert("問題インポートが正しく行えませんでした。");
        }
    });
    // テスト項目のチェックを行う
    function test_item_check() {
        // 制限時間チェック
        if ($("[name='limit_time_btn']:checked").val() == "y") {
            var lt = $("#limit_time").val();
            if (lt == "") { alert("制限時間が入力されていません。"); $("#limit_time").focus(); return false; }
            if (!lt.match(/^[1-9][0-9]*$/)) { $("#limit_time_text").focus(); return alert("制限時間には、０より大きい数字を入力して下さい。"); }
        }
        // 点数条件チェック
        var bs = $("#border_score").val();
        if (bs == "") { alert("点数条件が入力されていません。"); $("#border_score").focus(); return false; }
        if (!bs.match(/^[0-9][0-9]*$/)) { $("#border_score").focus(); return alert("点数条件に数字を入力して下さい。"); }
        if (parseInt(bs,10)>100) { $("#border_score").focus(); return alert("点数条件には0から100までの整数を指定してください。"); }

        if (!convertAndCheckDateTime()) return;

        for (var idx in seqToNameIndex) {
            var caption = "問題"+seqToNameIndex[idx];

            // 問題文
            var elem = ee("question_"+idx);
            if (elem.value=="") { $(elem).focus(); return alert(caption+"の問題文が入力されていません。"); }

            // 選択肢、選択肢入力の必須チェック
            var cnt = int(ee("choice_cnt_"+idx).value);
            for (var i = 1; i <= cnt; i++) {
                elem = ee("choice_item_"+idx+"_"+i);
                if (elem.value=="") { $(elem).focus(); return alert(caption+"の選択項目が入力されていません。"); }
            }

            var check_cnt = 0;
            for (var cidx=1; cidx<=cnt; cidx++) {
                if (ee("choice_answer_"+idx+"_"+cidx).checked) check_cnt++;
            }
            if (check_cnt == 0) { alert(caption+"の正答が選択されていません。"); return false; }

            // 複数選択不可で2以上ある場合はエラー
            if (!ee("imc_"+idx).checked && check_cnt > 1) { return alert(caption+"の正答は複数選択不可です。（正答数:" + check_cnt+"）"); }
        }
        return "ok";
    }
    function save_test() {
        // 共通項目のチェック
        if (test_item_check()!="ok") return;
        if (!confirm("保存します。よろしいですか？")) return;
        if (test_count) $("#btn_event").val("be_update");
        else $("#btn_event").val("be_create");
        document.mainform.submit();
    }

    function delete_test() {
        if (!confirm("テストを削除し、未作成の状態にします。よろしいですか？")) return;
        $("#btn_event").val("be_delete");
        document.mainform.submit();
    }

    function preview_test() {
        var url =
        "study_test_edit.php?session=" + session + "&btn_event=be_show_test_preview&contents_id="+contents_id+
        "&item_count="+ee("test_count").value+"&item_id_comma_list="+encodeURIComponent(item_id_comma_list);
        window_open(url, "test_preview", 800, 600);
    }

    function export_question() {
        $("#btn_event").val("be_export_question");
        document.mainform.submit();
    }

    function import_question() {
        if ($("#import_xml_file").val() == "") { alert("ファイルが選択されていません。\n本画面の「問題エクスポート」より取得したXMLファイルを指定してください。"); return; }
        if (!confirm("問題のインポートを行います。よろしいですか？")) return;
        convertAndCheckDateTime(1);
        // IE11のみ裏でsubmit実行
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.indexOf("msie")== -1 && ua.indexOf('trident/7') != -1){
            setInterval(function(){ document.dummyform.submit(); }, 800);
        }
        $("#btn_event").val("be_import_question");
        document.mainform.submit();
    }
    function show_test_edit_attach(num) {
        var url = "study_test_edit.php?session=" + session + "&btn_event=be_show_test_edit_attach&contents_id="+contents_id+"&item_num="+num;
        window_open(url, "test_edit_attach", 700, 300);
    }
    function cancel_attach(btn, num) { // 取消ボタン押下
        if (btn.className.indexOf("btn-off")>=0) return;
        ee("new_image_path_"+num).value = "";
        ee("new_image_name_"+num).value = "";
        ee("image_container_"+num).style.display = "none";
        ee("image_container_org_"+num).style.display = "";
        ee("attach_delete_"+num).value = "";
        ee("image_name_disp_"+num).innerHTML = ee("image_name_"+num).value;
        btn.className = "btn-main btn-off";
        if (ee("image_path_"+num).value) ee("btn_hide_attach_"+num).className = "btn-main";
    }
    function hide_attach(btn, num) { // 削除ボタン押下
        if (btn.className.indexOf("btn-off")>=0) return;
        ee("new_image_path_"+num).value = "";
        ee("new_image_name_"+num).value = "";
        ee("image_container_"+num).style.display = "none";
        ee("image_container_org_"+num).style.display = "none";
        ee("btn_cancel_attach_"+num).className = "btn-main";
        ee("attach_delete_"+num).value = "1";
        ee("image_name_disp_"+num).innerHTML = "";
        btn.className = "btn-main btn-off";
    }
    function move_item(num, dest) {
        var div = ee("div"+num);
        var ids = item_id_comma_list.split(",");
        for (var nidx=0; nidx<ids.length; nidx++) {
            if (ids[nidx]==num) {
                if (dest=="up" && nidx>0) { div.parentNode.insertBefore(div, ee("div"+(ids[nidx-1]))); break; }
                if (dest=="down" && nidx<ids.length-1) { div.parentNode.insertBefore(ee("div"+(ids[nidx+1])), div); break; }
            }
        }
        updatePrepare();
    }

    function updatePrepare() {
        seqToNameIndex = {};
        item_ids = [];
        var name_seq = 0;
        var nodes = ee("test_start_tag").childNodes;
        for (var nidx=0; nidx<nodes.length; nidx++) {
            var node = ee("test_start_tag").childNodes[nidx];
            if (node.tagName!="DIV") continue;
            var idx = int(node.getAttribute("index"));
            sequence_item_count = Math.max(sequence_item_count, idx);
            item_ids.push(idx);
            name_seq++;
            seqToNameIndex[idx] = name_seq;
            ee("question_"+idx).name = "question_"+name_seq;
            ee("summary_"+idx).name = "summary_"+name_seq;
            ee("seq_"+idx).innerHTML = "問題"+name_seq;
            ee("attach_delete_"+idx).name = "attach_delete_"+name_seq;
            ee("new_image_path_"+idx).name = "new_image_path_"+name_seq;
            ee("new_image_name_"+idx).name = "new_image_name_"+name_seq;
            ee("image_path_"+idx).name = "image_path_"+name_seq;
            ee("image_name_"+idx).name = "image_name_"+name_seq;
            ee("imc_"+idx).name = ""; // いったんカラに。途中でnameを変更すると、他のチェックボックスと干渉してしまう。
            ee("imc_"+idx+"_off").name = "";
            ee("choice_cnt_"+idx).name = "choice_cnt_"+name_seq;
            for (var cidx=1; cidx<=10; cidx++) {
                ee("choice_answer_"+idx+"_"+cidx).name = "choice_answer_"+name_seq+"_"+cidx;
                ee("choice_item_"+idx+"_"+cidx).name = "choice_item_"+name_seq+"_"+cidx;
            }
        }
        for (var nidx=0; nidx<nodes.length; nidx++) {
            var node = ee("test_start_tag").childNodes[nidx];
            if (node.tagName!="DIV") continue;
            var idx = int(node.getAttribute("index"));
            ee("imc_"+idx).name = "is_multiple_choice_"+seqToNameIndex[idx];
            ee("imc_"+idx+"_off").name = "is_multiple_choice_"+seqToNameIndex[idx];
        }
        ee("question_count").innerHTML = name_seq;
        ee("test_count").value = name_seq;
        item_id_comma_list = item_ids.join(",");
        if (name_seq>1) {
            $("[name=btn_various]").each(function() { $(this).removeAttr("disabled").removeClass("btn-off"); });
        } else {
            $("[name=btn_various]").each(function() { $(this).attr("disabled", "disabled").addClass("btn-off"); });
        }
        change_limit_time_btn();
    }
    function update_endtime() {
        $("#btn_event").val("be_update_endtime");
        document.mainform.submit();
    }
</script>

<!-- セッションタイムアウト防止 START 20141203TN -->
<script type="text/javascript">
    //{literal}
    function set_auto_session_update(){
        setTimeout(auto_session_update,60000);
    }
    
    function auto_session_update(){
        document.session_update_form.submit();
        setTimeout(auto_session_update,60000);
    }  
    //{/literal}
</script>
<form name="session_update_form" action="study_session_update.php" method="post" target="session_update_frame">
    <input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->





<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>テスト編集</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="<?=h($contents["title"])?>" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>








<div class="usual_box" style="width:973px">
<form name="mainform" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
    <input type="hidden" name="btn_event" id="btn_event" value="" />
    <input type="hidden" name="contents_id" value="<?=$contents_id?>" />
    <input type="hidden" name="update_date" value="<?=@$test_info["update_date"]?>" /><!-- ロック用timestamp -->
    <input type="hidden" name="emp_personal" value="" />
    <input type="hidden" name="start_date" value="" />
    <input type="hidden" name="end_date" value="" />
    <input type="hidden" name="test_count" id="test_count" value="<?=$test_count?>" />




    <? if (!$edit_auth) { ?>
    <div style="padding-bottom:5px"><div class="warningbox">
        権限がありません。閲覧のみ可能です。
    </div></div>
    <? } else if ($test_executing_count) { ?>
    <div style="padding-bottom:5px"><div class="warningbox">
        テストが実施されました。編集はできません。
    </div></div>
    <? } ?>



    <!-- テストメイン開始 -->
    <table cellspacing="0" cellpadding="0" class="inputlist">
        <!-- 公開フラグ -->
        <tr>
            <th style="width:90px">テスト期間</th>
            <td>
                <div style="padding:3px">
                <span class="brown">開始</span>
                <input name="start_day" id="start_day" type="text" class="text" size="18" readonly value="<?=@$array_start[0]?>" style="width:120px" />
                <select name="st_hour" id="st_hour">
                <? for ($hh=0; $hh<=23; $hh++) { ?>
                    <option value="<?=$hh?>"<?=($hh-$array_start[1]?"":" selected")?>><?=$hh?></option>
                <? } ?>
                </select>
                <span class="input_subguide">時</span>

                <select name="st_minute" id="st_minute">
                <? for ($mm=0; $mm<=59; $mm+=5) { ?>
                    <?$mm00=sprintf("%02d", $mm); ?>
                    <option value="<?=$mm00?>"<?=($mm-$array_start[2]?"":" selected")?>><?=$mm00?></option>
                <? } ?>
                </select>
                <span class="input_subguide">分</span>
                &nbsp;&nbsp;&nbsp;
                <span class="brown">終了</span>
                <input name="end_day" id="end_day" type="text" class="text" size="18" readonly value="<?=@$array_end[0]?>" style="width:120px" />
                <select name="ed_hour" id="ed_hour">
                <? for ($hh=0; $hh<=23; $hh++) { ?>
                    <option value="<?=$hh?>"<?=($hh-$array_end[1]?"":" selected")?>><?=$hh?></option>
                <? } ?>
                </select>
                <span class="input_subguide">時</span>

                <select name="ed_minute" id="ed_minute">
                <? for ($mm=0; $mm<=59; $mm+=5) { ?>
                    <?$mm00=sprintf("%02d", $mm); ?>
                    <option value="<?=$mm00?>"<?=($mm-$array_end[2]?"":" selected")?>><?=$mm00?></option>
                <? } ?>
                </select>
                <span class="input_subguide">分</span>
                
                    <button type="button" class="btn-main" id="btn-endtime-save" onclick="update_endtime()"　navititle="終了日時保存" navi="終了日時のみを保存します。">保存</button>
                
                </div>
                
               
            </td>            
            
        </tr>




        <tr>
            <th>条件</th>
            <td>
                <div style="padding:3px">
                    <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                    <th><span class="brown"><nobr>点数条件</nobr></span></th>
                    <td><nobr>
                        <input name="border_score" id="border_score" type="text" class="text" maxlength="3" size="3"
                            value="<?=(int)@$test_info["border_score"]?>" style="text-align: right; ime-mode: disabled;" />&nbsp;％以上正答で受講済</nobr>
                    </td>

                    <th style="padding-left:20px"><span class="brown"><nobr>制限時間</nobr></span></th>

                    <th><input name="limit_time_btn" type="radio" value="y" id="rlt_y"<?=(@$test_info["limit_time"]?" checked":"")?>
                        onclick="change_limit_time_btn()" /></th>
                    <td><label for="rlt_y"><nobr>あり</nobr></label></td>
                    <th><input name="limit_time" id="limit_time" type="text" maxlength="3" size="3"
                        value="<?=@$test_info["limit_time"]?>" style="text-align: right; ime-mode: disabled;"
                         class="text<?=(@$test_info["limit_time"]?"":" text_readonly")?>"
                         onkeydown="return this.className.indexOf('text_readonly')<0;"
                         /></th><td>分</td>
                    <th><input name="limit_time_btn" type="radio" value="n" id="rlt_n"<?=(@$test_info["limit_time"]?"":" checked")?>
                        onclick="change_limit_time_btn()" /></th>
                    <td><label for="rlt_n"><nobr>なし</nobr></label></td>

                    <th style="padding-left:20px"><span class="brown"><nobr>問題表示順</nobr></span></th>

                    <th><input name="test_item_order" type="radio" value="0" id="rtio_0"<?=(@$test_info["test_item_order"]?"":" checked")?> /></th>
                    <td><label for="rtio_0"><nobr>番号順</nobr></label></td>
                    <th><input name="test_item_order" type="radio" value="1" id="rtio_1"<?=(@$test_info["test_item_order"]?" checked":"")?> /></th>
                    <td><label for="rtio_1"><nobr>シャッフル</nobr></label></td>

                    </tr></table>
                </div>
            </td>
        </tr>
    </table>

    <div style="padding:3px 5px" navititle="問題インポート" navi="他で作成したテスト内容を添付画像データごと取り込みます。<br>本画面の「問題エクスポート」より取得したXMLファイルのみ指定できます。">
        <nobr><span style="color:#de5836">問題インポート</span>
                <input type="file" name="import_xml_file" id="import_xml_file" value="">
                <button type="button" class="btn-main" id="btn_import" onclick="import_question()">問題インポート</button>
    </nobr></div>


    <div style="padding-top:5px">
    <div class="section_title">
        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
        <td>■ 問題</td>
        <td class="right">
            <span style="padding-right:20px">計<b><span style="color:#ff0000; font-size:16px; padding:0 3px"
                id="question_count" ><?=$test_count?></span></b>問</span>
            <button type="button" class="btn-main" id="btn_add_item" onclick="add_item()" navititle="問題追加" navi="このテストの問題をひとつ増やします。">問題追加</button>
            <button type="button" class="btn-main" id="btn_export_question" onclick="export_question()" navititle="問題エクスポート" navi="ボタンを押下すると、テスト内容XMLデータを取得できます。<br>これには添付画像情報も含まれます。<br>取得したXMLデータファイルは、他の研修・講義テストへ複製できます。<br>エクスポートファイルは、画像も含め、現在編集中の状態が出力されます。">問題エクスポート</button>
            <button type="button" class="btn-main" id="btn_preview" onclick="preview_test()" navititle="プレビュー" navi="実際にテストを実施する際のフォーマットで開きます。<br>画像は原寸サイズになります。">プレビュー</button>
        </td>
        </tr></table>
    </div>
    </div>



    <div class="scroll_div" style="overflow-y:scroll; overflow-x:hidden; border:1px solid #dddddd; width:972px" heightMargin="30" id="scroll_div">
    <div id="test_start_tag" style="width:946px; padding:5px 0 5px 5px">

        <? for ($num=1; $num<=count($test_items); $num++) { ?>
        <?     $item = $test_items[$num-1]; ?>
        <div index="<?=$num?>" id="div<?=$num?>" style="padding:5px 0 30px 0">

            <input type="hidden" id="new_image_path_<?=$num?>" value="<?=h(@$item["new_image_path"])?>" />
            <input type="hidden" id="new_image_name_<?=$num?>" value="<?=h(@$item["new_image_name"])?>" />
            <input type="hidden" id="image_path_<?=$num?>" value="<?=h($item["image_path"])?>" />
            <input type="hidden" id="image_name_<?=$num?>" value="<?=h($item["image_name"])?>" />
            <input type="hidden" id="attach_delete_<?=$num?>" value="" />

            <div class="beige">
                <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
                    <td><b><span id="seq_<?=$num?>">問題<?=$num?></span></b></td>
                    <td class="right">
                        <button type="button" onclick="move_item(<?=$num?>,'up')" class="btn-main<?=($test_count==1?" btn-off":"")?>"
                            name="btn_various">上へ</button>
                        <button type="button" onclick="move_item(<?=$num?>,'down')" class="btn-main<?=($test_count==1?" btn-off":"")?>"
                            name="btn_various">下へ</button>
                        <button type="button" onclick="delete_item(<?=$num?>)" class="btn-main<?=($test_count==1?" btn-off":"")?>"
                            name="btn_various">削除</button>
                    </td>
                </tr></table>
            </div>

            <div style="padding-bottom:5px">
                <div style="background-color:#ffffe5; padding:10px; border:1px solid #eeeeee; border-bottom:1px solid #dddddd">
                    <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
                        <td class="vtop" style="width:550px">
                            <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="vtop"><nobr>問題文</nobr></td>
                                <td style="padding:0 5px"><textarea id="question_<?=$num?>" rows="4" cols="60"><?=h($item["question"])?></textarea></td>
                            </tr>
                            <tr>
                                <td class="vtop"><nobr>解説文</nobr></td>
                                <td style="padding:0 5px"><textarea id="summary_<?=$num?>" rows="4" cols="60"><?=h($item["comment"])?></textarea></td>
                            </tr>
                            </table>
                        </td>
                        <td class="vtop">
                            <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%">
                                <tr><td><nobr>添付画像ファイル</nobr></td>
                                    <td style="text-align:right">
                                        <button type="button" name="btn_imageedit" class="btn-main" onclick="show_test_edit_attach(<?=$num?>)">参照</button>
                                        <button type="button" name="btn_imageedit" class="btn-main<?=($item["image_path"]?"":" btn-off")?>"
                                            id="btn_hide_attach_<?=$num?>" onclick="hide_attach(this, <?=$num?>)">削除</button>
                                        <button type="button"  name="btn_imageedit" class="btn-main<?=($item["new_image_path"]?"":" btn-off")?>" id="btn_cancel_attach_<?=$num?>"
                                            onclick="cancel_attach(this, <?=$num?>)">取り消し</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="image_name_disp_<?=$num?>" colspan="2"><?=h($item["image_name"])?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="padding:5px 0" colspan="2">
                                        <span id="image_container_org_<?=$num?>">
                                        <? if ($item["image_path"] && $item["image_path_url"]) {?>
                                                <img src="<?=$item["image_path_url"]?>" alt=""<?=@$item["image_size_style"]?> style="border:2px" />
                                        <? } ?>
                                        </span>
                                        <span id="image_container_<?=$num?>">
                                        <? if ($item["new_image_path"] && $item["image_path_url"]) {?>
                                                <img src="<?=$item["image_path_url"]?>" alt=""<?=@$item["image_size_style"]?> style="border:2px" />
                                        <? } ?>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr></table>
                </div>
            </div>

            <div style="padding-bottom:5px">
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                    <td class="brown2">選択肢数</td>
                    <td><select id="choice_cnt_<?=$num?>" onchange="selected_choice_count(<?=$num?>, this.value)">
                    <? for ($cnum=2; $cnum<=10; $cnum++) { ?>
                        <option value="<?=$cnum?>"<?=($cnum==$item["choice_cnt"]?" selected":"")?>><?=$cnum?></option>
                    <? } ?>
                    </select>
                    </td>
                    <td style="padding-left:20px" class="brown">複数選択</td>
                    <td><input type="radio" value="t" id="imc_<?=$num?>"<?=($item["multiple"]=="t"?" checked":"")?> /></td>
                    <td><label for="imc_<?=$num?>">可</label></td>
                    <td><input type="radio" value="f" id="imc_<?=$num?>_off"<?=($item["multiple"]!="t"?" checked":"")?>/></td>
                    <td><label for="imc_<?=$num?>_off">不可</label></td>
                </tr></table>
            </div>

            <!-- 選択肢 -->
            <table width="585" cellspacing="0" cellpadding="0" class="inputlist2">
                <tr>
                    <th width="40"><nobr>項番&nbsp;</nobr></th>
                    <th width="40"><nobr>正答</th>
                    <th style="text-align:left; padding-left:6px">内容</th>
                </tr>
                <? for ($ccnt=1; $ccnt<=10; $ccnt++) { ?>
                <?    $id = $num."_".$ccnt; ?>
                <?    $c_answer = $item["answers"][$ccnt-1]; ?>
                <?    $c_choices = $item["choices"][$ccnt-1]; ?>
                <?    $display_none = ($ccnt>$item["choice_cnt"]? ' style="display:none"':""); ?>
                <tr id="choice_tr_<?=$id?>" <?=$display_none?>>
                    <td class="center"><?=$ccnt?></td>
                    <td class="center"><input type="checkbox" id="choice_answer_<?=$id?>" value="t"<?=($c_answer=="t"?" checked":"")?>></td>
                    <td><input type="text" class="text" id="choice_item_<?=$id?>" value="<?=h($c_choices)?>" size="100" style="ime-mode:active;"></td>
                </tr>
                <? } ?>
            </table>
        </div>
        <? } ?>
    </div>
    </div>




    <!-- ボタン部 -->
    <div style="text-align:right; padding:5px">
        <span class="gray">※すべての操作は、「テスト保存」あるいは「テスト削除」ボタンを押すまで保存されません。</span>
        <button type="button" class="btn-main" id="btn_save" onclick="save_test()">テスト保存</button>
        <button type="button" class="btn-main" id="btn_delete" onclick="delete_test()">テスト削除</button>
    </div>


</form>
</div>









<!-- 問題ブロックのコピー元 -->
<div type="item" id="copy_question_div" style="display:none">

    <input type="hidden" id="new_image_path___item_no__" />
    <input type="hidden" id="new_image_name___item_no__" />
    <input type="hidden" id="image_path___item_no__" />
    <input type="hidden" id="image_name___item_no__" />
    <input type="hidden" id="attach_delete___item_no__" />

    <div style="padding:5px 0 30px 0">
        <div style="padding-bottom:5px">
            <div class="beige">
                <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
                    <td><b><span id="seq___item_no__">問題</span></b></td>
                    <td class="right">
                        <button type="button" onclick="move_item(__item_no__,'up')" class="btn-main btn-off"
                            name="btn_various">上へ</button>
                        <button type="button" onclick="move_item(__item_no__,'down')" class="btn-main btn-off"
                            name="btn_various">下へ</button>
                        <button type="button" class="btn-main btn-off" id="delete_item_btn___item_no__"
                            onclick="delete_item(__item_no__)" name="btn_various">削除</button>
                    </td>
                </tr></table>
            </div>

            <div style="background-color:#ffffe5; padding:10px; border:1px solid #eeeeee; border-bottom:1px solid #dddddd;">
                <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
                    <td class="vtop" style="width:550px">
                        <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="vtop"><nobr>問題文</nobr></td>
                            <td style="padding:0 5px"><textarea id="question___item_no__" rows="4" cols="60"></textarea></td>
                        </tr>
                        <tr>
                            <td class="vtop"><nobr>解説文</nobr></td>
                            <td style="padding:0 5px"><textarea id="summary___item_no__" rows="4" cols="60"></textarea></td>
                        </tr>
                        </table>
                    </td>

                    <td class="vtop">
                        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%">
                            <tr><td><nobr>添付画像ファイル</nobr></td>
                                <td style="text-align:right">
                                    <button type="button" name="btn_imageedit" class="btn-main" onclick="show_test_edit_attach(__item_no__)">参照</button>
                                    <button type="button" name="btn_imageedit" class="btn-main btn-off"
                                        id="btn_hide_attach___item_no__" onclick="hide_attach(this, __item_no__)">削除</button>
                                    <button type="button"  name="btn_imageedit" class="btn-main btn-off" id="btn_cancel_attach___item_no__"
                                        onclick="cancel_attach(this, __item_no__)">取り消し</button>
                                </td>
                            </tr>
                            <tr>
                                <td id="image_name_disp___item_no__" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding:5px 0" colspan="2">
                                    <span id="image_container_org___item_no__"></span>
                                    <span id="image_container___item_no__" style="display:none"></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr></table>
            </div>
        </div>

        <div style="padding-bottom:5px">
            <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <td class="brown2">選択肢数</td>
                <td><select id="choice_cnt___item_no__" onchange="selected_choice_count(__item_no__, this.value)">
                <? for ($cnum=2; $cnum<=10; $cnum++) { ?>
                    <option value="<?=$cnum?>"<?=($cnum==2?" selected":"")?>><?=$cnum?></option>
                <? } ?>
                </select>
                </td>
                <td style="padding-left:20px" class="brown">複数選択</td>
                <td><input type="radio" id="imc___item_no__" value="t" /></td>
                <td><label for="imc___item_no__">可</label></td>
                <td><input type="radio" id="imc___item_no___off" value="f" checked/></td>
                <td><label for="imc___item_no___off">不可</label></td>
            </tr></table>
        </div>

        <!-- 選択肢 -->
        <table width="585" cellspacing="0" cellpadding="0" class="inputlist2">
            <tr>
                <th width="40"><nobr>項番</nobr></th>
                <th width="40"><nobr>正答</nobr></th>

                <th style="text-align:left; padding-left:6px">内容</th>
            </tr>
            <? for ($ccnt=1; $ccnt<=10; $ccnt++) { ?>
            <tr id="choice_tr___item_no___<?=$ccnt?>" <?=($ccnt>2 ? ' style="display:none"':"")?>>
                <td class="center"><?=$ccnt?></td>
                <td class="center"><input type="checkbox" id="choice_answer___item_no___<?=$ccnt?>" value="t"<?=($c_answer=="t"?" checked":"")?>></td>
                <td><input type="text" class="text" id="choice_item___item_no___<?=$ccnt?>" value="" size="100" style="ime-mode:active;"></td>
            </tr>
            <? } ?>
        </table>
    </div>
</div>





<form name="dummyform" id="dummyform" method="POST" target="dummy_iframe" action="study_contents.php?session=<?=GG_SESSION_ID?>">
    <input type="hidden" name="session2" value="<?=GG_SESSION_ID?>" />
</form>
<iframe frameborder="0" name="dummy_iframe" id="dummy_iframe" style="position:absolute; left:0; top:0 width:0; height:0"></iframe>




</body>
</html>
