<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<style type="text/css">
{literal}
.percent {
    mso-style-parent:style0;
    mso-number-format:"0\.0%";
    border:1.0pt solid black;
    white-space:normal;
}
{/literal}
</style>
</head>
    <body>
        <!-- ///study_questionnaire_excel  -->


        <table cellspacing="0" cellpadding="0" style="border:1px solid #000000">
            <tr>
                <th style="border:1px solid #000">研修・講義タイトル</td>
                <td style="border:1px solid #000">{$contents.title|escape}</td>
            </tr>
            <tr>
                <th style="border:1px solid #000">アンケート期間</td>
                <td style="border:1px solid #000">{$start_date}<br>〜<br>{$end_date}</td>
            </tr>
        </table>
        <br><br>


        <table cellspacing="0" cellpadding="0" style="border-collapse:collapse">
            <tr>
                <th style="border:1px solid #000">設問</th>
                <th style="border:1px solid #000">回答結果</th>
                <th style="border:1px solid #000">回答数</th>
                <th style="border:1px solid #000">割合</th>
            </tr>
            {foreach from=$total key=key item=item}
                {if $item.type == "3"}
                    <tr>
                        <td style="border:1px solid #000">
                               {php}
                               $question_br = str_replace("\n", "<br>",h($this->_tpl_vars['item']["question"]));
                               $question_convert = str_replace(" ", "&nbsp;",$question_br);
                               echo $question_convert;
                                 {/php}
                            </td>
                        </td>
                        <td style="border:1px solid #000">{if $item.text_answers|@count > 0}（下部に記載）{/if}</td>
                        <td style="border:1px solid #000">{$item.text_answers|@count}</td>
                        <td style="border:1px solid #000" class="percent">{$item.text_answers_ratio}%</td>
                    </tr>
                {else}
                {php} $cnt = 0; {/php}
                    {foreach from=$item.choices key=id item=item_sub}
                        {php} $cnt++; {/php}
                        <tr>
                            {php} if ($cnt==1) { {/php}
                            <td style="border:1px solid #000" rowspan="{$item.choices|@count}">
                                 {php}
                               $question_br = str_replace("\n", "<br>",h($this->_tpl_vars['item']["question"]));
                               $question_convert = str_replace(" ", "&nbsp;",$question_br);
                               echo $question_convert;
                                 {/php}
                            </td>
                            {php} } {/php}
                            <td style="border:1px solid #000">{$item_sub.choice_string|escape}</td>
                            <td style="border:1px solid #000">{$item_sub.choice_count}</td>
                            <td style="border:1px solid #000" class="percent">{$item_sub.choice_ratio}%</td>
                        </tr>
                    {/foreach}
                {/if}
            {/foreach}
        </table>
        <br><br>

        <table cellspacing="0" cellpadding="0" style="border:1px solid #000000">
            <tr>
                <th style="border:1px solid #000">設問</td>
                <th style="border:1px solid #000">感想</td>
                <th style="border:1px solid #000">職員ID</td>
                <th style="border:1px solid #000">職員氏名</td>
            </tr>
        </table>

        {foreach from=$total key=key item=item}
            {if $item.type == "3" && $item.text_answers|@count > 0}
                <table cellspacing="0" cellpadding="0" class="list">
                    {php} $cnt = 0; {/php}
                    {foreach from=$item.text_answers key=item_id item=item_sub}
                        {php} $cnt++; {/php}
                        <tr>
                            {php} if ($cnt==1) { {/php}
                                <td style="border:1px solid #000" rowspan="{$item.text_answers|@count}">{$item.question|escape}</td>
                            {php} } {/php}
                            <td style="border:1px solid #000">{$item_sub.impression|escape|nl2br}</td>
                            <td style="border:1px solid #000">{$item_sub.emp_id|escape}</td>
                            <td style="border:1px solid #000">{$item_sub.emp_name|escape}</td>
                        </tr>
                    {/foreach}
                </table>
            {/if}
        {/foreach}
    </body>
</html>

