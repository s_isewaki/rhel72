<? require_once("view/header.tpl"); ?>
    <title>バリテス｜集合研修受付</title>
    <style type="text/css">
    * { font-family:verdana,"メイリオ"; }
    div#header {  font-size:24px; line-height:40px }
    div#header table td { background-color:#f19833; color:#ffffff; font-size:28px; padding-left:50px; line-height:40px; padding-bottom:30px }
    </style>

    <script type="text/javascript">
        var contents_id = "<?=$contents_id?>";
        $(document).ready(function() {
            ee("emp_personal_id").value = "";
            if (!contents_id) {
                if (opener && opener.get_actual_entry_contents_info) {
                    var info = opener.get_actual_entry_contents_info();
                    if (info) {
                        contents_id = info.contents_id;
                        ee("contents_title").innerHTML = info.contents_title;
                    }
                }
            }
            if (!contents_id) { alert("不正な画面呼び出しです。"); return; }
            tryFocus("emp_personal_id");
        });

        function isKeyDown(tbox, event) {
            ee("msg_ok1").style.display = "none";
            ee("msg_ok2").style.display = "none";
            ee("msg_error").style.display = "none";
            if (!isEnterKey(tbox, event)) return true;
            var id = tbox.value;
            if (id.length==0) return false;
            var radioVal = $("input[name='id_type']:checked").val();
            var data = "event=ev_exec_assembly_actual_entry&contents_id="+contents_id+"&emp_personal_id="+id+"&login_user_id=<?=GG_LOGIN_USER_ID?>"+"&id_flg="+radioVal;
            $.ajax({ url:"study_assembly.php", method:"POST", data:data, success:function(msg) {
                var name = msg.split(":");  // 職員IDマッチングと同時に職員名も表示 20141203TN
                if (name[0]=="ok") {
                    tbox.value = "";
                    ee("msg_name").innerHTML = name[1];
                    ee("msg_ok1").style.display = "";
                    ee("msg_ok2").style.display = "";
                } else {
                    tbox.value = "";
                    ee("msg_error_inner").innerHTML = msg;
                    ee("msg_error").style.display = "";
                }
            }});
        }
    </script>

</head>
<body>
<!-- ///study_assembly_user  -->



<script type="text/javascript">
    function exe_apply_submit(obj, btn_event){
        if (obj.className.indexOf("btn-off")>-1) return;
        if (btn_event == "be_register"){
            if ($("input:radio[name^='apply_no']:checked").size() == 0) { alert("出席申込を選択してください。"); return false; }
        }
        $("[name=btn_event]").val(btn_event);
        document.apply_main.submit();
    }
</script>





<div id="header"><table cellspacing="0" cellpadding="0" style="width:100%"><tr><td style="height:200px; vertical-align:bottom; text-align:center" id="contents_title"></td></tr></table></div>

       <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th>区分</th>
            <td>
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <th><input type="radio" name="id_type" id="emp1" value="0" checked></th><td><label for="emp1">職員ID</label></td>
                <th><input type="radio" name="id_type" id="emp2"  value="1"></th><td><label for="emp2">ログインID</label></td>
                </tr></table>
            </td>
        </table>
<div style="text-align:center; font-size:24px; padding:30px 0 10px 0;">
    <div style="margin:auto; text-align:center; width:300px; color:#f19833; padding:10px; letter-spacing:5px">◆&nbsp;研修受付&nbsp;◆</div>
</div>


 




<div style="text-align:center; font-size:16px; padding:30px 0 20px 0; width:100%; color:#999999; display:none1">
    <div style="margin:auto; text-align:center; width:800px; padding:2px">IDを入力後、エンターキーを押してください。</div>
</div>

<div style="text-align:center; font-size:20px; padding:0px 0 10px 0">
<form name="mainform">
    ID：
    <input type="text" class="text" style="font-size:20px; ime-mode:disabled" maxlength="20" id="emp_personal_id"
    onkeydown="return isKeyDown(this,event)"
     />
</form>
</div>

<!--20141203TN-->
<div style="text-align:center; font-size:20px; padding:10px 0 10px 0; width:100%; display:none" id="msg_ok1">
    <div style="margin:auto; text-align:center; width:400px; color:#ff6666"><span id="msg_name"></span></div>
</div>

<div style="text-align:center; font-size:20px; padding:10px 0 10px 0; width:100%; display:none" id="msg_ok2">
    <div style="margin:auto; text-align:center; width:400px; color:#ff6666">受付が完了しました。</div>
</div>

<div style="text-align:center; font-size:20px; padding:20px 0 10px 0; width:100%; display:none" id="msg_error">
    <div style="margin:auto; text-align:center; color:#ff6666" id="msg_error_inner"></div>
</div>

</body>
</html>
