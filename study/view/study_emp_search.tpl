{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}職員検索</title>
</head>
<body onresize="resized()">
<!-- ///study_emp_search  -->




<script type="text/javascript">
  


    var notify_list_count = {$notif_list_count};
    var cur_page_num = {$page_num};
    var cur_per_page_size = {$per_page_size};
    var total_count = {$total_count};
    var last_page = {$last_page};
    {literal}
    function paging(mode) {
        var page_num = parseInt($("#page_num").val());
        var new_page_num = parseInt($("#new_page_num").val());
        if (last_page < 1) last_page = 1;
        if (!page_num || isNaN(page_num)) page_num = 1;
        if (mode=="next") $("#page_num").val(++cur_page_num);
        if (mode=="prev") $("#page_num").val(--cur_page_num);
        if (mode=="top") $("#page_num").val(1);
        if (mode=="last") $("#page_num").val(last_page);
        if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
        if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
        if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
        if (mode) document.mainform.submit();
    }
    function showHistory(emp_id) {
        var url  = "study_menu_user.php?session="+session+"&is_popup=1&target_emp_id=" + emp_id;
        window_open(url, "emp_history_view", 1000, 600);
    }
    $(document).ready(function() {
        if (notify_list_count && ee("notify_new_icon")) {
            ee("notify_new_icon").style.display = "";
        }
    });
    {/literal}
</script>




{* コンテンツヘッダー *}
{php} $current_screen = "user@study_emp_search"; require_once("view/contents_header.tpl"); {/php}



    {if !$disp_auth}
    <div style="padding-top:10px">
    <div class="warningbox">所属への教育担当者権限、または教育責任者権限がありません。</div>
    </div>
    {/if}
    
{if $disp_auth}
<form name="mainform" method="POST">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <div style="padding:5px" class="findbox">
        <table cellspacing="0" cellpadding="0" class="rgroup">
            <tr>
                <th>{$smarty.const.GG_CLASS_NM|escape}</th>
                <td colspan="3">
                    <select name="class" id="emp_class" onchange="ddlClassChanged(this)">
                        <option value="">全て</option>
                        {foreach from=$class_list item=class_item }
                            <option value="{$class_item.class_id}"{if $class eq $class_item.class_id} selected{/if}>{$class_item.class_nm|escape}</option>
                        {/foreach}
                    </select>
                    <span style="padding-left:20px">{$smarty.const.GG_ATRB_NM|escape}</span>
                    <select name="attr" id="emp_attr" onchange="ddlAttrChanged(this)">
                        <option value="">全て</option>
                        {foreach from=$attr_list item=attr_item }
                            <option value="{$attr_item.atrb_id}"{if $attr eq $attr_item.atrb_id} selected{/if}>{$attr_item.atrb_nm|escape}</option>
                        {/foreach}
                    </select>
                    <span style="padding-left:20px">{$smarty.const.GG_DEPT_NM|escape}</span>
                    <select name="dept" id="emp_dept" onchange="ddlDeptChanged(this)">
                        <option value="">全て</option>
                        {foreach from=$dept_list item=dept_item }
                            <option value="{$dept_item.dept_id}"{if $dept eq $dept_item.dept_id} selected{/if}>{$dept_item.dept_nm|escape}</option>
                        {/foreach}
                    </select>
                    <span style="{php} if (!GG_ROOM_NM) echo "display:none";{/php}">
                    <span style="padding-left:20px">{$smarty.const.GG_ROOM_NM|escape}</span>
                    <select name="room" id="emp_room">
                        <option value="">全て</option>
                        {foreach from=$room_list item=room_item }
                            <option value="{$room_item.room_id}"{if $room eq $room_item.room_id} selected{/if}>{$room_item.room_nm|escape}</option>
                        {/foreach}
                    </select>
                    </span>
                    <script type="text/javascript">
                    ddlClassChanged(ee("emp_class"));
                    setSelection(ee("emp_attr"), "{$attr}");
                    ddlAttrChanged(ee("emp_attr"));
                    setSelection(ee("emp_dept"), "{$dept}");
                    ddlDeptChanged(ee("emp_dept"));
                    setSelection(ee("emp_room"), "{$room}");
                    </script>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" class="rgroup" style="margin-top:5px; width:100%">
            <tr>
                <td>
                職員ID
                <input type="text" class="text imeoff" size="20" name="emp_id" value="{$id|escape}" />
                <span style="padding-left:20px">職員名</span>
                <input type="text" class="text" size="20" name="emp_name" value="{$name|escape}" />
                </td>
                
                <!--20151118 順天堂医院 by Y.yamamoto--> 
                <td>
                </select>
            <span style="padding-left:20px">退職/停止</span>
                <select name="emp_expire_type" navititle="退職/停止" navi="過去の受講情報を修正しなければならない場合などに、必要に応じて退職済/利用停止職員を抽出できます。<br>通常は空欄のまま検索してください。<br>※退職済職員は、前日付またはそれ以前に退職済である職員が該当します。">
                <option></option>
                <option value="include_emp_retire" {if $emp_expire_type == 'include_emp_retire'} selected {/if}>退職済職員を含める</option>
                <option value="include_emp_del_flg" {if $emp_expire_type == 'include_emp_del_flg'} selected {/if}>利用停止職員を含める</option>
            </select></nobr>

                </td>
                <!--ここまで-->
                
                
                <td class="right">
        <button type="button" class="btn-main" id="search" onclick="document.mainform.submit()">検索</button>
                </td>
            </tr>
        </table>
    </div>





    <div style="padding:5px 0">
        <table celllspacing="0" cellpadding="0" class="rgroup"><tr>



        <td style="letter-spacing:0"><nobr>
            <span style="padding-right:20px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b>{$total_count}</b></span>人</span>
            <span style="font-size:12px">表示件数</span>
            <select name="per_page_size" onchange="$('#page_num').val(1); document.mainform.submit()">
                {php}
                    $per_page_size = (int)$this->_tpl_vars["per_page_size"];
                    $ary = array(5,10,20,30,40,50,100,200,300,500,-1);
                    foreach ($ary as $v) {
                        echo '<option value="'.$v.'"    '.($v==$per_page_size?" selected":"").'>'.($v==-1?"全件":$v."件").'</option>';
                    }
                {/php}
            </select>
        </td>


        <td style="text-align:right; font-size:12px"><nobr>
            {php}
                $page_num = $this->_tpl_vars["page_num"];
                $last_page = $this->_tpl_vars["last_page"];
                $dis = ($page_num<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" />
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('top')"{php}echo $dis{/php}>≪</button>
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('prev')"{php}echo $dis{/php}>＜</button>
            <input type="text" class="text imeoff" maxlength="4" style="width:40px" id="new_page_num" value="{$page_num}"
            ><span style="font-size:12px; letter-spacing:0; padding-left:1px" class="imeoff">ページ</span>
            {php}
                $dis = ($last_page<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:38px" onclick="paging('move')"{php}echo $dis{/php}>移動</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('next')"{php}echo $dis{/php}>＞</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('last')"{php}echo $dis{/php}>≫</button></nobr>

            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td> &nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>
            <FIELDSET  style="width:180px"><LEGEND>受講出力</LEGEND>

            年度<input type="text"  name="nendo" size="4"/>
            <button type="submit" class="btn-main" style="width:78px"  name="btn_event" value="export_list" onclick=document.mainform.submit()>受講出力</button></nobr>
            </FIELDSET>
        </td>
        </tr></table>
    </div>



    <table id="emp_list_header" style="width:850px" cellspacing="0" cellpadding="0" class="list">
        <tr>
            <th width="100">職員ID</th>
            <th width="160">職員氏名</th>
               {php} if (GG_BARITESS_NENSUU_OPT_FLAG) { {/php}
                <th width="60">経験年数</th>
                <th width="60">勤続年数</th>
            {php} }{/php}
            <th width="300">所属</th>
            <th>受講履歴</th>
        </tr>
    </table>

    <div style="width:850px;">
        <table id="emp_list_body" cellspacing="0" cellpadding="0" class="list">
            {foreach from=$emp_list item=item}
            <tr>
                <td width="100" class="center">{$item.emp_personal_id}</td>
                <td width="160">
                    <!--20151118 順天堂医院 by Y.yamamoto-->
                    {if mb_strlen($item.emp_retire) ==8 and $item.emp_retire<date("Ymd")}
                        <span class="retired">退職</span>
                    {/if}
                    {if $item.emp_del_flg eq 't'}
                     <span class="retired" navi="利用停止中の職員です">停止</span>
                    {/if}
                    <!--ここまで-->
                    {$item.last_name|escape}&nbsp;{$item.first_name|escape}
                </td>
                {php} if (GG_BARITESS_NENSUU_OPT_FLAG) { {/php}
                <td width="60">{$item.exp_diff}</td>
                <td width="60">{$item.work_diff}</td>
                {php} }{/php}
                <td width="300">
                    {$item.class_nm|escape}
                    {if !empty($item.atrb_nm) }
                        ＞{$item.atrb_nm|escape}
                    {/if}
                    {if !empty($item.dept_nm) }
                        ＞{$item.dept_nm|escape}
                    {/if}
                </td>
                <td class="center">
                    <a type="emp_history" href="javascript:void(0)" onclick="return showHistory('{$item.emp_id}')">受講履歴表示</a>
                </td>
            </tr>
            {/foreach}
        </table>
    </div>



</form>
{/if}

</body>
</html>
