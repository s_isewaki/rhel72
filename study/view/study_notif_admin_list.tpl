{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}お知らせ一覧</title>
</head>
<body onresize="resized()">
<!-- ///study_notif_admin_list  -->





<script type="text/javascript">
    var contents_id = "{$contents_id}";
    var result = "{$result}";
    {literal}

    // 作成画面を開く
    function create_notif(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url  = "study_notif_admin.php?session="+session+"&contents_id="+contents_id+"&event=ev_open_create";
        window_open(url, "edit_notif", 850, 420);
    }

    // 作成画面を開く
    function edit_notif(notif_id) {
        var url = "study_notif_admin.php?session="+session+"&contents_id="+contents_id+"&event=ev_open_edit&notifid="+notif_id;
        window_open(url, "edit_notif", 850, 420);
        return false;
    }

    function delete_notif(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;

        var ary = document.getElementsByName("notif_id");
        var notif_id_list = [];
        for (var idx=0; idx<ary.length; idx++) {
            var elem = ary[idx];
            if (elem.checked) notif_id_list.push(ary[idx].value);
        }

        if(!notif_id_list.length) { alert("削除対象となるお知らせが選択されていません。"); return false; }
        if (!confirm("選択したお知らせを削除します。\n既読者が存在する場合、既読状態ごとクリアされます。\n\n削除してよろしいですか？\n")) return false;

        ee("notif_id_comma_list").value = notif_id_list.join(",");
        $("[name=event]").val("ev_delete");
        document.mainform.submit();
    }


    function allCheckEntry(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var caption = $(btn).html();
        var ary = document.getElementsByName("notif_id");
        var bool = (caption == "選択");
        for (var idx=0; idx<ary.length; idx++) ary[idx].checked = bool;
        $(btn).html(bool?"解除":"選択");
    }

    $(document).ready(function() {
        if (result=="success") {
            parentRefreshRequest("mainform");
        }
    });
    {/literal}
</script>






<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>お知らせ一覧 </nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents_title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





{if not $edit_auth}
<div style="padding:5px"><div class="warningbox">
    権限がありません。閲覧のみ可能です。
</div></div>
{/if}
<div style="padding:5px; padding-bottom:8px">
    <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" onclick="create_notif(this)">作成</button>
    <span style="padding-left:20px">
    <button type="button" class="btn-main{if $notif_list|@count eq 0 || (not $edit_auth)} btn-off{/if}" onclick="delete_notif(this)" navititle="削除" navi="選択対象を削除します。<br>期読者が存在する場合、既読状態ごとクリアされます。">削除</button></span>
</div>





<div class="usual_box">
<form name="mainform" action="study_notif_admin.php" method="POST">
    <input type="hidden" name="event" id="event" value="" />
    <input type="hidden" name="notif_id_comma_list" id="notif_id_comma_list" value="" />
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />

    <table cellspacing="0" cellpadding="0" class="list">
        <tr>
            <th width="60"><button type="button" class="btn-sel{if $notif_list|@count eq 0 || (not $edit_auth)} btn-off{/if}" onclick="allCheckEntry(this)">選択</button></th>
            <th width="275">タイトル</th>
            <th width="130">対象者</th>
            <th width="190">終了日時</th>
            <th width="90">更新日</th>
            <th>更新者</th>
        </tr>
    </table>
    {if $notif_list|@count}
    <div style="overflow-y:scroll; border-bottom:1px solid #dddddd; background-color:#f5f5f5;" class="scroll_div" heightMargin="0">
        <table cellspacing="0" cellpadding="0" class="list">
            {foreach from=$notif_list item=item}
                <tr>
                <td width="60" class="center"><input name="notif_id" type="checkbox" value="{$item.id}"></td>
                <td width="275">
                    {php}
                    $end_date = $this->_tpl_vars['item']['end_date'];
                    $start_date = $this->_tpl_vars['item']['start_date'];
                    $today = date("YmdHi");
                    if (!$start_date || !$end_date || $start_date > $today || $today > $end_date) {
                    {/php}
                    <span class="graybox">期間外</span>
                    {php} } {/php}
                    <a href="javascript:void(0)" onclick="return edit_notif({$item.id})">{$item.title|escape}</a>
                </td>
                <td width="130" class="center">
                    {if $item.target eq "0"}未受講者{/if}
                    {if $item.target eq "1"}受講者全員{/if}
                    {if $item.target eq "2"}集合研修出席者{/if}
                    {if $item.target eq "3"}集合研修欠席者{/if}
                    {if $item.target eq "4"}オンライン受講者{/if}
                </td>
                <td width="190" class="center">{$item.end_date_fmt|escape}</td>
                <td width="90" class="center">{$item.update_datetime|date_format:"%Y/%m/%d"}</td>
                <td>{$item.emp_lt_nm}&nbsp;{$item.emp_ft_nm}</td>
            </tr>
            {/foreach}
        </table>
    </div>
    {/if}



    {if $notif_list|@count eq 0}
        <div style="padding:10px 5px" class="gray2">お知らせは登録されていません。</div>
    {/if}

</form>
</div>




</body>
</html>