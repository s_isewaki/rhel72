{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}テスト結果</title>
    {literal}
    <style type="text/css">
    td.right_arrow { background:url(../img/right_red.gif) no-repeat 96px 4px }
    </style>
    {/literal}
</head>
<body onresize="resized()">
<!-- ///study_test_user_result  -->



<script type="text/javascript">
    var test_direct = "{$test_direct}";
    {literal}
    function show_test(id, count){
        for (var i = 1; i <= count; i++) {
            ee("obj" + i).style.display = (id == i ? "inline" : "none");
            ee("td_button"+i).className = (id == i ? "right_arrow" : "");
        }
    }

    $(document).ready(function() {
        if (test_direct) {
            grandParentRefreshRequest("mainform");
            setTimeout("if(opener && opener!=window) opener.focus(); window.focus();", 500);
        }
        ee("td_button1").className = "right_arrow";
    });
    {/literal}
</script>



<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>テスト結果</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents.title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





{if $target_emp_name}
    <div class="findbox"><span class="brown">解答者</span>&nbsp;&nbsp;{$target_emp_name|escape}</div>
{/if}





<div class="usual_box">

    <table width="100%" cellspacing="0" cellpadding="0"><tr>
    <td valign="top" style="width:350px">

        <div class="scroll_div orange_border" heightMargin="144">
            <table cellspacing="0" cellpadding="0" class="list" style="width:330px">
                <tr>
                    <th>問題</th>
                    <th>解答</th>
                    <th>結果</th>
                    <th width="110">正答確認</th>
                </tr>
                {section name=test_item loop=$test_items}
                <tr>
                    <td class="center">{$smarty.section.test_item.index_next}</td>
                     {if $test_items[test_item].result eq "×"}
                        <td class="center">{$test_items[test_item].answer}</td>
                        <td class="center" style="background-color:#ffdddd; color:#ff0000; font-weight:bold">{$test_items[test_item].result}</td>
                     {else}
                        <td class="center">{$test_items[test_item].answer}</td>
                        <td class="center" style="background-color:#99f599; color:#33aa33; font-weight:bold">{$test_items[test_item].result}</td>
                     {/if}
                    <td id="td_button{$smarty.section.test_item.index_next}" style="text-align:center"><button type="button" class="btn-main" id="question"
                    onclick="show_test({$smarty.section.test_item.index_next}, {$test_items|@count})">確認</button></td>
                </tr>
                {/section}
            </table>
        </div>




        <div class="orange_border" style="height:130px; overflow-y:scroll;">
            {foreach from=$test_result item=item}
            <div style="padding:10px 0 0 5px"><b>{$item.no}回目</b></div>
            <div style="padding:2px 0 0 15px">
                {$item.test_date|date_format:"%Y年%m月%d日 %H時%M分"}&nbsp;&nbsp;&nbsp;
                正解率:{$item.correct_count}/{$test_item_count}({$item.percentage}%)
            </div>
            {/foreach}
            <div>&nbsp;</div>
        <div>

    </td>

    <td valign="top">
        <div class="scroll_div orange_border" heightMargin="12">
            <div style="padding:5px">
                {section name=test_item loop=$test_items }
                {if $smarty.section.test_item.index_next eq "1"}
                    <div id="obj{$smarty.section.test_item.index_next}" style="">
                {else}
                    <div id="obj{$smarty.section.test_item.index_next}" style="display:none">
                {/if}
                    <div style="padding:5px 0">

                        <div class="beige">
                            <div style="padding-bottom:4px"><b>問題{$smarty.section.test_item.index_next}</b></div>
                            <div style="background-color:#ffffe5; padding:10px; border:1px solid #eeeeee; border-bottom:1px solid #dddddd;word-break: break-all;"
                                >{$test_items[test_item].question|escape|nl2br}</div>
                        </div>


                        {if $test_items[test_item].image_path_url}
                        <div style="padding-top:15px">
                            <img src="{$test_items[test_item].image_path_url}" alt="{$test_items[test_item].image_path_url}" />
                        </div>
                        {/if}

                        <div style="padding:15px 5px 15px 5px">
                        <b>解答</b>
                        <div style="padding-top:5px; line-height:1.7;word-break: break-all;">
                        {foreach from=$test_items[test_item].answers item=answer name="answer"}
                            {assign var="index" value=$smarty.foreach.answer.index+1}
                            {if $answer eq "t"}
                                <span class="marubatu_maru">○</span>
                                &nbsp;({$index})&nbsp;{$test_items[test_item].choices[$smarty.foreach.answer.index]|escape}<br>
                            {else}
                                <span class="marubatu_batu">×</span>
                                &nbsp;({$index})&nbsp;{$test_items[test_item].choices[$smarty.foreach.answer.index]|escape}<br>
                            {/if}
                        {/foreach}
                        </div>
                        </div>

                        {if $test_items[test_item].comment ne ""}
                        <div style="padding:5px 0 5px 5px;word-break: break-all;">
                            <b>解説</b><br>
                            <div style="padding:5px">
                                <div style="padding:5px; background-color:#eeeeee; line-height:1.5">
                                    {$test_items[test_item].comment|escape|nl2br}
                                </div>
                            </div>
                        </div>
                        {/if}
                    </div>
                    </div>
                {/section}
            </div>
        </div>
    </td>
    </tr></table>


</div>
</body>
</html>