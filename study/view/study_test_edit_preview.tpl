<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>テスト実施（プレビュー）</title>
</head>
<body onresize="resized()">
<!-- ///study_test_user_execute  -->




<script type="text/javascript">
    var item_id_comma_list = "<?=$item_id_comma_list?>";
    $(document).ready(function() {
        if (!opener || !opener.document) return;
        pdoc = opener.document;
        var kStart = pdoc.getElementById("start_day").value;
        var kEnd = pdoc.getElementById("end_day").value;
        ee("kikan").innerHTML = '';
        if (kStart && kEnd) {
        ee("kikan").innerHTML =
            kStart+"&nbsp;"+
            getSelectionValue(pdoc.getElementById("st_hour"))+"&nbsp;:&nbsp;"+
            getSelectionValue(pdoc.getElementById("st_minute"))+
            " 〜 "+
            kEnd+"&nbsp;"+
            getSelectionValue(pdoc.getElementById("ed_hour"))+"&nbsp;:&nbsp;"+
            getSelectionValue(pdoc.getElementById("ed_minute"));
        }

        if (pdoc.getElementById("rlt_y").checked) {
            var ltime = pdoc.getElementById("limit_time").value;
            if (int(ltime) > 0) {
                ee("td_timer").style.display = "";
                ee("timer").innerHTML = ltime + "：00";
            }
        }
        var list = item_id_comma_list.split(",");
        for (var nn=1; nn<=list.length; nn++) {
            var num = list[nn-1];
            if (!pdoc.getElementById("question_"+num)) continue;
            ee("question_"+num).innerHTML = htmlEscape(pdoc.getElementById("question_"+num).value).replace(/\n/g, "<br>");
            if (!pdoc.getElementById("attach_delete_"+num).value) {
                var image_tag = "";
                var container = pdoc.getElementById("image_container_"+num);
                if (container.style.display!="none") image_tag = container.innerHTML;
                container = pdoc.getElementById("image_container_org_"+num);
                if (container.style.display!="none") image_tag = container.innerHTML;
                ee("image_container_"+num).innerHTML = image_tag;
                $("#image_container_"+num+" img").removeAttr("width").removeAttr("height");
            }
            var choice_cnt = int(pdoc.getElementById("choice_cnt_"+num).value);
            for (var idx=1; idx<=choice_cnt; idx++) {
                ee("choice_tr_"+num+"_"+idx).style.display = "";
                ee("span_"+num+"_"+idx).innerHTML = pdoc.getElementById("choice_item_"+num+"_"+idx).value;
                var checkOrRadio = "check";
                if (!pdoc.getElementById("imc_"+num).checked) checkOrRadio = "radio";
                ee("answer_"+num+"_"+idx+"_"+checkOrRadio).style.display = "";
            }
        }
    });
</script>






<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>テスト実施（プレビュー）</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>






<div class="usual_box">
    <!--本体-->
    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="70">テスト期間</th>
            <td id="kikan" style="padding-left:4px"></td>
        </tr>
    </table>
</div>




<div class="usual_box">
    <div class="section_title">
        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
            <td style="width:auto"><b>合計<span style="padding:0 5px"><?=$item_count?></span>問</b></td>
            <td id="td_timer" style="width:auto; display:none"><b>制限時間&nbsp;&nbsp;&nbsp;<span id="timer" style="font-size:16px; letter-spacing:3px"></span></b></td>
            <td style="width:300px; text-align:right; background-color:#ee88d7; color:#ffffff; border:1px solid #d170b9"><nobr>※テスト完了後、必ず採点ボタンを押してください。</nobr></td>
            <td style="width:50px; text-align:right"><button type="button" class="btn-main btn-off">採点</button></td>
        </tr></table>
    </div>
</div>



<div class="usual_box">
    <div style="overflow-y:scroll; overflow-x:hidden; border:1px solid #7ee47c; position:relative" class="scroll_div" heightMargin="0">
        <div style="padding:20px 5px">

            <? $ary = explode(",", $item_id_comma_list); ?>
            <? foreach ($ary as $num) { ?>
            <div class="content">
                <div class="beige">
                    <div style="padding-bottom:4px"><b>問題<?=$num?></b></div>
                    <div id="question_<?=$num?>"
                        style="background-color:#ffffe5; padding:10px; border:1px solid #eeeeee; border-bottom:1px solid #dddddd;word-break: break-all;"></div>
                </div>


                <div style="padding:10px 0 26px 0">
                    <span id="image_container_<?=$num?>">
                    <img src="" alt=""><br/>
                    </span>

                    <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%">
                    <? for ($idx=1; $idx<=10; $idx++) {?>
                    <?     $id= $num."_".$idx; ?>
                    <tr id="choice_tr_<?=$id?>" style="display:none">
                        <th style="width:10px; vertical-align:top">  <!-- //チェッックボックスの位置を上に揃えました20141203TN -->
                            <input id="answer_<?=$id?>_check" type="checkbox" name="answer_<?=$num?>" style="display:none">
                            <input id="answer_<?=$id?>_radio" type="radio" name="answer_<?=$num?>" style="display:none">
                        </th>
                        <td><label style="display:block;word-break: break-all;" for="answer_<?=$id?>">(<?=$idx?>)&nbsp;<span id="span_<?=$id?>"></span></label></td>
                    </tr>
                    <? } ?>
                    </table>
                </div>
            </div>
            <? } ?>

            <div style="padding:30px 0"><div class="section_title">（-- 以上 --）</div></div>
        </div>
    </div>
</div>


</body>
</html>
