<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>集合研修申込</title>
</head>
<body onresize="resized()">
<!-- ///study_assembly_user  -->





<script type="text/javascript">
    var result = "<?=$result?>";
    function exe_apply_submit(obj, btn_event){
        if (obj.className.indexOf("btn-off")>-1) return;
        if (btn_event == "be_register"){
            if ($("input:radio[name^='apply_no']:checked").size() == 0) { alert("出席申込を選択してください。"); return false; }
        }
        $("[name=btn_event]").val(btn_event);
        document.apply_main.submit();
    }
    $(document).ready(function() {
        if (result=="success") { parentRefreshRequest("mainform"); window.close(); }
    });
</script>





<? if ($result!="success") { ?>



<iframe frameborder="0" name="download_frame" style="position:absolute; left:0; top:0; width:0; height:0"></iframe>


<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>集合研修申込</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="<?=h($contents_title)?>" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





<div class="usual_box">
    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="140">出欠登録の受付期間</th>
            <td><?=$limit_date?></td>
        </tr>
        <tr>
            <th>備考</th>
            <td><?=$notes?></td>
        </tr>
    </table>
</div>


<div style="padding:20px 5px 0 5px">
    <div>出欠申込状況：&nbsp;&nbsp;<b style="color:#ff0000"><?=$status_name?></b></div>
</div>




<div class="usual_box">
<form name="apply_main" action="" method="post" target="_self">
    <input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
    <input type="hidden" name="contents_id" value="<?=$contents_id?>" />
    <input type="hidden" name="event" value="ev_exec_assembly_user" />
    <input type="hidden" name="btn_event" value="" />

    <table cellspacing="0" cellpadding="0" class="list">
        <tr>
            <th width="140">開催日</th>
            <th width="110">開催時刻</th>
            <th>開催場所</th>
            <th>講師</th>
            <th width="70">定員</th>
            <th width="70">申込者数</th>
            <th width="70">出席申込</th>
        </tr>
        <?
            foreach ($asm_list as $item) {
                $cls = "";
                if ($item["is_limit_over"]) $cls = ' class="out_of_date"';
                if ((int)$item["no"]==$mosikomi_no) $cls = ' class="pickup_color"';
        ?>
        <tr<?=$cls?>>
            <td class="center"><?=$item["date_jp"]?></td>
            <td class="center"><?=$item["time_jp"]?></td>
            <td><?=h($item["place"])?></td>
            <td><?=h($item["lecturer"])?></td>
            <td class="center"><?=h($item["persons_limit"])?></td>
            <td class="center"><?=$item["application_count"]?></td>
            <td class="center">
                <input type="radio" name="apply_no" value="<?=$item["no"]?>"<?=($item["is_limit_over"]?' disabled="disabled"':"")?>
                <? if ((int)$item["no"]==$mosikomi_no) echo " checked"; ?> />
            </td>
        </tr>
        <? } ?>
    </table>

    <?
        $available_days = 0;
        foreach ($asm_list as $item) {
            if ($item["is_limit_over"]) continue;
            $available_days++; // 選択可能なデータあり
            if ((int)$item["no"]==$mosikomi_no) $is_checked = 1; // かつその選択可能データは選択されている場合。
        }
        $cls_register = " btn-off";
        $cls_absent = " btn-off";
        $cls_clear = " btn-off";
        if ($assembly_status==1 || $assembly_status==2) $cls_clear = "";
        if ($assembly_status!=2) $cls_absent = "";
        if ($available_days > 0 && $available_days - $is_checked > 0) $cls_register = "";
    ?>

    <div style="padding:10px 0; text-align:right">
        <button type="button" class="btn-main<?=$cls_register?>" onclick="exe_apply_submit(this, 'be_register')">出席</button>
        <button type="button" class="btn-main<?=$cls_absent?>" onclick="exe_apply_submit(this, 'be_absent')">欠席</button>
        <button type="button" class="btn-main<?=$cls_clear?>" onclick="exe_apply_submit(this, 'be_clear')">クリア</button>
    </div>
</form>
</div>



<?  } ?>


</body>
</html>
