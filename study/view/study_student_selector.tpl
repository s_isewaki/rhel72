<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>職員選択</title>
</head>
<body onresize="resized()">
<!-- ///study_student_selector  -->




<script type="text/javascript">
    function allCheckEmp(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var caption = btn.innerHTML;
        var ary = document.getElementsByName("emp_id");
        var bool = (caption == "選択");
        for (var idx=0; idx<ary.length; idx++) ary[idx].checked = bool;
        btn.innerHTML = (bool?"解除":"選択");
    }

    function paging(mode) {
        var cur_page_num = <?=$page_num?>;
        var cur_per_page_size = <?=$per_page_size?>;
        var total_count = <?=(int)@$total_count?>;
        var page_num = parseInt($("#page_num").val());
        var new_page_num = parseInt($("#new_page_num").val());
        var last_page = <?=$last_page?>;
        if (last_page < 1) last_page = 1;
        if (!page_num || isNaN(page_num)) page_num = 1;
        if (mode=="next") $("#page_num").val(++cur_page_num);
        if (mode=="prev") $("#page_num").val(--cur_page_num);
        if (mode=="top") $("#page_num").val(1);
        if (mode=="last") $("#page_num").val(last_page);
        if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
        if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
        if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
        if (mode) document.sub_main.submit();
    }

    // body_onload
    $(document).ready(function() {

        // 受講者登録ボタン
        $("#setting").click(function() {
            var ary = document.getElementsByName("emp_id");
            var emp_id_list = [];
            for (var idx=0; idx<ary.length; idx++) {
                var elem = ary[idx];
                if (elem.checked) emp_id_list.push(ary[idx].value);
            }
            <? if ($js_callback) { ?>
            if (opener && opener.<?=$js_callback?>) {
                if (!emp_id_list.length) return alert("職員を指定してください。");
                window.close();
                opener.<?=$js_callback?>(emp_id_list);
                return;
            }
            <? } ?>
        });
    });
</script>






<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>職員選択</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>







<div class="usual_box">
<form name="sub_main" action="" method="post" target="_self">
    <input type="hidden" name="js_callback" value="<?=$js_callback?>" />

    <div class="findbox">
    <table cellspacing="0" cellpadding="0" class="rgroup">
        <tr>
            <td><nobr>
            <?=h(GG_CLASS_NM)?>
            <select name="class" id="emp_class" onchange="ddlClassChanged(this)">
                <option value="">全て</option>
                <? foreach ($class_list as $row) { ?>
                    <option value="<?=$row["class_id"]?>"<?=($class==$row["class_id"]?" selected":"")?>><?=h($row["class_nm"])?></option>
                <? } ?>
            </select>
            <span style="padding-left:20px"><?=h(GG_ATRB_NM)?></span>
            <select name="attr" id="emp_attr" onchange="ddlAttrChanged(this)">
                <option class_id="" value="">全て</option>
                <? foreach ($attr_list as $row) { ?>
                    <option value="<?=$row["atrb_id"]?>"<?=($attr==$row["atrb_id"]?" selected":"")?>><?=h($row["atrb_nm"])?></option>
                <? } ?>
            </select>
            <span style="padding-left:20px"><?=h(GG_DEPT_NM)?></span>
            <select name="dept" id="emp_dept" onchange="ddlDeptChanged(this)" style="width:200px">
                <option attr_id="" value="">全て</option>
                <? foreach ($dept_list as $row) { ?>
                    <option value="<?=$row["dept_id"]?>"<?=($dept==$row["dept_id"]?" selected":"")?>><?=h($row["dept_nm"])?></option>
                <? } ?>
            </select>
            <span style="<?=(GG_ROOM_NM?"":"display:none")?>">
            <span style="padding-left:20px;"><?=h(GG_ROOM_NM)?></span>
            <select name="room" id="emp_room">
                <option attr_id="" value="">全て</option>
                <? foreach ($room_list as $row) { ?>
                    <option value="<?=$row["room_id"]?>"<?=($room==$row["room_id"]?" selected":"")?>><?=h($row["room_nm"])?></option>
                <? } ?>
            </select>
            </span>
            <script type="text/javascript">
            ddlClassChanged(ee("emp_class"));
            setSelection(ee("emp_attr"), "<?=$attr?>");
            ddlAttrChanged(ee("emp_attr"));
            setSelection(ee("emp_dept"), "<?=$dept?>");
            ddlDeptChanged(ee("emp_dept"));
            setSelection(ee("emp_room"), "<?=$room?>");
            </script>
            </nobr></td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" class="rgroup" style="margin-top:5px">
        <tr>
            <td><nobr>職種
            <select name="job" style="width:160px">
                <option value="">全て</option>
                <? foreach ($job_list as $row) { ?>
                    <option value="<?=$row["job_id"]?>"<?=($job==$row["job_id"]?" selected":"")?>><?=h($row["job_nm"])?></option>
                <? } ?>
            </select>
            <span style="padding-left:20px">役職</span>
            <select name="status" style="width:110px">
                <option value="">全て</option>
                <? foreach ($status_list as $row) { ?>
                    <option value="<?=$row["st_id"]?>"<?=($status==$row["st_id"]?" selected":"")?>><?=h($row["st_nm"])?></option>
                <? } ?>
            </select>

            <span style="padding-left:20px">退職/停止</span>
                <select name="emp_expire_type" navititle="退職/停止" navi="過去の受講情報を修正しなければならない場合などに、必要に応じて退職済/利用停止職員を抽出できます。<br>通常は空欄のまま検索してください。<br>※退職済職員は、前日付またはそれ以前に退職済である職員が該当します。">
                <option></option>
                <option value="include_emp_retire" <?=($emp_expire_type=="include_emp_retire"?" selected":"")?>>退職済職員を含める</option>
                <option value="include_emp_del_flg" <?=($emp_expire_type=="include_emp_del_flg"?" selected":"")?>>利用停止職員を含める</option>
            </select></nobr>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" class="rgroup" style="margin-top:5px; width:100%">
        <tr>
            <td>職員ID
                <input type="text" class="text imeoff" size="15"  maxlength="12" name="in_emp_id" value="<?=h($input_emp_id)?>" />
            <span style="padding-left:20px">職員名</span>
            <input type="text" class="text" size="20" maxlength="30" name="emp_name" value="<?=h($input_emp_name)?>" />
            </td>
            <td style="text-align:right">
        <button type="button" id="search" class="btn-main" onclick="document.sub_main.submit()">検索</button>
            </td>
        </tr>
    </table>
    </div>





    <div style="padding:5px 2px 0 5px">
        <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
            <td style="letter-spacing:0"><nobr>
                <? if ($contents["student_count"]) {?>
                <span style="padding-right:0px; font-size:11px;">受講者<span style="font-size:16px; color:#dd0000; padding:0 2px"><b><?=$contents["student_count"]?></b></span>人／</span>
                <? } ?>
                <span style="padding-right:20px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b><?=$total_count?></b></span>人</span>
                <span style="font-size:12px">表示件数</span>
                <select name="per_page_size" onchange="$('#page_num').val(1); document.sub_main.submit()">
                    <? $ary = array(5,10,20,30,40,50,100,200,300,500,-1); ?>
                    <? foreach ($ary as $v) {?><option value="<?=$v?>"<?=($v==$per_page_size?" selected":"")?>><?=($v==-1?"全件":$v."件")?></option><?}?>
                </select>
            </td>
            <td style="font-size:12px"><nobr>
                <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" />
                <?$dis = ($page_num<=1?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('top')"<?=$dis?>>≪</button>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('prev')"<?=$dis?>>＜</button>
                <input type="text" class="text imeoff" maxlength="4" style="width:40px" id="new_page_num" value="<?=$page_num?>"
                ><span style="font-size:12px; letter-spacing:0; padding-left:1px" class="imeoff">ページ</span>
                <?$dis = ($last_page<=1?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:38px" onclick="paging('move')"<?=$dis?>>移動</button>
                <?$dis = ($page_num>=$last_page?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('next')"<?=$dis?>>＞</button>
                <?$dis = ($page_num>=$last_page?" disabled":""); ?>
                <button type="button" class="btn-main <?=($dis?"btn-off":"")?>" style="width:26px" onclick="paging('last')"<?=$dis?>>≫</button></nobr>
            </td>
        </tr></table>
    </div>




    <div style="padding-top:5px">

        <table id="emp_list_header" cellspacing="0" cellpadding="0" class="list">
            <tr navi="並び順：所属コード昇順、職員漢字氏名昇順">
                <th width="40"><button type="button" class="btn-sel<?=(!count($emp_list) ? " btn-off":"")?>" onclick="allCheckEmp(this)">選択</button></th>
                <th width="110">職員ID</th>
                <th width="170">職員氏名</th>
                <? if (GG_BARITESS_NENSUU_OPT_FLAG) { ?>
                <th width="60">経験年数</th>
                <th width="60">勤続年数</th>
                <? }?>

                <th>所属</th>
            </tr>
        </table>

    <? if (count($emp_list)) { ?>
        <div class="scroll_div" heightMargin="30">
            <table id="emp_list_body" cellspacing="0" cellpadding="0" class="list">
                <? foreach ($emp_list as $row) { ?>
                <tr>
                    <td width="40" class="center" id="<?=$row["emp_id"]?>" ><input name="emp_id" type="checkbox" value="<?=$row["emp_id"]?>" /></td>
                    <td width="110"><?=$row["emp_personal_id"]?></td>
                    <td width="170">
                        <?=(strlen($row["emp_retire"])==8 && $row["emp_retire"]<date("Ymd") ? '<span class="retired" navi="退職：'.substr($row["emp_retire"],0,4).'/'.substr($row["emp_retire"],4,2).'/'.substr($row["emp_retire"],6,2).'" >退職</span>' : '')?>
                        <?=($row["emp_del_flg"]=="t" ? '<span class="retired" navi="利用停止中の職員です">停止</span>' : '')?>
                        <?=($row["emp_manabu_flg"]!="t" ? '<span class="no_auth" navi="システムの閲覧権限/利用権限がありません">[権なし]</span>' : '')?>
                        <?=h($row["last_name"])?>&nbsp;<?=h($row["first_name"])?>
                    </td>
                     <? if (GG_BARITESS_NENSUU_OPT_FLAG) { ?>
                    <td width="60"><?=$row["exp_diff"]?></td>
                    <td width="60"><?=$row["work_diff"]?></td>
                     <? }?>
                    <td><?=h($row["class_nm"])?>
                    <? if ($row["atrb_nm"]) echo "＞" . h($row["atrb_nm"]); ?>
                    <? if ($row["dept_nm"]) echo "＞" . h($row["dept_nm"]); ?>
                    </td>
                </tr>
                <? } ?>
            </table>
        </div>


        <div style="text-align:right; padding-top:8px;">
            <button type="button" class="btn-main" id="setting">職員登録</button>
        </div>
    <? } ?>
    </div>


</form>
</div>
</body>
</html>