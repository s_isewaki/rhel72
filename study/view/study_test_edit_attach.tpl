{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}画像ファイル選択</title>
</head>
<body onresize="resized()">
<!-- ///study_test_edit  -->

<script type="text/javascript">
    var item_num  = "{$item_num}";
    var result = "{$result}";
    var file_path = "{$file_path}";
    var file_name = "{$file_name}";
    var file_path_url = "{$file_path_url}";
    var image_size_style = '{$image_size_style}';
    var contents_id = "{$contents_id}";
    {literal}
    function tryUpload(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        btn.className = "btn-main btn-off";

        // 添付ファイルの拡張子チェック
        var elem = ee("attach_file");
        if ($(elem).val()=="") return alert("ファイルを指定してください。");
        var w_array = $(elem).val().split(".");
        if (w_array.length > 0) {
            var extension = w_array[w_array.length-1].toLowerCase();
            if (extension != "png" && extension != "gif" && extension != "jpg") {
                return alert("画像ファイルの拡張子ではありません。\n.png, .jpg, .gifのいずれかである必要があります。");
            }
        }
        // IE11のみ裏でsubmit実行
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.indexOf("msie")== -1 && ua.indexOf('trident/7') != -1){
            setInterval(function(){ if (opener && opener!=window && opener.document && opener.document.dummyform) opener.document.dummyform.submit(); }, 800);
        }
        setTimeout(function() { document.mainform.submit(); }, 10);
    }

    $(document).ready(function() {
        if (result!="success") return;
        if (opener && opener.document && opener.document.getElementById("image_container_"+item_num)) {
            var container = opener.document.getElementById("image_container_"+item_num);
            container.innerHTML = '<img src="'+file_path_url+'" alt="" '+image_size_style+' />';
            opener.document.getElementById("new_image_path_"+item_num).value = file_path;
            opener.document.getElementById("new_image_name_"+item_num).value = file_name;
            opener.document.getElementById("image_name_disp_"+item_num).innerHTML = file_name;
            opener.document.getElementById("image_container_org_"+item_num).style.display = "none";
            opener.document.getElementById("image_container_"+item_num).style.display = "";
            opener.document.getElementById("btn_cancel_attach_"+item_num).className = "btn-main";
            window.close();
        }
    });




    {/literal}
</script>






<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>画像ファイル選択</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>






<div class="usual_box">
<form name="mainform" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="btn_event" value="be_exec_test_edit_attach_upload" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />
    <input type="hidden" name="item_num" value="{$item_num}" />

    <table cellspacing="0" cellpadding="0" class="inputlist">
    <tr>
        <th width="90">画像ファイル</th>
        <td><input type="file" name="attach_file" id="attach_file" value="" style="width:60%;" /></td>
    </tr>
    </table>

    <div class="gray" style="padding-top:10px">※実際のテストでは、原寸サイズの画像が公開されます。</div>
    <div class="gray" style="padding-top:2px">&nbsp;&nbsp;&nbsp;あらかじめ適切なサイズの画像をアップロードしてください。</div>

    <div style="padding:5px 0; text-align:right">
        <button type="button" class="btn-main" onclick="tryUpload(this)">アップロード</button>
    </div>

</form>
</div>
</body>
</html>
