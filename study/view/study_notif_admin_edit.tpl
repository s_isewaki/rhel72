{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}{$title}</title>
    <script type="text/javascript" src="js/jquery.treeview.js"></script>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_notif_admin_edit  -->






<script type="text/javascript">
    var result = "{$result}";
    var preview_flg = "{$preview_flg}";
    {literal}
    function validate_require() {
        var info_title = $("[name=info_title]").val();
        var info_message = $("[name=info_message]").val();
        if (info_title == "") { alert("タイトルが入力されていません。"); $("[name=info_title]").focus(); return false; }
        if (info_message == "") { alert("内容が入力されていません。"); $("[name=info_message]").focus(); return false; }
        if (!ee("ts1").checked && !ee("ts2").checked && !ee("ts3").checked && !ee("ts4").checked && !ee("ts5").checked) {
            alert("対象者を指定してください。"); return false;
        }
        if (!convertAndCheckDateTime()) return false;
        return true;
    }

    function getByte(string){
        if(string.length == 0){return 0;}
        var count = 0;
        for(var i = 0; i <string.length; i++){
            if( escape(string.charAt(i)).length  < 4 ) count = count + 1;
            else count = count + 2;
        }
        return count;
    }

    function validate_length() {
        var info_title = $("[name=info_title]").val();
        if (getByte(info_title) > 100) { alert("タイトルが長すぎます。"); $("[name=info_title]").focus(); return false; }
        return true;
    }

    $(document).ready(function() {
        if (result=="success") {
            parentRefreshRequest("mainform", {"event":""});
            grandParentRefreshRequest("mainform");
            window.close();
            return;
        }

        if (preview_flg!="t") {
            $("#start_day").datepicker( { dateFormat:"yy年mm月dd日", buttonImageOnly:true });
            $("#end_day").datepicker( { dateFormat: "yy年mm月dd日", buttonImageOnly:true });
        }
    });

    function exec_reg(btn, evt) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if (!validate_require()) return false; // 必須チェック
        var info_title = $("[name=info_title]").val();
        if (getByte(info_title) > 100) { alert("タイトルが長すぎます。"); $("[name=info_title]").focus(); return false; }
        $("[name=event]").val(evt);
        document.mainform.submit();
    }

    function exec_set_confirm_mark(btn, mark_backward) {
        if (btn.className.indexOf("btn-off")>=0) return;
        $("[name=mark_backward]").val(mark_backward);
        document.mainform.submit();
    }

    {/literal}
</script>






<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>{$title|escape}</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents_title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>







{if $preview_flg eq "t"}


    <div style="padding:10px 5px"><div style="padding:8px; border-bottom:3px solid #f4962a; background-color:#f5f5f5;">
        <span style="color:#f4962a">■</span>
       {$information.title|escape}
    </div></div>


    <div style="padding:10px 20px 30px 20px">
        {php} echo str_replace("\n", "<br />", h($this->_tpl_vars['information']['message']));{/php}
    </div>

    <div class="usual_box">
        <table cellspacing="0" cellpadding="0" class="inputlist">


            <tr>
                <th width="80"><nobr>研修講義</nobr></th>
                <td><span class="input_subguide">{$folder_joint_name|escape}＞{$contents_title|escape}</span></td>
            </tr>

            <tr>
                <th>送信先</th>
                <td><nobr>
                {if $array_check[1]}受講者全員{/if}
                {if $array_check[2]}集合研修出席者{/if}
                {if $array_check[3]}集合研修欠席者{/if}
                {if $array_check[4]}オンライン受講者{/if}
                {if $array_check[0]}未受講者{/if}</nobr>
            </tr>
        </table>
    </div>

    <div style="padding:0 5px 5px 5px; color:#999999">
        掲示期間：
        {$array_start[0]}&nbsp;{$array_start[1]}
        <span class="input_subguide">時</span>&nbsp;{$array_start[2]}&nbsp;<span class="input_subguide">分 </span>
        &nbsp;&nbsp;〜&nbsp;&nbsp;
        {$array_end[0]}&nbsp;{$array_end[1]}
        <span class="input_subguide">時</span>&nbsp;{$array_end[2]}&nbsp;<span class="input_subguide">分</span>
    </div>


    <div style="padding:5px; text-align:right">
        <button type="button" class="btn-main{if $information.is_notif_confirmed} btn-off{/if}" onclick="exec_set_confirm_mark(this,'')">既読にする</button>
        <button type="button" class="btn-main{if !$information.is_notif_confirmed} btn-off{/if}" onclick="exec_set_confirm_mark(this,'1')">未読にする</button>
    </div>
    <form name="mainform" action="" method="post">
        <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
        <input type="hidden" name="event" value="ev_exec_set_confirm_mark" />
        <input type="hidden" name="mark_backward" value="" />
        <input type="hidden" name="result" value="{$result}" />
        <input type="hidden" name="notif_id" value="{$information.id}" />
        <input type="hidden" name="contents_id" value="{$contents_id}" />
        <input type="hidden" name="preview_flg" value="t" />
    </form>
{else}




<div class="usual_box">
<form name="mainform" action="" method="post">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="event" value="{$event}" />
    <input type="hidden" name="result" value="{$result}" />
    <input type="hidden" name="notif_id" value="{$information.id}" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />
    <input type="hidden" name="start_date" value="{$array_start[0]}" />
    <input type="hidden" name="end_date" value="{$array_end[0]}" />


    {if not $edit_auth}
    <div style="padding-bottom:5px"><div class="warningbox">
        権限がありません。閲覧のみ可能です。
    </div></div>
    {/if}




    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="70"><nobr>掲示期間</nobr></th>
            <td>
                <div style="padding-left:3px"></nobr>
                <span class="brown">開始日時</span>
                <input type="text" class="text" size="18" id="start_day" readonly value="{$array_start[0]}" style="width:120px" />
                <select name="st_hour">
                    {section name=st_item start=0 loop=24 step=1}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $array_start[1]}
                            <option value="{$smarty.section.st_item.index}" selected>{$smarty.section.st_item.index}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index}">{$smarty.section.st_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="st_minute">
                    {section name=st_item start=0 loop=60 step=5}
                        {if $smarty.section.st_item.index|string_format:"%02d" eq $array_start[2]}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}" selected>{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.st_item.index|string_format:"%02d"}">{$smarty.section.st_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分 </span>
                &nbsp;&nbsp;&nbsp;
                <span class="brown">終了日時</span>
                <input id="end_day" type="text" class="text" size="18" readonly value="{$array_end[0]}" style="width:120px" />
                <select name="ed_hour">
                    {section name=ed_item start=0 loop=24 step=1}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $array_end[1]}
                            <option value="{$smarty.section.ed_item.index}" selected>{$smarty.section.ed_item.index}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index}">{$smarty.section.ed_item.index}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">時</span>

                <select name="ed_minute">
                    {section name=ed_item start=0 loop=60 step=5}
                        {if $smarty.section.ed_item.index|string_format:"%02d" eq $array_end[2]}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}" selected>{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {else}
                            <option value="{$smarty.section.ed_item.index|string_format:"%02d"}">{$smarty.section.ed_item.index|string_format:"%02d"}</option>
                        {/if}
                    {/section}
                </select>
                <span class="input_subguide">分</span>
                </nobr></div>
            </td>
        </tr>
        <tr>
            <th>研修講義</th>
            <td><span class="input_subguide">{$folder_joint_name|escape}＞{$contents_title|escape}</span></td>
        </tr>
        <tr>
            <th>タイトル</th>
            <td><input name="info_title" maxlength="100" size="100" type="text" class="text" value="{$information.title|escape}" style="width:60%;ime-mode:active;"></td>
        </tr>
        <tr>
            <th>内容</th>
            <td>
                <textarea name="info_message" cols=80 rows=5 >{$information.message|escape}</textarea>
            </td>
        </tr>



        <tr>
            <th>対象者</th>
            <td>
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <th><input type="radio" name="target_select" id="ts1" value="1" {$array_check[1]}></th>
                <td navititle="受講者全員" navi="すべての受講者が閲覧できます。"
                ><label for="ts1"><nobr>受講者全員</nobr></label></td>
                <th><input type="radio" name="target_select" id="ts2" value="2" {$array_check[2]}></th>
                <td navititle="集合研修出席者" navi="集合研修へ出席した受講者が閲覧できます。"
                ><label for="ts2"><nobr>集合研修出席者</nobr></label></td>
                <th><input type="radio" name="target_select" id="ts3" value="3" {$array_check[3]}></th>
                <td navititle="集合研修欠席者" navi="集合研修へ出席していない受講者が閲覧できます。"
                ><label for="ts3"><nobr>集合研修欠席者</nobr></label></td>
                <th><input type="radio" name="target_select" id="ts4" value="4" {$array_check[4]}></th>
                <td navititle="オンライン受講者" navi="テスト実施中／テスト合格者が閲覧できます。"
                ><label for="ts4"><nobr>オンライン受講者</nobr></label></td>
                <th><input type="radio" name="target_select" id="ts5" value="0" {$array_check[0]}></th>
                <td navititle="未受講者" navi="「受講済」でない受講者が閲覧できます。"
                ><label for="ts5"><nobr>未受講者</nobr></label></td>
                </tr></table>
            </td>
        </tr>
        {if $information.id}
        <tr>
            <th>再通知</th>
            <td>
            <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <th><input type="checkbox" name="is_notif_again" id="is_notif_again" /><th>
                <td><label for="is_notif_again" class="important_label">再通知する（既読ユーザの既読済み状態を解除）</label></td>
            </tr></table>
            </td>
        </tr>
        {/if}
    </table>


    <div style="padding:10px 0; text-align:right">
        {if $event eq "ev_open_create"}
        <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" onclick="exec_reg(this, 'ev_exec_create')" style="width:50px">作成</button>
        {else}
        <button type="button" class="btn-main{if not $edit_auth} btn-off{/if}" onclick="exec_reg(this, 'ev_exec_edit')" style="width:50px">更新</button>
        {/if}
    </div>


</form>
</div>
{/if}


</body>
</html>
