<? require_once("view/header.tpl"); ?>
    <title><?=GG_TITLEBAR_TITLE?>アンケート結果</title>
</head>
<body onresize="resized()">
<!-- ///study_questionnaire_result  -->




<script type="text/javascript">
    var keys = [<?=implode(",", array_keys($total))?>];

    function link_impression(item_id){
        $('#chk_all_impression').attr("checked", false);

        for (var idx = 0; idx < keys.length; idx++) $('#div_' + keys[idx]).toggle(false);
        $('#div_' + item_id).toggle(true);

        var index = $("#div_" + item_id).index();
        var p = $(".position").eq(index).offset().top;
        $('html,body').animate({ scrollTop: p }, 'fast');
        return false;
    }

    function all_impression(checked){
        for (var idx = 0; idx < keys.length; idx++) $('#div_' + keys[idx]).toggle(checked);
    }
</script>



<div id="title_area"><table cellspacing="0" cellpadding="0"><tr><th><nobr>アンケート結果</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="<?=h($contents["title"])?>" /></td><td><a onclick="window.close();"></a></td></tr></table></div>
<div id="title_margin">&nbsp;</div>


<div style="padding:5px">
    <span style="color:#de5836">開始日時</span>&nbsp;<?=$start_date?><span style="padding-left:30px">
    <span style="color:#de5836">終了日時</span>&nbsp;<?=$end_date?></span>
</div>


<div class="usual_box" style="padding-top:10px">
<form name="sub_main" action="" method="post" target="_self">
    <input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
   <!-- <input type="hidden" name="btn_event" value="be_questionnaire_excel" /> -->
    <input type="hidden" name="contents_id" value="<?=$contents_id?>" />

       <div style="padding:5px" class="findbox">
        <table cellspacing="0" cellpadding="0" class="rgroup">
            <tr>
                <th>職種:</th>
                <td colspan="1">
                    <select name="emp_job" style="width:160px">
                    <option value="">全て</option>
                    <?foreach ($job_list as $job_item) {?>
                        <? $selected = ($job_item["job_id"]==$emp_job?" selected":"")?>
                        <option value="<?=$job_item["job_id"]?>"<?=$selected?>><?=h($job_item["job_nm"])?></option>

                    <?}?>
                    </select>
                </td>
                <td style="text-align:right; padding-bottom:4px; padding-right:5px">
                <button type="submit" class="btn-main" id="search_student" onclick="document.form_sub.submit();">検索</button></span>
                </td>                
            </tr>
        </table>
    </div>

    <div style="text-align:right">
        <button type="submit" class="btn-main" name="btn_event" value="be_questionnaire_csv"　 onclick="document.sub_main.submit()">アンケートCSV出力</button>
        <button type="submit" class="btn-main"  name="btn_event" value="be_questionnaire_excel" onclick="document.sub_main.submit()">アンケート結果出力</button>
    </div>

                    
                    
    <table id="emp_list_header" cellspacing="0" cellpadding="0" class="list">
        <tr>
            <th width="350">設問</th>
            <th>回答結果</th>
            <th width="50">回答数</th>
            <th width="50">割合</th>
        </tr>

        <? foreach ($total as $key => $item) { ?>
            <?if ($item["type"]=="3"){ ?>
                <tr>
                    <th class="pale">
                           <?
                           $question_br = str_replace("\n", "<br>",h($item["question"]));
                           $question_convert = str_replace(" ", "&nbsp;",$question_br);
                           echo $question_convert;
                           ?>
                    
                    </th>
                    <td>
                        <? if (count($item["text_answers"])>0) { ?>
                        <a onclick="return link_impression(<?=$key?>)" href="javascript:void(0)">回答状況一覧</a>
                        <? } ?>
                    </td>
                    <td class="center"><?=count($item["text_answers"])?></td>
                    <td class="right"><?=$item["text_answers_ratio"]?>%</td>
                </tr>
            <? } else { ?>
                <? $cnt = 0; ?>
                <? foreach ($item["choices"] as $item_sub) { ?>
                    <? $cnt++; ?>
                    <tr>
                        <? if ($cnt==1) { ?>
                        <th class="pale" rowspan="<?=count($item["choices"])?>">
                           <?
                           $question_br = str_replace("\n", "<br>",h($item["question"]));
                           $question_convert = str_replace(" ", "&nbsp;",$question_br);
                           echo $question_convert;
                           ?></th>
                        <? } ?>
                        <td><?=h($item_sub["choice_string"])?></td>
                        <td class="center"><?=$item_sub["choice_count"]?></td>
                        <td class="right"><?=$item_sub["choice_ratio"]?>%</td>
                    </tr>
                <? } ?>
            <? } ?>
        <? } ?>
    </table>


    <? if ($type3_answer_exist) { ?>
    <div style="text-align: left; margin-left: 2px; margin-top: 10px; margin-bottom: 2px;">
        <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
        <th><input type="checkbox" onclick="all_impression(this.checked)" id="chk_all_impression" /></th>
        <td><label for="chk_all_impression">回答状況全表示</label></td>
        </tr></table>
    </div>


    <table id="emp_list_header" cellspacing="0" cellpadding="0" class="list">
        <tr>
            <th width="180">設問</th>
            <th width="360">感想</th>
            <th width="120">職員ID</th>
            <th>職員氏名</th>
        </tr>
    </table>
    <div style="height:350px; overflow-y:auto;">
        <? foreach ($total as $key => $item) { ?>
            <? if ($item["type"]!="3" || !count($item["text_answers"])) continue; ?>
            <div id="div_<?=$key?>" style="display:none" class="position">
                <table cellspacing="0" cellpadding="0" class="list">
                    <? $cnt = 0; ?>
                    <? foreach ($item["text_answers"] as $item_sub) { ?>
                        <? $cnt++; ?>
                    <tr>
                        <? if ($cnt==1) { ?>
                            <td rowspan="<?=count($item["text_answers"])?>" width="180"><?=h($item["question"])?></td>
                        <? } ?>
                            <td width="360"><?=nl2br(h($item_sub["impression"]))?></td>
                            <td width="120" class="center"><?=h($item_sub["emp_personal_id"])?></td>
                            <td class="center"><?=h($item_sub["emp_name"])?></td>
                    </tr>
                    <? } ?>
                </table>
            </div>
        <? } ?>
    </div>
    <? } ?>


</form>
</div>
</body>
</html>
