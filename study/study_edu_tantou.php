<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 教育担当者関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyFolderModel.php';

default_view();
die;



//**************************************************************************************************
// 教育担当者管理
//**************************************************************************************************
function default_view() {

    $btn_event = $_REQUEST["btn_event"];
    $emp_id_comma_list = $_REQUEST["emp_id_comma_list"];

    $url_strings = array();
    $url_keys = array(
        "session", "target_folder_id", "target_branch_id", "target_emp_id", "folder_selected_ids"
        ,"class_id","atrb_id","dept_id","room_id","emp_personal_id","emp_name","per_page_size","page_num"
    );

    foreach ($url_keys as $key) {
        $url_strings[] = $key."=".urlencode($_REQUEST[$key]);
    }

    //==========================================================================
    // 所属選択ダイアログを開く
    //==========================================================================
    if ($btn_event=="be_show_edu_tantou_branch" || $btn_event=="be_exec_edu_tantou_branch") {
        $result = "";
        $emp_id = $_REQUEST["target_emp_id"];

        //====================================
        // 教育担当者の所属を追加
        //====================================
        if ($btn_event=="be_exec_edu_tantou_branch") {
            $emp_id = $_REQUEST["target_emp_id"];
            StudyCommonModel::add_edu_tantou_branch($emp_id, $_REQUEST["emp_class"], $_REQUEST["emp_attr"], $_REQUEST["emp_dept"], $_REQUEST["emp_room"]);
            $result = "success";
        }

        require_once 'conf/smarty_start.conf';
        $smarty->assign("class_list", StudyCommonModel::get_class_list(1));
        $smarty->assign("attr_list", StudyCommonModel::get_attr_list(0,1));
        $smarty->assign("dept_list", StudyCommonModel::get_dept_list(0,1));
        $smarty->assign("room_list", StudyCommonModel::get_room_list(0,1));
        $smarty->assign("result", $result);
        $smarty->assign("target_emp_id", $emp_id);
        $smarty->assign("target_branch_id", $branch_id);
        $smarty->display("study_edu_tantou_branch.tpl");
        die;
    }

    //==========================================================================
    // 教育担当者の講座を削除
    //==========================================================================
    if ($btn_event=="be_del_employee_branch") {
        list ($class_id, $atrb_id, $dept_id, $room_id) = explode(",", $_REQUEST["target_branch_id"]);
        $emp_id = $_REQUEST["target_emp_id"];
        StudyCommonModel::delete_edu_tantou_branch($emp_id, $class_id, $atrb_id, $dept_id, $room_id);
        header("Location:study_edu_tantou.php?".implode("&",$url_strings));
        die;
    }

    //==========================================================================
    // 教育担当者を削除
    //==========================================================================
    if ($btn_event=="be_del_employee" && $emp_id_comma_list) {
        StudyCommonModel::delete_edu_tantou_folders_branches(explode(",", $emp_id_comma_list));
        header("Location:study_edu_tantou.php?".implode("&",$url_strings));
        die;
    }

    //==========================================================================
    // 教育担当者を追加
    //==========================================================================
    if ($btn_event=="be_add_employee" && $emp_id_comma_list) {
        StudyCommonModel::add_edu_tantou_folders_branches(explode(",", $emp_id_comma_list));
        header("Location:study_edu_tantou.php?".implode("&",$url_strings));
        die;
    }

    //==========================================================================
    // 教育担当者に講座を追加
    //==========================================================================
    if ($btn_event=="be_add_employee_folder") {
        $folder_id = $_REQUEST["target_folder_id"];
        $emp_id = $_REQUEST["target_emp_id"];
        StudyCommonModel::add_edu_tantou_folder($emp_id, $folder_id);
        header("Location:study_edu_tantou.php?".implode("&",$url_strings));
        die;
    }

    //==========================================================================
    // 教育担当者の講座を削除
    //==========================================================================
    if ($btn_event=="be_del_employee_folder") {
        $folder_id = $_REQUEST["target_folder_id"];
        $emp_id = $_REQUEST["target_emp_id"];
        StudyCommonModel::del_edu_tantou_folder($emp_id, $folder_id);
        header("Location:study_edu_tantou.php?".implode("&",$url_strings));
        die;
    }

    //------------------------------------------------------
    // ページング
    //------------------------------------------------------

    //------------------------------------------------------
    // DBから一覧取得
    //------------------------------------------------------
    $folder_selected_ids = explode("\t", $_REQUEST["folder_selected_ids"]);
    $parent_folder_id = ((int)$folder_selected_ids[count($folder_selected_ids)-1]);
    $folder_list = dbFind("select id, name, parent_id from study_folder where delete_flg = false"); // 表示講座名取得用の一覧
    $parent_ids = StudyCommonModel::get_folder_parents("id", $parent_folder_id, $folder_list);

    $emp_raw_list = StudyCommonModel::get_emp_tantou_folders_branches_list($_REQUEST, $parent_ids, $folder_list);

    $folder_selected_ids = implode("\t", (array)$parent_ids);

    //------------------------------------------------------
    // ページング
    // 面倒なのと、膨大に件数があるわけではない、ということで、全件検索した結果から出力分だけ切り出す
    //------------------------------------------------------
    $per_page_size = (int)@$_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;
    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;

    $total_count = count($emp_raw_list);
    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;

    $emp_list = array();
    if ($per_page_size < 0) {
        $emp_list = $emp_raw_list;
    } else {
        $idx = 0;
        $start_num = $per_page_size * ($page_num-1) + 1;
        $end_num = $start_num + $per_page_size - 1;
        foreach ($emp_raw_list as $row) {
            $idx++;
            if ($idx<$start_num || $idx>$end_num) continue;
            $emp_list[] = $row;
        }
    }



    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';

    //------------------------------------------------------
    // コンボボックス選択肢
    //------------------------------------------------------
    //$smarty->assign("class_list", StudyCommonModel::get_class_list(1));
    //$smarty->assign("attr_list", StudyCommonModel::get_attr_list(1));
    //$smarty->assign("dept_list", StudyCommonModel::get_dept_list(1));
    //$smarty->assign("room_list", StudyCommonModel::get_room_list(1));
    $smarty->assign("folder_list", StudyFolderModel::get_folder_list("", "", 0));

    // ページング
    $smarty->assign("page_num", $page_num);
    $smarty->assign("per_page_size", $per_page_size);
    $smarty->assign("total_count", $total_count);
    $smarty->assign("last_page", $last_page);


    $smarty->assign("folder_selected_ids", $folder_selected_ids);
    $smarty->assign("name", $_REQUEST["emp_name"]);
    $smarty->assign("folder_name", $_REQUEST["folder_name"]);
    $smarty->assign("class_id", $_REQUEST["class_id"]);
    $smarty->assign("atrb_id", $_REQUEST["atrb_id"]);
    $smarty->assign("dept_id", $_REQUEST["dept_id"]);
    $smarty->assign("room_id", $_REQUEST["room_id"]);
    $smarty->assign("emp_personal_id", $_REQUEST["emp_personal_id"]);

    $smarty->assign("emp_list_count", count($emp_list));
    $smarty->assign("emp_list", $emp_list);
    $smarty->display("study_edu_tantou.tpl");
}
?>
