<?php
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【共通】DBアクセスクラス
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once("./Cmx.php");
require_once('./libs/MDB2.php');

/**
 * MDBラッパークラス.
 * Controllerで生成され、モデルで使用される
 */
class StudyMDBWrapper {

    // コネクション
    var $connection;

    // ロガー
    var $logger;

    // 接続状態
    var $isConnect = false;

    var $spTypes = array();
    var $spParams = array();
    var $isRemainParams = 0;

    /**
     * コンストラクタ
     */
    function StudyMDBWrapper() {
        $this->__construct();
    }
    function __construct() {
        // Logインスタンス
        $this->logger = new common_log_class(basename(__FILE__));
        // コネクション取得
        $this->connection =& MDB2::connect(CMX_DB_DSN);
        if (PEAR::isError($con)) {
            $emsg = $this->get_backtrace($sql)."DB接続エラー： ".$con->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        } else {
            $this->isConnect = true;
        }
    }

    /**
     * コネクションのファクトリ
     */
    function create() {
        return new StudyMDBWrapper();
    }

    /**
     * トランザクションを開始する.
     * @return 開始に成功した場合はtrueを返す.
     */
    function beginTransaction() {

        $res = $this->connection->beginTransaction();
        // エラー処理
        if(PEAR::isError($res)) {
            $emsg = $this->get_backtrace($sql)."トランザクション開始エラー： ".$res->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        }
        return true;
    }

    /**
     * トランザクションを終了する.
     * @return 終了に成功した場合はtrueを返す.
     */
    function endTransaction() {

        $res = $this->connection->commit();
        // エラー処理
        if(PEAR::isError($res)) {
            $emsg = $this->get_backtrace($sql)."トランザクションコミットエラー： ".$res->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        }

        return true;
    }

    /**
     * コネクションを取得する.
     */
    function get() {
        return $this->connection;
    }

    /**
     * 検索クエリ.
     * @param $sql SQL
     * @param $types bind型
     * @param $param bind値(key,value)
     * @return true or false
     */
    function find($sql, $types = array(), $param = array(), $fetchMode = MDB2_FETCHMODE_ASSOC) {

        $this->queryDebug($sql, array_keys($param), array_values($param), $types);

        // Prepare
        $con = $this->get();
        $statement = $con->prepare($sql,$types);
        if (PEAR::isError($statement)) {
            $emsg = $this->get_backtrace($sql)."mdb->prepare(find)(sql, types)に失敗しました".$statement->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg.$sql, __FILE__." ".__LINE__);
        }
        // Execute
        $query_result = $statement->execute($param);
        if (PEAR::isError($query_result)) {
            $emsg = $this->get_backtrace($sql)."mdb->execute(find)(param)に失敗しました".$query_result->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        }

        $fetch_result = $query_result->fetchAll($fetchMode);
        $statement->free();
        $query_result->free();

        if (!$this->isRemainParams) {
            $this->spTypes = array();
            $this->spParams = array();
        }
        return $fetch_result;
    }

    /**
     * 更新処理
     * @param $sql SQL
     * @param $types bind型
     * @param $param bind値(key,value)
     * @return true or false
     */
    function update($sql, $types, $param) {

        $this->queryDebug($sql, array_keys($param), array_values($param), $types);

        // Prepare
        $con = $this->get();
        $statement = $con->prepare($sql,$types);
        if (PEAR::isError($statement)) {
            $emsg = $this->get_backtrace($sql)."mdb->prepare(update)(sql, types)に失敗しました".$statement->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        }

        // Execute
        $query_result = $statement->execute($param);
        if (PEAR::isError($query_result)) {
            $emsg = $this->get_backtrace($sql)."mdb->execute(update)(param)に失敗しました".$query_result->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        }
        $statement->free();
        if (!$this->isRemainParams) {
            $this->spTypes = array();
            $this->spParams = array();
        }
        return true;
    }

    /**
     * 最新のシーケンスを取得する.
     * @param $sequence_name
     * @return false or seq_no
     */
    function getNextSequence($sequence_name) {
        $con = $this->get();
        $seq = $con->nextID($sequence_name);
        if (PEAR::isError($seq)) {
            $emsg = $this->get_backtrace($sql)."mdb->nextID('sequence_name')に失敗しました".$seq->getDebugInfo();
            $this->logger->error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        }
        return $seq;
    }

    function queryDebug($sql, $columns = NULL, $values, $types) {
        $this->logger->debug($this->get_backtrace($sql)."PREPARE QUERY:".$sql);
        for ($i = 0, $size = sizeof($columns); $i < $size;  $i++) {
            $this->logger->debug($columns[$i]."|".var_export($values[$i], TRUE)."|".$types[$i]);
            $sql = str_replace(":".$columns[$i], "".var_export($values[$i], TRUE)."", $sql);
        }
        $this->logger->debug($this->get_backtrace($sql)."EXECUTE QUERY:".$sql);
    }

    function get_backtrace($sql) {
        if (!function_exists("debug_backtrace")) return;
        $ary = debug_backtrace();
        $out = array();
        $prev = "";
        for ($idx = count($ary)-1; $idx>=0; $idx--) {
            $out[] = "【".basename($ary[$idx]["file"])."】 --- ".$prev.$ary[$idx]["line"]."行目で --- ".$ary[$idx]["function"]."() を実行";
            $prev = "  ".$ary[$idx]["function"]."() 関数";
            if ($ary[$idx]["function"]=="dbExec") break;
        }
        return implode("<br>", $out)."<br><br>" . ($sql ? "【SQL】<br>".dbSqlDump($sql)."<br><br>" : "");
    }
}

