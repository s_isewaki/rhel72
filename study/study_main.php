<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【管理側】研修・講義管理 関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyFolderModel.php';
require_once 'model/StudyContentsModel.php';
require_once 'model/StudyNotificationModel.php';
require_once 'model/StudyAssemblyModel.php';

$event = $_REQUEST["event"];

if ($event=="ev_tree_click") display_contents_and_die();
if ($event=="ev_contents_search") show_and_exec_keysearch_and_die();
if ($event=="ev_get_tanto_list") get_tanto_list_and_die();
if ($event=="export_csv") export_csv_die();
display_menu(); // メニューを表示
die;


//**************************************************************************************************
//受講履歴のCSV出力（20151101 Y.yamamoto） for NK
//**************************************************************************************************
function export_csv_die(){
    $sort_string = "title-asc"; // ソート文字列 title-asc,create_date-desc など
    $ext_joken = @$_REQUEST["ext_joken"]; // pub_only or misyu_only
    $selected_folder_id = $_REQUEST["selected_folder_id"]; // 選択中の講座
    // コンテンツを取得
    $contents_full_list = StudyContentsModel::get_contents_list_under_folder($selected_folder_id, $sort_string, $ext_joken);
   
    //if(count($contents_full_list) == 0){
       // return;
    //}
    
    ob_clean();

    $file_name = "emplist_" . date("Ymd") . ".csv";
    header("Content-Disposition: attachment; filename=$file_name");
    header("Content-Type: application/octet-stream; name=$file_name");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");

    $title = "職員ID,氏名,";
    foreach ($contents_full_list as $row) {
        $titles[] = $row["title"]; 
        //$title .=  mb_substr($date,0,4)."/".mb_substr($date,4,2)."/".mb_substr($date,6,2).",";
        $title .=  $row["title"].",";
    }
  
    $title .="\r\n";
    $csv_header = mb_convert_encoding($title, "SJIS-win", 'eucJP-win');
    echo($csv_header);
    //ここまでヘッダ
     
    foreach ($contents_full_list as $row) {
        $id[] = $row["id"];           
    }
    
    $emp_id_list = StudyCommonModel::get_student_list_for_couse($id); 
  
    foreach ($emp_id_list as $emp_list_tmp) {
        $emp_id = $emp_list_tmp["emp_id"];
        $emp_personal_id = $emp_list_tmp["emp_personal_id"];
        $first_name = $emp_list_tmp["first_name"];
        $last_name = $emp_list_tmp["last_name"];
 
        $history_list = StudyCommonModel::get_student_couse_history("$emp_id",$id);
        $output_dates =$emp_personal_id.",";
        $output_dates .= $last_name." ".$first_name.",";
     
        for($i=0;$i<count($titles);$i++){
            $date_history_flg = 0;
            foreach($history_list as $history_list_tmp){
                $emp_title = $history_list_tmp[title];
                if($emp_title==$titles[$i]){
                    $actual_status = $history_list_tmp["assembly_actual_status"];
                    $status = $history_list_tmp["assembly_status"];
                    $contents_id =  $history_list_tmp["test_id"];
                    if($actual_status!=1){
                        $asm_list = StudyAssemblyModel::get_assembly_list($contents_id); // 開催日一覧
                        $asm_date_array = end($asm_list);
                        if($status!=2){
                            $output_dates .= "×";
                        }else if($asm_date_array['date']<$history_list_tmp['assembly_status_date']){
                         $output_dates .= "▲";
                        }else{
                         $output_dates .= "△";
                        }
                        
                    }
                }else{                       
                    $date_history_flg=1;                    
                }
            }
            if($date_history_flg=1){
                $output_dates .= ",";
            }
       
        }
        $output_dates .="\r\n";
            
        $csv_data = mb_convert_encoding($output_dates , "SJIS-win", 'eucJP-win');
        echo($csv_data);
    } 
    die;
}


//**************************************************************************************************
// 管理メニュー画面処理（ツリー＆一覧画面）
//**************************************************************************************************
function display_menu() {
    $folder_auth_strict = 0;
    if (!GG_STUDY_ADMIN_FLG) $folder_auth_strict = 1;
    require_once 'conf/smarty_start.conf';
    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_main.tpl@all", "", "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);
    $smarty->assign("folder_list", StudyFolderModel::get_folder_list("","",$folder_auth_strict)); // 講座ツリーリスト。権限によりリスト構成が変わる
    $smarty->assign("selected_folder_id", $_REQUEST["selected_folder_id"]); // 再表示の場合はsubmit前の講座を選択
    $smarty->assign("notif_list_count",  count(StudyNotificationModel::get_user_notif_list("")));// 未読の、お知らせの数
    $smarty->display("study_main.tpl");
}






//**************************************************************************************************
// コンテンツ一覧を表示する処理。右下一覧部のみ。AJAXで呼ばれる
//**************************************************************************************************
function display_contents_and_die() {
    $sort_string = @$_REQUEST["sort_string"]; // ソート文字列 title-asc,create_date-desc など
    $ext_joken = @$_REQUEST["ext_joken"]; // pub_only or misyu_only
    $selected_folder_id = $_REQUEST["selected_folder_id"]; // 選択中の講座

    // コンテンツを取得
    $contents_full_list = StudyContentsModel::get_contents_list_under_folder($selected_folder_id, $sort_string, $ext_joken);

    //------------------------------------------------------
    // ページング
    // 面倒なのと、膨大に件数があるわけではない、ということで、全件検索した結果から出力分だけ切り出す
    //------------------------------------------------------
    $per_page_size = $_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;

    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;

    $total_count = count($contents_full_list);
    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;

    $contents_list = array();
    if ($per_page_size < 0) {
        $contents_list = $contents_full_list;
    } else {
        $idx = 0;
        $start_num = $per_page_size * ($page_num-1) + 1;
        $end_num = $start_num + $per_page_size - 1;
        foreach ($contents_full_list as $row) {
            $idx++;
            if ($idx<$start_num || $idx>$end_num) continue;
            $contents_list[] = $row;
        }
    }

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_main_right_part.tpl@all", $selected_folder_id, "");
    $disp_auth = $auth_matrix["disp_auth"];
    if(count($contents_list) == 0){
     $edit_auth = "";      
    }else{
        $edit_auth = $auth_matrix["edit_auth"];
    }
    //$edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $folder_auth_strict = 0;
    if (!GG_STUDY_ADMIN_FLG) $folder_auth_strict = 1;
    $template_vars = array(
        "right_part_call_from" => $_REQUEST["right_part_call_from"],
        "page_num" => (int)$page_num,
        "disp_auth" => $disp_auth,
        "edit_auth" => $edit_auth,
        "per_page_size" => (int)$per_page_size,
        "total_count" => (int)$total_count,
        "last_page" => (int)$last_page,
        "sort_string" => ((@$sort_string == "") ? "create_date-desc" : $sort_string),
        "ext_joken" => $ext_joken,
        "folder_list" => StudyFolderModel::get_folder_list($selected_folder_id, $sort_string, $folder_auth_strict), // 講座ツリー。権限の有る親講座のみ指定される
        "contents_list" => $contents_list
    );
    display_template("study_main_right_part.tpl", $template_vars);
    die;
}






//**************************************************************************************************
// 講座内検索
//
// 権限によっては講座を非表示にする必要があるが、SQL-SELECT時点では不明
// 一覧表示時点では、権限より上階層名も出す。
//**************************************************************************************************
function show_and_exec_keysearch_and_die() {
    require_once 'conf/smarty_start.conf';
    // 検索ボタン
    $contents_list = array();
    if ($_REQUEST["btn_event"] === "be_search") {
        $contents_full_list = StudyContentsModel::keyword_search($_REQUEST["keyword"], $_REQUEST["bunrui_year"]); // 研修・講義一覧取得
        $folder_list = StudyFolderModel::get_folder_list("","",1); // 講座一覧。権限制限あり

        foreach($contents_full_list as $key => $row) {
            $folder_parent_id_list = StudyCommonModel::get_folder_parents("id", $row["folder_id"], $folder_list);
            if (@$folder_parent_id_list[0] == 99999) continue; // 権限外につきスルー（教育担当者かどうかは確認不要。教育担当者でなければ99999は存在しない）
            if (@$folder_parent_id_list[0] == -1) continue; // 間違って親が削除されている。スルー
            $contents_list[$key] = $row;
            $contents_list[$key]["folder_joint_name"] = implode("＞", StudyCommonModel::get_folder_parents("name", $row["folder_id"], $folder_list));
        }
    }
    $smarty->assign("contents_list", $contents_list);
    $smarty->assign("btn_event", $_REQUEST["btn_event"]);
    $smarty->assign("keyword", $_REQUEST["keyword"]);
    $smarty->assign("bunrui_year", $_REQUEST["bunrui_year"]);
    $smarty->display("study_main_keysearch.tpl");
    die;
}






//**************************************************************************************************
// 講座内検索
//
// 権限によっては講座を非表示にする必要があるが、SQL-SELECT時点では不明
// 一覧表示時点では、権限より上階層名も出す。
//**************************************************************************************************
function get_tanto_list_and_die() {
    $contents_list = array();
    $selected_folder_id = $_REQUEST["selected_folder_id"]; // 選択中の講座
    $emp_list = StudyCommonModel::get_emp_tantou_folders_list($selected_folder_id);
    $html = array();
    if (count($emp_list)) {
        $html[] = '<table cellspacing="" cellpadding="" class="list" style="width:100%">';
        $html[] = '<tr><th><nobr>担当者職員ID</nobr></th><th><nobr>担当者職員氏名</nobr></th></tr>';
        foreach ($emp_list as $row) {
            $html[] = '<tr><td style="text-align:center"><span style="font-size:12px">'.h($row["emp_personal_id"]).'</td>';
            $html[]= '<td><a href="javascript:void(0)" onclick="del_edu_tantou_folder(\''.$row["emp_id"].'\')"><nobr>'.h($row["name"]).'</nobr></a></td></tr>';
        }
        $html[] = "</table>";
        $html[] = '<div style="padding:5px 0; text-align:right"><button type="button" class="btn-main" onclick="location.href=\'study_edu_tantou.php?session='.GG_SESSION_ID.'&folder_selected_ids='.$selected_folder_id.'\';">教育担当者一覧へ</button></div>';
    }
    else {
        $html[] = '<div style="background-color:#f5f5f5; color:#888888; padding:10px">担当者は存在しません</div>';
    }
    echo implode("\n", $html);
    die;
}


?>
