<?
$event = $_REQUEST["event"];
if ($event=="ev_exec_assembly_actual_entry" && $_REQUEST["login_user_id"]) {
    define("ASSEMBLY_ACTUAL_ENTRY_STANDALONE_AND_IGNORE_SESSION_MODE", 1);
}


//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 集合研修関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyAssemblyModel.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyContentsModel.php';


if ($event=="ev_show_assembly_kaisaibi" || $event=="ev_exec_create_date") showexec_assembly_kaisaibi_and_die($event);
if ($event=="ev_show_assembly_user" || $event=="ev_exec_assembly_user") showexec_assembly_user_and_die($event);
if ($event=="ev_show_assembly_import") showexec_assembly_import_and_die();
if ($event=="ev_show_assembly_date") showexec_assembly_date_and_die();
if ($event=="ev_show_assembly_actual_entry") show_assembly_actual_entry_and_die($event);
if ($event=="ev_exec_assembly_actual_entry") exec_assembly_actual_entry_and_die($event);



default_view($event);
die;







//**************************************************************************************************
// 管理側 集合研修画面処理
//**************************************************************************************************
function default_view($event) {
    $contents_id = $_REQUEST["contents_id"];

    //==========================================================================
    // 開催日を削除する場合
    //==========================================================================
    if ($event=="ev_exec_delete_assembly") {
        StudyAssemblyModel::delete_assembly($contents_id, $_REQUEST["dates"]);
        header("Location:study_assembly.php?result=success&session=".GG_SESSION_ID."&contents_id=".$contents_id);
        die;
    }

    //==========================================================================
    // 申込期間を変更する場合(画面上部のみ更新)
    //==========================================================================
    if ($event=="ev_exec_update_deadline") {
        StudyAssemblyModel::updata_assembly_start_end_date($contents_id, $_REQUEST["start_date"], $_REQUEST["end_date"], $_REQUEST["notes"]);
        header("Location:study_assembly.php?result=success&session=".GG_SESSION_ID."&contents_id=".$contents_id);
        die;
    }

    //------------------------------------------------------
    // 研修・講義情報を取得
    //------------------------------------------------------
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $array_start = conv_ymdhm_to_jp_and_time($contents["assembly_start_date"]);
    $array_end = conv_ymdhm_to_jp_and_time($contents["assembly_end_date"]);

    //------------------------------------------------------
    // 出欠予定・未登録人数を取得
    //------------------------------------------------------
    $yotei_counts = StudyAssemblyModel::get_assembly_application_counts($contents_id);

    //------------------------------------------------------
    // 開催日情報一覧
    //------------------------------------------------------
    $asm_list = StudyAssemblyModel::get_assembly_list($contents_id); // 開催日一覧取得
    $arr_week = array("日","月","火","水","木","金","土");
    foreach ($asm_list as $key=>$item){
        //開催日
        $tmp_date = date("Y年m月d日",strtotime($item["date"]));
        $w = date("w", strtotime($item["date"]));
        $asm_list[$key]["date"] = $tmp_date . "(" . $arr_week[$w] . ")";

        //開催時間
        $tmp_start = date("H:i",strtotime($item["start_time"]));
        $tmp_end   = date("H:i",strtotime($item["end_time"]));
        $asm_list[$key]["start_time"]    = $tmp_start;
        $asm_list[$key]["end_time"]      = $tmp_end;
        $asm_list[$key]["persons_limit"] = $item["persons_limit"]; // 定員

        //出席申込者数
        $ret = StudyAssemblyModel::get_assembly_application_counts($item["id"], $item["no"]);
        $asm_list[$key]["syusseki"]  = $ret["assembly_status1"];
        $asm_list[$key]["kesseki"]   = $ret["assembly_status2"];
        $asm_list[$key]["mitouroku"] = $ret["assembly_status0"];
    }

    $contents["assembly_actual_count"] = (int)$contents["assembly_actual_count"];

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_assembly_list.tpl@all", $contents["folder_id"], "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';
    $smarty->assign("syusseki", $yotei_counts["assembly_status1"]);
    $smarty->assign("kesseki", $yotei_counts["assembly_status2"]);
    $smarty->assign("mitouroku", $yotei_counts["assembly_status0"]);
    $smarty->assign("student_count", (int)$contents["student_count"]);
    $smarty->assign("result", $_REQUEST["result"]);
    $smarty->assign("info_count", count($asm_list));
    $smarty->assign("contents", $contents);
    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("array_start", $array_start);
    $smarty->assign("array_end", $array_end);
    $smarty->assign("notes", $contents["assembly_notes"]);
    $smarty->assign("asm_list", $asm_list);
    $smarty->display("study_assembly_list.tpl");
}


//**************************************************************************************************
// 集合研修作成ダイアログ表示と登録
//**************************************************************************************************
function show_assembly_actual_entry_and_die($event){
    $template_vars = array();
    display_template("study_assembly_actual_entry.tpl", $template_vars);
    die;
}


function exec_assembly_actual_entry_and_die($event){
    $contents_id = $_POST["contents_id"];
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $emp_personal_id = $_POST["emp_personal_id"];
    $login_user_id = $_POST["login_user_id"];
    $id_flg = $_POST["id_flg"];
    
    if($id_flg == 0){
        $row = StudyCommonModel::get_user_data_by_emp_personal_id($emp_personal_id);
    }else{
        $row = StudyCommonModel::get_user_data_by_emp_login_id($emp_personal_id);
    }
    if (!$contents_id) { echo "研修・講義が識別できません。画面を開きなおしてください。"; die; }
    if (!$contents) { echo "研修・講義に変更があったか、識別できません。画面を開きなおしてください。"; die; }
    if (!$row) { echo "IDが識別できません。確認してください。"; die; }
    $emp_id = $row["emp_id"];
    if (!$emp_id) { echo "IDが識別できません。確認してください。"; die; }

    // 受講者として存在しなければ追加
    StudyCommonModel::add_student($contents_id, array($emp_id));
    // 出席に
    StudyAssemblyModel::change_assembly_actual_status($contents_id, array($emp_id), 1, date("Ymd"));
    $emp_lt_nm = $row["emp_lt_nm"];
    $emp_ft_nm = $row["emp_ft_nm"];
    $emp_nm = "職員氏名：".$emp_lt_nm." ".$emp_ft_nm." "."さん";   
    echo "ok:$emp_nm";  // 姓名追加 20141203TN 
    die;
}





//**************************************************************************************************
// 集合研修作成ダイアログ表示と登録
//**************************************************************************************************
function showexec_assembly_kaisaibi_and_die($event){
    $contents_id = $_REQUEST["contents_id"];
    $no = $_REQUEST["no"];

    require_once 'conf/smarty_start.conf';
    $smarty->assign("result", "");

    //==========================================================================
    // 開催日を登録する場合
    //==========================================================================
    if ($event=="ev_exec_create_date") {
        $no = StudyAssemblyModel::create_assembly($contents_id, $no, $_REQUEST);
        $smarty->assign("result", "success");
    }

    //------------------------------------------------------
    // $noがあれば開催日詳細取得
    //------------------------------------------------------
    if ($no) {
        $asm = StudyAssemblyModel::get_assembly_date_record($contents_id, $no);
        $date = date("Y年m月d日",strtotime($asm["date"])); //開催日
        $start_time = array(substr($asm["start_time"], 0, 2), substr($asm["start_time"], 2, 2));
        $end_time = array(substr($asm["end_time"], 0, 2), substr($asm["end_time"], 2, 2));
        $place = $asm["place"]; // 開催場所
        $lecturer = $asm["lecturer"]; // 講師
        $persons_limit = $asm["persons_limit"]; // 定員

        $smarty->assign("date", $date);
        $smarty->assign("start_time", $start_time);
        $smarty->assign("end_time", $end_time);
        $smarty->assign("place", $place);
        $smarty->assign("lecturer", $lecturer);
        $smarty->assign("persons_limit", $persons_limit);
        $smarty->assign("mode", "更新");
    }else{
        $smarty->assign("date", "");
        $smarty->assign("start_time", array("",""));
        $smarty->assign("end_time", array("",""));
        $smarty->assign("place", "");
        $smarty->assign("lecturer", "");
        $smarty->assign("persons_limit", "");
        $smarty->assign("mode", "登録");
    }

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_assembly_kaisaibi.tpl@all", $contents["folder_id"], "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("no", $no);
    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);
    $smarty->display("study_assembly_kaisaibi.tpl");
    die;
}





//**************************************************************************************************
// 【ユーザ側】集合研修申込画面を開く・出欠申込みを登録する
//**************************************************************************************************
function showexec_assembly_user_and_die($event){
    $contents_id = $_REQUEST["contents_id"];
    $result = "";

    //==========================================================================
    // 出欠申込を登録する場合
    //==========================================================================
    if ($event=="ev_exec_assembly_user") {
        $assembly_status = "1"; // be_registerの場合
        if ($_REQUEST["btn_event"] == "be_absent") $assembly_status = "2";
        if ($_REQUEST["btn_event"] == "be_clear")  $assembly_status = "0";
        StudyAssemblyModel::set_user_assembly_application($contents_id, $_REQUEST["apply_no"], $assembly_status);
        $result = "success";
    }

    //------------------------------------------------------
    // 研修・講義情報取得
    //------------------------------------------------------
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $start_date = conv_ymdhm_to_jp_and_time2($contents["assembly_start_date"]);
    $end_date = conv_ymdhm_to_jp_and_time2($contents["assembly_end_date"]);
    $limit_date = ($start_date || $end_date ? $start_date . " 〜 " . $end_date : "");

    //------------------------------------------------------
    // 申込済情報取得
    //------------------------------------------------------
    $assembly_status = StudyAssemblyModel::get_assembly_status($contents_id); // ログイン者の申込ステータス 0or1or2
    $mosikomi_no = StudyAssemblyModel::get_mosikomi_no($contents_id); // 申込んでいる場合の番号
    $status_name = "未登録";
    if ($assembly_status==1) $status_name = "出席";
    if ($assembly_status==2) $status_name = "欠席";

    //------------------------------------------------------
    // 開催日一覧取得
    //------------------------------------------------------
    $asm_list = StudyAssemblyModel::get_assembly_list($contents_id); //開催日一覧取得
    $arr_week = array("日","月","火","水","木","金","土");
    foreach ($asm_list as $key=>$item){
        //開催日時
        $w = date("w", strtotime($item["date"]));
        $asm_list[$key]["date_jp"] = date("Y年m月d日",strtotime($item["date"])) . "(" . $arr_week[$w] . ")";
        $asm_list[$key]["time_jp"] = date("H:i",strtotime($item["start_time"])) . "〜" . date("H:i",strtotime($item["end_time"]));
        $asm_list[$key]["application_count"] = StudyAssemblyModel::get_mosikomi_emp_count($item["id"], $item["no"]);//申込者数

        // 自分が選択していない日付で、定員オーバーなら選択できない
        $asm_list[$key]["is_limit_over"] = "";
        if (strlen($item["persons_limit"]) && intval($item["persons_limit"]) <= intval($asm_list[$key]["application_count"])) {
            if ($mosikomi_no!=(int)$item["no"]) $asm_list[$key]["is_limit_over"] = "1";
        }
        $start_ymdhm = $item["date"].$item["start_time"];
        $today = date("YmdHm");
        if ($start_ymdhm < $today) $asm_list[$key]["is_limit_over"] = "1"; // 開始日時を過ぎた。受付不可。
        if (!strlen($item["persons_limit"])) $asm_list[$key]["persons_limit"] = "(なし)";
    }

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $template_vars = array(
        "result"         => $result,
        "contents_id"    => $contents_id,
        "contents_title" => $contents["title"],
        "limit_date"     => $limit_date,
        "notes"          => $contents["assembly_notes"],
        "asm_list"       => $asm_list,
        "assembly_status" => $assembly_status,
        "mosikomi_no"    => $mosikomi_no,
        "status_name"    => $status_name
    );
    display_template("study_assembly_user.tpl", $template_vars);
    die;
}







//**************************************************************************************************
// 集合研修受講実績インポート画面の表示と、画面からのCSV登録
//**************************************************************************************************
function showexec_assembly_import_and_die(){
    $contents_id = $_REQUEST["contents_id"];
    $btn_event = $_REQUEST["btn_event"];
    $assembly_application_date = $_REQUEST["assembly_application_date"]; // YYYYMMDD

    require_once 'conf/smarty_start.conf';
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("assembly_application_day", $_REQUEST["assembly_application_day"]); // 漢字
    $smarty->assign("message", "");
    $smarty->assign("result", "");
    $smarty->assign("id_type", $_REQUEST["id_type"]);

    //------------------------------------------------------
    // 開催日リンク一覧準備
    //------------------------------------------------------
    $asm_list = StudyAssemblyModel::get_assembly_list($contents_id); //開催日一覧取得
    $arr_week = array("日","月","火","水","木","金","土");
    foreach ($asm_list as $key=>$item){
        //開催日時
        $w = date("w", strtotime($item["date"]));
        $ymd_jp = date("Y年m月d日",strtotime($item["date"]));
        $link_caption = $ymd_jp . "(" . $arr_week[$w] . ")  ".date("H:i",strtotime($item["start_time"])) . "〜" . date("H:i",strtotime($item["end_time"]));
        $asm_list[$key]["link_caption"] = $link_caption."  ".h($item["place"]);
        $asm_list[$key]["ymd_jp"] = $ymd_jp;
    }
    $smarty->assign("asm_list", $asm_list);


    //==========================================================================
    // インポート実行
    //==========================================================================
    if ($btn_event == "be_exec_assembly_import"){
        //----------------------------------------
        // DBからマッチング用リストを準備
        //----------------------------------------
        $student_list = StudyCommonModel::get_student_list($contents_id, array(), 1, 0, 0); // 全受講者
        $emp_id_list = array();
        foreach ($student_list as $row) {
            $emp_id_list[] = $row["emp_id"];
        }
        //<!--20151118 順天堂医院 by Y.yamamoto--> 
        $db_user_list = StudyCommonModel::get_user_list_auth_csv(1);
        //ここまで
        foreach ($db_user_list as $key => $value){
            $key_class[$key] = $value['class_nm'];
            $key_atrb[$key] = $value['atrb_nm'];
            $key_dept[$key] = $value['dept_nm'];
            $key_room[$key] = $value['room_nm'];
            $key_last[$key] = $value['last_name'];
            $key_first[$key] = $value['first_name'];
        }
        //array_multisort ($key_class , SORT_ASC ,$key_atrb , SORT_ASC , $key_dept , SORT_ASC , $key_room , SORT_ASC , $key_last , SORT_ASC , $key_first , SORT_ASC,$db_user_list );

        
        
        
        $match_id_list = array();
        foreach($db_user_list as $db_item) {
            if($_REQUEST["id_type"] == "login"){
             $key =$db_item["login_id"];
            }else if($_REQUEST["id_type"] == "personal"){
             $key = $db_item["emp_personal_id"];
            }else if($_REQUEST["id_type"] == "idm"){
             $key = $db_item["emp_idm"];
            }
//            $key = ($_REQUEST["id_type"] == "login") ? $db_item["login_id"] : $db_item["emp_personal_id"];
            if (!in_array($db_item["emp_id"], $emp_id_list)) continue;
            $match_id_list[$key] = $db_item["emp_id"];
        }
        //----------------------------------------
        // CSVファイルから職員IDの配列を取得、行ループ
        //----------------------------------------
        $readLines = explode("\n",mb_convert_encoding(file_get_contents($_FILES['csv']['tmp_name']), "eucJP-win", "sjis-win"));
        $emp_ids = array();
        $reg_list = array();
        $error_line = array();
        $error_count = 0;
        foreach ($readLines as $line) {
            if ($line=="職員ID,氏名,所属") continue;
            if ($line=="ログインID,氏名,所属") continue;
            //--------------------------
            // 行をカンマ分割、先頭要素を取得
            //--------------------------
            $line_array = explode(",", $line);
            $id = trim($line_array[0]);
            if (!strlen($id)) continue;
            //--------------------------
            // マッチングリストに無い。エラー
            //--------------------------
            if (@$match_id_list[$id]=="") {
                $error_count++;
                if ($error_count <= 5) {
                    if (strlen($id)>30) $id = substr($id,0,30)." …";
                    $error_line[]= $id;
                }
            }
            //--------------------------
            // マッチングした。リストに追加
            //--------------------------
            else {
                $emp_ids[] = $match_id_list[$id];
            }
        }
        //----------------------------------------
        // エラーがあった場合
        //----------------------------------------
        if (count($error_line) > 0) {
            //$id_caption = ($_REQUEST["id_type"] == "login") ? "ログインID" : "職員ID";
            if($_REQUEST["id_type"] == "login"){
             $id_caption ="ログインID";
            }else if($_REQUEST["id_type"] == "personal"){
             $id_caption = "職員ID";
            }else if($_REQUEST["id_type"] == "idm"){
             $id_caption = "ICカード(IDm)";
            }
            $errors = array();
            $errors[]= "入力されたデータに誤りがあります。";
            $errors[]= "以下の" . $id_caption . "に該当する職員は、受講者として登録されていません。";
            foreach($error_line as $line) $errors[] = "・".preg_replace('/[\r\n\"]*/', "", $line);
            if ($error_count > 5) $errors[]= "（合計：".$error_count."件）";

            $smarty->assign("message", implode("\\n",$errors));
            $smarty->assign("result", "error");
        }
        else if (!count($emp_ids)) {
            $smarty->assign("message", "登録データはありませんでした。");
            $smarty->assign("result", "error");
        }
        //==========================================================================
        // 登録する
        //==========================================================================
        else {
            dbBeginTransaction();
            StudyAssemblyModel::change_assembly_actual_status($contents_id, $emp_ids, 1, $assembly_application_date); // 出席に
            dbEndTransaction();
            $smarty->assign("result", "success");
        }
    }
    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $smarty->display("study_assembly_import.tpl");
    die;
}







//**************************************************************************************************
// 集合研修 受講実績の登録画面。 受講者管理画面の下部「出席」ボタンで開かれる画面の表示と、そこからの登録処理
//**************************************************************************************************
function showexec_assembly_date_and_die(){
    require_once 'conf/smarty_start.conf';
    $smarty->assign("contents_id", $_REQUEST["contents_id"]);
    $smarty->assign("assembly_application_date", $_REQUEST["assembly_application_date"]);
    $smarty->assign("result", "");

    //------------------------------------------------------
    // 開催日リンク一覧準備
    //------------------------------------------------------
    $asm_list = StudyAssemblyModel::get_assembly_list($_REQUEST["contents_id"]); //開催日一覧取得
    $arr_week = array("日","月","火","水","木","金","土");
    foreach ($asm_list as $key=>$item){
        //開催日時
        $w = date("w", strtotime($item["date"]));
        $ymd_jp = date("Y年m月d日",strtotime($item["date"]));
        $link_caption = $ymd_jp . "(" . $arr_week[$w] . ")  ".date("H:i",strtotime($item["start_time"])) . "〜" . date("H:i",strtotime($item["end_time"]));
        $asm_list[$key]["link_caption"] = $link_caption."  ".h($item["place"]);
        $asm_list[$key]["ymd_jp"] = $ymd_jp;
    }
    $smarty->assign("asm_list", $asm_list);

    //==========================================================================
    // 登録実行
    //==========================================================================
    if ($_REQUEST["btn_event"] == "be_exec_assembly_date") {
        $emp_ids = explode(",", $_REQUEST["emp_id_comma_list"]);
        dbBeginTransaction();
        StudyAssemblyModel::change_assembly_actual_status($_REQUEST["contents_id"], $emp_ids, 1, $_REQUEST["assembly_application_date"]); // 出席に
        dbEndTransaction();
        $smarty->assign("result", "success");
    }
    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $smarty->display("study_assembly_date.tpl");
    die;
}
?>
