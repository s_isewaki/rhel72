<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【ユーザ側】職員検索関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyQuestionnaireModel.php';
require_once 'model/StudyNotificationModel.php';
//require (dirname(__FILE__)."/../conf/conf.inf");

$btn_event = $_REQUEST["btn_event"];





if ($btn_event=="export_list") export_csv_and_die();

default_view($event);
die;



//**************************************************************************************************
// 管理画面 職員検索タブクリック時に表示される画面を表示
//**************************************************************************************************
function default_view() {
    require_once 'conf/smarty_start.conf';

    //------------------------------------------------------
    // ページング
    //------------------------------------------------------
    $per_page_size = (int)@$_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;
    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;


    //------------------------------------------------------
    // 一覧検索/取得
    //------------------------------------------------------
    $emp_list = array();
    $total_count = 0;
    $joken = array(
        "class"  => $_REQUEST["class"],     // 部
        "attr"   => $_REQUEST["attr"],      // 課
        "dept"   => $_REQUEST["dept"],      // 科
        "room"   => $_REQUEST["room"],      // 室
        "id"     => $_REQUEST["emp_id"], // 職員ID
        "name"   => $_REQUEST["emp_name"],  // 職員名
        "emp_expire_type"=>@$_REQUEST["emp_expire_type"] //退職利用状況 <!--20151118 順天堂医院 by Y.yamamoto--> 
    );
    $total_count = count(StudyCommonModel::get_user_list($joken));
    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;

    $joken["per_page_size"] = $per_page_size;
    $joken["page_num"] = $page_num;
    $emp_list = StudyCommonModel::get_user_list($joken);

    foreach ($emp_list as $key => $value){
        $key_class[$key] = $value['class_nm'];
        $key_atrb[$key] = $value['atrb_nm'];
        $key_dept[$key] = $value['dept_nm'];
        $key_room[$key] = $value['room_nm'];
        $key_last[$key] = $value['last_name'];
        $key_first[$key] = $value['first_name'];
    }
    //array_multisort ($key_class , SORT_ASC ,$key_atrb , SORT_ASC , $key_dept , SORT_ASC , $key_room , SORT_ASC , $key_last , SORT_ASC , $key_first , SORT_ASC,$emp_list );


    $today = date("Ymd");
    foreach ($emp_list as $key=>$item) {
        $career = StudyCommonModel::get_staff_career($item['emp_id']);

        $now = date("Y/m/j");

        $exp_start=$career[0]["exp_start"];
        $work_start = $career[0]["work_start"];
        $emp_list[$key]["exp_diff"] = "";
        $emp_list[$key]["work_diff"] = "";
        if($exp_start){
            $exp_diff = day_diff($exp_start,$now,$career[0]["exp_minus"]);
            $exp_month = $exp_diff%12;
            $exp_year = floor($exp_diff/12);
            if($exp_year<0) $exp_year = 0;
            if($exp_month<0) $exp_month = 0;
            $emp_list[$key]["exp_diff"] = $exp_year."年".$exp_month."月";
        }

        if($work_start){
            $work_diff = day_diff($work_start,$now,$career[0]["work_minus"]);
            $work_month = $work_diff%12;
            $work_year = floor($work_diff/12);
            if($work_year<0) $work_year = 0;
            if($work_month<0) $work_month = 0;
            $emp_list[$key]["work_diff"] = $work_year."年".$work_month."月";
            //$emp_list[$key]["work_diff"] = day_diff($work_start,$now,$career[0]["work_minus"]);
        }
    }


    // ページング
    $smarty->assign("page_num", $page_num);
    $smarty->assign("per_page_size", $per_page_size);
    $smarty->assign("total_count", $total_count);
    $smarty->assign("last_page", $last_page);

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_emp_search.tpl@all", "", "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);

    //------------------------------------------------------
    // コンボボックス選択肢
    //------------------------------------------------------
    $smarty->assign("class_list", StudyCommonModel::get_class_list());
    $smarty->assign("attr_list", StudyCommonModel::get_attr_list());
    $smarty->assign("dept_list", StudyCommonModel::get_dept_list());
    $smarty->assign("room_list", StudyCommonModel::get_room_list());


    //$smarty->assign("BARITESS_NENSUU_OPT_FLAG",$BARITESS_NENSUU_OPT_FLAG);

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $smarty->assign("emp_list", $emp_list);
    $smarty->assign("name", $_REQUEST["emp_name"]);
    $smarty->assign("class", $_REQUEST["class"]);
    $smarty->assign("attr", $_REQUEST["attr"]);
    $smarty->assign("dept", $_REQUEST["dept"]);
    $smarty->assign("room", $_REQUEST["room"]);
    
    // <!--20151118 順天堂医院 by Y.yamamoto--> 
    $smarty->assign("emp_expire_type",$_REQUEST["emp_expire_type"]);
    // ここまで 
    $smarty->assign("id", $_REQUEST["emp_id"]);
    $smarty->assign("notif_list_count",  count(StudyNotificationModel::get_user_notif_list("")));// 未読の、お知らせの数
    $smarty->display("study_emp_search.tpl");
}

function export_csv_and_die(){
//require (dirname(__FILE__)."/../conf/conf.inf");
    //$staff_nensuu_flg = $BARITESS_NENSUU_OPT_FLAG;

    ob_clean();

    $file_name = "staff_" . date("Ymd") . ".csv";
    header("Content-Disposition: attachment; filename=$file_name");
    header("Content-Type: application/octet-stream; name=$file_name");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");

    $title = "研修名,職員氏名,職種,役職,".GG_CLASS_NM.",".GG_ATRB_NM.",".GG_DEPT_NM;
    if(GG_CLASS_CNT == 4){
        $title .=",".GG_ROOM_NM;
    }
    if(GG_BARITESS_NENSUU_OPT_FLAG){
       $title .= ",経験年数,勤続年数,集合研修受講日,集,ア,テ"."\n";
    }else{
       $title .= ",集合研修受講日,集,ア,テ"."\n";
    }
    $csv_header = mb_convert_encoding($title, "SJIS-win", 'eucJP-win');
    echo($csv_header);
    //ここまでヘッダ
    $search_nendo = $_REQUEST["nendo"];
    $study_list = array();
    $study_list = StudyCommonModel::get_all_study_staff($search_nendo); // 全受講者
    foreach ($study_list as $item) {
    
    
        $lastname = $item["last_name"];
        $firstname = $item["first_name"];

        $job_nm = $item["job_nm"];
        $st_nm = $item["st_nm"];
        $class_nm = $item["class_nm"];
        $atrb_nm = $item["atrb_nm"];
        $dept_nm = $item["dept_nm"];
        $room_nm = $item["room_nm"];

        $study_date = $item["date"];
        $test_date = $item["test_date"];

        if(is_null($study_date)){
            $study_date = "-";
        }else{
            $study_date = conv_ymd($study_date);
        }

        $assembly_status_date = conv_ymd($item["assembly_actual_date"]);
        
        //if($study_date == conv_ymd($assembly_status_date)){
        if( $item["assembly_actual_status"]==1){
            $assembly_status = "○";
        }else if($item["assembly_status"]==2 || $item["assembly_status"]==1){
             $assembly_status = "×";
        }else{
            $assembly_status = "-";
        }

        $question_status = $item["questionnaire_status"];
        if($question_status == 0){
            $question_status = "-";
        }else{
             $question_status = "○";
        }

        $test_status = $item["test_status"];
        if($test_status == 0){
            $test_status = "-";
        }else{
             $test_status = "○";
        }

         if(GG_BARITESS_NENSUU_OPT_FLAG){
            $exp_start = $item["exp_start"];
            $exp_minus = $item["exp_minus"];
            $work_start = $item["work_start"];
            $work_minus = $item["work_minus"];

            $base_day = '';
            //テスト日
            if($test_date){
                $base_day = $test_date;
            }
            //アンケート日
            $questionnaire_date =  StudyQuestionnaireModel::get_questionnaire_answer_date($item["test_id"],$item["emp_id"]);
            if($questionnaire_date){
                $base_day = date('Ymd', strtotime($questionnaire_date[0]["update_date"]));
            }

            //集合研修日
            if($assembly_status_date){
                $base_day = $assembly_status_date;

            }

            if(is_null($exp_start) == False &&  $base_day != ''){
                $diff = day_diff($exp_start,$base_day,$exp_minus);
                $exp_month = $diff%12;
                $exp_year = floor($diff/12);
                if($exp_year<0) $exp_year = 0;
                if($exp_month<0) $exp_month = 0;

                $exp_diff = $exp_year."年".$exp_month."月";
               // $exp_diff = day_diff($exp_start,$now,$exp_minus);
            }else{
                $exp_diff = "-";
            }
            if(is_null($work_start) == False && $base_day != ''){
                $diff = day_diff($work_start,$base_day,$work_minus);
                $work_month = $diff%12;
                $work_year = floor($diff/12);
                if($work_year<0) $work_year = 0;
                if($work_month<0) $work_month = 0;

                $work_diff = $work_year."年".$work_month."月";
                //$work_diff = day_diff($work_start,$now,$exp_minus);
            }else{
                $work_diff = "-";
            }

            $output_dates = $item["title"].",$lastname $firstname,$job_nm,$st_nm,$class_nm,$atrb_nm,$dept_nm,";
             if(GG_CLASS_CNT == 4){
                $output_dates .= "$room_nm,";
             }
            $output_dates .= "$exp_diff,$work_diff,$assembly_status_date,$assembly_status,$question_status,$test_status";

        }else{
            $output_dates = $item["title"].",$lastname $firstname,$job_nm,$st_nm,$class_nm,$atrb_nm,$dept_nm,";
            if(GG_CLASS_CNT == 4){
                $output_dates .= "$room_nm,";
            }
            $output_dates .= "$assembly_status_date,$assembly_status,$question_status,$test_status";
        }

        $output_dates .="\n";
        $csv_data = mb_convert_encoding($output_dates , "SJIS-win", 'eucJP-win');
        echo($csv_data);
    }



    die;

}
?>
