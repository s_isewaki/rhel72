<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【ユーザ側】研修・講義 関連を扱うURL、および管理側からも、職員検索で呼ばれるユーザ個別の研修・講義画面を扱う
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyContentsModel.php';
require_once 'model/StudyNotificationModel.php';
require_once 'model/StudyFolderModel.php';


default_view();
die;





//**************************************************************************************************
// ユーザ側 研修・講義一覧画面処理
//**************************************************************************************************
function default_view() {
    //------------------------------------------------------
    // 取得するユーザのIDとユーザ名
    // ユーザ側では、ログイン者の情報を表示するが、
    // 管理側からは、ユーザを指定して履歴画面として表示させるため
    //------------------------------------------------------
    $is_popup = @$_REQUEST["is_popup"]; // 管理側からポップアップで呼ばれる場合

    $target_emp_id = GG_LOGIN_EMP_ID;
    $is_emp_proxy = "";
    if (GG_ADMIN_FLG || GG_STUDY_ADMIN_FLG || GG_STUDY_TANTOU_FLG) {
        if ($_REQUEST["target_emp_id"]) {
            $target_emp_id = $_REQUEST["target_emp_id"];
            $is_emp_proxy = "1";
        }
    }
    $target_emp = StudyCommonModel::get_user_data_by_emp_id($target_emp_id);

    $folder_list = StudyFolderModel::get_folder_list("", "", 0);

    //------------------------------------------------------
    // 検索実行、一覧取得
    //------------------------------------------------------
    $menu_user_sort = ($_REQUEST["menu_user_sort"]!="desc" ? "asc" : $_REQUEST["menu_user_sort"]);
    $contents_full_list = StudyContentsModel::get_contents_status_list_for_emp(
        $target_emp["emp_id"],
        $menu_user_sort,
        $_REQUEST["disp_out_of_date"],
        $_REQUEST["bunrui_year"],
        $_REQUEST["keyword"],
        $_REQUEST["folder_selected_ids"],
        $folder_list
    );


    //------------------------------------------------------
    // ページング
    // 面倒なのと、膨大に件数があるわけではない、ということで、全件検索した結果から出力分だけ切り出す
    //------------------------------------------------------
    $per_page_size = $_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;

    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;

    $total_count = count($contents_full_list);
    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;

    $contents_list = array();
    if ($per_page_size < 0) {
        $contents_list = $contents_full_list;
    } else {
        $idx = 0;
        $start_num = $per_page_size * ($page_num-1) + 1;
        $end_num = $start_num + $per_page_size - 1;
        foreach ($contents_full_list as $row) {
            $idx++;
            if ($idx<$start_num || $idx>$end_num) continue;
            $contents_list[] = $row;
        }
    }



    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';


    $smarty->assign("folder_list", $folder_list);
    $smarty->assign("folder_selected_ids", $_REQUEST["folder_selected_ids"]);
    $smarty->assign("page_num", (int)$page_num);
    $smarty->assign("per_page_size", (int)$per_page_size);
    $smarty->assign("total_count", (int)$total_count);
    $smarty->assign("last_page", (int)$last_page);
    $smarty->assign("bunrui_year", $_REQUEST["bunrui_year"]);
    $smarty->assign("keyword", $_REQUEST["keyword"]);
    $smarty->assign("disp_out_of_date", $_REQUEST["disp_out_of_date"]);
    $smarty->assign("is_popup", $is_popup);
    $smarty->assign("contents_list", $contents_list);
    $smarty->assign("notif_list_count", count(StudyNotificationModel::get_user_notif_list(""))); // 未読の、お知らせの数
    $smarty->assign("is_emp_proxy", $is_emp_proxy);
    $smarty->assign("target_emp_id", $target_emp["emp_id"]);
    $smarty->assign("emp_lt_nm", $target_emp["emp_lt_nm"]);
    $smarty->assign("emp_ft_nm", $target_emp["emp_ft_nm"]);
    $smarty->assign("menu_user_sort", $menu_user_sort); //
    $smarty->assign("update_sort", ($update_sort == null) ? "desc" : $update_sort );
    $smarty->display("study_menu_user.tpl");
}

?>
