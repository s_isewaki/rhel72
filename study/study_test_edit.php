<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【管理側】テスト関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyAssemblyModel.php';
require_once 'model/StudyTestModel.php';
require_once 'model/StudyContentsModel.php';
require_once 'model/StudyCommonModel.php';
require_once './Unserializer.php';


$btn_event = $_REQUEST["btn_event"];

if ($btn_event=="be_export_question") export_question_and_die();
if ($btn_event=="be_show_test_edit_total"|| $btn_event=="be_test_excel" || $btn_event=="be_test_csv"){
    show_test_edit_total_and_die($btn_event);
}
if ($btn_event=="be_test_execute" || $btn_event=="be_grading") showexec_test_execute_and_die();
if ($btn_event=="be_show_test_preview") show_test_preview_and_die();
if ($btn_event=="be_show_test_edit_attach") show_test_edit_attach_and_die();
if ($btn_event=="be_exec_test_edit_attach_upload") exec_test_edit_attach_upload_and_die();
if ($btn_event=="be_update_endtime"){ 
echo
    $tmp_end_day = mb_substr($_REQUEST["end_day"],0,4) . mb_substr($_REQUEST["end_day"],5,2) . mb_substr($_REQUEST["end_day"],8,2);
    $temp_hour = str_pad($_REQUEST["ed_hour"], 2, "0", STR_PAD_LEFT);
    $end_day = $tmp_end_day .$temp_hour .$_REQUEST["ed_minute"];
    update_endtime_and_die($end_day,$_REQUEST["contents_id"]);
}

default_view($btn_event);
die;



//**************************************************************************************************
//
//**************************************************************************************************
function default_view($btn_event) {
    require_once 'conf/smarty_start.conf';

    $result = $_REQUEST["result"];
    $contents_id = $_REQUEST["contents_id"];

    //==========================================================================
    // 新規作成処理/更新処理
    //==========================================================================
    if ($btn_event=="be_create" || $btn_event=="be_update") {
        StudyTestModel::update_test($contents_id);
        header("Location:study_test_edit.php?session=".GG_SESSION_ID."&contents_id=".$contents_id."&result=success");
        die;
    }

    //==========================================================================
    // テストの削除の場合
    //==========================================================================
    if ($btn_event=="be_delete") {
        StudyTestModel::delete_test($contents_id);
        header("Location:study_test_edit.php?session=".GG_SESSION_ID."&contents_id=".$contents_id."&result=success");
        die;
    }

    //==========================================================================
    // ＤＢ値を取得
    //==========================================================================
    $test_info = StudyTestModel::get_test_row($contents_id);
    $test_items = StudyTestModel::get_test_items($contents_id);
    $contents = StudyContentsModel::get_contents_row($contents_id);

    //==========================================================================
    // 問題インポート ヘッダ部分はREQUEST値を引継ぎ。問題部分は全消ししてxmlから。画像はworkディレクトリへ。保存はしない。
    //==========================================================================
    if ($btn_event=="be_show_from_menu") {
        StudyTestModel::try_sweep_work_dir($contents_id);
    }
    $import_status = "";
    if ($btn_event=="be_import_question") {
        //StudyTestModel::try_sweep_work_dir($contents_id);
        $contents["test_start_date"] = $_REQUEST["start_date"];
        $contents["test_end_date"]   = $_REQUEST["end_date"];
        $test_info["update_date"] = $_REQUEST["update_date"];
        $test_info["limit_time"] = $_REQUEST["limit_time"];
        $test_info["border_score"] = $_REQUEST["border_score"];
        $test_info["test_item_order"] = $_REQUEST["test_item_order"];
        $filename = $_FILES['import_xml_file']['tmp_name'];
        $xmlobj = new XML_Unserializer();
        $enc = mb_internal_encoding();
        mb_internal_encoding("utf-8");
        $status = $xmlobj->unserialize(preg_replace("/[\x0b]/u", "", file_get_contents($filename))); // 垂直タブ除去
        mb_internal_encoding($enc);
        $xml = $xmlobj->getUnserializedData();
        $test_items = array();

        $test_count = $_REQUEST["test_count"];
        for ($num = 1; $num <= $test_count; $num++) {
            $choice_cnt = (int)$_REQUEST["choice_cnt_".$num]; // 選択肢
            $select_answers_array = array();
            $correct_answers_array = array();
            for ($ii=1; $ii<=$choice_cnt; $ii++) {
                $id = $num."_".$ii;
                $select_answers_array[]= $_REQUEST["choice_item_".$id];
                $correct_answers_array[]= ($_REQUEST["choice_answer_".$id] ? "t" : "f");
            }

            $test_items[] = array(
                "id" => $contents_id,
                "no" => $num,
                "question" => $_REQUEST["question_".$num],
                "comment" => $_REQUEST["summary_".$num],
                "image_path" => $_REQUEST["image_path_".$num],
                "image_name" => $_REQUEST["image_name_".$num],
                "new_image_name" => $_REQUEST["new_image_name_".$num],
                "new_image_path" => $_REQUEST["new_image_path_".$num],
                "choice_cnt" => $choice_cnt,
                "choices" => implode(",", $select_answers_array),
                "multiple" => ($_REQUEST["is_multiple_choice_".$num]=="t"?"t":"f"),
                "answers" => implode(",", $correct_answers_array)
            );
        }

        $num = $test_count;
        if ($xml && is_array($xml)) {
            $stop_flg = false;
            // 正常に読み込めた場合の処理
            foreach ($xml["items"]["item"] as $buf) {
                $val = $buf;
                if (!is_array($val)) {
                    $val = $xml["items"]["item"];
                    $stop_flg = true;
                }
                $num++;

                // 画像バイナリ
                $image_path = ""; // カラとする。XMLのものは無視する。
                $image_name = mb_convert_encoding($val["image_name"],"eucJP-win","UTF-8");
                $image_obj = $val["image_obj"];
                if (strlen($image_obj)>10 && $image_name) { //適当に10バイト以上なら
                    $dir = GG_TEST_IMAGE_WORK_HOME.$contents_id;
                    if (!is_dir($dir)) mkdir($dir, 0777);
                    chmod($dir, 0777);
                    $image_ext = end(explode('.', $image_name));
                    $image_path = $dir."/".date("YmdHis")."_xml".$num.".".$image_ext; // 日本語でバケるのでファイル名は今後つけない。
                    $fh = fopen($image_path, "w");
                    fwrite($fh, base64_decode($image_obj));
                    fclose($fh);
                }

                $test_items[] = array(
                    "id" => $contents_id,
                    "no" => $num,
                    "question" => mb_convert_encoding($val["question"],"eucJP-win","UTF-8"),
                    "comment" => mb_convert_encoding($val["summary"],"eucJP-win","UTF-8"),
                    "image_path" => "",
                    "image_name" => "",
                    "new_image_name" => $image_name,
                    "new_image_path" => $image_path,
                    "choice_cnt" => mb_convert_encoding($val["choices_cnt"],"eucJP-win","UTF-8"),
                    "choices" => mb_convert_encoding($val["choices"],"eucJP-win","UTF-8"),
                    "multiple" => ($val["multiple"] ? "t" : "f"),
                    "answers" => mb_convert_encoding($val["answers"],"eucJP-win","UTF-8")
                );
                $import_status = "ok";
                if ($stop_flg) break;
            }
        }
        if ($import_status != "ok") $import_status = "error";
    }

    //------------------------------------------------------
    // 研修講義情報からテスト期間を取得
    //------------------------------------------------------
    $array_start = conv_ymdhm_to_jp_and_time($contents["test_start_date"]);
    $array_end   = conv_ymdhm_to_jp_and_time($contents["test_end_date"]);

    //------------------------------------------------------
    // テスト項目のうち、以下は配列にする
    //------------------------------------------------------
    foreach ($test_items as $idx=>$item) {
        $test_items[$idx]["choices"] = split(",", $item["choices"]);
        $test_items[$idx]["answers"] = split(",", $item["answers"]);

        $path = $test_items[$idx]["image_path"];
        if ($test_items[$idx]["new_image_path"]) $path = $test_items[$idx]["new_image_path"];
        list($iw, $ih) = @getimagesize($path);
        $w_max = 300;
        $h_max = 130;
        $test_items[$idx]["image_size_style"] = "";
        if ($iw > $w_max || $ih > $h_max) {
            if ($iw / $w_max < $ih / $h_max) $test_items[$idx]["image_size_style"] = ' height="'.$h_max.'"'; // 縦長である
            else $test_items[$idx]["image_size_style"] = ' width="'.$w_max.'"'; // 横長である
        }

        if (substr($path,0,strlen(GG_BASE_FOLDER)+1)==GG_BASE_FOLDER."/") $path = substr($path, strlen(GG_BASE_FOLDER)+1);
        $test_items[$idx]["image_path_url"] = $path;
    }

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_test_edit.tpl@all", $contents["folder_id"], "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $template_vars = array(
        "import_status" => $import_status,
        "result" => $result,
        "disp_auth" => $disp_auth,
        "edit_auth" => $edit_auth,
        "contents" => $contents,
        "contents_id" => $contents_id,
        "test_info" => $test_info,
        "test_items" => $test_items,
        "test_executing_count" => (int)$contents["test_ok_count"] + (int)$contents["test_ng_count"], // テスト実施数
        "test_count" => count($test_items),
        "array_start" => $array_start,
        "array_end" => $array_end
    );
    display_template("study_test_edit.tpl", $template_vars);
    die;
}



//**************************************************************************************************
// テストの添付画像ファイルダイアログを開く
//**************************************************************************************************
function show_test_edit_attach_and_die() {
    require_once 'conf/smarty_start.conf';
    $smarty->assign("result", "");
    $smarty->assign("file_path", "");
    $smarty->assign("file_name", "");
    $smarty->assign("file_path_url", "");
    $smarty->assign("image_size_style", "");
    $smarty->assign("contents_id", $_REQUEST["contents_id"]);
    $smarty->assign("item_num", $_REQUEST["item_num"]);
    $smarty->display("study_test_edit_attach.tpl");
    die;
}


//**************************************************************************************************
// テストの添付画像ダイアログからのアップロードを行う。ファイルをworkディレクトリに一時保存する。
// 画面復帰後、親画面（テスト編集画面）へ、JavaScript越しに値を反映する。
//**************************************************************************************************
function exec_test_edit_attach_upload_and_die() {
    $result = "";
    $contents_id = $_REQUEST["contents_id"];
    $item_num = $_REQUEST["item_num"];
    $file_name = "";
    $file_path = "";
    $file_path_url = "";
    $image_size_style = "";

    if (is_uploaded_file($_FILES["attach_file"]["tmp_name"])) {
        $file_name = $_FILES["attach_file"]["name"];
        $dir = GG_TEST_IMAGE_WORK_HOME.$contents_id;
        if (!is_dir($dir)) mkdir($dir, 0777);
        chmod($dir, 0777);
        $image_ext = end(explode('.', $file_name));
        $file_path = $dir."/".date("YmdHis")."_up.".$image_ext; // 日本語でバケるのでファイル名は今後つけない。
        if (!move_uploaded_file($_FILES["attach_file"]["tmp_name"], $file_path)) {
            $emsg = "添付ファイルのディスク書込みに失敗しました。".$file_path;
            logger_error($emsg);
            fatal_exception($emsg, __FILE__." ".__LINE__);
        }
        chmod($file_path, 0666);

        list($iw, $ih) = @getimagesize($file_path);
        $w_max = 300;
        $h_max = 130;
        if ($iw > $w_max || $ih > $h_max) {
            if ($iw / $w_max < $ih / $h_max) $image_size_style = ' height="'.$h_max.'"'; // 縦長である
            else $image_size_style = ' width="'.$w_max.'"'; // 横長である
        }

        if (substr($file_path,0,strlen(GG_BASE_FOLDER)+1)==GG_BASE_FOLDER."/") $file_path_url = substr($file_path, strlen(GG_BASE_FOLDER)+1);
        $result = "success";
    }
    require_once 'conf/smarty_start.conf';
    $smarty->assign("result", $result);
    $smarty->assign("file_path", $file_path);
    $smarty->assign("file_name", $file_name);
    $smarty->assign("file_path_url", $file_path_url);
    $smarty->assign("image_size_style", $image_size_style);
    $smarty->assign("contents_id", $_REQUEST["contents_id"]);
    $smarty->assign("item_num", $_REQUEST["item_num"]);
    $smarty->display("study_test_edit_attach.tpl");
    die;
}

function update_endtime_and_die($end_day,$contents_id)
{

     StudyAssemblyModel::updata_test_end_date($contents_id, $end_day);
}




//**************************************************************************************************
// 管理側、テストのプレビュー。内容は子画面から親画面へJavaScriptでアクセスして取得
//**************************************************************************************************
function show_test_preview_and_die() {
    $template_vars = array(
        "contents_id" => $_REQUEST["contents_id"],
        "item_count" => $_REQUEST["item_count"],
        "item_id_comma_list" => $_REQUEST["item_id_comma_list"],
        "limit_time" => $_REQUEST["limit_time"]
    );
    display_template("study_test_edit_preview.tpl", $template_vars);
    die;
}











//**************************************************************************************************
// 問題別正答率(％)表
//**************************************************************************************************
function show_test_edit_total_and_die($btn_event) {
    $contents_id = $_REQUEST["contents_id"];
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $contents_title = $contents["title"];

    //------------------------------------------------------
    // テスト正答を取得する。「問題番号＝正答の配列(t,f,t,f...)」形式で取得
    //------------------------------------------------------
    $test_items = StudyTestModel::get_test_items($contents_id);
    $true_answers = array();
    foreach ($test_items as $row_test) {
        $ary = array();
        $ans = explode(",", $row_test["answers"]); // t,f,t,f,....
        for ($idx=1; $idx<=count($ans); $idx++) {
            if ($ans[$idx-1]=="t") $ary[] = $idx;
        }
        $true_answers[$row_test["no"]] = implode(",", $ary);
    }
    //------------------------------------------------------
    // 受講履歴を取得（最初の３回までと、最後の１回）
    //------------------------------------------------------
    $rows = StudyTestModel::get_test_result_for_total_showing($contents_id);  // テスト結果を取得。最初の３回までと、最後の１回を取得。
    $bunbo = array(); // 分母。テストが実施された数
    $bunsi = array(); // 分子。そのテストに正解した数
    $percents = array(); // パーセント表
    $bunbun = array(); // 「分子/分母」形式で表示する場合。ただし、開発時確認用。画面では利用していない。
    $heikin_row = count($true_answers) + 1; // 平均行の行番号。問題数の次行にくるので、問題数プラス１となる
    foreach($rows as $row) {
        $user_answers = explode("|", $row["user_answers"]); // カンマつなぎで問題別正否が格納されている。「1,0,1」など。正解時は1。
        $kaime = (int)$row["no"]; // 何回目のテスト反復実施かの番号。縦列番号となる。SQL実行時、1から3までと、「最後の実施時の番号」があり得る。
        if ($kaime <= 3) { // 最終実行時の回であれば、最終回は4列目なので、4とする。
            for ($monme=1; $monme<=count($true_answers); $monme++) { // 1問目から
                $bunbo[$monme][$kaime]  =  ((int)@$bunbo[$monme][$kaime]) + 1; // 分母 $monme問目 $kaime回目
                $bunbo[$heikin_row][$kaime]  =  ((int)@$bunbo[$heikin_row][$kaime]) + 1; // 分母 $monme問目 $kaime回目
                if ($user_answers[$monme-1]!=$true_answers[$monme]) continue; // 正解でなければ飛ばす
                $bunsi[$monme][$kaime]  =  ((int)@$bunsi[$monme][$kaime]) + 1; // 分子 $monme問目 $kaime回目
                $bunsi[$heikin_row][$kaime]  =  ((int)@$bunsi[$heikin_row][$kaime]) + 1; // 分子 $monme問目 $kaime回目
            }
        }
        if ($row["is_newest"]=="1") {
            $kaime = 4; // 最終実行時の回であれば、最終回は4列目なので、4とする。
            for ($monme=1; $monme<=count($true_answers); $monme++) { // 1問目から
                $bunbo[$monme][$kaime]  =  ((int)@$bunbo[$monme][$kaime]) + 1; // 分母 $monme問目 $kaime回目
                $bunbo[$heikin_row][$kaime]  =  ((int)@$bunbo[$heikin_row][$kaime]) + 1; // 分母 $monme問目 $kaime回目
                if ($user_answers[$monme-1]!=$true_answers[$monme]) continue; // 正解でなければ飛ばす
                $bunsi[$monme][$kaime]  =  ((int)@$bunsi[$monme][$kaime]) + 1; // 分子 $monme問目 $kaime回目
                $bunsi[$heikin_row][$kaime]  =  ((int)@$bunsi[$heikin_row][$kaime]) + 1; // 分子 $ monme問目 $kaime回目
            }
        }
    }

    for ($monme=1; $monme<=$heikin_row; $monme++) { // $monme問目
        for ($kaime = 1; $kaime <= 4; $kaime++) { // $kaime回目
            $percents[$monme][$kaime] = get_percent($bunsi[$monme][$kaime],  $bunbo[$monme][$kaime], 2, 100)."%";
            $bunbun[$monme][$kaime]= ((int)@$bunsi[$monme][$kaime])."/".((int)@$bunbo[$monme][$kaime]);
        }
    }

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    if ($btn_event=="be_show_test_edit_total") {
        require_once 'conf/smarty_start.conf';
        $smarty->assign("contents_title", $contents_title);
        $smarty->assign("bunbun", $bunbun);
        $smarty->assign("percents", $percents);
        $smarty->display("study_test_edit_total.tpl");
        die;
    }

    //------------------------------------------------------
    // エクセルの場合
    //------------------------------------------------------
    if($btn_event=="be_test_excel") {
        require_once 'conf/smarty_start.conf';
        $smarty->assign("contents_title", $contents_title);
        $smarty->assign("bunbun", $bunbun);
        $smarty->assign("percents", $percents);

        ob_start();
        $excel_name = mb_convert_encoding($contents["title"], "SJIS-win", 'eucJP-win');
        $smarty->display("study_test_excel.tpl");
        $excel_data = mb_convert_encoding(ob_get_contents(), "SJIS-win", 'eucJP-win');
        ob_end_clean();
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");
        echo($excel_data);

        die;
    }


    //------------------------------------------------------
    // CSVの場合
    //------------------------------------------------------
    if($btn_event=="be_test_csv") {
        //require (dirname(__FILE__)."/../conf/conf.inf");
        //$staff_nensuu_flg = $BARITESS_NENSUU_OPT_FLAG;

        $excel_name = mb_convert_encoding($contents["title"], "SJIS-win", 'eucJP-win');
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        $classnm = GG_CLASS_NM; // 部
        $atrbnm = GG_ATRB_NM;   // 課
        $deptnm = GG_DEPT_NM;   // 科
        $roomnm = GG_ROOM_NM;   // 室 値がなければ３階層まで

        $excel_name .= "\n";

        $title = "氏名,職種,役職,$classnm,$atrbnm,$deptnm,";

        if(GG_CLASS_CNT == 4){
            $title .="$roomnm,";
        }

        if(GG_BARITESS_NENSUU_OPT_FLAG){
            $title .= "職種年数,勤続年数,回数";
        }else{
             $title .= "回数";
        }



        for ($monme=1; $monme<=count($true_answers); $monme++) { // 1問目から
             $title .=",第 $monme 問回答,第 $monme 問正誤";
         }

        $title .=",正答率\n";
        $excel_name .= mb_convert_encoding($title, "SJIS-win", 'eucJP-win');
        echo($excel_name);
        //ここまでヘッダ

        $hairetu = array();
        foreach($rows as $item_id) {
            if(!in_array($item_id["emp_id"], $hairetu)){
                array_push($hairetu, $item_id["emp_id"]);
            }
        }
        foreach($hairetu as $emp_id) {
            $joken["emp_id"] = $emp_id;
            $user_list = StudyCommonModel::get_user_list($joken,2);
            /*foreach ($user_list as $key => $value){
                $key_class[$key] = $value['class_nm'];
                $key_atrb[$key] = $value['atrb_nm'];
                $key_dept[$key] = $value['dept_nm'];
                $key_room[$key] = $value['room_nm'];
                $key_last[$key] = $value['last_name'];
                $key_first[$key] = $value['first_name'];
            }*/
            foreach($user_list as $user_row){
                $first_name = $user_row["first_name"];
                $last_name = $user_row["last_name"];
                $job_nm = $user_row["job_nm"];
                $st_nm = $user_row["st_nm"];
                $class_nm = $user_row["class_nm"];
                $atrb_nm = $user_row["atrb_nm"];
                $dept_nm = $user_row["dept_nm"];
                if(GG_CLASS_CNT == 4){
                    $room_nm = $user_row["room_nm"];
                }

            }

            $study_emp_list = StudyTestModel::get_test_result($contents_id, $emp_id);
            $emp_career = StudyCommonModel::get_staff_career($emp_id);

            foreach ($study_emp_list as $key=>$item_answer) {
                $output_dates = "$last_name $first_name,$job_nm,$st_nm,$class_nm,$atrb_nm,$dept_nm";
                 if(GG_CLASS_CNT == 4){
                     $output_dates .= ",".$room_nm;
                }
                if(GG_BARITESS_NENSUU_OPT_FLAG){
                    if($emp_career[0]["work_start"]){
                        $tmp_work_nensuu = day_diff($emp_career[0]["work_start"],$study_emp_list[$key]["test_date"],$emp_career[0]["work_minus"]);
                        $tmp_work_month = $tmp_work_nensuu%12;
                        $tmp_work_year = floor($tmp_work_nensuu/12);
                        if($tmp_work_year<0) $tmp_work_year = 0;
                        if($tmp_work_month<0) $tmp_work_month = 0;

                        $work_nensuu = $tmp_work_year."年".$tmp_work_month."月";
                    }else{
                        $work_nensuu = "-";
                    }

                    if($emp_career[0]["exp_start"]){
                        $tmp_exp_nensuu = day_diff($emp_career[0]["exp_start"],$study_emp_list[$key]["test_date"],$emp_career[0]["exp_minus"]);
                        $tmp_exp_month = $tmp_exp_nensuu%12;
                        $tmp_exp_year = floor($tmp_exp_nensuu/12);

                        if($tmp_exp_year<0) $tmp_exp_year = 0;
                        if($tmp_exp_month<0) $tmp_exp_month = 0;

                        $exp_nensuu = $tmp_exp_year."年".$tmp_exp_month."月";
                    }else{
                        $exp_nensuu = "-";
                    }

                    $output_dates .= ",$exp_nensuu,$work_nensuu," . $item_answer["no"]."回目,";
                }else{
                     $output_dates .= ",".$item_answer["no"]."回目,";
                }
                $staff_answers = explode("|", $item_answer["answers"]);

                for ($monme=0; $monme<count($true_answers); $monme++) { // 1問目か
                    if($true_answers[$monme+1] == $staff_answers[$monme]){
                        $seikai = "O";
                    }else{
                        $seikai = "×";
                    }
                        $staff_answers[$monme] = str_replace(","," ",$staff_answers[$monme]);
                        $output_dates .="$staff_answers[$monme], $seikai,";
                }
                $output_dates .= $item_answer["percentage"]."\n";
                $csv_data = mb_convert_encoding($output_dates , "SJIS-win", 'eucJP-win');
                echo($csv_data);
            }
        }
        die;
    }

}








//**************************************************************************************************
// 問題エクスポート
//**************************************************************************************************
function export_question_and_die() {
    $xml = array();
    $xml[]= "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    $xml[]= "<comedix>";
    $xml[]= "   <title>e-learing</title>";
    $xml[]= "   <items>";


    $test_count = $_REQUEST["test_count"];
    for ($num = 1; $num <= $test_count; $num++) {
        $no = $i + 1;

        // 選択肢
        $choice_cnt = (int)$_REQUEST["choice_cnt_".$num];
        $select_answers_array = array();
        $correct_answers_array = array();
        for ($idx=1; $idx<=$choice_cnt; $idx++) {
            $id = $num."_".$idx;
            $select_answers_array[]= $_REQUEST["choice_item_".$id];
            $correct_answers_array[]= ($_REQUEST["choice_answer_".$id] ? "t" : "f");
        }
        $image_stream = "";
        $image_name = "";
        $image_path = "";
        if (!$_REQUEST["attach_delete_".$num]) {
            $image_name = $_REQUEST["image_name_".$num];
            $image_path = $_REQUEST["image_path_".$num];
            if ($_REQUEST["new_image_path_".$num]) {
                $image_name = $_REQUEST["new_image_name_".$num];
                $image_path = $_REQUEST["new_image_path_".$num];
            }
            $image_stream = base64_encode(file_get_contents($image_path));
        }

        $xml[]= "       <item no=\"" . $no . "\">";
        $xml[]= "           <question>" . mb_convert_encoding(h($_REQUEST["question_".$num]),'UTF-8','eucJP-win') . "</question>";
        $xml[]= "           <summary>" . mb_convert_encoding(h($_REQUEST["summary_".$num]),'UTF-8','eucJP-win') . "</summary>";
        $xml[]= "           <image_name>" . mb_convert_encoding(h($image_name),'UTF-8','eucJP-win') . "</image_name>";
        $xml[]= "           <choices_cnt>" . h($choice_cnt) . "</choices_cnt>";
        $xml[]= "           <multiple>" . (h($_REQUEST["is_multiple_choice_".$num]) === "t" ? true:false) . "</multiple>";
        $xml[]= "           <choices>" . mb_convert_encoding(h(implode(",", $select_answers_array)),'UTF-8','eucJP-win') . "</choices>";
        $xml[]= "           <answers>" . mb_convert_encoding(h(implode(",", $correct_answers_array)),'UTF-8','eucJP-win') . "</answers>";
        $xml[]= "           <image_obj>" . $image_stream . "</image_obj>";
        $xml[]= "       </item>";
    }

    $xml[]= "   </items>";
    $xml[]= "</comedix>";
    header("Content-Type: application/xml");
    header("Content-Disposition: attachment; filename=question.xml");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
    ob_clean();
    echo(implode("\n",$xml));
    die;
}

?>
