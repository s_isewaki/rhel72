<?
$btn_event = $_REQUEST["btn_event"];
if ($btn_event=="dummy") {
    session_start();
    echo "ok";
    die;
}

require_once 'StudyInitController.php';
require_once 'model/StudyFolderModel.php';
require_once 'model/StudyContentsModel.php';
require_once 'model/StudyCommonModel.php';



if ($_REQUEST["zz_download_file_path"]) {
    header("Location:".$_REQUEST["download_file_url"]."?xx_download_file_path=".$_REQUEST["zz_download_file_path"]);
    die;
}


if ($btn_event=="dummy") { echo "ok"; die; }
if ($btn_event=="be_show_study_contents_movie") show_study_contents_movie_and_die();
if ($btn_event=="be_show_contents_summary") show_contents_summary_and_die();
if ($btn_event=="be_summary_syozokubetu" || $btn_event=="be_download_syozokubetu") showexec_summary_syozokubetu_and_die($btn_event);
if ($btn_event=="be_show_download_dialog") show_download_dialog_and_die();
if ($btn_event=="be_check_duplicate_contents") exec_check_duplicate_contents();



default_view($btn_event);
die;


//**************************************************************************************************
// ajaxで呼ばれる。登録を実際に行う前にチェック
// ajaxはUTF8で送信されるが、日本語は、ここで受信した時点で文字化け回避不可。
// なので送信時に、UTF16を16進表現で送信するようにしている。
// utf16hex2str()関数でデコードする。
//**************************************************************************************************
function exec_check_duplicate_contents() {
    ob_clean();
    $contents_name = utf16hex2str($_REQUEST["contents_name_utf16hex"]);
    // 新規の場合はcontents_idはカラである。
    $ret = StudyContentsModel::contents_exists($_REQUEST["folder_id"], $contents_name, $_REQUEST["contents_id"]);
    if ($ret) echo "保存先講座に同名の研修・講義が存在します。タイトルまたは講座を変更してください。";
    else echo "ok";
    die;
}



//**************************************************************************************************
// 研修・講義ダイアログ処理（管理画面の詳細 / ユーザ側プレビュー兼用）
//**************************************************************************************************
function default_view($btn_event) {
    require_once 'conf/smarty_start.conf';
    $contents_id = $_REQUEST["contents_id"];

    $smarty->assign("result", "");

    //==========================================================================
    // 研修・講義の新規作成処理
    //==========================================================================
    if ($btn_event=="be_create") {
        $contents_id = StudyContentsModel::edit_contents($_REQUEST, "create");
        $smarty->assign("result", "success");
    }

    //==========================================================================
    // 研修・講義の複製処理
    //==========================================================================
    if ($btn_event=="be_copy") {
        $new_contents_id = StudyContentsModel::edit_contents($_REQUEST, "copy");
        StudyContentsModel::copy_extra_data($contents_id, $new_contents_id);
        $smarty->assign("result", "success");
        $contents_id = $new_contents_id;
    }

    //==========================================================================
    // 研修・講義の更新処理
    //==========================================================================
    if ($btn_event=="be_update") {
        StudyContentsModel::edit_contents($_REQUEST, "update");
        $smarty->assign("result", "success");
    }

    //==========================================================================
    // 研修・講義の削除処理
    //==========================================================================
    if ($btn_event=="be_delete") {
        StudyContentsModel::delete_contents($contents_id);
        $smarty->assign("result", "success");
    }

    //------------------------------------------------------
    // ここから表示処理
    // 研修・講義データの取得
    //------------------------------------------------------
    $contents = StudyContentsModel::get_contents_row($contents_id);

    // 更新モード
    if ($_REQUEST["event"] === "ev_edit_contents") {
        if ($_REQUEST["btn_event"]!=="be_delete" && !$contents) business_exception("研修・講義が存在しません。", "close");
        $contents["save_folder_id"] = "";
    }
    // データが無い場合(新規登録用)
    if (!$contents || !$contents["id"]) {
        $contents = array(
            "title"=>"", "no"=>"", "file_name"=>"", "page_cnt"=>"", "keyword"=>"", "file_path"=>"", "type"=>"",
            "summary"=>"", "save_folder_id"=>"", "id"=>"","folder_id" => $_REQUEST["save_folder_id"],
            "bunrui_year"=>"", "file_capacity"=>"", "student_count"=>"", "policy_code"=>"", "assembly_actual_count"=>"",
            "questionnaire_answer_count"=>"", "test_ok_count"=>"", "test_ng_count"=>"", "policy_for_questionnaire"=>"", "policy_for_test"=>""
        );
    }

    //------------------------------------------------------
    // 講座名
    //------------------------------------------------------
    $folder_id = $contents["folder_id"];
    if (!$folder_id) $folder_id = $_REQUEST["save_folder_id"];
    $folder_row = StudyFolderModel::get_folder_row($folder_id);
    $folder_name = $folder_row["name"];

    //------------------------------------------------------
    // 添付ファイル
    //------------------------------------------------------
    $files = StudyContentsModel::get_files_info_to_array($contents["files_info"]);
    $file_count = count($files);
    $def = array("file_type"=>"", "file_no"=>"", "file_url"=>"", "file_name"=>"", "file_path"=>"", "file_capacity"=>"");
    for ($idx=$file_count; $idx<10; $idx++) $files[] = $def;
    if ($file_count==0 && (!$contents["title"] || $contents["file_name"])) $file_count = 1; // タイトル無しは新規とみなす。
    //------------------------------------------------------
    // colrow_data（セルごとの従業員数）を作成（ただし、このページでは厳密には不要となっている）
    //------------------------------------------------------
    $student_list = StudyCommonModel::get_student_list($contents_id, array(), 1, 0, 0);
    $policy_info = get_policy_info($contents["policy_code"]);
    $colrow_data = StudyContentsModel::make_calculated_summary($student_list, $contents_id, $policy_info, "html");

    $js_colrow_data = array();
    foreach ($colrow_data as $k => $v) $js_colrow_data[] = '"'.$k.'":"'.$v.'"';

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_contents.tpl@all", $folder_id, "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    if ($_REQUEST["preview_flg"]) {
        $contents["files_info"] = StudyContentsModel::get_files_info_to_array($contents["files_info"]);
        $template_vars = array(
            "contents" => $contents
        );
        display_template("study_contents_preview.tpl", $template_vars);
    } else {
        $smarty->assign("contents_id", $contents_id);
        $smarty->assign("disp_auth", $disp_auth);
        $smarty->assign("edit_auth", $edit_auth);
        $smarty->assign("files", $files);
        $smarty->assign("file_count", $file_count);
        $smarty->assign("event", $_REQUEST["event"]);
        $smarty->assign("btn_event", $_REQUEST["btn_event"]);
        $smarty->assign("contents", $contents);
        $smarty->assign("folder_id", $folder_id);
        $smarty->assign("folder_name", $folder_name);
        $smarty->assign("js_colrow_data", implode(",",$js_colrow_data));
        $smarty->assign("upload_max_filesize", ini_get("upload_max_filesize"));
        $smarty->assign("org_folder_id", (int)$_REQUEST["org_folder_id"]);
        $smarty->display("study_contents.tpl");
    }
}









//**************************************************************************************************
// 受講状況画面を開く
//**************************************************************************************************
function show_contents_summary_and_die() {
    $contents_id = $_REQUEST["contents_id"];
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $student_list = StudyCommonModel::get_student_list($contents_id, array(), 1, 0, 0);

    $policy_code = $contents["policy_code"];
    $is_summary_preview = "";
    if ($_REQUEST["policy_code"] && $policy_code !=$_REQUEST["policy_code"]) {
        $policy_code = $_REQUEST["policy_code"];
        $is_summary_preview = 1;
    }
    $policy_info = get_policy_info($policy_code);
    $colrow_data = StudyContentsModel::make_calculated_summary($student_list, $contents_id, $policy_info, "html");

    $js_colrow_data = array();
    foreach ($colrow_data as $k => $v) $js_colrow_data[] = '"'.$k.'":"'.$v.'"';

    require_once 'conf/smarty_start.conf';
    $smarty->assign("contents", $contents);
    $smarty->assign("student_count", (int)$contents["student_count"]);
    $smarty->assign("is_summary_preview", $is_summary_preview);
    $smarty->assign("policy_code", $policy_code);
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("js_colrow_data", implode(",",$js_colrow_data));
    $smarty->display("study_contents_summary.tpl");
    die;
}








//**************************************************************************************************
// 教材・テキストダウンロードダイアログを開く
//**************************************************************************************************
function show_download_dialog_and_die() {
    $contents_id = $_REQUEST["contents_id"];
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $contents["files_info"] = StudyContentsModel::get_files_info_to_array($contents["files_info"]);

    $template_vars = array(
        "result" => $_REQUEST["result"], // study_download.php も参照のこと
        "contents" => $contents,
        "contents_id" => $contents_id
    );
    display_template("study_contents_download.tpl", $template_vars);
    die;
}

function show_study_contents_movie_and_die() {
    $contents_id = $_REQUEST["contents_id"];
    $file_path = $_REQUEST["file_path"];
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $contents["files_info"] = StudyContentsModel::get_files_info_to_array($contents["files_info"]);
    $finfo = array();
    foreach ($contents["files_info"] as $tmp) {
        if ($tmp["file_path"]==$file_path) $finfo = $tmp;
    }
    $ary = explode(".", $finfo["file_name"]);
    $file_ext = $ary[count($ary)-1];

    $template_vars = array(
        "finfo" => $finfo,
        "contents" => $contents,
        "movie_url" => $finfo["movie_url"],
        "download_url" => $finfo["download_url"],
        "real_url" => $finfo["real_url"],
        "file_ext" => strtolower($file_ext),
        "download_url" => $finfo["download_url"]
    );
    display_template("study_contents_movie.tpl", $template_vars);
    die;
}




//**************************************************************************************************
// 所属別受講状況表を表示する
// SQLの段階では、受講者ステータスをSUMできない。重複があり得るので、ひとりひとり進捗状況を判定しないといけない。
// しかし、ひとりひとりをmake_calculated_summary()関数にかけると時間がかかるので、
// 最低単位（room_id）ごとに処理する。
//**************************************************************************************************
function showexec_summary_syozokubetu_and_die($btn_event) {
    $contents_id = $_REQUEST["contents_id"];

    $output_type = "html";
    if ($btn_event=="be_download_syozokubetu") $output_type = "excel";

    $contents = StudyContentsModel::get_contents_row($contents_id);
    $student_count = $contents["student_count"];
    $policy_code = $contents["policy_code"];
    if ($_REQUEST["policy_code"]) $policy_code = $_REQUEST["policy_code"];
    $policy_info = get_policy_info($policy_code);

    $raw_rows = StudyContentsModel::get_syozokubetu_list($contents_id);
    $division_rows = array();
    foreach ($raw_rows as $r) {
        $key = $r["class_id"]."_".$r["atrb_id"]."_".$r["dept_id"];
        $division_rows[$key][] = $r;
    }
    $data_list = array();
    $section_names = array();
    $data_hier = array();
    foreach ($division_rows as $key => $rows) {
        $data_list[$key] = StudyContentsModel::make_calculated_summary($rows, $contents_id, $policy_info, $output_type);
        $cnm = $rows[0]["class_nm"];
        if ($cnm == "") $cnm = "(なし)";
        $anm = $rows[0]["atrb_nm"];
        if ($anm == "") $anm = "(なし)";
        $dnm = $rows[0]["dept_nm"];
        if ($dnm == "") $dnm = "(なし)";
        $section_names[$key] = array("class_nm"=>$cnm, "atrb_nm"=>$anm, "dept_nm"=>$dnm);
        $section_names[$rows[0]["class_id"]."_".$rows[0]["atrb_id"]] = array("class_nm"=>$cnm, "atrb_nm"=>$anm);
        $section_names[$rows[0]["class_id"]] = array("class_nm"=>$cnm);
        $ary = explode("_", $key);
        $data_hier[$ary[0]][$ary[1]][$ary[2]] = $key; // class_id / atrb_id / dept_id
    }

    $section_row_count = array();
    foreach ($data_hier as $class_id => $hier1) {
        $class_cnt = 0;
        foreach ($hier1 as $atrb_id => $hier1) {
            $cnt = count($hier1) + 1;
            $section_row_count[$class_id."_".$atrb_id] = $cnt;
            $class_cnt += $cnt;
        }
        $section_row_count[$class_id] = $class_cnt + 1;
    }

    //------------------------------------------------------
    // 小計合計作成対象を作成
    // 「受講状況表」のうち、どのマスを、何という名前で、どの順番に出すかを決める。
    //------------------------------------------------------
    $total_info = get_policy_total_info($policy_info);
    $is_multi_pattern = $total_info["is_multi_pattern"];
    $max_group_size = $total_info["max_group_size"];

    $make_fields = array();
    foreach ($policy_info as $func => $info) {
        $col = $info["col"];
        $jp = $info["jp"];
        $hissu = ($info["hissu_mark"]=="+" ? 1 : 0);
        $nini = ($hissu ? "" : "(任意実施)");

        // �‖仂歇埒瑤論萋�列のみで出力
        if ($col==1) {
            $make_fields[] = array("field"=>"col".$col."_row1", "jp"=>"受講者数", "disp"=>"yes", "postfix"=>"");
        } else {
            $make_fields[] = array("field"=>"col".$col."_row1", "jp"=>"受講者数", "disp"=>"calc_only", "postfix"=>"");
        }
        // ��単体受講済数も常に出力
        $make_fields[] = array("field"=>"col".$col."_row2", "jp"=>$jp."受講済数".$nini, "disp"=>"yes", "postfix"=>"");
        // �Ｌそた瑤眈錣暴侘蓮△燭世掘▲謄好箸両豺腓麓損榁罎般ぜ損椶鯤�割して出力
        if ($func=="test") {
            $make_fields[] = array("field"=>"col".$col."_row11", "jp"=>$jp."実施中", "disp"=>"yes", "postfix"=>"");
            $make_fields[] = array("field"=>"col".$col."_row12", "jp"=>$jp."未実施", "disp"=>"yes", "postfix"=>"");
        } else {
            $make_fields[] = array("field"=>"col".$col."_row3", "jp"=>$jp."未修数", "disp"=>"yes", "postfix"=>"");
        }
        $last_col = $col;
    }

    //------------------------------------------------------
    // 小計合計作成対象として最後に追加する項目
    // ANDマルチの場合は、差引数を出力
    // ANDマルチでないが、ORグループのみ「AまたはB」「AまたはBまたはC」でも出力。
    // どちらにしても、ANDマルチ（差引）の数を出せばよい。
    //------------------------------------------------------
    if ($is_multi_pattern || $max_group_size >=2) {
        $ss = "累積";
        if ($is_multi_pattern) $ss = "差引";
        $make_fields[] = array("field"=>"col".$last_col."_row8", "jp"=>$jp.$ss."受講済数", "disp"=>"calc_only", "postfix"=>"");
        $make_fields[] = array("field"=>"col".$last_col."_row9", "jp"=>$jp.$ss."未修数", "disp"=>"calc_only", "postfix"=>"");
    }

    // 受講率は全部表示した後、最後だけ出力することにする
    // 受講率はANDマルチ（差引受講済数）のものを出力することでよい。キャプションのみ変更しておく。
    $ss = "総合";
    // if ($max_group_size >=2) $ss = "累積"; // ANDパターン復活の場合は戻す
    // if ($is_multi_pattern)   $ss = "差引"; // ANDパターン復活の場合は戻す
    $make_fields[] = array("field"=>"col".$last_col."_row10", "jp"=>$ss."受講率％", "disp"=>"yes", "postfix"=>"%");


    //------------------------------------------------------
    // 小計合計を作成してゆく
    //------------------------------------------------------
    $key_list = array();
    foreach ($data_list as $key => $row) $key_list[$key] = "";

    for ($node = 3; $node>=1; $node--) {
        $parent_key_list = array();
        foreach ($key_list as $key=>$dummy) {
            $ary = explode("_", $key);
            array_pop($ary);
            $parent_key = implode("_", $ary);
            if ($parent_key==="") $parent_key = "last_total";
            $parent_key_list[$parent_key] = "";
            foreach ($make_fields as $mfield) {
                $field = $mfield["field"];
                $data_list[$parent_key][$field] = ((int)@$data_list[$parent_key][$field]) + $data_list[$key][$field];
            }
        }
        // 率を再計算。小計合計行は、差引数�┐鯤�子にして再計算なのだが、
        // シングル設定（必須項目がひとつしかない場合）は、累計も差引も不要であるので、
        // 分子は単体受講率�△鰺�用する。
        foreach ($parent_key_list as $parent_key => $dummy) {
            $pfix = "col".$last_col."_row";
            $student_sub_count = $data_list[$parent_key][$pfix."1"];
            if (!$is_multi_pattern && $max_group_size ==1) {
                $data_list[$parent_key][$pfix."10"] = get_percent($data_list[$parent_key][$pfix."2"], $student_sub_count,1,100);
            } else {
                $data_list[$parent_key][$pfix."10"] = get_percent($data_list[$parent_key][$pfix."8"], $student_sub_count,1,100);
            }
        }
        $key_list = $parent_key_list;
    }

    //==========================================================================
    // 出力１）HTML出力の場合
    //==========================================================================
    if ($output_type=="html") {
        $template_vars = array(
            "contents_id" => $contents_id,
            "section_names" => $section_names,
            "section_row_count" => $section_row_count,
            "policy_info" => $policy_info,
            "make_fields" => $make_fields,
            "data_hier" => $data_hier,
            "data_list" => $data_list
        );
        display_template("study_contents_syozokubetu.tpl", $template_vars);
        die;
    }
    //==========================================================================
    // 出力２）エクセル出力の場合(under php version 5.1.6)
    //==========================================================================
    if ($output_type=="excel" && phpversion() < '5.1.6') {
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=unit_history.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        ob_start();
        $template_vars = array(
            "section_names" => $section_names,
            "section_row_count" => $section_row_count,
            "policy_info" => $policy_info,
            "make_fields" => $make_fields,
            "data_hier" => $data_hier,
            "data_list" => $data_list
        );
        display_template("study_contents_syozokubetu_excel.tpl", $template_vars);
        $data = mb_convert_encoding(ob_get_contents(), "SJIS-win", 'eucJP-win');
        ob_end_clean();
        echo $data;
        die;
    }

    //==========================================================================
    // 出力３）エクセル出力の場合(upper php version 5.1.6)
    // PHP4はメソッドチェーン（  A()->B()->C() など ）があるとコンパイルエラー
    //==========================================================================
    if ($output_type=="excel" && phpversion() >= '5.1.6') {
        require_once './libs/phpexcel/Classes/PHPExcel.php';

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $xxds = $excel->getDefaultStyle();
        $xxft = $xxds->getFont();
        $xxft->setName(utf8('ＭＳ Ｐゴシック')); // 標準フォント設定
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle(utf8("所属別受講状況表"));

        //--------------------------------------------------
        // １列目
        //--------------------------------------------------
        $sheet->setCellValueByColumnAndRow(0, 1, utf8(GG_CLASS_NM));
        $sheet->setCellValueByColumnAndRow(1, 1, utf8(GG_ATRB_NM));
        $sheet->setCellValueByColumnAndRow(2, 1, utf8(GG_DEPT_NM));
        $col = 2;
        foreach ($make_fields as $mfield) {
            if ($mfield["disp"]!="yes") continue;
            $col++;
            $sheet->setCellValueByColumnAndRow($col, 1, utf8($mfield["jp"]));
        }
        $col_max = $col;
        for ($idx=0; $idx<=$col_max; $idx++) {
            $xxst = $sheet->getStyleByColumnAndRow($idx, 1);
            $xxal = $xxst->getAlignment();
            $xxal->setHorizontal("center");
        }

        $set_area = $sheet->getStyle("A1:" . chr(0x40 + $col_max+1) . "1");
        $xxfi = $set_area->getFill();
        $xxfi->SetFillType("solid");
        $xxsc = $xxfi->getStartColor();
        $xxsc->setRGB("ffe7d2"); // 薄いオレンジ


        // class別ループ
        $cur_row = 1;
        foreach ($data_hier as $class_id => $data_hier2) {
            $class_counter = 0;
            // atrb別ループ
            foreach ($data_hier2 as $atrb_id => $data_hier3) {
                $attr_counter = 0;
                // dept別ループ
                foreach ($data_hier3 as $dept_id => $data_hier4) {
                    $class_counter++;
                    $attr_counter++;
                    $cur_row++;
                    $data_row = $data_list[$class_id."_".$atrb_id."_".$dept_id];
                    $section_name = $section_names[$class_id."_".$atrb_id."_".$dept_id];
                    if ($class_counter == 1) {
                        $sheet->setCellValueByColumnAndRow(0, $cur_row, utf8($section_name["class_nm"]));
                    }
                    if ($attr_counter == 1) {
                        $sheet->setCellValueByColumnAndRow(1, $cur_row, utf8($section_name["atrb_nm"]));
                    }
                    $sheet->setCellValueByColumnAndRow(2, $cur_row, utf8($section_name["dept_nm"]));

                    $col = 2;
                    foreach ($make_fields as $mfield) {
                        if ($mfield["disp"]!="yes") continue;
                        $col++;
                        $cell_data = $data_row[$mfield["field"]];
                        if ($col==$col_max) $cell_data = $cell_data / 100;
                        $sheet->setCellValueByColumnAndRow($col, $cur_row, $cell_data);
                    }
                }

                //--------------------------------
                // atrb別小計（dept合計）
                //--------------------------------
                $cur_row++;
                $data_row = $data_list[$class_id."_".$atrb_id];
                $section_name = $section_names[$class_id."_".$atrb_id];
                $sheet->setCellValueByColumnAndRow(2, $cur_row, utf8("小計"));

                $col = 2;
                foreach ($make_fields as $mfield) {
                    if ($mfield["disp"]!="yes") continue;
                    $col++;
                    $cell_data = $data_row[$mfield["field"]];
                    if ($col==$col_max) $cell_data = $cell_data / 100;
                    $sheet->setCellValueByColumnAndRow($col, $cur_row, $cell_data);
                }
                $sheet->mergeCells("B" . ($cur_row - $section_row_count[$class_id."_".$atrb_id]+1). ":" . "B" . $cur_row);
            }

            //--------------------------------
            // class別小計（atrb合計）
            //--------------------------------
            $cur_row++;
            $data_row = $data_list[$class_id];
            $section_name = $section_names[$class_id];
            $sheet->setCellValueByColumnAndRow(1, $cur_row, utf8("小計"));
            $sheet->setCellValueByColumnAndRow(2, $cur_row, "");
            $sheet->mergeCells("B" . $cur_row . ":" . "C" . $cur_row);

            $col = 2;
            foreach ($make_fields as $mfield) {
                if ($mfield["disp"]!="yes") continue;
                $col++;
                $cell_data = $data_row[$mfield["field"]];
                if ($col==$col_max) $cell_data = $cell_data / 100;
                $sheet->setCellValueByColumnAndRow($col, $cur_row, $cell_data);
            }
            $sheet->mergeCells("A" . ($cur_row - $section_row_count[$class_id]+1). ":" . "A" . $cur_row);
        }

        //--------------------------------
        // 全体合計（class合計）
        //--------------------------------
        $cur_row++;
        $data_row = $data_list["last_total"];
        $section_name = $section_names[$class_id];
        $sheet->setCellValueByColumnAndRow(0, $cur_row, utf8("合計"));
        $sheet->setCellValueByColumnAndRow(1, $cur_row, "");
        $sheet->setCellValueByColumnAndRow(2, $cur_row, "");
        $sheet->mergeCells("A" . $cur_row . ":" . "C" . $cur_row);

        $col = 2;
        foreach ($make_fields as $mfield) {
            if ($mfield["disp"]!="yes") continue;
            $col++;
            $cell_data = $data_row[$mfield["field"]];
            if ($col==$col_max) $cell_data = $cell_data / 100;
            $sheet->setCellValueByColumnAndRow($col, $cur_row, $cell_data);
        }

        //--------------------------------
        // 仕上げ
        //--------------------------------
        // ヘッダ列はセンター寄せ
        $xxst = $sheet->getStyle("A1:" . "C".($cur_row));
        $xxal = $xxst->getAlignment();
        $xxal->setVertical("center");
        // 率の列は%フォーマットへ
        $set_area = $sheet->getStyle(chr(0x40 + $col_max+1) . "2:" . chr(0x40 + $col_max+1) . $cur_row);
        $xxnf = $set_area->getNumberFormat();
        $xxnf->setFormatCode("0.0%");
        // 罫線は細くする
        $set_area = $sheet->getStyle("A1:" . chr(0x40 + $col_max+1) . $cur_row);
        $xxbo = $set_area->getBorders();
        $xxab = $xxbo->getAllBorders();
        $xxab->setBorderStyle(thin);
        ob_clean();
        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="unit_history.xls"');
        $writer->save("php://output");
        die;
    }
}

?>
