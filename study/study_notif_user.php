<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【ユーザ側】お知らせ一覧 関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyFolderModel.php';
require_once 'model/StudyNotificationModel.php';


default_view();
die;




//**************************************************************************************************
// ユーザ側お知らせ一覧画面処理
//**************************************************************************************************
function default_view() {
    //------------------------------------------------------
    // ログイン者へのお知らせ一覧検索
    //------------------------------------------------------
    $notif_full_list = StudyNotificationModel::get_user_notif_list($_REQUEST["is_show_archive"]);
    $folder_list = StudyFolderModel::get_folder_list(); // 講座一覧。講座名作成用

    //------------------------------------------------------
    // ページング
    // 面倒なのと、膨大に件数があるわけではない、ということで、全件検索した結果から出力分だけ切り出す
    //------------------------------------------------------
    $per_page_size = (int)@$_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;
    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;

    $total_count = count($notif_full_list);
    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;

    $notif_list = array();
    if ($per_page_size < 0) {
        $notif_list = $notif_full_list;
    } else {
        $idx = 0;
        $start_num = $per_page_size * ($page_num-1) + 1;
        $end_num = $start_num + $per_page_size - 1;
        foreach ($notif_full_list as $row) {
            $idx++;
            if ($idx<$start_num || $idx>$end_num) continue;
            $notif_list[] = $row;
        }
    }

    foreach ($notif_list as $key=>$notif) {
        $notif_list[$key]["folder_joint_name"] = implode("＞", StudyCommonModel::get_folder_parents("name", $notif["folder_id"], $folder_list));
        $dt_start = date("Y/m/d", strtotime($notif["start_date"]));
        $dt_update = date("Y/m/d", strtotime($notif["update_datetime"]));
        $notif_list[$key]["display_notif_ymd"] = ($dt_start > $dt_update ? $dt_start : $dt_update); // 画面表示は更新日と公開開始日の大きいほう
    }


    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';

    // ページング
    $smarty->assign("page_num", (int)$page_num);
    $smarty->assign("per_page_size", (int)$per_page_size);
    $smarty->assign("total_count", (int)$total_count);
    $smarty->assign("last_page", (int)$last_page);

    $smarty->assign("notif_list", $notif_list);
    $smarty->assign("is_show_archive", $_REQUEST["is_show_archive"]);
    $smarty->display("study_notif_user.tpl");
}
?>
