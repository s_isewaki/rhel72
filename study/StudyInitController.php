<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【共通】ここから、すべての始まり
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

// 開発モード
// １）SQLエラーでエラー画面に遷移せずに停止させたい場合
// ２）受講者管理画面で、テスト削除ボタンが表示されるようになる
// ３）権限番号を表示するようになる
//define("DEV_MODE", 1);

define("WBR", "&#8203;"); // HTML強制改行文字



//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// 全サーバリクエストの開始
// 各URLで呼び出されるファイルは、最初に必ずこのソースファイルをインクルードする。
//
// URLファイルは「comedix/study_xxxxxx.php」でなく、「comedix/study/study_xxxxxx.php」 である。
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■


define("GG_TITLEBAR_TITLE", "CoMedix バリテス | ");



//******************************************************************************
// comedix直下へソースコードを配置しないで良くするための仕組み --- ディレクトリパス調整   【chdir】
//
// カレントディレクトリを強制変更しないと、comedix直下のLIB系ファイルのロードができない。
//
// バリテス eラーニングのURLは、comedix/study/ というように、comedixよりひとつ下階層になる。
// よってこのファイルは「comedix/study/StudyInitController.php」であり、ひとつ下階層に存在することになる。
// クライアントからのURL呼出があって、apacheがphpエンジンを動かして、phpエンジンがphpソースをコールしたとき、
// コールされたphpソースの場所がカレントディレクトリになるわけだが、
// もしカレントディレクトリがこのファイルと同じディレクトリなら、ひとつ上にのぼって、comedix直下とする。という話。
//******************************************************************************
define("GG_BASE_FOLDER", basename(dirname(__FILE__))); // 通常 「study」となる。
set_include_path("./".GG_BASE_FOLDER . PATH_SEPARATOR . get_include_path()); // 上階層にいく前に、このディレクトリをLIBインクルード先の第一優先で追加。
if (dirname(__FILE__) == getcwd()) chdir("../"); // カレントディレクトリがstudyなら、ひとつ上階層のcomedixへカレントディレクトリ移動




//******************************************************************************
// 資料・テキストなどのテスト時利用のコンテンツファイル保存先
//
// このファイルは、comedix直下の次の階層にある。カレントディレクトリがcomedix直下に変わっているので、
// 保存先をひとつ下の階層へ。具体的には「study/tmp/」となる。
// 別のディレクトリを指定したい場合は変えてもいいが、データベースデータの変更が必要になる場合に注意。
// それより前に、前提的に、ファイル格納場所はHTTP-URLが到達する場所でないといけないので、
// この下の設定は変更せず、「comedix/study/tmp」に対し、linux側にシンボリックリンクを張るように。
// 各ディレクトリは、事前に書き込み可能にすること。
//******************************************************************************
define("GG_ATTACH_BASE", GG_BASE_FOLDER."/tmp/"); // tmpという名前にされているが、大事な永続ファイルが格納される重要な場所
define("GG_ATTACH_HOME", GG_ATTACH_BASE."test/"); // テストの画像格納場所
define("GG_FILE_HOME", GG_ATTACH_BASE."contents/"); // 研修・講義の画像格納場所
define("GG_TEST_IMAGE_WORK_HOME", GG_ATTACH_BASE."work/"); // テスト画像の保存前一時格納場所





//******************************************************************************
// エラー系処理関数。これらには批判的に記載している。多用に注意。
//
// err_exit()：
//     よく利用する強制エラー画面転送。
//     サーバ側処理途中で強制的に落とすため、画面の復元ができなくなる上、
//     sessionパラメータを渡していないので戻れない。
//     サーバセッションを切るわけではないため、ユーザは戻るボタンで回帰可能な場合もあるが、
//     戻れるかどうかもわからない。
//     エラーの原因もトレースしにくい。
//     一斉に各画面が利用できなくなるので、可能な限りは現画面でエラー表現すべきで、基本的に非利用が望ましいが、現状は利用するほか無い。
// err_continue()
//     DOCTYPEヘッダ部の出力を無視した用途となってしまうのと、
//     現状態、前画面の出力状態・呼出し状態を考慮しない単発関数であるので、利用場所を選ぶ。利用可能な局面が非常に限られる。
//     history.back()もwindow.close()も、同じ画面で使っても効くときと効かないときがあり得る。
//     これを汎用的なものとして多用すると、上かぶせのエラーが発生したり、解析に手間取る不具合を生みやすい。
// business_exception()
//     err_continue()へのラッパー。そもそも命名も紛らわしいが、そもそもこのラッパーを利用する必要性自体が無い。
// fatal_exception()
//     err_exit()に同じ。（開発用のものか？）DBエラー時にコールしているが、
//     err_exit()を呼んで即エラー画面に飛ぶので、何の為に存在するか、、、あまり意味はないと見える。
//******************************************************************************
function err_exit(){
    if (defined("DEV_MODE")) die;
    echo '<script type="text/javascript" src="js/showpage.js"></script>';
    echo '<script type="text/javascript">showErrorPage(window);</script>';
    exit; //アプリ終了
}
function err_continue($message, $handle_type = "back"){
    echo '<script type="text/javascript">alert("'.$message.'");</script>';
    if ($handle_type=="back") echo '<script type="text/javascript">history.back();</script>';
    if ($nandle_type=="close") echo '<script type="text/javascript">window.close();</script>';
    exit; // アプリ終了
}
function business_exception($message, $handle_type="back") {
    err_continue($message, $handle_type); // この関数でアプリexit
}
function fatal_exception($errmsg, $pos) {
    echo $pos."<br>";
    echo $errmsg."<br>";
    echo "(echo at StudyInitController.php - fatal_exception())";
    err_exit(); // この関数でアプリexit
}





//******************************************************************************
// 共通コール
// 「about_xxxx」系などは、カレントディレクトリをcomedix直下に変更してから読み込まなければ、
//  インクルードPATHを指定しても無理。その先の先の読み込みのどこかでエラーとなる。
//******************************************************************************
require_once("about_comedix.php"); // DB接続設定を呼んだりする。多用するh()関数もここに。
require_once("about_certify.php"); // ログイン確認・数少ないセッション変数管理としても。重要。
require_once("./common_log_class.ini"); // ログの排出関連。ログは、comedix/log/ へ出力される。
require_once 'StudyMdbWrapper.php'; // DBユーティリティクラス読み込み
require_once 'model/StudyCommonModel.php'; // 多用するDBアクセス処理系を中心に関数群が掲載されている。




//******************************************************************************
// ロガー。DBエラー時と、たまにその他で利用。
//
// これまでログファイルに記載したいという理由から、実行時ファイル名を以下のように、
// コンストラクタで指定する必要があったため、これまでは全コントロール、全クラスで、
// ロガーインスタンスを作成していた。そうしないと、DB系エラー発生個所が、すべてStudyMdbWrapper.phpになってしまうため。
// しかし、StudyMdbWrapper::get_backtrace()を準備した。PHP4.3以降、これでもう各所で$loggerを作成する必要は無い。
// なお、StudyMdbWrapper以外でログを出したい場合は、logger_info()、logger_error() 関数を利用するとよい。その他必要に応じて追加を。
//******************************************************************************
$log = new common_log_class(basename(__FILE__));

function logger_info($msg) {
    global $log;
    $log->info($msg);
}
function logger_error($msg) {
    global $log;
    $log->error($msg);
}







//******************************************************************************
// セッションチェック
//******************************************************************************

//====================================
// セッションID。必ず渡ってくる約束。
// プログラム内、画面側の多数箇所で所構わず参照されるため、define値にしておく。
// いちいち引数で渡したり、いちいち$smarty変数にバインドしないように。
//====================================
$session = $_REQUEST["session"];    //セッション
define("GG_SESSION_ID", $session);
$study_admin_auth = 0;
//====================================
// セッションIDをチェック
//====================================
$fname = $_SERVER['PHP_SELF'];
if (!defined("ASSEMBLY_ACTUAL_ENTRY_STANDALONE_AND_IGNORE_SESSION_MODE")) {
    $session = certify("","",$session, $fname);
    $session = qualify_session($session,$fname);
    if($session == "0"){
        $log->warn("invalid session");
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
    //====================================
    // セッション者にバリテス操作権限があるかをチェック
    //====================================
    $study_auth=check_authority($session,75,$fname);
    if($study_auth=="0"){
        $log->warn("Nothing authority 75");
        err_exit();
        exit;
    }
    //====================================
    // セッション者が管理者権限を持つかどうかを判定
    //====================================
    $study_admin_auth = (int)check_authority($session,76,$fname);
}
define("GG_ADMIN_FLG", $study_admin_auth); // 管理権限ありなら1(すなわちtrue)、管理権限なしなら0(すなわちfalse)



//******************************************************************************
// DBアクセスラッパー関数群
//
// DBへは短く簡潔にアクセスしたい。
// DB接続などの事前準備などを意識することなく、いつでも、どこからでも、安全にアクセスしたい。
// いろんな箇所で、$mdb2 = new StudyMdbWrapper()ということをしたくない。
// $mdb2を関数引数やメンバ変数保持などを行って、管理しなければいけない苦労を撤廃したい。
// $mdb2->find(xxxx, yyyy, zzzz) というように、毎々、冗長な不要引数をつけたくない。
//
// このPHPWebサイトは、マルチスレッドで動作していないため、
// 各所各所でクラスをnew()でインスタンス化などしなくとも、安全にスレッドセーフであるし、
// DBコネクションもひとつだけしか接続しない。
// つまり例えばDBセレクトカーソルでループしながら、別のコネクションでUPDATE、などということは行わない。
// DBコネクションはシングルトンひとつで十分。というか、よっぽどのことがなければ、複数DB接続は行わないように。
//
// よって本サイトでは、以下を利用のこと。
//******************************************************************************

$mdb2 = StudyMDBWrapper::create();

function dbRemainParams() {
    global $mdb2;
    $mdb2->isRemainParams = 1;
}
function dbClearParams() {
    global $mdb2;
    $mdb2->spTypes = array();
    $mdb2->spParams = array();
    $mdb2->isRemainParams = 0;
}
function dbPStr($fieldValue, $fieldType = "text") {
    global $mdb2;
    $fieldName = "SP".(count($mdb2->spTypes) + 1);
    array_push($mdb2->spTypes, $fieldType);
    $mdb2->spParams[$fieldName] = $fieldValue;
    return ":".$fieldName;
}
//function dbPInt($fieldValue) { // 文字列を渡すと落ちるので廃止 代替 dbPIntCを利用
//    global $mdb2;
//    return dbPStr($fieldValue, "integer");
//}
function dbPIntC($fieldValue) {
    if ($fieldValue!="") $fieldValue = (int)@$fieldValue;
    global $mdb2;
    return dbPStr($fieldValue, "integer");
}
function dbPBool($boolValue) {
    if ($boolValue) return "true";
    return "false";
//    global $mdb2;
//    return dbPStr($fieldValue, "boolean"); // PHP4ではNGにつき、固定値を渡す。
}
function dbPTime($fieldValue) {
    global $mdb2;
    return dbPStr($fieldValue, "timestamp");
}
function dbPFloat($fieldValue) {
    global $mdb2;
    return dbPStr($fieldValue, "float");
}
function dbFind($sql) {
    global $mdb2;
    return $mdb2->find($sql, $mdb2->spTypes, $mdb2->spParams);
}
function dbExec($sql) {
    global $mdb2;
    return $mdb2->update($sql, $mdb2->spTypes, $mdb2->spParams);
}
function dbGetTopRow($sql) {
    global $mdb2;
    $rows = dbFind($sql);
    foreach ($rows as $row) return $row;
    return "";
}
function dbGetOne($sql) {
    global $mdb2;
    $rows = $mdb2->find($sql, $mdb2->spTypes, $mdb2->spParams, MDB2_FETCHMODE_ORDERED);
    foreach ($rows as $row) return $row[0];
    return "";
}
function dbSqlDump($sql){
    global $mdb2;
    $keys = array();
    foreach ($mdb2->spParams as $k=>$v) $keys[] = $k;
    for ($idx = count($keys)-1; $idx>=0; $idx--) {
        $k = $keys[$idx];
        $sql = str_replace(":".$k, "'".$mdb2->spParams[$k]."'", $sql);
    }
    return $sql;
}
function dbEscapePattern($s) {
    global $mdb2;
    $con = $mdb2->get();
    return $con->escapePattern($s);
}
function dbGetNextSequence($sequence_name) {
    global $mdb2;
    return $mdb2->getNextSequence($sequence_name);
}
function dbBeginTransaction() {
    global $mdb2;
    $mdb2->beginTransaction();
}
function dbEndTransaction() {
    global $mdb2;
    $mdb2->endTransaction();
}




//******************************************************************************
// ログイン者のID
// こちらも多方面で多用するため、define化。
// ※いちいち引数で渡さないように。
// ※管理者がユーザ画面を操作している場合などが混在するため、
//   操作対象者のemp_idなのか、ログイン者のemp_idなのかをはっきりさせないと、あぶなっかしい。
//******************************************************************************
if (defined("ASSEMBLY_ACTUAL_ENTRY_STANDALONE_AND_IGNORE_SESSION_MODE")) {
    define("GG_LOGIN_EMP_ID", $_REQUEST["login_emp_id"]);
} else {
    $sql = "select emp_id from empmst where emp_id = (select emp_id from session where session_id = ".dbPStr(GG_SESSION_ID).")";
    define("GG_LOGIN_EMP_ID", dbGetOne($sql));
}




//******************************************************************************
// 教育責任者と教育担当者の判定
//******************************************************************************

//====================================
// 管理者でない場合、教育責任者かどうか確認
// 教育責任者は、管理者と同じ機能の、「閲覧」が可能
// ただし、「教育責任者」「教育担当者」タブは操作させない。
//====================================
$study_admin_flg = 0;
$study_tantou_flg = 0;
$study_tantou_folders = array();
$study_tantou_branches = array();
if ((int)dbGetOne("select count(*) from study_edu_admins where emp_id = ".dbPStr(GG_LOGIN_EMP_ID))) $study_admin_flg = 1;

//====================================
// 教育担当者かどうか確認。教育担当者は、所定講座の中の研修・講義をいじれる。
//====================================
$rows = dbFind("select * from study_edu_tantou_folders where emp_id = ".dbPStr(GG_LOGIN_EMP_ID));
foreach ($rows as $row) {
    if ($row["folder_id"]) $study_tantou_folders[]= $row["folder_id"];
}
$rows = dbFind("select * from study_edu_tantou_branches where emp_id = ".dbPStr(GG_LOGIN_EMP_ID));
foreach ($rows as $row) {
    if ($row["class_id"]) $study_tantou_branches[]= array(
        "class_id"=>$row["class_id"], "atrb_id"=>$row["atrb_id"], "dept_id"=>$row["dept_id"], "room_id"=>$row["room_id"]
    );
}
if (count($study_tantou_folders) || count($study_tantou_branches)) $study_tantou_flg = 1;
define("GG_STUDY_ADMIN_FLG", $study_admin_flg); // 教育責任者かどかのboolean値
define("GG_STUDY_TANTOU_FLG", $study_tantou_flg); // 教育担当者かどうかのboolean値




//******************************************************************************
// 「部・課・科」という所属タイトル名
//******************************************************************************
$db_class_name = dbGetTopRow("select * from classname");
define("GG_CLASS_NM", @$db_class_name["class_nm"]); // 部
define("GG_ATRB_NM", @$db_class_name["atrb_nm"]);   // 課
define("GG_DEPT_NM", @$db_class_name["dept_nm"]);   // 科
define("GG_ROOM_NM", @$db_class_name["room_nm"]);   // 室 値がなければ３階層まで。
define("GG_CLASS_CNT", @$db_class_name["class_cnt"]); // 3階層or4階層 20141217TN

define("GG_BARITESS_NENSUU_OPT_FLAG", dbGetOne("select value from system_config where key = 'baritess.nensuu_opt_flag'"));




//******************************************************************************
// コンテンツ作成ポリシーを取得。study_contentsテーブルのpolicy_code値を分解して作成する。
// 集合研修・ｱﾝｹｰﾄ・ﾃｽﾄのどれを、どの組み合わせで、どの順番で行うかを示す。
//******************************************************************************
function get_policy_info($policy_code) {
    $ary = explode("/", $policy_code);
    $funcs = array("A"=>"assembly", "Q"=>"questionnaire", "T"=>"test");
    $jp_list = array("A"=>"集合研修", "Q"=>"ｱﾝｹｰﾄ", "T"=>"ﾃｽﾄ");
    $group_indexes = array();
    $ret = array();
    $col = 0;
    foreach ($ary as $section) {
        $col++;
        $func       = substr($section, 0, 1);
        $hissu_mark = substr($section, 1, 1);
        $group      = substr($section, 2, 1);

        if ($func!="A" && $func!="Q" && $func!="T") continue;
        if ($hissu_mark!="+" && $hissu_mark!="-") continue;
        if (!$group) continue;
        $group_indexes[$group] = ((int)@$group_indexes[$group]) + 1;
        $ret[$funcs[$func]] = array(
            "jp"=>$jp_list[$func], // 日本語
            "col"=>$col, // 列順
            "hissu_mark"=>$hissu_mark, // 必須列なら"+"
            "group"=>(int)$group, // OR条件グループ
            "group_index"=>$group_indexes[$group] // OR条件グループ内順番
        );
    }
    foreach ($ret as $func=>$r) {
        $ret[$func]["group_size"] = $group_indexes[$r["group"]]; // OR条件グループの構成数
    }
    return $ret;
}




//******************************************************************************
// 上記get_policy_info()で作成した構造体をもとにして、以下を判定する
//
// ●AND条件かどうかの値（is_multi_pattern）
//   ※「XおよびY」「XおよびY」「XおよびYおよびZ」「YおよびZ」 形式があればがtrue
//   ※それ以外はfalse
//
// ●OR条件グループの最大数（max_group_size）
//   ※「XまたはY」「XまたはZ」「YまたはZ」構造なら2
//   ※「XまたはYまたはZ」形式なら3
//   ※ それ以外は1
//
// ●OR条件グループごとの機能の仲間と出現順（group_order）
//   そもそも、ANDというものは、以下のように考える。
//   ⇒[第一グループ] のみ
//   ⇒[第一グループ] および [第二グループ]
//   ⇒[第一グループ] および [第二グループ] および [第三グループ]
//
//   ひとつひとつのグループはOR条件である。いいかえると、例えば、
//   ⇒例）[第一(X or Y or Z)グループ]
//   ⇒例）[第一(X or Y)グループ] および [第二(Z)グループ]
//   ⇒例）[第一(X)グループ] および [第二(Y or Z)グループ]
//   ⇒例）[第一(X)グループ] および [第二(Y)グループ] および [第三(Z)グループ]
//
//   そこで、以下のように返す。出現順も重要。
//   ⇒例）第一グループが「XまたはY」で、第二グループが「およびZ」の場合は、array("1" => array("X","Y"), "2" => array("Z"))
//   ⇒例）第一グループが「XまたはZ」で、第二グループが「およびY」の場合は、array("1" => array("X","Z"), "2" => array("Y"))
//   ⇒例）第一グループが「XまたはYまたはZ」の場合は、array("1" => array("X","Y","Z"))
//   ⇒例）第一グループが「X」、第二が「Y」第三が「Z」の場合は、array("1" => array("X"), "2"=>array("Y"), "3" => array("Z"))
//******************************************************************************
function get_policy_total_info($policy_info) {
    $hissu_group_sizes = array();
    $group_order = array();
    $max_group_size = 0;
    foreach ($policy_info as $func => $info) {
        if ($info["hissu_mark"]!="+") continue;
        $group = $info["group"];
        $hissu_group_sizes[$group] = $info["group_size"];
        $max_group_size = max($max_group_size, $info["group_size"]);
        $group_order[(int)$info["group"]][] = $func;
    }
    $is_multi_pattern = 0;
    if (count($hissu_group_size)>=2 && $max_group_size >=2) $is_multi_pattern = 1;
    return array("max_group_size"=>$max_group_size, "is_multi_pattern"=>$is_multi_pattern, "group_order"=>$group_order);
}




//******************************************************************************
// パーセント率を算出する
//
// 引数のうち、
// ・$decimal:小数点数
// ・$bunritu:100ならパーセント表示、1を指定したら1が100%という意味
// 所属別一覧画面では「100%は100.0」で出力 -> get_percent(bunsi, bunbo, 1, 100) のように指定する
// 所属別エクセルでは「100%は1.000」で出力 -> get_percent(bunsi, bunbo, 3, 1)   のように指定する
// この率は概算率でなく、達成度とする。つまり、切り捨てである。
// ※例えば1000人中999人合格の場合、四捨五入だと100%になってしまう。
// ※3人中2日合格の場合、66.7%ではない。第一、66.7%に達していない。66.6%を返す。
// ※1000人中1人のみ合格の場合、切り捨てゼロになる。合格1件以上あるのに切り捨ててゼロになる場合は、最小値を返す。
// ※今後200%など100%を超える表現が必要なとき、破綻する。
//******************************************************************************
function get_percent($bunsi, $bunbo, $decimal=2, $bunritu=100) {
    $bunsi = (int)$bunsi;
    $bunbo = (int)$bunbo;
    $ret = 0;
    if (((int)$bunsi) && ((int)$bunbo)) {
        $ruijo = ($decimal ? pow(10, $decimal) : 1);
        $percent = (int)($ruijo * $bunritu * $bunsi / $bunbo);
        if ($bunsi>0 && $percent==0) $ret = 1 / $ruijo;
        else $ret = $percent / $ruijo;
    }
    return number_format($ret, $decimal);
}





//******************************************************************************
// $smartyに代わる即席テンプレートエンジンの構成
// コントロール側の変数が簡単に漏えいしないように、
// あるいは、viewPHPにどの変数を引き渡したかを明確にするように、この関数を介することにした。
// ここで呼ばれるtplは、smartyファイルではない。通常のPHPファイルである。
// この「テンプレートPHP」の中でも、$smartyのassign変数を見ている場合があるかもしれないが、それは自由とする。
// 現時点では、この$smarty第二引数は、常にカラでコールされる。
//******************************************************************************
function display_template($tpl_name, $template_args, $smarty=null) {
    extract($template_args);
    require_once("view/".$tpl_name);
}




//====================================
// 日付コンバート関連
// 画面⇒サーバはYYYYMMDDHHMMなのに、サーバ⇒画面へは日本語プラス配列というややこしい状況である。時間切れにつき精査断念。
//====================================
function conv_ymd($ymd, $regex="Y/m/d") {
    if (!strlen($ymd)) return "";
    if (strlen($ymd)==8) return date($regex, strtotime($ymd));
}
function conv_ymdhm_to_jp_and_time($ymdhm) {
    if (!$ymdhm || strlen($ymdhm)!=12) return array("","","");
    $ymd = substr($ymdhm, 0, 4) . "年" . substr($ymdhm, 4, 2) . "月" . substr($ymdhm, 6, 2) . "日";
    return array($ymd, substr($ymdhm, 8, 2), substr($ymdhm, 10, 2));
}
function conv_ymdhm_to_jp_and_time2($ymdhm) {
    if (!$ymdhm || strlen($ymdhm)!=12) return "";
    $ymd = substr($ymdhm, 0, 4) . "年" . substr($ymdhm, 4, 2) . "月" . substr($ymdhm, 6, 2) . "日";
    return $ymd." ".substr($ymdhm, 8, 2)."時".substr($ymdhm, 10, 2)."分";
}
function conv_ymdhm_to_slash_ymd($ymdhm) {
    if (!$ymdhm || strlen($ymdhm)!=12) "";
    return substr($ymdhm, 0, 4) . "/" . substr($ymdhm, 4, 2) . "/" . substr($ymdhm, 6, 2);
}




//====================================
// eucをutf8へ。エクセル対策。
//====================================
function utf8($euc) {
    return mb_convert_encoding($euc, "UTF-8", "eucJP-win");
}
function utf16hex2str($hex) {
    $len = (int)(strlen($hex) / 2);
    $out = array();
    for ($idx=0; $idx<$len; $idx++) {
        $out[] = mb_convert_encoding(pack("n", hexdec(substr($hex, $idx*4, 4))), "eucJP-win", "UTF-16");
    }
    return implode("",  $out);
}

// おわり

function day_diff($date_start,$base_date ,$date_minus) {
   //$now = date("Y/m/j");
    $date1= strtotime($base_date);
    $date2=strtotime($date_start);

    $month1=date("Y",$date1)*12+date("m",$date1);
    $month2=date("Y",$date2)*12+date("m",$date2);

    $daydiff = $month1 - $month2-$date_minus;
    // 戻り値
    return $daydiff;
    }
