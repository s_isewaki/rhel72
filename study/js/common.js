var dlgResizableOption = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
var str = function(s) { if (!s && s!=0) return ""; return s; }
var int = function(s) { if (!s) return 0; var num = parseInt(s,10); if (!num || isNaN(num)) return 0; return num; };
var ee = function(id) { return document.getElementById(id); }
var trim = function(s) { if (s==undefined) return ""; s+=""; return s.replace(/^[\s　]+|[\s　]+$/g, ''); }
var isIE6 = (navigator.userAgent.toUpperCase().indexOf("MSIE 6")>=0) ? true : false;
var htmlEscape = function(str) { return trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\"/g,"&quot;").replace(/'/g,"&#39;"); }
var tryFocus = function(id) { if (!id) return; var o=ee(id); if(!o || o.disabled || o.readOnly) return; o.focus(); try{o.select();}catch(ex){} }
var isEnterKey = function(elem, evt){if(!elem.disabled && !elem.readOnly) var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==13; }
var utf16Hex = function(s){var out=[]; for (var idx=0; idx<s.length; idx++) out.push(("0000"+s.charCodeAt(idx).toString(16)).slice(-4)); return out.join("");}
var getSelectionValue = function(obj) { if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].value; }
var setSelection = function(obj,v){var o=obj.options; for(var i=0; i<o.length; i++) if(o[i].value==v) {obj.selectedIndex=i;return;}obj.selectedIndex=0;}
var setComboColor = function(cmb) { if (!cmb) return; cmb.className = cmb.options[cmb.selectedIndex].className; }
var btnDisabled = function(disabled, id) {
    if (disabled) { $("#"+id).attr("disabled", "dissabled").addClass("btn-off"); }
    else { $("#"+id).removeAttr("disabled").removeClass("btn-off"); }
}







//**************************************************************************************************
// ポップアップ画面から親画面をりロードさせる
//**************************************************************************************************

var refreshRequireFormName = "";
//var refreshRequireFormParams = {};
function setRefreshFormName(formName, events) {
//    refreshRequireFormParams = events;
    refreshRequireFormName = formName;
}
function parentRefreshRequest(formName, events) {
    if (!opener || !opener.setRefreshFormName || !opener.document || !opener.document[formName]) return;
    if (opener==window) return;
    opener.setRefreshFormName(formName, events);
}
function grandParentRefreshRequest(formName, events) {
    if (!opener || !opener.opener || !opener.opener.setRefreshFormName || !opener.opener.document || !opener.opener.document[formName]) return;
    if (opener==window) return;
    if (opener.opener==window) return;
    try { opener.opener.setRefreshFormName(formName, events); } catch(ex) { alert("ng"); }
}

var gTimer = setInterval(function() {
    if (!refreshRequireFormName) return;
    var frm = document[refreshRequireFormName];
    if (frm && frm.tagName == "FORM") {
        if (gTimer) clearInterval(gTimer);
        gTimer = 0;
        frm.submit();
    }
}, 100);

var windowStocker = {};
function window_open(url, wname, width, height) {
    var x = (screen.width - width) / 2;
    var y = (screen.height - height) / 2;
    opt = "left=" + x + ",top=" + y + ",width=" + width + ",height=" + height + dlgResizableOption;
    if (windowStocker[wname]) {
        try { windowStocker[wname].close(); } catch(ex){}
    }
    windowStocker[wname] = window.open(url, wname, opt);
}

//**************************************************************************************************
// オンロード処理
//**************************************************************************************************
var floatingpage=0; // このページのiframe
var menupage = 0; // Comedixの袖メニューiframe
var pwin = 0; // Comedixフレームページ
$(document).ready(function() {
    pwin = window.parent; // Comedixフレームページ
    if (pwin && window==parent) pwin = 0;
    if (pwin) floatingpage = pwin.document.getElementById("floatingpage");
    if (pwin) menupage = pwin.document.getElementById('menublock');

    // ■ ボタン以外の入力コントロールでの、エンターキー無効化
    $('input[type!="submit"][type!="button"]').keypress(function(e){
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) return false;
        return true;
    });

    // ■ 画面リサイズ調整
    resized();
    // ■ ナビ初期化
    gNavi.initialize();
    // ■ 親画面へリサイズイベント付与
    if (pwin) $(pwin).bind("resize", function() { if (typeof resizeIframeEx2 === "function") resizeIframeEx2(); });
});





//**************************************************************************************************
// ページリサイズ調整関連
//**************************************************************************************************



//------------------------------------------------
// 画面リサイズイベント、スクロール表の高さ調整
//------------------------------------------------
function resized(t) {
    $(".auto_height").each(function(){
        var mh = this.getAttribute("minHeight");
        var hm = this.getAttribute("heightMargin");
        if (!hm) hm = 0;
        this.style.height = "auto";
        if (!mh) return;
        var h = Math.max(mh, this.offsetHeight - hm + 10);
        this.style.height = (h) + "px";
    });
    $(".scroll_div").each(function(){
        var hm = this.getAttribute("heightMargin");
        if (!hm) hm = 0;
        var wh = window.innerHeight;
        if (!wh && document.documentElement) wh = document.documentElement.clientHeight;
        if (!wh) wh = document.getElementsByTagName("body")[0].clientHeight;
        wh = wh - this.offsetTop - 10 - hm;
        if (wh < 1) return;
        this.style.height = (wh) + "px";
    });
    if (!t) setTimeout(function(){ resized(1); }, 100);
}





//**************************************************************************************************
// 日付コントロール保存時チェック、開始＆終了日はカラか両方必須とする
//**************************************************************************************************
var getYYYYMMDDFromJpYMD = function(jp) {
    jp = $.trim(jp.replace("年", "/").replace("月", "/").replace("日", "")).split("/");
    if (jp.length!="3") return "";
    return $.trim("0000"+jp[0]).slice(-4)+$.trim("00"+jp[1]).slice(-2)+$.trim("00"+jp[2]).slice(-2);
}

var convertAndCheckDateTime = function(is_no_alert) {
    var s_ymd = $.trim($("#start_day").val()); //開始日
    var e_ymd = $.trim($("#end_day").val()); //終了日
    var s_ymdhms = "";
    var e_ymdhms = "";

    if (s_ymd!="" || e_ymd!=""){
        if (s_ymd=="") { if (!is_no_alert) alert("終了日を指定する場合は、開始日を指定してください。"); return false; }
        if (e_ymd=="") { if (!is_no_alert) alert("開始日を指定する場合は、終了日を指定してください。"); return false; }

        if (s_ymd != "") s_ymdhms = getYYYYMMDDFromJpYMD(s_ymd) + ("0"+$("[name=st_hour]").val()).slice(-2) + $("[name=st_minute]").val();
        if (e_ymd != "") e_ymdhms = getYYYYMMDDFromJpYMD(e_ymd) + ("0"+$("[name=ed_hour]").val()).slice(-2) + $("[name=ed_minute]").val();

        if (s_ymdhms >= e_ymdhms) { if (!is_no_alert) alert("開始・終了日時が正しく指定されていません。"); return false; }
    }
    $("[name=start_date]").val(s_ymdhms); // 開始日hidden
    $("[name=end_date]").val(e_ymdhms); // 終了日hidden
    return true;
}







//**************************************************************************************************
// 所属プルダウン変更イベント
//**************************************************************************************************

// 「部」プルダウン変更⇒「課」プルダウン選択肢を変更
var ddlClassChanged = function(ddl) {
    var class_id = $(ddl).val();
    var attr_el = $("#emp_attr");
    attr_el.empty();
    attr = attr_el.get(0);
    attr.options[0] = new Option("全て", "");
    attr.selectedIndex = 0;
    for (var idx=0; idx<attr_list.length; idx++) {
        var param = attr_list[idx].split("\t");
        if (param[0]==class_id || !class_id) attr.options[attr.options.length] = new Option(param[2], param[1]);
    }
    ddlAttrChanged(attr);
}

// 「課」プルダウン変更⇒「科」プルダウン選択肢を変更
var ddlAttrChanged = function(ddl) {
    var class_id = $("#emp_class").val();
    var attr_id = $(ddl).val();
    var dept_el = $("#emp_dept");
    dept_el.empty();
    dept = dept_el.get(0);
    dept.options[0] = new Option("全て", "");
    dept.selectedIndex = 0;
    for (var idx=0; idx<dept_list.length; idx++) {
        var param = dept_list[idx].split("\t");
        if (class_id!="" && class_id!=param[0]) continue;
        if (attr_id!="" && attr_id!=param[1]) continue;
        dept.options[dept.options.length] = new Option(param[3], param[2]);
    }
    ddlDeptChanged(attr);
}
// 「室」プルダウン変更⇒「室」プルダウン選択肢を変更
var ddlDeptChanged = function(ddl) {
    var class_id = $("#emp_class").val();
    var attr_id = $("#emp_attr").val();
    var dept_id = $("#emp_dept").val();
    var room_id = $(ddl).val();
    var room_el = $("#emp_room");
//    if (!room_el) return;
    room_el.empty();
    room = room_el.get(0);
    room.options[0] = new Option("全て", "");
    room.selectedIndex = 0;
    for (var idx=0; idx<room_list.length; idx++) {
        var param = room_list[idx].split("\t");
        if (class_id!="" && class_id!=param[0]) continue;
        if (attr_id!="" && attr_id!=param[1]) continue;
        if (dept_id!="" && dept_id!=param[2]) continue;
        room.options[room.options.length] = new Option(param[4], param[3]);
    }
}









//**************************************************************************************************
// ナビ
//**************************************************************************************************
var gNavi = {
    globalMouseX : 0,
    globalMouseY : 0,
    $tblNavi : 0,
    iframeNavi : 0,
    tdNavi : 0,
    thNaviTitle : 0,
    initialize : function() {
        var self = this;
        var tags = [];
        // 入力ガイドのコンテナ
        // IE6なら、操作ガイド・入力メモがプルダウンの下になってしまうので、IFRAMEを準備
        if (isIE6) tags.push(
            '<iframe id="iframeNavi" frameborder="0" tabindex="-1" scrolling="no" noresize src="./img/dummy.gif"' +
            ' style="display:none; position:absolute;"></iframe>'
        );
        tags.push(
            '<table cellspacing="0" cellpadding="0" id="tblNavi" onmouseover="gNavi.hide(this)"><tr>' +
            '    <th id="thNaviTitle"></th><td id="tdNavi"></td>' +
            '</tr></table>'
        );
        $("body").append(tags.join(""));
        this.$tblNavi = $("#tblNavi");
        this.iframeNavi = ee("iframeNavi");
        this.tdNavi = ee("tdNavi");
        this.thNaviTitle = ee("thNaviTitle");
        $("html").mousemove(function(evt){
            self.globalMouseX = evt.pageX;
            self.globalMouseY = evt.pageY;
        });
        $(document).on('mouseenter', '[navi]', function(){ self.show(this); });
        $(document).on('mouseleave', '[navi]', function(){ self.hide(); });
    },
    show : function(target){
        var gmsg = $(target).attr("navi");
        var gtitle = $(target).attr("navititle");
        if(!gmsg || gmsg=="undefined" || gmsg=="") return;

        var win = window;
        var wh = win.innerHeight;
        if (document.all || window.opera) wh = win.document.documentElement.clientHeight;
        if (!wh) wh = win.document.getElementsByTagName("body")[0].clientHeight;
        if (!wh) wh = document.body.clientHeight;

        if (floatingpage) {
            var wh2 = (pwin.document.documentElement.clientHeight || pwin.document.body.clientHeight);
            if (pwin.innerHeight && wh2 > pwin.innerHeight) wh2 = pwin.innerHeight;
            wh2 -= 116;
            var sctop = (pwin.document.documentElement.scrollTop || pwin.document.body.scrollTop);
            wh2 += sctop;
            if (wh2 < wh) wh = wh2;
        }
        if (wh<1) return;

        var ww = window.innerWidth;
        if (!ww && document.documentElement) ww = document.documentElement.clientWidth;
        if (!ww) ww = document.body.clientWidth;
        if (!ww) return;

        this.tdNavi.innerHTML = gmsg.replace(/(\n)/g, "<br />");
        this.thNaviTitle.innerHTML = trim(gtitle);
        this.$tblNavi.css({top:-1000, width:"auto"});
        this.$tblNavi.show();

        var tblh = this.$tblNavi.get(0).offsetHeight;
        var tblw = this.$tblNavi.get(0).offsetWidth;
        if (tblw > ww - 20) { tblw = ww - 20; this.$tblNavi.css({"width":tblw}); }
        this.$tblNavi.hide();

        var wy = (wh - tblh - 20);
        if (wy <= this.globalMouseY) wy = this.globalMouseY - tblh - 30;

        this.$tblNavi.css({top:wy, left:18});
        if (isIE6) $(this.iframeNavi).css({"top":wy, "left":18, "width":tblw, "height":tblh})
        if (isIE6) $(this.iframeNavi).fadeIn(300);

        this.$tblNavi.fadeIn(300);
    },
    hide : function() {
        this.$tblNavi.stop(true, true).hide();
        if (isIE6) $(this.iframeNavi).stop(true, true).hide();
    }
};






//**************************************************************************************************
// 受講状況テーブル
//**************************************************************************************************
var PolicyTable = {
    iSel:0, mSel:0, fBlock:0,firstPCode:"",studentCount:0,colrowData:{},

    initializePolicy : function(pcode, student_count, colrow_data, policy_for_questionnaire, policy_for_test) {
        this.firstPCode = pcode;
        this.studentCount = int(student_count);
        this.colrowData = colrow_data;
        this.pcode_pre = document.getElementById("policy_code_pre");
        this.pcode = document.getElementById("policy_code");
        if (ee("policy_for_test")) {
            setSelection(ee("policy_for_test"), policy_for_test);
            setSelection(ee("policy_for_questionnaire"), policy_for_questionnaire);
        }
        $(this.pcode_pre).append(this.pcode_pre_selector.replace(/@A/g, this.defSelI["A"]).replace(/@Q/g, this.defSelI["Q"]).replace(/@T/g, this.defSelI["T"]));
        if (!this.pcode_pre) return;
        if (pcode=="") pcode = "A+1/Q+1/T+1";
        var pc1 = pcode + "           ";
        pc1 = trim(pc1.substring(0,1) + pc1.substring(4,5) + pc1.substring(8,9));
        this.pcode_pre_selected(pc1);
        setSelection(this.pcode, pcode);
        this.makeSummaryDisplay(pcode);

        // 機能を絞った状態で、ポリシーコードを再取得
        var cur = getSelectionValue(this.pcode);
        // 集合研修とテストがあれば、「テストについて」を表示
        if (ee("div_policy_for_test")) {
            setSelection(ee("policy_for_questionnaire"), policy_for_questionnaire);
            setSelection(ee("policy_for_test"), policy_for_test);
        }
    },
    indexArray : [],
    defSelI : {"A":"集合研修", "Q":"ｱﾝｹｰﾄ", "T":"ﾃｽﾄ"},
    defSelJ : {"A":"集合研修出席", "Q":"ｱﾝｹｰﾄ回答", "T":"ﾃｽﾄ合格"},

    pcode_pre_selector :
    '<option value="Q">@Q</option>'+
    '<option value="T">@T</option>'+
    '<option value="A">@A</option>'+
    '<option value="AQ">@A　＋　@Q</option>'+
    '<option value="AQT">@A　＋　@Q　＋　@T</option>'+
    //'<option value="QA">@Q　＋　@A</option>'+
    '<option value="AT">@A　＋　@T</option>'+
    //'<option value="TA">@T　＋　@A</option>'+
    //'<option value="QT">@Q　＋　@T</option>'+
    '<option value="TQ">@T　＋　@Q</option>',
    //'<option value="ATQ">@A　＋　@T　＋　@Q</option>'+
    //'<option value="QAT">@Q　＋　@A　＋　@T</option>'+
    //'<option value="QTA">@Q　＋　@T　＋　@A</option>'+
    //'<option value="TAQ">@T　＋　@A　＋　@Q</option>'+
    //'<option value="TQA">@T　＋　@Q　＋　@A</option>',

    pcode_selector_options : [
        {key:"_1+1/_2-2/_3-3", text:"@1　のみ"},
        {key:"_1-1/_2+2/_3-3", text:"@2　のみ"},
        {key:"_1-1/_2-2/_3+3", text:"@3　のみ"},
        {key:"_1+1/_2+1/_3-2", text:"@1　または　@2"},
        //{key:"_1+1/_2+2/_3-3", text:"@1　と　@2"},
        {key:"_1+1/_2-2/_3+1", text:"@1　または　@3"},
        //{key:"_1+1/_2-2/_3+3", text:"@1　と　@3"},
        {key:"_1-1/_2+2/_3+2", text:"@2　または　@3"},
        //{key:"_1-1/_2+2/_3+3", text:"@2　と　@3"},
        //{key:"_1+1/_2+2/_3+2", text:"@1　と、　@2　または　@3"},
        //{key:"_1+1/_2+1/_3+2", text:"@1　または　@2と、　@3"},
        {key:"_1+1/_2+1/_3+1", text:"@1　または　@2　または　@3"}
        //{key:"_1+1/_2+2/_3+3", text:"@1　と　@2　と　@3"}
    ],
    pcode_pre_selected : function(v){
        setSelection(this.pcode_pre, v);
        v = getSelectionValue(this.pcode_pre);
        this.indexArray = v.split("");
        var len = this.indexArray.length;
        var selector = [];
        var cur = getSelectionValue(this.pcode);
        var curIndex = -1;
        for (var idx=0; idx<this.pcode_selector_options.length; idx++) {
            var obj = this.pcode_selector_options[idx];
            var txt = obj["text"];
            var key = obj["key"];

            if (len<=2 && txt.indexOf("@3")>=0) continue;
            if (len<=1 && txt.indexOf("@2")>=0) continue;
            for (var n=1; n<=len; n++) txt = txt.replace("@"+n, this.defSelJ[this.indexArray[n-1]]); // 日本語に

            var keys = key.split('/');
            if (len<=2) key = keys[0]+"/"+keys[1]; // 3番目以降は利用しない
            if (len<=1) key = keys[0]; // 2番目以降は利用しない

            if (len>=3) key = key.replace("_3", this.indexArray[2]);
            if (len>=2) key = key.replace("_2", this.indexArray[1]);
            if (len>=1) key = key.replace("_1", this.indexArray[0]);

            selector.push('<option value="'+key+'">'+txt+'</option>');
            if (key==cur) curIndex = idx;
        }
        $(this.pcode).append(selector.join(""));
        setSelection(this.pcode, cur);
        // ここから機能縮小
        // 常に末尾をセレクト。これで「最大のOR選択」になる。
        this.pcode.selectedIndex = this.pcode.length-1;

        // 集合研修とテストがあれば、「テストについて」を表示
        if (ee("div_policy_for_test")) {
            ee("div_policy_for_test").style.display = (v.indexOf("A")>=0 && v.indexOf("T")>=0 ? "" : "none");
            ee("policy_for_test").selectedIndex = 0;
            // 集合研修とアンケートがあれば、「アンケートについて」を表示
            ee("div_policy_for_questionnaire").style.display = (v.indexOf("A")>=0 && v.indexOf("Q")>=0 ? "" : "none");
            ee("policy_for_questionnaire").selectedIndex = 0;
        }

        this.makeSummaryDisplay(getSelectionValue(this.pcode));
    },
    showStudentList : function(assembly_actual_status, no, test_status, questionnaire_status) {
        if (!contents_id || is_summary_preview) { alert("ここからは研修・講義を登録したのち、閲覧可能になります。"); return false; }
        var url =
        "study_student.php?session="+session+"&contents_id="+contents_id+
        "&no="+str(no)+
        "&assembly_actual_status="+str(assembly_actual_status)+
        "&test_status="+str(test_status)+
        "&questionnaire_status="+str(questionnaire_status);
        window_open(url, "setting_student3", 1000, 650);
    },
    getLink : function(caption, pInfo, otherParam) {
        var a_st = ""; // assembly_actual_status
        var no = "";
        var t_st = ""; // test_status
        var q_st = ""; // questionnaire_status
        var test_status = "";
        if (pInfo) {
            if (pInfo.func_name=="test") {
                if (otherParam=="ok") t_st = "2";
                if (otherParam=="ng") t_st = "-2";
                if (otherParam=="jukocyu") t_st = "1";
                if (otherParam=="mijuko") t_st = "-1";
            }
            if (pInfo.func_name=="questionnaire") {
                if (otherParam=="ok") q_st = "1";
                if (otherParam=="ng") q_st = "0";
            }
            if (pInfo.func_name=="assembly") {
                if (otherParam=="ok") a_st = "1";
                if (otherParam=="ng") a_st = "-3";
            }
        }
        return '<a href="javascript:void(0)" onclick="PolicyTable.showStudentList(\''+a_st+'\',\''+no+'\',\''+t_st+'\',\''+q_st+'\')">'+str(caption)+'</a>';
    },
    getPolicyInfo : function(policy_code) {
        var ary = policy_code.split("/");
        var funcs = {"A":"assembly", "Q":"questionnaire", "T":"test"};
        var jp_list = {"A":"集合研修", "Q":"ｱﾝｹｰﾄ", "T":"ﾃｽﾄ"};
        var group_indexes = [];
        var ret = {};
        var col = 0;
        for (var idx=0; idx<ary.length; idx++) {
            var section = ary[idx]+"   ";
            col++;
            var func       = section.substring(0, 1);
            var hissu_mark = section.substring(1, 2);
            var group      = int(section.substring(2, 3));

            if (func!="A" && func!="Q" && func!="T") continue;
            if (hissu_mark!="+" && hissu_mark!="-") continue;
            if (!group) continue;
            group_indexes[group] = int(group_indexes[group]) + 1;
            ret[func] = {
                "func":func,
                "func_name":funcs[func],
                "jp":jp_list[func], // 日本語
                "col":col, // 列順
                "hissu_mark":hissu_mark, // 必須列なら"+"
                "group":group, // OR条件グループ
                "group_index":group_indexes[group] // OR条件グループ内順番
            };
        }
        for (var func in ret) {
            ret[func]["group_size"] = group_indexes[ret[func]["group"]]; // OR条件グループの構成数
        }
        return ret;
    },
    makeSummaryDisplay : function(cur, student_count, colrow_data){
        student_count = int(student_count);
        if (!student_count || !colrow_data) {
            student_count = this.studentCount;
            colrow_data = this.colrowData;
            if (this.firstPCode!=cur) colrow_data = {};
        }

        if (!this.fBlock) this.fBlock = document.getElementById("flow_block");
        if (!cur) { this.fBlock.innerHTML = ""; return; }
        var funcs = cur.split('/');
        cur += "********";
        var groupSize = [0, 0, 0, 0];
        var groupExists = 0;
        var isMulti = 0;
        var testCol = 0;
        var lastHissuCol = 0;
        for (var col=1; col<=funcs.length; col++) {
            var funcInfo = funcs[col-1].split("");
            var func =  funcInfo[0];
            var hissu = funcInfo[1];
            var group = int(funcInfo[2]);
            if (hissu=="+") {
                if (groupSize[group]) groupExists = 1;
                groupSize[group]++;
            }
            if (func=="T") testCol = col;
            if (hissu=="+") lastHissuCol = col;
        }
        var isMulti = (groupSize[1]?1:0) + (groupSize[2]?1:0) + (groupSize[3]?1:0) > 1;
        var policyInfo = this.getPolicyInfo(cur);
        var html = [];
        html.push('<div style="padding-bottom:10px;">受講者数： '+this.getLink(student_count)+'</div>');
        var groupIndex = [0, 0, 0, 0];
        var hiphen = '<span class="gray">≫</span>';

        html.push(
            '<td><table cellspacing="0" cellpadding="0" class="jj_list">'+
            '   <tr><th>&nbsp;</th></tr>'+
            '   <tr><th><nobr>実施済</nobr></th></tr>'+
            '   <tr><th>未実施</th></tr>'+
            '   <tr><th>受講率</th></tr></table></td>'
        );

        for (var col=1; col<=funcs.length; col++) {
            var funcInfo = funcs[col-1].split("");
            var func = funcInfo[0];
            var hissu = funcInfo[1];
            var group = funcInfo[2];
            groupIndex[group]++;

            var isGroup = 0;
            if (col==1 && cur.substring(6, 7 )==group) isGroup = 1;
            if (col==2 && cur.substring(10,11)==group) isGroup = 1;

            html.push(
                '<td><table cellspacing="0" cellpadding="0" class="jj_list">'+
                '   <tr><th><nobr>'+this.defSelJ[funcInfo[0]]+'</nobr></th></tr>'+
                '   <tr><td class="juko_jyokyo_num"><b>'+this.getLink(colrow_data["col"+col+"_row2"],policyInfo[func], 'ok')+'</b></td></tr>'+
                '   <tr><td class="juko_jyokyo_num">'+this.getLink(colrow_data["col"+col+"_row3"],policyInfo[func], "ng")+'</td></tr>'+
                '   <tr><td class="juko_jyokyo_num">'+str(colrow_data["col"+col+"_row4"])+'%</td></tr></table></td>'
            );

            if (col==funcs.length) {
                var countOk = colrow_data["col"+col+"_row2"];
                var countNg = colrow_data["col"+col+"_row3"];
                var percent = colrow_data["col"+col+"_row4"];
                if (groupExists || isMulti) {
                    countOk = colrow_data["col"+col+"_row8"];
                    countNg = colrow_data["col"+col+"_row9"];
                    percent = colrow_data["col"+col+"_row10"];
                }

                html.push(
                '<td><table cellspacing="0" cellpadding="0" class="jj_list">'+
                '   <tr><th><nobr>総合</nobr></th></tr>'+
                '   <tr><td class="juko_jyokyo_num">'+str(countOk)+'</td></tr>'+
                '   <tr><td class="juko_jyokyo_num">'+str(countNg)+'</td></tr>'+
                '   <tr><td class="juko_jyokyo_num"><b>'+str(percent)+'%</b></td></tr></table></td>'
                );
            }
        }

        html.push(
            '</table></td>'
        );
        this.fBlock.innerHTML = '<table cellspacing="0" cellpadding="0" class="ttl"><tr>' + html.join("") + '</tr></table>';
    }
}
