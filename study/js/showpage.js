function showLoginPage(currentWin, base) {
    if (base) {
        alert("showpage.js debug.   "+base);
        showPage(currentWin, base+'../login.php');
        return;
    }
    showPage(currentWin, '../login.php');
}

function showErrorPage(currentWin, base) {
    if (base) {
        showPage(currentWin, base+'../error.php');
        return;
    }
    showPage(currentWin, '../error.php');
}

function showPage(currentWin, url) {
    var mainWin = getMainWin(currentWin);
    mainWin.location = url;
    if (currentWin != mainWin && !currentWin.closed) {
        currentWin.close();
    }
}

function getMainWin(currentWin) {
    try {
        if (currentWin.name == 'comedix_main') {
            return currentWin;
        }

        if (currentWin.opener) {
            if (currentWin.opener.location.hostname == currentWin.location.hostname) {
                return getMainWin(currentWin.opener);
            }
        }

        if (currentWin.top) {
            if (currentWin.top.location.hostname == currentWin.location.hostname && currentWin.top != currentWin) {
                return getMainWin(currentWin.top);
            }
        }
    } catch (e) {
    }
    return currentWin;
}
