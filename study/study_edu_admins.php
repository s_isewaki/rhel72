<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 教育責任者関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyCommonModel.php';

default_view();
die;



//**************************************************************************************************
// 教育責任者管理
//**************************************************************************************************
function default_view() {
    $btn_event = $_REQUEST["btn_event"];
    $emp_id_comma_list = $_REQUEST["emp_id_comma_list"];


    //==========================================================================
    // 教育責任者を削除
    //==========================================================================
    if ($btn_event=="be_del_employee" && $emp_id_comma_list) {
        $ary = explode(",", $emp_id_comma_list);
        foreach ($ary as $emp_id) {
            if ($emp_id) StudyCommonModel::delete_edu_admins($emp_id);
        }
        header("Location: study_edu_admins.php?session=".GG_SESSION_ID);
        die;
    }

    //==========================================================================
    // 教育責任者を追加
    //==========================================================================
    if ($btn_event=="be_add_employee" && $emp_id_comma_list) {
        $ary = explode(",", $emp_id_comma_list);
        foreach ($ary as $emp_id) {
            if ($emp_id) StudyCommonModel::add_edu_admins($emp_id);
        }
        header("Location: study_edu_admins.php?session=".GG_SESSION_ID);
        die;
    }

    //------------------------------------------------------
    // DBから一覧取得
    //------------------------------------------------------x
    $emp_list = StudyCommonModel::get_emp_admins_list();

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';
    $smarty->assign("emp_list_count", count($emp_list));
    $smarty->assign("emp_list", $emp_list);
    $smarty->display("study_edu_admins.tpl");
}
?>
