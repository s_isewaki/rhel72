<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 教材・テキストダウンロードダイアログ関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyFolderModel.php';
require_once 'model/StudyCommonModel.php';

/**
 * バリテス e-ラーニング 講座関連コントローラ
 * 講座作成、編集画面の表示
 * 講座削除
 */

$btn_event = $_REQUEST["btn_event"];


// 表示処理
display_view($btn_event);
die;




//**************************************************************************************************
// 講座編集画面処理
//**************************************************************************************************
function display_view($btn_event) {
    require_once 'conf/smarty_start.conf';
    $folder_id = $_REQUEST["current_folder_id"];
    $smarty->assign("result", "");

    $folder_list = StudyFolderModel::get_folder_list(); // 講座一覧。講座名作成用
    $folder_joint_name = implode("＞", StudyCommonModel::get_folder_parents("name", $folder_id, $folder_list));

    //==========================================================================
    // 講座新規作成処理
    //==========================================================================
    if ($btn_event=="be_create_folder") {
        StudyFolderModel::create_folder($_REQUEST["folder_name"], $_REQUEST["target_folder_id"]);
        $smarty->assign("result", "success");
    }

    //==========================================================================
    // 講座名変更処理
    //==========================================================================
    if ($btn_event=="be_edit_folder") {
        StudyFolderModel::change_folder_name($folder_id, $_REQUEST["current_parent_id"],$_REQUEST["folder_name"],$_REQUEST["current_folder_name"]);
        $smarty->assign("result", "success");
    }

    //==========================================================================
    // 講座削除処理
    //==========================================================================
    if ($btn_event=="be_delete_folder") {
        StudyFolderModel::delete_folder($folder_id);
        $smarty->assign("result", "success");
    }

    //==========================================================================
    // 講座移動処理
    //==========================================================================
    if ($btn_event=="be_move_folder") {
        StudyFolderModel::move_folder($folder_id, $_REQUEST["target_folder_id"], $_REQUEST["current_folder_name"]);
        $smarty->assign("result", "success");
    }

    //------------------------------------------------------
    // 表示状態を調整
    //------------------------------------------------------
    $folder_name = "";
    $selected_folder_id = "";

    $parent_folder_id = "";
    if ($_REQUEST["selected_folder_id"]) $parent_folder_id = $_REQUEST["parent_folder_id"];

    if ($_REQUEST["event"]=="ev_pickup_folder") {
        $title = "講座選択";
        $target_label = "講座";
        // デフォルトのツリー選択と講座名
        if ($_REQUEST["current_folder_id"]) {
            $selected_folder_id = $_REQUEST["current_folder_id"];
            $folder = StudyFolderModel::get_folder_row($_REQUEST["current_folder_id"]);
            $folder_name = $folder["name"];
        }
    } else if ($_REQUEST["event"] == "ev_create_folder") {
        $title = "講座作成";
        $target_label = "作成先";
        // デフォルトのツリー選択
        $selected_folder_id = $_REQUEST["selected_folder_id"];
    } else {
        $title = "講座編集";
        $target_label = "移動先";
        // 編集対象の講座名を取得
        $folder = StudyFolderModel::get_folder_row($_REQUEST["current_folder_id"]);
        if (!$folder) business_exception("指定された講座は存在しません。", "close");
        $folder_name = $folder["name"];
    }


    //------------------------------------------------------
    // この画面を呼ぶのは４箇所
    // ・ study_folder_admin 講座管理        （ADMINのみ画面操作可。フル操作可。担当者責任者はこのURL自体が呼ばれないので無視してよい）
    // ・ study_edu_tantou   担当者追加など  （ADMINのみ画面操作可。フル操作可。担当者責任者はこのURL自体が呼ばれないので無視してよい）
    // ・ study_main         研修講義一覧    （ADMINかどうかは無視。担当者のみ絞り込んだ上で操作可、責任者なら全閲覧可・リードオンリー）
    // ・ study_contents     研修講義詳細画面（ADMINかどうかは無視。担当者のみ絞り込んだ上で操作可、責任者なら全閲覧可・リードオンリー）
    //------------------------------------------------------
    $call_from = $_REQUEST["call_from"];


    //------------------------------------------------------
    // １）講座を自己担当分のみにするかどうか
    // ◆担当者でなければ絞込みの必要が無い。（責任者と担当者との兼務の場合、責任者とみなされる）
    // ◆担当者のうち、「ADMIN＋担当者」という場合がある。
    // ◆ADMINだったら「study_edu_tantou」「study_folder_admin」からの処理では絞り込まない。
    //------------------------------------------------------
    $is_auth_strict = 0;
    if ($call_from=="study_folder_admin") {} // 講座管理。管理者しか利用できないモードではある
    if ($call_from=="study_edu_tantou") {} // 教育担当者へのフォルダ割当。管理者しか利用できないモードではある
    if ($call_from=="study_contents") { // ユーザ側研修・講義管理 ⇒ 研修・講義詳細画面からの、講座セレクト時
        if (GG_STUDY_TANTOU_FLG) $is_auth_strict = 1; // 担当者のみなら表示制限つける。管理者は来るがedit_authはカラとなるため無視してよい。責任者不問
    }
    if ($call_from=="study_main") { // 研修・講義の、ユーザ側での編集系・新規作成系
        if (GG_STUDY_TANTOU_FLG) $is_auth_strict = 1; // 担当者なら表示制限つける。管理者は来るがedit_authはカラとなるため無視してよい。責任者不問
    }

    //------------------------------------------------------
    // ２）「最上位講座を指定」が選択できるかどうか
    // ◆呼び出し元が「管理側」の場合とする。study_edu_tantouは除く。作成・移動・編集などは、「study_folder_admin」の全挙動で選択可能
    //------------------------------------------------------
    $is_topnode_radio_selectable = 0;
    if ($call_from=="study_folder_admin") {
        if (GG_ADMIN_FLG) $is_topnode_radio_selectable = 1;
    }


    //------------------------------------------------------
    // ３）権限
    //------------------------------------------------------
    if ($_REQUEST["event"]=="ev_create_folder") {
        $auth_matrix = StudyCommonModel::get_auth_matrix("study_folder.tpl@ev_create_folder", $folder_id, "");
    } else {
        $auth_matrix = StudyCommonModel::get_auth_matrix("study_folder.tpl@all", $folder_id, "");
    }
    if ($call_from=="study_folder_admin") {
        $auth_matrix = array("disp_auth"=>"管理者", "edit_auth"=>"管理者");
    }
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];


    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $smarty->assign("title", $title);
    $smarty->assign("target_label", $target_label);
    $smarty->assign("call_from", $call_from);
    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);
    $smarty->assign("is_topnode_radio_selectable", $is_topnode_radio_selectable);
    $smarty->assign("js_callback", $_REQUEST["js_callback"]);
    $smarty->assign("event", $_REQUEST["event"]);
    $smarty->assign("folder_name", $folder_name);
    $smarty->assign("selected_folder_id", $selected_folder_id);
    $smarty->assign("parent_folder_id", $parent_folder_id);
    $smarty->assign("folder_list", StudyFolderModel::get_folder_list("","",$is_auth_strict)); // 講座ツリーリスト。権限によりリスト構成が変わる
    $smarty->assign("current_folder_id", $_REQUEST["current_folder_id"]);
    $smarty->assign("folder_joint_name", $folder_joint_name);
    $smarty->assign("current_parent_id", $_REQUEST["parent_folder_id"]);
    $smarty->assign("current_folder_name", $folder_name);
    $smarty->display("study_folder.tpl");
}

?>


