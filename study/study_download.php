<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 教材・テキストダウンロード専用URL
//
//
// ★★★ .htaccessを参照のこと。 ★★★
//
//
//
// 教材・テキストは、以下画面からダウンロード可能である
// ◆ 研修・講義ダイアログ（管理側）
// ◆ 教材・テキストダウンロードダイアログ（管理側＋ユーザ側）
//
// ダウンロードファイルは日本語を含み、ブラウザでダウンロード時に文字化けするので、
// 上記ダウンロード場所の、Aタグリンク先URL自体を、最初から日本語ファイル名としている。ダイアログ側を参照のこと。
//
// プラス、URLのGETパラメータに「session」と「zz_download_file_path」を添加している。
//
// .htaccessでは、「zz_download_file_path」がGETパラメータに在すると、
// このPHPスクリプトを実行するようになっている。それだけ。
//
// なお、このPHPスクリプトのみ、
// .htaccessで、output_buffering=off になっている。
//
// ダイアログでのダウンロードターゲットは、隠しiframeとなっている。
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyContentsModel.php';
ob_clean();

$path = GG_FILE_HOME.$_REQUEST["zz_download_file_path"];
if (is_file($path)) {
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment");
    $handle = fopen($path, "r");
    while(!feof($handle)){
        $bytes = fread($handle, 1000 * 1024);
        echo $bytes;
        flush();
    }
    fclose($handle);
    die;
}

$path = GG_FILE_HOME.$_REQUEST["zz_movie_file_path"];
if (is_file($path)) {
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment");
    $handle = fopen($path, "r");
    while(!feof($handle)){
        $bytes = fread($handle, 1000 * 1024);
        echo $bytes;
        flush();
    }
    fclose($handle);
    die;
}
//==========================================================
// 以下、ファイルが無い場合
//==========================================================
require_once("view/header.tpl");
?>
<script type="text/javascript">
    alert("ファイルが存在しません。");
</script>
</head>
<body>
</body>
</html>