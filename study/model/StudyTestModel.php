<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// テスト関連のDBアクセス・ユーティリティ
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
class StudyTestModel {

    //**********************************************************************************************
    // 【管理側】WORK画像を削除（一時アップロード場所）
    //**********************************************************************************************
    function try_sweep_work_dir($contents_id) {
        $dir = GG_TEST_IMAGE_WORK_HOME.$contents_id;
        if (!is_dir($dir)) return;
        foreach (glob($dir."/*") as $filepath) unlink($filepath); // 削除
        rmdir($dir);
    }
    //**********************************************************************************************
    // 答え合わせをする(1問ずつ)
    // user_answer: 「2」や「1,3」など。チェックした番号。チェックボックスの場合は複数がカンマつなぎ
    // true_answer: 「t,f,f,t」など。順番が問題番号となっている。その問題が正答ならtとなっている。
    //**********************************************************************************************
    function check_answer($user_answer, $true_answer) {
        $true_answers = explode(",", $true_answer); // 正答
        $user_answers = explode(",", $user_answer); // ユーザの解答
        for ($idx=1; $idx<=count($true_answers); $idx++) { // 正答をループさせる
            $t_or_f = $true_answers[$idx-1]; // この選択肢がチェックすべき正選択肢(t)か、チェックしてはいけない誤選択肢(f)かの判定
            if ($t_or_f=="t" && !@in_array($idx, $user_answers)) return false; // 正選択肢なのに、ユーザ回答にその番号が無い。つまりユーザ不正解確定
            if ($t_or_f=="f" &&  @in_array($idx, $user_answers)) return false; // 誤選択肢なのに、ユーザ回答にその番号が有る。つまりユーザ不正解確定
        }
        return true;
    }

    //**********************************************************************************************
    // テスト取得
    //**********************************************************************************************
    function get_test_row($id) {
        return dbGetTopRow("select * from study_test where id = ".dbPIntC($id)." and edition = 1");
    }
    //**********************************************************************************************
    // テスト問題を取得
    //**********************************************************************************************
    function get_test_items($contents_id) {
        return dbFind("select * from study_test_item where id = ".dbPIntC($contents_id)." and edition = 1 order by no");
    }
    //**********************************************************************************************
    // 【管理側】テスト結果を取得。最初の３回までと、最後の１回を取得。
    //**********************************************************************************************
    function get_test_result_for_total_showing($contents_id) {
        $sql =
        " select tr.no, tr.emp_id,tr.is_newest, tr.answers as user_answers from study_test_status ts".
        " inner join study_test_result tr on (".
        "     tr.test_id = ts.test_id and tr.edition = 1 and tr.emp_id = ts.emp_id and (tr.no <= 3 or tr.is_newest = 1)".
        " )".
        " where ts.test_id = ".dbPIntC($contents_id)." and ts.edition = 1 and (ts.test_status = 1 or ts.test_status = 2)";
        return dbFind($sql);
    }
    //**********************************************************************************************
    // 【ユーザ側】テスト結果を取得
    //**********************************************************************************************
    function get_test_result($contents_id, $emp_id) {
        $sql = "select * from study_test_result where test_id=".dbPIntC($contents_id)." and edition = 1 and emp_id=".dbPStr($emp_id);
        return dbFind($sql." order by no asc");
    }
    //**********************************************************************************************
    // 【管理側】テスト＋テスト問題の削除
    //**********************************************************************************************
    function clear_test_result($contents_id, $emp_id) {
        //--------------------------
        // テスト削除
        //--------------------------
        dbExec("delete from study_test_result where test_id = ".dbPIntC($contents_id)." and edition = 1 and emp_id = ".dbPStr($emp_id));
        //--------------------------
        // テスト合否を更新
        //--------------------------
        StudyCommonModel::update_test_status($contents_id, $emp_id, "0");
    }
    //**********************************************************************************************
    // 【管理側】テスト＋テスト問題の削除
    //**********************************************************************************************
    function delete_test($contents_id) {
        dbBeginTransaction();
        //--------------------------
        // テスト削除
        //--------------------------
        dbExec("delete from study_test where id = ".dbPIntC($contents_id)." and edition = 1");
        //--------------------------
        // テスト問題削除
        //--------------------------
        dbExec("delete from study_test_item where id = ".dbPIntC($contents_id)." and edition = 1");
        //--------------------------
        // 添付画像ファイル削除
        //--------------------------
        $file_pattern = $test_id."_1_*_*";
        foreach (glob(GG_ATTACH_HOME . $file_pattern) as $filename) {
            unlink($filename);
        }
        //--------------------------
        // 一時アップロードファイルがあれば削除
        //--------------------------
        StudyTestModel::try_sweep_work_dir($contents_id);
        //--------------------------
        // テスト問題数サマリング再集計
        //--------------------------
        StudyCommonModel::update_test_count($contents_id);
        //--------------------------
        // テスト期間をクリア更新
        //--------------------------
        StudyCommonModel::update_contents_test_date($contents_id, "", "");
        dbEndTransaction();
    }
    function copy_images($contents_id, $new_contents_id) {
        $rows = dbFind("select no, image_path from study_test_item where id = ".dbPIntC($new_contents_id)." and edition = 1");
        foreach ($rows as $row) {
            $cur_image_path = $row["image_path"];
            if (!$cur_image_path) continue;
            $image_ext = end(explode('.', $cur_image_path));
            $new_image_path = GG_ATTACH_HOME.$new_contents_id."_1_".$row["no"].".".$image_ext; // _1_ は、旧edition。現在"1固定"となっている。
            if (!copy($cur_image_path, $new_image_path)) {
                $emsg = "テスト画像の複製に失敗しました。".$new_image_path;
                logger_error($emsg);
            }
            $sql =
            " update study_test_item set image_path = ".dbPStr($new_image_path).
            " where id = ".dbPIntC($new_contents_id)." and no = ".dbPIntC($row["no"])." and edition = 1";
            dbExec($sql);
        }
    }
    //**********************************************************************************************
    // 【管理側】テストの新規登録/更新
    //**********************************************************************************************
    function update_test($contents_id) {
        dbBeginTransaction(); // トランザクション開始
        //--------------------------
        // テストが存在するか確認
        //--------------------------
        $is_exist = StudyTestModel::get_test_row($contents_id);
        //--------------------------
        // テストがなければ新規作成
        //--------------------------
        if (!$is_exist) {
            $sql =
            " insert into study_test (".
            "     id, edition".
            "    ,create_user_id, update_user_id, create_date, update_date".
            "    ,delete_flg, impression_flg, limit_time, border_score, test_item_order".
            " ) values (".
            " ".dbPIntC($contents_id). // id
            ",".dbPIntC(1). // edition
            ",".dbPStr(GG_LOGIN_EMP_ID). // create_user_id
            ",".dbPStr(GG_LOGIN_EMP_ID). // update_user_id
            ",".dbPTime(date("Y/m/d H:i:s", time())). // create_date
            ",".dbPTime(date("Y/m/d H:i:s", time())). // update_date
            ",".dbPBool(false). // delete_flg
            ",".dbPIntC($_REQUEST["impression_flg"]). // impression_flg
            ",".dbPIntC($_REQUEST["limit_time"]). // limit_time
            ",".dbPIntC($_REQUEST["border_score"]). // border_score
            ",".dbPIntC($_REQUEST["test_item_order"]). // test_item_order
            " )";
            dbExec($sql);
        }
        //--------------------------
        // テストがあれば更新
        //--------------------------
        else {
            $sql =
            " update study_test set".
            " update_user_id = ".dbPStr(GG_LOGIN_EMP_ID).
            ",update_date = ".dbPTime(date("Y/m/d H:i:s", time())).
            ",impression_flg = ".dbPIntC($_REQUEST["impression_flg"]).
            ",limit_time = ".dbPIntC($_REQUEST["limit_time"]).
            ",border_score = ".dbPIntC($_REQUEST["border_score"]).
            ",test_item_order = ".dbPIntC($_REQUEST["test_item_order"]).
            " where id = ".dbPIntC($contents_id)." and edition = 1";
            dbExec($sql);
        }
        //--------------------------
        // いったん全ての添付画像ファイルをrenameして退避する
        //--------------------------
        $file_pattern = $contents_id."_1_*";
        foreach (glob(GG_ATTACH_HOME . $file_pattern) as $filename) {
            if (substr($filename, -9)=="_evacuate") { unlink($filename); continue; } // もともとリネーム済のものがあれば、ゴミとみなす。削除
            rename($filename, $filename."_evacuate"); //リネーム退避
        }
        //--------------------------
        // テスト問題をいったん全デリート
        //--------------------------
        dbExec("delete from study_test_item where id = ".dbPIntC($contents_id)." and edition = 1");

        //========================================
        // テスト問題を登録
        //========================================
        $test_count = $_REQUEST["test_count"];
        for ($num = 1; $num <= $test_count; $num++) {
            // ◆◆◆ 保存するファイル名とファイルパス。ひとまず更新前のものをセットする。
            // ◆◆◆ 問題番号を入れ替えているかもしれないので、パスは$numとズレているかもしれない。
            $image_name = $_REQUEST["image_name_".$num]; // 更新前の値。HIDDEN値
            $image_path = $_REQUEST["image_path_".$num]; // 更新前の値。HIDDEN値
            // ◆◆◆ そもそも退避しただろうファイルがない場合は、ファイルをDBへカラ登録することになる
            if (!$image_path || !is_file($image_path."_evacuate")) {
                $image_path = "";
                $image_name = "";
            }
            // ◆◆◆ 一時アップロードファイルが存在する場合（インポート時もここにセットされている）
            // ◆◆◆ 一時アップロードがなければ、アップロードはされていない、ということになる
            $new_image_path = @$_REQUEST["new_image_path_".$num];
            $new_image_name = @$_REQUEST["new_image_name_".$num];
            if (!$new_image_path || !is_file($new_image_path)) {
                $new_image_path = "";
                $new_image_name = "";
            }
            // ◆◆◆ 新ファイル有無に関わらず、ファイルを削除しようとした場合
            // ◆◆◆ 既存ファイルはカラ登録することになる
            $is_attach_delete = $_REQUEST["attach_delete_".$num];
            if ($is_attach_delete) {
                $image_name = "";
                $image_path = "";
            }
            // ◆◆◆ 一時ファイルがあって、ファイル削除でなければ、正ファイルとして保存する。
            // ◆◆◆ ファイル削除指定した場合は、画面側で$new_image_pathはカラになっている。
            if ($new_image_path) {
                $image_name = $new_image_name;
                $image_ext = end(explode('.', $image_name));
                //$image_path = GG_ATTACH_HOME.$contents_id."_1_".$num."_".$image_name; // 今後、image_nameは文字化けにつき廃止
                $image_path = GG_ATTACH_HOME.$contents_id."_1_".$num.".".$image_ext; // _1_ は、旧edition。現在"1固定"となっている。
                if (!copy($new_image_path, $image_path)) {
                    $emsg = "インポートファイルのディスク書込みに失敗しました。".$image_path;
                    logger_error($emsg);
                    fatal_exception($emsg, __FILE__." ".__LINE__);
                }
            }
            // ◆◆◆ 一時ファイルがなく、元ファイルがある場合、かつ、削除でない場合
            // ◆◆◆ 退避ファイルから正ファイルに変更。このとき、問題番号順が変更されていたら、パスは正しく変更されるであろう。
            else if ($image_path) {
                $image_ext = end(explode('.', $image_name));
                //$after_image_path = GG_ATTACH_HOME.$contents_id."_1_".$num."_".$image_name; // 今後、image_nameは文字化けにつき廃止
                $after_image_path = GG_ATTACH_HOME.$contents_id."_1_".$num.".".$image_ext; // _1_ は、旧edition。現在"1固定"となっている。
                rename($image_path."_evacuate", $after_image_path); // 退避ファイルから、正ファイルへ
                $image_path = $after_image_path; // ファイルパスを、正ファイルパスへ
            }
            // ◆◆◆ 選択肢を配列にする。
            // ◆◆◆ 回答パターンを配列にする。
            $choice_cnt = (int)$_REQUEST["choice_cnt_".$num];
            $select_answers_array = array();
            $correct_answers_array = array();
            for ($i=1; $i<=$choice_cnt; $i++) {
                $select_answers_array[]= $_REQUEST["choice_item_".$num."_".$i];
                $correct_answers_array[] = ($_REQUEST["choice_answer_".$num."_".$i] ? "t" : "f");
            }
            //--------------------------
            // テスト問題を保存
            //--------------------------
            $sql =
            " insert into study_test_item (".
            "     id, edition, no, question, comment, image_path, image_name, choice_cnt, multiple, choices, answers".
            " ) values (".
            " ".dbPIntC($contents_id). // id
            ",".dbPIntC(1). // edition
            ",".dbPIntC($num). // no
            ",".dbPStr($_REQUEST["question_".$num]). // question
            ",".dbPStr($_REQUEST["summary_".$num]). // comment
            ",".dbPStr($image_path). // image_path
            ",".dbPStr($image_name). // image_name
            ",".dbPIntC($choice_cnt). // choice_cnt
            ",".dbPBool($_REQUEST["is_multiple_choice_".$num] === "t"?true:false). // multiple
            ",".dbPStr(implode(",", $select_answers_array)). // choices // 選択肢 カンマつなぎで格納
            ",".dbPStr(implode(",", $correct_answers_array)). // answers // 正答 tとfの配列。カンマつなぎで格納
            ")";
            dbExec($sql);
        }
        //============================================================
        // 残ったままの一時ファイルがあれば削除。アップロードしたのに、「取消」されたもの。
        //============================================================
        StudyTestModel::try_sweep_work_dir($contents_id);
        //============================================================
        // テスト問題をすべて登録しなおした後、退避したまま余った添付画像ファイルは削除
        //============================================================
        $file_pattern = $contents_id."_1_*";
        foreach (glob(GG_ATTACH_HOME . $file_pattern) as $filename) {
            if (substr($filename, -9)=="_evacuate") unlink($filename); // 削除
        }
        //--------------------------
        // テスト問題数サマリング再集計
        //--------------------------
        StudyCommonModel::update_test_count($contents_id);
        //--------------------------
        // テスト期間を更新
        //--------------------------
        StudyCommonModel::update_contents_test_date($contents_id, $_REQUEST["start_date"], $_REQUEST["end_date"]);
        dbEndTransaction(); // トランザクション終了
    }

    //**********************************************************************************************
    // 【ユーザ側】テスト結果をDB登録
    //**********************************************************************************************
    function create_test_result($contents_id) {
        //--------------------------
        // 今回の実施回数(履歴数)を準備
        //--------------------------
        $sql = "select max(no) as no from study_test_result where test_id = ".dbPIntC($contents_id)." and edition = 1 and emp_id = ".dbPStr(GG_LOGIN_EMP_ID);
        $exec_count = ((int)dbGetOne($sql)) + 1; // 今回の実施回数
        //--------------------------
        // テスト情報取得
        //--------------------------
        $test_info = StudyTestModel::get_test_row($contents_id); // テスト情報を取得
        $test_items = dbFind("select * from study_test_item where id = ".dbPIntC($contents_id)." and edition = 1 order by no"); // テスト問題取得
        $test_count = count($test_items); // テスト問題数を取得(正答率の母数)
        //--------------------------
        // 答え合わせ
        //--------------------------
        $correct_count = 0; // 正解数カウンタ
        $user_answers = explode("|", $_REQUEST["user_answers"]);
        for ($i = 0; $i < $test_count; $i++) {
            if (StudyTestModel::check_answer(@$user_answers[$i], $test_items[$i]["answers"])) $correct_count++; // 答え合わせ。正解をインクリメント
        }
        $percentage = number_format(($correct_count / $test_count) * 100, 2); // 正答率を取得 四捨五入となる。切上げ100%、切捨て0%は、無いとみなす
        //--------------------------
        // 既存テスト結果の最新フラグを外す
        //--------------------------
        dbExec("update study_test_result set is_newest = 0 where test_id = ".dbPIntC($contents_id)." and edition = 1 and emp_id = ".dbPStr(GG_LOGIN_EMP_ID));
        //--------------------------
        // テスト結果を保存
        //--------------------------
        $sql =
        " insert into study_test_result (".
        "     test_id, edition, emp_id, no, answers, correct_count, percentage, test_date, is_newest, shuffled_order".
        " ) values (".
        " ".dbPIntC($contents_id).
        ",1".
        ",".dbPStr(GG_LOGIN_EMP_ID).
        ",".dbPIntC($exec_count). // 実行回数(履歴数)
        ",".dbPStr($_REQUEST["user_answers"]). // ユーザ解答
        ",".dbPIntC($correct_count). // 正解数
        ",".dbPFloat($percentage). // 正答率
        ",".dbPTime(date("Y/m/d H:i:s", time())). // テスト日
        ",1". // 最新フラグ
        ",".dbPStr($_REQUEST["shuffled_order"]).
        " )";
        dbExec($sql);
        //--------------------------
        // テスト合否を判定
        //--------------------------
        $test_status = "1"; // 受講中（不合格）
        if ($percentage >= ((int)$test_info["border_score"])) $test_status = "2"; // 合格なら受講済み(合格)。点数条件が0なら0点で合格。
        //--------------------------
        // テスト合否を更新
        //--------------------------
        StudyCommonModel::update_test_status($contents_id, GG_LOGIN_EMP_ID, $test_status);
    }
}

