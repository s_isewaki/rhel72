<?php
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// 研修・講義関連のDBアクセス・ユーティリティ
// 研修・講義自体と、最終集計関連を扱う
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyTestModel.php';

$files_info_fields = array("file_no","file_type","file_name","file_url","file_path","file_capacity");

class StudyContentsModel {

    //======================================================
    // ファイル容量表示をKBからMBへ
    //======================================================
    function file_capacity_to_megabyte($file_capacity) {
        if (!$file_capacity) return $file_capacity;
        $file_capacity = ceil($file_capacity / 1024 * 1000) / 1000;
        return $file_capacity;
    }

    //======================================================
    // files_infoをバラす
    //======================================================
    function get_files_info_to_array($files_info) {
        $finfo = explode("\n", $files_info);
        $ret = array();
        foreach ($finfo as $info) {
            if (!$info) continue;
            $aa = explode("\t", $info);
            $tmp = array();
            global $files_info_fields;
            foreach ($files_info_fields as $idx2=>$field) $tmp[$field] = @$aa[$idx2];
            if (!$tmp["file_name"]) continue;

            //旧パラメータなら新パラメータへ
            $ft = $tmp["file_type"];
            if ($ft!="" && $ft!="movie" && $ft!="browser") {
                if ($ft=="doc") $ft = "";
                if ($ft=="xls") $ft = "";
                if ($ft=="ppt") $ft = "";
                if ($ft=="pdf") $ft = "browser";
                if ($ft=="txt") $ft = "";
                if ($ft=="jpg") $ft = "browser";
                if ($ft=="gif") $ft = "browser";
                if ($ft=="swf") $ft = "browser";
                if ($ft=="mpg") $ft = "movie";
                if ($ft=="mov") $ft = "movie";
                $tmp["file_type"] = $ft;
            }
            $tmp["file_capacity_mb"] = StudyContentsModel::file_capacity_to_megabyte($tmp["file_capacity"]);


            $tmp["download_url"] = $tmp["file_url"];
            $tmp["movie_url"] = $tmp["file_url"];
            $tmp["browser_url"] = $tmp["file_url"];
            $tmp["real_url"] = $tmp["file_url"];
            if (!$tmp["file_url"] && $tmp["file_path"]) {
                $tmp["download_url"] = "tmp/contents/download/".GG_SESSION_ID."/".h(basename($tmp["file_path"]))."/".$tmp["file_name"];
                $tmp["movie_url"] = "tmp/contents/movie/".GG_SESSION_ID."/".h(basename($tmp["file_path"]))."/".$tmp["file_name"];
                $tmp["browser_url"] = "tmp/contents/browser/".GG_SESSION_ID."/".h(basename($tmp["file_path"]))."/".$tmp["file_name"];
                $tmp["real_url"] = "tmp/contents/".h(basename($tmp["file_path"]));
            }
            $ret[] = $tmp;
        }
        return $ret;
    }

    //**********************************************************************************************
    // 研修・講義行を取得
    //**********************************************************************************************
    function get_contents_row($contents_id) {
        $now = date("YmdHi");
        $sql =
        " select *".
        ",case when test_start_date <= '".$now."' and '".$now."' <= test_end_date then '' else '1' end as is_out_of_date". // ユーザテスト実行画面で利用
        ",case when test_end_date < '".$now."' then '1' else '' end as is_date_expired". // ユーザテスト実行画面で利用
        " from study_contents".
        " where id = ".dbPIntC($contents_id)." and delete_flg = ".dbPBool(false);
        return dbGetTopRow($sql);
    }

    //**********************************************************************************************
    // 【管理側】講座の中に同名の研修・講義が存在する場合はtrueを返す
    //**********************************************************************************************
    function contents_exists($folder_id, $title, $self_id = null) {
        $sql =
        " select count(*) from study_contents".
        " where folder_id = ".dbPIntC($folder_id)." and delete_flg = ".dbPBool(false).
        " and title = ".dbPStr($title);
        if ($self_id) $sql .= " and not id = ".dbPIntC($self_id);
        if ((int)dbGetOne($sql)) return true;
        return false;
    }

    //**********************************************************************************************
    // 【管理側】研修・講義一覧を取得 (講座内検索画面用)
    //**********************************************************************************************
    function keyword_search($keyword, $bunrui_year) {
        //--------------------------
        // キーワードを分割
        //--------------------------
        $key_array = explode(" ", str_replace("　", " ", $keyword));
        $key_sql = "";
        $cnt = 0;
        foreach ($key_array as $kw) {
            if ($kw=="") continue;
            $cnt++;
            $key_sql.=" and (co.title like ".dbPStr("%".$kw."%")." or co.keyword like ".dbPStr("%".$kw."%")." or co.summary like ".dbPStr("%".$kw."%").")";
            if ($cnt>=5) break; // キーワードは５個までとする
        }
        //--------------------------
        // 検索
        //--------------------------
        $sql =
        " select co.id, co.folder_id, co.title, co.type, co.file_capacity, co.bunrui_year, sf.parent_id".
        " from study_contents co".
        " inner join study_folder sf on (sf.id = co.folder_id and sf.delete_flg = false)".
        " where co.delete_flg = false".$key_sql;
        if (strlen($bunrui_year)) $sql .= " and bunrui_year = ".dbPStr($bunrui_year);
        $sql .= " order by co.title, sf.name, sf.parent_id, co.folder_id";
        return dbFind($sql);
    }

    //**********************************************************************************************
    // 【管理側】講座内の研修・講義一覧を取得（研修・講義管理画面右下）
    //**********************************************************************************************
    function get_contents_list_under_folder($folder_id, $sort_string, $ext_joken) {
        //--------------------------
        // ソート文字列を作成
        //--------------------------
        $sql_sort_string = array();
        $ss = explode(",", $sort_string);
        foreach ($ss as $sstr) {
            if ($sstr=="title-asc") $sql_sort_string[]= "title asc";
            if ($sstr=="title-desc") $sql_sort_string[]= "title desc";
            if ($sstr=="create_date-asc") $sql_sort_string[]= "create_date asc";
            if ($sstr=="create_date-desc") $sql_sort_string[]= "create_date desc";
        }
        if (!count($sql_sort_string)) $sql_sort_string[]= "create_date desc";

        //--------------------------
        // いま実施中かどうかを判定する部分SQL
        //--------------------------
        $now = date("YmdHi");
        $sql_out_of_date =
            ",case when".
            "     (co.assembly_start_date <= '".$now."' and '".$now."' <= co.assembly_end_date and co.policy_code like '%A%' and co.assembly_count > 0)".
            "  or (co.questionnaire_start_date <= '".$now."' and '".$now."' <= questionnaire_end_date and co.policy_code like '%Q%')".
            "  or (co.test_start_date <= '".$now."' and '".$now."' <= test_end_date and co.policy_code like '%T%')".
            "  then '' else '1' end as is_out_of_date";
        //--------------------------
        // 検索
        //--------------------------
        $sql =
        " select * from (".
        " select co.id, co.title, co.type, co.type as file_type, co.update_date, co.file_capacity, co.file_path".
        ",co.file_name, coalesce(co.contents_total_status, 0) as contents_total_status".
        ",co.files_info as files_info_ary".
        ",co.assembly_start_date, co.assembly_end_date, co.questionnaire_start_date, co.questionnaire_end_date, co.test_start_date, co.test_end_date".
        ",co.policy_code, co.policy_jp1, co.policy_jp2, co.policy_for_questionnaire, co.policy_for_test".
        ",coalesce(co.student_count,0) as student_count, co.assembly_count, co.assembly_actual_count".
        ",co.questionnaire_count, co.test_count, co.notif_count, co.questionnaire_answer_count".
        ",co.test_ok_count, co.test_ng_count, co.bunrui_year".
        ",co.create_date".
        // <!-- RHEL7対応 2016.01.06 by suzuki -->
        //",to_char(co.create_date, 'YYYY/MM/DD HH24:MI:SS') as create_date_disp".
        ",to_char(co.create_date, ".dbPStr('YYYY/MM/DD HH24:MI:SS').") as create_date_disp".
        // ここまで
        ",te.id as test_id, te.end_date, coalesce(te.border_score,0) as border_score".
        " ".$sql_out_of_date.
        " from study_contents co".
        " left outer join study_test te on (co.id = te.id and te.edition = 1)".
        " where co.folder_id = ".dbPIntC($folder_id).
        " and co.delete_flg = ".dbPBool(false).
        " ) d".
        " where 1 = 1".
        " " . ($ext_joken=="misyu_only" ? " and (student_count = 0 or contents_total_status <> 1)" : "").
        " " . ($ext_joken=="pub_only" ? " and student_count > 0 and is_out_of_date = ''" : "").
        " order by ".implode(", ", $sql_sort_string);
        $rows = dbFind($sql);
        //--------------------------
        // 教材・ﾃｷｽﾄをバラす
        // 実施内容をバラす
        //--------------------------
        foreach ($rows as $idx=>$row) {
            $rows[$idx]["files_info"] = StudyContentsModel::get_files_info_to_array($row["files_info_ary"]);
            $rows[$idx]["policy_info"] = get_policy_info($row["policy_code"]);
        }
        return $rows;
    }

    //**********************************************************************************************
    // 【管理側】所属別一覧用
    // ※ 所属は以前、room_idまで取得していたが、利用していなかった。よってdept_idまでに変更
    //**********************************************************************************************
    function get_syozokubetu_list($contents_id) {
        $sql =
        " select el.study_emp_id as emp_id, cm.class_nm, am.atrb_nm, dm.dept_nm".
        ",coalesce(cm.class_id, 0) as class_id".
        ",coalesce(am.atrb_id, 0) as atrb_id".
        ",coalesce(dm.dept_id, 0) as dept_id".
        ",ts.assembly_actual_status, ts.questionnaire_status, ts.test_status, ts.total_status".
        " from study_emp_list el".
        " inner join study_test_status ts on (ts.emp_id = el.study_emp_id and ts.edition = 1 and ts.test_id = ".dbPIntC($contents_id).")".
        " inner join empmst em on (em.emp_id = el.study_emp_id)".
        " left outer join classmst cm on (cm.class_id = em.emp_class and cm.class_del_flg = false)".
        " left outer join atrbmst am on (am.atrb_id = em.emp_attribute and am.atrb_del_flg = false)".
        " left outer join deptmst dm on (dm.dept_id = em.emp_dept and dm.dept_del_flg = false)".
        " where el.id = ".dbPIntC($contents_id).
        " order by cm.class_id, am.atrb_id, dm.dept_id";
        return dbFind($sql);
    }

    //**********************************************************************************************
    // 【ユーザ側】ユーザの研修・講義一覧  ログイン者でなく管理者が履歴から呼び出す場合あり
    //**********************************************************************************************
    function get_contents_status_list_for_emp($emp_id, $menu_user_sort, $disp_out_of_date, $bunrui_year, $keyword, $folder_selected_ids, &$folder_list) {
        $ids = explode("\t", $folder_selected_ids);
        $folder_id = (int)$ids[count($ids)-1];

        //--------------------------
        // いま実施中かどうかを判定する部分SQL
        //--------------------------
        $now = date("YmdHi");
        $sql_out_of_date =

        //--------------------------
        // キーワードを分割
        //--------------------------
        $key_array = explode(" ", str_replace("　", " ", $keyword));
        $key_sql = "";
        $cnt = 0;
        foreach ($key_array as $kw) {
            if ($kw=="") continue;
            $cnt++;
            $key_sql.=" and (co.title like ".dbPStr("%".$kw."%")." or co.keyword like ".dbPStr("%".$kw."%")." or co.summary like ".dbPStr("%".$kw."%").")";
            if ($cnt>=5) break;
        }
        //--------------------------
        // 検索
        //--------------------------
        $sql =
        " select co.id, co.title, co.type, co.type as file_type, co.update_date, co.file_capacity, co.file_path".
        ",co.file_name, co.contents_total_status, co.folder_id".
        ",co.files_info as files_info_ary".
        ",co.assembly_start_date, co.assembly_end_date, co.questionnaire_start_date, co.questionnaire_end_date, co.test_start_date, co.test_end_date".
        ",co.student_count, co.assembly_count, co.assembly_actual_count, co.questionnaire_count, co.test_count, co.notif_count, co.questionnaire_answer_count".
        ",co.test_ok_count, co.test_ng_count, co.bunrui_year, co.policy_code, co2.policy_for_questionnaire, co2.policy_for_test".
        ",co2.max_end_date, case when co2.is_out_of_date = '1' then '999999999999' else co2.max_end_date end as max_end_date1".
        ",co2.assembly_require, co2.questionnaire_require, co2.test_require, co2.is_out_of_date".
        ",te.id as test_id, te.end_date, coalesce(te.border_score,0) as border_score".
        ",tr.test_date, tr.percentage, ts.test_status, ts.total_status".
        ",case when ts.assembly_status = 1 then 1 when ts.assembly_status = 2 then 2 else 0 end as assembly_status".
        ",ts.questionnaire_status, ts.assembly_actual_status, ts.assembly_actual_date".
        ",aa.no as assembly_no".
        ",sa.date as assembly_date, sa.start_time as assembly_start_time, sa.end_time as assembly_end_time".
        " from study_contents co".
        " inner join (".
        "     select id, policy_for_questionnaire, policy_for_test".
        "    ,d.assembly_end_date, d.questionnaire_end_date, d.test_end_date".
        "    ,case when".
        "         (assembly_start_date <= '".$now."' and '".$now."' <= assembly_end_date and assembly_policy_include = 1 and assembly_count > 0)".
        "      or (questionnaire_start_date <= '".$now."' and '".$now."' <= questionnaire_end_date and questionnaire_policy_include = 1 )".
        "      or (test_start_date <= '".$now."' and '".$now."' <= test_end_date and test_policy_include = 1)".
        "      then '' else '1' end as is_out_of_date".
        "    ,case when assembly_end_date >= questionnaire_end_date and assembly_end_date >= test_end_date then assembly_end_date".
        "          when questionnaire_end_date >= test_end_date then questionnaire_end_date".
        "          else test_end_date end as max_end_date".
        "    ,case when assembly_end_date <> '' and assembly_end_date >= '".$now."' then 1 else 0 end as assembly_require".
        "    ,case when questionnaire_end_date <> '' and questionnaire_end_date >= '".$now."' then 1 else 0 end as questionnaire_require".
        "    ,case when test_end_date <> '' and test_end_date >= '".$now."' then 1 else 0 end as test_require".
        "    ,assembly_policy_include, questionnaire_policy_include, test_policy_include".
        "     from (".
        "         select id".
        "        ,assembly_start_date, questionnaire_start_date, test_start_date, policy_code, assembly_count".
        "        ,coalesce(policy_for_questionnaire, '') as policy_for_questionnaire".
        "        ,coalesce(policy_for_test, '') as policy_for_test".
        "        ,case when policy_code like '%A%' and assembly_count > 0 then coalesce(assembly_end_date,'') else '' end as assembly_end_date".
        "        ,case when policy_code like '%Q%' then coalesce(questionnaire_end_date,'') else '' end as questionnaire_end_date".
        "        ,case when policy_code like '%T%' then coalesce(test_end_date,'') else '' end as test_end_date".
        "        ,case when policy_code like '%A%' then 1 else 0 end as assembly_policy_include".
        "        ,case when policy_code like '%Q%' then 1 else 0 end as questionnaire_policy_include".
        "        ,case when policy_code like '%T%' then 1 else 0 end as test_policy_include".
        "         from study_contents".
        "         where id in ( select id from study_emp_list where study_emp_id = ".dbPStr($emp_id).")".
        "     ) d".
        " ) co2 on (co2.id = co.id)".
        " inner join study_test_status ts on ( ts.test_id = co.id and ts.edition = 1 and ts.emp_id = ".dbPStr($emp_id).")".
        " left outer join study_test te on (co.id = te.id and te.edition = 1)".
        " left outer join study_test_result tr on ( tr.test_id = te.id and tr.edition = 1 and tr.emp_id = ".dbPStr($emp_id)." and tr.is_newest = 1)".
        " left outer join study_assembly_application aa on ( aa.id = co.id and aa.emp_id = ".dbPStr($emp_id).")".
        " left outer join study_assembly sa on ( sa.id = co.id and sa.no = aa.no and sa.delete_flg = false )".
        " where co.delete_flg = false".
        " and (co2.assembly_end_date <> '' or co2.questionnaire_end_date <> '' or co2.test_end_date <> '')".
        " " . ($bunrui_year ? " and co.bunrui_year = ".dbPStr($bunrui_year) : "").
        $key_sql;


        //=================================================
        // 履歴を表示しない場合
        // 集合研修/アンケート/テスト のうち、どれかが要記入のもの
        //=================================================
        if (!$disp_out_of_date) {
            $sql .=
            " and co2.is_out_of_date = ''". // 最大終了日が終了していないもの
            " and (".

            // 集合研修が要入力のものを抽出
            "         (co2.assembly_require = 1 and ts.assembly_status = 0)". // 集合研修ありで、集合研修が未終了のもの

            // アンケートが要入力のものを抽出
            "      or (".
            "          (co.questionnaire_start_date <= '".$now."' and '".$now."' <= co.questionnaire_end_date)". // ｱﾝｹｰﾄを実施する。かつ、本日が期間内
            "          and ts.questionnaire_status <> 1". // ｱﾝｹｰﾄ済フラグがオフ
            "          and not (".
            "              co2.assembly_policy_include = 1". // 集合研修を実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.questionnaire_policy_include = 1". // アンケートも実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.policy_for_questionnaire = 'A+'". // アンケートは集合研修が済んでからで
            "              and (ts.assembly_actual_status is null or ts.assembly_actual_status <> 1)". // なのに集合研修実績なし
            "          )".
            "          and not (".
            "              co2.assembly_policy_include = 1". // 集合研修を実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.questionnaire_policy_include = 1". // アンケートも実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.policy_for_questionnaire = 'A-'". // アンケートは集合研修を受けていれば不要で
            "              and (ts.assembly_actual_status is not null and ts.assembly_actual_status = 1)". // なのに集合研修実績あり
            "          )".
            "      )".

            // テストが要入力のものを抽出
            "      or (".
            "          (co.test_start_date <= '".$now."' and '".$now."' <= co.test_end_date)". // テストを実施する。かつ、本日が期間内
            "          and ts.test_status <> 2". // テスト合格済フラグがオフ
            "          and not (".
            "              co2.assembly_policy_include = 1". // 集合研修を実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.test_policy_include = 1". // テストも実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.policy_for_test = 'A+'". // テストは集合研修が済んでからで
            "              and (ts.assembly_actual_status is null or ts.assembly_actual_status <> 1)". // なのに集合研修実績なし
            "          )".
            "          and not (".
            "              co2.assembly_policy_include = 1". // 集合研修を実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.test_policy_include = 1". // テストも実施することになっている場合で（日付設定不備・期間外は無視してよい）
            "              and co2.policy_for_test = 'A-'". // テストは集合研修を受けていれば不要で
            "              and (ts.assembly_actual_status is not null and ts.assembly_actual_status = 1)". // なのに集合研修実績あり
            "          )".
            "      )".
            " )";
        }

        $sql = " select * from (".$sql.") d order by max_end_date1 asc, is_out_of_date, max_end_date desc ";//.$menu_user_sort;
        $raw_rows = dbFind($sql);
        $rows = array();
        //--------------------------
        // 教材・ﾃｷｽﾄをバラす
        // 実施内容をバラす
        //--------------------------
        foreach ($raw_rows as $idx=>$row) {
            $parent_ids = StudyCommonModel::get_folder_parents("id", $row["folder_id"], $folder_list);
            if ($folder_id && !in_array($folder_id, $parent_ids)) continue;
            $row["folder_ids"] = implode("\t", $parent_ids);
            $row["folder_disp"] = h(implode("＞",StudyCommonModel::get_folder_parents("name", $row["folder_id"], $folder_list)));
            $row["files_info"] = StudyContentsModel::get_files_info_to_array($row["files_info_ary"]);
            $row["policy_info"] = get_policy_info($row["policy_code"]);
            $rows[] = $row;
        }
        return $rows;
    }

    //**********************************************************************************************
    // 【管理側】研修・講義  新規登録 or 編集 or 複製
    //**********************************************************************************************
    function edit_contents($req, $exec_mode) { // update or create or copy

        //==================================================
        // 教材・ﾃｷｽﾄ
        // 情報はfiles_infoフィールドにまとめて格納する
        //==================================================
        $file_count = (int)$req["file_count"]; // 教材ﾃｷｽﾄのファイル数 1〜10、またはゼロ
        $files_info = array(); // DBへ格納用の配列
        $move_files = array(); // アップロードファイル情報
        $del_files = array(); // 削除予定ファイルパス
        $copy_files = array(); // 複製ファイル情報
        for ($idx=1; $idx<=10; $idx++) { // 最大10個
            //--------------------------
            // 指定したファイル数以上のファイル
            //--------------------------
            if ($idx > $file_count) {
                $del_files[] = @$req["file_path".$idx]; // 元ファイルパスHIDDEN値
                continue; // 次へ
            }
            //--------------------------
            // DB保存予定の値
            //--------------------------
            $info = array(
                "file_no"       => @$req["file_no".$idx],           // 画面入力 ファイル番号テキスト
                "file_type"     => @$req["file_type".$idx],         // 画面入力 ファイルタイプ
                "file_name"     => @$req["cur_file_name".$idx],     // 更新前のファイル名(HIDDEN)
                "file_url"      => "",                              // ファイルURL、ひとまずカラに
                "file_path"     => @$req["file_path".$idx],         // 更新前のファイルパス(HIDDEN)
                "file_capacity" => @$req["cur_file_capacity".$idx], // 更新前のファイルサイズ(HIDDEN)(キロバイト)
            );

            $which = ($req["file_which".$idx] == "url" ? "url" : "upload"); // URL指定またはアップロード
            //--------------------------
            // ◆URL指定の場合
            //--------------------------
            if ($which=="url") {
                $del_files[] = @$req["file_path".$idx]; // 元ファイルパスHIDDEN値
                $info["file_url"] = $req["file_url".$idx]; // URL
                $info["file_name"] = $req["file_name".$idx]; // リンク表示
                $info["file_path"] = ""; // 新ファイルパス：なし
                $info["file_capacity"] = ""; // 新ファイルサイズ：なし
            }
            //--------------------------
            // ◆ｱｯﾌﾟﾛｰﾄﾞ指定の場合
            //--------------------------
            else {
                //--------------------------
                // ファイルパス未指定の場合
                //--------------------------
                if (!@$req["file_path".$idx]) {
                    $info["file_name"] = "";
                    $info["file_capacity"] = "";
                }
                //--------------------------
                // 削除指定の場合
                //--------------------------
                if ($req["delete_flag".$idx]) {
                    $del_files[] = @$req["file_path".$idx]; // 元ファイルパスHIDDEN値
                    $info["file_path"] = ""; // 更新前ファイルパスをクリア
                    $info["file_name"] = ""; // 更新前ファイル名クリア
                    $info["file_capacity"] = ""; // 更新前ファイルサイズをクリア
                }
                //--------------------------
                // 削除指定でなく、アップロードファイルがある場合
                //--------------------------
                else if (@$_FILES["file".$idx]["tmp_name"] && @$_FILES["file".$idx]["size"]) { // 有効ファイル名と有効サイズならアップロードする
                    $del_files[] = @$req["file_path".$idx]; // 元ファイルパスHIDDEN値
                    $new_size = @$_FILES["file".$idx]["size"];  // アップロードファイルのサイズ
                    if ($new_size > 1024) $new_size = round($new_size / 1024); // ファイル容量はKB整数単位で保存する
                    else if ($new_size != 0) $new_size = 1; // もしサイズが0バイト以上で、1Kb以下なら、1Kbにする
                    $info["file_capacity"] = $new_size; // ファイル容量
                    $info["file_name"] = @$_FILES["file".$idx]["name"]; // 新ファイル名をセット
                    $info["file_path"] = GG_FILE_HOME.GG_SESSION_ID."_".time()."_".$idx; // 新しいファイルパスをセット
                    $move_files[@$_FILES["file".$idx]["tmp_name"]] = $info["file_path"];
                }
                //--------------------------
                // 削除指定でなく、アップロードファイルもないが、複製時更新前ファイルがある場合
                //--------------------------
                else if ($exec_mode=="copy" && @$req["file_path".$idx]) {
                    $info["file_path"] = GG_FILE_HOME.GG_SESSION_ID."_".time()."_".$idx; // 新しいファイルパスをセット
                    $copy_files[@$req["file_path".$idx]] = $info["file_path"];
                }
            }
            $atmp = array();
            global $files_info_fields;
            foreach ($files_info_fields as $field) $atmp[] = $info[$field]; // パラメータを決められた順にPUSHする
            $files_info[] = implode("\t", $atmp); // タブ区切りで１件追加
        }
        dbBeginTransaction(); // トランザクション開始
        //==================================================
        // 更新
        //==================================================
        if ($exec_mode=="update") {
            $contents_id = $req["contents_id"];
            $sql =
            " update study_contents set".
            " folder_id = ".dbPIntC($req["save_folder_id"]).
            ",title = ".dbPStr($req["contents_name"]). // 研修・講義タイトル
            ",keyword = ".dbPStr($req["keyword"]). // キーワード
            ",summary = ".dbPStr($req["summary"]). // 内容
            ",page_cnt = ".dbPIntC($page_cnt).
            ",update_user_id = ".dbPStr(GG_LOGIN_EMP_ID).
            ",update_date = ".dbPTime(date("Y/m/d H:i:s", time())).
            ",files_info = ".dbPStr(implode("\n",$files_info)). // ファイル情報。複数あれば改行つなぎで追加
            ",bunrui_year = ".dbPStr($req["bunrui_year"]).
            ",policy_code = ".dbPStr($req["policy_code"]).
            ",policy_jp1 = ".dbPStr($req["policy_jp1"]).
            ",policy_jp2 = ".dbPStr($req["policy_jp2"]).
            ",policy_for_questionnaire = ".dbPStr($req["policy_for_questionnaire"]).
            ",policy_for_test = ".dbPStr($req["policy_for_test"]).
            " where id = ".dbPIntC($contents_id)." and delete_flg = false";
            dbExec($sql);
            //--------------------------
            // ★★★総合値の再計算(全受講者)
            //--------------------------
            StudyCommonModel::update_total_status($contents_id);
        }
        //==================================================
        // 新規登録 or 複製複製
        //==================================================
        else {
            $contents_id = dbGetNextSequence("study_contents_id");
            $sql =
            " insert into study_contents (".
            " id, folder_id, title, keyword, summary, page_cnt, create_user_id, update_user_id, create_date, update_date".
            ",files_info, delete_flg, bunrui_year, policy_code, policy_jp1, policy_jp2, policy_for_questionnaire, policy_for_test".
            " ) values (".
            " ".dbPIntC($contents_id).
            ",".dbPIntC($req["save_folder_id"]).
            ",".dbPStr($req["contents_name"]). // 研修・講義タイトル
            ",".dbPStr($req["keyword"]). // キーワード
            ",".dbPStr($req["summary"]). // 内容
            ",".dbPIntC($page_cnt).
            ",".dbPStr(GG_LOGIN_EMP_ID).
            ",".dbPStr(GG_LOGIN_EMP_ID).
            ",".dbPTime(date("Y/m/d H:i:s", time())).
            ",".dbPTime(date("Y/m/d H:i:s", time())).
            ",".dbPStr(implode("\n",$files_info)). // ファイル情報。複数あれば改行つなぎで追加
            ",".dbPBool(false).
            ",".dbPStr($req["bunrui_year"]).
            ",".dbPStr($req["policy_code"]).
            ",".dbPStr($req["policy_jp1"]).
            ",".dbPStr($req["policy_jp2"]).
            ",".dbPStr($req["policy_for_questionnaire"]).
            ",".dbPStr($req["policy_for_test"]).
            ")";
            dbExec($sql);
        }

        dbEndTransaction(); // トランザクション終了

        //==================================================
        // アップロードファイルがあれば、ディスクにファイルを保存
        //==================================================
        foreach ($move_files as $tmp_path => $new_path) {
            if (!@move_uploaded_file($tmp_path, $new_path)) {
                $emsg = "アップロードファイルのディスク書込みに失敗しました。(".$tmp_path." to ".$new_path.")";
                logger_error($emsg);
                fatal_exception($emsg, __FILE__." ".__LINE__);
            }
            chmod($new_path, 0666);
        }
        //==================================================
        // 複製ファイルがあれば複製
        //==================================================
        foreach ($copy_files as $from_path => $new_path) {
            if (!@copy($from_path, $new_path)) {
                $emsg = "ファイルの複製に失敗しました。(".$from_path." to ".$new_path.")";
                logger_error($emsg);
            }
            chmod($new_path, 0666);
        }
        //==================================================
        // 新規・複製でなければ、更新前ファイルを削除
        //==================================================
        if ($exec_mode=="update") {
            foreach ($del_files as $del_path) {
                if (!$del_path) continue;
                if (!@unlink($del_path)) {
                    $emsg = $del_path."の削除に失敗しました。";
                    logger_error($emsg);
                }
            }
        }
        return $contents_id;
    }

    //**********************************************************************************************
    // 【管理側】研修・講義を複製
    //  この処理を実行する前に、study_contentsテーブルは作成済み
    //**********************************************************************************************
  function copy_extra_data($contents_id, $new_contents_id) {
        dbBeginTransaction(); // トランザクション開始
        //--------------------------
        // 受講者を複製
        //--------------------------
        $sql =
            " insert into study_emp_list ( id, study_emp_id ) ".
            " select ".dbPIntC($new_contents_id).", study_emp_id".
            " from study_emp_list".
            " inner join authmst on (study_emp_id = authmst.emp_id and authmst.emp_manabu_flg = true )".
            " where id = ".dbPIntC($contents_id);
        dbExec($sql);
        //--------------------------
        // 受講者ステータスを複製
        //--------------------------
        $sql =
            " insert into study_test_status (".
            "     test_id, edition, emp_id, total_status, assembly_status".
            "    ,test_status, assembly_actual_status, assembly_actual_date, questionnaire_status".
            " ) select".
            "     ".dbPIntC($new_contents_id).", 1, study_emp_id, 0, 0".
            "    ,0, 0, '', 0".
            " from study_emp_list".
            " inner join authmst on (study_emp_id = authmst.emp_id and authmst.emp_manabu_flg = true )".
            " where id = ".dbPIntC($contents_id);
        dbExec($sql);
        //--------------------------
        // ★★★受講者数を再計算
        //--------------------------
        StudyCommonModel::update_student_count($new_contents_id);
        //--------------------------
        // アンケートを複製
        //--------------------------
        $sql =
            " insert into study_questionnaire ( id, edition, no, question, type, required, choice_cnt, choices ) ".
            " select ".dbPIntC($new_contents_id).
            ",1, no, question, type, required, choice_cnt, choices".
            " from study_questionnaire".
            " where id = ".dbPIntC($contents_id).
            " and edition = 1";
        dbExec($sql);
        //--------------------------
        // ★★★アンケート設問数を再計算
        //--------------------------
        StudyCommonModel::update_questionnaire_count($new_contents_id);
        //--------------------------
        // テストを複製
        //--------------------------
        $sql =
            " insert into study_test (".
            " id".
            ",edition, title, student_group_id, msg_title".
            ",create_user_id, update_user_id, create_date, update_date".
            ",delete_flg, impression_flg, limit_time, border_score, test_item_order, student_personal_id".
            " ) select".
            " ".dbPIntC($new_contents_id).
            ",1, title, student_group_id, msg_title".
            ",".dbPStr(GG_LOGIN_EMP_ID).
            ",".dbPStr(GG_LOGIN_EMP_ID).
            ",".dbPTime(date("Y/m/d H:i:s", time())).
            ",".dbPTime(date("Y/m/d H:i:s", time())).
            ",delete_flg, impression_flg, limit_time, border_score, test_item_order, student_personal_id".
            " from study_test".
            " where id = ".dbPIntC($contents_id).
            " and edition = 1";
        dbExec($sql);
        //--------------------------
        // テスト問題を複製
        //--------------------------
        $sql =
            " insert into study_test_item (".
            " id".
            ",edition, no, question, comment, image_path, image_name, choice_cnt, multiple, choices, answers".
            " ) select".
            " ".dbPIntC($new_contents_id).
            ",1, no, question, comment, image_path, image_name, choice_cnt, multiple, choices, answers".
            " from study_test_item".
            " where id = ".dbPIntC($contents_id).
            " and edition = 1";
        dbExec($sql);
        //--------------------------
        // テスト画像を複製
        //--------------------------
        StudyTestModel::copy_images($contents_id, $new_contents_id);
        //--------------------------
        // ★★★テスト問題数を再計算
        //--------------------------
        StudyCommonModel::update_test_count($new_contents_id);

        dbEndTransaction(); // トランザクション終了
        return $new_contents_id;
    }
    //**********************************************************************************************
    // 【管理側】研修・講義を削除 ファイルも削除
    //**********************************************************************************************
    function delete_contents($contents_id) {
        //--------------------------
        // ファイル抹消
        //--------------------------
        $files_info = dbGetOne("select files_info from study_contents where id = ".dbPIntC($contents_id));
        $files_info_ary = StudyContentsModel::get_files_info_to_array($files_info);
        foreach ($files_info_ary as $row) {
            $file_path = $row["file_path"];
            if ($file_path && is_file($file_path)) unlink ($file_path);
        }
        //--------------------------
        // delete_flgを立てる
        //--------------------------
        $sql =
        " update study_contents set".
        " delete_flg = true".
        ",update_date = ".dbPTime(date("Y/m/d H:i:s", time())).
        ",update_user_id = ".dbPStr(GG_LOGIN_EMP_ID).
        " where id = ".dbPIntC($contents_id)." and delete_flg = false";
        dbExec($sql);
    }

    //**********************************************************************************************
    // 【管理側】受講状況の元データを作成
    //**********************************************************************************************
    function make_calculated_summary(&$rows, $contents_id, $policy_info, $output_type) {
        $ttl = array();
        $record_count = count($rows);

        //--------------------------------------------------
        // ◆◆◆col:列番号 ＝ 受講項目の並び順をあらわす（集合研修/アンケート/テスト）
        // ◆◆◆row:行番号 ＝ 以下?から?まで
        //--------------------------------------------------
        // ?受講者数
        // ------------ 共通出力／単体結果
        // ?単体受講済数
        // ?単体未修数（?＋?）
        // ?単体受講率％
        // ------------ グループ（OR条件）がある場合
        // ?累積受講済数
        // ?累積未修数
        // ?累積受講率％
        // ------------ 共通出力／差引結果（＝マルチ構成（AND構成）の結果）
        // ?差引受講済数
        // ?差引未修数
        // ?差引受講率％
        // ------------ エクストラ
        // ?ﾃｽﾄ実施中（テスト不合格者。?に含まれる）
        // ?ﾃｽﾄ未実施（?−?）
        //--------------------------------------------------

        //--------------------------------
        // 初期化
        //--------------------------------
        foreach ($policy_info as $func => $info) {
            $ttl["col".$info["col"]."_row1"] = $record_count; // ?対象者数
            $ttl["col".$info["col"]."_row2"] = 0; // ?単体受講済数
            if ($info["group_size"]>=2) { // ORグループの場合
                $ttl["col".$info["col"]."_row5".$row] = 0; // ?累積受講済数
            }
            $ttl["col".$info["col"]."_row9"] = 0; // ?差引未修数
            if ($func=="test") { // テストの場合
                $ttl["col".$info["col"]."_row11"] = 0; // ? 単体ﾃｽﾄ実施中
                $ttl["col".$info["col"]."_row12"] = 0; // ? 単体ﾃｽﾄ未実施
            }
        }

        //==========================
        // ◆受講者ループ
        //==========================
        foreach ($rows as $data_idx => $row) {
            $is_group_ok = array(); // ORグループ別で、OK状態を管理する
            $is_multi_ng = 0; // ANDマルチ構成は、NG状態を管理する
            //==========================
            // ◆受講項目別ループ
            //==========================
            foreach ($policy_info as $func => $info) {
                $col = $info["col"]; // 列番号(=受講項目並び番号)
                $group = $info["group"]; // この列のグループ
                $hissu_mark = $info["hissu_mark"]; // この列が計算対象なら"+"
                $group_index = $info["group_index"]; // この列のグループ内順番
                $group_size = $info["group_size"]; // この列のグループメンバ数

                // ◆◆◆まずは受講項目が受講済かどうかを確認
                $isOK = 0;
                if ($func=="questionnaire" && $row["questionnaire_status"] =="1") $isOK = 1;
                else if ($func=="assembly" && $row["assembly_actual_status"] =="1") $isOK = 1;
                else if ($func=="test" && $row["test_status"] =="2") $isOK = 1;

                // ◆◆◆この受講項目の単体受講済状況(? and ??)
                if ($isOK) $ttl["col".$col."_row2"]++; // ?単体受講済数
                if ($func=="test" && !$isOK) {
                    if ($row["test_status"] =="1") $ttl["col".$col."_row11"]++; // ?単体ﾃｽﾄ実施中
                    if (!(int)$row["test_status"]) $ttl["col".$col."_row12"]++; // ?単体ﾃｽﾄ未実施
                }
                // ◆◆◆ORグループについて(?)
                if ($isOK) $is_group_ok[$group] = 1; // この受講項目が受講済なら、ORグループとして、OKとなる
                if ($group_size >= 2) { // ORグループなら
                    if ($is_group_ok[$group]) $ttl["col".$col."_row5"]++; // ?累積受講済数 この時点で、ORグループとしてOKなら1追加
                }
                // ◆◆◆ANDマルチ構成について(?)
                if ($group_index==$group_size) { // ORグループを全部チェックした後なら
                    if ($hissu_mark=="+" && !$is_group_ok[$group]) $is_multi_ng = 1; // このORグループが計算対象かつ結果NGなら、行全体としてNGである
                    if ($is_multi_ng) $ttl["col".$col."_row9"]++; // ?差引未修数 この時点で全体としてNGとなっているなら、この受講項目に1追加
                }
            }
        }

        //--------------------------------------------------------------
        // その他項目を計算
        //--------------------------------------------------------------
        foreach ($policy_info as $func => $info) {
            $col = $info["col"];
            $ttl["col".$col."_row3"] = $record_count - $ttl["col".$col."_row2"]; // ?単体未修数
            $ttl["col".$col."_row4"] = get_percent($ttl["col".$col."_row2"], $record_count, 1, 100); // ?単体受講率
            if ($info["group_size"]>=2) { // ORグループの場合
                $ttl["col".$col."_row6"] = $record_count - $ttl["col".$col."_row5"]; // ?累積未修数
                $ttl["col".$col."_row7"] = get_percent($ttl["col".$col."_row5"], $record_count, 1, 100); // ?累積受講率
            }
            $ttl["col".$col."_row8"] = $record_count - $ttl["col".$col."_row9"]; // ?差引済数
            $ttl["col".$col."_row10"] = get_percent($ttl["col".$col."_row8"], $record_count, 1, 100); // ?差引受講率
        }
        return $ttl;
    }
}
