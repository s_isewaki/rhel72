<?
require_once("StudyFolderModel.php");

//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// 共通系DBアクセス・ユーティリティ（ユーザ系・ステータス系・一部サマリングデータ作成系 など）
// 一部、DB関連外関数も扱う
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
class StudyCommonModel {


    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    //
    // Comedixデータアクセス関連
    //
    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    //**********************************************************************************************
    // 指定$emp_idユーザの情報を取得
    //**********************************************************************************************
    function get_user_data_by_emp_id($emp_id) {
        $sql =
        " select emp_personal_id,  emp_id,  emp_birth,  emp_job,  emp_lt_nm,  emp_ft_nm ,emp_email2 from empmst".
        " where emp_id = ".dbPStr($emp_id);
        return dbGetTopRow($sql);
    }
    function get_user_data_by_emp_personal_id($emp_personal_id) {
        $sql =
        " select em.emp_personal_id,  em.emp_id,  em.emp_birth,  em.emp_job,  em.emp_lt_nm,  em.emp_ft_nm from empmst em".
        " inner join authmst on (authmst.emp_id = em.emp_id and authmst.emp_manabu_flg  = true)".   
        " where  em.emp_personal_id = ".dbPStr($emp_personal_id);
        return dbGetTopRow($sql);
    }
    
        function get_user_data_by_emp_login_id($emp_personal_id) {
        $sql =
        " select em.emp_personal_id,  em.emp_id,  em.emp_birth,  em.emp_job,  em.emp_lt_nm,  em.emp_ft_nm from empmst em".
        " inner join authmst on (authmst.emp_id = em.emp_id and authmst.emp_manabu_flg  = true)".   
        " left outer join login on (em.emp_id = login.emp_id)".
        " where  login.emp_login_id = ".dbPStr($emp_personal_id);
        return dbGetTopRow($sql);
    }
    
    
    
    
    
    //**********************************************************************************************
    // 「職員ID=ログインID」のリストを返す
    //**********************************************************************************************
    function get_login_id_list() {
        $ret = array();
        $sql =
        " select em.emp_id, lg.emp_login_id from login lg".
        " inner join empmst em on (lg.emp_id = em.emp_id)".
        " where em.emp_id <> ''";
        $rows = dbFind($sql);
        foreach ($rows as $row) {
            $ret[$row["emp_id"]] = $row["emp_login_id"];
        }
        return $ret;
    }

    //**********************************************************************************************
    // コンボボックス選択肢
    //**********************************************************************************************

    //======================================================
    // コンボボックス用「職種」
    //======================================================
    function get_job_list() {
        return dbFind("select * from jobmst where job_del_flg = false order by order_no");
    }
    //======================================================
    // コンボボックス用「役職」
    //======================================================
    function get_status_list() {
        return dbFind("select * from stmst where st_del_flg = false order by order_no");
    }
    //======================================================
    // コンボボックス用「部」 権限制限あり
    //======================================================
    function get_class_list($no_auth_strict=0) {
        $sql = "select * from classmst where class_del_flg = false";
        //--------------------
        // 教育担当者権限のみなら制限（実質は職員検索画面のみ制限）
        //--------------------
        if (!$no_auth_strict && GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG) {
            global $study_tantou_branches;
            $sql .= " and (1 <> 1";
            foreach ($study_tantou_branches as $r) {
                //if (!$r["class_id"]) continue;
                $sql .= " or class_id = ".dbPIntC($r["class_id"]);
            }
            $sql .= ")";
        }
        return dbFind($sql." order by order_no asc");
    }
    //======================================================
    // コンボボックス用「課」 権限制限あり
    //======================================================
    function get_attr_list($class = null, $no_auth_strict=0) {
        $sql = "select * from atrbmst am".
        " left outer join classmst cm on (cm.class_id = am.class_id)".
        " where am.atrb_del_flg = false".
        " and cm.class_del_flg = false";
        if ($class) $sql .= " and am.class_id = ".dbPIntC($class);
        //--------------------
        // 教育担当者権限のみなら制限（実質は職員検索画面のみ制限）
        //--------------------
        if (!$no_auth_strict && GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG) {
            global $study_tantou_branches;
            $sql .= " and (1 <> 1";
            foreach ($study_tantou_branches as $r) {
                $sql .=
                " or (".
                "     am.class_id=".dbPIntC($r["class_id"]).
                "     " . ($r["atrb_id"] ? " and am.atrb_id = ".dbPIntC($r["atrb_id"]) : "").
                " )";
            }
            $sql .= ")";
        }
        return dbFind($sql." order by am.order_no");
    }
    //======================================================
    // コンボボックス用「科」 権限制限あり
    //======================================================
    function get_dept_list($attr = null, $no_auth_strict=0) {
        $sql =
        " select dm.*, am.class_id from deptmst dm".
        " left outer join atrbmst am on (am.atrb_id = dm.atrb_id)".
        " left outer join classmst cm on (cm.class_id = am.class_id)".
        " where dm.dept_del_flg = false".
        " and am.atrb_del_flg = false".
        " and cm.class_del_flg = false";
        if ($attr) $sql .= " and dm.atrb_id = ".dbPIntC($attr);
        //--------------------
        // 教育担当者権限のみなら制限（実質は職員検索画面のみ制限）
        //--------------------
        if (!$no_auth_strict && GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG) {
            global $study_tantou_branches;
            $sql .= " and (1 <> 1";
            foreach ($study_tantou_branches as $r) {
                $sql .=
                " or (".
                "     am.class_id=".dbPIntC($r["class_id"]).
                "     " . ($r["atrb_id"] ? " and dm.atrb_id=".dbPIntC($r["atrb_id"]) : "").
                "     " . ($r["dept_id"] ? " and dm.dept_id=".dbPIntC($r["dept_id"]) : "").
                " )";
            }
            $sql .= ")";
        }
        return dbFind($sql." order by dm.order_no asc");
    }
    //======================================================
    // コンボボックス用「室」 権限制限あり
    //======================================================
    function get_room_list($room_id = null, $no_auth_strict=0) {
        $sql =
        " select cr.*, am.atrb_id, am.class_id from classroom cr".
        " left outer join deptmst dm on (dm.dept_id = cr.dept_id)".
        " left outer join atrbmst am on (am.atrb_id = dm.atrb_id)".
        " left outer join classmst cm on (cm.class_id = am.class_id)".
        " where cr.room_del_flg = false";
        " and dm.dept_del_flg = false".
        " and am.atrb_del_flg = false".
        " and cm.class_del_flg = false";
        if ($room_id) $sql .= " and cr.room_id = ".dbPIntC($room_id);
        //--------------------
        // 教育担当者権限のみなら制限（実質は職員検索画面のみ制限）
        //--------------------
        if (!$no_auth_strict && GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG) {
            global $study_tantou_branches;
            $sql .= " and (1 <> 1";
            foreach ($study_tantou_branches as $r) {
                $sql .=
                " or (".
                "     am.class_id=".dbPIntC($r["class_id"]).
                "     " . ($r["atrb_id"] ? " and dm.atrb_id=".dbPIntC($r["atrb_id"]) : "").
                "     " . ($r["dept_id"] ? " and dm.dept_id=".dbPIntC($r["dept_id"]) : "").
                "     " . ($r["room_id"] ? " and cr.room_id=".dbPIntC($r["room_id"]) : "").
                " )";
            }
            $sql .= ")";
        }
        return dbFind($sql." order by cr.order_no asc");
    }













    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    //
    // 受講者
    //
    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    //**********************************************************************************************
    // 受講者追加
    //**********************************************************************************************
    function add_student($contents_id, $emp_list) {
        foreach($emp_list as $emp_id) {
            if (!$emp_id) continue;
            //--------------------------
            // 既に存在する場合は追加しない
            //--------------------------
            if ((int)dbGetOne("select count(id) from study_emp_list where id = ".dbPIntC($contents_id)." and study_emp_id = ".dbPStr($emp_id))) continue;
            //--------------------------
            // 受講者追加
            //--------------------------
            dbExec("insert into study_emp_list ( id, study_emp_id ) values (".dbPIntC($contents_id).", ".dbPStr($emp_id).");");
            //--------------------------
            // 受講者ステータス追加
            //--------------------------
            $sql =
                " insert into study_test_status (".
                "     test_id, edition, emp_id, total_status, assembly_status".
                "    ,test_status, assembly_actual_status, assembly_actual_date, questionnaire_status".
                " ) values (".
                "     ".dbPIntC($contents_id).", 1, ".dbPStr($emp_id).", 0, 0".
                "    ,0, 0, '', 0".
                " )";
            dbExec($sql);
        }
        //--------------------------
        // ★★★受講者数サマリング再計算
        //--------------------------
        StudyCommonModel::update_student_count($contents_id);
        //--------------------------
        // ★★★総合値の再計算
        //--------------------------
        StudyCommonModel::update_total_status($contents_id);
    }
    //**********************************************************************************************
    // 受講者削除
    //**********************************************************************************************
    function del_student($contents_id, $emp_list) {
        foreach ($emp_list as $emp_id) {
            if (!$emp_id) continue;
            //--------------------------
            // なんらかを実施済なら追加しない
            // テスト不合格でも実施済とみなし、追加しない
            //--------------------------
            $sql =
            " select count(*) from study_test_status".
            " where test_id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr($emp_id).
            " and (assembly_actual_status = 1 or test_status = 1 or test_status = 2 or questionnaire_status = 1)";
            if ((int)dbGetOne($sql)) continue;
            //--------------------------
            // 受講者削除
            //--------------------------
            dbExec("delete from study_emp_list where id = ".dbPIntC($contents_id)." and study_emp_id = ".dbPStr($emp_id));
            //--------------------------
            // 受講者ステータス削除
            //--------------------------
            dbExec("delete from study_test_status where test_id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr($emp_id)." and edition = 1");
            //--------------------------
            // 集合研修申込情報を削除
            //--------------------------
            dbExec("delete from study_assembly_application where id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr($emp_id));
            //--------------------------
            // お知らせ既読状態を削除
            //--------------------------
            $sql =
            " delete from study_notif_confirm where notif_emp_id = ".dbPStr($emp_id).
            " and notif_id in ( select id from study_notification where contents_id = ".dbPIntC($contents_id).")";
            dbExec($sql);
        }
        //--------------------------
        // ★★★受講者数サマリング再計算
        //--------------------------
        StudyCommonModel::update_student_count($contents_id);
        //--------------------------
        // ★★★総合値の再計算
        //--------------------------
        StudyCommonModel::update_total_status($contents_id);
    }
    //**********************************************************************************************
    // 従業員一覧取得（受講者かどうかは関係ない）
    //**********************************************************************************************
    function get_user_list($joken, $no_auth_strict=0) {

        $sql =
        " select distinct em.emp_id".
        ",em.emp_personal_id".
        ",em.emp_ft_nm AS first_name".
        ",em.emp_lt_nm AS last_name".
        ",em.emp_kn_ft_nm".
        ",em.emp_kn_lt_nm".
        ",em.emp_retire".
        ",authmst.emp_del_flg".
        ",authmst.emp_manabu_flg".
        ",cm.class_nm AS class_nm".
        ",am.atrb_nm".
        ",dm.dept_nm".
        ",cr.room_nm".
        ",login.emp_login_id AS login_id".
        ",jobmst.job_nm".
        ",stmst.st_nm".
        " from empmst em".
        " inner join authmst on (em.emp_id = authmst.emp_id  and authmst.emp_manabu_flg = true)".
        " left outer join classmst cm on (em.emp_class = cm.class_id AND cm.class_del_flg = false)".
        " left outer join atrbmst am on (em.emp_attribute = am.atrb_id AND am.atrb_del_flg = false)".
        " left outer join deptmst dm on (em.emp_dept = dm.dept_id AND dm.dept_del_flg = false)".
        " left outer join classroom cr on (em.emp_room = cr.room_id AND cr.room_del_flg = false)".
        " left outer join jobmst on (em.emp_job = jobmst.job_id AND jobmst.job_del_flg = false)".
        " left outer join stmst on (em.emp_st = stmst.st_id AND stmst.st_del_flg = false)".
        " left outer join login on (em.emp_id = login.emp_id)".
        " left outer join concurrent ccr on (em.emp_id = ccr.emp_id)".
        " where em.emp_id is not null ";

        //--------------------
        // 画面指定された検索条件
        //--------------------
        $where = "";
        if ($joken["class"])  $where .= " and (em.emp_class = ".dbPIntC($joken["class"])." or ccr.emp_class = ".dbPIntC($joken["class"]).")";
        if ($joken["attr"])   $where .= " and (em.emp_attribute = ".dbPIntC($joken["attr"])." or ccr.emp_attribute = ".dbPIntC($joken["attr"]).")";
        if ($joken["dept"])   $where .= " and (em.emp_dept = ".dbPIntC($joken["dept"])." or ccr.emp_dept = ".dbPIntC($joken["dept"]).")";
        if ($joken["room"])   $where .= " and (em.emp_room = ".dbPIntC($joken["room"])." or ccr.emp_room = ".dbPIntC($joken["room"]).")";
        if ($joken["id"])     $where .= " and em.emp_personal_id like ".dbPStr("%".str_replace("%","'",$joken["id"])."%");
        if ($joken["emp_id"]) $where .= " and em.emp_id = ".dbPStr($joken["emp_id"]); // ★★★ 現状、emp_idが渡ってくることは無い(20140821)
        if ($joken["job"])    $where .= " and em.emp_job = ".dbPIntC($joken["job"]);
        if ($joken["status"]) $where .= " and em.emp_st = ".dbPIntC($joken["status"]);
        if ($joken["name"]) {
            $names = explode(" ", str_replace("　", " ", $joken["name"]));
            $tmp = array();
            foreach ($names as $nm) {
                if (!strlen($nm)) continue;
                $tmp[]= "(em.emp_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or em.emp_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or  em.emp_kn_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")."  or em.emp_kn_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." )";
            }
            if (count($tmp)) $where .= " and ".implode(" and ", $tmp);
        }


        $where .= " and (";
        $where .= "   (";
        $where .= "     (em.emp_retire is null or em.emp_retire = '' or em.emp_retire > '".date("Ymd")."')";
        $where .= "     and authmst.emp_del_flg = false";
        $where .= "   )";
        if (@$joken["emp_expire_type"]=="include_emp_retire") {
            $where .= " or (em.emp_retire is not null and em.emp_retire <> '' and em.emp_retire < '".date("Ymd")."')";
        }
        if (@$joken["emp_expire_type"]=="include_emp_del_flg")  $where .= " or authmst.emp_del_flg = true";
        $where .= " )";


        //--------------------
        // 教育担当者権限なら制限。管理者の場合は「教育責任者」「教育担当者」を呼ぶ「職員選択画面」では制限をかけない。
        //--------------------
        if (GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG && !$no_auth_strict) {
            global $study_tantou_branches;
            $sql .= " and (1 <> 1";
            foreach ($study_tantou_branches as $r) {
                if ($r["class_id"]) $sql .= " or (";
                if ($r["class_id"]) $sql .= "     cm.class_id=".dbPIntC($r["class_id"]);
                if ($r["atrb_id"])  $sql .= "     and am.atrb_id=".dbPIntC($r["atrb_id"]);
                if ($r["dept_id"])  $sql .= "     and dm.dept_id=".dbPIntC($r["dept_id"]);
                if ($r["room_id"])  $sql .= "     and cr.room_id=".dbPIntC($r["room_id"]);
                if ($r["class_id"]) $sql .= " )";
            }
            $sql .= ")";
        }

        $sql .= $where." order by am.atrb_nm, dm.dept_nm, cr.room_nm, em.emp_kn_lt_nm, em.emp_kn_ft_nm";
        if (((int)@$joken["per_page_size"])>0) {
            if ($joken["page_num"]<1) $joken["page_num"] = 1;
            $sql .= " limit ".((int)$joken["per_page_size"]) . " offset ".(($joken["page_num"]-1)*$joken["per_page_size"]);
        }
        return dbFind($sql);
    }
    
    
     //**********************************************************************************************
    // 従業員一覧取得(ライセンス権限がある人のみ)
    //**********************************************************************************************
        function get_user_list_auth($joken, $no_auth_strict=0) {
        $sql =
        " select distinct em.emp_id".
        ",em.emp_personal_id".
        ",em.emp_ft_nm AS first_name".
        ",em.emp_lt_nm AS last_name".
        ",em.emp_kn_ft_nm".
        ",em.emp_kn_lt_nm".
        ",em.emp_retire".
        ",em.emp_email".
        ",authmst.emp_del_flg".
        ",authmst.emp_manabu_flg".
        ",cm.class_nm AS class_nm".
        ",am.atrb_nm".
        ",dm.dept_nm".
        ",cr.room_nm".
        ",login.emp_login_id AS login_id".
        ",jobmst.job_nm".
        ",stmst.st_nm".
        " from empmst em".
        " inner join authmst on (em.emp_id = authmst.emp_id and authmst.emp_manabu_flg = true )".
        " left outer join classmst cm on (em.emp_class = cm.class_id AND cm.class_del_flg = false)".
        " left outer join atrbmst am on (em.emp_attribute = am.atrb_id AND am.atrb_del_flg = false)".
        " left outer join deptmst dm on (em.emp_dept = dm.dept_id AND dm.dept_del_flg = false)".
        " left outer join classroom cr on (em.emp_room = cr.room_id AND cr.room_del_flg = false)".
        " left outer join jobmst on (em.emp_job = jobmst.job_id AND jobmst.job_del_flg = false)".
        " left outer join stmst on (em.emp_st = stmst.st_id AND stmst.st_del_flg = false)".
        " left outer join login on (em.emp_id = login.emp_id)".
        " left outer join concurrent ccr on (em.emp_id = ccr.emp_id)".
        " where em.emp_id is not null ";

        //--------------------
        // 画面指定された検索条件
        //--------------------
        $where = "";
        if ($joken["class"])  $where .= " and (em.emp_class = ".dbPIntC($joken["class"])." or ccr.emp_class = ".dbPIntC($joken["class"]).")";
        if ($joken["attr"])   $where .= " and (em.emp_attribute = ".dbPIntC($joken["attr"])." or ccr.emp_attribute = ".dbPIntC($joken["attr"]).")";
        if ($joken["dept"])   $where .= " and (em.emp_dept = ".dbPIntC($joken["dept"])." or ccr.emp_dept = ".dbPIntC($joken["dept"]).")";
        if ($joken["room"])   $where .= " and (em.emp_room = ".dbPIntC($joken["room"])." or ccr.emp_room = ".dbPIntC($joken["room"]).")";
        if ($joken["id"])     $where .= " and em.emp_personal_id like ".dbPStr("%".str_replace("%","'",$joken["id"])."%");
        if ($joken["emp_id"]) $where .= " and em.emp_id = ".dbPStr($joken["emp_id"]); // ★★★ 現状、emp_idが渡ってくることは無い(20140821)
        if ($joken["job"])    $where .= " and em.emp_job = ".dbPIntC($joken["job"]);
        if ($joken["status"]) $where .= " and em.emp_st = ".dbPIntC($joken["status"]);
        if ($joken["name"]) {
            $names = explode(" ", str_replace("　", " ", $joken["name"]));
            $tmp = array();
            foreach ($names as $nm) {
                if (!strlen($nm)) continue;
                 $tmp[]= "(em.emp_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or em.emp_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or  em.emp_kn_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")."  or em.emp_kn_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." )";
            }
            if (count($tmp)) $where .= " and ".implode(" and ", $tmp);
        }


        $where .= " and (";
        $where .= "   (";
       
        $where .= "     (em.emp_retire is null or em.emp_retire = '' or em.emp_retire > '".date("Ymd")."') and";
       
        $where .= "      authmst.emp_del_flg = false";
        $where .= "   )";
        if (@$joken["emp_expire_type"]=="include_emp_retire") {
            $where .= " or (em.emp_retire is not null and em.emp_retire <> '' and em.emp_retire < '".date("Ymd")."')";
        }
        if (@$joken["emp_expire_type"]=="include_emp_del_flg")  $where .= " or authmst.emp_del_flg = true";
        $where .= " )";


        //--------------------
        // 教育担当者権限なら制限。管理者の場合は「教育責任者」「教育担当者」を呼ぶ「職員選択画面」では制限をかけない。
        //--------------------
        if (GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG && !$no_auth_strict) {
            global $study_tantou_branches;
            $sql .= " and (1 <> 1";
            foreach ($study_tantou_branches as $r) {
                if ($r["class_id"]) $sql .= " or (";
                if ($r["class_id"]) $sql .= "     cm.class_id=".dbPIntC($r["class_id"]);
                if ($r["atrb_id"])  $sql .= "     and am.atrb_id=".dbPIntC($r["atrb_id"]);
                if ($r["dept_id"])  $sql .= "     and dm.dept_id=".dbPIntC($r["dept_id"]);
                if ($r["room_id"])  $sql .= "     and cr.room_id=".dbPIntC($r["room_id"]);
                if ($r["class_id"]) $sql .= " )";
            }
            $sql .= ")";
        }

        $sql .= $where." order by am.atrb_nm, dm.dept_nm, cr.room_nm, em.emp_kn_lt_nm, em.emp_kn_ft_nm";
        if (((int)@$joken["per_page_size"])>0) {
            if ($joken["page_num"]<1) $joken["page_num"] = 1;
            $sql .= " limit ".((int)$joken["per_page_size"]) . " offset ".(($joken["page_num"]-1)*$joken["per_page_size"]);
        }
        return dbFind($sql);
    }
    
    
    
    
    //**********************************************************************************************
    // 受講者一覧取得
    //**********************************************************************************************
    function get_student_list($contents_id, $joken, $page_num, $per_page_size, $is_no_pager_count_only) {

        $where = "";
        if (@$joken["emp_expire_type"]=="is_emp_retire") {
            $where .= " and em.emp_retire <> '' and em.emp_retire is not null and em.emp_retire <= '".date("Ymd")."'";
        }
        if (@$joken["emp_expire_type"]=="is_emp_del_flg")  $where .= " and aum.emp_del_flg = true";
        if (@$joken["emp_expire_type"]=="is_not_emp_manabu_flg")  $where .= " and aum.emp_manabu_flg = false";
        
        if (strlen(@$joken["emp_class"]))  $where .= " and em.emp_class = ".dbPIntC($joken["emp_class"]);
        if (strlen(@$joken["emp_attr"] ))  $where .= " and em.emp_attribute = ".dbPIntC($joken["emp_attr"]);
        if (strlen(@$joken["emp_dept"] ))  $where .= " and em.emp_dept = ".dbPIntC($joken["emp_dept"]);
        if (strlen(@$joken["emp_room"] ))  $where .= " and em.emp_room = ".dbPIntC($joken["emp_room"]);
        if (strlen(@$joken["emp_personal_id"])) $where .= " and em.emp_personal_id like ".dbPStr("%".str_replace("%","'",$joken["emp_personal_id"])."%");
        if (strlen(@$joken["emp_job"]))    $where .= " and em.emp_job = ".dbPIntC($joken["emp_job"]);
        if (strlen(@$joken["emp_status"])) $where .= " and em.emp_st = ".dbPIntC($joken["emp_status"]);
        if (@$joken["total_status"]==="0") $where .= " and (ts.total_status is null or ts.total_status <> 1)";
        if (@$joken["total_status"]==="1") $where .= " and ts.total_status = 1";
        if (@$joken["assembly_actual_status"]=="1") $where .= " and ts.assembly_actual_status = '1'";
        if (@$joken["assembly_actual_status"]=="2") $where .= " and ts.assembly_actual_status = '2'";
        if (@$joken["assembly_actual_status"]=="-1") $where .= " and (ts.assembly_actual_status = 0 or ts.assembly_actual_status is null)";
        if (@$joken["assembly_actual_status"]=="-3") $where .= " and (ts.assembly_actual_status <> '1' or ts.assembly_actual_status is null)";
        if (substr(@$joken["assembly_actual_status"],0,2)=="1_") {
            $where .= " and ts.assembly_actual_date = ".dbPStr(substr($joken["assembly_actual_status"],2));
        }
        if (strlen(@$joken["emp_name"])) {
            $names = explode(" ", str_replace("　", " ", $joken["emp_name"]));
            $tmp = array();
            foreach ($names as $nm) {
                if (!strlen($nm)) continue;
                //$tmp[]= "(em.emp_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or em.emp_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%").")";
                 $tmp[]= "(em.emp_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or em.emp_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or  em.emp_kn_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")."  or em.emp_kn_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." )";    
            }
            if (count($tmp)) $where .= " and ".implode(" and ", $tmp);
        }


        if (strlen(@$joken["no"])) { // 集合研修申込 -1:未登録、-2:欠席 -3:出席すべて 1以上の整数は日付の番号
            if ($joken["no"]=="-1") $where .= " and (ts.assembly_status is null or ts.assembly_status = 0)";
            else if ($joken["no"]=="-2") $where .= " and ts.assembly_status = 2";
            else if ($joken["no"]=="-3") $where .= " and ts.assembly_status = 1";
            else $where .= " and (saa.no = ".dbPIntC($joken["no"]) ." and ts.assembly_status = 1)";
        }

        if (@$joken["test_status"]=="1") $where .= " and ts.test_status = 1";
        if (@$joken["test_status"]=="2") $where .= " and ts.test_status = 2";
        if (@$joken["test_status"]=="-1") $where .= " and (ts.test_status is null or ts.test_status = 0)";
        if (@$joken["test_status"]=="-2") $where .= " and (ts.test_status is null or not ts.test_status = 2)";

        if (@$joken["questionnaire_status"]==="1") $where .= " and ts.questionnaire_status = 1";
        if (@$joken["questionnaire_status"]==="0") $where .= " and (ts.questionnaire_status is null or ts.questionnaire_status <> 1)";

        if ($is_no_pager_count_only) {
            $count_sql =
                " select count(*) from (".
                " select distinct el.study_emp_id from study_emp_list el".
                " inner join study_contents co on (co.id = el.id)".
                " inner join empmst em on (el.study_emp_id = em.emp_id)".
                " inner join authmst aum on (aum.emp_id = em.emp_id)".
                " left outer join study_test_status ts on (ts.test_id = co.id and ts.edition = 1 and ts.emp_id = el.study_emp_id)".
                " left outer join study_test st on (st.id = el.id and st.delete_flg = false and st.edition = 1)".
                " left outer join study_assembly_application saa on (saa.id = el.id and saa.emp_id = el.study_emp_id)".
                " where el.id = ".dbPIntC((int)$contents_id).$where.
                " ) d";
            return (int)dbGetOne($count_sql); // ページングなしの全件数
        }

        $sql =
            " select distinct".
            " el.id as id".
            ",el.study_emp_id as emp_id".
            ",em.emp_ft_nm as first_name".
            ",em.emp_lt_nm as last_name".
            ",em.emp_kn_ft_nm ".
            ",em.emp_kn_lt_nm".
            ",cm.class_nm".
            ",cm.class_id".
            ",am.atrb_nm".
            ",am.atrb_id".
            ",dm.dept_nm".
            ",dm.dept_id".
            ",cr.room_nm".
            ",cr.room_id".
            ",jm.job_nm".
            ",jm.job_id".
            ",em.emp_personal_id".
            ",ts.questionnaire_status".
            ",ts.test_status".
            ",ts.assembly_status".
            ",ts.assembly_actual_status".
            ",ts.assembly_actual_date".
            ",ts.total_status".
            ",ts.assembly_actual_status_late".
            ",ts.assembly_status_date".
            ",saa.no".
            ",aum.emp_manabu_flg".
            ",sa.date as assembly_mosikomi_date".
            ",sa.start_time as assembly_mosikomi_start_time".
            ",sa.end_time as assembly_mosikomi_end_time".
            ",sa.place as assembly_mosikomi_place".
            " from study_emp_list el".
            " inner join study_contents co on (co.id = el.id)".
            " inner join empmst em on (el.study_emp_id = em.emp_id)".
            " inner join authmst aum on (aum.emp_id = em.emp_id )".
            " left outer join classmst cm on (em.emp_class = cm.class_id and cm.class_del_flg = false)".
            " left outer join atrbmst am on (em.emp_attribute = am.atrb_id and am.atrb_del_flg = false)".
            " left outer join deptmst dm on (em.emp_dept = dm.dept_id and dm.dept_del_flg = false)".
            " left outer join classroom cr on (em.emp_room = cr.room_id and cr.room_del_flg = false)".
            " left outer join jobmst jm on (em.emp_job = jm.job_id and jm.job_del_flg = false)".
            " left outer join study_test_status ts on (ts.test_id = co.id and ts.edition = 1 and ts.emp_id = el.study_emp_id)".
            " left outer join study_test st on (st.id = el.id and st.delete_flg = false and st.edition = 1)".
            " left outer join study_assembly_application saa on (saa.id = el.id and saa.emp_id = el.study_emp_id)".
            " left outer join study_assembly sa on (sa.id = saa.id and sa.no = saa.no)".
            " where el.id = ".dbPIntC((int)$contents_id) .$where.
            //" order by em.emp_kn_lt_nm, em.emp_kn_ft_nm asc";
            " order by am.atrb_nm, dm.dept_nm, cr.room_nm, em.emp_kn_lt_nm, em.emp_kn_ft_nm";
        if ($per_page_size>0) {
            if ($page_num<1) $page_num = 1;
            $sql .= " limit ".$per_page_size . " offset ".(($page_num-1)*$per_page_size);
        }
        return dbFind($sql);
    }


    //**********************************************************************************************
    // 受講者番号の一覧を取得
    //**********************************************************************************************
    function get_student_list_for_couse($contents_id) {
        $sql =
            " select ".
            " distinct(el.study_emp_id) as emp_id".
            ",em.emp_personal_id".
            ",em.emp_ft_nm as first_name".
            ",em.emp_lt_nm as last_name".
            ",em.emp_kn_ft_nm ".
            ",em.emp_kn_lt_nm".
            " from study_emp_list el".
            " inner join empmst em on (el.study_emp_id = em.emp_id)".
            " where el.id in (".implode(",", $contents_id).")" .
            " order by em.emp_kn_lt_nm,em.emp_kn_ft_nm";
            
           return dbFind($sql);
    }
    
    function get_student_couse_history($emp_id,$test_ids) {
        $sql =
            " select ".
            " co.title,status.assembly_actual_status,status.assembly_status,status.test_id,status.assembly_status_date ".
            " FROM study_test_status status".
            " left outer join study_contents co on co.id = status.test_id".
            " where status.emp_id = '$emp_id' and status.test_id in (".implode(",", $test_ids).")" .
            " order by co.title asc";
           return dbFind($sql);
    }

    


    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    //
    // 教育責任者・教育担当者
    //
    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■



    //**********************************************************************************************
    // 教育責任者
    //**********************************************************************************************

    //======================================================
    // 教育責任者 削除
    //======================================================
    function delete_edu_admins($emp_id) {
        dbExec("delete from study_edu_admins where emp_id = ".dbPStr($emp_id));
    }
    //======================================================
    // 教育責任者 追加
    //======================================================
    function add_edu_admins($emp_id) {
        $id = dbGetOne("select emp_id from study_edu_admins where emp_id = ".dbPStr($emp_id));
        if (!$id) dbExec("insert into study_edu_admins (emp_id) values (".dbPStr($emp_id).")");
    }
    //======================================================
    // 教育責任者 一覧取得
    //======================================================
    function get_emp_admins_list() {
        $sql =
        " select ed.emp_id, em.emp_ft_nm, em.emp_lt_nm, em.emp_kn_ft_nm,em.emp_kn_lt_nm,em.emp_personal_id from study_edu_admins ed".
        " left outer join empmst em on (em.emp_id = ed.emp_id)".
        " order by em.emp_kn_lt_nm, em.emp_kn_ft_nm";
        return dbFind($sql);
    }


    //**********************************************************************************************
    // 教育担当者
    //**********************************************************************************************

    //======================================================
    // 教育担当者 追加
    //======================================================
    function add_edu_tantou_folders_branches($emp_ids) {
        foreach ($emp_ids as $emp_id) {
            if (!$emp_id) continue;
            // ◆ 講座権限 追加済なら無視 講座IDゼロで１件追加
            $id = dbGetOne("select emp_id from study_edu_tantou_folders where emp_id = ".dbPStr($emp_id));
            if (!$id) dbExec("insert into study_edu_tantou_folders (emp_id, folder_id) values (".dbPStr($emp_id).", 0)");
            // ◆ 所属権限 追加済なら無視 部/課/科IDをゼロで１件追加
            $id = dbGetOne("select emp_id from study_edu_tantou_branches where emp_id = ".dbPStr($emp_id));
            if (!$id) dbExec("insert into study_edu_tantou_branches (emp_id, class_id, atrb_id, dept_id, room_id) values (".dbPStr($emp_id).",0,0,0,0)");
        }
    }
    //======================================================
    // 教育担当者 削除 チェックボックスで複数指定される
    //======================================================
    function delete_edu_tantou_folders_branches($emp_ids) {
        foreach ($emp_ids as $emp_id) {
            if (!$emp_id) continue;
            // ◆ 講座権限削除
            dbExec("delete from study_edu_tantou_branches where emp_id = ".dbPStr($emp_id));
            // ◆ 所属権限削除
            dbExec("delete from study_edu_tantou_folders where emp_id = ".dbPStr($emp_id));
        }
    }
    //======================================================
    // 教育担当者 講座 追加
    // ◆ 追加する講座より上位講座が存在する場合は追加しない
    // ◆ 追加する講座より下位講座が存在する場合は削除する
    //======================================================
    function add_edu_tantou_folder($emp_id, $folder_id) {
        studyCommonModel::del_edu_tantou_folder($emp_id, $folder_id);
        dbExec("insert into study_edu_tantou_folders (emp_id, folder_id) values (".dbPStr($emp_id).", ".dbPIntC($folder_id).")");

        $folder_list = dbFind("select id, name, parent_id from study_folder where delete_flg = false"); // 表示講座名取得用の一覧
        $ary = dbFind("select folder_id from study_edu_tantou_folders where emp_id = ".dbPStr($emp_id));
        $tantou_folders = array();
        foreach ($ary as $row) {
            if ($row["folder_id"]) $tantou_folders[]= $row["folder_id"]; // この職員の全担当講座ID
        }

        //-------------------------
        // 全担当講座をループ
        //-------------------------
        foreach ($ary as $row) {
            $fid = $row["folder_id"]; // とある講座
            $nodes = StudyCommonModel::get_folder_parents("id", $fid, $folder_list); // とある講座の階層
            for ($idx=0; $idx<count($nodes)-1; $idx++) { // 階層の親を順に確認
                if ($nodes[$idx] && in_array($nodes[$idx], $tantou_folders)) { // 親講座が登録済である場合は
                    studyCommonModel::del_edu_tantou_folder($emp_id, $fid); // とある講座を削除する
                    break;
                }
            }
        }
    }
    //======================================================
    // 教育担当者 講座 削除
    //======================================================
    function del_edu_tantou_folder($emp_id, $folder_id) {
        dbExec("delete from study_edu_tantou_folders where emp_id = ".dbPStr($emp_id)." and folder_id = ".dbPStr($folder_id));
    }
    //======================================================
    // 教育担当者 所属 追加
    //======================================================
    function add_edu_tantou_branch($emp_id, $class_id, $atrb_id, $dept_id, $room_id) {
        StudyCommonModel::delete_edu_tantou_branch($emp_id, $class_id, $atrb_id, $dept_id, $room_id);
        $sql =
        "insert into study_edu_tantou_branches (emp_id, class_id, atrb_id, dept_id, room_id) values (".
            dbPStr($emp_id).", ".dbPIntC((int)$class_id).", ".dbPIntC((int)$atrb_id).", ".dbPIntC((int)$dept_id).", ".dbPIntC((int)$room_id).
        ")";
        dbExec($sql);
    }
    //======================================================
    // 教育担当者 所属 削除
    //======================================================
    function delete_edu_tantou_branch($emp_id, $class_id, $atrb_id, $dept_id, $room_id) {
        $sql =
        " delete from study_edu_tantou_branches where emp_id = ".dbPStr($emp_id).
        " and class_id = ".dbPIntC((int)$class_id)." and atrb_id = ".dbPIntC((int)$atrb_id)." and dept_id = ".dbPIntC((int)$dept_id)." and room_id = ".dbPIntC((int)$room_id);
        dbExec($sql);
    }
    //======================================================
    // 教育担当者 一覧取得
    //======================================================
    function get_emp_tantou_folders_branches_list($req, $parent_ids, &$folder_list) {
        $emp_personal_id = $req["emp_personal_id"];
        $class_id = $req["class_id"];
        $atrb_id = $req["atrb_id"];
        $dept_id = $req["dept_id"];
        $room_id = $req["room_id"];

        //--------------------
        // 講座権限を全取得しておく
        // emp_id別に配列化
        //--------------------
        $f_rows = dbFind("select * from study_edu_tantou_folders");
        $af_rows = array();
        $af2_rows = array();
        foreach ($f_rows as $row) {
            if (!$row["folder_id"]) continue;
            $folder_disp = str_replace("\n", WBR."＞", h(implode("\n",StudyCommonModel::get_folder_parents("name", $row["folder_id"], $folder_list))));
            $af_rows[$row["emp_id"]][] = array(
                "folder_disp" => $folder_disp,
                "folder_id"   => $row["folder_id"],
                "is_include_joken" => $is_include_joken
            );
            if (is_array($parent_ids) && $parent_ids[0]) {
                if (!in_array($row["folder_id"], (array)$parent_ids)) continue;
                $af2_rows[$row["emp_id"]] = 1;
            }
        }
        //--------------------
        // 所属権限を全取得しておく
        // これもemp_id別に配列化
        //--------------------
        $sql =
        " select tb.emp_id, tb.class_id, tb.atrb_id, tb.dept_id, cm.class_nm, am.atrb_nm, dm.dept_nm, cr.room_nm".
        " from study_edu_tantou_branches tb".
        " left outer join classmst cm on (cm.class_id = tb.class_id and cm.class_del_flg = false)".
        " left outer join atrbmst am on (am.atrb_id = tb.atrb_id and am.atrb_del_flg = false)".
        " left outer join deptmst dm on (dm.dept_id = tb.dept_id and dm.dept_del_flg = false)".
        " left outer join classroom cr on (cr.room_id = tb.room_id and cr.room_del_flg = false)".
        " where 1 = 1".
        " " . ($class_id ? " and cm.class_id = ".dbPIntC($class_id) : "").
        " " . ($atrb_id ? " and am.atrb_id = ".dbPIntC($atrb_id) : "").
        " " . ($dept_id ? " and dm.dept_id = ".dbPIntC($dept_id) : "").
        " " . ($room_id ? " and dm.room_id = ".dbPIntC($room_id) : "");
        $b_rows = dbFind($sql);
        $ab_rows = array();
        foreach ($b_rows as $row) {
            if (!$row["class_id"]) continue;
            $ary = array($row["class_nm"]);
            if ($row["atrb_nm"]) $ary[]=$row["atrb_nm"];
            if ($row["dept_nm"]) $ary[]=$row["dept_nm"];
            if ($row["room_nm"]) $ary[]=$row["room_nm"];
            $ab_rows[$row["emp_id"]][] = array(
                "branch_disp" => str_replace("\n", WBR."＞", h(implode("\n", $ary))),
                "branch_id"   => $row["class_id"].",".$row["atrb_id"].",".$row["dept_id"].",".$row["room_id"]
            );
        }
        //--------------------
        // 講座か所属、どちらかの権限を持つ教育担当者の一覧を取得
        // 名前昇順
        //--------------------
        $where_emp_name = "";
        if ($req["emp_name"]) {
            $names = explode(" ", str_replace("　", " ", $req["emp_name"]));
            $tmp = array();
            foreach ($names as $nm) {
                if (!strlen($nm)) continue;
                //$tmp[]= "(em.emp_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or em.emp_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%").")";
                $tmp[]= "(em.emp_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or em.emp_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or  em.emp_kn_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")."  or em.emp_kn_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." )";    
            }
            if (count($tmp)) $where_emp_name = " and ".implode(" and ", $tmp);
        }

        $sql =
        " select em.emp_lt_nm || ' ' || em.emp_ft_nm as name, em.emp_personal_id, em.emp_id, em.emp_kn_lt_nm,em.emp_kn_ft_nm".
        " from empmst em".
        " where (em.emp_id in ( select emp_id from study_edu_tantou_branches  )".
        " or em.emp_id in ( select emp_id from study_edu_tantou_folders  ))".
        " " . ($emp_personal_id ? " and em.emp_personal_id = ".dbPStr($emp_personal_id) : "").
        " " . $where_emp_name.
        " order by em.emp_kn_lt_nm, em.emp_kn_ft_nm";
        $raw_rows = dbFind($sql);
        $rows = array();
        foreach ($raw_rows as $idx => $row) {
            $emp_id = $row["emp_id"];
            if (($class_id || $atrb_id || $dept_id || $room_id) && !is_array($ab_rows[$emp_id])) continue;
            if (is_array($parent_ids) && $parent_ids[0] && !$af2_rows[$emp_id]) continue;

            $row["folder_list"] = (is_array($af_rows[$emp_id]) ? $af_rows[$emp_id] : array()); // 講座権限があれば、そのリストを紐つけ
            $row["branches"]    = (is_array($ab_rows[$emp_id]) ? $ab_rows[$emp_id] : array()); // 所属権限があれば、そのリストを紐つけ
            $rows[] = $row;
        }
        return $rows;
    }

    //======================================================
    // 教育担当者一覧取得（指定講座への権限者一覧）
    //======================================================
    function get_emp_tantou_folders_list($folder_id) {
        $folder_list = dbFind("select id, name, parent_id from study_folder where delete_flg = false"); // 表示講座名取得用の一覧
        $folder_parents = StudyCommonModel::get_folder_parents("id", $folder_id, $folder_list);
        if (!count($folder_parents)) $folder_parents[] = -1;
        $sql =
        " select em.emp_lt_nm || ' ' || em.emp_ft_nm as name, em.emp_personal_id, em.emp_id from empmst em".
        " inner join study_edu_tantou_folders tf on (tf.emp_id = em.emp_id and tf.folder_id in (".implode(",", $folder_parents)."))";
        return dbFind($sql);
    }


    //**********************************************************************************************
    // 指定講座の階層を配列で作成して返す
    //**********************************************************************************************
    function get_folder_parents($return_type, $folder_id, $folder_list) {
        $folder_id = (int)$folder_id;
        if ($return_type!="id") $return_type = "name"; // リターンタイプ。id配列を返すか、name配列を返すか
        $id_array = array();
        $nm_array = array();
        for (;;) {
            $append_status = 0;
            if (!$folder_id) break;
            foreach ($folder_list as $row) {
                $append_status = 1;
                if (((int)$row["id"])!=$folder_id) continue;   // folder_idのデータ行でなければスルー
                if (((int)$row["id"])!=$folder_id) break;   // データ行が削除済みなら終了
                $append_status = 2;
                $id_array[]= (int)$row["id"];                  // 対象行発見。idを配列の先頭へ
                $nm_array[]= $row["name"];                     // 続いて、名前も配列の先頭へ
                if (!$folder_id || $folder_id==99999) break 2; // 【終了】自分がトップノードならここで終了
                $folder_id = (int)$row["parent_id"];           // その親IDを取得
                if (!$folder_id) break 2;                      // 【終了】親IDが指定されていない。ここで終了
                if (in_array($folder_id, $id_array)) break 2;  // 【終了】循環無限ループ防止。親を既に取得済の場合もここで終了
                $append_status = 3;
                break; // 最初から
            }
            if ($append_status==1) {
                $id_array[] = -1;
                $nm_array[] = "(不明)";
                break;
            }
            if (!$append_status) break;
        }
        if ($return_type=="id") return array_reverse($id_array);
        return array_reverse($nm_array);
    }


    // 問題をシャッフル順に並べ替え
    function sort_shuffled_order(&$array, $shuffled_order) {
        $before_sort = $array;
        $count = count($array);
        $array = array();
        for ($i = 0; $i < $count; $i++) {
            $array[] = $before_sort[$shuffled_order[$i] - 1];
        }
    }














    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    //
    // 各種サマリング・総合値再計算
    //
    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    //**********************************************************************************************
    // 各種サマリング対応
    //**********************************************************************************************
    // ◆ 受講者数
    function update_student_count($contents_id) {
        $cnt = dbGetOne("select count(*) from study_emp_list where id = ".dbPIntC($contents_id));
        dbExec("update study_contents set student_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }
    // ◆ 集合研修の開催日数
    function update_assembly_count($contents_id) {
        $cnt = (int)dbGetOne("select count(*) from study_assembly where delete_flg = false and id = ".dbPIntC($contents_id));
        dbExec("update study_contents set assembly_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }
    // ◆ アンケート設問数
    function update_questionnaire_count($contents_id) {
        $cnt = dbGetOne("select count(*) from study_questionnaire where id = ".dbPIntC($contents_id)." and edition = 1");
        dbExec("update study_contents set questionnaire_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }
    // ◆ アンケート回答者数
    function update_questionnaire_answer_count($contents_id) {
        $cnt = dbGetOne("select count(*) from study_test_impression_new where no = 1 and test_id = ".dbPIntC($contents_id)." and edition = 1");
        dbExec("update study_contents set questionnaire_answer_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }
    // ◆ テスト問題数
    function update_test_count($contents_id) {
        $cnt = dbGetOne("select count(*) from study_test_item where id = ".dbPIntC($contents_id)." and edition = 1");
        dbExec("update study_contents set test_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }
    // ◆ テスト期間
    function update_contents_test_date($contents_id, $start_date, $end_date) {
        $sql =
        " update study_contents set".
        " test_start_date = ".dbPStr($start_date).
        ",test_end_date = ".dbPStr($end_date).
        " where id = ".dbPIntC($contents_id);
        dbExec($sql);
    }
    // ◆ お知らせ件数
    function update_notif_count($contents_id) {
        $sql =
        " select count(*) from study_notification".
        " where delete_flg = false and contents_id = ".dbPIntC($contents_id)." and edition = 1";
        $cnt = (int)dbGetOne($sql);
        dbExec("update study_contents set notif_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }
    // ◆ テスト結果、テスト結果件数
    function update_test_status($contents_id, $emp_id, $test_status) {
        //--------------------
        // テスト結果(1:受講中(不合格) 2:合格)
        // ※ テスト未受講という指定は機能的に実装されていない。
        //--------------------
        $sql =
        " update study_test_status set test_status = ".dbPIntC($test_status).
        " where test_id = ".dbPIntC($contents_id)." and edition = 1 and emp_id = ".dbPStr($emp_id);
        dbExec($sql);
        //--------------------
        // テスト受講中(不合格者)の件数 サマリング再計算
        //--------------------
        $cnt = (int)dbGetOne("select count(*) from study_test_status where test_id = ".dbPIntC($contents_id)." and test_status = 1 and edition = 1");
        dbExec("update study_contents set test_ng_count = ".$cnt." where id = ".dbPIntC($contents_id));
        //--------------------
        // テスト合格者件数 サマリング再計算
        //--------------------
        $cnt = (int)dbGetOne("select count(*) from study_test_status where test_id = ".dbPIntC($contents_id)." and test_status = 2 and edition = 1");
        dbExec("update study_contents set test_ok_count = ".$cnt." where id = ".dbPIntC($contents_id));
        //--------------------
        // ★★★総合値の再計算
        //--------------------
        StudyCommonModel::update_total_status($contents_id, $emp_id);
    }
    //**********************************************************************************************
    // 総合値の再計算（全受講者を再計算）
    // 受講者の受講状況を再確認し再登録する。
    // 受講者を指定しない場合は、全員の受講状況をひとりひとり再確認し、再登録する。
    // 全員が受講済なら、全体として受講済とする
    //**********************************************************************************************
    function update_total_status($contents_id, $emp_id = 0) {
        //--------------------
        // 実施内容を取得
        //--------------------
        $row = dbGetTopRow("select policy_code from study_contents where id = ".dbPIntC($contents_id));
        $policy_info = get_policy_info($row["policy_code"]);
        $total_info = get_policy_total_info($policy_info);
        $group_order = $total_info["group_order"];

        //--------------------
        // 受講者ステータスを取得
        //--------------------
        $sql =
        " select * from study_test_status".
        " where test_id = ".dbPIntC($contents_id)." and edition = 1".
        " ".($emp_id ? " and emp_id = ".dbPStr($emp_id) : "");
        $rows = dbFind($sql);
        foreach ($rows as $row) {
            //--------------------
            // 各実施内容についての受講済を確認
            //--------------------
            $org_total_status = (int)$row["total_status"];
            $group_ok_count = 0;
            if (count($group_order)) { // グループがある（つまり必須受講がある）場合（ちなみに現状は必ず必須対象は１つ以上存在する）
                foreach ($group_order as $group => $funcs) { // グループごとに確認。グループごとはAND条件。どのグループが未修であってもNG。
                    $is_group_ok = 0; // このグループの受講状態：未修とする
                    foreach ($funcs as $func) { // グループ内はOR条件。ひとつでもOKならよい。
                        $ok_flag = 0; // この必須対象の受講済フラグ：いったん未修
                        if ($func=="assembly"      && $row["assembly_actual_status"] =="1") $ok_flag = 1;
                        if ($func=="questionnaire" && $row["questionnaire_status"]   =="1") $ok_flag = 1;
                        if ($func=="test"          && $row["test_status"]            =="2") $ok_flag = 1;
                        if ($ok_flag==1) $is_group_ok = 1; // この必須対象が受講済なら、グループとして受講済OK。
                    }
                    if ($is_group_ok) $group_ok_count++;
                }
            }
            //--------------------
            // 総合的に、受講済かどうかを確認
            //--------------------
            $total_status = 0; // 対象受講者の総合ステータス：いったん未修
            if ($group_ok_count == count($group_order)) $total_status = 1; // 全差分グループがOKなら受講済に
            //--------------------
            // 総合状態に変更があった場合はステータス更新
            //--------------------
            if ($org_total_status != $total_status) {
                $sql =
                " update study_test_status set total_status = ".$total_status.
                " where test_id = ".dbPIntC($contents_id)." and edition = 1 and emp_id = ".dbPStr($row["emp_id"]);
                dbExec($sql);
            }
        }

        //--------------------
        // 総合受講済の人数と、受講者の人数が等しい場合は
        // 全体として受講済とする
        //--------------------
        $sql =
        " select count(*) from study_test_status".
        " where test_id = ".dbPIntC($contents_id)." and edition = 1 and ( total_status is null or total_status <> '1')";
        $cnt = (int)dbGetOne($sql);
        $contents_total_status = "1";
        if ($cnt) $contents_total_status = "0";
        dbExec("update study_contents set contents_total_status = ".$contents_total_status." where id =".dbPIntC($contents_id));
    }


    function get_auth_matrix($tpl_name_and_mode, $folder_id, $emp_id) {
        $tantou_folder_auth = 0;
        $tantou_branch_auth = 0;
        global $study_tantou_branches;
        global $study_tantou_folders;

        if (GG_STUDY_TANTOU_FLG) {
            // まず、講座(フォルダ)に対し、講座(フォルダ)権限があるか
            if ($folder_id) {
                $folder_list = StudyFolderModel::get_folder_list("", "", 1); // この取得結果は教育担当権限範囲内でのフォルダ一覧、で取得される
                $nodes = StudyCommonModel::get_folder_parents("id", $folder_id, $folder_list); // とある講座の階層
                if ($nodes[0]) {
                    foreach ($folder_list as $idx => $row) {
                        if ($row["id"]!=$nodes[0]) continue;
                        if (!$row["parent_id"]) $tantou_folder_auth = 1;
                        break;
                    }
                }
            }

            // ★★★ 現状、emp_idが渡ってくることは無い(20140821)
            if ($emp_id) {
                $user_list = StudyCommonModel::get_user_list(array("emp_id"=>$emp_id)); // この取得結果は所属権限範囲内でのフォルダ一覧、となる。
                
                foreach ($user_list as $key => $value){
                    $key_class[$key] = $value['class_nm'];
                    $key_atrb[$key] = $value['atrb_nm'];
                    $key_dept[$key] = $value['dept_nm'];
                    $key_room[$key] = $value['room_nm'];
                    $key_last[$key] = $value['last_name'];
                    $key_first[$key] = $value['first_name'];
                }
                //array_multisort ($key_class , SORT_ASC ,$key_atrb , SORT_ASC , $key_dept , SORT_ASC , $key_room , SORT_ASC , $key_last , SORT_ASC , $key_first , SORT_ASC,$user_list );

                
                
                if (count($user_list)) $tantou_branch_auth = 1;
            }
        }

        $matrix = array();


        //============================================================
        // テスト結果
        //
        // folder_id ：必須
        // emp_id    ：必須
        //============================================================
        // 全員：編集不可
        //
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：本人          ：閲覧可
        // 1   ：責任者        ：閲覧可
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可
        // 1   ：担当者(所属)  ：閲覧可
        //     ：シス管        ：―
        //     ：一般          ：―
        /*
        if ($tpl_name_and_mode == "study_test_user_result.tpl@all") {
            $disp_auth = "";
            if (!$folder_id || !$emp_id) $disp_auth = "";
            else if ($emp_id == GG_LOGIN_EMP_ID) $disp_auth = "本人";
            else if (GG_STUDY_ADMIN_FLG)         $disp_auth = "責任者";
            else if ($tantou_folder_auth)        $disp_auth = "フォルダ担当";
            else if ($tantou_branch_auth)        $disp_auth = "所属担当";
            return array("disp_auth"=>$disp_auth, "edit_auth"=>"");
        }
        */


        //============================================================
        // 研修・講義詳細
        //
        // folder_id ：任意
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_contents.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // 職員検索
        //
        // folder_id ：なし
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_emp_search.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if (GG_STUDY_TANTOU_FLG && count($study_tantou_branches))     { $disp_auth = "所属担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // 受講者一覧
        //
        // folder_id ：必須
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_student_list.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // 集合研修申込一覧
        // 開催日入力
        //
        // folder_id ：必須
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_assembly_list.tpl@all" || $tpl_name_and_mode == "study_assembly_kaisaibi.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // アンケート一覧
        //
        // folder_id ：必須
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_questionnaire_list.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // テスト編集
        //
        // folder_id ：必須
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_test_edit.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // お知らせ一覧（編集用）
        //
        // folder_id ：必須
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_notif_admin_list.tpl@all" || $tpl_name_and_mode == "study_notif_admin_edit.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // 研修・講座管理
        //
        // folder_id ：必須
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、参照されていない
        if ($tpl_name_and_mode == "study_main.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if (GG_STUDY_TANTOU_FLG && count($study_tantou_folders))  { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; } // 少し注意
            else if (GG_STUDY_ADMIN_FLG)                          { $disp_auth = "責任者"; }
            else if (GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG && !count($study_tantou_folders)) { }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }
        if ($tpl_name_and_mode == "study_main_right_part.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }


        //============================================================
        // フォルダ
        //
        // folder_id ：任意
        // emp_id    ：なし
        //============================================================
        // 優先：権限          ：仕様
        //-------------------------------
        // 1   ：担当者(ﾌｫﾙﾀﾞ) ：閲覧可 編集可
        // 2   ：責任者        ：閲覧可 (編集不可)
        //     ：シス管        ：閲覧可 編集可
        //     ：担当者(所属)  ：―
        //     ：一般          ：―
        //     ：本人          ：―
        // ★実質、閲覧可否は参照されていない
        if ($tpl_name_and_mode == "study_folder.tpl@ev_create_folder") {
            $disp_auth = "";
            $edit_auth = "";
            if (count($study_tantou_folders)) { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG)  { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }
        if ($tpl_name_and_mode == "study_folder.tpl@all") {
            $disp_auth = "";
            $edit_auth = "";
            if ($tantou_folder_auth)     { $disp_auth = "フォルダ担当"; $edit_auth = "フォルダ担当"; }
            else if (GG_STUDY_ADMIN_FLG) { $disp_auth = "責任者"; }
            return array("disp_auth"=>$disp_auth, "edit_auth"=>$edit_auth);
        }

        echo "get_auth_matrix 呼び出し不正 ".$tpl_name_and_mode;
        die;
    }
    
    
    function get_staff_career($emp_id){
        $sql =
        "select * from study_staff_career".
        " where emp_id = ".dbPStr($emp_id);
        return dbFind($sql);
    }
    
        
    function get_all_staff_career($joken){

        $sql =
        "select distinct em.emp_id,".
        " em.emp_personal_id,cm.class_nm AS class_nm,am.atrb_nm,dm.dept_nm,cr.room_nm,em.emp_ft_nm as first_name,em.emp_lt_nm as last_name,jobmst.job_nm ,sc.exp_start,sc.exp_minus,sc.exp_coment,sc.work_start, sc.work_minus,sc.work_coment from empmst em ".
        " inner join authmst on (authmst.emp_id = em.emp_id and authmst.emp_manabu_flg  = true)". 
        " left outer join study_staff_career sc on (sc.emp_id = em.emp_id)".
        " left outer join jobmst on (em.emp_job = jobmst.job_id AND jobmst.job_del_flg = false)".
        " left outer join concurrent ccr on (em.emp_id = ccr.emp_id)".
        " left outer join classmst cm on (em.emp_class = cm.class_id AND cm.class_del_flg = false)".
        " left outer join atrbmst am on (em.emp_attribute = am.atrb_id AND am.atrb_del_flg = false)".
        " left outer join deptmst dm on (em.emp_dept = dm.dept_id AND dm.dept_del_flg = false)".
        " left outer join classroom cr on (em.emp_room = cr.room_id AND cr.room_del_flg = false)".
        " left outer join stmst on (em.emp_st = stmst.st_id AND stmst.st_del_flg = false)".  
        " where em.emp_id is not null";
        //--------------------
        // 画面指定された検索条件
        //--------------------
        $where = "";
        
        $where .= " and (";
        $where .= "     (em.emp_retire is null or em.emp_retire = '' or em.emp_retire > '".date("Ymd")."')";
        $where .= "     and authmst.emp_del_flg = false";
        $where .= "   )";      
        
        if ($joken["class"])  $where .= " and (em.emp_class = ".dbPIntC($joken["class"])." or ccr.emp_class = ".dbPIntC($joken["class"]).")";
        if ($joken["attr"])   $where .= " and (em.emp_attribute = ".dbPIntC($joken["attr"])." or ccr.emp_attribute = ".dbPIntC($joken["attr"]).")";
        if ($joken["dept"])   $where .= " and (em.emp_dept = ".dbPIntC($joken["dept"])." or ccr.emp_dept = ".dbPIntC($joken["dept"]).")";
        if ($joken["room"])   $where .= " and (em.emp_room = ".dbPIntC($joken["room"])." or ccr.emp_room = ".dbPIntC($joken["room"]).")";
        if ($joken["id"])     $where .= " and em.emp_personal_id like ".dbPStr("%".str_replace("%","'",$joken["id"])."%");
        if ($joken["emp_id"]) $where .= " and em.emp_id = ".dbPStr($joken["emp_id"]); // ★★★ 現状、emp_idが渡ってくることは無い(20140821)
        if ($joken["job"])    $where .= " and em.emp_job = ".dbPIntC($joken["job"]);
        if ($joken["status"]) $where .= " and em.emp_st = ".dbPIntC($joken["status"]);
        if ($joken["name"]) {
            $names = explode(" ", str_replace("　", " ", $joken["name"]));
            $tmp = array();
            foreach ($names as $nm) {
                if (!strlen($nm)) continue;
                 $tmp[]= "(em.emp_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or em.emp_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." or  em.emp_kn_ft_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")."  or em.emp_kn_lt_nm like ".dbPStr("%".str_replace("%","'",$nm)."%")." )";
            }
            if (count($tmp)) $where .= " and ".implode(" and ", $tmp);
        }
        
        $sql .= $where." order by am.atrb_nm, dm.dept_nm, cr.room_nm, em.emp_lt_nm, em.emp_ft_nm";
        
        if (((int)@$joken["per_page_size"])>0) {
            if ($joken["page_num"]<1) $joken["page_num"] = 1;
                $sql .= " limit ".((int)$joken["per_page_size"]) . " offset ".(($joken["page_num"]-1)*$joken["per_page_size"]);
        }
        
        return dbFind($sql);
    }

    function add_staff_career($readLines) {
           
            
        for($i=1;$i<count($readLines);$i++){ 
            
            $staffdata =  explode(",", $readLines[$i]);
   
            //for($j=0;$j<  count($staffdata);$j++){
                $emp_personal_id = $staffdata[0];
                
                if (!$emp_personal_id) continue;
                $tmp_emp_id = dbFind("select emp_id from empmst where emp_personal_id = ".dbPStr($emp_personal_id));
                $emp_id = $tmp_emp_id[0][emp_id];
                //--------------------------
                // 既に存在する場合は更新
                //--------------------------
                if (!$emp_id) continue;

                
                
                
                if ((int)dbGetOne("select count(emp_id) from study_staff_career where emp_id = ".dbPStr($emp_id))){
                    $sql =
                    " update study_staff_career set ".
                    " exp_start = ".dbPStr($staffdata[3]).
                    " ,exp_minus = ".dbPStr($staffdata[4]).
                    " ,exp_coment = ".dbPStr($staffdata[5]).
                    " ,work_start = ".dbPStr($staffdata[6]). 
                    " ,work_minus = ".dbPStr($staffdata[7]).
                    " ,work_coment = ".dbPStr($staffdata[8]).
                    " where emp_id = ".dbPStr($emp_id);
                }else{
                    
                    $sql =
                    " insert into study_staff_career (".
                    "    emp_id, exp_start,exp_minus,exp_coment,work_start, work_minus,work_coment".
                    " ) values (".
                    "     ".dbPStr($emp_id).",".dbPStr($staffdata[3]).",".dbPStr($staffdata[4]).",".dbPStr($staffdata[5]).",".
                    "    ".dbPStr($staffdata[6]).",".dbPStr($staffdata[7]).",".dbPStr($staffdata[8]).
                    " )";
                }
            dbExec($sql);
            //}
            
        }
    }
    
    function get_all_study_staff($nendo){
        $sql =
        "select". 
        " st.test_id,st.emp_id,st.test_status,st.assembly_status,st.assembly_actual_status,st.questionnaire_status,st.assembly_actual_date, str.test_date ".
        ",cm.class_nm AS class_nm".
        ",am.atrb_nm".
        ",dm.dept_nm".
        ",cr.room_nm".
        ",jobmst.job_nm".
        ",stmst.st_nm".
        ",em.emp_ft_nm as first_name,em.emp_lt_nm as last_name,em.emp_kn_lt_nm,em.emp_kn_ft_nm".
        ",ca.exp_start,ca.exp_minus,ca.work_start,ca.work_minus ".
        ",co.title,co.bunrui_year,am.atrb_nm".
        " from study_test_status st ".
        " inner join empmst em on (st.emp_id = em.emp_id)".
        " inner join authmst on (authmst.emp_id = em.emp_id and authmst.emp_manabu_flg  = true)". 
        " left outer join classmst cm on (em.emp_class = cm.class_id AND cm.class_del_flg = false)".
        " left outer join atrbmst am on (em.emp_attribute = am.atrb_id AND am.atrb_del_flg = false)".
        " left outer join deptmst dm on (em.emp_dept = dm.dept_id AND dm.dept_del_flg = false)".
        " left outer join classroom cr on (em.emp_room = cr.room_id AND cr.room_del_flg = false)".
        " left outer join jobmst on (em.emp_job = jobmst.job_id AND jobmst.job_del_flg = false)".
        " left outer join stmst on (em.emp_st = stmst.st_id AND stmst.st_del_flg = false)".              
        " left outer join study_staff_career ca on (st.emp_id = ca.emp_id)".
        " left outer join study_test_result str on (st.test_id = str.test_id and st.emp_id=str.emp_id and str.is_newest='1')".
        " left outer join study_contents co on (st.test_id = co.id)";
      
        $where = "";
        if ($nendo){
            $where .= " where co.bunrui_year = ".dbPStr($nendo); 
        }

        $sql .= $where. " order by st.test_id,st.emp_id,class_nm,am.atrb_nm,dm.dept_nm,cr.room_nm,em.emp_kn_lt_nm,em.emp_kn_ft_nm";
        
      
        return dbFind($sql);
    }
    
    function get_lecture_tantou($name){
        $sql = "select emp_id from empmst where (emp_lt_nm || emp_ft_nm) ='$name'";
        return dbFind($sql);
    }
    
    //**********************************************************************************************
    // 従業員一覧追加<!--20151118 順天堂医院 by Y.yamamoto--> 
    //**********************************************************************************************
        function get_user_list_auth_csv($no_auth_strict=0) {
        $sql =
        " select distinct em.emp_id".
        ",em.emp_personal_id".
        ",em.emp_ft_nm AS first_name".
        ",em.emp_lt_nm AS last_name".
        ",em.emp_kn_ft_nm".
        ",em.emp_kn_lt_nm".
        ",em.emp_retire".
        ",em.emp_email".
        ",em.emp_idm".
        ",authmst.emp_del_flg".
        ",authmst.emp_manabu_flg".
        ",cm.class_nm AS class_nm".
        ",am.atrb_nm".
        ",dm.dept_nm".
        ",cr.room_nm".
        ",login.emp_login_id AS login_id".
        ",jobmst.job_nm".
        ",stmst.st_nm".
        " from empmst em".
        " inner join authmst on (em.emp_id = authmst.emp_id and authmst.emp_manabu_flg = true )".
        " left outer join classmst cm on (em.emp_class = cm.class_id AND cm.class_del_flg = false)".
        " left outer join atrbmst am on (em.emp_attribute = am.atrb_id AND am.atrb_del_flg = false)".
        " left outer join deptmst dm on (em.emp_dept = dm.dept_id AND dm.dept_del_flg = false)".
        " left outer join classroom cr on (em.emp_room = cr.room_id AND cr.room_del_flg = false)".
        " left outer join jobmst on (em.emp_job = jobmst.job_id AND jobmst.job_del_flg = false)".
        " left outer join stmst on (em.emp_st = stmst.st_id AND stmst.st_del_flg = false)".
        " left outer join login on (em.emp_id = login.emp_id)".
        " left outer join concurrent ccr on (em.emp_id = ccr.emp_id)".
        " where em.emp_id is not null ";

        $where = "";
     
        //--------------------
        // 教育担当者権限なら制限。管理者の場合は「教育責任者」「教育担当者」を呼ぶ「職員選択画面」では制限をかけない。
        //--------------------
        if (GG_STUDY_TANTOU_FLG && !GG_STUDY_ADMIN_FLG && !$no_auth_strict) {
            global $study_tantou_branches;
            $sql .= " and (1 <> 1";
            foreach ($study_tantou_branches as $r) {
                if ($r["class_id"]) $sql .= " or (";
                if ($r["class_id"]) $sql .= "     cm.class_id=".dbPIntC($r["class_id"]);
                if ($r["atrb_id"])  $sql .= "     and am.atrb_id=".dbPIntC($r["atrb_id"]);
                if ($r["dept_id"])  $sql .= "     and dm.dept_id=".dbPIntC($r["dept_id"]);
                if ($r["room_id"])  $sql .= "     and cr.room_id=".dbPIntC($r["room_id"]);
                if ($r["class_id"]) $sql .= " )";
            }
            $sql .= ")";
        }

        $sql .= $where." order by am.atrb_nm, dm.dept_nm, cr.room_nm, em.emp_kn_lt_nm, em.emp_kn_ft_nm";

        return dbFind($sql);
    }
    
    
    
    
}

