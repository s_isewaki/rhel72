<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// お知らせ関連のDBアクセス・ユーティリティ
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
class StudyNotificationModel {

    //**********************************************************************************************
    // お知らせ１件取得（管理側編集時、ユーザ側プレビュー時）
    //**********************************************************************************************
    function get_notif_row($id){
        $sql =
        " select sn.id, sn.contents_id, sn.title, sn.message, sn.target, sn.start_date, sn.end_date".
        ",sc.folder_id, sc.title as contents_title".
        ",case when nc.notif_emp_id is null then '' else '1' end as is_notif_confirmed".
        " from study_notification sn".
        " inner join study_contents sc on ( sc.id = sn.contents_id )".
        " left outer join study_notif_confirm nc on ( nc.notif_emp_id = ".dbPStr(GG_LOGIN_EMP_ID)." and  nc.notif_id = sn.id )".
        " where sn.id = ".dbPIntC($id);
        return dbFind($sql);
    }

    //**********************************************************************************************
    // 【管理側】お知らせ一覧取得
    //**********************************************************************************************
    function get_admin_notif_list($contents_id) {
        $sql =
        " select".
        " sn.id, sn.contents_id, sn.title, sn.message, sn.target, sn.update_datetime, sn.start_date, sn.end_date".
        ",em.emp_ft_nm, em.emp_lt_nm".
        " from study_notification sn".
        " left outer join empmst em on (sn.update_user_id = em.emp_id)".
        " where sn.delete_flg = false and sn.contents_id = ".dbPIntC($contents_id).
        " order by sn.end_date, sn.id";
        return dbFind($sql);
    }

    //**********************************************************************************************
    // 【ユーザ側】お知らせ一覧取得
    //**********************************************************************************************
    function get_user_notif_list($is_show_archive) {
        $sql =
        " select".
        " sn.id, sn.contents_id, sn.title, sn.message, sn.update_datetime, sn.start_date, sn.end_date".
        ",sc.folder_id, sc.title as contents_name".
        ",em.emp_lt_nm, em.emp_ft_nm".
        ",case when nc.notif_emp_id is null then '' else '1' end as is_notif_confirmed".
        ",case when '".date("YmdHi")."' <= sn.end_date then '1' else '' end as is_now_publishing".
        " from study_notification sn".
        " inner join study_emp_list el on ( el.study_emp_id = ".dbPStr(GG_LOGIN_EMP_ID)." and el.id = sn.contents_id )".
        " inner join study_contents sc on ( sc.id = sn.contents_id and sc.delete_flg = false )".
        " left outer join study_notif_confirm nc on ( nc.notif_emp_id = ".dbPStr(GG_LOGIN_EMP_ID)." and  nc.notif_id = sn.id )".
        " left outer join empmst em on ( em.emp_id = sn.update_user_id )".
        " left outer join study_test st on ( st.id = sn.contents_id and st.edition = 1)".
        " left outer join study_test_status ts on ( ts.test_id = sn.contents_id and ts.edition = 1 and ts.emp_id = ".dbPStr(GG_LOGIN_EMP_ID).")".
        " where sn.delete_flg = false".
        " and sn.start_date <> '' and sn.start_date <= '".date("YmdHi")."'".
        " ". (!$is_show_archive ? " and nc.notif_emp_id is null and sn.end_date >= '".date("YmdHi")."'" : "").
        " and ( nc.notif_emp_id is not null".
        "     or (sn.target = '0' and (ts.total_status <> '1' or ts.total_status is null))". // 未受講者
        "     or (sn.target = '1')". // 受講者全員
        "     or (sn.target = '2' and ts.assembly_actual_status = 1)". // 集合研修出席者
        "     or (sn.target = '3' and (ts.assembly_actual_status <> 1 or ts.assembly_actual_status is null))". // 集合研修欠席者
        "     or (sn.target = '4' and (ts.test_status = 1 or ts.test_status = 2))". // オンライン受講者 (テスト中またはテスト合格)
        " )".
        " order by sn.update_datetime desc, sn.end_date desc";
        return dbFind($sql);
    }

    //**********************************************************************************************
    // 【管理側】お知らせ新規作成
    //**********************************************************************************************
    function create_notif($req) {
        $start_date = ($_REQUEST["start_date"]) ? $_REQUEST["start_date"] : ""; // 確認不要かも
        $end_date = ($_REQUEST["end_date"]) ? $_REQUEST["end_date"] : ""; // 確認不要かも

        $id = dbGetNextSequence("study_notification_id");

        $sql =
        " insert into study_notification (".
        "     id, contents_id, edition, title, message, create_user_id, update_user_id, create_datetime, update_datetime".
        "    ,delete_flg, target, start_date, end_date".
        ") values (".
        " ".dbPIntC($id).
        ",".dbPIntC($req["contents_id"]).
        ",1".
        ",".dbPStr($req["info_title"]).
        ",".dbPStr($req["info_message"]).
        ",".dbPStr(GG_LOGIN_EMP_ID).
        ",".dbPStr(GG_LOGIN_EMP_ID).
        ",".dbPTime(date('Y/m/d H:i:s', time())).
        ",".dbPTime(date('Y/m/d H:i:s', time())).
        ",".dbPBool(false).
        ",".dbPIntC($req["target_select"]).
        ",".dbPStr($start_date).
        ",".dbPStr($end_date).
        ")";
        dbExec($sql);
    }

    //**********************************************************************************************
    // 【管理側】お知らせ更新
    //**********************************************************************************************
    function update_notif($req) {
        $start_date = ($req["start_date"]) ? $req["start_date"] : ""; // 確認不要かも
        $end_date = ($req["end_date"]) ? $req["end_date"] : ""; // 確認不要かも
        $sql =
        " update study_notification set".
        " title = ".dbPStr($req["info_title"]).
        ",message = ".dbPStr($req["info_message"]).
        ",target = ".dbPIntC($req["target_select"]).
        ",start_date = ".dbPStr($start_date).
        ",end_date = ".dbPStr($end_date).
        " where id = ".dbPIntC($req["notif_id"]);
        dbExec($sql);
        if ($req["is_notif_again"]) {
            dbExec("delete from study_notif_confirm where notif_id = ".dbPIntC($req["notif_id"]));
        }
    }

    //**********************************************************************************************
    // 【管理側】お知らせ削除、既読状態もあわせて削除
    //**********************************************************************************************
    function delete_notif($id) {
        dbExec("delete from study_notification where id = ".dbPIntC($id));
        dbExec("delete from study_notif_confirm where notif_id = ".dbPIntC($id));
    }


    //**********************************************************************************************
    // 【ユーザ側】お知らせの既読/未読切替（$mark_backward＝未指定なら既読）
    //**********************************************************************************************
    function mark_to_confirm($notif_id, $mark_backward) {
        dbExec("delete from study_notif_confirm where notif_id = ".dbPIntC($notif_id)." and notif_emp_id = ".dbPStr(GG_LOGIN_EMP_ID));
        if (!$mark_backward) {
            dbExec("insert into study_notif_confirm ( notif_id, notif_emp_id ) values (".dbPIntC($notif_id).",".dbPStr(GG_LOGIN_EMP_ID).")");
        }
    }
}


