<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// 集合研修関連のDBアクセス・ユーティリティ
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
require_once 'model/StudyCommonModel.php';

class StudyAssemblyModel {

    //**********************************************************************************************
    // 集合研修 取得系
    //**********************************************************************************************
    //--------------------------------------------------------------------------
    // 開催日詳細一覧取得（開催日昇順、しかし開催日は一意になっていない。時刻と場所が必要）
    //--------------------------------------------------------------------------
    function get_assembly_list($contents_id) {
        return dbFind("select * from study_assembly where id = ".dbPIntC($contents_id)." and delete_flg = false order by date");
    }
    //--------------------------------------------------------------------------
    // 開催日情報取得
    //--------------------------------------------------------------------------
    function get_assembly_date_record($contents_id, $no) {
        return dbGetTopRow("select * from study_assembly where id = ".dbPIntC($contents_id)." and delete_flg = false and no = ".dbPIntC($no));
    }
    //--------------------------------------------------------------------------
    // 【ユーザ側】自分が申込んだ状態を返す：  0(未申込) or 1(出席) or 2(欠席)
    //--------------------------------------------------------------------------
    function get_assembly_status($contents_id) {
        $sql =
        " select assembly_status from study_test_status".
        " where edition = 1 and test_id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr(GG_LOGIN_EMP_ID);
        return (int)dbGetOne($sql);
    }
    //--------------------------------------------------------------------------
    // 【ユーザ側】自分が申込んだ集合研修の開催日番号を取得。集合研修申込画面での、ラジオボタン初期オン状態を取得
    //--------------------------------------------------------------------------
    function get_mosikomi_no($contents_id) {
        return (int)dbGetOne("select no from study_assembly_application where id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr(GG_LOGIN_EMP_ID));
    }
    //--------------------------------------------------------------------------
    // 【管理側】集合研修開催日に対する出席申込者の数を取得。集合研修申込管理画面の一覧のリンク表示用
    //--------------------------------------------------------------------------
    function get_mosikomi_emp_count($id, $no) {
        return (int)dbGetOne("select count(emp_id) from study_assembly_application where id = ".dbPIntC($id)." and no = ".dbPIntC($no));
    }
    //--------------------------------------------------------------------------
    // 【管理側】集合研修実績日のリスト取得。受講者管理画面のプルダウン用
    //--------------------------------------------------------------------------
    function get_assembly_actual_date_list($contents_id) {
        $sql =
        " select assembly_actual_date from study_test_status".
        " where test_id=".dbPIntC($contents_id)." and edition=1 and assembly_actual_status=1".
        " and assembly_actual_date <> '' and assembly_actual_date is not null".
        " group by assembly_actual_date";
        return dbFind($sql);
    }
    //--------------------------------------------------------------------------
    // 【管理側】集合研修への申込者カウント取得
    //--------------------------------------------------------------------------
    function get_assembly_application_counts($contents_id, $no=0) {
        $sql =
        " select ts.assembly_status from study_test_status ts".
        " left outer join study_assembly_application saa on (saa.id = ts.test_id and saa.emp_id = ts.emp_id)".
        " where ts.test_id = ".dbPIntC($contents_id)." and ts.edition = 1".
        " ".($no ? " and saa.no = ".dbPIntC($no) : "");
        $rows = dbFind($sql);
        $res = array("assembly_status0"=>0, "assembly_status1"=>0, "assembly_status2"=>0);
        foreach ($rows as $row) {
            if ((int)$row["assembly_status"]==0) $res["assembly_status0"]++; // 未登録数 インクリメント
            if ((int)$row["assembly_status"]==1) $res["assembly_status1"]++; // 出席申込数 インクリメント
            if ((int)$row["assembly_status"]==2) $res["assembly_status2"]++; // 欠席申込数 インクリメント
        }
        return $res;
    }

    //**********************************************************************************************
    // 【管理側】集合研修開催日の削除（ユーザ申込情報も同時に削除）
    //  $no_list : 開催日番号リスト（チェックボックスで複数指定される場合あり）
    //**********************************************************************************************
    function delete_assembly($contents_id, $no_list) {
        if(empty($no_list)) return;
        foreach ($no_list as $no) {
            //--------------------------
            // 開催日を削除
            //--------------------------
            dbExec("update study_assembly set delete_flg = ".dbPBool(true)." where id = ".dbPIntC($contents_id)." and no = ".dbPIntC($no));
            //--------------------------
            // 開催日への申込があれば、未申込にする
            //--------------------------
            $sql =
            " update study_test_status set assembly_status = 0". // 0:未申込みへ
            " where test_id = ".dbPIntC($contents_id)." and edition = 1".
            " and emp_id in (".
            "     select emp_id from study_assembly_application where id = ".dbPIntC($contents_id)." and no = ".dbPIntC($no).
            " )";
            dbExec($sql);
            //--------------------------
            // 開催日への申込みがあれば、開催日を削除
            //--------------------------
            dbExec("delete from study_assembly_application where id = ".dbPIntC($contents_id)." and no = ".dbPIntC($no));
        }
        //--------------------------
        //★★★集合研修開催日数 サマリング再計算
        //--------------------------
        StudyCommonModel::update_assembly_count($contents_id);
    }

    //**********************************************************************************************
    // 【管理側】集合研修申込期間・備考の登録
    //**********************************************************************************************
    function updata_assembly_start_end_date($id, $start_date, $end_date, $notes){
        $sql =
        " update study_contents set" .
        " assembly_start_date = ".dbPStr($start_date).
        ",assembly_end_date = ".dbPStr($end_date).
        ",assembly_notes = ".dbPStr($notes).
        " where delete_flg = false and id = ".dbPIntC($id);
        dbExec($sql);
    }
    
    //**********************************************************************************************
    // 【管理側】テスト期間終了日時更新
    //**********************************************************************************************
    
    function updata_test_end_date($id, $end_date){
        $sql =
        " update study_contents set" .
        " test_end_date = ".dbPStr($end_date).
        " where delete_flg = false and id = ".dbPIntC($id);
        dbExec($sql);
    }
    
    

    //**********************************************************************************************
    // 【管理側】集合研修開催日の作成
    //**********************************************************************************************
    function create_assembly($contents_id, $no, $req) {
        $tmp_date = trim($req["date"]);
        $date = mb_substr($tmp_date,0,4) . mb_substr($tmp_date,5,2) . mb_substr($tmp_date,8,2);
        $start_time = str_pad($req["st_hour"], 2, "0", STR_PAD_LEFT) . $req["st_minute"];
        $end_time   = str_pad($req["ed_hour"], 2, "0", STR_PAD_LEFT) . $req["ed_minute"];
        $persons_limit = ($req["persons_limit"]=="" ? "" : max((int)$req["persons_limit"], 0));
        //--------------------------
        // 開催日をいったん削除
        //--------------------------
        dbExec("delete from study_assembly where id = ".dbPIntC($contents_id)." and no = ".dbPIntC($no)." and delete_flg = false");
        //--------------------------
        // 開催日番号がなければ新規作成につき採番
        //--------------------------
        if (!$no) $no = dbGetOne("select max(no) from study_assembly where id = ".dbPIntC($contents_id)) + 1;
        //--------------------------
        // 開催日の新規登録
        //--------------------------
        $sql =
        " insert into study_assembly (".
        "     id, no, date, start_time, end_time, place, lecturer, persons_limit, delete_flg".
        " ) values (".
        " ".dbPIntC($contents_id).
        ",".dbPIntC($no). // 開催日番号
        ",".dbPStr($date). // 開催日YYYYMMDDDD
        ",".dbPStr($start_time). // 開始時刻 HHMM
        ",".dbPStr($end_time). // 終了時刻 HHMM
        ",".dbPStr($req["place"]). // 開催場所
        ",".dbPStr($req["lecturer"]). // 講師
        ",".dbPIntC($persons_limit). // 定員：空値(NULL)であれば「定員なし」である）
        ",".dbPBool(false).
        " )";
        dbExec($sql);
        //--------------------------
        //★★★集合研修開催日数 サマリング再計算
        //--------------------------
        StudyCommonModel::update_assembly_count($contents_id);

        return $no; // 開催日番号を戻す
    }

    //**********************************************************************************************
    // 【ユーザ側】集合研修申込みを登録する(DELETE-INSERT)
    // $assembly_status : 0(ｸﾘｱ) or 1(出席) or 2(欠席)
    // $no              : 開催日番号／1(出席)のときのみ整数値が指定される
    //**********************************************************************************************
    function set_user_assembly_application($contents_id, $no, $assembly_status){
        if ($assembly_status!="1" && $assembly_status!="2") $assembly_status = "0";
        //--------------------------
        // 開催日への申込があれば、いったん削除
        //--------------------------
        dbExec("delete from study_assembly_application where id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr(GG_LOGIN_EMP_ID));
        //--------------------------
        // 出席登録なら開催日への申込をインサート
        //--------------------------
        if ($assembly_status=="1") {
            $sql =
            " insert into study_assembly_application (".
            "     id, no, emp_id, update_date".
            " ) values (".
            " ".dbPIntC($contents_id).
            ",".dbPIntC($no). // 開催日番号
            ",".dbPStr(GG_LOGIN_EMP_ID). // 申込者
            ",".dbPTime(date("Y/m/d H:i:s", time())). // 登録日（特に利用されていない）
            " )";
            dbExec($sql);
        }
        //--------------------------
        // 0(ｸﾘｱ) or 1(出席) or 2(欠席) の登録
        //--------------------------
        $sql =
        " update study_test_status set assembly_status = ".$assembly_status.
        " where edition = 1 and test_id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr(GG_LOGIN_EMP_ID);
        dbExec($sql);
        //--------------------------
        //※※※集合研修「予定」サマリングは不要。管理していない。
        //--------------------------
    }

    
    
    
    
    //**********************************************************************************************
    // 【管理側】集合研修申込みを登録する(DELETE-INSERT)
    // $assembly_status : 0(ｸﾘｱ) or 1(出席) or 2(欠席)
    //**********************************************************************************************
    function set_manager_assembly_application($contents_id, $assembly_status, $emp_ids){
         if ($assembly_status!="1" && $assembly_status!="2"){
            $assembly_status = "0";
             $date = "";
         }else{
            $date = date("Ymd");
         }
        foreach ($emp_ids as $emp_id) {
            $sql =
                " update study_test_status set assembly_status = ".$assembly_status.
                " ,assembly_status_date = ".dbPStr($date).
                " where edition = 1 and test_id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr($emp_id);
            dbExec($sql);
        }
        

    }
    
    
    
    
    
    
    
    //**********************************************************************************************
    // 【管理側】実際に出欠したかを登録する（画面チェックボックス操作）
    // $assembly_actual_status : 1 or 0  ／受講者管理画面下部ボタンで「出席」押下時は1、ｸﾘｱ押下時は0
    // $assembly_actual_date   : 上記出席ボタン押下で開いたダイアログで指定した日付
    //**********************************************************************************************
    function change_assembly_actual_status($contents_id, $emp_ids, $assembly_actual_status, $assembly_actual_date) {
        foreach ($emp_ids as $emp_id) {
            //--------------------------
            // 出席済または未出席にする
            //--------------------------
            $sql =
            " update study_test_status set".
            " assembly_actual_status = ".dbPIntC($assembly_actual_status). // 0(ｸﾘｱ) or 1(出席済)
            ",assembly_actual_date = ".dbPStr($assembly_actual_date). // ダイアログでの指定日
            " where test_id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr($emp_id)." and edition = 1";
            dbExec($sql);
            //--------------------------
            // ★★★総合値の再計算
            //--------------------------
            StudyCommonModel::update_total_status($contents_id, $emp_id);
        }
        //--------------------------
        //★★★集合研修「実績」サマリング再計算
        //--------------------------
        $cnt = (int)dbGetOne("select count(*) from study_test_status where assembly_actual_status = 1 and test_id = ".dbPIntC($contents_id)." and edition=1");
        dbExec("update study_contents set assembly_actual_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }

    //**********************************************************************************************
    // 【管理側】複数受講者をすべて出席済にする（CSV一括登録）
    // $assembly_application_date : ダイアログで指定した日付（YYYYMMDD）
    //**********************************************************************************************
    function import_assembly_application($contents_id, $emp_list, $assembly_application_date) {
        foreach($emp_list as $emp_id) {
            if (!$emp_id) continue;
            //--------------------------
            // 出席済にする
            //--------------------------
            $sql =
            " update study_test_status set".
            " assembly_actual_status = 1". // 常に「出席」
            ",assembly_actual_date = ".dbPStr($assembly_application_date). // ダイアログでの指定日
            " where test_id = ".dbPIntC($contents_id)." and emp_id = ".dbPStr($emp_id)." and edition = 1";
            dbExec($sql);
            //--------------------------
            // ★★★総合値の再計算
            //--------------------------
            StudyCommonModel::update_total_status($contents_id, $emp_id);
        }
        //--------------------------
        //★★★集合研修「実績」サマリング再計算
        //--------------------------
        $cnt = (int)dbGetOne("select count(*) from study_test_status where assembly_actual_status = 1 and test_id = ".dbPIntC($contents_id)." and edition=1");
        dbExec("update study_contents set assembly_actual_count = ".$cnt." where id = ".dbPIntC($contents_id));
    }
}

