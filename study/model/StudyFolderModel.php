<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// 講座関連のDBアクセス・ユーティリティ
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
class StudyFolderModel {

    //**********************************************************************************************
    // 講座情報１件取得
    //**********************************************************************************************
    function get_folder_row($folder_id) {
        return dbGetTopRow("select * from study_folder where id = ".dbPIntC($folder_id));
    }

    //**********************************************************************************************
    // 講座リストを取得
    // 「講座ツリービュー」作成時および、「講座名パス(xxx＞yyy＞zzz)」を作成する際に利用
    //
    // $parent_id      :「 研修・講義管理」画面での講座クリック時は値が入る
    // $sort1          : "desc":名前降順出力 それ以外:名前昇順出力
    // $is_auth_strict : 1のとき、権限によって「ツリー」の出力内容を変える。「講座名パス」表示の場合は権限に関係なく表示するため、値指定が無い
    //
    // ※ 一般ユーザが見た場合と、管理者が見た場合、および教育担当者が見た場合で、講座名パスは統一すべき
    //    ただし、教育担当者がツリービューを見ると、「そんな講座は存在しない」ように見えるので、
    //    教育担当者の場合は、ツリーのトップノードにマウスホバーすると、フルパスが見えるようにする、などしたい所かもしれない
    //
    // ※ 講座検索の場合は、教育担当者でも管理外講座は表示させないように、別途対処している
    //   （↑講座ツリーを作成する際、一番親が99999なら、管理外という判定を行っている）
    //**********************************************************************************************
    function get_folder_list($parent_id = null, $sort_string = "", $is_auth_strict = 0) {
        //--------------------------
        // ソート文字列を作成
        //--------------------------
        $sql_sort_string = array();
        $ss = explode(",", $sort_string);
        foreach ($ss as $sstr) {
            if ($sstr=="title-asc") $sql_sort_string[]= "name asc";
            if ($sstr=="title-desc") $sql_sort_string[]= "name desc";
            if ($sstr=="create_date-asc") $sql_sort_string[]= "create_date asc";
            if ($sstr=="create_date-desc") $sql_sort_string[]= "create_date desc";
        }
        if (!count($sql_sort_string)) $sql_sort_string[]= "name asc, create_date desc";

        //--------------------------
        // 一覧取得
        //--------------------------
      
        $sql =
        // <!-- RHEL7対応 2016.01.06 by suzuki -->
        //" select *, name as long_name, to_char(create_date, 'YYYY/MM/DD HH24:MI:SS') as create_date_disp".
        " select *, name as long_name, to_char(create_date, ".dbPStr('YYYY/MM/DD HH24:MI:SS').") as create_date_disp".
        // ここまで
        " from study_folder".
        " where delete_flg = ".dbPBool(false); // 基本SQL

        if ($parent_id) $sql .= " and parent_id = ".dbPIntC($parent_id); // parent_idが指定されていたら条件を追加
        $rows = dbFind($sql." order by ".implode(", ", $sql_sort_string)); // 名前ソート、デフォルト昇順

        if (!$is_auth_strict || !GG_STUDY_TANTOU_FLG) return $rows; // 担当権限に関連しない場合はここで終了

        //==================================================
        // 教育担当者権限の場合
        // 教育担当者なら担当講座配下のみツリー操作可能
        // ◆担当者講座の親講座IDをカラ値にすれば、講座ツリーで先頭にすることができる。
        // ◆もともとルート講座だったものは、担当者の講座でなければ、親講座を99999にする。これで親ナシになるだろう。
        //==================================================
        global $study_tantou_folders; // 管理可能な講座id配列。StudyInitController.php参照
        foreach ($rows as $idx => $row) {
            if (in_array($row["id"], $study_tantou_folders)) {
                $rows[$idx]["long_name"] = implode("\t",StudyCommonModel::get_folder_parents("name", $row["id"], $rows));
            }
        }
        foreach ($rows as $idx => $row) {
            if (in_array($row["id"], $study_tantou_folders)) {
                $rows[$idx]["original_parent_id"] = $rows[$idx]["parent_id"];
                $rows[$idx]["parent_id"] = "";
            }
            else if (!$row["parent_id"]) {
                $rows[$idx]["parent_id"] = 99999;
            }
        }
        return $rows;
    }

    //**********************************************************************************************
    // 講座新規登録
    //**********************************************************************************************
    function create_folder($folder_name, $parent_id = null) {
        if (StudyFolderModel::folder_exists($parent_id, $folder_name) > 0) {
            business_exception("登録先に同一名の研修・講義が存在します。タイトルを変更して登録してください。");
        }
        $seq = dbGetNextSequence("study_folder_id"); // 最新のシーケンスを取得
        $sql =
        " insert into study_folder (".
        "     id, parent_id, name, create_user_id, update_user_id, create_date, update_date, delete_flg".
        " ) values (".
        " ".dbPIntC($seq).
        ",".dbPIntC($parent_id).
        ",".dbPStr($folder_name).
        ",".dbPStr(GG_LOGIN_EMP_ID).
        ",".dbPStr(GG_LOGIN_EMP_ID).
        ",".dbPTime(date("Y/m/d H:i:s", time())).
        ",".dbPTime(date("Y/m/d H:i:s", time())).
        ",".dbPBool(false).
        " )";
        dbExec($sql);
    }

    //**********************************************************************************************
    // 講座削除
    //**********************************************************************************************
    function delete_folder($folder_id) {
        if (dbGetOne("select count(*) from study_folder where delete_flg = false and parent_id = ".dbPIntC($folder_id))) {
            business_exception("配下に講座が存在します。削除できません。");
        }
        if (dbGetOne("select count(*) from study_contents where delete_flg = false and folder_id = ".dbPIntC($folder_id))) {
            business_exception("配下に研修・講義が登録されています。削除できません。");
        }
        dbExec("update study_folder set update_user_id = ".dbPStr(GG_LOGIN_EMP_ID).", delete_flg = ".dbPBool(true)." where id = ".dbPIntC($folder_id));
    }

    //**********************************************************************************************
    // 講座移動
    //**********************************************************************************************
    function move_folder($folder_id, $target_folder_id, $folder_name) {
        if (StudyFolderModel::folder_exists($target_folder_id, $folder_name)) business_exception("移動先に同一名の講座が存在するため、移動できません。");
        $folder_list = StudyFolderModel::get_folder_list(); // 全講座
        $parent_id_list = StudyCommonModel::get_folder_parents("id", $target_folder_id, $folder_list); // 移動先講座の親リスト
        if (in_array((int)$folder_id, $parent_id_list)) business_exception("配下の講座へは移動できません。"); // 念の為。JavaScriptでも実施している。

        $sql =
        " update study_folder set".
        " parent_id = ".($target_folder_id ? dbPIntC($target_folder_id) : "null").
        ",update_user_id = ".dbPStr(GG_LOGIN_EMP_ID).
        ",update_date = ".dbPTime(date("Y/m/d H:i:s", time())).
        " where id = ".dbPIntC($folder_id);
        dbExec($sql);
    }

    //**********************************************************************************************
    // 講座名の変更
    //**********************************************************************************************
    function change_folder_name($current_folder_id, $current_parent_id, $new_folder_name, $current_folder_name) {
        if ($new_folder_name !== $current_folder_name &&
            StudyFolderModel::folder_exists($current_parent_id ,$new_folder_name)) {
            business_exception("指定された講座名は既に存在します。");
        }
        $sql =
        " update study_folder set".
        " name = ".dbPStr($new_folder_name).
        ",update_user_id = ".dbPStr(GG_LOGIN_EMP_ID).
        ",update_date = ".dbPTime(date("Y/m/d H:i:s", time())).
        " where id = ".dbPIntC($current_folder_id);
        dbExec($sql);
    }

    //**********************************************************************************************
    // 講座名重複チェック  同一階層に指定した講座名があればtrueを返す
    //**********************************************************************************************
    function folder_exists($parent_id, $name) {
        $sql = "select count(*) from study_folder where delete_flg = ".dbPBool(false)." and name = ".dbPStr($name);
        if ($parent_id) $sql .= " and parent_id = ".dbPIntC($parent_id); // parent_idが指定されていたら条件を追加
        else            $sql .= " and parent_id is null"; // ルートのみ検索する
        if ((int)dbGetOne($sql)) return true;
        return false;
    }
}
