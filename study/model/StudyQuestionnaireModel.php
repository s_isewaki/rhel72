<?php
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// アンケート関連のDBアクセス・ユーティリティ
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
require_once 'model/StudyCommonModel.php';

class StudyQuestionnaireModel {

    //**********************************************************************************************
    // アンケート設問取得  設問番号順
    //**********************************************************************************************
    function get_questionnaire($id) {
        return dbFind("select * from study_questionnaire where id = ".dbPIntC($id)." and edition = 1 order by no");
    }

    //**********************************************************************************************
    // 【管理側】アンケート設問を保存（DELETE-INSERT）
    //**********************************************************************************************
    function create_questionnaire($contents_id, $params, $start_date, $end_date) {
        //--------------------------
        // いったん削除
        //--------------------------
        dbExec("delete from study_questionnaire where id = ".dbPIntC($contents_id)." and edition = 1");
        //--------------------------
        // 設問の数インサート
        //--------------------------
        $loop_count = count($params);
        $item_no = 1;
        for ($i = 0; $i < $loop_count; $i++) {
               
            $question = str_replace (array("&nbsp;","&#160;"),' ',$params[$i][0]);
            
            $sql =
            " insert into study_questionnaire (".
            "     id, edition, no, question, type, required, choice_cnt, choices".
            " ) values (".
            " ".dbPIntC($contents_id).
            ",1".
            ",".dbPIntC($item_no).
            ",".dbPStr($question). // question 設問内容
            ",".dbPIntC($params[$i][1]). // type 1:単数(ラジオ) 2:複数(チェックボックス) 3:文章入力
            ",".dbPBool($params[$i][2]). // required 必須なら1（=true)
            ",".dbPIntC($params[$i][3]). // choice_cnt // 選択肢数
            ",".dbPStr($params[$i][4]). // choices // 選択肢、カンマ区切りで
            " )";
            dbExec($sql);
            $item_no++;
        }
        //--------------------------
        // ★★★アンケート公開期間を研修講義テーブルへ
        //--------------------------
        $sql =
        " update study_contents set" .
        " questionnaire_start_date = " . dbPStr($start_date).
        ",questionnaire_end_date = " . dbPStr($end_date).
        " where id = " . dbPIntC($contents_id);
        dbExec($sql);
        //--------------------------
        // ★★★アンケート設問数サマリング再計算
        //--------------------------
        StudyCommonModel::update_questionnaire_count($contents_id);
    }


    //**********************************************************************************************
    // 【ユーザ側】アンケート回答取得
    //**********************************************************************************************
    function get_questionnaire_answer($contents_id, $emp_id) {
        $sql =
        " select sq.*, tin.impression from study_questionnaire sq".
        " left outer join study_test_impression_new tin on (".
        "     tin.test_id = ".dbPIntC($contents_id).
        "     and tin.edition = 1".
        "     and tin.emp_id = ".dbPStr($emp_id).
        "     and tin.no = sq.no".
        " )".
        " where sq.id = ".dbPIntC($contents_id) .
        " and sq.edition = 1".
         " order by sq.no";
       
        return dbFind($sql);
    }

    //**********************************************************************************************
    // 【管理側】全受講者のアンケート回答を取得  アンケート回答日時降順
    //**********************************************************************************************
    function get_questionnaire_all_answer($contents_id,$joken) {
        $sql =
        " select imp.no, imp.impression, em.emp_id, em.emp_personal_id, em.emp_lt_nm || ' ' || em.emp_ft_nm as emp_name".
        " from study_test_impression_new imp".
        " inner join empmst AS em on (imp.emp_id = em.emp_id)".
        " left outer join jobmst on (em.emp_job = jobmst.job_id)".
        " where imp.test_id = ".dbPIntC($contents_id).
        " and imp.edition = 1";
        
        //--------------------
        // 画面指定された検索条件
        //--------------------
        $where = "";
        if ($joken["job"])    $where .= " and em.emp_job = ".dbPIntC($joken["job"]);
              
        $sql .= $where." order by imp.update_date desc";
        return dbFind($sql);
    }
    
        //**********************************************************************************************
    // 【管理側】アンケート回答時刻取得
    //**********************************************************************************************
    function get_questionnaire_answer_date($contents_id, $emp_id) {
        $sql =
        " select tin.update_date from study_test_impression_new tin".
        " where tin.test_id = ".dbPIntC($contents_id) .
        " and tin.emp_id=".dbPStr($emp_id).
        " and tin.edition = 1".
        " group by tin.update_date";
       
        return dbFind($sql);
    }
    
    
    

    //**********************************************************************************************
    // 【管理側】アンケート回答を保存
    //**********************************************************************************************
    function update_questionnaire_answer($contents_id, $emp_id, $rows, $is_delete_only) {
        //--------------------------
        // アンケート回答を削除
        //--------------------------
        $now1 = date("Y/m/d H:i:s" ,time());
        dbExec("delete from study_test_impression_new where test_id = ".dbPIntC($contents_id)." and edition = 1 and emp_id = ".dbPStr($emp_id));

        //--------------------------
        // 登録（ユーザ側のみ、管理側は削除のみ）
        //--------------------------
        if (!$is_delete_only) {
            foreach ($rows as $idx => $row) {
                $num = $row["no"];
                $survey = $_REQUEST["survey".$num]; // ラジオボタン、またはフリー入力
                if ($row["type"]== "2") $survey = implode(",", $_REQUEST["survey".$num]); // チェックボックス 回答番号をカンマつなぎで
                $sql =
                " insert into study_test_impression_new (".
                "     test_id, edition, no, emp_id, impression, update_date".
                " ) values (".
                "     ".dbPIntC($contents_id).",1,".dbPIntC($num).",".dbPStr($emp_id).",".dbPStr($survey).",".dbPTime($now1).
                " )";
                dbExec($sql);
            }
        }
        //--------------------------
        // アンケート回答済/未回答状態を更新
        //--------------------------
        if (!$is_delete_only) {
            dbExec("update study_test_status set questionnaire_status = 1 where edition=1 and test_id=".dbPIntC($contents_id)." and emp_id=".dbPStr($emp_id));
        } else {
            dbExec("update study_test_status set questionnaire_status = 0 where edition=1 and test_id=".dbPIntC($contents_id)." and emp_id=".dbPStr($emp_id));
        }
        //--------------------------
        // ★★★アンケート回答済件数サマリング再計算
        //--------------------------
        StudyCommonModel::update_questionnaire_answer_count($contents_id);
        //--------------------------
        // ★★★総合値の再計算
        //--------------------------
        StudyCommonModel::update_total_status($contents_id, $emp_id);
    }
}

