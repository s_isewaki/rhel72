<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【ユーザ側】テスト実施とテスト結果表示関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyContentsModel.php';
require_once 'model/StudyTestModel.php';
require_once 'model/StudyFolderModel.php';


$btn_event = $_REQUEST["btn_event"];


if ($btn_event=="be_show_test_user_result") show_test_user_result_and_die();
if ($btn_event=="be_grading") exec_test_save_and_redirect();


default_view($btn_event);
die;








//**************************************************************************************************
// ユーザ側テスト実行画面
//**************************************************************************************************
function default_view($btn_event) {
    $contents_id = $_REQUEST["contents_id"];

    $contents = StudyContentsModel::get_contents_row($contents_id);
    $test_start_date = $contents["test_start_date"];
    $test_end_date = $contents["test_end_date"];
    $is_date_expired = (conv_ymdhm_to_jp_and_time2($test_end_date) < date("YmdHi") ? "1" : "");
    $is_out_of_date  = (date("YmdHi") > $test_end_date || date("YmdHi") < $test_start_date || $is_date_expired ? "1" : "");

    //------------------------------------------------------
    // テスト内容を取得
    //------------------------------------------------------
    $test_info = StudyTestModel::get_test_row($contents_id);
    $test_items = StudyTestModel::get_test_items($contents_id);

    $no_array = array();
    foreach ($test_items as $key=>$item) {
        $choices = split(",", $item["choices"]);
        $answers = split(",", $item["answers"]);
        unset($item["choices"]);
        unset($item["answers"]);
        $test_items[$key]["choices"] = $choices;
        $test_items[$key]["answers"] = $answers;
        $path = $test_items[$key]["image_path"];
        if (substr($path,0,strlen(GG_BASE_FOLDER)+1)==GG_BASE_FOLDER."/") $path = substr($path, strlen(GG_BASE_FOLDER)+1);
        $test_items[$key]["image_path_url"] = $path;
        $shuffled_order[] = $item["no"];
    }
    unset($item);   // 最後の要素への参照を解除

    // 問題表示順が「1:シャッフル」の場合
    if ($test_info["test_item_order"] == 1) {
        if ($_REQUEST["shuffled_order"]) {
            $shuffled_order = explode(",", $_REQUEST["shuffled_order"]);
        } else {
            shuffle($shuffled_order);
        }
        StudyCommonModel::sort_shuffled_order($test_items, $shuffled_order);
    }
    require_once 'conf/smarty_start.conf';
    $smarty->assign("contents", $contents);
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("test_start_date_jp", conv_ymdhm_to_jp_and_time2($test_start_date));
    $smarty->assign("test_end_date_jp", conv_ymdhm_to_jp_and_time2($test_end_date));
    $smarty->assign("is_out_of_date", $is_out_of_date);
    $smarty->assign("is_date_expired", $is_date_expired);
    $smarty->assign("test_info", $test_info);
    $smarty->assign("shuffled_order", $shuffled_order);
    $smarty->assign("test_items", $test_items);
    $smarty->display("study_test_user_execute.tpl");
    die;
}








//**************************************************************************************************
// テスト終了の場合：採点して保存してリダイレクト
//**************************************************************************************************
function exec_test_save_and_redirect() {
    $contents_id = $_REQUEST["contents_id"];
    StudyTestModel::create_test_result($contents_id);
    header("Location: study_test_user.php?session=".GG_SESSION_ID."&btn_event=be_show_test_user_result&test_direct=1&contents_id=".$contents_id);
    die;
}








//**************************************************************************************************
// テスト結果表示
// ユーザ側研修・講義画面からの「前回」リンクと、テスト終了と同時に表示される場合の２パターン
//**************************************************************************************************
function show_test_user_result_and_die() {
    $contents_id = $_REQUEST["contents_id"];
    $target_emp_id = $_REQUEST["target_emp_id"];
    if (!GG_ADMIN_FLG && !GG_STUDY_ADMIN_FLG && !GG_STUDY_TANTOU_FLG) $target_emp_id = GG_LOGIN_EMP_ID;
    if (!$target_emp_id) $target_emp_id = GG_LOGIN_EMP_ID;

    $test_info = StudyTestModel::get_test_row($contents_id); // テスト情報を取得
    $test_items = StudyTestModel::get_test_items($contents_id); // テスト項目を取得
    $test_result = StudyTestModel::get_test_result($contents_id, $target_emp_id); // テスト結果を全て取得する

    $target_emp_name = "";
    if ($_REQUEST["target_emp_id"]) {
        $emp_info = StudyCommonModel::get_user_data_by_emp_id($target_emp_id);
        $target_emp_name = $emp_info["emp_lt_nm"]." ".$emp_info["emp_ft_nm"];
    }

    $contents = StudyContentsModel::get_contents_row($contents_id);

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    //$auth_matrix = StudyCommonModel::get_auth_matrix("study_test_user_result.tpl@all", $contents["folder_id"], $target_emp_id);
    //$disp_auth = $auth_matrix["disp_auth"];

    //------------------------------------------------------
    // 直近の回答分から誤答問題を抽出
    //------------------------------------------------------
    $test_result_latest = $test_result[count($test_result) - 1];
    $user_all_answers = explode("|", $test_result_latest["answers"]);
    for ($i = 0; $i < count($test_items); $i++) {
        $test_items[$i]["no"] = $i + 1;
        if ($user_all_answers[$i] == "") {
            $test_items[$i]["result"] = "×";
            $test_items[$i]["answer"] = "未解答";
        }else{
            $test_items[$i]["answer"] = $user_all_answers[$i];
            //ラジオとチェックボックスの区別。チェックボックスの場合はカラ要素有無を問わず、必ず設問数と同じ要素で、カンマ区切り複数である。
            $test_items[$i]["result"] = "○";
            if (!StudyTestModel::check_answer($user_all_answers[$i], $test_items[$i]["answers"])) {
                $test_items[$i]["result"] = "×";
            }
        }
        $path = $test_items[$i]["image_path"];
        if (substr($path,0,strlen(GG_BASE_FOLDER)+1)==GG_BASE_FOLDER."/") $path = substr($path, strlen(GG_BASE_FOLDER)+1);
        $test_items[$i]["image_path_url"] = $path;
    }

    //------------------------------------------------------
    // 問題表示順が「1:シャッフル」の場合は、問題をシャッフル後の順番に並べ替え
    //------------------------------------------------------
    if ($test_info["test_item_order"] == 1 && $test_result_latest["shuffled_order"]) {
        $shuffled_order = explode(",", $test_result_latest["shuffled_order"]);
        StudyCommonModel::sort_shuffled_order($test_items, $shuffled_order);
    }

    //------------------------------------------------------
    // 問題に回答を紐つける
    //------------------------------------------------------
    foreach ($test_items as $key=>$item) {
        $choices = split(",", $item["choices"]);
        $answers = split(",", $item["answers"]);
        unset($item["choices"]);
        unset($item["answers"]);
        $test_items[$key]["choices"] = $choices;
        $test_items[$key]["answers"] = $answers;
    }


    //------------------------------------------------------
    // ここまできて何だが、
    // 表示権限がなければ個人情報をクリアする
    //------------------------------------------------------
    //if (!$disp_auth) {
    //    foreach ($test_items as $key=>$item) {
    //        $test_items[$key]["result"] = ""; // ○×をクリア
    //    }
    //    $test_result = array(); // 「n回目」の左下履歴をクリア
    //}

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';
    $smarty->assign("test_items", $test_items);
    //$smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("test_direct", $_REQUEST["test_direct"]);
    $smarty->assign("contents", $contents);
    $smarty->assign("test_item_count", count($test_items));
    $smarty->assign("test_result", $test_result);
    $smarty->assign("target_emp_name", $target_emp_name);
    $smarty->display("study_test_user_result.tpl");
    die;
}

?>
