<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【管理側】お知らせ関連を扱うURL、及びユーザのお知らせ詳細プレビューダイアログも含む
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyFolderModel.php';
require_once 'model/StudyNotificationModel.php';
require_once 'model/StudyContentsModel.php';
require_once 'model/StudyCommonModel.php';

$event = $_REQUEST["event"];

if (in_array($event, array("ev_open_create","ev_open_edit","ev_exec_set_confirm_mark","ev_exec_edit","ev_exec_create"))) {
    show_and_exec_notif_edit_and_die($event);
}

default_view($event);
die;



//**************************************************************************************************
// お知らせ編集ダイアログ処理（プレビュー兼用）
//**************************************************************************************************
function show_and_exec_notif_edit_and_die($event) {
    require_once 'conf/smarty_start.conf';
    $smarty->assign("result", "");
    $smarty->assign("event", $event);

    //==========================================================================
    // 「既読にする・未読にする」 を登録する
    //==========================================================================
    if ($event=="ev_exec_set_confirm_mark") {
        $notif_id = $_REQUEST["notif_id"];
        StudyNotificationModel::mark_to_confirm($notif_id, $_REQUEST["mark_backward"]);
        $smarty->assign("result", "success");
    }

    //==========================================================================
    // ダイアログで更新ボタンを押した場合
    //==========================================================================
    if ($event=="ev_exec_edit") {
        StudyNotificationModel::update_notif($_REQUEST);
        StudyCommonModel::update_notif_count($_REQUEST["contents_id"]);
        $smarty->assign("event", "ev_open_edit");
        $smarty->assign("result", "success");
    }

    //==========================================================================
    // ダイアログで作成ボタンを押した場合
    //==========================================================================
    if ($event=="ev_exec_create") {
        StudyNotificationModel::create_notif($_REQUEST);
        StudyCommonModel::update_notif_count($_REQUEST["contents_id"]);
        $smarty->assign("event", "ev_open_create");
        $smarty->assign("result", "success");
    }

    //------------------------------------------------------
    // ダイアログタイトル
    //------------------------------------------------------
    if ($_REQUEST["preview_flg"] == "t") {
        $preview_flg = "t";
        $title = "お知らせ";
    }else if ($_REQUEST["event"]=="ev_open_create") {
        $title = "お知らせ作成";
    }else {
        $title = "お知らせ編集";
    }

    //------------------------------------------------------
    // お知らせ情報
    //------------------------------------------------------
    $information = StudyNotificationModel::get_notif_row($_REQUEST["notifid"]);
    $target = $information[0]["target"];
    $array_start = conv_ymdhm_to_jp_and_time($information[0]["start_date"]);
    $array_end = conv_ymdhm_to_jp_and_time($information[0]["end_date"]);

    $array_check = array("","","","",""); // 対象者チェック
    $array_check[$target]="checked";

    //------------------------------------------------------
    // 研修・講義情報
    //------------------------------------------------------
    $contents_id = $_REQUEST["contents_id"];
    if (!$contents_id) $contents_id = $information[0]["contents_id"];
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $folder_id  = $contents["folder_id"];

    //------------------------------------------------------
    // 講座情報
    //------------------------------------------------------
    $folder_list = StudyFolderModel::get_folder_list(); // 講座一覧。講座名作成用
    $folder_joint_name = implode("＞", StudyCommonModel::get_folder_parents("name", $folder_id, $folder_list));

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_notif_admin_edit.tpl@all", $folder_id, "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $smarty->assign("title", $title);
    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);
    $smarty->assign("preview_flg", $preview_flg);
    $smarty->assign("contents_title", $contents["title"]);
    $smarty->assign("folder_joint_name", $folder_joint_name);
    $smarty->assign("array_start", $array_start);
    $smarty->assign("array_end", $array_end);
    $smarty->assign("array_check", $array_check);
    $smarty->assign("information", $information[0]);
    $smarty->assign("contents_id", $contents_id);
    $smarty->display("study_notif_admin_edit.tpl");
    die;
}









//**************************************************************************************************
// お知らせ一覧画面処理
//**************************************************************************************************
function default_view($event) {
    require_once 'conf/smarty_start.conf';
    $contents_id = $_REQUEST["contents_id"];
    $smarty->assign("result", "");

    //==========================================================================
    // ダイアログで削除ボタンを押した場合
    //==========================================================================
    if ($event=="ev_delete") {
        dbBeginTransaction();
        $ids = explode(",", $_REQUEST["notif_id_comma_list"]);
        foreach ($ids as $notif_id) {
            if ($notif_id) StudyNotificationModel::delete_notif($notif_id);
        }
        StudyCommonModel::update_notif_count($_REQUEST["contents_id"]);
        dbEndTransaction();
        $smarty->assign("event", "ev_delete");
        $smarty->assign("result", "success");
    }

    //------------------------------------------------------
    // 一覧検索
    //------------------------------------------------------
    $notif_list = StudyNotificationModel::get_admin_notif_list($contents_id);
    foreach($notif_list as $key=>$item){
         $notif_list[$key]["end_date_fmt"] = "";
        if($item["end_date"]) {
            $notif_list[$key]["end_date_fmt"] = conv_ymdhm_to_jp_and_time2($item["end_date"]);
        }
    }

    $contents = StudyContentsModel::get_contents_row($contents_id);

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_notif_admin_list.tpl@all", $contents["folder_id"], "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("contents_title", $contents["title"]);
    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);
    $smarty->assign("notif_list", $notif_list);
    $smarty->display("study_notif_admin_list.tpl");
}

?>
