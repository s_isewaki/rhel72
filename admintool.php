<?
ini_set("display_errors","1");
error_reporting(E_ALL);
session_start();

require_once("about_comedix.php");

// 唯一のセッション内変数。ログインしているかどうかのフラグを格納
$IS_LOGIN_OK = @$_SESSION["admintool_auth_type"] ? true : false;

// モード
// ・ログイン前はカラ
// ・"login" ログイン時
// ・"logout" ログアウト時
// ・"functions" モジュール一覧を表示する場合
// ・"remove_module" モジュール削除時
// ・"regist_module" モジュール追加登録時
// ・"download_module" モジュールファイルのダウンロード
// ・"addon" 追加したモジュールが動作するモード。
$MODE = @$_REQUEST["mode"];

// リクエストされたモジュール識別子名
// 各モジュールにて、自分モジュールを特定するために利用ください。

// モジュールを読み込ませるには、modeにaddonを渡す必要がある
// 小文字モジュール（URLで渡ってきたもの）は削除しておくので注意
$module = "";
$MODULE = @$_REQUEST["module"];

// モジュールファイル実体名
$MODULE_FILE_NAME = "";
if ($MODE=="addon"){
	if ($MODULE && substr($MODULE, -4, 4)==".mod") {
		$MODULE_FILE_NAME = ".".$MODULE;
	}
}

// このコンテンツが、ドキュメントルートにあるとして、
// ドキュメントルートを以下とする
$DOCUMENT_ROOT_DIR = dirname(__FILE__)."/";


// $self_url この機能画面のHTTP URL
$SELF_URL = atmodule_get_self_url();


// モジュール格納ディレクトリ（サーバ内絶対パス）
$MODULE_BASE_DIR = $DOCUMENT_ROOT_DIR."admintool/";
$MODULE_DIR_PATH = $MODULE_BASE_DIR."modules/";
$MODULE_ZIPDIR_PATH = $MODULE_BASE_DIR."zip/";
if (!is_dir($MODULE_BASE_DIR)) mkdir($MODULE_BASE_DIR, 0755);
if (!is_dir($MODULE_DIR_PATH)) mkdir($MODULE_DIR_PATH, 0755);
if (!is_dir($MODULE_ZIPDIR_PATH)) mkdir($MODULE_ZIPDIR_PATH, 0755);

// モジュールファイルまでのフルパス
$MODULE_FILE_PATH = "";
if ($MODULE_FILE_NAME != "" ) $MODULE_FILE_PATH = $MODULE_DIR_PATH . $MODULE_FILE_NAME;



//------------------------------------------
// 内部関数（１）自分のURLの取得
//------------------------------------------
function atmodule_get_self_url(){
	$pos = strpos($_SERVER["PHP_SELF"], "?");
	if ($pos===FALSE) $pos = strlen($_SERVER["PHP_SELF"]);
	return substr($_SERVER["PHP_SELF"], 0, $pos);
}

//------------------------------------------
// 汎用関数（１）共通HTMLヘッダスクリプトを返す
//------------------------------------------
function atmodule_get_common_html_header($stylesheet_script="", $metatag_script=""){
$out = <<< EOF
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=euc-jp"/>
	{$metatag_script}
	<title>CoMedix サーバ管理</title>
	<style type="text/css">
		body, table { font-size:13px; }
		body { margin:4px; padding:0; }
		table, img { border:0; }
		label, button { cursor:pointer }
		form { padding:0; margin:0 }
		a:link    { color:#00f; }
		a:visited { color:#088; }
		a:hover   { color:#f50; }
		a.always_blue:link    { color:#00f; }
		a.always_blue:visited { color:#00f; }
		a.always_blue:hover   { color:#f50; }

		.subtitle { padding:4px; padding-left:8px; margin-top:12px; background-color:#666; color:#fff; }
		.login_table td {padding:4px}
		.module_table { border-collapse:collapse; margin-top:6px; }
		.module_table th { background-color:#999; color:#fff; padding:4px; border:1px solid #aaa; font-weight:normal; }
		.module_table td { padding:4px; border:1px solid #aaa; }

		.simple_table { border-collapse:collapse; }
		.simple_table td { border: 1px solid #666; padding:2px }
		.simple_table th { border: 1px solid #666; padding:2px }
		{$stylesheet_script}
	</style>
</head>
EOF;
return $out;
}

//------------------------------------------
// 汎用関数（２）共通HTMLボディ冒頭部スクリプトを返す
//------------------------------------------
function atmodule_get_common_html_body_title($use_list_link = true){
	global $SELF_URL;
	global $IS_LOGIN_OK;
	$list_link = $use_list_link ? "機能モジュール一覧へ戻る" : "";
	$ret = array();
	$ret[] = "<body>";
	$ret[] = '<div style="background-color:#ddd; border:1px solid #999">';
	$ret[] = '<div style="float:left; padding:6px;">CoMedix サーバ管理</div>';
	if ($IS_LOGIN_OK) {
		$ret[] = '<div style="float:right; padding:6px;"><a href="'.$SELF_URL.'?mode=logout" class="always_blue">管理ログアウト</a></div>';
	}
	if ($use_list_link) {
		$ret[] = '<div style="float:right; padding:6px;"><a href="'.$SELF_URL.'?mode=functions" class="always_blue">'.$list_link.'</a></div>';
	}
	$ret[] = '<br style="clear:both">';
	$ret[] = '</div>';
	return implode($ret, "\n");
}


//------------------------------------------
// 汎用関数（３）共通HTMLフッタスクリプトを返す
//------------------------------------------
function atmodule_get_common_html_footer(){
	return "</body>\n</html>";
}

//------------------------------------------
// 汎用関数（４）ダウンロード用のHTMLヘッダの標準出力
//------------------------------------------
function atmodule_output_download_html_header($filename, $content_length=0){
	header("Content-Disposition: attachment ; filename=".$filename);
	header("Content-type: application/octet-stream");
	header('Content-Transfer-Encoding: binary');
	if ($content_length) header('Content-Length: '.$content_length);
	header("Cache-Control:");
	header("Pragma:");
}

//------------------------------------------
// 汎用関数（５）ZIPにしてダウンロード
//------------------------------------------
function atmodule_output_download_as_zip($filename, $contents_stream, $zip_password="@@@admintool"){
	global $MODULE_ZIPDIR_PATH;
	$zip_filename = $filename . ".zip";
	$save_filepath = "";
	$zip_dirpath = "";
	$zip_filepath = "";
	for ($i = 1; $i < 100; $i++){
		$zip_dirpath = $MODULE_ZIPDIR_PATH."dir_".date("Ymd_His")."_".$i;
		if (!is_dir($zip_dirpath)) break;
	}
	mkdir($zip_dirpath, 0755);
	$save_filepath = $zip_dirpath."/".$filename;
	$zip_filepath = $zip_dirpath."/".$zip_filename;

	if ($fh = fopen($save_filepath, "w")) {
		if (flock($fh, LOCK_EX)) {
			fwrite($fh, $contents_stream);
			flock($fh, LOCK_UN);
		}
		fclose($fh);
	}
	$zip_password = str_replace(" ", "", $zip_password);
	if ($zip_password) $zip_password = "-P ".$zip_password. " -e ";
	exec("zip ".$zip_password." -j ".$zip_filepath." ".$save_filepath);
	unlink($save_filepath);

	$size = filesize($zip_filepath);
	atmodule_output_download_html_header($zip_filename, $size);

	readfile($zip_filepath);
	unlink($zip_filepath);
	rmdir($zip_dirpath);
	die;
}


//------------------------------------------
// ログアウト処理
//------------------------------------------
if ($MODE=="logout"){
	$_SESSION["admintool_auth_type"] = "";
  header("Location: ".$SELF_URL);
  die;
}


//------------------------------------------
// ログイン確認
//------------------------------------------
if (!$IS_LOGIN_OK && $MODE=="login") {
	$_SESSION["admintool_auth_type"] = "";
	$con = connect2db($_SERVER["PHP_SELF"]);
	$sql =
		" select tool_emp_login_id, is_ignore_login_table, auth_type from admintool_login".
		" where tool_emp_login_id = '".pg_escape_string(@$_REQUEST["emp_login_id"])."'".
		" and tool_emp_login_pass = '".pg_escape_string(@$_REQUEST["emp_login_pass"])."'";
	$rs = pg_exec($con, $sql);
	$authinfo = @pg_fetch_array($rs);
	if (@$authinfo["tool_emp_login_id"]){
		if (!@$authinfo["is_ignore_login_table"]){
			$sql =
				" select emp_id from login".
				" where emp_id = '".pg_escape_string(@$_REQUEST["emp_login_id"])."'".
				" and emp_login_pass = '".pg_escape_string(@$_REQUEST["emp_login_pass"])."'".
				" and emp_login_flg = 't'";
			$rs = pg_exec($con, $sql);
			$emp_id = @pg_fetch_result($rs, 0, 0);
			if ($emp_id) $authinfo["auth_type"] = "";
		}
		$_SESSION["admintool_auth_type"] = $authinfo["auth_type"];
	}
	if ($_SESSION["admintool_auth_type"]) {
	  header("Location: ".$SELF_URL);
	  die;
	}
}


//------------------------------------------
// ログインしていなければログイン画面を出して終了
//------------------------------------------
if (!$IS_LOGIN_OK) {
	echo atmodule_get_common_html_header();
	echo atmodule_get_common_html_body_title(false);
?>
	<div class="subtitle">管理者ログイン</div>
	<form name="frm" method="post">
	<input type="hidden" name="mode" value="login">
	<table class="login_table" style="margin-top:20px">
		<tr>
			<td>ログインID</td>
			<td><input type="text" name="emp_login_id" style="width:200px" maxlength="30" autocomplete="off"></td>
		</tr>
		<tr>
			<td>パスワード</td>
			<td><input type="password" name="emp_login_pass" style="width:200px" maxlength="30" autocomplete="off"></td>
		</tr>
	</table>
	<div style="margin-top:10px"><input type="submit" value="ログイン" /></div>
	<? if (!is_dir($MODULE_DIR_PATH)) { ?>
	<div style="color:red">モジュール格納用のディレクトリが作成できません。</div>
	<? } ?>
<?
	echo atmodule_get_common_html_footer();
	die;
}



//------------------------------------------
// 外部モジュール読込み
//------------------------------------------
if ($MODULE_FILE_PATH){
	require($MODULE_FILE_PATH);
	die;
}



//------------------------------------------
// モジュールのダウンロード指定があった場合
//------------------------------------------
if ($MODE=="download_module"){
	$filename = trim(@$_REQUEST["filename"]);
	atmodule_output_download_html_header($filename);
	$fp = fopen($MODULE_DIR_PATH.".".$filename,"r");
	while(!feof($fp)) echo fgets($fp, 4096);
	fclose($fp);
	die;
}


//------------------------------------------
// モジュール抹消指定があった場合。モジュールファイルの抹消を行う。
//------------------------------------------
if ($MODE=="remove_module"){
	$filename = trim(@$_REQUEST["filename"]);
	if ($filename!=""){
		if (is_file($MODULE_DIR_PATH.".".$filename)) {
			unlink($MODULE_DIR_PATH.".".$filename);
		}
	}
  header("Location: ".$SELF_URL);
  die;
}

//------------------------------------------
// モジュール新規登録指定があった場合。ファイルのアップロードを行う。
//------------------------------------------
$errmsg = "";
if ($MODE=="regist_module"){
	if (@$_FILES["new_module"]){
		do {
			$filename = $_FILES["new_module"]["name"];
			if ($filename=="") break;
			switch ($_FILES["new_module"]["error"]) {
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
				$errmsg = 'ファイルサイズが大きすぎます。';
				break 2;
			case UPLOAD_ERR_PARTIAL:
			case UPLOAD_ERR_NO_FILE:
				$errmsg = 'アップロードに失敗しました。再度実行してください。';
				break 2;
			}
			// 「mod」拡張子を除去
			$filebase = $filename;
			if (strtolower(substr($filename, -4, 4)) == ".mod") $filebase = substr($filename, 0, strlen($filename)-4);
			if (substr($filebase, 0, 1) == ".") $filebase = substr($filebase, 1);

			// ファイルがユニーク名になるように
			$new_filename = "";
			for($i = 0; $i <= 100; $i++){
				$new_filename = ".".$filebase. ($i > 0 ? $i : "") . ".mod";
				if (!is_file($MODULE_DIR_PATH.$new_filename)) break;
			}

			// 格納
			$ret = copy($_FILES["new_module"]["tmp_name"], $MODULE_DIR_PATH.$new_filename);
			if ($ret == false) {
				$errmsg = 'ファイルの保存に失敗しました。再度実行してください。';
				break;
			}
		} while(false);
	}
  header("Location: ".$SELF_URL);
  die;
}


//------------------------------------------
// 機能モジュール一覧を表示させる。
// 機能モジュールはすべて存在を再確認する
//------------------------------------------
$modules = array();
$hdir = opendir($MODULE_DIR_PATH);
while (false !== ($filename = readdir($hdir))) {
	$filepath = $MODULE_DIR_PATH.$filename;
	if (substr($filename,0,1) != ".") continue;
	if (is_dir($filepath) || $filename==".." || $filename==".") continue;
	$filename = substr($filename, 1);
	$module_title = "";
	$module_information = "";
	$fp = fopen($filepath, "r");
	if (!$fp) continue;
	while (! feof($fp)) {
		$load = fgets($fp, 4096);
		if ($module_title == "") {
			$pos = (strpos($load, "ATMODULE_HTML_TITLE"));
			if ($pos!==FALSE){
				$module_title = trim(substr($load, $pos+19));
				if (substr($module_title,-2,2)=="?>") $module_title = trim(substr($module_title,0,strlen($module_title)-2));
			}
		}
		if ($module_information == "") {
			$pos = (strpos($load, "ATMODULE_HTML_INFORMATION"));
			if ($pos!==FALSE){
				$module_information = trim(substr($load, $pos+25));
				if (substr($module_information,-2,2)=="?>") $module_information = trim(substr($module_information,0,strlen($module_information)-2));
			}
		}
		if ($module_title != "" && $module_information != "") {
			$modules[] = array("filename" => $filename, "title" => $module_title, "info" => $module_information);
			break;
		}
	}
	fclose($fp);
}
closedir($hdir);

?>
	<?= atmodule_get_common_html_header() ?>
	<?= atmodule_get_common_html_body_title(false) ?>
	<div class="subtitle">機能モジュール一覧</div>
	<? if (count($modules)) { ?>
	<table class="module_table">
		<tr>
			<th>機能モジュール</th>
			<th>機能説明</th>
			<th>モジュールファイル</th>
			<th>モジュール抹消</th>
		</tr>
		<? foreach ($modules as $idx => $row){ ?>
		<tr>
			<td><a class="always_blue" href="<?=$SELF_URL?>?mode=addon&module=<?=$row["filename"]?>"><?=$row["title"]?></a></td>
			<td><?=$row["info"]?></td>
			<td><a class="always_blue" href="<?=$SELF_URL?>?mode=download_module&filename=<?=$row["filename"]?>" target="_blank">ファイルダウンロード</a></td>
			<td><a class="always_blue" href="" onclick="removeModule('<?=$row["filename"]?>'); return false;"><?=h($row["filename"])?></a></td>
		</tr>
		<? } ?>
	</table>
	<script type="text/javascript">
		function removeModule(filename){
			if (!confirm("「"+filename+"」を抹消します。よろしいですか？")) return;
			window.location.href = "<?=$SELF_URL?>?mode=remove_module&filename="+filename;
		}
	</script>
	<? } ?>

	<div style="margin-top:20px;">
		<div style="color:red; padding:4px"><?=$errmsg?></div>
		<div style="border:1px solid #aaa; background-color:#ccc; padding:4px">
			<form name="frm" method="post" enctype="multipart/form-data">
			<input type="file" name="new_module" size="80" style="width:500px">
			<input type="hidden" name="mode" value="regist_module">
			<input type="submit" value="新しいモジュールの登録">
			</form>
		</div>
	</div>
<?
	echo atmodule_get_common_html_footer();
	die;
?>
