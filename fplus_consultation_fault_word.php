<?php
require_once("about_comedix.php");
require_once ( './libs/phpword/PHPWord.php' );
require_once ( './libs/phpword/PHPWord/IOFactory.php' );
require_once ( './libs/phpword/PHPWord/Writer/Word2007.php' );
require_once("fplus_common.ini");

// データベースに接続
$con = connect2db($fname);

// 影響レベル取得
$level = get_inci_level_mst();

// 切断
pg_close($con);

// オブジェクト
$PHPWord = new PHPWord();

//====================================================================
// ヘッダ設定
//====================================================================
header("Content-Disposition: attachment; filename=fplus_medical_accident_info_".Date("Ymd_Hi").".docx;");
header("Content-Transfer-Encoding: binary");

// 日本語はUTF-8へ変換する。(mb_convert_encoding関数を使用)
// 変換しない場合は文書ファイルが壊れて生成される。

// デフォルトのフォント
$PHPWord->setDefaultFontName(mb_convert_encoding('ＭＳ 明朝','UTF-8','EUC-JP'));
$PHPWord->setDefaultFontSize(10.5);

// プロパティ
$properties = $PHPWord->getProperties();
$properties->setTitle(mb_convert_encoding('医療機器不具合報告書','UTF-8','EUC-JP'));
$properties->setCreator(mb_convert_encoding('Medi-System','UTF-8','EUC-JP')); 
$properties->setSubject(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setCompany(mb_convert_encoding('（株）メディシステムソリューション','UTF-8','EUC-JP'));
$properties->setCategory(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setCreated(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setModified(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setKeywords(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setLastModifiedBy(mb_convert_encoding('Medi-System','UTF-8','EUC-JP'));

// セクションとスタイル、単位に注意(1/144インチ)
$section = $PHPWord->createSection();
$sectionStyle = $section->getSettings();
$sectionStyle->setLandscape();
$sectionStyle->setPortrait();
$sectionStyle->setMarginLeft(1700);
$sectionStyle->setMarginRight(1700);
$sectionStyle->setMarginTop(1700);
$sectionStyle->setMarginBottom(1700);

// タイトル
$PHPWord->addFontStyle('title_pStyle', array('size'=>10.5,'spacing'=>100));
$PHPWord->addParagraphStyle('title_align', array('align'=>'center'));
$section->addText(mb_convert_encoding('医療機器不具合報告書','UTF-8','EUC-JP'), 'title_pStyle', 'title_align');

$section->addTextBreak(1);

// 発見年月日
if ($data_discovery_date != "") {
	$y = substr($data_discovery_date, 0, 4);
	$m = substr($data_discovery_date, 5, 2);
	$d = substr($data_discovery_date, 8, 2);
	$data_discovery_date = $y . "年" . (int)$m . "月" . (int)$d . "日";
}
$section->addText(mb_convert_encoding('発見年月日 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_discovery_date,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 発生年月日
if ($data_occurrences_date != "") {
	$y = substr($data_occurrences_date, 0, 4);
	$m = substr($data_occurrences_date, 5, 2);
	$d = substr($data_occurrences_date, 8, 2);
	$data_occurrences_date = $y . "年" . (int)$m . "月" . (int)$d . "日";
}
$section->addText(mb_convert_encoding('発生年月日 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_occurrences_date,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 製造業者又は販売会社
$section->addText(mb_convert_encoding('製造業者又は販売会社 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_manufacturer,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 分類
$str = "";
switch ($data_fault_class) {
	case "1":
		$str = "１．生命維持管理に使用する医療機器";
		break;
	case "2":
		$str = "２．その他の医療機器";
		break;
	case "3":
		$str = "３．医療材料";
		break;
	case "4":
		$str = "４．その他";
		break;
}
$section->addText(mb_convert_encoding('分類 ' . $str,'UTF-8','EUC-JP'));

// 機種名
$section->addText(mb_convert_encoding('機種名（医療機器名称等） ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_apparatus_name,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 製造年月
$data_manufacture = "";
if ($data_manufacture_year != "") {
	$data_manufacture .= $data_manufacture_year . "年";
}
if ($data_manufacture_month != "") {
	$data_manufacture .= $data_manufacture_month . "月";
}
$section->addText(mb_convert_encoding('製造年月 ' . $data_manufacture,'UTF-8','EUC-JP'));

// 購入年月
$data_purchase = "";
if ($data_purchase_year != "") {
	$data_purchase .= $data_purchase_year . "年";
}
if ($data_purchase_month != "") {
	$data_purchase .= $data_purchase_month . "月";
}
$section->addText(mb_convert_encoding('購入（リース開始）年月 ' . $data_purchase,'UTF-8','EUC-JP'));

// 直近の保守・点検年月
$data_check = "";
if ($data_check_year != "") {
	$data_check .= $data_check_year . "年";
}
if ($data_check_month != "") {
	$data_check .= $data_check_month . "月";
}
$section->addText(mb_convert_encoding('直近の保守・点検年月 ' . $data_check,'UTF-8','EUC-JP'));

$section->addTextBreak(1);

// 不具合の内容
$section->addText(mb_convert_encoding('不具合の内容','UTF-8','EUC-JP'));
$ary_fault_contents = preg_split("/\n/", $data_fault_contents);
for($j=0;$j<count($ary_fault_contents);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_fault_contents[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// 発生時の状況
$section->addText(mb_convert_encoding('発生時の状況','UTF-8','EUC-JP'));
$ary_occurrences_situation = preg_split("/\n/", $data_occurrences_situation);
for($j=0;$j<count($ary_occurrences_situation);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_occurrences_situation[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// ＫＫＲの患者影響
$section->addText(mb_convert_encoding('KKRの患者影響','UTF-8','EUC-JP'));
if ($data_patient_influence_check[0]=="1") {
	$str = "あり";
} else if ($data_patient_influence_check[0]=="0") {
	$str = "なし";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  ' . $str,'UTF-8','EUC-JP'));
if ($data_patient_influence_check[0]=="1") {
	foreach ($level as $key => $val) {
		if ($data_patient_influence_level == $val["short_name"]) {
			$str = $val["easy_name"];
			break;
		}
	}
	$section->addText(mb_convert_encoding('  影響レベル ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($str,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));
	$ary_patient_influence_contents = preg_split("/\n/", $data_patient_influence_contents);
	for($j=0;$j<count($ary_patient_influence_contents);$j++){
		$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_patient_influence_contents[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
	}
}

// 出力
$WORD_OUT =  PHPWord_IOFactory::createWriter($PHPWord,'WORD2007');
//$WORD_OUT->save('./fplus/workflow/tmp/test.docx');
$WORD_OUT->save('php://output');

exit;

php?>
