<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="fplus_approve_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<?echo($category)?>">
<input type="hidden" name="workflow" value="<?echo($workflow)?>">
<input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
<input type="hidden" name="apply_emp_nm" value="<?echo($apply_emp_nm)?>">
<input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
<input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
<input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
<input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
<input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
<input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
<input type="hidden" name="apv_stat" value="<?echo($apv_stat)?>">
<input type="hidden" name="page" value="<?echo($page)?>">
</form>

<?

require_once("about_comedix.php");
require_once("fplus_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;

}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

$must_send_mail_applys = array();
$files = array();
for ($i = 0, $count = count($approve_chk); $i < $count; $i++)
{
	// パラメータ取得
	$app_values = $approve_chk[$i];
	$arr_app = split(",", $app_values);

	$apply_id       = $arr_app["0"];
	$apv_order      = $arr_app["1"];
	$apv_sub_order  = $arr_app["2"];

	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
	$wkfw_appr = $arr_apply_wkfwmst[0]["wkfw_appr"];

	$arr_applyapv_per_hierarchy = $obj->get_applyapv_per_hierarchy($apply_id, $apv_order);
	$next_notice_div = $arr_applyapv_per_hierarchy[0]["next_notice_div"];

	// 承認処理
	$obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, "", $next_notice_div, $session, "LIST");

	// メールを送るかどうか判断、送る場合は宛先を配列に追加
	if (fplus_must_send_mail_to_next_approvers($con, $obj, $apply_id, $wkfw_appr, $apv_order, $approve, $next_notice_div, $fname)) {

		// 次の階層で未承認の承認者（全員のはず）のメールアドレスを取得
		$next_apv_order = $apv_order + 1;
		$sql = "select e.emp_email2 from empmst e";
		$cond = "where exists (select * from fplusapplyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id and a.apv_order = $next_apv_order and a.apv_stat = '0')";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$to_addresses = array();
		while ($row = pg_fetch_array($sel)) {
			if ($row["emp_email2"] != "") {
				$to_addresses[] = $row["emp_email2"];
			}
		}

		// メール送信に必要な情報を配列に格納
		if (count($to_addresses) > 0) {
			$emp_detail = $obj->get_empmst_detail($arr_apply_wkfwmst[0]["emp_id"]);

			$must_send_mail_applys[] = array(
				"to_addresses" => $to_addresses,
				"wkfw_content_type" => $arr_apply_wkfwmst[0]["wkfw_content_type"],
				"content" => $arr_apply_wkfwmst[0]["apply_content"],
				"apply_title" => $arr_apply_wkfwmst[0]["apply_title"],
				"emp_nm" => fplus_format_emp_nm($emp_detail[0]),
				"emp_mail" => fplus_format_emp_mail($emp_detail[0]),
				"emp_pos" => fplus_format_emp_pos($emp_detail[0]),
				"approve_label" => ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認"
			);
		}
	}

	// 文書登録処理
	$tmp_files = fplus_register_library($con, $approve, $obj, $apply_id, $fname);
	$files = array_merge($tmp_files, $files);
}

// トランザクションをコミット
//pg_query($con, "rollback");
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// メール送信
if (count($must_send_mail_applys) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	foreach ($must_send_mail_applys as $data) {
		$mail_subject = "[CoMedix] 受付依頼のお知らせ";
		$mail_content = fplus_format_mail_content($data["wkfw_content_type"], $data["content"]);
		$mail_separator = str_repeat("-", 60) . "\n";
		$additional_headers = "From: {$data["emp_mail"]}";
		$additional_parameter = "-f{$data["emp_mail"]}";

		foreach ($data["to_addresses"] as $to_address) {
			$mail_body = "以下の受付依頼がありました。\n\n";
			$mail_body .= "報告者：{$data["emp_nm"]}\n";
			$mail_body .= "所属：{$data["emp_pos"]}\n";
			$mail_body .= "表題：{$data["apply_title"]}\n";
			if ($mail_content != "") {
				$mail_body .= $mail_separator;
				$mail_body .= "{$mail_content}\n";
			}

			mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
		}
	}
}

// ファイルコピー
foreach ($files as $tmp_file) {
	fplus_copy_apply_file_to_library($tmp_file);
}

// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
