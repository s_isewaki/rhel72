<?php

//<!-- ************************************************************************ -->
//<!-- 勤務シフト作成　共通ＣＬＡＳＳ -->
//<!-- ************************************************************************ -->
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("show_select_values.ini");
require_once("duty_shift_common.ini");
require_once("date_utils.php");
require_once("timecard_bean.php");
require_once("timecard_common_class.php");

class duty_shift_common_class {

    var $file_name; // 呼び出し元ファイル名
    var $_db_con;   // DBコネクション
    var $timecard_bean;    // タイムカード情報インスタンス
    var $aomori_flag; //青森県中様用対応 20130328
    // 20140210 start
    var $data_assoc_flg = false;
    var $data_st_assoc = array(); //st_idをキーに役職名を格納
    var $data_job_assoc = array(); //job_idをキーに職種名を格納
    var $data_emp_idx = array(); //emp_idをキーとして、位置を格納
    // 20140210 end
    var $arr_emp_info = array(); //様式９用職員情報
    // 当直・待機
    var $duty_or_oncall_str = "当直";

    /*     * ********************************************************************** */

    // コンストラクタ
    // @param object $con DBコネクション
    // @param string $fname 画面名
    /*     * ********************************************************************** */
    function duty_shift_common_class($con, $fname) {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
        // 全項目リストを初期化
        // 「勤務シフト集計方法変更」対応
        // タイムカード情報インスタンスを生成
        $this->timecard_bean = new timecard_bean();
        $this->timecard_bean->select($this->_db_con, $this->file_name);
        //青森県中様用対応 20130328
        $this->aomori_flag = file_exists("opt/aomori_flag");
        // system_config
        $conf = new Cmx_SystemConfig();
        $duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
        if ($duty_or_oncall_flg == "") {
            $duty_or_oncall_flg = "1";
        }
        $this->duty_or_oncall_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：固定値）
//-------------------------------------------------------------------------------------------------------------------------

    /*     * ********************************************************************** */
    // 勤務シフト表示事由情報取得
    //
    // @return  $arr_reason_disp    事由情報
    /*     * ********************************************************************** */
    function get_arr_reason_disp() {
        // 勤務パターンプルダウンに休暇として表示している事由
        $arr_reason_disp = array("24", "22", "23", "1", "37", "4", "17", "5", "6", "7", "40", "41", "8", "44", "45", "46", "47", "25", "26", "27", "28", "29", "30", "31", "32", "61", "57", "58", "59", "60", "54", "55", "56", "72", "62", "63", "64");
        //代替出勤、振替出勤、公外出、通院、私用、交通遅延、遅刻、早退を休暇扱いとする 20141031
        $arr_reason_disp = array_merge($arr_reason_disp, array("14", "15", "33", "9", "10", "11", "12", "13"));

        return $arr_reason_disp;
        //データは以下のSQLで確認できます
        //select * from atdbk_reason_mst where reason_id in ('24', '22', '23', '1', '37', '4', '17', '5', '6', '7', '40', '41', '8', '44', '45', '46', '47', '25', '26', '27', '28', '29', '30', '31', '32', '61', '57', '58', '59', '60', '54', '55', '56', '72', '62', '63', '64') order by to_number(reason_id,'99');
        // 1    有給休暇
        // 4    代替休暇
        // 5    特別休暇
        // 6    一般欠勤
        // 7    病傷欠勤
        // 8    その他休
        //17    振替休暇
        //22    法定休暇
        //23    所定休暇
        //24    公休
        //25    産休
        //26    育児休業
        //27    介護休業
        //28    傷病休職
        //29    学業休職
        //30    忌引
        //31    夏休
        //32    結婚休
        //37    年休
        //40    リフレッシュ休暇
        //41    初盆休暇
        //44    半有半公
        //45    希望(公休)
        //46    待機(公休)
        //47    管理当直前(公休)
        //54    半夏半公
        //55    半夏半有
        //56    半夏半欠
        //57    半有半欠
        //58    半特半有
        //59    半特半公
        //60    半特半欠
        //61    年末年始
        //62    半正半有
        //63    半正半公
        //64    半正半欠
        //72    半夏半特
    }

    /*     * ********************************************************************** */

    // 事由情報（午前有休／午後有休）取得
    // @param   $data_flg 1:公休等全て取得 "":公休等シフトのプルダウン分を除く
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_reason_2($data_flg = "") {

        /* データ例）マスタから取得、nameは表示変更(display_name)、事由標準設定(default_name)の順で優先設定
          $data = array(
          0 => array("id" => "2", "name" => "午前有休", "font_name" => "／午前有休", "day_count" => "0.5")
          );

         */
        // 勤務パターンプルダウンに休暇として表示している事由
        $arr_reason_disp = $this->get_arr_reason_disp();
        $data = array();
        // atdbk_reason_mstから取得する
        $sql = "select * from atdbk_reason_mst  ";
        $reason_str = "";
        for ($reason_idx = 0; $reason_idx < count($arr_reason_disp); $reason_idx++) {
            if ($reason_idx > 0) {
                $reason_str .= ", ";
            }
            $reason_str .= "'$arr_reason_disp[$reason_idx]'";
        }
        if ($data_flg == "1") {
            $data_cond = "";
        } else {
            $data_cond = " reason_id not in ($reason_str) and ";
        }
        $cond = "where $data_cond  display_flag = 't' order by sequence ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);

        for ($i = 0; $i < $num; $i++) {
            $row_data = array();
            $row_data["id"] = pg_fetch_result($sel, $i, "reason_id");
            $tmp_default_name = pg_fetch_result($sel, $i, "default_name");
            $tmp_display_name = pg_fetch_result($sel, $i, "display_name");
            $tmp_name = ($tmp_display_name != "") ? $tmp_display_name : $tmp_default_name;
            $row_data["name"] = $tmp_name;
            $row_data["font_name"] = "／" . $tmp_name;
            $row_data["day_count"] = pg_fetch_result($sel, $i, "day_count");
            $data[$i] = $row_data;
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // カラー情報取得（文字色）
    // @param   なし
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_font_color_array() {
        $data = array(
            array("color" => "#000000", "fcolor" => '#ffffff', "name" => "黒"),
            array("color" => "#ffffff", "fcolor" => '#000000', "name" => "白"),
            array("color" => "#ff0000", "fcolor" => '#000000', "name" => "赤"),
            array("color" => "#0000ff", "fcolor" => '#ffffff', "name" => "青"),
            array("color" => "#ffff00", "fcolor" => '#000000', "name" => "黄"),
            array("color" => "#00ff00", "fcolor" => '#000000', "name" => "ライム"),
            array("color" => "#008000", "fcolor" => '#000000', "name" => "緑"),
            array("color" => "#000080", "fcolor" => '#ffffff', "name" => "紺"), // navy
            array("color" => "#4b0082", "fcolor" => '#ffffff', "name" => "藍"), // indigo
        );

        return $data;
    }

    /*     * ********************************************************************** */

    // カラー情報取得（背景色）
    // @param   なし
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_back_color_array() {
        $data = array(
            array("color" => "#ffffff", "fcolor" => '#000000', "name" => "白"), // white
            array("color" => "#000000", "fcolor" => '#ffffff', "name" => "黒"), // black
            array("color" => "#ff8080", "fcolor" => '#000000', "name" => "淡い赤"),
            array("color" => "#80ffff", "fcolor" => '#000000', "name" => "淡い青"),
            array("color" => "#ffff80", "fcolor" => '#000000', "name" => "淡い黄"),
            array("color" => "#80ff80", "fcolor" => '#000000', "name" => "淡い緑"),
            array("color" => "#ff80ff", "fcolor" => '#000000', "name" => "ピンク"),
            array("color" => "#ff0000", "fcolor" => '#000000', "name" => "赤"), // red
            array("color" => "#0000ff", "fcolor" => '#ffffff', "name" => "青"), // blue
            array("color" => "#ffff00", "fcolor" => '#000000', "name" => "黄"), // yellow
            array("color" => "#008000", "fcolor" => '#000000', "name" => "緑"), // green
            array("color" => "#00ff00", "fcolor" => '#000000', "name" => "ライム"), // lime
            array("color" => "#ffa500", "fcolor" => '#000000', "name" => "オレンジ"), // orange
            array("color" => "#f0e68c", "fcolor" => '#000000', "name" => "カーキ"), // khaki
            array("color" => "#808080", "fcolor" => '#000000', "name" => "灰"), // gray
            array("color" => "#c0c0c0", "fcolor" => '#000000', "name" => "銀"), // silver
            array("color" => "#000080", "fcolor" => '#ffffff', "name" => "紺"), // navy
            array("color" => "#4b0082", "fcolor" => '#ffffff', "name" => "藍"), // indigo
            array("color" => "#800080", "fcolor" => '#ffffff', "name" => "紫"), // purple
            array("color" => "#a52a2a", "fcolor" => '#000000', "name" => "茶"), // brown
            array("color" => "#40e0d0", "fcolor" => '#000000', "name" => "turquoise"), // turquoise
            array("color" => "#32cd32", "fcolor" => '#000000', "name" => "limegreen"), // limegreen
            array("color" => "#ee82ee", "fcolor" => '#000000', "name" => "violet"), // violet
            array("color" => "#8a2be2", "fcolor" => '#000000', "name" => "blueviolet"), // blueviolet
        );

        return $data;
    }

    /*     * ********************************************************************** */

    // カラー情報取得（ハイライト背景色）
    // @param   なし
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_high_color_array() {
        $data = array(
            array("color" => "#ffff00", "fcolor" => '#000000', "name" => "黄"), // yellow
            array("color" => "#ffffe0", "fcolor" => '#000000', "name" => "lightyellow"), // lightyellow
            array("color" => "#808080", "fcolor" => '#000000', "name" => "灰"), // gray
            array("color" => "#d3d3d3", "fcolor" => '#000000', "name" => "lightgrey"),
            array("color" => "#add8e6", "fcolor" => '#000000', "name" => "skyblue"),
            array("color" => "#87cefa", "fcolor" => '#000000', "name" => "lightskyblue"),
            array("color" => "#ff0000", "fcolor" => '#000000', "name" => "赤"), //20141007追加
            array("color" => "#ff8080", "fcolor" => '#000000', "name" => "淡い赤"), //20141007追加
            array("color" => "#fa8072", "fcolor" => '#000000', "name" => "salmon"),
            array("color" => "#ffa07a", "fcolor" => '#000000', "name" => "lightsalmon"),
            array("color" => "#7fffd4", "fcolor" => '#000000', "name" => "aquamarine"),
            array("color" => "#adff2f", "fcolor" => '#000000', "name" => "greenyellow"), // greenyellow
            array("color" => "#ffa500", "fcolor" => '#000000', "name" => "オレンジ"), // orange
            array("color" => "#f0e68c", "fcolor" => '#000000', "name" => "カーキ"), // khaki
        );

        return $data;
    }

    /*     * ********************************************************************** */

    // 届出区分情報取得
    // @param   なし
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_report_kbn_array() {
        $data = array(
            0 => array("id" => REPORT_KBN_BASIC7, "name" => "７対１入院基本料"),
            1 => array("id" => REPORT_KBN_BASIC10, "name" => "１０対１入院基本料"),
            2 => array("id" => REPORT_KBN_BASIC13, "name" => "１３対１入院基本料"),
            3 => array("id" => REPORT_KBN_BASIC15, "name" => "１５対１入院基本料"),
            4 => array("id" => REPORT_KBN_BASIC18, "name" => "１８対１入院基本料"),
            5 => array("id" => REPORT_KBN_BASIC20, "name" => "２０対１入院基本料"), //追加20、IDは既にあるため920とする 20111206
            6 => array("id" => REPORT_KBN_BASIC25, "name" => "２５対１入院基本料"), //追加25、IDは既にあるため925とする 20111206
            7 => array("id" => REPORT_KBN_BASIC30, "name" => "３０対１入院基本料"), //追加30 20111206
            8 => array("id" => REPORT_KBN_CURE_BASIC1, "name" => "療養病棟入院基本料１"), //文言改修 201108202 旧：療養病棟入院基本料２・８割以上 インデックス修正 20111206
            9 => array("id" => REPORT_KBN_CURE_BASIC2, "name" => "療養病棟入院基本料２"), //旧：療養病棟入院基本料２・８割未満 インデックス修正 20111206
            10 => array("id" => REPORT_KBN_SPECIFIC, "name" => "特定入院料"), //追加 20121011
            11 => array("id" => REPORT_KBN_SPECIFIC_PLUS, "name" => "特定入院料（看護職員＋看護補助者）"), //追加 20121011
            12 => array("id" => REPORT_KBN_COMMUNITY_CARE, "name" => "地域包括ケア病棟入院料(13対1)"), //追加 20140324
            13 => array("id" => REPORT_KBN_COMMUNITY_CARE2, "name" => "地域包括ケア病棟入院料(15対1)") //追加 20140324
        );

        return $data;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（権限関連）
//-------------------------------------------------------------------------------------------------------------------------
    /*     * ********************************************************************** */
    // 権限チェック（ユーザ画面）
    // @param   $session, $fname
    // @return  $flg    ＯＫ／ＮＧ
    /*     * ********************************************************************** */
    function check_authority_user($session, $fname) {
        $flg = "1";

        //ユーザ画面用
        $shift_flg = check_authority($session, 69, $fname);
        if ($shift_flg == "0") {
            $flg = "";
        }

        return $flg;
    }

    /*     * ********************************************************************** */

    // 権限チェック（管理画面）
    // @param   $session, $fname
    // @return  $ret    ＯＫ／ＮＧ
    /*     * ********************************************************************** */
    function check_authority_Management($session, $fname) {
        $flg = "1";

        //管理画面用
        $list_flg = check_authority($session, 70, $fname);
        if ($list_flg == "0") {
            $flg = "";
        }

        return $flg;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：行／列　関連）
//-------------------------------------------------------------------------------------------------------------------------
    /*     * ********************************************************************** */
    // 行／列タイトル情報取得
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $gyo_retu_flg       行／列フラグ（1:行、2:列）
    // @param   $data_pattern       勤務シフトパターン情報
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_total_title_array($pattern_id, $gyo_retu_flg, $data_pattern) {
        $data = array();

        // 集計方法変更ならCustomメソッドを返して終了
        $change_switch = $this->GetSumCustomSwith($pattern_id);
        if ($change_switch == 1) {
            return $this->get_total_title_array_Custom($pattern_id, $gyo_retu_flg, $data_pattern);
        } else if ($change_switch == 2) {
            return $this->get_total_title_array_Custom2($pattern_id, $gyo_retu_flg);
        }
        //-------------------------------------------------------------------
        //初期設定
        //-------------------------------------------------------------------
        //行の場合
        if ($gyo_retu_flg == "1") {
            $wk_id = "count_kbn_gyo";
            $wk_name = "count_kbn_gyo_name";
        }
        //列の場合
        if ($gyo_retu_flg == "2") {
            $wk_id = "count_kbn_retu";
            $wk_name = "count_kbn_retu_name";
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
//      $data = array();
        $m = 0;
        $holiday_id = "";
        $holiday_name = "";
        $other_id = "";
        $other_name = "";
        for ($i = 0; $i < count($data_pattern); $i++) {
            //区分が空白は除く。非表示設定項目
            if ($data_pattern[$i][$wk_id] == "") {
                continue;
            }
            //-------------------------------------------------------------------
            //すでに存在するかチェック
            //-------------------------------------------------------------------
            $wk_flg = "";
            for ($k = 0; $k < count($data); $k++) {
                if ($data_pattern[$i][$wk_id] == $data[$k]["id"]) {
                    $wk_flg = "1";
                    break;
                }
            }
//echo("<br>wk_id=".$data_pattern[$i][$wk_id]); // debug
            //-------------------------------------------------------------------
            //存在しない場合、追加
            //-------------------------------------------------------------------
            if ($wk_flg == "") {
                //-------------------------------------------------------------------
                //追加
                //-------------------------------------------------------------------
                $wk_ng_flg = "";
                //休暇を名称ではなくIDで確認
                if ($data_pattern[$i]["atdptn_ptn_id"] == "10") {
                    $holiday_id = $data_pattern[$i]["atdptn_ptn_id"];
                    //名称設定は一回だけ
                    if ($data_pattern[$i]["reason"] == "34") {
                        $holiday_name = $data_pattern[$i][$wk_name];
                    }
                    $wk_ng_flg = "1";
                }
                //その他か
                if ($data_pattern[$i][$wk_id] == "9999") {
                    $other_id = $data_pattern[$i][$wk_id];
                    $other_name = $data_pattern[$i][$wk_name];
                    $wk_ng_flg = "1";
                }
                //休暇、その他以外は追加
                if ($wk_ng_flg == "") {
                    //集計指定が休暇の場合は除く 2008/11/18
                    if ($data_pattern[$i][$wk_id] != "10") {
                        $data[$m]["id"] = $data_pattern[$i][$wk_id];
                        $data[$m]["name"] = $data_pattern[$i][$wk_name];
                        $m++;
                    }
                }
            }
        }
        //その他を追加
        if ($other_id != "") {
            $data[$m]["id"] = $other_id;
            $data[$m]["name"] = "その他";
            $m++;
        }
        //休暇を追加
        if ($holiday_id != "") {
            $data[$m]["id"] = $holiday_id;
            $data[$m]["name"] = $holiday_name;
            $m++;
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 行／列タイトル情報取得、「集計設定B」課題対応 2012.2.1
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $gyo_retu_flg       行／列フラグ（1:行、2:列）
    // @param   $data_pattern       勤務シフトパターン情報
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_total_title_array_Custom($pattern_id, $gyo_retu_flg, $data_pattern) {

        $customName = array();
        // DB読み込みは縦横で1回だけにしたいがロジック的に面倒
        $customName = $this->custom_table_title_name($pattern_id); // 集計方法変更の名称、この項目だけで集計する

        $data = array();

        for ($k = 0; $k < count($customName); $k++) {
            $data[$k]["id"] = 0; // 既存機能との互換用にダミー。このidに意味は無いが休暇集計及び年休公休の集計の項目を表示させないため0とする
            $data[$k]["name"] = $customName[$k];
        }
        $data[0]["change_switch"] = "1";  // 行集計の右端に休暇集計及び年休公休の集計の項目を表示しない分、<TABLE><TR><TD>のcolspanを縮めるスイッチ
        // duty_shift_menu_common_class.php 判定を追加
        return $data;
    }

    /*     * ********************************************************************** */

    // 行／列タイトル情報取得、「集計設定A」
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $gyo_retu_flg       行／列フラグ（1:行、2:列）
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_total_title_array_Custom2($pattern_id, $gyo_retu_flg) {

        // DB読み込みは縦横で1回だけにしたいがロジック的に面倒
        $names = $this->count_names($pattern_id); // 集計方法変更の名称、この項目だけで集計する

        $data = array();
        foreach ($names as $r) {
            // 行項目ではない
            if ($gyo_retu_flg === '1' and $r['count_row_flg'] === 'f') {
                continue;
            }

            // 列項目ではない
            if ($gyo_retu_flg === '2' and $r['count_col_flg'] === 'f') {
                continue;
            }

            $data[] = array(
                "id" => 0, // 既存機能との互換用にダミー。このidに意味は無いが休暇集計及び年休公休の集計の項目を表示させないため0とする
                "name" => $r['count_name'],
                "count_id" => $r['count_id'],
                "job_filter" => $r['jobs'],
            );
        }

        // 行集計の右端に休暇集計及び年休公休の集計の項目を表示しない分、<TABLE><TR><TD>のcolspanを縮めるスイッチ
        $data[0]["change_switch"] = "1";

        return $data;
    }

    /*     * ********************************************************************** */

    // 休暇事由タイトル情報取得
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $data_pattern       勤務シフトパターン情報
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */

    function get_total_title_hol_array($pattern_id, $data_pattern) {
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();

        $change_switch = $this->GetSumCustomSwith($pattern_id);
        if ($change_switch != 0) { // このスイッチがオンのときは処理しない 2012.1.27
            return $array;
        }

        $m = 0;
        $yuukyuu_flg = false;
        $yuukyuu_idx = 0;
        for ($i = 0; $i < count($data_pattern); $i++) {

            //非表示、事由ありの休暇以外は除く。
            if ($data_pattern[$i]["reason_display_flag"] == "f" ||
                    $data_pattern[$i]["atdptn_ptn_id"] != "10") {
                continue;
            }
            //半有半公等を除く
            if ($data_pattern[$i]["reason"] >= "44" &&
                    $data_pattern[$i]["reason"] <= "47") {
                continue;
            }
            //半有半欠等を除く(半夏半公54) 20110808
            if (($data_pattern[$i]["reason"] >= "54" &&
                    $data_pattern[$i]["reason"] <= "60") ||
                    ($data_pattern[$i]["reason"] >= "62" &&
                    $data_pattern[$i]["reason"] <= "64") ||
                    $data_pattern[$i]["reason"] == "72") {
                continue;
            }
            //有休年休、コードは有休の1、名称は表示フラグにより編集
            if ($data_pattern[$i]["reason"] == "1" ||
                    $data_pattern[$i]["reason"] == "37") {
                if ($yuukyuu_flg == false) {
                    $data[$m]["reason"] = "1";
//                  $data[$m]["name"] = $data_pattern[$i]["reason_name"];
                    $data[$m]["name"] = "年休有休";
                    $yuukyuu_idx = $m;
                    $yuukyuu_flg = true;
                    $m++;
//              } else {
//                  $data[$yuukyuu_idx]["name"] = $data[$yuukyuu_idx]["name"].$data_pattern[$i]["reason_name"];
                }
            } elseif ($data_pattern[$i]["reason"] == "34") {
                $data[$m]["reason"] = $data_pattern[$i]["reason"];
//              $data[$m]["name"] = $data_pattern[$i]["reason_name"];
                $data[$m]["name"] = "休暇";
                $m++;
            } else {
                $data[$m]["reason"] = $data_pattern[$i]["reason"];
                $data[$m]["name"] = $data_pattern[$i]["reason_name"];
                $m++;
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 行／列タイトル合計情報取得
    // ・事由に午前有休、午後有休、半前代替休、半後代替休、半前振替休、半後振替休を選択した場合には、
    //   合計行、合計列に対して、選択した勤務シフトパターンに+0.5、休暇に対して+0.5する。
    // ・午前有休、午後有休は、休暇（有給）に+0.5、半前代替休、半後代替休は、
    //   休暇（代替）に+0.5、半前振替休、半後振替休は、休暇（振替）に+0.5する。
    //
    // @param   $group_id           勤務シフトグループＩＤ
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $gyo_retu_flg       行／列フラグ（1:行、2:列）
    // @param   $rslt_flg           実績フラグ（１：実績、２：退勤時刻の打刻なしは集計しない）20100112 変更
    // @param   $day_cnt            日数
    // @param   $plan_array         勤務シフト情報
    // @param   $title_gyo_array    行タイトル情報
    // @param   $title_retu_array   列タイトル情報
    // @param   $calendar_array     カレンダー情報
    // @param   $data_atdptn        出勤パターン情報取得
    //
    // @return  $ret            取得情報
    /*     * ********************************************************************** */
    function get_total_cnt_array($group_id, $pattern_id, $gyo_retu_flg, $rslt_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn) {

        $ret = array();
        $gyo_array = array();
        $retu_array = array();
        $holiday_array = array();
        $holiday_idx = "";
        $today = date("Ymd");

        // 集計方法変更ならCustomメソッドを返して終了
        $change_switch = $this->GetSumCustomSwith($pattern_id);
        if ($change_switch == 1) {
            return $this->get_total_cnt_array_Custom($group_id, $pattern_id, $gyo_retu_flg, $rslt_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
        } else if ($change_switch == 2) {
            return $this->get_total_cnt_array_Custom2($group_id, $pattern_id, $gyo_retu_flg, $rslt_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array);
        }

        // 集計見直し 2008/11/10
        // 半休チェック、 事由が半休の場合、0.5ずつ加算
        //"2"午前有休 "3"午後有休 "18"半前代替休 "19"半後代替休
        //"20"半前振替休 "21"半後振替休 "35"午前公休 "36"午後公休
        //"38"午前年休 "39"午後年休 "42"午前リフレッシュ "43"午後リフレッシュ
        //"48"午前欠勤 "49"午後欠勤 "50"午前夏休 "51"午後夏休 "52"午前その他 "53"午後その他
//      $half_chk_array = array("2", "3", "18", "19", "20", "21", "35", "36", "38", "39", "42", "43", "48", "49", "50", "51", "52", "53");

        $add_info = $this->get_hol_add_info();

        //換算日数情報
        $arr_workday_count = array();
        for ($i = 0; $i < count($data_atdptn); $i++) {
            $wk_id = $data_atdptn[$i]["id"];
            $arr_workday_count[$wk_id] = $data_atdptn[$i]["workday_count"];
        }

        /*         * ***************************************************************** */
        //個人日数合計（行）を算出設定
        /*         * ***************************************************************** */
        if ($gyo_retu_flg == "1") {
            for ($k = 0; $k < count($title_gyo_array); $k++) {
//echo("<br>gyo=".$title_gyo_array[$k]["id"]); // debug
                //配列の1段目の後ろに2段目の合計を設定
                $k2 = count($title_gyo_array) + $k;
                //-------------------------------------------------------------------
                //行タイトル単位で算出設定
                //-------------------------------------------------------------------
                for ($i = 0; $i < count($plan_array); $i++) {
                    //-------------------------------------------------------------------
                    //初期値設定
                    //-------------------------------------------------------------------
                    $gyo_array[$i][$k] = 0.0;
                    $gyo_array[$i][$k2] = 0.0;
                    $holiday_array[$i][$k]["cnt"] = 0.0;
                    $holiday_array[$i][$k2]["cnt"] = 0.0;
                    //-------------------------------------------------------------------
                    //日数分を加算設定
                    //-------------------------------------------------------------------
                    for ($m = 1; $m <= $day_cnt; $m++) {
                        //-------------------------------------------------------------------
                        //加算行判定
                        //-------------------------------------------------------------------
//                      if ($rslt_flg == "1") {
//                          $wk_gyo = "rslt_count_kbn_gyo_" . $m;
//                          $wk_pattern_id = "rslt_pattern_id_" . $m;
//                          $wk_atdptn_ptn_id = "rslt_id_" . $m;
//                          $wk_reason_2 = "rslt_reason_2_" . $m;
//                      } else {
                        $wk_gyo = "count_kbn_gyo_" . $m;
                        $wk_pattern_id = "pattern_id_" . $m;
                        $wk_atdptn_ptn_id = "atdptn_ptn_id_" . $m;
                        $wk_reason_2 = "reason_2_" . $m;
//                      }
                        //応援追加グループ
                        $wk_assist_group = "assist_group_" . $m;
                        $wk_assist_flg = "";
                        if ($plan_array[$i][$wk_assist_group] != "" &&
                                $plan_array[$i][$wk_assist_group] != $group_id) {
                            $wk_assist_flg = "1";
                        }
//echo("<br> wk_assist_group=".$plan_array[$i][$wk_assist_group]);
//echo(" group_id=".$group_id);
                        //-------------------------------------------------------------------
                        //加算
                        //-------------------------------------------------------------------
                        //応援追加グループ確認条件追加
                        if (/* $plan_array[$i][$wk_pattern_id] == $pattern_id && */ $wk_assist_flg == "") {
                            if ($plan_array[$i][$wk_gyo] == $title_gyo_array[$k]["id"]) {
                                // 勤務パターンが休暇以外
                                if ($plan_array[$i][$wk_atdptn_ptn_id] != "10") {
//$wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
//echo(" wk_reason_2=".$plan_array[$i][$wk_reason_2]);
                                    $wk_reason_id = $plan_array[$i][$wk_reason_2];
                                    //換算日数が0.5で事由が未設定の場合。半日振出
                                    $wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
                                    if ($arr_workday_count[$wk_id] == 0.5 &&
                                            ($plan_array[$i][$wk_reason_2] == "" ||
                                            $plan_array[$i][$wk_reason_2] == "65" ||
                                            $plan_array[$i][$wk_reason_2] == "66")) {
                                        $gyo_array[$i][$k] += 0.5;
                                        $holiday_array[$i][$k]["cnt"] += 0.5;
                                    } else
                                    // 事由が半休の場合、0.5ずつ加算
//                          if (in_array($plan_array[$i][$wk_reason_2], $half_chk_array)) {
                                    if ($add_info[$wk_reason_id]["add_day"] == 0.5) {
                                        $gyo_array[$i][$k] += 0.5;
//echo(" $wk_reason_id".$add_info[$wk_reason_id]["add_day"]);
                                        //休暇を名称ではなくIDで確認
                                        if ($title_gyo_array[$k]["id"] != "10") {
                                            $holiday_array[$i][$k]["cnt"] += 0.5;
                                        }
//                          } else
//                          if ($plan_array[$i][$wk_reason_2] == "65" ||
//                              $plan_array[$i][$wk_reason_2] == "66") {
                                        //午前振出、午後振出
//                              $gyo_array[$i][$k] += 0.5;
                                    } else {
                                        // 事由が半休以外の場合、その行に+1
                                        $gyo_array[$i][$k] += 1;
                                    }
                                } else {
                                    // 勤務パターンが休暇の場合
                                    // 休暇に+1
                                    $holiday_array[$i][$k]["cnt"] += 1;
//echo("<br> gyo=".$plan_array[$i][$wk_gyo]); // debug
                                }
                            }
                        }
                        // 前日のレコードの翌日フラグを確認 20100622
                        $wk_prev_idx = $m - 1;
                        $wk_prev_next_day_flg = $plan_array[$i]["next_day_flag_$wk_prev_idx"];
                        //集計行２段対応、$gyo_array[$i]配列の1段目の後ろに2段目の合計を設定
                        //シフト作成画面で2段目が実績の場合、システム日以降集計しない ※ 条件中止 20090703
                        //if ($rslt_flg != "2" || ($rslt_flg == "2" && $today >= $calendar_array[$m-1]["date"])) {
                        //勤務実績は退勤時刻も打刻済の場合に表示、休日は除く 20100112
                        $wk_atdptn_ptn_id = "rslt_id_" . $m;
                        $wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
                        if ($rslt_flg != "2" ||
                                $plan_array[$i]["rslt_id_$m"] == "10" ||
                                $arr_workday_count[$wk_id] == 0 || //換算日数0の場合
                                $wk_prev_next_day_flg == 1 || // 前日のレコードの翌日フラグを確認 20100622
                                ($rslt_flg == "2" &&
                                $plan_array[$i]["rslt_start_time_$m"] != "" /* && 20100820
                                  $plan_array[$i]["rslt_end_time_$m"] != "" */ )) {
                            $wk_gyo = "rslt_count_kbn_gyo_" . $m;
                            $wk_pattern_id = "rslt_pattern_id_" . $m;
                            $wk_reason_2 = "rslt_reason_2_" . $m;
                            //応援追加グループ
                            $wk_assist_group = "assist_group_" . $m;
                            //希望の応援追加分
                            $wk_rslt_assist_group = "rslt_assist_group_" . $m;
                            $wk_assist_flg = "";
                            if (($plan_array[$i][$wk_rslt_assist_group] != "" &&
                                    $plan_array[$i][$wk_rslt_assist_group] != $group_id) ||
                                    ($plan_array[$i][$wk_rslt_assist_group] == "" &&
                                    $plan_array[$i][$wk_assist_group] != "" &&
                                    $plan_array[$i][$wk_assist_group] != $group_id)) {
                                $wk_assist_flg = "1";
                            }
                            if (/* $plan_array[$i][$wk_pattern_id] == $pattern_id && */ $wk_assist_flg == "") {
                                if ($plan_array[$i][$wk_gyo] == $title_gyo_array[$k]["id"]) {
                                    // 勤務パターンが休暇以外
                                    if ($plan_array[$i][$wk_atdptn_ptn_id] != "10") {
                                        $wk_reason_id = $plan_array[$i][$wk_reason_2];
                                        //換算日数が0.5で事由が未設定の場合。半日振出
                                        $wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
                                        if ($arr_workday_count[$wk_id] == 0.5 &&
                                                ($plan_array[$i][$wk_reason_2] == "" ||
                                                $plan_array[$i][$wk_reason_2] == "65" ||
                                                $plan_array[$i][$wk_reason_2] == "66")) {
                                            $gyo_array[$i][$k2] += 0.5;
                                            $holiday_array[$i][$k2]["cnt"] += 0.5;
                                        } else
                                        // 事由が半休の場合、0.5ずつ加算
                                        //                          if (in_array($plan_array[$i][$wk_reason_2], $half_chk_array)) {
                                        if ($add_info[$wk_reason_id]["add_day"] == 0.5) {
                                            $gyo_array[$i][$k2] += 0.5;
                                            //休暇を名称ではなくIDで確認
                                            if ($title_gyo_array[$k]["id"] != "10") {
                                                $holiday_array[$i][$k2]["cnt"] += 0.5;
                                            }
                                        } else {
                                            // 事由が半休以外の場合、その行に+1
                                            $gyo_array[$i][$k2] += 1;
                                        }
                                    } else {
                                        // 勤務パターンが休暇の場合
                                        // 休暇に+1
                                        $holiday_array[$i][$k2]["cnt"] += 1;
                                        //echo("<br> gyo=".$plan_array[$i][$wk_gyo]); // debug
                                    }
                                }
                            }
                        } // 20100112 追加
                    }
                }
                //-------------------------------------------------------------------
                //休暇のインデックス取得
                //-------------------------------------------------------------------
                //休暇を名称ではなくIDで確認
                if ($title_gyo_array[$k]["id"] == "10") {
                    $holiday_idx = $k;
                }
            }
            //-------------------------------------------------------------------
            //休暇の場合の再設定
            //-------------------------------------------------------------------
            if ($holiday_idx != "") {
                $holiday_idx2 = count($title_gyo_array) + $holiday_idx;
                for ($k = 0; $k < count($title_gyo_array); $k++) {
                    $k2 = count($title_gyo_array) + $k;
                    for ($i = 0; $i < count($plan_array); $i++) {
                        $gyo_array[$i][$holiday_idx] += $holiday_array[$i][$k]["cnt"];
                        $gyo_array[$i][$holiday_idx2] += $holiday_array[$i][$k2]["cnt"];
                    }
                }
            }
            //-------------------------------------------------------------------
            //データ設定
            //-------------------------------------------------------------------
            $ret = $gyo_array;
        }

        /*         * ***************************************************************** */
        //日数合計（列）を算出設定
        /*         * ***************************************************************** */
        if ($gyo_retu_flg == "2") {
            //-------------------------------------------------------------------
            //列タイトル単位で算出設定
            //-------------------------------------------------------------------
            for ($k = 0; $k < count($title_retu_array); $k++) {
                for ($m = 1; $m <= $day_cnt; $m++) {
                    //-------------------------------------------------------------------
                    //初期値設定
                    //-------------------------------------------------------------------
                    $retu_array[$m][$k] = 0.0;
                    $holiday_array[$m][$k]["cnt"] = 0.0;
                    //-------------------------------------------------------------------
                    //スタッフ数分を加算設定
                    //-------------------------------------------------------------------
                    for ($i = 0; $i < count($plan_array); $i++) {
                        //-------------------------------------------------------------------
                        //加算行判定
                        //-------------------------------------------------------------------
                        if ($rslt_flg >= "1") {
                            $wk_retu = "rslt_count_kbn_retu_" . $m;
                            $wk_pattern_id = "rslt_pattern_id_" . $m;
                            $wk_atdptn_ptn_id = "rslt_id_" . $m;
                            $wk_reason_2 = "rslt_reason_2_" . $m;
                        } else {
                            $wk_retu = "count_kbn_retu_" . $m;
                            $wk_pattern_id = "pattern_id_" . $m;
                            $wk_atdptn_ptn_id = "atdptn_ptn_id_" . $m;
                            $wk_reason_2 = "reason_2_" . $m;
                        }
                        //応援追加グループ
                        $wk_assist_group = "assist_group_" . $m;
                        $wk_assist_flg = "";
                        if (($plan_array[$i][$wk_assist_group] != "" &&
                                $plan_array[$i][$wk_assist_group] != $group_id)) {
                            $wk_assist_flg = "1";
                        }

                        // 前日のレコードの翌日フラグを確認 20100622
                        $wk_prev_idx = $m - 1;
                        $wk_prev_next_day_flg = $plan_array[$i]["next_day_flag_$wk_prev_idx"];
                        //勤務実績は退勤時刻も打刻済の場合に表示、休日は除く 20100112
                        //$wk_atdptn_ptn_id = "rslt_id_" . $m;
                        $wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
                        if ($rslt_flg != "2" ||
                                $plan_array[$i]["rslt_id_$m"] == "10" ||
                                $arr_workday_count[$wk_id] == 0 || //換算日数0の場合
                                $wk_prev_next_day_flg == 1 || // 前日のレコードの翌日フラグを確認 20100622
                                ($rslt_flg == "2" &&
                                $plan_array[$i]["rslt_start_time_$m"] != "" /* && 20100820
                                  $plan_array[$i]["rslt_end_time_$m"] != "" */)) {
                            //-------------------------------------------------------------------
                            //加算
                            //-------------------------------------------------------------------
                            //応援追加グループ確認条件追加
                            if (/* $plan_array[$i][$wk_pattern_id] == $pattern_id && */ $wk_assist_flg == "") {
                                if ($plan_array[$i][$wk_retu] == $title_retu_array[$k]["id"]) {
                                    // 勤務パターンが休暇以外
                                    if ($plan_array[$i][$wk_atdptn_ptn_id] != "10") {
                                        $wk_reason_id = $plan_array[$i][$wk_reason_2];
                                        //換算日数が0.5で事由が未設定の場合
                                        $wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
                                        if ($arr_workday_count[$wk_id] == 0.5 &&
                                                ($plan_array[$i][$wk_reason_2] == "" ||
                                                $plan_array[$i][$wk_reason_2] == "65" ||
                                                $plan_array[$i][$wk_reason_2] == "66")) {
                                            $retu_array[$m][$k] += 0.5;
                                            $holiday_array[$m][$k]["cnt"] += 0.5;
                                        } else
                                        // 事由が半休の場合、0.5ずつ加算
                                        //                          if (in_array($plan_array[$i][$wk_reason_2], $half_chk_array)) {
                                        if ($add_info[$wk_reason_id]["add_day"] == 0.5) {
                                            $retu_array[$m][$k] += 0.5;
                                            //休暇を名称ではなくIDで確認
                                            if ($title_retu_array[$k]["id"] != "10") {
                                                $holiday_array[$m][$k]["cnt"] += 0.5;
                                            }
                                            //                          } else
                                            //                          if ($plan_array[$i][$wk_reason_2] == "65" ||
                                            //                              $plan_array[$i][$wk_reason_2] == "66") {
                                            //午前振出、午後振出
                                            //                              $retu_array[$m][$k] += 0.5;
                                        } else {
                                            // 事由が半休以外の場合、その行に+1
                                            $retu_array[$m][$k] += 1;
                                        }
                                    } else {
                                        // 勤務パターンが休暇の場合
                                        // 休暇に+1
                                        $holiday_array[$m][$k]["cnt"] += 1;
                                    }
                                }
                            }
                        } // 20100112 追加
                    }
                }
                //-------------------------------------------------------------------
                //休暇のインデックス取得
                //-------------------------------------------------------------------
                //休暇を名称ではなくIDで確認
                if ($title_retu_array[$k]["id"] == "10") {
                    $holiday_idx = $k;
                }
            }
            //-------------------------------------------------------------------
            //休暇の場合の再設定
            //-------------------------------------------------------------------
            if ($holiday_idx != "") {
                for ($k = 0; $k < count($title_retu_array); $k++) {
                    for ($m = 1; $m <= $day_cnt; $m++) {
                        $retu_array[$m][$holiday_idx] += $holiday_array[$m][$k]["cnt"];
                    }
                }
            }
            //-------------------------------------------------------------------
            //データ設定
            //-------------------------------------------------------------------
            $ret = $retu_array;
        }
        return $ret;
    }

    /*     * ********************************************************************** */

    // 行／列タイトル合計情報取得 、「集計設定B」課題対応 2012.1.27
    // ・事由、休暇に関わらず、「勤務表集計設定」画面にて設定した値で集計する。
    // private function 外部呼出し禁止 ※2012.02.10PHP4ではprivate指定できないのでpublicとする
    // @param   $group_id       勤務シフトグループＩＤ
    // @param   $pattern_id     出勤グループＩＤ
    // @param   $gyo_retu_flg       行／列フラグ（1:行、2:列）
    // @param   $rslt_flg       実績フラグ（１：実績、２：退勤時刻の打刻なしは集計しない）20100112 変更
    // @param   $day_cnt        日数
    // @param   $plan_array     勤務シフト情報
    // @param   $title_gyo_array    行タイトル情報
    // @param   $title_retu_array   列タイトル情報
    // @param   $calendar_array     カレンダー情報
    // @param   $data_atdptn        出勤パターン情報取得
    //
    // @return  $ret            取得情報
    /*     * ********************************************************************** */
    function get_total_cnt_array_Custom($group_id, $pattern_id, $gyo_retu_flg, $rslt_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn) {
        $custumValue_array = array();

        // 縦横でDBアクセス2回しないようにしたいがロジック的に無理
        $custumValue_array = $this->custom_table_read($pattern_id); // 変更情報取得

        $ret = array();
        $gyo_array = array();
        $retu_array = array();

        /*         * ***************************************************************** */
        //個人日数合計（行）を算出設定
        /*         * ***************************************************************** */
        if ($gyo_retu_flg == "1") {
            $wk_titlecount = count($title_gyo_array);
            $wk_plancount = count($plan_array);
            for ($i = 0; $i < $wk_plancount; $i++) { // 職員毎。行データ
                for ($k = 0; $k < $wk_titlecount; $k++) { // 集計項目初期化
                    $gyo_array[$i][$k] = 0.0;
                    $k2 = $k + $wk_titlecount;
                    $gyo_array[$i][$k2] = 0.0; // 2段目
                }
                for ($m = 1; $m <= $day_cnt; $m++) { // 日毎
                    $titleName = "";
                    $wk_gyo = "count_kbn_gyo_" . $m;
                    $wk_gyo2 = "rslt_count_kbn_gyo_" . $m; // 2012.5.9 不具合11対応
                    $plan_id = $plan_array[$i][$wk_gyo];
                    $plan_id2 = $plan_array[$i][$wk_gyo2]; // 2012.5.9 不具合11対応
                    $plan_array_font_name = $plan_array[$i]["font_name_$m"];
                    $rslt_array_font_name = $plan_array[$i]["rslt_name_$m"]; // 2012.5.9 不具合11対応
                    // 設定の無いまたは休暇の実績$plan_array_font_nameには &nbsp が設定されている
                    $wk_reason_id = 0;
                    if ($plan_id == 10) {
//                      $wk_reason_2 = "reason_2_" . $m;// reason情報(休暇詳細) 他functionはreason_2_なのにここでは値が来ない
                        $wk_reason = "reason_" . $m;    // reason情報(休暇詳細)
                        $wk_reason_id = $plan_array[$i][$wk_reason]; // plan_arrayのreason-nameを取り出す準備
                    }
                    $wk_reason_id2 = 0;
                    // 実績データの有無を判定
                    $result_switch = 0;
                    if ($plan_id2 == 10 && $rslt_flg >= '1') { // 休暇の実績
                        $result_switch = 1;
                    }
                    if ($plan_id2 != 10 && $rslt_flg >= '1' && $plan_array[$i]["rslt_start_time_$m"] != "") { // 休暇以外の出勤実績
                        $result_switch = 1;
                    }
                    // 2012.5.9 不具合11対応
                    // 休暇の実績、または、勤務実績（タイムカードの開始時刻がある）
                    if ($plan_id2 == 10 && $result_switch == 1) { // 実績データがあるとき
                        $wk_reason2 = "rslt_reason_" . $m;  // reason情報(休暇詳細)
                        $wk_reason_id2 = $plan_array[$i][$wk_reason2]; // plan_arrayのreason-nameを取り出す準備
                    }
                    for ($k = 0; $k < $wk_titlecount; $k++) { // 集計項目毎
                        // 加算累計。集計項目毎に値があるか見ながら加算
                        $k2 = $wk_titlecount + $k; // 2段目の位置
                        $titleName = $title_gyo_array[$k]['name'];
                        $addValue = $custumValue_array[$plan_array_font_name][$titleName][$wk_reason_id]; // plan_idとtitleName(変更名)とreason_idをキーとして変更した加算値を取り出す
                        $gyo_array[$i][$k]+=$addValue;
                        // 2012.5.9 不具合11対応
//                      if ( $rslt_flg >= '1' && $plan_array[$i]["rslt_start_time_$m"] != "" ){ // 実績があるときは2段目も集計
                        if ($result_switch == 1) { // 実績データがあるとき
                            $addValue = $custumValue_array[$rslt_array_font_name][$titleName][$wk_reason_id2]; // plan_idとtitleName(変更名)とreason_idをキーとして変更した加算値を取り出す
                            $gyo_array[$i][$k2]+=$addValue;
                        }
                    }
                }
            }
            $ret = $gyo_array;
        }
        /*         * ***************************************************************** */
        //日数合計（列）を算出設定
        /*         * ***************************************************************** */
        if ($gyo_retu_flg == "2") {
            for ($m = 1; $m <= $day_cnt; $m++) { // 日毎
                for ($k = 0; $k < count($title_retu_array); $k++) { // 集計項目初期化
                    $retu_array[$m][$k] = 0.0;
                }
                for ($i = 0; $i < count($plan_array); $i++) { // 職員毎。行データ
                    $titleName = "";
                    if ($rslt_flg >= '1') { // 2012.5.9 不具合11対応
                        $wk_retu = "rslt_count_kbn_retu_" . $m;
                        $plan_array_font_name = $plan_array[$i]["rslt_name_$m"];
                    } else {
                        $wk_retu = "count_kbn_retu_" . $m;
                        $plan_array_font_name = $plan_array[$i]["font_name_$m"];
                    }
                    $plan_id = $plan_array[$i][$wk_retu];
                    $wk_reason_id = 0;
                    if ($plan_id == 10) {
                        // reason情報(休暇詳細)
                        if ($rslt_flg >= '1') { // 2012.5.9 不具合11対応
                            $wk_reason = "rslt_reason_" . $m;
                        } else {
                            $wk_reason = "reason_" . $m;
                        }
                        $wk_reason_id = $plan_array[$i][$wk_reason]; // plan_arrayのreason-nameを取り出す準備
                    }
                    for ($k = 0; $k < count($title_retu_array); $k++) { // 集計項目毎
                        $titleName = $title_retu_array[$k]['name'];
                        $retu_array[$m][$k] += $custumValue_array[$plan_array_font_name][$titleName][$wk_reason_id]; // plan_idと変更名とreason_idをキーとして変更した加算値を取り出す
                    }
                }
            }
            //-------------------------------------------------------------------
            //データ設定
            //-------------------------------------------------------------------
            $ret = $retu_array;
        }
        return $ret;
    }

    /*     * ********************************************************************** */

    // 行／列タイトル合計情報取得 、「集計設定A」
    // ・事由、休暇に関わらず、「勤務表集計設定」画面にて設定した値で集計する。
    // private function 外部呼出し禁止 ※2012.02.10PHP4ではprivate指定できないのでpublicとする
    // @param   $group_id       勤務グループＩＤ
    // @param   $pattern_id     出勤グループＩＤ
    // @param   $gyo_retu_flg       行／列フラグ（1:行、2:列）
    // @param   $rslt_flg       実績フラグ（１：実績、２：退勤時刻の打刻なしは集計しない）20100112 変更
    // @param   $day_cnt        日数
    // @param   $plan_array     勤務シフト情報
    // @param   $title_gyo_array    行タイトル情報
    // @param   $title_retu_array   列タイトル情報
    //
    // @return  $ret            取得情報
    /*     * ********************************************************************** */
    function get_total_cnt_array_Custom2(
    $group_id, $pattern_id, $gyo_retu_flg, $rslt_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array) {

        // count_valueテーブル
        $cvalues = $this->count_values($pattern_id);

        $gyo_array = array();
        $retu_array = array();

        /*         * ***************************************************************** */
        //個人日数合計（行）を算出設定
        /*         * ***************************************************************** */
        if ($gyo_retu_flg == "1") {
            $wk_titlecount = count($title_gyo_array);
            $wk_plancount = count($plan_array);

            for ($i = 0; $i < $wk_plancount; $i++) { // 職員毎。行データ
                // 集計項目初期化
                for ($k = 0; $k < $wk_titlecount; $k++) {
                    $gyo_array[$i][$k] = 0;
                    $gyo_array[$i][$k + $wk_titlecount] = 0; // 2段目
                }
                // 日毎
                for ($day = 1; $day <= $day_cnt; $day++) {
                    // 予定パターン
                    if ($plan_array[$i]["atdptn_ptn_id_$day"] == '10') {
                        $plan_pattern = sprintf('10%02d', $plan_array[$i]["reason_$day"]);
                    } else {
                        $plan_pattern = sprintf('%02d00', $plan_array[$i]["atdptn_ptn_id_$day"]);
                    }

                    // 実績パターン
                    if ($plan_array[$i]["rslt_id_$day"] == '10') {
                        $rslt_pattern = sprintf('10%02d', $plan_array[$i]["rslt_reason_$day"]);
                    } else {
                        $rslt_pattern = sprintf('%02d00', $plan_array[$i]["rslt_id_$day"]);
                    }

                    // 集計項目毎
                    foreach ($title_gyo_array as $k => $name) {
                        // 応援職員の移動前の勤務はノーカウントにする
                        // 勤務パターングループのIDが異なる勤務は集計しない
                        if ($pattern_id === $plan_array[$i]["pattern_id_$day"]) {
                            $gyo_array[$i][$k] += (float) $cvalues[$plan_pattern][$name['count_id']];
                        }
                        if ($pattern_id === $plan_array[$i]["rslt_pattern_id_$day"]) {
                            $gyo_array[$i][$k + $wk_titlecount] += (float) $cvalues[$rslt_pattern][$name['count_id']];
                        }
                    }
                }
            }
            return $gyo_array;
        }
        /*         * ***************************************************************** */
        //日数合計（列）を算出設定
        /*         * ***************************************************************** */
        if ($gyo_retu_flg == "2") {
            // 日毎
            for ($day = 1; $day <= $day_cnt; $day++) {
                // 集計項目初期化
                for ($k = 0; $k < count($title_retu_array); $k++) {
                    $retu_array[$day][$k] = 0;
                }

                // 職員毎。行データ
                for ($i = 0; $i < count($plan_array); $i++) {
                    $job_id = $plan_array[$i]["job_id"];

                    if ($rslt_flg >= '1') { // 2012.5.9 不具合11対応
                        // 実績パターン
                        if ($plan_array[$i]["rslt_id_$day"] == '10') {
                            $pattern = sprintf('10%02d', $plan_array[$i]["rslt_reason_$day"]);
                        } else {
                            $pattern = sprintf('%02d00', $plan_array[$i]["rslt_id_$day"]);
                        }
                        $assist_group = $plan_array[$i]["assist_group_$day"];
                    } else {
                        // 予定パターン
                        if ($plan_array[$i]["atdptn_ptn_id_$day"] == '10') {
                            $pattern = sprintf('10%02d', $plan_array[$i]["reason_$day"]);
                        } else {
                            $pattern = sprintf('%02d00', $plan_array[$i]["atdptn_ptn_id_$day"]);
                        }
                        $assist_group = $plan_array[$i]["assist_group_$day"];
                    }

                    // 集計項目毎
                    foreach ($title_retu_array as $k => $name) {
                        // 職種フィルタ
                        if (empty($name['job_filter']) or in_array($job_id, $name['job_filter'])) {
                            // 応援職員の移動前の勤務はノーカウントにする
                            // 勤務グループのIDが異なる勤務は集計しない
                            if ($group_id === $assist_group) {
                                $retu_array[$day][$k] += (float) $cvalues[$pattern][$name['count_id']];
                            }
                        }
                    }
                }
            }
            return $retu_array;
        }
        return array();
    }

//※行／列タイトル合計情報取得を改造し、休暇事由別の集計にする。
    /*     * ********************************************************************** */
    //
    // ・事由に午前有休、午後有休、半前代替休、半後代替休、半前振替休、半後振替休を選択した場合には、
    //   合計行、合計列に対して、選択した勤務シフトパターンに+0.5、休暇に対して+0.5する。
    // ・午前有休、午後有休は、休暇（有給）に+0.5、半前代替休、半後代替休は、
    //   休暇（代替）に+0.5、半前振替休、半後振替休は、休暇（振替）に+0.5する。
    //
    // @param   $group_id           勤務シフトグループＩＤ
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $gyo_retu_flg       行／列フラグ（1:行、2:列）
    // @param   $rslt_flg           実績フラグ（１：実績、２：システム日以降は集計なし）
    // @param   $day_cnt            日数
    // @param   $plan_array         勤務シフト情報
    // @param   $title_gyo_array    行タイトル情報
    // @param   $title_retu_array   列タイトル情報
    // @param   $calendar_array     カレンダー情報
    // @param   $title_hol_array    休暇事由情報
    // @param   $data_atdptn        出勤パターン情報取得
    //
    // @return  $ret            取得情報
    /*     * ********************************************************************** */
    function get_hol_cnt_array($group_id, $pattern_id, $gyo_retu_flg, $rslt_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn) {

        $ret = array();
        $gyo_array = array();

        $change_switch = $this->GetSumCustomSwith($pattern_id);

        if ($change_switch != 0) { // このスイッチがオンのときは処理しない 2012.1.27
            return $gyo_array;
        }

        $retu_array = array();
        $holiday_array = array();
        $holiday_idx = "";
        $today = date("Ymd");

        // 集計見直し 2008/11/10
        // 半休チェック、 事由が半休の場合、0.5ずつ加算
        //"2"午前有休 "3"午後有休 "18"半前代替休 "19"半後代替休
        //"20"半前振替休 "21"半後振替休 "35"午前公休 "36"午後公休
        //"38"午前年休 "39"午後年休 "42"午前リフレッシュ "43"午後リフレッシュ
        //"48"午前欠勤 "49"午後欠勤 "50"午前夏休 "51"午後夏休 "52"午前その他 "53"午後その他
//      $half_chk_array = array("2", "3", "18", "19", "20", "21", "35", "36", "38", "39", "42", "43", "48", "49", "50", "51", "52", "53"); ここでは未使用
//半休の情報
        //換算日数情報
        $arr_workday_count = array();
        for ($i = 0; $i < count($data_atdptn); $i++) {
            $wk_id = $data_atdptn[$i]["id"];
            $arr_workday_count[$wk_id] = $data_atdptn[$i]["workday_count"];
        }
        //集計用情報
        //半休半休の前、後の集計先事由、日数、集計位置を保持する。
        $add_info = $this->get_hol_add_info();

        for ($i = 1; $i <= 72; $i++) { //72：add_infoテーブルに追加された場合はここも修正
            if ($add_info[$i]["add_day"] > 0) {

                for ($k = 0; $k < count($title_hol_array); $k++) {
                    $reason = $title_hol_array[$k]["reason"];
                    if ($reason == $add_info[$i]["add_reason"]) {
                        $add_info[$i]["add_idx"] = $k;
                        break;
                    }
                }
            }
            if ($add_info[$i]["add_day2"] > 0) {

                for ($k = 0; $k < count($title_hol_array); $k++) {
                    $reason = $title_hol_array[$k]["reason"];
                    if ($reason == $add_info[$i]["add_reason2"]) {
                        $add_info[$i]["add_idx2"] = $k;
                        break;
                    }
                }
            }
        }

        //休暇事由別合計を算出
        //配列の1段目の後ろに2段目の合計を設定
        $title_hol_array_cnt = count($title_hol_array);

        //-------------------------------------------------------------------
        //行タイトル単位で算出設定
        //-------------------------------------------------------------------
        for ($i = 0; $i < count($plan_array); $i++) {
            //-------------------------------------------------------------------
            //初期値設定
            //-------------------------------------------------------------------
            for ($k = 0; $k < $title_hol_array_cnt; $k++) {
                $gyo_array[$i][$k] = 0.0;
                $k2 = $title_hol_array_cnt + $k;
                $gyo_array[$i][$k2] = 0.0;
            }

            //-------------------------------------------------------------------
            //日数分を加算設定
            //-------------------------------------------------------------------
            for ($m = 1; $m <= $day_cnt; $m++) {

                //-------------------------------------------------------------------
                //加算
                //-------------------------------------------------------------------
                $wk_pattern_id = "pattern_id_" . $m;
                $wk_atdptn_ptn_id = "atdptn_ptn_id_" . $m;
                $wk_reason = "reason_" . $m;
                $wk_reason_2 = "reason_2_" . $m;
                //応援追加グループ
                $wk_assist_group = "assist_group_" . $m;
                $wk_assist_flg = "";
                if ($plan_array[$i][$wk_assist_group] != "" &&
                        $plan_array[$i][$wk_assist_group] != $group_id) {
                    $wk_assist_flg = "1";
                }
                if (/* $plan_array[$i][$wk_pattern_id] == $pattern_id && */
                        $wk_assist_flg == "") {
                    $wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
                    //午前振出、午後振出は集計なし
//                  $tmp_reason_2 = $plan_array[$i][$wk_reason_2];
//                  if ($tmp_reason_2 == "65" ||
//                      $tmp_reason_2 == "66") {
//                      ;
//                  } else
                    if ($arr_workday_count[$wk_id] == 0.5 &&
                            ($plan_array[$i][$wk_reason_2] == "" ||
                            $plan_array[$i][$wk_reason_2] == "65" ||
                            $plan_array[$i][$wk_reason_2] == "66")) {
                        //換算日数が0.5で事由が未設定の場合
                        //休暇に0.5追加
                        $add_idx = $add_info[34]["add_idx"];
                        $gyo_array[$i][$add_idx] += 0.5;
                    } else
                    // 勤務パターンが休暇
                    if ($plan_array[$i][$wk_atdptn_ptn_id] == "10" ||
                            $plan_array[$i][$wk_reason_2] != "") {
                        $wk_reason = $plan_array[$i][$wk_reason];
                        $wk_reason_2 = $plan_array[$i][$wk_reason_2];
                        if ($wk_reason_2 != "") {
                            $wk_reason = $wk_reason_2;
                        }
                        $wk_reason = (int) $wk_reason;
                        $add_idx = $add_info[$wk_reason]["add_idx"];
                        $add_day = $add_info[$wk_reason]["add_day"];

                        if ($add_day != "") {
                            $gyo_array[$i][$add_idx] += $add_day;
                        }
                        //半休半休の後ろ側の集計
                        $add_idx2 = $add_info[$wk_reason]["add_idx2"];
                        $add_day2 = $add_info[$wk_reason]["add_day2"];

                        if ($add_idx2 != "" && $add_day2 != "") {
                            $gyo_array[$i][$add_idx2] += $add_day2;
                        }
                        /*
                          switch($wk_reason) {
                          case 44:    // 半有半公の公休分0.5を加算
                          $add_idx = $add_info[24]["add_idx"];
                          $gyo_array[$i][$add_idx] += 0.5;
                          break;
                          case 57:    // 半有半欠の欠勤分0.5を加算
                          $add_idx = $add_info[6]["add_idx"];
                          $gyo_array[$i][$add_idx] += 0.5;
                          break;
                          }
                         */
                    }
                }
                //集計行２段目、$gyo_array[$i]配列の1段目の後ろに2段目の合計を設定
                //シフト作成画面で2段目が実績の場合、システム日以降集計しない
//              if ($rslt_flg != "2" || ($rslt_flg == "2" && $today >= $calendar_array[$m-1]["date"])) {

                $wk_pattern_id = "rslt_pattern_id_" . $m;
                $wk_atdptn_ptn_id = "rslt_id_" . $m;
                $wk_reason = "rslt_reason_" . $m;
                $wk_reason_2 = "rslt_reason_2_" . $m;
                //応援追加グループ
                $wk_assist_group = "assist_group_" . $m;
                //希望の応援追加分
                $wk_rslt_assist_group = "rslt_assist_group_" . $m;
                $wk_assist_flg = "";
                if (($plan_array[$i][$wk_rslt_assist_group] != "" &&
                        $plan_array[$i][$wk_rslt_assist_group] != $group_id) ||
                        ($plan_array[$i][$wk_rslt_assist_group] == "" &&
                        $plan_array[$i][$wk_assist_group] != "" &&
                        $plan_array[$i][$wk_assist_group] != $group_id)) {
                    $wk_assist_flg = "1";
                }
                if (/* $plan_array[$i][$wk_pattern_id] == $pattern_id && */
                        $wk_assist_flg == "") {
                    $wk_id = $plan_array[$i][$wk_atdptn_ptn_id];
                    //午前振出、午後振出は集計なし
//                      $tmp_reason_2 = $plan_array[$i][$wk_reason_2];
//                      if ($tmp_reason_2 == "65" ||
//                          $tmp_reason_2 == "66") {
//                          ;
//                      } else
                    if ($arr_workday_count[$wk_id] == 0.5 &&
                            ($plan_array[$i][$wk_reason_2] == "" ||
                            $plan_array[$i][$wk_reason_2] == "65" ||
                            $plan_array[$i][$wk_reason_2] == "66")) {
                        //換算日数が0.5で事由が未設定の場合
                        //休暇に0.5追加
                        $k2 = $title_hol_array_cnt + $add_info[34]["add_idx"];
                        $gyo_array[$i][$k2] += 0.5;
                    } else
                    // 勤務パターンが休暇
                    if ($plan_array[$i][$wk_atdptn_ptn_id] == "10" ||
                            $plan_array[$i][$wk_reason_2] != "") {

                        $wk_reason = $plan_array[$i][$wk_reason];
                        $wk_reason_2 = $plan_array[$i][$wk_reason_2];
                        if ($wk_reason_2 != "") {
                            $wk_reason = $wk_reason_2;
                        }
                        $wk_reason = (int) $wk_reason;
                        $add_idx = $add_info[$wk_reason]["add_idx"];
                        $add_day = $add_info[$wk_reason]["add_day"];

                        if ($add_day != "") {
                            $k2 = $title_hol_array_cnt + $add_idx;
                            $gyo_array[$i][$k2] += $add_day;
                        }

                        //半休半休の後ろ側の集計
                        $add_idx2 = $add_info[$wk_reason]["add_idx2"];
                        $add_day2 = $add_info[$wk_reason]["add_day2"];

                        if ($add_idx2 != "" && $add_day2 != "") {
                            $k2 = $title_hol_array_cnt + $add_idx2;
                            $gyo_array[$i][$k2] += $add_day2;
                        }
                        /*
                          // 半有半公の公休分0.5を加算
                          if ($wk_reason == 44) {
                          $k2 = $title_hol_array_cnt + $add_info[24]["add_idx"];
                          $gyo_array[$i][$k2] += 0.5;
                          }
                         */
                    }
                }
//              }
            }
        }

        return $gyo_array;
    }

    /**
     * 年休残数を取得する
     *
     * シフト実績(result)、シフト予定(plan)、シフト希望(hope)データを、種別をキーにした連想配列にセットして返す。
     * シフト予定データ$paid_hol_all_array["plan"]には、処理により適宜「シフト予定(下書き含まない)データ」、「シフト予定(下書き含む)」、
     * 「シフト希望データ」のいずれかがセットされる。
     *
     * @param array   $plan_array     勤務シフト情報
     * @param array   $calendar_array カレンダー情報
     * @param integer $duty_yyyy      指定年
     * @param integer $duty_mm        指定月
     * @param string  $plan_type      シフトデータ種別
     *                                "result":実績
     *                                "plan":シフト予定(下書き含まない)
     *                                "create":シフト予定(下書き含む)
     *                                "hope":シフト希望
     *
     * @return 取得情報
     */
    function get_paid_hol_rest_array(
    $plan_array, $calendar_array, $duty_yyyy, $duty_mm, $plan_type) {
        $paid_hol_all_array = array();

        //-------------------------------------------------------------------
        // 勤務シフト情報から職員リストを作成
        //-------------------------------------------------------------------
        $staff_id_array = array();
        foreach ($plan_array as $value) {
            $staff_id_array[] = $value["staff_id"];
        }

        //-------------------------------------------------------------------
        // 有給休暇残日数(実績)
        //-------------------------------------------------------------------
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[count($calendar_array) - 1]["date"];
        $paid_hol_all_array["result"] = $this->get_paid_hol_result_array(
                $staff_id_array, $start_date, $end_date, $duty_yyyy, $duty_mm
        );

        //-------------------------------------------------------------------
        // 有給休暇残日数(予定)
        //-------------------------------------------------------------------
        // 「前日までの有給休暇残日数 - 本日以降の有給休暇取得数」で算出する為、
        // まず前日までの有給休暇残日数を取得
        $yesterday = date("Ymd", strtotime("-1 day"));    // 操作日(マシン日付)前日
        $paid_hol_result_array = $this->get_paid_hol_result_array(
                $staff_id_array, date("Ymd", strtotime("-7 day")), // 開始日は適当にセット(７日前)
                $yesterday, substr($yesterday, 0, 4), // 「指定年」は前日日付から
                substr($yesterday, 4, 2)                  // 「指定月」も前日日付から
        );

        $today = date("Ymd");    // 本日日付
        if ($plan_type == "hope") {    // 「出勤表＞勤務シフト希望」画面から呼び出される場合、予定として勤務希望データをセットする
            $paid_hol_all_array["plan"] = $this->get_paid_hol_plan_array($staff_id_array, $paid_hol_result_array, $today, "", "hope");
            // ここでは$paid_hol_all_array["hope"]をセットしない
        } else {
            // 「勤務シフト作成＞シフト作成」画面又は「勤務シフト作成＞勤務実績入力」画面から呼び出される場合、シフトデータ種別は"create"
            // 「出勤表＞勤務シフト参照」から呼び出される場合、シフトデータ種別は"plan"
            $paid_hol_all_array["plan"] = $this->get_paid_hol_plan_array($staff_id_array, $paid_hol_result_array, $today, "", $plan_type);
            $paid_hol_all_array["hope"] = $this->get_paid_hol_plan_array($staff_id_array, $paid_hol_result_array, $today, "", "hope");
        }

        return $paid_hol_all_array;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：ＤＢ読み込み）
//-------------------------------------------------------------------------------------------------------------------------
    /*     * ********************************************************************** */
    // 自動シフト作成用　勤務シフト情報取得（スタッフ情報）
    // @param   $group_id       勤務シフトグループＩＤ
    // @param   $start_yyyy     検索開始年
    // @param   $start_mm       検索開始月
    // @param   $end_yyyy       検索終了年
    // @param   $end_mm         検索終了月
    // @param   $data_st        ＤＢ(stmst)より役職情報取得
    // @param   $data_job       ＤＢ(jobmst)より職種情報取得
    // @param   $data_emp       ＤＢ(empmst)より職員情報取得
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_plan_staff_array_auto($group_id, $start_yyyy, $start_mm, $end_yyyy, $end_mm, $data_st, $data_job, $data_emp) {
        //-------------------------------------------------------------------
        //スタッフ情報ＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_plan_staff";
        $cond = "where group_id = '$group_id' ";
        $cond .= "and duty_yyyy >= $start_yyyy ";
        $cond .= "and duty_mm >= $start_mm ";
        $cond .= "and duty_yyyy <= $end_yyyy ";
        $cond .= "and duty_mm <= $end_mm ";
        $cond .= "order by group_id, duty_yyyy, duty_mm, no ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["staff_id"] = pg_fetch_result($sel, $i, "emp_id");
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフト情報取得（スタッフ情報）
    // @param   $group_id       勤務シフトグループＩＤ
    // @param   $duty_yyyy      勤務年
    // @param   $duty_mm        勤務月
    // @param   $data_st        ＤＢ(stmst)より役職情報取得
    // @param   $data_job       ＤＢ(jobmst)より職種情報取得
    // @param   $data_emp       ＤＢ(empmst)より職員情報取得
    // @param   $month_flg      月またがり 0:前月から当月 1:当月から当月 2:当月から翌月 未使用 20110301
    // @param   $stride_flag    月またがりフラグ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_plan_staff_array($group_id, $duty_yyyy, $duty_mm, $data_st, $data_job, $data_emp, $month_flg = "1", $stride_flag = false) {

        if (!$stride_flag) {
            //月またがりがあっても当月を基準とする 20110301
            $cond_yyyymm = "duty_yyyy=$duty_yyyy and duty_mm=$duty_mm ";
        } else {
            // 月またがり無し
            if ($month_flg == "1") {
                $cond_yyyymm = "duty_yyyy=$duty_yyyy and duty_mm=$duty_mm ";
            }
            // 月またがり有り
            else {
                //当月から翌月
                if ($month_flg == "0") {
                    $next_yyyy = $duty_yyyy;
                    $next_mm = $duty_mm - 1;
                    if ($next_mm < 1) {
                        $next_yyyy--;
                        $next_mm = 12;
                    }
                }
                //前月から当月
                elseif ($month_flg == "2") {
                    $next_yyyy = $duty_yyyy;
                    $next_mm = $duty_mm + 1;
                    if ($next_mm >= 13) {
                        $next_yyyy = $duty_yyyy + 1;
                        $next_mm = 1;
                    }
                }
                $cond_yyyymm = "((duty_yyyy=$duty_yyyy and duty_mm=$duty_mm) or (duty_yyyy=$next_yyyy and duty_mm=$next_mm)) ";
            }
        }
        //-------------------------------------------------------------------
        //スタッフ情報ＤＢより情報取得
        //-------------------------------------------------------------------
        if ($group_id == "") {
            $sql = "SELECT a.*,b.standard_id,c.team_id,t.team_name ";
            $sql .= "FROM duty_shift_plan_staff a ";
            $sql .= "LEFT JOIN duty_shift_group b USING (group_id) ";
            $sql .= "LEFT JOIN duty_shift_plan_team c USING (group_id,emp_id,duty_yyyy,duty_mm) ";
            $sql .= "LEFT JOIN duty_shift_staff_team t USING (team_id) ";
            $cond = "WHERE $cond_yyyymm ";
            $cond .= "ORDER BY a.group_id, a.duty_yyyy, a.duty_mm, a.no";
        } else {
            $sql = "SELECT a.*,c.team_id,t.team_name ";
            $sql .= "FROM duty_shift_plan_staff a ";
            $sql .= "LEFT JOIN duty_shift_plan_team c USING (group_id,emp_id,duty_yyyy,duty_mm) ";
            $sql .= "LEFT JOIN duty_shift_staff_team t USING (team_id) ";
            $cond = "WHERE a.group_id = '$group_id' AND $cond_yyyymm ";
            $cond .= "ORDER BY a.group_id, a.duty_yyyy, a.duty_mm, no";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $arr_chk_emp = array();
        $data = array();
        $j = 0;
        for ($i = 0; $i < $num; $i++) {
            $wk_id = pg_fetch_result($sel, $i, "emp_id");
            //月またがりがあっても当月を基準とする 20110301
            // 月またがり有り時、重複を除く
            if ($month_flg != "1" && $stride_flag == true) {
                if (in_array($wk_id, $arr_chk_emp)) {
                    continue;
                }
                $arr_chk_emp[] = $wk_id;
            }

            $wk_data = $this->get_duty_shift_staff_one($wk_id, $data_st, $data_job, $data_emp);

            $data[$j]["group_id"] = pg_fetch_result($sel, $i, "group_id");        //勤務シフトグループＩＤ
            $data[$j]["id"] = $wk_id;                                  //スタッフＩＤ（職員ＩＤ）
            $data[$j]["no"] = pg_fetch_result($sel, $i, "no");                    //表示順
            $data[$j]["duty_yyyy"] = pg_fetch_result($sel, $i, "duty_yyyy");  //利用可能年
            $data[$j]["duty_mm"] = pg_fetch_result($sel, $i, "duty_mm");      //利用可能月
            $data[$j]["name"] = $wk_data["name"];                       //スタッフ名称
            $data[$j]["job_id"] = $wk_data["job_id"];                   //職種ID
            $data[$j]["job_name"] = $wk_data["job_name"];               //職種名
            $data[$j]["sex"] = $wk_data["sex"];                     //性別
            // 最新役職を反映させる 20130219
            $data[$j]["st_id"] = $wk_data["st_id"];                     //役職ID
            $data[$j]["status"] = $wk_data["status"];                   //役職

            $data[$j]["duty_years"] = $wk_data["duty_years"];           //勤務年数
            if ($group_id == "") {
                $data[$j]["standard_id"] = pg_fetch_result($sel, $i, "standard_id");  //施設基準
            }

            $data[$j]["team_id"] = pg_fetch_result($sel, $i, "team_id");      //チームID
            $data[$j]["team_name"] = pg_fetch_result($sel, $i, "team_name");  //チーム名

            $j++;
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトチーム情報取得
    // @param   $group_id       勤務シフトグループＩＤ
    // @param   $duty_yyyy      勤務年
    // @param   $duty_mm        勤務月
    // @param   $month_flg      月またがり 0:前月から当月 1:当月から当月 2:当月から翌月
    // @param   $stride_flag    月またがりフラグ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_plan_team_array($group_id, $duty_yyyy, $duty_mm, $month_flg = "1", $stride_flag = false) {

        if (!$stride_flag) {
            //月またがりがあっても当月を基準とする 20110301
            $cond_yyyymm = "duty_yyyy=$duty_yyyy AND duty_mm=$duty_mm ";
        } else {
            // 月またがり無し
            if ($month_flg == "1") {
                $cond_yyyymm = "duty_yyyy=$duty_yyyy AND duty_mm=$duty_mm ";
            }
            // 月またがり有り
            else {
                //当月から翌月
                if ($month_flg == "0") {
                    $next_yyyy = $duty_yyyy;
                    $next_mm = $duty_mm - 1;
                    if ($next_mm < 1) {
                        $next_yyyy--;
                        $next_mm = 12;
                    }
                }
                //前月から当月
                elseif ($month_flg == "2") {
                    $next_yyyy = $duty_yyyy;
                    $next_mm = $duty_mm + 1;
                    if ($next_mm >= 13) {
                        $next_yyyy = $duty_yyyy + 1;
                        $next_mm = 1;
                    }
                }
                $cond_yyyymm = "((duty_yyyy=$duty_yyyy AND duty_mm=$duty_mm) OR (duty_yyyy=$next_yyyy AND duty_mm=$next_mm)) ";
            }
        }
        //-------------------------------------------------------------------
        //チーム情報ＤＢより情報取得
        //-------------------------------------------------------------------
        if ($group_id == "") {
            $sql = "SELECT a.*,t.team_name ";
            $sql .= "FROM duty_shift_plan_team a ";
            $sql .= "LEFT JOIN duty_shift_staff_team t USING (team_id) ";
            $cond = "WHERE $cond_yyyymm ";
            $cond .= "ORDER BY a.group_id, a.duty_yyyy, a.duty_mm";
        } else {
            $sql = "SELECT a.*,t.team_name ";
            $sql .= "FROM duty_shift_plan_team a ";
            $sql .= "LEFT JOIN duty_shift_staff_team t USING (team_id) ";
            $cond = "WHERE a.group_id='$group_id' AND $cond_yyyymm ";
            $cond .= "ORDER BY a.group_id, a.duty_yyyy, a.duty_mm";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $arr_chk_emp = array();
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $wk_id = pg_fetch_result($sel, $i, "emp_id");

            // 月またがり有り時、重複を除く
            if ($month_flg != "1") {
                if (in_array($wk_id, $arr_chk_emp)) {
                    continue;
                }
                $arr_chk_emp[] = $wk_id;
            }
            $data[$wk_id]["group_id"] = pg_fetch_result($sel, $i, "group_id");  //勤務シフトグループＩＤ
            $data[$wk_id]["id"] = $wk_id;                          //スタッフＩＤ（職員ＩＤ）
            $data[$wk_id]["duty_yyyy"] = pg_fetch_result($sel, $i, "duty_yyyy"); //利用可能年
            $data[$wk_id]["duty_mm"] = pg_fetch_result($sel, $i, "duty_mm");       //利用可能月
            $data[$wk_id]["team_id"] = pg_fetch_result($sel, $i, "team_id");       //チームID
            $data[$wk_id]["team_name"] = pg_fetch_result($sel, $i, "team_name"); //チーム名
        }
        return $data;
    }

    //////////////////////
    /*     * ********************************************************************** */
    // 勤務シフト情報取得（勤務シフト情報の編集設定）
    // @param   $sel                検索ＳＱＬ情報
    // @param   $num                検索件数
    // @param   $day_cnt            日数
    // @param   $data_st            ＤＢ(stmst)役職情報
    // @param   $data_job           ＤＢ(jobmst)職種情報
    // @param   $data_emp           ＤＢ(empmst)職員情報
    // @param   $data_pattern_all   勤務シフトパターン情報
    // @param   $calendar_array     カレンダー情報
    //
    // @return  $wk_data2       取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_plan_set($sel, $num, $day_cnt, $data_st, $data_job, $data_emp, $data_pattern_all, $calendar_array) {
        $start_date = $calendar_array[0]["date"];
        //-------------------------------------------------------------------
        //勤務シフト情報の編集設定
        //-------------------------------------------------------------------
        $wk_data = array();
        for ($i = 0; $i < $num; $i++) {
            //-------------------------------------------------------------------
            //ＤＢより取得値
            //-------------------------------------------------------------------
            $wk_data[$i]["staff_id"] = pg_fetch_result($sel, $i, "emp_id");           //職員ＩＤ
            $key_duty_date = trim(pg_fetch_result($sel, $i, "duty_date"));            //勤務年月日
            $key_pattern_id = trim(pg_fetch_result($sel, $i, "pattern_id"));      //出勤グループＩＤ
            $key_atdptn_ptn_id = trim(pg_fetch_result($sel, $i, "atdptn_ptn_id"));    //出勤パターンＩＤ
            $key_reason = trim(pg_fetch_result($sel, $i, "reason"));              //事由
            $key_group_id = trim(pg_fetch_result($sel, $i, "group_id"));              //グループＩＤ
            //-------------------------------------------------------------------
            //職員DBより職員名設定
            //-------------------------------------------------------------------
            if ($this->data_assoc_flg) {
                $k = $this->data_emp_idx[$wk_data[$i]["staff_id"]];
                $data["staff_name"] = $data_emp[$k]["name"];
            }
            //連想配列への設定がない場合は、以前通りの動作
            else {
                for ($k = 0; $k < count($data_emp); $k++) {
                    if ($wk_data[$i]["staff_id"] == $data_emp[$k]["id"]) {
                        $wk_data[$i]["staff_name"] = $data_emp[$k]["name"];
                        break;
                    }
                }
            }
            //-------------------------------------------------------------------
            //勤務シフト記号情報DBより設定
            //-------------------------------------------------------------------
            //20140127 start 呼出元で記号情報設定しているためここでは不要
            //for ($k=0;$k<count($data_pattern_all);$k++) {
            //if (($key_pattern_id == $data_pattern_all[$k]["pattern_id"]) &&
            //  ($key_atdptn_ptn_id == $data_pattern_all[$k]["atdptn_ptn_id"])) {
            if ($key_atdptn_ptn_id != "") {
                //20140127 end
                $m = ((date_utils::to_timestamp_from_ymd($key_duty_date) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
                $wk_data[$i]["pattern_id_$m"] = $key_pattern_id;                                //出勤グループＩＤ
                $wk_data[$i]["atdptn_ptn_id_$m"] = $key_atdptn_ptn_id;                          //出勤パターンＩＤ
                $wk_data[$i]["group_id_$m"] = $key_group_id;                            //出勤パターンＩＤ
                //$ret["34"] = ""（事由無し）;
                //$ret["24"] = "公休";
                //$ret["22"] = "法定休暇";
                //$ret["23"] = "所定休暇";
                //$ret["1"] = "有給休暇";
                //$ret["37"] = "年休";
                //$ret["4"] = "代替休暇";
                //$ret["17"] = "振替休暇";
                //$ret["5"] = "特別休暇";
                //$ret["6"] = "欠勤（一般欠勤）";
                //$ret["7"] = "病欠（病傷欠勤）";
                //$ret["8"] = "その他休";
                //$ret["40"] = "リフレッシュ休暇";
                //$ret["41"] = "初盆休暇";
                //25:産休〜32:結婚休 44:半有半公 61:年末年始
                //54:半夏半〜64公半正半欠追加20110808
                //※勤務パターンのプルダウンに追加する場合、ここも修正
                if (($key_reason == "34") ||
                        ($key_reason == "1") || ($key_reason == "37") ||
                        ($key_reason >= "4" && $key_reason <= "8") ||
                        ($key_reason == "17") ||
                        ($key_reason >= "22" && $key_reason <= "32") ||
                        ($key_reason == "40") || ($key_reason == "41") ||
                        ($key_reason >= "44" && $key_reason <= "47") ||
                        ($key_reason >= "54" && $key_reason <= "64") ||
                        $key_reason == "72" || $key_reason == "33" || ($key_reason >= "9" && $key_reason <= "15") //追加 20141031
                ) {
                    $wk_data[$i]["reason_$m"] = $key_reason;    //事由
                }
                //$ret["2"] = "午前有休";
                //$ret["3"] = "午後有休";
                else if (($key_reason == "2") ||
                        ($key_reason == "3")) {
                    $wk_data[$i]["reason_2_$m"] = $key_reason;  //事由
                    $wk_data[$i]["reason_$m"] = "1";            //事由（有給休暇）
                }
                //$ret["38"] = "午前年休";
                //$ret["39"] = "午後年休";
                else if (($key_reason == "38") ||
                        ($key_reason == "39")) {
                    $wk_data[$i]["reason_2_$m"] = $key_reason;  //事由
                    $wk_data[$i]["reason_$m"] = "37";           //事由（年休）
                }
                //$ret["18"] = "半前代替休";
                //$ret["19"] = "半後代替休";
                else if (($key_reason == "18") ||
                        ($key_reason == "19")) {
                    $wk_data[$i]["reason_2_$m"] = $key_reason;  //事由
                    $wk_data[$i]["reason_$m"] = "4";            //事由（代替休暇）
                }
                //$ret["20"] = "半前振替休";
                //$ret["21"] = "半後振替休";
                else if (($key_reason == "20") ||
                        ($key_reason == "21")) {
                    $wk_data[$i]["reason_2_$m"] = $key_reason;  //事由
                    $wk_data[$i]["reason_$m"] = "17";           //事由（振替休暇）
                }
                //$ret["35"] = "半前公休";
                //$ret["36"] = "半後公休";
                else if (($key_reason == "35") ||
                        ($key_reason == "36")) {
                    $wk_data[$i]["reason_2_$m"] = $key_reason;  //事由
                    $wk_data[$i]["reason_$m"] = "24";           //事由（公休）
                }
                //$ret["42"] = "半前リフレッシュ";
                //$ret["43"] = "半後リフレッシュ";
                else if (($key_reason == "42") ||
                        ($key_reason == "43")) {
                    $wk_data[$i]["reason_2_$m"] = $key_reason;  //事由
                    $wk_data[$i]["reason_$m"] = "40";           //事由（リフレッシュ）
                }
                // 上記以外
                else {
                    $wk_data[$i]["reason_2_$m"] = $key_reason;  //事由
                    // 休暇の場合、事由無しの休暇とするため"34"を設定
                    if ($key_atdptn_ptn_id == "10") {
                        $wk_data[$i]["reason_$m"] = "34";
                    }
                }
//echo("<br>$m reason=".$wk_data[$cnt]["reason_$m"]); //debug
//echo("  reason_2=".$wk_data[$cnt]["reason_2_$m"]); //debug
                //20140127 start
                //$wk_data[$i]["font_name_$m"] = $data_pattern_all[$k]["font_name"];                //文字（勤務状況名）
                //$wk_data[$i]["font_color_$m"] = $data_pattern_all[$k]["font_color_id"];           //文字（色）
                //$wk_data[$i]["back_color_$m"] = $data_pattern_all[$k]["back_color_id"];           //文字（背景色）
                //$wk_data[$i]["count_kbn_gyo_$m"] = $data_pattern_all[$k]["count_kbn_gyo"];        //集計行（出勤パターンＩＤ）
                //$wk_data[$i]["count_kbn_retu_$m"] = $data_pattern_all[$k]["count_kbn_retu"];  //集計列（出勤パターンＩＤ）
                //break;
                //}
                //201400127 end
            }
        }
        //-------------------------------------------------------------------
        //取得データを職員毎に再設定
        //-------------------------------------------------------------------
        $k = 0;
        $wk_staff_id = "";
        $wk_data2 = array();
        for ($i = 0; $i < count($wk_data); $i++) {
            if ($i == 0) {
                $wk_staff_id = $wk_data[0]["staff_id"];
            } else if ($wk_staff_id != $wk_data[$i]["staff_id"]) {
                $wk_staff_id = $wk_data[$i]["staff_id"];
                $k++;
            }
            //職員データ設定
            $wk_data2[$k]["staff_id"] = $wk_data[$i]["staff_id"];
            $wk_data2[$k]["staff_name"] = $wk_data[$i]["staff_name"];
            //勤務状況
            for ($m = 1; $m <= $day_cnt; $m++) {
                if ($wk_data[$i]["pattern_id_$m"] != "") {
                    $wk_data2[$k]["pattern_id_$m"] = $wk_data[$i]["pattern_id_$m"];
                }       //出勤グループＩＤ
                if ($wk_data[$i]["atdptn_ptn_id_$m"] != "") {
                    $wk_data2[$k]["atdptn_ptn_id_$m"] = $wk_data[$i]["atdptn_ptn_id_$m"];
                }   //出勤パターンＩＤ
                if ($wk_data[$i]["reason_$m"] != "") {
                    $wk_data2[$k]["reason_$m"] = $wk_data[$i]["reason_$m"];
                }                        //事由
                if ($wk_data[$i]["reason_2_$m"] != "") {
                    $wk_data2[$k]["reason_2_$m"] = $wk_data[$i]["reason_2_$m"];
                }                  //事由
                if ($wk_data[$i]["group_id_$m"] != "") {
                    $wk_data2[$k]["group_id_$m"] = $wk_data[$i]["group_id_$m"];
                }                  //事由
                if ($wk_data[$i]["count_kbn_gyo_$m"] != "") {
                    $wk_data2[$k]["count_kbn_gyo_$m"] = $wk_data[$i]["count_kbn_gyo_$m"];
                }   //集計行（出勤パターンＩＤ）
                if ($wk_data[$i]["count_kbn_retu_$m"] != "") {
                    $wk_data2[$k]["count_kbn_retu_$m"] = $wk_data[$i]["count_kbn_retu_$m"];
                } //集計列（出勤パターンＩＤ）
                if ($wk_data[$i]["font_name_$m"] != "") {
                    $wk_data2[$k]["font_name_$m"] = $wk_data[$i]["font_name_$m"];           //文字（勤務状況名）
                    $wk_data2[$k]["font_color_$m"] = $wk_data[$i]["font_color_$m"];         //文字（背景色）
                    $wk_data2[$k]["back_color_$m"] = $wk_data[$i]["back_color_$m"];         //文字（色）
                }
            }
            //職種DBより職種設定
            $data_staff_one = $this->get_duty_shift_staff_one($wk_data2[$k]["staff_id"], $data_st, $data_job, $data_emp);
            $wk_data2[$k]["job_id"] = $data_staff_one["job_id"];
            $wk_data2[$k]["job_name"] = $data_staff_one["job_name"];
        }

        return $wk_data2;
    }

    /*     * ********************************************************************** */

    // 勤務シフト情報取得（勤務シフト情報のコメント設定）
    // @param   $sel                検索ＳＱＬ情報
    // @param   $num                検索件数
    // @param   $day_cnt            日数
    // @param   $calendar_array     カレンダー情報
    //
    // @return  $wk_data2       取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_plan_set_comment($sel, $num, $day_cnt, $calendar_array) {
        $start_date = $calendar_array[0]["date"]; //20140213
        //-------------------------------------------------------------------
        //勤務シフト情報の編集設定
        //-------------------------------------------------------------------
        $wk_data = array();
        for ($i = 0; $i < $num; $i++) {
            //-------------------------------------------------------------------
            //ＤＢより取得値
            //-------------------------------------------------------------------
            $wk_data[$i]["staff_id"] = pg_fetch_result($sel, $i, "emp_id");           //職員ＩＤ
            $key_duty_date = trim(pg_fetch_result($sel, $i, "duty_date"));            //勤務年月日
            $key_comment = trim(pg_fetch_result($sel, $i, "comment"));                //コメント
            //-------------------------------------------------------------------
            //指定日に設定
            //-------------------------------------------------------------------
            //20140213
            $m = ((date_utils::to_timestamp_from_ymd($key_duty_date) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
            /*
              $m=0;
              for ($p=0;$p<count($calendar_array);$p++) {
              if ($calendar_array[$p]["date"] == $key_duty_date) {
              $m = $p + 1;
              break;
              }
              }
             */
            $wk_data[$i]["comment_$m"] = $key_comment;  //出勤グループＩＤ
        }
        //-------------------------------------------------------------------
        //取得データを職員毎に再設定
        //-------------------------------------------------------------------
        $k = 0;
        $wk_staff_id = "";
        $wk_data2 = array();
        for ($i = 0; $i < count($wk_data); $i++) {
            if ($i == 0) {
                $wk_staff_id = $wk_data[0]["staff_id"];
            } else if ($wk_staff_id != $wk_data[$i]["staff_id"]) {
                $wk_staff_id = $wk_data[$i]["staff_id"];
                $k++;
            }
            //職員データ設定
            $wk_data2[$k]["staff_id"] = $wk_data[$i]["staff_id"];
            $wk_data2[$k]["staff_name"] = $wk_data[$i]["staff_name"];
            //勤務状況
            for ($m = 1; $m <= $day_cnt; $m++) {
                if ($wk_data[$i]["comment_$m"] != "") {
                    $wk_data2[$k]["comment_$m"] = $wk_data[$i]["comment_$m"];
                }   //コメント
            }
        }

        return $wk_data2;
    }

/////////////////////////////////////////
    // 引数に$emp_id=""を追加しているため呼び出し元の修正時注意する。
    /*     * ********************************************************************** */
    // 勤務シフト情報取得
    // @param   $group_id           勤務シフトグループＩＤ
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $duty_yyyy          年
    // @param   $duty_mm            月
    // @param   $day_cnt            日数
    // @param   $individual_flg     検索対象ＤＢフラグ          １：勤務希望
    // @param   $hope_get_flg       「勤務希望取り込み」フラグ  １：勤務希望取り込み
    // @param   $show_data_flg      ２行目表示データ判定フラグ  １：勤務実績、２：勤務希望、３：当直
    // @param   $set_data           再設定する情報
    // @param   $data_week          週情報
    // @param   $calendar_array     カレンダー情報
    // @param   $data_atdptn        出勤パターン情報
    // @param   $data_pattern_all   勤務シフトパターン情報
    // @param   $data_st            役職情報
    // @param   $jobmst             職種情報
    // @param   $empmst             職員情報
    // @param   $emp_id             ログインユーザ
    // @param   $group_array        グループ情報
    // @param   $sche               パラメータ追加 20131115 取得元テーブル(atdbk(出勤予定):未指定時 / atdbk_sche(当初予定))
    //                              出勤予定テーブル名に付加する拡張子("_sche")
    //
    // @return  $data       取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_plan_array(
    $group_id, $pattern_id, $duty_yyyy, $duty_mm, $day_cnt, $individual_flg, $hope_get_flg, $show_data_flg, $set_data, $data_week, $calendar_array, $data_atdptn, $data_pattern_all, $data_st, $data_job, $data_emp, $emp_id = "", $group_array = null, $sche) {

        global $arr_pos_value;
        global $arr_time_value;

        $arr_pos_value[] = "get_duty_shift_plan_array start duty_shift_common_class内 ";
        $arr_time_value[] = time();
        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $wk_cnt = count($calendar_array) - 1;
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[$wk_cnt]["date"];
        $second_date = $calendar_array[1]["date"]; //平均夜勤時間計算時前月末かどうかの確認用 20120301
        // 20140127 start
        // 勤務パターン情報キー位置
        $arr_data_pattarn_key = array();
        for ($k = 0; $k < count($data_pattern_all); $k++) {
            $wk_tm_gid = $data_pattern_all[$k]["pattern_id"];
            $wk_ptn_id = $data_pattern_all[$k]["atdptn_ptn_id"];
            $wk_reason_id = $data_pattern_all[$k]["reason"];
            if ($wk_ptn_id == "10") {
                $wk_key = $wk_tm_gid . "_" . $wk_ptn_id . "_" . $wk_reason_id;
            } else {
                $wk_key = $wk_tm_gid . "_" . $wk_ptn_id;
            }
            $arr_data_pattarn_key["$wk_key"] = $k;
        }
        //print_r($arr_data_pattarn_key);//debug
        // 20140127 end
        // 20140210
        $this->set_data_assoc($data_st, $data_job, $data_emp);

        $stride_flag = false;
        if (is_null($group_array)) {
            //年月から月またがり確認 20110221
            $wk_duty_yyyymm = $duty_yyyy . sprintf("%02d", $duty_mm);
            $month_flg = "1"; //当月から当月
            if (substr($start_date, 0, 6) != $wk_duty_yyyymm) {
                $month_flg = "0"; //前月から当月
            } elseif (substr($end_date, 0, 6) != $wk_duty_yyyymm) {
                $month_flg = "2"; //当月から翌月
            }
            //debug 20140509
            //$month_flg = "0"; //
        } else {
            //月単位出力対応　20130529
            $stride_flag = true;
            $start_month_flg1 = $group_array["start_month_flg1"];
            $start_day1 = $group_array["start_day1"];
            $month_flg1 = $group_array["month_flg1"];

            if ($start_month_flg1 == "1" && $start_day1 == "1" && $month_flg1 == "1") {
                //当月1日から当月末
                $month_flg = "1";
            } else {
                if ($start_month_flg1 == "0") {
                    $month_flg = "2";   //前月から当月
                } elseif ($month_flg1 == "2") {
                    $month_flg = "0";   //当月から翌月
                }
            }
        }
        $arr_pos_value[] = "勤務パターン情報キー位置設定等 ";
        $arr_time_value[] = time();
        //-------------------------------------------------------------------
        //ＤＢ(duty_shift_staff)より勤務シフトスタッフ情報を取得
        //ＤＢ(duty_shift_plan_staff)より勤務シフト情報（スタッフ情報）を取得
        //ＤＢ(duty_shift_plan_team)より勤務シフトチームを取得
        //-------------------------------------------------------------------
        $data_staff = $this->get_duty_shift_staff_array($group_id, $data_st, $data_job, $data_emp);
        $arr_pos_value[] = "get_duty_shift_staff_array スタッフ情報";
        $arr_time_value[] = time();
        $data_plan_staff = $this->get_duty_shift_plan_staff_array($group_id, $duty_yyyy, $duty_mm, $data_st, $data_job, $data_emp, $month_flg, $stride_flag);
        $arr_pos_value[] = "get_duty_shift_plan_staff_array スタッフ情報月別";
        $arr_time_value[] = time();
        $data_plan_team = $this->get_duty_shift_plan_team_array($group_id, $duty_yyyy, $duty_mm, $month_flg, $stride_flag);
        $arr_pos_value[] = "get_duty_shift_plan_team_array チーム";
        $arr_time_value[] = time();

        //*******************************************************************
        //スタッフの決定
        //*******************************************************************
        $data = array();
        $plan_rslt_flg = "";    //１：予定、２：実績、３：ＤＢ取得なし
        if (count($set_data) > 0) {
            //-------------------------------------------------------------------
            //勤務シフト情報の各名称設定
            //-------------------------------------------------------------------
            for ($i = 0; $i < count($set_data); $i++) {
                //-------------------------------------------------------------------
                //予定／実績判定
                //-------------------------------------------------------------------
                $plan_rslt_flg = $set_data[$i]["plan_rslt_flg"];
                //-------------------------------------------------------------------
                //設定
                //-------------------------------------------------------------------
                $data[$i] = $set_data[$i];
                $data[$i]["no"] = $i + 1;
                // 年
                $data[$i]["year"] = $duty_yyyy;
                // 月
                $data[$i]["month"] = $duty_mm;
                //-------------------------------------------------------------------
                //職員DBより職員名設定
                //-------------------------------------------------------------------
                // 20140210 start
//                for ($k=0;$k<count($data_emp);$k++) {
//                  if ($data[$i]["staff_id"]  == $data_emp[$k]["id"]) {
                $k = $this->data_emp_idx[$data[$i]["staff_id"]];
                $data[$i]["staff_name"] = $data_emp[$k]["name"];
                $data[$i]["sex"] = $data_emp[$k]["sex"];
//                      break;
//                  }
//              }
                // 20140210 end
                //-------------------------------------------------------------------
                //職種DBより職種設定
                //-------------------------------------------------------------------
                $data_staff_one = $this->get_duty_shift_staff_one($data[$i]["staff_id"], $data_st, $data_job, $data_emp);
                $data[$i]["job_id"] = $data_staff_one["job_id"];
                $data[$i]["job_name"] = $data_staff_one["job_name"];

                //登録時点役職
                for ($k = 0; $k < count($data_plan_staff); $k++) {
                    if ($data[$i]["staff_id"] == $data_plan_staff[$k]["id"]) {
                        $data[$i]["status"] = $data_plan_staff[$k]["status"];
                        $data[$i]["team_id"] = $data_plan_staff[$k]["team_id"];      //チームID
                        $data[$i]["team_name"] = $data_plan_staff[$k]["team_name"];    //チーム名
                        break;
                    }
                }
                //データがない場合はマスタの役職
                if ($data[$i]["status"] == "") {
                    $data[$i]["status"] = $data_staff_one["status"];
                }
                //チームがない場合は$data_plan_teamのチーム
                if ($data[$i]["team_id"] == "") {
                    $data[$i]["team_id"] = $data_plan_team[$data[$i]["staff_id"]]["team_id"];    //チームID
                    $data[$i]["team_name"] = $data_plan_team[$data[$i]["staff_id"]]["team_name"];  //チーム名
                }
                //チームがない場合は$data_staffのチーム
                if ($data[$i]["team_id"] == "") {
                    for ($k = 0; $k < count($data_staff); $k++) {
                        if ($data[$i]["staff_id"] == $data_staff[$k]["id"]) {
                            $data[$i]["team_id"] = $data_staff[$k]["team_id"];   //チームID
                            $data[$i]["team_name"] = $data_staff[$k]["team_name"]; //チーム名
                            break;
                        }
                    }
                }
            }
        } else {
            //-------------------------------------------------------------------
            //スタッフ情報設定
            //-------------------------------------------------------------------
            if (count($data_plan_staff) > 0) {
                //登録済み年月単位のスタッフ
                for ($i = 0; $i < count($data_plan_staff); $i++) {
                    $data[$i]["staff_id"] = $data_plan_staff[$i]["id"];               //スタッフＩＤ
                    $data[$i]["staff_name"] = $data_plan_staff[$i]["name"];             //指名
                    $data[$i]["job_id"] = $data_plan_staff[$i]["job_id"];           //職種ＩＤ
                    $data[$i]["job_name"] = $data_plan_staff[$i]["job_name"];         //職種名
                    $data[$i]["st_id"] = $data_plan_staff[$i]["st_id"];            //役職ID 20130219
                    $data[$i]["status"] = $data_plan_staff[$i]["status"];           //役職
                    $data[$i]["sex"] = $data_plan_staff[$i]["sex"];              //性別
                    $data[$i]["team_id"] = $data_plan_staff[$i]["team_id"];          //チームID
                    $data[$i]["team_name"] = $data_plan_staff[$i]["team_name"];        //チーム名
                }
            } else {
                //基本スタッフ
                for ($i = 0; $i < count($data_staff); $i++) {
                    $data[$i]["staff_id"] = $data_staff[$i]["id"];                //スタッフＩＤ
                    $data[$i]["staff_name"] = $data_staff[$i]["name"];              //指名
                    $data[$i]["job_id"] = $data_staff[$i]["job_id"];            //職種ＩＤ
                    $data[$i]["job_name"] = $data_staff[$i]["job_name"];          //職種名
                    $data[$i]["st_id"] = $data_plan_staff[$i]["st_id"];        //役職ID 20130219
                    $data[$i]["status"] = $data_staff[$i]["status"];            //役職
                    $data[$i]["sex"] = $data_staff[$i]["sex"];               //性別
                    $data[$i]["team_id"] = $data_staff[$i]["team_id"];           //チームID
                    $data[$i]["team_name"] = $data_staff[$i]["team_name"];         //チーム名
                    //data_plan_teamがあればそっちを使う
                    if ($data_plan_team[$data[$i]["staff_id"]]["team_id"] != "") {
                        $data[$i]["team_id"] = $data_plan_team[$data[$i]["staff_id"]]["team_id"];    //チームID
                        $data[$i]["team_name"] = $data_plan_team[$data[$i]["staff_id"]]["team_name"];  //チーム名
                    }
                }
            }
            //出勤表＞勤務シフト希望の場合、ログインユーザを先頭にする。
            if (strstr($this->file_name, "atdbk_duty_shift") && $emp_id != "") {
                $tmp_data = array();
                $j = 0;
                for ($i = 0; $i < count($data); $i++) {
                    if ($data[$i]["staff_id"] == $emp_id) {
                        $tmp_data[$j] = $data[$i];
                        $j++;
                        break;
                    }
                }
                for ($i = 0; $i < count($data); $i++) {
                    if ($data[$i]["staff_id"] != $emp_id) {
                        $tmp_data[$j] = $data[$i];
                        $j++;
                    }
                }
                $data = $tmp_data;
            }
        }
        $arr_pos_value[] = "スタッフの決定";
        $arr_time_value[] = time();
        //-------------------------------------------------------------------
        //予定／実績検索用
        //-------------------------------------------------------------------
        $cond_add = "";
        for ($i = 0; $i < count($data); $i++) {
            $wk_id = $data[$i]["staff_id"];
            if ($cond_add == "") {
                $cond_add .= "and (a.emp_id = '$wk_id' ";
            } else {
                $cond_add .= "or a.emp_id = '$wk_id' ";
            }
        }
        if ($cond_add != "") {
            $cond_add .= ") ";
        }
        //-------------------------------------------------------------------
        //下書き／希望／コメント検索用
        //-------------------------------------------------------------------
        $cond_add_2 = "";
        for ($i = 0; $i < count($data); $i++) {
            $wk_id = $data[$i]["staff_id"];
            if ($cond_add_2 == "") {
                $cond_add_2 .= "and (emp_id = '$wk_id' ";
            } else {
                $cond_add_2 .= "or emp_id = '$wk_id' ";
            }
        }
        if ($cond_add_2 != "") {
            $cond_add_2 .= ") ";
        }

        //*******************************************************************
        //勤務シフト情報の編集
        //*******************************************************************
        //-------------------------------------------------------------------
        //情報設定（勤務条件）
        //-------------------------------------------------------------------
        for ($i = 0; $i < count($data); $i++) {
            //-------------------------------------------------------------------
            //ＤＢ(duty_shift_staff_employmen)より勤務シフトスタッフ情報(個別勤務条件)を取得
            //-------------------------------------------------------------------
            $data_staff_employmen = $this->get_duty_shift_staff_employmen($data[$i]["staff_id"]);
            //-------------------------------------------------------------------
            //勤務条件を設定
            //-------------------------------------------------------------------
            $data[$i]["ward_pattern_id"] = $data_staff_employmen["ward_pattern_id"];        //所属病棟の出勤グループＩＤ；
            $data[$i]["other_post_flg"] = $data_staff_employmen["other_post_flg"];          //他部署兼務（１：有り）
            $data[$i]["duty_form"] = $data_staff_employmen["duty_form"];                    //勤務形態（1:常勤、2:非常勤）
            $data[$i]["duty_mon_day_flg"] = $data_staff_employmen["duty_mon_day_flg"];      //勤務日（月曜日）（１：有り）
            $data[$i]["duty_tue_day_flg"] = $data_staff_employmen["duty_tue_day_flg"];      //勤務日（火曜日）（１：有り）
            $data[$i]["duty_wed_day_flg"] = $data_staff_employmen["duty_wed_day_flg"];      //勤務日（水曜日）（１：有り）
            $data[$i]["duty_thurs_day_flg"] = $data_staff_employmen["duty_thurs_day_flg"];  //勤務日（木曜日）（１：有り）
            $data[$i]["duty_fri_day_flg"] = $data_staff_employmen["duty_fri_day_flg"];      //勤務日（金曜日）（１：有り）
            $data[$i]["duty_sat_day_flg"] = $data_staff_employmen["duty_sat_day_flg"];      //勤務日（土曜日）（１：有り）
            $data[$i]["duty_sun_day_flg"] = $data_staff_employmen["duty_sun_day_flg"];      //勤務日（日曜日）（１：有り）
            $data[$i]["duty_hol_day_flg"] = $data_staff_employmen["duty_hol_day_flg"];      //勤務日（祝日）（１：有り）
            $data[$i]["night_duty_flg"] = $data_staff_employmen["night_duty_flg"];      //夜勤の有無（１：有り、２：夜専）
            $data[$i]["night_staff_cnt"] = $data_staff_employmen["night_staff_cnt"];    //夜勤従事者への計上人数
            $data[$i]["tmcd_group_id"] = $data_staff_employmen["tmcd_group_id"];    //職員登録＞勤務条件のパターングループ20140203
            //-------------------------------------------------------------------
            //勤務条件を設定（勤務可能なシフト）
            //-------------------------------------------------------------------
            for ($k = 0; $k < 99; $k++) {
                if ($data_staff_employmen["week_flg_$k"] != "") {
                    $data[$i]["week_flg_$k"] = $data_staff_employmen["week_flg_$k"];
                }            //月曜〜金曜
                if ($data_staff_employmen["sat_flg_$k"] != "") {
                    $data[$i]["sat_flg_$k"] = $data_staff_employmen["sat_flg_$k"];
                }               //土曜
                if ($data_staff_employmen["sun_flg_$k"] != "") {
                    $data[$i]["sun_flg_$k"] = $data_staff_employmen["sun_flg_$k"];
                }               //日曜
                if ($data_staff_employmen["holiday_flg_$k"] != "") {
                    $data[$i]["holiday_flg_$k"] = $data_staff_employmen["holiday_flg_$k"];
                }   //祝日
            }
        }
        $arr_pos_value[] = "情報設定（勤務条件）";
        $arr_time_value[] = time();

        //*******************************************************************
        //勤務予定情報(atdbk)を追加
        //*******************************************************************
        //-------------------------------------------------------------------
        // 検索条件（職員）
        //-------------------------------------------------------------------
        $wk_cond_add = "";
        // "1"を追加 2008/5/26
        if (($plan_rslt_flg == "") || ($plan_rslt_flg == "1") || ($plan_rslt_flg == "2")) {
            if (($individual_flg != "1") && ($hope_get_flg != "1")) {
                //if (count($data_plan_staff) > 0) { //新規作成時、他病棟へ応援追加された分が取得されないのでコメント化 20121022
                $wk_cond_add = $cond_add;
                //}
            }
        }
        if ($wk_cond_add == "") {
            //職員追加時、追加職員のデータ取得
            for ($i = 0; $i < count($data); $i++) {
                if ($data[$i]["add_staff_flg"] == "1") {
                    $wk_id = $data[$i]["staff_id"];
                    if ($wk_cond_add == "") {
                        $wk_cond_add .= "and (a.emp_id = '$wk_id' ";
                    } else {
                        $wk_cond_add .= "or a.emp_id = '$wk_id' ";
                    }
                }
            }
            if ($wk_cond_add != "") {
                $wk_cond_add .= ") ";
            }
        }
        //-------------------------------------------------------------------
        // 勤務予定情報を取得
        //-------------------------------------------------------------------
        if ($wk_cond_add != "") {
            //-------------------------------------------------------------------
            // 勤務予定情報を取得
            // ※勤務予定情報を取得　又は　当初予定を取得
            //-------------------------------------------------------------------
            //$wk_data2 = $this->get_atdbk_atdbkrslt_array("atdbk", $wk_cond_add, $calendar_array);
            $wk_data2 = $this->get_atdbk_atdbkrslt_array("atdbk" . $sche, $wk_cond_add, $calendar_array, $data, $sche);
            //-------------------------------------------------------------------
            //情報を設定
            //-------------------------------------------------------------------
            for ($i = 0; $i < count($data); $i++) {
                for ($k = 0; $k < count($wk_data2); $k++) {
                    if ($wk_data2[$k]["staff_id"] == $data[$i]["staff_id"]) {
                        //日ごとの設定
                        for ($m = 1; $m <= $day_cnt; $m++) {
                            if ($wk_data2[$k]["atdptn_ptn_id_$m"] != "") {
                                $data[$i]["pattern_id_$m"] = $wk_data2[$k]["pattern_id_$m"];        //出勤グループＩＤ
                                $data[$i]["atdptn_ptn_id_$m"] = $wk_data2[$k]["atdptn_ptn_id_$m"];  //出勤パターンＩＤ
                                $data[$i]["reason_$m"] = $wk_data2[$k]["reason_$m"];                //事由
                                $data[$i]["reason_2_$m"] = $wk_data2[$k]["reason_2_$m"];            //事由（午前有給／午後有給）
                            }
                            // 計算時の不具合対応、初期化、再設定 2008/05/26
                            // wk_data2(DB)が未設定ならばクリア
                            if ($wk_data2[$k]["atdptn_ptn_id_$m"] == "") {
                                $data[$i]["pattern_id_$m"] = "";    //出勤グループＩＤ
                                $data[$i]["atdptn_ptn_id_$m"] = ""; //出勤パターンＩＤ
                                $data[$i]["reason_$m"] = "";        //事由
                                $data[$i]["reason_2_$m"] = "";      //事由（午前有給／午後有給）
                            }
                            // 画面のデータを再設定
                            // 計算時の開始日は引数の配列から取得
                            if ($set_data[$i]["comp_flg"] == "1") {
                                $tmp_start_day = intval(substr($calendar_array[1]["date"], 6, 2));
                            } else {
                                // 計算以外の場合は1日から
                                $tmp_start_day = 1;
                            }
                            // 入力項目の番号 位置あわせのため-1する
                            $tmp_idx = $tmp_start_day + $m - 1;
                            // set_data(画面上のデータ)がある場合、再設定
                            if ($set_data[$i]["pattern_id_$tmp_idx"] != "") {
                                $data[$i]["pattern_id_$m"] = $set_data[$i]["pattern_id_$tmp_idx"];  //出勤グループＩＤ
                                $data[$i]["atdptn_ptn_id_$m"] = $set_data[$i]["atdptn_ptn_id_$tmp_idx"];    //出勤パターンＩＤ
                                $data[$i]["reason_$m"] = $set_data[$i]["reason_$tmp_idx"];      //事由
                                $data[$i]["reason_2_$m"] = $set_data[$i]["reason_2_$tmp_idx"];      //事由（午前有給／午後有給）
                            }
                        }
                    }
                }
            }
        }
        $arr_pos_value[] = "勤務予定情報(atdbk)";
        $arr_time_value[] = time();

        //*******************************************************************
        //勤務実績情報(atdbkrslt)を追加
        //*******************************************************************
        $wk_cond_add = "";
        if (($plan_rslt_flg == "") || ($plan_rslt_flg == "1")) {
            if ($show_data_flg == "2") {
                $wk_cond_add = $cond_add_2;
            } else {
                $wk_cond_add = $cond_add;
            }
        }
        if ($wk_cond_add == "") {
            if ($show_data_flg == "2") {
                $wk = "emp_id";
            } else {
                $wk = "a.emp_id";
            }
            //職員追加時、追加職員のデータ取得
            for ($i = 0; $i < count($data); $i++) {
                if ($data[$i]["add_staff_flg"] == "1") {
                    $wk_id = $data[$i]["staff_id"];
                    if ($wk_cond_add == "") {
                        $wk_cond_add .= "and (" . $wk . " = '$wk_id' ";
                    } else {
                        $wk_cond_add .= "or " . $wk . " = '$wk_id' ";
                    }
                }
            }
            if ($wk_cond_add != "") {
                $wk_cond_add .= ") ";
            }
        }
        if ($wk_cond_add != "") {
            //-------------------------------------------------------------------
            // 勤務実績情報を設定
            //-------------------------------------------------------------------
            if ($show_data_flg == "1") {
                //-------------------------------------------------------------------
                // 勤務実績情報を取得
                //-------------------------------------------------------------------
                $data_rslt = $this->get_atdbk_atdbkrslt_array("atdbkrslt", $wk_cond_add, $calendar_array, $data);
                //-------------------------------------------------------------------
                //取得情報を追加
                //-------------------------------------------------------------------
                for ($i = 0; $i < count($data); $i++) {
                    for ($k = 0; $k < count($data_rslt); $k++) {
                        if ($data[$i]["staff_id"] == $data_rslt[$k]["staff_id"]) {
                            //日ごとの設定
                            for ($m = 1; $m <= $day_cnt; $m++) {
                                $data[$i]["rslt_start_time_$m"] = $data_rslt[$k]["start_time_$m"];          //勤務開始時刻
                                $data[$i]["rslt_end_time_$m"] = $data_rslt[$k]["end_time_$m"];              //勤務終了時刻
                                $data[$i]["rslt_meeting_time_$m"] = $data_rslt[$k]["meeting_time_$m"];      //研修・会議時間
                                $data[$i]["rslt_out_time_$m"] = $data_rslt[$k]["out_time_$m"];      //外出時刻
                                $data[$i]["rslt_ret_time_$m"] = $data_rslt[$k]["ret_time_$m"];      //復帰時刻
                                $data[$i]["rslt_meeting_start_time_$m"] = $data_rslt[$k]["meeting_start_time_$m"];      //研修・会議開始時刻
                                $data[$i]["rslt_meeting_end_time_$m"] = $data_rslt[$k]["meeting_end_time_$m"];      //研修・会議終了時刻

                                if ($data_rslt[$k]["atdptn_ptn_id_$m"] != "") {
                                    $data[$i]["rslt_pattern_id_$m"] = $data_rslt[$k]["pattern_id_$m"];      //出勤グループＩＤ
                                    $data[$i]["rslt_id_$m"] = $data_rslt[$k]["atdptn_ptn_id_$m"];           //出勤パターンＩＤ
                                    $data[$i]["rslt_reason_$m"] = $data_rslt[$k]["reason_$m"];              //事由
                                    $data[$i]["rslt_reason_2_$m"] = $data_rslt[$k]["reason_2_$m"];          //事由（午前有給／午後有給）
                                }
                                $data[$i]["night_duty_$m"] = $data_rslt[$k]["night_duty_$m"];   //当直（１：有り、２：無し）
                                $data[$i]["next_day_flag_$m"] = $data_rslt[$k]["next_day_flag_$m"]; //
                                $data[$i]["previous_day_flag_$m"] = $data_rslt[$k]["previous_day_flag_$m"]; //
                                $data[$i]["paid_hol_start_time_$m"] = $data_rslt[$k]["paid_hol_use_hour_$m"]; // 研修・会議終了時刻 2014/10
                                $data[$i]["paid_hol_use_hour_$m"] = $data_rslt[$k]["paid_hol_use_hour_$m"]; // 研修・会議終了時刻 2014/10
                                $data[$i]["paid_hol_use_minute_$m"] = $data_rslt[$k]["paid_hol_use_minute_$m"]; // 研修・会議終了時刻 2014/10
                            }
                        }
                    }
                }
            }
            $arr_pos_value[] = "勤務実績情報(atdbkrslt)";
            $arr_time_value[] = time();

            //-------------------------------------------------------------------
            // 勤務希望情報を設定
            //-------------------------------------------------------------------
            if ($show_data_flg == "2") {
                //-------------------------------------------------------------------
                //勤務希望情報を取得
                //-------------------------------------------------------------------
                $sql = "select * from duty_shift_plan_individual ";
                $cond = "where 1 = 1 ";
                $cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
                $cond .= $wk_cond_add;
                $cond .= "order by emp_id, duty_date";
                $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                if ($sel == 0) {
                    pg_close($this->_db_con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $num = pg_num_rows($sel);
                //-------------------------------------------------------------------
                //勤務シフト情報の編集設定
                //-------------------------------------------------------------------
                $data_rslt = $this->get_duty_shift_plan_set($sel, $num, $day_cnt, $data_st, $data_job, $data_emp, $data_pattern_all, $calendar_array);
                //-------------------------------------------------------------------
                //取得情報を追加
                //-------------------------------------------------------------------
                for ($i = 0; $i < count($data); $i++) {
                    //画面データの位置確認
                    $g_idx = 0;
                    for ($g_idx = 0; $g_idx < count($set_data); $g_idx++) {
                        if ($data[$i]["staff_id"] == $set_data[$g_idx]["staff_id"]) {
                            break;
                        }
                    }
                    //DBデータの位置確認
                    for ($k = 0; $k < count($data_rslt); $k++) {
                        if ($data[$i]["staff_id"] == $data_rslt[$k]["staff_id"]) {
                            break;
                        }
                    }
                    //日ごとの設定
                    for ($m = 1; $m <= $day_cnt; $m++) {
                        // 画面のデータがある場合は設定 2008/6/13
                        if ($set_data[$g_idx]["rslt_pattern_id_$m"] != "") {
                            $data[$i]["rslt_pattern_id_$m"] = $set_data[$g_idx]["rslt_pattern_id_$m"];      //出勤グループＩＤ
                            $data[$i]["rslt_id_$m"] = $set_data[$g_idx]["rslt_id_$m"];          //出勤パターンＩＤ
                            $data[$i]["rslt_reason_$m"] = $set_data[$g_idx]["rslt_reason_$m"];              //事由
                            $data[$i]["rslt_reason_2_$m"] = $set_data[$g_idx]["rslt_reason_2_$m"];          //事由（午前有給／午後有給）
                            $data[$i]["rslt_assist_group_$m"] = $set_data[$g_idx]["rslt_assist_group_$m"];          //応援追加情報、希望用
                        } else
                        // DBのデータを設定
                        if ($data_rslt[$k]["atdptn_ptn_id_$m"] != "") {
                            $data[$i]["rslt_pattern_id_$m"] = $data_rslt[$k]["pattern_id_$m"];      //出勤グループＩＤ
                            $data[$i]["rslt_id_$m"] = $data_rslt[$k]["atdptn_ptn_id_$m"];           //出勤パターンＩＤ
                            $data[$i]["rslt_reason_$m"] = $data_rslt[$k]["reason_$m"];              //事由
                            $data[$i]["rslt_reason_2_$m"] = $data_rslt[$k]["reason_2_$m"];          //事由（午前有給／午後有給）
                            $data[$i]["rslt_assist_group_$m"] = $data_rslt[$k]["group_id_$m"];          //応援追加情報、希望用
                        }
                    }
                }
                //-------------------------------------------------------------------
                //データ未登録の場合、希望日（休暇）を自動設定
                //-------------------------------------------------------------------
                for ($i = 0; $i < count($data); $i++) {
                    $wk_flg = "";
                    for ($m = 1; $m <= $day_cnt; $m++) {
                        if ($data[$i]["rslt_id_$m"] != "") {
                            $wk_flg = "1";
                        }
                    }
                    if ($wk_flg == "") {
                        //-------------------------------------------------------------------
                        //データ未登録の場合、希望日（休暇）を自動設定
                        //-------------------------------------------------------------------
                        $wk_data = $this->get_duty_shift_plan_array_2(
                                $data, $i, $group_id, $pattern_id, $start_date, $end_date, $day_cnt, $data_week, $data_atdptn, $data_pattern_all);
                        for ($k = 1; $k <= $day_cnt; $k++) {
                            $data[$i]["rslt_id_$k"] = $wk_data["atdptn_ptn_id_$k"]; //出勤パターンＩＤ
                            $data[$i]["rslt_font_name_$k"] = $wk_data["font_name_$k"];          //文字（勤務状況名）
                            $data[$i]["rslt_font_color_$k"] = $wk_data["font_color_$k"];            //文字（色）
                            $data[$i]["rslt_back_color_$k"] = $wk_data["back_color_$k"];            //文字（背景色）
                            $data[$i]["rslt_count_kbn_gyo_$k"] = $wk_data["count_kbn_gyo_$k"];  //集計行（出勤パターンＩＤ）
                            $data[$i]["rslt_count_kbn_retu_$k"] = $wk_data["count_kbn_retu_$k"];    //集計列（出勤パターンＩＤ）
                            $data[$i]["rslt_reason_$k"] = $wk_data["reason_$k"];                    //事由（１：有給）
                            $data[$i]["rslt_reason_2_$k"] = $wk_data["reason_2_$k"];                //事由（午前有給／午後有給）
                        }
                    }
                }
                $arr_pos_value[] = "勤務希望情報(duty_shift_plan_individual)";
                $arr_time_value[] = time();
            }
            //-------------------------------------------------------------------
            // 当直情報を設定
            //-------------------------------------------------------------------
            if ($show_data_flg == "3") {
                //-------------------------------------------------------------------
                // 当直情報を取得
                // ※勤務予定情報を取得　又は　当初予定を取得
                //-------------------------------------------------------------------
                //$data_rslt = $this->get_atdbk_atdbkrslt_array("atdbk", $wk_cond_add, $calendar_array);
                $data_rslt = $this->get_atdbk_atdbkrslt_array("atdbk" . $sche, $wk_cond_add, $calendar_array, $data, $sche);
                //-------------------------------------------------------------------
                //取得情報を追加
                //-------------------------------------------------------------------
                for ($i = 0; $i < count($data); $i++) {
                    for ($k = 0; $k < count($data_rslt); $k++) {
                        if ($data[$i]["staff_id"] == $data_rslt[$k]["staff_id"]) {
                            //日ごとの設定
                            for ($m = 1; $m <= $day_cnt; $m++) {
                                if ($data_rslt[$k]["night_duty_$m"] != "") {
                                    $data[$i]["night_duty_$m"] = $data_rslt[$k]["night_duty_$m"];   //当直（１：有り、２：無し）
                                }
                            }
                        }
                    }
                }
                $arr_pos_value[] = "当直情報";
                $arr_time_value[] = time();
            }
        }

        //*******************************************************************
        //コメント情報を追加
        //*******************************************************************
        $wk_cond_add = "";
        if ($plan_rslt_flg == "") {
            $wk_cond_add = $cond_add_2;
        }
        if ($wk_cond_add == "") {
            //職員追加時、追加職員のデータ取得
            for ($i = 0; $i < count($data); $i++) {
                if ($data[$i]["add_staff_flg"] == "1") {
                    $wk_id = $data[$i]["staff_id"];
                    if ($wk_cond_add == "") {
                        $wk_cond_add .= "and (emp_id = '$wk_id' ";
                    } else {
                        $wk_cond_add .= "or emp_id = '$wk_id' ";
                    }
                }
            }
            if ($wk_cond_add != "") {
                $wk_cond_add .= ") ";
            }
        }

        if ($wk_cond_add != "") {
            //-------------------------------------------------------------------
            //データ取得
            //-------------------------------------------------------------------
            //取得方法変更 20140214
            $wk_cal_start_date = $calendar_array[0]["date"];
            for ($i = 0; $i < count($data); $i++) {
                $wk_staff_id = $data[$i]["staff_id"];
                $sql = "select * from duty_shift_plan_comment ";
                $cond = "where emp_id = '$wk_staff_id' ";
                $cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
                $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                if ($sel == 0) {
                    pg_close($this->_db_con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $num = pg_num_rows($sel);
                //-------------------------------------------------------------------
                //勤務シフト情報の編集設定
                //-------------------------------------------------------------------
                for ($j = 0; $j < $num; $j++) {
                    //-------------------------------------------------------------------
                    //ＤＢより取得値
                    //-------------------------------------------------------------------
                    $key_duty_date = trim(pg_fetch_result($sel, $j, "duty_date"));            //勤務年月日
                    $key_comment = trim(pg_fetch_result($sel, $j, "comment"));                //コメント
                    //-------------------------------------------------------------------
                    //指定日に設定
                    //-------------------------------------------------------------------
                    $m = ((date_utils::to_timestamp_from_ymd($key_duty_date) - date_utils::to_timestamp_from_ymd($wk_cal_start_date)) / (24 * 3600)) + 1;
                    $data[$i]["comment_$m"] = $key_comment; //出勤グループＩＤ
                }
            }

            /*
              $sql = "select * from duty_shift_plan_comment ";
              $cond = "where 1 = 1 ";
              $cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
              $cond .= $wk_cond_add;
              $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
              if ($sel == 0) {
              pg_close($this->_db_con);
              echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
              echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
              exit;
              }
              $num = pg_num_rows($sel);
              //-------------------------------------------------------------------
              //勤務シフト情報の編集設定
              //-------------------------------------------------------------------
              $data_rslt = $this->get_duty_shift_plan_set_comment($sel, $num, $day_cnt, $calendar_array);
              //-------------------------------------------------------------------
              //情報を設定
              //-------------------------------------------------------------------
              for ($i=0;$i<count($data);$i++) {
              for ($k=0;$k<count($data_rslt);$k++) {
              if ($data[$i]["staff_id"] == $data_rslt[$k]["staff_id"]) {
              for ($m=1;$m<=$day_cnt;$m++) {
              if ($data_rslt[$k]["comment_$m"] != "") {
              $data[$i]["comment_$m"] = $data_rslt[$k]["comment_$m"];         //コメント
              }
              }
              }
              }
              }
             */
        }
        $arr_pos_value[] = "コメント情報";
        $arr_time_value[] = time();

        //*******************************************************************
        //勤務シフト情報ＤＢより情報取得
        //希望　又は　下書き
        //*******************************************************************
        $wk_cond_add = "";
        if (($plan_rslt_flg == "") || ($plan_rslt_flg == "2") || ($hope_get_flg == "1") || $set_data[0]["auto_flg"] == "1" //自動作成の場合（下書きを変更しないための情報取得）20130315
        ) {
            $wk_cond_add = $cond_add_2;
        }
        if ($wk_cond_add == "") {
            //職員追加時、追加職員のデータ取得
            for ($i = 0; $i < count($data); $i++) {
                if ($data[$i]["add_staff_flg"] == "1") {
                    $wk_id = $data[$i]["staff_id"];
                    if ($wk_cond_add == "") {
                        $wk_cond_add .= "and (emp_id = '$wk_id' ";
                    } else {
                        $wk_cond_add .= "or emp_id = '$wk_id' ";
                    }
                }
            }
            if ($wk_cond_add != "") {
                $wk_cond_add .= ") ";
            }
        }
        if ($wk_cond_add != "") {
            //-------------------------------------------------------------------
            //「勤務取り込み希勤」の場合、希望データ取得
            //-------------------------------------------------------------------
            if ($hope_get_flg == "1") {
                $individual_flg = "1";
            }
            //-------------------------------------------------------------------
            //ＤＢ名称設定
            //-------------------------------------------------------------------
            if ($individual_flg == "1") {
                $db_name = "duty_shift_plan_individual";    // 勤務シフト情報（希望）を取得
            } else {
                $db_name = "duty_shift_plan_draft";         // 勤務シフト情報（下書き）を取得
            }

            //-------------------------------------------------------------------
            //希望データ取得時は初期化
            //-------------------------------------------------------------------
            if ($individual_flg == "1") {
                //-------------------------------------------------------------------
                //初期化
                //-------------------------------------------------------------------
                for ($i = 0; $i < count($data); $i++) {
                    for ($m = 1; $m <= $day_cnt; $m++) {
                        $data[$i]["pattern_id_$m"] = "";
                        $data[$i]["atdptn_ptn_id_$m"] = "";
                        $data[$i]["reason_$m"] = "";
                        $data[$i]["reason_2_$m"] = "";
                    }
                }
            }
            //-------------------------------------------------------------------
            //データ取得
            //-------------------------------------------------------------------
            $sql = "select * from " . $db_name;
            $cond = "where 1 = 1 ";
            $cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
            $cond .= $wk_cond_add;
            $cond .= "order by emp_id, duty_date";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $num = pg_num_rows($sel);
            //-------------------------------------------------------------------
            //勤務シフト情報の編集設定
            //-------------------------------------------------------------------
            $wk_data2 = $this->get_duty_shift_plan_set($sel, $num, $day_cnt, $data_st, $data_job, $data_emp, $data_pattern_all, $calendar_array);

//職員別下書き有無確認
            $arr_emp_draft_flg = array();
            global $refer_flg;
//勤務シフト参照で下書きの場合は、取得しない 2008/09/22
//echo("refer_flg=$refer_flg individual_flg=$individual_flg");
            if ($refer_flg == "1" && $individual_flg != "1") {
                for ($i = 0; $i < count($wk_data2); $i++) {
                    $wk_emp_id = $wk_data2[$i]["staff_id"];
                    for ($m = 1; $m <= $day_cnt; $m++) {
                        if ($wk_data2[$i]["atdptn_ptn_id_$m"] != "") {
                            $arr_emp_draft_flg[$wk_emp_id] = "1";
                            break;
                        }
                    }
                }
            }
            //-------------------------------------------------------------------
            //情報を設定
            //-------------------------------------------------------------------

            for ($i = 0; $i < count($data); $i++) {
//下書きがある場合、$dataをクリアし$wk_data2からの設定をしない
                $wk_emp_id = $data[$i]["staff_id"];
                if (($refer_flg == "1" && $individual_flg != "1") && $arr_emp_draft_flg[$wk_emp_id] == "1") {
                    for ($m = 1; $m <= $day_cnt; $m++) {
                        $data[$i]["pattern_id_$m"] = "";
                        $data[$i]["atdptn_ptn_id_$m"] = "";
                        $data[$i]["reason_$m"] = "";
                        $data[$i]["reason_2_$m"] = "";
                    }
                    continue;
                }

                //画面データの位置確認
                $g_idx = 0;
                for ($g_idx = 0; $g_idx < count($set_data); $g_idx++) {
                    if ($data[$i]["staff_id"] == $set_data[$g_idx]["staff_id"]) {
                        break;
                    }
                }
                //DBデータの位置確認
                for ($k = 0; $k < count($wk_data2); $k++) {
                    if ($data[$i]["staff_id"] == $wk_data2[$k]["staff_id"]) {
                        break;
                    }
                }

                //日ごとの設定
                for ($m = 1; $m <= $day_cnt; $m++) {
                    // 希望取り込み
                    if ($hope_get_flg == "1") {
//echo("<br> data $m atdptn_ptn_id_=".$set_data[$g_idx]["atdptn_ptn_id_$m"]); //debug
                        // 予定がある場合、希望を取り込まない 2008/10/29
                        if ($set_data[$g_idx]["atdptn_ptn_id_$m"] != "") {
                            // 画面の予定を設定
                            $data[$i]["pattern_id_$m"] = $set_data[$g_idx]["pattern_id_$m"];        //出勤グループＩＤ
                            $data[$i]["atdptn_ptn_id_$m"] = $set_data[$g_idx]["atdptn_ptn_id_$m"];          //出勤パターンＩＤ
                            $data[$i]["reason_$m"] = $set_data[$g_idx]["reason_$m"];                //事由
                            $data[$i]["reason_2_$m"] = $set_data[$g_idx]["reason_2_$m"];            //事由（午前有給／午後有給）
                        } else
                        if ($set_data[$g_idx]["rslt_pattern_id_$m"] != "") {
                            // 画面の希望データがある場合は設定
                            $data[$i]["pattern_id_$m"] = $set_data[$g_idx]["rslt_pattern_id_$m"];       //出勤グループＩＤ
                            $data[$i]["atdptn_ptn_id_$m"] = $set_data[$g_idx]["rslt_id_$m"];            //出勤パターンＩＤ
                            $data[$i]["reason_$m"] = $set_data[$g_idx]["rslt_reason_$m"];               //事由
                            $data[$i]["reason_2_$m"] = $set_data[$g_idx]["rslt_reason_2_$m"];           //事由（午前有給／午後有給）
                        } else
                        // DBのデータを設定
                        if ($wk_data2[$k]["atdptn_ptn_id_$m"] != "") {
                            $data[$i]["pattern_id_$m"] = $wk_data2[$k]["pattern_id_$m"];        //出勤グループＩＤ
                            $data[$i]["atdptn_ptn_id_$m"] = $wk_data2[$k]["atdptn_ptn_id_$m"];  //出勤パターンＩＤ
                            $data[$i]["reason_$m"] = $wk_data2[$k]["reason_$m"];                //事由
                            $data[$i]["reason_2_$m"] = $wk_data2[$k]["reason_2_$m"];            //事由（午前有給／午後有給）
                        }
                    } else
                    // DBのデータを設定
                    if ($wk_data2[$k]["atdptn_ptn_id_$m"] != "") {
                        $data[$i]["pattern_id_$m"] = $wk_data2[$k]["pattern_id_$m"];        //出勤グループＩＤ
                        $data[$i]["atdptn_ptn_id_$m"] = $wk_data2[$k]["atdptn_ptn_id_$m"];  //出勤パターンＩＤ
                        $data[$i]["reason_$m"] = $wk_data2[$k]["reason_$m"];                //事由
                        $data[$i]["reason_2_$m"] = $wk_data2[$k]["reason_2_$m"];            //事由（午前有給／午後有給）
                        $data[$i]["draft_$m"] = "1"; //下書き 20130315
                    } else {
                        // 2008/11/14 下書きのシフトのクリア
                        // $numがあれば下書きと判断
//                  if ($num > 0) {
                        // 2008/12/24 条件変更
                        if (($refer_flg == "1" && $individual_flg != "1") && $arr_emp_draft_flg[$wk_emp_id] == "1") {
                            $data[$i]["atdptn_ptn_id_$m"] = ""; //出勤パターンＩＤ
                            $data[$i]["reason_$m"] = "";        //事由
                            $data[$i]["reason_2_$m"] = "";      //事由（午前有給／午後有給）
                        }
                    }
                }
            }
            $arr_pos_value[] = "希望　又は　下書き";
            $arr_time_value[] = time();
        }

        //他病棟と重複スタッフ情報
        //取得方法変更 20140212 start
        $arr_other_staff_flg = array();
        //月別の所属確認、応援先から削除された場合の対処 20091019
        $arr_group_emp_id = array();
        for ($i = 0; $i < count($data); $i++) {
            $wk_id = $data[$i]["staff_id"];
            $sql = "select a.emp_id, a.group_id from duty_shift_plan_staff a";
            $cond = "where a.emp_id = '$wk_id' ";
            $cond .= "and a.duty_yyyy = $duty_yyyy ";
            $cond .= "and a.duty_mm = $duty_mm ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $num = pg_num_rows($sel);
            for ($j = 0; $j < $num; $j++) {
                $wk_staff_id = pg_fetch_result($sel, $j, "emp_id");
                $wk_group_id = pg_fetch_result($sel, $j, "group_id");
                if ($wk_group_id != $group_id) {
                    $arr_other_staff_flg["$wk_staff_id"] = "1";
                }
                $arr_group_emp_id["$wk_group_id"]["$wk_staff_id"] = "1";
            }
        }
        //取得方法変更 20140212 end
        /*
          $cond_add = "";
          for ($i=0;$i<count($data);$i++) {
          $wk_id = $data[$i]["staff_id"];
          if ($cond_add == "") {
          $cond_add .= " and (a.emp_id = '$wk_id' ";
          } else {
          $cond_add .= "or a.emp_id = '$wk_id' ";
          }
          }
          if ($cond_add != "") {
          $cond_add .= ") ";
          }
          $sql = "select a.emp_id from duty_shift_plan_staff a";
          $cond = "where a.group_id != '$group_id' ";
          $cond .= "$cond_add ";
          $cond .= "and a.duty_yyyy = $duty_yyyy ";
          $cond .= "and a.duty_mm = $duty_mm ";
          $cond .= "order by a.emp_id";
          $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
          if ($sel == 0) {
          pg_close($this->_db_con);
          echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
          echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
          exit;
          }

          $arr_other_staff_flg = array();
          $num = pg_num_rows($sel);
          for ($i=0;$i<$num;$i++) {
          $wk_staff_id = pg_fetch_result($sel,$i,"emp_id");
          $arr_other_staff_flg["$wk_staff_id"] = "1";
          }
         */

        //職員の所属するグループID
        $belong_group_array = array();

        //職員が割当てられたグループ（過去の所属グループ設定用）
        //$assign_group_array = array();
        //職員が割当てられたグループの数（過去の所属グループ設定用）
        //$assign_group_cnt_array = array();
        //月別の所属確認、応援先から削除された場合の対処 20091019
        //$arr_group_emp_id = array();
        //上の処理で取得 20140212
//月毎のスタッフ情報から取得
//      $sql = "select a.emp_id, a.main_group_id, a.group_id from duty_shift_plan_staff a ";
//      $cond = " where (a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm)  "; //$cond_add
//      $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
//      if ($sel == 0) {
//          pg_close($this->_db_con);
//          echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//          echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//          exit;
//      }
        //$num = pg_num_rows($sel);
        //for ($i=0;$i<$num;$i++) {
        //$wk_staff_id = pg_fetch_result($sel,$i,"emp_id");
        //$wk_group_id = pg_fetch_result($sel,$i,"group_id");
        //$wk_main_group_id = pg_fetch_result($sel,$i,"main_group_id");
        //if ($wk_main_group_id != "") {
        //  $belong_group_array["$wk_staff_id"] = $wk_main_group_id;
        //}
        //$assign_group_array["$wk_staff_id"] = $wk_group_id;
        //if ($assign_group_cnt_array["$wk_staff_id"] == "") {
        //  $assign_group_cnt_array["$wk_staff_id"] = 1;
        //} else {
        //  $assign_group_cnt_array["$wk_staff_id"]++;
        //}
        //$arr_group_emp_id["$wk_group_id"]["$wk_staff_id"] = "1";
        //}
        /* 主に所属しているグループを職員設定にするためコメント化 20110302
          /*
          //月毎のスタッフ情報にない場合で、割当られたグループが1つなら所属グループとする
          foreach ($assign_group_array as $wk_staff_id => $wk_group_id) {
          if ($belong_group_array["$wk_staff_id"] == "") {
          if ($assign_group_cnt_array["$wk_staff_id"] == 1) {
          $belong_group_array["$wk_staff_id"] = $assign_group_array["$wk_staff_id"];
          }
          }
          }
         */
        //シフト希望以外
        if (!($individual_flg == "1" && $hope_get_flg != "1")) {
            //応援追加データ　又は　当初予定を取得
            //取得方法変更 20140212 start
            for ($d_idx = 0; $d_idx < count($data); $d_idx++) {
                $wk_id = $data[$d_idx]["staff_id"];

                $sql = "select a.emp_id, a.duty_date, a.group_id, a.assist_flg, b.pattern, b.reason, b.tmcd_group_id, a.main_group_id from duty_shift_plan_assist" . $sche . " a left join atdbk" . $sche . " b on b.emp_id = a.emp_id and b.date = a.duty_date  ";
                $cond = "where a.emp_id = '$wk_id' ";
                $cond .= "and a.duty_date >= '$start_date' ";
                $cond .= "and a.duty_date <= '$end_date' ";
                $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                if ($sel == 0) {
                    pg_close($this->_db_con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                $num = pg_num_rows($sel);
                for ($i = 0; $i < $num; $i++) {
                    $wk_staff_id = pg_fetch_result($sel, $i, "emp_id");
                    $wk_duty_date = pg_fetch_result($sel, $i, "duty_date");
                    $wk_group_id = pg_fetch_result($sel, $i, "group_id");
                    $wk_assist_flg = pg_fetch_result($sel, $i, "assist_flg");
                    $wk_pattern = pg_fetch_result($sel, $i, "pattern");
                    $wk_reason = pg_fetch_result($sel, $i, "reason");
                    $wk_tmcd_group_id = pg_fetch_result($sel, $i, "tmcd_group_id");
                    //日付
                    $m = ((date_utils::to_timestamp_from_ymd($wk_duty_date) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
                    $data[$d_idx]["assist_group_$m"] = $wk_group_id;
                    $data[$d_idx]["assist_flg_$m"] = $wk_assist_flg;
                    //平均夜勤時間計算時の前月末の応援追加確認用データ 20120301
                    if ($wk_duty_date == $start_date &&
                            substr($start_date, 0, 6) != substr($second_date, 0, 6)) {
                        $data[$d_idx]["assist_group_0"] = $wk_group_id;
                    }
                    //日付毎のデータも設定
                    $data[$d_idx]["assist_group_$wk_duty_date"] = $wk_group_id;
                }
            }
        }
        //取得方法変更 20140212 end
        //職員設定から所属グループ取得
        //取得方法変更 20140212 start
        $arr_data_staff_group_id = array();
        for ($k = 0; $k < count($data_staff); $k++) {
            $wk_staff_id = $data_staff[$k]["id"];
            $arr_data_staff_group_id[$wk_staff_id] = $data_staff[$k]["group_id"];
        }
        for ($i = 0; $i < count($data); $i++) {
            $wk_staff_id = $data[$i]["staff_id"];
            //取得済みのdata_staff配列にあるか確認、ある場合は同じグループ
            if ($arr_data_staff_group_id[$wk_staff_id] != "") {
                $belong_group_array["$wk_staff_id"] = $group_id;
            }
            //ない場合応援追加のため、DBから確認
            else {
                $sql = "select a.emp_id, a.group_id as belong_group_id from duty_shift_staff a ";
                $cond = " where a.emp_id = '$wk_staff_id' ";
                $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                if ($sel == 0) {
                    pg_close($this->_db_con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $num = pg_num_rows($sel);
                if ($num > 0) {
                    $belong_group_array["$wk_staff_id"] = pg_fetch_result($sel, 0, "belong_group_id");
                }
            }
        }
//        $sql = "select a.emp_id, a.group_id as belong_group_id from duty_shift_staff a ";
//      $cond = " where true $cond_add ";
//      $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
//      if ($sel == 0) {
//          pg_close($this->_db_con);
//          echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//          echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//          exit;
//      }
//
//      $num = pg_num_rows($sel);
//      for ($i=0;$i<$num;$i++) {
//          $wk_staff_id = pg_fetch_result($sel,$i,"emp_id");
//          $wk_belong_group_id = pg_fetch_result($sel,$i,"belong_group_id");
//          //月毎のデータがない場合、職員設定の所属から設定
//          if ($wk_belong_group_id != "" && $belong_group_array["$wk_staff_id"] == "") {
//              $belong_group_array["$wk_staff_id"] = $wk_belong_group_id;
//          }
//      }
        //取得方法変更 20140212 end

        for ($i = 0; $i < count($data); $i++) {
            $wk_staff_id = $data[$i]["staff_id"];
            for ($m = 1; $m <= $day_cnt; $m++) {
                //シフト希望
                if ($individual_flg == "1" && $hope_get_flg != "1") {
                    // 画面の応援情報がある場合はそれを設定
                    if ($set_data[$i]["assist_group_$m"] != "") {
                        $data[$i]["assist_group_$m"] = $set_data[$i]["assist_group_$m"];
                    } else {
                        for ($k = 0; $k < count($wk_data2); $k++) {
                            if ($data[$i]["staff_id"] == $wk_data2[$k]["staff_id"]) {
                                if ($wk_data2[$k]["group_id_$m"] != "") {
                                    $data[$i]["assist_group_$m"] = $wk_data2[$k]["group_id_$m"];
                                }
                                break;
                            }
                        }
                    }
                } else {
                    // 画面の応援情報がある場合はそれを設定
                    if ($set_data[$i]["assist_group_$m"] != "") {
                        $data[$i]["assist_group_$m"] = $set_data[$i]["assist_group_$m"];
                    } else {
                        // 下書きにデータがある場合設定 20090219
                        for ($k = 0; $k < count($wk_data2); $k++) {
                            if ($data[$i]["staff_id"] == $wk_data2[$k]["staff_id"]) {
                                if ($wk_data2[$k]["atdptn_ptn_id_$m"] != "") {
                                    $data[$i]["assist_group_$m"] = $wk_data2[$k]["group_id_$m"];
                                }
                                break;
                            }
                        }

                        // シフト設定あり、DBの情報なし、所属するグループが別の場合設定
                        if ($data[$i]["atdptn_ptn_id_$m"] != "" &&
                                $data[$i]["assist_group_$m"] == "") {
                            // 所属するグループが別の場合設定
                            if ($belong_group_array["$wk_staff_id"] != $group_id) {
                                $data[$i]["assist_group_$m"] = $belong_group_array["$wk_staff_id"];
                            }
                        }
                    }
                }
                //未設定には処理中のグループを設定
                if ($data[$i]["assist_group_$m"] == "") {
                    $data[$i]["assist_group_$m"] = $group_id;
                }
                //応援先のグループが月別の所属にあるか確認し、ない場合は処理中のグループを設定
                else {
                    $wk_group_id = $data[$i]["assist_group_$m"];
                    $wk_emp_id = $data[$i]["staff_id"];
                    if ($arr_group_emp_id[$wk_group_id][$wk_emp_id] != "1") {
                        $data[$i]["assist_group_$m"] = $group_id;
                        $data[$i]["group_chg_flg"] = "1"; //更新フラグ設定用
                    }
                }
                if ($data[$i]["rslt_assist_group_$m"] == "") {
                    $data[$i]["rslt_assist_group_$m"] = $group_id;
                }
                //勤務パターングループ変更
                if ($data[$i]["assist_group_$m"] == $group_id &&
                        $data[$i]["pattern_id_$m"] != $pattern_id) {
                    $data[$i]["pattern_id_$m"] = $pattern_id;
                }
            }
            //平均夜勤時間計算時前月末のデータ 20120301
            if (substr($start_date, 0, 6) != substr($second_date, 0, 6)) {
                //未設定の場合、処理中のグループを設定
                if ($data[$i]["assist_group_0"] == "") {
                    $wk_staff_id = $data[$i]["staff_id"];
                    $data[$i]["assist_group_0"] = $belong_group_array["$wk_staff_id"];
                }
            }
        }
        //4週計算で日付が02以降の場合、日付毎の応援データがない場合、処理中のグループを設定 20120305
        if (substr($second_date, 6, 2) != "01") {
            for ($i = 0; $i < count($data); $i++) {
                for ($cal_idx = 0; $cal_idx < count($calendar_array); $cal_idx++) {
                    $wk_duty_date = $calendar_array[$cal_idx]["date"];
                    if ($data[$i]["assist_group_$wk_duty_date"] == "") {
                        $wk_staff_id = $data[$i]["staff_id"];
                        $data[$i]["assist_group_$wk_duty_date"] = $belong_group_array["$wk_staff_id"];
                    }
                }
            }
        }
        $arr_pos_value[] = "応援追加データ";
        $arr_time_value[] = time();
        //*******************************************************************
        //勤務シフト情報の編集
        //*******************************************************************
        //-------------------------------------------------------------------
        //情報設定（表示文字エリアの未設定時の文字設定）
        //情報設定（表示順の設定）
        //情報設定（個人勤務条件の設定）
        //-------------------------------------------------------------------
        for ($i = 0; $i < count($data); $i++) {
            //-------------------------------------------------------------------
            //表示順
            //-------------------------------------------------------------------
            $data[$i]["no"] = $i + 1;
            //-------------------------------------------------------------------
            //他病棟と重複スタッフか
            //-------------------------------------------------------------------
            $wk_id = $data[$i]["staff_id"];

            //重複スタッフの場合
//echo("arr wk_id=".$arr_other_staff_flg["$wk_id"]);
            if ($arr_other_staff_flg["$wk_id"] == "1") {
                $data[$i]["other_staff_flg"] = "1";     //他病棟スタッフ（１：スタッフ）
            }

            //-------------------------------------------------------------------
            //データ未登録の場合、希望日（休暇）を自動設定
            //-------------------------------------------------------------------
            $wk_flg = "";
            for ($m = 1; $m <= $day_cnt; $m++) {
                if ($data[$i]["atdptn_ptn_id_$m"] != "") {
                    $wk_flg = "1";
                }
            }
            if ($individual_flg == "1") {
                if ($wk_flg == "") {
                    //-------------------------------------------------------------------
                    //データ未登録の場合、希望日（休暇）を自動設定
                    //-------------------------------------------------------------------
                    $wk_data = $this->get_duty_shift_plan_array_2(
                            $data, $i, $group_id, $pattern_id, $start_date, $end_date, $day_cnt, $data_week, $data_atdptn, $data_pattern_all);
                    for ($k = 1; $k <= $day_cnt; $k++) {
                        $data[$i]["atdptn_ptn_id_$k"] = $wk_data["atdptn_ptn_id_$k"];   //出勤パターンＩＤ
                        $data[$i]["font_name_$k"] = $wk_data["font_name_$k"];           //文字（勤務状況名）
                        $data[$i]["font_color_$k"] = $wk_data["font_color_$k"];         //文字（色）
                        $data[$i]["back_color_$k"] = $wk_data["back_color_$k"];         //文字（背景色）
                        $data[$i]["count_kbn_gyo_$k"] = $wk_data["count_kbn_gyo_$k"];   //集計行（出勤パターンＩＤ）
                        $data[$i]["count_kbn_retu_$k"] = $wk_data["count_kbn_retu_$k"]; //集計列（出勤パターンＩＤ）
                        $data[$i]["reason_$k"] = $wk_data["reason_$k"];                 //事由（１：有給）
                        $data[$i]["reason_2_$k"] = $wk_data["reason_2_$k"];             //事由（午前有給／午後有給）
                    }
                }
            }
            //-------------------------------------------------------------------
            //希望のデータ判定フラグ設定
            //-------------------------------------------------------------------
            if ($hope_get_flg == "1") {
                for ($k = 1; $k <= $day_cnt; $k++) {
                    $data[$i]["individual_flg_$k"] = "";
                    if ($data[$i]["atdptn_ptn_id_$k"] != "") {
                        $data[$i]["individual_flg_$k"] = $data[$i]["atdptn_ptn_id_$k"];
                    }
                }
            }
            //-------------------------------------------------------------------
            //空文字／文字色／文字背景色設定
            //-------------------------------------------------------------------
            for ($m = 1; $m <= $day_cnt; $m++) {
                if ($data[$i]["font_name_$m"] == "") {
                    $data[$i]["font_name_$m"] = "&nbsp;";
                }                      //予定 "_" -> " "
                if ($data[$i]["rslt_name_$m"] == "") {
                    $data[$i]["rslt_name_$m"] = "&nbsp;";
                }                      //実績 "_" -> " "
                if ($data[$i]["pattern_id_$m"] == "") {
                    $data[$i]["pattern_id_$m"] = $pattern_id;
                }             //予定
                if ($data[$i]["rslt_pattern_id_$m"] == "") {
                    $data[$i]["rslt_pattern_id_$m"] = $pattern_id;
                }   //実績
                //-------------------------------------------------------------------
                //予定／実績の表示文字・文字色・文字背景色設定
                //勤務シフト記号情報DBより設定
                //-------------------------------------------------------------------
                //予定
                if (($data[$i]["pattern_id_$m"] != "") &&
                        ($data[$i]["atdptn_ptn_id_$m"] != "")) {
                    // 20140128 start 設定方法変更
                    //デフォルト 34:事由無し
                    if (trim($data[$i]["reason_$m"]) == "" &&
                            $data[$i]["atdptn_ptn_id_$m"] == "10") {
                        $data[$i]["reason_$m"] = "34";                          //事由（34:事由無し）
                        $data[$i]["reason_2_$m"] = "";                          //事由（午前有給／午後有給）
                    }
                    //勤務パターン情報設定
                    $wk_flg = "";
                    $wk_tm_gid = $data[$i]["pattern_id_$m"];
                    $wk_ptn_id = $data[$i]["atdptn_ptn_id_$m"];
                    $wk_reason_id = $data[$i]["reason_$m"];

                    if ($wk_ptn_id == "10") {
                        $wk_key = $wk_tm_gid . "_" . $wk_ptn_id . "_" . $wk_reason_id;
                    } else {
                        $wk_key = $wk_tm_gid . "_" . $wk_ptn_id;
                    }
                    $k = $arr_data_pattarn_key["$wk_key"];
                    if (isset($k)) {
                        $data[$i]["font_name_$m"] = $data_pattern_all[$k]["font_name"];             //文字（勤務状況名）
                        $data[$i]["font_color_$m"] = $data_pattern_all[$k]["font_color_id"];        //文字（色）
                        $data[$i]["back_color_$m"] = $data_pattern_all[$k]["back_color_id"];        //文字（背景色）
                        $data[$i]["count_kbn_gyo_$m"] = $data_pattern_all[$k]["count_kbn_gyo"];     //集計行（出勤パターンＩＤ）
                        $data[$i]["count_kbn_retu_$m"] = $data_pattern_all[$k]["count_kbn_retu"];   //集計列（出勤パターンＩＤ）
                    }
                    // 20140128 end
                }

                //実績
                if (($data[$i]["rslt_pattern_id_$m"] != "") &&
                        ($data[$i]["rslt_id_$m"] != "")) {
                    // 20140128 start 設定方法変更
                    //デフォルト 34:事由無し
                    if (trim($data[$i]["rslt_reason_$m"]) == "" &&
                            $data[$i]["rslt_id_$m"] == "10") {
                        $data[$i]["rslt_reason_$m"] = "34";                         //事由（34:事由無し）
                        $data[$i]["rslt_reason_2_$m"] = "";                         //事由（午前有給／午後有給）
                    }
                    //勤務パターン情報設定
                    $wk_flg = "";
                    $wk_tm_gid = $data[$i]["rslt_pattern_id_$m"];
                    $wk_ptn_id = $data[$i]["rslt_id_$m"];
                    $wk_reason_id = $data[$i]["rslt_reason_$m"];

                    if ($wk_ptn_id == "10") {
                        $wk_key = $wk_tm_gid . "_" . $wk_ptn_id . "_" . $wk_reason_id;
                    } else {
                        $wk_key = $wk_tm_gid . "_" . $wk_ptn_id;
                    }
                    $k = $arr_data_pattarn_key["$wk_key"];
                    if (isset($k)) {
                        $data[$i]["rslt_name_$m"] = $data_pattern_all[$k]["font_name"];             //文字（勤務状況名）
                        $data[$i]["rslt_font_color_$m"] = $data_pattern_all[$k]["font_color_id"];       //文字（色）
                        $data[$i]["rslt_back_color_$m"] = $data_pattern_all[$k]["back_color_id"];       //文字（背景色）
                        $data[$i]["rslt_count_kbn_gyo_$m"] = $data_pattern_all[$k]["count_kbn_gyo"];        //集計行（出勤パターンＩＤ）
                        $data[$i]["rslt_count_kbn_retu_$m"] = $data_pattern_all[$k]["count_kbn_retu"];  //集計列（出勤パターンＩＤ）
                    }
                    // 20140128 end
                }
            }
        }
        $arr_pos_value[] = "勤務シフト情報の編集 duty_shift_common_class return";
        $arr_time_value[] = time();

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフト情報取得（データ未設定の場合、休暇を設定）
    // @param   $data               勤務シフト情報
    // @param   $data_idx           勤務シフト情報のインデックス
    // @param   $group_id           勤務シフトグループＩＤ
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $start_date         勤務開始年月日
    // @param   $end_date           勤務終了年月日
    // @param   $day_cnt            日数
    // @param   $data_week          週情報
    // @param   $data_atdptn        出勤パターン情報
    // @param   $data_pattern_all   勤務シフトパターン情報
    //
    // @return  $data       取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_plan_array_2(
    $data, $data_idx, $group_id, $pattern_id, $start_date, $end_date, $day_cnt, $data_week, $data_atdptn, $data_pattern_all) {
        //-------------------------------------------------------------------
        //初期値設定
        //-------------------------------------------------------------------
        $i = $data_idx;
        $set_data = array();
        //-------------------------------------------------------------------
        //ＳＱＬ
        //-------------------------------------------------------------------
        $wk_id = $data[$i]["staff_id"];
        $sql = "select * from duty_shift_plan_individual ";
        $cond = "where emp_id = '$wk_id' ";
        $cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
        $cond .= "order by emp_id, duty_date";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $wk_num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //データ未登録の場合、希望日（休暇）を自動設定
        //-------------------------------------------------------------------
        if ($wk_num <= 0) {
            //-------------------------------------------------------------------
            //休暇のID取得
            //-------------------------------------------------------------------
            $wk_vacation_id = "";
            for ($m = 0; $m < count($data_atdptn); $m++) {
                //休暇を名称ではなくIDで確認
                if ($data_atdptn[$m]["id"] == "10") {
                    for ($k = 0; $k < count($data_pattern_all); $k++) {
                        if (($data_pattern_all[$k]["pattern_id"] == $pattern_id) &&
                                ($data_pattern_all[$k]["atdptn_ptn_id"] == $data_atdptn[$m]["id"])) {
                            $wk_vacation_id = $data_pattern_all[$k]["atdptn_ptn_id"];       //出勤パターンＩＤ
                            $wk_font_name_v = $data_pattern_all[$k]["font_name"];           //文字（勤務状況名）
                            $wk_font_color_v = $data_pattern_all[$k]["font_color_id"];      //文字（色）
                            $wk_back_color_v = $data_pattern_all[$k]["back_color_id"];      //文字（背景色）
                            $wk_count_kbn_gyo_v = $data_pattern_all[$k]["count_kbn_gyo"];   //集計行（出勤パターンＩＤ）
                            $wk_count_kbn_retu_v = $data_pattern_all[$k]["count_kbn_retu"]; //集計列（出勤パターンＩＤ）
                            break;
                        }
                    }
                }
            }
            //-------------------------------------------------------------------
            //希望日（休暇）を自動設定
            //-------------------------------------------------------------------
            for ($k = 1; $k <= $day_cnt; $k++) {
                $wk_flg = "";
                if (($data_week[$k]["name"] == "月") && ($data[$i]["duty_mon_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($data_week[$k]["name"] == "火") && ($data[$i]["duty_tue_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($data_week[$k]["name"] == "水") && ($data[$i]["duty_wed_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($data_week[$k]["name"] == "木") && ($data[$i]["duty_thurs_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($data_week[$k]["name"] == "金") && ($data[$i]["duty_fri_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($data_week[$k]["name"] == "土") && ($data[$i]["duty_sat_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($data_week[$k]["name"] == "日") && ($data[$i]["duty_sun_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($data_week[$k]["type"] == "6") && ($data[$i]["duty_hol_day_flg"] != "1")) {
                    $wk_flg = "1";
                }
                if (($wk_flg == "1") && ($wk_vacation_id != "")) {
                    $set_data["atdptn_ptn_id_$k"] = $wk_vacation_id;        //出勤パターンＩＤ
                    $set_data["font_name_$k"] = $wk_font_name_v;            //文字（勤務状況名）
                    $set_data["font_color_$k"] = $wk_font_color_v;          //文字（色）
                    $set_data["back_color_$k"] = $wk_back_color_v;          //文字（背景色）
                    $set_data["count_kbn_gyo_$k"] = $wk_count_kbn_gyo_v;    //集計行（出勤パターンＩＤ）
                    $set_data["count_kbn_retu_$k"] = $wk_count_kbn_retu_v;  //集計列（出勤パターンＩＤ）
                    $set_data["reason_$k"] = "24";                          //事由（１：有給-> 34:事由無し）※24:公休へ変更 20090714
                    $set_data["reason_2_$k"] = "";                          //事由（午前有給／午後有給）
                }
            }
        }

        return $set_data;
    }

/////////////////////////////////////////
    /*     * ********************************************************************** */
    // 出勤表／勤務（予定／実績）情報取得
    // @param   $db_name            テーブル名称
    // @param   $cond_add           検索条件
    // @param   $calendar_array     カレンダー情報
    // @param   $data_staff             勤務情報（職員ID参照のため追加） 20140203
    // @param   $sche               パラメータ追加 20131115 取得元テーブル(atdbk(出勤予定):未指定時 / atdbk_sche(当初予定))
    //                              出勤予定テーブル名に付加する拡張子("_sche")
    //
    // @return  $data           取得情報
    /*     * ********************************************************************** */
    function get_atdbk_atdbkrslt_array($db_name, $cond_add, $calendar_array, $data_staff, $sche) {
        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $day_cnt = count($calendar_array);
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[($day_cnt - 1)]["date"];

        $data = array();
        //職員ID毎にデータを取得
        for ($s_idx = 0; $s_idx < count($data_staff); $s_idx++) {
            $staff_id = $data_staff[$s_idx]["staff_id"];
            //-------------------------------------------------------------------
            // 予定／実績ＤＢより情報取得
            //-------------------------------------------------------------------
            $sql = "select ";
            $sql .= "a.emp_id, a.date, a.pattern, a.reason, a.night_duty, a.tmcd_group_id, ";
            if ($db_name == "atdbkrslt") {
                $sql .= "a.start_time, a.end_time, a.meeting_time, a.meeting_start_time, a.meeting_end_time, ";
                $sql .= "a.out_time, a.ret_time, a.previous_day_flag,  a.next_day_flag, "; //翌日フラグ 20100622
            }
            $sql .= "b.emp_lt_nm, b.emp_ft_nm ";
            $sql .= ", c.group_id as assist_group_id, d.group_id as main_group_id, case when e.standard_id is null then f.standard_id else e.standard_id end as standard_id "; //施設基準を取得 2009/01/13
            $sql .= ",phh.start_time, phh.use_hour, phh.use_minute "; // 打刻時刻 2014/10
            $sql .= "from $db_name as a inner join empmst as b ";
            $sql .= "on a.emp_id = b.emp_id ";
            $sql .= "left join duty_shift_plan_assist" . $sche . " c on c.emp_id = a.emp_id and c.duty_date = a.date ";
            $sql .= "left join duty_shift_staff d on d.emp_id = a.emp_id ";
            $sql .= "left join duty_shift_group e on e.group_id = c.group_id ";
            $sql .= "left join duty_shift_group f on f.group_id = d.group_id ";
            $sql .= "left join timecard_paid_hol_hour phh on (phh.emp_id = a.emp_id and phh.start_date = a.date)"; // 打刻時刻 2014/10
            $cond = "where a.emp_id = '$staff_id' and a.date >= '$start_date' and a.date <= '$end_date' ";
            //if ($cond_add != "") {
            //  $cond .= $cond_add;
            //}
            $cond .= "order by a.emp_id, a.date";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $num = pg_num_rows($sel);
            //-------------------------------------------------------------------
            // ＤＢ値設定
            //-------------------------------------------------------------------
            //$cnt = 0;
            //$wk_staff_id = "";
            for ($i = 0; $i < $num; $i++) {
                //-------------------------------------------------------------------
                //対象データか判定
                //-------------------------------------------------------------------
                $key_staff_id = pg_fetch_result($sel, $i, "emp_id");
                $data[$s_idx]["staff_id"] = $key_staff_id;
                $cnt = $s_idx;
                //-------------------------------------------------------------------
                //既にスタッフが存在する場合、そのスタッフを利用
                //-------------------------------------------------------------------
                //if ($i == 0) {
                //  $wk_staff_id = $key_staff_id;
                //} else if ($wk_staff_id != $key_staff_id) {
                //  $wk_staff_id = $key_staff_id;
                //  $cnt++;
                //}
                //-------------------------------------------------------------------
                //ＤＢより取得値
                //-------------------------------------------------------------------
                $data[$cnt]["staff_name"] = pg_fetch_result($sel, $i, "emp_lt_nm") . " " . pg_fetch_result($sel, $i, "emp_ft_nm");
                $key_night_duty = pg_fetch_result($sel, $i, "night_duty");                //当直（１：有り、２：無し）
                $key_date = trim(pg_fetch_result($sel, $i, "date"));                  //年月日
                $key_start_time = trim(pg_fetch_result($sel, $i, "start_time"));      //開始時刻
                $key_end_time = trim(pg_fetch_result($sel, $i, "end_time"));          //終了時刻
                $key_meeting_time = trim(pg_fetch_result($sel, $i, "meeting_time"));  //研修・会議時間
                $key_out_time = trim(pg_fetch_result($sel, $i, "out_time"));  //外出時刻
                $key_ret_time = trim(pg_fetch_result($sel, $i, "ret_time"));  //復帰時刻
                $key_pattern_id = trim(pg_fetch_result($sel, $i, "tmcd_group_id"));       //出勤グループＩＤ
                $key_atdptn_ptn_id = trim(pg_fetch_result($sel, $i, "pattern"));      //出勤パターンＩＤ
                $key_reason = trim(pg_fetch_result($sel, $i, "reason"));              //事由
                $key_standard_id = trim(pg_fetch_result($sel, $i, "standard_id"));                //施設基準
                $key_assist_group = trim(pg_fetch_result($sel, $i, "assist_group_id"));               //応援
                $key_meeting_start_time = trim(pg_fetch_result($sel, $i, "meeting_start_time"));      //研修・会議開始時刻
                $key_meeting_end_time = trim(pg_fetch_result($sel, $i, "meeting_end_time"));          //研修・会議終了時刻
                $key_previous_day_flag = trim(pg_fetch_result($sel, $i, "previous_day_flag"));            //
                $key_next_day_flag = trim(pg_fetch_result($sel, $i, "next_day_flag"));            //翌日フラグ 20100622
                //-------------------------------------------------------------------
                //年月日をキーに設定
                //-------------------------------------------------------------------
                $m = ((date_utils::to_timestamp_from_ymd($key_date) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
                $data[$cnt]["night_duty_$m"] = $key_night_duty;         //当直（１：有り、２：無し）
                $data[$cnt]["pattern_id_$m"] = $key_pattern_id;         //出勤グループＩＤ
                $data[$cnt]["atdptn_ptn_id_$m"] = $key_atdptn_ptn_id;   //出勤パターンＩＤ
                $data[$cnt]["standard_id_$m"] = $key_standard_id;           //施設基準
                $data[$cnt]["assist_group_$m"] = $key_assist_group;         //応援
                $data[$cnt]["paid_hol_start_time_$m"] = trim(pg_fetch_result($sel, $i, "start_time")); // 打刻時刻 2014/10
                $data[$cnt]["paid_hol_use_hour_$m"] = trim(pg_fetch_result($sel, $i, "use_hour")); // 打刻時刻 2014/10
                $data[$cnt]["paid_hol_use_minute_$m"] = trim(pg_fetch_result($sel, $i, "use_minute")); // 打刻時刻 2014/10
                //$ret["34"] = ""（事由無し）;
                //$ret["24"] = "公休";
                //$ret["22"] = "法定休暇";
                //$ret["23"] = "所定休暇";
                //$ret["1"] = "有給休暇";
                //$ret["37"] = "年休";
                //$ret["4"] = "代替休暇";
                //$ret["17"] = "振替休暇";
                //$ret["5"] = "特別休暇";
                //$ret["6"] = "欠勤（一般欠勤）";
                //$ret["7"] = "病欠（病傷欠勤）";
                //$ret["8"] = "その他休";
                //$ret["40"] = "リフレッシュ休暇";
                //$ret["41"] = "初盆休暇";
                //25:産休〜32:結婚休
                //25:産休〜32:結婚休 44:半有半公 61:年末年始
                //54:半夏半〜64公半正半欠追加20110808
                //※勤務パターンのプルダウンに追加する場合、ここも修正
                if (($key_reason == "34") ||
                        ($key_reason == "1") || ($key_reason == "37") ||
                        ($key_reason >= "4" && $key_reason <= "8") ||
                        ($key_reason == "17") ||
                        ($key_reason >= "22" && $key_reason <= "32") ||
                        ($key_reason == "40") || ($key_reason == "41") ||
                        ($key_reason >= "44" && $key_reason <= "47") ||
                        ($key_reason >= "54" && $key_reason <= "64") ||
                        $key_reason == "72" || $key_reason == "33" || ($key_reason >= "9" && $key_reason <= "15") //追加 20141031
                ) {
                    $data[$cnt]["reason_$m"] = $key_reason;     //事由
                }
                //$ret["2"] = "午前有休";
                //$ret["3"] = "午後有休";
                else if (($key_reason == "2") ||
                        ($key_reason == "3")) {
                    $data[$cnt]["reason_2_$m"] = $key_reason;   //事由
                    $data[$cnt]["reason_$m"] = "1";             //事由（有給休暇）
                }
                //$ret["38"] = "午前年休";
                //$ret["39"] = "午後年休";
                else if (($key_reason == "38") ||
                        ($key_reason == "39")) {
                    $data[$cnt]["reason_2_$m"] = $key_reason;   //事由
                    $data[$cnt]["reason_$m"] = "37";            //事由（年休）
                }
                //$ret["18"] = "半前代替休";
                //$ret["19"] = "半後代替休";
                else if (($key_reason == "18") ||
                        ($key_reason == "19")) {
                    $data[$cnt]["reason_2_$m"] = $key_reason;   //事由
                    $data[$cnt]["reason_$m"] = "4";             //事由（代替休暇）
                }
                //$ret["20"] = "半前振替休";
                //$ret["21"] = "半後振替休";
                else if (($key_reason == "20") ||
                        ($key_reason == "21")) {
                    $data[$cnt]["reason_2_$m"] = $key_reason;   //事由
                    $data[$cnt]["reason_$m"] = "17";            //事由（振替休暇）
                }
                //$ret["35"] = "午前公休";
                //$ret["36"] = "午後公休";
                else if (($key_reason == "35") ||
                        ($key_reason == "36")) {
                    $data[$cnt]["reason_2_$m"] = $key_reason;   //事由
                    $data[$cnt]["reason_$m"] = "24";            //事由（公休）
                }
                //$ret["42"] = "午前リフレッシュ";
                //$ret["43"] = "午後リフレッシュ";
                else if (($key_reason == "42") ||
                        ($key_reason == "43")) {
                    $data[$cnt]["reason_2_$m"] = $key_reason;   //事由
                    $data[$cnt]["reason_$m"] = "40";            //事由（リフレッシュ）
                }
                // 上記以外
                else {
                    $data[$cnt]["reason_2_$m"] = $key_reason;   //事由
                    // 休暇の場合、事由無しの休暇とするため"34"を設定
                    if ($key_atdptn_ptn_id == "10") {
                        $data[$cnt]["reason_$m"] = "34";
                    }
                }

                //-------------------------------------------------------------------
                //開始／終了時刻
                //研修・会議時間
                //-------------------------------------------------------------------
                if ($db_name == "atdbkrslt") {
                    $data[$cnt]["start_time_$m"] = $key_start_time;         //開始時刻
                    $data[$cnt]["end_time_$m"] = $key_end_time;             //終了時刻
                    $data[$cnt]["meeting_time_$m"] = $key_meeting_time;     //研修・会議時間
                    $data[$cnt]["out_time_$m"] = $key_out_time;         //外出時刻
                    $data[$cnt]["ret_time_$m"] = $key_ret_time;         //復帰時刻
                    $data[$cnt]["meeting_start_time_$m"] = $key_meeting_start_time;         //研修・会議開始時刻
                    $data[$cnt]["meeting_end_time_$m"] = $key_meeting_end_time;             //研修・会議終了時刻
                    $data[$cnt]["previous_day_flag_$m"] = $key_previous_day_flag;               //
                    $data[$cnt]["next_day_flag_$m"] = $key_next_day_flag;                //翌日フラグ 20100622
                }
            }
        } //end of for $s_idx
        //-------------------------------------------------------------------
        //出勤グループＩＤが未設定の場合、職員情報の出勤グループＩＤ設定
        //-------------------------------------------------------------------
        /* get_duty_shift_staff_employmen()関数で取得済みのため削除20140203
          $cond_add = "";
          for ($i=0;$i<count($data);$i++) {
          $wk_id = $data[$i]["staff_id"];
          if ($cond_add == "") {
          $cond_add .= " (emp_id = '$wk_id' ";
          } else {
          $cond_add .= "or emp_id = '$wk_id' ";
          }
          }
          if ($cond_add != "") {
          $cond_add .= ") ";
          }
          $sql = "select emp_id, tmcd_group_id from empcond";
          if ($cond_add != "") {
          $cond = "where $cond_add ";
          } else {
          $cond = " ";
          }
          $cond .= "order by emp_id";
          $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
          if ($sel == 0) {
          pg_close($this->_db_con);
          echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
          echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
          exit;
          }

          $arr_tmcd_group_id = array();
          $num = pg_num_rows($sel);
          for ($i=0;$i<$num;$i++) {
          $wk_staff_id = pg_fetch_result($sel,$i,"emp_id");
          $arr_tmcd_group_id["$wk_staff_id"] = pg_fetch_result($sel,$i,"tmcd_group_id");
          }
         */
        for ($i = 0; $i < count($data); $i++) {
            $wk_emp_id = $data[$i]["staff_id"];

            //-------------------------------------------------------------------
            //出勤グループＩＤが未設定の場合、職員情報の出勤グループＩＤ設定
            //-------------------------------------------------------------------
            for ($m = 1; $m <= $day_cnt; $m++) {
                if ($data[$i]["pattern_id_$m"] == "") {
//              $data[$i]["pattern_id_$m"] = $tmcd_group_id;
//                  $data[$i]["pattern_id_$m"] = $arr_tmcd_group_id["$wk_emp_id"];
                    $data[$i]["pattern_id_$m"] = $data[$i]["tmcd_group_id"]; //20140203
                }
            }
        }

        return $data;
    }

    /**
     * 届出書用　出勤表／勤務（予定／実績）情報取得
     *
     * 同一施設基準を持つシフトグループでは、シフト作成期間(締め日)の設定は同じ
     *
     * @param   $db_name            テーブル名称、実質的にatdbkrsltの実績専用
     * @param   $cond_add           検索条件
     * @param   $calendar_array     カレンダー情報
     * @param   $standard_id        施設基準
     * @param   $group_id           グループID
     *
     * @return  array           取得情報
     */
    function get_atdbk_atdbkrslt_array_for_report($db_name, $cond_add, $calendar_array, $standard_id, $group_id = "") {
        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $day_cnt = count($calendar_array);
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[($day_cnt - 1)]["date"];
        //end_dateは翌月1日のため、get_atdbk_for_reportと同様2番目の日付を指定
        $wk_this_mon_end = $calendar_array[1]["date"];
        $duty_yyyy = substr($wk_this_mon_end, 0, 4);
        $duty_mm = intval(substr($wk_this_mon_end, 4, 2));

        //シフトグループ情報を取得
        $group_array = $this->get_group_info_for_report($standard_id, $group_id);

        //対象グループが存在しなければ、空の配列を返す
        if (count($group_array) == 0) {
            return array();
        }

        //シフト期間をチェック
        if (!$this->is_same_shift_term($group_array)) {
            return array("error_msg" => "シフトグループ間でシフト作成期間が異なる為、計算できません。");
        }

        //4週計算で後半部分が翌々月にまたがる場合、翌月を基準に職員情報を取得する 20141117
        $wk_duty_yyyy = $duty_yyyy;
        $wk_duty_mm = $duty_mm;
        $start_2_date = $calendar_array[1]["date"];
        $start_2_day = substr($start_2_date, 6, 2);
        $start_2_mon = substr($start_2_date, 4, 2);
        //4週28日＋前月1日＋翌月1日で30、開始日が1日以外で判断
        if ($day_cnt == 30 && $start_2_day != "01") {
            $end_2_date = $calendar_array[($day_cnt - 2)]["date"];
            $end_2_day = substr($end_2_date, 6, 2); //4週計算、最終日
            $end_2_mon = substr($end_2_date, 4, 2);
            $wk_group_end_day = $group_array[0]["end_day1"]; //シフト作成期間、翌月の日
            if ($end_2_day > $wk_group_end_day && $start_2_mon != $end_2_mon) { // 月も確認 20150527
                $wk_duty_mm++;
                if ($wk_duty_mm > 12) {
                    $wk_duty_mm = 1;
                    $wk_duty_yyyy ++;
                }
            }
        }
        //職員情報取得
        $arr_emp_info = $this->get_emp_info_for_report($standard_id, $group_id, $wk_duty_yyyy, $wk_duty_mm);
        $this->arr_emp_info = $arr_emp_info;

        //期間情報取得
        //グループ間でシフト期間は同じなので、代表してインデックス0の期間情報を取得する
        $shift_term = $this->get_shift_term($group_array[0], $duty_yyyy, $duty_mm, $calendar_array);

        $g_str = "";
        for ($i = 0; $i < count($group_array); $i++) {
            if ($g_str != "") {
                $g_str .= ", ";
            }
            $g_str .= "'" . $group_array[$i]["group_id"] . "'";
        }

        //SQL文を生成
        $sqlsub = array();
        for ($i = 0; $i < count($shift_term); $i++) {
            $wk_start_date = $shift_term[$i]["start_date"];
            $wk_end_date = $shift_term[$i]["end_date"];
            $wk_year = $shift_term[$i]["year"];
            $wk_month = $shift_term[$i]["month"];
            $sqlsub[] = "select a.emp_id, a.date, a.pattern, a.reason, a.start_time, a.end_time, b.group_id, c.duty_date, c.group_id as assist_group_id, d.group_id as main_group_id, a.tmcd_group_id, a.meeting_start_time, a.meeting_end_time, a.out_time, a.ret_time, a.previous_day_flag, a.next_day_flag from atdbkrslt a "
                    . " left join duty_shift_plan_staff b on b.group_id in ($g_str) and b.emp_id = a.emp_id and b.duty_yyyy = $wk_year and b.duty_mm = $wk_month "
                    . " left join duty_shift_plan_assist c on c.emp_id = a.emp_id and c.duty_date = a.date "
                    . " left join duty_shift_staff d on d.emp_id = a.emp_id "
                    . " where a.date >= '$wk_start_date' and a.date <= '$wk_end_date' "
                    . " and a.start_time != '' and a.end_time != '' "
                    . " and b.group_id is not null "
                    . " and (c.group_id in ($g_str) or (c.group_id is null and (b.group_id in ($g_str))))";
        }
        //件数の確認
        if (!($group_id != "" && $group_id != "0")) {
            $cnt_sql = "";
            foreach ($sqlsub as $value) {
                if ($cnt_sql === "") {
                    $cnt_sql = $value;
                } else {
                    $cnt_sql .= " union $value";
                }
            }
            $cnt_sql = "select e.group_id, e.emp_id, count(*) as cnt from ( $cnt_sql";
            $cnt_sql .= " ) e group by e.group_id, e.emp_id ";
            $cnt_sel = select_from_table($this->_db_con, $cnt_sql, $cond, $this->file_name);
            if ($cnt_sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            $arr_data_cnt = array();
            while ($row = pg_fetch_array($cnt_sel)) {
                $wk_group_id = $row["group_id"];
                $wk_emp_id = $row["emp_id"];
                $arr_data_cnt[$wk_group_id][$wk_emp_id] = $row["cnt"];
            }
            $arr_emp_info_old = $arr_emp_info;

            //データ件数がある場合
            //複数グループある場合、先に出てきたグループ
            $arr_emp_info = array();
            $arr_emp_chk = array();
            $idx = 0;
            for ($i = 0; $i < count($arr_emp_info_old); $i++) {
                $wk_emp_id = $arr_emp_info_old[$i]["emp_id"];
                $wk_group_id = $arr_emp_info_old[$i]["group_id"];
                if ($arr_data_cnt[$wk_group_id][$wk_emp_id] > 0 &&
                        $arr_emp_chk[$wk_emp_id] == "") {
                    $arr_emp_chk[$wk_emp_id] = $idx + 1;
                    $arr_emp_info[$idx] = $arr_emp_info_old[$i];
                    $arr_emp_info[$idx]["no"] = $idx + 1;
                    $idx++;
                }
            }
        }

        //職員の表示順
        $arr_emp_no = array();
        for ($i = 0; $i < count($arr_emp_info); $i++) {
            $tmp_emp_id = $arr_emp_info[$i]["emp_id"];
            $arr_emp_no[$tmp_emp_id] = $arr_emp_info[$i]["no"];
        }

        $sql = "";
        foreach ($sqlsub as $value) {
            if ($sql === "") {
                $sql = $value;
            } else {
                $sql .= " union $value";
            }
        }
        $sql = "select * from ( $sql ) e order by e.emp_id, e.date ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        // ＤＢ値設定
        //-------------------------------------------------------------------
        $data = array();
        $cnt = 0;
        $wk_staff_id = "";
        $arr_no = array();
        for ($i = 0; $i < $num; $i++) {
            //-------------------------------------------------------------------
            //対象データか判定
            //-------------------------------------------------------------------
            $key_staff_id = pg_fetch_result($sel, $i, "emp_id");
            if ($arr_emp_no[$key_staff_id] != "") {
                $cnt = $arr_emp_no[$key_staff_id] - 1;
            } else { //20141023
                continue;
            }
            //-------------------------------------------------------------------
            //ＤＢより取得値
            //-------------------------------------------------------------------
            $data[$cnt]["staff_id"] = $key_staff_id;
            $data[$cnt]["assist_group_id"] = pg_fetch_result($sel, $i, "assist_group_id");
            $data[$cnt]["main_group_id"] = pg_fetch_result($sel, $i, "main_group_id");
            $data[$cnt]["assist_group_name"] = pg_fetch_result($sel, $i, "assist_group_name");
            $data[$cnt]["st_nm"] = pg_fetch_result($sel, $i, "st_nm");

            $key_night_duty = pg_fetch_result($sel, $i, "night_duty");                //当直（１：有り、２：無し）
            $key_date = trim(pg_fetch_result($sel, $i, "date"));                  //年月日
            $key_start_time = trim(pg_fetch_result($sel, $i, "start_time"));      //開始時刻
            $key_end_time = trim(pg_fetch_result($sel, $i, "end_time"));          //終了時刻
            $key_meeting_time = trim(pg_fetch_result($sel, $i, "meeting_time"));  //研修・会議時間
            $key_out_time = trim(pg_fetch_result($sel, $i, "out_time"));  //外出時刻
            $key_ret_time = trim(pg_fetch_result($sel, $i, "ret_time"));  //復帰時刻
            $key_pattern_id = trim(pg_fetch_result($sel, $i, "tmcd_group_id"));       //出勤グループＩＤ
            $key_atdptn_ptn_id = trim(pg_fetch_result($sel, $i, "pattern"));      //出勤パターンＩＤ
            $key_reason = trim(pg_fetch_result($sel, $i, "reason"));              //事由
            $key_meeting_start_time = trim(pg_fetch_result($sel, $i, "meeting_start_time"));      //研修・会議開始時刻
            $key_meeting_end_time = trim(pg_fetch_result($sel, $i, "meeting_end_time"));          //研修・会議終了時刻
            $key_previous_day_flag = trim(pg_fetch_result($sel, $i, "previous_day_flag"));            //
            $key_next_day_flag = trim(pg_fetch_result($sel, $i, "next_day_flag"));            //
            //-------------------------------------------------------------------
            //年月日をキーに設定
            //-------------------------------------------------------------------
            $m = ((date_utils::to_timestamp_from_ymd($key_date) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
            $data[$cnt]["night_duty_$m"] = $key_night_duty;         //当直（１：有り、２：無し）
            $data[$cnt]["pattern_id_$m"] = $key_pattern_id;         //出勤グループＩＤ
            $data[$cnt]["atdptn_ptn_id_$m"] = $key_atdptn_ptn_id;   //出勤パターンＩＤ
            $data[$cnt]["standard_id_$m"] = $standard_id;   //施設基準
            //-------------------------------------------------------------------
            //開始／終了時刻
            //研修・会議時間
            //-------------------------------------------------------------------
            $data[$cnt]["start_time_$m"] = $key_start_time;         //開始時刻
            $data[$cnt]["end_time_$m"] = $key_end_time;             //終了時刻
            $data[$cnt]["meeting_time_$m"] = $key_meeting_time;     //研修・会議時間
            $data[$cnt]["out_time_$m"] = $key_out_time;         //外出時刻
            $data[$cnt]["ret_time_$m"] = $key_ret_time;         //復帰時刻
            $data[$cnt]["meeting_start_time_$m"] = $key_meeting_start_time;     //研修・会議開始時刻
            $data[$cnt]["meeting_end_time_$m"] = $key_meeting_end_time;         //研修・会議終了時刻
            $data[$cnt]["previous_day_flag_$m"] = $key_previous_day_flag;               //
            $data[$cnt]["next_day_flag_$m"] = $key_next_day_flag;               //
        }
        //名前設定
        for ($i = 0; $i < count($data); $i++) {
            $staff_id = $data[$i]["staff_id"];
            $no = $arr_emp_no[$staff_id] - 1;
            $data[$i]["staff_name"] = $arr_emp_info[$no]["emp_name"];
            $data[$i]["main_group_name"] = $arr_emp_info[$no]["group_name"];
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 出勤表／勤務実績情報取得（様式９用履歴）
    // @param   $standard_id        施設基準
    // @param   $duty_yyyy          勤務年
    // @param   $suty_mm            勤務月
    // @param   $cond_add           検索条件
    // @param   $calendar_array     カレンダー情報
    // @param   $group_id           グループID
    //
    // @return  $data       取得情報
    /*     * ********************************************************************** */
    function get_atdbkrslt_history_array($standard_id, $duty_yyyy, $duty_mm, $cond_add, $calendar_array, $group_id = "") {

        $data = $this->get_atdbk_atdbkrslt_array_for_report("atdbkrslt", $cond_add, $calendar_array, $standard_id, $group_id);

        return $data;
    }

    /**
     * 予定取得
     *
     * @param mixed $cond_add       検索条件
     * @param mixed $calendar_array カレンダー情報
     * @param mixed $standard_id    施設基準
     * @param mixed $group_id       グループID
     * @param mixed $draft_reg_flg  下書きデータ登録状態
     *
     * @return mixed $data 取得情報
     */
    function get_atdbk_for_report($cond_add, $calendar_array, $standard_id, $group_id, $draft_reg_flg) {
        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $day_cnt = count($calendar_array);
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[($day_cnt - 1)]["date"];
        //end_dateは翌月1日のため、duty_yyyy,duty_mm用に当月末を指定
        //$wk_this_mon_end = $calendar_array[($day_cnt - 2)]["date"];
        //4週の場合、年月が合わないので、2番目の日付を指定 20120305
        $wk_this_mon_end = $calendar_array[1]["date"];
        $duty_yyyy = substr($wk_this_mon_end, 0, 4);
        $duty_mm = intval(substr($wk_this_mon_end, 4, 2));

        //シフトグループ情報を取得
        $group_array = $this->get_group_info_for_report($standard_id, $group_id);

        //対象グループが存在しなければ、空の配列を返す
        if (count($group_array) == 0) {
            return array();
        }

        //シフト期間をチェック
        if (!$this->is_same_shift_term($group_array)) {
            return array("error_msg" => "シフトグループ間でシフト作成期間が異なる為、計算できません。");
        }

        //4週計算で後半部分が翌々月にまたがる場合、翌月を基準に職員情報を取得する 20141117
        $wk_duty_yyyy = $duty_yyyy;
        $wk_duty_mm = $duty_mm;
        $start_2_date = $calendar_array[1]["date"];
        $start_2_day = substr($start_2_date, 6, 2);
        $start_2_mon = substr($start_2_date, 4, 2);
        //4週28日＋前月1日＋翌月1日で30、開始日が1日以外で判断
        if ($day_cnt == 30 && $start_2_day != "01") {
            $end_2_date = $calendar_array[($day_cnt - 2)]["date"];
            $end_2_day = substr($end_2_date, 6, 2); //4週計算、最終日
            $end_2_mon = substr($end_2_date, 4, 2);
            $wk_group_end_day = $group_array[0]["end_day1"]; //シフト作成期間、翌月の日
            if ($end_2_day > $wk_group_end_day && $start_2_mon != $end_2_mon) { // 月も確認 20150527
                $wk_duty_mm++;
                if ($wk_duty_mm > 12) {
                    $wk_duty_mm = 1;
                    $wk_duty_yyyy ++;
                }
            }
        }
        //職員情報取得
        if (count($this->arr_emp_info) == 0) {
            $arr_emp_info = $this->get_emp_info_for_report($standard_id, $group_id, $wk_duty_yyyy, $wk_duty_mm);
            $this->arr_emp_info = $arr_emp_info;
        } else {
            $arr_emp_info = $this->arr_emp_info;
        }
        //期間情報取得
        //グループ間でシフト期間は同じなので、代表してインデックス0の期間情報を取得する
        $shift_term = $this->get_shift_term($group_array[0], $duty_yyyy, $duty_mm, $calendar_array);

        $g_str = "";
        for ($i = 0; $i < count($group_array); $i++) {
            if ($g_str != "") {
                $g_str .= ", ";
            }
            $g_str .= "'" . $group_array[$i]["group_id"] . "'";
        }

        //SQL文を生成
        $sqlsub = array();
        for ($i = 0; $i < count($shift_term); $i++) {
            $wk_start_date = $shift_term[$i]["start_date"];
            $wk_end_date = $shift_term[$i]["end_date"];
            $wk_year = $shift_term[$i]["year"];
            $wk_month = $shift_term[$i]["month"];

            $sqlsub[] = "select"
                    . " a.emp_id,"
                    . " a.date,"
                    . " a.pattern,"
                    . " a.reason,"
                    . " b.group_id,"
                    . " c.duty_date,"
                    . " c.group_id as assist_group_id,"
                    . " d.group_id as main_group_id,"
                    . " a.tmcd_group_id,"
                    . " replace(k.officehours2_start, ':', '') as start_time,"
                    . " replace(k.officehours2_end, ':', '') as end_time,"
                    . " l.previous_day_flag,"
                    . " j.type,"
                    . " l.meeting_start_time,"
                    . " l.meeting_end_time,"
                    . " l.over_24hour_flag"
                    . " from atdbk a "
                    . " left join duty_shift_plan_staff b on b.group_id in ($g_str) and b.emp_id = a.emp_id and b.duty_yyyy = $wk_year and b.duty_mm = $wk_month "
                    . " left join duty_shift_plan_assist c on c.emp_id = a.emp_id and c.duty_date = a.date "
                    . " left join duty_shift_staff d on d.emp_id = a.emp_id "
                    . " left join calendar j on j.date = a.date "
                    . " left join officehours k on k.tmcd_group_id = a.tmcd_group_id and k.pattern = a.pattern and k.officehours_id = CAST(case when j.type = '2' or j.type = '3' then j.type else '1' end AS varchar) "
                    . " left join atdptn l on l.group_id = a.tmcd_group_id and a.pattern = CAST(l.atdptn_id AS varchar) "
                    . " where a.date >= '$wk_start_date' and a.date <= '$wk_end_date' "
                    . " and b.group_id is not null "
                    . " and (c.group_id in ($g_str) or (c.group_id is null and (b.group_id in ($g_str))))";

            //下書きがある場合は、下書きも考慮する(シフト作成画面から呼ばれる場合のみ)
            //届出書添付書類画面から呼ばれる場合は施設基準単位で算出される場合があり、SQL文が冗長になるので、下書きの有無を判断する
            if ($draft_reg_flg == "1") {
                $sqlsub[] = "select"
                        . " a.emp_id,"
                        . " a.duty_date as date,"
                        . " CAST(a.atdptn_ptn_id AS varchar) as pattern,"
                        . " a.reason,"
                        . " b.group_id,"
                        . " c.duty_date,"
                        . " c.group_id as assist_group_id,"
                        . " d.group_id as main_group_id,"
                        . " a.pattern_id,"
                        . " replace(k.officehours2_start, ':', '') as start_time,"
                        . " replace(k.officehours2_end, ':', '') as end_time,"
                        . " l.previous_day_flag,"
                        . " j.type,"
                        . " l.meeting_start_time,"
                        . " l.meeting_end_time,"
                        . " l.over_24hour_flag"
                        . " from duty_shift_plan_draft a "
                        . " left join duty_shift_plan_staff b on b.group_id in ($g_str) and b.emp_id = a.emp_id and b.duty_yyyy = $wk_year and b.duty_mm = $wk_month "
                        . " left join duty_shift_plan_assist c on c.emp_id = a.emp_id and c.duty_date = a.duty_date "
                        . " left join duty_shift_staff d on d.emp_id = a.emp_id "
                        . " left join calendar j on j.date = a.duty_date "
                        . " left join officehours k on k.tmcd_group_id = a.pattern_id and k.pattern = CAST(a.atdptn_ptn_id AS varchar) and k.officehours_id = CAST(case when j.type = '2' or j.type = '3' then j.type else '1' end AS varchar) "
                        . " left join atdptn l on l.group_id = a.pattern_id and a.atdptn_ptn_id = l.atdptn_id "
                        . " where a.duty_date >= '$wk_start_date' and a.duty_date <= '$wk_end_date' "
                        . " and b.group_id is not null "
                        . " and (c.group_id in ($g_str) or (c.group_id is null and (b.group_id in ($g_str))))";
            }
        }
        //件数の確認
        if (!($group_id != "" && $group_id != "0")) {
            $cnt_sql = "";
            foreach ($sqlsub as $value) {
                if ($cnt_sql === "") {
                    $cnt_sql = $value;
                } else {
                    $cnt_sql .= " union $value";
                }
            }
            $cnt_sql = "select e.group_id, e.emp_id, count(*) as cnt from ( $cnt_sql";
            $cnt_sql .= " ) e group by e.group_id, e.emp_id ";
            $cnt_sel = select_from_table($this->_db_con, $cnt_sql, $cond, $this->file_name);
            if ($cnt_sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            $arr_data_cnt = array();
            while ($row = pg_fetch_array($cnt_sel)) {
                $wk_group_id = $row["group_id"];
                $wk_emp_id = $row["emp_id"];
                $arr_data_cnt[$wk_group_id][$wk_emp_id] = $row["cnt"];
            }
            $arr_emp_info_old = $arr_emp_info;

            //データ件数確認
            //1職員で複数グループある場合、先に出てきたグループ
            $arr_emp_info = array();
            $arr_emp_chk = array();
            $idx = 0;
            for ($i = 0; $i < count($arr_emp_info_old); $i++) {
                $wk_emp_id = $arr_emp_info_old[$i]["emp_id"];
                $wk_group_id = $arr_emp_info_old[$i]["group_id"];
                if ($arr_data_cnt[$wk_group_id][$wk_emp_id] > 0 &&
                        $arr_emp_chk[$wk_emp_id] == "") {
                    $arr_emp_chk[$wk_emp_id] = $idx + 1;
                    $arr_emp_info[$idx] = $arr_emp_info_old[$i];
                    $arr_emp_info[$idx]["no"] = $idx + 1;
                    $idx++;
                }
            }
        }

        //職員の表示順
        $arr_emp_no = array();
        for ($i = 0; $i < count($arr_emp_info); $i++) {
            $tmp_emp_id = $arr_emp_info[$i]["emp_id"];
            $arr_emp_no[$tmp_emp_id] = $arr_emp_info[$i]["no"];
        }

        $sql = "";
        foreach ($sqlsub as $value) {
            if ($sql === "") {
                $sql = $value;
            } else {
                $sql .= " union $value";
            }
        }
        $sql = "select * from ( $sql ) e order by e.emp_id, e.date ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);

        //-------------------------------------------------------------------
        // ＤＢ値設定
        //-------------------------------------------------------------------
        $data = array();
        $cnt = 0;
        $wk_staff_id = "";
        $arr_no = array();
        for ($i = 0; $i < $num; $i++) {
            //-------------------------------------------------------------------
            //対象データか判定
            //-------------------------------------------------------------------
            $key_staff_id = pg_fetch_result($sel, $i, "emp_id");
            if ($arr_emp_no[$key_staff_id] != "") {
                $cnt = $arr_emp_no[$key_staff_id] - 1;
            } else { //20141023
                continue;
            }
            //-------------------------------------------------------------------
            //ＤＢより取得値
            //-------------------------------------------------------------------
            $data[$cnt]["staff_id"] = $key_staff_id;
            $data[$cnt]["assist_group_id"] = pg_fetch_result($sel, $i, "assist_group_id");
            $data[$cnt]["main_group_id"] = pg_fetch_result($sel, $i, "main_group_id");
            $data[$cnt]["assist_group_name"] = pg_fetch_result($sel, $i, "assist_group_name");
            $data[$cnt]["st_nm"] = pg_fetch_result($sel, $i, "st_nm");

            $key_night_duty = pg_fetch_result($sel, $i, "night_duty");                //当直（１：有り、２：無し）
            $key_date = trim(pg_fetch_result($sel, $i, "date"));                  //年月日
            $key_start_time = trim(pg_fetch_result($sel, $i, "start_time"));      //開始時刻
            $key_end_time = trim(pg_fetch_result($sel, $i, "end_time"));          //終了時刻
            $key_meeting_time = trim(pg_fetch_result($sel, $i, "meeting_time"));  //研修・会議時間
            $key_out_time = trim(pg_fetch_result($sel, $i, "out_time"));  //外出時刻
            $key_ret_time = trim(pg_fetch_result($sel, $i, "ret_time"));  //復帰時刻
            $key_pattern_id = trim(pg_fetch_result($sel, $i, "tmcd_group_id"));       //出勤グループＩＤ
            $key_atdptn_ptn_id = trim(pg_fetch_result($sel, $i, "pattern"));      //出勤パターンＩＤ
            $key_reason = trim(pg_fetch_result($sel, $i, "reason"));              //事由
            $key_meeting_start_time = trim(pg_fetch_result($sel, $i, "meeting_start_time"));      //研修・会議開始時刻
            $key_meeting_end_time = trim(pg_fetch_result($sel, $i, "meeting_end_time"));          //研修・会議終了時刻
            $key_previous_day_flag = trim(pg_fetch_result($sel, $i, "previous_day_flag"));            //
            $key_over_24hour_flag = trim(pg_fetch_result($sel, $i, "over_24hour_flag"));          //
            //-------------------------------------------------------------------
            //年月日をキーに設定
            //-------------------------------------------------------------------
            $m = ((date_utils::to_timestamp_from_ymd($key_date) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
            $data[$cnt]["night_duty_$m"] = $key_night_duty;         //当直（１：有り、２：無し）
            $data[$cnt]["pattern_id_$m"] = $key_pattern_id;         //出勤グループＩＤ
            $data[$cnt]["atdptn_ptn_id_$m"] = $key_atdptn_ptn_id;   //出勤パターンＩＤ
            $data[$cnt]["standard_id_$m"] = $standard_id;   //施設基準
            //-------------------------------------------------------------------
            //開始／終了時刻
            //研修・会議時間
            //-------------------------------------------------------------------
            $data[$cnt]["start_time_$m"] = $key_start_time;         //開始時刻
            $data[$cnt]["end_time_$m"] = $key_end_time;             //終了時刻
            $data[$cnt]["meeting_time_$m"] = $key_meeting_time;     //研修・会議時間
            $data[$cnt]["out_time_$m"] = $key_out_time;         //外出時刻
            $data[$cnt]["ret_time_$m"] = $key_ret_time;         //復帰時刻
            $data[$cnt]["meeting_start_time_$m"] = $key_meeting_start_time;     //研修・会議開始時刻
            $data[$cnt]["meeting_end_time_$m"] = $key_meeting_end_time;         //研修・会議終了時刻
            $data[$cnt]["previous_day_flag_$m"] = $key_previous_day_flag;               //
            $data[$cnt]["over_24hour_flag_$m"] = $key_over_24hour_flag;             //
        }
        //名前設定
        for ($i = 0; $i < count($data); $i++) {
            $staff_id = $data[$i]["staff_id"];
            $no = $arr_emp_no[$staff_id] - 1;
            $data[$i]["staff_name"] = $arr_emp_info[$no]["emp_name"];
            $data[$i]["main_group_name"] = $arr_emp_info[$no]["group_name"];
        }

        return $data;
    }

    /**
     * 実績、予定組合せ取得
     *
     * @param mixed $cond_add 検索条件
     * @param mixed $calendar_array カレンダー情報
     * @param mixed $standard_id 施設基準
     * @param mixed $group_id グループID
     * @param mixed $rslt_array 実績情報
     * @param mixed $plan_array 予定情報
     * @return mixed $data          取得情報
     *
     */
    function get_atdbkrslt_atdbk_for_report($cond_add, $calendar_array, $standard_id, $group_id, $rslt_array, $plan_array) {

        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $day_cnt = count($calendar_array);
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[($day_cnt - 1)]["date"];
        //end_dateは翌月1日のため、get_atdbk_for_reportと同様2番目の日付を指定
        $wk_this_mon_end = $calendar_array[1]["date"];
        $duty_yyyy = substr($wk_this_mon_end, 0, 4);
        $duty_mm = intval(substr($wk_this_mon_end, 4, 2));

        //実績、予定取得時に設定したものを使用するのでコメント化 20141117
        //$arr_emp_info = $this->get_emp_info_for_report($standard_id, $group_id, $duty_yyyy, $duty_mm);
        //-------------------------------------------------------------------
        //エラー処理
        //-------------------------------------------------------------------
        //実績情報か予定情報のどちらかにエラーメッセージがセットされていたら、その配列をそのまま返す
        if (isset($rslt_array["error_msg"])) {
            return $rslt_array;
        }

        if (isset($plan_array["error_msg"])) {
            return $plan_array;
        }


        //rslt,planのデータを確認
        $arr_exist_flg = array();
        for ($i = 0; $i < count($rslt_array); $i++) {
            $wk_emp_id = $rslt_array[$i]["staff_id"];
            $arr_exist_flg[$wk_emp_id] = 1;
        }
        for ($i = 0; $i < count($plan_array); $i++) {
            $wk_emp_id = $plan_array[$i]["staff_id"];
            $arr_exist_flg[$wk_emp_id] = 1;
        }
        //20141117
        $arr_emp_info_old = $this->arr_emp_info;
        $arr_emp_info = array();
        $arr_emp_chk = array();
        $idx = 0;
        for ($i = 0; $i < count($arr_emp_info_old); $i++) {
            $wk_emp_id = $arr_emp_info_old[$i]["emp_id"];
            $wk_group_id = $arr_emp_info_old[$i]["group_id"];
            if ($arr_exist_flg[$wk_emp_id] > 0 &&
                    $arr_emp_chk[$wk_emp_id] == "") {
                $arr_emp_chk[$wk_emp_id] = $idx + 1;
                $arr_emp_info[$idx] = $arr_emp_info_old[$i];
                $arr_emp_info[$idx]["no"] = $idx + 1;
                $idx++;
            }
        }

        $arr_group_staff_idx = array();
        $arr_staff_idx = array();
        $data = array();
        for ($i = 0; $i < count($arr_emp_info); $i++) {
            $wk_emp_id = $arr_emp_info[$i]["emp_id"];
            $wk_group_id = $arr_emp_info[$i]["group_id"];
            $data[$i]["staff_id"] = $wk_emp_id;
            $data[$i]["main_group_id"] = $wk_group_id;
            $arr_group_staff_idx[$wk_group_id][$wk_emp_id] = $i;
            $arr_staff_idx[$wk_emp_id] = $i;
        }

        //実績データを設定、日付確認
        $today = date("Ymd");
        $day_idx = ((date_utils::to_timestamp_from_ymd($today) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
        if ($day_idx >= 1) {

            $wk_day_max = min($day_idx, $day_cnt + 1); //処理対象日位置

            for ($i = 0; $i < count($rslt_array); $i++) {
                $wk_emp_id = $rslt_array[$i]["staff_id"];
                if ($rslt_array[$i]["assist_group_id"] != "") {
                    $wk_group_id = $rslt_array[$i]["assist_group_id"];
                } else {
                    $wk_group_id = $rslt_array[$i]["main_group_id"];
                }

                $wk_idx = $arr_staff_idx[$wk_emp_id];

                $data[$wk_idx]["staff_name"] = $rslt_array[$i]["staff_name"];
                $data[$wk_idx]["main_group_name"] = $rslt_array[$i]["main_group_name"];
                $data[$wk_idx]["st_nm"] = $rslt_array[$i]["st_nm"];
                $data[$wk_idx]["staff_id"] = $rslt_array[$i]["staff_id"];

                //データ件数
                $wk_day_data_cnt = 0;
                //日毎データ、前日まで
                for ($j = 1; $j < $wk_day_max; $j++) {
                    $data[$wk_idx]["pattern_id_$j"] = $rslt_array[$i]["pattern_id_$j"];         //出勤グループＩＤ
                    $data[$wk_idx]["atdptn_ptn_id_$j"] = $rslt_array[$i]["atdptn_ptn_id_$j"];       //出勤パターンＩＤ
                    $data[$wk_idx]["standard_id_$j"] = $rslt_array[$i]["standard_id_$j"];       //施設基準

                    $data[$wk_idx]["start_time_$j"] = $rslt_array[$i]["start_time_$j"];     //開始時刻
                    $data[$wk_idx]["end_time_$j"] = $rslt_array[$i]["end_time_$j"];     //終了時刻
                    $data[$wk_idx]["meeting_time_$j"] = $rslt_array[$i]["meeting_time_$j"];     //研修・会議時間
                    $data[$wk_idx]["out_time_$j"] = $rslt_array[$i]["out_time_$j"];     //外出時刻
                    $data[$wk_idx]["ret_time_$j"] = $rslt_array[$i]["ret_time_$j"];     //復帰時刻
                    $data[$wk_idx]["meeting_start_time_$j"] = $rslt_array[$i]["meeting_start_time_$j"];     //研修・会議開始時刻
                    $data[$wk_idx]["meeting_end_time_$j"] = $rslt_array[$i]["meeting_end_time_$j"];     //研修・会議終了時刻
                    $data[$wk_idx]["previous_day_flag_$j"] = $rslt_array[$i]["previous_day_flag_$j"];       //前日フラグ
                    $data[$wk_idx]["next_day_flag_$j"] = $rslt_array[$i]["next_day_flag_$j"];       //翌日フラグ
                    if ($rslt_array[$i]["start_time_$j"] != "" &&
                            $rslt_array[$i]["end_time_$j"]) {
                        $wk_day_data_cnt++;
                    }
                }

                //データ件数
                $data[$wk_idx]["day_data_cnt"] = $wk_day_data_cnt;
            }
        }
        //予定データを設定
        if ($day_idx <= $day_cnt) {

            if ($day_idx < 1) {
                $wk_day_max = 1;
            } else {
                $wk_day_max = $day_idx;
            }

            for ($i = 0; $i < count($plan_array); $i++) {
                $wk_emp_id = $plan_array[$i]["staff_id"];
                if ($plan_array[$i]["assist_group_id"] != "") {
                    $wk_group_id = $plan_array[$i]["assist_group_id"];
                } else {
                    $wk_group_id = $plan_array[$i]["main_group_id"];
                }
                $wk_idx = $arr_staff_idx[$wk_emp_id];

                $data[$wk_idx]["staff_name"] = $plan_array[$i]["staff_name"];
                $data[$wk_idx]["main_group_name"] = $plan_array[$i]["main_group_name"];
                $data[$wk_idx]["st_nm"] = $plan_array[$i]["st_nm"];
                $data[$wk_idx]["staff_id"] = $plan_array[$i]["staff_id"];

                //データ件数
                $wk_day_data_cnt = 0;
                //日毎データ、当日以降
                for ($j = $wk_day_max; $j <= $day_cnt; $j++) {
                    $data[$wk_idx]["pattern_id_$j"] = $plan_array[$i]["pattern_id_$j"];         //出勤グループＩＤ
                    $data[$wk_idx]["atdptn_ptn_id_$j"] = $plan_array[$i]["atdptn_ptn_id_$j"];       //出勤パターンＩＤ
                    $data[$wk_idx]["standard_id_$j"] = $plan_array[$i]["standard_id_$j"];       //施設基準

                    $data[$wk_idx]["start_time_$j"] = $plan_array[$i]["start_time_$j"];     //開始時刻
                    $data[$wk_idx]["end_time_$j"] = $plan_array[$i]["end_time_$j"];     //終了時刻
                    $data[$wk_idx]["meeting_time_$j"] = $plan_array[$i]["meeting_time_$j"];     //研修・会議時間
                    $data[$wk_idx]["out_time_$j"] = $plan_array[$i]["out_time_$j"];     //外出時刻
                    $data[$wk_idx]["ret_time_$j"] = $plan_array[$i]["ret_time_$j"];     //復帰時刻
                    $data[$wk_idx]["meeting_start_time_$j"] = $plan_array[$i]["meeting_start_time_$j"];     //研修・会議開始時刻
                    $data[$wk_idx]["meeting_end_time_$j"] = $plan_array[$i]["meeting_end_time_$j"];     //研修・会議終了時刻
                    $data[$wk_idx]["previous_day_flag_$j"] = $plan_array[$i]["previous_day_flag_$j"];       //前日フラグ
                    $data[$wk_idx]["over_24hour_flag_$j"] = $plan_array[$i]["over_24hour_flag_$j"];     //24時間超フラグ
                    if ($plan_array[$i]["start_time_$j"] != "" &&
                            $plan_array[$i]["end_time_$j"]) {
                        $wk_day_data_cnt++;
                    }
                }
                //データ件数
                $data[$wk_idx]["day_data_cnt"] += $wk_day_data_cnt;
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 半日応援情報取得（様式９用）
    // @param   $standard_id        施設基準
    // @param   $duty_yyyy          勤務年
    // @param   $suty_mm            勤務月
    // @param   $cond_add           検索条件
    // @param   $calendar_array     カレンダー情報
    // @param   $group_id           グループID
    // @param   $rslt_array     勤務実績情報(※職員位置等確認）
    // @param   $plan_result_flag   予定実績フラグ 1:実績 2:予定 3:システム前日まで実績、当日以降予定使用
    //
    // @return  $data       取得情報
    /*     * ********************************************************************** */
    function get_rslt_assist_array($standard_id, $duty_yyyy, $duty_mm, $cond_add, $calendar_array, $group_id, $rslt_array, $plan_result_flag) {
        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $day_cnt = count($calendar_array);
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[($day_cnt - 1)]["date"];
        $wk_duty_mm = intval($duty_mm);

        $today = date("Ymd");
        $wk_plan_result_flag = $plan_result_flag;
        if ($wk_plan_result_flag == "3") {
            //システム日が開始日より前の場合、予定から取得
            if ($today < $start_date) {
                $wk_plan_result_flag = "1";
            }
            //システム日が終了日より後の場合、実績から取得
            if ($today > $end_date) {
                $wk_plan_result_flag = "2";
            }
        }

        //半日応援情報
        if ($wk_plan_result_flag == "1" || //実績
                $wk_plan_result_flag == "2") { //予定
            $sql = "select a.emp_id, a.date, a.tmcd_group_id, a.pattern, b.assist_group_id, b.assist_start_time, b.assist_end_time, c.standard_id ";
            if ($wk_plan_result_flag == "1") { //実績
                $sql .= ", a.previous_day_flag from atdbkrslt a ";
            } else { //予定
                $sql .= ", b.previous_day_flag from atdbk a ";
            }
            $sql .= "left join atdptn b on a.tmcd_group_id = b.group_id and a.pattern = CAST(b.atdptn_id AS varchar) ";
            $sql .= "left join duty_shift_group c on b.assist_group_id = c.group_id ";
            $cond = "where a.date >= '$start_date' and a.date <= '$end_date' and b.assist_time_flag = 1 and b.assist_start_time != '' ";
            if ($group_id != "" && $group_id != "0") {
                $cond .= "and b.assist_group_id = '$group_id' ";
            } else {
                $cond .= "and c.standard_id = '$standard_id' ";
            }
            $cond .= "order by a.emp_id, a.date";
        } else { //"3":実績と予定
            //前日まで実績、当日以降予定から取得
            $sql = "select * from (select a.emp_id, a.date, a.tmcd_group_id, a.pattern, b.assist_group_id, b.assist_start_time, b.assist_end_time, c.standard_id ";
            $sql .= ", a.previous_day_flag from atdbkrslt a ";
            $sql .= "left join atdptn b on a.tmcd_group_id = b.group_id and a.pattern = CAST(b.atdptn_id AS varchar) ";
            $sql .= "left join duty_shift_group c on b.assist_group_id = c.group_id ";
            $sql .= "where a.date >= '$start_date' and a.date < '$today' and b.assist_time_flag = 1 and b.assist_start_time != '' ";
            if ($group_id != "" && $group_id != "0") {
                $sql .= "and b.assist_group_id = '$group_id' ";
            } else {
                $sql .= "and c.standard_id = '$standard_id' ";
            }

            $sql .= " union ";
            $sql .= " select a.emp_id, a.date, a.tmcd_group_id, a.pattern, b.assist_group_id, b.assist_start_time, b.assist_end_time, c.standard_id ";
            $sql .= ", b.previous_day_flag from atdbk a ";
            $sql .= "left join atdptn b on a.tmcd_group_id = b.group_id and a.pattern = CAST(b.atdptn_id AS varchar) ";
            $sql .= "left join duty_shift_group c on b.assist_group_id = c.group_id ";
            $sql .= "where a.date >= '$today' and a.date <= '$end_date' and b.assist_time_flag = 1 and b.assist_start_time != '' ";
            if ($group_id != "" && $group_id != "0") {
                $sql .= "and b.assist_group_id = '$group_id' ";
            } else {
                $sql .= "and c.standard_id = '$standard_id' ";
            }

            $sql .= ") d order by d.emp_id, d.date";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        $assist_data_cnt = $num;
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $arr_half_assist = array();
        $arr_assist_emp_chk = array(); //職員存在チェック用
        for ($i = 0; $i < $num; $i++) {

            $wk_emp_id = pg_fetch_result($sel, $i, "emp_id");
            $arr_half_assist[$i]["emp_id"] = $wk_emp_id;
            $arr_half_assist[$i]["date"] = pg_fetch_result($sel, $i, "date");
            $arr_half_assist[$i]["tmcd_group_id"] = pg_fetch_result($sel, $i, "tmcd_group_id");
            $arr_half_assist[$i]["pattern"] = pg_fetch_result($sel, $i, "pattern");
            $arr_half_assist[$i]["previous_day_flag"] = pg_fetch_result($sel, $i, "previous_day_flag");
            $arr_half_assist[$i]["assist_group_id"] = pg_fetch_result($sel, $i, "assist_group_id");
            $arr_half_assist[$i]["assist_start_time"] = pg_fetch_result($sel, $i, "assist_start_time");
            $arr_half_assist[$i]["assist_end_time"] = pg_fetch_result($sel, $i, "assist_end_time");
            $arr_half_assist[$i]["standard_id"] = pg_fetch_result($sel, $i, "standard_id");

            $arr_assist_emp_chk[$wk_emp_id] = $arr_half_assist[$i]["assist_group_id"];
        }
        //rsltデータの職員に無ければ追加する
        foreach ($arr_assist_emp_chk as $wk_emp_id => $wk_group_id) {
            $exist_flg = false;
            for ($i = 0; $i < count($rslt_array); $i++) {
                if ($group_id == "0") { //全病棟一括、グループ、職員IDを比較
                    if ($wk_emp_id == $rslt_array[$i]["staff_id"] &&
                            $wk_group_id == $rslt_array[$i]["main_group_id"]) {
                        $exist_flg = true;
                        break;
                    }
                } else {//病棟グループ指定
                    if ($wk_emp_id == $rslt_array[$i]["staff_id"]) {
                        $exist_flg = true;
                        break;
                    }
                }
            }
            if ($exist_flg == false) {
                $idx = count($rslt_array);
                $rslt_array[$idx]["staff_id"] = $wk_emp_id;
                $sql = "select ";
                $sql .= "a.emp_id, a.emp_lt_nm, a.emp_ft_nm, g.st_nm ";
                $sql .= "from empmst as a ";
                $sql .= "left join stmst g on g.st_id = a.emp_st ";
                $cond = "where a.emp_id = '$wk_emp_id' ";
                $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                if ($sel == 0) {
                    pg_close($this->_db_con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $num = pg_num_rows($sel);
                if ($num > 0) {
                    $rslt_array[$idx]["staff_name"] = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
                    $rslt_array[$idx]["st_nm"] = pg_fetch_result($sel, 0, "st_nm");
                }

                if ($group_id == "0") { //全病棟一括
                    $rslt_array[$idx]["main_group_id"] = $wk_group_id;
                }
            }
        }
        //-------------------------------------------------------------------
        // ＤＢ値設定
        //-------------------------------------------------------------------
        $data = array();

        for ($i = 0; $i < $assist_data_cnt; $i++) {
            //職員の位置
            $staff_idx = -1;
            for ($j = 0; $j < count($rslt_array); $j++) {
                if ($group_id == "0") { //全病棟一括、グループ、職員IDを比較
                    if ($rslt_array[$j]["staff_id"] == $arr_half_assist[$i]["emp_id"] &&
                            $rslt_array[$j]["main_group_id"] == $arr_half_assist[$i]["assist_group_id"]) {
                        $staff_idx = $j;
                        break;
                    }
                } else { //病棟グループ指定
                    if ($rslt_array[$j]["staff_id"] == $arr_half_assist[$i]["emp_id"]) {
                        $staff_idx = $j;
                        break;
                    }
                }
            }
            if ($staff_idx != -1) {
                $data[$staff_idx]["staff_id"] = $rslt_array[$staff_idx]["staff_id"];
                $data[$staff_idx]["staff_name"] = $rslt_array[$staff_idx]["staff_name"];
                $data[$staff_idx]["st_nm"] = $rslt_array[$staff_idx]["st_nm"];
                //$data[$staff_idx]["staff_idx"] =　$staff_idx; //位置
                //日付位置
                $date_idx = ((date_utils::to_timestamp_from_ymd($arr_half_assist[$i]["date"]) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;

                $data[$staff_idx]["assist_group_id_$date_idx"] = $arr_half_assist[$i]["assist_group_id"];
                $data[$staff_idx]["assist_start_time_$date_idx"] = $arr_half_assist[$i]["assist_start_time"];
                $data[$staff_idx]["assist_end_time_$date_idx"] = $arr_half_assist[$i]["assist_end_time"];
                $data[$staff_idx]["standard_id_$date_idx"] = $arr_half_assist[$i]["standard_id"];
                $data[$staff_idx]["pattern_id_$date_idx"] = $arr_half_assist[$i]["tmcd_group_id"];
                $data[$staff_idx]["atdptn_ptn_id_$date_idx"] = $arr_half_assist[$i]["pattern"];
                $data[$staff_idx]["previous_day_flag_$date_idx"] = $arr_half_assist[$i]["previous_day_flag"];
                //データ件数
                $data[$staff_idx]["cnt"] = ($data[$staff_idx]["cnt"] == "") ? 1 : intval($data[$staff_idx]["cnt"]) + 1;

                //グループID、追加
                $data[$staff_idx]["main_group_id"] = $arr_half_assist[$i]["assist_group_id"];
            }
        }
        //データ件数が無い職員データには、0を設定
        for ($i = 0; $i < count($rslt_array); $i++) {
            if ($data[$i]["cnt"] == "") {
                $data[$i]["cnt"] = 0;
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 病棟情報取得（様式９用）
    //※get_group_id_from_standard_idに置き換えるため未使用。
    // @param   $standard_id        施設基準
    // @param   $duty_yyyy          勤務年
    // @param   $suty_mm            勤務月
    // @param   $cond_add           検索条件
    // @param   $calendar_array     カレンダー情報
    // @param   $flg                1:看護職員 2:看護補助者
    //
    // @return  $data       取得情報
    /*     * ********************************************************************** */
    function get_group_id_for_report($standard_id, $duty_yyyy, $duty_mm, $cond_add, $calendar_array, $flg) {

        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $day_cnt = count($calendar_array);
        $start_date = $calendar_array[0]["date"];
        $end_date = $calendar_array[($day_cnt - 1)]["date"];
        //作成期間が月またがりで、終了日より後にだけ登録がある場合、データが取得されない不具合対応 20110802
        $duty_yyyy2 = $duty_yyyy;
        $duty_mm2 = $duty_mm + 1;
        if ($duty_mm2 > 12) {
            $duty_mm2 = 1;
            $duty_yyyy2++;
        }

        //施設基準が一致するグループの職員の勤務情報を確認して、データがあるグループを取得
        $sql = "select group_id,group_name from duty_shift_group where group_id in (select ";
        $sql .= "case when e.group_id is null then f.group_id else e.group_id end as g_id ";
        $sql .= "from duty_shift_atdbkrslt_history as a ";
        $sql .= "inner join empmst as b on a.emp_id = b.emp_id ";
        $sql .= "left join duty_shift_plan_assist c on c.emp_id = a.emp_id and c.duty_date = a.date ";
//      $sql .= "left join duty_shift_staff d on d.emp_id = a.emp_id ";
        // 異動があった場合、過去月に出力されない不具合の対応 20090929
//      $sql .= "left join duty_shift_plan_staff d on d.emp_id = a.emp_id and d.duty_yyyy = $duty_yyyy and d.duty_mm = $duty_mm ";
        $sql .= "left join ";
        $sql .= "(select distinct on (group_id, emp_id) * from duty_shift_plan_staff where ((duty_yyyy = $duty_yyyy and duty_mm = $duty_mm) or (duty_yyyy = $duty_yyyy2 and duty_mm = $duty_mm2)) ";
        $sql .= ") d ";
        $sql .= " on d.emp_id = a.emp_id ";
        $sql .= "left join duty_shift_group e on e.group_id = c.group_id ";
        $sql .= "left join duty_shift_group f on f.group_id = d.group_id ";
//      $sql .= "left join stmst g on g.st_id = b.emp_st ";
        $sql .= "left join jobmst h on h.job_id = b.emp_job ";
        $sql .= "where (e.standard_id = '$standard_id' or (e.standard_id is null and f.standard_id = '$standard_id')) ";
        $sql .= "and a.duty_yyyy = $duty_yyyy ";
        $sql .= "and a.duty_mm = $duty_mm ";
        $sql .= "and a.start_time != '' and a.end_time != '' "; //時刻未設定を除く
        //応援以外の場合メイン所属グループか未設定（職員設定の所属なしで応援）、応援の場合応援グループ
//      $sql .= "and ((c.group_id = '' and d.main_group_id = d.group_id ) or (c.group_id != '' and c.group_id = d.group_id)) ";
        $sql .= "and ((c.group_id is null and (d.main_group_id = d.group_id or d.main_group_id is null)) or (c.group_id is not null and c.group_id = d.group_id)) ";
        if ($flg == "1") {
            $sql .= "and h.job_nm in ('看護師','准看護師','保健師','助産師') ";
        } else {
            $sql .= "and h.job_id in (select job_id from duty_shift_rpt_assist_job) ";
        }
        $sql .= "group by g_id )";

        $cond = "order by group_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        $data = array();
        // データなしの場合、勤務実績情報取得
        if ($num <= 0) {
//          $data = $this->get_atdbk_atdbkrslt_array_for_report("atdbkrslt", $cond_add, $calendar_array, $group_id);
            $sql = "select group_id,group_name from duty_shift_group where group_id in (select ";
            $sql .= "case when e.group_id is null then f.group_id else e.group_id end as g_id ";
            $sql .= "from atdbkrslt as a inner join empmst as b ";
            $sql .= "on a.emp_id = b.emp_id ";
            $sql .= "left join duty_shift_plan_assist c on c.emp_id = a.emp_id and c.duty_date = a.date ";
//          $sql .= "left join duty_shift_staff d on d.emp_id = a.emp_id ";
// 異動があった場合、過去月に出力されない不具合の対応 20090929
            $sql .= "left join duty_shift_plan_staff d on d.emp_id = a.emp_id and ((duty_yyyy = $duty_yyyy and duty_mm = $duty_mm) or (duty_yyyy = $duty_yyyy2 and duty_mm = $duty_mm2)) ";
            $sql .= "left join duty_shift_group e on e.group_id = c.group_id ";
            $sql .= "left join duty_shift_group f on f.group_id = d.group_id ";
//          $sql .= "left join stmst g on g.st_id = b.emp_st ";
            $sql .= "left join jobmst h on h.job_id = b.emp_job ";
            $sql .= "where ((a.date > '$start_date' and a.date <= '$end_date' and a.start_time != '' and a.end_time != '') or ";
            $sql .= " (a.date = '$start_date' and (a.start_time > a.end_time or a.next_day_flag = 1)) )";
            $sql .= "and (e.standard_id = '$standard_id' or (e.standard_id is null and f.standard_id = '$standard_id')) ";
            $sql .= "and ((c.group_id is null and (d.main_group_id = d.group_id or d.main_group_id is null)) or (c.group_id is not null and c.group_id = d.group_id)) ";
            if ($cond_add != "") {
                $sql .= $cond_add;
            }
            if ($flg == "1") {
                $sql .= "and h.job_nm in ('看護師','准看護師','保健師','助産師') ";
            } else {
                $sql .= "and h.job_id in (select job_id from duty_shift_rpt_assist_job) ";
            }
            $sql .= "group by g_id ) ";
            $cond = "order by group_id ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $num = pg_num_rows($sel);
            // データありの場合
            if ($num > 0) {
                for ($i = 0; $i < $num; $i++) {
                    $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
                    $data[$i]["group_name"] = pg_fetch_result($sel, $i, "group_name");
                }
            }
        } else {
            // データありの場合
            for ($i = 0; $i < $num; $i++) {
                $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
                $data[$i]["group_name"] = pg_fetch_result($sel, $i, "group_name");
            }
        }

        //半日応援情報
        $sql = "select c.group_id, c.group_name from atdbkrslt a ";
        $sql .= "inner join empmst as d on a.emp_id = d.emp_id ";
        $sql .= "left join atdptn b on a.tmcd_group_id = b.group_id and a.pattern = CAST(b.atdptn_id AS varchar) ";
        $sql .= "left join duty_shift_group c on b.assist_group_id = c.group_id ";
        $sql .= "left join jobmst h on h.job_id = d.emp_job ";
        $sql .= "where a.date >= '$start_date' and a.date <= '$end_date' and b.assist_time_flag = 1 and b.assist_start_time != '' ";
        $sql .= "and c.standard_id = '$standard_id' ";
        if ($flg == "1") {
            $sql .= "and h.job_nm in ('看護師','准看護師','保健師','助産師') ";
        } else {
            $sql .= "and exists (select job_id from duty_shift_rpt_assist_job i where i.job_id = h.job_id) ";
        }
        $cond = "group by c.group_id, c.group_name order by c.group_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        $arr_group = array();
        for ($i = 0; $i < $num; $i++) {

            $wk_group_id = pg_fetch_result($sel, $i, "group_id");
            $wk_group_name = pg_fetch_result($sel, $i, "group_name");
            //$dataになければ追加
            $wk_exist_flg = false;
            for ($j = 0; $j < count($data); $j++) {
                if ($wk_group_id == $data[$j]["group_id"]) {
                    $wk_exist_flg = true;
                    break;
                }
            }
            if ($wk_exist_flg == false) {
                $wk_count = count($data);
                $data[$wk_count]["group_id"] = $wk_group_id;
                $data[$wk_count]["group_name"] = $wk_group_name;
            }
        }

        return $data;
    }

    /**
     * 指定された施設基準の病棟ID取得
     *
     * @param mixed $standard_id 施設基準
     * @return mixed $data      取得情報
     */
    function get_group_id_from_standard_id($standard_id) {
        $sql = "select group_id, group_name from duty_shift_group ";
        $cond = "where standard_id = '$standard_id' ";
        $cond .= "order by order_no ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        $data = array();
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
                $data[$i]["group_name"] = pg_fetch_result($sel, $i, "group_name");
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループ情報取得
    // @param   $group_id           勤務シフトグループＩＤ
    // @param   $data_emp           職員情報 未使用に変更(2008/08/29)
    // @param   $data_wktmgrp       勤務パターングループ情報
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp) {
        //-------------------------------------------------------------------
        //勤務シフトグループ情報ＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "select a.*, " .
                " b.emp_lt_nm||' '||b.emp_ft_nm as person_charge_name, " .
                " c.emp_lt_nm||' '||c.emp_ft_nm as caretaker_name, " .
                " d.emp_lt_nm||' '||d.emp_ft_nm as caretaker2_name, " .
                " e.emp_lt_nm||' '||e.emp_ft_nm as caretaker3_name, " .
                " f.emp_lt_nm||' '||f.emp_ft_nm as caretaker4_name, " .
                " g.emp_lt_nm||' '||g.emp_ft_nm as caretaker5_name, " .
                " h.emp_lt_nm||' '||h.emp_ft_nm as caretaker6_name, " .
                " i.emp_lt_nm||' '||i.emp_ft_nm as caretaker7_name, " .
                " j.emp_lt_nm||' '||j.emp_ft_nm as caretaker8_name, " .
                " k.emp_lt_nm||' '||k.emp_ft_nm as caretaker9_name, " .
                " l.emp_lt_nm||' '||l.emp_ft_nm as caretaker10_name " .
                " from duty_shift_group a " .
                "  left join empmst b on b.emp_id = a.person_charge_id " .
                "  left join empmst c on c.emp_id = a.caretaker_id " .
                "  left join empmst d on d.emp_id = a.caretaker2_id " .
                "  left join empmst e on e.emp_id = a.caretaker3_id " .
                "  left join empmst f on f.emp_id = a.caretaker4_id" .
                "  left join empmst g on g.emp_id = a.caretaker5_id " .
                "  left join empmst h on h.emp_id = a.caretaker6_id " .
                "  left join empmst i on i.emp_id = a.caretaker7_id" .
                "  left join empmst j on j.emp_id = a.caretaker8_id " .
                "  left join empmst k on k.emp_id = a.caretaker9_id " .
                "  left join empmst l on l.emp_id = a.caretaker10_id";
        if ($group_id == "") {
            $cond = "where 1=1 order by a.order_no, a.group_id";
        } else {
            $cond = "where a.group_id = '$group_id'";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = trim(pg_fetch_result($sel, $i, "group_id"));                  //グループID
            $data[$i]["group_name"] = trim(pg_fetch_result($sel, $i, "group_name"));              //グループ名称
            $data[$i]["standard_id"] = trim(pg_fetch_result($sel, $i, "standard_id"));                //施設基準ID
            $data[$i]["person_charge_id"] = trim(pg_fetch_result($sel, $i, "person_charge_id"));  //シフト管理者ID（責任者）
            $data[$i]["caretaker_id"] = trim(pg_fetch_result($sel, $i, "caretaker_id"));          //シフト管理者ID（代行者）
            $data[$i]["caretaker2_id"] = trim(pg_fetch_result($sel, $i, "caretaker2_id"));
            $data[$i]["caretaker3_id"] = trim(pg_fetch_result($sel, $i, "caretaker3_id"));
            $data[$i]["caretaker4_id"] = trim(pg_fetch_result($sel, $i, "caretaker4_id"));
            $data[$i]["caretaker5_id"] = trim(pg_fetch_result($sel, $i, "caretaker5_id"));
            $data[$i]["caretaker6_id"] = trim(pg_fetch_result($sel, $i, "caretaker6_id"));
            $data[$i]["caretaker7_id"] = trim(pg_fetch_result($sel, $i, "caretaker7_id"));
            $data[$i]["caretaker8_id"] = trim(pg_fetch_result($sel, $i, "caretaker8_id"));
            $data[$i]["caretaker9_id"] = trim(pg_fetch_result($sel, $i, "caretaker9_id"));
            $data[$i]["caretaker10_id"] = trim(pg_fetch_result($sel, $i, "caretaker10_id"));
            $data[$i]["pattern_id"] = trim(pg_fetch_result($sel, $i, "pattern_id"));              //勤務パターンID
            $data[$i]["close_day"] = trim(pg_fetch_result($sel, $i, "close_day"));                    //希望勤務締切日
            $data[$i]["duty_upper_limit_day"] = trim(pg_fetch_result($sel, $i, "duty_upper_limit_day"));  //連続勤務日数上限
            $data[$i]["order_no"] = trim(pg_fetch_result($sel, $i, "order_no"));  //表示順

            $data[$i]["person_charge_name"] = trim(pg_fetch_result($sel, $i, "person_charge_name"));  //責任者名設定
            $data[$i]["caretaker_name"] = trim(pg_fetch_result($sel, $i, "caretaker_name"));          //代行者名設定
            $data[$i]["caretaker2_name"] = trim(pg_fetch_result($sel, $i, "caretaker2_name"));
            $data[$i]["caretaker3_name"] = trim(pg_fetch_result($sel, $i, "caretaker3_name"));
            $data[$i]["caretaker4_name"] = trim(pg_fetch_result($sel, $i, "caretaker4_name"));
            $data[$i]["caretaker5_name"] = trim(pg_fetch_result($sel, $i, "caretaker5_name"));
            $data[$i]["caretaker6_name"] = trim(pg_fetch_result($sel, $i, "caretaker6_name"));
            $data[$i]["caretaker7_name"] = trim(pg_fetch_result($sel, $i, "caretaker7_name"));
            $data[$i]["caretaker8_name"] = trim(pg_fetch_result($sel, $i, "caretaker8_name"));
            $data[$i]["caretaker9_name"] = trim(pg_fetch_result($sel, $i, "caretaker9_name"));
            $data[$i]["caretaker10_name"] = trim(pg_fetch_result($sel, $i, "caretaker10_name"));
            //勤務パターンDBより勤務パターングループ名設定
            for ($k = 0; $k < count($data_wktmgrp); $k++) {
                if ($data[$i]["pattern_id"] == $data_wktmgrp[$k]["id"]) {
                    $data[$i]["pattern_name"] = $data_wktmgrp[$k]["name"];
                    break;
                }
            }
            $data[$i]["start_month_flg1"] = pg_fetch_result($sel, $i, "start_month_flg1");
            $data[$i]["start_day1"] = pg_fetch_result($sel, $i, "start_day1");
            $data[$i]["month_flg1"] = pg_fetch_result($sel, $i, "month_flg1");
            $data[$i]["end_day1"] = pg_fetch_result($sel, $i, "end_day1");
            $data[$i]["start_day2"] = pg_fetch_result($sel, $i, "start_day2");
            $data[$i]["month_flg2"] = pg_fetch_result($sel, $i, "month_flg2");
            $data[$i]["end_day2"] = pg_fetch_result($sel, $i, "end_day2");
            $data[$i]["four_week_chk_flg"] = pg_fetch_result($sel, $i, "four_week_chk_flg");
            $data[$i]["chk_start_date"] = pg_fetch_result($sel, $i, "chk_start_date");
            $data[$i]["print_title"] = pg_fetch_result($sel, $i, "print_title");
            $data[$i]["need_less_chk_flg"] = pg_fetch_result($sel, $i, "need_less_chk_flg");
            $data[$i]["need_more_chk_flg"] = pg_fetch_result($sel, $i, "need_more_chk_flg");
            $data[$i]["reason_setting_flg"] = pg_fetch_result($sel, $i, "reason_setting_flg");
            //SFC連携用病棟コード追加 20100709
            $data[$i]["sfc_group_code"] = pg_fetch_result($sel, $i, "sfc_group_code");
            //画面レイアウト設定
            $data[$i]["team_disp_flg"] = pg_fetch_result($sel, $i, "team_disp_flg");
            $data[$i]["job_disp_flg"] = pg_fetch_result($sel, $i, "job_disp_flg");
            $data[$i]["st_disp_flg"] = pg_fetch_result($sel, $i, "st_disp_flg");
            $data[$i]["es_disp_flg"] = pg_fetch_result($sel, $i, "es_disp_flg"); //常勤・非常勤の表示フラグ 20130129
            //勤務管理連動フラグ 20120305
            $data[$i]["timecard_auto_flg"] = pg_fetch_result($sel, $i, "timecard_auto_flg");
            $data[$i]["paid_hol_disp_flg"] = pg_fetch_result($sel, $i, "paid_hol_disp_flg");    //有給休暇残日数表示フラグ
            $data[$i]["display"] = unserialize(pg_fetch_result($sel, $i, "display"));
            $data[$i]["hosp_patient_cnt"] = pg_fetch_result($sel, $i, "hosp_patient_cnt");      //１日平均入院患者数(病棟別)
            //画面表示 20131107
            $data[$i]["unit_disp_flg"] = pg_fetch_result($sel, $i, "unit_disp_flg"); //ユニット表示
            $data[$i]["transfer_holiday_days_flg"] = pg_fetch_result($sel, $i, "transfer_holiday_days_flg"); //繰越公休残日数表示
            $data[$i]["approval_column_print_flg"] = pg_fetch_result($sel, $i, "approval_column_print_flg"); //決裁欄の印刷
            $data[$i]["disp_tosYotei_flg"] = pg_fetch_result($sel, $i, "disp_tosYotei_flg"); //当初予定
            //チェック機能フラグ 20131107
            $data[$i]["seven_work_chk_flg"] = pg_fetch_result($sel, $i, "seven_work_chk_flg"); //7日連続勤務チェック
            $data[$i]["part_week_chk_flg"] = pg_fetch_result($sel, $i, "part_week_chk_flg"); //非常勤の週間勤務時間数チェック
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループ情報（組み合わせ指定）取得
    // @param   $group_id   勤務シフトグループＩＤ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_seq_pattern_array($group_id) {
        //-------------------------------------------------------------------
        //勤務シフトグループ情報ＤＢ（組み合わせ指定）より情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_group_seq_pattern";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = '$group_id' ";
        }
        $cond .= "order by group_id, no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");                                //グループID
            $data[$i]["no"] = pg_fetch_result($sel, $i, "no");                                            //表示順
            $data[$i]["today_atdptn_ptn_id"] = pg_fetch_result($sel, $i, "today_atdptn_ptn_id");      //当日出勤パターンＩＤ
            $data[$i]["nextday_atdptn_ptn_id"] = pg_fetch_result($sel, $i, "nextday_atdptn_ptn_id");  //翌日出勤パターンＩＤ
            $data[$i]["today_reason"] = pg_fetch_result($sel, $i, "today_reason");        //当日事由
            $data[$i]["nextday_reason"] = pg_fetch_result($sel, $i, "nextday_reason");    //翌日事由
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループ情報（禁止組み合わせ）取得
    // @param   $group_id   勤務シフトグループＩＤ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_NG_pattern_array($group_id) {
        //-------------------------------------------------------------------
        //勤務シフトグループ情報ＤＢ（禁止組み合わせ）より情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_group_ng_pattern";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = '$group_id' ";
        }
        $cond .= "order by group_id, no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");                                //グループID
            $data[$i]["no"] = pg_fetch_result($sel, $i, "no");                                            //表示順
            $data[$i]["today_atdptn_ptn_id"] = pg_fetch_result($sel, $i, "today_atdptn_ptn_id");      //当日禁止出勤パターンＩＤ
            $data[$i]["nextday_atdptn_ptn_id"] = pg_fetch_result($sel, $i, "nextday_atdptn_ptn_id");  //翌日禁止出勤パターンＩＤ
            $wk = pg_fetch_result($sel, $i, "today_reason");
            $data[$i]["today_reason"] = ($wk == "0") ? "" : $wk;    //当日事由
            $wk = pg_fetch_result($sel, $i, "nextday_reason");
            $data[$i]["nextday_reason"] = ($wk == "0") ? "" : $wk;  //翌日事由
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループ情報（連続勤務シフト上限）取得
    // @param   $group_id   勤務シフトグループＩＤ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_upper_limit_array($group_id) {
        //-------------------------------------------------------------------
        //勤務シフトグループ情報ＤＢ（連続勤務シフト上限）より情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_group_upper_limit";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = '$group_id' ";
        }
        $cond .= "order by group_id, no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");                    //グループID
            $data[$i]["no"] = pg_fetch_result($sel, $i, "no");                                //表示順
            $data[$i]["limit_atdptn_ptn_id"] = pg_fetch_result($sel, $i, "atdptn_ptn_id");    //出勤パターンＩＤ
            $data[$i]["upper_limit_day"] = pg_fetch_result($sel, $i, "upper_limit_day");  //上限日
            $wk = pg_fetch_result($sel, $i, "reason");
            $data[$i]["reason"] = ($wk == "0") ? "" : $wk;  //事由
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループ情報（勤務シフト間隔）取得
    // @param   $group_id   勤務シフトグループＩＤ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_interval_array($group_id) {
        //-------------------------------------------------------------------
        //勤務シフトグループ情報ＤＢ（勤務シフト間隔）より情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_group_interval";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = '$group_id' ";
        }
        $cond .= "order by group_id, no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");                    //グループID
            $data[$i]["no"] = pg_fetch_result($sel, $i, "no");                                //表示順
            $data[$i]["interval_atdptn_ptn_id"] = pg_fetch_result($sel, $i, "atdptn_ptn_id"); //出勤パターンＩＤ
            $data[$i]["interval_day"] = pg_fetch_result($sel, $i, "interval_day");    //上限日
            $wk = pg_fetch_result($sel, $i, "reason");
            $data[$i]["reason"] = ($wk == "0") ? "" : $wk;  //事由
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループ情報（４週８休シフト種類）取得
    // @param   $group_id   勤務シフトグループＩＤ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_hol_pattern_array($group_id) {
        //-------------------------------------------------------------------
        //勤務シフトグループ情報ＤＢ（４週８休シフト種類）より情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_group_hol_pattern";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = '$group_id' ";
        }
        $cond .= "order by group_id, no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id"); //グループID
            $data[$i]["no"] = pg_fetch_result($sel, $i, "no");            //表示順
            $data[$i]["ptn_id"] = pg_fetch_result($sel, $i, "ptn_id");    //パターンＩＤ
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループ情報（決裁欄役職）取得
    // @param   $group_id   勤務シフトグループＩＤ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_st_name_array($group_id) {
        //-------------------------------------------------------------------
        //勤務シフトグループ情報ＤＢ（決裁欄役職）より情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_group_st_name";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = '$group_id' ";
        }
        $cond .= "order by group_id, no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id"); //グループID
            $data[$i]["no"] = pg_fetch_result($sel, $i, "no");                //表示順
            $data[$i]["st_name"] = pg_fetch_result($sel, $i, "st_name");  //役職名
        }
        //20120426追加 start
        //グループ指定でデータがない場合にはデフォルト値を返す
        if ($group_id != "" && $num == 0) {
            $data = $this->get_duty_shift_group_st_name_default();
        }
        //20120426追加 end

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトスタッフチーム情報取得
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_staff_team_array() {
        //-------------------------------------------------------------------
        //勤務シフトスタッフチームＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "SELECT * FROM duty_shift_staff_team";
        $cond = "ORDER BY no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        while ($row = pg_fetch_array($sel)) {
            $data[$row["team_id"]]["team_id"] = $row["team_id"];    //チームID
            $data[$row["team_id"]]["no"] = $row["no"];         //表示順
            $data[$row["team_id"]]["team_name"] = $row["team_name"];  //チーム名
        }

        return $data;
    }

    //20120426追加 start
    /*     * ********************************************************************** */
    // 勤務シフトグループ情報（決裁欄役職デフォルト値）取得
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_group_st_name_default() {
        return array(array("st_name" => "院長"), array("st_name" => "看護部長"), array("st_name" => "師長"));
    }

    //20120426追加 end

    /*     * ********************************************************************** */
    // 勤務シフトスタッフ情報取得
    // @param   $group_id   勤務シフトグループＩＤ
    // @param   $data_st    ＤＢ(stmst)より役職情報取得 (必要なくなった)
    // @param   $data_job   ＤＢ(jobmst)より職種情報取得 (必要なくなった)
    // @param   $data_emp   ＤＢ(empmst)より職員情報取得 (必要なくなった)
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_staff_array($group_id, $data_st, $data_job, $data_emp) {
        //-------------------------------------------------------------------
        //スタッフ情報ＤＢより情報取得
        //-------------------------------------------------------------------
        if ($group_id == "") {
            $sql = <<<SQL_END
SELECT s.*,
s.emp_id as id,
g.standard_id,
t.team_name,
emp.emp_lt_nm || ' ' || emp.emp_ft_nm as name,
emp.emp_sex as sex,
job.job_id as job_id,
job.job_nm as job_name,
st.st_nm as status,
dp.dept_nm as dp_name,
rm.room_nm as rm_name,
empl.night_duty_flg,
case
    when emp_join is null then '入職日未定'
    when emp_join = '' then '入職日未定'
    when to_date(emp_join,'YYYYMMDD') >= CURRENT_DATE then '0年0ヶ月'
    when extract(month from CURRENT_DATE) < extract(month from to_date(emp_join,'YYYYMMDD')) then
        extract(year from CURRENT_DATE) - extract(year from to_date(emp_join,'YYYYMMDD')) -1 || '年' ||
        extract(month from CURRENT_DATE) - extract(month from to_date(emp_join,'YYYYMMDD')) +12 || 'ヶ月'
    else
        extract(year from CURRENT_DATE) - extract(year from to_date(emp_join,'YYYYMMDD')) || '年' ||
        extract(month from CURRENT_DATE) - extract(month from to_date(emp_join,'YYYYMMDD')) || 'ヶ月'
end as duty_years
FROM duty_shift_staff s
LEFT JOIN empmst emp USING (emp_id)
LEFT JOIN jobmst job on emp.emp_job=job.job_id
LEFT JOIN stmst st ON emp.emp_st=st.st_id
LEFT JOIN deptmst dp ON emp.emp_dept=dp.dept_id
LEFT JOIN classroom rm ON emp.emp_room=rm.room_id and emp.emp_dept=rm.dept_id
LEFT JOIN duty_shift_group g USING (group_id)
LEFT JOIN duty_shift_staff_team t USING (team_id)
LEFT JOIN duty_shift_staff_employment empl USING (emp_id)
SQL_END;

            $cond = "ORDER BY s.group_id, s.no";
        } else {
            $sql = <<<SQL_END
SELECT s.*,
s.emp_id as id,
t.team_name,
emp.emp_lt_nm || ' ' || emp.emp_ft_nm as name,
emp.emp_sex as sex,
job.job_id as job_id,
job.job_nm as job_name,
st.st_nm as status,
dp.dept_nm as dp_name,
rm.room_nm as rm_name,
empl.night_duty_flg,
case
    when emp_join is null then '入職日未定'
    when emp_join = '' then '入職日未定'
    when to_date(emp_join,'YYYYMMDD') >= CURRENT_DATE then '0年0ヶ月'
    when extract(month from CURRENT_DATE) < extract(month from to_date(emp_join,'YYYYMMDD')) then
        extract(year from CURRENT_DATE) - extract(year from to_date(emp_join,'YYYYMMDD')) -1 || '年' ||
        extract(month from CURRENT_DATE) - extract(month from to_date(emp_join,'YYYYMMDD')) +12 || 'ヶ月'
    else
        extract(year from CURRENT_DATE) - extract(year from to_date(emp_join,'YYYYMMDD')) || '年' ||
        extract(month from CURRENT_DATE) - extract(month from to_date(emp_join,'YYYYMMDD')) || 'ヶ月'
end as duty_years
,emp.emp_personal_id
FROM duty_shift_staff s
LEFT JOIN empmst emp USING (emp_id)
LEFT JOIN jobmst job on emp.emp_job=job.job_id
LEFT JOIN stmst st ON emp.emp_st=st.st_id
LEFT JOIN deptmst dp ON emp.emp_dept=dp.dept_id
LEFT JOIN classroom rm ON emp.emp_room=rm.room_id and emp.emp_dept=rm.dept_id
LEFT JOIN duty_shift_staff_team t USING (team_id)
LEFT JOIN duty_shift_staff_employment empl USING (emp_id)
SQL_END;

            $cond = "WHERE s.group_id = '$group_id' ";
            $cond .= "ORDER BY s.no";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトスタッフ情報取得（職員ＩＤ指定）
    // @param   $emp_id     職員ＩＤ
    // @param   $data_st    役職情報取得
    // @param   $data_job   職種情報取得
    // @param   $data_emp   職員情報取得
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_staff_one($emp_id, $data_st, $data_job, $data_emp) {
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        if ($this->data_assoc_flg) {
            $k = $this->data_emp_idx[$emp_id];
            $data["name"] = $data_emp[$k]["name"];
            $data["sex"] = $data_emp[$k]["sex"];
            $data["job_id"] = $data_emp[$k]["job_id"];
            $data["job_name"] = $this->data_job_assoc[$data["job_id"]];
            $data["status"] = $this->data_st_assoc[$data_emp[$k]["st_id"]];
            $data["duty_years"] = $this->get_number_of_years($data_emp[$k]["join"]);
        }
        //連想配列への設定がない場合は、以前通りの動作
        else {
            //職種DBより名称設定
            for ($k = 0; $k < count($data_emp); $k++) {
                if ($emp_id == $data_emp[$k]["id"]) {
                    $data["name"] = $data_emp[$k]["name"];
                    $data["sex"] = $data_emp[$k]["sex"];

                    //職種DBより名称設定
                    for ($m = 0; $m < count($data_job); $m++) {
                        if ($data_emp[$k]["job_id"] == $data_job[$m]["id"]) {
                            $data["job_id"] = $data_job[$m]["id"];
                            $data["job_name"] = $data_job[$m]["name"];
                            break;
                        }
                    }

                    //役職DBより名称設定
                    for ($m = 0; $m < count($data_st); $m++) {
                        if ($data_emp[$k]["st_id"] == $data_st[$m]["id"]) {
                            $data["status"] = $data_st[$m]["name"];
                            break;
                        }
                    }

                    //勤務年数算出
                    $data["duty_years"] = $this->get_number_of_years($data_emp[$k]["join"]);
                    break;
                }
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトスタッフ情報取得（職員ＩＤ指定:SELECTで取得）
    // @param   $emp_id     職員ＩＤ
    // @param   $group_id   グループID
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_staff_one_select($emp_id, $group_id) {
        // SELECT SQL
        $sql = <<<SQL_END
SELECT s.*,
emp.emp_id as id,
t.team_name,
emp.emp_lt_nm || ' ' || emp.emp_ft_nm as name,
emp.emp_sex as sex,
job.job_id as job_id,
job.job_nm as job_name,
st.st_nm as status,
dp.dept_nm as dp_name,
case
when emp_join is null then '入職日未定'
when emp_join = '' then '入職日未定'
when to_date(emp_join,'YYYYMMDD') >= CURRENT_DATE then '0年0ヶ月'
when extract(month from CURRENT_DATE) < extract(month from to_date(emp_join,'YYYYMMDD')) then
    extract(year from CURRENT_DATE) - extract(year from to_date(emp_join,'YYYYMMDD')) -1 || '年' ||
    extract(month from CURRENT_DATE) - extract(month from to_date(emp_join,'YYYYMMDD')) +12 || 'ヶ月'
else
    extract(year from CURRENT_DATE) - extract(year from to_date(emp_join,'YYYYMMDD')) || '年' ||
    extract(month from CURRENT_DATE) - extract(month from to_date(emp_join,'YYYYMMDD')) || 'ヶ月'
end as duty_years
,emp.emp_personal_id
FROM empmst emp
LEFT JOIN duty_shift_staff s ON emp.emp_id=s.emp_id AND s.group_id='$group_id'
LEFT JOIN jobmst job ON emp.emp_job=job.job_id
LEFT JOIN stmst st ON emp.emp_st=st.st_id
LEFT JOIN deptmst dp ON emp.emp_dept=dp.dept_id
LEFT JOIN duty_shift_staff_team t USING (team_id)
SQL_END;

        $cond = "WHERE emp.emp_id='$emp_id' ";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = pg_fetch_array($sel);
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトスタッフ情報取得（個別勤務条件）
    // @param   $emp_id         職員ＩＤ
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_staff_employmen($emp_id) {
        $data = array();
        //-------------------------------------------------------------------
        //ＤＢ(empcond)より職員情報（勤務条件）取得
        //-------------------------------------------------------------------
        $sql = "select * from empcond";
        $cond = "where emp_id = '$emp_id' ";
        $cond .= "order by emp_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        if ($num > 0) {
            if (pg_fetch_result($sel, 0, "duty_form") != "") {
                $data["wage"] = pg_fetch_result($sel, 0, "wage");                     //給与支給区分
                $data["tmcd_group_id"] = pg_fetch_result($sel, 0, "tmcd_group_id");       //出勤パターンＩＤ
                $data["duty_form"] = pg_fetch_result($sel, 0, "duty_form");               //勤務形態（常勤／非常勤）
            }
            $data["other_post_flg"] = pg_fetch_result($sel, 0, "other_post_flg");         //他部署兼務） 20120418
        }
        //-------------------------------------------------------------------
        //スタッフ情報（個人勤務条件）ＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "select ";
        $sql .= "a.group_id, ";
        $sql .= "a.other_post_flg, ";
        $sql .= "a.duty_mon_day_flg, ";
        $sql .= "a.duty_tue_day_flg, ";
        $sql .= "a.duty_wed_day_flg, ";
        $sql .= "a.duty_thurs_day_flg, ";
        $sql .= "a.duty_fri_day_flg, ";
        $sql .= "a.duty_sat_day_flg, ";
        $sql .= "a.duty_sun_day_flg, ";
        $sql .= "a.duty_hol_day_flg, ";
        $sql .= "a.night_duty_flg, ";
        $sql .= "a.night_staff_cnt, ";
        $sql .= "b.pattern_id ";
        $sql .= "from duty_shift_staff_employment as a left join duty_shift_group as b ";
        $sql .= "on a.group_id = b.group_id ";
        $cond = "where a.emp_id = '$emp_id' ";
        $cond .= "order by a.emp_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        if ($num > 0) {
            $data["group_id"] = pg_fetch_result($sel, 0, "group_id");

            //$data["other_post_flg"] = pg_fetch_result($sel,0,"other_post_flg");           //他部署兼務 勤務条件へ移動 20120418

            $data["duty_mon_day_flg"] = pg_fetch_result($sel, 0, "duty_mon_day_flg");     //勤務日（月曜日）（１：有り）
            $data["duty_tue_day_flg"] = pg_fetch_result($sel, 0, "duty_tue_day_flg");     //勤務日（火曜日）（１：有り）
            $data["duty_wed_day_flg"] = pg_fetch_result($sel, 0, "duty_wed_day_flg");     //勤務日（水曜日）（１：有り）
            $data["duty_thurs_day_flg"] = pg_fetch_result($sel, 0, "duty_thurs_day_flg"); //勤務日（木曜日）（１：有り）
            $data["duty_fri_day_flg"] = pg_fetch_result($sel, 0, "duty_fri_day_flg");     //勤務日（金曜日）（１：有り）
            $data["duty_sat_day_flg"] = pg_fetch_result($sel, 0, "duty_sat_day_flg");     //勤務日（土曜日）（１：有り）
            $data["duty_sun_day_flg"] = pg_fetch_result($sel, 0, "duty_sun_day_flg");     //勤務日（日曜日）（１：有り）
            $data["duty_hol_day_flg"] = pg_fetch_result($sel, 0, "duty_hol_day_flg");     //勤務日（祝日）（１：有り）

            $data["night_duty_flg"] = pg_fetch_result($sel, 0, "night_duty_flg");     //夜勤の有無
            $data["night_staff_cnt"] = pg_fetch_result($sel, 0, "night_staff_cnt");       //夜勤従事者

            $data["ward_pattern_id"] = pg_fetch_result($sel, 0, "pattern_id");            //所属病棟のグループＩＤ
        } else {
            //デフォルトを有りにする。
            $data["duty_mon_day_flg"] = "1";        //勤務日（月曜日）（１：有り）
            $data["duty_tue_day_flg"] = "1";        //勤務日（火曜日）（１：有り）
            $data["duty_wed_day_flg"] = "1";        //勤務日（水曜日）（１：有り）
            $data["duty_thurs_day_flg"] = "1";      //勤務日（木曜日）（１：有り）
            $data["duty_fri_day_flg"] = "1";        //勤務日（金曜日）（１：有り）
            $data["duty_sat_day_flg"] = "1";        //勤務日（土曜日）（１：有り）
            $data["duty_sun_day_flg"] = "1";        //勤務日（日曜日）（１：有り）
            $data["duty_hol_day_flg"] = "1";        //勤務日（祝日）（１：有り）
        }
        //-------------------------------------------------------------------
        //スタッフ情報（個人勤務条件＿出勤パターン）ＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_staff_employment_atdptn";
        $cond = "where emp_id = '$emp_id' ";
        $cond .= "order by emp_id, week_flg, atdptn_ptn_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        for ($i = 0; $i < $num; $i++) {
            $wee_flg = pg_fetch_result($sel, $i, "week_flg");
            $k = pg_fetch_result($sel, $i, "atdptn_ptn_id");
            if ($wee_flg == "1") {
                $data["week_flg_$k"] = pg_fetch_result($sel, $i, "use_flg");
            }       //月〜金（１：有り）
            if ($wee_flg == "2") {
                $data["sat_flg_$k"] = pg_fetch_result($sel, $i, "use_flg");
            }       //土（１：有り）
            if ($wee_flg == "3") {
                $data["sun_flg_$k"] = pg_fetch_result($sel, $i, "use_flg");
            }       //日（１：有り）
            if ($wee_flg == "4") {
                $data["holiday_flg_$k"] = pg_fetch_result($sel, $i, "use_flg");
            }   //祝日（１：有り）
        }
        if ($num <= 0) {
            for ($k = 0; $k <= 99; $k++) {
                $data["week_flg_$k"] = "1";             //月曜〜金曜（１：有り）
                $data["sat_flg_$k"] = "1";              //土曜（１：有り）
                $data["sun_flg_$k"] = "1";              //日曜（１：有り）
                $data["holiday_flg_$k"] = "1";          //祝日曜（１：有り）
            }
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトスタッフ情報取得（個別勤務条件を勤務シフトグループ単位で取得）
    // @param   $group_id       勤務シフトグループ
    // @param   $staff_array    スタッフ情報（指定シフトグループの）
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_staff_employmen_2($group_id, $staff_array) {
        //-------------------------------------------------------------------
        //初期処理
        //-------------------------------------------------------------------
        $idx_array = array();
        $data = $staff_array;
        for ($i = 0; $i < count($data); $i++) {
            $data[$i]["staff_id"] = $data[$i]["id"];
            $idx_array[$data[$i]["id"]] = $i;
        }
        //-------------------------------------------------------------------
        //スタッフ情報（個人勤務条件）ＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_staff_employment";
        $cond = "where group_id = '$group_id' ";
        $cond .= "order by group_id, emp_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        while ($row = pg_fetch_array($sel)) {
            $idx = $idx_array[$row["emp_id"]];
            if (empty($idx)) {
                continue;
            }
            $data[$idx]["other_post_flg"] = $row["other_post_flg"];         //他部署兼務

            $data[$idx]["duty_mon_day_flg"] = $row["duty_mon_day_flg"];   //勤務日（月曜日）（１：有り）
            $data[$idx]["duty_tue_day_flg"] = $row["duty_tue_day_flg"];   //勤務日（火曜日）（１：有り）
            $data[$idx]["duty_wed_day_flg"] = $row["duty_wed_day_flg"];   //勤務日（水曜日）（１：有り）
            $data[$idx]["duty_thurs_day_flg"] = $row["duty_thurs_day_flg"]; //勤務日（木曜日）（１：有り）
            $data[$idx]["duty_fri_day_flg"] = $row["duty_fri_day_flg"];   //勤務日（金曜日）（１：有り）
            $data[$idx]["duty_sat_day_flg"] = $row["duty_sat_day_flg"];   //勤務日（土曜日）（１：有り）
            $data[$idx]["duty_sun_day_flg"] = $row["duty_sun_day_flg"];   //勤務日（日曜日）（１：有り）
            $data[$idx]["duty_hol_day_flg"] = $row["duty_hol_day_flg"];   //勤務日（祝日）（１：有り）

            $data[$idx]["night_duty_flg"] = $row["night_duty_flg"];     //夜勤の有無
            $data[$idx]["night_staff_cnt"] = $row["night_staff_cnt"];    //夜勤従事者
        }
        //-------------------------------------------------------------------
        //スタッフ情報（個人勤務条件＿出勤パターン）ＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_staff_employment_atdptn";
        $cond = "where group_id = '$group_id' ";
        $cond .= "order by group_id, emp_id, week_flg, atdptn_ptn_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        for ($i = 0; $i < count($data); $i++) {
            for ($m = 0; $m <= 99; $m++) {
                $data[$i]["week_flg_$m"] = "1";             //月曜〜金曜（１：有り）
                $data[$i]["sat_flg_$m"] = "1";              //土曜（１：有り）
                $data[$i]["sun_flg_$m"] = "1";              //日曜（１：有り）
                $data[$i]["holiday_flg_$m"] = "1";          //祝日曜（１：有り）
            }
        }
        for ($i = 0; $i < $num; $i++) {
            $key_staff_id = pg_fetch_result($sel, $i, "emp_id");
            for ($k = 0; $k < count($data); $k++) {
                if ($data[$k]["staff_id"] == $key_staff_id) {
                    $wee_flg = pg_fetch_result($sel, $i, "week_flg");
                    $m = pg_fetch_result($sel, $i, "atdptn_ptn_id");
                    if ($wee_flg == "1") {
                        $data[$k]["week_flg_$m"] = pg_fetch_result($sel, $i, "use_flg");
                    }       //月〜金（１：有り）
                    if ($wee_flg == "2") {
                        $data[$k]["sat_flg_$m"] = pg_fetch_result($sel, $i, "use_flg");
                    }       //土（１：有り）
                    if ($wee_flg == "3") {
                        $data[$k]["sun_flg_$m"] = pg_fetch_result($sel, $i, "use_flg");
                    }       //日（１：有り）
                    if ($wee_flg == "4") {
                        $data[$k]["holiday_flg_$m"] = pg_fetch_result($sel, $i, "use_flg");
                    }   //祝日（１：有り）
                    break;
                }
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフト記号情報取得
    // @param   $pattern_id         出勤グループＩＤ,"array"という文字列の場合、第3引数の配列を条件に使用する
    // @param   $data_atdptn        出勤パターン情報
    // @param   $arr_pattern_id     出勤グループＩＤの配列 20140220
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_pattern_array($pattern_id, $data_atdptn, $arr_pattern_id) {

        // 休暇事由の件数、追加がある場合は変更すること
        $holiday_reason_cnt = 46;
        // 20140128 start
        //一覧の場合、記号件数がある場合にデータ取得する
        if ($this->get_pattern_cnt_check_flg()) {
            $tmgrpchk_flg = true;
            $arr_duty_shift_pattern_cnt = $this->get_arr_duty_shift_pattern_cnt();
        } else {
            $tmgrpchk_flg = false;
            $arr_duty_shift_pattern_cnt = array();
        }
        // 20140128 end
        //事由名配列
        $arr_reason = array();
        $arr_reason_name["1"] = "有給";
        $arr_reason_name["4"] = "代替";
        $arr_reason_name["5"] = "特別";
        $arr_reason_name["6"] = "欠勤";
        $arr_reason_name["7"] = "病欠";
        $arr_reason_name["8"] = "その他";
        $arr_reason_name["17"] = "振替";
        $arr_reason_name["22"] = "法定";
        $arr_reason_name["23"] = "所定";
        $arr_reason_name["24"] = "公休";
        $arr_reason_name["25"] = "産休";
        $arr_reason_name["26"] = "育児";
        $arr_reason_name["27"] = "介護";
        $arr_reason_name["28"] = "傷病";
        $arr_reason_name["29"] = "学業";
        $arr_reason_name["30"] = "忌引";
        $arr_reason_name["31"] = "夏休";
        $arr_reason_name["32"] = "結婚";
        $arr_reason_name["37"] = "年休";
        $arr_reason_name["40"] = "リフレ";
        $arr_reason_name["41"] = "初盆";
        $arr_reason_name["44"] = "半有半公";
        $arr_reason_name["45"] = "希望(公休)";
        $arr_reason_name["46"] = "待機(公休)";
        $arr_reason_name["47"] = "管理当直前(公休)";
        $arr_reason_name["54"] = "半夏半公";
        $arr_reason_name["55"] = "半夏半有";
        $arr_reason_name["56"] = "半夏半欠";
        $arr_reason_name["57"] = "半有半欠";
        $arr_reason_name["58"] = "半特半有";
        $arr_reason_name["59"] = "半特半公";
        $arr_reason_name["60"] = "半特半欠";
        $arr_reason_name["61"] = "年末年始";
        $arr_reason_name["62"] = "半正半有";
        $arr_reason_name["63"] = "半正半公";
        $arr_reason_name["64"] = "半正半欠";
        $arr_reason_name["72"] = "半夏半特";
        //公外出等の名称取得 20141031
        $sql = "select reason_id, default_name, display_name from atdbk_reason_mst";
        $cond = "where reason_id in ('33', '9', '10', '11', '12', '13', '14', '15') ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);        //取得
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            $wk_id = $row["reason_id"];
            $arr_reason_name[$wk_id] = ($row["display_name"]) ? $row["display_name"] : $row["default_name"];
        }
        // 処理速度対応 20140130
        $reason_display_set = $this->get_reason_display_setting();
        $arr_pattern_id_first_chk = array();
        //-------------------------------------------------------------------
        //出勤グループＩＤ数分の情報取得
        //-------------------------------------------------------------------
        $data_all = array();
        for ($p = 0, $p_cnt = count($data_atdptn); $p < $p_cnt; $p++) {
            $wk_pattern_id = $data_atdptn[$p]["group_id"];
            // 20140128 start
            if ($tmgrpchk_flg) {
                if ($arr_duty_shift_pattern_cnt["$wk_pattern_id"] == "") {
                    continue;
                }
            }
            // 20140128 end
            //-------------------------------------------------------------------
            //対象出勤グループＩＤの場合のみ設定
            //-------------------------------------------------------------------
            if (($pattern_id == $wk_pattern_id) || ($pattern_id == "") || ($pattern_id == "array" && in_array($wk_pattern_id, $arr_pattern_id)) //20140220 応援追加の対象分のID
            ) {
                if ($arr_pattern_id_first_chk[$wk_pattern_id] == "") { //20111012 出勤パターングループごとに初回かチェック
                    $arr_pattern_id_first_chk[$wk_pattern_id] = "1";

                    //-------------------------------------------------------------------
                    //勤務シフト記号情報ＤＢより情報取得
                    //-------------------------------------------------------------------
                    $sql = "select *, ";
                    $sql .= "case reason ";
                    $sql .= "when '34' then 901 ";
                    $sql .= "when '24' then 902 ";
                    $sql .= "when '22' then 903 ";
                    $sql .= "when '23' then 904 ";
                    $sql .= "when '1'  then 905 ";
                    $sql .= "when '37' then 906 ";
                    $sql .= "when '44' then 907 ";
                    $sql .= "when '45' then 908 ";
                    $sql .= "when '46' then 909 ";
                    $sql .= "when '47' then 910 ";
                    $sql .= "when '4'  then 911 ";
                    $sql .= "when '17' then 912 ";
                    $sql .= "when '5'  then 913 ";
                    $sql .= "when '6'  then 914 ";
                    $sql .= "when '7'  then 915 ";
                    $sql .= "when '40' then 916 ";
                    $sql .= "when '41' then 917 ";
                    $sql .= "when '25' then 918 ";
                    $sql .= "when '26' then 919 ";
                    $sql .= "when '27' then 920 ";
                    $sql .= "when '28' then 921 ";
                    $sql .= "when '29' then 922 ";
                    $sql .= "when '30' then 923 ";
                    $sql .= "when '31' then 924 ";
                    $sql .= "when '61' then 925 ";
                    $sql .= "when '32' then 926 ";
                    $sql .= "when '57' then 927 ";
                    $sql .= "when '58' then 928 ";
                    $sql .= "when '59' then 929 ";
                    $sql .= "when '60' then 930 ";
                    $sql .= "when '54' then 931 ";
                    $sql .= "when '55' then 932 ";
                    $sql .= "when '56' then 933 ";
                    $sql .= "when '72' then 934 ";
                    $sql .= "when '62' then 935 ";
                    $sql .= "when '63' then 936 ";
                    $sql .= "when '64' then 937 ";
                    $sql .= "when '14' then 938 "; //追加 20141030
                    $sql .= "when '15' then 939 "; //追加 20141030
                    $sql .= "when '33' then 940 "; //追加 20141030
                    $sql .= "when '9' then 941 "; //追加 20141030
                    $sql .= "when '10' then 942 "; //追加 20141030
                    $sql .= "when '11' then 943 "; //追加 20141030
                    $sql .= "when '12' then 944 "; //追加 20141030
                    $sql .= "when '13' then 945 "; //追加 20141030
                    $sql .= "when '8' then 946 ";
                    $sql .= "else atdptn_ptn_id end as orderno1 ";
                    $sql .= "from duty_shift_pattern ";
                    $cond = "where pattern_id = $wk_pattern_id ";
                    $cond .= "order by order_no, orderno1 ";

                    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                    if ($sel == 0) {
                        pg_close($this->_db_con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                    $num = pg_num_rows($sel);
                    //-------------------------------------------------------------------
                    //情報設定
                    //-------------------------------------------------------------------
                    $data = array();
                    for ($i = 0; $i < $num; $i++) {
                        //-------------------------------------------------------------------
                        //ＤＢよりの取得値
                        //-------------------------------------------------------------------
                        $data[$i]["pattern_id"] = trim(pg_fetch_result($sel, $i, "pattern_id"));          //出勤グループＩＤ
                        $data[$i]["atdptn_ptn_id"] = trim(pg_fetch_result($sel, $i, "atdptn_ptn_id"));        //出勤パターンＩＤ
                        $data[$i]["reason"] = trim(pg_fetch_result($sel, $i, "reason"));                  //事由
                        $data[$i]["count_kbn_gyo"] = trim(pg_fetch_result($sel, $i, "count_kbn_gyo"));        //集計区分(行)
                        $data[$i]["count_kbn_retu"] = trim(pg_fetch_result($sel, $i, "count_kbn_retu"));  //集計区分(列)
                        $data[$i]["font_name"] = trim(pg_fetch_result($sel, $i, "font_name"));                //表示文字
                        $data[$i]["font_color_id"] = trim(pg_fetch_result($sel, $i, "font_color_id"));        //文字色
                        $data[$i]["back_color_id"] = trim(pg_fetch_result($sel, $i, "back_color_id"));        //背景色
                        $data[$i]["order_no"] = trim(pg_fetch_result($sel, $i, "order_no"));      //表示順
                        $data[$i]["allotment_order_no"] = trim(pg_fetch_result($sel, $i, "allotment_order_no"));      //分担表 20100712 追加
                        $data[$i]["sfc_code"] = trim(pg_fetch_result($sel, $i, "sfc_code"));      //連携コード 20100712 追加
                        $data[$i]["stamp_disp_flg"] = trim(pg_fetch_result($sel, $i, "stamp_disp_flg"));      //スタンプ非表示 20120305 追加
                        $data[$i]["output_kbn"] = trim(pg_fetch_result($sel, $i, "output_kbn"));      //看護日誌出力区分 20150716 追加
                        //-------------------------------------------------------------------
                        //各名称設定
                        //-------------------------------------------------------------------
                        for ($k = 0, $k_cnt = count($data_atdptn); $k < $k_cnt; $k++) {
                            //出勤パターン名
                            if (($data[$i]["pattern_id"] == $data_atdptn[$k]["group_id"]) &&
                                    ($data[$i]["atdptn_ptn_id"] == $data_atdptn[$k]["id"])) {
                                $data[$i]["atdptn_ptn_name"] = $data_atdptn[$k]["name"];
                                $data[$i]["display_flag"] = $data_atdptn[$k]["display_flag"]; //表示フラグ 20120830
                            }
                            //行用
                            if (($data[$i]["pattern_id"] == $data_atdptn[$k]["group_id"]) &&
                                    ($data[$i]["count_kbn_gyo"] == $data_atdptn[$k]["id"])) {
                                $data[$i]["count_kbn_gyo_name"] = $data_atdptn[$k]["name"];
                            }
                            //列用
                            if (($data[$i]["pattern_id"] == $data_atdptn[$k]["group_id"]) &&
                                    ($data[$i]["count_kbn_retu"] == $data_atdptn[$k]["id"])) {
                                $data[$i]["count_kbn_retu_name"] = $data_atdptn[$k]["name"];
                            }
                        }
                        //-------------------------------------------------------------------
                        //事由名称設定（出勤パターンが休暇の場合のみ）
                        //$ret["34"] = "";
                        //$ret["24"] = "公休";
                        //$ret["22"] = "法定";
                        //$ret["23"] = "所定";
                        //$ret["1"] = "有給休暇";
                        //$ret["37"] = "年休";
                        //$ret["4"] = "代替休暇";
                        //$ret["17"] = "振替休暇";
                        //$ret["5"] = "特別休暇";
                        //$ret["6"] = "欠勤（一般欠勤）";
                        //$ret["7"] = "病欠（病傷欠勤）";
                        //$ret["40"] = "リフレ（リフレッシュ休暇）";
                        //$ret["41"] = "初盆";
                        //$ret["8"] = "その他休";
                        //-------------------------------------------------------------------
                        $wk_idx = $data[$i]["reason"];
                        $data[$i]["reason_name"] = $arr_reason_name[$wk_idx];
                    }
                    //-------------------------------------------------------------------
                    //データ設定時
                    //-------------------------------------------------------------------
                    if ($num > 0) {
                        //-------------------------------------------------------------------
                        //休暇の事由の有無
                        //-------------------------------------------------------------------
                        $wk_flg = "";
                        // 休暇事由のデータ件数
                        $wk_reason_cnt = 0;
                        $wk_array = array();
                        $m = 0;
                        //休暇以外、休暇の順でなくそのまま設定 20150324
                        $wk_array = $data;
                        for ($i = 0, $i_cnt = count($data); $i < $i_cnt; $i++) {
                            //休暇を名称ではなくIDで確認
                            if ($data[$i]["atdptn_ptn_id"] == "10") {
                                $wk_atdptn_ptn_id = $data[$i]["atdptn_ptn_id"];     //出勤パターンＩＤ
                                $wk_font_name = "";             //表示文字
                                $wk_count_kbn_gyo = 9999;       //集計区分(行)
                                $wk_count_kbn_retu = 9999;      //集計区分(列)
                                $wk_font_color_id = "#000000";  //文字色
                                $wk_back_color_id = "#ffffff";  //背景色
                                if (trim($data[$i]["reason"]) != "") {
                                    $wk_flg = "1";
                                    $wk_reason_cnt++;
                                }
                                $wk_atdptn_ptn_name = $data[$i]["atdptn_ptn_name"];         //出勤パターン名
                                $wk_count_kbn_gyo_name = $data[$i]["count_kbn_gyo_name"];   //行名
                                $wk_count_kbn_retu_name = $data[$i]["count_kbn_retu_name"]; //列名
                                //} else {
                                //休暇以外のデータ設定
                                //$wk_array[$m] = $data[$i];
                                //$m++;
                            }
                        }
                        /*
                          // 既存の休暇データ設定
                          if ($wk_reason_cnt < $holiday_reason_cnt) {
                          for($i=0,$i_cnt = count($data); $i<$i_cnt; $i++) {
                          if ($data[$i]["atdptn_ptn_id"] == "10" && trim($data[$i]["reason"]) != "") {
                          $wk_array[$m] = $data[$i];
                          $m++;
                          }
                          }
                          }
                         */
                        //-------------------------------------------------------------------
                        //データが存在し、休暇に事由がない時、事由追加で休暇の再設定
                        //-------------------------------------------------------------------
                        // if ($wk_flg == "") {
                        // 登録済みの件数が少ない場合
                        if ($wk_reason_cnt < $holiday_reason_cnt) {
                            $data = $this->setReasonPatternArray2($wk_array, $wk_pattern_id, $wk_atdptn_ptn_id, $wk_atdptn_ptn_name, $wk_count_kbn_gyo, $wk_count_kbn_retu, $wk_count_kbn_gyo_name, $wk_count_kbn_retu_name, $wk_font_name, $wk_font_color_id, $wk_back_color_id);
                        }
                    }

                    // 出勤表の休暇種別等画面の変更名称と非表示設定を反映
                    for ($i = 0, $i_cnt = count($data); $i < $i_cnt; $i++) {
                        $tmp_reason_id = $data[$i]["reason"];
                        if (!empty($reason_display_set[$data[$i]["reason"]])) {
                            $data[$i]["reason_display_flag"] = $reason_display_set[$data[$i]["reason"]]['display_flag'];
                            if (!empty($reason_display_set[$data[$i]["reason"]]['display_name'])) {
                                $data[$i]["reason_name"] = $reason_display_set[$data[$i]["reason"]]['display_name'];
                            }
                        }
                    }
                    //-------------------------------------------------------------------
                    //データ設定
                    //-------------------------------------------------------------------
                    $k = count($data_all);
                    for ($i = 0, $i_cnt = count($data); $i < $i_cnt; $i++) {
                        $data_all[$k] = $data[$i];
                        $k++;
                    }
                }
            }
        }
        return $data_all;
    }

    /*     * ********************************************************************** */

    // 勤務シフト記号情報取得（履歴）
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $duty_yyyy          勤務年
    // @param   $duty_mm            勤務月
    // @param   $data_atdptn        出勤パターン情報
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_pattern_history_array($pattern_id, $duty_yyyy, $duty_mm, $data_atdptn) {
        return $this->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
    }

    /*     * *
     * 出勤表の休暇種別等画面の設定取得
     *
     * @return  $data   取得情報 reason_idをキーとした配列に表示変更(display_name)と表示フラグ(display_flag)を設定
     */

    function get_reason_display_setting() {
        $sql = "select * from atdbk_reason_mst";
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);  //取得
        if ($sel === 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }

        //情報設定
        $data = array();
        while ($row = pg_fetch_assoc($sel)) {
            $data[$row['reason_id']] = array(
                'display_name' => (empty($row['display_name']) ? $row['default_name'] : $row['display_name']),
                'display_flag' => $row['display_flag'],
            );
        }
        return $data;
    }

    function get_reason_display_flag() {
        return $this->get_reason_display_setting();
    }

    /*     * ********************************************************************** */

    // 施設基準情報取得
    // @param   $standard_id    施設基準ＩＤ
    //
    // @return  $ret            取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_institution_standard_array($standard_id) {
        //-------------------------------------------------------------------
        //ＳＱＬ
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_institution_standard";
        if ($standard_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where standard_id = $standard_id ";
        }
        $cond .= "order by standard_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //件数取得
        //-------------------------------------------------------------------
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //データ設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["standard_id"] = pg_fetch_result($sel, $i, "standard_id");  //施設基準ＩＤ（１〜１０）
            $data[$i]["ward_cnt"] = pg_fetch_result($sel, $i, "ward_cnt");            //病棟数
            $data[$i]["sickbed_cnt"] = pg_fetch_result($sel, $i, "sickbed_cnt");  //病床数
            $data[$i]["report_kbn"] = pg_fetch_result($sel, $i, "report_kbn");        //届出区分
            $data[$i]["report_patient_cnt"] = pg_fetch_result($sel, $i, "report_patient_cnt");        //届出時入院患者数
            $data[$i]["hosp_patient_cnt"] = pg_fetch_result($sel, $i, "hosp_patient_cnt");            //一日平均入院患者数
            $data[$i]["nursing_assistance_flg"] = pg_fetch_result($sel, $i, "nursing_assistance_flg");    //看護補助加算の有無
            $data[$i]["hospitalization_cnt"] = pg_fetch_result($sel, $i, "hospitalization_cnt");  //平均在院日数 20130402項目復活
            $data[$i]["prov_start_hhmm"] = pg_fetch_result($sel, $i, "prov_start_hhmm");      //夜勤時間帯（開始時分）
            $data[$i]["prov_end_hhmm"] = pg_fetch_result($sel, $i, "prov_end_hhmm");          //夜勤時間帯（終了時分）

            $data[$i]["limit_time"] = pg_fetch_result($sel, $i, "limit_time");                    //月平均夜勤時間上限
            $data[$i]["shift_flg"] = pg_fetch_result($sel, $i, "shift_flg");                  //交代制

            $data[$i]["send_start_hhmm_1"] = pg_fetch_result($sel, $i, "send_start_hhmm_1");  //申し送り時間１（開始時分）
            $data[$i]["send_end_hhmm_1"] = pg_fetch_result($sel, $i, "send_end_hhmm_1");      //申し送り時間１（終了時分）
            $data[$i]["send_start_hhmm_2"] = pg_fetch_result($sel, $i, "send_start_hhmm_2");  //申し送り時間１（開始時分）
            $data[$i]["send_end_hhmm_2"] = pg_fetch_result($sel, $i, "send_end_hhmm_2");      //申し送り時間１（終了時分）
            $data[$i]["send_start_hhmm_3"] = pg_fetch_result($sel, $i, "send_start_hhmm_3");  //申し送り時間１（開始時分）
            $data[$i]["send_end_hhmm_3"] = pg_fetch_result($sel, $i, "send_end_hhmm_3");      //申し送り時間１（終了時分）

            $data[$i]["labor_cnt"] = pg_fetch_result($sel, $i, "labor_cnt");                  ////所定労働時間

            $data[$i]["nurse_staffing_flg"] = pg_fetch_result($sel, $i, "nurse_staffing_flg");    //看護配置加算の有無
            $data[$i]["acute_nursing_flg"] = pg_fetch_result($sel, $i, "acute_nursing_flg");  //急性期看護補助加算の有無
            $data[$i]["acute_nursing_cnt"] = pg_fetch_result($sel, $i, "acute_nursing_cnt");  //急性期看護補助加算の配置比率
            $data[$i]["nursing_assistance_cnt"] = pg_fetch_result($sel, $i, "nursing_assistance_cnt");    //看護補助加算の配置比率
            //様式９平成２４年度改訂対応 20120406
            $data[$i]["acute_assistance_flg"] = pg_fetch_result($sel, $i, "acute_assistance_flg");    //急性期看護補助体制加算の届出区分
            $data[$i]["acute_assistance_cnt"] = pg_fetch_result($sel, $i, "acute_assistance_cnt");    //急性期看護補助体制加算の配置比率
            $data[$i]["night_acute_assistance_flg"] = pg_fetch_result($sel, $i, "night_acute_assistance_flg");    //夜間急性期看護補助体制加算の届出区分
            $data[$i]["night_acute_assistance_cnt"] = pg_fetch_result($sel, $i, "night_acute_assistance_cnt");    //夜間急性期看護補助体制加算の配置比率
            $data[$i]["night_shift_add_flg"] = pg_fetch_result($sel, $i, "night_shift_add_flg");  //看護職員夜間配置加算の有無
            //「特定入院料」、「特定入院料（看護職員＋看護補助者)」追加対応 20121011
            $data[$i]["specific_type"] = pg_fetch_result($sel, $i, "specific_type");    //特定入院料種類
            $data[$i]["nurse_staffing_cnt"] = pg_fetch_result($sel, $i, "nurse_staffing_cnt");    //看護配置
            $data[$i]["assistance_staffing_cnt"] = pg_fetch_result($sel, $i, "assistance_staffing_cnt");    //看護補助配置
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 施設基準情報取得（様式９用　履歴）
    // @param   $standard_id    施設基準ＩＤ
    // @param   $duty_yyyy      勤務年
    // @param   $duty_mm        勤務月
    //
    // @return  $ret            取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_institution_history_array($standard_id, $duty_yyyy, $duty_mm) {
        //-------------------------------------------------------------------
        //ＳＱＬ
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_institution_history ";
        $cond = "where standard_id = $standard_id ";
        $cond .= "and duty_yyyy = $duty_yyyy ";
        $cond .= "and duty_mm = $duty_mm ";
        $cond .= "order by standard_id, duty_yyyy, duty_mm";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //件数取得
        //-------------------------------------------------------------------
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //データ有無判定
        //-------------------------------------------------------------------
        $data = array();
        if ($num <= 0) {
            $data = $this->get_duty_shift_institution_standard_array($standard_id);
        } else {
            //-------------------------------------------------------------------
            //データ設定
            //-------------------------------------------------------------------
            for ($i = 0; $i < $num; $i++) {
                $data[$i]["standard_id"] = pg_fetch_result($sel, $i, "standard_id");  //施設基準ＩＤ（１〜１０）
                $data[$i]["ward_cnt"] = pg_fetch_result($sel, $i, "ward_cnt");            //病棟数
                $data[$i]["sickbed_cnt"] = pg_fetch_result($sel, $i, "sickbed_cnt");  //病床数
                $data[$i]["report_kbn"] = pg_fetch_result($sel, $i, "report_kbn");        //届出区分
                $data[$i]["report_patient_cnt"] = pg_fetch_result($sel, $i, "report_patient_cnt");        //届出時入院患者数
                $data[$i]["hosp_patient_cnt"] = pg_fetch_result($sel, $i, "hosp_patient_cnt");            //一日平均入院患者数
                $data[$i]["nursing_assistance_flg"] = pg_fetch_result($sel, $i, "nursing_assistance_flg");    //看護補助加算の有無

                $data[$i]["hospitalization_cnt"] = pg_fetch_result($sel, $i, "hospitalization_cnt");  //平均在院日数

                $data[$i]["prov_start_hhmm"] = pg_fetch_result($sel, $i, "prov_start_hhmm");      //夜勤時間帯（開始時分）
                $data[$i]["prov_end_hhmm"] = pg_fetch_result($sel, $i, "prov_end_hhmm");          //夜勤時間帯（終了時分）

                $data[$i]["limit_time"] = pg_fetch_result($sel, $i, "limit_time");                    //月平均夜勤時間上限
                $data[$i]["shift_flg"] = pg_fetch_result($sel, $i, "shift_flg");                  //交代制

                $data[$i]["send_start_hhmm_1"] = pg_fetch_result($sel, $i, "send_start_hhmm_1");  //申し送り時間１（開始時分）
                $data[$i]["send_end_hhmm_1"] = pg_fetch_result($sel, $i, "send_end_hhmm_1");      //申し送り時間１（終了時分）
                $data[$i]["send_start_hhmm_2"] = pg_fetch_result($sel, $i, "send_start_hhmm_2");  //申し送り時間１（開始時分）
                $data[$i]["send_end_hhmm_2"] = pg_fetch_result($sel, $i, "send_end_hhmm_2");      //申し送り時間１（終了時分）
                $data[$i]["send_start_hhmm_3"] = pg_fetch_result($sel, $i, "send_start_hhmm_3");  //申し送り時間１（開始時分）
                $data[$i]["send_end_hhmm_3"] = pg_fetch_result($sel, $i, "send_end_hhmm_3");      //申し送り時間１（終了時分）

                $data[$i]["labor_cnt"] = pg_fetch_result($sel, $i, "labor_cnt");                  ////所定労働時間

                $data[$i]["nurse_staffing_flg"] = pg_fetch_result($sel, $i, "nurse_staffing_flg");    //看護配置加算の有無
                $data[$i]["acute_nursing_flg"] = pg_fetch_result($sel, $i, "acute_nursing_flg");  //急性期看護補助加算の有無
                $data[$i]["acute_nursing_cnt"] = pg_fetch_result($sel, $i, "acute_nursing_cnt");  //急性期看護補助加算の配置比率
                $data[$i]["nursing_assistance_cnt"] = pg_fetch_result($sel, $i, "nursing_assistance_cnt");    //看護補助加算の配置比率
                //様式９平成２４年度改訂対応 20120406
                $data[$i]["acute_assistance_flg"] = pg_fetch_result($sel, $i, "acute_assistance_flg");    //急性期看護補助体制加算の届出区分
                $data[$i]["acute_assistance_cnt"] = pg_fetch_result($sel, $i, "acute_assistance_cnt");    //急性期看護補助体制加算の配置比率
                $data[$i]["night_acute_assistance_flg"] = pg_fetch_result($sel, $i, "night_acute_assistance_flg");    //夜間急性期看護補助体制加算の届出区分
                $data[$i]["night_acute_assistance_cnt"] = pg_fetch_result($sel, $i, "night_acute_assistance_cnt");    //夜間急性期看護補助体制加算の配置比率
                $data[$i]["night_shift_add_flg"] = pg_fetch_result($sel, $i, "night_shift_add_flg");  //看護職員夜間配置加算の有無
                //「特定入院料」、「特定入院料（看護職員＋看護補助者)」追加対応 20121011
                $data[$i]["specific_type"] = pg_fetch_result($sel, $i, "specific_type");    //特定入院料種類
                $data[$i]["nurse_staffing_cnt"] = pg_fetch_result($sel, $i, "nurse_staffing_cnt");    //看護配置
                $data[$i]["assistance_staffing_cnt"] = pg_fetch_result($sel, $i, "assistance_staffing_cnt");    //看護補助配置
            }
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトチーム更新
    // @param   $data_cnt   更新データ件数
    // @param   $group_id   勤務グループ
    // @param   $duty_yyyy  勤務年
    // @param   $duty_mm    勤務月
    // @param   $staff_id   更新スタッフ(array)
    // @param   $team       更新チーム(array)
    //
    /*     * ********************************************************************** */
    function update_duty_shift_plan_team($data_cnt, $group_id, $duty_yyyy, $duty_mm, $staff_id, $team) {

        //チームデータ事前に削除
        $sql = "DELETE FROM duty_shift_plan_team ";
        $cond = "WHERE group_id='$group_id' AND duty_yyyy=$duty_yyyy AND duty_mm=$duty_mm ";
        $del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($del == 0) {
            pg_query($this->_db_con, "rollback");
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //チームを更新する
        for ($i = 0; $i < $data_cnt; $i++) {
            //空ならNULL
            if ($team[$i] == "") {
                $team[$i] = null;
            }

            //INSERT
            $sql = "INSERT INTO duty_shift_plan_team (group_id, emp_id, duty_yyyy, duty_mm, team_id) VALUES (";
            $content = array($group_id, $staff_id[$i], $duty_yyyy, $duty_mm, $team[$i]);
            $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
            if ($ins == 0) {
                pg_query($this->_db_con, "rollback");
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }

    /*     * ********************************************************************** */

    // 施設基準情報更新（様式９用　履歴）
    // @param   $standard_id    施設基準ＩＤ
    // @param   $duty_yyyy      勤務年
    // @param   $duty_mm        勤務月
    //
    /*     * ********************************************************************** */
    function update_duty_shift_institution_history($standard_id, $duty_yyyy, $duty_mm) {

        $sql = "select count(*) as cnt from duty_shift_institution_history ";
        $cond = "where standard_id = $standard_id ";
        $cond .= "and duty_yyyy = $duty_yyyy ";
        $cond .= "and duty_mm = $duty_mm ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //件数取得
        //-------------------------------------------------------------------
        $cnt = pg_fetch_result($sel, $i, "cnt");
        if ($cnt > 0) {
            //データがあれば申し送り時刻を更新する
            $standard_array = $this->get_duty_shift_institution_standard_array($standard_id);

            $sql = "update duty_shift_institution_history set";
            //届出区分の更新を追加 start 20120410
            $set = array("send_start_hhmm_1", "send_end_hhmm_1", "send_start_hhmm_2", "send_end_hhmm_2", "send_start_hhmm_3", "send_end_hhmm_3", "nurse_staffing_flg", "acute_nursing_flg", "acute_nursing_cnt", "nursing_assistance_flg", "nursing_assistance_cnt", "prov_start_hhmm", "prov_end_hhmm", "report_kbn", "acute_assistance_flg", "acute_assistance_cnt", "night_acute_assistance_flg", "night_acute_assistance_cnt", "night_shift_add_flg");
            //
            $setvalue = array($standard_array[0]["send_start_hhmm_1"], $standard_array[0]["send_end_hhmm_1"], $standard_array[0]["send_start_hhmm_2"], $standard_array[0]["send_end_hhmm_2"], $standard_array[0]["send_start_hhmm_3"], $standard_array[0]["send_end_hhmm_3"], $standard_array[0]["nurse_staffing_flg"], $standard_array[0]["acute_nursing_flg"], $standard_array[0]["acute_nursing_cnt"], $standard_array[0]["nursing_assistance_flg"], $standard_array[0]["nursing_assistance_cnt"], $standard_array[0]["prov_start_hhmm"], $standard_array[0]["prov_end_hhmm"], $standard_array[0]["report_kbn"], $standard_array[0]["acute_assistance_flg"], $standard_array[0]["acute_assistance_cnt"], $standard_array[0]["night_acute_assistance_flg"], $standard_array[0]["night_acute_assistance_cnt"], $standard_array[0]["night_shift_add_flg"]);
            //届出区分の更新を追加 end 20120410
            $cond = "where standard_id = $standard_id ";
            $cond .= "and duty_yyyy = $duty_yyyy ";
            $cond .= "and duty_mm = $duty_mm ";
            $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
            if ($upd == 0) {
                pg_query($this->_db_con, "rollback");
                pg_close($this->_db_con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
        }
    }

    /*     * ********************************************************************** */

    // 必要人数情報（標準）の取得
    // @param   $group_id       勤務シフトグループＩＤ
    // @param   $data_job       職種情報
    // @param   $data_atdptn    出勤パターン情報
    //
    // @return  $data           取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_need_cnt_standard_array($group_id, $data_job, $data_atdptn) {
        //-------------------------------------------------------------------
        // 検索
        //-------------------------------------------------------------------
        $sql = "SELECT c.*, c.team as team_id, j1.job_nm as job_name, j2.job_nm as job_name2, j3.job_nm as job_name3, t.team_name, ";
        $sql .= "CASE c.sex WHEN '1' THEN '男性' WHEN '2' THEN '女性' ELSE c.sex END as sex_name ";
        $sql .= "FROM duty_shift_need_cnt_standard c ";
        $sql .= "LEFT JOIN jobmst j1 ON c.job_id=j1.job_id ";
        $sql .= "LEFT JOIN jobmst j2 ON c.job_id2=j2.job_id ";
        $sql .= "LEFT JOIN jobmst j3 ON c.job_id3=j3.job_id ";
        $sql .= "LEFT JOIN duty_shift_staff_team t ON c.team=t.team_id ";
        $cond = "where group_id='$group_id' ";
        $cond .= "order by group_id, no ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);   //件数取得
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        if ($num > 0) {
            $data = pg_fetch_all($sel);
        } else {
            for ($i = 0; $i < count($data_atdptn); $i++) {
                $data[$i]["group_id"] = $group_id;
                $data[$i]["atdptn_ptn_id"] = $data_atdptn[$i]["id"];
                $data[$i]["job_id"] = $data_job[0]["id"];
                $data[$i]["job_name"] = $data_job[0]["name"];
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 必要人数情報（カレンダー）の取得
    // @param   $group_id       勤務シフトグループＩＤ
    // @param   $duty_yyyy      年
    // @param   $duty_mm        月
    // @param   $day_cnt        日数
    // @param   $data_job       職種情報
    // @param   $data_atdptn    出勤パターン情報
    // @param   $data_week      週情報
    // @param   $data_calendar  カレンダー情報
    //
    // @return  $ret            取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_need_cnt_calendar_array(
    $group_id, $duty_yyyy, $duty_mm, $day_cnt, $data_job, $data_atdptn, $data_week, $data_calendar) {
        //-------------------------------------------------------------------
        //開始／終了年月日設定
        //-------------------------------------------------------------------
        $wk_cnt = count($data_calendar) - 1;
        $start_date = $data_calendar[0]["date"];
        $end_date = $data_calendar[$wk_cnt]["date"];
        //-------------------------------------------------------------------
        // 必要人数情報（カレンダー＿職種）の取得
        //-------------------------------------------------------------------
        $sql = "SELECT c.*, c.team as team_id, j1.job_nm as job_name, j2.job_nm as job_name2, j3.job_nm as job_name3, t.team_name, ";
        $sql .= "CASE c.sex WHEN '1' THEN '男性' WHEN '2' THEN '女性' ELSE c.sex END as sex_name ";
        $sql .= "FROM duty_shift_need_cnt_calendar_job c ";
        $sql .= "LEFT JOIN jobmst j1 ON c.job_id=j1.job_id ";
        $sql .= "LEFT JOIN jobmst j2 ON c.job_id2=j2.job_id ";
        $sql .= "LEFT JOIN jobmst j3 ON c.job_id3=j3.job_id ";
        $sql .= "LEFT JOIN duty_shift_staff_team t ON c.team=t.team_id ";
        $cond = "where group_id='$group_id' ";
        $cond .= "and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm ";
        $cond .= "order by  group_id, no, duty_yyyy, duty_mm";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num_job = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //データ設定
        //-------------------------------------------------------------------
        $data = array();
        if ($num_job > 0) {

            //空の日別必要人数配列をあらかじめ用意
            $need_cnt = array();
            for ($i = 1; $i <= $day_cnt; $i++) {
                $need_cnt[$i] = "";
            }

            while ($row = pg_fetch_array($sel)) {
                $row["need_cnt"] = $need_cnt;
                $data[] = $row;
            }
            //atdptn_ptn_id順に並べ替え
            $atdptn = array();
            foreach ($data as $key => $row) {
                $atdptn[$key] = $row["atdptn_ptn_id"];
            }
            array_multisort($atdptn, SORT_ASC, $data);

            //-------------------------------------------------------------------
            // 必要人数情報（カレンダー）の取得
            //-------------------------------------------------------------------
            //ＳＱＬ
            $sql = "select * from duty_shift_need_cnt_calendar";
            $cond = "where group_id='$group_id' ";
            $cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
            $cond .= "order by  group_id, duty_date";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            //-------------------------------------------------------------------
            //データ設定
            //-------------------------------------------------------------------
            $wk_array = pg_fetch_all($sel);
            //表示用に再設定
            for ($i = 0; $i < count($data); $i++) {
                for ($k = 0; $k < count($wk_array); $k++) {
                    if ($data[$i]["atdptn_ptn_id"] == $wk_array[$k]["atdptn_ptn_id"] and
                            $data[$i]["job_id"] == $wk_array[$k]["job_id"] and
                            $data[$i]["team_id"] == $wk_array[$k]["team"] and
                            $data[$i]["sex"] == $wk_array[$k]["sex"]
                    ) {
                        //開始日からの位置を計算
                        $wk = (($this->to_timestamp($wk_array[$k]["duty_date"]) - $this->to_timestamp($start_date)) / (24 * 60 * 60)) + 1;
                        $data[$i]["need_cnt"][$wk] = $wk_array[$k]["need_cnt"];
                    }
                }
            }
        } else {
            //-------------------------------------------------------------------
            // 必要人数情報（標準）の取得
            //-------------------------------------------------------------------
            $data_reed_sta = $this->get_duty_shift_need_cnt_standard_array($group_id, $data_job, $data_atdptn);
            //-------------------------------------------------------------------
            //データ設定
            //-------------------------------------------------------------------
            for ($i = 0; $i < count($data_atdptn); $i++) {
                for ($k = 0; $k < count($data_reed_sta); $k++) {
                    if ($data_atdptn[$i]["id"] == $data_reed_sta[$k]["atdptn_ptn_id"]) {
                        $wk = $data_reed_sta[$k];

                        for ($p = 1; $p <= $day_cnt; $p++) {
                            //平日
                            $wk["need_cnt"][$p] = $data_reed_sta[$k]["week_cnt"];
                            //祝日
                            if (($data_calendar[$p - 1]["type"] == "4") ||
                                    ($data_calendar[$p - 1]["type"] == "5") ||
                                    ($data_calendar[$p - 1]["type"] == "6")) {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["holiday_cnt"];
                            }
                            //七曜日
                            if ($data_week[$p]["name"] == "月" && $data_reed_sta[$k]["mon_cnt"] != "") {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["mon_cnt"];
                            }
                            if ($data_week[$p]["name"] == "火" && $data_reed_sta[$k]["tue_cnt"] != "") {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["tue_cnt"];
                            }
                            if ($data_week[$p]["name"] == "水" && $data_reed_sta[$k]["wed_cnt"] != "") {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["wed_cnt"];
                            }
                            if ($data_week[$p]["name"] == "木" && $data_reed_sta[$k]["thu_cnt"] != "") {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["thu_cnt"];
                            }
                            if ($data_week[$p]["name"] == "金" && $data_reed_sta[$k]["fri_cnt"] != "") {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["fri_cnt"];
                            }
                            if ($data_week[$p]["name"] == "土") {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["sat_cnt"];
                            }
                            if ($data_week[$p]["name"] == "日") {
                                $wk["need_cnt"][$p] = $data_reed_sta[$k]["sun_cnt"];
                            }
                        }

                        $data[] = $wk;
                    }
                }
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 自動シフト条件情報の取得
    // @param   なし
    //
    // @return  $data       取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_auto_array() {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_auto";
        $cond .= "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = pg_fetch_array($sel);
        if ($num <= 0) {
            $data["pattern_day"] = "";
            $data["ptn_search_cnt"] = "";
            $data["analytical_time"] = "";
            $data["auto_logic"] = "";
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務シフトグループIDを採番
    // @param   なし
    //
    // @return  $ret    採番ＩＤ
    /*     * ********************************************************************** */
    function get_new_duty_shift_group_id() {
        //-------------------------------------------------------------------
        //ＭＡＸ値ＳＥＬＥＣＴ
        //-------------------------------------------------------------------
        $sql = "select max(group_id) from duty_shift_group";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $max = pg_fetch_result($sel, 0, "max");
        //-------------------------------------------------------------------
        //採番
        //-------------------------------------------------------------------
        $val = substr($max, 4, 12) + 1;
        $yymm = date("y") . date("m");
        for ($i = 8; $i > strlen($val); $i--) {
            $zero .= "0";
        }
        $ret = "$yymm$zero$val";

        return $ret;
    }

    /*     * ********************************************************************** */

    // カレンダーＤＢ(calendar)情報取得
    // @param   $start_date     検索開始日
    // @param   $end_date       検索終了日
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_calendar_array($start_date, $end_date) {
        //-------------------------------------------------------------------
        //日付設定
        //-------------------------------------------------------------------
        $wk_yyyy = substr($start_date, 0, 4);
        $wk_mm = substr($start_date, 4, 2);
        $wk_dd = substr($start_date, 6, 2);
        $data = array();
        for ($i = 1; $i <= 3650; $i++) {
            $tmp_date = mktime(0, 0, 0, $wk_mm, ((int) $wk_dd + $i - 1), $wk_yyyy);

            $wk = date("Ymd", $tmp_date);
            if ($wk <= $end_date) {
                $data[$i - 1]["date"] = $wk;
                $data[$i - 1]["type"] = "";
                $data[$i - 1]["memo"] = "";
            }
            if ($wk >= $end_date) {
                break;
            }
        }
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select * from calendar";
        $cond = "where date >= '$start_date' ";
        $cond .= "and date <= '$end_date' ";
        $cond .= "order by date";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        for ($i = 0; $i < $num; $i++) {
            $wk_date = pg_fetch_result($sel, $i, "date");

            for ($k = 0; $k < count($data); $k++) {
                if ($data[$k]["date"] == $wk_date) {
                    $data[$k]["type"] = pg_fetch_result($sel, $i, "type");
                    $data[$k]["memo"] = pg_fetch_result($sel, $i, "memo");
                }
            }
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務パターンＤＢ(wktmgrp)情報取得
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_wktmgrp_array() {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select group_id, group_name from wktmgrp";
        $cond = "order by group_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["id"] = pg_fetch_result($sel, $i, "group_id");
            $data[$i]["name"] = pg_fetch_result($sel, $i, "group_name");
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 出勤パターンＤＢ(atdptn)情報取得
    // @param   $group_id       勤務パターンＩＤ
    // @param   $order_flag     表示順フラグ（省略可）1:表示順を使用、"":atdptn_id順
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_atdptn_array($group_id, $order_flag = "") {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select * from atdptn";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = $group_id ";
        }
        $cond .= "order by group_id";
        if ($order_flag == "1") {
            $cond .= ", atdptn_order_no";
        }
        $cond .= ", atdptn_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
            $data[$i]["id"] = pg_fetch_result($sel, $i, "atdptn_id");
            $data[$i]["name"] = pg_fetch_result($sel, $i, "atdptn_nm");
            $data[$i]["workday_count"] = pg_fetch_result($sel, $i, "workday_count");  // 勤務日数換算。夜勤明け等対応 2008/06/10
            $data[$i]["reason"] = pg_fetch_result($sel, $i, "reason");    // 事由設定用 2009/01/08
            $data[$i]["display_flag"] = pg_fetch_result($sel, $i, "display_flag");    // 表示フラグ 20101007
            $data[$i]["after_night_duty_flag"] = pg_fetch_result($sel, $i, "after_night_duty_flag");  // 明け設定 20110927
            $data[$i]["previous_day_flag"] = pg_fetch_result($sel, $i, "previous_day_flag");  // 前日フラグ設定 20120228
        }
        return $data;
    }

    // 20140220 start
    /*     * ********************************************************************** */
    // 出勤パターンＤＢ(atdptn)情報取得、複数勤務パターンＩＤ対応
    // @param   $arr_group_id   勤務パターンＩＤの配列
    // @param   $order_flag     表示順フラグ（省略可）1:表示順を使用、"":atdptn_id順
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_atdptn_array_2($arr_group_id, $order_flag = "") {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $cond_gstr = implode(",", $arr_group_id);
        $sql = "select * from atdptn";
        $cond = "where group_id in ($cond_gstr) ";
        $cond .= "order by group_id";
        if ($order_flag == "1") {
            $cond .= ", atdptn_order_no";
        }
        $cond .= ", atdptn_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
            $data[$i]["id"] = pg_fetch_result($sel, $i, "atdptn_id");
            $data[$i]["name"] = pg_fetch_result($sel, $i, "atdptn_nm");
            $data[$i]["workday_count"] = pg_fetch_result($sel, $i, "workday_count");  // 勤務日数換算。夜勤明け等対応 2008/06/10
            $data[$i]["reason"] = pg_fetch_result($sel, $i, "reason");    // 事由設定用 2009/01/08
            $data[$i]["display_flag"] = pg_fetch_result($sel, $i, "display_flag");    // 表示フラグ 20101007
            $data[$i]["after_night_duty_flag"] = pg_fetch_result($sel, $i, "after_night_duty_flag");  // 明け設定 20110927
            $data[$i]["previous_day_flag"] = pg_fetch_result($sel, $i, "previous_day_flag");  // 前日フラグ設定 20120228
        }
        return $data;
    }

    // 20140220 end
    /*     * ********************************************************************** */
    // 半日応援情報取得
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_half_assist_array() {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select * from atdptn";
        $cond = "where assist_time_flag = 1 ";
        $cond .= "order by group_id, atdptn_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $wk_group_id = pg_fetch_result($sel, $i, "group_id");
            $wk_atdptn_id = pg_fetch_result($sel, $i, "atdptn_id");
            $data["$wk_group_id"]["$wk_atdptn_id"]["assist_start_time"] = pg_fetch_result($sel, $i, "assist_start_time");
            $data["$wk_group_id"]["$wk_atdptn_id"]["assist_end_time"] = pg_fetch_result($sel, $i, "assist_end_time");
            $data["$wk_group_id"]["$wk_atdptn_id"]["assist_group_id"] = pg_fetch_result($sel, $i, "assist_group_id");
        }
        return $data;
    }

    /**
     * 出勤パターンＤＢ(atdptn)情報1件取得
     *
     * @param mixed $group_id 勤務パターングループＩＤ
     * @param mixed $pattern_id 勤務パターンＩＤ
     * @return mixed This is the return value description
     *
     */
    function get_atdptn_info($group_id, $pattern_id) {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select * from atdptn";
        $cond = "where group_id = $group_id and atdptn_id = $pattern_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        if ($num == 1) {
            $data = pg_fetch_array($sel);
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 出勤パターンＤＢ(atdptn)情報取得（履歴）※未使用
    // @param   $group_id       勤務パターンＩＤ
    // @param   $duty_yyyy      勤務年
    // @param   $duty_mm        勤務月
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_atdptn_history_array($duty_yyyy, $duty_mm) {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_atdptn_history";
        if ($group_id == "") {
            $cond = "where 1=1 ";
        } else {
            $cond = "where group_id = $group_id ";
        }
        $cond .= "and duty_yyyy = $duty_yyyy ";
        $cond .= "and duty_mm = $duty_mm ";
        $cond .= "order by duty_yyyy, duty_mm, group_id, atdptn_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        if ($num <= 0) {
            $data = $this->get_atdptn_array($group_id);
        } else {
            for ($i = 0; $i < $num; $i++) {
                //ＤＢよりの取得値
                $data[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
                $data[$i]["id"] = pg_fetch_result($sel, $i, "atdptn_id");
                $data[$i]["name"] = pg_fetch_result($sel, $i, "atdptn_nm");
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 職員ＤＢ(empmst)情報取得
    // @param   $emp_id     職員ＩＤ
    //
    // @return  $data_emp   取得情報
    /*     * ********************************************************************** */
    function get_empmst_array($emp_id) {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_job, emp_st, emp_join, emp_sex from empmst";
        if ($emp_id == "") {
            $cond = "where 1 = 1 ";
        } else {
            $cond = "where emp_id = '$emp_id' ";
        }
        $cond .= "order by emp_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data_emp = array();
        for ($i = 0; $i < $num; $i++) {
            $data_emp[$i]["id"] = pg_fetch_result($sel, $i, "emp_id");        //職員ＩＤ
            $data_emp[$i]["name"] = pg_fetch_result($sel, $i, "emp_lt_nm") . " " . pg_fetch_result($sel, $i, "emp_ft_nm");    //職員名（漢字）
            $data_emp[$i]["job_id"] = pg_fetch_result($sel, $i, "emp_job");       //職種ＩＤ
            $data_emp[$i]["st_id"] = pg_fetch_result($sel, $i, "emp_st");        //役職ＩＤ
            $data_emp[$i]["join"] = pg_fetch_result($sel, $i, "emp_join");  //入職日(YYYYMMDD)
            $data_emp[$i]["sex"] = pg_fetch_result($sel, $i, "emp_sex");       //性別
        }

        return $data_emp;
    }

    /*     * ********************************************************************** */

    // 指定グループの職員情報取得
    // @param   $group_id   グループＩＤ
    //
    // @return  $data_emp   取得情報
    /*     * ********************************************************************** */
    function get_group_empmst_array($group_id) {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select e.emp_id, e.emp_lt_nm, e.emp_ft_nm, e.emp_job, e.emp_st, e.emp_join from empmst e left join duty_shift_staff d on d.emp_id = e.emp_id ";
        if ($group_id != "") {
            $cond = "where d.group_id = '$group_id' ";
        } else {
            $cond = "";
        }
        $cond .= " order by e.emp_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data_emp = array();
        for ($i = 0; $i < $num; $i++) {
            $data_emp[$i]["id"] = pg_fetch_result($sel, $i, "emp_id");        //職員ＩＤ
            $data_emp[$i]["name"] = pg_fetch_result($sel, $i, "emp_lt_nm") . " " . pg_fetch_result($sel, $i, "emp_ft_nm");    //職員名（漢字）
            $data_emp[$i]["job_id"] = pg_fetch_result($sel, $i, "emp_job");       //職種ＩＤ
            $data_emp[$i]["st_id"] = pg_fetch_result($sel, $i, "emp_st");        //役職ＩＤ
            $data_emp[$i]["join"] = pg_fetch_result($sel, $i, "emp_join");  //入職日(YYYYMMDD)
        }

        return $data_emp;
    }

    /*     * ********************************************************************** */

    // 有効なグループの職員情報取得
    // @param   $emp_id     職員ＩＤ
    // @param   $duty_yyyy  年（省略可）
    // @param   $duty_mm    月（省略可）
    //
    // @return  $data_emp   取得情報
    // ※年月が設定されている場合のみ、応援追加グループ分も取得
    /*     * ********************************************************************** */
    function get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm) {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        /*
          $next_mm = $duty_mm + 1;
          if ($next_mm >= 13) {
          $next_yyyy = $duty_yyyy + 1;
          $next_mm = 1;
          } else {
          $next_yyyy = $duty_yyyy;
          }
          $prev_mm = $duty_mm - 1;
          if ($prev_mm <= 0) {
          $prev_yyyy = $duty_yyyy - 1;
          $prev_mm = 12;
          } else {
          $prev_yyyy = $duty_yyyy;
          }
         */
        $sql = "select e.emp_id, e.emp_lt_nm, e.emp_ft_nm, e.emp_job, e.emp_st, e.emp_join, e.emp_sex from empmst e, " .
                "(select emp_id from duty_shift_staff where group_id in " .
                " (select group_id from duty_shift_group " .
                "   where person_charge_id = '$emp_id' or caretaker_id = '$emp_id' or caretaker2_id = '$emp_id' or caretaker3_id = '$emp_id' or caretaker4_id = '$emp_id' or caretaker5_id = '$emp_id' or caretaker6_id = '$emp_id' or caretaker7_id = '$emp_id' or caretaker8_id = '$emp_id' or caretaker9_id = '$emp_id' or caretaker10_id = '$emp_id' " .
                "  union select group_id from duty_shift_staff where emp_id = '$emp_id' ";
        $sql .= ") ";
        if ($duty_yyyy != "" && $duty_mm != "") {
            //          $sql .= " union select emp_id from duty_shift_plan_staff where group_id in (select group_id from duty_shift_plan_staff where emp_id = '$emp_id' and duty_shift_plan_staff.duty_yyyy = $duty_yyyy and duty_shift_plan_staff.duty_mm = $duty_mm )";
            $sql .= " union select emp_id from duty_shift_plan_staff where group_id in (select distinct(group_id) from duty_shift_plan_staff where ((duty_shift_plan_staff.duty_yyyy = $duty_yyyy and duty_shift_plan_staff.duty_mm = $duty_mm)) )";
        }
        // or (duty_shift_plan_staff.duty_yyyy = $next_yyyy and duty_shift_plan_staff.duty_mm = $next_mm) or (duty_shift_plan_staff.duty_yyyy = $prev_yyyy and duty_shift_plan_staff.duty_mm = $prev_mm) //前月、翌月を除く 20110304
        $sql .= ") d ";

        $cond = "where e.emp_id = d.emp_id order by e.emp_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data_emp = array();
        for ($i = 0; $i < $num; $i++) {
            $data_emp[$i]["id"] = pg_fetch_result($sel, $i, "emp_id");        //職員ＩＤ
            $data_emp[$i]["name"] = pg_fetch_result($sel, $i, "emp_lt_nm") . " " . pg_fetch_result($sel, $i, "emp_ft_nm");    //職員名（漢字）
            $data_emp[$i]["job_id"] = pg_fetch_result($sel, $i, "emp_job");       //職種ＩＤ
            $data_emp[$i]["st_id"] = pg_fetch_result($sel, $i, "emp_st");        //役職ＩＤ
            $data_emp[$i]["join"] = pg_fetch_result($sel, $i, "emp_join");  //入職日(YYYYMMDD)
            $data_emp[$i]["sex"] = pg_fetch_result($sel, $i, "emp_sex");       //性別
        }

        return $data_emp;
    }

// 20140210 start
    /**
     * シフトグループIDに所属する職員の情報を取得する
     *  年月の指定がある場合duty_shift_plan_staffから、ない場合はduty_shift_staffから取得
     * @param mixed $group_id シフトグループID
     * @param mixed $duty_yyyy 年
     * @param mixed $duty_mm 月
     * @param mixed $month_flg 0:前月から当月 1:当月から当月 2:当月から翌月 ""未設定は月またがりを考慮しない（印刷とエクセルで、月単位で出力にした場合に応援追加分の職員名職種名が必要になるための対応）
     * @return array $data_emp  取得情報
     *
     */
    function get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm, $month_flg = "") {
        $data_emp = array();
        if ($group_id == "") {
            return $data_emp;
        }
        if ($duty_yyyy != "" && $duty_mm != "") {
            $prev_mm = $duty_mm - 1;
            if ($prev_mm <= 0) {
                $prev_yyyy = $duty_yyyy - 1;
                $prev_mm = 12;
            } else {
                $prev_yyyy = $duty_yyyy;
            }
            $next_mm = $duty_mm + 1;
            if ($next_mm >= 13) {
                $next_yyyy = $duty_yyyy + 1;
                $next_mm = 1;
            } else {
                $next_yyyy = $duty_yyyy;
            }

            $sql = "select a.emp_id, e.emp_lt_nm, e.emp_ft_nm, e.emp_job, e.emp_st, e.emp_join, e.emp_sex from duty_shift_plan_staff a left join empmst e on e.emp_id = a.emp_id  ";
            $cond = "where a.group_id = '$group_id' and (";
            $cond .= " (a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm ) ";
            //2:当月から翌月、データとしては前月分取得
            if ($month_flg == "2") {
                $cond .= " or (a.duty_yyyy = $prev_yyyy and a.duty_mm = $prev_mm ) ";
            }
            //0:前月から当月、データとしては翌月分取得
            if ($month_flg == "0") {
                $cond .= " or (a.duty_yyyy = $next_yyyy and a.duty_mm = $next_mm ) ";
            }
            $cond .= " ) ";
        } else {
            $sql = "select a.emp_id, e.emp_lt_nm, e.emp_ft_nm, e.emp_job, e.emp_st, e.emp_join, e.emp_sex from duty_shift_staff a left join empmst e on e.emp_id = a.emp_id  ";
            $cond = "where a.group_id = '$group_id' ";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //年月ごとにない場合、職員設定から検索
        if ($num == 0 && $duty_yyyy != "" && $duty_mm != "") {
            $sql = "select a.emp_id, e.emp_lt_nm, e.emp_ft_nm, e.emp_job, e.emp_st, e.emp_join, e.emp_sex from duty_shift_staff a left join empmst e on e.emp_id = a.emp_id  ";
            $cond = "where a.group_id = '$group_id' ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $num = pg_num_rows($sel);
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        for ($i = 0; $i < $num; $i++) {
            $data_emp[$i]["id"] = pg_fetch_result($sel, $i, "emp_id");        //職員ＩＤ
            $data_emp[$i]["name"] = pg_fetch_result($sel, $i, "emp_lt_nm") . " " . pg_fetch_result($sel, $i, "emp_ft_nm");    //職員名（漢字）
            $data_emp[$i]["job_id"] = pg_fetch_result($sel, $i, "emp_job");       //職種ＩＤ
            $data_emp[$i]["st_id"] = pg_fetch_result($sel, $i, "emp_st");        //役職ＩＤ
            $data_emp[$i]["join"] = pg_fetch_result($sel, $i, "emp_join");  //入職日(YYYYMMDD)
            $data_emp[$i]["sex"] = pg_fetch_result($sel, $i, "emp_sex");       //性別
        }

        return $data_emp;
    }

    /**
     * 職員設定(duty_shift_staff)から職員の所属するシフトグループIDを取得
     *
     * @param mixed $emp_id 職員ID
     * @return mixed シフトグループID
     *
     */
    function get_group_id_from_duty_shift_staff($emp_id) {
        $sql = "select a.emp_id, a.group_id from duty_shift_staff a ";
        $cond = " where a.emp_id = '$emp_id'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        $group_id = "";
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        if ($num > 0) {
            $group_id = pg_fetch_result($sel, 0, "group_id");
        }
        return $group_id;
    }

    /**
     * 職員が年月ごとのシフト作成に登録されているか件数を取得する
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $emp_id 職員ID
     * @param mixed $duty_yyyy 年
     * @param mixed $duty_mm 月
     * @return mixed 件数
     *
     */
    function check_duty_shift_plan_staff($group_id, $emp_id, $duty_yyyy, $duty_mm) {
        $sql = "select count(*) as cnt from duty_shift_plan_staff a ";
        $cond = " where a.group_id = '$group_id' and a.emp_id = '$emp_id' and a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $cnt = pg_fetch_result($sel, 0, "cnt");
        return $cnt;
    }

    //役職名、職種名、職員データ位置をclassの連想配列変数に設定
    function set_data_assoc($data_st, $data_job, $data_emp) {
        $num = count($data_st);
        for ($i = 0; $i < $num; $i++) {
            $key = $data_st[$i]["id"];
            $this->data_st_assoc[$key] = $data_st[$i]["name"];
        }
        $num = count($data_job);
        for ($i = 0; $i < $num; $i++) {
            $key = $data_job[$i]["id"];
            $this->data_job_assoc[$key] = $data_job[$i]["name"];
        }
        $num = count($data_emp);
        for ($i = 0; $i < $num; $i++) {
            $emp_id = $data_emp[$i]["id"];
            $this->data_emp_idx[$emp_id] = $i;
        }
        $this->data_assoc_flg = true;
    }

    // 20140210 end
    /*     * ********************************************************************** */
    // 指定施設基準の職員情報取得
    // @param   $standard_id    施設基準ＩＤ
    // @param   $duty_yyyy  年
    // @param   $duty_mm    月
    //
    // @return  $data_emp   取得情報
    /*     * ********************************************************************** */
    function get_standard_empmst_array($standard_id, $duty_yyyy, $duty_mm) {
        $data_emp = array();

        if ($standard_id == "" || $duty_yyyy == "" || $duty_mm == "") {
            return $data_emp;
        }
        //勤務条件の履歴取得 20120418
        $data_hist = $this->get_empcond_history($standard_id, $duty_yyyy, $duty_mm);

        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        //月またがり、グループ数確認 20120717
        $month_fromto_1 = 0; //前月から当月
        $month_fromto_2 = 0; //当月から翌月
        //施設基準にあうグループ情報を取得
        $sql = "select * from duty_shift_group ";
        $cond = "where standard_id = '$standard_id' and (start_month_flg1 = '0' or month_flg1 = '2')";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $group_num = pg_num_rows($sel);
        for ($i = 0; $i < $group_num; $i++) {
            $start_month_flg1 = pg_fetch_result($sel, $i, "start_month_flg1");
            $month_flg1 = pg_fetch_result($sel, $i, "month_flg1");
            // 前月から当月
            if ($start_month_flg1 == "0") {
                $month_fromto_1++;
            }
            // 当月から翌月
            if ($month_flg1 == "2") {
                $month_fromto_2++;
            }
        }

        //前月も対象（開始日より前にだけデータがある場合）20120717
        $duty_yyyy1 = $duty_yyyy;
        $duty_mm1 = $duty_mm - 1;
        if ($duty_mm1 < 1) {
            $duty_mm1 = 12;
            $duty_yyyy1--;
        }
        //作成期間が月またがりで、終了日より後にだけ登録がある場合、データが取得されない不具合対応 20110802
        $duty_yyyy2 = $duty_yyyy;
        $duty_mm2 = $duty_mm + 1;
        if ($duty_mm2 > 12) {
            $duty_mm2 = 1;
            $duty_yyyy2++;
        }
        $sql = "select distinct on (e.emp_id) e.emp_id, e.emp_lt_nm, e.emp_ft_nm, e.emp_job, e.emp_st, e.emp_join, ";
        $sql .= "e.emp_personal_id,  ";
        $sql .= "b.pattern_id,  ";
        $sql .= "c.group_id as staff_employment_group_id, ";
        $sql .= "c.duty_mon_day_flg, ";
        $sql .= "c.duty_tue_day_flg, ";
        $sql .= "c.duty_wed_day_flg, ";
        $sql .= "c.duty_thurs_day_flg, ";
        $sql .= "c.duty_fri_day_flg, ";
        $sql .= "c.duty_sat_day_flg, ";
        $sql .= "c.duty_sun_day_flg, ";
        $sql .= "c.duty_hol_day_flg, ";
        $sql .= "c.night_duty_flg, ";
        $sql .= "c.night_staff_cnt, ";
        $sql .= "d.wage, d.tmcd_group_id, d.duty_form, ";
        $sql .= "d.other_post_flg, "; // 他部署兼務追加 20120418
        $sql .= "f.st_nm, g.job_nm ";

        $sql .= " from empmst e ";
        $sql .= " inner join duty_shift_plan_staff a on a.emp_id = e.emp_id ";
        $sql .= " inner join duty_shift_group b on b.group_id = a.group_id ";
        $sql .= " left join duty_shift_staff_employment c on c.emp_id = e.emp_id ";
        $sql .= " left join empcond d on d.emp_id = e.emp_id ";
        $sql .= " left join stmst f on f.st_id = e.emp_st ";
        $sql .= " left join jobmst g on g.job_id = e.emp_job ";
        $sql .= " where b.standard_id = '$standard_id' and ((a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm) ";
        // 前月から当月
        if ($month_fromto_1 > 0) {
            //翌月を条件追加
            $sql .= "  or (a.duty_yyyy = $duty_yyyy2 and a.duty_mm = $duty_mm2)   ";
            //翌々月対応 20141117
            $duty_yyyy3 = $duty_yyyy2;
            $duty_mm3 = $duty_mm2 + 1;
            if ($duty_mm3 > 12) {
                $duty_mm3 = 1;
                $duty_yyyy3++;
            }
            $sql .= "  or (a.duty_yyyy = $duty_yyyy3 and a.duty_mm = $duty_mm3)   ";
        }
        // 当月から翌月
        if ($month_fromto_2 > 0) {
            //前月を条件追加
            $sql .= "  or (a.duty_yyyy = $duty_yyyy1 and a.duty_mm = $duty_mm1)  ";
            //翌々月対応 20141117
            $duty_yyyy3 = $duty_yyyy;
            $duty_mm3 = $duty_mm + 1;
            if ($duty_mm3 > 12) {
                $duty_mm3 = 1;
                $duty_yyyy3++;
            }
            $sql .= "  or (a.duty_yyyy = $duty_yyyy3 and a.duty_mm = $duty_mm3)   ";
        }
        //作成期間が当月の場合も、4週計算用に翌月を追加 20150408
        if ($month_fromto_1 == 0 && $month_fromto_2 == 0) {
            //翌月を条件追加
            $sql .= "  or (a.duty_yyyy = $duty_yyyy2 and a.duty_mm = $duty_mm2)   ";
        }
        $sql .= " ) order by e.emp_id  ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        for ($i = 0; $i < $num; $i++) {
            $data_emp[$i]["id"] = pg_fetch_result($sel, $i, "emp_id");        //職員ＩＤ
            $data_emp[$i]["emp_personal_id"] = pg_fetch_result($sel, $i, "emp_personal_id");       //職員ＩＤ 20140411
            $data_emp[$i]["name"] = pg_fetch_result($sel, $i, "emp_lt_nm") . " " . pg_fetch_result($sel, $i, "emp_ft_nm");    //職員名（漢字）
            $data_emp[$i]["job_id"] = pg_fetch_result($sel, $i, "emp_job");       //職種ＩＤ
            $data_emp[$i]["job_name"] = pg_fetch_result($sel, $i, "job_nm");      //職種
            $data_emp[$i]["st_id"] = pg_fetch_result($sel, $i, "emp_st");        //役職ＩＤ
            $data_emp[$i]["st_name"] = pg_fetch_result($sel, $i, "st_nm");     //役職
            $data_emp[$i]["join"] = pg_fetch_result($sel, $i, "emp_join");  //入職日(YYYYMMDD)

            $data_emp[$i]["pattern_id"] = pg_fetch_result($sel, $i, "pattern_id");    //
            $data_emp[$i]["staff_employment_group_id"] = pg_fetch_result($sel, $i, "staff_employment_group_id"); //
            $data_emp[$i]["other_post_flg"] = pg_fetch_result($sel, $i, "other_post_flg");    //
            if ($data_emp[$i]["staff_employment_group_id"] != "") {
                $data_emp[$i]["duty_mon_day_flg"] = pg_fetch_result($sel, $i, "duty_mon_day_flg");  //
                $data_emp[$i]["duty_tue_day_flg"] = pg_fetch_result($sel, $i, "duty_tue_day_flg");  //
                $data_emp[$i]["duty_wed_day_flg"] = pg_fetch_result($sel, $i, "duty_wed_day_flg");  //
                $data_emp[$i]["duty_thurs_day_flg"] = pg_fetch_result($sel, $i, "duty_thurs_day_flg");    //
                $data_emp[$i]["duty_fri_day_flg"] = pg_fetch_result($sel, $i, "duty_fri_day_flg");  //
                $data_emp[$i]["duty_sat_day_flg"] = pg_fetch_result($sel, $i, "duty_sat_day_flg");  //
                $data_emp[$i]["duty_sun_day_flg"] = pg_fetch_result($sel, $i, "duty_sun_day_flg");  //
                $data_emp[$i]["duty_hol_day_flg"] = pg_fetch_result($sel, $i, "duty_hol_day_flg");  //
            } else {
                //デフォルト 1:有り
                $data_emp[$i]["duty_mon_day_flg"] = "1";  //
                $data_emp[$i]["duty_tue_day_flg"] = "1";  //
                $data_emp[$i]["duty_wed_day_flg"] = "1";  //
                $data_emp[$i]["duty_thurs_day_flg"] = "1";  //
                $data_emp[$i]["duty_fri_day_flg"] = "1";  //
                $data_emp[$i]["duty_sat_day_flg"] = "1";  //
                $data_emp[$i]["duty_sun_day_flg"] = "1";  //
                $data_emp[$i]["duty_hol_day_flg"] = "1";  //
            }
            $data_emp[$i]["night_duty_flg"] = pg_fetch_result($sel, $i, "night_duty_flg");    //
            $data_emp[$i]["night_staff_cnt"] = pg_fetch_result($sel, $i, "night_staff_cnt");   //
            $data_emp[$i]["wage"] = pg_fetch_result($sel, $i, "wage");  //
            $data_emp[$i]["tmcd_group_id"] = pg_fetch_result($sel, $i, "tmcd_group_id"); //
            $data_emp[$i]["duty_form"] = pg_fetch_result($sel, $i, "duty_form"); //
            //勤務条件履歴設定 20120418
            $emp_id = $data_emp[$i]["id"];
            if ($data_hist[$emp_id]["duty_form"] != "") {
                $data_emp[$i]["duty_form"] = $data_hist[$emp_id]["duty_form"];
            }
            $wk_data_hist_emp = $data_hist[$emp_id];
            if (array_key_exists("other_post_flg", $wk_data_hist_emp)) {
                $data_emp[$i]["other_post_flg"] = $data_hist[$emp_id]["other_post_flg"];
            }
            if ($data_hist[$emp_id]["job_id"] != "") {
                $data_emp[$i]["job_id"] = $data_hist[$emp_id]["job_id"];
                $data_emp[$i]["job_name"] = $data_hist[$emp_id]["job_name"];
            }
        }

        return $data_emp;
    }

    /*     * ********************************************************************** */

    // 役職ＤＢ(stmst)情報取得
    // @param   なし
    //
    // @return  $data_st    取得情報
    /*     * ********************************************************************** */
    function get_stmst_array() {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select st_id,st_nm from stmst";
        $cond = "order by order_no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);        //役職一覧を取得
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data_st = array();
        for ($i = 0; $i < $num; $i++) {
            $data_st[$i]["id"] = pg_fetch_result($sel, $i, "st_id");
            $data_st[$i]["name"] = pg_fetch_result($sel, $i, "st_nm");
        }

        return $data_st;
    }

    /*     * ********************************************************************** */

    // 職種ＤＢ(jobmst)情報取得
    // @param   なし
    //
    // @return  $data_job   取得情報
    /*     * ********************************************************************** */
    function get_jobmst_array() {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select job_id,job_nm from jobmst";
        $cond = "where job_del_flg = 'f' order by order_no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);        //職種一覧を取得
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data_job = array();
        for ($i = 0; $i < $num; $i++) {
            $data_job[$i]["id"] = pg_fetch_result($sel, $i, "job_id");
            $data_job[$i]["name"] = pg_fetch_result($sel, $i, "job_nm");
        }

        return $data_job;
    }

    //事由情報取得関数
    function get_pattern_reason_array() {

        // 20140128 start
        //一覧の場合、記号件数がある場合にデータ取得する
        if ($this->get_pattern_cnt_check_flg()) {
            $tmgrpchk_flg = true;
            $arr_duty_shift_pattern_cnt = $this->get_arr_duty_shift_pattern_cnt();
        } else {
            $tmgrpchk_flg = false;
            $arr_duty_shift_pattern_cnt = array();
        }
        // 20140128 end
        $group_pattern_reason_array = array();
        //全グループｘパターン取得
        $sql = "select group_id, group_name from wktmgrp";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

        while ($row = pg_fetch_array($sel)) {

            $group_id = $row["group_id"];
            $group_name = $row["group_name"];
            // 20140128 start
            if ($tmgrpchk_flg) {
                if ($arr_duty_shift_pattern_cnt["$group_id"] == "") {
                    continue;
                }
            }
            // 20140128 end

            $pattern_reason_array = array();
            $sql = "select * from atdptn";
            $cond = "WHERE group_id = " . $group_id . " order by atdptn_id";
            $sel2 = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            while ($row2 = pg_fetch_array($sel2)) {
                $pattern_reason_array["{$row2["atdptn_id"]}"] = $row2["reason"];
            }

            //事由
            $group_pattern_reason_array["{$group_id}"] = $pattern_reason_array;
        }
        return $group_pattern_reason_array;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：その他）
//-------------------------------------------------------------------------------------------------------------------------
    /*     * ********************************************************************** */
    // 指定年月日の年数取得
    // @param   $entry_day  年月日（yyyymmdd）
    //
    // @return  $ret        年月（xx年xx月）
    /*     * ********************************************************************** */
    function get_number_of_years($entry_day) {
        //-------------------------------------------------------------------
        //勤務年数算出
        //-------------------------------------------------------------------
        $ret = "入職日未定";
        if (trim($entry_day) != "") {
            $wk_yyyy = substr($entry_day, 0, 4);
            $wk_mm = substr($entry_day, 4, 2);
            $wk_dd = substr($entry_day, 6, 2);
            //現在日付の取得
            $date = getdate();
            $now_yyyy = $date["year"];
            $now_mm = $date["mon"];
            $now_dd = $date["mday"];
            $now_date = sprintf("%04d%02d%02d", $now_yyyy, $now_mm, $now_dd);
            //勤務年数
            if ((int) $entry_day <= (int) $now_date) {
                if ($now_mm < $wk_mm) {
                    $now_yyyy = $now_yyyy - 1;
                    $now_mm = $now_mm + 12;
                }
                $ret = ($now_yyyy - $wk_yyyy) . "年" . ($now_mm - $wk_mm) . "ヶ月";
            } else {
                $ret = "0年0ヶ月";
            }
        }
        return $ret;
    }

    /*     * ********************************************************************** */

    // 休暇の事由設定
    // @param   $data               勤務パターン情報
    // @param   $pattern_id         勤務グループＩＤ
    // @param   $atdptn_ptn_id      勤務パターンＩＤ
    // @param   $atdptn_ptn_name    勤務パターン名
    //
    // @return  $data   勤務パターン情報
    /*     * ********************************************************************** */
    function setReasonPatternArray($data, $pattern_id, $atdptn_ptn_id, $atdptn_ptn_name, $count_kbn_gyo, $count_kbn_retu, $count_kbn_gyo_name, $count_kbn_retu_name, $font_name, $font_color_id, $back_color_id) {
        $m = count($data);
        // 既に追加されたことがあるか確認
        $exist_flg = false;
        $exist_cnt = 0; //以前に追加された件数
        for ($i = 0; $i < $m; $i++) {
            if (trim($data[$i]["reason"]) != "") {
                $exist_flg = true;
                $exist_cnt++;
            }
        }
// debug
//echo("exist_cnt=".$exist_cnt);
        // 休暇事由の件数、追加がある場合は変更すること
        $holiday_reason_cnt = 14;
        // 事由の追加作業が何回か行われているため、旧データを移動
        // 開始位置と終了位置を調整
        if ($exist_flg) {
            /*
              if ($exist_cnt == 5) {
              $start_pos = $m - 5;
              $end_pos = $m + 7;  //年休追加のため6->7
              } else {
              $start_pos = $m - 11;
              $end_pos = $m + 1;
              }
             */
            // 開始位置 = 全体件数 - DBへの追加済み休暇事由数
            $start_pos = $m - $exist_cnt;
            // 終了位置 = 全体件数 + (休暇事由の件数 - DBへの追加済み休暇事由数)
            $end_pos = $m + ($holiday_reason_cnt - $exist_cnt);
        } else {
            $start_pos = $m;
//          $end_pos = $m + 12; //年休追加のため11->12
            $end_pos = $m + $holiday_reason_cnt;
        }
//echo("start_pos=$start_pos $end_pos");
        // 位置をシフト
        if ($exist_flg) {
            // 旧情報の事由の位置
            $arr_data_idx = array();
            for ($i = $start_pos; $i < $m; $i++) {
                $tmp_reason = $data[$i]["reason"];
                $arr_data_idx[$tmp_reason] = $i;
            }
            // 後ろから移動
            // 事由の移動位置を配列情報にする、年休追加(37)
            // 公休,法定休暇,所定休暇,有給休暇,年休,代替休暇,振替休暇,特別休暇,一般欠勤, 病傷欠勤,リフレッシュ休暇, 初盆休暇, その他休
            $arr_shift_reason = array("34", "24", "22", "23", "1", "37", "4", "17", "5", "6", "7", "40", "41", "8");
            for ($i = $end_pos; $i >= $start_pos; $i--) {
                // 移動先の位置
                $tmp_idx = $i - $start_pos;
                $tmp_reason = $arr_shift_reason[$tmp_idx];
                if ($tmp_reason != "") {
                    // 事由から旧情報の位置取得
                    $src_idx = $arr_data_idx[$tmp_reason];
                    // 元が設定済みの場合、元と先が同じなら不要
                    if ($data[$src_idx]["reason"] != "" && $data[$src_idx]["reason"] != $data[$i]["reason"]) {
                        $data[$i]["reason"] = $data[$src_idx]["reason"];
                        $data[$src_idx]["reason"] = "";
                        $data[$i]["reason_name"] = $data[$src_idx]["reason_name"];
                        $data[$i]["pattern_id"] = $data[$src_idx]["pattern_id"];                //出勤グループＩＤ
                        $data[$i]["atdptn_ptn_id"] = $data[$src_idx]["atdptn_ptn_id"];      //出勤パターンＩＤ
                        $data[$i]["count_kbn_gyo"] = $data[$src_idx]["count_kbn_gyo"];      //集計区分(行)
                        $data[$i]["count_kbn_retu"] = $data[$src_idx]["count_kbn_retu"];        //集計区分(列)
                        $data[$i]["font_name"] = $data[$src_idx]["font_name"];              //表示文字
//echo("<br>$i $src_idx font_name".$data[$src_idx]["font_name"]); //debug
                        $data[$i]["font_color_id"] = $data[$src_idx]["font_color_id"];      //文字色
                        $data[$i]["back_color_id"] = $data[$src_idx]["back_color_id"];      //背景色
                        //-------------------------------------------------------------------
                        //設定（名称）
                        //-------------------------------------------------------------------
                        $data[$i]["atdptn_ptn_name"] = $data[$src_idx]["atdptn_ptn_name"];          //出勤パターン名
                        $data[$i]["count_kbn_gyo_name"] = $data[$src_idx]["count_kbn_gyo_name"];        //行名
                        $data[$i]["count_kbn_retu_name"] = $data[$src_idx]["count_kbn_retu_name"];  //列名
                    }
                }
            }
        }
        for ($i = $start_pos; $i < $end_pos; $i++) {

//      for($i=$m; $i<($m + 11); $i++) {
            //-------------------------------------------------------------------
            //設定
            //-------------------------------------------------------------------
            //新規の場合設定、既存は旧情報設定済み
            if ($exist_flg && $data[$i]["reason"] != "") {
                continue;
            }
            $data[$i]["pattern_id"] = $pattern_id;              //出勤グループＩＤ
            $data[$i]["atdptn_ptn_id"] = $atdptn_ptn_id;        //出勤パターンＩＤ
            $data[$i]["count_kbn_gyo"] = $count_kbn_gyo;        //集計区分(行)
            $data[$i]["count_kbn_retu"] = $count_kbn_retu;      //集計区分(列)
            $data[$i]["font_name"] = $font_name;                //表示文字
            $data[$i]["font_color_id"] = $font_color_id;        //文字色
            $data[$i]["back_color_id"] = $back_color_id;        //背景色
            //-------------------------------------------------------------------
            //設定（名称）
            //-------------------------------------------------------------------
            $data[$i]["atdptn_ptn_name"] = $atdptn_ptn_name;            //出勤パターン名
            $data[$i]["count_kbn_gyo_name"] = $count_kbn_gyo_name;      //行名
            $data[$i]["count_kbn_retu_name"] = $count_kbn_retu_name;    //列名
            //-------------------------------------------------------------------
            //出勤パターン値設定
            //$ret["34"] = ""（事由無し）;
            //$ret["24"] = "公休";
            //$ret["22"] = "法定休暇";
            //$ret["23"] = "所定休暇";
            //$ret["1"] = "有給休暇";
            //$ret["37"] = "年休";
            //$ret["4"] = "代替休暇";
            //$ret["17"] = "振替休暇";
            //$ret["5"] = "特別休暇";
            //$ret["6"] = "欠勤（一般欠勤）";
            //$ret["7"] = "病欠（病傷欠勤）";
            //$ret["40"] = "リフレ（リフレッシュ休暇）";
            //$ret["41"] = "初盆";
            //$ret["8"] = "その他休";
            //-------------------------------------------------------------------
            if ($i == $start_pos) {
                $data[$i]["reason"] = "34";             //事由
                $data[$i]["reason_name"] = "";          //事由名称（事由無し）
            }
            if ($i == ($start_pos + 1)) {
                $data[$i]["reason"] = "24";             //事由
                $data[$i]["reason_name"] = "公休";      //事由名称
            }
            if ($i == ($start_pos + 2)) {
                $data[$i]["reason"] = "22";             //事由
                $data[$i]["reason_name"] = "法定";      //事由名称
            }
            if ($i == ($start_pos + 3)) {
                $data[$i]["reason"] = "23";             //事由
                $data[$i]["reason_name"] = "所定";      //事由名称
            }
            if ($i == ($start_pos + 4)) {
                $data[$i]["reason"] = "1";              //事由
                $data[$i]["reason_name"] = "有給";      //事由名称
            }
            if ($i == ($start_pos + 5)) {
                $data[$i]["reason"] = "37";             //事由
                $data[$i]["reason_name"] = "年休";      //事由名称
            }
            if ($i == ($start_pos + 6)) {
                $data[$i]["reason"] = "4";              //事由
                $data[$i]["reason_name"] = "代替";      //事由名称
            }
            if ($i == ($start_pos + 7)) {
                $data[$i]["reason"] = "17";             //事由
                $data[$i]["reason_name"] = "振替";      //事由名称
            }
            if ($i == ($start_pos + 8)) {
                $data[$i]["reason"] = "5";              //事由
                $data[$i]["reason_name"] = "特別";      //事由名称
            }
            if ($i == ($start_pos + 9)) {
                $data[$i]["reason"] = "6";              //事由
                $data[$i]["reason_name"] = "欠勤";      //事由名称
            }
            if ($i == ($start_pos + 10)) {
                $data[$i]["reason"] = "7";              //事由
                $data[$i]["reason_name"] = "病欠";      //事由名称
            }
            if ($i == ($start_pos + 11)) {
                $data[$i]["reason"] = "40";             //事由
                $data[$i]["reason_name"] = "リフレ";    //事由名称
            }
            if ($i == ($start_pos + 12)) {
                $data[$i]["reason"] = "41";             //事由
                $data[$i]["reason_name"] = "初盆";      //事由名称
            }
            if ($i == ($start_pos + 13)) {
                $data[$i]["reason"] = "8";              //事由
                $data[$i]["reason_name"] = "その他";    //事由名称
            }
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 休暇の事由設定、追加事由対応
    // @param   $data               勤務パターン情報
    // @param   $pattern_id         勤務グループＩＤ
    // @param   $atdptn_ptn_id      勤務パターンＩＤ
    // @param   $atdptn_ptn_name    勤務パターン名
    //
    // @return  $data   勤務パターン情報
    /*     * ********************************************************************** */
    function setReasonPatternArray2($data, $pattern_id, $atdptn_ptn_id, $atdptn_ptn_name, $count_kbn_gyo, $count_kbn_retu, $count_kbn_gyo_name, $count_kbn_retu_name, $font_name, $font_color_id, $back_color_id) {

        // 件数確認
        $add_cnt1 = 0;  //半有半公等
        $add_cnt2 = 0;  //産休等
        $add_cnt3 = 0;  //年休
        $add_cnt4 = 0;  //リフレ等
        $add_cnt5 = 0;  //年末年始
        $add_cnt6 = 0;  //半有半欠等
        $add_cnt7 = 0;  //公外出等 青森県中様用対応 20130328
        $add_cnt8 = 0;  //代替出勤、振替出勤、交通遅延追加 20141031
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]["reason"] >= "44" && $data[$i]["reason"] <= "47") {
                $add_cnt1++;
            }
            if ($data[$i]["reason"] >= "25" && $data[$i]["reason"] <= "32") {
                $add_cnt2++;
            }
            if ($data[$i]["reason"] == "37") {
                $add_cnt3++;
            }
            if ($data[$i]["reason"] >= "40" && $data[$i]["reason"] <= "41") {
                $add_cnt4++;
            }
            if ($data[$i]["reason"] == "61") {
                $add_cnt5++;
            }
            if (($data[$i]["reason"] >= "54" && $data[$i]["reason"] <= "60") ||
                    ($data[$i]["reason"] >= "62" && $data[$i]["reason"] <= "64") ||
                    $data[$i]["reason"] == "72") {
                $add_cnt6++;
            }
            //追加 20141031 (青森県中様用、テスト環境にもあるかもしれないため分けて処理)
            if ($data[$i]["reason"] == "33" || $data[$i]["reason"] == "9" || $data[$i]["reason"] == "10" || $data[$i]["reason"] == "12" || $data[$i]["reason"] == "13"
            ) {
                $add_cnt7++;
            }
            //追加 20141031
            if ($data[$i]["reason"] == "11" //追加 20141031
                    || $data[$i]["reason"] == "14" //追加 20141031
                    || $data[$i]["reason"] == "15" //追加 20141031
            ) {
                $add_cnt8++;
            }
        }

        // atdptn
        // atdbk_reason_mst
        // 表示フラグ
        $sql = "select reason_id, display_flag, display_name from atdbk_reason_mst";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);        //取得
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //情報設定
        $arr_disp_flg = array();
        $arr_disp_name = array(); //20130123
        for ($i = 0; $i < $num; $i++) {
            $tmp_reason_id = pg_fetch_result($sel, $i, "reason_id");
            $arr_disp_flg["$tmp_reason_id"] = pg_fetch_result($sel, $i, "display_flag");
            $arr_disp_name["$tmp_reason_id"] = pg_fetch_result($sel, $i, "display_name"); //20130123
        }

        $data_new = array();
        // 追加位置確認
        $wk_pos1 = count($data) - 1;
        for ($i = count($data) - 1; $i >= 0; $i--) {
            if ($data[$i]["reason"] == "1" || //有休か年休
                    $data[$i]["reason"] == "37") {
                $wk_pos1 = $i;
                break;
            }
        }

        $wk_pos2 = count($data) - 1;
        for ($i = count($data) - 1; $i >= 0; $i--) {
            if ($data[$i]["reason"] == "8") {   //その他
                $wk_pos2 = $i;
                break;
            }
        }
        $wk_pos3 = count($data) - 1;
        for ($i = count($data) - 1; $i >= 0; $i--) {
            if ($data[$i]["reason"] == "1") {   //有休
                $wk_pos3 = $i;
                break;
            }
        }
        $wk_pos4 = count($data) - 1;
        for ($i = count($data) - 1; $i >= 0; $i--) {
            if ($data[$i]["reason"] == "7") {   //病欠
                $wk_pos4 = $i;
                break;
            }
        }
        $wk_pos5 = count($data) - 1;
        // 不足分を追加
        $add_idx = 0;
        for ($i = 0; $i < count($data); $i++) {
            // 年休
            if ($add_cnt3 == 0 &&
                    $i == $wk_pos3 + 1) {
                $data_new[$add_idx]["reason"] = "37";
                $data_new[$add_idx]["reason_name"] = "年休";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }
            // 半有半公等
            if ($add_cnt1 == 0 &&
                    $i == $wk_pos1 + 1) {
                $data_new[$add_idx]["reason"] = "44";
                $data_new[$add_idx]["reason_name"] = "半有半公";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "45";
                $data_new[$add_idx]["reason_name"] = "希望(公休)";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "46";
                $data_new[$add_idx]["reason_name"] = "待機(公休)";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "47";
                $data_new[$add_idx]["reason_name"] = "管理当直前(公休)";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }
            // リフレ等
            if ($add_cnt4 == 0 &&
                    $i == $wk_pos4 + 1) {
                $data_new[$add_idx]["reason"] = "40";
                $data_new[$add_idx]["reason_name"] = "リフレ";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "41";
                $data_new[$add_idx]["reason_name"] = "初盆";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }
            // 産休等
            if ($add_cnt2 == 0 &&
                    $i == $wk_pos2) {
                $data_new[$add_idx]["reason"] = "25";
                $data_new[$add_idx]["reason_name"] = "産休";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "26";
                $data_new[$add_idx]["reason_name"] = "育児";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "27";
                $data_new[$add_idx]["reason_name"] = "介護";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "28";
                $data_new[$add_idx]["reason_name"] = "傷病";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "29";
                $data_new[$add_idx]["reason_name"] = "学業";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "30";
                $data_new[$add_idx]["reason_name"] = "忌引";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "31";
                $data_new[$add_idx]["reason_name"] = "夏休";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "32";
                $data_new[$add_idx]["reason_name"] = "結婚";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }

            // 年末年始
            if ($add_cnt5 == 0 &&
                    $i == $wk_pos5) {
                $data_new[$add_idx]["reason"] = "61";
                $data_new[$add_idx]["reason_name"] = "年末年始";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }
            // 半有半欠等
            if ($add_cnt6 == 0 &&
                    $i == $wk_pos2) {
                $data_new[$add_idx]["reason"] = "57";
                $data_new[$add_idx]["reason_name"] = "半有半欠";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "58";
                $data_new[$add_idx]["reason_name"] = "半特半有";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "59";
                $data_new[$add_idx]["reason_name"] = "半特半公";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "60";
                $data_new[$add_idx]["reason_name"] = "半特半欠";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "54";
                $data_new[$add_idx]["reason_name"] = "半夏半公";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "55";
                $data_new[$add_idx]["reason_name"] = "半夏半有";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "56";
                $data_new[$add_idx]["reason_name"] = "半夏半欠";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "72";
                $data_new[$add_idx]["reason_name"] = "半夏半特";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "62";
                $data_new[$add_idx]["reason_name"] = "半正半有";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "63";
                $data_new[$add_idx]["reason_name"] = "半正半公";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "64";
                $data_new[$add_idx]["reason_name"] = "半正半欠";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }
            //追加 20141031
            if ($add_cnt7 == 0 &&
                    $i == $wk_pos2) {
                $data_new[$add_idx]["reason"] = "14";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["14"] != "") ? $arr_disp_name["14"] : "代替出勤";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "15";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["15"] != "") ? $arr_disp_name["15"] : "振替出勤";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "33";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["33"] != "") ? $arr_disp_name["33"] : "公外出";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "9";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["9"] != "") ? $arr_disp_name["9"] : "通院";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "10";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["10"] != "") ? $arr_disp_name["10"] : "私用";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "11";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["11"] != "") ? $arr_disp_name["11"] : "交通遅延";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "12";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["12"] != "") ? $arr_disp_name["12"] : "遅刻";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "13";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["13"] != "") ? $arr_disp_name["13"] : "早退";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }
            //追加 20141031
            if ($add_cnt7 > 0 && $add_cnt8 == 0 &&
                    $i == $wk_pos2) {
                $data_new[$add_idx]["reason"] = "14";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["14"] != "") ? $arr_disp_name["14"] : "代替出勤";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "15";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["15"] != "") ? $arr_disp_name["15"] : "振替出勤";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
                $data_new[$add_idx]["reason"] = "11";
                $data_new[$add_idx]["reason_name"] = ($arr_disp_name["11"] != "") ? $arr_disp_name["11"] : "交通遅延";
                $data_new[$add_idx]["pattern_id"] = $pattern_id;
                $data_new[$add_idx]["atdptn_ptn_id"] = "10";
                $data_new[$add_idx]["atdptn_ptn_name"] = $atdptn_ptn_name;
                $add_idx++;
            }
            $data_new[$add_idx] = $data[$i];
            $add_idx++;
        }

        return $data_new;
    }

    /*     * ********************************************************************** */

    // 小数点以下の後ろの0を除く、全部除かれる場合は.ドットも除く
    // @param   $str                数字文字列
    //
    // @return                      後ろの0を除いた文字列
    /*     * ********************************************************************** */

    function rtrim_zero($str) {

        $arr_str = explode("\.", $str);
        if (count($arr_str) != 2) {
            return($str);
        }

        $tmp_str = $arr_str[0];
        $dec_str = rtrim($arr_str[1], "0\.");
        if ($dec_str != "") {
            $tmp_str .= "." . $dec_str;
        }
        return $tmp_str;
    }

    /*     * ********************************************************************** */

    // 有効な勤務シフトグループを取得する
    // 　責任者、代行者、職員設定、応援のグループ
    // 　年月が省略されている場合は応援を除く
    // @param   $group_array        全体のグループ
    // @param   $emp_id             ログイン職員
    // @param   $duty_yyyy          年（省略可）
    // @param   $duty_mm            月（省略可）
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_valid_group_array($group_array, $emp_id, $duty_yyyy, $duty_mm) {
        $ret_group_array = array();
        /* 管理者のみとする 20140210
          // 職員設定
          $sql = "select group_id from duty_shift_staff ";
          $cond = "where emp_id = '$emp_id'";
          $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
          if ($sel == 0) {
          pg_close($this->_db_con);
          echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
          echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
          exit;
          }
          $num = pg_num_rows($sel);
          if ($num > 0) {
          $belong_group_id = pg_fetch_result($sel,0,"group_id");
          }

          $arr_plan_group_id = array();
          // 年月が設定されている場合
          if ($duty_yyyy != "" && $duty_mm != "") {
          // 応援
          $sql = "select group_id from duty_shift_plan_staff ";
          $cond = "where emp_id = '$emp_id' and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm";
          $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
          if ($sel == 0) {
          pg_close($this->_db_con);
          echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
          echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
          exit;
          }
          $num = pg_num_rows($sel);
          for ($i=0; $i < $num; $i++) {
          $arr_plan_group_id[] = pg_fetch_result($sel,$i,"group_id");
          }
          }
         */
        // 責任者、代行者 //20140210 職員設定、応援確認を除く
        $num = count($group_array);
        for ($i = 0; $i < $num; $i++) {
            if ($emp_id == $group_array[$i]["person_charge_id"] ||
                    $emp_id == $group_array[$i]["caretaker_id"] ||
                    $emp_id == $group_array[$i]["caretaker2_id"] ||
                    $emp_id == $group_array[$i]["caretaker3_id"] ||
                    $emp_id == $group_array[$i]["caretaker4_id"] ||
                    $emp_id == $group_array[$i]["caretaker5_id"] ||
                    $emp_id == $group_array[$i]["caretaker6_id"] ||
                    $emp_id == $group_array[$i]["caretaker7_id"] ||
                    $emp_id == $group_array[$i]["caretaker8_id"] ||
                    $emp_id == $group_array[$i]["caretaker9_id"] ||
                    $emp_id == $group_array[$i]["caretaker10_id"] /* || 20140210
              $group_array[$i]["group_id"] == $belong_group_id ||
              in_array($group_array[$i]["group_id"], $arr_plan_group_id) */) {
                $ret_group_array[] = $group_array[$i];
            }
        }

        return $ret_group_array;
    }

    /*     * ********************************************************************** */

    // 勤務日誌用に有効な勤務シフトグループを取得する
    // 　責任者、代行者、職員設定のグループ
    // @param   $group_array        全体のグループ
    // @param   $emp_id             ログイン職員
    // @param   $sfc_group_code_flg 連携用病棟コード確認フラグ(省略可) "1":連携用病棟コードが入っていることを条件に追加する
    //
    // @return  $data   取得情報（各データの"create_flg"に1を設定済）
    /*     * ********************************************************************** */
    function get_valid_group_array_for_diary($group_array, $emp_id, $sfc_group_code_flg = "") {
        $ret_group_array = array();
        // 職員設定
        $sql = "select group_id from duty_shift_staff ";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $belong_group_id = "";
        $num = pg_num_rows($sel);
        if ($num > 0) {
            $belong_group_id = pg_fetch_result($sel, 0, "group_id");
        }

        // 責任者、代行者、職員設定
        $num = count($group_array);
        for ($i = 0; $i < $num; $i++) {
            //連携用病棟コード確認
            if ($sfc_group_code_flg == "1") {
                if ($group_array[$i]["sfc_group_code"] == "") {
                    continue;
                }
            }
            if ($emp_id == $group_array[$i]["person_charge_id"] ||
                    $emp_id == $group_array[$i]["caretaker_id"] ||
                    $emp_id == $group_array[$i]["caretaker2_id"] ||
                    $emp_id == $group_array[$i]["caretaker3_id"] ||
                    $emp_id == $group_array[$i]["caretaker4_id"] ||
                    $emp_id == $group_array[$i]["caretaker5_id"] ||
                    $emp_id == $group_array[$i]["caretaker6_id"] ||
                    $emp_id == $group_array[$i]["caretaker7_id"] ||
                    $emp_id == $group_array[$i]["caretaker8_id"] ||
                    $emp_id == $group_array[$i]["caretaker9_id"] ||
                    $emp_id == $group_array[$i]["caretaker10_id"] ||
                    $group_array[$i]["group_id"] == $belong_group_id) {
                $group_array[$i]["create_flg"] = "1";
                $ret_group_array[] = $group_array[$i];
            }
        }

        return $ret_group_array;
    }

    /*     * ********************************************************************** */

    // 勤務シフ作成 登録子画面情報を取得する
    // @param   $emp_id             ログイン職員
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_last_ptn($emp_id) {
        $ret_ptn_array = array();

        // 前回指定した勤務シフト、事由を取得
        $sql = "select last_ptn_id, last_reason_2, last_copy_cnt from duty_shift_staff_option ";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        if ($num > 0) {
            $ret_ptn_array["last_ptn_id"] = pg_fetch_result($sel, 0, "last_ptn_id");
            $ret_ptn_array["last_reason_2"] = pg_fetch_result($sel, 0, "last_reason_2");
            $ret_ptn_array["last_copy_cnt"] = pg_fetch_result($sel, 0, "last_copy_cnt");
        }

        return $ret_ptn_array;
    }

    /*     * ********************************************************************** */

    //
    // 勤務シフ作成 登録子画面情報更新
    //
    // @param   $emp_id             ログイン職員
    // @param   $last_ptn_id        勤務シフト
    // @param   $last_reason_2      事由
    /*     * ********************************************************************** */
    function update_last_ptn($emp_id, //職員ID
            $last_ptn_id, //勤務シフト
            $last_reason_2, //事由
            $last_copy_cnt  //連続回数
    ) {

        if ($last_ptn_id == "" && $last_reason_2 == "") {
            return;
        }

        // データが存在するかチェック
        $sql = "select last_ptn_id, last_reason_2, last_copy_cnt from duty_shift_staff_option";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        if ($num > 0) {
            // 値が同じ場合、更新しない
            $wk_ptn_id = pg_fetch_result($sel, 0, "last_ptn_id");
            $wk_reason_2 = pg_fetch_result($sel, 0, "last_reason_2");
            $wk_copy_cnt = pg_fetch_result($sel, 0, "last_copy_cnt");

            if ($last_ptn_id != $wk_ptn_id || $last_reason_2 != $wk_reason_2 || $last_copy_cnt != $wk_copy_cnt) {
                $sql = "update duty_shift_staff_option set";
                $set = array("last_ptn_id", "last_reason_2", "last_copy_cnt");
                $setvalue = array($last_ptn_id, $last_reason_2, $last_copy_cnt);
                $cond = "where emp_id = '$emp_id'";
                $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
                if ($upd == 0) {
                    pg_query($this->_db_con, "rollback");
                    pg_close($this->_db_con);
                    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                    echo("<script language='javascript'>showErrorPage(window);</script>");
                    exit;
                }
            }
        } else {
            // 存在しない場合、ＩＮＳＥＲＴ
            $sql = "insert into duty_shift_staff_option (emp_id, last_ptn_id, last_reason_2, last_copy_cnt ";
            $sql .= ") values (";
            // シフトグループ $pattern_id -> $$ptn_id 2008/06/09
            $content = array($emp_id, $last_ptn_id, $last_reason_2, $last_copy_cnt);
            $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
            if ($ins == 0) {
                pg_query($this->_db_con, "rollback");
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }

    /*     * ********************************************************************** */

    // シフトデータ指定範囲情報取得
    // @return  $data       取得情報
    /*     * ********************************************************************** */
    function get_atdbk_data($db_name, $calendar_array, $plan_array) {

        $cond_add = "";
        for ($i = 0; $i < count($plan_array); $i++) {
            $wk_id = $plan_array[$i]["staff_id"];
            if ($cond_add == "") {
                $cond_add .= "and (a.emp_id = '$wk_id' ";
            } else {
                $cond_add .= "or a.emp_id = '$wk_id' ";
            }
        }
        if ($cond_add != "") {
            $cond_add .= ") ";
        }

        $data = $this->get_atdbk_atdbkrslt_array($db_name, $cond_add, $calendar_array);
        return $data;
    }

    /*     * ********************************************************************** */

    // 勤務分担表情報取得
    // @param   $pattern_id         出勤グループＩＤ
    // @param   $data_atdptn        出勤パターン情報
    //
    // @return  $data   取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_allotment_array($pattern_id, $data_atdptn) {

        $data = array();

        if ($pattern_id == "") {
            return $data;
        }

        $sql = "select * from duty_shift_pattern";
        $cond = "where pattern_id = $pattern_id and atdptn_ptn_id != 10 ";
        $cond .= " order by allotment_order_no, order_no ";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);

        for ($i = 0; $i < $num; $i++) {
            //-------------------------------------------------------------------
            //ＤＢよりの取得値
            //-------------------------------------------------------------------
            $data[$i]["pattern_id"] = trim(pg_fetch_result($sel, $i, "pattern_id"));          //出勤グループＩＤ
            $data[$i]["atdptn_ptn_id"] = trim(pg_fetch_result($sel, $i, "atdptn_ptn_id"));        //出勤パターンＩＤ
            //-------------------------------------------------------------------
            //各名称設定
            //-------------------------------------------------------------------
            for ($k = 0; $k < count($data_atdptn); $k++) {
                //出勤パターン名
                if (($data[$i]["pattern_id"] == $data_atdptn[$k]["group_id"]) &&
                        ($data[$i]["atdptn_ptn_id"] == $data_atdptn[$k]["id"])) {
                    $data[$i]["atdptn_ptn_name"] = $data_atdptn[$k]["name"];
                    break;
                }
            }
        }
        return $data;
    }

    /*     * ********************************************************************** */

    // 自動作成時のデフォルトパターン情報取得
    // @param   $pattern_id 勤務パターングループＩＤ
    //
    // @return  $data           取得情報
    /*     * ********************************************************************** */
    function get_auto_default_ptn_info($pattern_id) {

        //未設定時SQLエラーとなる不具合対応 20091019
        $data = array();
        if ($pattern_id == "") {
            return $data;
        }
        //-------------------------------------------------------------------
        //ＳＱＬ
        //-------------------------------------------------------------------
        $sql = "select atdptn_ptn_id, reason from duty_shift_pattern ";
        $cond = "where pattern_id = $pattern_id ";
        $cond .= "order by order_no, atdptn_ptn_id limit 1";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $num = pg_num_rows($sel);
        if ($num > 0) {
            $data["atdptn_ptn_id"] = pg_fetch_result($sel, 0, "atdptn_ptn_id");
            $data["reason"] = pg_fetch_result($sel, 0, "reason");
        }

        return $data;
    }

    //休暇集計用情報
    function get_hol_add_info() {

//事由により0.5日を2箇所に集計する情報を持つ
        //追加された場合はget_hol_cnt_array()も修正する
        $add_info = array();
        $add_info[1] = array("add_reason" => 1, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0);   //有休
        $add_info[2] = array("add_reason" => 1, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0); //午前有休
        $add_info[3] = array("add_reason" => 1, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0); //午後有休
        $add_info[37] = array("add_reason" => 1, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0);  //年休
        $add_info[38] = array("add_reason" => 1, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午前年休
        $add_info[39] = array("add_reason" => 1, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午後年休
        $add_info[57] = array("add_reason" => 1, "add_day" => 0.5, "add_reason2" => 6, "add_day2" => 0.5);  //半有半欠
        $add_info[24] = array("add_reason" => 24, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //公休
        $add_info[35] = array("add_reason" => 24, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午前公休
        $add_info[36] = array("add_reason" => 24, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午後公休

        $add_info[44] = array("add_reason" => 1, "add_day" => 0.5, "add_reason2" => 24, "add_day2" => 0.5); //半有半公
        $add_info[45] = array("add_reason" => 24, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //希望(公休)
        $add_info[46] = array("add_reason" => 24, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //待機(公休)
        $add_info[47] = array("add_reason" => 24, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //管理当直前(公休)
        $add_info[4] = array("add_reason" => 4, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0);   //代替休暇
        $add_info[18] = array("add_reason" => 4, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //半前代替休
        $add_info[19] = array("add_reason" => 4, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //半後代替休
        $add_info[17] = array("add_reason" => 17, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //振替休暇
        $add_info[20] = array("add_reason" => 17, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //半前振替休
        $add_info[21] = array("add_reason" => 17, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //半後振替休
        $add_info[5] = array("add_reason" => 5, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0);   //特別休暇
        $add_info[67] = array("add_reason" => 5, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午前特別
        $add_info[68] = array("add_reason" => 5, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午後特別

        $add_info[58] = array("add_reason" => 5, "add_day" => 0.5, "add_reason2" => 1, "add_day2" => 0.5);  //半特半有
        $add_info[59] = array("add_reason" => 5, "add_day" => 0.5, "add_reason2" => 24, "add_day2" => 0.5); //半特半公
        $add_info[60] = array("add_reason" => 5, "add_day" => 0.5, "add_reason2" => 6, "add_day2" => 0.5);  //半特半欠

        $add_info[6] = array("add_reason" => 6, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0);   //一般欠勤
        $add_info[48] = array("add_reason" => 6, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午前欠勤
        $add_info[49] = array("add_reason" => 6, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午後欠勤
        $add_info[7] = array("add_reason" => 7, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0);   //病傷欠勤
        $add_info[25] = array("add_reason" => 25, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //産休
        $add_info[26] = array("add_reason" => 26, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //育児休業
        $add_info[27] = array("add_reason" => 27, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //介護休業
        $add_info[28] = array("add_reason" => 28, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //傷病休職
        $add_info[29] = array("add_reason" => 29, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //学業休職
        $add_info[30] = array("add_reason" => 30, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //忌引
        $add_info[31] = array("add_reason" => 31, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //夏休
        $add_info[50] = array("add_reason" => 31, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午前夏休
        $add_info[51] = array("add_reason" => 31, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午後夏休
        $add_info[54] = array("add_reason" => 31, "add_day" => 0.5, "add_reason2" => 24, "add_day2" => 0.5);    //半夏半公
        $add_info[55] = array("add_reason" => 31, "add_day" => 0.5, "add_reason2" => 1, "add_day2" => 0.5); //半夏半有
        $add_info[56] = array("add_reason" => 31, "add_day" => 0.5, "add_reason2" => 6, "add_day2" => 0.5); //半夏半欠
        $add_info[72] = array("add_reason" => 31, "add_day" => 0.5, "add_reason2" => 5, "add_day2" => 0.5); //半夏半特
        $add_info[61] = array("add_reason" => 61, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //年末年始
        $add_info[69] = array("add_reason" => 61, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午前正休
        $add_info[70] = array("add_reason" => 61, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午後正休
        $add_info[62] = array("add_reason" => 61, "add_day" => 0.5, "add_reason2" => 1, "add_day2" => 0.5); //半正半有
        $add_info[63] = array("add_reason" => 61, "add_day" => 0.5, "add_reason2" => 24, "add_day2" => 0.5);    //半正半公
        $add_info[64] = array("add_reason" => 61, "add_day" => 0.5, "add_reason2" => 6, "add_day2" => 0.5); //半正半欠
        $add_info[32] = array("add_reason" => 32, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //結婚休
        $add_info[40] = array("add_reason" => 40, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //リフレッシュ休暇
        $add_info[42] = array("add_reason" => 40, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午前リフレッシュ
        $add_info[43] = array("add_reason" => 40, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);   //午後リフレッシュ
        $add_info[41] = array("add_reason" => 41, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //初盆休暇
        $add_info[8] = array("add_reason" => 8, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0);   //その他休
        $add_info[52] = array("add_reason" => 8, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午前その他
        $add_info[53] = array("add_reason" => 8, "add_day" => 0.5, "add_reason2" => 0, "add_day2" => 0);    //午後その他
        $add_info[22] = array("add_reason" => 22, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //法定休暇
        $add_info[23] = array("add_reason" => 23, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //所定休暇

        $add_info[34] = array("add_reason" => 34, "add_day" => 1, "add_reason2" => 0, "add_day2" => 0); //事由無し休暇

        return $add_info;
    }

    /**
     * 職員設定から追加された職員を取得する
     *
     * @param mixed $group_id グループID
     * @param mixed $staff_id 登録済職員IDの配列
     * @param mixed $add_staff_id_all 追加中の職員IDのカンマ区切り文字列
     * @return mixed 追加された職員IDのカンマ区切り文字列
     *
     */
    function get_add_staffset($group_id, $staff_id, $add_staff_id_all) {
        //グループの職員を取得
        $data_staff = $this->get_duty_shift_staff_array($group_id, "", "", "");
        //画面の職員
        $arr_all = explode(",", $add_staff_id_all);
        $arr_staff_id = array_merge($staff_id, $arr_all);

        $ret_str = "";
        for ($i = 0; $i < count($data_staff); $i++) {
            $wk_staff_id = $data_staff[$i]["id"];
            if (!in_array($wk_staff_id, $arr_staff_id)) {
                if ($ret_str != "") {
                    $ret_str .= ",";
                }
                $ret_str .= $wk_staff_id;
            }
        }
        return $ret_str;
    }

    /*     * ************************************************************************************************************************ */
    /*     * ************************************************************************************************************************ */
    /*     * ************************************************************************************************************************ */

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（show_select_values.iniより転用）
//-------------------------------------------------------------------------------------------------------------------------
    /*     * ********************************************************************** */
    // 時表示（0〜23)
    /*     * ********************************************************************** */
    function show_hour_options_0_23($id, $hour, $blank = true) {
        echo("<select name=\"$id\">");
        if ($blank) {
            echo("<option value=\"\">\n");
        }
        for ($i = 0; $i <= 23; $i++) {
            $val = sprintf("%02d", $i);
            echo("<option value=\"$val\"");
            if ($val == $hour) {
                echo(" selected");
            }
            echo(">$val\n");
        }

        echo("</select>");
    }

    /*     * ********************************************************************** */

    // 時表示（22〜28)
    /*     * ********************************************************************** */
    function show_hour_options_22_4($id, $hour, $blank = true) {
        echo("<select name=\"$id\">");
        if ($blank) {
            echo("<option value=\"\">\n");
        }
        for ($i = 22; $i <= 28; $i++) {
            if ($i < 24) {
                $val = sprintf("%02d", $i);
            } else {
                $val = sprintf("%02d", ($i - 24));
            }
            echo("<option value=\"$val\"");
            if ($val == $hour) {
                echo(" selected");
            }
            echo(">$val\n");
        }
        echo("</select>");
    }

    /*     * ********************************************************************** */

    // 分表示（00〜45)１５分単位
    /*     * ********************************************************************** */
    function show_min_options_15($id, $min, $blank = true) {
        echo("<select name=\"$id\">");
        if ($blank) {
            echo("<option value=\"\">\n");
        }
        for ($i = 0; $i <= 45; $i += 15) {
            $val = sprintf("%02d", $i);
            echo("<option value=\"$val\"");
            if ($val == $min) {
                echo(" selected");
            }
            echo(">$val\n");
        }
        echo("</select>");
    }

    /*     * ********************************************************************** */

    // 分表示（00〜55)５分単位
    /*     * ********************************************************************** */
    function show_min_options_5($id, $min, $blank = true) {
        echo("<select name=\"$id\">");
        if ($blank) {
            echo("<option value=\"\">\n");
        }

        for ($i = 0; $i <= 55; $i += 5) {
            $val = sprintf("%02d", $i);
            echo("<option value=\"$val\"");
            if ($val == $min) {
                echo(" selected");
            }
            echo(">$val\n");
        }
        echo("</select>");
    }

    /*     * ********************************************************************** */

    // 分表示（00〜59)１分単位
    /*     * ********************************************************************** */
    function show_min_options_00_59($id, $min, $blank = true) {
        echo("<select name=\"$id\">");
        if ($blank) {
            echo("<option value=\"\">\n");
        }
        for ($i = 0; $i <= 59; $i++) {
            $val = sprintf("%02d", $i);
            echo("<option value=\"$val\"");
            if ($val == $min) {
                echo(" selected");
            }
            echo(">$val\n");
        }
        echo("</select>");
    }

    //期間変更用の開始日付、終了日付を配列で返す
    function get_term_date($duty_yyyy, $duty_mm, $start_month_flg, $start_day, $month_flg, $end_day) {
        $arr_date = array();

        //開始日
        $wk_start_day = ($start_day == "" || $start_day == "0") ? 1 : $start_day;
        //前月対応
        if ($start_month_flg == "0") {
            $wk_st_duty_mm = $duty_mm - 1;
            if ($wk_st_duty_mm <= 0) {
                $wk_st_duty_mm = 12;
                $wk_st_duty_yyyy = $duty_yyyy - 1;
            } else {
                $wk_st_duty_yyyy = $duty_yyyy;
            }
        } else {
            $wk_st_duty_yyyy = $duty_yyyy;
            $wk_st_duty_mm = $duty_mm;
        }

        $wk_days_in_month = $this->days_in_month($wk_st_duty_yyyy, $wk_st_duty_mm);
        if ($wk_start_day > $wk_days_in_month) {
            $wk_start_day = $wk_days_in_month;
        }
        $arr_date[0] = sprintf("%04d%02d%02d", $wk_st_duty_yyyy, $wk_st_duty_mm, $wk_start_day);

        //終了日
        $wk_duty_mm = ($month_flg == "2") ? $duty_mm + 1 : $duty_mm;
        if ($wk_duty_mm == 13) {
            $wk_duty_mm = 1;
            $wk_duty_yyyy = $duty_yyyy + 1;
        } else {
            $wk_duty_yyyy = $duty_yyyy;
        }
        $wk_days_in_month = $this->days_in_month($wk_duty_yyyy, $wk_duty_mm);
        if ($end_day > $wk_days_in_month) {
            $wk_end_day = $wk_days_in_month;
        } else {
            $wk_end_day = $end_day;
        }
        $arr_date[1] = sprintf("%04d%02d%02d", $wk_duty_yyyy, $wk_duty_mm, $wk_end_day);

        return $arr_date;
    }

    //グループID別の指定日が属する期間を取得
    function get_term_from_group_date($group_id, $group_array, $belong_date) {
        //対象グループの情報を設定
        for ($i = 0; $i < count($group_array); $i++) {
            if ($group_id == $group_array[$i]["group_id"]) {
                $start_month_flg1 = $group_array[$i]["start_month_flg1"];
                $start_day1 = $group_array[$i]["start_day1"];
                $month_flg1 = $group_array[$i]["month_flg1"];
                $end_day1 = $group_array[$i]["end_day1"];
                break;
            }
        }
        if ($start_month_flg1 == "") {
            $start_month_flg1 = "1";
        }
        if ($start_day1 == "" || $start_day1 == "0") {
            $start_day1 = "1";
        }
        if ($month_flg1 == "" || $end_day1 == "0") {
            $month_flg1 = "1";
        }
        if ($end_day1 == "" || $end_day1 == "0") {
            $end_day1 = "99";
        }

        $wk_yyyy = substr($belong_date, 0, 4);
        $wk_mm = substr($belong_date, 4, 2);
        $arr_date = $this->get_term_date($wk_yyyy, $wk_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);

        //範囲内の場合はそのままリターン
        if ($arr_date[0] <= $belong_date && $arr_date[1] >= $belong_date) {
            return $arr_date;
        }
        //範囲前
        if ($arr_date[0] > $belong_date) {
            $wk_mm = $wk_mm - 1;
            if ($wk_mm <= 0) {
                $wk_mm = 12;
                $wk_yyyy = $wk_yyyy - 1;
            }
        }
        //範囲後
        if ($arr_date[1] < $belong_date) {
            $wk_mm = $wk_mm + 1;
            if ($wk_mm >= 13) {
                $wk_mm = 1;
                $wk_yyyy = $wk_yyyy + 1;
            }
        }

        $arr_date = $this->get_term_date($wk_yyyy, $wk_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);

        return $arr_date;
    }

    function get_group_name($group_id) {
        $sql = "select group_name from duty_shift_group ";
        $cond = "where group_id = '$group_id' ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $group_name = "";
        $num = pg_num_rows($sel);
        if ($num > 0) {
            $group_name = pg_fetch_result($sel, 0, "group_name");
        }
        return $group_name;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（atdbk_register.phpより転用）
//-------------------------------------------------------------------------------------------------------------------------
    /*     * ********************************************************************** */
    // 指定年月の日数を求める
    /*     * ********************************************************************** */
    function days_in_month($year, $month) {
        for ($day = 31; $day >= 28; $day--) {
            if (checkdate($month, $day, $year)) {
                return $day;
            }
        }
        return false;
    }

    /*     * ********************************************************************** */

    // 翌日日付をyyyymmdd形式で取得
    /*     * ********************************************************************** */
    function next_date($yyyymmdd) {
        return date("Ymd", strtotime("+1 day", $this->to_timestamp($yyyymmdd)));
    }

    /*     * ********************************************************************** */

    // 前日日付をyyyymmdd形式で取得
    /*     * ********************************************************************** */
    function last_date($yyyymmdd) {
        return date("Ymd", strtotime("-1 day", $this->to_timestamp($yyyymmdd)));
    }

    /*     * ********************************************************************** */

    // 日付をタイムスタンプに変換
    /*     * ********************************************************************** */
    function to_timestamp($yyyymmdd) {
        $y = substr($yyyymmdd, 0, 4);
        $m = substr($yyyymmdd, 4, 2);
        $d = substr($yyyymmdd, 6, 2);
        return mktime(0, 0, 0, $m, $d, $y);
    }

    /*     * ********************************************************************** */

    // タイムスタンプから曜日を返す
    /*     * ********************************************************************** */
    function get_weekday($date) {
        $wd = date("w", $date);
        switch ($wd) {
            case "0":
                return "日";
                break;
            case "1":
                return "月";
                break;
            case "2":
                return "火";
                break;
            case "3":
                return "水";
                break;
            case "4":
                return "木";
                break;
            case "5":
                return "金";
                break;
            case "6":
                return "土";
                break;
        }
    }

//add 20111115-->
    /*     * ********************************************************************** */
    // 所定労働時間(通常勤務日/休日)情報取得
    // @param   $group_id       グループＩＤ
    // @return  $data           取得情報
    /*     * ********************************************************************** */
    function get_duty_shift_officehours2_array($group_id) {
        //-------------------------------------------------------------------
        //ＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "SELECT D.GROUP_ID AS SG_ID ";
        $sql.= "     , D.GROUP_NAME AS SG_NAME ";
        $sql.= "     , W.GROUP_ID AS PG_ID ";
        $sql.= "     , W.GROUP_NAME AS PG_NAME ";
        $sql.= "     , O.PATTERN ";
        $sql.= "     , A.ATDPTN_NM ";
        $sql.= "     , REPLACE(O.OFFICEHOURS2_START,':','') AS START_TIME ";
        $sql.= "     , REPLACE(O.OFFICEHOURS2_END,':','') AS END_TIME ";
        $sql.= "     , EMPCOND_OFFICEHOURS_FLAG "; //個人別の所定時間対応 20140318
        $sql.= "  FROM DUTY_SHIFT_GROUP D ";
        $sql.= "     , WKTMGRP W ";
        $sql.= "     , OFFICEHOURS O ";
        $sql.= "     , ATDPTN A ";
        $cond = " WHERE D.PATTERN_ID     = W.GROUP_ID ";
        $cond.="   AND W.GROUP_ID       = O.TMCD_GROUP_ID ";
        $cond.="   AND A.GROUP_ID       = W.GROUP_ID ";
        $cond.="   AND CAST(A.ATDPTN_ID AS VARCHAR)     = O.PATTERN ";
        $cond.="   AND D.GROUP_ID       = '" . $group_id . "' ";
        $cond.="   AND O.OFFICEHOURS_ID = '1' ";
        $cond.="   AND ((O.OFFICEHOURS2_START <>'' AND O.OFFICEHOURS2_END <>'') OR (A.EMPCOND_OFFICEHOURS_FLAG = '1')) "; //所定労働・休憩を指定しないで（時刻が未設定のため）、所定労働・休憩は個人の勤務条件を優先する場合に、情報が取得できなかった対応 20140609
        $cond.=" ORDER BY W.GROUP_ID ";
        $cond.="     , A.ATDPTN_ORDER_NO ";
        $cond.="     , A.ATDPTN_ID ";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //件数取得
        //-------------------------------------------------------------------
        $num = pg_num_rows($sel);

        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $data[$i]["sg_id"] = pg_fetch_result($sel, $i, "SG_ID");
            $data[$i]["sg_name"] = pg_fetch_result($sel, $i, "SG_NAME");
            $data[$i]["pg_id"] = pg_fetch_result($sel, $i, "PG_ID");
            $data[$i]["pg_name"] = pg_fetch_result($sel, $i, "PG_NAME");
            $data[$i]["pattern"] = pg_fetch_result($sel, $i, "PATTERN");
            $data[$i]["atdptn_nm"] = pg_fetch_result($sel, $i, "ATDPTN_NM");
            $data[$i]["start_time"] = pg_fetch_result($sel, $i, "START_TIME");
            $data[$i]["end_time"] = pg_fetch_result($sel, $i, "END_TIME");
            $data[$i]["empcond_officehours_flag"] = pg_fetch_result($sel, $i, "EMPCOND_OFFICEHOURS_FLAG"); //個人別の所定時間対応 20140318
        }

        return $data;
    }

    /*     * ********************************************************************** */

    // 「集計変更を有効にする」スイッチを取得する 2012.2.1
    // @param   $pattern_id     勤務パターングループ
    // @return  ナシ
    // @eval    $change_switchに値を設定する 0:変更無し、1:変更あり
    /*     * ********************************************************************** */
    function GetSumCustomSwith($pattern_id) {
        if (empty($pattern_id)) {
            return 0;
        }
        $sql = "SELECT sum_change_sw FROM duty_shift_entity_sum_change_sw";
        $cond = "WHERE group_id = $pattern_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) != 0) {
            return( pg_fetch_result($sel, 0, "sum_change_sw") );
        } else {
            return( 0 );
        }
    }

    /*     * ********************************************************************** */

    // 「集計変更を有効にする」情報を取得する 2012.2.1
    // @param   $pattern_id     勤務パターングループ
    // @return  連想配列
    /*     * ********************************************************************** */
    function custom_table_read($pattern_id) {
        $return_array = array();
        $sql = "SELECT VAL.font_name , NAM.sum_unit_name , VAL.reason , VAL.sum_unit_value FROM duty_shift_entity_sum_unit_value AS VAL ";
        $sql = $sql . "JOIN duty_shift_entity_sum_unit_name AS NAM ON (VAL.group_id=NAM.group_id AND VAL.sum_unit_name_id=NAM.sum_unit_name_id) ";
        $cond = " WHERE VAL.group_id=$pattern_id ORDER BY NAM.sum_unit_name_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) { // DB Error
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $rowCount = pg_num_rows($sel);
        for ($r = 0; $r < $rowCount; $r++) {
            $font_name = pg_fetch_result($sel, $r, "font_name");
            $sumname = pg_fetch_result($sel, $r, "sum_unit_name");
            $reason = pg_fetch_result($sel, $r, "reason");
            $value = pg_fetch_result($sel, $r, "sum_unit_value");
            if ($sumname == "") {
                $sumname = 0;
            }
            if ($reason == "") {
                $reason = 0;
            }
            if ($value == "") {
                $value = 0;
            }
            $return_array[$font_name][$sumname][$reason] = $value;
        }
        return $return_array;
    }

    /*     * ********************************************************************** */

    // 「集計変更を有効にする」情報を取得する、集計見出し名用 2012.2.1
    // @param   $pattern_id     勤務パターングループ
    // @return  連想配列
    /*     * ********************************************************************** */
    function custom_table_title_name($pattern_id) {
        $return_array = array();
        $sql = "SELECT sum_unit_name FROM duty_shift_entity_sum_unit_name ";
        $cond = "WHERE group_id=$pattern_id AND sum_unit_name<>'' AND sum_unit_name_id IN ";
        $cond = $cond . "(SELECT sum_unit_name_id FROM duty_shift_entity_sum_unit_value WHERE group_id=$pattern_id) ORDER BY sum_unit_name_id;";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) { // DB Error
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $rowCount = pg_num_rows($sel);
        for ($r = 0; $r < $rowCount; $r++) {
            $return_array[$r] = pg_fetch_result($sel, $r, "sum_unit_name");
        }
        return $return_array;
    }

    /*     * ********************************************************************** */

    // 勤務表集計項目：集計名を取得
    // @param   $pattern_id     勤務パターングループ
    // @return  集計名
    /*     * ********************************************************************** */
    function count_names($pattern_id) {
        $sql = "SELECT * FROM duty_shift_sign_count_name WHERE group_id=" . p($pattern_id) . " ORDER BY order_no, count_id";
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) { // DB Error
            pg_close($this->_db_con);
            js_error_exit();
        }

        $names = array();
        while ($row = pg_fetch_assoc($sel)) {
            $row['jobs'] = array_keys(unserialize($row['job_filter']));
            $names[] = $row;
        }
        return $names;
    }

    /*     * ********************************************************************** */

    // 勤務表集計項目：集計値を取得
    // @param   $pattern_id     勤務パターングループ
    // @return  集計値
    /*     * ********************************************************************** */
    function count_values($pattern_id) {
        $sql = "SELECT * FROM duty_shift_sign_count_value WHERE group_id=" . p($pattern_id);
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) { // DB Error
            pg_close($this->_db_con);
            js_error_exit();
        }

        $values = array();
        while ($row = pg_fetch_assoc($sel)) {
            $values[$row['pattern']][$row['count_id']] = $row['count_value'];
        }
        return $values;
    }

    /*     * ********************************************************************** */

    // 勤務希望データ取得 20120403
    // @param   $group_id       グループID
    // @param   $pattern_id     勤務パターングループ
    // @param   $start_date     開始日
    // @param   $end_date       終了日
    // @return  配列
    /*     * ********************************************************************** */
    function get_plan_individual($group_id, $pattern_id, $start_date, $end_date) {
        ///-----------------------------------------------------------------------------
        // 上書きアラートの為の勤務希望データ取得
        ///-----------------------------------------------------------------------------
        if ($group_id != "") { // 2012.6.13 不具合修正、comedixインストール直後で初めて勤務シフトを選択するとシステムエラーとなる対応。初期ではduty_shift_staffテーブルがカラのため。
            $sql = "
                SELECT
                dspi.*,
                to_date(duty_date,'YYYYMMDD')-to_date('{$start_date}','YYYYMMDD')+1 AS day,
                atdptn_nm
                FROM duty_shift_plan_individual dspi
                LEFT JOIN atdptn a ON atdptn_ptn_id=atdptn_id AND a.group_id={$pattern_id}
                WHERE dspi.group_id='{$group_id}' AND duty_date BETWEEN '{$start_date}' AND '{$end_date}'
                ";
            $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $plan_individual = array();
            while ($row = pg_fetch_assoc($sel)) {
                $plan_individual["e{$row["emp_id"]}"]["d{$row["day"]}"] = array(
                    "id" => $row["atdptn_ptn_id"],
                    "date" => preg_replace("/^\d\d\d\d(\d\d)(\d\d)$/", "$1/$2", $row["duty_date"]),
                    "nm" => $row["atdptn_nm"],
                );
            }
        } else {
            $plan_individual = "";
        }
        return $plan_individual;
    }

    /*     * ********************************************************************** */

    // 勤務条件履歴データ取得 20120418
    // @param   $standard_id    施設基準ＩＤ
    // @param   $duty_yyyy  年
    // @param   $duty_mm    月
    //
    // @return  $data_emp   取得情報、職員IDと項目名をキーとした配列 [$emp_id]["duty_form"],["other_post_flg"],["job_id"],["job_name"]
    /*     * ********************************************************************** */
    function get_empcond_history($standard_id, $duty_yyyy, $duty_mm) {
        $data_emp = array();

        if ($standard_id == "" || $duty_yyyy == "" || $duty_mm == "") {
            return $data_emp;
        }
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        //作成期間が月またがりで、終了日より後にだけ登録がある場合の対応
        $duty_yyyy2 = $duty_yyyy;
        $duty_mm2 = $duty_mm + 1;
        if ($duty_mm2 > 12) {
            $duty_mm2 = 1;
            $duty_yyyy2++;
        }
        $wk_ymd = $duty_yyyy . sprintf("-%02d-01 00:00:00", $duty_mm);
        //基準日（1日）以降の最も近い履歴を取得し設定、ない場合は未設定となり、呼出元get_standard_empmst_array関数のempmst,empcondの項目を使用
        $sql = "
                select a.emp_id, a.duty_form  from empcond_duty_form_history a
                inner join (
                select e.emp_id, min(e.empcond_duty_form_history_id) as min_id  from empcond_duty_form_history e  inner join duty_shift_plan_staff a on a.emp_id = e.emp_id  inner join duty_shift_group b on b.group_id = a.group_id
                where e.histdate >= '$wk_ymd' and
                b.standard_id = '$standard_id' and ((a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm) or (a.duty_yyyy = $duty_yyyy2 and a.duty_mm = $duty_mm2)) group by e.emp_id ) as b
                on a.emp_id = b.emp_id and a.empcond_duty_form_history_id = b.min_id order by a.emp_id
                ";
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {

            $emp_id = $row["emp_id"];
            $data_emp[$emp_id]["duty_form"] = $row["duty_form"];
        }
        //他部署兼務
        $sql = "
                select a.emp_id, a.other_post_flg  from empcond_other_post_flg_history a
                inner join (
                select e.emp_id, min(e.empcond_other_post_flg_history_id) as min_id  from empcond_other_post_flg_history e  inner join duty_shift_plan_staff a on a.emp_id = e.emp_id  inner join duty_shift_group b on b.group_id = a.group_id
                where e.histdate >= '$wk_ymd' and
                b.standard_id = '$standard_id' and ((a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm) or (a.duty_yyyy = $duty_yyyy2 and a.duty_mm = $duty_mm2)) group by e.emp_id ) as b
                on a.emp_id = b.emp_id and a.empcond_other_post_flg_history_id = b.min_id order by a.emp_id
                ";
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {

            $emp_id = $row["emp_id"];
            $data_emp[$emp_id]["other_post_flg"] = $row["other_post_flg"];
        }
        //職種
        $sql = "
                select a.emp_id, a.job_id, c.job_nm  from job_history a
                inner join (
                select e.emp_id, min(e.job_history_id) as min_id  from job_history e  inner join duty_shift_plan_staff a on a.emp_id = e.emp_id  inner join duty_shift_group b on b.group_id = a.group_id
                where e.histdate >= '$wk_ymd' and
                b.standard_id = '$standard_id' and ((a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm) or (a.duty_yyyy = $duty_yyyy2 and a.duty_mm = $duty_mm2)) group by e.emp_id ) as b
                on a.emp_id = b.emp_id and a.job_history_id = b.min_id
                left join jobmst c on c.job_id = a.job_id
                order by a.emp_id
                ";
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {

            $emp_id = $row["emp_id"];
            $data_emp[$emp_id]["job_id"] = $row["job_id"];
            $data_emp[$emp_id]["job_name"] = $row["job_nm"];
        }
        return $data_emp;
    }

    /*     * ********************************************************************** */

    // 勤務シフトスキル名取得
    // @return  $data   取得情報
    // スキルレベルを増減する場合は以下のテーブルに対してデータを増減してください
    // その際「必要人数設定」の要求スキルレベルに影響しますので確認してください。
    /*     * ********************************************************************** */
    function get_duty_shift_skill_array() {
        //-------------------------------------------------------------------
        //勤務シフトスタッフチームＤＢより情報取得
        //-------------------------------------------------------------------
        $sql = "SELECT * FROM duty_shift_skill_name";
        $cond = "ORDER BY skill_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        while ($row = pg_fetch_array($sel)) {
            $data[$row["skill_id"]]["skill_id"] = $row["skill_id"];   //スキルID
            $data[$row["skill_id"]]["skill_name"] = $row["skill_name"];   //スキル名
        }
        return $data;
    }

    /**
     * 施設基準か、シフトグループ（病棟）のグループ情報を取得
     *
     * @param mixed $standard_id 施設基準
     * @param mixed $group_id グループID
     * @return mixed This is the return value description
     *
     */
    function get_group_info_for_report($standard_id, $group_id) {
        $sql = "select * from duty_shift_group ";
        if ($group_id != "" && $group_id != "0") {
            $cond = "where group_id = '$group_id' ";
        } else {
            $cond = "where standard_id = '$standard_id' order by order_no";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $group_array = array();
        $num = pg_num_rows($sel);
        if ($num > 0) {
            $group_array = pg_fetch_all($sel);
        }
        for ($i = 0; $i < $num; $i++) {
            if ($group_array[$i]["start_month_flg1"] == "") {
                $group_array[$i]["start_month_flg1"] = "1";
            }
            if ($group_array[$i]["start_day1"] == "" || $group_array[$i]["start_day1"] == "0") {
                $group_array[$i]["start_day1"] = "1";
            }
            if ($group_array[$i]["month_flg1"] == "" || $group_array[$i]["end_day1"] == "0") {
                $group_array[$i]["month_flg1"] = "1";
            } //終了日確認
            if ($group_array[$i]["end_day1"] == "" || $group_array[$i]["end_day1"] == "0") {
                $group_array[$i]["end_day1"] = "99";
            }
        }
        return $group_array;
    }

    /**
     * 施設基準か、シフトグループ（病棟）に指定年月時点で所属している職員の情報を取得(様式９用）
     * 月またがり対応
     *
     * @param mixed $standard_id 施設基準ＩＤ
     * @param mixed $group_id グループID
     * @param mixed $duty_yyyy 年
     * @param mixed $duty_mm 月
     * @return mixed 職員の情報
     *
     */
    function get_emp_info_for_report($standard_id, $group_id, $duty_yyyy, $duty_mm) {

        $group_array = $this->get_group_info_for_report($standard_id, $group_id);

        $tukimatagari_ari_cnt = 0;
        $tukimatagari_nasi_cnt = 0;
        //月またがりフラグ 1:前月から当月 2:当月から翌月 "":当月1日から当月末 20120717
        $month_fromto_flg = "";
        for ($i = 0; $i < count($group_array); $i++) {
            //月またがりフラグ
            $start_month_flg1 = $group_array[$i]["start_month_flg1"];
            $start_day1 = $group_array[$i]["start_day1"];
            $month_flg1 = $group_array[$i]["month_flg1"];
            $end_day1 = $group_array[$i]["end_day1"];
            //当月1日から当月末
            if ($start_month_flg1 == "1" &&
                    $start_day1 == "1" &&
                    $month_flg1 == "1") {
                $group_array[$i]["tukimatakari_flg"] = false;
                $tukimatagari_nasi_cnt++;
            } else {
                $group_array[$i]["tukimatakari_flg"] = true;
                $tukimatagari_ari_cnt++;
                if ($start_month_flg1 == "0") {
                    $month_fromto_flg = "1";
                } elseif ($month_flg1 == "2") {
                    $month_fromto_flg = "2";
                }
            }
        }
        $duty_yyyy2 = $duty_yyyy;
        if ($tukimatagari_ari_cnt > 0) {
            if ($month_fromto_flg == "1") {
                $duty_mm2 = $duty_mm + 1;
                if ($duty_mm2 > 12) {
                    $duty_mm2 = 1;
                    $duty_yyyy2++;
                }
            } else {
                $duty_mm2 = $duty_mm - 1;
                if ($duty_mm2 < 1) {
                    $duty_mm2 = 12;
                    $duty_yyyy2--;
                }
            }
        } else {
            $duty_mm2 = $duty_mm + 1;
            if ($duty_mm2 > 12) {
                $duty_mm2 = 1;
                $duty_yyyy2++;
            }
        }

        $cond_plan_staff_str = "";
        //全ての病棟が月またがり以外
        if ($tukimatagari_nasi_cnt == count($group_array)) {
            $cond_plan_staff_str = "((duty_yyyy = $duty_yyyy and duty_mm = $duty_mm or duty_yyyy = $duty_yyyy2 and duty_mm = $duty_mm2) and d.standard_id = '$standard_id')"; //施設基準 20140404 翌月を追加 20150408
        }
        //一部月またがり、病棟毎に年月を条件設定する
        //(グループ and (年月指定、作成期間が月またがりなら翌月も指定))
        else {
            $cond_plan_staff_str = "(";
            for ($i = 0; $i < count($group_array); $i++) {
                if ($i > 0) {
                    $cond_plan_staff_str .= " or ";
                }
                $cond_plan_staff_str .= "(a.group_id = '" . $group_array[$i]["group_id"] . "' and ";
                if ($group_array[$i]["tukimatakari_flg"] == true) {
                    $cond_plan_staff_str .= " ((a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm) or (a.duty_yyyy = $duty_yyyy2 and a.duty_mm = $duty_mm2)) )";
                } else {
                    $cond_plan_staff_str .= " ((a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm)) )";
                }
            }
            $cond_plan_staff_str .= ")";
        }

        $sql = "select ";
        $sql .= "a.emp_id, a.duty_yyyy, a.duty_mm, a.no, a.group_id, b.emp_personal_id, b.emp_lt_nm||' '||b.emp_ft_nm as emp_name, c.group_id as main_group_id, d.group_name ";
        $sql .= "from duty_shift_plan_staff a ";
        $sql .= " left join empmst b on a.emp_id = b.emp_id ";
        $sql .= " left join duty_shift_staff c on c.emp_id = a.emp_id ";
        $sql .= " left join duty_shift_group d on d.group_id = a.group_id ";
        $cond = "where $cond_plan_staff_str ";
        $cond_desc = "";
        if ($month_fromto_flg == "2") { //当月から翌月
            $cond_desc = " desc ";
        }
        $cond .= "order by d.order_no, a.duty_yyyy $cond_desc, a.duty_mm $cond_desc, a.no ";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $arr_no = array();
        $idx = 0;
        $arr_emp_info = array();
        $num = pg_num_rows($sel);
        for ($i = 0; $i < $num; $i++) {
            $emp_id = pg_fetch_result($sel, $i, "emp_id");
            $proc_flg = false;
            //月またがり時、2件ある場合の対応
            if ($group_id != "" && $group_id != "0") {
                if ($arr_no[$emp_id] == "") {
                    $proc_flg = true;
                    $arr_no[$emp_id] = $idx + 1;
                }
            } else {
                $wk_group_id = pg_fetch_result($sel, $i, "group_id");
                if ($arr_no[$wk_group_id][$emp_id] == "") {
                    $proc_flg = true;
                    $arr_no[$wk_group_id][$emp_id] = $idx + 1;
                }
            }
            if ($proc_flg == true) {
                $arr_emp_info[$idx]["no"] = $idx + 1;
                $arr_emp_info[$idx]["emp_id"] = $emp_id;
                $arr_emp_info[$idx]["emp_name"] = pg_fetch_result($sel, $i, "emp_name");
                $arr_emp_info[$idx]["group_id"] = pg_fetch_result($sel, $i, "group_id");
                $arr_emp_info[$idx]["group_name"] = pg_fetch_result($sel, $i, "group_name");
                $idx++;
            }
        }
        return $arr_emp_info;
    }

    /**
     * シフトグループ期間情報を取得
     *
     * 同一施設基準を持つシフトグループでは、シフト作成期間(締め日)の設定は同じであるという前提で、情報を取得
     *
     * @param mixed $standard_id 施設基準
     * @param mixed $group_id グループID
     * @param mixed $duty_yyyy 集計対象年
     * @param mixed $duty_mm 集計対象月
     * @param array $calendar_array カレンダー情報
     * @return mixed 取得情報、グループ毎にシフト作成期間の開始終了の日付、月またがりの場合は、前半と後半の日付
     *
     */
    function get_shift_term($group_info, $duty_yyyy, $duty_mm, $calendar_array) {
        //-------------------------------------------------------------------
        //期間を設定
        //-------------------------------------------------------------------
        $data = array();
        //期間情報を配列にセット
        $s_date = $calendar_array[0]["date"];                          //カレンダー開始日
        $e_date = $calendar_array[count($calendar_array) - 1]["date"]; //カレンダー終了日
        $start_day1 = $group_info["start_day1"];                      //シフト開始日
        $end_day1 = $group_info["end_day1"];                        //シフト終了日

        $month_flg1 = $group_info["month_flg1"];
        $start_month_flg1 = $group_info["start_month_flg1"];

        $shift_term = array();
        if ($start_month_flg1 == "1" && $start_day1 == "1" && $month_flg1 == "1") {    //シフト作成期間が当月1日から当月末
            $wk_s_date = $duty_yyyy . sprintf("%02d", $duty_mm) . "01"; //対象月の1日の日付
            if ($calendar_array[1]["date"] === $wk_s_date) {    //計算開始日が対象月の1日
                $end_date1 = $e_date;
            } else {    //開始日が対象月の1日以外
                $end_date1 = date("Ymd", mktime(0, 0, 0, intval($duty_mm) + 1, 0, $duty_yyyy));    //対象月の月末
                $start_date2 = date("Ymd", mktime(0, 0, 0, intval($duty_mm) + 1, 1, $duty_yyyy));    //翌月の1日
            }
            $shift_term[] = array(
                "yyyymm" => $duty_yyyy . sprintf("%02d", $duty_mm),
                "year" => $duty_yyyy,
                "month" => intval($duty_mm),
                "start_date" => $s_date,
                "end_date" => $end_date1
            );

            //計算開始日が対象月の1日以外の場合、次月分も取得
            if ($calendar_array[1]["date"] !== $wk_s_date) {
                $shift_term[] = array(
                    "yyyymm" => substr($start_date2, 0, 6),
                    "year" => substr($start_date2, 0, 4),
                    "month" => intval(substr($start_date2, 4, 2)),
                    "start_date" => $start_date2,
                    "end_date" => $e_date
                );
            }
        } else {    //シフト作成期間が月またがり
            $close_date = $duty_yyyy . sprintf("%02d%02d", intval($duty_mm), $end_day1); //締め日
            if ($close_date < $calendar_array[1]["date"]) {    //カレンダー情報には前日日付からセットされているので、計算開始日として2番目の日付で比較
                //計算開始日(カレンダー情報の2番目の日付)が締め日より後なら、翌月の締め日を取得
                $close_date = date("Ymd", mktime(0, 0, 0, intval($duty_mm) + 1, $end_day1, $duty_yyyy));
                $duty_yyyy = substr($close_date, 0, 4);
                $duty_mm = intval(substr($close_date, 4, 2));
            }

            if ($start_month_flg1 == "0") {
                //前月・当月のシフト作成期間
                $target_year1 = $duty_yyyy;
                $target_mon1 = intval($duty_mm);

                $start_date2 = date("Ymd", mktime(0, 0, 0, intval($duty_mm) + 1, 1, $duty_yyyy));
                $target_year2 = substr($start_date2, 0, 4);
                $target_mon2 = intval(substr($start_date2, 4, 2));
            } elseif ($month_flg1 == "2") {
                //当月・翌月のシフト作成期間
                $start_date1 = date("Ymd", mktime(0, 0, 0, intval($duty_mm) - 1, 1, $duty_yyyy));
                $target_year1 = substr($start_date1, 0, 4);
                $target_mon1 = intval(substr($start_date1, 4, 2));

                $target_year2 = $duty_yyyy;
                $target_mon2 = intval($duty_mm);
            }
            //シフト作成期間が当月2日からの場合の不具合対応 20140305
            else {
                $target_year1 = $duty_yyyy;
                $target_mon1 = intval($duty_mm);
                $e_date = $close_date; //次のシフト分は取得しないように、日付を合わせる
            }

            if ($e_date <= $close_date) {
                //カレンダー終了日が締め日以前ならば、取得期間終了日にカレンダー終了日をセット(次のシフト分は取得しないので)
                $end_date1 = $e_date;
            } else {
                $end_date1 = $close_date;
            }

            $shift_term[] = array(
                "yyyymm" => $target_year1 . sprintf("%02d", $target_mon1),
                "year" => $target_year1,
                "month" => $target_mon1,
                "start_date" => $s_date,
                "end_date" => $end_date1
            );

            if ($e_date > $close_date) {    //カレンダー終了日が締め日より後ならば、次のシフト分のデータも取得
                $shift_term[] = array(
                    "yyyymm" => $target_year2 . sprintf("%02d", $target_mon2),
                    "year" => $target_year2,
                    "month" => $target_mon2,
                    "start_date" => $this->next_date($close_date), //締め日翌日
                    "end_date" => $e_date
                );
            }
        }

        return $shift_term;
    }

    /**
     * 職員が夜勤従事者(夜勤有り又は夜専)か判定する
     *
     * @param array $info 職員一人当たりの情報
     * @return boolean true:夜勤従事者/false:夜勤従事者でない
     */
    function is_night_shift($info) {
        // night_duty_flgが1又は2であれば夜勤従事者
        if ($info["night_duty_flg"] == 1 || $info["night_duty_flg"] == 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 勤務シフト予定の有給休暇日数を取得する
     *
     * @param string $emp_id     職員ID
     * @param string $start_date データ取得開始日
     * @param string $end_date   データ取得終了日
     * @param string $plan_type  シフトデータ種別
     * @param       $sche       パラメータ追加 20131115 取得元テーブル(atdbk(出勤予定):未指定時 / atdbk_sche(当初予定))
     *                           出勤予定テーブル名に付加する拡張子("_sche")
     *
     * @return double 有給休暇日数
     */
    function get_paid_hol_plan($emp_id, $start_date, $end_date, $plan_type, $sche) {
        // データ取得 reason:44半有半公は0.5日とする 55半夏半有 57半有半欠 58半特半有 62半正半有 2午前有休 3午後有休 38午前年休 39午後年休
        if ($plan_type === "plan") {    // 登録済のシフト予定データのみ
            $sql = "select sum(case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end) as cnt from atdbk" . $sche;
            $cond = "where (emp_id = '$emp_id' and date >= '$start_date'";
            if ($end_date !== "") {
                $cond .= " and date <= '$end_date'";
            }
            $cond .= ")";
            $cond .= " and ((reason='1' or reason='2' or reason='3' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (reason >= '37' and reason<='39'))";
        } elseif ($plan_type === "create") {    // 下書きデータも考慮したシフト予定データ
            $sql = "select coalesce(a.cnt, 0) + coalesce(b.cnt, 0) as cnt from";
            $sql .= "(select sum(case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end) as cnt from atdbk" . $sche;
            $sql .= " where (emp_id = '$emp_id' and date >= '$start_date'";
            if ($end_date !== "") {
                $sql .= " and duty_date <= '$end_date'";
            }
            $sql .= ") and ((reason='1' or reason='2' or reason='3' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (reason >= '37' and reason<='39'))) a,";
            $sql .= "(select sum(case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end) as cnt from duty_shift_plan_draft";
            $sql .= " where (emp_id = '$emp_id' and duty_date >= '$start_date'";
            if ($end_date !== "") {
                $sql .= " and duty_date <= '$end_date'";
            }
            $sql .= ") and ((reason='1' or reason='2' or reason='3' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (reason >= '37' and reason<='39'))) b";
            $cond = "";
        } elseif ($plan_type === "hope") {    // 勤務シフト希望
            $sql = "select sum(case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end) as cnt from duty_shift_plan_individual";
            $cond = "where (emp_id = '$emp_id' and duty_date >= '$start_date'";
            if ($end_date !== "") {
                $cond .= " and duty_date <= '$end_date'";
            }
            $cond .= ")";
            $cond .= " and ((reason='1' or reason='2' or reason='3' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (reason >= '37' and reason<='39'))";
        }

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $cnt = pg_fetch_result($sel, 0, "cnt");

        return (double) $cnt;
    }

    /**
     * 実績年休残数リストを取得する
     *
     * @param array   $staff_id_array 職員リスト
     * @param string  $start_date     開始日
     * @param string  $end_date       終了日
     * @param integer $duty_yyyy      指定年
     * @param integer $duty_mm        指定月
     * @return array 取得情報(各職員の年休残数が、職員リストと同じ順序で格納されている)
     */
    function get_paid_hol_result_array($staff_id_array, $start_date, $end_date, $duty_yyyy, $duty_mm) {
        $data = array();

        if (count($staff_id_array) == 0) {
            return $data;
        }

        //グループ毎のテーブルから勤務時間を取得する
        $timecard_common_class = new timecard_common_class($this->_db_con, $this->file_name, $staff_id_array, $start_date, $end_date);

        foreach ($staff_id_array as $staff_id) {
            $data[] = $timecard_common_class->get_nenkyu_zan($duty_yyyy, $duty_mm, $staff_id, $this->timecard_bean->closing_paid_holiday, $start_date, $end_date);
        }

        return $data;
    }

    /**
     * 有給休暇日数リストを取得する
     *
     * @param array  $staff_id_array        職員リスト
     * @param array  $paid_hol_result_array 前日までの有給休暇残日数
     * @param string $start_date            開始日
     * @param string $end_date              終了日
     * @param string $plan_type             シフトデータ種別
     * @return array 取得情報(各職員の年休残数が、職員リストと同じ順序で格納されている)
     */
    function get_paid_hol_plan_array($staff_id_array, $paid_hol_result_array, $start_date, $end_date, $plan_type) {
        $data = array();

        $count = count($staff_id_array);
        if ($count == 0) {
            return $data;
        }

        for ($i = 0; $i < $count; $i++) {
            $data[$i] = $paid_hol_result_array[$i] - $this->get_paid_hol_plan($staff_id_array[$i], $start_date, $end_date, $plan_type);
        }

        return $data;
    }

    /**
     * シフトグループ間のシフト期間をチェックする
     *
     * @param array $group_array 比較を行うシフトグループ
     * @return boolean true:シフト期間が同じ/false:シフト期間が異なる
     */
    function is_same_shift_term($group_array) {
        $count = count($group_array);

        //チェックするシフトグループ数が複数でなければ、無条件にtrue
        if ($count < 2) {
            return true;
        }

        //配列の先頭のシフトグループの期間を基準に比較する
        $start_day1 = $group_array[0]["start_day1"];
        $end_day1 = $group_array[0]["end_day1"];
        $month_flg1 = $group_array[0]["month_flg1"];
        $start_month_flg1 = $group_array[0]["start_month_flg1"];

        for ($i = 1; $i < $count; $i++) {
            if ($group_array[$i]["start_day1"] !== $start_day1 || $group_array[$i]["end_day1"] !== $end_day1 || $group_array[$i]["month_flg1"] !== $month_flg1 || $group_array[$i]["start_month_flg1"] !== $start_month_flg1) {
                return false;
            }
        }

        return true;
    }

    /**
     * 役職・職種・雇用勤務形態の履歴情報 20130219
     *
     * @param array  $calendar_array    勤務シフト情報
     * @param array $wk_group           シフトグループ情報
     * @return array                    取得情報
     */
    function get_history_info($calendar_array, $wk_group) {
        reset($calendar_array);
        $start_key = key($calendar_array);
        $start_date = $calendar_array[$start_key]["date"];  //開始日
        end($calendar_array);
        $end_key = key($calendar_array);
        $end_date = $calendar_array[$end_key]["date"];      //終了日

        if ($start_date != "" && $end_date != "") {
            if ($wk_group[0]["job_disp_flg"] == "t") {
                $job_id_info = $this->get_history("job_id");    //職種履歴
                $job_id_name = $this->get_stjob_name("job_id");
            }

            if ($wk_group[0]["st_disp_flg"] == "t") {
                $st_id_info = $this->get_history("st_id");  //役職履歴
                $st_id_name = $this->get_stjob_name("st_id");
            }

            if ($wk_group[0]["es_disp_flg"] == "t") {
                $duty_form_info = $this->get_history("duty_form"); //雇用勤務履歴
            }
        }

        return array("job_id_info" => $job_id_info, "job_id_name" => $job_id_name,
            "st_id_info" => $st_id_info, "st_id_name" => $st_id_name,
            "duty_form_info" => $duty_form_info, "start_date" => $start_date, "end_date" => $end_date);
    }

    /**
     * 役職・職種・雇用勤務形態を取得 20130219
     *
     * @param string  $mode_name       役職・職種・雇用勤務形態モード
     * @return array 取得情報
     */
    function get_history($mode_name) {
        switch ($mode_name) {
            case "job_id":  //職種履歴
                $sql = "select emp_id, job_id, histdate from job_history";
                $cond = "where job_history_id > 0 order by histdate";
                $buf_value = "job_id";
                break;
            case "st_id":   //役職履歴
                $sql = "select emp_id, st_id, histdate from st_history";
                $cond = "where st_history_id > 0 order by histdate";
                $buf_value = "st_id";
                break;
            case "duty_form": //雇用勤務形態履歴
                $sql = "select emp_id, duty_form, histdate from empcond_duty_form_history";
                $cond = "where empcond_duty_form_history_id > 0 order by histdate";
                $buf_value = "duty_form";
                break;
        }

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $arr_info = array();
        $num = pg_num_rows($sel);
        for ($i = 0; $i < $num; $i++) {
            $s_id = pg_fetch_result($sel, $i, "emp_id");      //職員ID
            $s_value = pg_fetch_result($sel, $i, $buf_value); //役職・職種・雇用形態のID
            $s_date = pg_fetch_result($sel, $i, "histdate");
            $s_date = date("Ymd", strtotime($s_date));   //履歴日

            $arr_info[$s_id][] = array($buf_value => $s_value, "histdate" => $s_date);
        }

        return $arr_info;
    }

    /**
     * 役職・職種の名称を取得 20130219
     *
     * @param string  $mode_name       役職・職種・雇用勤務形態モード
     * @return array 取得情報
     */
    function get_stjob_name($mode_name) {
        switch ($mode_name) {
            case "job_id":
                $sql = "select job_id, job_nm from jobmst";
                $buf_id = "job_id";
                $buf_nm = "job_nm";
                break;
            case "st_id":
                $sql = "select st_id, st_nm from stmst";
                $buf_id = "st_id";
                $buf_nm = "st_nm";
                break;
        }
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $arr_info = array();
        $num = pg_num_rows($sel);
        for ($i = 0; $i < $num; $i++) {
            $d_id = pg_fetch_result($sel, $i, $buf_id);
            $d_nm = pg_fetch_result($sel, $i, $buf_nm);
            $arr_info[$d_id] = $d_nm;
        }

        return $arr_info;
    }

    /**
     * 役職・職種・雇用勤務形態の名称とカラーを取得 20130219
     *
     * @param array  $arr_info       役職・職種・雇用勤務形態情報
     * @param array  $arr_name       役職・職種・雇用勤務形態の全部名称
     * @param string $mode_name      役職・職種・雇用勤務形態モード
     * @param string $start_date     取得対象データの開始日
     * @param string $end_date       取得対象データの終了日
     * @return array 取得情報
     */
    function get_show_name($arr_info, $arr_name, $mode_name, $start_date, $end_date) {
        $name = "";
        $color = "";
        $max_key = max(array_keys($arr_info));  //最終履歴
        foreach ($arr_info as $value) {
            $alter_flag = FALSE;
            if ($start_date <= $value["histdate"] && $value["histdate"] <= $end_date) {
                $color = "FF0000";  //指定期間内に変更履歴あり
                $alter_flag = TRUE;
            }

            if ($start_date < $value["histdate"]) {
                $id = $value[$mode_name];
                $name = $arr_name[$id];

                if ($alter_flag == FALSE) {
                    break;
                }

                if ($arr_info[$max_key]["histdate"] == $value["histdate"]) {
                    $name = ""; //最終履歴の場合、最新名称表示
                }
            }
        }

        return array("name" => $name, "color" => $color);
    }

    function set_disp_tosYotei_flg($group_id, $flag) {

        //-------------------------------------------------------------------
        //表示フラグを設定
        //-------------------------------------------------------------------
        $sql = "update duty_shift_group set disp_tosYotei_flg='" . $flag . "'";
        $sql.= " where group_id = '$group_id'";
        $upd = insert_into_table_no_content($this->_db_con, $sql, $this->file_name);
        if ($upd == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    //-------------------------------------------------------------------
    // ユニット表示する階層名を取得する
    //-------------------------------------------------------------------
    function get_emp_unitname($emp_id, $group_id) {

        // ユニット（階層レベルを取得する）
        $disp_level = $this->get_emp_unitlevel($group_id);


        switch ($disp_level) {

            case '1':       //
                $sql = "select emp_id, emp_class, emp_attribute, emp_dept, class_nm as nm from empmst ";
                $sql .= "left join classmst on empmst.emp_class=classmst.class_id";
                $cond = "where empmst.emp_id='" . $emp_id . "'";
                break;

            case '2':       //
                $sql = "select emp_id, emp_class, emp_attribute, emp_dept, atrb_nm as nm from empmst ";
                $sql .= "left join atrbmst on empmst.emp_attribute=atrbmst.atrb_id and empmst.emp_class=atrbmst.class_id";
                $cond = "where empmst.emp_id='" . $emp_id . "'";
                break;

            case '3':       //
                $sql = "select emp_id, emp_class, emp_attribute, emp_dept, dept_nm as nm from empmst ";
                $sql .= "left join deptmst on empmst.emp_dept=deptmst.dept_id and empmst.emp_attribute=deptmst.atrb_id";
                $cond = "where empmst.emp_id='" . $emp_id . "'";
                break;

            case '4':       //
                $sql = "select emp_id, emp_class, emp_attribute, emp_dept, room_nm as nm from empmst ";
                $sql .= "left join classroom on empmst.emp_room=classroom.room_id and empmst.emp_dept=classroom.dept_id";
                $cond = "where empmst.emp_id='" . $emp_id . "'";
        }

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $nm = pg_fetch_result($sel, 0, "nm");

        if ($nm == "") {
            $nm = "(未設定)";
        }

        return $nm;
    }

    //-------------------------------------------------------------------
    // ユニット（階層レベルを取得する）
    //-------------------------------------------------------------------
    function get_emp_unitlevel($group_id) {

        // ユニット表示の階層レベルを取得する
        $sql = "select unit_disp_level from duty_shift_group ";
        $cond = " where group_id = '$group_id'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel !== 0) {
            $ret_level = pg_fetch_result($sel, 0, "unit_disp_level");
        } else {
            $ret_level = '3';
        }


        return $ret_level;
    }

    // 20140128 start
    //-------------------------------------------------------------------
    // 勤務パターングループ毎の勤務記号件数
    //-------------------------------------------------------------------
    function get_arr_duty_shift_pattern_cnt() {

        $sql = "select pattern_id, count(*) as cnt from duty_shift_pattern ";
        $cond = " group by pattern_id order by pattern_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_duty_shift_pattern_cnt = array();
        while ($row = pg_fetch_array($sel)) {
            $key = $row["pattern_id"];
            $arr_duty_shift_pattern_cnt[$key] = $row["cnt"];
        }

        return $arr_duty_shift_pattern_cnt;
    }

    function get_pattern_cnt_check_flg() {
        if (strpos($this->file_name, "duty_shift_menu.php") !== false ||
                strpos($this->file_name, "duty_shift_results.php") !== false ||
                strpos($this->file_name, "atdbk_duty_shift.php") !== false) {
            $tmgrpchk_flg = true;
        } else {
            $tmgrpchk_flg = false;
        }
        return $tmgrpchk_flg;
    }

    // 20140128 end
    // 20140220 start
    /**
     * get_arr_pattern_id
     * 応援追加されている出勤パターングループ取得
     *
     * @param mixed $group_id シフトグループ
     * @param mixed $duty_yyyy シフトグループ年
     * @param mixed $duty_mm 月
     * @param mixed $arr_emp_id 応援追加職員
     * @param mixed $pattern_id シフトグループのパターングループID
     * @return mixed 勤務パターングループの配列
     *
     */
    function get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, $arr_emp_id, $pattern_id) {

        $arr_id = array();
        if ($group_id == "") {
            return $arr_id;
        }
        //対象月の病棟に登録された職員が、別の病棟に追加されているか確認し、その病棟の勤務パターングループを取得
        //応援追加対応で、職員IDを追加で確認。月に未登録だが職員設定にいる場合もあるのでそれも取得。
        //応援追加職員
        $cond_empstr = "";
        $emp_cnt = count($arr_emp_id);
        if ($emp_cnt > 0) {
            foreach ($arr_emp_id as $emp_id) {
                if ($emp_id != "") {
                    if ($cond_empstr != "") {
                        $cond_empstr .= ",";
                    }
                    $cond_empstr .= "'" . $emp_id . "'";
                }
            }
        }
        //月別
        $sql = "select distinct(pattern_id) from duty_shift_group ";
        $cond = "where group_id in ( "
                . " select distinct(b.group_id) from duty_shift_plan_staff b where b.duty_yyyy = $duty_yyyy and b.duty_mm = $duty_mm "
                . " and (exists (select a.emp_id from duty_shift_plan_staff a "
                . "  where a.group_id = '$group_id' and a.emp_id = b.emp_id "
                . " and a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm)) ";

        if ($cond_empstr != "") {
            $cond .= "    or (b.emp_id in ($cond_empstr) and b.duty_yyyy = $duty_yyyy and b.duty_mm = $duty_mm) ";
        }
        $cond .= "  ) ";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            if ($row["pattern_id"] != $pattern_id) {
                $arr_id[] = $row["pattern_id"];
            }
        }
        //職員登録
        $sql = "select distinct(pattern_id) from duty_shift_group ";
        $cond = "where group_id in ( "
                . " select distinct(b.group_id) from duty_shift_staff b where "
                . " (exists (select a.emp_id from duty_shift_plan_staff a "
                . "  where a.group_id = '$group_id' and a.emp_id = b.emp_id "
                . " and a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm)) ";

        if ($cond_empstr != "") {
            $cond .= "    or (b.emp_id in ($cond_empstr)) ";
        }
        $cond .= "  ) ";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            if ($row["pattern_id"] != $pattern_id) {
                if (!in_array($row["pattern_id"], $arr_id)) {
                    $arr_id[] = $row["pattern_id"];
                }
            }
        }

        return $arr_id;
    }

    // 20140220 end

    /**
     * 集計設定の桁数に4桁の数値があるか確認
     * 　集計設定Aに小数以下2位(例7.75)
     * @param mixed $patern_id 勤務パターングループID
     * @return false:なし、true:あり
     *
     */
    function is_exist_len4_sign_count_value($patern_id) {

        $sql = "SELECT * FROM duty_shift_sign_count_value ";
        $cond = "where group_id=$patern_id and length(count_value) >= 4 limit 1 ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $num = pg_num_rows($sel);

        return ($num == 1);
    }

    /**
     * Excel印刷パラメータ指定画面の設定保存用のキー情報
     *
     * @return 「configテーブルkey」と「入力項目の名前の配列」
     *
     */
    function get_x5para_conf_key() {
        //configテーブルのkey => inputの名前
        $arr_key = array("BlancSet" => "excel_blancrows"
            , "weekColor" => "excel_weekcolor"
            , "hopeColor" => "excel_hopecolor"
            , "setFitPage" => "excel_setfitpage"
            , "ratio" => "excel_ratio"
            , "setPapaerSize" => "excel_paper_size"
            , "marginTop" => "excel_mtop"
            , "marginLeft" => "excel_mleft"
            , "marginHead" => "excel_mhead"
            , "marginBottom" => "excel_mbottom"
            , "marginRight" => "excel_mright"
            , "marginFoot" => "excel_mfoot"
            , "setRowCount" => "excel_row_count"
            , "rowType" => "excel_row_type"
            , "selectweek" => "excel_date"
            , "selectmonth" => "excel_yearmonth"
            , "week4start" => "excel_week4start"
            , "event" => "excel_event"
        );
        return $arr_key;
    }

    /**
     * グループ別設定登録
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $key キー
     * @param mixed $val 値
     * @return なし
     *
     */
    function conf_insert($group_id, $key, $val) {
        $sql = "insert into duty_shift_group_config (group_id, key, value ";
        $sql .= ") values (";
        $content = array($group_id, $key, p($val));
        $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
        if ($ins == 0) {
            pg_query($this->_db_con, "rollback");
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    /**
     * グループ別設定更新
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $key キー
     * @param mixed $val 値
     * @return なし
     *
     */
    function conf_update($group_id, $key, $val) {
        $sql = "update duty_shift_group_config set";
        $set = array("value");
        $setvalue = array(p($val));
        $cond = "where group_id='$group_id' and key='$key'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con, "rollback");
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    /**
     * グループ別設定削除
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $key キー、省略時はシフトグループIDのみ条件とする
     * @return なし
     *
     */
    function conf_delete($group_id, $key = "") {
        $sql = "delete from duty_shift_group_config ";
        $cond = "where group_id='$group_id' ";
        if ($key != "") {
            $cond .= " and key='$key' ";
        }
        $del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($del == 0) {
            pg_query($this->_db_con, "rollback");
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    /**
     * グループ別設定取得
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $key キー
     * @return 設定値
     *
     */
    function conf_get($group_id, $key) {
        $sql = "select value from duty_shift_group_config ";
        $cond = "where group_id='$group_id' and key='$key'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) == 0) {
            return null;
        }
        return pg_fetch_result($sel, 0, "value");
    }

    /**
     * グループ別設定
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $key キー
     * @param mixed $val 値
     * @return なし
     *
     */
    function conf_set($group_id, $key, $newval) {
        $sql = "select value from duty_shift_group_config ";
        $cond = "where group_id='$group_id' and key='$key'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        if (pg_num_rows($sel) == 0) {
            $this->conf_insert($group_id, $key, $newval);
        } else {
            $val = pg_fetch_result($sel, 0, "value");
            if ($newval != $val) {
                $this->conf_update($group_id, $key, $newval);
            }
        }
    }

    /**
     * グループ別設定をまとめて取得
     *
     * @param mixed $group_id シフトグループID
     * @return 値を設定した配列
     *
     */
    function conf_get_group($group_id) {
        $sql = "select key, value from duty_shift_group_config ";
        $cond = "where group_id='$group_id' ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $base_len = strlen($key);
        $arr_val = array();
        while ($row = pg_fetch_array($sel)) {
            $arr_val[$row["key"]] = $row["value"];
        }
        return $arr_val;
    }

    /*     * ********************************************************************** */

    // 行事予定(event)情報取得
    // @param   $group_id       シフトグループ
    // @param   $start_date     検索開始日
    // @param   $end_date       検索終了日
    //
    // @return  $data   取得情報(日付をキーとする配列)
    /*     * ********************************************************************** */
    function get_event_data($group_id, $start_date, $end_date) {
        //-------------------------------------------------------------------
        //検索
        //-------------------------------------------------------------------
        $sql = "select * from duty_shift_event";
        $cond = "where group_id = '$group_id' and date >= '$start_date' ";
        $cond .= "and date <= '$end_date' ";
        $cond .= "order by date";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        for ($i = 0; $i < $num; $i++) {
            $wk_date = pg_fetch_result($sel, $i, "date");

            $data["$wk_date"]["event1"] = pg_fetch_result($sel, $i, "event1");
            $data["$wk_date"]["event2"] = pg_fetch_result($sel, $i, "event2");
            $data["$wk_date"]["event3"] = pg_fetch_result($sel, $i, "event3");
            $data["$wk_date"]["event4"] = pg_fetch_result($sel, $i, "event4");
            if ($data["$wk_date"]["event1"] != "" ||
                    $data["$wk_date"]["event2"] != "" ||
                    $data["$wk_date"]["event3"] != "" ||
                    $data["$wk_date"]["event4"] != "") {
                $mark_flg = "1";
            } else {
                $mark_flg = "0";
            }
            $data["$wk_date"]["mark_flg"] = $mark_flg;
        }

        return $data;
    }

}
