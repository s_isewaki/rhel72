<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?
ini_set("max_execution_time", 0);
require_once("about_comedix.php");
require_once("duty_shift_yui_calendar_util.ini");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_menu_common_class.php");
require_once("duty_shift_auto_common_class.php");
require_once("duty_shift_auto_bunsan_common_class.php");
require_once("duty_shift_time_common_class.php");
require_once("duty_shift_check_common_class.php");
require_once("date_utils.php");
//require_once("get_menu_label.ini");
require_once("show_class_name.ini");    // ユニット（３階層名）表示のため追加

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

///-----------------------------------------------------------------------------
// 自動シフト処理での再表示時は子ウィンドウを出す
///-----------------------------------------------------------------------------
if ($auto_flg == "1") {
    $childwin_auto_sts = "1";
?>
<script type="text/javascript">
    var wx = 380;
    var wy = 260;
    var dx = screen.width / 2 - (wx / 2);
    var dy = screen.top;
    var base = screen.height / 2 - (wy / 2);
    var args = new Array();
    args[0] = 'start 0';
    args[1] = 'start 1';
    var url = 'duty_shift_menu_auto_sts.php';
    url += '?session=<?=$session?>';
    childwin_auto_sts = window.open(url, 'AutoPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin_auto_sts.focus();
    for(var c=0;c<3000;c++){ // 子ウィンドウが立ち上がるまで待つ
        var cid = childwin_auto_sts.document.getElementById('display1');
        if( cid ){
            break;
        }
    }
    // --- javascript end
</script>

<?
}

///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// 勤務シフト作成
//$shift_menu_label = get_shift_menu_label($con, $fname);
$shift_menu_label = "勤務シフト作成";
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_menu = new duty_shift_menu_common_class($con, $fname);
$obj_time = new duty_shift_time_common_class($con, $fname);
$obj_report = new duty_shift_report_common_class($con, $fname);    //届出書添付書類画面 様式９ 共通部品ＣＬＡＳＳ
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
$old_data_cnt = $data_cnt;
///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];
//初期表示を翌月にする 2008/11/07
$now_mm = $now_mm + 1;
if ($now_mm >= 13) {
    $now_yyyy = $now_yyyy + 1;
    $now_mm = 1;
}
$first_flg = ($duty_mm == ""); //初期表示時、月が未指定の場合 20140529

///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
if ($duty_mm == "") { $duty_mm = $now_mm; }
///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);

//表示期間外の分を表示する 20130307
$extra_day =0;
$reappear_flg = false;
if ($week_index >= 0 && $week_index != null){
        $data_cnt = "";
        $reappear_flg = true;
        $extra_day =  $week_index * 7;
}

///-----------------------------------------------------------------------------
//各変数値の初期値設定
///-----------------------------------------------------------------------------
$rslt_flg = "";             //表示ＨＩＤＤＥＮ判定フラグ（１：予定、２：実績）
$highlight_flg = "1";       //強調表示フラグ（１：強調表示）
$draft_flg = "";            //下書きフラグ（１：下書き）
$person_charge_id = "";     //シフト管理者（責任者）
$caretaker_id = "";         //シフト管理者（代行者）
$caretaker2_id = "";        //シフト管理者（代行者）
$caretaker3_id = "";        //シフト管理者（代行者）
$caretaker4_id = "";        //シフト管理者（代行者）
$caretaker5_id = "";
$caretaker6_id = "";
$caretaker7_id = "";
$caretaker8_id = "";
$caretaker9_id = "";
$caretaker10_id = "";
$check_show_flg = "1";      //警告行表示フラグ（１：表示）
$staff_del_flg = "1";       //スタッフ削除可能フラグ（１：可能）
if ($data_cnt == "") { $data_cnt = 0; }
if ($add_staff_cnt == "") { $add_staff_cnt = 0; }

// 件数確認用に退避
$wk_add_staff_cnt = $add_staff_cnt;
$wk_del_staff_id = $del_staff_id;
//入力形式で表示する日（開始／終了）
if ($edit_start_day == "") { $edit_start_day = 0; }
if ($edit_end_day == "") { $edit_end_day = 0; }

$individual_flg = "";

//４週計算用のカレンダー年月日
if ($date_y1 == "") { $date_y1 = $duty_yyyy; };
if ($date_m1 == "") { $date_m1 = $duty_mm; };
if ($date_d1 == "") { $date_d1 = 1; };
$night_start_day = $date_d1;

$err_msg_1 = "";
$err_msg_2 = "";

//２行目表示データフラグ（１：勤務実績、２：勤務希望、３：当直、４：コメント）
$show_data_flg = "";
if ($plan_results_flg == "2") { $show_data_flg = "1"; }     //勤務実績表示
if ($plan_hope_flg == "2") { $show_data_flg = "2"; }        //勤務希望表示
if ($plan_duty_flg == "2") { $show_data_flg = "3"; }        //当直表示
if ($plan_comment_flg == "2") { $show_data_flg = "4"; }     //コメント表示

//表示行数
$show_gyo_cnt = 1;
if (($plan_results_flg == "2") ||
        ($plan_hope_flg == "2") ||
        ($plan_duty_flg == "2") ||
        ($plan_comment_flg == "2")) {
    $show_gyo_cnt = 2;
}
//登録状態、希望非表示時のデータ登録用 1:下書き 2:登録済み "":未登録
$draft_reg_flg = "";

//休暇詳細 1:表示 0:非表示 "":詳細用の処理不要
if ($hol_dt_flg == "") { $hol_dt_flg = "0"; }

// ユニット（３階層名）表示のため追加
$arr_class_name = get_class_name_array($con, $fname);

///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

//職員設定取込ボタン
if ($staff_set_add_flg == "1") {
    //グループIDが未設定時は、hiddenから設定
    if ($group_id == "") {
        $group_id = $cause_group_id;
    }
    if ($group_id != "") {
        //職員設定から追加された職員を取得する
        $add_staff_id = $obj->get_add_staffset($group_id, $staff_id, $add_staff_id_all);
    } else {
        $add_staff_id = "";
    }
    if ($add_staff_id != "") {
        $wk_emp_id_array = explode(",", $add_staff_id);   //追加職員ID
        $add_staff_cnt = count($wk_emp_id_array);   //追加職員数
        $wk_add_staff_cnt = $add_staff_cnt;
    }
}

///-----------------------------------------------------------------------------
//ＤＢ(stmst)より役職情報取得
//ＤＢ(jobmst)より職種情報取得
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務グループ情報を取得
///-----------------------------------------------------------------------------
$data_st = $obj->get_stmst_array();
$data_job = $obj->get_jobmst_array();

$data_wktmgrp = $obj->get_wktmgrp_array();
///-----------------------------------------------------------------------------
// 勤務シフトグループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", "", $data_wktmgrp);
if (count($group_array) <= 0) {
    $err_msg_1 = "勤務シフトグループ情報が未設定です。管理画面で登録してください。";
} else {
    // 更新権限あるグループ取得 20140210 //職員設定、応援追加されていても含めない
    $group_array = $obj->get_valid_group_array($group_array, $emp_id, $duty_yyyy, $duty_mm);
    // 1件もない場合
    if (count($group_array) <= 0) {
        $err_msg_1 = "シフト管理者として登録されたシフトグループがありません。管理画面＞シフトグループ一覧＞シフトグループ登録画面で登録してください。";
    }
    else {
        // 通常画面の場合
        if ($fullscreen_flg != "1"){
            if ($group_id == ""){
                $group_id = $group_array[0]["group_id"];
            } else {
                //職員設定から移動してきた場合の対応 20100827
                $group_id_flg = false;

                //権限、職員設定されているか確認
                for ($i=0; $i<count($group_array); $i++) {
                    if ($group_id == $group_array[$i]["group_id"]) {
                        $group_id_flg = true;
                        break;
                    }
                }
                //権限がないグループの場合、先頭のグループにする
                if (!$group_id_flg) {
                    $group_id = $group_array[0]["group_id"];
                }
            }
        } else {
            // 全画面の場合
            $group_id = $cause_group_id;
        }
    }
}
//対象グループのシフト管理者を設定
for ($i=0;$i<count($group_array);$i++) {
    if ($group_id == $group_array[$i]["group_id"]) {
        $standard_id = $group_array[$i]["standard_id"];                     //施設基準
        $person_charge_id = $group_array[$i]["person_charge_id"];           //シフト管理者ID（責任者）
        $caretaker_id = $group_array[$i]["caretaker_id"];                   //シフト管理者ID（代行者）
        $caretaker2_id = $group_array[$i]["caretaker2_id"];
        $caretaker3_id = $group_array[$i]["caretaker3_id"];
        $caretaker4_id = $group_array[$i]["caretaker4_id"];
        $caretaker5_id = $group_array[$i]["caretaker5_id"];
        $caretaker6_id = $group_array[$i]["caretaker6_id"];
        $caretaker7_id = $group_array[$i]["caretaker7_id"];
        $caretaker8_id = $group_array[$i]["caretaker8_id"];
        $caretaker9_id = $group_array[$i]["caretaker9_id"];
        $caretaker10_id = $group_array[$i]["caretaker10_id"];
        $duty_upper_limit_day = $group_array[$i]["duty_upper_limit_day"];   //連続勤務日数上限
        $start_month_flg1 = $group_array[$i]["start_month_flg1"];
        $start_day1 = $group_array[$i]["start_day1"];
        $month_flg1 = $group_array[$i]["month_flg1"];
        $end_day1 = $group_array[$i]["end_day1"];
        $start_day2 = $group_array[$i]["start_day2"];
        $month_flg2 = $group_array[$i]["month_flg2"];
        $end_day2 = $group_array[$i]["end_day2"];
        $need_less_chk_flg = $group_array[$i]["need_less_chk_flg"];
        $need_more_chk_flg = $group_array[$i]["need_more_chk_flg"];
        $reason_setting_flg = $group_array[$i]["reason_setting_flg"];
        $seven_work_chk_flg = $group_array[$i]["seven_work_chk_flg"];   //7日連続勤務チェック　20131107
        $part_week_chk_flg = $group_array[$i]["part_week_chk_flg"];     //非常勤の週間勤務時間数チェック
        break;
    }
}
if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
if ($end_day1 == "" || $end_day1 == "0") {  $end_day1 = "99";}
if ($start_day2 == "" || $start_day2 == "0") {  $start_day2 = "0";}
if ($month_flg2 == "" || $end_day2 == "") { $month_flg2 = "1";}
if ($end_day2 == "" || $end_day2 == "0") {  $end_day2 = "99";}

$seven_work_chk_flg = ($seven_work_chk_flg == "") ? "f" : $seven_work_chk_flg; //7日連続勤務チェック
$part_week_chk_flg = ($part_week_chk_flg == "") ? "f" : $part_week_chk_flg; //非常勤の週間勤務時間数チェック

if ($start_day2 != "0" && $end_day2 != "0") {
    $term_btn_flg = "1";
    // 期間変更ボタンが押された場合、切り替える
    if ($term_chg_flg == "1") {
        $term_chg_id = ($term_chg_id != "1") ? "1" : "2";
    } else {
        $term_chg_id = ($term_chg_id == "") ? "1" : $term_chg_id;
    }
} else {
    $term_btn_flg = "0";
    $term_chg_id = "1";
}

///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);

//初回で年月が未指定の場合のみ、当日が範囲に入るように年月を調整して、翌月を決定する
if ($first_flg) {
    //当月の範囲
    $arr_date_thismonth = $obj->get_term_date($date["year"], $date["mon"], $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    $today = date("Ymd");
    //当日が範囲より前の場合は、前月とする
    if ($today < $arr_date_thismonth[0]) {
        $duty_mm--;
        if ($duty_mm < 1) {
            $duty_yyyy--;
            $duty_mm = 12;
        }
        $arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    } else if ($today > $arr_date_thismonth[1]) {
        //当日が範囲より後の場合は、翌月とする
        $duty_mm++;
        if ($duty_mm > 12) {
            $duty_yyyy++;
            $duty_mm = 1;
        }
        $arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    }
}

// 有効なグループの職員を取得
//$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm);
// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);
// 追加がある場合、取得
$add_staff_id_all_array = explode(",", $add_staff_id_all);
$add_staff_id_all_cnt = count($add_staff_id_all_array);
$wk_array = array();
if ($add_staff_cnt > 0 || $add_staff_id_all_cnt > 0) {
    $wk_emp_id_array = explode(",", $add_staff_id);
    $wk_array = array_merge($add_staff_id_all_array, $wk_emp_id_array);
    $wk_cnt = count($data_emp);
    $add_staff_id_all = "";
    foreach ($wk_array as $wk_add_staff_id) {
        if ($wk_add_staff_id == "") {
            continue;
        }
        $wk_data_emp = $obj->get_empmst_array($wk_add_staff_id);
        $data_emp[$wk_cnt]["id"]    = $wk_data_emp[0]["id"];
        $data_emp[$wk_cnt]["name"]  = $wk_data_emp[0]["name"];
        $data_emp[$wk_cnt]["job_id"]    = $wk_data_emp[0]["job_id"];
        $data_emp[$wk_cnt]["st_id"]     = $wk_data_emp[0]["st_id"];
        $data_emp[$wk_cnt]["join"]  = $wk_data_emp[0]["join"];
        $wk_cnt++;

        if ($add_staff_id_all != "") {
            $add_staff_id_all .= ",";
        }
        // 削除の場合は除く
        if ($del_staff_id != $wk_add_staff_id) {
            $add_staff_id_all .= $wk_add_staff_id;
        }
    }
}

$start_date = $arr_date[0];
$end_date = $arr_date[1];

//連続勤務or週間勤務時間チェック 20131107
$warning_flag = 0;
if (($seven_work_chk_flg == "t") || ($part_week_chk_flg == "t")) {
    $warning_flag = 1;
}

if ($warning_flag == 1) {
    $chk_start_day= date("Ymd", strtotime("-6 day",date_utils::to_timestamp_from_ymd($start_date) ));
    $chk_calendar_array = $obj->get_calendar_array($chk_start_day, $end_date);
    $chk_day_cnt=count($chk_calendar_array);
}

$origin_start_date = $start_date;
if ($reappear_flg){     //表示期間の開始日 20130307
    $start_date= date("Ymd", strtotime("-$extra_day day",date_utils::to_timestamp_from_ymd($start_date) ));
}

$calendar_array = $obj->get_calendar_array($start_date, $end_date);
$day_cnt=count($calendar_array);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
$tmp_date = $start_date;
for ($k=1; $k<=$day_cnt; $k++) {
    $tmp_date = strtotime($tmp_date);
    $week_array[$k]["name"] = $obj->get_weekday($tmp_date);
    if (($week_array[$k]["name"] != "土") &&
            ($week_array[$k]["name"] != "日")) {
        if (($calendar_array[$k - 1]["type"] == "4") ||
                ($calendar_array[$k - 1]["type"] == "5") ||
                ($calendar_array[$k - 1]["type"] == "6")) {
            $week_array[$k]["type"] = "6";
        }
    }
    $tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
}

//権限チェック
$create_flg = "";
if (($emp_id == $person_charge_id) || ($emp_id == $caretaker_id) || ($emp_id == $caretaker2_id) || ($emp_id == $caretaker3_id) || ($emp_id == $caretaker4_id) || ($emp_id == $caretaker5_id) || ($emp_id == $caretaker6_id) || ($emp_id == $caretaker7_id) || ($emp_id == $caretaker8_id) || ($emp_id == $caretaker9_id) || ($emp_id == $caretaker10_id)) {
    $create_flg = "1";
}
///-----------------------------------------------------------------------------
//指定月の全曜日を設定（別枠で作成）20131225 START
///-----------------------------------------------------------------------------
$week_array2 = array();
$tmp_date = $start_date;
for ($k=1; $k<=$day_cnt; $k++) {
    $tmp_date = strtotime($tmp_date);
    //曜日を取得
    $week_array2[$k]["name"] = $obj->get_weekday($tmp_date);

    // 曜日を配列に設定（平日=1  土曜=6  日曜=7）
    if (($week_array2[$k]["name"] != "土") || ($week_array2[$k]["name"] != "日")) {
        $week_array2[$k]["type"] = 1;
    }
    if ($week_array2[$k]["name"] == "土") {
        $week_array2[$k]["type"] = 6;
    }
    if ($week_array2[$k]["name"] == "日") {
        $week_array2[$k]["type"] = 7;
    }
    // 祝日・年末年始（上書き）
    if (($calendar_array[$k - 1]["type"] == "6") || ($calendar_array[$k - 1]["type"] == "7")) {
        $week_array2[$k]["type"] = 8;
    }
    // 法定休・所定休（無視）
    if (($calendar_array[$k - 1]["type"] == "4") || ($calendar_array[$k - 1]["type"] == "5") ) {
        ;
    }
    $tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
}
///-----------------------------------------------------------------------------
//指定月の全曜日を設定（別枠で作成）20131225 END
///-----------------------------------------------------------------------------

///-----------------------------------------------------------------------------
// 禁止組み合わせ情報を取得
// 連続勤務シフト上限情報を取得
///-----------------------------------------------------------------------------
$NG_pattern_array = $obj->get_duty_shift_group_NG_pattern_array($group_id);
$upper_limit_array = $obj->get_duty_shift_group_upper_limit_array($group_id);
// 勤務シフト間隔情報を取得
$interval_array = $obj->get_duty_shift_group_interval_array($group_id);
//事由情報取得
$arr_group_pattern_reason = $obj->get_pattern_reason_array();
///-----------------------------------------------------------------------------
//必要人数情報（カレンダー）の取得
///-----------------------------------------------------------------------------
$sql = "select count(*) as cnt from duty_shift_need_cnt_calendar";
$cond = "where group_id = '$group_id' ";
$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$need_cnt = pg_fetch_result($sel, 0, "cnt");
///-----------------------------------------------------------------------------
// 施設基準情報を取得
///-----------------------------------------------------------------------------
$prov_start_hhmm = "";
$prov_end_hhmm = "";
if ($standard_id != "") {
    $standard_array = $obj->get_duty_shift_institution_standard_array($standard_id);
    if (count($standard_array) > 0) {
        $prov_start_hhmm = $standard_array[0]["prov_start_hhmm"];   //開始時分（夜勤時間帯）
        $prov_end_hhmm = $standard_array[0]["prov_end_hhmm"];       //終了時分（夜勤時間帯）
        $limit_time = $standard_array[0]["limit_time"];             //月平均夜勤時間上限
        $labor_cnt = $standard_array[0]["labor_cnt"];               //常勤職員の所定労働時間
        //申し送り時間
        for($i=0; $i<3; $i++) {
            $k = $i + 1;
            $send_start_hhmm[$k] = $standard_array[0]["send_start_hhmm_$k"];    //開始時分
            $send_end_hhmm[$k] = $standard_array[0]["send_end_hhmm_$k"];        //終了時分
        }
    }
}
///-----------------------------------------------------------------------------
// 指定グループの出勤パターン(atdptn)情報を取得
///-----------------------------------------------------------------------------
//勤務シフトグループ情報ＤＢより情報取得
$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
$pattern_id = "";
if (count($wk_group) > 0) {
    $pattern_id = $wk_group[0]["pattern_id"];
    $four_week_chk_flg = $wk_group[0]["four_week_chk_flg"];
    $chk_start_date_db = $wk_group[0]["chk_start_date"];
    $paid_hol_disp_flg = $wk_group[0]["paid_hol_disp_flg"];    //有給休暇残日数表示フラグ

    // 集計行表示位置
    $count_row = $wk_group[0]['display']["count_row_position"] + 0;
}

// ４週８休の線位置
$four_week_line = 0;

if ($four_week_chk_flg == "t") {

    //基準日からの日数
    $diff_days = date_utils::get_time_difference($chk_start_date_db."0000", $start_date."0000") / (24 * 60);
    if ($start_date < $chk_start_date_db) {
        $diff_days = $diff_days * (-1);
    }
    //28日単位の回数
    $four_week_times = floor($diff_days / 28);
    //基準日から28日単位の開始日を取得
    $wk_st_date = date_utils::add_day_ymdhi($chk_start_date_db."0000", $four_week_times * 28);
    $chk_start_date = substr($wk_st_date, 0, 8);

    //開始日が範囲内になるまで28日を足す
    while ($chk_start_date < $calendar_array[0]["date"]) {
        $wk_st_date = date_utils::add_day_ymdhi($chk_start_date."0000", 28);
        $chk_start_date = substr($wk_st_date, 0, 8);
    }
    // DBの開始基準日が終了日以前の場合
    if ($chk_start_date_db <= $end_date) {
        // ４週８休の線位置
        if ($chk_start_date >= $calendar_array[0]["date"] &&
                $chk_start_date <= $calendar_array[$day_cnt-1]["date"]) {
            for ($i=0; $i<$day_cnt; $i++) {
                if ($chk_start_date == $calendar_array[$i]["date"]) {
                    $four_week_line = $i + 1;
                    break;
                }
            }
        }
    } else {
        // 開始基準日以前はチェックしない
        $four_week_chk_flg = "f";
    }
}

///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
//20140220 start
$data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
//応援追加されている出勤パターングループ確認
$arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, $wk_array, $pattern_id);
$pattern_id_cnt = count($arr_pattern_id);
//$group_emp_add_cnt = $obj->get_add_emp_cnt($group_id, $duty_yyyy, $duty_mm);
echo("<!-- 応援パターングループ $pattern_id_cnt -->\n");
if ($pattern_id_cnt > 0) {
    $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
    $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
    $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
    $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
}
//応援なしの場合、同じパターングループのデータを設定
else {
    $data_atdptn_all = $data_atdptn;
    $data_pattern_all = $data_pattern;
}
//20140220 end

///-----------------------------------------------------------------------------
//必要人数情報（カレンダー）の取得
///-----------------------------------------------------------------------------
$need_array = $obj->get_duty_shift_need_cnt_calendar_array(
        $group_id, $duty_yyyy, $duty_mm, $day_cnt,
        $data_job, $data_atdptn, $week_array, $calendar_array);
///-----------------------------------------------------------------------------
// 勤務シフト情報を取得判定（新規表示／再表示）
///-----------------------------------------------------------------------------
$plan_array = array();
$set_data = array();
$chk_set_data = array();
$wk_flg == "";
if (($data_cnt > 0 ||
    ($data_cnt == 0 && $add_staff_cnt > 0) //職員設定数が０で応援追加する場合 20140407
) &&
        ($group_id == $cause_group_id) &&
        ($duty_yyyy == $cause_duty_yyyy) &&
        ($duty_mm == $cause_duty_mm)) {
    $wk_flg = "1";
} else {
    $edit_start_day = 0;
    $edit_end_day = 0;

    $recomputation_flg = "";
    $date_y1 = $duty_yyyy;
    $date_m1 = $duty_mm;
    $date_d1 = 1;
    $night_sumtime = "";
}
//更新時用情報 20090611
if ($wk_flg == "1") {
    if ($del_staff_id != "") {
        $delete_id_cnt++;
    }
} else {
    $delete_id_cnt = 0;
    $upd_flg = array();
    $rslt_upd_flg = array(); //実績と希望の2段目用
    $cmt_upd_flg = array();
}

///-----------------------------------------------------------------------------
//再表示時
///-----------------------------------------------------------------------------
if ($wk_flg == "1") {

    //前週を表示する場合 20130318
    $extra_day2 = 0;
    if ($week_index == "" && $before_week != 0) {
        $extra_day2 =  $before_week * 7;
    }

    //連続勤務or週間勤務時間数チェック 20131107
    $chk_count = ($warning_flag == 1) ? 2 : 1;

    for($t=0; $t < $chk_count; $t++) {
        //1回目通常データ、2回目チェック用データ 20131107
        if ($t == 0){
            $tmp_day_cnt = $day_cnt;
            $tmp_extra_day2 = $extra_day2;
        }else{
            $tmp_day_cnt = $day_cnt + 6;
            $tmp_extra_day2 = $extra_day2 - 6;
        }

        $m=0;
        $wk_data = array();
        for($i=0;$i<$data_cnt;$i++) {
            //応援追加情報
            $assist_str = "assist_group_$i";
            $arr_assist_group = explode(",", $$assist_str);
            //希望分
            if ($show_data_flg == "2") {
                $rslt_assist_str = "rslt_assist_group_$i";
                $arr_rslt_assist_group = explode(",", $$rslt_assist_str);
            }
            ///-----------------------------------------------------------------------------
            //既存データの追加
            ///-----------------------------------------------------------------------------
            for($k=1;$k<=$tmp_day_cnt;$k++) {
                if ($del_staff_id != $staff_id[$i]) {
                    $wk_data[$i]["plan_rslt_flg"] = "1";        //１：実績は再取得
                    //作成期間変更時はずらして設定
                    if ($term_chg_flg == "1") {
                        //期間１
                        if ($term_chg_id == "1") {
                            $wk_diff = $start_day1 - $start_day2;
                            //設定元位置
                            $src_k = $k + $wk_diff;
                            //範囲外は設定不要
                        } else {
                            //期間２
                            $wk_diff = $start_day2 - $start_day1;
                            $src_k = $k + $wk_diff;
                        }
                        //範囲外は設定不要
                        if ($src_k <= 0) {
                            continue;
                        }
                    } else {
                        $src_k = $k + $tmp_extra_day2;
                    }
                    $wk2 = "pattern_id_$i" . "_" . $src_k;
                    $wk3 = "atdptn_ptn_id_$i" . "_" . $src_k;
                    $wk4 = "individual_flg_$i" . "_" . $src_k;
                    $wk5 = "comment_$i" . "_" . $src_k;
                    $wk6 = "reason_2_$i" . "_" . $src_k;
                    // 勤務シフト希望のデータ
                    $wk7 = "rslt_pattern_id_$i" . "_" . $src_k;
                    $wk8 = "rslt_id_$i" . "_" . $src_k;
                    $wk9 = "rslt_reason_2_$i" . "_" . $src_k;

                    $wk_data[$m]["staff_id"] = $staff_id[$i];                   //職員ＩＤ
                    $wk_data[$m]["pattern_id_$k"] = $$wk2;                      //出勤グループＩＤ

                    if ((int)substr($$wk3,0,2) > 0) {
                        $wk_data[$m]["atdptn_ptn_id_$k"] = (int)substr($$wk3,0,2);  //出勤パターンＩＤ
                    }
                    if ((int)substr($$wk3,2,2) > 0) {
                        $wk_data[$m]["reason_$k"] = (int)substr($$wk3,2,2);     //事由
                    }

                    $wk_data[$m]["reason_2_$k"] = $$wk6;                        //事由（午前有給／午後有給）
                    $wk_data[$m]["individual_flg_$k"] = $$wk4;                  //希望取込データフラグ（空白以外：希望取込データ）
                    $wk_data[$m]["comment_$k"] = $$wk5;                         //コメント

                    if ($show_data_flg == "2") {
                        // 勤務シフト希望のデータ
                        $wk_data[$m]["rslt_pattern_id_$k"] = $$wk7;                     //出勤グループＩＤ

                        if ((int)substr($$wk8,0,2) > 0) {
                            $wk_data[$m]["rslt_id_$k"] = (int)substr($$wk8,0,2);    //出勤パターンＩＤ
                        }
                        if ((int)substr($$wk8,2,2) > 0) {
                            $wk_data[$m]["rslt_reason_$k"] = (int)substr($$wk8,2,2);        //事由
                        }

                        $wk_data[$m]["rslt_reason_2_$k"] = $$wk9;                       //事由（午前有給／午後有給）
                    }
                }

                //応援情報  20130318
                $k1 = $k + $extra_day2;
                $wk_data[$m]["assist_group_$k"] = $arr_assist_group[$k1 - 1];
                if ($show_data_flg == "2") {
                    $wk_data[$m]["rslt_assist_group_$k"] = $arr_rslt_assist_group[$k1 - 1];
                }
            }

            ///-----------------------------------------------------------------------------
            //削除スタッフか判定
            ///-----------------------------------------------------------------------------
            if ($del_staff_id != $staff_id[$i]) {
                $m++;
            }
        }

        if ($t == 0){
            ///-----------------------------------------------------------------------------
            //削除スタッフを考慮してエリア再設定
            ///-----------------------------------------------------------------------------
            for($j=0;$j<$m;$j++) {
                $set_data[$j] = $wk_data[$j];
            }
            ///-----------------------------------------------------------------------------
            //スタッフの追加
            ///-----------------------------------------------------------------------------
            if ($add_staff_cnt > 0) {
                $wk_emp_id_array = explode(",", $add_staff_id);
                foreach ($wk_emp_id_array as $wk_add_staff_id) {
                    ///-----------------------------------------------------------------------------
                    //すでに存在する職員は追加しない
                    ///-----------------------------------------------------------------------------
                    $wk_flg = "";
                    for($k=0;$k<$data_cnt;$k++) {
                        if ($set_data[$k]["staff_id"] == $wk_add_staff_id) {
                            $wk_flg = "1";
                            break;
                        }
                    }
                    ///-----------------------------------------------------------------------------
                    //データ設定
                    ///-----------------------------------------------------------------------------
                    if ($wk_flg == "") {
                        $set_data[$data_cnt]["staff_id"] = $wk_add_staff_id;
                        $set_data[$data_cnt]["add_staff_flg"] = "1";
                        $data_cnt++;
                    }
                }
            }
        }else{
            //削除スタッフを考慮してエリア再設定 20131107
            for($j=0;$j<$m;$j++) {
                $chk_set_data[$j] = $wk_data[$j];
            }
            //スタッフの追加
            if ($add_staff_cnt > 0) {
                $wk_emp_id_array = explode(",", $add_staff_id);
                foreach ($wk_emp_id_array as $wk_add_staff_id) {
                    //すでに存在する職員は追加しない
                    $wk_flg = "";
                    for($k=0;$k<$data_cnt;$k++) {
                        if ($chk_set_data[$k]["staff_id"] == $wk_add_staff_id) {
                            $wk_flg = "1";
                            break;
                        }
                    }
                    //データ設定
                    if ($wk_flg == "") {
                        $chk_set_data[$data_cnt]["staff_id"] = $wk_add_staff_id;
                        $chk_set_data[$data_cnt]["add_staff_flg"] = "1";
                        $data_cnt++;
                    }
                }
            }
        }
    }

}
//自動作成の場合（下書きを変更しないための情報取得）20130319
for ($i=0; $i<count($set_data); $i++) {
    $set_data[$i]["auto_flg"] = $auto_flg;
}

//前月考慮を含めて連続勤務チェック情報取得 20131107
if ($warning_flag == 1) {
    for ($i=0; $i<count($chk_set_data); $i++) {
        $chk_set_data[$i]["auto_flg"] = $auto_flg;
    }

    $chk_plan_array = $obj->get_duty_shift_plan_array(
            $group_id, $pattern_id,
            $duty_yyyy, $duty_mm, $chk_day_cnt,
            $individual_flg,
            $hope_get_flg,
            $show_data_flg,
            $chk_set_data, $week_array, $chk_calendar_array,
            $data_atdptn, $data_pattern_all,
            $data_st, $data_job, $data_emp);
}

///-----------------------------------------------------------------------------
// 勤務シフト情報を取得
///-----------------------------------------------------------------------------
$plan_array = $obj->get_duty_shift_plan_array(
        $group_id, $pattern_id,
        $duty_yyyy, $duty_mm, $day_cnt,
        $individual_flg,
        $hope_get_flg,
        $show_data_flg,
        $set_data, $week_array, $calendar_array,
        $data_atdptn, $data_pattern_all,
        $data_st, $data_job, $data_emp);

$data_cnt = count($plan_array);
$wk_hope_get_flg = $hope_get_flg;
//再表示利用変数クリア
$hope_get_flg = "";
$add_staff_cnt = 0;
$add_staff_id = "";
$del_staff_id = "";
///-----------------------------------------------------------------------------
// 勤務シフト情報の下書き／登録済みデータ有無
///-----------------------------------------------------------------------------
//検索条件（職員）
$cond_add = "";
for ($i=0;$i<count($plan_array);$i++) {
    $wk_id = $plan_array[$i]["staff_id"];
    if ($cond_add == "") {
        $cond_add .= "and (emp_id = '$wk_id' ";
    } else {
        $cond_add .= "or emp_id = '$wk_id' ";
    }
}
if ($cond_add != "") {
    $cond_add .= ") ";
}

//応援追加以外の時、登録状態の取得
if ($wk_add_staff_cnt == 0) {
    //下書き
    $sql = "select count(*) as cnt from duty_shift_plan_draft";
    $cond = "where 1 = 1 ";
    $cond .= "and duty_date >= '$start_date'and duty_date <= '$end_date' ";
    $cond .= $cond_add;
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num_1 = pg_fetch_result($sel, 0, "cnt");
    if ($num_1 > 0) {
        $finish_flg = "[下書き：有り]";
        $draft_reg_flg = "1";
    } else {
        //登録済み
        $sql = "select count(*) as cnt from duty_shift_plan_staff";
        $cond = "where group_id = '$group_id' ";
        $cond .= "and duty_yyyy = $duty_yyyy ";
        $cond .= "and duty_mm = $duty_mm ";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num_2 = pg_fetch_result($sel, 0, "cnt");
        if ($num_2 > 0) {
            $finish_flg = "[登録：有り]";
            $draft_reg_flg = "2";
        }
    }
    $old_draft_reg_flg = $draft_reg_flg;
} else {
    //応援追加の場合は、直前の登録状態を表示
    $draft_reg_flg = $old_draft_reg_flg;
    if ($draft_reg_flg == "1") {
        $finish_flg = "[下書き：有り]";
    } elseif ($draft_reg_flg == "2") {
        $finish_flg = "[登録：有り]";
    }
}
///-----------------------------------------------------------------------------
// 行タイトル（行ごとの集計する勤務パターン）を取得
// 列タイトル（列ごとの集計する勤務パターン）を取得
///-----------------------------------------------------------------------------
$title_gyo_array = $obj->get_total_title_array($pattern_id, "1", $data_pattern);
$title_retu_array = $obj->get_total_title_array($pattern_id, "2", $data_pattern);
if ((count($title_gyo_array) <= 0) || (count($title_retu_array) <= 0)) {
    $err_msg_2 = "勤務シフト記号情報が未設定です。管理画面で登録してください。";
}

///-----------------------------------------------------------------------------
// ユニット表示仕変
$unit_disp_level = $obj->get_emp_unitlevel($group_id);
///-----------------------------------------------------------------------------

// ユニット（３階層名）表示のため追加
$arr_class_name = get_class_name_array($con, $fname);
$unit_name = $arr_class_name[intval($unit_disp_level) - 1];

///////////////////////////
///-----------------------------------------------------------------------------
//月平均夜勤時間の設定
///-----------------------------------------------------------------------------
$view_YakinAvg = false;
if ($comp_flg == "1") {
    $view_YakinAvg = true;
    $wk_err_flg = "";
    $comp_flg = "";
}
///////////////////////////
///-----------------------------------------------------------------------------
//自動シフト作成
///-----------------------------------------------------------------------------
if ($auto_flg == "1") {

    //ｎ週前表示対応 20130319
    $start_date = $origin_start_date;
    // 職員設定->勤務条件より、職員毎の勤務形態と勤務日チェックの情報を得る。
    $emp_cond_list = array();
    for($i=0 ; $i < count($plan_array) ; $i++ ){
        $emp_cond_list[$plan_array[$i]["staff_id"]] = $obj->get_duty_shift_staff_employmen($plan_array[$i]["staff_id"]);
    }
    $obj_auto2 = new duty_shift_auto_bunsan_common_class($group_id, $con, $fname);
    $set_data = $obj_auto2->set_auto(
        $plan_array,
        $duty_yyyy,
        $duty_mm,
        $start_month_flg1,
        $start_date,
        $end_date,
        $auto_start_day,
        $auto_end_day,
        $auto_ptn_id,
        $auto_btn_flg,
        $emp_cond_list,
        $shift_case
    );

    //表示用にデータ再設定
    for ($i=0; $i<count($set_data); $i++) {
        $set_data[$i]["plan_rslt_flg"] = "1";
        $set_data[$i]["auto_flg"] = "1"; //自動作成の場合（下書きを変更しないための情報取得）20130319
    }

    //再表示
    $plan_array = $obj->get_duty_shift_plan_array(
        $group_id,
        $pattern_id,
        $duty_yyyy,
        $duty_mm,
        $day_cnt,
        $individual_flg,
        $hope_get_flg,
        $show_data_flg,
        $set_data,
        $week_array,
        $calendar_array,
        $data_atdptn,
        $data_pattern_all,
        $data_st,
        $data_job,
        $data_emp
    );
    $data_cnt = count($plan_array);
    $wk_auto_flg = $auto_flg;
    $auto_flg = "";
    $check_flg = "1";       //警告メッセージ表示フラグ（１：表示）
    $auto_shift_setsw = 1;  // 子ウィンドウを消すときのスイッチ
}
///////////////////////////
///-----------------------------------------------------------------------------
//過去月コピー
///-----------------------------------------------------------------------------
if ($copy_before_month_flg == "1") {
    ///-----------------------------------------------------------------------------
    //検索期間設定
    ///-----------------------------------------------------------------------------
    //開始年月日算出
    $wk_start_yyyy = (int)$copy_yyyy;
    $wk_start_mm = (int)$copy_mm;
    //終了年月日算出
    $arr_date = $obj->get_term_date($wk_start_yyyy, $wk_start_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    $wk_start_date = $arr_date[0];
    $wk_end_date_0 = $arr_date[1];

    //1週間プラスする
    $wk_end_date_0 = $wk_end_date_0."0000";
    $wk_end_date_0 = date_utils::add_day_ymdhi($wk_end_date_0, 7);
    $wk_end_date = substr($wk_end_date_0, 0, 8);

    $old_calendar_array = $obj->get_calendar_array($wk_start_date, $wk_end_date);
    //月またがりがあっても当月を基準とする 20110301
    /*
    ///-----------------------------------------------------------------------------
    //前月データ有無チェック
    ///-----------------------------------------------------------------------------
    $wk_duty_yyyymm = $duty_yyyy.sprintf("%02d",$duty_mm);
    $wk_next_yyyy = $duty_yyyy;
    $wk_next_mm = $duty_mm;
    //前月から当月
    if (substr($start_date, 0, 6) != $wk_duty_yyyymm) {
        $wk_next_yyyy = substr($wk_start_date, 0, 4);
        $wk_next_mm = (int)substr($wk_start_date, 4, 2);
    } elseif (substr($end_date, 0, 6) != $wk_duty_yyyymm) {
        //当月から翌月
        $wk_next_yyyy = substr($wk_end_date, 0, 4);
        $wk_next_mm = (int)substr($wk_end_date, 4, 2);
    }
    */
    $sql = "select count(*) as cnt from duty_shift_plan_staff";
    $cond = "where group_id = '$group_id' ";
//  $cond .= "and ((duty_yyyy = $wk_start_yyyy ";
    $cond .= "and (duty_yyyy = $wk_start_yyyy ";
    $cond .= "and duty_mm = $wk_start_mm) ";
//  $cond .= " or (duty_yyyy = $wk_next_yyyy ";
//  $cond .= "and duty_mm = $wk_next_mm)) ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num = pg_fetch_result($sel, 0, "cnt");
    ///-----------------------------------------------------------------------------
    //勤務シフト情報取得（前月）
    ///-----------------------------------------------------------------------------
    $cond_add = "";
    for ($i=0;$i<count($plan_array);$i++) {
        $wk_id = $plan_array[$i]["staff_id"];
        if ($cond_add == "") {
            $cond_add .= "and (a.emp_id = '$wk_id' ";
        } else {
            $cond_add .= "or a.emp_id = '$wk_id' ";
        }
    }
    if ($cond_add != "") {
        $cond_add .= ") ";
    }
    $old_plan_array = $obj->get_atdbk_atdbkrslt_array("atdbk", $cond_add, $old_calendar_array, $plan_array);
    if ((count($old_plan_array) <= 0) || ($num <= 0)){
        //エラーメッセージ
        $wk = "指定した月に勤務シフト情報が存在しません。登録後に再実行してください。";
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>alert('$wk');</script>");
        $wk_err_flg = "1";
    }
    ///-----------------------------------------------------------------------------
    //前月の曜日を設定
    ///-----------------------------------------------------------------------------
    //期間変更対応
    $old_week_array = array();
    $tmp_date = $wk_start_date;
    for ($k=1; $k<=32; $k++) {
        $tmp_date = strtotime($tmp_date);
        $old_week_array[$k]["name"] = $obj->get_weekday($tmp_date);
        $tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
    }
    ///-----------------------------------------------------------------------------
    //前月のコピー開始日を算出
    //最初の同じ曜日よりコピー
    ///-----------------------------------------------------------------------------
    for ($m=1; $m<=count($old_week_array); $m++) {
        if ($week_array[1]["name"] == $old_week_array[$m]["name"]) {
            $t = $m;
            break;
        }
    }
    ///-----------------------------------------------------------------------------
    //前月コピー
    ///-----------------------------------------------------------------------------
    $set_data = array();
    if ($wk_err_flg == "") {
        for ($i=0; $i<$data_cnt; $i++) {
            $set_data[$i]["staff_id"] = $plan_array[$i]["staff_id"];
            ///-----------------------------------------------------------------------------
            //最初の同じ曜日よりコピー
            ///-----------------------------------------------------------------------------
            for ($m=1; $m<=$day_cnt; $m++) {
                $set_data[$i]["pattern_id_$m"] = "";
                $set_data[$i]["atdptn_ptn_id_$m"] = "";
                $set_data[$i]["individual_flg_$m"] = $plan_array[$i]["individual_flg_$m"];
                $set_data[$i]["comment_$m"] = $plan_array[$i]["comment_$m"];
                ///-----------------------------------------------------------------------------
                //前月コピー
                ///-----------------------------------------------------------------------------
                if ($plan_array[$i]["individual_flg_$m"] != "" ||
                        ($plan_array[$i]["pattern_id_$m"] != "" &&
                            $plan_array[$i]["pattern_id_$m"] != $pattern_id) ||
                        $plan_array[$i]["atdptn_ptn_id_$m"] != "") {
                    //希望取り込みで設定されているデータにコピーは出来ない。
                    // 出勤パターングループIDが違う場合を条件追加(重複職員の別グループのシフトは対象外とする) 2008/11/28
                    // 設定済みのパターンは変えない 2008/11/28
                    $set_data[$i]["pattern_id_$m"] = $plan_array[$i]["pattern_id_$m"];
                    $set_data[$i]["atdptn_ptn_id_$m"] = $plan_array[$i]["atdptn_ptn_id_$m"];
                    $set_data[$i]["reason_$m"] = $plan_array[$i]["reason_$m"];
                    $set_data[$i]["reason_2_$m"] = $plan_array[$i]["reason_2_$m"];

                } else {
                    //前月コピー
                    for ($k=0; $k<count($old_plan_array); $k++) {
                        if ($plan_array[$i]["staff_id"] == $old_plan_array[$k]["staff_id"]) {
                            $p = $t + $m - 1;
                            if ($p <= count($old_calendar_array)){
                                $set_data[$i]["pattern_id_$m"] = $old_plan_array[$k]["pattern_id_$p"];
                                //応援情報  20090625
                                $set_data[$i]["assist_group_$m"] = $old_plan_array[$k]["assist_group_$p"];
                                $set_data[$i]["atdptn_ptn_id_$m"] = $old_plan_array[$k]["atdptn_ptn_id_$p"];
                                $set_data[$i]["reason_$m"] = $old_plan_array[$k]["reason_$p"];
                                $set_data[$i]["reason_2_$m"] = $old_plan_array[$k]["reason_2_$p"];
                            }
                        }
                    }
                }
            }
        }
        ///-----------------------------------------------------------------------------
        //表示用にデータ再設定
        ///-----------------------------------------------------------------------------
        for ($i=0; $i<count($set_data); $i++) {
            $set_data[$i]["plan_rslt_flg"] = "1";
        }
        ///-----------------------------------------------------------------------------
        //再表示
        ///-----------------------------------------------------------------------------
        $plan_array = $obj->get_duty_shift_plan_array(
                $group_id, $pattern_id,
                $duty_yyyy, $duty_mm, $day_cnt,
                $individual_flg,
                $hope_get_flg,
                $show_data_flg,
                $set_data, $week_array, $calendar_array,
                $data_atdptn, $data_pattern_all,
                $data_st, $data_job, $data_emp);
        $data_cnt = count($plan_array);
    }

    $wk_copy_before_month_flg = $copy_before_month_flg;
    $copy_before_month_flg = "";
}
///////////////////////////
///-----------------------------------------------------------------------------
//１日コピー
///-----------------------------------------------------------------------------
if ($copy_day_flg == "1") {
    ///-----------------------------------------------------------------------------
    //１日コピー
    ///-----------------------------------------------------------------------------
    $m = $copy_day_cause;

    $set_data = array();
    for ($i=0; $i<$data_cnt; $i++) {
        for ($k=1; $k<=$day_cnt; $k++) {
            $set_data[$i]["staff_id"] = $plan_array[$i]["staff_id"];                    //職員ＩＤ
            $set_data[$i]["pattern_id_$k"] = $plan_array[$i]["pattern_id_$k"];          //出勤グループＩＤ
            $set_data[$i]["atdptn_ptn_id_$k"] = $plan_array[$i]["atdptn_ptn_id_$k"];    //出勤パターンＩＤ
            $set_data[$i]["reason_$k"] = $plan_array[$i]["reason_$k"];                  //事由
            $set_data[$i]["reason_2_$k"] = $plan_array[$i]["reason_2_$k"];              //事由（午前休暇／午後休暇）
            $set_data[$i]["individual_flg_$k"] = $plan_array[$i]["individual_flg_$k"];  //勤務希望取込データフラグ（１：取り込みデータ）
            $set_data[$i]["comment_$k"] = $plan_array[$i]["comment_$k"];                //コメント
            if ($k == $copy_day_point) {
                $set_data[$i]["pattern_id_$k"] = $plan_array[$i]["pattern_id_$m"];
                $set_data[$i]["atdptn_ptn_id_$k"] = $plan_array[$i]["atdptn_ptn_id_$m"];
                $set_data[$i]["reason_$k"] = $plan_array[$i]["reason_$m"];
                $set_data[$i]["reason_2_$k"] = $plan_array[$i]["reason_2_$m"];
                $set_data[$i]["individual_flg_$k"] = "";
            }
        }
    }
    ///-----------------------------------------------------------------------------
    //表示用にデータ再設定
    ///-----------------------------------------------------------------------------
    for ($i=0; $i<count($set_data); $i++) {
        $set_data[$i]["plan_rslt_flg"] = "1";
    }
    ///-----------------------------------------------------------------------------
    //再表示
    ///-----------------------------------------------------------------------------
    $plan_array = $obj->get_duty_shift_plan_array(
            $group_id, $pattern_id,
            $duty_yyyy, $duty_mm, $day_cnt,
            $individual_flg,
            $hope_get_flg,
            $show_data_flg,
            $set_data, $week_array, $calendar_array,
            $data_atdptn, $data_pattern_all,
            $data_st, $data_job, $data_emp);
    $data_cnt = count($plan_array);

    $copy_day_flg = "";
}
///////////////////////////
///-----------------------------------------------------------------------------
//個人日数合計（行）を算出設定
//個人日数合計（列）を算出設定
///-----------------------------------------------------------------------------
//2段目が実績の場合、システム日以降は、行を集計しない。
if ($show_data_flg == "1") {
    $wk_rs_flg = "2";
} else {
    $wk_rs_flg = "";
}
$gyo_array = $obj->get_total_cnt_array($group_id, $pattern_id, "1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);

$retu_array = $obj->get_total_cnt_array($group_id, $pattern_id, "2", "", $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);

$title_hol_array = array();
$hol_cnt_array = array();
$title_hol_array = $obj->get_total_title_hol_array($pattern_id, $data_pattern, $data_atdptn);
//休暇事由情報
$hol_cnt_array = $obj->get_hol_cnt_array($group_id, $pattern_id, "1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn);


///-----------------------------------------------------------------------------
// テーブルレイアウト設定
///-----------------------------------------------------------------------------
$table_layout = "fixed";    //テーブルレイアウト設定
$shift_table_width = 250;    //テーブル全体の幅

//ブラウザがIEの場合html要素のdisplay:none指定でレイアウトが崩れる場合があるので、
//テーブルのレイアウトと幅を調整
$agent = getenv("HTTP_USER_AGENT");
if (preg_match("/MSIE/", $agent)) {
    if ($show_data_flg > 0) {    //2行目を表示する場合
        $table_layout = "auto";

        $wk_width = 0;
        if ($wk_group[0]["team_disp_flg"] == "t") {    //チーム表示
            $wk_width += 40;
        }

        if ($wk_group[0]["unit_disp_flg"] == "t") {    //ユニット表示
            $wk_width += 100;
        }

        if ($wk_group[0]["job_disp_flg"] == "t") {    //職種表示
            $wk_width += 120;
        }

        if ($wk_group[0]["st_disp_flg"] == "t") {    //役職表示
            $wk_width += 60;
        }

        if ($wk_group[0]["es_disp_flg"] == "t") {    //雇用・勤務形態表示 20130219
            $wk_width += 60;
        }

        if ($paid_hol_disp_flg === "t") {    //有給休暇残日数表示
            $wk_width += 30;
        }

        //個人日数
        if ($total_disp_flg == "1") {
            //集計表示なし
            $person_day_cnt = 0;
        } else {
            //集計表示あり
            $person_day_cnt = max(count($title_gyo_array), 1);
        }

        //休暇詳細
        $hol_detail_cnt = 0;
        if ($hol_dt_flg == "1") {
            for ($k=0; $k<count($title_hol_array); $k++) {
                //公休・年休非表示
                if ($title_hol_array[$k]["reason"] == "1" ||
                        $title_hol_array[$k]["reason"] == "24") {
                    continue;
                }
                $hol_detail_cnt++;
            }
        }

        //テーブル全体の幅
        if ($plan_comment_flg == "2") {
            $day_width = 40;
        } else {
            $day_width = 24;
        }
        $shift_table_width = $wk_width + $day_width * $day_cnt + 30 * $person_day_cnt + 30 * ($hol_detail_cnt + 1);

        //編集時のテーブル幅調整
        if ($edit_start_day && $edit_end_day) {
            if ($edit_start_day === $edit_end_day) {    //1日のみ編集
                $shift_table_width += 200 - $day_width;
            } else {    //週単位編集
                $shift_table_width = $wk_width + 100 * ($edit_end_day - $edit_start_day + 1) + 30 * $person_day_cnt;
            }
        }
        $shift_table_width += 550;    //調整
    }
}

//-------------------------------------------------------------------------------
// 有給休暇残日数を取得
//-------------------------------------------------------------------------------
$paid_hol_all_array = array();
if ($paid_hol_disp_flg === "t") {
    $paid_hol_all_array = $obj->get_paid_hol_rest_array($plan_array, $calendar_array, $duty_yyyy, $duty_mm, "create");
}

///-----------------------------------------------------------------------------
//警告メッセージの設定
///-----------------------------------------------------------------------------
//組み合わせ指定確認用配列
$seq_pattern_array = $obj->get_duty_shift_group_seq_pattern_array($group_id);
if ($check_flg == "1") {
    $obj_check = new duty_shift_check_common_class($group_id, $con, $fname);
    if ($register_flg != "1"){  //連続勤務or週間勤務時間チェックの場合は表示しない
    $warning_gyo_array = $obj_check->check_rows(
            $plan_array,
            $duty_yyyy,
            $duty_mm,
            $start_date,
            $end_date
            );
    $warning_retu_array = $obj_check->check_columns(
            $plan_array,
            $duty_yyyy,
            $duty_mm,
            $start_date,
            $end_date
            );
    }
    $check_flg = "";
    $tmp_check_flg = "t";
}else{
    //連続勤務or週間勤務時間チェック 20131107
    if ($warning_flag == 1) {
        $obj_check = new duty_shift_check_common_class($group_id, $con, $fname);
    }
}
//連続勤務or週間勤務時間チェック 20131107
if ($warning_flag == 1) {
    $chk_info = $obj_check->check_shift_work($seven_work_chk_flg, $part_week_chk_flg, $chk_plan_array, $chk_day_cnt,
            $chk_start_day, $end_date, $draft_reg_flg, $data_atdptn, $pattern_id, 7);
}
///////////////////////////

// 登録子画面の指定情報保存
$obj->update_last_ptn($emp_id, $last_ptn_id, $last_reason_2, $last_copy_cnt);
// 勤務記号の初期値
$def_ptn_info=$obj->get_auto_default_ptn_info($pattern_id);
if ($def_ptn_info["reason"] == " ") {
    $def_ptn_info["reason"] = "";
}
//JavaScriptで使用するpatern_id
$wk_pattern_id = ($pattern_id == "") ? "0" : $pattern_id;
// 週の場合、スタンプフラグクリア
if ($edit_start_day != $edit_end_day) {
    $stamp_flg = "";
}

///-----------------------------------------------------------------------------
// 上書きアラートの為の勤務希望データ取得
///-----------------------------------------------------------------------------
//希望情報取得 20120403
$plan_individual = $obj->get_plan_individual($group_id, $pattern_id, $start_date, $end_date);

// <script type="text/javascript" src="....."></script>
?>
<title>CoMedix <? echo($shift_menu_label); ?> | 勤務シフト作成</title>
<?php
// <script type="text/javascript" src="....."></script>
include_js("js/fontsize.js");
include_js("js/jquery/jquery-1.7.2.min.js");
include_js("js/browser-support.js");
include_js("js/duty_shift/menu.js");

//勤務パターン数によってフローティングウインドウ用の画像サイズを調整
$floatWinCnt=0;
$floatWinRow=1;
for ($k=0;$k<count($data_pattern_all);$k++) {
    if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&   $data_pattern_all[$k]["font_name"] != "" &&
            $data_pattern_all[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20130219
        $floatWinCnt++;
    }
}
//key入力用ウィンドウサイズ
//横幅

//１列
if($floatWinCnt < 25){
    $floatWinWidKey=150;
//２列
}elseif($floatWinCnt < 56 && $floatWinCnt > 25){
    $floatWinWidKey=180;
//３列
}elseif($floatWinCnt < 86 && $floatWinCnt > 55){
    $floatWinWidKey=250;
//４列
}else{
    $floatWinWidKey=320;
}

//縦幅
if($floatWinCnt >= 25){
    $floatWinHeiKey = 110 + (20*25);
}else{
    $floatWinHeiKey = 110 + (20*$floatWinCnt);
}


//スタンプ用ウインドウサイズ
//件数に応じた大きさにする 20130219 start
//ウィンドウの高さ＝上下の余白＋行数＊高さ22、件数が少ない場合でも最低限3行分とする、$floatWinCntへの+1は未設定分
$wk_row = ceil(($floatWinCnt+1) / 15);
if ($wk_row < 3) {
    $wk_row = 3;
}
$floatWinHeiStp = 44 + ($wk_row * 22);
//件数に応じた大きさにする 20130219 end

if($floatWinCnt > 7){
    $floatWinWidStp=800;
}else{
    $floatWinWidStp=500;
}

//key入力用ウィンドウ位置
$floatWinTopKey=-1000;
$floatWinLeftKey=-1000;

//スタンプ用ウインドウ位置
if ($stamp_flg != "1"){
    $floatWinTopStp=-1000;
    $floatWinLeftStp=-1000;
}else{
    $floatWinTopStp=5;
    $floatWinLeftStp=390;
    $floatWinDisStp='';
}

?>

<style type="text/css">
#floatWindowKey{
    position:absolute;
    top:<?=$floatWinTopKey?>px;
    left:<?=$floatWinLeftKey?>px;
    width:<?=$floatWinWidKey?>px;
    height:<?=$floatWinHeiKey?>px;
    /*filter:alpha(opacity=90);*/ /*IE*/
    /*-moz-opacity:0.9;*/ /*FF*/
    /*opacity:0.9;*/ /*OTHER*/
}
#floatWindowKey dl{
    width:<?=$floatWinWidKey?>px;
    height:<?=$floatWinHeiKey?>px;
    background:url("./images/floatWindow_g.gif");
    margin:1;
}

#floatWindowStp{
    display:<?=$floatWinDisStp?>;
    position:absolute;
    top:<?=$floatWinTopStp?>px;
    left:<?=$floatWinLeftStp?>px;
    width:<?=$floatWinWidStp?>px;
    height:<?=$floatWinHeiStp?>px;
    /*filter:alpha(opacity=90);*/ /*IE*/
    /*-moz-opacity:0.9;*/ /*FF*/
    /*opacity:0.9;*/ /*OTHER*/
}
#floatWindowStp dl{
    width:<?=$floatWinWidStp?>px;
    height:<?=$floatWinHeiStp?>px;
    background:url("./images/floatWindow_g.gif");
    margin:1;
}
</style>

<!-- 一括設定用曜日情報 20131225 START -->
<script type="text/javascript">
//タイプが必要な場合の設定リスト
var arr_type = new Array(0
<?
for ($i=1; $i<=count($week_array2); $i++) {
    echo(", {$week_array2[$i]["type"]}");
}
?>
);

// 曜日が必要な場合の設定リスト
var arr_name = new Array(0
<?
for ($i=1; $i<=count($week_array2); $i++) {
    echo(", '{$week_array2[$i]["name"]}'");
}
?>
);
</script>
<!-- 一括設定用曜日情報 20131225 END -->

<?
///-----------------------------------------------------------------------------
// 外部ファイルを読み込む
// カレンダー作成、関数出力
///-----------------------------------------------------------------------------
duty_shift_write_yui_calendar_use_file_read_0_12_2();
duty_shift_write_yui_calendar_script2(1);
?>
<script type="text/javascript">
    var IS_SHIFT_MAKER = 1; // 2014/10 シフト作成画面であることを示す。duty_shift_common_class.php参照
    var childwin_auto = null;
    var childwin_copy = null;
    var childwin_staff = null;
    var childwin_print = null;
    var childwin_print_select = null;
    var childwin_ptn_update = null;
    var plan_individual = <?php echo cmx_json_encode($plan_individual); ?> ;
    var view_YakinAvg = <? if ($view_YakinAvg) { echo 'true'; } else { echo 'false'; } ?>;
    var fullscreen_flg = <? if ($fullscreen_flg) { echo 'true'; } else { echo 'false'; } ?>;
    var change_switch = <?=$obj->GetSumCustomSwith($pattern_id);?>;
    var count_row = <?=h($count_row); ?>;

    ///-----------------------------------------------------------------------------
    //自動作成
    ///-----------------------------------------------------------------------------
    function autoMake() {
        if (!document.mainform.data_exist_flg) {
            alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
            return false;
        }
        closeChildWin();
        ///-----------------------------------------------------------------------------
        //自動作成
        ///-----------------------------------------------------------------------------
//        wx = 420;
//        wy = 260;
        wx = 440;       // ウインドウ拡張 20131225
        wy = 360;       // ウインドウ拡張 20131225

        dx = screen.width / 2 - (wx / 2);
        dy = screen.top;
        base = screen.height / 2 - (wy / 2);
        var url = 'duty_shift_menu_auto.php';
        url += '?session=<? echo $session; ?>';
        url += '&duty_yyyy=<? echo $duty_yyyy; ?>';
        url += '&duty_mm=<? echo $duty_mm; ?>';
        url += '&start_date=<? echo $origin_start_date; ?>';
        url += '&end_date=<? echo $end_date; ?>';
        url += '&pattern_id=<? echo $pattern_id; ?>';
        // 必要人数設定情報取得のため 20140114
        url += '&group_id=<? echo $group_id; ?>';
        childwin_auto = window.open(url, 'AutoPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
        childwin_auto.focus();
    }
    ///-----------------------------------------------------------------------------
    //過去月コピー
    ///-----------------------------------------------------------------------------
    function copyBeforeMonth() {
        if (!document.mainform.data_exist_flg) {
            alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
            return false;
        }
        closeChildWin();
        wx = 300;
        wy = 130;
        dx = screen.width / 2 - (wx / 2);
        dy = screen.top;
        base = screen.height / 2 - (wy / 2);
        var url = 'duty_shift_menu_copy.php';
        url += '?session=<?=$session?>';
        url += '&duty_yyyy=<?=$duty_yyyy?>';
        url += '&duty_mm=<?=$duty_mm?>';
        childwin_copy = window.open(url, 'copyPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
        childwin_copy.focus();
    }
    ///-----------------------------------------------------------------------------
    //印刷選択子画面
    ///-----------------------------------------------------------------------------
    function printSelect(total_print_flg, pdf_print_flg) {
        if (!document.mainform.data_exist_flg) {
            alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
            return false;
        }
        closeChildWin();
        wx = 300;
        wy = 200;
        dx = screen.width / 2 - (wx / 2);
        dy = screen.top;
        base = screen.height / 2 - (wy / 2);
        var url = 'duty_shift_menu_print_select.php';
        url += '?session=<?=$session?>';
        url += '&show_data_flg=<?=$show_data_flg?>';
        url += '&duty_yyyy=<?=$duty_yyyy?>';
        url += '&duty_mm=<?=$duty_mm?>';
        childwin_print_select = window.open(url, 'printSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
        childwin_print_select.focus();
    }
    ///-----------------------------------------------------------------------------
    //Excel出力
    ///-----------------------------------------------------------------------------
    function makeExcel() {
        if (!document.mainform.data_exist_flg) {
            alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
            return false;
        }
        closeChildWin();
        var total_print_flg = "0";
        if (document.mainform.total_disp_flg.value != "1") {
            total_print_flg = "1";
        }
//  PHP5.1.6対応 2012.1.11
//      document.mainform.total_print_flg.value = total_print_flg;
//      document.mainform.action = "duty_shift_menu_excel.php";
//      document.mainform.target = "download";
//      document.mainform.submit();
    <?
        $phpVersionName=phpversion();
    ?>
    var PHP_VER ;
    PHP_VER = "<? echo $phpVersionName; ?>";

    if ( PHP_VER >= "5.1.6" ){ // PHP5.1.6以降対応 2012.1.11
    <?
    $wkdat = "";
    $wkstart = array();
    $tmp_date = $origin_start_date;
    $wkcnt=0;
    foreach($week_array as $num=>$name){
        $tmp_date = strtotime($tmp_date);
        if( $name["name"] == "月" ){
            $buf = date("Ymd", $tmp_date);
            if ($buf <= $end_date) {                //表示期間のみを表示する 20130307
                $dispday = date("Y-m-d", $tmp_date);
                $wkdat .= $dispday.",";
            }
            $wkstart[$wkcnt] = $num;
            $wkcnt++;
        }
        $tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
    }
    $wkdat = rtrim($wkdat, ",");
    ?>
    var wkdate = "<? echo $wkdat; ?>";
    var wkstart = "<? echo $wkstart[0]; ?>";
            var url = 'duty_shift_menu_excel_5para.php';
            url += '?session=<?=$session?>';
            url += '&emp_id=<?=$emp_id?>';
            url += '&duty_yyyy=<?=$duty_yyyy?>';
            url += '&duty_mm=<?=$duty_mm?>';
            url += '&group_id=<? echo $group_id; ?>';
            url += '&duty_list='+wkdate;
            url += '&duty_start='+wkstart;
        childwin_excel = window.open(url , 'CoMedixExcelParam', 'width=550,height=580,scrollbars=yes,resizable=yes');
            childwin_excel.focus();
    } else{ // PHP5対応、PHP5ではないときの従来処理 2012.1.11
            document.mainform.total_print_flg.value = total_print_flg;
            document.mainform.action = "duty_shift_menu_excel.php";
            document.mainform.target = "download";
            document.mainform.submit();
    }
    }

    function openEventEdit(date, idx) {
        wx = 270;
        wy = 300;
	    base_left = (screen.width  - wx) / 2;
	    base = (screen.height - wy) / 2;
        var url = './duty_shift_event.php';
        url += '?session=<?=$session?>';
        url += '&group_id=<?=$group_id?>';
        url += '&date='+date;
        url += '&wk_idx='+idx;
        childwin_staff = window.open(url, 'eventpopup', 'left='+(base_left)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
        childwin_staff.focus();
    }

    function chg_daytext(day, idx, mark_flg) {
    	var o = document.getElementById("doc"+idx);
    	var t = day;
    	if (mark_flg == 1) {
    		t += "<font color='red'>・</font>";
    	}
    	o.innerHTML = t;
    }

    ///-----------------------------------------------------------------------------
    // スタッフ選択画面（ＯＰＥＮ）
    ///-----------------------------------------------------------------------------
    function openEmployeeList(item_id) {
        dx = screen.width;
        dy = screen.top;
        base = 0;
        wx = 720;
        wy = 600;
        var url = './emplist_popup.php';
        url += '?session=<?=$session?>';
        url += '&emp_id=<?=$emp_id?>';
        url += '&mode=19';
        url += '&item_id='+item_id;
        childwin_staff = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
        childwin_staff.focus();
    }

    ///-----------------------------------------------------------------------------
    // スタッフ選択画面からの選択データ反映
    ///-----------------------------------------------------------------------------
    function add_target_list(item_id, emp_id, emp_name)
    {
        var emp_ids = emp_id.split(", ");
        var emp_names = emp_name.split(", ");

        //画面再表示
        i = 0;
        i = '<?=$data_cnt?>';
        i++;
        document.mainform.del_staff_id.value = "";
        document.mainform.add_staff_cnt.value = emp_ids.length;
        document.mainform.add_staff_id.value = emp_ids;
        document.mainform.action="duty_shift_menu.php";
        document.mainform.target = "";
        document.mainform.submit();
    }

//組み合わせ指定
var arr_today = new Array();
var arr_nextday = new Array();
var arr_seqptn = new Array();
<?
//組み合わせによる自動設定
//  $arr_seq_pattern = $obj->get_duty_shift_group_seq_pattern_array($group_id);
for ($i=0; $i<count($seq_pattern_array); $i++) {

    $wk_today = sprintf("%02d%02d", $seq_pattern_array[$i]["today_atdptn_ptn_id"], $seq_pattern_array[$i]["today_reason"]);
    $wk_nextday = sprintf("%02d%02d", $seq_pattern_array[$i]["nextday_atdptn_ptn_id"], $seq_pattern_array[$i]["nextday_reason"]);
    echo("arr_today[$i] = '$wk_today';\n");
    echo("arr_nextday[$i] = '$wk_nextday';\n");
    echo("arr_seqptn['{$seq_pattern_array[$i]["today_atdptn_ptn_id"]}'] = '{$seq_pattern_array[$i]["nextday_atdptn_ptn_id"]}';\n");
}

//月をまたがっている場合に自動設定されない不具合の対応 20100519
//$end_day = substr($end_date, 6, 2);
$end_day = $day_cnt;
?>
        var end_day = <?=$end_day?>;
        var wk_pattern_id = <?=$wk_pattern_id?>;

var arr_atdptn_reason = new Array();
<?
//事由情報を出力
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
    foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
        if ($reason != "" && !($wk_atdptn_id == "10" && $reason == "24")) {
            echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
        }
    }
}
?>
var stamp_ptn = '<? $wk_stamp_ptn = ($stamp_ptn != "") ? $stamp_ptn :$def_ptn_info["atdptn_ptn_id"]; echo($wk_stamp_ptn); ?>';
var stamp_reason = '<? $wk_stamp_reason = ($stamp_reason != "") ? $stamp_reason :$def_ptn_info["reason"]; echo($wk_stamp_reason); ?>';



var arr_ptn_name = new Array();
var arr_font_color = new Array();
var arr_bg_color = new Array();
var arr_reason_name = new Array();
<?
for ($k=0;$k<count($data_pattern_all);$k++) {
    if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&
            $data_pattern_all[$k]["font_name"] != "") {

        $wk_atdptn_ptn_id = $data_pattern_all[$k]["atdptn_ptn_id"];
        $wk_reason = $data_pattern_all[$k]["reason"];
        $wk_key = ($wk_atdptn_ptn_id != "10") ? $wk_atdptn_ptn_id : $wk_atdptn_ptn_id."_".$wk_reason;

        //シングルクォーテーションがある場合、Javascriptエラーになるためエスケープ 20110817
        $wk_font_name = str_replace("'", "\'", h($data_pattern_all[$k]["font_name"]));
        echo("arr_ptn_name['".$wk_key."'] = '".$wk_font_name."';\n");
        echo("arr_font_color['".$wk_key."'] = '".$data_pattern_all[$k]["font_color_id"]."';\n");
        echo("arr_bg_color['".$wk_key."'] = '".$data_pattern_all[$k]["back_color_id"]."';\n");

    }
}

$reason_2_array = $obj->get_reason_2("");
for ($m=0; $m<count($reason_2_array); $m++) {
    $wk_key = $reason_2_array[$m]["id"];
    echo("arr_reason_name['".$wk_key."'] = '".$reason_2_array[$m]["font_name"]."';\n");
}
?>
    function setStamp(emp_idx, day, plan_hope_flg) {

        var copy_cnt = document.mainform.elements['continuous_cnt'].value;
        var wk_day = parseInt(day);
        var wk_id = 'staff_id['+emp_idx+']';
        var emp_id = document.mainform.elements[wk_id].value;
        var setday =1;
        //勤務希望アラート
        if (!alertHope(emp_idx, day, plan_hope_flg, stamp_ptn, copy_cnt)) {
            return;
        }

        //連続回数分繰返し
        for (cnt = 0; cnt < copy_cnt; cnt++) {

        if (plan_hope_flg == '0') {
            if (document.mainform.elements['show_data_flg'].value != '') {
                var data_idx = (parseInt(emp_idx) * 2) + 1;
            } else {
                var data_idx = parseInt(emp_idx) + 1;
            }
            var wk0 = 'data' + data_idx + '_' + wk_day;
            var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+wk_day;
            var wk2 = 'reason_2_'+emp_idx+'_'+wk_day;
            setUpdFlg(emp_idx);
        } else {
            var data_idx = (parseInt(emp_idx) + 1) * 2;
            var wk0 = 'data' + data_idx + '_' + wk_day;
            var wk1 = 'rslt_id_'+emp_idx+'_'+wk_day;
            var wk2 = 'rslt_reason_2_'+emp_idx+'_'+wk_day;
            setRsltUpdFlg(emp_idx);
        }
        var cell = document.getElementById(wk0);
        cell.setAttribute("plan_ptn", stamp_ptn);  // 2014/10 タブ属性追加（属性で追加管理しなくても、現状、別途保持しているかもしれない）
        cell.setAttribute("reason", stamp_reason);  // 2014/10 タブ属性追加（属性で追加管理しなくても、現状、別途保持しているかもしれない）
        //未設定にする
        if (stamp_ptn == '') {
            document.mainform.elements[wk1].value = '0000';
            document.mainform.elements[wk2].value = '';

            //デフォルト背景色をセット
            cell.style.backgroundColor = getDefaultBackColor(wk_day);

            cell.innerHTML = '&nbsp;';
            changeCell(wk0, emp_idx, wk_day, plan_hope_flg);

            wk_day++;
            if (wk_day > end_day) {//終了日確認
                break;
            }
            continue;
        }
        //組合せ確認
        var wk_reason = (stamp_reason != '' && stamp_reason != ' ' && stamp_reason != '0') ? stamp_reason : '00';
        atdptn_ptn_id = (stamp_ptn < 10) ? '0' + stamp_ptn : stamp_ptn;
        atdptn_ptn_id = atdptn_ptn_id + wk_reason;

        document.mainform.elements[wk1].value = atdptn_ptn_id;

        //事由設定
        var flg = (plan_hope_flg == '2') ? 2 : 1;
        var wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, atdptn_ptn_id, flg);

        wk_ptn_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn+'_'+stamp_reason;

        //背景色[白:#FFFFFF]以外は適用
        if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
            cell.style.backgroundColor = arr_bg_color[wk_ptn_id];
        } else {
            //背景色[白:#FFFFFF]の場合、デフォルト背景色をセット
            cell.style.backgroundColor = getDefaultBackColor(wk_day);
        }

        var wk_ptn_name = arr_ptn_name[wk_ptn_id];

<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
        if (wk_reason_2 != '') {
            wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
        }
<? } ?>
        var wk_font_color = arr_font_color[wk_ptn_id];

        fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);

        //組合せ設定
        var auto_set_flg = false;
        var auto_set_flg2 = false;
        var auto_set_flg3 = false;

        //2日目
        wk_day++;
        if (wk_day > end_day) {//終了日確認
            break;
        }
        if ((wk_day) <= end_day) {
            var wk0 = 'data' + data_idx + '_' + (wk_day);
            if (plan_hope_flg == '0') {
                var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
                var wk2 = 'reason_2_'+emp_idx+'_'+(wk_day);
            } else {
                var wk1 = 'rslt_id_'+emp_idx+'_'+(wk_day);
                var wk2 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
            }
            for (var i=0; i<arr_today.length; i++) {
                if (arr_today[i] == atdptn_ptn_id) {
                    document.mainform.elements[wk1].value = arr_nextday[i];
                    document.mainform.elements[wk2].value = '';
                    auto_set_flg = true;
                    break;
                }
            }
            if (auto_set_flg == true) {
                var cell = document.getElementById(wk0);
                wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
                wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
                wk_ptn_id = (wk_atdptn_ptn_id != '10') ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
                wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

                //背景色[白:#FFFFFF]以外は適用
                if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
                    cell.style.backgroundColor = arr_bg_color[wk_ptn_id];
                } else {
                    //背景色[白:#FFFFFF]の場合、デフォルト背景色をセット
                    cell.style.backgroundColor = getDefaultBackColor(wk_day);
                }
                var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
                if (wk_reason_2 != '') {
                    wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
                }
<? } ?>
                var wk_font_color = arr_font_color[wk_ptn_id];

                fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
                cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
                changeCell(wk0, emp_idx, wk_day, plan_hope_flg);
                setday=2;

                wk_day++;//組合せ設定できたら対象日を進める
                if (wk_day > end_day) {//終了日確認
                    break;
                }
            }
        }
        //3日目
        if (auto_set_flg == true && (wk_day) <= end_day) {
            var wk0 = 'data' + data_idx + '_' + (wk_day);
            if (plan_hope_flg == '0') {
                var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
                var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
                var wk4 = 'reason_2_'+emp_idx+'_'+(wk_day);
            } else {
                var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
                var wk3 = 'rslt_id_'+emp_idx+'_'+(wk_day);
                var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
            }
            var yokuyoku_ptn_id = document.mainform.elements[wk2].value;

            for (var i=0; i<arr_today.length; i++) {
                if (arr_today[i] == yokuyoku_ptn_id) {
                    document.mainform.elements[wk3].value = arr_nextday[i];
                    document.mainform.elements[wk4].value = '';
                    auto_set_flg2 = true;
                    break;
                }
            }
            if (auto_set_flg2 == true) {
                var cell = document.getElementById(wk0);
                wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
                wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
                wk_ptn_id = (wk_atdptn_ptn_id != 10) ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
                wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

                //背景色[白:#FFFFFF]以外は適用
                if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
                    cell.style.backgroundColor = arr_bg_color[wk_ptn_id];
                } else {
                    //背景色[白:#FFFFFF]の場合、デフォルト背景色をセット
                    cell.style.backgroundColor = getDefaultBackColor(wk_day);
                }
                var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
                if (wk_reason_2 != '') {
                    wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
                }
<? } ?>
                var wk_font_color = arr_font_color[wk_ptn_id];

                fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
                cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
                changeCell(wk0, emp_idx, wk_day, plan_hope_flg);
                setday=3;

                wk_day++;//組合せ設定できたら対象日を進める
                if (wk_day > end_day) {//終了日確認
                    break;
                }
            }
        }
        //4日目
        if (auto_set_flg2 == true && (wk_day) <= end_day) {
            var wk0 = 'data' + data_idx + '_' + (wk_day);
            if (plan_hope_flg == '0') {
                var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
                var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
                var wk4 = 'reason_2_'+emp_idx+'_'+(wk_day);
            } else {
                var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
                var wk3 = 'rslt_id_'+emp_idx+'_'+(wk_day);
                var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
            }
            var yokuyokuyoku_ptn_id = document.mainform.elements[wk2].value;

            for (var i=0; i<arr_today.length; i++) {
                if (arr_today[i] == yokuyokuyoku_ptn_id) {
                    document.mainform.elements[wk3].value = arr_nextday[i];
                    document.mainform.elements[wk4].value = '';
                    auto_set_flg3 = true;
                    break;
                }
            }
            if (auto_set_flg3 == true) {
                var cell = document.getElementById(wk0);
                wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
                wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
                wk_ptn_id = (wk_atdptn_ptn_id != 10) ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
                wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

                //背景色[白:#FFFFFF]以外は適用
                if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
                    cell.style.backgroundColor = arr_bg_color[wk_ptn_id];
                } else {
                    //背景色[白:#FFFFFF]の場合、デフォルト背景色をセット
                    cell.style.backgroundColor = getDefaultBackColor(wk_day);
                }
                var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
                if (wk_reason_2 != '') {
                    wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
                }
<? } ?>
                var wk_font_color = arr_font_color[wk_ptn_id];

                fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
                cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
                changeCell(wk0, emp_idx, wk_day, plan_hope_flg);
                setday=4;
                wk_day++;//組合せ設定できたら対象日を進める
                if (wk_day > end_day) {//終了日確認
                    break;
                }
            }
        }

        }
        //キー入力時
        if (document.mainform.stamp_flg.value == "") {
            //目印画像の移動
            markFlagChange('','right',setday);
        }
    }

    //スクロール
    function scroll_region(flg) {

        switch(flg) {
        case 1: //left
<?
    if ($fullscreen_flg != "1") {
    ?>
            parent.frames['floatingpage'].document.body.scrollLeft = 0;
<? } else {?>
            window.scrollTo(0,0);
<? } ?>
            break;
        case 2: //up
            var wk_ht = document.getElementById('region').offsetHeight;
            var wk_pos = document.getElementById('region').scrollTop
            document.getElementById('region').scrollTop = wk_pos - wk_ht;
            break;
        case 3: //right
<?
if ($fullscreen_flg != "1") {
    ?>
            parent.frames['floatingpage'].document.body.scrollLeft = 900;
<? } else {?>
            window.scrollTo(900,0);
<? } ?>
            break;
        case 4: //down
            var wk_ht = document.getElementById('region').offsetHeight;
            var wk_pos = document.getElementById('region').scrollTop
            document.getElementById('region').scrollTop = wk_pos + wk_ht;
            break;
        }
    }

    //--HTML出力
    function outputLAYER(layName,html){
    if(document.getElementById){        //e5,e6,n6,n7,m1,o7,s1用
      document.getElementById(layName).innerHTML=html;
    } else if(document.all){            //e4用
      document.all(layName).innerHTML=html;
    } else if(document.layers) {        //n4用
       with(document.layers[layName].document){
         open();
         write(html);
         close();
      }
    }
    }

    //--マウス追跡
    /*==================================================================
    followingLAYER()

    Syntax :
     追跡レイヤー名 = new followingLAYER('レイヤー名'
                                ,右方向位置,下方向位置,動作間隔,html)

     レイヤー名 マウスを追跡させるレイヤー名
     右方向位置 マウスから右方向へ何ピクセル離すか
     下方向位置 マウスから下方向へ何ピクセル離すか
     動作間隔   マウスを追跡する間隔(1/1000秒単位 何秒後に動くか?)
     html       マウスを追跡するHTML

    ------------------------------------------------------------------*/
    /*--/////////////ここから下は触らなくても動きます/////////////--*/

    //--追跡オブジェクト
    //e4,e5,e6,n4,n6,n7,m1,o6,o7,s1用
    function followingLAYER(layName,ofx,ofy,delay,html){
    this.layName = layName;   //マウスを追跡させるレイヤー名
    this.ofx     = ofx;       //マウスから右方向へ何ピクセル離すか
    this.ofy     = ofy;       //マウスから下方向へ何ピクセル離すか
    this.delay   = delay;     //マウスを追跡するタイミング
    if(document.layers)
      this.div='<layer name="'+layName+'" left="-100" top="-100">\n'              + html + '</layer>\n';
    else
      this.div='<div id="'+layName+'"\n'              +'style="position:absolute;left:-100px;top:-100px;z-index:2;">\n'              + html + '</div>\n'; <? //メッセージウィンドウを前に表示するためzindexに2を指定 ?>
    document.write(this.div);
    }

    //--メソッドmoveLAYER()を追加する
    followingLAYER.prototype.moveLAYER = moveLAYER; //メソッドを追加する
    function moveLAYER(layName,x,y){
    if(document.getElementById){        //e5,e6,n6,n7,m1,o6,o7,s1用
        document.getElementById(layName).style.left = x;
        document.getElementById(layName).style.top  = y;
    } else if(document.all){            //e4用
        document.all(layName).style.pixelLeft = x;
        document.all(layName).style.pixelTop  = y;
    } else if(document.layers)          //n4用
        document.layers[layName].moveTo(x,y);
    }

    //--Eventをセットする(マウスを動かすとdofollow()を実行します)
    document.onmousemove = dofollow;
    //--n4マウスムーブイベント走査開始
    if(document.layers)document.captureEvents(Event.MOUSEMOVE);
    //--oの全画面のEventを拾えないことへの対策
    if(window.opera){
    op_dmydoc ='<div id="dmy" style="position:absolute;z-index:0'             +'                     left:100%;top:100%"></div> ';
    document.write(op_dmydoc);
    }

    //--イベント発生時にマウス追跡実行
    function dofollow(e){
    for(var i=0 ; i < a.length ; i++ )
      setTimeout("a["+i+"].moveLAYER(a["+i+"].layName,"          +(getMouseX(e)+a[i].ofx)+","+(getMouseY(e)+a[i].ofy)+")"       ,a[i].delay);
    }

    //--マウスX座標get
    function getMouseX(e){
    if(navigator.userAgent.search(
             "Opera(\ |\/)6") != -1 )   //o6用
        return e.clientX;
    else if(document.all)               //e4,e5,e6用
        return document.body.scrollLeft+event.clientX;
    else if(document.layers ||
            document.getElementById)    //n4,n6,n7,m1,o7,s1用
        return e.pageX;
    }

    //--マウスY座標get
    function getMouseY(e){
    if(navigator.userAgent.search(
             "Opera(\ |\/)6") != -1 )   //o6用
        return e.clientY;
    else if(document.all)               //e4,e5,e6用
        return document.body.scrollTop+event.clientY;
    else if(document.layers ||
            document.getElementById)    //n4,n6,n7,m1,o7,s1用
        return e.pageY;
    }

    /*////////////////////////////// マウスを追跡するレイヤーここまで */

    //テンプレートを作成しoutputLAYERへ渡す
    function showMsg(msg1){
        outputLAYER('test0',msg1);
    }

    //レイヤーの中身を消す
    function hideMsg(){
        var msg1 ='';
        outputLAYER('test0',msg1);
    }

    //レイヤーの数だけa[i]=…部分を増減して使ってください
    var a = new Array();
    a[0]  = new followingLAYER('test0',20,10,100,'');
//  a[1]  = new followingLAYER('test1',20,10,200,'');
//  a[2]  = new followingLAYER('test2',20,10,300,'');

//十字ハイライト
<? $obj_menu->createCrossHighlightJavaScript($show_data_flg); ?>

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<link rel="stylesheet" type="text/css" href="css/shift/shift.css">
<link rel="stylesheet" type="text/css" href="css/shift/menu.css">
</head>

<?
if (($err_msg_1 != "") || ($err_msg_2 != "")) {
    echo("<body bgcolor=\"#ffffff\" text=\"#000000\" topmargin=\"0\" leftmargin=\"0\" marginheight=\"0\" marginwidth=\"0\">\n");
} else {
    if ($dragdrop_flg == "1") {
        $dragdrop_action = "constructDragDrop();";
    } else {
        $dragdrop_action = "";
    }
    if ($standard_id != "") {
        $init_cal_action = "initcal(); updateCal1();";
    } else {
        $init_cal_action = "";
    }
    echo("<body bgcolor=\"#ffffff\" text=\"#000000\" topmargin=\"0\" leftmargin=\"0\" marginheight=\"0\" marginwidth=\"0\" onload=\"$init_cal_action init_action(); $dragdrop_action start_auto_session_update();\">\n");
}
?>

<!-- セッションタイムアウト防止 START -->
<form name="session_update_form" action="session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="<?=$session?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->

<?
// 全画面時は表示しない
if ($fullscreen_flg != "1") {
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 画面遷移／タブ -->
    <!-- ------------------------------------------------------------------------ -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
    <?
    // 画面遷移
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    show_user_title($session, $section_admin_auth);         //duty_shift_common.ini
    echo("</table>\n");

    // タブ
    $arr_option = "&group_id=" . $group_id;
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    show_user_tab_menuitem($session, $fname, $arr_option);  //duty_shift_user_tab_common.ini
    echo("</table>\n");

    // 下線
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
    ?>
    </td></tr></table>
<? } else {
    // 全画面時
    for ($i=0;$i<count($group_array);$i++) {
        if ($group_id == $group_array[$i]["group_id"]) {
            $tmp_group_name = $group_array[$i]["group_name"];
            break;
        }
    }
    $target_title = " {$duty_yyyy}年{$duty_mm}月 $tmp_group_name";
     ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr bgcolor="#5279a5">
        <td width="100%" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>シフト作成 <?=$target_title?></b></font></td>
        <td width="32" bgcolor="#dddddd" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
        </tr>
        </table>

    <? } ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- データ不備時 -->
    <!-- ------------------------------------------------------------------------ -->
<?
if (($err_msg_1 != "") || ($err_msg_2 != "")) {
    echo($err_msg_1);
    if ($err_msg_1 != "") { echo("<br>\n"); }
    echo($err_msg_2);
    echo("<br>\n");
    echo("<form name=\"mainform\" method=\"post\">\n");
    echo("<table id=\"header\" class=\"list\"></table>\n");
    echo("<div id=\"region\">\n");
    echo("<table id=\"data\" class=\"list\"></table>\n");
    echo("</div>\n");
    echo("<table id=\"error\" class=\"list\"></table>\n");
    echo("<table id=\"summary\" class=\"list\"></table>\n");
    echo("</form>");
} else {
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 勤務表 -->
    <!-- ------------------------------------------------------------------------ -->
    <form name="mainform" method="post">
        <!-- ------------------------------------------------------------------------ -->
        <!-- ＨＩＤＤＥＮ -->
        <!-- ------------------------------------------------------------------------ -->
    <?
    //合計行ハイライト用数値、休暇内訳追加対応
    if ($hol_dt_flg == "1") {
        $wk_count_sum_title = count($title_gyo_array) + count($title_hol_array) + 1;
    } else {
        $wk_count_sum_title = count($title_gyo_array) + 2; //公休、有休追加対応
    }
    echo($obj_menu->showHidden_1($data_cnt,
                $day_cnt,
                $edit_start_day,
                $edit_end_day,
                $wk_count_sum_title,$week_index));

    echo("<input type=\"hidden\" name=\"create_flg\" value=\"$create_flg\">\n");        //登録可能フラグ（１：可能）
    echo("<input type=\"hidden\" name=\"show_gyo_cnt\" value=\"$show_gyo_cnt\">\n");    //表示行数
    echo("<input type=\"hidden\" name=\"regist_first_flg\" value=\"1\">\n");    //登録ボタン初回フラグ
        ?>
    <?
    // 全画面時は表示しない
    if ($fullscreen_flg != "1") {
        ?>
        <!-- ------------------------------------------------------------------------ -->
        <!--  シフトグループ（病棟）名 、年、月 -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?
        $url = "duty_shift_menu.php";
        $url_aption = "";
        $wk_group_show_flg = "";                //シフトグループ表示フラグ（１：表示）
        $data_name = "";        //２行目の表示データ名
        if ($plan_results_flg == "2") {
            $data_name = "２段目：勤務実績";
        } else
            if ($plan_hope_flg == "2") {
                $data_name = "２段目：勤務希望";
            } else
                if ($plan_duty_flg == "2") {
                    $data_name = "２段目：当直";
                } else
                    if ($plan_comment_flg == "2") {
                        $data_name = "２段目：コメント";
                    }

        echo($obj_menu->showHead($session, $fname, $wk_group_show_flg, $group_id,
                    $group_array,
                    $duty_yyyy, $duty_mm, $url, $url_aption, $finish_flg,$data_name, $week_index));
        ?>
        </table>
        <? } ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 各種ボタン -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <?
    // 希望取込ボタン有効フラグ、表示開始月が前月以前は無効
    $wk_start_mmdd = substr($start_date, 0, 6);
    if ($wk_start_mmdd >= date("Ym")) {
        $hope_get_btn_flg = "1";
    } else {
        $hope_get_btn_flg = "";
    }

    // 夜勤のみ表示ボタン有効フラグ
    $night_shift = array_filter($plan_array, array($obj, "is_night_shift"));    // 夜勤従事者を抽出
    $night_only_flg = (count($night_shift) > 0) ? "1" : "0";
    // 下書きボタンフラグ、当月以前は無効とする 20150306
    $draft_button_flg = ($origin_start_date > date("Ymd")) ? "1" : "0";//1;//

    echo($obj_menu->showButton($create_flg,
                $plan_results_flg,
                $plan_hope_flg,
                $plan_duty_flg,
                $plan_comment_flg,
                $edit_start_day,
                $edit_end_day,
                $finish_flg,
                $stamp_flg,
                $fullscreen_flg,
                $term_btn_flg,
                $hope_get_btn_flg,
                $total_disp_flg,
                $night_only_flg,
                $standard_id,
                $draft_button_flg));

        ?>
        </table>
        </td>
        </tr>
        </table>

<?//■スタンプエリアの生成開始?>
<input type="hidden" name="listPosition" id="listPosition">
<input type="hidden" name="listPositionPlanHope" id="listPositionPlanHope">

<div id="floatWindowStp" style="position:absolute;z-index:1;">
<a href="" class="close" onclick="floatWindowHide(0,'Stp','');setStampFlg();"><img src="./images/close.gif" alt="閉じる" /></a>
<dl id="dl_floatWindowStp">
<dt></dt>
<dd>
<table border="0" cellspacing="0" cellpadding="0" frame="void">
<tr>
<td>
    <?
    //スタンプの指定無しの場合、シフト記号登録の先頭のパターンを取得
    for ($k=0;$k<count($data_pattern_all);$k++) {
        if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&
                (($stamp_ptn != "" && $data_pattern_all[$k]["atdptn_ptn_id"] == $stamp_ptn) ||
                    ($stamp_ptn == "" && $data_pattern_all[$k]["font_name"] != "")) &&
                $data_pattern_all[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20120306
            $wk_ptn_name = $data_pattern_all[$k]["font_name"];
            $wk_font_color = $data_pattern_all[$k]["font_color_id"];
            $wk_back_color = $data_pattern_all[$k]["back_color_id"];
            break;
        }
    }
?>
        <? $stamp_tbl_disp = ($stamp_flg == "1") ? "" : "none"; ?>
          <table border="0" cellspacing="0" cellpadding="1" class="list">
            <tr>
              <input type="hidden" name="stmpPosition" value="" id="stmpPosition">
              <td width="50" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連続</font>
              </td>
            </tr>
            <tr>
              <td align="center" style="background-color:#ffffff;">
                <select name="continuous_cnt" id="continuous_cnt">
        <?
                for ($c_idx=1; $c_idx<=31; $c_idx++) {
                    echo("<option value=\"$c_idx\">$c_idx</option>\n");
                }
        ?>
                </select>
              </td>
            </tr>


            <tr>
              <td id="stamp_cel" bgcolor="<?echo($wk_back_color);?>" align="center">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="<?echo($wk_font_color);?>"><?echo($wk_ptn_name);?></font>
              </td>
            </tr>


        </table>
      </td>

      <td width="5">
        <table>
        </table>
      </td>

      <td>
        <table border="0" cellspacing="0" cellpadding="1" class="list" style="margin-left : auto ; margin-right : auto ; text-align : left ;">
          <tr valign="middle">
    <?

    $wdpa = $data_pattern_all;
    $data_ptn_cnt = count($data_pattern_all);
    $wdpa[$data_ptn_cnt]["pattern_id"] = $pattern_id;
    $wdpa[$data_ptn_cnt]["atdptn_ptn_id"] = "";
    $wdpa[$data_ptn_cnt]["reason"] = "";
    $wdpa[$data_ptn_cnt]["font_name"] = "未設";
    $wdpa[$data_ptn_cnt]["atdptn_ptn_name"] = "未設定";

    $col_max = 15;
    $wk_cnt = 0;
    for ($k=0;$k<count($wdpa);$k++) {
        if ($pattern_id == $wdpa[$k]["pattern_id"] &&
                $wdpa[$k]["font_name"] != "" &&
                $wdpa[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20120306

            if ($wk_cnt % $col_max == 0 && $wk_cnt > 0) {
                echo("</tr><tr>\n");
            }
            $wk_atdptn_ptn_id = $wdpa[$k]["atdptn_ptn_id"];
            $wk_id = ($wk_atdptn_ptn_id != "10") ? $wk_atdptn_ptn_id : $wk_atdptn_ptn_id."_".$wdpa[$k]["reason"];

            if (!$wdpa[$k]["back_color_id"]){
                $style = "";
            }else{
                $style = " style=\"background-color:".$wdpa[$k]["back_color_id"]."\"";
            }
            echo("<td class=\"td03\" id=\"stamp_$wk_id\"".$style." onClick=\"setStampPtn('");
            echo($wdpa[$k]["atdptn_ptn_id"]);
            echo("','");
            echo($wdpa[$k]["reason"]);
            echo("','");
            echo("stamp_$wk_id");
            echo("');\">");

            if (!$wdpa[$k]["reason_name"]){
            echo("<div style=\"color: ".$wdpa[$k]["font_color_id"]."\" title=\"" . $wdpa[$k]["atdptn_ptn_name"] . "\">");
            }else{
                echo("<div style=\"color: ".$wdpa[$k]["font_color_id"]."\" title=\"" . $wdpa[$k]["atdptn_ptn_name"] ."/".$wdpa[$k]["reason_name"]. "\">");
            }
			$wk_font_name = h($wdpa[$k]["font_name"]);
			if (strpos($wk_font_name, "&") === false) {
				$wk_font_name = mb_substr($wk_font_name,0,3);
			}
			echo($wk_font_name);
            echo("</div>");
            echo("</td>\n");

            $wk_cnt++;
        }
    }
?>
        </table>
       </td>
      </tr>
     </table>
    <?/*    </td>
       </tr>
  </table>*/?>
</dd>
</dl>
</div>

<div id="floatWindowKey" style="position:absolute;z-index:1;">
<a href="" class="close" onclick="floatWindowHide(0,'Key','');"><img src="./images/close.gif" alt="閉じる" /></a>
<dl id="dl_floatWindowKey">
<dt>
</dt>
<dd>
<?
    echo("<table>\n");
    echo("<tr>\n");
    echo("<td style=\"font-size:13px;\">\n");
    echo("『<b>＠</b>』キーで未設定\n");
    echo("</td>\n");
    echo("</tr>\n");
    echo("<tr>\n");
    echo("<td style=\"font-size:13px;\">\n");
    echo("『<b>矢印</b>』キーで移動\n");
    echo("</td>\n");
    echo("</tr>\n");
    echo("</table>\n");
?>
<table border="0" cellspacing="0" cellpadding="0" frame="void">

<?
    //キー割り当て キーボードの並び順 ※＠除く
    $arr_stampkey = array("↑","←","→","↓","@",
                        "1", "2", "3", "4","5","6","7","8","9","0",
                        "Q","W","E","R","T","Y","U","I","O","P",
                        "A","S","D","F","G","H","J","K","L",
                        "Z","X","C","V","B","N","M");

    $col_max = 25;
    $wk_cnt = 0;
    $rows_flg = 0;

    echo("<tr>\n");
    echo("<td valign=\"top\">\n");
    echo("<table class=\"list\" style=\"display:table-cell;\">\n");
/*
    echo("<tr>\n");
    echo("<td class=\"td01\"><div title=\"『$arr_stampkey[0]』キーを押してください\">$arr_stampkey[0]</div></td>\n");
    echo("<td class=\"td02\" onClick=\"markFlagChange('','up',1);\"><div title=\"上へ進む\">↑</div></td>\n");
    echo("</tr>\n");
    echo("<tr>\n");
    echo("<td class=\"td01\"><div title=\"『$arr_stampkey[1]』キーを押してください\">$arr_stampkey[1]</div></td>\n");
    echo("<td class=\"td02\" onClick=\"markFlagChange('','left',1);\"><div title=\"左へ進む\">←</div></td>\n");
    echo("</tr>\n");
    echo("<tr>\n");
    echo("<td class=\"td01\"><div title=\"『$arr_stampkey[2]』キーを押してください\">$arr_stampkey[2]</div></td>\n");
    echo("<td class=\"td02\" onClick=\"markFlagChange('','right',1);\"><div title=\"右へ進む\">→</div></td>\n");
    echo("</tr>\n");
    echo("<tr>\n");
    echo("<td class=\"td01\"><div title=\"『$arr_stampkey[3]』キーを押してください\">$arr_stampkey[3]</div></td>\n");
    echo("<td class=\"td02\" onClick=\"markFlagChange('','down',1);\"><div title=\"下へ進む\">↓</div></td>\n");
    echo("</tr>\n");
    echo("<tr>\n");
    echo("<td class=\"td01\"><div title=\"『$arr_stampkey[4]』キーを押してください\">$arr_stampkey[4]</div></td>\n");
    echo("<td class=\"td02\" id=\"not_stamp_\" onClick=\"setStampPtn('','','stamp_');\"><div title=\"未設定\">未設</div></td>\n");
    echo("</tr>\n");
*/
    // 最終データの『未設』はループさせない
    for ($k=0;$k<count($wdpa)-1;$k++) {

        //パターンID合致、文言がNull以外
        if ($pattern_id == $wdpa[$k]["pattern_id"] && $wdpa[$k]["font_name"] != "" &&
                $wdpa[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20120306

            //次列へ
//          if ($wk_cnt == 28 || $wk_cnt == 55 || $wk_cnt == 85 || $wk_cnt == 115){
            if ($wk_cnt % 28 == 0 ){
                echo("</table>\n");
                echo("</td>\n");
                echo("<td valign=\"top\">\n");
                echo("<table class=\"list\" style=\"display:table-cell;\">\n");
            }

            echo("<tr>\n");
            //『{,*,},_,@』の5データ分加算
            $stmpKeyTarget=$wk_cnt + 5;
            if ($arr_stampkey[$stmpKeyTarget]!="") {
                $shift_title="『$arr_stampkey[$stmpKeyTarget]』キーを押してください";
            } else {
                $shift_title="";
            }
            echo("<td class=\"td01\"><div title=\"$shift_title\">$arr_stampkey[$stmpKeyTarget]</div></td>\n");

            $wk_atdptn_ptn_id = $wdpa[$k]["atdptn_ptn_id"];
            $wk_id = ($wk_atdptn_ptn_id != "10") ? $wk_atdptn_ptn_id : $wk_atdptn_ptn_id."_".$wdpa[$k]["reason"];
            $style = "";

            //文字色＆背景色
            if ($wdpa[$k]["back_color_id"]!="#ffffff") {
                $style = "background-color:".$wdpa[$k]["back_color_id"].";";
            }
            if ($wdpa[$k]["font_color_id"]!="#000000") {
                $style = $style."color:".$wdpa[$k]["font_color_id"].";";
            }
            if ($style!=""){
                $style = "style=\"".$style."\"";
            }

            echo("<td class=\"td02\" id=\"not_stamp_$wk_cnt\" $style onClick=\"setStampPtn('");
            echo($wdpa[$k]["atdptn_ptn_id"] . "','" . $wdpa[$k]["reason"] . "','" . "stamp_" . $wk_id. "');\">");

            if (!$wdpa[$k]["reason_name"]){
                echo("<div title=\"" . $wdpa[$k]["atdptn_ptn_name"] . "\">");
            }else{
                echo("<div title=\"" . $wdpa[$k]["atdptn_ptn_name"] ."/".$wdpa[$k]["reason_name"]. "\">");
            }
			$wk_font_name = h($wdpa[$k]["font_name"]);
			if (strpos($wk_font_name, "&") === false) {
				$wk_font_name = mb_substr($wk_font_name,0,3);
			}
			echo($wk_font_name);
            echo("</div></td>\n");

            echo("</tr>\n");

            $wk_cnt++;
        }
    }

    echo("</table>\n");
    echo("</td>\n");
    echo("</tr>\n");
    echo("</table>\n");

//-----------------------------------------------------------------------------------------------------------------------

?>
</dd>
</dl>
</div>

        <!-- ------------------------------------------------------------------------ -->
        <!-- 見出し -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="<? echo($shift_table_width); ?>" border="0" cellspacing="0" cellpadding="0" class="non_list" style="table-layout:<? echo ($table_layout); ?>;">
    <?
    //週        使用停止 20130307
//  echo($obj_menu->showTitleSub1($duty_yyyy, $duty_mm,
//              $day_cnt, $edit_start_day, $edit_end_day,
//              $week_array, $title_gyo_array, $check_show_flg, $plan_comment_flg, $wk_group));
        ?>
        </table>
        <table width="<? echo($shift_table_width); ?>" id="header" border="0" cellspacing="0" cellpadding="0" class="list" style="table-layout:<? echo ($table_layout); ?>;">
    <?
    //日・曜日
    echo($obj_menu->showTitleSub2(
            $duty_yyyy,
            $duty_mm,
            $day_cnt,
            $edit_start_day,
            $edit_end_day,
            $week_array,
            &$title_gyo_array, //行合計タイトル情報 集計列幅対応 20140502
            $check_show_flg,
            $calendar_array,
            $total_disp_flg,
            $four_week_line,
            $hol_dt_flg,
            $title_hol_array,
            $plan_comment_flg,
            $plan_results_flg,
            $paid_hol_disp_flg,
            $wk_group,
            "create_shift",
            $extra_day,
            $unit_name,         //ユニット（３階層）名
            $gyo_array, //行合計情報 集計列幅対応 20140502     
            ""          //勤務シフト希望フラグ（１:勤務シフト希望）
    ));
    ?>
        </table>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 勤務シフトデータ（職員（列）年月日（行） -->
        <!-- ------------------------------------------------------------------------ -->
        <? // 2014/10 onscrollイベント追加。duty_shift_menu_common_class.php参照 ?>
        <div id="region" style="overflow-x:hidden; overflow-y:scroll; overflow:auto; border:#5279a5 solid 0px;" onscroll="regionScrolled()">
        <table width="<? echo($shift_table_width); ?>" id="data" border="0" cellspacing="0" cellpadding="0" class="list" style="table-layout:<? echo ($table_layout); ?>;">
    <?
    //divのstyleからcursor:pointer; を除く 20110722
    $wk_emp_id = "";
    $obj_menu->showList(
            $plan_array,
            $data_pattern,
            $warning_gyo_array,
            $plan_results_flg,
            $plan_hope_flg,
            $plan_duty_flg,
            $plan_comment_flg,
            $day_cnt,
            $edit_start_day,
            $edit_end_day,
            $week_array,
            $title_gyo_array,
            $gyo_array,
            $check_show_flg,
            $wk_emp_id,
            $staff_del_flg,
            0,
            $data_cnt,
            $highlight_flg,
            $group_id,
            $pattern_id,
            "",
            $create_flg,
            $total_disp_flg,
            $four_week_line,
            $calendar_array,
            $hol_dt_flg,
            $title_hol_array,
            $hol_cnt_array,
            $paid_hol_all_array,
            $reason_setting_flg,
            $stamp_flg,
            $wk_group,
            $plan_individual,
            $paid_hol_disp_flg,
            "create_shift",
            $extra_day
    );
    ?>
        </table>
        </div>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 警告 -->
        <!-- ------------------------------------------------------------------------ -->
        <div id="outCountWindow">
        <div id="outCountWindowBar"></div>
        <div id="countWindow">
        <table width="<? echo($shift_table_width); ?>" border="0" cellspacing="0" cellpadding="0" class="list" style="table-layout:<? echo ($table_layout); ?>;">
    <?
    $paid_hol_array[] = $paid_hol_all_array["plan"];
    echo($obj_menu->showError(
            $day_cnt,
            $edit_start_day,
            $edit_end_day,
            $week_array,
            $title_gyo_array,
            $gyo_array,
            $title_retu_array,
            $retu_array,
            $warning_retu_array,
            $check_show_flg,
            $data_cnt,
            $total_disp_flg,
            $four_week_line,
            $hol_dt_flg,
            $title_hol_array,
            $hol_cnt_array,
            $plan_comment_flg,
            $wk_group,
            $calendar_array,
            $paid_hol_disp_flg,
            $paid_hol_array,
            "create_shift"
    ));
    ?>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 列計 -->
        <!-- ------------------------------------------------------------------------ -->
    <?
    //2段目の集計有無
    if ($total_disp_flg != "1" && ($plan_results_flg == "2" || $plan_results_flg == "3" || $plan_hope_flg == "2")) {
        $wk_total_gyo_flg = "2";
    } else {
        $wk_total_gyo_flg = "1";
    }

    //有給休暇残日数
    $paid_hol_array = array();
    if ($paid_hol_disp_flg === "t") {
        if ($plan_hope_flg == "2") {
            $paid_hol_array[] = $paid_hol_all_array["hope"];
        } else {
            $paid_hol_array[] = $paid_hol_all_array["result"];
        }
    }
    echo($obj_menu->showTitleRetu(
            $day_cnt,
            $edit_start_day,
            $edit_end_day,
            $week_array,
            $title_gyo_array,
            $gyo_array,
            $title_retu_array,
            $retu_array,
            $check_show_flg,
            $data_cnt,
            $four_week_line,
            $wk_total_gyo_flg,
            $total_disp_flg,
            $hol_dt_flg,
            $title_hol_array,
            $hol_cnt_array,
            $plan_comment_flg,
            $wk_group,
            $calendar_array,
            $paid_hol_disp_flg,
            $paid_hol_array
    ));
    ?>
        </table>
        </div>
        </div>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 当病棟の月平均夜勤時間 -->
        <!-- ------------------------------------------------------------------------ -->
    <?
    if ($standard_id != "") {
        ?>
        <div id="floatYakinAvg">
        <div id="floatYakinAvg_header">月平均夜勤時間<span class="close">[×]</span></div>
        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
        <?
        echo($obj_menu->showNightTime(
                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                    $draft_reg_flg,         //下書きデータ登録状態
                    $night_start_day,       //４週計算用（開始日）
                    compact("date_y1", "date_m1", "date_d1"),    //「計算」開始年月日
                    compact("duty_yyyy", "duty_mm"),             //勤務年月
                    $limit_time,            //月平均夜勤時間上限
                    $day_cnt,               //日数
                    $standard_id,           //施設基準ID
                    $group_id               //グループID
                ));
        ?>
        </table>
        </div>
        <?
    }
        ?>
        <? $stamp_tbl_disp = ($stamp_flg == "1") ? "" : "none"; ?>
        <!-- ------------------------------------------------------------------------ -->
        <!-- ＨＩＤＤＥＮ -->
        <!-- ------------------------------------------------------------------------ -->
    <?
    //-------------------------------------------------------------------------------
    //共通
    //-------------------------------------------------------------------------------
    echo($obj_menu->showHidden_2($rslt_flg,
                $plan_array, $calendar_array,
                $session,
                $day_cnt,
                $plan_results_flg,
                $plan_hope_flg,
                $plan_duty_flg,
                $plan_comment_flg,
                $group_id,
                $duty_yyyy, $duty_mm,
                $edit_start_day, $edit_end_day,
                $add_staff_cnt, $add_staff_id, $del_staff_id,
                $pattern_id,
                $draft_flg,
                $check_flg,
                $fullscreen_flg,
                $reason_setting_flg));
    //-------------------------------------------------------------------------------
    //処理ボタン
    //-------------------------------------------------------------------------------
    echo("<input type=\"hidden\" name=\"comp_flg\" value=\"$comp_flg\">\n");                            //計算処理実行フラグ（１：実行）
    echo("<input type=\"hidden\" name=\"auto_flg\" value=\"$auto_flg\">\n");                            //自動シフト作成実行フラグ（１：実行）
    echo("<input type=\"hidden\" name=\"hope_get_flg\" value=\"$hope_get_flg\">\n");                    //希望取り込み実行フラグ（１：実行）
    echo("<input type=\"hidden\" name=\"copy_before_month_flg\" value=\"$copy_before_month_flg\">\n");  //前月コピー実行フラグ（１：実行）
    echo("<input type=\"hidden\" name=\"staff_set_add_flg\" value=\"\">\n");    //職員設定取込フラグ 1:実行
    //-------------------------------------------------------------------------------
    //過去月コピー
    //-------------------------------------------------------------------------------
    echo("<input type=\"hidden\" name=\"copy_yyyy\" value=\"$copy_yyyy\">\n");              //過去月コピー開始年
    echo("<input type=\"hidden\" name=\"copy_mm\" value=\"$copy_mm\">\n");                  //過去月コピー開始月
    //-------------------------------------------------------------------------------
    //１日コピー
    //-------------------------------------------------------------------------------
    echo("<input type=\"hidden\" name=\"copy_day_flg\" value=\"$copy_day_flg\">\n");        //１日コピー実行フラグ（１：実行）
    echo("<input type=\"hidden\" name=\"copy_day_cause\" value=\"$copy_day_cause\">\n");    //１日コピー（コピー元の日）
    echo("<input type=\"hidden\" name=\"copy_day_point\" value=\"$copy_day_point\">\n");    //１日コピー（コピー先の日）
    //-------------------------------------------------------------------------------
    //自動シフト用
    //-------------------------------------------------------------------------------
    echo("<input type=\"hidden\" name=\"auto_start_day\" value=\"$auto_start_day\">\n");    //自動シフト作成開始日
    echo("<input type=\"hidden\" name=\"auto_end_day\" value=\"$auto_end_day\">\n");    //自動シフト作成終了日
    echo("<input type=\"hidden\" name=\"auto_ptn_id\" value=\"\">\n");  //自動シフト対象とするシフト
    echo("<input type=\"hidden\" name=\"auto_btn_flg\" value=\"\">\n"); //自動シフトボタンフラグ
    echo("<input type=\"hidden\" name=\"prov_start_hhmm\" value=\"$prov_start_hhmm\">\n");  //自動シフト作成実行フラグ（１：実行）
    echo("<input type=\"hidden\" name=\"need_cnt\" value=\"$need_cnt\">\n");                //必要人数
    echo("<input type=\"hidden\" name=\"shift_case\" value=\"$shift_case\">\n");            //自動シフト実行選択
    //-------------------------------------------------------------------------------
    //希望取り込みデータ判定フラグ
    //-------------------------------------------------------------------------------
    for ($i=0; $i<$data_cnt; $i++) {
        for ($k=1; $k<=$day_cnt; $k++) {
            $wk2 = $plan_array[$i]["individual_flg_$k"];
            $wk3 = "individual_flg_$i" . "_" . $k;
            echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk2\" >\n");
        }
    }
    //-------------------------------------------------------------------------------
    //月平均夜勤時間
    //-------------------------------------------------------------------------------
    echo("<input type=\"hidden\" name=\"night_sumtime\" value=\"$night_sumtime\">\n");    //月平均夜勤時間（予定）

    //ドラッグ＆コピー有効 1:有効
    echo("<input type=\"hidden\" name=\"dragdrop_flg\" value=\"$dragdrop_flg\">\n");
    //集計行列の印刷 0:しない 1:する
    echo("<input type=\"hidden\" name=\"total_print_flg\" value=\"\">\n");
    //PDF印刷 0:WEBページ 1:PDF
    echo("<input type=\"hidden\" name=\"pdf_print_flg\" value=\"\">\n");
    //印刷用に２行目表示データフラグを出力（１：勤務実績、２：勤務希望、３：当直、４：コメント）
    //ただし、４：コメントは１：勤務実績にする
    //          if ($show_data_flg == "4") {
    //              $show_data_flg = "1";
    //          }
    echo("<input type=\"hidden\" name=\"show_data_flg\" value=\"$show_data_flg\">\n");
    echo("<input type=\"hidden\" name=\"add_staff_id_all\" value=\"$add_staff_id_all\">\n");
    //月単位PDF印刷 20130219
    echo("<input type=\"hidden\" name=\"selectmonth\" value=\"\">\n");
    echo("<input type=\"hidden\" name=\"extra_day\" value=\"$extra_day\">\n");
    //作成期間処理フラグ
    echo("<input type=\"hidden\" name=\"term_chg_flg\" value=\"\">\n");
    //作成期間番号
    echo("<input type=\"hidden\" name=\"term_chg_id\" value=\"$term_chg_id\">\n");
    //希望データ保存（勤務希望非表示になる時）
    echo("<input type=\"hidden\" name=\"hope_save_flg\" value=\"\">\n");
    //集計表示非表示、1:非表示 "":表示（他機能画面の初期表示もあるため）
    echo("<input type=\"hidden\" name=\"total_disp_flg\" value=\"$total_disp_flg\">\n");
    //登録子画面の指定情報
    echo("<input type=\"hidden\" name=\"last_ptn_id\" value=\"\">\n");
    echo("<input type=\"hidden\" name=\"last_reason_2\" value=\"\">\n");
    echo("<input type=\"hidden\" name=\"last_copy_cnt\" value=\"\">\n");
    //登録状態、希望非表示時のデータ登録用 1:下書き 2:登録済み "":未登録
    echo("<input type=\"hidden\" name=\"draft_reg_flg\" value=\"$draft_reg_flg\">\n");
    //登録状態、応援追加後の表示用 1:下書き 2:登録済み "":未登録
    echo("<input type=\"hidden\" name=\"old_draft_reg_flg\" value=\"$old_draft_reg_flg\">\n");
    //休暇日数詳細 1:表示 "":非表示
    echo("<input type=\"hidden\" name=\"hol_dt_flg\" value=\"$hol_dt_flg\">\n");

    //登録時、データがそろっていることの確認用
    echo("<input type=\"hidden\" name=\"data_exist_flg\" value=\"1\">\n");
    //シフトグループIDの変更確認用
    echo("<input type=\"hidden\" name=\"group_change_flg\" value=\"\">\n");
    //削除数
    echo("<input type=\"hidden\" name=\"delete_id_cnt\" value=\"$delete_id_cnt\">\n");
    //更新時用情報 20090611
    for ($i=0; $i<$data_cnt; $i++) {
        //自動作成、希望取込、過去月コピー後は全て更新状態とする
        //下書き状態の場合 20090721
        //応援先の所属から削除された場合 20091020
        //応援追加職員か判断し、変更フラグ設定 20111025
        $wk_staff_id = $plan_array[$i]["staff_id"];
        if ($wk_auto_flg == "1" ||
                $wk_hope_get_flg == "1" ||
                $wk_copy_before_month_flg == "1" ||
                $draft_reg_flg == "1" ||
                $plan_array[$i]["group_chg_flg"] == "1" ||
                in_array($wk_staff_id, $wk_array)
                ) {
            $upd_flg[$i] = 1;
        }
        echo("<input type=\"hidden\" name=\"upd_flg[$i]\" value=\"{$upd_flg[$i]}\">\n");
    }
    for ($i=0; $i<$data_cnt; $i++) {
        echo("<input type=\"hidden\" name=\"rslt_upd_flg[$i]\" value=\"{$rslt_upd_flg[$i]}\">\n");
    }
    for ($i=0; $i<$data_cnt; $i++) {
        echo("<input type=\"hidden\" name=\"cmt_upd_flg[$i]\" value=\"{$cmt_upd_flg[$i]}\">\n");
    }
    //表示順更新
    echo("<input type=\"hidden\" name=\"update_no_flg\" value=\"\">\n");
    //選択された職員の位置。スクロール調整用
    echo("<input type=\"hidden\" name=\"emp_idx\" value=\"\">\n");
    //スタンプ有効 1:有効
    echo("<input type=\"hidden\" name=\"stamp_flg\" value=\"$stamp_flg\">\n");
    echo("<input type=\"hidden\" name=\"stamp_ptn\" value=\"$stamp_ptn\">\n");
    echo("<input type=\"hidden\" name=\"stamp_reason\" value=\"$stamp_reason\">\n");
    //キー入力にて処理が実行された場合 1:実行 Null:未実行
    echo("<input type=\"hidden\" name=\"keyInput_flg\" value=\"\">\n");

    //-------------------------------------------------------------------------------
    // PHP Excel 5.1.6対応 2011.1.11
    //-------------------------------------------------------------------------------
    echo("<input type=\"hidden\" name=\"excel_ratio\"      value=\"$excel_ratio\">\n");
    echo("<input type=\"hidden\" name=\"excel_paper_size\" value=\"$excel_paper_size\">\n");
    echo("<input type=\"hidden\" name=\"excel_setfitpage\" value=\"$excel_setfitpage\">\n");
    echo("<input type=\"hidden\" name=\"excel_mtop\"       value=\"$excel_mtop\">\n");
    echo("<input type=\"hidden\" name=\"excel_mbottom\"    value=\"$excel_mbottom\">\n");
    echo("<input type=\"hidden\" name=\"excel_mleft\"      value=\"$excel_mleft\">\n");
    echo("<input type=\"hidden\" name=\"excel_mright\"     value=\"$excel_mright\">\n");
    echo("<input type=\"hidden\" name=\"excel_mhead\"      value=\"$excel_mhead\">\n");
    echo("<input type=\"hidden\" name=\"excel_mfoot\"      value=\"$excel_mfoot\">\n");
    echo("<input type=\"hidden\" name=\"excel_row_count\"  value=\"$excel_row_count\">\n");
    echo("<input type=\"hidden\" name=\"excel_row_type\"   value=\"$excel_row_type\">\n");
    // 2012.8.22追加
    echo("<input type=\"hidden\" name=\"excel_blancrows\">\n");
    echo("<input type=\"hidden\" name=\"excel_date\">\n");
    echo("<input type=\"hidden\" name=\"excel_weekcolor\">\n");
    echo("<input type=\"hidden\" name=\"excel_hopecolor\">\n");
    echo("<input type=\"hidden\" name=\"excel_yearmonth\"      value=\"\">\n");// 20130219 追加
    echo("<input type=\"hidden\" name=\"excel_week4start\"     value=\"\">\n");// 20140507 追加
    echo("<input type=\"hidden\" name=\"excel_event\"          value=\"\">\n");// 20150709 追加

    //連続勤務or週間勤務時間数チェック 20131107 $warning_flag
    $j = 0;
    echo("<input type=\"hidden\" name=\"register_flg\"  value=\"$register_flg\">\n"); //登録フラグ
    echo("<input type=\"hidden\" name=\"workdays_flg\" value=\"$warning_flag\">\n");
    foreach ($chk_info as $info){
        for($i = 0; $i<count($info["date"]); $i++){
            $workdays_date = "workdays_date$j" . "[" . $i . "]";
            $workdays_msg = "workdays_msg$j" . "[" . $i . "]";
            $value_during = $info['date'][$i]['during'];
            $value_msg = $info['date'][$i]['msg'];
            echo("<input type=\"hidden\" name=\"$workdays_date\"  value=\"$value_during\">\n");
            echo("<input type=\"hidden\" name=\"$workdays_msg\"  value=\"$value_msg\">\n");
        }
        echo("<input type=\"hidden\" name=\"workdays_emp_id$j\"  value=\"{$info['emp_id']}\">\n");
        echo("<input type=\"hidden\" name=\"workdays_emp_name$j\"  value=\"{$info['emp_name']}\">\n");
        $j++;
    }
    $workdays_count = count($chk_info);
    echo("<input type=\"hidden\" name=\"workdays_count\"  value=$workdays_count>\n");
    ?>

    </form>
    <iframe name="download" width="0" height="0" frameborder="0"></iframe>

    <? } ?>



<? pg_close($con); ?>


<?
$wk_err_flg = false;
$wk_err_flg2 = false;
// 件数チェック
if ($old_data_cnt != "" &&
        ($group_id == $cause_group_id) &&
        ($duty_yyyy == $cause_duty_yyyy) &&
        ($duty_mm == $cause_duty_mm)) {
    // 今回追加削除分算出
    $wk_data_cnt = $data_cnt - $wk_add_staff_cnt;
    if ($wk_del_staff_id != "") {
        $wk_data_cnt = $data_cnt + 1;
    }
    //自動作成以外
    if ($wk_auto_flg != "1" &&
            $old_data_cnt != $wk_data_cnt) {
        $wk_err_flg = true;
    }
}

if ($wk_err_flg) {
?>
<script language="JavaScript">
alert('データが全て転送されませんでした。直前の画面に戻ります。');
history.back();
</script>
    <?
} else {
    // 氏名の空白確認
    for ($i=0; $i<$data_cnt; $i++) {
        //echo("<br>$i name=".$plan_array[$i]["staff_name"]);
        if ($plan_array[$i]["staff_name"] == "") {
            $wk_err_flg2 = true;
            break;
        }
    }

    if ($wk_err_flg2 && $err_msg_1 == "") { // グループ設定、管理者のエラー以外の場合 20140210
        //メッセージに設定不足の場合のキャンセルの説明を追加 20100303
?>
<script language="JavaScript">
if(confirm('職員のデータを取得できませんでした。OKボタンを押すと直前の画面に戻ります。\n設定がまだの場合には、キャンセルを押して設定をしてください。')) {
    history.back();
}
</script>
        <?
    }
}
?>

<!-- ************************************************************************ -->
<!-- JavaScript（スクロールの位置調整） -->
<!-- ************************************************************************ -->
<script language="JavaScript">
    var data = document.getElementById("data");
    var header = document.getElementById("header");

<?
// シフト更新した位置によりスクロール位置を変更する
if ($emp_idx > 4) {
    $line_top = ($emp_idx - 4) * $show_gyo_cnt;
    ?>
    var max = <?=$line_top?>;
    scrolltop = 0;
    for(i=0; i<max; i++) {
        scrolltop += data.rows[i].offsetHeight;
    }
    document.getElementById('region').scrollTop = scrolltop;
    <?
}
    ?>
</script>


<!-- ************************************************************************ -->
<!-- JavaScript（ドラッグアンドドロップ処理） -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">

<script type="text/javascript" src="js/yui/build/event/event-min.js"></script>
<script type="text/javascript" src="js/yui/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript">

// ドラッグアンドドロップ処理
function constructDragDrop()
{
    var arr_mmdd = Array(
<?
$mmdd_str = "";
foreach ($calendar_array as $tmp_date) {
    $wk_mmdd = "'" . substr($tmp_date["date"], 4, 2) ."/". substr($tmp_date["date"], 6, 2) . "'";
    if ($mmdd_str != "") {
        $mmdd_str .= ",";
    }
    $mmdd_str .= $wk_mmdd;
}
echo($mmdd_str);
?>
    );
    var day_cnt = document.mainform.day_cnt.value;

    for (var i=1; i<=day_cnt; i++) {

        var wk_id = "doc" + i;

        var doc = new YAHOO.util.DD(wk_id);
        doc.startPos = YAHOO.util.Dom.getXY(wk_id);

        //-------------------------------------------------------------------------------
        //初期ドラック時
        //-------------------------------------------------------------------------------
        doc.startDrag = function (x, y) {
            var el = this.getDragEl();
            el.style.backgroundColor = '#fefe83';
            el.style.zIndex = 999;
        }
        //-------------------------------------------------------------------------------
        //ドラック＆ドロップ時
        //-------------------------------------------------------------------------------
        doc.onDragDrop = function (e, id) {

            var item_obj   = this.getDragEl();
            var target_obj = YAHOO.util.DDM.getElement(id);

            //対象チェック
            if(target_obj.id.indexOf("doc") != -1)
            {
//              var wk = item_obj.id.substring(3,5) + " 日を " + target_obj.id.substring(3,5) + " 日にコピーします。よろしいですか？";
                var mmdd_idx1 = parseInt(item_obj.id.substring(3,5),10)-1;
                var mmdd_idx2 = parseInt(target_obj.id.substring(3,5),10)-1;
                var wk = arr_mmdd[mmdd_idx1] + " を " + arr_mmdd[mmdd_idx2] + " にコピーします。よろしいですか？";
                if (confirm(wk)) {
                    document.mainform.copy_day_cause.value = item_obj.id.substring(3,5);        //コピー元
                    document.mainform.copy_day_point.value = target_obj.id.substring(3,5);      //コピー先
                    document.mainform.copy_day_flg.value = "1";
                    document.mainform.action="duty_shift_menu.php";
                    document.mainform.target = "";
                    document.mainform.submit();
                }
                return;
            }

            if(!this.canAccept(e, id))
            {
                return;
            }
        }
        //-------------------------------------------------------------------------------
        //ドラック＆ドロップ終了時、元の場所に戻る
        //-------------------------------------------------------------------------------
        doc.endDrag = function (x, y) {
            var el = this.getDragEl();
            YAHOO.util.Dom.setXY(el, this.startPos);
            el.style.backgroundColor = '';
            el.style.zIndex = 0;
        }
    }
}

</script>

<?
if ($hol_dt_flg == "1") {
?>
<script type="text/javascript">
scrollTo(900,0);
</script>
    <?
}
?>
<script language="JavaScript">
<? //シフトグループ変更後と表示中に無効化、表示後有効化 20111221 ?>
    var group_id = document.getElementById('group_id');
    var duty_yyyy = document.getElementById('duty_yyyy');
    if (group_id) group_id.disabled = false;
    if (duty_yyyy) duty_yyyy.disabled = false;

<?  if ($childwin_auto_sts == "1") {  ?>
        childwin_auto_sts.document.getElementById("display1").innerHTML = "終了";
        childwin_auto_sts.close();
        childwin_auto_sts = null;
<? } ?>
</script>

<?php
//20131107
if ($warning_flag == 1 && $tmp_check_flg == "t"){
    if($workdays_count > 0){
?>
<script type="text/javascript">
    open_warn_msg();
</script>
    <?php }else{ ?>
        <?php  if($register_flg == "1"){ ?>
<script type="text/javascript">
    document.mainform.workdays_flg.value = 0;
    editData(2);
    document.mainform.workdays_flg.value = 1;
        <?php } ?>
</script>
    <?php } ?>
    <?
}
//下書き状態時メッセージ対応 20150306
if ($draft_reg_flg == "1" && $origin_start_date <= date("Ymd")) {
?>
<script language="JavaScript">
alert('下書き状態になっています。登録ボタンを押して、登録状態にして下さい。');
</script>
<?
}
?>

</body>
</html>
