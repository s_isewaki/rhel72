<?php
require_once("class/Cmx/Model/SystemConfig.php");
require_once("webmail/config/config.php");

function webmail_quota_plugin_enabled() {
	global $plugins;
	return in_array("check_quota", $plugins);
}

function webmail_quota_get_default() {
	$system_config = webmail_quota_system_config();
	return intval($system_config->get('webmail.default_quota'));
}

function webmail_quota_set_default($quota) {
	$system_config = webmail_quota_system_config();
	$system_config->set('webmail.default_quota', $quota);
}

function webmail_quota_change($account, $quota) {
	if (preg_match("/[a-z]/", $account) > 0) {
		$cwd = dirname(__FILE__);
		$quota_in_kb = $quota * 1024;
		exec("/usr/bin/perl $cwd/mboxadm/setquota $account $quota_in_kb 2>&1 1> /dev/null");
	}
}

function webmail_quota_change_to_default($account) {
	static $default_quota;
	if (!isset($default_quota)) {
		$default_quota = webmail_quota_get_default();
	}
	if ($default_quota > 0) {
		webmail_quota_change($account, $default_quota);
	}
}

function webmail_quota_system_config() {
	static $system_config;
	if (!isset($system_config)) {
		$system_config = new Cmx_SystemConfig();
	}
	return $system_config;
}
