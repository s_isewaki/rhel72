<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("duty_shift_common_class.php");
require_once("date_utils.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//利用するCLASS
$obj = new duty_shift_common_class($con, $fname);

//現在日付の取得
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];

//各変数値の初期値設定
if ($date_y1 == "") { $date_y1 = $duty_yyyy; };
if ($date_m1 == "") { $date_m1 = $duty_mm; };
if ($date_d1 == "") { $date_d1 = 1; };

// 出勤パターンＤＢ(atdptn)情報取得
$data_atdptn = $obj->get_atdptn_array($pattern_id);

// 勤務シフト記号情報取得（履歴）
$data_pattern = $obj->get_duty_shift_pattern_history_array($pattern_id, $duty_yyyy, $duty_mm, $data_atdptn);

// 事由情報（午前有休／午後有休）取得
$reason_2_all_array = $obj->get_reason_2("1");

// 出勤パターン選択ドロップダウンリストを作成
// リストオブジェクト追加作成 20131225 START
$list = createPatternSelectList($pattern_id, $data_pattern, $reason_2_all_array, "pattern");
$list2 = createPatternSelectList($pattern_id, $data_pattern, $reason_2_all_array, "pattern2");
// リストオブジェクト追加作成 20131225 END

// 実行内容選択リスト（必要人数設定内容を展開する START 20140114
$joblist = createJobSelectlist($con, $fname, $group_id);
// 実行内容選択リスト（必要人数設定内容を展開する END


// データベースを切断
pg_close($con);


// 実行内容選択リスト（必要人数設定内容を展開する）
function createJobSelectlist($con, $fname, $group_id)
{

	// 必要人数設定の値のオフセットを50とする
	$offset = 50;

	// 規定のjob分のリスト
	$list = "<option value=\"1\">全て</option>";
	$list .= "<option value=\"2\">勤務希望取り込み</option>";
	$list .= "<option value=\"3\">個人定型勤務条件</option>";
	$list .= "<option value=\"4\">休暇（公休）</option>";


	$cmd = "select '必要人数割当' as caption, duty_shift_need_cnt_standard.no, atdptn.atdptn_nm ";
	$cmd .= "from duty_shift_need_cnt_standard, duty_shift_group, atdptn ";

	$cond = "where ";
	$cond .= "duty_shift_group.group_id=duty_shift_need_cnt_standard.group_id ";
	$cond .= "and atdptn.atdptn_id=duty_shift_need_cnt_standard.atdptn_ptn_id ";
	$cond .= "and atdptn.group_id=duty_shift_group.pattern_id ";
	$cond .= "and duty_shift_need_cnt_standard.group_id = '$group_id' ";
	$cond .= "and duty_shift_need_cnt_standard.atdptn_ptn_id<>10 ";
	$cond .= "order by duty_shift_need_cnt_standard.no";

// 必要人数設定の内容を取得する
	$sel = select_from_table($con, $cmd, $cond, $fname);

	if ($sel != 0) {

    	$num = pg_num_rows($sel);

    	if ($num > 0 ) {
			for( $i = 0; $i < $num; $i++ ) {
				$temp_no = pg_fetch_result($sel,$i,"no") + $offset;
				$temp_cap = pg_fetch_result($sel,$i,"caption")."(".pg_fetch_result($sel,$i,"atdptn_nm").")";
				$list .= "<option value=\"$temp_no\">$temp_cap</option>";
			}
	    } else {
	    	$list .= "<option value=\"$offset\">必要人数割当</option>";
		}
	} else {
	    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	    echo("<script language='javascript'>showLoginPage(window);</script>");
	    exit;
	}

	return $list;

}
/**
 * 出勤パターン選択ドロップダウンリストを作成する
 *
 * @param string $pattern_id         勤務パターンＩＤ
 * @param array  $pattern_array      勤務シフト記号情報（履歴）
 * @param array  $reason_2_all_array 事由情報（午前有休／午後有休）
 *
 * @return string 作成したドロップダウンリスト
 */
function createPatternSelectList($pattern_id, $pattern_array, $reason_2_all_array, $objectname)
{
// オブジェクトIDを可変に変更 20131225
//    $list = "<select id=\"pattern\" name=\"pattern\">\n";
    $list = "<select id=\"".$objectname."\" name=\"".$objectname."\">\n";

    foreach ($pattern_array as $pattern) {
        // 出勤表の休暇種別等画面の非表示を確認
        if ($pattern["reason_display_flag"] == "f") {
            continue;
        }

        // 出勤表のパターン名＞出勤パターン名画面の非表示を確認
        if ($pattern["display_flag"] == "f") {
            continue;
        }

        // 表示記号が設定されていないものは表示しない START 20131227
        if ( $pattern["font_name"] == '' ) {
        	continue;
        }
        // 名称が設定されていないものは表示しない END

        // ＩＤ
        $wk_id = sprintf("%02d%02d", $pattern["atdptn_ptn_id"], $pattern["reason"]);
        $list .= "<option value=\"$wk_id:{$pattern["font_name"]}:{$pattern["font_color_id"]}:{$pattern["back_color_id"]}\">";

        // 名称
        $wk_name = $pattern["atdptn_ptn_name"];

        // 半有半公等
        if (($pattern["reason"] >= "45" && $pattern["reason"] <= "47")) {
            $wk_name = $pattern["reason_name"];
            // 表示変更
            foreach ($reason_2_all_array as $reason) {
                if ($reason["id"] == $pattern["reason"]) {
                    $wk_name = $reason["name"];
                    break;
                }
            }
        } elseif ($pattern["reason_name"] != "") {
            $wk_name .= "({$pattern["reason_name"]})";
        }
        $list .= "{$wk_name}</option>\n";
    }
    $list .= "</select>";
    return $list;
}

//-------------------------------------------------------------------
// HTML
//-------------------------------------------------------------------
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 自動シフト作成「子画面」</title>

<script type="text/javascript">
	var count=0;
	var gsts=0;
	// 「ＯＫ」開始日を設定する
	function OkBtn() {

		var start_dd=document.mainform.date_d1.value;
		var end_dd=document.mainform.date_d2.value;
		var auto_ptn_id='0';
        var shift_case = document.mainform.shift_case.value;

        if ( document.mainform.condition.checked ){
			auto_ptn_id='1';
		}

		//開始日を指定して自動シフトを実行
		if(parent.opener && !parent.opener.closed && parent.opener.add_start_dd){
			parent.opener.add_start_dd(start_dd, end_dd, auto_ptn_id, "1", shift_case );
		}

	}

	// 「キャンセル」
	function CancelBtn() {
		window.close();
	}

    // 空白セルに一括割り当て
    function setPatternToBlancCells() {
      if (!parent.opener) {
        return;
      }

      if (parent.opener.closed) {
        return;
      }

      var pattern  = document.mainform.pattern.value;
      var start_dd = document.mainform.date_d1.value;
      var end_dd   = document.mainform.date_d2.value;
      parent.opener.setPatternToBlancCells(pattern, start_dd, end_dd);

      window.close();
    }

    // 空白セルに一括割り当て（曜日別）20131225
    function setPatternToBlancCellsWeekType() {
      if (!parent.opener) {
    	  return;
      }

      if (parent.opener.closed) {
    	  return;
      }

      var week1 = (document.mainform.day1.checked) ? 't' : 'f';
      var week2 = (document.mainform.day2.checked) ? 't' : 'f';
      var week3 = (document.mainform.day3.checked) ? 't' : 'f';
      var week4 = (document.mainform.day4.checked) ? 't' : 'f';

      var pattern2  = document.mainform.pattern2.value;
      var start_dd = document.mainform.date_d1.value;
      var end_dd   = document.mainform.date_d2.value;

      if ( week1 != 't' && week2 != 't' && week3 != 't' && week4 != 't' ) {
          alert('設定する曜日を一つ以上選んで下さい。');
          return;
      }

      parent.opener.setPatternToBlancCellsWeekType(pattern2, start_dd, end_dd, week1, week2, week3, week4);

      window.close();
    }
</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px; font-size:10px;}
td.spacing {font-size:14px; font-weight:bold; color:white;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr height="32" bgcolor="#5279a5">
	<td class="spacing">自動シフト設定</td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
</table>

<form name="mainform" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr height="22">
	<!--td colspan="4"><b>＜対象とする勤務シフトの選択＞</b></td-->
</tr>
<!--tr height="22">
	<td colspan="4">
	<label><input type="radio" id="auto_ptn_id" name="auto_ptn_id" value="0" checked>全て</label>
	</td>
</tr-->
<tr height="22">
	<td colspan="4"><b>＜開始日と終了日＞</b></td>
</tr>
<tr height="22">
	<td width="20%" align="center"><b>開始日</b></td>
	<td width="30%">
		<select id="date_d1" name="date_d1">
<?
// 期間変更対応のため、日付ではなく配列位置を示す
$wk_timestamp = date_utils::to_timestamp_from_ymd($start_date);
for ($i=1; $i<=32; $i++) {

	$wk_day_idx = $i - 1;
	$wk_date = date("Ymd", strtotime($wk_day_idx." day", $wk_timestamp));
	if ($wk_date > $end_date) {
		break;
	}
	printf('<option value="%s">%04d/%02d/%02d</option>', $i, substr($wk_date, 0, 4), substr($wk_date, 4, 2), substr($wk_date, 6, 2));
}
?>
		</select>
	</td>
	<td width="20%" align="center"><b>終了日</b></td>
	<td width="30%">
		<select id="date_d2" name="date_d2">
<?
$wk_timestamp = date_utils::to_timestamp_from_ymd($start_date);
for ($i=1; $i<=32; $i++) {

	$wk_day_idx = $i - 1;
	$wk_date = date("Ymd", strtotime($wk_day_idx." day", $wk_timestamp));
	if ($wk_date > $end_date) {
		break;
	}
	$selected = ($wk_date == $end_date) ? " selected" : "";
	printf('<option value="%s" %s>%04d/%02d/%02d</option>', $i, $selected, substr($wk_date, 0, 4), substr($wk_date, 4, 2), substr($wk_date, 6, 2));
}
?>
		</select>
	</td>
</tr>
</table>

<br>

<div align="center" style="font-size: 13px;">
<table border="0">
<tr>
 <td><input type="button" value="自動割当実行" onclick="OkBtn();"></td>
 <td>&nbsp&nbsp</td>
 <td><font size="2">実行条件&nbsp
<select id="shift_case" name="shift_case"><? echo $joblist; ?></select>
</font></td>
</tr>
<tr>
 <td></td>
 <td>&nbsp&nbsp</td>
 <td><font size="2"><label><input type="checkbox" name="condition">希望以外をクリアーして割当</label></font></td>
</tr>
</table>

<hr size="5" color="#0000ff">
<br>
<!-- 以下レイアウト変更 20131225 START -->
<table width="97%" border="0" cellspacing="0" cellpadding="0">
<tr height="20"><td colspan=2><font size="2">空白のセルに</font></td></tr>
<tr height="30"><td><font size="2"> <? echo $list; ?> を</td><td><font size="2"><input type="button" value="一括で割り当てる" onclick="setPatternToBlancCells();";></font></td></tr>
<tr height="15"><td> </td><td> </td></tr>
<tr height="20"><td><font size="2">空白のセルで</font></td>
<td><font size="2">&nbsp<label><input type="checkbox" name="day1" value="f">平日</label>&nbsp<label><input type="checkbox" name="day2" value="f">土曜</label>&nbsp<label><input type="checkbox" name="day3" value="f">日曜</label>&nbsp<label><input type="checkbox" name="day4" value="f">祝日</label>&nbspの時に</font></td></tr>
<tr height="30">
<td><font size="2"> <? echo $list2; ?> を</td><td><font size="2"><input type="button" value="一括で割り当てる" onclick="setPatternToBlancCellsWeekType();";></font></td></tr>
</table>
<!-- レイアウト変更 20131225 END -->
</div>
</form>
</body>
</html>
