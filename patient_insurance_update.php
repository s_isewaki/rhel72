<?
require_once('about_session.php');
require_once('about_authority.php');

$fname = $PHP_SELF;

// sessionのチェック
$session = qualify_session($session, $fname);
if ($session == '0') {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == '0') {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if ($isr_code != '' && (20 < mb_strlen($isr_code))) {
    echo("<script type=\"text/javascript\">alert('記号・番号(前)は20文字以内で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($isr_id != '' && (20 < mb_strlen($isr_id))) {
    echo("<script type=\"text/javascript\">alert('記号・番号(後)は20文字以内で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($psn_nm != '' && (40 < mb_strlen($psn_nm))) {
    echo("<script type=\"text/javascript\">alert('保険者名称は40文字以内で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($psn_id != '' && !preg_match('/^[0-9]+$/D', $psn_id)) {
    echo("<script type=\"text/javascript\">alert('保険者番号は半角数値で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($psn_id != '' && (8 < strlen($psn_id))) {
    echo("<script type=\"text/javascript\">alert('保険者番号は8桁以内で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($ch_psn_id != '' && !preg_match('/^[0-9]+$/D', $ch_psn_id)) {
    echo("<script type=\"text/javascript\">alert('公費負担者番号は半角数値で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($ch_psn_id != '' && (8 < strlen($ch_psn_id))) {
    echo("<script type=\"text/javascript\">alert('公費負担者番号は8桁以内で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($rsp_id != '' && !preg_match('/^[0-9]+$/D', $rsp_id)) {
    echo("<script type=\"text/javascript\">alert('受給者番号は半角数値で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if ($rsp_id != '' && (7 < strlen($rsp_id))) {
    echo("<script type=\"text/javascript\">alert('受給者番号は7桁以内で入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

$kf_date = '';
if ($kf_date_y != '' && $kf_date_m != '' && $kf_date_d != '') {
    $kf_date = $kf_date_y . $kf_date_m . $kf_date_d;
}
$st_date = '';
if ($st_date_y != '' && $st_date_m != '' && $st_date_d != '') {
    $st_date = $st_date_y . $kf_date_m . $st_date_d;
}
if ($wareki_flg1 == '') {
    $wareki_flg1 = '0';
}
if ($wareki_flg2 == '') {
    $wareki_flg2 = '0';
}

// データベースに接続
$con = connect2db($fname);

pg_query($con, 'begin');

if ($sql_type == 'insert') {
    // 新規
    $sql     = 'insert into ptinsurance (' .
                   'ptif_id, ' .
                   'iss_date, ' .
                   'acq_date, ' .
                   'wareki_flg1, ' .
                   'wareki_flg2, ' .
                   'insurance_code, ' .
                   'insurance_id, ' .
                   'relation, ' .
                   'charge_rate, ' .
                   'isr_person_id, ' .
                   'isr_person_name, ' .
                   'chr_person_id, ' .
                   'recipient_id' .
               ') values (';
    $content = array(
                   $pt_id,
                   $kf_date,
                   $st_date,
                   $wareki_flg1,
                   $wareki_flg2,
                   $isr_code,
                   $isr_id,
                   $relation,
                   $ch_rate,
                   $psn_id,
                   $psn_nm,
                   $ch_psn_id,
                   $rsp_id
               );

    $ins = insert_into_table($con, $sql, $content, $fname);

    if ($ins == 0) {
        pg_query($con, 'rollback');
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
} elseif ($sql_type == 'update') {
    // 更新
    $sql = 'update ptinsurance set';
    $set      = array(
                    'iss_date',
                    'acq_date',
                    'wareki_flg1',
                    'wareki_flg2',
                    'insurance_code',
                    'insurance_id',
                    'relation',
                    'charge_rate',
                    'isr_person_id',
                    'isr_person_name',
                    'chr_person_id',
                    'recipient_id'
                );
    $setvalue = array(
                    $kf_date,
                    $st_date,
                    $wareki_flg1,
                    $wareki_flg2,
                    $isr_code,
                    $isr_id,
                    $relation,
                    $ch_rate,
                    $psn_id,
                    $psn_nm,
                    $ch_psn_id,
                    $rsp_id
                );
    $cond = "where ptif_id = '" . $pt_id . "'";

    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);

    if ($upd == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, 'commit');
pg_close($con);

// 画面を再表示
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script type=\"text/javascript\">location.href=\"patient_insurance.php?session=$session&pt_id=$pt_id&key1=$url_key1&key2=$url_key2&page=$page\"</script>");
?>
