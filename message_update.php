<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 伝言メモ | 再送登録</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("show_select_values.ini"); ?>
<?
$fname=$PHP_SELF;

$session=qualify_session($session,$fname);
if($session=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$ward=check_authority($session,4,$fname);
if($ward=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
?>
<?
$con=connect2db($fname);
if($con=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

$select_message="select * from message where message_id='$message_id'";
//echo($select_message);
$result_select=pg_exec($con,$select_message);
if($result_select==false){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

$date=pg_result($result_select,$i,date);
$message_date=pg_result($result_select, $i,message_date);
$message_towhich_id=pg_result($result_select,$i,message_towhich_id);
$message_towhich_device=pg_result($result_select,$i,message_towhich_device);
$message_company=pg_result($result_select,$i,message_company);
$message_name=pg_result($result_select,$i,message_name);
$message_tel1=pg_result($result_select,$i,message_tel1);
$message_tel2=pg_result($result_select,$i,message_tel2);
$message_tel3=pg_result($result_select,$i,message_tel3);
if ($message_tel1 != "" && $message_tel2 != "" && $message_tel3 != "") {
    $message_tel = "{$message_tel1}-{$message_tel2}-{$message_tel3}";
} else {
    $message_tel = "";
}
$message_service=pg_result($result_select,$i,message_service);
$message=pg_result($result_select,$i,message);

$received_year=substr($date,0,4);
$received_month=substr($date,4,2);
$received_day=substr($date,6,2);
$received_hour=substr($date,8,2);
$received_minute=substr($date,10,2);
$received_date=$received_year."/".$received_month."/".$received_day."&nbsp;".$received_hour.":".$received_minute;
$received_date_numeric=$received_year.$received_month.$received_day.$received_hour.$received_minute;

$message_year=substr($message_date,0,4);
$message_month=substr($message_date,4,2);
$message_day=substr($message_date,6,2);
$message_hour=substr($message_date,8,2);
$message_minute=substr($message_date,10,2);

$select_empmst="select * from empmst where emp_id='$message_towhich_id'";
$result_select=pg_exec($con,$select_empmst);
if($result_select==false){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$emp_lt_nm=pg_result($result_select,0,emp_lt_nm);
$emp_ft_nm=pg_result($result_select,0,emp_ft_nm);
$message_towhich_email=pg_result($result_select,0,"emp_email2");
$message_towhich_email_mobile=pg_result($result_select,0,"emp_m_email");
$emp_nm = $emp_lt_nm." ".$emp_ft_nm;

// Eメール送受信設定を取得
$sql = "select use_email from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$use_email = pg_fetch_result($sel, 0, "use_email");
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="message_menu.php?session=<? echo($session); ?>"><img src="img/icon/b05.gif" width="32" height="32" border="0" alt="伝言メモ"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="message_menu.php?session=<? echo($session); ?>"><b>伝言メモ</b></a></font></td>
</tr>
</table>
<form name="message" action="./message_update_exe.php?session=<? echo($session); ?>&message_id=<? echo($message_id); ?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./message_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受信履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="./message_left.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >送信履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="message_update.php?session=<? echo($session); ?>&message_id=<? echo($message_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>再送登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="message_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ作成</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日時：</font></td>
<td width="480"><input name="message_date1" type="hidden" value="<? echo($message_year); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message_year); ?>/</font><input name="message_date2" type="hidden" value="<? echo($message_month); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message_month); ?>/</font><input name="message_date3" type="hidden" value="<? echo($message_day); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message_day); ?></font>&nbsp;<input name="message_date4" type="hidden" value="<? echo($message_hour); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message_hour); ?>：<input name="message_date5" type="hidden" value="<? echo($message_minute); ?>"><? echo($message_minute); ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">宛先：</font></td>
<td><input name="message_towhich_name" type="hidden" size="25" maxlength="250" value="<? echo($emp_nm); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_nm); ?></font>&nbsp;<input name="message_towhich_id" type="hidden" value="<? echo($message_towhich_id); ?>"><input name="message_towhich_email" type="hidden" value="<? echo($message_towhich_email); ?>"><input name="message_towhich_email_mobile" type="hidden" value="<? echo($message_towhich_email_mobile); ?>"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">相手先所属名：</font></td>
<td><input name="message_company" type="hidden" size="50" maxlength="50" value="<? echo($message_company); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message_company); ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">相手先氏名：</font></td>
<td><input name="message_name" type="hidden" size="25" maxlength="250" value="<? echo($message_name); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message_name); ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">電話番号：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message_tel); ?></font><input name="message_tel1" type="hidden" size="6" maxlength="6" value="<? echo($message_tel1); ?>"><input name="message_tel2" type="hidden" size="6" maxlength="6" value="<? echo($message_tel2); ?>"><input name="message_tel3" type="hidden" size="6" maxlength="6" value="<? echo($message_tel3); ?>"></td>
</tr>
<tr>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">用件：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「<? echo($received_date); ?>の伝言メモを確認してください」と表示されます。</font><input name="message_service" type="hidden" value="0"><input name="received_date_numeric" type="hidden" value="<? echo($received_date_numeric); ?>">
<br><textarea name="message" rows="5" cols="40" style="ime-mode: active;"></textarea>
</td>
</tr>
<? if ($use_email == "t") { ?>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メールの送信先：</font></td>
<td><?
if($message_towhich_device=="1"){
echo("<input name=\"message_towhich_device\" type=\"radio\" value=\"1\" checked>");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("Eメール");
echo("</font>");
echo("&nbsp;<input name=\"message_towhich_device\" type=\"radio\" value=\"2\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("携帯");
echo("</font>");
echo("&nbsp;<input name=\"message_towhich_device\" type=\"radio\" value=\"0\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("送らない");
echo("</font>");
}elseif($message_towhich_device == "2"){
echo("<input name=\"message_towhich_device\" type=\"radio\" value=\"1\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("Eメール");
echo("</font>");
echo("&nbsp;<input name=\"message_towhich_device\" type=\"radio\" value=\"2\" checked>");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("携帯");
echo("</font>");
echo("&nbsp;<input name=\"message_towhich_device\" type=\"radio\" value=\"0\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("送らない");
echo("</font>");
}else{
echo("<input name=\"message_towhich_device\" type=\"radio\" value=\"1\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("Eメール");
echo("</font>");
echo("&nbsp;<input name=\"message_towhich_device\" type=\"radio\" value=\"2\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("携帯");
echo("</font>");
echo("&nbsp;<input name=\"message_towhich_device\" type=\"radio\" value=\"0\" checked>");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("送らない");
echo("</font>");
}
?></td>
</tr>
<? } ?>
<tr>
<td height="22" colspan="2" align="right"><input type="submit" value="再送"></td>
</tr>
</table>
<? if ($use_email != "t") { ?>
<input type="hidden" name="message_towhich_device" value="0">
<? } ?>
</form>
</td>
</tr>
</table>
</body>
</html>