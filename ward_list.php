<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>マスターメンテナンス | 病棟一覧</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("show_select_values.ini"); ?>
<? require("show_ward_list.ini"); ?>
<?
// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$ward = check_authority($session,21,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// DBコネクション
$con = connect2db($fname);
?>
<script type="text/javascript">
function controlWardList(img, trID) {
	var trDisplay;
	if (img.className == 'close') {
		img.className = 'open';
		trDisplay = '';
	} else {
		img.className = 'close';
		trDisplay = 'none';
	}

	var trs = document.getElementsByTagName('tr');
	for (var i = 0, j = trs.length; i < j; i++) {
		if (trs[i].id == trID) {
			trs[i].style.display = trDisplay;
		}
	}
}

function deleteWard() {
	if (document.ward.elements['del_ward[]'] == undefined) {
		alert('削除可能な棟が存在しません');
		return;
	}

	if (document.ward.elements['del_ward[]'].length == undefined) {
		if (!document.ward.elements['del_ward[]'].checked) {
			alert('削除対象が選択されていません');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.ward.elements['del_ward[]'].length; i < j; i++) {
			if (document.ward.elements['del_ward[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('削除対象が選択されていません');
			return;
		}
	}

	if (confirm('削除します。よろしいですか？')) {
		document.ward.submit();
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">
form {margin:0;}
img.close {
	background-image:url("images/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("images/minus.gif");
	vertical-align:middle;
}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="building_list.php?session=<? echo($session); ?>"><b>病棟</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<form name="ward" action="ward_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<? if ($ward_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<? } ?>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="building_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">棟一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="building_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">棟登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="ward_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病棟一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="ward_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="ward_div_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟区分</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="room_equipment_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteWard();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<? show_building_list($con,$session,$bldg_cd,$fname); ?>
</table>
</td>
<!-- right -->
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
