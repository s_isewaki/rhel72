<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("referer_common.ini");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 12, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "event", $fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2 = pg_fetch_result($sel, 0, "menu2");
$menu2_1 = pg_fetch_result($sel, 0, "menu2_1");

// 施設一覧を取得
$sql = "select fcl_id, name from timegdfcl";
$cond = "order by fcl_id";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 行事情報を取得
$sql = "select fcl_id, fcl_detail, event_date, event_time, event_dur, event_name, event_content, time_flg, contact from timegd";
$cond = "where event_id = $e_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$fcl_id = pg_fetch_result($sel, 0, "fcl_id");
$fcl_detail = pg_fetch_result($sel, 0, "fcl_detail");
$event_date = pg_fetch_result($sel, 0, "event_date");
list($date1, $date2, $date3) = explode("-", $event_date);
$time_flg = pg_fetch_result($sel, 0, "time_flg");
if ($time_flg == "t") {
	$event_time = pg_fetch_result($sel, 0, "event_time");
	$event_dur = pg_fetch_result($sel, 0, "event_dur");
	list($start_hrs, $start_min) = explode(":", $event_time);
	list($dur_hrs, $dur_min) = explode(":", $event_dur);
}
$event_name = pg_fetch_result($sel, 0, "event_name");
$event_content = pg_fetch_result($sel, 0, "event_content");
$contact = pg_fetch_result($sel, 0, "contact");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 院内行事登録/法人内行事登録
$event_reg_title =  $_label_by_profile["EVENT_REG"][$profile_type];
?>
<title>CoMedix <? echo($event_reg_title); ?> | 行事詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setTimeDisabled() {
	var disabled = document.mainform.time_flg.checked;
	document.mainform.start_hrs.disabled = disabled;
	document.mainform.start_min.disabled = disabled;
	document.mainform.dur_hrs.disabled = disabled;
	document.mainform.dur_min.disabled = disabled;
}

function actDelete() {

	if (confirm('削除してよろしいですか？')) {
		document.mainform.sch_del.value="on";
		document.mainform.submit();
	}

}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setTimeDisabled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

	<? if ($sch != "on") { //スケジュール機能から飛んできた場合にはヘッダリストは表示しない?>

	<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="time_guide_list.php?session=<? echo($session); ?>"><img src="img/icon/b22.gif" width="32" height="32" border="0" alt="<? echo($event_reg_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="time_guide_list.php?session=<? echo($session); ?>"><b><? echo($event_reg_title); ?></b></a></font></td>
</tr>
</table>
	<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($menu2); ?></b></a> &gt; <a href="intra_info_event.php?session=<? echo($session); ?>"><b><? echo($menu2_1); ?></b></a> &gt; <a href="time_guide_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_info_event.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
	<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_list.php?session=<? echo($session); ?>&fcl_id=<? echo($srch_fcl_id); ?>&date=<? echo($srch_date); ?>&mode=display&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行事一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="time_guide_detail.php?session=<? echo($session); ?>&e_id=<? echo($e_id); ?>&srch_fcl_id=<? echo($srch_fcl_id); ?>&srch_date=<? echo($srch_date); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>行事詳細</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="time_guide_class_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="time_guide_class_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_fcl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_fcl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>

<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="time_guide_update_confirm.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" width="140" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設</font></td>
<td><select name="fcl_id">
<option value="-">全体
<?
while ($row = pg_fetch_array($sel_fcl)) {
	$tmp_fcl_id = $row["fcl_id"];
	$tmp_name = $row["name"];
	echo("<option value=\"$tmp_fcl_id\"");
	if ($tmp_fcl_id == $fcl_id) {
		echo(" selected");
	}
	echo(">$tmp_name\n");
}
?>
</select><input type="text" name="fcl_detail" value="<? echo($fcl_detail); ?>" size="21" maxlength="50" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="date1"><? show_update_years($date1, 1); ?></select>/<select name="date2"><? show_select_months($date2); ?></select>/<select name="date3"><? show_select_days($date3); ?></select></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="start_hrs"><? show_hour_options_0_23($start_hrs, true); ?></select>：<select name="start_min"><? show_min_options_5($start_min); ?></select>〜<select name="dur_hrs"><? show_hour_options_0_23($dur_hrs, true); ?></select>：<select name="dur_min"><? show_min_options_5($dur_min); ?></select><br>
<input type="checkbox" name="time_flg" value="f"<? if ($time_flg == "f") {echo(" checked");} ?> onclick="setTimeDisabled();">指定しない
</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行事名</font></td>
<td><input name="event_name" type="text" size="50" maxlength="50" value="<? echo($event_name); ?>" style="ime-mode:active;"></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
<td><textarea name="event_content" rows="5" cols="40" style="ime-mode:active;"><? echo($event_content); ?></textarea></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></td>
<td><textarea name="contact" cols="30" rows="3" style="ime-mode:active;"><? echo($contact) ?></textarea></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新">

<? if ($sch == "on") { //スケジュール機能から飛んできた場合には削除ボタンを表示?>
<input type="button" value="削除" onclick="actDelete()">
<? } ?>

</td>

</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="e_id" value="<? echo($e_id); ?>">
<input type="hidden" name="srch_fcl_id" value="<? echo($srch_fcl_id); ?>">
<input type="hidden" name="srch_date" value="<? echo($srch_date); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="sch" value="<? echo($sch); ?>">
<input type="hidden" name="sch_del" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
