<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションをチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限をチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

for ($i = 1; $i <= 9; $i++) {
	$varname = "required$i";
	if ($$varname != "t") {
		$$varname = "f";
	}

	if ($$varname == "t") {
		$varname = "display$i";
		$$varname = "t";
	}

	$varname = "display$i";
	if ($$varname != "t") {
		$$varname = "f";
	}
}

// 担当医が「必須」の場合、診療科も「必須」
if ($required6 == "t") {
	$required5 = "t";
	$display5 = "t";
}

// 診療科が「必須」の場合、退院先施設名も「必須」
if ($required5 == "t") {
	$required4 = "t";
	$display4 = "t";
}

// 担当医が「表示」の場合、診療科も「表示」
if ($display6 == "t") {
	$display5 = "t";
}

// 診療科が「表示」の場合、退院先施設名も「表示」
if ($display5 == "t") {
	$display4 = "t";
}

$set = array();
$setvalue = array();
for ($i = 1; $i <= 9; $i++) {
	$varname = "required$i";
	$set[] = $varname;
	$setvalue[] = $$varname;

	$varname = "display$i";
	$set[] = $varname;
	$setvalue[] = $$varname;
}

// データベースに接続
$con = connect2db($fname);

// 表示設定を更新
$sql = "update bedcheckout set";
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 元画面に戻る
echo "<script type=\"text/javascript\">location.href = 'bed_check_setting_out.php?session=$session';</script>";
?>
