<?
require_once("about_comedix.php");
require("./conf/sql.inf");

function get_age($birth) {
  $yr = date("Y");
  $md = date("md");
  $birth_yr = substr($birth, 0, 4);
  $birth_md = substr($birth, 4, 8);
  $age = $yr - $birth_yr;
  if ($md < $birth_md) {
    $age = $age - 1;
  }
  return $age;
}
function format_date($ymd) {
  if (strlen($ymd) === 8) {
    return preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $ymd);
  }
  return "";
}


$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
  js_login_exit();
}

$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
  js_login_exit();
}

$con = connect2db($fname);

//================================================
// 傷病名一覧
//================================================
$sql = "select * from sum_pt_disease";
$cond = q("where ptif_id = '%s' order by start_ymd, id", $pt_id);
$sel1 = select_from_table($con,$sql,$cond,$fname);
if ($sel1 == 0) {
  pg_close($con);
  js_error_exit();
}


//================================================
// 患者名・患者カナ・患者生年月日・患者性別
//================================================
$sql = "select * from ptifmst";
$cond = "where ptif_id = '".$_REQUEST["pt_id"]."' and ptif_del_flg = 'f' order by ptif_id";
$sel2 = select_from_table($con,$sql,$cond,$fname);
if($sel2 == 0){
  pg_close($con);
  js_error_exit();
}

//================================================
// 患者入院情報
//================================================
$sql =
" select inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_in_dt, ward.ward_name, ptrm.ptrm_name, dr.dr_nm, sect.sect_nm".
" from inptmst inpt".
" left outer join bldgmst bldg1 on (bldg1.bldg_cd = inpt.bldg_cd)".
" left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)".
" left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
" left outer join sectmst sect on (sect.enti_id = inpt.inpt_enti_id and sect.sect_id = inpt.inpt_sect_id)".
" left outer join drmst dr on (dr.enti_id = inpt.inpt_enti_id and dr.sect_id = inpt.inpt_sect_id and dr.dr_id = inpt.dr_id)";
$cond = "where inpt.ptif_id = '". $_REQUEST["pt_id"]."'";
$sel3 = select_from_table($con,$sql,$cond,$fname);
$inpt_row = pg_fetch_array($sel3);

//========================================================================================
// PDF印刷
//========================================================================================
if (@$_REQUEST["print_pdf"]) {
  require_once('fpdf153/mbfpdf.php');
  class CustomMBFPDF extends MBFPDF {
    var $data = array();
    var $most_btm = 285;
    var $mgn = 15.0;
    var $rowCount = 0;

    function init($sel1, $sel2, $inpt_row) {
      $this->rowCount = pg_num_rows($sel1);
      $this->data["kanja_name"] = pg_result($sel2,0,"ptif_lt_kaj_nm") ." ". pg_result($sel2,0,"ptif_ft_kaj_nm");
      $this->data["kanja_kana"] = pg_result($sel2,0,"ptif_lt_kana_nm") ." ".pg_result($sel2,0,"ptif_ft_kana_nm");
      $birth = pg_result($sel2,0,"ptif_birth");
      $this->data["age"] = get_age($birth);
      $this->data["birth_ymd"] = substr($birth,0,4)."/".substr($birth,4,2)."/".substr($birth,6,2);
      $gendary = array("", "（ 男性 ）", "（ 女性 ）");
      $this->data["gender"] = @$gendary[(int)@pg_result($sel2,0,"ptif_sex")];
      $this->data["dr_nm"] = $inpt_row["dr_nm"];
      $this->data["ward_name"] = $inpt_row["ward_name"];
      $this->data["sect_nm"] = $inpt_row["sect_nm"];
    }
        // 文字列幅計算をオーバーライド
        function GetMBStringWidth($s) {
            return parent::GetMBStringWidth(mb_convert_encoding($s, "euc-jp", "sjis"));
        }
        //改ページのヘッダ
        function Header(){
          $mgn = $this->mgn;
          //患者情報
          $this->SetFont(GOTHIC,'',18);
          $this->Cell(0, 15, "傷病名一覧",'',1,'C');
          $this->SetFont(GOTHIC,'',12);
          $X = $this->GetX();
          $Y = $this->GetY();

          //$this->Cell(0, 5.0, "",'1',1,'');
          //$this->Cell(0, 5.0, "",'1',1,'');
          //$this->Cell(0, 5.0, "",'1',1,'');
          //$this->Cell(0, 5.0, "",'1',1,'');
          //$this->Cell(0, 5.0, "",'1',1,'');
          //$this->setY($Y);
          //$this->setX(90);
          //$this->Cell(105, 25.0, "",'1',1,'');

          $this->setY($Y);
          $this->SetFont(GOTHIC,'',9.5);
          $this->Cell(90, 5, "患 者 ID： ".$_REQUEST['pt_id'],'',2,'L');
          $this->Cell(90, 5, "",'',2,'L');
          $this->Cell(45, 5, "患 者 名： ".$this->data["kanja_name"],'',0,'L');
          $this->Cell(45, 5, $this->data["gender"],'',2,'L');
          $this->setX($mgn);
          $this->Cell(45, 5, "生年月日： ".$this->data["birth_ymd"],'',0,'L');
          $this->Cell(45, 5, "（ ".$this->data["age"]."才 ）",'',2,'L');

          $this->setY($Y);
          $this->setX(110);
          $this->Cell(85, 5, "病 　 棟： ".$this->data["ward_name"],'',2,'L');
          $this->Cell(85, 5, "診 療 科： ".$this->data["sect_nm"],'',2,'L');
          $this->Cell(85, 5, "担 当 医： ".$this->data["dr_nm"],'',2,'L');
          $this->Cell(85, 5, "",'',2,'L');
          $this->Cell(85, 5, "作 成 日： ".date("Y年m月d日"),'',2,'R');

          $this->setY($Y+6);
          $this->setX(33);
          $this->SetFont(GOTHIC,'',7);
          $this->Cell(90, 5, $this->data["kanja_kana"],'',2,'L');
          $this->setY(55);
          $this->setX(0);

          $this->setX($mgn);
          $Y = $this->GetY();
          $height = $this->most_btm - $mgn - $Y;
          $this->Cell(0, $height, "",'1',1,'');
          $this->Line($mgn+80, $Y, $mgn+80, $this->most_btm - $mgn);
          $this->Line($mgn+100, $Y, $mgn+100, $this->most_btm - $mgn);
          $this->Line($mgn+108, $Y, $mgn+108, $this->most_btm - $mgn);
          $this->Line($mgn+118, $Y, $mgn+118, $this->most_btm - $mgn);
          $this->Line($mgn+138, $Y, $mgn+138, $this->most_btm - $mgn);
          $this->Line($mgn+148, $Y, $mgn+148, $this->most_btm - $mgn);
          $this->Line($mgn+162, $Y, $mgn+162, $this->most_btm - $mgn);

          $this->setX($mgn);
          $this->setY($Y);

          $this->SetFont(GOTHIC,'',10);
          $this->Cell(80, 5, "傷病名",'',0,'C');
          $this->Cell(20, 5, "開始年月日",'',0,'C');
          $this->Cell(8,  5, "主",'',0,'C');
          $this->Cell(10, 5, "特疾",'',0,'C');
          $this->Cell(20, 5, "終了年月日",'',0,'C');
          $this->Cell(10, 5, "転帰",'',0,'C');
          $this->Cell(14, 5, "ICD10",'',0,'C');
          $this->Cell(18, 5, "レセ電算",'',0,'C');

          $this->setX($mgn);
          $this->setY($Y+5);
          for ($idx=1; $idx<=30; $idx++) {
            $this->Cell(0, 7.0, "",'1',1,'');
          }
          $this->setY($Y+5);
        }
        //改ページのフッター
        function Footer(){
            if ($this->rowCount<=30) return;
            $this->SetY(-15);
            $this->SetFont(GOTHIC,'',8);
            $this->Cell(0, 5.0, $this->PageNo() . 'ページ', 0, 1, 'C');
        }

        //｢EUC-JP → SJIS｣変換のためのオーバーライド
        //MultiCell()もWrite()もこのCell()で表示しているので、オーバーライドはCell()だけでOK
        function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '') {
          $enc = mb_internal_encoding();
          if($enc != "sjis-win"){
            $txt = mb_convert_encoding($txt, "sjis-win", $enc) ;
            parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link) ;
          }
        }
    }

    $pdf=new CustomMBFPDF();
    $pdf->init($sel1, $sel2, $inpt_row);
    $pdf->AddMBFont(GOTHIC, 'SJIS');
    $pdf->SetMargins($pdf->mgn, $pdf->mgn, $pdf->mgn);
    $pdf->SetAutoPageBreak(false, 0);
    $pdf->Open();
    $pdf->AddPage();

  $cnt = 0;
  while ($row = pg_fetch_array($sel1)) {
    $cnt++;
    if ($cnt>30) {
      $pdf->AddPage();
      $cnt= 1;
    }
    $pdf->setX($pdf->mgn);
    $tenki = "";
    if ($row["outcome"]=="1") $tenki="治癒";
    if ($row["outcome"]=="2") $tenki="死亡";
    if ($row["outcome"]=="3") $tenki="中止";
    if ($row["outcome"]=="4") $tenki="移行";
    $pdf->Cell(80, 7, $row["disease_name"],'',0,'L');
    $pdf->Cell(20, 7, format_date($row["start_ymd"]),'',0,'C');
    $pdf->Cell(8,  7, ($row["is_primary"]=="t"?"主":"") ,'',0,'C');
    $pdf->Cell(10, 7, ($row["is_specified"]=="t"?"特":"") ,'',0,'C');
    $pdf->Cell(20, 7, format_date($row["end_ymd"]),'',0,'C');
    $pdf->Cell(10, 7, $tenki,'',0,'C');
    $pdf->Cell(14, 7, $row["icd10"],'',0,'L');
    $pdf->Cell(18, 7, $row["rece_code"],'',2,'R');
  }
  $pdf->Output();
  die;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix メドレポート｜傷病名一覧</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
// この子画面の現在の高さを求める
function getCurrentHeight(){
  if (!window.parent) return 0;
  var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
  h = Math.max(h, document.body.scrollHeight);
  h = Math.max(h, 300);
  return (h*1+50);
}

// この子画面の最初の高さを親画面(summary_rireki.php)に通知しておく。onloadで呼ばれる。
// 一旦サイズを小さくしたのち、正しい値を再通知するようにしないといけない。
function setDefaultHeight(){
  if (!window.parent) return;
  window.parent.setDefaultHeight("hidari", 300);
  resetIframeHeight();
  var h = getCurrentHeight();
  if (h) window.parent.setDefaultHeight("hidari",h);
}

// この子画面がリサイズされたら、高さを再測定して親画面(summary_rireki.php)に通知する。
// 「サマリ内容ポップアップ」が開くときも呼ばれる。onloadでも呼ばれる。
// 親画面はこの子画面のIFRAMEサイズを変更する。
// 一旦大きくしたIFRAMEのサイズは通常小さくできない。
// よって、getCurrentHeightでサイズ測定する前に、画面を一旦元に戻さないといけない。
function resizeIframeHeight(){
  resetIframeHeight();
  var h = getCurrentHeight();
  if (h) window.parent.resizeIframeHeight("hidari", h);
}

// この子画面の高さを、最初の高さに戻す。「サマリ内容ポップアップ」が閉じるとき呼ばれる。
// 親画面(summary_rireki.php)は、この子画面IFRAMEの高さを最初の値に戻す。
function resetIframeHeight(){
  if (!window.parent) return;
  window.parent.resetIframeHeight("hidari");
}

function dosubmit() {
  document.frm.itm_ord.value = parent.document.getElementById("itm_ord").value;
  document.frm.submit();
}

var registerWindow;
function openRegisterWindow() {
  registerWindow = window.open('summary_disease_register.php?session=<?=$session?>&pt_id=<?=$pt_id?>', 'disease', 'directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=480');
}

var detailWindow;
function openDetailWindow(id) {
  detailWindow = window.open('summary_disease_detail.php?session=<?=$session?>&id=' + id, 'disease', 'directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=480');
}

function updateDiseaseList() {
  $.post(
    'summary_disease_list_update.php',
    $('#mainform').serialize(),
    function(data, textStatus, jqXHR) {
      alert(data);
      if (data == '更新しました。') {
        location.reload(true);
      }
    }
  );
}

function closeWindows() {
  if (registerWindow && !registerWindow.closed) {
    registerWindow.close();
  }
  if (detailWindow && !detailWindow.closed) {
    detailWindow.close();
  }
}

function highlightCells(tr) {
  changeCellsColor(tr, '#fefc8f');
}
function dehighlightCells(tr) {
  changeCellsColor(tr, '');
}
function changeCellsColor(tr, color) {
  tr.style.backgroundColor = color;
}
function printPDF() {
  document.frmPrintPDF.submit();
}
</script>
</head>
<body style="background-color:#f4f4f4" onload="setDefaultHeight();resizeIframeHeight();" onunload="closeWindows();">

<div style="float:right">
<input type="button" value="印刷" onclick="printPDF();">
<input type="button" value="更新" onclick="updateDiseaseList();"<? if (pg_num_rows($sel1) === 0) {echo(" disabled=\"disabled\"");} ?>>
<input type="button" value="追加" onclick="openRegisterWindow();">
</div>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#63bafe"><a href="./summary_rireki_table.php?session=<?=$session?>&pt_id=<?=$pt_id?>" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#0055dd">医療文書</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#63bafe"><a href="#" onclick="dosubmit();" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#0055dd">検査時系列</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#156cec"><a href="summary_disease_list.php?session=<?=$session?>&pt_id=<?=$pt_id?>" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>傷病名一覧</b></font></a></td>
</tr>
</table>

<form name="frmPrintPDF" action="summary_disease_list.php" method="post" target="_blank">
  <input type="hidden" name="print_pdf" value="1" />
  <input type="hidden" name="session" value="<?=$session?>">
  <input type="hidden" name="pt_id" value="<?=$pt_id?>">
</form>
<form name="frm" action="summary_kensakekka_timeseries.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="pt_id" value="<?=$pt_id?>">
<input type="hidden" style="width:100%;" name="itm_ord" id="itm_ord" value="">
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#156cec"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<form id="mainform">
<table class="prop_table" cellspacing="1">
<tr>
<th>傷病名</th>
<th>開始年月日</th>
<th>主</th>
<th>特疾</th>
<th>終了年月日</th>
<th>転帰</th>
<th>ICD10</th>
<th>レセ電算</th>
</tr>
<?
$line_no = 1;
while ($row = pg_fetch_array($sel1)) {
?>
<tr>
<td style="cursor:pointer;" onmouseover="highlightCells(this.parentNode);" onmouseout="dehighlightCells(this.parentNode);" onclick="openDetailWindow('<? eh_jsparam($row["id"]); ?>');">
<? eh($row["disease_name"]); ?>
<input type="hidden" name="dis<? eh($row["id"]); ?>[line_no]" value="<? eh($line_no); ?>">
</td>
<td><input type="text" name="dis<? eh($row["id"]); ?>[start_ymd]" value="<? eh(format_date($row["start_ymd"])); ?>" size="12" maxlength="10" style="ime-mode:inactive;"></td>
<td>
<select name="dis<? eh($row["id"]); ?>[is_primary]">
<option value="f"></option>
<option value="t"<? if ($row["is_primary"] === "t") {echo(" selected=\"selected\"");} ?>>主</option>
</select>
</td>
<td>
<select name="dis<? eh($row["id"]); ?>[is_specified]">
<option value="f"></option>
<option value="t"<? if ($row["is_specified"] === "t") {echo(" selected=\"selected\"");} ?>>特</option>
</select>
</td>
<td><input type="text" name="dis<? eh($row["id"]); ?>[end_ymd]" value="<? eh(format_date($row["end_ymd"])); ?>" size="12" maxlength="10" style="ime-mode:inactive;"></td>
<td>
<select name="dis<? eh($row["id"]); ?>[outcome]">
<option value=""></option>
<option value="1"<? if ($row["outcome"] === "1") {echo(" selected=\"selected\"");} ?>>治癒</option>
<option value="2"<? if ($row["outcome"] === "2") {echo(" selected=\"selected\"");} ?>>死亡</option>
<option value="3"<? if ($row["outcome"] === "3") {echo(" selected=\"selected\"");} ?>>中止</option>
<option value="4"<? if ($row["outcome"] === "4") {echo(" selected=\"selected\"");} ?>>移行</option>
</select>
</td>
<td style="cursor:pointer;" onmouseover="highlightCells(this.parentNode);" onmouseout="dehighlightCells(this.parentNode);" onclick="openDetailWindow('<? eh_jsparam($row["id"]); ?>');"><? eh($row["icd10"]); ?></td>
<td style="cursor:pointer;" onmouseover="highlightCells(this.parentNode);" onmouseout="dehighlightCells(this.parentNode);" onclick="openDetailWindow('<? eh_jsparam($row["id"]); ?>');"><? eh($row["rece_code"]); ?></td>
</tr>
<?
  $line_no++;
}
?>
</table>
</form>

</body>
</html>
<? pg_close($con); ?>