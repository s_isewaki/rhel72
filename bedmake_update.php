<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//入力チェック
if (!is_array($bdmk_id)) {
	echo("<script language=\"javascript\">alert(\"更新対象を選択してください\");</script>\n");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$today = date("Ymd");
$now = date("Hi");

//----------Transaction begin----------
$con = connect2db($fname);
pg_exec($con,"begin transaction");

foreach ($bdmk_id as $tmp_bdmk_id) {

	$sql = "update bedmake set";
	$set = array("comp_dt", "comp_tm");
	$setvalue = array($today, $now);
	$cond = "where bdmk_id = '$tmp_bdmk_id'";
	$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($up == 0) {
		pg_exec($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

//----------コミットする----------
pg_exec($con, "commit");
pg_close($con);

echo("<script language='javascript'>location.href = 'bedmake_operation.php?session=$session';</script>");
?>
