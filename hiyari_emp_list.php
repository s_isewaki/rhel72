<?php

require_once('Cmx.php');

$item_id = $_GET['input_div'];

$callback = 0;
if (!empty($_GET['call_back_mode'])) {
    $callback = ($_GET['mode'] === '2') ? 2 : 1;
}

header("Location: " . CMX_BASE_URL . "/emplist/emplist.php?mode=hiyari&callback={$callback}&item_id={$item_id}");
