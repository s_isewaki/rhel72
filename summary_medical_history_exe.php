<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//権限チェック
//==============================
$dept = check_authority($session,57,$fname);
if($dept == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//入力チェック
//==============================
//特になし。
//if($summary_problem ==""){
//	echo("<script language=\"javascript\">alert(\"プロブレムを入力してください\");</script>\n");
//	echo("<script language=\"javascript\">history.back();</script>\n");
//	exit;
//}

//==============================
//ＤＢのコネクション作成(トランザクション開始)
//==============================
$con = connect2db($fname);
pg_query($con, "begin transaction");

//==========================================================================================
//病歴テーブルに登録
//==========================================================================================

$now_history_sql = pg_escape_string($now_history);
$family_history_sql = pg_escape_string($family_history);

//==============================
//ＤＢデータの存在有無判定
//==============================
$is_new = true;
$tbl = "select count(*) from medical_history";
$cond = "where pt_id = '$pt_id'";
$sel_exist = select_from_table($con,$tbl,$cond,$fname);
if ($sel_exist == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$num = pg_result($sel_exist,0,"count");
if($num > 0)
{
	//既に登録
	$is_new = false;
}

//==============================
//ＤＢ登録
//==============================
if($is_new)
{
	$content = array($pt_id, $now_history_sql, $family_history_sql);
	$intbl = "insert into medical_history (pt_id,now_history,family_history) values(";
	$in = insert_into_table($con,$intbl,$content,$fname);
	if($in == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

//==============================
//ＤＢ更新
//==============================
else
{
	$sql = "update medical_history set";
	$set = array("now_history", "family_history");
	$setvalue = array($now_history_sql, $family_history_sql);
	$cond = "where pt_id = '$pt_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

//==========================================================================================
//既往歴テーブルに登録
//==========================================================================================

//==============================
//ＤＢデータを削除
//==============================
$sql = "delete from medical_history_past";
$cond = "where pt_id = '$pt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//==============================
//ＤＢ登録
//==============================

$number = 0;
for($i=0;$i<$past_num;$i++)
{
	$past_about_date_param = "past_about_date_$i";
	$past_contents_param = "past_contents_$i";
	$past_about_date_sql = pg_escape_string($$past_about_date_param);
	$past_contents_sql = pg_escape_string($$past_contents_param);
	
	//入力が空の場合は登録しない。（上に詰める）
	if($past_about_date_sql=="" && $past_contents_sql=="")
	{
		continue;
	}
	$number = $number + 1;
	
	$content = array($pt_id, $number, $past_about_date_sql, $past_contents_sql);
	$intbl = "insert into medical_history_past (pt_id,number,about_date,contents) values(";
	$in = insert_into_table($con,$intbl,$content,$fname);
	if($in == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

//==============================
// ＤＢコネクション切断(トランザクション終了)
//==============================
pg_query($con, "commit");
pg_close($con);

//==============================
// 再度入力画面を表示
//==============================
echo("<script language=\"javascript\">location.href=\"./summary_medical_history.php?session=$session&pt_id=$pt_id\";</script>\n");
exit;

?>