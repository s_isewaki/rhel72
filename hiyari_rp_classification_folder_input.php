<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_post_select_box.ini");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once('Cmx/View/Smarty.php');
require_once('Cmx/Model/SystemConfig.php');
//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');



//評価予定期日の表示の有無
$sysConf = new Cmx_SystemConfig();
$predetermined_eval = $sysConf->get('fantol.predetermined.eval');


//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//ポストバック時の処理
//==============================
if(! empty($is_postback))
{

    //==============================
    //入力チェック
    //==============================

    //未入力チェック
    if($folder_name == "")
    {
        //未入力エラー
        echo("<script language=\"javascript\">alert(\"フォルダ名が入力されていません。\");</script>\n");
        echo("<script language=\"javascript\">history.back();</script>\n");
        exit;
    }

    //重複名称チェック
    $folder_name_sql = pg_escape_string($folder_name);
    $sql = "select count(*) from inci_classification_folder where folder_name = '$folder_name_sql'";
    if($common_flg == "t" )
    {
        //共有フォルダ同士で同名はNG
        $sql .=  " and common_flg ";
        $tmp_common_flg_name = "共有フォルダ";
    }
    else
    {
        //個人単位で、個人フォルダ同士で同名はNG
        $sql .=  " and not common_flg and creator_emp_id = '$emp_id' ";
        $tmp_common_flg_name = "個人フォルダ";
    }
    if($folder_id != "")
    {
        $sql .=  " and not folder_id = $folder_id";
    }
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }
    if (pg_fetch_result($sel,0,"count") != 0 || $folder_name == "全て")
    {
        //名称重複エラー
        echo("<script language=\"javascript\">alert(\"同じ名前の{$tmp_common_flg_name}が登録されています。\");</script>\n");
        echo("<script language=\"javascript\">history.back();</script>\n");
        exit;
    }

    // AND_OR条件のNULLチェック
    if($and_or_div == "")
    {
        $input_cnt = 0;

        //フラグ系項目

        //患者影響レベル
        if($inci_level_regist_flg == "f")
        {
            $input_cnt++;
        }
        //発生要因
        if($happened_cause_regist_flg == "f")
        {
            $input_cnt++;
        }
        //インシデントの背景・要因
        if($inci_background_regist_flg == "f")
        {
            $input_cnt++;
        }
        //改善策
        if($improve_plan_regist_flg == "f")
        {
            $input_cnt++;
        }
        //効果の確認
        if($effect_confirm_regist_flg == "f")
        {
            $input_cnt++;
        }
        //警鐘事例
        if($warning_case_regist_flg == "t")
        {
            $input_cnt++;
        }
        //警鐘事例
        if($analysis_target_flg == "t")
        {
            $input_cnt++;
        }

        //入力系項目


        //表題へのキーワード
        if($title_keyword != "")
        {
            $input_cnt++;
        }
        //インシデントの内容へのキーワード
        if($incident_contents_keyword != "")
        {
            $input_cnt++;
        }
        //報告部署(第二階層以降がある場合、第一階層は必ず設定されている。)
        if($search_emp_class != "")
        {
            $input_cnt++;
        }
        //報告者名
        if($search_emp_name != "")
        {
            $input_cnt++;
        }
        //報告者職種
        if($search_emp_job != "")
        {
            $input_cnt++;
        }
        //患者ID
        if($patient_id != "")
        {
            $input_cnt++;
        }
        //患者年齢(FromもしくはToの何れか入力あり)
        if($patient_year_from != "" || $patient_year_to != "")
        {
            $input_cnt++;
        }
        //患者氏名
        if($patient_name != "")
        {
            $input_cnt++;
        }
        //評価予定日
        if($evaluation_date != "")
        {
            $input_cnt++;
        }

        //患者影響レベル
        if($use_level != "")
        {
            $input_cnt++;
        }
        //ヒヤリハット分類
        if($hiyari_summary != "")
        {
            $input_cnt++;
        }
        //インシデントの概要
        if($use_120 != "")
        {
            $input_cnt++;
        }

        //２つ以上条件が指定されている場合
        if($input_cnt > 1)
        {
            //強制的にＡＮＤに設定
            $and_or_div = "0";
        }
    }

    //==============================
    //入力系項目をDB値に変換
    //==============================

    $inci_summary                = ($inci_summary == "-") ? null : $inci_summary;
    $title_keyword               = ($title_keyword == "") ? null : pg_escape_string($title_keyword);
    $incident_contents_keyword   = ($incident_contents_keyword == "") ? null : pg_escape_string($incident_contents_keyword);
    $emp_class_condition         = ($search_emp_class == "") ? null : $search_emp_class;
    $emp_attribute_condition     = ($search_emp_attribute == "") ? null : $search_emp_attribute;
    $emp_dept_condition          = ($search_emp_dept == "") ? null : $search_emp_dept;
    $emp_room_condition          = ($search_emp_room == "") ? null : $search_emp_room;
    $emp_name_condition          = ($search_emp_name == "") ? null : pg_escape_string($search_emp_name);
    $emp_job_condition           = ($search_emp_job == "") ? null : $search_emp_job;
    $patient_id_condition        = ($patient_id == "") ? null : pg_escape_string($patient_id);
    $patient_year_from_condition = ($patient_year_from == "") ? null : $patient_year_from;
    $patient_year_to_condition   = ($patient_year_to == "") ? null : $patient_year_to;
    $patient_name_condition      = ($patient_name == "") ? null : pg_escape_string($patient_name);
    $evaluation_date             = ($evaluation_date == "") ? null : $evaluation_date;

    //==============================
    //発生場面・内容送信データの収集
    //==============================

    $condition_400 = null;
    for($item_list_400_index = 0; $item_list_400_index < 10; $item_list_400_index++)
    {
        $bamen_list_param = "bamen_$item_list_400_index";
        $naiyo_list_param = "naiyo_$item_list_400_index";

        $condition_400[$item_list_400_index][0]['flg'] = in_array($item_list_400_index,$use_400_bamen);
        $condition_400[$item_list_400_index][1]['flg'] = in_array($item_list_400_index,$use_400_naiyo);
        $condition_400[$item_list_400_index][0]['list'] = $$bamen_list_param;
        $condition_400[$item_list_400_index][1]['list'] = $$naiyo_list_param;
    }

    $condition_1000 = null;
    $item_list_900 = get_superitem_list($con, $fname);
    foreach($item_list_900 as $code_900 => $name_900){
        $item_list_1000_index = $code_900 - 1;

        $kind_list_param    = "kind_{$item_list_1000_index}";
        $scene_list_param   = "scene_{$item_list_1000_index}";
        $content_list_param = "content_{$item_list_1000_index}";

        $condition_1000[$item_list_1000_index][0]['flg'] = in_array($item_list_1000_index, $use_1000_kind);
        $condition_1000[$item_list_1000_index][1]['flg'] = in_array($item_list_1000_index, $use_1000_scene);
        $condition_1000[$item_list_1000_index][2]['flg'] = in_array($item_list_1000_index, $use_1000_content);
        $condition_1000[$item_list_1000_index][0]['list'] = $$kind_list_param;
        $condition_1000[$item_list_1000_index][1]['list'] = $$scene_list_param;
        $condition_1000[$item_list_1000_index][2]['list'] = $$content_list_param;
    }

    $detail_condition_1000 = null;
    $item_list = get_item_list_all($con, $fname);
    foreach($item_list as $item_id => $item_name){
        $kind_list_param    = "kind_detail_{$item_id}";
        $scene_list_param   = "scene_detail_{$item_id}";
        $content_list_param = "content_detail_{$item_id}";

        $detail_condition_1000[$item_id][0]['flg'] = !empty($$kind_list_param) ? true : false;//in_array($item_id, $use_1000_detail_kind);
        $detail_condition_1000[$item_id][1]['flg'] = !empty($$scene_list_param) ? true : false;//in_array($item_id, $use_1000_detail_scene);
        $detail_condition_1000[$item_id][2]['flg'] = !empty($$content_list_param) ? true : false;//in_array($item_id, $use_1000_detail_content);
        $detail_condition_1000[$item_id][0]['list'] = $$kind_list_param;
        $detail_condition_1000[$item_id][1]['list'] = $$scene_list_param;
        $detail_condition_1000[$item_id][2]['list'] = $$content_list_param;
    }

    //==============================
    //DB処理
    //==============================
    if($mode == "new")
    {
        //==============================
        //トランザクション開始
        //==============================
        pg_query($con,"begin transaction");

        //==============================
        //フォルダIDの採番
        //==============================
        $sql = "SELECT max(folder_id) FROM inci_classification_folder";
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0){
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }
        $folder_id = pg_result($sel,0,"max");
        $folder_id++;

        //==============================
        //分類フォルダ追加
        //==============================
        $sql  = "insert into inci_classification_folder(";
        $sql .= " folder_id,";
        $sql .= " folder_name,";
        $sql .= " common_flg,";
        $sql .= " creator_emp_id,";
        $sql .= " and_or_div,";

        $sql .= " title_and_or_div,";
        $sql .= " incident_and_or_div,";

        $sql .= " inci_level_regist_flg,";
        $sql .= " happened_cause_regist_flg,";
        $sql .= " inci_background_regist_flg,";
        $sql .= " improve_plan_regist_flg,";
        $sql .= " effect_confirm_regist_flg,";
        $sql .= " warning_case_regist_flg,";
        $sql .= " analysis_target_flg,";

        $sql .= " inci_summary_condition,";
        $sql .= " title_keyword_condition,";
        $sql .= " incident_contents_condition,";
        $sql .= " emp_class_condition,";
        $sql .= " emp_attribute_condition,";
        $sql .= " emp_dept_condition,";
        $sql .= " emp_room_condition,";
        $sql .= " emp_name_condition,";
        $sql .= " emp_job_condition,";
        $sql .= " patient_id_condition,";
        $sql .= " patient_year_from_condition,";
        $sql .= " patient_year_to_condition,";
        $sql .= " patient_name_condition,";
        $sql .= " evaluation_date_condition,";

        $sql .= " use_level_list,";
        $sql .= " use_hiyari_summary_list,";
        $sql .= " use_incident_summary_list,";
        $sql .= " use_2010_summary_list";

        $sql .= ") values(";
        $registry_data = array(
            $folder_id,
            $folder_name_sql,
            $common_flg,
            $emp_id, //注意！：旧バージョンの共有フォルダの場合nullの可能性あり。共有フォルダでcreator_emp_idを使用する場合は注意が必要。
            $and_or_div,

            $title_and_or_div,
            $incident_and_or_div,

            $inci_level_regist_flg,
            $happened_cause_regist_flg,
            $inci_background_regist_flg,
            $improve_plan_regist_flg,
            $effect_confirm_regist_flg,
            $warning_case_regist_flg,
            $analysis_target_flg,

            "",//$inci_summary,は廃止
            $title_keyword,
            $incident_contents_keyword,
            $emp_class_condition,
            $emp_attribute_condition,
            $emp_dept_condition,
            $emp_room_condition,
            $emp_name_condition,
            $emp_job_condition,
            $patient_id_condition,
            $patient_year_from_condition,
            $patient_year_to_condition,
            $patient_name_condition,
            $evaluation_date,

            join ( ",",$use_level),//CSV
            join ( ",",$hiyari_summary),//CSV
            join ( ",",$use_120),//CSV
            join ( ",",$use_900)//CSV
        );

        $result = insert_into_table($con, $sql, $registry_data, $fname);
        if ($result == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //==============================
        //発生場面・内容の条件追加
        //==============================

        for($item_list_400_index = 0; $item_list_400_index < 10; $item_list_400_index++)
        {
            for($target_item_400 = 0; $target_item_400 < 2; $target_item_400++)
            {
                if($condition_400[$item_list_400_index][$target_item_400]['flg'])
                {
                    $use_list_400 = $condition_400[$item_list_400_index][$target_item_400]['list'];

                    $sql  = "insert into inci_classification_folder_400_condition(";
                    $sql .= " folder_id,";
                    $sql .= " index_400,";
                    $sql .= " target_item,";
                    $sql .= " use_list";
                    $sql .= ") values(";
                    $registry_data = array(
                        $folder_id,
                        $item_list_400_index,
                        $target_item_400,
                        join ( ",",$use_list_400)//CSV
                    );

                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }


        //==============================
        //概要2010の条件追加
        //==============================
        $item_list_900 = get_superitem_list($con, $fname);
        foreach($item_list_900 as $code_900 => $name_900){
            $item_list_1000_index = $code_900 - 1;

            // 0:種類 1:場面 2:内容
            for($target_item_1000 = 0; $target_item_1000 < 3; $target_item_1000++)
            {
                if($condition_1000[$item_list_1000_index][$target_item_1000]['flg'])
                {
                    $use_list_1000 = $condition_1000[$item_list_1000_index][$target_item_1000]['list'];

                    $sql  = "insert into inci_classification_folder_1000_condition(";
                    $sql .= " folder_id,";
                    $sql .= " index_1000,";
                    $sql .= " code,";
                    $sql .= " use_list";
                    $sql .= ") values(";
                    $registry_data = array(
                        $folder_id,
                        $item_list_1000_index,
                        $target_item_1000,
                        join ( ",",$use_list_1000)//CSV
                    );
                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }

        $item_list = get_item_list_all($con, $fname);
        foreach($item_list as $item_id => $item_name){
            // 0:種類 1:場面 2:内容
            for($target_item_1000 = 0; $target_item_1000 < 3; $target_item_1000++)
            {
                if($detail_condition_1000[$item_id][$target_item_1000]['flg'])
                {
                    $use_detail_list_1000 = $detail_condition_1000[$item_id][$target_item_1000]['list'];

                    $sql  = "insert into inci_classification_folder_1000_detail_condition(";
                    $sql .= " folder_id,";
                    $sql .= " use_index,";
                    $sql .= " code,";
                    $sql .= " use_detail_list";
                    $sql .= ") values(";
                    $registry_data = array(
                        $folder_id,
                        $item_id,
                        $target_item_1000,
                        join ( ",",$use_detail_list_1000)//CSV
                    );

                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }


        //==============================
        //トランザクション終了
        //==============================
        pg_query($con, "commit");

        //==============================
        //自分画面を閉じる。
        //==============================
        echo("<script language='javascript'>");
        echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
        echo("window.close();");
        echo("</script>");
        exit;

    }
    elseif($mode == "update")
    {
        //==============================
        //トランザクション開始
        //==============================
        pg_query($con,"begin transaction");

        //==============================
        //分類フォルダ更新
        //==============================

        $sql = "update inci_classification_folder set";
        $set = array(
                "folder_name",
                "and_or_div",

                "title_and_or_div",
                "incident_and_or_div",

                "inci_level_regist_flg",
                "happened_cause_regist_flg",
                "inci_background_regist_flg",
                "improve_plan_regist_flg",
                "effect_confirm_regist_flg",
                "warning_case_regist_flg",
                "analysis_target_flg",

                "inci_summary_condition",
                "title_keyword_condition",
                "incident_contents_condition",
                "emp_class_condition",
                "emp_attribute_condition",
                "emp_dept_condition",
                "emp_room_condition",
                "emp_name_condition",
                "emp_job_condition",
                "patient_id_condition",
                "patient_year_from_condition",
                "patient_year_to_condition",
                "patient_name_condition",
                "evaluation_date_condition",

                "use_level_list",
                "use_hiyari_summary_list",
                "use_incident_summary_list",
                "use_2010_summary_list"
                );
        $setvalue = array(
                $folder_name_sql,
                $and_or_div,

                $title_and_or_div,
                $incident_and_or_div,

                $inci_level_regist_flg,
                $happened_cause_regist_flg,
                $inci_background_regist_flg,
                $improve_plan_regist_flg,
                $effect_confirm_regist_flg,
                $warning_case_regist_flg,
                $analysis_target_flg,

                "",//$inci_summary, は廃止
                $title_keyword,
                $incident_contents_keyword,
                $emp_class_condition,
                $emp_attribute_condition,
                $emp_dept_condition,
                $emp_room_condition,
                $emp_name_condition,
                $emp_job_condition,
                $patient_id_condition,
                $patient_year_from_condition,
                $patient_year_to_condition,
                $patient_name_condition,
                $evaluation_date,

                join ( ",",$use_level),//CSV
                join ( ",",$hiyari_summary),//CSV
                join ( ",",$use_120),//CSV
                join ( ",",$use_900)//CSV
                );
        $cond = "where folder_id = $folder_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //==============================
        //発生場面・内容の条件更新(デリートインサート)
        //==============================

        //デリート
        $sql  = "delete from inci_classification_folder_400_condition where folder_id = $folder_id ";
        $result = delete_from_table($con, $sql, "",  $fname);
        if ($result == 0){
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }

        //インサート
        for($item_list_400_index = 0; $item_list_400_index < 10; $item_list_400_index++)
        {
            for($target_item_400 = 0; $target_item_400 < 2; $target_item_400++)
            {
                if($condition_400[$item_list_400_index][$target_item_400]['flg'])
                {
                    $use_list_400 = $condition_400[$item_list_400_index][$target_item_400]['list'];

                    $sql  = "insert into inci_classification_folder_400_condition(";
                    $sql .= " folder_id,";
                    $sql .= " index_400,";
                    $sql .= " target_item,";
                    $sql .= " use_list";
                    $sql .= ") values(";
                    $registry_data = array(
                        $folder_id,
                        $item_list_400_index,
                        $target_item_400,
                        join ( ",",$use_list_400)//CSV
                    );
                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }

        //==============================
        //概要2010の条件追加
        //==============================

        //デリート
        $sql  = "delete from inci_classification_folder_1000_condition where folder_id = $folder_id ";
        $result = delete_from_table($con, $sql, "",  $fname);
        if ($result == 0){
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }
        //インサート
        $item_list_900 = get_superitem_list($con, $fname);
        foreach($item_list_900 as $code_900 => $name_900){
            $item_list_1000_index = $code_900 - 1;
            
            // 0:種類 1:場面 2:内容
            for($target_item_1000 = 0; $target_item_1000 < 3; $target_item_1000++)
            {
                if($condition_1000[$item_list_1000_index][$target_item_1000]['flg'])
                {
                    $use_list_1000 = $condition_1000[$item_list_1000_index][$target_item_1000]['list'];

                    $sql  = "insert into inci_classification_folder_1000_condition(";
                    $sql .= " folder_id,";
                    $sql .= " index_1000,";
                    $sql .= " code,";
                    $sql .= " use_list";
                    $sql .= ") values(";
                    $registry_data = array(
                        $folder_id,
                        $item_list_1000_index,
                        $target_item_1000,
                        join ( ",",$use_list_1000)//CSV
                    );
                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }

        //デリート
        $sql  = "delete from inci_classification_folder_1000_detail_condition where folder_id = $folder_id ";
        $result = delete_from_table($con, $sql, "",  $fname);
        if ($result == 0){
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }
        //インサート
        $item_list = get_item_list_all($con, $fname);
        foreach($item_list as $item_id => $item_name){
            // 0:種類 1:場面 2:内容
            for($target_item_1000 = 0; $target_item_1000 < 3; $target_item_1000++)
            {
                if($detail_condition_1000[$item_id][$target_item_1000]['flg'])
                {
                    $use_detail_list_1000 = $detail_condition_1000[$item_id][$target_item_1000]['list'];

                    $sql  = "insert into inci_classification_folder_1000_detail_condition(";
                    $sql .= " folder_id,";
                    $sql .= " use_index,";
                    $sql .= " code,";
                    $sql .= " use_detail_list";
                    $sql .= ") values(";
                    $registry_data = array(
                        $folder_id,
                        $item_id,
                        $target_item_1000,
                        join ( ",",$use_detail_list_1000)//CSV
                    );

                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }

        //==============================
        //トランザクション終了
        //==============================
        pg_query($con, "commit");

        //==============================
        //自分画面を閉じる。
        //==============================
        echo("<script language='javascript'>");
        echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
        echo("window.close();");
        echo("</script>");
        exit;

    }

}

//==============================
//フォルダ情報取得
//==============================
if($mode == "update")
{
    //==============================
    //フォルダ情報
    //==============================
    $sql = "select * from inci_classification_folder where folder_id = $folder_id";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if(pg_num_rows($sel) != 1){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $folder_name                = pg_fetch_result($sel,0,"folder_name");

    $inci_level_regist_flg      = pg_fetch_result($sel,0,"inci_level_regist_flg");
    $happened_cause_regist_flg  = pg_fetch_result($sel,0,"happened_cause_regist_flg");
    $inci_background_regist_flg = pg_fetch_result($sel,0,"inci_background_regist_flg");
    $improve_plan_regist_flg    = pg_fetch_result($sel,0,"improve_plan_regist_flg");
    $effect_confirm_regist_flg  = pg_fetch_result($sel,0,"effect_confirm_regist_flg");
    $analysis_target_flg        = pg_fetch_result($sel,0,"analysis_target_flg");
    $warning_case_regist_flg    = pg_fetch_result($sel,0,"warning_case_regist_flg");

//  $inci_summary               = pg_fetch_result($sel,0,"inci_summary_condition");
    $title_keyword              = pg_fetch_result($sel,0,"title_keyword_condition");
    $incident_contents_keyword  = pg_fetch_result($sel,0,"incident_contents_condition");
    $search_emp_class           = pg_fetch_result($sel,0,"emp_class_condition");
    $search_emp_attribute       = pg_fetch_result($sel,0,"emp_attribute_condition");
    $search_emp_dept            = pg_fetch_result($sel,0,"emp_dept_condition");
    $search_emp_room            = pg_fetch_result($sel,0,"emp_room_condition");
    $search_emp_name            = pg_fetch_result($sel,0,"emp_name_condition");
    $search_emp_job             = pg_fetch_result($sel,0,"emp_job_condition");
    $patient_id          = pg_fetch_result($sel,0,"patient_id_condition");
    $patient_year_from   = pg_fetch_result($sel,0,"patient_year_from_condition");
    $patient_year_to     = pg_fetch_result($sel,0,"patient_year_to_condition");
    $patient_name        = pg_fetch_result($sel,0,"patient_name_condition");
    $evaluation_date            = pg_fetch_result($sel,0,"evaluation_date_condition");


    $use_level_csv            = pg_fetch_result($sel,0,"use_level_list");
    $hiyari_summary_csv            = pg_fetch_result($sel,0,"use_hiyari_summary_list");
    $use_120_csv            = pg_fetch_result($sel,0,"use_incident_summary_list");
    $use_900_csv            = pg_fetch_result($sel,0,"use_2010_summary_list");
    $use_level = split(",", $use_level_csv );
    $hiyari_summary = split(",", $hiyari_summary_csv );
    $use_120 = split(",", $use_120_csv );
    $use_900 = split(",", $use_900_csv );

    $and_or_div     = pg_fetch_result($sel,0,"and_or_div");
    $common_flg     = pg_fetch_result($sel,0,"common_flg");

    $title_and_or_div = pg_fetch_result($sel,0,"title_and_or_div");
    $incident_and_or_div = pg_fetch_result($sel,0,"incident_and_or_div");

    //==============================
    //発生場面・内容情報
    //==============================

    //検索
    $sql = "select * from inci_classification_folder_400_condition where folder_id = $folder_id";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //変数クリア
    $use_400_bamen = null;
    $use_400_naiyo = null;
    for($item_list_400_index = 0; $item_list_400_index < 10; $item_list_400_index++)
    {
        $tmp = "bamen_{$item_list_400_index}";
        $$tmp = null;
        $tmp = "naiyo_{$item_list_400_index}";
        $$tmp = null;
    }

    //変数格納
    $db_data = pg_fetch_all($sel);
    foreach($db_data as $db_record)
    {
        $item_list_400_index = $db_record['index_400'];
        if($db_record['target_item'] == 0)
        {
            $use_400_bamen[] = $item_list_400_index;
            $tmp = "bamen_{$item_list_400_index}";
            $$tmp = split(",", $db_record['use_list'] );
        }
        else
        {
            $use_400_naiyo[] = $item_list_400_index;
            $tmp = "naiyo_{$item_list_400_index}";
            $$tmp = split(",", $db_record['use_list'] );
        }
    }

    //==============================
    //概要2010情報
    //==============================

    //検索
    $sql = "select * from inci_classification_folder_1000_condition where folder_id = $folder_id";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //変数クリア
    $use_1000_kind = null;
    $use_1000_scene = null;
    $use_1000_content = null;
    $item_list_900 = get_superitem_list($con, $fname);
    foreach($item_list_900 as $code_900 => $name_900){
        $item_list_1000_index = $code_900 - 1;

        $tmp = "kind_{$item_list_1000_index}";
        $$tmp = null;
        $tmp = "scene_{$item_list_1000_index}";
        $$tmp = null;
        $tmp = "content_{$item_list_1000_index}";
        $$tmp = null;
    }

    //変数格納
    $db_data = pg_fetch_all($sel);
    foreach($db_data as $db_record)
    {
        $item_list_1000_index = $db_record['index_1000'];
        if($db_record['code'] == 0)
        {
            $use_1000_kind[] = $item_list_1000_index;
            $tmp = "kind_{$item_list_1000_index}";
            $$tmp = split(",", $db_record['use_list'] );
        }
        else if($db_record['code'] == 1)
        {
            $use_1000_scene[] = $item_list_1000_index;
            $tmp = "scene_{$item_list_1000_index}";
            $$tmp = split(",", $db_record['use_list'] );
        }
        else
        {
            $use_1000_content[] = $item_list_1000_index;
            $tmp = "content_{$item_list_1000_index}";
            $$tmp = split(",", $db_record['use_list'] );
        }
    }

    //検索
    $sql = "select * from inci_classification_folder_1000_detail_condition where folder_id = $folder_id";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //変数クリア
    $use_1000_detail_kind = null;
    $use_1000_detail_scene = null;
    $use_1000_detail_content = null;
    $item_list = get_item_list_all($con, $fname);
    foreach($item_list as $item_id => $item_name){
        $tmp = "kind_detail_{$item_id}";
        $$tmp = null;
        $tmp = "scene_detail_{$item_id}";
        $$tmp = null;
        $tmp = "content_detail_{$item_id}";
        $$tmp = null;
    }

    //変数格納
    $db_data = pg_fetch_all($sel);
    foreach($db_data as $db_record)
    {
        $item_list_1000_index = $db_record['use_index'];
        if($db_record['code'] == 0)
        {
            $use_1000_detail_kind[] = $item_list_1000_index;
            $tmp = "kind_detail_{$item_list_1000_index}";
            $$tmp = split(",", $db_record['use_detail_list'] );
        }
        else if($db_record['code'] == 1)
        {
            $use_1000_detail_scene[] = $item_list_1000_index;
            $tmp = "scene_detail_{$item_list_1000_index}";
            $$tmp = split(",", $db_record['use_detail_list'] );
        }
        else
        {
            $use_1000_detail_content[] = $item_list_1000_index;
            $tmp = "content_detail_{$item_list_1000_index}";
            $$tmp = split(",", $db_record['use_detail_list'] );
        }
    }

    //==============================
    //画面情報
    //==============================
    $PAGE_TITLE ="分類ラベル更新";
    $SUBMIT_BUTTON_TITLE = "変更";
}
else
{
    //==============================
    //画面情報
    //==============================
    $folder_name = "";
    $PAGE_TITLE ="分類ラベル登録";
    $SUBMIT_BUTTON_TITLE = "作成";
}


//==============================
//患者影響レベル情報取得
//==============================
$rep_obj = new hiyari_report_class($con, $fname);
$level_infos = $rep_obj->get_inci_level_infos($inci_lvl);

//==============================
//ヒヤリ・ハット分類マスタ取得
//==============================
$arr_hiyari_summary = get_report_item_list($con, $fname,125,85);

//==============================
//インシデントの概要マスタ取得
//==============================
$item_list_400 = null;
$item_list_400[0][0] = get_report_item_list($con, $fname,400,10);
$item_list_400[0][1] = get_report_item_list($con, $fname,400,20);
$item_list_400[1][0] = get_report_item_list($con, $fname,410,10);
$item_list_400[1][1] = get_report_item_list($con, $fname,410,30);
$item_list_400[2][0] = get_report_item_list($con, $fname,420,10);
$item_list_400[2][1] = get_report_item_list($con, $fname,420,20);
$item_list_400[3][0] = get_report_item_list($con, $fname,430,10);
$item_list_400[3][1] = get_report_item_list($con, $fname,430,20);
$item_list_400[4][0] = get_report_item_list($con, $fname,440,10);
$item_list_400[4][1] = get_report_item_list($con, $fname,440,20);
$item_list_400[5][0] = get_report_item_list($con, $fname,450,10);
$item_list_400[5][1] = get_report_item_list($con, $fname,450,20);
$item_list_400[6][0] = get_report_item_list($con, $fname,460,10);
$item_list_400[6][1] = get_report_item_list($con, $fname,460,20);
$item_list_400[7][0] = get_report_item_list($con, $fname,470,10);
$item_list_400[7][1] = get_report_item_list($con, $fname,470,20);
$item_list_400[8][0] = get_report_item_list($con, $fname,480,10);
$item_list_400[8][1] = get_report_item_list($con, $fname,480,20);
$item_list_400[9][0] = get_report_item_list($con, $fname,490,10);
$item_list_400[9][1] = get_report_item_list($con, $fname,490,20);

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", $PAGE_TITLE);
$smarty->assign("session", $session);
$smarty->assign("mode", $mode);

$smarty->assign(predetermined_eval,$predetermined_eval);
//------------------------------
//分類フォルダ
//------------------------------
$smarty->assign("folder_id", $folder_id);
$smarty->assign("folder_name", $folder_name);
switch($common_flg){
    case 't':
        $smarty->assign("folder_type", "共有フォルダ");
        break;
    case 'f':
        $smarty->assign("folder_type", "個人フォルダ");
        break;
    default:
        $smarty->assign("folder_type", '');
}

//------------------------------
//報告者
//------------------------------
//部署
$arr_class_name = get_class_name_array($con, $fname);
$smarty->assign("arr_class_name", $arr_class_name);
$smarty->assign("post_select_data", get_post_select_data($con, $fname));
$smarty->assign("class_id", $search_emp_class);
$smarty->assign("attribute_id", $search_emp_attribute);
$smarty->assign("dept_id", $search_emp_dept);
$smarty->assign("room_id", $search_emp_room);

//氏名
$smarty->assign("search_emp_name", $search_emp_name);

//職種
$job_list = array();
$sel_job = get_job_mst($con, $fname);
while ($row = pg_fetch_array($sel_job)){
    $job_list[ $row["job_id"] ] = $row["job_nm"];
}
$smarty->assign("job_list", $job_list);
$smarty->assign("search_emp_job", $search_emp_job);

//------------------------------
//患者
//------------------------------
$smarty->assign("patient_id", $patient_id);
$smarty->assign("patient_name", $patient_name);

//患者年齢(年数)
$patient_year = get_patient_year($con, $fname);
$smarty->assign("patient_year", $patient_year);
$smarty->assign("patient_year_from", $patient_year_from);
$smarty->assign("patient_year_to", $patient_year_to);

//------------------------------
//キーワード
//------------------------------
$smarty->assign("title_keyword", $title_keyword);
$smarty->assign("title_and_or_div", $title_and_or_div);
$smarty->assign("incident_contents_keyword", $incident_contents_keyword);
$smarty->assign("incident_and_or_div", $incident_and_or_div);

//------------------------------
//未入力チェック
//------------------------------
$smarty->assign("inci_level_regist_flg", $inci_level_regist_flg);
$smarty->assign("happened_cause_regist_flg", $happened_cause_regist_flg);
$smarty->assign("inci_background_regist_flg", $inci_background_regist_flg);
$smarty->assign("improve_plan_regist_flg", $improve_plan_regist_flg);
$smarty->assign("effect_confirm_regist_flg", $effect_confirm_regist_flg);

//------------------------------
//評価日
//------------------------------
$smarty->assign("evaluation_date", $evaluation_date);

//------------------------------
//事例分析検討対象
//------------------------------
$smarty->assign("analysis_target_flg", $analysis_target_flg);

//------------------------------
//警鐘事例
//------------------------------
$smarty->assign("warning_case_regist_flg", $warning_case_regist_flg);

//------------------------------
//患者影響レベル
//------------------------------
$level = array();
foreach ($level_infos as $level_info){
    if(!$level_info["use_flg"]){
        continue;
    }

    $code = $level_info["easy_code"];    
    $info[$code]['name'] = $level_info["easy_name"];
    $info[$code]['is_checked'] = in_array($code, $use_level);

    if(count($info) == 4){
        $level[] = $info;
        $info = array();
    }
}
if(count($info) > 0){
    $level[] = $info;
}
$smarty->assign("level", $level);

//------------------------------
//ヒヤリ・ハット分類
//------------------------------
$hiyari_list = array();
foreach($arr_hiyari_summary as $code => $name){
    $hiyari_list[$code]['is_checked'] = in_array($code, $hiyari_summary);
    $hiyari_list[$code]['name'] = $name;
}
$smarty->assign("hiyari_list", $hiyari_list);

//------------------------------
//インシデントの概要
//------------------------------
if ($design_mode == 1){
    $gaiyo_list = array();
    $item_list_120 = get_report_item_list($con, $fname,120,80);
    foreach($item_list_120 as $code_120 => $name_120){
        $item_list_400_index = $code_120 - 1;

        $info['item_list_400_index'] = $item_list_400_index;
        $info['name'] = $name_120;
        $info['is_checked'] = in_array($code_120, $use_120);
        $info['is_bamen_checked'] = in_array($item_list_400_index, $use_400_bamen);
        $info['is_naiyo_checked'] = in_array($item_list_400_index, $use_400_naiyo);

        $bamen = array();
        $bamen_list = $item_list_400[$item_list_400_index][0];
        foreach($bamen_list as $code => $name){
            $obj_name = "bamen_{$item_list_400_index}";
            $bamen[$code]['obj_name'] = $obj_name;
            $bamen[$code]['is_checked'] = in_array($code,$$obj_name);
            $bamen[$code]['name'] = $name;
        }
        $info['detail']['bamen']['title'] = '場<br>面';
        $info['detail']['bamen']['list'] = $bamen;

        $naiyo = array();
        $naiyo_list = $item_list_400[$item_list_400_index][1];
        foreach($naiyo_list as $code => $name){
            $obj_name = "naiyo_{$item_list_400_index}";
            $naiyo[$code]['obj_name'] = $obj_name;
            $naiyo[$code]['is_checked'] = in_array($code,$$obj_name);
            $naiyo[$code]['name'] = $name;
        }
        $info['detail']['naiyo']['title'] = '内<br>容';
        $info['detail']['naiyo']['list'] = $naiyo;

        $gaiyo_list[$code_120] = $info;
    }
    $smarty->assign("gaiyo_list", $gaiyo_list);
}

//------------------------------
//概要・場面・内容（2010年改訂）
//------------------------------
$gaiyo2010_list = array();
$item_list_900 = get_superitem_list($con, $fname);
foreach($item_list_900 as $code_900 => $name_900){
    $item_list_1000_index = $code_900 - 1;

    $info = array();

    $info['item_list_1000_index'] = $item_list_1000_index;
    $info['name'] = $name_900;
    $info['is_checked'] = in_array($code_900,$use_900);
    $info['is_kind_checked'] = in_array($item_list_1000_index,$use_1000_kind);
    $info['is_scene_checked'] = in_array($item_list_1000_index,$use_1000_scene);
    $info['is_content_checked'] = in_array($item_list_1000_index,$use_1000_content);

    //種類
    $kind = array();
    $kind_list = get_item_list($con, $fname, $code_900, 'kind');
    foreach($kind_list as $code => $name){
        $obj_name = "kind_{$item_list_1000_index}";
        $kind[$code]['obj_name'] = $obj_name;
        $kind[$code]['is_checked'] = in_array($code,$$obj_name);
        $kind[$code]['name'] = $name;
        $kind[$code]['is_detail_checked'] = in_array($code,$use_1000_detail_kind);

        //項目
        $detail = array();
        $detail_list = get_subitem_list($con, $fname, $code, 'kind');
        foreach($detail_list as $detail_code => $detail_name) {
            $obj_name = "kind_detail_{$code}";
            $detail[$detail_code]['obj_name'] = $obj_name;
            $detail[$detail_code]['is_checked'] = in_array($detail_code,$$obj_name);
            $detail[$detail_code]['name'] = $detail_name;
        }
        $kind[$code]['detail_list'] = $detail;
        $kind[$code]['row_count'] = ceil(count($detail) / 2);
    }
    $info['sub']['kind']['title'] = '種<br>類';
    $info['sub']['kind']['list'] = $kind;
    $info['sub']['kind']['row_count'] = ceil(count($kind) / 2);

    //場面
    $scene = array();
    $scene_list = get_item_list($con, $fname, $code_900, 'scene');
    foreach($scene_list as $code => $name){
        $obj_name = "scene_{$item_list_1000_index}";
        $scene[$code]['obj_name'] = $obj_name;
        $scene[$code]['is_checked'] = in_array($code,$$obj_name);
        $scene[$code]['name'] = $name;
        $scene[$code]['is_detail_checked'] = in_array($code,$use_1000_detail_scene);

        //項目
        $detail = array();
        $detail_list = get_subitem_list($con, $fname, $code, 'scene');
        foreach($detail_list as $detail_code => $detail_name) {
            $obj_name = "scene_detail_{$code}";
            $detail[$detail_code]['obj_name'] = $obj_name;
            $detail[$detail_code]['is_checked'] = in_array($detail_code,$$obj_name);
            $detail[$detail_code]['name'] = $detail_name;
        }
        $scene[$code]['detail_list'] = $detail;
        $scene[$code]['row_count'] = ceil(count($detail) / 2);
    }
    $info['sub']['scene']['title'] = '場<br>面';
    $info['sub']['scene']['list'] = $scene;
    $info['sub']['scene']['row_count'] = ceil(count($scene) / 2);

    //内容
    $content = array();
    $content_list = get_item_list($con, $fname, $code_900, 'content');
    foreach($content_list as $code => $name){
        $obj_name = "content_{$item_list_1000_index}";
        $content[$code]['obj_name'] = $obj_name;
        $content[$code]['is_checked'] = in_array($code,$$obj_name);
        $content[$code]['name'] = $name;
        $content[$code]['is_detail_checked'] = in_array($code,$use_1000_detail_content);

        //項目
        $detail = array();
        $detail_list = get_subitem_list($con, $fname, $code, 'content');
        foreach($detail_list as $detail_code => $detail_name) {
            $obj_name = "content_detail_{$code}";
            $detail[$detail_code]['obj_name'] = $obj_name;
            $detail[$detail_code]['is_checked'] = in_array($detail_code,$$obj_name);
            $detail[$detail_code]['name'] = $detail_name;
        }
        $content[$code]['detail_list'] = $detail;
        $content[$code]['row_count'] = ceil(count($detail) / 2);
    }
    $info['sub']['content']['title'] = '内<br>容';
    $info['sub']['content']['list'] = $content;
    $info['sub']['content']['row_count'] = ceil(count($content) / 2);

    $gaiyo2010_list[$code_900] = $info;
}
$smarty->assign("gaiyo2010_list", $gaiyo2010_list);

//------------------------------
//AND_OR条件
//------------------------------
$smarty->assign("and_or_div", $and_or_div);

$smarty->assign("SUBMIT_BUTTON_TITLE", $SUBMIT_BUTTON_TITLE);

if ($design_mode == 1){
    $smarty->display("hiyari_rp_classification_folder_input1.tpl");
}
else{
    $smarty->display("hiyari_rp_classification_folder_input2.tpl");
}

//==============================================================================
// 内部関数
//==============================================================================
/**
 * 「インシデントの概要」リストオプション出力
 * @param $arr_inci  インシデントの概要情報格納配列
 * @param $easy_code 項目コード
 */
function show_inci_summary_options($arr_inci, $easy_code) {
    echo("<option value=\"-\">----------");
    foreach ($arr_inci as $key => $val) {
        echo("<option value=\"$key\"");
        if ($easy_code == $key) {
            echo(" selected");
        }
        echo(">$val\n");
    }
}


//private 職種オプション出力。
function show_job_options2($con, $fname, $sel_job_id)
{
    $arr_job_mst = array();
    $sql = "select * from jobmst";
    $cond = "where job_del_flg = 'f' order by order_no";
    $sel = select_from_table($con,$sql,$cond,$fname);       //職種一覧を取得
    if($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    echo("<option value=\"\">");
    while ($row = pg_fetch_array($sel))
    {
        $id = $row["job_id"];
        $name = $row["job_nm"];
        echo("<option value=\"$id\"");
        if ($id == $sel_job_id)
        {
            echo(" selected");
        }
        echo(">$name\n");
    }
}


/**
 * 報告書項目の一覧を取得
 * @param $con DBコネクション
 * @param $fname 画面名
 * @param $grp_code グループコード
 * @param $easy_item_code 項目コード
 */
function get_report_item_list($con, $fname,$grp_code,$easy_item_code)
{
    $arr = array();
    $sql = "select easy_code, easy_name from inci_report_materials where grp_code = $grp_code and easy_item_code = $easy_item_code order by easy_num";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel)) {

        $code = $row["easy_code"];
        $name = $row["easy_name"];
        $arr[$code] = $name;
    }

    //非表示項目を除外
    if($grp_code == 125 && $easy_item_code == 85)
    {
        $rep_obj = new hiyari_report_class($con, $fname);
        $item_element_no_disp = $rep_obj->get_item_element_no_disp();

        $arr2 = null;
        foreach($arr as $code => $name)
        {
            if(!in_array($code , $item_element_no_disp[$grp_code][$easy_item_code]) )
            {
                $arr2[$code] = $name;
            }
        }
        $arr = $arr2;
    }
    else
    {
        //この関数に対するユースケースで非表示項目となり得るケースはない。
    }

    return $arr;
}

/**
 * 2010年版親項目を取得する
 */
function get_superitem_list($con, $fname) {
    $sql = "SELECT super_item_id, super_item_name FROM inci_super_item_2010 WHERE disp_flg = 't' AND available = 't' ORDER BY super_item_order";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel)) {

        $code = $row["super_item_id"];
        $name = $row["super_item_name"];
        $arr[$code] = $name;
    }

    return $arr;
}

/**
 * 2010年版項目を取得する
 */
function get_item_list($con, $fname, $id, $code) {
    $sql = "SELECT item_id, item_name FROM inci_item_2010 WHERE super_item_id = {$id} AND item_code = '{$code}' AND disp_flg = 't' AND available = 't' ORDER BY item_order";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel)) {

        $code = $row["item_id"];
        $name = $row["item_name"];
        $arr[$code] = $name;
    }

    return $arr;
}

/**
 * 2010年版項目を取得する
 */
function get_item_list_all($con, $fname) {
    $sql = "SELECT item_id, item_name FROM inci_item_2010 WHERE disp_flg = 't' AND available = 't'";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel)) {

        $code = $row["item_id"];
        $name = $row["item_name"];
        $arr[$code] = $name;
    }

    return $arr;
}

/**
 * 2010年版孫項目を取得する
 */
function get_subitem_list($con, $fname, $id, $code) {
    $sql = "SELECT sub_item_id, sub_item_name FROM inci_sub_item_2010 WHERE item_id = {$id} AND item_code = '{$code}' AND disp_flg = 't' AND available = 't' ORDER BY sub_item_order";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel)) {

        $code = $row["sub_item_id"];
        $name = $row["sub_item_name"];
        $arr[$code] = $name;
    }

    return $arr;
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);
