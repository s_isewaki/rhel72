<?
class pxdoc_utils
{
	/**
	 * 横線を描画する
	 * @param $left_index   描画開位置（横）
	 * @param $top_index    描画開位置（縦）
	 * @param $right_index  描画開位置（横）
	 * @param $stroke_width 線の太さ    省略時:1
	 * @param $indent       インデント  省略時:0
	 */
	function out_horizontal_line($left_index, $top_index, $right_index, $stroke_width, $indent){
		echo(pxdoc_utils::get_horizontal_line($left_index, $top_index, $right_index, $stroke_width, $indent));
	}

	/**
	 * 横線を描画する
	 * @param $left_index   描画開位置（横）
	 * @param $top_index    描画開位置（縦）
	 * @param $right_index  描画開位置（横）
	 * @param $stroke_width 線の太さ    省略時:1
	 * @param $indent       インデント  省略時:0
	 */
	function get_horizontal_line($left_index, $top_index, $right_index, $stroke_width = 1, $indent = 0){
		$indent_value = "";
		for ($i = 0; $indent > $i; $i++){
			$indent_value .= "\t";
		}
		return $indent_value."<line x1=\"".$left_index."\" y1=\"".$top_index."\" x2=\"".$right_index."\" y2=\"".$top_index."\" stroke=\"black\" stroke-width=\"".$stroke_width."\" />\n";
	}

	/**
	 * 縦線を描画する
	 * @param $left_index   描画開位置（横）
	 * @param $top_index    描画開位置（縦）
	 * @param $bottom_index 描画開位置（縦）
	 * @param $stroke_width 線の太さ    省略時:2
	 * @param $indent       インデント  省略時:0
	 */
	function out_vertical_line($left_index, $top_index, $bottom_index, $stroke_width, $indent){
		echo(pxdoc_utils::get_vertical_line($left_index, $top_index, $bottom_index, $stroke_width, $indent));
	}

	/**
	 * 縦線を描画する
	 * @param $left_index   描画開位置（横）
	 * @param $top_index    描画開位置（縦）
	 * @param $bottom_index 描画開位置（縦）
	 * @param $stroke_width 線の太さ    省略時:2
	 * @param $indent       インデント  省略時:0
	 */
	function get_vertical_line($left_index, $top_index, $bottom_index, $stroke_width = 2, $indent = 0){
		$indent_value = "";
		for ($i = 0; $indent > $i; $i++){
			$indent_value .= "\t";
		}
		return $indent_value."<line x1=\"".$left_index."\" y1=\"".$top_index."\" x2=\"".$left_index."\" y2=\"".$bottom_index."\" stroke=\"black\" stroke-width=\"".$stroke_width."\" />\n";
	}

	/**
	 * 文字を描画する
	 * @param $left_index   描画開位置（横）
	 * @param $top_index    描画開位置（縦）
	 * @param $text_value   文字列
	 * @param $centering    センタリング  有:true 無:false デフォルト:false (有時は$left_indexが中心になる)
	 * @param $right_adjust 右寄せ        有:true 無:false デフォルト:false (有時は$left_indexが最右になる)
	 *                      $centeringと$right_adjust両方がtrueの場合$centerinが優先
	 * @param $bold         太字          太字:true 指定なし:false
	 * @param $indent       インデント  省略時:0
	 * @param $font_color   色  省略時:黒
	 */
	function out_text($left_index, $top_index, $text_value, $centering, $right_adjust, $bold, $indent, $font_color){
		echo(pxdoc_utils::get_text($left_index, $top_index, $text_value, $centering, $right_adjust, $bold, $indent, $font_color));
	}

	/**
	 * 文字出力タグを返す
	 * @param $left_index   描画開位置（横）
	 * @param $top_index    描画開位置（縦）
	 * @param $text_value   文字列
	 * @param $centering    センタリング  有:true 無:false デフォルト:false (有時は$left_indexが中心になる)
	 * @param $right_adjust 右寄せ        有:true 無:false デフォルト:false (有時は$left_indexが最右になる)
	 *                      $centeringと$right_adjust両方がtrueの場合$centerinが優先
	 * @param $bold         太字          太字:true 指定なし:false
	 * @param $indent       インデント  省略時:0
	 * @param $font_color   色  省略時:黒
	 */
	function get_text($left_index, $top_index, $text_value, $centering = false, $right_adjust = false, $bold = false, $indent = 0, $font_color = ""){
		$indent_value = "";
		for ($i = 0; $indent > $i; $i++){
			$indent_value .= "\t";
		}
		$text_anchor = "";
		//文字表示位置
		if ($centering){
			//センタリング
			$text_anchor = " text-anchor=\"middle\"";
		}
		else if ($right_adjust){
			//右寄せ
			$text_anchor = " text-anchor=\"end\"";
		}

		//太字
		if ($bold){
			$font_weight = " font-weight=\"bold\"";
		}
		//色追加 20100922
		if ($font_color) {
			$fill = " fill=\"$font_color\"";
		}
		return $indent_value."<text x=\"".$left_index."\"  y=\"".$top_index."\"".$text_anchor.$font_weight.$fill.">".$text_value."</text>\n";
	}

	/**
	 * 文字を描画する(テキストエリア）
	 * @param $left_index   描画開位置（横）
	 * @param $top_index    描画開位置（縦）
	 * @param $width        テキストエリア幅
	 * @param $text_value   文字列
	 * @param $centering    センタリング  有:true 無:false デフォルト:false (有時は$left_indexが中心になる)
	 * @param $right_adjust 右寄せ        有:true 無:false デフォルト:false (有時は$left_indexが最右になる)
	 *                      $centeringと$right_adjust両方がtrueの場合$centerinが優先
	 * @param $indent       インデント  省略時:0
	 */
	function out_textarea($left_index, $top_index, $width, $text_value, $centering = false, $right_adjust = false, $bold = false, $indent = 0){
		$indent_value = "";
		for ($i = 0; $indent > $i; $i++){
			$indent_value .= "\t";
		}
		$text_anchor = "";
		//文字表示位置
		if ($centering){
			//センタリング
			$text_anchor = " text-anchor=\"middle\"";
		}
		else if ($right_adjust){
			//右寄せ
			$text_anchor = " text-anchor=\"end\"";
		}

		//太字
		if ($bold){
			$font_weight = " font-weight=\"bold\"";
		}

		echo($indent_value."<textArea x=\"".$left_index."\"  y=\"".$top_index."\" width=\"".$width."\"".$text_anchor.$font_weight.">".$text_value."</textArea>\n");
	}
}
?>