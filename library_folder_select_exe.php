<?php
// 日本語
require("about_session.php");
require("about_authority.php");
require("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session=="0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = 32;
if ($path=="3") $auth_id = 63;
if ($path=="W") $auth_id = 3;

$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");

// 書庫名を取得
$archive_nm = lib_get_archive_name($a);

// カテゴリ名を取得
$cate_nm = lib_get_category_name($con, $c, $fname);

// フォルダパスを取得
$folder_path = lib_get_folder_path($con, $f, $fname);

// 使用可能な書庫一覧を取得
$available_archives = lib_available_archives($con, $fname, false);

pg_close($con);
?>
<script type="text/javascript">
var archive_nm = '<? echo($archive_nm); ?>';
var cate_nm = '<? echo($cate_nm); ?>';
var folder_path = '<? str_replace("&gt;", ">", lib_write_folder_path($folder_path)); ?>';

<? if (count($available_archives) > 1) { ?>
if (folder_path == '') {
	var path = new Array(archive_nm, cate_nm).join(' > ');
} else {
	var path = new Array(archive_nm, cate_nm, folder_path).join(' > ');
}
<? } else { ?>
if (folder_path == '') {
	var path = new Array(cate_nm).join(' > ');
} else {
	var path = new Array(cate_nm, folder_path).join(' > ');
}
<? } ?>

<? if ($path == "W") { ?>
opener.document.getElementById('lib_folder_path').innerHTML = path;
opener.document.wkfw.lib_archive.value = '<? echo($a); ?>';
opener.document.wkfw.lib_cate_id.value = '<? echo($c); ?>';
opener.document.wkfw.lib_folder_id.value = '<? echo($f); ?>';
opener.onChangeLibraryArchive(false);
<? } elseif ($type == 'link') { ?>
opener.document.getElementById('folder_path').innerHTML = path;
opener.document.mainform.link_arch_id.value = '<? echo($a); ?>';
opener.document.mainform.link_cate_id.value = '<? echo($c); ?>';
opener.document.mainform.link_id.value = '<? echo($f); ?>';
opener.onChangeArchive(true);
<? } elseif ($type == 'parent') { ?>
opener.document.getElementById('parent_path').innerHTML = path;
opener.document.mainform.arch_id.value = '<? echo($a); ?>';
opener.document.mainform.link_arch_id.value = '<? echo($a); ?>';
opener.document.mainform.category.value = '<? echo($c); ?>';
opener.document.mainform.link_cate_id.value = '<? echo($c); ?>';
opener.document.mainform.link_folder_id.value = '<? echo($f); ?>';
opener.onChangeArchive(true);
<? } else { ?>
opener.document.getElementById('folder_path').innerHTML = path;
opener.document.mainform.archive.value = '<? echo($a); ?>';
opener.document.mainform.category.value = '<? echo($c); ?>';
opener.document.mainform.folder_id.value = '<? echo($f); ?>';
opener.onChangeArchive(true);
<? } ?>

self.close();
</script>
