<?
$forPDF = false ;
$klst ;
if ($_REQUEST["direct_template_pdf_print_requested"])
{
  $forPDF = true ;
  $klst = new Klst() ;
  ob_start() ;
}

ob_start();
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");
require_once("read_xml.ini");
ob_end_clean();

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}

//ログイン職員ＩＤ
$emp_id = get_emp_id($con, $session, $fname);


//=================================================
// 対象日付がない場合は本日を設定
//=================================================
if(@$apply_date_default == "on") list($date_y, $date_m, $date_d) = split("/", date("Y/m/d", strtotime("today")));
$target_yyyymmdd = $date_y. ((int)@$date_m <10 ? "0".(int)@$date_m : $date_m) . ((int)@$date_d <10 ? "0".(int)@$date_d : $date_d);

//=================================================
// 病棟などが指定されていなければデフォルトを設定する
//=================================================
if (@$apply_date_default && @$sel_bldg_cd=="" && @$sel_ward_cd==""){
	$sql =
		" select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
		" inner join empmst emp on (".
		"   emp.emp_class = swer.class_id".
		"   and emp.emp_attribute = swer.atrb_id".
		"   and emp.emp_dept = swer.dept_id".
		"   and emp.emp_id = '".pg_escape_string($emp_id)."'".
		" )";
	$emp_ward = @$c_sot_util->select_from_table($sql);
	$_REQUEST["sel_bldg_cd"] = @pg_fetch_result($emp_ward, 0, "bldg_cd");
	$_REQUEST["sel_ward_cd"] = @pg_fetch_result($emp_ward, 0, "ward_cd");
	$sel_bldg_cd= $_REQUEST["sel_bldg_cd"];
	$sel_ward_cd= $_REQUEST["sel_ward_cd"];
}
$sel_team_id = (int)@$_REQUEST["sel_team_id"];

$THIS_WORKSHEET_ID = "1"; // [sotmst][sot_id]の固定値

ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | 経管栄養一覧</title>
<? write_yui_calendar_use_file_read_0_12_2();// 外部ファイルを読み込む ?>
<? write_yui_calendar_script2(1);// カレンダー作成、関数出力 ?>
<script type="text/javascript">
function popupPrintPreview(){
//	window.open(
//		"sot_worksheet_keikan_eiyou.php?session=<?=$session?>&date_y=<?=@$date_y?>&date_m=<?=@$date_m?>&date_d=<?=@$date_d?>&sel_bldg_cd=<?=@$sel_bldg_cd?>&sel_ward_cd=<?=@$sel_ward_cd?>&sel_ptrm_room_no=<?=@$sel_ptrm_room_no?>&sel_team_id=<?=@$sel_team_id?>&print_preview=y",
//		"worksheet_keikan_eiyou_print",
//		"directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1024,height=700"
//	);

  document.frm_pdf_print.submit() ;

}
</script>
<style type="text/css" media="all">
	body { margin:0; color:#000; background-color:#fff; }
	.list {border-collapse:collapse; }
	.list td {border:#5279a5 solid 1px; padding:1px;}
	table.block_in {border-collapse:collapse;}
	table.block_in td {border:#5279a5 solid 0px;}
	img { border: 0; }
	.date_header { background-color:#ffa; }
	.c_red { color:#f05 }
	.apply_header { border: 1px solid #ccc; }
<? if (@$_REQUEST["print_preview"]) {?>
	.print_preview_hidden { display:none; }
<? } ?>
</style>
<? if (@$_REQUEST["print_preview"]) {?>
<style type="text/css" media="print">
	html { font-size:14px; }
	.print_color_black { color:#000; }
	.print_target_table th { font-size:14px }
	.print_target_table td { font-size:14px }
	.print_hidden { display:none; }
	.list td {border:#000 solid 1px;}
	.date_header { border: 1px solid #777; }
	.apply_header { border: 1px solid #777; }
</style>
<? } ?>
</head>
<body onload="<? if (@$_REQUEST["print_preview"]) {?>print();close();<?}else{ ?>initcal();<?}?>"<? if (@$_REQUEST["print_preview"]) {?>style="padding:1px"<? } ?>>

<div class="print_preview_hidden">

<? $param = "&date_y=".@$date_y."&date_m=".@$date_m."&date_d=".@$date_d."&sel_bldg_cd=".@$sel_bldg_cd."&sel_ward_cd=".@$sel_ward_cd."&sel_ptrm_room_no=".@$sel_ptrm_room_no."&sel_team_id=".$sel_team_id; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "経管栄養一覧", $param); ?>

<? summary_common_show_main_tab_menu($con, $fname, "看護支援", 1); ?>


<script type="text/javascript">
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
	combobox[1] = document.getElementById("sel_bldg_cd");
	combobox[2] = document.getElementById("sel_ward_cd");
	combobox[3] = document.getElementById("sel_ptrm_room_no");
	if (node < 3) combobox[3].options.length = 1;
	if (node < 2) combobox[2].options.length = 1;

	<? // 初期化用 自分コンボを再選択?>
	var cmb = combobox[node];
	if (node > 0 && cmb.value != selval) {
		for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
	}

	var idx = 1;
	if (node == 1){
		for(var i in ward_selections){
			if (ward_selections[i].bldg_cd != selval) continue;
			combobox[2].options.length = combobox[2].options.length + 1;
			combobox[2].options[idx].text  = ward_selections[i].name;
			combobox[2].options[idx].value = ward_selections[i].code;
			idx++;
		}
	}
	if (node == 2){
		for(var i in room_selections){
			if (room_selections[i].bldg_cd != combobox[1].value) continue;
			if (room_selections[i].ward_cd != selval) continue;
			combobox[3].options.length = combobox[3].options.length + 1;
			combobox[3].options[idx].text  = room_selections[i].name;
			combobox[3].options[idx].value = room_selections[i].code;
			idx++;
		}
	}
}

function searchByDay(day_incriment){
	var yy = document.search.date_y;
	var mm = document.search.date_m;
	var dd = document.search.date_d;
	var y = (parseInt(yy.value,10) ? yy.value : yy.options[1].value);
	var m = (parseInt(mm.value,10) ? mm.value : 1)*1;
	var d = (parseInt(dd.value,10) ? dd.value : 1)*1;
	var dt = new Date(y, m-1, d);
	dt.setTime(dt.getTime() + day_incriment * 86400000);

	var idx_y = yy.options[1].value - dt.getFullYear() + 1;
	if (idx_y < 1) return;
	yy.selectedIndex = idx_y;
	mm.selectedIndex = dt.getMonth()+1;
	dd.selectedIndex = dt.getDate();
	document.search.submit();
}

function searchByToday(){
	var dt = new Date();
	document.search.date_y.selectedIndex = document.search.date_y.options[1].value - dt.getFullYear() + 1;
	document.search.date_m.selectedIndex = dt.getMonth()+1;
	document.search.date_d.selectedIndex = dt.getDate();
	document.search.submit();
}

</script>
<?
$EMPTY_OPTION = '<option value="">　　　　</option>';

// チーム一覧
$team_options = array();
$sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel)){
	$team_options[] =
	$selected = ($row["team_id"] == $sel_team_id ? " selected" : "");
	$team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}

// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) {
	$selected = ($row["bldg_cd"] == @$_REQUEST["sel_bldg_cd"] ? " selected" : "");
	$bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$mst = $c_sot_util->getPatientsRoomInfo();
$ary = array();
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";
?>

<form name="search" action="sot_worksheet_keikan_eiyou.php?session=<?=$session?>" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="excelout" value="">
<table class="list_table" style="margin-top:4px; margin-bottom:8px" cellspacing="1">
	<tr>
		<th>事業所(棟)</th>
		<td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
		<th>病棟</th>
		<td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
		<th>病室</th>
		<td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
		<script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
		<script type="text/javascript">sel_changed(2, "<?=@$_REQUEST["sel_ward_cd"]?>");</script>
		<script type="text/javascript">sel_changed(3, "<?=@$_REQUEST["sel_ptrm_room_no"]?>");</script>
		<th bgcolor="#fefcdf">チーム</th>
		<td>
			<select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
		</td>
	</tr>
	<tr>
		<th>日付</th>
		<td colspan="7">
			<div style="float:left">
				<div style="float:left">
					<select id="date_y1" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>&nbsp;/
					<select id="date_m1" name="date_m"><? show_select_months($date_m, true); ?></select>&nbsp;/
					<select id="date_d1" name="date_d"><? show_select_days($date_d, true); ?></select>
				</div>
				<div style="float:left">
					<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
					<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
				</div>
				<div style="float:left; padding-left:4px">
					<input type="button" onclick="searchByDay('-7');" value="≪" title="１週間前"/><!--
				--><input type="button" onclick="searchByDay('-1');" value="<" title="１日前"/><!--
				--><input type="button" onclick="searchByDay('1');" value=">" title="１日後"/><!--
				--><input type="button" onclick="searchByDay('7');" value="≫" title="１週間後"/>
				</div>
				<div style="float:left;">
					<input type="button" onclick="searchByToday();" value="本日"/>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="float:right">
				<input type="button" onclick="document.search.excelout.value=''; document.search.submit();" value="検索"/>
				<input type="button" onclick="document.search.excelout.value=''; popupPrintPreview();" value="印刷"/>
				<input type="button" onclick="document.search.excelout.value='1'; document.search.submit();" value="Excel出力"/>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>


</form>

  <form id="frm_pdf_print" name="frm_pdf_print" action="sot_worksheet_keikan_eiyou.php" method="post" target="_blank">
  <input type="hidden" name="session" value="<?=$session?>" />
  <input type="hidden" name="date_y" value="<?=@$date_y?>" />
  <input type="hidden" name="date_m" value="<?=@$date_m?>" />
  <input type="hidden" name="date_d" value="<?=@$date_d?>" />
  <input type="hidden" name="sel_bldg_cd" value="<?=@$sel_bldg_cd?>" />
  <input type="hidden" name="sel_ward_cd" value="<?=@$sel_ward_cd?>" />
  <input type="hidden" name="sel_ptrm_room_no" value="<?=@$sel_ptrm_room_no?>" />
  <input type="hidden" name="sel_team_id" value="<?=@$sel_team_id?>" />
  <input type="hidden" name="print_preview" value="y" />
  <input type="hidden" name="direct_template_pdf_print_requested" value="1" />
  </form>

<? summary_common_show_worksheet_tab_menu("経管栄養一覧", $param); ?>


</div><!-- end of [class="print_preview_hidden"] -->


<?
//====================================
// スケジュール中の患者と病室を取得
// ・患者ごとに、どの期間を取得すべきかを求める
//   ・指定日に入院しているなら、入院開始予定日と終了予定日の間のスケジュールをとる
//   ・指定日に入院していないなら、直近の退院日からのスケジュールをとる。
//     ・直近の退院日もない場合は、システム稼動日からが求める期間となる。
//====================================
/*
$sel = select_from_table($con, "select emp_id, emp_lt_nm, emp_ft_nm from empmst", "", $fname);
$empmst = array();
while ($row = pg_fetch_array($sel)){
	$empmst[$row["emp_id"]] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
}
*/
$target_week = "";
if (checkdate((int)@$date_m, (int)@$date_d, (int)@$date_y)) {
	$target_ymd_time = mktime(0, 0, 0, (int)$date_m, (int)$date_d, (int)$date_y);
	$weeks_en = array("sun","mon","tue","wed","thu","fri","sat");
	$target_week = $weeks_en[date("w", $target_ymd_time)];
}

$where = "";

if ((int)@$_REQUEST["sel_bldg_cd"] || $sel_team_id==0) { // チームがなければ必須指定
	$where .= " and inpt.bldg_cd = " .      (int)@$_REQUEST["sel_bldg_cd"];
}
if ((int)@$_REQUEST["sel_ward_cd"] || $sel_team_id==0) { // チームがなければ必須指定
	$where .= " and inpt.ward_cd = " .      (int)@$_REQUEST["sel_ward_cd"];
}

if (@$_REQUEST["sel_ptrm_room_no"]!="") $where .= " and inpt.ptrm_room_no = " . (int)$_REQUEST["sel_ptrm_room_no"];

$team_inner_join = "";
if ($sel_team_id) {
	$team_inner_join =
		" inner join sot_ward_team_relation t on (".
		"   t.bldg_cd = inpt.bldg_cd and t.ward_cd = inpt.ward_cd and t.ptrm_room_no = inpt.ptrm_room_no".
		"   and team_id = " . $sel_team_id.
		" )";
}

// 対象日の入院中患者情報
$sql_nyuin_kanja_list =
  " select".
  " inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
	",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, bldg.bldg_name, wd.ward_name, ptrm.ptrm_name".
	" from (".
			" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
			",max(inpt.inpt_in_dt) as inpt_in_dt".
			",min(inpt.inpt_out_dt) as inpt_out_dt".
			" from (".
					" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id, inpt.inpt_in_dt, '' as inpt_out_dt".
					" from inptmst inpt".
					$team_inner_join.
					" where inpt.inpt_in_dt <= '" . $target_yyyymmdd . "'"." " . $where.
					" union".
					" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id, inpt.inpt_in_dt, inpt.inpt_out_dt".
					" from inpthist inpt".
					$team_inner_join.
					" where inpt.inpt_in_dt <= '" . $target_yyyymmdd . "'".
					" and inpt.inpt_out_dt >= '" . $target_yyyymmdd . "'".
					" " . $where.
			" ) inpt".
			" group by inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
	" ) inpt".
	" left join ptifmst pt on (pt.ptif_id = inpt.ptif_id)".
	" left join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
	" left join wdmst wd on (wd.bldg_cd = inpt.bldg_cd and wd.ward_cd = inpt.ward_cd)".
	" left join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
	" group by".
	" inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
	",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, bldg.bldg_name, wd.ward_name, ptrm.ptrm_name";

// 対象日が停止期間に含まれる食事停止箋から最新のスケジュールのものを抽出し、そのxml_file_nameを取得するSQL
// 患者IDごとに１レコードが取得される
$sql_mealstop =
  " select sss.schedule_seq, smxl.smry_xml" .
  " from sot_summary_schedule sss" .
  " inner join (" .
  "   select max(sss2.schedule_seq) as schedule_seq_max" .
  "   from sot_summary_schedule sss2" .
"     inner join summary smry on (smry.tmpl_id = sss2.tmpl_id and smry.ptif_id = sss2.ptif_id and smry.summary_id = sss2.summary_id and smry.summary_seq = sss2.summary_seq)" .
  "   where sss2.delete_flg = false" .
  "     and sss2.start_ymd <= " . $target_yyyymmdd .
  "     and sss2.end_ymd   >= " . $target_yyyymmdd .
  "     and sss2.temporary_update_flg = 0" .
  "     and sss2.tmpl_chapter_code = 'mstp'" .
  "     and sss2.tmpl_file_name = 'tmpl_MealStop.php'" .
  "     and sss2.ptif_id = '%PTIF_ID%'" .
  " ) sss3 on (sss3.schedule_seq_max = sss.schedule_seq)" .
  " inner join sum_xml smxl on (smxl.xml_file_name = sss.xml_file_name)";

// スケジュールと受付実施情報を照らしあわせた情報
// およびひとつひとつの指示を照らし合わせた情報
$sql_schedule =
  " select".
  " sss.apply_id".
  ",sss.start_ymd".
  ",sss.update_seq".
  ",sss.schedule_seq".
  ",sss.tmpl_chapter_code".
  ",sss.tmpl_id".
  ",sss.tmpl_file_name".
  ",sss.summary_id".
  ",sss.summary_seq".
  ",sss.emp_id".
  ",sss.ptif_id as d_ptif_id".
  ",sss.xml_file_name".
  ",sss.assign_pattern".
  ",sss.assign_sun_flg".
  ",sss.assign_mon_flg".
  ",sss.assign_tue_flg".
  ",sss.assign_wed_flg".
  ",sss.assign_thu_flg".
  ",sss.assign_fri_flg".
  ",sss.assign_sat_flg".
  ",sss.start_ymd".
  ",sss.end_ymd".
  ",sss.times_per_ymd".
  ",date_part('month',sss.regist_timestamp)||'/'||date_part('day',sss.regist_timestamp)||' '||".
  " date_part('hour',sss.regist_timestamp)||':'||to_char(sss.regist_timestamp, 'MI') as regist_timestamp".
  ",sssv.schedule_various_seq".
  ",sssv.string2".
  ",sssv.string4".
  ",sssv.string5".
  ",sssv.string6".
  ",sssv.string7".
  ",sssv.string12".
  ",sssv.string17".
  ",sssv.string20".
  ",d.ptif_id".
  ",d.bldg_cd".
  ",d.ward_cd".
  ",d.ptrm_room_no".
  ",d.inpt_bed_no".
  ",d.inpt_sect_id".
  ",d.ptif_lt_kaj_nm".
  ",d.ptif_ft_kaj_nm".
  ",d.bldg_name".
  ",d.ward_name".
  ",d.ptrm_name".
  ",swa.accepted_emp_id".
  ",swa.operated_emp_id".
",ti.link_cd2".
",'' as isDate".
  " from sot_summary_schedule sss".
  " inner join (".$sql_nyuin_kanja_list.") d on (d.ptif_id = sss.ptif_id)".
  " inner join sot_summary_schedule_various sssv on (".
  "   sssv.schedule_seq = sss.schedule_seq".
  "   and sssv.string1 = '経管栄養'".
  " )".
  " inner join summary smry on (".
  "   smry.ptif_id = sss.ptif_id".
  "   and smry.summary_seq = sss.summary_seq".
  " )".
  " left join sot_worksheet_apply swa on (".
  "   swa.schedule_various_seq = sssv.schedule_various_seq".
  "   and swa.worksheet_date = '" . $target_yyyymmdd . "'".
  " )".
  " left join tmplitem ti on (".
  "   ti.item_name = sssv.string2".
  "   and ti.mst_cd = 'A262'".
  " )".
	" where sss.delete_flg = false".
	" and sss.start_ymd <= " . $target_yyyymmdd.
	" and sss.end_ymd >= " . $target_yyyymmdd.
	" and sss.times_per_ymd > 0".
	" and sss.temporary_update_flg = 0".
	" and tmpl_file_name = 'tmpl_MealStart.php'".
	" order by d.bldg_cd, d.ward_cd, d.ptrm_room_no, d.inpt_bed_no, d.ptif_id, sss.start_ymd, sss.schedule_seq, sssv.schedule_various_seq";

$sel = $c_sot_util->select_from_table($sql_schedule);
$data = (array)pg_fetch_all($sel);

// ここにて抽出するのは
// 食事停止箋  MERGE型、検索で抽出されたら、そのまま表示する
// 食事開始箋  OVERRIDE型、過去最新と、本日分を再抽出しないといけない。

// 食事開始箋の、昨日以前の最新のスケジュールはどれか、を取得する。
// あとで本日分とマージする。
$latest_schedule_seq = array(); // オーバーライド判定用
foreach($data as $key => $row){
	if (!$row) continue;
	if ($row["tmpl_file_name"] != "tmpl_MealStart.php") continue; // 医師指示は関係ない。予測指示とDr.Callのみ。

	if ((int)$row["start_ymd"] < $target_yyyymmdd) { // 本日発効されたものは、関係ない。昨日までに発効されたやつが問題。
		$latest_schedule_seq[$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][$row["ptif_id"]] = $row["summary_seq"];
	}
}


$schedule = array();
foreach($data as $key => $row){
	if (!$row) continue;
	// 過去分で、食事開始処方箋のとき、最新のみを取得。
	if ((int)$row["start_ymd"] < $target_yyyymmdd) {
		if ($row["tmpl_file_name"] == "tmpl_MealStart.php") {
			$latest_seq = @$latest_schedule_seq[$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][$row["ptif_id"]];
		if ($latest_seq != $row["summary_seq"]) continue;
		}
	}
	// 値がなければ含めない。
	if (
		$row["string2"]=="" && $row["string4"]=="" && $row["string5"]=="" && $row["string6"]=="" &&
		$row["string7"]=="" && $row["string17"]=="" && $row["string12"]=="") {
		continue;
	}
	$ptif_key = $row["bldg_cd"]."@".$row["ward_cd"]."@".$row["ptrm_room_no"]."@".$row["ptif_id"];
	$schedule[$ptif_key][] = $row;
}
?>

<?
if (@$_REQUEST["excelout"]){
	ob_end_clean();
	xlsOut($schedule);
	die;
}
ob_end_flush();
?>

<? if (!@$_REQUEST["sel_ward_cd"] && !$sel_team_id) { ?>
	<div style="padding:10px; color:#aaa; text-align:center">病棟またはチームを指定してください。</div>
<? } ?>

<? if (count($schedule)){ ?>
	<table class="listp_table print_target_table" <? if (@$_REQUEST["print_preview"]) {?>style="width:600px"<? } ?> cellspacing="1">
	<tr>
		<? if (@$_REQUEST["print_preview"]) {?>
		<th style="white-space:nowrap; width:30px">病室</th>
		<th style="white-space:nowrap; width:90px">患者氏名</th>
		<th style="white-space:nowrap; width:140px">栄養剤</th>
		<th style="white-space:nowrap; width:30px">朝</th>
		<th style="white-space:nowrap; width:30px">昼</th>
		<th style="white-space:nowrap; width:30px">夕</th>
		<th style="white-space:nowrap; width:50px">夜間(時/kcal)</th>
		<th style="white-space:nowrap; width:120px">投与方法</th>
		<th style="white-space:nowrap; width:80px">コメント</th>
		<? } else { ?>
		<th style="white-space:nowrap; width:8%">病室</th>
		<th style="white-space:nowrap; width:14%">患者氏名</th>
		<th style="white-space:nowrap; width:23%">栄養剤</th>
		<th style="white-space:nowrap; width:9%">朝</th>
		<th style="white-space:nowrap; width:9%">昼</th>
		<th style="white-space:nowrap; width:9%">夕</th>
		<th style="white-space:nowrap; width:9%">夜間(時/kcal)</th>
		<th style="white-space:nowrap; width:9%">投与方法</th>
		<th style="white-space:nowrap; width:10%">コメント</th>
		<? } ?>
	</tr>
<?
    $xml = new template_xml_class();
    $mealstop_data = array();
    $schedule2 = array();
    $seq_rowspan_map = array();
    foreach($schedule as $key => $rows)
    {
      $rows2 = array();
      $last_summary_seq = 0;
      $rowspan = 0 ;
      foreach($rows as $idx => $row)
      {
        if ($last_summary_seq && $last_summary_seq != $row["summary_seq"])
        {
          $row["isdate"] = h($row["regist_timestamp"]) . " 登録" ;
        }
        $last_summary_seq = $row["summary_seq"];

        if (!array_key_exists($row["ptif_id"], $mealstop_data)) {
            $tmp_sql_mealstop = str_replace("%PTIF_ID%", $row["ptif_id"], $sql_mealstop);
            $sel_mealstop = $c_sot_util->select_from_table($tmp_sql_mealstop);
            if (pg_num_rows($sel_mealstop) > 0) {
                $mealstop_data[$row["ptif_id"]] = array(
                    "schedule_seq" => pg_fetch_result($sel_mealstop, 0, "schedule_seq"),
                    "smry_xml" => pg_fetch_result($sel_mealstop, 0, "smry_xml")
                );
            } else {
                $mealstop_data[$row["ptif_id"]] = array(
                    "schedule_seq" => "",
                    "smry_xml" => ""
                );
            }
        }
        $xmlStop = $mealstop_data[$row["ptif_id"]]["smry_xml"];
        if (strlen($xmlStop) > 0)
        {
          $scheduleSeqStart = (int)$row["schedule_seq"];
          $scheduleSeqStop  = (int)$mealstop_data[$row["ptif_id"]]["schedule_seq"];
          if ($scheduleSeqStop > $scheduleSeqStart)
          {
            $xmlStop = mb_convert_encoding($xmlStop, "UTF-8", "EUC-JP") ;
            $data = $xml->xml_text_to_object_ary($xmlStop) ;
            $riyu = $data->nodes["template"]->nodes["WorkSheet3"]->nodes["riyu"]->nodes["mstp_riyu"]->value ;
            if (($riyu == "退院") || ($riyu == "絶食") || ($riyu == "外泊"))
            {
              $row["string20"] = "@NoDisp@" ;
            }
          }
        }
        if ($row["string20"] != "@NoDisp@")
        {
          $rowspan++ ;
          if (strlen($row["isdate"]) > 0) $rowspan++ ;
          if (!isset($seq_rowspan_map[$row["summary_seq"]])) {
            $seq_rowspan_map[$row["summary_seq"]] = 1;
          } else {
            $seq_rowspan_map[$row["summary_seq"]]++;
          }
        }
        $rows2[$idx] = $row ;
      }
      $rows2["rowspan"] = $rowspan ;
      $schedule2[$key] = $rows2 ;
    }



  foreach($schedule2 as $key3 => $rows3)
  {

if ($forPDF)
{
  $ptif = new Ptif() ;
}

      $rowspan = $rows3["rowspan"] ;

      $lineNum = 0 ;
      $ptHeaderFlg = false ;
      foreach($rows3 as $idx3 => $row3){

       if (is_string($idx3)) continue ;

       if ($row3["string20"] == "@NoDisp@") continue ;
	?>

		<? if (strlen($row3["isdate"]) > 0)
       {
		     if ($lineNum > 0)
         {
    ?>
		<tr>
			<td colspan="7" style="padding:2px; background-color:#ffbfff; text-align:center"><?= h($row3["regist_timestamp"]) ?>&nbsp;登録</td>
		</tr>
    <?
         }
	  	   else
         {
           if ($forPDF) { $row3["isdate"] = "" ; }
           $rowspan-- ;
         }
       }
		?>

		<tr>
			<? if ($ptHeaderFlg==false){ ?>
				<td style="vertical-align:top" rowspan="<?=$rowspan?>"><?= h($row3["ptrm_name"])?></td>
				<td style="vertical-align:top" rowspan="<?=$rowspan?>"><?=h($row3["ptif_lt_kaj_nm"]." ".$row3["ptif_ft_kaj_nm"])?></td>
			<? } ?>
			<td style="vertical-align:top" rowspan="<?=$rowspan2?>"><?= h($row3["string2"])?></td>
			<td style="vertical-align:top" rowspan="<?=$rowspan2?>"><?= h($row3["string4"])?></td>
			<td style="vertical-align:top" rowspan="<?=$rowspan2?>"><?= h($row3["string5"])?></td>
			<td style="vertical-align:top" rowspan="<?=$rowspan2?>"><?= h($row3["string6"])?></td>
			<td style="vertical-align:top" rowspan="<?=$rowspan2?>"><?= h($row3["string7"])?></td>
			<td style="vertical-align:top" rowspan="<?=$rowspan2?>"><?= h($row3["string17"])?></td>
			<? if (isset($seq_rowspan_map[$row3["summary_seq"]])) { ?>
			<td style="vertical-align:top" rowspan="<?=$seq_rowspan_map[$row3["summary_seq"]]?>"><?= str_replace("\n","<br>",h($row3["string20"]))?></td>
			<?   unset($seq_rowspan_map[$row3["summary_seq"]]); ?>
			<? } ?>
			<? if ($ptHeaderFlg==false){ ?>
			<?   $ptHeaderFlg=true ; ?>
			<?   $lineNum++ ; ?>
			<? } ?>
		</tr>
	<?

if ($forPDF)
{
  if ($lineNum == 1)
  {
    $ptif->room = $row3["ptrm_name"] ;
    $ptif->name = $row3["ptif_lt_kaj_nm"] . " " . $row3["ptif_ft_kaj_nm"] ;
  }
  $ptif->eiyos[count($ptif->eiyos)] = $row3 ;
}

}

if ($forPDF)
{
  $klst->ptifs[count($klst->ptifs)] = $ptif ;
}

		}
	?>
	</table>
<? } ?>

</body>
<? pg_close($con); ?>
</html>








<? //-------------------------------------------------------------------------?>
<? // エクセル出力                                                            ?>
<? //-------------------------------------------------------------------------?>
<? function xlsOut($schedule) { ?>
<?   header("content-type: application/vnd.ms-excel"); ?>
<?   header("Pragma: public"); ?>
<?   header("Expires: 0"); ?>
<?   header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); ?>
<?   header("Content-Disposition: attachment; filename=fplus_aggregate_".Date("Ymd_Hi").".xls;"); ?>
<?   header('Content-Type: application/vnd.ms-excel'); ?>
<?   header("Content-Transfer-Encoding: binary"); ?>
<?   $fontfamily = mb_convert_encoding("ＭＳ Ｐゴシック", "SJIS"); ?>

<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=shift_jis">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel=File-List href="xls.files/filelist.xml">
<link rel=Edit-Time-Data href="xls.files/editdata.mso">
<link rel=OLE-Object-Data href="xls.files/oledata.mso">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Version>11.9999</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.98in .79in .98in .79in;
	mso-header-margin:.51in;
	mso-footer-margin:.51in;}
tr
	{mso-height-source:auto;
	mso-ruby-visibility:none;}
col
	{mso-width-source:auto;
	mso-ruby-visibility:none;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	border:none;
	mso-protection:locked visible;
	mso-style-name:標準;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	border:.5pt solid windowtext;
	background:silver;
	mso-pattern:auto none;}
.xl25
	{mso-style-parent:style0;
	border:.5pt solid windowtext;}
ruby
	{ruby-align:left;}
rt
	{color:windowtext;
	font-size:6.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	mso-char-type:katakana;
	display:none;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Sheet1</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:Print>
      <x:ValidPrinterInfo/>
      <x:PaperSizeIndex>9</x:PaperSizeIndex>
      <x:HorizontalResolution>300</x:HorizontalResolution>
      <x:VerticalResolution>300</x:VerticalResolution>
     </x:Print>
     <x:Selected/>
     <x:DoNotDisplayGridlines/>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet2</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet3</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>4725</x:WindowHeight>
  <x:WindowWidth>8475</x:WindowWidth>
  <x:WindowTopX>480</x:WindowTopX>
  <x:WindowTopY>30</x:WindowTopY>
  <x:AcceptLabelsInFormulas/>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>

<body link=blue vlink=purple>

<table x:str border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse;table-layout:fixed;'>
 <tr>
  <td class=xl24><ruby><?=mb_convert_encoding("病室","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("患者氏名","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("栄養剤","SJIS")?></ruby>ID</td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("朝","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("昼","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("夕","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("夜間(時/kcal)","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("投与方法","SJIS")?></ruby></td>
 </tr>
<?
foreach ($schedule as $key => $rows) {
	$row_count = count($rows);
	for ($i = 0; $i < $row_count; $i++) {
		$row = $rows[$i];
?>
<tr>
<?
		if ($i == 0) {
?>
	<td class=xl25 rowspan='<? echo($row_count); ?>' style='vertical-align:top;border-top:none'><?= mb_convert_encoding(h($row["ptrm_name"]),"SJIS")?></td>
	<td class=xl25 rowspan='<? echo($row_count); ?>' style='vertical-align:top;border-top:none;border-left:none'><?= mb_convert_encoding(h($row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"]),"SJIS")?></td>
<?
		}
?>
	<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["string2"]),"SJIS")?></td>
	<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["string4"]),"SJIS")?></td>
	<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["string5"]),"SJIS")?></td>
	<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["string6"]),"SJIS")?></td>
	<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["string7"]),"SJIS")?></td>
	<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["string17"]),"SJIS")?></td>
</tr>
<?
	}
}
?>
</table>

</body>

</html>
<? } ?>

<? //-------------------------------------------------------------------- ?>
<? // PDF印刷用クラス定義                                                 ?>
<? //-------------------------------------------------------------------- ?>
<?
class Klst
{
  var $TITLE = "経管栄養一覧" ;
  var $dtStr ;
//  var $bldgs = array() ;
  var $ptifs = array() ;
}
//class Bldg
//{
//  var $name ;
//  var $wards = array() ;
//}
//class Ward
//{
//  var $name ;
//  var $rooms = array() ;
//}
//class Room
//{
//  var $name ;
//  var $ptifs = array() ;
//}
class Ptif
{
  var $room ;
  var $name ;
  var $eiyos = array() ;
}
?>
<? //-------------------------------------------------------------------- ?>
<? // PDF印刷スクリプトを出力する                                         ?>
<? //-------------------------------------------------------------------- ?>
<?
if ($forPDF)
{
  ob_end_clean() ;

  require_once('fpdf153/mbfpdf.php') ;

  //=================================================
  // PDF生成ライブラリの拡張クラス定義
  // ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //=================================================
  class CustomMBFPDF extends MBFPDF
  {
    // A4横：297 x 210
    var $A4W = 297 ;
    var $A4H = 210 ;

    var $MOST_LEFT  =   7 ;
    var $MOST_RIGHT = 290 ;
    var $MOST_TOP   =   7 ;
    var $MOST_BTM   = 201 ;
    var $CELL_H     = 5.5 ;
    var $LINE_W_1   = 0.2 ;
    var $LINE_W_2   = 0.5 ;

    var $HD_CELLS = array(
      array(15, "病室"         ),
      array(33, "患者氏名"     ),
      array(70, "栄養剤"       ),
      array(27, "朝"           ),
      array(27, "昼"           ),
      array(27, "夕"           ),
      array(27, "夜間(時/kcal)"),
      array(27, "投与方法"     ),
      array(30, "コメント"     )
      ) ;

    var $klTitle ;

    var $dtStr1 ;
    var $dtStr2 ;

    var $eiyosX ;

    var $saveY ;
    var $currY ;

    var $currPtif ; // 現在処理している患者データ

    var $pagePtifNum ;

    var $pno = 0;

    var $PRINT_JS = "(print\\('true'\\);)" ; // 印刷ダイアログボックスを表示するためにPDF文書内に埋め込むJavaScript
    var $printJs ;
    var $cin ; // 現在までに生成されたのCustomMBFPDFインスタンス数

    // コンストラクタ(PHP5可)
    function CustomMBFPDF($klTitle, $dtStr)
    {
      $this->MBFPDF("L", "mm", "A4") ; // PHPでは継承元クラスのコンストラクタは明示的に呼び出す必要がある

      $this->klTitle = $klTitle ;
      $this->dtStr1  = date("y/m/d h:i") ;
      $this->dtStr2  = $dtStr ;

      $this->eiyosX  = $this->MOST_LEFT + $this->HD_CELLS[0][0] + $this->HD_CELLS[1][0] ;
    }

    function AddPage0()
    {
      $this->AddPage() ;
      $this->currY = $this->getY() ;
      $this->pagePtifNum = 0 ;
      $this->pno++ ;
    }

    function AddPage1()
    {
      $this->AddPage0() ;
      $this->writeTitleHeader() ;
      $this->writeClmnsHeader() ;
    }

    function AddPage2()
    {
      $this->fillWhite($this->MOST_LEFT - 2, $this->saveY - 0.1, $this->MOST_RIGHT - $this->MOST_LEFT + 4, $this->A4H - $this->saveY) ;
      $this->SetLineWidth($this->LINE_W_2) ;
      $this->Line($this->MOST_LEFT + 0.15, $this->saveY - 0.15, $this->MOST_RIGHT - 0.15, $this->saveY - 0.15) ;
      $this->writePageNo() ;
      $this->AddPage1() ;
      $this->writePtifDataConts() ;
    }

    // 指定範囲を白で塗りつぶす
    function fillWhite($x, $y, $w, $h)
    {
      $this->SetFillColor(255, 255, 255) ;
      $this->Rect($x, $y, $w, $h, "F") ;
    }

    // タイトルヘッダーを出力する
    function writeTitleHeader()
    {
      $this->setY($this->currY) ;
      $this->SetFont(GOTHIC, "",  8.0) ;
      $this->Cell(0, 3.0, $this->dtStr1,  "", 1, "R") ;
      $this->SetFont(GOTHIC, "", 16.0) ;
      $this->Cell(0, 5.0, $this->klTitle, "", 1, "C") ;
      $this->SetFont(GOTHIC, "",  9.5) ;
      $this->Cell(0, 6.0, $this->dtStr2,  "", 1, "L") ;
      $this->currY = $this->GetY() - 0.5 ;
    }

    // 項目ヘッダーを出力する
    function writeClmnsHeader()
    {
      $this->setY($this->currY) ;
      $this->SetFont(GOTHIC, "", 9.5) ;
      $this->SetLineWidth($this->LINE_W_1) ;
      $this->SetFillColor(210);
      foreach($this->HD_CELLS as $key => $cell)
      {
        $lineEnd = ($key < count($this->HD_CELLS) - 1) ? 0 : 1 ;
        $this->Cell($cell[0], $this->CELL_H, $cell[1], "1", $lineEnd, "L", 1) ;
      }
      $this->currY = $this->GetY() ;
      $sX = $this->MOST_LEFT ;
      $this->SetLineWidth($this->LINE_W_1) ;
      for ($i = 0 ; $i < 2 ; $i++)
      {
        $add = ($i > 0) ? $this->HD_CELLS[$i - 1][0] : 0 ;
        $sX += $add ;
        $this->Line($sX, $this->currY, $sX, $this->MOST_BTM) ;
      }
    }

    function writePageNo()
    {
      $this->SetFont(GOTHIC, '' , 11) ;
      $this->setXY($this->MOST_LEFT, $this->MOST_BTM) ;
      $this->Cell(0, 3, "- " . $this->pno . " -", '', 0, 'C') ;
    }

    function writePtifDataConts()
    {
      $roomName = $this->currPtif->room ;
      $ptifName = $this->currPtif->name ;
      //      if (($roomName == null) || ($ptifName == null)) return ;

      $this->saveY = $this->currY ;
      $this->setY($this->currY) ;
      $this->Cell($this->HD_CELLS[0][0], $this->CELL_H, $roomName, "0", 0, "L") ;
      $this->Cell($this->HD_CELLS[1][0], $this->CELL_H, $ptifName, "0", 0, "L") ;

      $this->SetLineWidth($this->LINE_W_1) ;
      $this->SetFillColor(210) ;
      foreach ($this->currPtif->eiyos as $idxe => $eiyo)
      {
        if ($idxe > 0) $eiyo["string20"] = "" ; // ←全行でコメントを出すときは、ここをコメントアウトする
        $this->writeEiyoDataConts($eiyo) ;
      }

      if ($this->pagePtifNum > 0)
      {
        $this->SetLineWidth($this->LINE_W_2) ;
        $this->Line($this->MOST_LEFT + 0.3, $this->saveY - 0.15, $this->MOST_RIGHT - 0.3, $this->saveY - 0.15) ;
      }
      $this->pagePtifNum++ ;
    }

    function writeEiyoDataConts($eiyo)
    {
      $isDate = $eiyo["isdate" ] ;
      if (strlen($isDate) > 0)
      {
        $this->setXY($this->eiyosX, $this->currY) ;
        $this->Cell($this->MOST_RIGHT - $this->eiyosX, $this->CELL_H, $isDate, "1", 1, "C", 0) ; // 背景色を入れる場合は最後の引数に｢1｣を指定する
        $this->currY = $this->GetY() ;
      }
      $fill = ($eiyo["link_cd2"] == "1") ? 1 : 0 ;
      $commX = $this->eiyosX ;
      for ($i = 2 ; $i < count($this->HD_CELLS) - 1 ; $i++)
      {
        $commX += $this->HD_CELLS[$i][0] ;
      }
      $lineType = "LTR" ;
      $this->setXY($commX, $this->currY) ;
      $this->MultiCell($this->HD_CELLS[8][0], $this->CELL_H, $eiyo["string20"], $lineType, "L", $fill) ;
      $tempY = $this->GetY() ;
      $cH = $tempY - $this->currY ;
      if ($cH > $this->CELL_H)
      {
        $this->setXY($this->eiyosX, $this->currY) ;
        $this->Cell($this->HD_CELLS[2][0], $cH, "", $lineType, 0, "L", $fill) ;
        $this->Cell($this->HD_CELLS[3][0], $cH, "", $lineType, 0, "L", $fill) ;
        $this->Cell($this->HD_CELLS[4][0], $cH, "", $lineType, 0, "L", $fill) ;
        $this->Cell($this->HD_CELLS[5][0], $cH, "", $lineType, 0, "L", $fill) ;
        $this->Cell($this->HD_CELLS[6][0], $cH, "", $lineType, 0, "L", $fill) ;
        $this->Cell($this->HD_CELLS[7][0], $cH, "", $lineType, 0, "L", $fill) ;
        $fill = 0 ; $lineType = "0" ;
      }
      $this->setXY($this->eiyosX, $this->currY) ;
      $this->Cell($this->HD_CELLS[2][0], $this->CELL_H, $eiyo["string2" ], $lineType, 0, "L", $fill) ;
      $this->Cell($this->HD_CELLS[3][0], $this->CELL_H, $eiyo["string4" ], $lineType, 0, "L", $fill) ;
      $this->Cell($this->HD_CELLS[4][0], $this->CELL_H, $eiyo["string5" ], $lineType, 0, "L", $fill) ;
      $this->Cell($this->HD_CELLS[5][0], $this->CELL_H, $eiyo["string6" ], $lineType, 0, "L", $fill) ;
      $this->Cell($this->HD_CELLS[6][0], $this->CELL_H, $eiyo["string7" ], $lineType, 0, "L", $fill) ;
      $this->Cell($this->HD_CELLS[7][0], $this->CELL_H, $eiyo["string17"], $lineType, 1, "L", $fill) ;
      $this->currY = $tempY ;
    }

    // 印刷ダイアログボックスを表示するためにオーバーライドします
    function Output($showPrintDialog)
    {
      $this->printJs = ($showPrintDialog) ? $this->PRINT_JS : "" ;
      parent::Output() ;
    }
    // 印刷ダイアログボックスを表示するためにオーバーライドします
    function _putresources()
    {
      parent::_putresources() ;
      if (strlen($this->printJs) < 1) return ;
      $this->_newobj() ;
      $this->cin = $this->n ;
      $this->_out("<<\n/Names [(EmbeddedJS) " . ($this->cin + 1) . " 0 R]\n>>\nendobj\n") ;
      $this->_newobj() ;
      $this->_out("<<\n/S /JavaScript\n/JS " . $this->printJs . "\n>>\nendobj\n") ;
    }
    function _putcatalog()
    {
      parent::_putcatalog() ;
      if (strlen($this->printJs) < 1) return ;
      $this->_out("/Names <</JavaScript " . $this->cin . " 0 R>>") ;
    }

  }
  //=================================================
  // ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
  // PDF生成ライブラリの拡張クラス定義
  //=================================================

  $klTitle = $klst->TITLE ;
  $dtStr   = $date_y . "年" . (int)$date_m . "月" . (int)$date_d . "日" ;

  //  $pdf = new MBFPDF() ;
  $pdf = new CustomMBFPDF($klTitle, $dtStr) ;
  $pdf->SetAutoPageBreak(false) ;
  $pdf->AddMBFont(GOTHIC, 'EUC-JP') ;
  $pdf->SetMargins($pdf->MOST_LEFT, $pdf->MOST_TOP) ;
  $pdf->Open() ;

  $pdf->AddPage1() ;

  foreach ($klst->ptifs as $idxp => $ptif) // 患者ループ 開始
  {
    $pdf->currPtif = $ptif ;
    $pdf->writePtifDataConts() ;
    // ページ下端を超えたら改ページ処理(2)を行う
    if ($pdf->currY > $pdf->MOST_BTM) $pdf->AddPage2() ;
  } // 患者ループ 終了

  $pdf->fillWhite($pdf->MOST_LEFT - 2, $pdf->currY - 0.1, $pdf->MOST_RIGHT - $pdf->MOST_LEFT + 4, $pdf->A4H - $pdf->currY) ;
  $pdf->SetLineWidth($pdf->LINE_W_1) ;
  $pdf->Line($pdf->MOST_LEFT, $pdf->currY - 0.15, $pdf->MOST_RIGHT, $pdf->currY - 0.15) ;
  $pdf->writePageNo() ;

  $pdf->Output(true) ;
  die ;

}
else
{
  ob_end_flush() ;
}

?>
