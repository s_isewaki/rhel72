<?
require_once("about_comedix.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form id="items" name="items" action="schedule_registration.php" method="post">
<input type="hidden" name="back" value="t">
<input type="hidden" name="schd_title" value="<?php eh($schd_title); ?>">
<input type="hidden" name="marker" value="<?php echo($marker); ?>">
<input type="hidden" name="place_id" value="<?php echo($place_id); ?>">
<input type="hidden" name="schd_plc" value="<?php echo($schd_plc); ?>">
<input type="hidden" name="schd_imprt" value="<?php echo($schd_imprt); ?>">
<input type="hidden" name="pub_limit" value="<?php echo($pub_limit); ?>">
<input type="hidden" name="pub_limit_dept" value="<?php echo($pub_limit_dept); ?>">
<input type="hidden" name="pub_limit_group_id" value="<?php echo($pub_limit_group_id); ?>">
<input type="hidden" name="schd_start_yrs" value="<?php echo($schd_start_yrs); ?>">
<input type="hidden" name="schd_start_mth" value="<?php echo($schd_start_mth); ?>">
<input type="hidden" name="schd_start_day" value="<?php echo($schd_start_day); ?>">
<input type="hidden" name="schd_start_hrs" value="<?php echo($schd_start_hrs); ?>">
<input type="hidden" name="schd_start_min" value="<?php echo($schd_start_min); ?>">
<input type="hidden" name="schd_dur_hrs" value="<?php echo($schd_dur_hrs); ?>">
<input type="hidden" name="schd_dur_min" value="<?php echo($schd_dur_min); ?>">
<input type="hidden" name="time_option" value="<?php echo($time_option); ?>">
<input type="hidden" name="timeless" value="<?php echo($timeless); ?>">
<input type="hidden" name="schd_type" value="<?php echo($schd_type); ?>">
<input type="hidden" name="schd_detail" value="<?php eh($schd_detail); ?>">
<input type="hidden" name="mygroup" value="<?php echo($mygroup); ?>">
<input type="hidden" name="target_id_list" value="<?php echo($target_id_list); ?>">
<input type="hidden" name="cate_id" value="<?php echo($cate_id); ?>">
<input type="hidden" name="facility_id" value="<?php echo($facility_id); ?>">
<input type="hidden" name="users" value="<?php echo($users); ?>">
<input type="hidden" name="repeat_type" value="<?php echo($repeat_type); ?>">
<input type="hidden" name="rpt2_1" value="<?php echo($rpt2_1); ?>">
<input type="hidden" name="rpt2_2" value="<?php echo($rpt2_2); ?>">
<input type="hidden" name="rpt3_1" value="<?php echo($rpt3_1); ?>">
<input type="hidden" name="rpt3_2" value="<?php echo($rpt3_2); ?>">
<input type="hidden" name="rpt3_3" value="<?php echo($rpt3_3); ?>">
<input type="hidden" name="rpt_end_year" value="<?php echo($rpt_end_year); ?>">
<input type="hidden" name="rpt_end_month" value="<?php echo($rpt_end_month); ?>">
<input type="hidden" name="rpt_end_day" value="<?php echo($rpt_end_day); ?>">
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="schd_emp_id" value="<?php echo($schd_emp_id); ?>">
<input type="hidden" name="calendar" value="<?php echo($calendar); ?>">
<input type="hidden" name="repeat" value="<?php echo($repeat); ?>">
<input type="hidden" name="datelist" value="<?php echo($datelist); ?>">
<input type="hidden" name="sendmail" value="<?php echo($sendmail); ?>">
<input type="hidden" name="alarm_h" value="<?php echo $_POST["alarm_h"]; ?>">
<input type="hidden" name="alarm_m" value="<?php echo $_POST["alarm_m"]; ?>">

</form>
<script type="text/javascript">
function appendLog(val) {
    var ipt = document.createElement('input');
    ipt.type = 'hidden';
    ipt.name = 'log[]';
    ipt.value = val;

    var frm = document.getElementById("items");
    frm.appendChild(ipt);
}
</script>
<?
require_once("get_values.ini");
require_once("webmail/config/config.php");
require_once("facility_common.ini");
require_once("schedule_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("Cmx/Model/SystemConfig.php");

$fname = $PHP_SELF;

mb_internal_encoding("EUC-JP");
mb_language("Japanese");

$with_calendar = ($calendar == "on");
$with_repeat = (!$with_calendar && $repeat == "on");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


// 入力チェック
$current_regex_encoding = mb_regex_encoding('EUC-JP');
if (mb_ereg_replace("[ 　]", "", $schd_title) === "") {
    echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
mb_regex_encoding($current_regex_encoding);
if (strlen($schd_title) > 100) {
    echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (strlen($schd_plc) > 50) {
    echo("<script type=\"text/javascript\">alert('行先（テキスト）が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!with_calendar && !checkdate($schd_start_mth, $schd_start_day, $schd_start_yrs)) {
    echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($with_calendar && $datelist == "") {
    echo("<script type=\"text/javascript\">alert('日付が選択されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($timeless == "") {
    if ("$schd_start_hrs$schd_start_min" >= "$schd_dur_hrs$schd_dur_min") {
        echo("<script type=\"text/javascript\">alert('時刻が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if ($with_repeat && ($repeat_type == "2" || $repeat_type == "3")) {
    if (!checkdate($rpt_end_month, $rpt_end_day, $rpt_end_year)) {
        echo("<script type=\"text/javascript\">alert('繰り返し終了日が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if ($target_id_list == "") {
    echo("<script type=\"text/javascript\">alert('登録対象者が選択されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($users != "") {
    if (preg_match("/^[0-9]{1,4}$/", $users) == 0) {
        echo("<script type=\"text/javascript\">alert('利用人数は半角数字1〜4桁で入力してください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}

// アラーム
$alarm = 0;
$conf = new Cmx_SystemConfig();
$schedule_alarm = $conf->get('schedule.alarm');
$schedule_alarm_start = $conf->get('schedule.alarm.start');
$schedule_alarm_end = $conf->get('schedule.alarm.end');
if ($schedule_alarm) {
    if (preg_match("/\D/",$_POST["alarm_h"]) or preg_match("/\D/",$_POST["alarm_m"])) {
        echo("<script type=\"text/javascript\">alert('アラーム通知時間は数値で入力してください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
    if ($schedule_alarm === "1") {
        if (is_numeric($_POST["alarm_h"])) {
            $alarm += $_POST["alarm_h"] * 60;
        }
        if (is_numeric($_POST["alarm_m"])) {
            $alarm += $_POST["alarm_m"];
        }
        if ($alarm and ($schedule_alarm_start > $alarm or $schedule_alarm_end < $alarm)) {
            echo("<script type=\"text/javascript\">alert('アラーム通知時間は設定可能範囲内で入力してください。');</script>");
            echo("<script type=\"text/javascript\">document.items.submit();</script>");
            exit;
        }
    }
    else if ($schedule_alarm === "2") {
        if ($_POST["alarm_m"]) {
            $alarm = 10;
        }
    }
}

// 登録対象日を配列で取得
$start_date = "$schd_start_yrs$schd_start_mth$schd_start_day";
$end_date = "$rpt_end_year$rpt_end_month$rpt_end_day";
$arr_date = array();
if ($with_calendar) {
    $arr_date = explode(",", $datelist);
} else if ($with_repeat) {
    switch ($repeat_type) {
    case "1":  // 繰り返さない
        array_push($arr_date, $start_date);
        break;
    case "2":  // 繰り返しタイプ1
        $arr_date = get_target_date1($start_date, $end_date, $rpt2_1, $rpt2_2);
        break;
    case "3":  // 繰り返しタイプ2
        $arr_date = get_target_date2($start_date, $end_date, $rpt3_1, $rpt3_2, $rpt3_3);
        break;
    }
} else {
    array_push($arr_date, $start_date);
}
if (count($arr_date) == 0) {
    echo("<script type=\"text/javascript\">alert('登録対象日がありません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

// 登録値の編集
$repeat_flg = (count($arr_date) > 1) ? "t" : "f";
$reg_time = get_millitime();
// 個人スケジュール連携フラグ
$schd_flg = "t";

if ($schd_imprt != "2") {
    $pub_limit = null;
} else {
    if ($pub_limit == "d") {
        $pub_limit = "d" . $pub_limit_dept;
    } else if ($pub_limit == "g") {
        $pub_limit = $pub_limit_group_id;
    }

    if ($pub_limit == "") {
        $pub_limit = null;
    }
}

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id), emp_ext from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);
$emp_ext = pg_fetch_result($sel, 0, 'emp_ext');

// 設備予約管理者以外の場合は予約可能期間の確認。「指定しない」表示の施設の確認。
if ($facility_id != 0) {
    // 予約可能期間の確認
    list($ret, $term) = check_term($con, $fname, $facility_id, $arr_date, $emp_id);
    if (!$ret) {
        echo("<script type=\"text/javascript\">alert('予約可能期間内($term)の日付を指定してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }

    // 「指定しない」表示の確認
    $sql = "select facility_name,show_timeless_flg from facility";
    $cond = "where facility_id = {$facility_id}";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $show_timeless_flg = pg_fetch_result($sel, 0, "show_timeless_flg");
    $facility_name = pg_fetch_result($sel, 0, "facility_name");
    if ($show_timeless_flg == 'f' && $timeless == 't') {
        echo("<script type=\"text/javascript\">alert('{$facility_name}の予約時刻が必要なため、時刻を指定してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
// トランザクションの開始
pg_query($con, "begin transaction");

// 行先名を取得
if ($place_id == "") {
    $place_name = "";
    // 行先が未指定の場合DBにnull設定
    $place_id = null;
} else {
    $sql = "select place_name from scheduleplace";
    $cond = "where place_id = $place_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $place_name = pg_fetch_result($sel, 0, "place_name");
}

// 登録対象職員を配列化
$arr_emp_id = array();
if ($target_id_list != "") {
    $arr_emp_id = array_merge($arr_emp_id, split(",", $target_id_list));
}

// 施設・設備予約ログを初期化
$log = array();

// 登録対象日をループ
foreach ($arr_date as $tmp_date) {

    // 登録対象職員をループ
    foreach ($arr_emp_id as $tmp_emp_id) {

        // スケジュールIDを採番
        $schd_id = get_schd_id($con, $fname, $timeless);

        // スケジュール情報を登録
        $tmp_status = ($tmp_emp_id == $emp_id) ? "1" : "2";
		$aprv_ctrl = $conf->get("schedule.aprv_ctrl");
		if($aprv_ctrl == "2")
		{
			//承認依頼しない(管理画面のその他タグで指定)
			$tmp_status = "3";
		}
		
        if ($timeless != "t") {
            // 24:00対応
            $schd_dur = "$schd_dur_hrs:$schd_dur_min";
            if ($schd_dur == "24:00") {
                $schd_dur = "23:59";
            }
            $sql = "insert into schdmst (schd_id, emp_id, schd_title, schd_plc, schd_type, schd_imprt, schd_start_date, schd_start_time, schd_day_flg, schd_dur, schd_detail, schd_place_id, schd_reg_id, schd_status, marker, repeat_flg, reg_time, schd_start_time_v, schd_dur_v, pub_limit, alarm, alarm_confirm) values (";
            $content = array($schd_id, $tmp_emp_id, p($schd_title), $schd_plc, $schd_type, $schd_imprt, $tmp_date, "$schd_start_hrs:$schd_start_min", "f", $schd_dur, p($schd_detail), $place_id, $emp_id, $tmp_status, $marker, $repeat_flg, $reg_time, "$schd_start_hrs$schd_start_min", "$schd_dur_hrs$schd_dur_min", $pub_limit, $alarm, 0);
        } else {
            $sql = "insert into schdmst2 (schd_id, emp_id, schd_title, schd_plc, schd_type, schd_imprt, schd_start_date, schd_detail, schd_place_id, schd_reg_id, schd_status, marker, repeat_flg, reg_time, pub_limit, alarm, alarm_confirm) values (";
            $content = array($schd_id, $tmp_emp_id, p($schd_title), $schd_plc, $schd_type, $schd_imprt, $tmp_date, p($schd_detail), $place_id, $emp_id, $tmp_status, $marker, $repeat_flg, $reg_time, $pub_limit, $alarm, 0);
        }
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    // 施設・設備選択時
    if ($facility_id != 0) {
        $s_time = "$schd_start_hrs$schd_start_min";
        $e_time = "$schd_dur_hrs$schd_dur_min";
        $date_for_log = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $tmp_date);
        $dup_count = 0;
        // 時刻指定がある場合
        if ($timeless == "") {
            // 予約が重複するかチェック
            $sql = "select count(*) from reservation";
            $cond = "where fclcate_id = $cate_id and facility_id = $facility_id and date = '$tmp_date' and not (start_time >= '$e_time' or end_time <= '$s_time') and reservation_del_flg = 'f'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $dup_count = pg_fetch_result($sel, 0, 0);
        }

        // 重複しない場合
        if ($dup_count == 0) {

            // 時刻指定がある場合
            if ($timeless == "") {
                // 予約IDを採番
                $reservation_id = get_reservation_id($con, $fname);

                // 予約レコードを作成
                $sql = "insert into reservation (reservation_id, fclcate_id, facility_id, emp_id, date, start_time, end_time, title, content, users, repeat_flg, schd_flg, reg_time, fclhist_id, emp_ext) values (";
                $contents = array($reservation_id, $cate_id, $facility_id, $emp_id, $tmp_date, $s_time, $e_time, p($schd_title), p($schd_detail), $users, $repeat_flg, $schd_flg, $reg_time, 0, $emp_ext);
            } else {
                // 予約IDを採番
                $reservation_id = get_reservation_id2($con, $fname);

                // 予約レコードを作成
                $sql = "insert into reservation2 (reservation_id, fclcate_id, facility_id, emp_id, date, title, content, users, repeat_flg, schd_flg, reg_time, fclhist_id, emp_ext) values (";
                $contents = array($reservation_id, $cate_id, $facility_id, $emp_id, $tmp_date, p($schd_title), p($schd_detail), $users, $repeat_flg, $schd_flg, $reg_time, 0, $emp_ext);
            }
            $ins = insert_into_table($con, $sql, $contents, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // ログを作成
            echo("<script type=\"text/javascript\">appendLog('$date_for_log,成功');</script>");
        // 重複する場合
        } else {

            // ログを作成
            echo("<script type=\"text/javascript\">appendLog('$date_for_log,失敗（すでに予約あり）');</script>");
        }
    }
}

// トランザクションをコミット
pg_query($con, "commit");

if ($sendmail == "t") {
    $to_login_ids = array();
    foreach ($arr_emp_id as $tmp_emp_id) {
        if ($tmp_emp_id != $emp_id) {
            $sql = "select get_mail_login_id(emp_id), emp_lt_nm, emp_ft_nm from empmst";
            $cond = "where emp_id = '$tmp_emp_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $tmp_emp_login_id = pg_fetch_result($sel, 0, 0);
            $tmp_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
            $to_login_ids["$tmp_emp_login_id"] = $tmp_emp_nm;
        }
    }

    $mail_schd_date_times = array();
    for ($i = 0, $j = count($arr_date); $i < $j; $i++) {
        $tmp_date = $arr_date[$i];
        $mail_schd_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $tmp_date);
        if ($i == 0) {
            $mail_schd_time = ($timeless == "t") ? "時刻指定なし" : "{$schd_start_hrs}:{$schd_start_min}〜{$schd_dur_hrs}:{$schd_dur_min}";
        } else {
            $mail_schd_time = "　〃";
        }
        $mail_schd_date_times[] = "$mail_schd_date $mail_schd_time";
    }
    $mail_schd_date_time = implode("\n　　　　　", $mail_schd_date_times);
    $mail_schd_detail = str_replace("\n", "\n　　　　　", $schd_detail);
    $subject = "[CoMedix] スケジュール登録のお知らせ";

    $place_name = ($place_id == "0") ? "その他" : $place_name;
    if ($place_name != "") {
        $place_name .= " {$schd_plc}";
    } else {
        $place_name = "{$schd_plc}";
    }

    $message = "下記のスケジュールが登録されました。\n\n";
    $message .= "登録者　：{$emp_nm}\n";
    $message .= "タイトル：{$schd_title}\n";
    $message .= "行先　　：{$place_name}\n";
    $message .= "日時　　：{$mail_schd_date_time}\n";
    $message .= "詳細　　：{$mail_schd_detail}\n";
    $message = mb_convert_kana($message, "KV"); // 半角カナ変換
    $additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";

    foreach ($to_login_ids as $to_login_id => $tmp_emp_nm) {
        $to_addr = mb_encode_mimeheader (mb_convert_encoding($tmp_emp_nm,"ISO-2022-JP","AUTO")) ." <$to_login_id@$domain>";
        mb_send_mail($to_addr, $subject, $message, $additional_headers);
    }
}

// データベース接続を閉じる
pg_close($con);

// 画面遷移
echo("<script type=\"text/javascript\">window.opener.location.reload(true);</script>");
if ($facility_id == 0) {
    echo("<script type=\"text/javascript\">self.close();</script>");
} else {
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
}

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 登録対象日付を配列で返す（繰り返しタイプ1）
function get_target_date1($start_date, $end_date, $cond1, $cond2) {

    // 戻り値となる配列を宣言
    $arr = array();

    // パス回数の上限をセット
    switch ($cond1) {
    case "1":  // 毎
        $pass_count = 0;
        break;
    case "2":  // 隔
        $pass_count = 1;
        break;
    }

    // スタート日の情報を変数に格納
    $weekday = date("w", to_timestamp($start_date));  // 曜日
    $day = substr($start_date, 6, 2); // 日

    // 期間ループ内で使用する変数を初期化
    $tmp_pass_count = 0;
    $pass_flag = false;
    $tmp_date = $start_date;

    // 期間をループ
    while ($tmp_date <= $end_date) {

        // 処理日付の曜日を取得
        $tmp_weekday = date("w", to_timestamp($tmp_date));

        // 登録対象日かどうか、および特殊日かどうかをチェック
        switch ($cond2) {
        case "1":  // 日：無条件で登録、無条件で特殊日
            $target_flag = true;
            $special_flag = true;
            break;
        case "2":  // 週：スタート日と曜日が同じであれば登録、登録日なら特殊日
            $target_flag = ($tmp_weekday == $weekday);
            $special_flag = $target_flag;
            break;
        case "3":  // 月：スタート日と日が同じであれば登録、登録日なら特殊日
            $target_flag = (substr($tmp_date, 6, 2) == $day);
            $special_flag = $target_flag;
            break;
        case "4":  // 月、水、金：月・水・金のいずれかであれば登録、金なら特殊日
            $target_flag = ($tmp_weekday == 1 || $tmp_weekday == 3 || $tmp_weekday == 5);
            $special_flag = ($tmp_weekday == 5);
            break;
        case "5":  // 火、木：火・木のいずれかであれば登録、木なら特殊日
            $target_flag = ($tmp_weekday == 2 || $tmp_weekday == 4);
            $special_flag = ($tmp_weekday == 4);
            break;
        case "6":  // 月〜金：土・日以外であれば登録、金なら特殊日
            $target_flag = ($tmp_weekday != 6 && $tmp_weekday != 0);
            $special_flag = ($tmp_weekday == 5);
            break;
        case "7":  // 土、日：土・日のいずれかであれば登録、日なら特殊日
            $target_flag = ($tmp_weekday == 6 || $tmp_weekday == 0);
            $special_flag = ($tmp_weekday == 0);
            break;
        }

        // 登録対象日かつパスフラグが立っていなければ配列に追加
        if ($target_flag && !$pass_flag) {
            array_push($arr, $tmp_date);
        }

        // 特殊日の場合
        if ($special_flag) {

            // パスフラグを立てる
            $pass_flag = true;

            // 次回のパス回数が上限を超える場合、パスをキャンセル
            if (++$tmp_pass_count > $pass_count) {
                $pass_flag = false;
                $tmp_pass_count = 0;
            }
        }

        // 日付を進める
        $tmp_date = next_date($tmp_date);
    }

    return $arr;

}

// 登録対象日付を配列で返す（繰り返しタイプ2）
function get_target_date2($start_date, $end_date, $cond1, $cond2, $cond3) {

    // 戻り値となる配列を宣言
    $arr = array();

    // 処理対象曜日を変数にセット
    switch ($cond3) {
    case "1":  // 日
        $weekday = 0;
        break;
    case "2":  // 月
        $weekday = 1;
        break;
    case "3":  // 火
        $weekday = 2;
        break;
    case "4":  // 水
        $weekday = 3;
        break;
    case "5":  // 木
        $weekday = 4;
        break;
    case "6":  // 金
        $weekday = 5;
        break;
    case "7":  // 土
        $weekday = 6;
        break;
    }

    // スタート日付の月の第1Ｘ曜日を求める
    $y = substr($start_date, 0, 4);
    $m = substr($start_date, 4, 2);
    for ($d = 1; $d <= 7; $d++) {
        if (date("w", mktime(0, 0, 0, $m, $d, $y)) == $weekday) {
            break;
        }
    }
    $tmp_date = "{$y}{$m}0{$d}";

    // スタート月からエンド月までの日付情報を月ごとにtmp配列に格納
    $arr_tmp = array();
    $tmp_ym = substr($tmp_date, 0, 6);
    $end_ym = substr($end_date, 0, 6);
    while ($tmp_ym <= $end_ym) {

        // 年月が配列のキーとして存在しなければ追加（配列で初期化）
        if (!array_key_exists($tmp_ym, $arr_tmp)) {
            $arr_tmp[$tmp_ym] = array();
        }

        // 下層の配列に処理日付を追加
        array_push($arr_tmp[$tmp_ym], $tmp_date);

        // 日付を翌週に進める
        $tmp_date = next_week($tmp_date);

        // 処理日付の年月を変数にセット
        $tmp_ym = substr($tmp_date, 0, 6);
    }

    // 日付情報のうち指定週のもののみをtmp配列にセット
    $arr_tmp2 = array();
    foreach ($arr_tmp as $val) {
        switch ($cond2) {
        case "1":  // 第1
            array_push($arr_tmp2, $val[0]);
            break;
        case "2":  // 第2
            array_push($arr_tmp2, $val[1]);
            break;
        case "3":  // 第3
            array_push($arr_tmp2, $val[2]);
            break;
        case "4":  // 第4
            array_push($arr_tmp2, $val[3]);
            break;
        case "5":  // 最終
            array_push($arr_tmp2, $val[count($val) - 1]);
            break;
        }
    }

    // 対象日付がなければリターン
    if (count($arr_tmp2) == 0) {
        return $arr_tmp2;
    }

    // 指定期間から外れる分を削除
    while ($arr_tmp2[0] < $start_date) {
        array_shift($arr_tmp2);
        if (count($arr_tmp2) == 0) {
            return $arr_tmp2;
        }
    }
    while ($arr_tmp2[count($arr_tmp2) - 1] > $end_date) {
        array_pop($arr_tmp2);
        if (count($arr_tmp2) == 0) {
            return $arr_tmp2;
        }
    }

    // 「毎月／毎隔月／毎3ヶ月ごと」を考慮して戻り値の配列にセット
    switch ($cond1) {
    case "1":  // 毎月
        $step = 1;
        break;
    case "2":  // 毎隔月
        $step = 2;
        break;
    case "3":  // 毎3ヶ月ごと
        $step = 3;
        break;
    }
    for ($i = 0; $i < count($arr_tmp2); $i += $step) {
        array_push($arr, $arr_tmp2[$i]);
    }

    return $arr;
}

// 翌日日付をyyyymmdd形式で取得
function next_date($yyyymmdd) {
    return date("Ymd", strtotime("+1 day", to_timestamp($yyyymmdd)));
}

// 翌週日付をyyyymmdd形式で取得
function next_week($yyyymmdd) {
    return date("Ymd", strtotime("+7 days", to_timestamp($yyyymmdd)));
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
    $y = substr($yyyymmdd, 0, 4);
    $m = substr($yyyymmdd, 4, 2);
    $d = substr($yyyymmdd, 6, 2);
    return mktime(0, 0, 0, $m, $d, $y);
}
?>
</body>
