<?
ob_start();
require_once("about_comedix.php");
require("show_select_values.ini");
require_once("sot_util.php");
require_once("html_to_text_for_tinymce.php");
ob_end_clean();

$for = false ;
$klst ;
if ($_REQUEST["direct_template_pdf_print_requested"])
{
  $forPDF = true ;
  ob_start() ;
}

// セッションのチェック
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 委員会IDを取得
$sql = "select pjt_id from proschd";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel)) {
	$pjt_id = pg_fetch_result($sel, 0, "pjt_id");
} else {
	$sql = "select pjt_id from proschd2";
	$cond = "where pjt_schd_id = '$pjt_schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$pjt_id = pg_fetch_result($sel, 0, "pjt_id");
}

$conf = new Cmx_SystemConfig();
$link_to_yoshiki9 = ($conf->get('project.link_to_yoshiki9') == 't');

// 初期表示時
if ($back != "t") {

	// 議事録情報を取得
	$sql = "select prcd_subject, prcd_place_id, prcd_place_detail, prcd_date, prcd_start_time, prcd_end_time, prcd_person, prcd_absentee, prcd_summary from proceeding";
	$cond = "where pjt_schd_id = '$pjt_schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$prcd_subject = pg_fetch_result($sel, 0, "prcd_subject");
	$prcd_place_id = pg_fetch_result($sel, 0, "prcd_place_id");
	$prcd_place_detail = pg_fetch_result($sel, 0, "prcd_place_detail");
	$prcd_date = pg_fetch_result($sel, 0, "prcd_date");
	$prcd_start_time = pg_fetch_result($sel, 0, "prcd_start_time");
	$prcd_end_time = pg_fetch_result($sel, 0, "prcd_end_time");
	$prcd_person = pg_fetch_result($sel, 0, "prcd_person");
	$prcd_absentee = pg_fetch_result($sel, 0, "prcd_absentee");
	$prcd_summary = pg_fetch_result($sel, 0, "prcd_summary");
	$prcd_year = substr($prcd_date, 0, 4);
	$prcd_month = substr($prcd_date, 4, 2);
	$prcd_day = substr($prcd_date, 6, 2);
	$prcd_start_hour = substr($prcd_start_time, 0, 2);
	$prcd_start_min = substr($prcd_start_time, 2, 2);
	$prcd_end_hour = substr($prcd_end_time, 0, 2);
	$prcd_end_min = substr($prcd_end_time, 2, 2);

	// 添付ファイル情報を取得
	$sql = "select prcdfile_no, prcdfile_name from prcdfile";
	$cond = "where pjt_schd_id = '$pjt_schd_id' order by prcdfile_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$file_id = array();
	$filename = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_file_no = $row["prcdfile_no"];
		$tmp_filename = $row["prcdfile_name"];

		array_push($file_id, $tmp_file_no);
		array_push($filename, $tmp_filename);

		// 一時フォルダにコピー
		$ext = strrchr($tmp_filename, ".");
		copy("proceeding/{$pjt_schd_id}_{$tmp_file_no}{$ext}", "proceeding/tmp/{$session}_{$tmp_file_no}{$ext}");
	}

	// 様式9連動時は出席者情報を別テーブルから取得
	if ($link_to_yoshiki9) {
		$sql = "select e.emp_lt_nm, e.emp_ft_nm from prcdatnd a inner join empmst e on a.emp_id = e.emp_id";
		$cond = "where pjt_schd_id = '$pjt_schd_id' order by order_no";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_emp_nms = array();
		while ($row = pg_fetch_array($sel)) {
			$tmp_emp_nms[] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
		}
		$prcd_person = implode("、", $tmp_emp_nms);
	}
}

// 30分以上前に保存されたファイルを削除
if (!is_dir("proceeding")) {
	mkdir("proceeding", 0755);
}
if (!is_dir("proceeding/tmp")) {
	mkdir("proceeding/tmp", 0755);
}
foreach (glob("proceeding/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// 承認階層数,承認者情報(設定部分)を取得〜配列に格納
$sql = "select * from prcdaprv_mng ";
$cond = "where pjt_schd_id = '$pjt_schd_id' order by prcdaprv_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$approve_cnt = pg_numrows($sel);

$arr_def_mng_ap = array();
$cnt = 0;
while ($row = pg_fetch_array($sel)) 
{
	$arr_def_mng_ap[$cnt]["notice"] = $row["next_notice_div"];
	$arr_def_mng_ap[$cnt]["order"] =  $row["prcdaprv_order"];
	$cnt++;
	
}

// 承認者情報を取得〜配列に格納
$sql = "select prcdaprv.prcdaprv_order, prcdaprv.prcdaprv_emp_id, prcdaprv.prcdaprv_decide_flg, prcdaprv.substitute_flg,
		prcdaprv.prcdaprv_date, empmst.emp_lt_nm, empmst.emp_ft_nm from prcdaprv inner join empmst on prcdaprv.prcdaprv_emp_id = empmst.emp_id";
$cond = "where prcdaprv.pjt_schd_id = '$pjt_schd_id' order by prcdaprv.prcdaprv_order, prcdaprv.prcdaprv_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$aprv_cnt = pg_num_rows($sel);
$approve = array();
$approve_id = array();
//$deci_flg = array();




$arr_def_ap_user = array();
$cnt = 0;
$level_cnt_forward = "";
while ($row = pg_fetch_array($sel)) 
{
	$tmp_emp_nm = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
	
	
	if($row["prcdaprv_order"] == "")
	{
		$level_cnt = 0;
	}
	else
	{
		$level_cnt = ($row["prcdaprv_order"] - 1);
	}
	
	if($cnt == 0)
	{
		//初期値設定
		$level_cnt_forward = $level_cnt;
	}
	else
	{
		if($level_cnt_forward != $level_cnt)
		{
			//階層が変わったので配列を変えます
			$level_cnt_forward = $level_cnt;
			$cnt = 0;
		}
	}
	
	
	if(($row["prcdaprv_date"] != "")&&($row["prcdaprv_date"] != "00000000"))
	{
		
		if($row["substitute_flg"] == "")
		{
			//代理承認ではなく、自分で承認しました
			
			$str_aprv_date = "(承認済 " . preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["prcdaprv_date"]).")";
			//echo("承認済&nbsp;" . preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["prcdaprv_date"]));
		}
		else
		{
			$str_aprv_date = "(代理承認済)";
		}
		
	}
	elseif($row["prcdaprv_date"] == "00000000")
	{
		//差戻しデータなので、「差戻」を表示する
		$str_aprv_date = "(差戻)";
	}
	else
	{
		$str_aprv_date = "";
	}
	
	$arr_def_ap_user[$level_cnt][$cnt]["name"] = $tmp_emp_nm.$str_aprv_date;
	//$arr_def_mng_ap[$cnt]["no"] =$row["prcdaprv_no"];
	
	$cnt++;
	
}

$prcdaprv_date = array();

while ($row = pg_fetch_array($sel)) {
	array_push($approve, "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}");
	array_push($approve_id, $row["prcdaprv_emp_id"]);
	//	if ($row["prcdaprv_decide_flg"] == "t") {
	//		array_push($deci_flg, $row["prcdaprv_emp_id"]);
	//	}
	array_push($prcdaprv_date, $row["prcdaprv_date"]);
}




// 議事録作成者の職員IDを取得
$sql = "select prcd_create_emp_id from proceeding";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$prcd_create_emp_id = pg_fetch_result($sel, 0, "prcd_create_emp_id");

// 議事録作成者名を取得
$sql = "select emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = '$prcd_create_emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$prcd_create_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . ' ' . pg_fetch_result($sel, 0, "emp_ft_nm");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 場所名を取得
$sql = "select place_name from scheduleplace";
$cond = "where place_id = $prcd_place_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$place_name = pg_fetch_result($sel, 0, "place_name");
}
if ($place_name == "") {
	$place_name = "その他";
}

// 委員会名（WG名）を取得
$sql = "select project.pjt_name, parent.pjt_name as parent_name from project left join project parent on project.pjt_parent_id = parent.pjt_id";
$cond = "where project.pjt_id = $pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pjt_name = pg_fetch_result($sel, 0, "pjt_name");
$parent_name = pg_fetch_result($sel, 0, "parent_name");
if ($parent_name != "") {
	$pjt_name = "$parent_name > $pjt_name";
}

if(!$forPDF){
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | 議事録印刷</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function showAprvFields() {
	document.prcd.action = 'proceeding_register.php';
	document.prcd.submit();
}

function attachFile() {
	window.open('proceeding_attach.php?session=<? echo($session); ?>', 'newwin3', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function deleteProceeding() {
	if (confirm('削除します。よろしいですか？')) {
		document.prcd.action = 'proceeding_delete.php';
		document.prcd.submit();
	}
}

function printPage() {
	window.open('proceeding_print.php?session=<? echo($session); ?>&pjt_schd_id=<? echo($pjt_schd_id); ?>', 'proprint', 'width=700,height=700,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
p.file {margin:0;}
</style>
<style type="text/css">

<!--
p.CLWrap1 {width:485px; word-break: break-all;}
-->

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print(); self.close();">
<center>
<form name="prcd" action="proceeding_update.php" method="post">
<table border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">委員会・WG名</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ ゴシック'"><? echo($pjt_name); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">議題</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'"><? echo($prcd_subject); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">場所</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'"><? echo($place_name); ?>&nbsp;<? echo($prcd_place_detail); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">日付</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'"><? echo($prcd_year); ?>/<? echo($prcd_month); ?>/<? echo($prcd_day); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">時刻</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'"><? echo($prcd_start_hour); ?>:<? echo($prcd_start_min); ?>〜<? echo($prcd_end_hour); ?>:<? echo($prcd_end_min); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">出席者</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'"><? echo(str_replace("\n", "<br>", $prcd_person)); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">欠席者</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'"><? echo(str_replace("\n", "<br>", $prcd_absentee)); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">記録者</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'"><? echo($prcd_create_emp_nm); ?></span></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" nowrap width="100"><span style="font-size:10pt;font-family:'ＭＳ Ｐゴシック'">概要</span></td>
<td><span style="font-size:10pt;font-family:'ＭＳ ゴシック'"><p class="CLWrap1">
<? 
//表示する前に先頭と最後のPタグをはずす
//$str_w1 = substr($prcd_summary,3);
//$str_w2 = substr($str_w1,0,strlen($str_w1)-4);
//echo($str_w2);
$chk_word = substr($prcd_summary,0,3);
if($chk_word == "<p>")
{
	$str_w1 = substr($prcd_summary,3);
	$str_w2 = substr($str_w1,0,strlen($str_w1)-4);
}
else
{
	$str_w2 = $prcd_summary;
}

echo($str_w2);



?>
</p></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="attach">
<?
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$ext = strrchr($tmp_filename, ".");

	echo("<p id=\"p_{$tmp_file_id}\" class=\"file\">\n");
	echo("$tmp_filename\n");
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
	echo("</p>\n");
}
?>
</div>
</font></td>
</tr>

<?

if(($aprv_cnt > 0) && ($approve_cnt < 1))
{
	//途中から階層が追加されたので追加前の承認者をデフォルトで1階層に設定する
	$approve_cnt = 1;
}
for ($i = 0; $i < $approve_cnt; $i++) 
{
	
	if($arr_def_ap_user[$i][0]["name"] != "" )
	{
		
		for ($wcnt1 = 0; $wcnt1 < count($arr_def_ap_user[$i]); $wcnt1++)
		{
			if($wcnt1 == 0)
			{
				$str_emp_nm = $arr_def_ap_user[$i][$wcnt1]["name"];
			}
			else
			{
				$str_emp_nm = $str_emp_nm.", ".$arr_def_ap_user[$i][$wcnt1]["name"];
			}			
		}
	}
	else
	{
		$str_emp_nm = "";
	}
	
	$str_dis_notice = "";
	if($arr_def_mng_ap[$i]["notice"] == "2")
	{
		$str_dis_notice = "(並列)";
	}
	else
	{
		$str_dis_notice = "(同期)";
	}
?>

<tr height="22">
	<td align="right" bgcolor="#f6f9ff">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認階層<?=$i+1?><?=$str_dis_notice?></font>
	</td>
	<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$str_emp_nm?></font>
	</td>
</tr>

<? 
}
?>



</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_schd_id" value="<? echo($pjt_schd_id); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
</form>
</center>
</body>
</html>


<?
$approve_cnt = 0;
if($mode == 'show_flg_update') {

	$sql = "select * from proceeding";
	$cond = "where pjt_schd_id = '$pjt_schd_id' and apv_fix_show_flg = 't' and prcd_status = '2'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$approve_cnt = pg_numrows($sel);
	if($approve_cnt > 0) {


		// トランザクションを開始
		pg_query($con, "begin");

		$sql = "update proceeding set";
		$set = array("apv_fix_show_flg");
		$setvalue = array('f');
		$cond = "where pjt_schd_id = '$pjt_schd_id' and apv_fix_show_flg = 't' and prcd_status = '2'";
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		// トランザクションをコミット
		pg_query($con, "commit");
	}
}
pg_close($con);
?>


<script type="text/javascript">
<!--
function page_close() {
<?
if($mode == 'show_flg_update' && $approve_cnt > 0) {
?>
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?
} else {
?>
	window.close();
<?
}
?>

}
//-->
</script>

<? //-------------------------------------------------------------------- ?>
<? // PDF印刷スクリプトを出力する                                         ?>
<? //-------------------------------------------------------------------- ?>
<?
}
else if($forPDF)
{
  // 添付ファイル名を展開
  $tmp_filename = implode("<br />", $filename);

  ob_end_clean() ;
  
  require_once('fpdf153/mbfpdf.php') ;
  //require_once("html_to_text_for_tinymce.php");
  
  //=================================================
  // PDF生成ライブラリの拡張クラス定義
  // ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //=================================================
  class CustomMBFPDF extends MBFPDF
  {
    //｢EUC-JP → SJIS｣変換のためのオーバーライド
    //MultiCell()もWrite()もこのCell()で表示しているので、オーバーライドはCell()だけでOK
    function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '')
    	{
      $enc = mb_internal_encoding();
      if($enc != "sjis-win"){
        $txt = mb_convert_encoding($txt, "sjis-win", $enc) ;
        parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link) ;
      }
    }

    //改ページ フッター
    function Footer(){
        $X = 15;
        $Y = $this->GetY();
        //if($Y >= 280){
           $this->Line($X, $Y ,$X + 180 ,$Y);
        //}
        $this->setY(-10);
        $this->SetFont(GOTHIC,'', 8);
        $this->Cell(0, 5.0, '- '.$this->PageNo().' -', 0, 1, 'C');
        $this->SetFont(GOTHIC,'', 10);
    }
    //ヘッダー
    function Header(){ 
        $X = 15;
        $Y = $this->GetY();
        if($this->PageNo() != 1){
            $this->Line($X,  $Y ,$X + 180 , $Y);
        }
    }

    //複数行になる文の表示
    function multi_row($item, $obj, $curr_y, $wid){
        $X = 15;
        $tt = $this->text_split($obj, $wid);
        for($i = 0; $i < count($tt); ++$i){
            $i == ceil(count($tt)/2) - 1 ? $s = $item : $s = '';
            $this->Cell(50, 6, $s, 'LR', 0,  'L');
        	$useTinyMCE = 1;
            $this->Cell(130, 6, strip_tags(html2text4tinymce2($tt[$i], $useTinyMCE, 0)), 'LR', 1,  'L');
        } 
        $this->Line($X, $this->GetY() ,$X + 180 , $this->GetY());
    }

//半角カナだったら表示する文字数を倍にする（２バイト文字だが表示は１バイトぶんなので）
	function check_LEN($str, $encode='EUC-JP') {
		$charList = array();

		$len = mb_strlen($str, $encode);
		$i = 0;
		while ($i < $len)
		{
			$charList[] = mb_substr($str, $i++, 1, $encode);
		}

		$len = 0;
		for ($i = 0; $i < count($charList); $i++) {
			if (mb_ereg("^[ｱ-ﾝﾞﾟｧ-ｫｬ-ｮｰ｡｢｣､]+$", $str)) {
				// 半角カナ
				$len++;
			}
		}
		return $len;
	}

   //入力値をセルの幅で分割
    function text_split($txt, $wid){
        $s = str_replace("\r\n",'@@',$txt);
        $s = str_replace("\n",'@@',$s);
        $s = str_replace("\r",'@@',$s);
        $s = str_replace("<br />",'@@',$s);
        $ary = explode('@@',$s);
        $int = 1;

        for($i = 0; $i< count($ary); ++$i){
            $str = array();
            $start = 0;

			//半角カナ文字を１バイト文字として換算する（表示エリアを最大に使うため）
			$hosei =  $this->check_LEN($ary[$i]);
			if($hosei > $wid){$hosei = $wid;}
			$div_line = $wid + $hosei;

            $int = ceil(mb_strwidth($ary[$i],"EUC-JP") / $wid);

 			//入力文字列をデコード
			$Chg_str = html_entity_decode($ary[$i]);

			if(mb_strwidth($Chg_str,"EUC-JP") > $wid){
                for($j = 0; $j< $int; ++$j){
					// </p> で改行してPDFに表示させる（Wordのオートコレクト対策）
					$tmp_p = explode("</p>", $ary[$i]);
					$ary_count_p = count($tmp_p);
					if ($ary_count_p != 1) {
						for ($k = count($ary) - 1; $k > $i; $k--) {
							$ary[$k + ($ary_count_p - 1)] = $ary[$k];
						}
						foreach ($tmp_p as $tmp_value) {
							$ary[$k++] = $tmp_value;
						}
						ksort($ary,SORT_NUMERIC);
					}

					// </tr>で改行してPDFに表示させる（Excelの表コピー対策）
					$tmp_tr = explode("</tr>", $ary[$i]);
					$ary_count_tr = count($tmp_tr);
					if ($ary_count_tr != 1) {
						for ($k = count($ary) - 1; $k > $i; $k--) {
							$ary[$k + ($ary_count_tr - 1)] = $ary[$k];
						}
						foreach ($tmp_tr as $tmp_value) {
							$ary[$k++] = $tmp_value;
						}
						ksort($ary,SORT_NUMERIC);
					}

					// 装飾系のものを排除及び変換
					$ary[$i] = strip_tags($ary[$i]);
					$ary[$i] = preg_replace("/&lt;/", "＜", $ary[$i]);
					$ary[$i] = preg_replace("/&gt;/", "＞", $ary[$i]);
					$ary[$i] = preg_replace("/&.*;/", "", $ary[$i]);

					$str[$j] = mb_strcut($ary[$i], $start, $div_line, "EUC-JP");
					mb_strwidth($str[$j],"EUC-JP") == ($div_line -1) ? $ww = $div_line - 1 : $ww = $div_line;
                    $start += $ww;
                }
                $tmp_ary = implode('@@', $str);
				$ary[$i] = preg_replace("/@+$/", "", $tmp_ary);
            }
        }
        $ss = implode('@@', $ary);
        $t  = explode('@@', $ss);
        return $t ;
    }
 
  }
  //=================================================
  // ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
  // PDF生成ライブラリの拡張クラス定義
  //=================================================


  $MOST_RIGHT = 15.0 ;
  $MOST_LEFT  = 15.0 ;
  $MOST_TOP   = 15.0 ;
  // $MOST_BTM   = 285 ;


  $pdf=new CustomMBFPDF();
  $pdf->AddMBFont(GOTHIC, 'SJIS') ;
  $pdf->SetMargins($MOST_LEFT, $MOST_TOP, $MOST_RIGHT) ;
  $pdf->SetAutoPageBreak(true, 15) ;
  $pdf->Open() ;
  $pdf->AddPage() ;

  $X = $pdf->GetX(); 
  $pdf->SetFont(GOTHIC, "", 10) ;
  	
  $pdf->Cell(50, 6, '委員会・WG名', 1, 0, "L") ;
  $pdf->Cell(130, 6, $pjt_name, 1, 1, "L") ;
  $pdf->multi_row('議題', $prcd_subject, $pdf->GetY(), 72);

  $pdf->multi_row('場所', $place_name. " " .$prcd_place_detail, $pdf->GetY(), 72);

  $pdf->Cell(50, 6, '日付', 1, 0, "L") ;
  $pdf->Cell(130, 6, $prcd_year."/".$prcd_month."/".$prcd_day, 1, 1, "L") ;
	
  $pdf->Cell(50, 6, '時刻', 1, 0, "L") ;
  $pdf->Cell(130, 6, $prcd_start_hour.':'.$prcd_start_min.'〜'.$prcd_end_hour.':'.$prcd_end_min, 1, 1, "L") ;

  $pdf->multi_row('出席者', $prcd_person, $pdf->GetY(), 72);
  $pdf->multi_row('欠席者', $prcd_absentee, $pdf->GetY(), 72);
  $pdf->Cell(50, 6, '記録者', 1, 0, "L") ;
  $pdf->Cell(130, 6, $prcd_create_emp_nm, 1, 1, "L") ;
  $wk_prcd_summary = str_replace("\"", "'", $prcd_summary);
  $pdf->multi_row('概要', $wk_prcd_summary, $pdf->GetY(), 72);
  $pdf->multi_row('添付ファイル', $tmp_filename, $pdf->GetY(), 72);


if(($aprv_cnt > 0) && ($approve_cnt < 1))
{
	//途中から階層が追加されたので追加前の承認者をデフォルトで1階層に設定する
	$approve_cnt = 1;
}
for ($i = 0; $i < $approve_cnt; $i++) 
{
	if($arr_def_ap_user[$i][0]["name"] != "" )
	{
		for ($wcnt1 = 0; $wcnt1 < count($arr_def_ap_user[$i]); $wcnt1++)
		{
			if($wcnt1 == 0)
			{
				$str_emp_nm = $arr_def_ap_user[$i][$wcnt1]["name"];
			}
			else
			{
				$str_emp_nm = $str_emp_nm.", ".$arr_def_ap_user[$i][$wcnt1]["name"];
			}			
		}
	}
	else
	{
		$str_emp_nm = "";
	}
	
	$str_dis_notice = "";
	if($arr_def_mng_ap[$i]["notice"] == "2")
	{
		$str_dis_notice = "(並列)";
	}
	else
	{
		$str_dis_notice = "(同期)";
	}
  //承認階層追加
  //$pdf->multi_row('承認階層', aa, $pdf->GetY(), 72);
  //$i2 = $i+1;
  $pdf->multi_row('承認階層'. ($i+1) .$str_dis_notice, $str_emp_nm, $pdf->GetY(), 72);
 }


 $pdf->Output() ;
  die ;
}
else
{
  ob_end_flush() ;
}

?>

