<?
function show_login_message($con, $emp_id, $font_size, $session, $fname) {
	$font_class = ($font_size == "2") ? "j16" : "j12";

	echo("<table width='100%' border='0' cellspacing='0' cellpadding='0' class='block'>\n");
	echo("<tr bgcolor='#bdd1e7'>\n");

	echo("<td>\n");
	echo("<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n");
	echo("<tr>\n");
	if ($font_size == "1") {
		echo("<td width=\"20\" height=\"22\" class=\"spacing\"><img src=\"img/icon/s05.gif\" width=\"20\" height=\"20\" border=\"0\" alt=\"伝言メモ\"></td>\n");
	} else {
		echo("<td width=\"32\" height=\"32\" class=\"spacing\"><img src=\"img/icon/b05.gif\" width=\"32\" height=\"32\" border=\"0\" alt=\"伝言メモ\"></td>\n");
	}
	echo("<td width=\"100%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\"><b>伝言メモ</b></font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
	echo("</td>\n");
	echo("</tr>\n");
	echo("<tr>\n");
	echo("<td colspan=\"2\">\n");

	$sql = "select message.*, empmst.emp_lt_nm, empmst.emp_ft_nm from message inner join empmst on message.emp_id = empmst.emp_id";
	$cond = "where message.message_towhich_id = '$emp_id' and message.msg_receiver_flg <> '2' and message.msg_receiver_flg <> '3' order by message.message_id desc";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">");
		echo("<tr>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">伝言はありません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
	} else {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">");
		while ($row = pg_fetch_array($sel)) {
			$message_id = $row["message_id"];
			$message_company = $row["message_company"];
			$message_name = $row["message_name"];
			$message_date = $row["message_date"];
			$message_reg_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

			$from = trim("$message_company $message_name");
			if ($from != "") {
				$from .= "様より";
			} else {
				$from = $message_reg_nm . "さんより";
			}

			$message_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $message_date);

			echo("<tr>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\"><a href=\"javascript:void(0);\" onclick=\"window.open('message_detail_login.php?session=$session&message_id=$message_id', 'newwin_login', 'scrollbars=yes,width=640,height=480');\">$from</a></font></td>\n");
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$message_date</font></td>\n");
			echo("</tr>\n");
		}
		echo("</table>\n");
	}

	echo("</td>\n");
	echo("</tr>\n");

	echo("</table>\n");
}

function get_cnt_login_message($con, $emp_id, $fname) {
	$sql = "select message.*, empmst.emp_lt_nm, empmst.emp_ft_nm from message inner join empmst on message.emp_id = empmst.emp_id";
	$cond = "where message.message_towhich_id = '$emp_id' and message.msg_receiver_flg <> '2' and message.msg_receiver_flg <> '3' order by message.message_id desc";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	return (pg_num_rows($sel));

}

?>
