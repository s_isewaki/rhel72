<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($ward == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($trashbox)) {
	echo("<script type=\"text/javascript\">alert('削除対象を選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 選択されたタスクをループ
foreach ($trashbox as $tmp_work_id) {

	// タスク情報を削除
	$sql = "delete from prowork";
	$cond = "where work_id = $tmp_work_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// タスク担当者情報を削除
	$sql = "delete from proworkmem";
	$cond = "where work_id = $tmp_work_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// タスク一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'project_task_menu.php?session=$session&pjt_id=$pjt_id&date=$date&scale=$scale&mode=$mode&adm_flg=$adm_flg';</script>");

?>
