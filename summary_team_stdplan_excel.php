<?php
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require('duty_shift_menu_excel_5_workshop.php');

//====================================
// ページ名
//====================================
$fname = $PHP_SELF;

//====================================
// セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// DBコネクション
//====================================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// パラメータを取得
//====================================
$pt_id = isset($_GET['pt_id']) ? $_GET['pt_id'] : '';
$team_id = isset($_GET['team_id']) ? intval($_GET['team_id']) : -1;

//====================================
// 標準チーム計画データを取得
//====================================
$data = getTeamStandardPlanData($con, $pt_id, $team_id, $fname);
pg_close($con);    // DB切断
if ($data === false) {
    require_once("summary_common.ini");
    summary_common_show_error_page_and_die();
}

//====================================
// Excel作成
//====================================
$fontMSPgothic = mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP');
$fontMSPmincho = mb_convert_encoding('ＭＳ Ｐ明朝','UTF-8','EUC-JP');
$excelObj = new ExcelWorkShop();    // PHP Excel 利用クラスのインスタンス
$excelObj->SetSheetName(mb_convert_encoding(mb_convert_encoding('チーム計画', "sjis-win", "eucJP-win"), "UTF-8", "sjis-win"));
$excelObj->PageRatio('USER', '92');
$excelObj->SetMargin(1, 0.5, 1.5, 1.5, 0.5, 0.5);

////////// 行の高さ設定
// 1ページ目
$excelObj->SetRowDim(5, 3);    // タイトル
$excelObj->SetRowDim(7, 190);
$excelObj->SetRowDim(8, 190);
$excelObj->SetRowDim(9, 190);
$excelObj->SetRowDim(10, 190);

// 2ページ目
$excelObj->SetRowDim(11, 200);
$excelObj->SetRowDim(12, 200);
$excelObj->SetRowDim(13, 200);
$excelObj->SetRowDim(14, 200);

// 3ページ目
$excelObj->SetRowDim(15, 200);
$excelObj->SetRowDim(16, 200);
$excelObj->SetRowDim(17, 200);
$excelObj->SetRowDim(18, 200);

// 4ページ目
$excelObj->SetRowDim(19, 200);
$excelObj->SetRowDim(20, 200);
$excelObj->SetRowDim(21, 200);
$excelObj->SetRowDim(22, 200);

////////// タイトル
$excelObj->SetValueJP2('F1', 'チーム計画', 'BOLD', $fontMSPgothic, 14);

////////// ヘッダ部
createHeaderCells($excelObj, $data);

////////// 患者の問題及びアウトカム
$lastNumProblemAndOutcome = createProblemAndOutcomeCells($excelObj, $data);

////////// チーム実践
$excelObj->CellMerge2('E6:H6', '', '', 'CCFFFF');
$excelObj->SetValueJP2('E6', 'チーム実践', 'HCENTER BOLD', $fontMSPmincho, 10);

// チーム情報
$teamProperties = array(
        array('key' => 'kyoyo', 'type' => 0, 'caption' => '共用'),
        array('key' => 'kango', 'type' => 1, 'caption' => '看護'),
        array('key' => 'kaigo', 'type' => 2, 'caption' => '介護'),
        array('key' => 'rihabiri', 'type' => 3, 'caption' => 'リハビリ')
);

$rowNum = 6;    // 行番号
$isDataExist = false;
// 拡張機能有効時のみ「共用」「看護」「介護」「リハビリ」のデータを出力
// foreach ($teamProperties as $value) {
//     if (isTeamPracticeDataExist($data, $value['key'])) {
//         $rowNum = createTeamPracticeCells($excelObj, $data, $value['key'], $value['type'], $value['caption'], ++$rowNum);
//         $isDataExist = true;
//     }
// }
// 「共用」のみ出力(拡張機能有効時はコメントアウト)
$value = $teamProperties[0];
if (isTeamPracticeDataExist($data, $value['key'])) {
    $rowNum = createTeamPracticeCells($excelObj, $data, $value['key'], $value['type'], $value['caption'], ++$rowNum);
    $isDataExist = true;
}

// チーム実践の全てのデータが無かった場合は、空欄の「共用」を出力
if (!$isDataExist) {
    $rowNum = createTeamPracticeCells($excelObj, $data, 'kyoyo', 0, '共用', ++$rowNum);
}
$lastNumTeamPractice = $rowNum;

////////// 評価日／解決・継続／サイン
$excelObj->CellMerge2('I6:L6', '', '', 'CCFFFF');
$excelObj->SetValueJP2('I6', '評価日／解決・継続／サイン', 'HCENTER BOLD', $fontMSPmincho, 10);
$rowNum = 6;    // 行番号
// 拡張機能有効時のみ「看護」「介護」「リハビリ」のデータを出力
// for ($i = 1; $i < count($teamProperties); $i++) {
//     if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
//         $rowNum = createAssessmentCells($excelObj, $data['ass_list'], $teamProperties[$i]['key'], $teamProperties[$i]['type'], $teamProperties[$i]['caption'], ++$rowNum);
//     }
// }
// 「看護」のみ出力(拡張機能有効時はコメントアウト)
$i = 1;
if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
    $rowNum = createAssessmentCells($excelObj, $data['ass_list'], $teamProperties[$i]['key'], $teamProperties[$i]['type'], $teamProperties[$i]['caption'], ++$rowNum);
}
$lastNumAssessment = $rowNum;

////////// 罫線
$lastNumMax = max($lastNumProblemAndOutcome, $lastNumTeamPractice, $lastNumAssessment);
$excelObj->SetArea("A6:L{$lastNumMax}");    // 外枠
$excelObj->SetBorder2('thin', '', 'outline');
$excelObj->SetArea('A6:L6');
$excelObj->SetBorder2('thin', '', 'outline');
$excelObj->SetArea("E6:H{$lastNumMax}");
$excelObj->SetBorder2('thin', '', 'left');
$excelObj->SetBorder2('thin', '', 'right');
if ($lastNumMax > 10) {
    $excelObj->SetArea('E10:L10');
    $excelObj->SetBorder2('thin', '', 'bottom');
    $excelObj->SetArea('E11:L11');
    $excelObj->SetBorder2('thin', '', 'top');
}
if ($lastNumMax > 14) {
    $excelObj->SetArea('E14:L14');
    $excelObj->SetBorder2('thin', '', 'bottom');
    $excelObj->SetArea('E15:L15');
    $excelObj->SetBorder2('thin', '', 'top');
}
if ($lastNumMax > 18) {
    $excelObj->SetArea('E18:H18');
    $excelObj->SetBorder2('thin', '', 'bottom');
}

// エクセル画面を開いたときのカーソル位置を左上に設定
$excelObj->SetArea('A1');
$excelObj->SetPosV('');

//====================================
// ダウンロード
//====================================
$filename = 'summary_team_stdplan.xls';
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Cache-Control: max-age=0');
ob_end_flush();
$excelObj->OutPut();

/**
 * チーム計画データを取得する
 * 
 * @param resource $con     データベース接続リソース
 * @param string   $pt_id   患者ID
 * @param integer  $team_id 標準チーム計画id
 * @param string   $fname   ページ名
 * 
 * @return mixed 成功時:取得したレコードの配列/失敗時:false
 */
function getTeamStandardPlanData($con, $pt_id, $team_id, $fname)
{
    //====================================
    // 患者氏名を取得
    //====================================
    $sql = "SELECT ptif_id,ptif_lt_kaj_nm,ptif_ft_kaj_nm FROM ptifmst WHERE ptif_id = '$pt_id' ";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    if(pg_num_rows($sel) != 1) {
        $data['kname'] = '';
    } else {
        $ptif_lt_name = pg_fetch_result($sel, 0, 'ptif_lt_kaj_nm');
        $ptif_ft_name = pg_fetch_result($sel, 0, 'ptif_ft_kaj_nm');
        $data['kname'] = $ptif_lt_name . ' ' . $ptif_ft_name;
    }

    //====================================
    // プロブレム名を取得
    //====================================
    $sql = "SELECT problem FROM summary_problem_list ";
    $sql .= "INNER JOIN sum_med_std_team ON summary_problem_list.problem_id = sum_med_std_team.problem_id ";
    $sql .= "WHERE summary_problem_list.ptif_id = '$pt_id' AND sum_med_std_team.team_id = $team_id";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    if(pg_num_rows($sel) != 1) {
        $data['problem'] = '';
    } else {
        $data['problem'] = pg_fetch_result($sel, 0, 'problem');
    }
        
    //====================================
    // チーム計画マスターを取得
    //====================================
    $sql = <<<_SQL_END_
SELECT 
M.team_id,
problem_id,
ptif_id,
create_date,
emp_name,
M.emp_job,
def_value,
s_detail,
e_detail,
risk_detail,
outcome,
left_lock_flag,
lef_lock_time,
lef_lock_emp_id,
kyoyo_op,
kyoyo_tp,
kyoyo_ep,
c0_lock_flag,
c0_lock_time,
c0_lock_emp_id,
kango_op,
kango_tp,
kango_ep,
c1_lock_flag,
c1_lock_time,
c1_lock_emp_id,
kaigo_op,
kaigo_tp,
kaigo_ep,
c2_lock_flag,
c2_lock_time,
c2_lock_emp_id,
rihabiri_op,
rihabiri_tp,
rihabiri_ep,
c3_lock_flag,
c3_lock_time,
c3_lock_emp_id,
 left_jobname||' '||left_emp_name AS left_creater_name,
kyoyo_jobname||' '||kyoyo_emp_name AS kyoyo_creater_name,
kango_jobname||' '||kango_emp_name AS kango_creater_name,
kaigo_jobname||' '||kaigo_emp_name AS kaigo_creater_name,
rihabiri_jobname||' '||rihabiri_emp_name AS rihabiri_creater_name
FROM sum_med_std_team AS M
LEFT OUTER JOIN sum_med_priactice0 AS P0 ON M.team_id=P0.team_id 
LEFT OUTER JOIN sum_med_priactice1 AS P1 ON M.team_id=P1.team_id 
LEFT OUTER JOIN sum_med_priactice2 AS P2 ON M.team_id=P2.team_id 
LEFT OUTER JOIN sum_med_priactice3 AS P3 ON M.team_id=P3.team_id 
_SQL_END_;
    $cond = " WHERE M.team_id={$team_id}";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    $mst = pg_fetch_assoc($sel);
    $data = array_merge($data, $mst);
    
    //====================================
    // 観察項目を取得
    //====================================
    $sql  = "SELECT MST.item_cd , TMP.item_name AS name FROM sum_med_std_obse AS MST ";
    $sql .= "INNER JOIN tmplitem AS TMP ON TMP.item_cd=MST.item_cd AND TMP.mst_cd='A263' ";
    $sql .= " WHERE team_id={$team_id} ORDER BY MST.update ";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    $data['observation_items'] = pg_fetch_all($sel);

    //==========================================
    // 患者の問題及びアウトカムの更新記録リストを取得
    //==========================================
    $sql  = "SELECT update ,left_jobname||' '||left_emp_name AS name FROM sum_med_rec_left ";
    $sql .= " WHERE team_id=$team_id ORDER BY update DESC ";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $data['left_update'] = pg_fetch_all($sel);
    
    //====================================
    // チーム実践の更新記録リストを取得
    //====================================
    // 共用
    $sql = "SELECT update_c0 AS update ,jobname||' '||emp_name AS name FROM sum_med_recorder0 ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c0 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all0 = pg_fetch_all($sel);
    foreach ($center_rec_all0 as $value) {
        $center_update[0][] = array('name' => $value['name'], 'update' => $value['update']);
    }
    
    // 看護
    $sql = "SELECT update_c1 AS update,jobname||' '||emp_name AS name FROM sum_med_recorder1 AS M ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c1 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all1 = pg_fetch_all($sel);
    foreach ($center_rec_all1 as $value) {
        $center_update[1][] = array('name' => $value['name'], 'update' => $value['update']);
    }

    // 介護
    $sql = "SELECT update_c2 AS update,jobname||' '||emp_name AS name  FROM sum_med_recorder2 AS M ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c2 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all2 = pg_fetch_all($sel);
    foreach ($center_rec_all2 as $value) {
        $center_update[2][] = array('name' => $value['name'], 'update' => $value['update']);
    }

    // リハビリ
    $sql = "SELECT update_c3 AS update,jobname||' '||emp_name AS name FROM sum_med_recorder3 AS M ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c3 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all3 = pg_fetch_all($sel);
    foreach ($center_rec_all3 as $value) {
        $center_update[3][] = array('name' => $value['name'], 'update' => $value['update']);
    }
    
    $data['center_update'] = $center_update;
    
    //====================================
    // 評価コメントを取得
    //====================================
    $sql = "SELECT team_type,emp_id,update,comment,jobname||' '||emp_name AS name FROM sum_med_std_asses ";
    $sql .= " WHERE team_id=$team_id ORDER BY team_type , update DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $fetch_all = pg_fetch_all($sel);
    foreach ($fetch_all as $value) {
        $type = intval($value['team_type']);
        $ass_list[$type][] = array('update' => $value['update'], 'name' => $value['name'], 'comment' => $value['comment']);
    }
    $data['ass_list'] = $ass_list;
    
    return $data;
}

/**
 * ヘッダ部のセルを作成
 *
 * @param object $excelObj Excel生成インスタンス
 * @param array  $data     出力データ
 *
 * @return void
 */
function createHeaderCells($excelObj, $data)
{
    global $fontMSPmincho;

    // 患者情報
    $excelObj->SetValueJP2('A2', sprintf('患者ID:%s', $data['ptif_id']), '', $fontMSPmincho, 10);
    $excelObj->SetValueJP2('D2', sprintf('患者氏名:%s', $data['kname']), '', $fontMSPmincho, 10);

    // プロブレム
    $excelObj->SetValueJP2('A3', sprintf('プロブレム:%s', $data['problem']), '', $fontMSPmincho, 10);

    // 立案年月日
    $excelObj->SetValueJP2('A4', sprintf('立案年月日:%s', date("Y年m月d日", strtotime($data['create_date']))), '', $fontMSPmincho, 10);

    // 責任者
    $excelObj->SetValueJP2('D4', sprintf('責任者　職種:%s 氏名:%s', $data['emp_job'], $data['emp_name']), '', $fontMSPmincho, 10);
}

/**
 * 患者の問題及びアウトカムのセルを作成
 *
 * @param object $excelObj Excel生成インスタンス
 * @param array  $data     出力データ
 *
 * @return integer 最下行の行番号
 */
function createProblemAndOutcomeCells($excelObj, $data)
{
    global $fontMSPmincho;

    $rowNum = 6;
    
    $excelObj->CellMerge2("A{$rowNum}:D{$rowNum}", '', '', 'CCFFFF');
    $excelObj->SetValueJP2("A{$rowNum}", '患者の問題及びアウトカム', 'HCENTER BOLD', $fontMSPmincho, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■定義:%s\n%s", $data['problem'], replaceTagToBreak($data['def_value'])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■S:症状・兆候\n%s", replaceTagToBreak($data['s_detail'])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■E:原因・関連因子\n%s", replaceTagToBreak($data['e_detail'])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■危険因子\n%s", replaceTagToBreak($data['risk_detail'])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■アウトカム\n%s", replaceTagToBreak($data['outcome'])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;

    // 観察項目
    $startRow = $rowNum;
    $endRow = $rowNum + 1;
    $excelObj->CellMerge("A{$startRow}:D{$endRow}");
    $observation_items = '';
    foreach ($data['observation_items'] as $value) {
        if (!empty($observation_items)) {
            $observation_items .= "\n";
        }
        $observation_items .= $value['name'];
    }
    $excelObj->SetValueJP2("A{$startRow}", sprintf("■観察項目\n%s", $observation_items), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum = $endRow + 1;

    // 患者の問題及びアウトカムの更新記録
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $left_update = sprintf('記載者:%s', $data['left_creater_name']);
    foreach ($data['left_update'] as $value) {
        if (!empty($left_update)) {
            $left_update .= "\n";
        }
        $left_update .= sprintf("更新日:%s\n", $value['update']);
        $left_update .= sprintf('更新者:%s', $value['name']);
    }
    $excelObj->SetValueJP2("A{$rowNum}", $left_update, 'WRAP TOP', $fontMSPmincho, 10);

    return $rowNum;
}

/**
 * チーム実践のセルを作成
 * 
 * @param object  $excelObj    Excel生成インスタンス
 * @param array   $data        出力データ
 * @param string  $key         チーム区分のキーワード
 * @param integer $type        チーム区分
 * @param string  $caption     見出し
 * @param integer $startRowNum 開始行番号
 * 
 * @return integer 最下行の行番号
 */
function createTeamPracticeCells($excelObj, $data, $key, $type, $caption, $startRowNum)
{
    global $fontMSPmincho;
    
    $rowNum = $startRowNum;
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    // 拡張機能有効時のみ「共用」「看護」「介護」「リハビリ」を表示
//     $excelObj->SetValueJP2("E{$rowNum}", sprintf("＜{$caption}＞\n■O-P:観察計画\n%s", replaceTagToBreak($data["{$key}_op"])), 'WRAP TOP', $fontMSPmincho, 10);
    $excelObj->SetValueJP2("E{$rowNum}", sprintf("■O-P:観察計画\n%s", replaceTagToBreak($data["{$key}_op"])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    $excelObj->SetValueJP2("E{$rowNum}", sprintf("■T-P:実践計画\n%s", replaceTagToBreak($data["{$key}_tp"])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    $excelObj->SetValueJP2("E{$rowNum}", sprintf("■E-P:教育・指導計画\n%s", replaceTagToBreak($data["{$key}_ep"])), 'WRAP TOP', $fontMSPmincho, 10);
    $rowNum++;
    
    // 更新記録
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    $update = sprintf('記載者:%s', $data["{$key}_creater_name"]);
    if (array_key_exists($type, $data['center_update'])) {
        foreach ($data['center_update'][$type] as $value) {
            if (!empty($update)) {
                $update .= "\n";
            }
            $update .= sprintf("更新日:%s\n", $value['update']);
            $update .= sprintf('更新者:%s', $value['name']);
        }
    }
    $excelObj->SetValueJP2("E{$rowNum}", $update, 'WRAP TOP', $fontMSPmincho, 10);

    return $rowNum;
}

/**
 * 評価日／解決・継続／サインのセルを作成
 * 
 * @param object  $excelObj    Excel生成インスタンス
 * @param array   $ass_list    出力データ
 * @param string  $key         チーム区分のキーワード
 * @param integer $type        チーム区分
 * @param string  $caption     見出し
 * @param integer $startRowNum 開始行番号
 * 
 * @return integer 最下行の行番号
 */
function createAssessmentCells($excelObj, $ass_list, $key, $type, $caption, $startRowNum)
{
    global $fontMSPmincho;
    
    $rowNum = $startRowNum;
    // 拡張機能有効時のみ「共用」「看護」「介護」「リハビリ」を表示
//    $assessment = "＜{$caption}＞";
    if (array_key_exists($type, $ass_list)) {
        foreach ($ass_list[$type] as $value) {
            if (!empty($assessment)) {
                $assessment .= "\n";
            }
            $assessment .= sprintf("評価日:%s\n", date( "Y年m月d日", strtotime($value['update'])));
            $assessment .= sprintf("評価者:%s\n", $value['name']);
            $assessment .= sprintf("評価:%s", $value['comment']);
        }
    }
    $startRow = $rowNum;
    $endRow = $rowNum + 3;
    $excelObj->CellMerge("I{$startRow}:L{$endRow}");
    $excelObj->SetValueJP2("I{$startRow}", $assessment, 'WRAP TOP', $fontMSPmincho, 10);
    
    return $endRow;
}

/**
 * チーム実践データの存在判定
 * 
 * @param array  $data 出力データ
 * @param string $key  チーム区分のキーワード
 * 
 * @return boolean OP、TP、EPのうち、少なくとも1つはデータが存在する:true/OP、TP、EPの全データが存在しない:false
 */
function isTeamPracticeDataExist($data, $key)
{
    if (empty($data["{$key}_op"]) && empty($data["{$key}_tp"])  && empty($data["{$key}_ep"])) {
        return false;
    } else {
        return true;
    }
}

/**
 * 文字列内の<BR>タグを改行コードに変換する
 * 
 * @param string $string 変換対象文字列
 * 
 * @return string 変換した文字列
 */
function replaceTagToBreak($string)
{
    return preg_replace('/<BR>/i', "\n", $string);
}
?>
