<?
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once('Cmx/Model/SystemConfig.php');

require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require_once("get_values.php");
require_once("smarty_setting.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_wf_utils.php");
require_once("hiyari_common.ini");
require_once("holiday.php");

//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0"){
    showLoginPage();
    exit;
}

// 権限チェック
$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0"){
    showLoginPage();
    exit;
}

// 患者参照権限を取得
$pt_auth = check_authority($session, 15, $fname);

//DBコネクション
$con = connect2db($fname);
if ($con == "0"){
    showErrorPage();
    exit;
}


//権限設定

// セッション開始
session_name("hyr_sid");
session_start();


//評価予定期日の表示の有無
$sysConf = new Cmx_SystemConfig();
$predetermined_eval = $sysConf->get('fantol.predetermined.eval');


//    // 戻る
//    if ( $_POST['back'] != '' ){
//
//        // ページ遷移する
//        redirectPage( "hiyari_easyinput_create.php?session=$session&" . SID );
//    }
//    // 登録
//    if ( $_POST['register'] != '' ){
//
//        // ページ遷移する
//        redirectPage( "hiyari_easyinput_register.php?session=$session&" . SID );
//    }
//    // フォーム入力無し（初回アクセス）
//    else {

    //呼び元画面より送信情報を取得
    $design_mode = $_POST['design_mode'];
    $style_code = $_POST['style_code'];
    $use_grp_code = $_POST['use_grp_code'];
    $must_grp_code = $_POST['must_grp_code'];

    $disp_setting = get_disp_setting($con,$fname);

    //報告ファイルの表示設定を取得
    $disp_setting = get_disp_setting($con,$fname);
    $must_item_disp_mode = $disp_setting["must_item_disp_mode"];

    //テーマ
    $theme = $_POST["this_term_theme"];

    //概要必須項目
    $item_must_2010_easy_item_code = $_POST["item_must_2010_easy_item_code"];
    $arr_2010_must = array();
    foreach ($item_must_2010_easy_item_code as $must_easy_item_code) {
        $arr_2010_must[] = "900_" . $must_easy_item_code;
    }

    //文言変更
    $eis_item_title=array();
    foreach ($_POST as $key=>$item_title){
        if (preg_match("/^name_/",$key)) {
            $title_no =explode("_", $key);
            if ($title_no[1]) {
                $eis_item_title[$title_no[1]] = $item_title;
            }
        }
    }

    $place_use_easy_item_code = $_POST["place_use_easy_item_code"];
    $is_110_65_use = false;
    if (count($place_use_easy_item_code) > 0){
        $use_grp_code[] = "110";

        if (in_array("65", $place_use_easy_item_code)) $is_110_65_use = true;
    }

    $patient_use_easy_item_code = $_POST['patient_use_easy_item_code'];
    $patient_must_easy_item_code = $_POST['patient_must_easy_item_code'];
    if(count($patient_use_easy_item_code) > 0)
    {
        $use_grp_code[] = "210";
    }

    $arr_patient_must = array();
    foreach($patient_must_easy_item_code as $must_easy_item_code)
    {
        $arr_patient_must[] = "210_".$must_easy_item_code;
    }

    $asses_use_easy_item_code = $_POST['asses_use_easy_item_code'];
    $asses_must_easy_item_code = $_POST['asses_must_easy_item_code'];
    if(count($asses_use_easy_item_code) > 0)
    {
        $use_grp_code[] = "140";
    }

    $arr_asses_must = array();
    foreach($asses_must_easy_item_code as $must_easy_item_code)
    {
        $arr_asses_must[] = "150_".$must_easy_item_code;
    }


    $summary_use_item_grp_code = $_POST['summary_use_item_grp_code'];
    $summary_must_item_grp_code = $_POST['summary_must_item_grp_code'];
    $rep_obj = new hiyari_report_class($con, $fname);
    $conv_table_eis_400 = $rep_obj->get_conv_table_eis_400();
    if(count($summary_use_item_grp_code) > 0)
    {
        $use_grp_code[] = "400";
    }

    $eis_400s_use = "";
    foreach($summary_use_item_grp_code as $item_grp)
    {
        foreach($conv_table_eis_400[$item_grp] as $grp_code => $item_list)
        {
            foreach($item_list as $item_code => $mustable)
            {
                $eis_400s_use[$grp_code][] = $item_code;
            }
        }
    }
    $arr_400_must = array();
    foreach($summary_must_item_grp_code as $item_grp)
    {
        foreach($conv_table_eis_400[$item_grp] as $grp_code => $item_list)
        {
            foreach($item_list as $item_code => $mustable)
            {
                if($mustable)
                {
                    $arr_400_must[] = "{$grp_code}_{$item_code}";
                }
            }
        }
    }

    // ログインユーザのプロフィールを取得
    $emp_id = get_emp_id($con, $session, $fname);
    $profile = get_profile($con, $emp_id, $fname);
    $emp_class_nm = get_emp_profile_class_name($profile, get_inci_profile($fname, $con));
    $experience = get_experience($profile);

    // 「プロフィールからコピー」ボタン表示セルのrowspanを算出
    $profile_rowspan = get_profile_rowspan($use_grp_code);

    //テンプレートに値を設定し、表示
    $smarty = new Cmx_View();

    //デフォルト値 20150325
     foreach ($_POST as $post_key=>$post_item){
         $tmp_post = strpos($post_key, "def_");
         if ($tmp_post !== false) {
             $arr_tmp_post = explode("_", $post_key);
             if (count($arr_tmp_post) == 3) {
                 $vals[$arr_tmp_post[1]][$arr_tmp_post[2]][] = $post_item;
             }
         }
     }
    $smarty->assign("vals", $vals);

    //報告書デザインとスタイル
    $smarty->assign("design_mode", $design_mode);
    $smarty->assign("style_code", $style_code);

    $smarty->assign("ptreg", 1);
    $smarty->assign("session", $session);
    $smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
    $smarty->assign("pt_auth", $pt_auth);
    $smarty->assign("profile", $profile);
    $smarty->assign("emp_class_nm", $emp_class_nm);
    $smarty->assign("experience", $experience);
    $smarty->assign("profile_rowspan", $profile_rowspan);
    $smarty->assign("hyr_sid", session_id());

    //日時モード
    $smarty->assign("time_setting_flg", getTimeSetting($con, $PHP_SELF));

    //必須項目の表示設定
    $smarty->assign("must_item_disp_mode", $must_item_disp_mode);

    //今期のテーマ
    $smarty->assign("theme", $theme);

    //文言変更
    $smarty->assign("eis_item_title", $eis_item_title);

    $smarty->assign("is_110_65_use", $is_110_65_use);

    // インクルードテンプレート用設定
    $rep_obj = new hiyari_report_class($con, $fname);
    $grps = $rep_obj->get_all_easy_items();

    //必須項目
    $item_must = $rep_obj->get_item_must_for_preview($must_grp_code);

    $item_must = array_merge($item_must, $arr_patient_must);
    $item_must = array_merge($item_must, $arr_400_must);
    $item_must = array_merge($item_must, $arr_asses_must);
    $item_must = array_merge($item_must, $arr_2010_must);

    // 発生した場面と内容に関する情報
    $method = $rep_obj->get_all_super_item();
    unset($grps[700]['easy_item_list'][10]['easy_list']);

    foreach($method as $key => $value) {
        $grps[700]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
        $grps[700]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[710]['easy_item_list'][10]['easy_list']);
    unset($grps[720]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_scene($value['super_item_id']);
        $scene[$value['super_item_id']] = $param;

        foreach($param as $value2) {
            $scene_item[$value2['item_id']] = $rep_obj->get_scene_item($value2['item_id']);
        }
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[740]['easy_item_list'][10]['easy_list']);
    unset($grps[750]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_kind($value['super_item_id']);
        $kind[$value['super_item_id']] = $param;

        foreach($param as $value2) {
            $kind_item[$value2['item_id']] = $rep_obj->get_kind_item($value2['item_id']);
        }
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[770]['easy_item_list'][10]['easy_list']);
    unset($grps[780]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_content($value['super_item_id']);
        $content[$value['super_item_id']] = $param;

        foreach($param as $value2) {
            $content_item[$value2['item_id']] = $rep_obj->get_content_item($value2['item_id']);
        }
    }

    // 患者の影響・対応
    if(array_key_exists(1400, $grps)) {
        $method = $rep_obj->get_all_ic_super_item();
        unset($grps[1400]['easy_item_list'][10]['easy_list']);

        foreach($method as $key => $value) {
            $grps[1400]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
            $grps[1400]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
        }

        // 発生した場面のデフォルト値を削除
        unset($grps[1400]['easy_item_list'][30]['easy_list']);

        foreach($method as $value) {
            $param = $rep_obj->get_ic_items($value['super_item_id'], 'influence');
            if ($param) $ic_influence[$value['super_item_id']] = $param;
        }

        // 発生した場面のデフォルト値を削除
        unset($grps[1400]['easy_item_list'][50]['easy_list']);

        foreach($method as $value) {
            $param = $rep_obj->get_ic_items($value['super_item_id'], 'mental');
            if ($param) $ic_mental[$value['super_item_id']] = $param;
        }
    }
    if(array_key_exists(1410, $grps)) {
        $method = $rep_obj->get_all_ic_super_item();
        unset($grps[1410]['easy_item_list'][10]['easy_list']);

        foreach($method as $key => $value) {
            $grps[1410]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
            $grps[1410]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
        }

        // 発生した場面のデフォルト値を削除
        unset($grps[1410]['easy_item_list'][20]['easy_list']);

        foreach($method as $value) {
            $param = $rep_obj->get_ic_items($value['super_item_id'], 'correspondence');
            if ($param) $ic_correspondence[$value['super_item_id']] = $param;
        }

        foreach($method as $value) {
            $params = $rep_obj->get_ic_items($value['super_item_id'], 'correspondence');
            if (is_array($params)){
                foreach($params as $param) {
                    $ic_parent_item[$param['item_id']] = $param['item_name'];
                }
            }
        }

        // 発生した場面のデフォルト値を削除
        unset($grps[1410]['easy_item_list'][30]['easy_list']);
        foreach($ic_correspondence as $value) {
            if (is_array($value)){
                foreach($value as $val) {
                    $param = $rep_obj->get_ic_sub_items($val['item_id'], 'correspondence');
                    if (is_array($param)){
                        foreach($param as $value2) {
                            $ic_sub_item[$value2['super_item_id']][$value2['item_id']] = $param;
                        }
                    }
                }
            }
        }
    }

    // 発生場面と内容
    $smarty->assign("scene", $scene);
    $smarty->assign("scene_item", $scene_item);
    $smarty->assign("kind", $kind);
    $smarty->assign("kind_item", $kind_item);
    $smarty->assign("content", $content);
    $smarty->assign("content_item", $content_item);

    // 影響と対応
    $smarty->assign("ic_influence", $ic_influence);
    $smarty->assign("ic_mental", $ic_mental);
    $smarty->assign("ic_correspondence", $ic_correspondence);
    $smarty->assign("ic_sub_item", $ic_sub_item);
    $smarty->assign("ic_parent_item", $ic_parent_item);

    // 概要2010年版
    if(array_key_exists(900, $grps)) {
        $method = $rep_obj->get_all_super_item_2010();
        unset($grps[900]['easy_item_list'][10]['easy_list']);

        foreach($method as $key => $value) {
            $grps[900]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
            $grps[900]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
            $grps[900]['easy_item_list'][10]['easy_list'][$key]['export_code'] = $value['export_item_id'];
            $grps[900]['easy_item_list'][10]['easy_list'][$key]['other_code'] = $value['other_code'];
        }

        // 発生した場面のデフォルト値を削除
        unset($grps[910]['easy_item_list'][10]['easy_list']);
        unset($grps[920]['easy_item_list'][10]['easy_list']);

        foreach($method as $value) {
            $param = $rep_obj->get_scene_2010($value['super_item_id']);
            if ($param) $scene_2010[$value['super_item_id']] = $param;
            if (is_array($param)){
                foreach($param as $value2) {
                    $param2 = $rep_obj->get_scene_item_2010($value2['item_id']);
                    if ($param2) $scene_item_2010[$value2['item_id']] = $param2;
                }
            }
        }

        // 発生した場面のデフォルト値を削除
        unset($grps[940]['easy_item_list'][10]['easy_list']);
        unset($grps[950]['easy_item_list'][10]['easy_list']);

        foreach($method as $value) {
            $param = $rep_obj->get_kind_2010($value['super_item_id']);
            if ($param) $kind_2010[$value['super_item_id']] = $param;
            if (is_array($param)){
                foreach($param as $value2) {
                    $param2 = $rep_obj->get_kind_item_2010($value2['item_id']);
                    if ($param2) $kind_item_2010[$value2['item_id']] = $param2;
                }
            }
        }

        // 発生した場面のデフォルト値を削除
        unset($grps[970]['easy_item_list'][10]['easy_list']);
        unset($grps[980]['easy_item_list'][10]['easy_list']);

        foreach($method as $value) {
            $param = $rep_obj->get_content_2010($value['super_item_id']);
            if ($param) $content_2010[$value['super_item_id']] = $param;
            if (is_array($param)){
                foreach($param as $value2) {
                    $param2 = $rep_obj->get_content_item_2010($value2['item_id']);
                    if ($param2) $content_item_2010[$value2['item_id']] = $param2;
                }
            }
        }

    }
    // 概要2010年版
    if ($design_mode == 1){
        $smarty->assign("kind_2010", $kind_2010);
        $smarty->assign("kind_item_2010", $kind_item_2010);
        $smarty->assign("scene_2010", $scene_2010);
        $smarty->assign("scene_item_2010", $scene_item_2010);
        $smarty->assign("content_2010", $content_2010);
        $smarty->assign("content_item_2010", $content_item_2010);
    }
    else{
        $gaiyo_selection = array();
        $gaiyo_selection[910]['list'] = $kind_2010;
        $gaiyo_selection[920]['list'] = $kind_item_2010;
        $gaiyo_selection[940]['list'] = $scene_2010;
        $gaiyo_selection[950]['list'] = $scene_item_2010;
        $gaiyo_selection[970]['list'] = $content_2010;
        $gaiyo_selection[980]['list'] = $content_item_2010;
        $smarty->assign("gaiyo_selection", $gaiyo_selection);
    }

    // コントロールタイプを変更
    if ($style_code == 2){
        $rep_obj->change_easy_item_type($grps);
    }

    // ユーザー定義カテゴリ項目
    $smarty->assign("user_cate_name", $user_cate_name);
    $use_user_grp_code = array();
    foreach ($use_grp_code as $grp_code){
        if ($grp_code >= 10000){
            $use_user_grp_code[] = $grp_code;
        }
    }
    if ($design_mode == 2){
        //$tabletypes = get_table_type($grps, $use_user_grp_code);
        $tabletypes = array();
        foreach($use_user_grp_code as $grp_code) {
            foreach($grps[$grp_code]['easy_item_list'] as $easy_item_code => $easy_item) {
                $max_len = 0;
                $tabletype = null;
                foreach($easy_item['easy_list'] as $easy) {
                    if ($max_len < mb_strlen($easy['easy_name'])){
                        $max_len = mb_strlen($easy['easy_name']);
                    }
                }
                if ($max_len < 10){
                    $tabletype = 'tabletype03';
                }
                if ($tabletype != null){
                    $tabletypes[$grp_code.'_'.$easy_item_code] = $tabletype;
                }
            }
/*            foreach($method as $key => $value) {
                $grps[$grp_code]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
                $grps[$grp_code]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
            }
*/            
        }
        
        $smarty->assign("tabletypes", $tabletypes);
    }

    $smarty->assign("grps", $grps);
    $smarty->assign("grp_flags", $use_grp_code);
    $smarty->assign("user_grp_flags", $use_user_grp_code);
    $smarty->assign("item_must", $item_must);

    $smarty->assign("patient_use", $patient_use_easy_item_code);
    $smarty->assign("eis_400_use", $eis_400s_use[400]);
    $smarty->assign("eis_410_use", $eis_400s_use[410]);
    $smarty->assign("eis_420_use", $eis_400s_use[420]);
    $smarty->assign("eis_430_use", $eis_400s_use[430]);
    $smarty->assign("eis_440_use", $eis_400s_use[440]);
    $smarty->assign("eis_450_use", $eis_400s_use[450]);
    $smarty->assign("eis_460_use", $eis_400s_use[460]);
    $smarty->assign("eis_470_use", $eis_400s_use[470]);
    $smarty->assign("eis_480_use", $eis_400s_use[480]);
    $smarty->assign("eis_490_use", $eis_400s_use[490]);

    $smarty->assign("asses_use", $asses_use_easy_item_code);
    //アセスメントのコメント
    $arr_str = array();
    if (in_array(10, $asses_use_easy_item_code)) {
        $arr_str[] = "転倒・転落";
    }
    if (in_array(20, $asses_use_easy_item_code)) {
        $arr_str[] = "ルート関連";
    }
    $asses_comment = join("、", $arr_str);
    $smarty->assign("asses_comment", $asses_comment);

    $smarty->assign("rel_grps", $rep_obj->get_relate_item_code_group());
    $smarty->assign("input_nocheck", 1);
    $smarty->assign("regist_date_y", date("Y"));
    $smarty->assign("regist_date_m", date("m"));
    $smarty->assign("regist_date_d", date("d"));

    //インシレベルに対する説明文
    $level_infos = $rep_obj->get_inci_level_infos();
    $smarty->assign("level_infos", $level_infos);

    //「今日ボタン」関連
    $this_ymd = date('Ymd');
    $this_month = substr($this_ymd, 4, 2);
    $week_arr = array(0=>'7',1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6');
    $day = mktime(0,0,0,date('m'), date('d'), date('Y'));
    $day_week = date('w', $day);
    $week_div = ($week_arr[$day_week] == 6 || $week_arr[$day_week] == 7 || get_holiday_name($this_ymd) != "") ? "2" : "1";

    $smarty->assign("this_ymd", date('Y/m/d')); // 本日日付
    $smarty->assign("this_month", intval($this_month)); // 月
    $smarty->assign("day_week", $week_arr[$day_week]); // 曜日
    $smarty->assign("week_div", $week_div); // 土・日・祝日 : 2  それ以外 : 1

    //DBアクセス情報
    $smarty->assign("con", $con);
    $smarty->assign("fname", $fname);

    //---------------------------------------
    //新デザイン用
    //---------------------------------------
    if ($design_mode == 2){
        //表示グループ（テーブル）の最後の項目フラグ
        $smarty->assign('last_item', $rep_obj->get_last_item());

        //ラジオボタン・チェックボックスの選択肢から非表示項目を抜いたリスト
        $smarty->assign('selection', $rep_obj->get_item_selection($grps));

        // 概要2010年版
        $gaiyo_selection = array();
        $gaiyo_selection[910]['list'] = $kind_2010;
        $gaiyo_selection[910]['col_num'] = 4;
        $gaiyo_selection[910]['item_width'] = '25%';

        $gaiyo_selection[920]['list'] = $kind_item_2010;
        $gaiyo_selection[920]['col_num'] = 2;
        $gaiyo_selection[920]['item_width'] = '50%';

        $gaiyo_selection[940]['list'] = $scene_2010;
        $gaiyo_selection[940]['col_num'] = 4;
        $gaiyo_selection[940]['item_width'] = '25%';

        $gaiyo_selection[950]['list'] = $scene_item_2010;
        $gaiyo_selection[950]['col_num'] = 2;
        $gaiyo_selection[950]['item_width'] = '50%';

        $gaiyo_selection[970]['list'] = $content_2010;
        $gaiyo_selection[970]['col_num'] = 4;
        $gaiyo_selection[970]['item_width'] = '25%';

        $gaiyo_selection[980]['list'] = $content_item_2010;
        $gaiyo_selection[980]['col_num'] = 2;
        $gaiyo_selection[980]['item_width'] = '50%';

        $smarty->assign("gaiyo_selection", $gaiyo_selection);
    }

    $smarty->assign(predetermined_eval,$predetermined_eval);

    if ($design_mode == 1){
        $smarty->display("hiyari_easyinput_preview.tpl");
    }
    else{
        $smarty->display("hiyari_easyinput_preview2.tpl");
    }

//    }


//DB切断
pg_close($con);

// 実行終了
exit;

?>
