<?php
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("get_values.php");
require_once("hiyari_db_class.php");
require_once("hiyari_common.ini");
ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
if ( !$session || !$session_auth || !$con ) {
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

$analysis_id = (int)$_REQUEST["a_id"];
$print_chart_data = $_REQUEST["print_chart_data"];
$chart_seq = (int)$_REQUEST["chart_seq"];
$swap_chart_seq = (int)$_REQUEST["swap_chart_seq"];
$swap_cliplist_seq = (int)$_REQUEST["swap_cliplist_seq"];
$self_unlock = ($_REQUEST["self_unlock"] == "yes" ? true : false);
$inga_mode = (@$_REQUEST["inga_mode"] ? "yes" : ""); // "yes" or empty
$renew_inga_mode = (@$_REQUEST["renew_inga_mode"] ? "yes" : ""); // "yes" or empty

// member_idで編集可能かcheckする
$login_emp_id  = get_emp_id($con,$session,$fname);


// 自分の編集ロックを解除して、ウィンドウを閉じる
if ($self_unlock){
    $sql = "update inci_analysis_rca_chart set editing_emp_id = '' where editing_emp_id = '".pg_escape_string($login_emp_id)."' and is_inga_mode = '".$inga_mode."'";
    update_set_table($con, $sql, array(), null, "", $fname);
  echo("<html><head><script type='text/javascript'>window.close();</script></head></html>");
  die;
}


if ($analysis_id || $chart_seq) {

    // 分析IDがあるが、チャートIDがない場合は、いったんチャートIDを取得
    if ($analysis_id && !$chart_seq){
        $sel = select_from_table($con, "select max(chart_seq) from inci_analysis_rca_chart where delete_flg = 'f' and analysis_id = ".$analysis_id." and is_inga_mode = '".$inga_mode."'", "", $fname);
        $chart_seq = (int)pg_fetch_result($sel, 0, 0);
    }

    // チャートIDがある場合は、チャート要素のカウントを取得
    $chart_item_count = 0;
    if ($chart_seq){
        $sel = select_from_table($con, "select chart_item_count from inci_analysis_rca_chart where delete_flg = 'f' and chart_seq = ".$chart_seq." and is_inga_mode = '".$inga_mode."'", "", $fname);
        $chart_item_count = (int)pg_fetch_result($sel, 0, 0);
    }

    // チャートIDがない場合は新規である。チャート要素カウントがない場合は更新する。
    if (($analysis_id && !$chart_seq) || !$chart_item_count){

        //==================================
        // 初期データの作成
        //==================================
        $sel = select_from_table($con, "select analysis_phenomenon from inci_analysis_rca_date where analysis_id = ".$analysis_id . " order by analysis_order", "", $fname);
        $chart_base_data = pg_fetch_all($sel);

        // RCA基本データがない場合は、ダミーで空要素を追加
        if (!is_array($chart_base_data) || !count($chart_base_data)){
            $chart_base_data = array();
            $chart_base_data[0] = array("boxtype" => "", "analysis_phenomenon" => "");
        }
        // RCA基本データ件数が1件のときは、末尾に結果要素を追加、これで最低２レコードとなった。
        if (count($chart_base_data)==1){
            $chart_base_data[1] = array("boxtype" => "", "analysis_phenomenon" => "");
        }

        $chart_base_data_js = array();
        $ttlcnt = count($chart_base_data);
        $cnt = 0;
        foreach($chart_base_data as $idx => $row){
            $cnt++;
            $af = str_replace('"', '\"', $row["analysis_phenomenon"]);
            if ($cnt<$ttlcnt){
                $chart_base_data_js[] = '"1_'.($idx+1).'":{"boxtype":"工程", "text":"'.$af.'", "dest":["1_'.($idx+2).'"]}';
            } else {
                $chart_base_data_js[] = '"1_'.($idx+1).'":{"boxtype":"結果", "text":"'.$af.'"}';
            }
        }

        if (!$chart_seq){
            $sel = select_from_table($con, "select nextval('inci_analysis_rca_chart_seq')", "", $fname);
            $chart_seq = (int)pg_fetch_result($sel, 0, 0);
            $sql =
                " insert into inci_analysis_rca_chart (".
                " chart_seq, analysis_no, analysis_id, update_ymdhms, chart_data, delete_flg, is_inga_mode".
                ") values (".
                " " . $chart_seq.
                ",'" . pg_escape_string($analysis_no). "'".
                ","  . $analysis_id.
                ",'" . Date("YmdHis"). "'".
                ",'" . pg_escape_string(implode($chart_base_data_js,","))."'".
                ",'f'".
                ",'".$inga_mode."'".
                ")";
        } else {
            $sql =
                " update inci_analysis_rca_chart set".
                " chart_data = '".pg_escape_string(implode($chart_base_data_js,","))."'".
                ",update_ymdhms = '".Date("YmdHis")."'".
                " where chart_seq = " . $chart_seq.
                " and delete_flg = 'f'".
                " and is_inga_mode = '".$inga_mode."'";
        }
        update_set_table($con, $sql, array(), null, "", $fname);
    }

    // 分析IDがある場合は、分析テーブルからタイトルを取得する
    // 分析IDがない場合は、チャートテーブルからテンプレートタイトルを取得
    if ($analysis_id){
        $sel = select_from_table($con, "select analysis_no, analysis_title from inci_analysis_regist where analysis_id = ".$analysis_id, "", $fname);
        $analysis_no = @pg_fetch_result($sel, 0, 0);
        $title = @pg_fetch_result($sel, 0, 1);
    }
    else {
        $sel = select_from_table($con, "select analysis_no, template_title from inci_analysis_rca_chart where delete_flg = 'f' and chart_seq = ".$chart_seq." and is_inga_mode = '".$inga_mode."'", "", $fname);
        $analysis_no = @pg_fetch_result($sel, 0, 0);
        $title = @pg_fetch_result($sel, 0, 1);
    }

    // 自分の編集ロックをすべて解除
    $sql =
        " update inci_analysis_rca_chart set".
        " editing_emp_id = ''".
        " where editing_emp_id = '".pg_escape_string($login_emp_id)."'";
    update_set_table($con, $sql, array(), null, "", $fname);

    // 編集ロック
    $sql =
        " update inci_analysis_rca_chart set".
        " editing_emp_id = '".pg_escape_string($login_emp_id)."'".
        " where chart_seq = ". $chart_seq.
        " and delete_flg = 'f'".
        " and (editing_emp_id is null or editing_emp_id = '')".
        " and is_inga_mode = '".$inga_mode."'";
    update_set_table($con, $sql, array(), null, "", $fname);

    // チャートデータと、クリップリストデータを取得する
    $sel = select_from_table($con, "select * from inci_analysis_rca_chart where chart_seq = ".$chart_seq." and delete_flg = 'f' and is_inga_mode = '".$inga_mode."'", "", $fname);
    $rs_rca_chart = pg_fetch_array($sel);
    $chart_seq = $rs_rca_chart["chart_seq"];
    $editing_emp_id = $rs_rca_chart["editing_emp_id"];

    // ただし因果図の場合は、保存データがないか、リニューアル指令で、
    // 通常図のチャートデータとクリップリストを取得する
    $ingaDataSwapFromNormalData = "";
    if ((!$chart_item_count || $renew_inga_mode) && $inga_mode && $analysis_id){
        $sel = select_from_table($con, "select max(chart_seq) from inci_analysis_rca_chart where delete_flg = 'f' and analysis_id = ".$analysis_id." and is_inga_mode = '' and chart_item_count > 0", "", $fname);
        $tmp_chart_seq = (int)@pg_fetch_result($sel, 0, 0);
        if ($tmp_chart_seq) {
            $sel = select_from_table($con, "select chart_data, cliplist_data from inci_analysis_rca_chart where chart_seq = ".$tmp_chart_seq, "", $fname);
            $rs_rca_chart["chart_data"] = pg_fetch_result($sel, 0, "chart_data");
            $rs_rca_chart["cliplist_data"] = pg_fetch_result($sel, 0, "cliplist_data");
            $ingaDataSwapFromNormalData = "yes";
        }
    }

    // 編集ロック者名を取得
    $sel = select_from_table($con, "select emp_lt_nm || ' ' || emp_ft_nm from empmst where emp_id = '".pg_escape_string($editing_emp_id)."'", "", $fname);
    $editing_emp_name = @pg_fetch_result($sel, 0, 0);
}

// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);

// member_idで編集可能かcheckする
$emp_id  = get_emp_id($con, $session, $fname);

$sql = "SELECT analysis_member_id FROM inci_analysis_regist WHERE analysis_id = {$analysis_id}";
$db->query($sql);
$analysis_member_id = $db->getOne();

$arr_member_id = explode(",", $analysis_member_id);
$update_flg = false;
foreach ( $arr_member_id as $member_id ) {
    if ($member_id == $emp_id) {
        $update_flg = true;
    }
}

if ($update_flg == false) {
    $update_flg = is_sm_emp($session, $fname, $con);
}

/*
$rs_rca_chart = array();
$analysis_id = "1";
$analysis_no = "ABCDEFG-HIJKL";
$chart_seq = "0";
$title = "";
$rs_rca_chart["template_title"] = "";
$rs_rca_chart["environment_data"] = "";
$session = "";
$rs_rca_chart["chart_data"] = '';
$rs_rca_chart["cliplist_data"] = '';
*/

function escape_crlf($str){
    return str_replace("\r", "", str_replace("\n", "\\n", trim($str)));
}
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp"/>

<script type="text/javascript">
var ingaMode = '<?=$inga_mode?>';
var ingaDataSwapFromNormalData = "<?=$ingaDataSwapFromNormalData?>";
var analysisId = <?=$analysis_id?>;
var analysisNo = "<?=$analysis_no?>";
var chartSeq = "<?=$chart_seq?>";
var rcaMap = {<?=escape_crlf($rs_rca_chart["chart_data"])?>};
var clipInitData = {<?=escape_crlf($rs_rca_chart["cliplist_data"])?>};
var templateTitle = "<?=escape_crlf(@$rs_rca_chart["template_title"])?>";
var envData = "<?=escape_crlf(@$rs_rca_chart["environment_data"])?>";


// zIndex z-index
//  通常：500番以降
//  マーキー：590番
//  ヘッダメニュー：600番以降
//  cliplist：700番
//  ベース：800番
//  ライン：1000番
//  ボックス：2000番
//  メニューボタンコンテナ：3000番
//  リサイズボタン：3010番
//  テキストエディット：4000番
//  クリップリスト：5000番
//  リサイズボーダー：6000番
//  印刷オプション：7000番

var BOX_BEHAVIOR = {
    "工程":{"stocktag":"Y","bgcolor":"#ff7","namecolor":"#8b8f36","tacolor":"#f0f0aa","bgimage":"url(inci/rca/img/bg_box_yellow.png)",
                    "borderL":"1px solid #eeee99","borderR":"1px solid #c7cb2e","borderT":"1px solid #f0f0aa","borderB":"1px solid #8b8f36", "divboxClass":"boxdiv_koutei"},
    "結果":{"stocktag":"G","bgcolor":"#ccc","namecolor":"#777777","tacolor":"#ddd","bgimage":"url(inci/rca/img/bg_box_gray.png)",
                    "borderL":"1px solid #bbbbbb","borderR":"1px solid #999999","borderT":"1px solid #eee","borderB":"1px solid #777777", "divboxClass":"boxdiv_kekka"},
    "質問":{"stocktag":"B","bgcolor":"#7bf","namecolor":"#366ca3","tacolor":"#9bc8ff","bgimage":"url(inci/rca/img/bg_box_blue.png)",
                    "borderL":"1px solid #4b9ae9","borderR":"1px solid #508ac5","borderT":"1px solid #9bc8ff","borderB":"1px solid #366ca3", "divboxClass":"boxdiv_situmon"},
    "原因":{"stocktag":"R","bgcolor":"#f9c","namecolor":"#964776","tacolor":"#ffc4dd","bgimage":"url(inci/rca/img/bg_box_red.png)",
                    "borderL":"1px solid #ea84b7","borderR":"1px solid #bd6096","borderT":"1px solid #ffc4dd","borderB":"1px solid #964776", "divboxClass":"boxdiv_genin"},
    "根本原因":{"stocktag":"V","bgcolor":"#f87","namecolor":"#8f5143","tacolor":"#ffbab0","bgimage":"url(inci/rca/img/bg_box_orange.png)",
                    "borderL":"1px solid #d97762","borderR":"1px solid #bc5b43","borderT":"1px solid #ffbab0","borderB":"1px solid #8f5143", "divboxClass":"boxdiv_konpon"},
    "パス":{"bgcolor":"transparent","namecolor":"#6a6a6a","tacolor":"#fff","bgimage":"",
                    "borderL":"0","borderR":"0","borderT":"0","borderB":"0", "divboxClass":"boxdiv_pass"},
    "白白":{"bgcolor":"transparent", "bgimage":"url(inci/rca/img/bg_preview1.gif?a=b)",
                    "borderL":"1px solid #fff","borderR":"1px solid #777","borderT":"1px solid #fff","borderB":"1px solid #777"},
    "白箱":{"bgcolor":"#fff", "bgimage":"url(inci/rca/img/bg_preview2.gif?a=b)",
                    "borderL":"1px solid #fff","borderR":"1px solid #777","borderT":"1px solid #fff","borderB":"1px solid #777"},
    "":      {"bgcolor":"transparent", "bgimage":"",
                    "borderL":"0","borderR":"0","borderT":"0","borderB":"0"}
};

var MAP_PADDING = 20;         <? // 上と左のパディングサイズ ?>
var BOX_DEFAULT_WIDTH = 128;  <? // 要素のデフォルト幅 ?>
var BOX_DEFAULT_HEIGHT = 64;  <? // 要素のデフォルト高 ?>
var BOX_MIN_HEIGHT = 24;      <? // 要素の最小高 ?>
var BOX_MIN_WIDTH = 96;       <? // 要素の最小幅 ?>
var BOX_MAX_HEIGHT = 512;     <? // 要素の最大高 ?>
var BOX_MAX_WIDTH = 512;      <? // 要素の最大幅 ?>
var MAX_YX_ENTRY_LENGTH = 250; <? // 要素を左または下に並べられる最大値の目安 ?>

<? // 矢印線の判定ビットコード：変更不可 ?>
var HORZ_L_UPPER = 16;
var HORZ_R_UPPER = 32;
var HORZ_L_UNDER = 64;
var HORZ_R_UNDER = 128;
var VERT_UPPER   = 256;
var VERT_CENTER  = 512;
var VERT_UNDER   = 1024;
var ARROW_RIGHT  = 2048;
var ARROW_DOWN   = 4096;
var ARROW_LEFT   = 8192;
var ARROW_UP     = 16384;

var mapWorld, cliplist, undo, textEditor, joinner, resizer, toolmenu, cond, lineRouteCalculator, printOption;


<? //------------------------------------------------------------------------------ ?>
<? // 汎用                                                                          ?>
<? //------------------------------------------------------------------------------ ?>
function px(x) { return  String(x) + "px"; }
function int(x) { if (isNaN(x)) return 0; return parseInt(x);}
function getHeight(elem){ return elem.offsetHeight || elem.clientHeight; }
function getWidth(elem){ return elem.offsetWidth || elem.clientWidth; }
function isOneOf(obj, list) { for (var l in list) if (list[l]===obj) return true; return false;  }

function plus1(n){ return parseInt(n*1+1*1); }
function YX(y, x) { return y+"_"+x; }
function getyx(rcaid) { var yx = rcaid.split("_"); return {"y":int(yx[0]), "x":int(yx[1])}; }

function getClientHeight(){
    if (document.body.clientHeight) return document.body.clientHeight;
    if (window.innerHeight) return window.innerHeight;
    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }
    return 0;
}

function getClientWidth(){
    if (document.body.clientWidth) return document.body.clientWidth;
    if (window.innerWidth) return window.innerWidth;
    if (document.documentElement && document.documentElement.clientWidth) {
        return document.documentElement.clientWidth;
    }
    return 0;
}

var ajaxObj = null;
function sendRequest(postval, url, receiver){
    if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
    else {
        try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
        catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
    }
  ajaxObj.onreadystatechange = receiver;
  ajaxObj.open("POST", url, true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval);
}

function btnHover(b) {
    b.style.border='1px solid #ea778b';
    toolmenu.currentHoveringId = b.id;
};

function btnBlur(b) {
    b.style.border='1px solid #dadada'; b.style.borderLeft='1px solid #eaeaea'; b.style.borderTop='1px solid #fafafa';
    toolmenu.currentHoveringId = "";
};

var swapImages = {};
function btnSwap(img, filename) {
    if (!swapImages[filename]){ swapImages[filename] = new Image(); swapImages[filename].src = "inci/rca/img/" + filename; }
    img.src = swapImages[filename].src;
};


<? //------------------------------------------------------------------------------ ?>
<? // リロード                                                                      ?>
<? //------------------------------------------------------------------------------ ?>
function dataReload(){
    if(!confirm("画面のリロードを行い、データベース上の最新状態に戻します。\n現在編集中のデータは、クリアされます。\n\n最新状態に戻してよろしいですか？")) return;
    window.location.href = "hiyari_analysis_rca_chart.php?a_id="+analysisId+"&chart_seq="+chartSeq+"&session=<?=$session?>&inga_mode="+ingaMode;
}


<? //------------------------------------------------------------------------------ ?>
<? // 設定画面との通信                                                              ?>
<? //------------------------------------------------------------------------------ ?>
var diversion_window = null;
function diversionWindowPopup(){
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=850,height=650";
    var url = "hiyari_analysis_rca_chart_diversion.php?a_id="+analysisId+"&chart_seq="+chartSeq+"&session=<?=$session?>";
    diversion_window = window.open(url, 'hiyari_analysis_rca_chart_diversion_window', option);
    setTimeout("if(diversion_window && !diversion_window.closed) diversion_window.focus()", 100);
}

var guidance_window = null;
function guidanceWindowPopup(){
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=850,height=650";
    var url = "hiyari_analysis_rca_chart_guidance.php?a_id="+analysisId+"&chart_seq="+chartSeq+"&session=<?=$session?>";
    guidance_window = window.open(url, 'hiyari_analysis_rca_chart_guidance_window', option);
    setTimeout("if(guidance_window && !guidance_window.closed) guidance_window.focus()", 100);
}

function templateDeleteNoticeReceiver(_chartSeq){
    if (_chartSeq != chartSeq) return;
    chartSeq = "";
    if (int(analysisId) || int(chartSeq)) return;
    cond.isSystemDisabled = true;
    templateTitle = "";
    cond.abortAllLockedFunctions();
    document.getElementById("rca_title").innerHTML = "";
    document.getElementById("rca_analysis_no").innerHTML = "<br/>";
    alert("編集中のデータが削除されたため、分析チャート画面は無効になりました。\n");
}

function templateRenameNoticeReceiver(_templateTitle){
    document.getElementById("rca_title").innerHTML = _templateTitle;
}


function swapChartData(chart_chart_seq, cliplist_chart_seq){
    var postval =
        "session=<?=$session?>"+
        "&chart_chart_seq="+chart_chart_seq+
        "&cliplist_chart_seq="+cliplist_chart_seq;
    sendRequest(postval, "hiyari_analysis_rca_chart_load.php", returnInquirySwap);
}

function returnInquirySwap(){
    if (ajaxObj == null) return;
    if (ajaxObj.readyState != 4) return;
    if (typeof(ajaxObj.status) != "number") return;
    if (ajaxObj.status != 200) return;
    if (!ajaxObj.responseText) return;
    try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
    catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
    if (!ret) return;

    if (ret.chart_data){
        cond.isDrawing = true;
        rcaMap = {};
        cond.abortAllLockedFunctions();
        for (var rcaid in ret.chart_data) rcaMap[rcaid] = bxu.cloneBox(ret.chart_data[rcaid]);
        if (!bxu.count(rcaMap)) cond.setDefaultMinimumChart();
        adjustSpace();
        cond.refreshMapWorldScales(false);
        drawMap(lineRouteCalculator.getResult(), true);
        cond.isDrawing = false;
    }

    if (ret.cliplist_data){
        cliplist = new CCliplist(ret.cliplist_data);
        cliplist.show();
    }

    ajaxObj = null;
}

<? //------------------------------------------------------------------------------ ?>
<? // 編集ロックとアンロック                                                        ?>
<? //------------------------------------------------------------------------------ ?>
function editUnlock(){
    var postval =
        "session=<?=$session?>"+
        "&analysis_no="+encodeURIComponent(encodeURIComponent(analysisNo))+
        "&analysis_id="+analysisId+
        "&chart_seq="+chartSeq+
        "&command_mode=unlock";

    if(!confirm("編集ロックを解除します。\n必要な編集データは、ロック解除前に保存を行ってください。\n\n編集ロックを解除してよろしいですか？")) return;
    sendRequest(postval, "hiyari_analysis_rca_chart_editlock.php", returnInquiryEditLock);
}
function editLock(){
    var postval =
        "session=<?=$session?>"+
        "&analysis_no="+encodeURIComponent(encodeURIComponent(analysisNo))+
        "&analysis_id="+analysisId+
        "&chart_seq="+chartSeq+
        "&command_mode=lock";
    if (!confirm("編集ロックを行います。\n画面上の編集中データは消去され、最新状態が表示されます。\n\n編集ロックを行ってよろしいですか？")) return;
    sendRequest(postval, "hiyari_analysis_rca_chart_editlock.php", returnInquiryEditLock);
}

function returnInquiryEditLock(){
    if (ajaxObj == null) return;
    if (ajaxObj.readyState != 4) return;
    if (typeof(ajaxObj.status) != "number") return;
    if (ajaxObj.status != 200) return;
    if (!ajaxObj.responseText) return;
    try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
    catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
    if (!ret) return;
    ajaxObj = null;
    if (!chartSeq && !ret.chart_seq) alert("エラーが発生しました。(101)");
    chartSeq = ret.chart_seq;

    var isUnlock = (ret.command_mode == "unlock" ? true : false);
    if (ret.warn_msg) isUnlock = !isUnlock;
    document.getElementById("btn_edit_lock").style.display = (isUnlock ? "" : "none");
    document.getElementById("btn_edit_unlock").style.display = (isUnlock ? "none" : "");
    document.getElementById("edit_lock_user_name").innerHTML = (isUnlock ? "編集ロックはされていません。" : "編集ロック中：" + ret.editing_emp_name);
    if (ret.warn_msg != "") alert(ret.warn_msg);
}




<? //------------------------------------------------------------------------------ ?>
<? // 保存                                                                          ?>
<? //------------------------------------------------------------------------------ ?>
function inquiryRegist(){
    var chartDataJson = bxu.getJsonDump();
    var chartVData = bxu.getVTextDump();
    var cliplistDataJson = cliplist.getJsonDump();
    var envData = "";

    var postval =
        "session=<?=$session?>"+
        "&analysis_no="+encodeURIComponent(encodeURIComponent(analysisNo))+
        "&template_title="+encodeURIComponent(encodeURIComponent(templateTitle))+
        "&analysis_id="+analysisId+
        "&chart_seq="+chartSeq+
        "&chart_item_count="+bxu.count(rcaMap)+
        "&cliplist_item_count="+bxu.count(cliplist.entries)+
        "&chart_data="+encodeURIComponent(encodeURIComponent(chartDataJson))+
        "&chart_v_data="+encodeURIComponent(encodeURIComponent(chartVData))+
        "&cliplist_data="+encodeURIComponent(encodeURIComponent(cliplistDataJson))+
        "&environment_data="+encodeURIComponent(encodeURIComponent(envData))+
        "&is_inga_mode="+ingaMode;
    sendRequest(postval, "hiyari_analysis_rca_chart_regist.php", returnInquiryRegist);
}

function returnInquiryRegist(){
    if (ajaxObj == null) return;
    if (ajaxObj.readyState != 4) return;
    if (typeof(ajaxObj.status) != "number") return;
    if (ajaxObj.status != 200) return;
    if (!ajaxObj.responseText) return;
    try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
    catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
    if (!ret) return;
    ajaxObj = null;
    if (!chartSeq && !ret.chart_seq) alert("エラーが発生しました。(101)");
    chartSeq = ret.chart_seq;
    if (ret.warn_msg) { alert(ret.warn_msg); return; }
    alert("保存しました。");
}





<? //------------------------------------------------------------------------------ ?>
<? // コマンド（１）ボックスの抹消                                                  ?>
<? //------------------------------------------------------------------------------ ?>
function btnDelBoxClick(yx){
    cond.isDrawing = true;
    var rcaid = YX(yx.y,yx.x); <? // 削除対象 ?>

    cond.resetMenu();

    var parents = bxu.getParentList(rcaid); <? // 親リスト ?>
    var brothers = bxu.getBrotherList(rcaid, parents); <? // 同じ親をもつ兄弟リスト ?>
    var friends = bxu.getFriendList(rcaid); <? // 同じ子を持つ友達リスト ?>

    <? // 兄弟が左にいるとき、その兄弟よりさらに左に子供を接続していた場合は、?>
    <? // 親にわが子をゆだねない ?>
    var isHikitugiNasi = false;

    var minBrotherX = 9999;
    var minChildX = 9999;
    for (var bid in brothers) minBrotherX = Math.min(minBrotherX, getyx(bid).x);
    for (var d in rcaMap[rcaid].dest) minChildX = Math.min(minChildX, getyx(rcaMap[rcaid].dest[d]).x);
    if (minBrotherX < yx.x && minChildX < minBrotherX) isHikitugiNasi = true;

    <? // 親が左にいるとき、同じ子を持つ友達が右に存在していた場合は、?>
    <? // 親にわが子をゆだねない ?>
    if (!isHikitugiNasi){
        var minParentX = 9999;
        var maxFriendX = 0;
        for (var pid in parents) minParentX = Math.min(minParentX, getyx(pid).x);
        if (minParentX < yx.x){
            for (var fid in friends) maxFriendX = Math.max(maxFriendX, getyx(fid).x);
            if (maxFriendX > yx.x) isHikitugiNasi = true;
        }
    }


    var del_dest = bxu.cloneBoxDest(rcaMap[rcaid].dest);
    for (var id in rcaMap){
        for (var d in rcaMap[id].dest){
            var did = rcaMap[id].dest[d];
            if (!did) continue; <? // removeDestで削除されると番号が詰められる。このためループ途中でundefinedになる場合は抜ける ?>
            var dyx = getyx(did);
            if (did==rcaid) { <? // 削除対象に接続する親なら ?>
                bxu.removeDest(id, rcaid); <? // 親からrcaidを削除 ?>

                <? // 別ルートが存在しなければ、親にわが子をゆだねる ?>
                var isExistAnotherRoute = false;

                var nextChildList = {};
                for (var bid in brothers) nextChildList[bid] = brothers[bid];

                for (;;){
                    var grandList = {};
                    for (var pid in nextChildList){
                        for (var d in nextChildList[pid].dest){
                            var ddid = nextChildList[pid].dest[d];
                            for (var del_d in del_dest) {
                                if (ddid==del_dest[del_d]){
                                    isExistAnotherRoute = true;
                                    break;
                                }
                            }
                            if (isExistAnotherRoute) break;
                            grandList[ddid] = rcaMap[ddid];
                        }
                        if (isExistAnotherRoute) break;
                    }
                    if (isExistAnotherRoute) break;
                    if (!bxu.count(grandList)) break;
                    nextChildList = {};
                    for (var gid in grandList) nextChildList[gid] = grandList[gid];
                }
                if (isExistAnotherRoute) continue;
                if (isHikitugiNasi) continue;
                for (var del_d in del_dest) bxu.addDestYX(id, del_dest[del_d]);
            }
        }
    }

    delete rcaMap[rcaid];

    <? // 自分より親が右の場合は、自分より右と下の要素を右にずらす ?>
    bxu.moveRightIfParentsAreRight();

    adjustSpace();
    cond.refreshMapWorldScales(false);
    drawMap(lineRouteCalculator.getResult(), true);
    cond.isDrawing = false;
}

<? //------------------------------------------------------------------------------ ?>
<? // コマンド（２）ボックスのテキスト編集開始                                      ?>
<? //------------------------------------------------------------------------------ ?>
function btnTextEditClick(){
    textEditor.applyChange();
    var yx = cond.getBoxYXP();
    var div = document.getElementById("boxdiv_" + YX(yx.y, yx.x));
    var td = div.childNodes[0].childNodes[0].childNodes[0].childNodes[0];
    textEditor.startEdit(td, yx.y, yx.x);
}


<? //------------------------------------------------------------------------------ ?>
<? // コマンド（３）ボックスの色変更                                                ?>
<? //------------------------------------------------------------------------------ ?>
function btnChangeColorClick(){
    cond.isDrawing = true;
    var yx = cond.getBoxYXP();
    var rca = rcaMap[YX(yx.y,yx.x)];
    if (ingaMode){
        if          (rca.boxtype=="パス" || rca.boxtype=="根本原因") rca.boxtype="原因";
        else        rca.boxtype = (rca.dest.length ? "パス" : "根本原因");
    } else {
        if          (rca.boxtype=="パス" || rca.boxtype=="根本原因") rca.boxtype="質問";
        else if (rca.boxtype=="質問") rca.boxtype="原因";
        else        rca.boxtype = (rca.dest.length ? "パス" : "根本原因");
    }
    toolmenu.boxname.innerHTML = rca.boxtype;
    toolmenu.boxname.style.color = bxu.getBoxBehavior(rca.boxtype).namecolor;
    drawMap(lineRouteCalculator.calculate(true));
    cond.isDrawing = false;
}


<? //------------------------------------------------------------------------------ ?>
<? // コマンド（４）線を削除・カットする                                            ?>
<? //------------------------------------------------------------------------------ ?>
function cutLine(){
    var yxp = cond.getBoxYXP();
    cond.hideLineMenu();
    var rcaid = YX(yxp.y,yxp.x);
    if (!bxu.isBox(rcaid)) return;

    var cinc = 0;
    var allChildren = [];
    for (var d in rcaMap[rcaid].dest) if (getyx(rcaMap[rcaid].dest[d]).y != 1) allChildren.push(rcaMap[rcaid].dest[d]);
    while (cinc < allChildren.length){
        var dest = rcaMap[allChildren[cinc]].dest;
        if (dest) for (var d in dest) if (getyx(dest[d]).y != 1) allChildren.push(dest[d]);
        cinc++;
    }
    var oAllChildren = {};
    var inc = 0;
    for (var i in allChildren) {
        var dyx = getyx(allChildren[i]);
        inc = Math.max(inc, dyx.x - yxp.x);
        oAllChildren[allChildren[i]] = true;
    }
    <? // 右向き線(工程/結果線）以外の線をすべて除去 ?>
    for (var d in rcaMap[rcaid].dest) if (getyx(rcaMap[rcaid].dest[d]).y>1) delete rcaMap[rcaid].dest[d];
    bxu.sortDest(rcaid);


    <? // カットした要素の下に要素がある場合は、カットした要素より上と、子要素以外の右列をすべて右にずらす ?>
    for (var nx = cond.colCount; nx > yxp.x; nx--){
        for (var ny = 1; ny <= cond.rowCount; ny++){
            if (oAllChildren[YX(ny,nx)]) continue;
            if (!bxu.isBox(YX(ny,nx))) continue;
            bxu.moveBoxTo(YX(ny,nx), 0, plus1(inc));
        }
    }
    for (var ny = 1; ny <= yxp.y; ny++){
        if (oAllChildren[YX(yxp.y,nx)]) continue;
        if (yxp.x==1 && ny==1) continue;
        if (!bxu.isBox(YX(ny,yxp.x))) continue;
        bxu.moveBoxTo(YX(ny,yxp.x), 0, plus1(inc));
    }
    <? // 自分より親が右の場合は、自分より右と下の要素を右にずらす ?>
    bxu.moveRightIfParentsAreRight();

    cond.isDrawing = true;
    adjustSpace();
    cond.refreshMapWorldScales(false);
    drawMap(lineRouteCalculator.getResult(), true);
    cond.isDrawing = false;
}

<? //------------------------------------------------------------------------------ ?>
<? // コマンド（５）ボックス追加、追加は右または下への追加しかない ?>
<? //------------------------------------------------------------------------------ ?>
function addBox(Y, X, position, exBoxtype){
    if (position != "lineB" && position != "lineR") return;

    var boxtype = "";
    if (exBoxtype=="Y") boxtype = "工程";
    if (exBoxtype=="G") boxtype = "結果";
    if (exBoxtype=="B") boxtype = "質問";
    if (exBoxtype=="BR") boxtype = "質問";
    if (exBoxtype=="RB") boxtype = "原因";
    if (exBoxtype=="R") boxtype = "原因";
    if (exBoxtype=="V") boxtype = "根本原因";

    var maxX = maxY = 0;
    var minX = minY = 9999;
    if (position == "lineR"){
        <? // rcaMapをずらしてコピーする。選択列の次の列はカラとなる。 ?>
        for (var y = 1; y <= cond.rowCount; y++){
            for (var x = cond.colCount; x > X; x--){
                if (!bxu.isBox(YX(y,x))) continue;
                var dontTouchDest = (x==plus1(X) && y==Y && Y==1);
                bxu.moveBoxTo(YX(y,x), 0, 1, dontTouchDest);
            }
        }

        <? // 指定行指定列に、ボックスタイプをセットする。 ?>
        rcaMap[YX(Y,plus1(X))] = {"boxtype":boxtype,"h":cond.getRowHeight(Y), "dest":[]};

        <? // １行目の場合は、真横に線が延びるため、真横に接続 ?>
        if (Y==1) rcaMap[YX(Y,plus1(X))].dest[0] = "1_"+plus1(plus1(x));

        <? // １行目でなければ、追加したボックスへのルートは、コピー元と同じにする ?>
        else {
            for (var rcaid in rcaMap){
                for (var d in rcaMap[rcaid].dest){
                    if (rcaMap[rcaid].dest[d]==YX(Y,X)){
                        var yx = getyx(rcaid);
                        bxu.addDestYX(YX(yx.y,yx.x), YX(Y,plus1(X)));
                    }
                }
            }
            <? // 追加したボックスからの下向きルートも、１行目向けを除き、コピー元と同じにする ?>
            for (var d in rcaMap[YX(Y,X)].dest){
                var did = rcaMap[YX(Y,X)].dest[d];
                var dyx = getyx(did);
                if (dyx.y > Y) { bxu.addDestYX(YX(Y, plus1(X)), did); }
            }
        }
    }

    if (position == "lineB"){

        <? // rcaMapをずらしてコピーする。選択行の次の行は、その次の行とデータ参照先が重複した状態。 ?>
        for (var y = cond.rowCount; y > Y; y--){
            for (var x = 1; x <= cond.colCount; x++){
                if (!bxu.isBox(YX(y,x))) delete rcaMap[YX(plus1(y),x)];
                else rcaMap[YX(plus1(y),x)] = bxu.cloneBox(rcaMap[YX(y,x)]);
            }
        }
        <? // 選択行の次の行は抹消 ?>
        for (var x = 1; x <= cond.colCount; x++) if (rcaMap[YX(plus1(Y),x)]) delete rcaMap[YX(plus1(Y),x)];
        <? // rcaMapのdestをずらす ?>
        for (var trcaid in rcaMap){
            if (!bxu.isBox(trcaid)) { alert("error addBox(1) " + trcaid); continue; }
            for (var d in rcaMap[trcaid].dest){
                var did = rcaMap[trcaid].dest[d];
                var dyx = getyx(did);
                if (dyx.y > Y || y > Y) rcaMap[trcaid].dest[d] = YX(plus1(dyx.y), dyx.x);
            }
        }

        <? // 指定行指定列に、ボックスを増やす。 ?>
        rcaMap[YX(plus1(Y),X)] = {"boxtype":boxtype,"w":cond.getColWidth(X), "dest":[]};
        <? // 当該ボックスからルートを、１行目向けを除き直上のボックスのルートと同じにする ?>
        if (boxtype!="根本原因") {
            var dcnt = 0;
            for (var d in rcaMap[YX(Y,X)].dest) {
                var newdid = rcaMap[YX(Y,X)].dest[d];
                var newdyx = getyx(newdid);
                if (newdyx.y < 2) continue;
                rcaMap[YX(plus1(Y),X)].dest[dcnt] = newdid;
                dcnt++;
            }
        }
        <? // 直上のボックスの、下向き矢印は当該ボックスのみとする ?>
        bxu.removeBottomDest(Y,X);
        bxu.addDestYX(YX(Y,X), YX(plus1(Y), X));
        Y++;
    }

    if (exBoxtype=="BR") {
        cond.refreshMapWorldScales(true);
        addBox(Y, X, position ,"R");
    }
    else if (exBoxtype=="RB") {
        cond.refreshMapWorldScales(true);
        addBox(Y, X, position ,"B");
    }
    else {
        cond.isDrawing = true;
        adjustSpace();
        cond.refreshMapWorldScales(false);
        drawMap(lineRouteCalculator.getResult(), true);
        cond.isDrawing = false;
    }
}


<? //------------------------------------------------------------------------------ ?>
<? // 共通（１）オブジェクト描画                                                    ?>
<? //------------------------------------------------------------------------------ ?>
function drawMap(line, isRefreshAllLine, noMakeHistory){
    if (!noMakeHistory) undo.makeHistory();
    var imgList = {};

    createOrRecycleImage = function (id, imgType, src){
        var newimg = 0;
        for (var img in imgList){
            if (img.substring(0,4) != "img"+imgType) continue;
            newimg = imgList[img];
            delete imgList[img];
            cond.unmountFromMapWorld(newimg);
            newimg.id = id;
            return newimg;
        }
        newimg = document.createElement("img");
        newimg.id = id;
        newimg.name = "img";
        newimg.src = "inci/rca/img/"+src;
        newimg.style.position = "absolute";
        newimg.style.zIndex = 1000;
        return newimg;
    }

    <? //================================= ?>
    <? // １）作成予定の線のIDを作成する   ?>
    <? //================================= ?>
    var newIdList = {};
    for (var rcaid in line){
        for (var position in line[rcaid]){
            var lineTypes = line[rcaid][position];
            var prefix = "imgx_" + rcaid + "_" + position + "_";
            if (lineTypes==[lineTypes|HORZ_L_UPPER] || lineTypes==[lineTypes|HORZ_R_UPPER]) { <? // LR上線 ?>
                newIdList[prefix + (lineTypes & ([HORZ_L_UPPER | HORZ_R_UPPER]))] = true;
            }
            if (lineTypes==[lineTypes|HORZ_L_UNDER] || lineTypes==[lineTypes|HORZ_R_UNDER]) { <? // LR下線 ?>
                newIdList[prefix + (lineTypes & ([HORZ_L_UNDER | HORZ_R_UNDER]))] = true;
            }
            if (lineTypes==[lineTypes|VERT_UPPER] || lineTypes==[lineTypes|VERT_CENTER] || lineTypes==[lineTypes|VERT_UNDER]) { <? // TB１本目の線 ?>
                newIdList[prefix + (lineTypes & [VERT_UPPER|VERT_CENTER|VERT_UNDER])] = true;
            }
            if (position == "PosB2"){ <? // TB2本目 ?>
                newIdList[prefix + VERT_UNDER] = true;
            }
            if (lineTypes==[lineTypes|ARROW_RIGHT]) { <? // 矢印 ?>
                prefix = "imgr_" + rcaid + "_" + position + "_";
                newIdList[prefix + (lineTypes & ARROW_RIGHT)] = true;
            }
            if (!ingaMode){
                if (lineTypes==[lineTypes|ARROW_DOWN]) { <? // 矢印 ?>
                    prefix = "imgb_" + rcaid + "_" + position + "_";
                    newIdList[prefix + (lineTypes & ARROW_DOWN)] = true;
                }
            }
            if (false){
                if (lineTypes==[lineTypes|ARROW_LEFT]) { <? // 矢印 ?>
                    prefix = "imgl_" + rcaid + "_" + position + "_";
                    newIdList[prefix + (lineTypes & ARROW_LEFT)] = true;
                }
            }
            if (ingaMode){
                if (lineTypes==[lineTypes|ARROW_UP]) { <? // 矢印 ?>
                    prefix = "imgu_" + rcaid + "_" + position + "_";
                    newIdList[prefix + (lineTypes & ARROW_UP)] = true;
                }
            }
        }
    }

    <? //================================= ?>
    <? // ２）作成予定の線が存在すれば、作成予定から削除 ?>
    <? //     作成予定にない既存の線は、使いまわし用リストに ?>
    <? //================================= ?>
    for (var i = mapWorld.childNodes.length-1; i >=0; i--){
        var elem = mapWorld.childNodes[i];
        if (elem.tagName != "IMG" || elem.name != "img") continue;
        var id = elem.id;
        if (isRefreshAllLine){ var img = mapWorld.removeChild(elem); delete img; continue; }
        if (newIdList[id]) { delete newIdList[id]; continue; }
        imgList[id] = elem;
    }

    <? //========================== ?>
    <? // ３）線の描画、不要な既存線は再利用する。足りなくなったら新規作成する。 ?>
    <? //========================== ?>
    var aaa = "";
    for (var rcaid in line){
        var rca = rcaMap[rcaid];
        var yx = getyx(rcaid);

        <? // ボックスの周辺４象限(position)の各々について実施 ?>
        for (var position in line[rcaid]){
            if (!line[rcaid][position]) continue;
            var eleft = cond.getColX(yx.x)+MAP_PADDING;
            var ewidth = bxu.getBoxWidth(yx.y, yx.x, rca);

            if (position!="PosBox" && position!="PosB" && position!= "PosB2") {
                eleft += bxu.getBoxWidth(yx.y, yx.x, rca);
                ewidth = !cond.getColX(plus1(yx.x)) ? 30: cond.getColX(plus1(yx.x)) - eleft + MAP_PADDING;
                var tmpw = !cond.getColX(plus1(yx.x)) ? 30: cond.getColX(plus1(yx.x)) - cond.getColX(yx.x) - cond.getColWidth(yx.x);
                var halfw = Math.ceil(tmpw/2) + (cond.getColWidth(yx.x) - bxu.getBoxWidth(yx.y, yx.x, rca));
            } else {
                var halfw = Math.ceil(cond.getColMinWidth(yx.x)/2);
            }

            var etop = cond.getRowY(yx.y)+MAP_PADDING;
            var eheight = bxu.getBoxHeight(yx.y, yx.x, rca);
            if (position!="PosBox" && position!="PosR") {
                etop += bxu.getBoxHeight(yx.y, yx.x, rca);
                eheight = !cond.getRowY(plus1(yx.y)) ? 30: cond.getRowY(plus1(yx.y)) - etop + MAP_PADDING;
                var tmph = !cond.getRowY(plus1(yx.y)) ? 30: cond.getRowY(plus1(yx.y)) - cond.getRowY(yx.y) - cond.getRowHeight(yx.y);
                var halfh = Math.ceil(tmph/2) + (cond.getRowHeight(yx.y) - bxu.getBoxHeight(yx.y, yx.x, rca));
            } else {
                var halfh = Math.ceil(cond.getRowMinHeight(yx.y)/2);
            }

            <? // 各象限について、ラインと矢印を描画 ?>
            var lineTypes = line[rcaid][position];

            <? // LR上線 ?>
            if (lineTypes==[lineTypes|HORZ_L_UPPER] || lineTypes==[lineTypes|HORZ_R_UPPER]) {
                var newid = "imgx_" + rcaid + "_" + position + "_" + (lineTypes & ([HORZ_L_UPPER | HORZ_R_UPPER]));
                if (newIdList[newid]) {
                    var img = createOrRecycleImage(newid, "l", "dot_g2.gif");
                    img.style.height = "1px";
                    var realW = ewidth;
                    if (lineTypes != [lineTypes|HORZ_R_UPPER]) realW = halfw; <? // Lのみ ?>
                    if (lineTypes != [lineTypes|HORZ_L_UPPER]) realW = ewidth - halfw; <? //Rのみ ?>
                    img.style.width = px(realW);
                    img.style.top = px(etop + halfh - 4);
                    img.style.left = px((lineTypes != [lineTypes|HORZ_L_UPPER]) ? eleft+halfw : eleft);
                    cond.mountToMapWorld(img);
                }
            }

            <? // LR下線 ?>
            if (lineTypes==[lineTypes|HORZ_L_UNDER] || lineTypes==[lineTypes|HORZ_R_UNDER]) {
                var htu = lineTypes.HorzTypeUnder;
                var newid = "imgx_" + rcaid + "_" + position + "_" + (lineTypes & ([HORZ_L_UNDER | HORZ_R_UNDER]));
                if (newIdList[newid]) {
                    var img = createOrRecycleImage(newid, "l", "dot_g2.gif");
                    img.style.height = "1px";
                    var realW = ewidth;
                    if (lineTypes != [lineTypes|HORZ_R_UNDER]) realW = halfw; <? // Lのみ ?>
                    if (lineTypes != [lineTypes|HORZ_L_UNDER]) realW = ewidth - halfw; <? //Rのみ ?>
                    img.style.width = px(realW);
                    img.style.top = px(etop + halfh + 4);
                    img.style.left = px((lineTypes != [lineTypes|HORZ_L_UNDER]) ? eleft+halfw : eleft);
                    cond.mountToMapWorld(img);
                }
            }

            <? // TB１本目の線 ?>
            if (position != "PosB2"){
                if (lineTypes==[lineTypes|VERT_UPPER] || lineTypes==[lineTypes|VERT_CENTER] || lineTypes==[lineTypes|VERT_UNDER]) {
                    var newid = "imgx_" + rcaid + "_" + position + "_" + (lineTypes & [VERT_UPPER|VERT_CENTER|VERT_UNDER]);
                    if (newIdList[newid]) {
                        var img = createOrRecycleImage(newid, "l", "dot_g2.gif");
                        var t = etop; <? // Upperがある ?>
                        if (lineTypes!=[lineTypes|VERT_UPPER]) { <? // Upperがない ?>
                            t = etop + halfh - 4;
                            if (lineTypes!=[lineTypes|VERT_CENTER]) { <? // Centerもない ?>
                                t = etop + halfh + 4;
                            }
                        }
                        var h = eheight; <? // 全部ある ?>
                        <? // Upperしかない ?>
                        if ((lineTypes==[lineTypes|VERT_UPPER]) && (lineTypes!=[lineTypes|VERT_CENTER]) && (lineTypes!=[lineTypes|VERT_UNDER])) {
                            h = halfh - 4;
                        }
                        <? // Centerはあるが、UpperかUnderがない ?>
                        if ((lineTypes==[lineTypes|VERT_CENTER]) && (lineTypes!=[lineTypes|VERT_CENTER] || lineTypes!=[lineTypes|VERT_UNDER])) {
                            h = halfh + 4;
                        }
                        <? // Underしかない ?>
                        if ((lineTypes!=[lineTypes|VERT_UPPER]) && (lineTypes!=[lineTypes|VERT_CENTER]) && (lineTypes==[lineTypes|VERT_UNDER])) {
                            h = halfh - 4;
                        }

                        img.style.height = px(h);
                        img.style.width = "1px";
                        img.style.top = px(t);
                        img.style.left = px(eleft + halfw);
                        cond.mountToMapWorld(img);
                    }
                }
            }

            <? // TB２本目の線 ?>
            if (position == "PosB2"){
                var newid = "imgx_" + rcaid + "_" + position + "_" + VERT_UNDER;
                if (newIdList[newid]) {
                    var img = createOrRecycleImage(newid, "l", "dot_g2.gif");
                    img.style.height = px(halfh - 4);
                    img.style.width = "1px";
                    img.style.top = px(etop + halfh + 4);
                    img.style.left = px(eleft + halfw);
                    cond.mountToMapWorld(img);
                }
            }

            <? // 矢印（右または下） ?>
            if (lineTypes==[lineTypes|ARROW_RIGHT] || lineTypes==[lineTypes|ARROW_DOWN]) {
                var is_r = (lineTypes==[lineTypes|ARROW_RIGHT]) ? true : false;
                var r_or_b = (is_r ? "r":"b");
                var newid = "img"+r_or_b+"_" + rcaid + "_" + position + "_" + (lineTypes & [ARROW_RIGHT|ARROW_DOWN]);
                if (newIdList[newid]) {
                    var img = createOrRecycleImage(newid, r_or_b, "arrow_"+r_or_b+".gif");
                    img.style.height = px(is_r?12:6);
                    img.style.width = px(is_r?6:12);
                    img.style.top = px(etop + (is_r ? halfh-2 : eheight-6));
                    img.style.left = px(eleft + (is_r ? ewidth-6 : halfw-6));
                    cond.mountToMapWorld(img);
                }
            }
            <? // 矢印（左または上） ?>
            if (lineTypes==[lineTypes|ARROW_LEFT] || lineTypes==[lineTypes|ARROW_UP]) {
                var is_l = (lineTypes==[lineTypes|ARROW_LEFT]) ? true : false;
                var l_or_u = (is_l ? "l":"u");
                var newid = "img"+l_or_u+"_" + rcaid + "_" + position + "_" + (lineTypes & [ARROW_LEFT|ARROW_UP]);
                if (newIdList[newid]) {
                    var img = createOrRecycleImage(newid, l_or_u, "arrow_"+l_or_u+".gif");
                    img.style.height = px(is_l?12:6);
                    img.style.width = px(is_l?6:12);
                    img.style.top = px(etop + (is_l ? halfh-2 : 2));
                    img.style.left = px(eleft + (is_l ? 2 : halfw-6));
                    cond.mountToMapWorld(img);
                }
            }
        }
    }

    <? //========================== ?>
    <? // ４）余った線を抹消する    ?>
    <? //========================== ?>
    for (var img in imgList){
        var delimg = mapWorld.removeChild(imgList[img]);
        delete delimg;
        delete imgList[img];
    }
    delete imgList;

    <? //========================== ?>
    <? // ５）ボックスのリネーム    ?>
    <? //========================== ?>
    for (var i = mapWorld.childNodes.length-1; i >=0; i--){
        var elem = mapWorld.childNodes[i];
        if (elem.tagName != "DIV" || elem.name != "boxdiv") continue;
        var n = elem.id;
        elem.id = "_" + n;
    }

    <? //========================== ?>
    <? // ６）ボックスを作成する    ?>
    <? //========================== ?>
    for (var rcaid in rcaMap){
        if (!bxu.isBox(rcaid)) { alert("error drawMap(1) " + rcaid); continue; }
        var rca = rcaMap[rcaid];
        var yx = getyx(rcaid);
        var eleft = cond.getColX(yx.x)+MAP_PADDING;
        var ewidth = bxu.getBoxWidth(yx.y, yx.h, rca);
        var etop = cond.getRowY(yx.y)+MAP_PADDING;
        var eheight = bxu.getBoxHeight(yx.y, yx.h, rca);
        var cName = bxu.getBoxBehavior(rca.boxtype).divboxClass;
        var text = textEditor.htmlWbrEscape(textEditor.trim(bxu.getBoxText(rca)));
        if (text == "") text = "<br>";

        var div = 0;
        if (rca.before) div = document.getElementById("_boxdiv_"+rca.before);
        if (div) {
            div.id = "boxdiv_" + rcaid;
            if (div.style.width  != px(ewidth) ) div.style.width = px(ewidth);
            if (div.style.left   != px(eleft)  ) div.style.left = px(eleft);
            if (div.style.height != px(eheight)) div.style.height = px(eheight);
            if (div.style.top    != px(etop)   ) div.style.top = px(etop);
            if (div.className != cName) div.className = cName;
            var td = div.childNodes[0].childNodes[0].childNodes[0].childNodes[0];
            if (td.innerHTML != text) td.innerHTML = text;
        } else {
            div = document.createElement("div");
            div.id = "boxdiv_" + rcaid;
            div.name = "boxdiv";
            div.className = cName;
            div.style.zIndex = (rca.boxtype=="パス" ? 900 : 2000);
            div.style.position = "absolute";
            div.innerHTML =
                '<table class="entity" style="width:100%; height:100%"><tbody><tr><td onclick="boxClickEvent(this);">'+text+'</td></tr></tbody></table>';
            div.style.width = px(ewidth);
            div.style.left = px(eleft);
            div.style.height = px(eheight);
            div.style.top = px(etop);
            cond.mountToMapWorld(div);
        }
        rcaMap[rcaid].before = rcaid;
    }

    <? //========================== ?>
    <? // ７）余計なボックスを削除  ?>
    <? //========================== ?>
    for (var i = mapWorld.childNodes.length-1; i >=0; i--){
        var elem = mapWorld.childNodes[i];
        if (elem.tagName != "DIV" || elem.name != "boxdiv") continue;
        if (elem.id.substring(0,7) != "_boxdiv") continue;
        mapWorld.removeChild(elem); delete elem;
    }
}


<? //------------------------------------------------------------------------------ ?>
<? // 共通（２）ボックスの空間を詰めれるなら詰める                                  ?>
<? //------------------------------------------------------------------------------ ?>
function adjustSpace(){
    cond.refreshMapWorldScales(true);
    var lines = lineRouteCalculator.calculate(true);
    <? // ボックスを上にひとつ詰めれるか ?>
    var i = 0;
    var needCalculate = false;
    var needRescale = false;
    for (;;){
        var isUpdated = false;
        i++;
        if (i > 10000) { alert("top adjustor limit over"); break; }
        for (var srcaid in rcaMap){
            if (!bxu.isBox(srcaid)) { alert("error adjustSpace (1) " + srcaid); return; }
            var syx = getyx(srcaid);
            if (syx.y==1 || syx.y==2) continue; <? // ２行目以上は詰めない?>
            if (bxu.isBox(YX(syx.y-1,syx.x))) continue; <? // 上位が存在すれば詰めない ?>
            var parents = bxu.getParentList(srcaid);

            <? // 親がいなければ ?>
            if (!bxu.count(parents)) {
                if (needCalculate) lines = lineRouteCalculator.calculate(true);
                needCalculate = false;
                if (lines[YX(syx.y-1, syx.x)]){
                    if (lines[YX(syx.y-1, syx.x)].PosB) continue; <? // 線が真上にあれば詰めない ?>
                    if (lines[YX(syx.y-1, syx.x)].PosBox) continue; <? // 要素が真上にあれば詰めない ?>
                }
                isUpdated = true;
                bxu.moveBoxTo(srcaid, -1, 0);
                needCalculate = true;
                needRescale = true;
                break;
            }

            var isNGExist = false;
            var minX = cond.colCount;
            var maxX = 0;
            for (var pid in parents){
                var yx = getyx(pid);
                if (yx.y >= syx.y-1) { isNGExist = true; break; } <? // 親のうち一つでも直上行なら詰めない ?>
                minX = Math.min(minX, yx.x);
                maxX = Math.max(maxX, yx.x);
            }
            if (!isNGExist){
                var brotherMinY = 9999;
                var brothers = bxu.getBrotherList(srcaid, parents);
                for (var bid in brothers){
                    var yx = getyx(bid);
                    if (yx.y == 1) continue;
                    minX = Math.min(minX, yx.x);
                    maxX = Math.max(maxX, yx.x);
                    brotherMinY = Math.min(brotherMinY, yx.y);
                }
                for (var x = minX; x <= maxX; x++){
                    if (!bxu.isBox(YX(syx.y-1,x))) continue;
                    if (brotherMinY < syx.y) continue;

                    var isBrother = false;
                    for (var b in brothers) if (b==YX(syx.y-1,x)) { isBrother = true; break; }
                    if (isBrother) continue;
                    isNGExist = true;
                    break;
                }
            }

            if (isNGExist) continue;
            isUpdated = true;
            bxu.moveBoxTo(srcaid, -1, 0);
            needCalculate = true;
            needRescale = true;
            break;
        }
        if (!isUpdated) break;
    }
    if (needRescale) cond.refreshMapWorldScales(true);
    if (needCalculate) lines = lineRouteCalculator.calculate(true);
    needCalculate = false;
    <? // ボックスを左にひとつ詰めれるか ?>
    i = 0;
    var targetX = 2;
    var targetY = 2;
    for (;;){
        var isUpdated = false;
        i++;
        if (i > 10000) { alert("left adjustor 2 limit over"); break; }

        if (needRescale) cond.refreshMapWorldScales(true);
        needRescale = false;

        for (var x = targetX; x <= cond.colCount; x++){
            for (var y = targetY; y <= cond.rowCount; y++){
                if (!bxu.isBox(YX(y,x))) continue; <? // ボックスでなければ詰められない ?>
                if (bxu.isBox(YX(y,x-1))) continue; <? // 左がボックスなら詰められない ?>
                if (y==2 && bxu.isBox(YX(1,x))) continue; <? // 上がルートなら詰められない ?>
                var parents = bxu.getParentList(YX(y,x));
                var isExistLeftParent = false;
                for (var p in parents) if (getyx(p).x < x) { isExistLeftParent = true; break; }
                if (!isExistLeftParent) continue; <? // 左側に親がいなければ詰められない ?>
                var isOK = false;
                if (needCalculate) lines = lineRouteCalculator.calculate(true);
                needCalculate = false;
                for (var ny = y; ny <= cond.rowCount; ny++){
                    if (bxu.isBox(YX(ny, x-1))) break;<? // ひとつ左列で、真横または下側にボックスがあれば詰められない ?>
                    if (lines[YX(ny,x-1)]){
                        var pb = lines[YX(ny,x-1)].PosB;
                        if (pb == [pb | HORZ_R_UPPER]) { isOK = true; break; }<? // ひとつ左列で、真横または下側に「右上」線があれば詰められる ?>
                        if (pb == [pb | HORZ_R_UNDER]) break;<? // ひとつ左列で、真横または下側に「右上」「右下」「左下」線があれば詰められない ?>
                        if (pb == [pb | HORZ_L_UNDER]) break;<? // ひとつ左列で、真横または下側に「右上」「右下」「左下」線があれば詰められない ?>
                    }
                    if (ny == cond.rowCount) isOK = true;
                }
                if (isOK){
                    isOK = false;
                    for (var ny = y-1; ny >= 0; ny--){
                        if (ny == 0) { isOK = true; break; }
                        if (bxu.isBox(YX(ny, x-1))) continue;
                        if (lines[YX(ny,x-1)]){
                            var pb = lines[YX(ny,x-1)].PosB;
                            if (!pb) break;
                            isOK = true; break;
                        }
                    }
                }
                if (isOK){
                    bxu.moveBoxTo(YX(y,x), 0, -1);
                    needCalculate = true;
                    needRescale = true;
                    targetX = x-1;
                    targetY = 2;
                    isUpdated = true;
                    break;
                }
            }
            if (isUpdated) break;
        }
        if (!isUpdated) break;
    }

    <? // 列を左に詰めれるか ?>
    var maxCol = 0;
    var maxRow = 0;
    var existCols = [];
    for (var rcaid in rcaMap){
        var yx = getyx(rcaid);
        maxCol = Math.max(maxCol, yx.x);
        maxRow = Math.max(maxRow, yx.y);
        existCols[yx.x] = true;
    }
    var shiftX = 0;
    var copyCols = [];
    var trueCols = [];
    var curX = 0;
    for (var nx = 1; nx <= maxCol; nx++){
        if (!existCols[nx]) { shiftX++; continue; }
        curX++;
        copyCols[curX] = parseInt(curX*1 + shiftX*1);
        trueCols[nx] = curX;
    }
    if (shiftX){
        <? // セル位置をずらす ?>
        for (var nx = 1; nx <= maxCol; nx++){
            var srcX = copyCols[nx];
            if (!srcX) continue;
            if (nx == srcX) continue;
            for (var ny = 1; ny <= maxRow; ny++){
                if (!bxu.isBox(YX(ny,srcX))) delete rcaMap[YX(ny,nx)];
                else {
                    rcaMap[YX(ny,nx)] = rcaMap[YX(ny,srcX)];
                }
                delete rcaMap[YX(ny,srcX)];
            }
        }
        <? // 紐付けをずらす ?>
        for (var rcaid in rcaMap){
            for (var d in rcaMap[rcaid].dest){
                var dyx = getyx(rcaMap[rcaid].dest[d]);
                if (dyx.x != trueCols[dyx.x]) {
                    rcaMap[rcaid].dest[d] = dyx.y+"_"+trueCols[dyx.x];
                }
            }
        }
    }
    if (needRescale || shiftX) cond.refreshMapWorldScales(true);
    if (needCalculate || shiftX) lineRouteCalculator.calculate(true);
}



<? //------------------------------------------------------------------------------ ?>
<? // 共通（３）ラインの精査と集約                                                  ?>
<? //------------------------------------------------------------------------------ ?>
var CLineRouteCalculator = function(){}
CLineRouteCalculator.prototype = {

    result : {},

    getResult : function(){ return this.result; },

    calculate : function(isCalculate){
        var _this = this;
        if (!isCalculate) return this.result;

        this.result = {};

        function mem(Y,X){
            if (!_this.result[YX(Y,X)]) _this.result[YX(Y,X)] = {};
            return _this.result[YX(Y,X)];
        }

        for (var rcaid in rcaMap){
            var yx = getyx(rcaid);
            var sy = yx.y;
            var sx = yx.x;
            var nx, ny;

            var childCount = 0;
            var prevX, prevY;
            var addedL = 0;
            var addedT = 0;
            for (var didx in rcaMap[rcaid].dest){
                var did = rcaMap[rcaid].dest[didx];
                var dest = getyx(did);

                if (!addedL && yx.y == 1 && dest.y == 1) { addedL = 1; mem(sy,sx).PosR |= [ARROW_LEFT]; }
                if (!addedT && dest.y > 1) { addedT = 1; mem(sy,sx).PosB |= [ARROW_UP]; }

                if (dest.y > yx.y && yx.y > 1){
                    childCount++;
                    if (childCount==1) { prevX = dest.x; prevY = dest.y-1 }
                    else { sx = prevX; sy = prevY }
                }
                <? // 右へ ?>
                if (sy==dest.y) {
                    for (var nx = sx; nx <= dest.x-1; nx++){
                        mem(sy,nx).PosR |= [HORZ_L_UNDER | HORZ_R_UNDER];
                        if (dest.x - nx == 1) { mem(sy, nx).PosR |= ARROW_RIGHT; break; }
                        if (!bxu.isBox(sy, plus1(nx))) mem(sy, plus1(nx)).PosBox |= [HORZ_L_UNDER | HORZ_R_UNDER];
                    }
                }
                <? // 下へ ?>
                if (sy < dest.y) {
                    <? // 右下か真下へ ?>
                    if (sx <= dest.x) {
                        <? // 接続先までボックスの無い空間であれば、真下へ進行⇒右へ進行 ?>
                        if (bxu.isBoxExistInRange(plus1(sy), sx, dest.y-1, dest.x)) {
                            for (var ny = sy; ny < dest.y; ny++){
                                mem(ny, sx).PosB |= [ VERT_UPPER | VERT_CENTER];
                                if (ny == dest.y-1) {
                                    if (sx < dest.x) {
                                        mem(ny, sx).PosB |= HORZ_R_UNDER;
                                        mem(ny, sx).PosRB |= [HORZ_L_UNDER | HORZ_R_UNDER];
                                        mem(ny, plus1(sx)).PosB |= HORZ_L_UNDER;
                                    }
                                    break;
                                }
                                mem(ny, sx).PosB |= [VERT_UNDER];
                                if (!bxu.isBox(YX(plus1(ny),sx))) mem(plus1(ny), sx).PosBox |= [VERT_UPPER | VERT_CENTER | VERT_UNDER];
                            }
                            for (var nx = sx; nx <= dest.x; nx++){
                                if (nx == dest.x) { mem(ny, nx).PosB |= [VERT_UNDER | ARROW_DOWN]; break; }
                                mem(ny, nx).PosB |= HORZ_R_UNDER;
                                mem(ny, nx).PosRB |= [HORZ_L_UNDER | HORZ_R_UNDER];
                                mem(ny, plus1(nx)).PosB |= HORZ_L_UNDER;
                            }
                        }
                        <? // 右へ進行⇒下へ進行 ?>
                        else {
                            mem(sy, sx).PosB |= VERT_CENTER;
                            if (childCount<=1) mem(sy, sx).PosB |= VERT_UPPER;
                            for (var nx = sx; nx <= dest.x; nx++){
                                if (nx < dest.x) {
                                    mem(sy, nx).PosB |= HORZ_R_UNDER;
                                    mem(sy, nx).PosRB |= [HORZ_L_UNDER | HORZ_R_UNDER];
                                    mem(sy, plus1(nx)).PosB |= HORZ_L_UNDER;
                                }
                                if (nx == dest.x) { break; }
                            }
                            for (var ny = sy; ny < dest.y; ny++){
                                mem(ny, nx).PosB |= VERT_UNDER;
                                if (ny == dest.y-1){ mem(ny, nx).PosB |= ARROW_DOWN; break; }
                                if (!bxu.isBox(YX(plus1(ny),nx))) {
                                    mem(plus1(ny) ,nx).PosBox |= [VERT_UPPER | VERT_CENTER | VERT_UNDER];
                                    mem(plus1(ny), nx).PosB |= [VERT_UPPER | VERT_CENTER];
                                }
                            }
                        }
                    <? // 左下へ（分岐の戻し）?>
                    } else if (sx > dest.x) {
                        <? // 接続先までボックスの無い空間であれば、真下へ進行⇒左へ進行 ?>
                        if (bxu.isBoxExistInRange(plus1(sy), dest.x, dest.y-1, sx)) {
                            mem(sy, sx).PosB |= VERT_UPPER;
                            for (var ny = sy; ny < dest.y; ny++){
                                if (ny == dest.y-1) break;
                                mem(ny, sx).PosB |= [VERT_CENTER | VERT_UNDER];
                                if (!bxu.isBox(YX(plus1(ny),sx))) {
                                    mem(plus1(ny), sx).PosBox |= [VERT_UPPER | VERT_CENTER | VERT_UNDER];
                                    mem(plus1(ny), sx).PosB |= VERT_UPPER;
                                }
                            }
                            for (var nx = sx; nx >= dest.x; nx--){
                                if (nx==dest.x) { mem(ny, nx).PosB |= [VERT_CENTER | VERT_UNDER | ARROW_DOWN]; break; }
                                mem(ny, nx).PosB |= HORZ_L_UPPER;
                                mem(ny, nx-1).PosRB |= [HORZ_L_UPPER | HORZ_R_UPPER];
                                mem(ny, nx-1).PosB |= HORZ_R_UPPER;
                            }
                        }
                        <? // 左へ進行⇒下へ進行 ?>
                        else {
                            mem(sy, sx).PosB |= VERT_UPPER;
                            for (var nx = sx; nx >= dest.x; nx--){
                                if (nx == dest.x) break;
                                mem(sy, nx).PosB |= [HORZ_L_UPPER];
                                mem(sy, nx-1).PosRB |= [HORZ_L_UPPER | HORZ_R_UPPER];
                                mem(sy, nx-1).PosB |= [HORZ_R_UPPER];
                            }
                            for (var ny = sy; ny < dest.y; ny++){
                                mem(ny, nx).PosB |= [VERT_CENTER | VERT_UNDER];
                                if (ny == dest.y-1) { mem(ny, nx).PosB |= ARROW_DOWN; break; }
                                if (!bxu.isBox(YX(plus1(ny),nx))) {
                                    mem(plus1(ny), nx).PosBox |= [VERT_UPPER | VERT_CENTER | VERT_UNDER];
                                    mem(plus1(ny) ,nx).PosB |= VERT_UPPER;
                                }
                            }
                        }
                    }
                }
            }
        }

        <? //========================== ?>
        <? // パスボックスの場合は、ボックスに線を描画し、直前の矢印を除去 ?>
        <? //========================== ?>
        for (var rcaid in rcaMap){
            if (rcaMap[rcaid].boxtype=="パス"){
                var yx = getyx(rcaid);
                if (yx.y == 1){
                    mem(yx.y, yx.x).PosBox |= [HORZ_L_UNDER | HORZ_R_UNDER];
                    var id = YX(yx.y, yx.x-1);
                    if (this.result[id]) mem(yx.y, yx.x-1).PosR &= [~ARROW_RIGHT];
                } else {
                    mem(yx.y, yx.x).PosBox |= [VERT_UPPER | VERT_CENTER | VERT_UNDER];
                    var id = YX(yx.y-1, yx.x);
                    if (this.result[id]) mem(yx.y-1, yx.x).PosB &= [~ARROW_DOWN];
                }
            }
        }

        <? //========================== ?>
        <? // PosBの上下線の場合は、２線にわかれる場合は、別のポジションIDを作成 ?>
        <? //========================== ?>
        for (var rcaid in this.result){
            var pb = this.result[rcaid].PosB;
            if (pb && (pb == [pb | VERT_UPPER | VERT_UNDER])){
                if (pb != [pb | VERT_CENTER]){
                    this.result[rcaid].PosB &= [~VERT_UNDER];
                    var yx2 = getyx(rcaid);
                    mem(yx2.y, yx2.x).PosB2 = VERT_UNDER;
                }
            }
        }
        return this.result;
    }
}



<? //****************************************************************************** ?>
<? // ボックスユーティリティ                                                        ?>
<? //****************************************************************************** ?>
var CBoxUtil = function(){}
CBoxUtil.prototype = {

    cloneBox : function(rcaobj){
        if (!rcaobj) { alert("error cloneBox "); return {"boxtype":"", "dest":[]}; }
        var out = {};
        for (var o in rcaobj) if (o!="dest") { out[o] = rcaobj[o]; continue; }
        out.dest = this.cloneBoxDest(rcaobj.dest);
        return out;
    },

    cloneBoxDest : function(dest){
        var out = [];
        for (var d in dest) out.push(dest[d]);
        return out;
    },

    count : function(obj) {
        if (!obj) return 0;
        var ret = 0;
        for (var o in obj) ret++;
        return ret;
    },

    getBoxBehavior : function(boxtype){
        if (!boxtype) return BOX_BEHAVIOR[""];
        return BOX_BEHAVIOR[boxtype] ? BOX_BEHAVIOR[boxtype] : BOX_BEHAVIOR["パス"];
    },

    getBoxWidth : function(Y, X, rca) {
        if (!rca) rca = rcaMap[YX(Y,X)];
        return (rca && rca.w) ? rca.w : cond.getColWidth(X);
    },

    getBoxHeight : function(Y, X, rca) {
        if (!rca) rca = rcaMap[YX(Y,X)];
        return (rca && rca.h) ? rca.h : cond.getRowHeight(Y);
    },

    getBoxText : function(rca) {
        if (!rca.text) return "";
        return rca.text;
    },

    isBox : function(rcaid){
        if (!rcaMap[rcaid]) return false;
        if (!rcaMap[rcaid].boxtype) return false;
        if (!BOX_BEHAVIOR[rcaMap[rcaid].boxtype]) return false;
        return true;
    },

    addDestYX : function(rcaid, drcaid){
        var exist = false;
        var cnt = 0;
        if (!this.isBox(rcaid)) { alert("error addDestYX(1) " + rcaid); return; }
        for (var d in rcaMap[rcaid].dest){
            cnt++;
            var dest = rcaMap[rcaid].dest[d];
            if (dest==drcaid) { exist = true; break; }
        }
        if (!exist) rcaMap[rcaid].dest[cnt] = drcaid;
        this.sortDest(rcaid);
    },

    removeDest : function(rcaid, del_rcaid){
        var curdest = this.cloneBoxDest(rcaMap[rcaid].dest);
        rcaMap[rcaid].dest = [];
        var idx = 0;
        for (var d in curdest){
            if (curdest[d] == del_rcaid) continue;
            rcaMap[rcaid].dest[idx] = curdest[d];
            idx++;
        }
    },

    removeBottomDest : function(y, x){
        var rcaid = YX(y,x);
        var curdest = this.cloneBoxDest(rcaMap[rcaid].dest);
        rcaMap[rcaid].dest = [];
        var idx = 0;
        for (var d in curdest){
            if (getyx(curdest[d]).y>y) continue;
            rcaMap[rcaid].dest[idx] = curdest[d];
            idx++;
        }
    },

    sortDest : function(rcaid){
        if (!this.isBox(rcaid)) { alert("error sortDest(1) " + rcaid); return; }
        var minDests = {};
        var maxDestX = 0;
        for (var d in rcaMap[rcaid].dest){
            var dest = rcaMap[rcaid].dest[d];
            var yx = getyx(dest);
            maxDestX = Math.max(maxDestX, yx.x);
            if (!minDests[yx.x]) minDests[yx.x] = yx.y;
            else if (minDests[yx.x] > yx.y) {
                minDests[yx.x] = yx.y;
            }
        }
        var idx = 0;
        rcaMap[rcaid].dest = [];
        for (var i = 1; i <= maxDestX; i++){
            if (!minDests[i+""]) continue;
            rcaMap[rcaid].dest[idx] = minDests[i+""]+"_"+i;
            idx++;
        }
    },

    moveBoxTo : function(rcaid, incrementY, incrementX, dontTouchDest){
        if (!incrementY && !incrementX) return;
        var yx = getyx(rcaid);
        var newY = parseInt(yx.y) + parseInt(incrementY);
        var newX = parseInt(yx.x) + parseInt(incrementX);
        rcaMap[YX(newY,newX)] = rcaMap[rcaid];
        delete rcaMap[rcaid];

        <? // 接続元を修正してゆく ?>
        if (dontTouchDest) return;
        for (var id in rcaMap){
            if (!bxu.isBox(id)) { alert("error moveBoxTo(1) " + id); continue; }
            for (var d in rcaMap[id].dest) {
                var dest = rcaMap[id].dest[d];
                if (dest == rcaid) {
                    rcaMap[id].dest[d] = newY + "_" + newX;
                    this.sortDest(id);
                }
            }
        }
    },

    isBoxExistInRange : function(minY, minX, maxY, maxX){
        if (minY < 1 || minX < 1) return false;
        if (maxY > cond.rowCount || maxX > cond.colCount) return false;
        for (var y = minY; y <= maxY; y++){
            for (var x = minX; x <= maxX; x++){
                if (this.isBox(YX(y,x))) return true;
            }
        }
        return false;
    },

    <? //--------------------------                        ?>
    <? // 指定要素のX位置から推測される「工程」要素のX位置 ?>
    <? //--------------------------                        ?>
    getVirtualSuperParentX : function(X){
        for (var x=cond.colCount-1; x>0; x--) { if (this.isBox(YX(1,x)) && x <= X) return x; }
        return 1;
    },


    <? //-------------------------- ?>
    <? // 指定要素の親の最上位Yを返す（通常は1、親なしの場合はそれ以上の値） ?>
    <? //-------------------------- ?>
    getRealSuperParentY : function(rcaid){
        var minY = getyx(rcaid).y;
        var nextList = {};
        nextList[rcaid] = rcaMap[rcaid];
        for (;;){
            var pList = {};
            for (var pid in nextList){
                var tmpList = this.getParentList(pid);
                for (var ppid in tmpList){
                    pList[ppid] = tmpList[ppid];
                    minY = Math.min(minY, getyx(ppid).y);
                }
            }
            if (!this.count(pList)) break;
            nextList = {};
            for (var gid in pList) nextList[gid] = pList[gid];
        }
        return minY;
    },

    <? //-------------------------- ?>
    <? // 指定要素の子ノードリスト  ?>
    <? //-------------------------- ?>
    getChildArray : function(rcaid){
        var out = [];
        for (var d in rcaMap[rcaid].dest){
            var drcaid = rcaMap[rcaid].dest[d];
            if (!this.isBox(drcaid)) { alert("error getChildArray(1) " + drcaid); continue; }
            if (isOneOf(rcaMap[drcaid], out)) continue;
            out.push(rcaMap[drcaid]);
        }
        return out;
    },

    <? //-------------------------- ?>
    <? // 指定要素の子ノードリスト(自分を含む)  ?>
    <? //-------------------------- ?>
    getChildIdArray : function(rcaid){
        var out = [];
        for (var d in rcaMap[rcaid].dest) out.push(rcaMap[rcaid].dest[d]);
        return out;
    },

    <? //-------------------------- ?>
    <? // 指定要素の全ての子ノードIDリスト(自分を含む) ?>
    <? //-------------------------- ?>
    getAllChildIdArray : function(rcaid){
        var out = [];
        out.push(rcaid);
        var cnt = 0;
        while (cnt < out.length){
            var dest = rcaMap[out[cnt]].dest;
            for (var d in dest) {
                if (isOneOf(dest[d], out)) continue;
                out.push(dest[d]);
            }
            cnt++;
        }
        return out;
    },

    <? //-------------------------- ?>
    <? // 指定要素の直属親ノードリスト ?>
    <? //-------------------------- ?>
    getParentList : function(rcaid){
        var parentList = {};
        for (var id in rcaMap){
        if (!bxu.isBox(id)) { alert("error getParentList " + id); continue; }
            for (var d in rcaMap[id].dest){
                if (rcaMap[id].dest[d] == rcaid) {
                    parentList[id] = rcaMap[id];
                }
            }
        }
        return parentList;
    },

    <? //-------------------------- ?>
    <? // 結合されている全要素リスト(自分を含む) ?>
    <? //-------------------------- ?>
    getAllAcquaintanceList : function(rcaid){
        var ids = this.getAllChildIdArray(rcaid);
        var out = {};
        for (var rid in ids) out[ids[rid]] = rcaMap[ids[rid]];
        var idx = 0;
        while (idx < ids.length) {
            var parentList = this.getParentList(ids[idx]);
            for (pid in parentList){
                if (out[pid]) continue;
                ids.push(pid);
                out[pid] = parentList[pid];
            }
            idx++;
        }
        return out;
    },

    <? //-------------------------- ?>
    <? // 指定要素の直属親と同一の親を持つノードリスト ?>
    <? //-------------------------- ?>
    getBrotherList : function(rcaid, parents){
        if (!parents) parents = this.getParentList(rcaid);
        var yx = getyx(rcaid);
        var brothersList = {};
        for (var id in parents){
            for (var d in parents[id].dest){
                if (parents[id].dest[d] != rcaid) {
                    var bid = parents[id].dest[d];
                    brothersList[bid] = rcaMap[bid];
                }
            }
        }
        return brothersList;
    },

    <? //-------------------------- ?>
    <? // 指定要素と同じ子を持つノードリスト ?>
    <? //-------------------------- ?>
    getFriendList : function(rcaid){
        var friendsList = {};
        var dest1 = rcaMap[rcaid].dest;
        for (var id in rcaMap){
            if (id==rcaid) continue;
            var dest2 = rcaMap[id].dest;
            for (var d1 in dest1){
                for (var d2 in dest2){
                    if (dest1[d1]!=dest2[d2]) continue;
                    friendsList[id] = rcaMap[id];
                }
            }
        }
        return friendsList;
    },

    <? //-------------------------- ?>
    <? // 自分より親が右の場合は、自分より右と下の要素を右にずらす ?>
    <? //-------------------------- ?>
    moveRightIfParentsAreRight : function(){
        cond.refreshMapWorldScales(true);
        for (;;){
            var isMoved = false;
            for (var ny = 2; ny <= cond.rowCount; ny++){
                for (var nx = 1; nx <= cond.colCount; nx++){
                    if (!bxu.isBox(YX(ny,nx))) continue;

                    var minX = 9999;
                    var parents = bxu.getParentList(YX(ny,nx));
                    for (var p in parents) {
                        minX = Math.min(minX, getyx(p).x);
                    }
                    if (minX <= nx) continue;
                    if (minX == 0 || minX==9999) continue;
                    for (var n2y = cond.rowCount; n2y >= ny; n2y--){
                        for (var n2x = cond.colCount; n2x >= nx; n2x--){
                            if (!bxu.isBox(YX(n2y,n2x))) continue;
                            bxu.moveBoxTo(YX(n2y,n2x), 0, minX - nx);
                            isMoved = true;
                        }
                    }
                    if (!isMoved) continue;
                    cond.refreshMapWorldScales(true);
                    break;
                }
                if (isMoved) break;
            }
            if (!isMoved) break;
        }
    },

    <? //-------------------------- ?>
    <? // 質問ボックスを除去 ?>
    <? //-------------------------- ?>
    eliminateAllSitumonBox : function(){
        for (;;){
            var isEliminated = false;
            for (rcaid in rcaMap){
                if (rcaMap[rcaid].boxtype != "質問") continue;
                btnDelBoxClick(getyx(rcaid));
                isEliminated = true;
                break;
            }
            if (!isEliminated) break;
        }
    },

    <? //------------------------------------------------------------------------------ ?>
    <? // ダンプ                                                                        ?>
    <? //------------------------------------------------------------------------------ ?>
    <? // rcaMap構造 = {                        ?>
    <? //    "y_x":{                              ?>
    <? //        "boxtype":(日本語),                ?>
    <? //        "w":(幅px),                        ?>
    <? //        "h":(高px),                        ?>
    <? //        "text":(テキスト),                 ?>
    <? //        "dest":["y_x", "y_x", "y_x", ....] ?>
    <? //    },  .....                            ?>
    <? // }                                     ?>

    getJsonDump : function(){
        var out = [];
        var cnt = 0;
        for (var rcaid in rcaMap){
            cnt++;
            var rca = rcaMap[rcaid];
            if (!this.isBox(rcaid)) { alert("error getJsonDump(1) " + rcaid); continue; }
            if (cnt > 1) out.push(",");
            out.push('"'+rcaid+'":{');
            if (rca) {
                if (rca.w=="undefined" || rca.h=="undefined") {
                    alert("【エラー】不正なデータがありました。\nこのデータは保存されますが、正しく復旧/再表示されない可能性があります。\n\n恐れ入りますが、管理者に報告ください。(エラー位置："+rcaid+")");
                }
                var rca_text=textEditor.trim(rca.text);
                rca_text = rca_text.replace(new RegExp('"','g'), '\\"');
                out.push('"w":"'+rca.w+'",');
                out.push('"h":"'+rca.h+'",');
                out.push('"boxtype":"'+rca.boxtype+'",');
                out.push('"text":"'+rca_text+'",');
                out.push('"dest":[');
                var cnt2 = 0;
                for (var d in rca.dest){
                    if (cnt2 > 0) out.push(",");
                    out.push('"'+rca.dest[d]+'"');
                    cnt2++;
                }
                out.push(']');
            }
            out.push("}");
        }
        return out.join("");
    },

    getVTextDump : function(){
        var out = [];
        var cnt = 0;
        for (var rcaid in rcaMap){
            cnt++;
            var rca = rcaMap[rcaid];
            if (!this.isBox(rcaid)) { alert("error getVTextDump(1) " + rcaid); continue; }
            if (rca.boxtype!="根本原因") continue;
            if (rca.text=="") continue;
            out.push(textEditor.trim(rca.text));
        }
        return out.join("\t");
    },

    alertRcaDump : function(msg){
        var out = [];
        for (var rcaid in rcaMap){
            var rca = rcaMap[rcaid];
                out.push(msg+"\n");
                out.push('"'+rcaid + '" = {');

            if (rca) {
                out.push('  "w":"'+rca.w+'",');
                out.push('  "h":"'+rca.h+'",');
                out.push('  "boxtype":"'+rca.boxtype+'",');
                out.push('  "text":"'+rca.text+'",');
                out.push('  "dest":[');
                for (var d in rca.dest){
                    out.push('      "'+rca.dest[d]+'" ');
                }
                out.push('  ]');
            }
            out.push('}');
        }
        alert(out.join("\n"));
    },

    gtime : [],
    gmsg : [],
    startST : function(msg){
        gtime = [];
        gmsg = [];
        this.setST(msg);
    },

    setST : function(msg){
        gtime.push(+new Date());
        gmsg.push(msg);
    },

    alertST : function(){
        this.setST("goal");
        var s = [];
        var n = 1;
        while (gtime[n]) { s[n] = n + " " + gmsg[n] + " " + (gtime[n] - gtime[n-1]);; n++; }
        alert(s.join("\n"));
    },

    <? //------------------------------------------------------------------------------ ?>
    <? // 印刷用線、矢印情報を作成する。                                                ?>
    <? // 画面に表示中のオブジェクトのstyleタグ内容などから、位置や内容を割り出す。     ?>
    <? //------------------------------------------------------------------------------ ?>
    getRealLineDumpForPrint : function(){
        var out = [];
        for (var i = 0; i < mapWorld.childNodes.length; i++){
            if (mapWorld.childNodes[i].tagName != "IMG" || mapWorld.childNodes[i].name != "img") continue;
            var sty = mapWorld.childNodes[i].style;
            var idparam = mapWorld.childNodes[i].id.split("_");
            var zy = idparam[1];
            var zx = idparam[2];
            var zp = idparam[3];
            var pw = parseInt(sty.width);
            var ph = parseInt(sty.height);
            var abs_l = parseInt(sty.left) - MAP_PADDING;
            var abs_t = parseInt(sty.top) - MAP_PADDING;
            var zw = pw;
            var zh = ph;
            var rel_l = abs_l - cond.getColX(zx);
            var rel_t = abs_t - cond.getRowY(zy);
            if (zp=="PosR" || zp=="PosRB") rel_l = rel_l - cond.getColWidth(zx);
            if (zp=="PosB" || zp=="PosB2" || zp=="PosRB") rel_t = rel_t - cond.getRowHeight(zy);
            if (zw==1) zw = 0;
            if (zh==1) zh = 0;
            out.push(mapWorld.childNodes[i].id + "," + rel_l + "," + rel_t + "," + zw + "," + zh + "," + abs_l + "," + abs_t);
        }
        return out.join(" ");
    }
}

<? //****************************************************************************** ?>
<? // 線単体の追加、線の結合関連                                                    ?>
<? //****************************************************************************** ?>
var CLineJoinner = function(){
    this.border.g.t = document.getElementById("border_ht");
    this.border.g.l = document.getElementById("border_hl");
    this.border.g.r = document.getElementById("border_hr");
    this.border.g.b = document.getElementById("border_hb");
    this.border.e.t = document.getElementById("border_ft");
    this.border.e.l = document.getElementById("border_fl");
    this.border.e.r = document.getElementById("border_fr");
    this.border.e.b = document.getElementById("border_fb");
    this.border.r.t = document.getElementById("border_st");
    this.border.r.l = document.getElementById("border_sl");
    this.border.r.r = document.getElementById("border_sr");
    this.border.r.b = document.getElementById("border_sb");
    this.lineJoinnerSelector = document.getElementById("line_joinner_selector");
    this.joinMessage = document.getElementById("join_message");
}
CLineJoinner.prototype = {
    targetY: "",
    targetX: "",
    border : {"g":{}, "e":{}, "r":{}},
    TLRB : ["t","l","r","b"],

    startJoin : function (){
        var yx = cond.getBoxYXP();
        this.targetY = yx.y;
        this.targetX = yx.x;
        cond.resetMenu();
        for (var pos in this.TLRB){
            this.border["r"][this.TLRB[pos]].style.width = px(6);
            this.border["r"][this.TLRB[pos]].style.height = px(6);
            this.border["r"][this.TLRB[pos]].style.display = "";
            cond.mountToMapWorld(this.border["r"][this.TLRB[pos]]);
        }
        this.locateFrame(yx.y, yx.x, "r", rcaMap[YX(yx.y,yx.x)]);
        this.lineJoinnerSelector.style.display = "";
    },

    isJoinning : function (){
        return (this.targetY || this.targetX) ? true : false;
    },

    execJoin : function (destY, destX){
        var joinnable = this.isJoinable(destY, destX, "box"); <? // cross / brother / link_direct / link_outside / no-parent ?>
        if (!joinnable) return;
        var rcaid = YX(this.targetY, this.targetX);
        var drcaid = YX(destY, destX);

        var destParents = bxu.getParentList(drcaid);
        var destBrothers = bxu.getBrotherList(YX(destY, destX), destParents);
        var srcChildIds = bxu.getChildIdArray(rcaid);
        var destFriends = bxu.getFriendList(drcaid);
        var allDestChildIds = bxu.getAllChildIdArray(drcaid);
        var allDestAcquaintanceList = bxu.getAllAcquaintanceList(drcaid);
        var allSrcAcquaintanceList = bxu.getAllAcquaintanceList(rcaid);

        <? // 接続先が完全に親なしかどうか ?>
        var destBounds = {"l":destX,"r":destX,"t":destY,"b":destY};
        for (aid in allDestAcquaintanceList){
            var cyx = getyx(aid);
//          if (cyx.y==1) continue;
            destBounds.l = Math.min(destBounds.l, cyx.x);
            destBounds.r = Math.max(destBounds.r, cyx.x);
            destBounds.t = Math.min(destBounds.t, cyx.y);
            destBounds.b = Math.max(destBounds.b, cyx.y);
        }

        <? //============== ?>
        <? // 接続          ?>
        <? //============== ?>
        bxu.addDestYX(YX(this.targetY, this.targetX), drcaid);
        var rca = rcaMap[rcaid];
        <? // 接続先のもともとの親は、接続元の子を接続する ?>
        for (var pid in destParents){
            for (var d in rca.dest) bxu.addDestYX(pid, rca.dest[d]);
        }
        <? // 接続元の子の全ての親は、接続先に接続する ?>
        for (var d in rcaMap[rcaid].dest){
            var did = rcaMap[rcaid].dest[d];
            var dparents = bxu.getParentList(did);
            for (var dpid in dparents){
                bxu.addDestYX(dpid, drcaid);
            }
        }
        <? // 接続元は、接続先の全ての兄弟に接続する ?>
        for (var bid in destBrothers) bxu.addDestYX(rcaid, bid);

        <? //============== ?>
        <? // 親なしシフト  ?>
        <? //============== ?>
        if (destBounds.t > 1){
            var shiftX = destX - this.targetX;
            var srcBounds = {"l":this.targetX,"r":this.targetX,"t":this.targetY,"b":this.targetY};
            for (var cidx in allSrcAcquaintanceList){
                var cyx = getyx(cidx);
                if (cyx.y==1) continue;
                srcBounds.l = Math.min(srcBounds.l, cyx.x);
                srcBounds.r = Math.max(srcBounds.r, cyx.x);
                srcBounds.t = Math.min(srcBounds.t, cyx.y);
                srcBounds.b = Math.max(srcBounds.b, cyx.y);
            }

            <? // 接続先が左側の場合は子以下をすべて移動 ?>
            var parentMoveStartX = 0;
            if (shiftX < 0) parentMoveStartX = this.targetX;
            <? // 接続先が右側かつ接続先の子よりさらに右側の場合は一番右の子の列 ?>
            else if (destBounds.l > srcBounds.r) parentMoveStartX = plus1(srcBounds.r);
            <? // 接続先が子の中間に位置する場合 ?>
            else parentMoveStartX = destX;
            <? // 接続元の以下と接続元より右を右に移動 ?>
            var parentMoveWidth = plus1(destBounds.r - destBounds.l);
            var childLocateWidth  = parentMoveStartX - destX + parseInt(destX - destBounds.l);
            var childLocateHeight  =  plus1(this.targetY - destY);
            <? // 退避 ?>
            var allDestAcquaintanceIdList = [];
            for (var cid in allDestAcquaintanceList){
                allDestAcquaintanceIdList.push(cid);
                bxu.moveBoxTo(cid, 1000, 1000);
            }
            for (var ny = 1; ny <= cond.rowCount; ny++){
                for (var nx = cond.colCount; nx >= parentMoveStartX; nx--){
                    if (!bxu.isBox(YX(ny,nx))) continue;
                    if (nx==this.targetX){
                        if (ny <= this.targetY) continue;<? // 親より上は移動しない ?>
                    }
                    if (isOneOf(YX(ny,nx), allDestAcquaintanceIdList)) continue; <? // 子がまぎれている場合も移動しない ?>
                    bxu.moveBoxTo(YX(ny,nx), 0, parentMoveWidth);
                }
            }
            <? // 接続先が接続先集団の左端でない場合は接続先側を右に移動 ?>
            if (destX > destBounds.l) {
                bxu.moveBoxTo(YX(this.targetY,this.targetX), 0, plus1(destX - destBounds.l));
            }

            <? // 接続先の以下を移動 ?>
            for (var cid in allDestAcquaintanceList){
                var yx = getyx(cid);
                bxu.moveBoxTo(YX(parseInt(yx.y+1000), parseInt(yx.x+1000)), childLocateHeight-1000, childLocateWidth-1000);
            }
            cond.refreshMapWorldScales(true);
            this.abort();
            cond.isDrawing = true;
            adjustSpace();
            cond.refreshMapWorldScales(false);
            drawMap(lineRouteCalculator.calculate(true));
            cond.isDrawing = false;
            return;
        }



        <? //============== ?>
        <? // 縦軸シフト    ?>
        <? //============== ?>

        var shiftY = 0;
        <? // 接続先Yが接続元Yより等しいか小さい場合は、接続元次の行へ ?>
        if (this.targetY >= destY) shiftY = this.targetY - destY*1 + 1*1;
        if (joinnable != "link_direct"){
            <? // 接続元と接続先の間に列がある場合は、間の列の一番下よりさらに下の行へ ?>
            if (this.targetX - destX > 1 || this.targetX - destX < -1){
                var sx = plus1(Math.min(this.targetX, destX));
                var ex = Math.max(this.targetX, destX)-1;
                var sy = Math.max(this.targetY, destY);
                for (var nx = sx; nx <= ex; nx++){
                    for (var ny = sy; ny <= cond.rowCount; ny++){
                        if (bxu.isBox(YX(ny,nx))) shiftY = Math.max(shiftY, ny - destY*1 + 1*1);
                    }
                }
            }
            <? // 少なくとも接続元の直接子と並ぶように ?>
            var minChildY = 9999;
            for (var d in rcaMap[rcaid].dest) minChildY = Math.min(minChildY, getyx(rcaMap[rcaid].dest[d]).y);
            if (minChildY < 9999) shiftY = Math.max(shiftY, minChildY - destY);
        }

        <? // 接続元より下にある要素か、接続先の子要素はすべて下にずらす。 ?>

        if (shiftY){
            <? // 接続先の兄弟とすべての子供を取得（値の計算のみ） ?>
            var childDestMaxX = destX;
            var childDestMinX = destX;
            var cinc = 0;
            var allDestChild = [];
            allDestChild.push(YX(destY, destX));
            for (var bid in destBrothers) allDestChild.push(bid);
            while (cinc < allDestChild.length){
                var dest = rcaMap[allDestChild[cinc]].dest;
                if (!dest) continue;
                for (var d in dest) {
                    allDestChild.push(dest[d]);
                    childDestMaxX = Math.max(getyx(dest[d]).x, childDestMaxX);
                    childDestMinX = Math.min(getyx(dest[d]).x, childDestMinX);
                }
                cinc++;
            }
            var oAllDestChild = {};
            for (var i in allDestChild) oAllDestChild[allDestChild[i]] = true;

            <? // 接続先の兄弟と子供以外を全体的に下にずらす ?>
            for (var ny = cond.rowCount; ny >= destY; ny--){
                for (var nx = 1; nx <= cond.colCount; nx++){
                    if (!oAllDestChild[YX(ny,nx)]) continue;
                    bxu.moveBoxTo(YX(ny,nx), shiftY, 0);
                    delete oAllDestChild[YX(ny,nx)];
                }
            }
            destY = destY*1 + shiftY*1;
            cond.refreshMapWorldScales(true);
        }

        <? // 接続元より下を、総合的に下にずらす ?>
        for (var sny = this.targetY*1+1*1; sny <= cond.rowCount; sny++) {
            if (!bxu.isBox(YX(sny, this.targetX))) continue;
            var shiftY2 = destY - sny;
            if (shiftY2 < 1) break;
            var cinc = 0;
            var allSrcChild = [];
            allSrcChild.push(YX(sny, this.targetX));
            while (cinc < allSrcChild.length){
                var dest = rcaMap[allSrcChild[cinc]].dest;
                if (dest) for (var d in dest) allSrcChild.push(dest[d]);
                cinc++;
            }
            var oAllSrcChild = {};
            for (var i in allSrcChild) oAllSrcChild[allSrcChild[i]] = true;
            for (var ny = cond.rowCount; ny >= this.targetY; ny--){
                for (var nx = 1; nx <= cond.colCount; nx++){
                    if (!oAllSrcChild[YX(ny,nx)]) continue;
                    bxu.moveBoxTo(YX(ny,nx), shiftY2, 0);
                    delete oAllSrcChild[YX(ny,nx)];
                }
            }
            cond.refreshMapWorldScales(true);
            break;
        }

        if (shiftY){
            <? // 接続先の兄弟と子供以外を全体的に右にずらす ?>
            for (var nx = cond.colCount; nx >= childDestMinX; nx--){
                for (var ny = destY*1 + shiftY*1; ny <= cond.rowCount; ny++){
                    if (!bxu.isBox(YX(ny,nx))) continue;
                    if (oAllDestChild[YX(ny,nx)]) continue;
                    bxu.moveBoxTo(YX(ny,nx), 0, plus1(childDestMaxX-childDestMinX));
                }
            }
            cond.refreshMapWorldScales(true);
        }

        this.abort();
        cond.isDrawing = true;
        adjustSpace();
        cond.refreshMapWorldScales(false);
        drawMap(lineRouteCalculator.calculate(true));
        cond.isDrawing = false;
    },

    setJoinStyle : function(posy, posx, position){
        if (!bxu.isBox(YX(posy, posx)) || position!="box"){
            this.hideFrame();
            this.joinMessage.style.color = "#7af";
            this.joinMessage.innerHTML = "接続先の要素を選択してください。";
            return;
        }
        if (this.targetY==posy && this.targetX==posx){
            this.hideFrame();
            this.joinMessage.style.color = "#f77";
            this.joinMessage.innerHTML = "ここから接続する要素を選択してください。";
            return;
        }

        var rca = rcaMap[YX(posy, posx)];
        var joinnable = this.isJoinable(posy, posx, position);

        var e_ok = (joinnable ? "e" : "g");
        var e_ng = (e_ok == "e" ? "g" : "e");
        for (var pos in this.TLRB){
            this.border[e_ok][this.TLRB[pos]].style.width = px(6);
            this.border[e_ok][this.TLRB[pos]].style.height = px(6);
            cond.mountToMapWorld(this.border[e_ok][this.TLRB[pos]]);
            cond.unmountFromMapWorld(this.border[e_ng][this.TLRB[pos]]);
        }
        this.locateFrame(posy, posx, e_ok, rca);
    },

    locateFrame : function(posy, posx, clr, rca){
        var boxHeight = bxu.getBoxHeight(posy, posx, rca);
        var boxWidth    = bxu.getBoxWidth(posy, posx, rca);
        this.border[clr].t.style.top     =  px(cond.getRowY(posy)-6 + MAP_PADDING);
        this.border[clr].t.style.left  =    px(cond.getColX(posx)-6 + MAP_PADDING);
        this.border[clr].t.style.width =    px(boxWidth + 12);
        this.border[clr].l.style.top =      px(cond.getRowY(posy)-6 + MAP_PADDING);
        this.border[clr].l.style.left =     px(cond.getColX(posx)-6 + MAP_PADDING);
        this.border[clr].l.style.height = px(boxHeight + 12);
        this.border[clr].b.style.top     =  px(cond.getRowY(posy) + MAP_PADDING + boxHeight);
        this.border[clr].b.style.left  =    px(cond.getColX(posx)-6 + MAP_PADDING);
        this.border[clr].b.style.width =    px(boxWidth + 12);
        this.border[clr].r.style.top =      px(cond.getRowY(posy)-6 + MAP_PADDING);
        this.border[clr].r.style.left =     px(cond.getColX(posx) + MAP_PADDING + boxWidth);
        this.border[clr].r.style.height = px(boxHeight + 12);
    },

    isJoinable : function(destY, destX, position){
        var rcaid = YX(this.targetY, this.targetX);
        var drcaid = YX(destY, destX);
        var rca = rcaMap[rcaid];

        var err = "";
        <? // 結線不可（その１）?>
        if (position != "box") err = "<br/>";
        <? // 結線不可（その２）?>
        else if (!bxu.isBox(drcaid)) err = "この位置へは接続できません。";
        <? // 結線不可（その３）?>
        else if (isOneOf(rcaMap[drcaid].boxtype, ["パス","工程","結果"])) err = "「"+rcaMap[drcaid].boxtype + "」要素へは接続できません。";
        if (err) { this.joinMessage.style.color = "#999"; this.joinMessage.innerHTML = err; return ""; }

        var srcParentList = bxu.getParentList(rcaid);
        var destParentList = bxu.getParentList(drcaid);

        var nextParentList = {};
        nextParentList[rcaid] = rcaMap[rcaid];

        for (;;){
            var grandList = {};
            for (var pid in nextParentList){
                var tmpList = bxu.getParentList(pid);
                for (var ppid in tmpList){
                    if (ppid==drcaid){
                        <? // 結線不可（その４）?>
                        this.joinMessage.style.color = "#999";
                        this.joinMessage.innerHTML = "親要素へは接続できません。";
                        return "";
                    }
                    for (var d in tmpList[ppid].dest){
                        if (tmpList[ppid].dest[d]==YX(destY,destX)){
                            <? // 結線不可（その５）?>
                            this.joinMessage.style.color = "#999";
                            this.joinMessage.innerHTML = "接続元の親要素と直接接続されている要素へは、接続できません。";
                            return "";
                        }
                    }
                    grandList[ppid] = tmpList[ppid];
                }
            }
            if (!bxu.count(grandList)) break;
            nextParentList = {};
            for (var gid in grandList) nextParentList[gid] = grandList[gid];
        }

        for (var d in rca.dest) {
            if (rca.dest[d]==YX(destY,destX)) {
                <? // 結線不可（その６）?>
                this.joinMessage.style.color = "#999";
                this.joinMessage.innerHTML = "既に接続されています。";
                return "";
            }
        }

        var nextChildList = {};
        nextChildList[rcaid] = rcaMap[rcaid];

        for (;;){
            var grandList = {};
            for (var pid in nextChildList){
                for (var d in nextChildList[pid].dest){
                    var ddid = nextChildList[pid].dest[d];
                    if (ddid==drcaid){
                        <? // 結線不可（その７）?>
                        this.joinMessage.style.color = "#999";
                        this.joinMessage.innerHTML = "子要素へ直接接続する矢印線は、作成できません。";
                        return "";
                    }
                    grandList[ddid] = rcaMap[ddid];
                }
            }
            if (!bxu.count(grandList)) break;
            nextChildList = {};
            for (var gid in grandList) nextChildList[gid] = grandList[gid];
        }

        nextChildList = bxu.getBrotherList(drcaid);
        nextChildList[drcaid] = rcaMap[drcaid];

        for (;;){
            var grandList = {};
            for (var pid in nextChildList){
                for (var d in nextChildList[pid].dest){
                    var ddid = nextChildList[pid].dest[d];
                    for (var dd in rca.dest){
                        if (ddid==rca.dest[dd]){
                            <? // 結線不可（その８）?>
                            this.joinMessage.style.color = "#999";
                            this.joinMessage.innerHTML = "接続済み子要素の親系統へは、接続できません。";
                            return "";
                        }
                    }
                    grandList[ddid] = rcaMap[ddid];
                }
            }
            if (!bxu.count(grandList)) break;
            nextChildList = {};
            for (var gid in grandList) nextChildList[gid] = grandList[gid];
        }

        var allDestAcquaintanceList = bxu.getAllAcquaintanceList(drcaid);
        var destBounds = {"l":destX,"r":destX,"t":destY,"b":destY};
        for (aid in allDestAcquaintanceList){
            var cyx = getyx(aid);
            destBounds.l = Math.min(destBounds.l, cyx.x);
            destBounds.r = Math.max(destBounds.r, cyx.x);
            destBounds.t = Math.min(destBounds.t, cyx.y);
            destBounds.b = Math.max(destBounds.b, cyx.y);
        }

        var minDestParentY = destBounds.t;
        if (minDestParentY > 1){
            if (minDestParentY != destY){
                <? // 結線不可（その９）?>
                this.joinMessage.style.color = "#999";
                this.joinMessage.innerHTML = "「工程」要素から分断された系列の子要素へは、接続できません。";
                return "";
            }
        } else {
            var minSrcParentY = bxu.getRealSuperParentY(rcaid);
            if (minSrcParentY > 1) {
                <? // 結線不可（その１０）?>
                this.joinMessage.style.color = "#999";
                this.joinMessage.innerHTML = "「工程」要素から分断された系列から、「工程」要素への系列へは、接続できません。";
                return "";
            }
        }
        if (destX != this.targetX) {
            var minSrcChildX = plus1(this.targetX);
            for (d in rca.dest) {
                var nyx = getyx(rca.dest[d]);
                minSrcChildX = Math.min(minSrcChildX, nyx.x);
            }
            var destCount = 0;
            for (var d in rca.dest) if (getyx(rca.dest[d]).y > 1) destCount++;
            if (destCount && minSrcChildX > this.targetX) {
                    <? // 結線不可（その１１）?>
                this.joinMessage.style.color = "#999";
                this.joinMessage.innerHTML = "子要素が親要素より右側にしか存在しない要素からは、接続できません。";
                return "";
            }
        }
        if (this.targetY == 1 && (destX != this.targetX)){
            for (var ny = plus1(this.targetY); ny <= cond.rowCount; ny++){
                if (!bxu.isBox(YX(ny, this.targetX))) continue;
                var minUnderBoxParentY = bxu.getRealSuperParentY(YX(ny,this.targetX));
                if (minUnderBoxParentY > 1){
                    <? // 結線不可（その１２）?>
                    this.joinMessage.style.color = "#999";
                    this.joinMessage.innerHTML = "「工程」要素の真下が分断された系列の場合は、その他要素へは接続できません。";
                    return "";
                }
                break;
            }
        }

        <? // 結線不可（その１３）?>
        if (bxu.getVirtualSuperParentX(this.targetX) != bxu.getVirtualSuperParentX(destX)) {
            if (minDestParentY == 1) {
                this.joinMessage.style.color = "#999";
                this.joinMessage.innerHTML = "異なる「工程」に含まれる要素へは、接続できません。(1)";
                return "";
            }
        }



        if (minDestParentY == 1) { <? // 親なしでなければ ?>
            var lines = lineRouteCalculator.getResult();
            var isLinkDirect = true;
            var isLinkNG = false;

            <? // 左下側へ接続 ?>
            if (this.targetX - destX > 0){
                <? // ダイレクトに接続できるか ?>
                for (var nx = plus1(destX); nx <= this.targetX; nx++) {
                    if (lines[YX(destY,nx)]){
                        var pb = lines[YX(destY,nx)].PosB;
                        if (pb == [pb | HORZ_L_UPPER]) isLinkDirect = false;
                        if (pb == [pb | VERT_UPPER]) isLinkDirect = false;
                    }
                    if (destY - this.targetY > 1){
                        for (var ny = this.targetY; ny <= destY-1; ny++) {
                            if (lines[YX(ny,nx)]){
                                var pb = lines[YX(ny,nx)].PosB;
                                if (pb == [pb | HORZ_L_UPPER]) isLinkDirect = false;
                            }
                        }
                    }
                }
                for (var ny = plus1(destY); ny < this.targetY; ny++) {
                    if (bxu.isBox(YX(ny, this.targetX))) isLinkDirect = false;
                    if (lines[YX(ny,this.targetX)]){
                        var pb = lines[YX(destY,nx)].PosB;
                        if (pb == [pb | HORZ_L_UNDER]) isLinkDirect = false;
                        if (pb == [pb | HORZ_L_UPPER]) isLinkDirect = false;
                    }
                }
                <? // 下に回りこめるか ?>
                if (!isLinkDirect){
                    <? // 接続元が袋小路ならNG(1) ?>
                    var maxRowWall = destY-1;
                    for (var ny = destY; ny <= cond.rowCount; ny++) {
                        for (var nx = plus1(destX); nx <= this.targetX-1; nx++) {
                            if (bxu.isBox(YX(ny,nx))) maxRowWall = Math.max(maxRowWall, ny);
                        }
                    }
                    for (var ny = this.targetY; ny <= maxRowWall; ny++) {
                        isLinkNG = false;
                        if (lines[YX(ny,this.targetX)]){
                            var pb = lines[YX(ny,this.targetX)].PosB;
                            if (pb == [pb | HORZ_L_UPPER]) {
                                if (plus1(ny) < maxRowWall) isLinkNG = true;
                                else if (this.targetX-destX==1) {}
                                else {
                                    for (var nx = this.targetX-1; nx > destX; nx--) {
                                        pb = lines[YX(ny,nx)].PosB;
                                        if (pb == [pb | HORZ_L_UPPER]) continue;
                                        isLinkNG = true;
                                    }
                                }
                            }
                        }
                        if (isLinkNG) {
                        <? // 結線不可（その１４）?>
                            this.joinMessage.style.color = "#999";
                            this.joinMessage.innerHTML = "交差する矢印線(1)は作成できません。";
                            return "";
                        }
                    }
                    <? // 接続先が袋小路ならNG(2) ?>
                    for (var ny = destY; ny <= cond.rowCount; ny++) {
                        isLinkNG = false;
                        if (lines[YX(ny,destX)]){
                            var pb = lines[YX(ny,destX)].PosB;
                            if (pb == [pb | HORZ_R_UPPER]) isLinkNG = true;
                        }
                        if (isLinkNG){
                            isLinkNG = false;
                            for (var nx = plus1(destX); nx <= this.targetX-1; nx++) {
                                if (lines[YX(destY-1,nx)]){
                                    var pb = lines[YX(destY-1,nx)].PosB;
                                    if (pb == [pb | VERT_CENTER]) isLinkNG = true;
                                    if (pb == [pb | HORZ_L_UNDER]) isLinkNG = true;
                                }
                                if (bxu.isBox(YX(destY,nx))) isLinkNG = true;
                            }
                        }
                        if (isLinkNG) {
                            <? // 結線不可（その１５）?>
                            this.joinMessage.style.color = "#999";
                            this.joinMessage.innerHTML = "交差する矢印線(2)は作成できません。";
                            return "";
                        }
                    }
                }
            }
            <? // 右下側へ接続 ?>
            if (this.targetX - destX < 0){
                <? // ダイレクトに接続できるか ?>
                for (var nx = plus1(this.targetX); nx <= destX; nx++) {
                    if (lines[YX(this.targetY,nx)]){
                        var pb = lines[YX(this.targetY,nx)].PosB;
                        if (pb == [pb | HORZ_L_UPPER]) isLinkDirect = false;
                        if (pb == [pb | VERT_UPPER]) isLinkDirect = false;
                    }
                }
                for (var ny = plus1(this.targetY); ny < destY; ny++) {
                    if (bxu.isBox(YX(ny, destX))) isLinkDirect = false;
                    if (lines[YX(this.targetY,nx)]){
                        var pb = lines[YX(this.targetY,nx)].PosB;
                        if (pb == [pb | HORZ_L_UNDER]) isLinkDirect = false;
                        if (pb == [pb | HORZ_L_UPPER]) isLinkDirect = false;
                    }
                }

                <? // 下に回りこめるか ?>
                if (!isLinkDirect){
                    <? // 接続元が袋小路ならNG(3) ?>
                    var maxRowWall = destY-1;
                    for (var ny = destY; ny <= cond.rowCount; ny++) {
                        for (var nx = plus1(this.targetX); nx <= destX-1; nx++) {
                            if (bxu.isBox(YX(ny,nx))) maxRowWall = Math.max(maxRowWall, ny);
                        }
                    }

                    for (var ny = this.targetY; ny <= maxRowWall; ny++) {
                        isLinkNG = false;
                        if (lines[YX(ny,this.targetX)]){
                            var pb = lines[YX(ny,this.targetX)].PosB;
                            if (pb == [pb | HORZ_R_UNDER]) {
                                if (plus1(ny) < maxRowWall) isLinkNG = true;
                                else if (this.targetX-destX==-1) {}
                                else {
                                    for (var nx = destX-1; nx > this.targetX; nx--) {
                                        pb = lines[YX(ny,nx)].PosB;
                                        if (pb == [pb | HORZ_R_UNDER]) continue;
                                        isLinkNG = true;
                                    }
                                }
                            }
                        }
                        if (isLinkNG){
                            <? // 結線不可（その１６）?>
                            this.joinMessage.style.color = "#999";
                            this.joinMessage.innerHTML = "交差する矢印線(3)は作成できません。";
                            return "";
                        }
                    }
                    <? // 接続先が袋小路ならNG(4) ?>
                    for (var ny = destY; ny <= cond.rowCount; ny++) {
                        isLinkNG = false;
                        if (lines[YX(ny, destX)]){
                            var pb = lines[YX(ny,destX)].PosB;
                            if (pb == [pb | HORZ_L_UPPER]) isLinkNG = true;
                        }
                        if (isLinkNG){
                            isLinkNG = false;
                            for (var nx = plus1(this.targetX); nx <= destX-1; nx++) {
                                if (lines[YX(destY-1,nx)]){
                                    var pb = lines[YX(destY-1,nx)].PosB;
                                    if (pb == [pb | VERT_CENTER]) isLinkNG = true;
                                    if (pb == [pb | HORZ_L_UNDER]) isLinkNG = true;
                                }
                                if (bxu.isBox(YX(destY,nx))) isLinkNG = true;
                            }
                        }
                        if (isLinkNG){
                            <? // 結線不可（その１７）?>
                            this.joinMessage.style.color = "#999";
                            this.joinMessage.innerHTML = "交差する矢印線(4)は作成できません。";
                            return "";
                        }
                    }
                }
            }
            <? // 真下へ接続 ?>
            if (this.targetX == destX){
                <? // ダイレクトに接続できるか ?>
                isLinkDirect = true;
                for (var ny = this.targetY; ny < destY; ny++) {
                    if (ny == this.targetY){
                        if (pb != [pb | VERT_UPPER]) {
                            if ((pb==[pb | HORZ_L_UPPER]) || (pb==[pb | HORZ_R_UPPER]) || (pb==[pb | HORZ_L_UNDER]) || (pb==[pb | HORZ_R_UNDER])) isLinkDirect = false;
                        }
                    }
                    else {
                        if (bxu.isBox(YX(ny, this.targetX))) isLinkDirect = false;
                        if (lines[YX(ny,this.targetX)]){
                            var pb = lines[YX(ny,this.targetX)].PosB;
                            if (pb == [pb | HORZ_L_UNDER]) isLinkDirect = false;
                            if (pb == [pb | HORZ_L_UPPER]) isLinkDirect = false;
                        }
                    }
                }
                if (!isLinkDirect){
                    <? // 結線不可（その１８）?>
                    this.joinMessage.style.color = "#999";
                    this.joinMessage.innerHTML = "交差する矢印線(5)は作成できません。";
                    return "";
                }
            }
        }

        <? // 結線不可（その１９）?>
        if (bxu.getVirtualSuperParentX(this.targetX) != bxu.getVirtualSuperParentX(destX)) {
            if (destBounds.t == 1) {
                this.joinMessage.style.color = "#999";
                this.joinMessage.innerHTML = "異なる「工程」に含まれる要素へは、接続できません。(2)";
                return "";
            }
        }


        var destParentMinX = destX;
        for (var pid in destParentList) destParentMinX = Math.min(destParentMinX, getyx(pid).x);
        if (destParentMinX < destX || !bxu.count(destParentList)){
            <? // 結線不可（その２０）?>
            if (destBounds.t>1) {
                srcParentMinX = this.targetX;
                for (var pid in srcParentList) if (getyx(pid).y > 1) srcParentMinX = Math.min(srcParentMinX, getyx(pid).x);
                var destXDist = destX - destBounds.l;
                var srcXDist = this.targetX - srcParentMinX;
                if (srcXDist < destXDist) {
                    this.joinMessage.style.color = "#999";
                    this.joinMessage.innerHTML = "この要素へは接続できません。(1)";
                    return "";
                }
            }
        }


        if (bxu.count(destParentList)){
            if (rca.dest.length){
                this.joinMessage.style.color = "#0a0";
                this.joinMessage.innerHTML = "複数の接続元から、複数の接続先への矢印線が、同時に作成されます。";
                return "cross";
            }
        }
        var dbrothers = bxu.getBrotherList(drcaid, destParentList);
        if (bxu.count(dbrothers)) {
            this.joinMessage.style.color = "#0a0";
            this.joinMessage.innerHTML = "複数の接続先への矢印線が、同時に作成されます。";
            return "brother";
        }

        this.joinMessage.style.color = "#0a0";
        this.joinMessage.innerHTML = "接続できます。" + (isLinkDirect ?"(1)":"") + (!isLinkDirect && !isLinkNG ? "(2)":"");
        if (isLinkDirect) return "link_direct";
        if (!isLinkNG) return "link_outside";
        return "ok";
    },

    hideFrame : function(is_hide_all){
        for (var pos in this.TLRB){
            cond.unmountFromMapWorld(this.border["e"][this.TLRB[pos]]);
            cond.unmountFromMapWorld(this.border["g"][this.TLRB[pos]]);
            if (is_hide_all) cond.unmountFromMapWorld(this.border["r"][this.TLRB[pos]]);
        }
    },

    abort : function (){
        this.targetY = "";
        this.targetX = "";
        this.hideFrame(true);
        this.lineJoinnerSelector.style.display = "none";
    }
}

<? //****************************************************************************** ?>
<? // ボックスリサイズ関連                                                          ?>
<? //****************************************************************************** ?>
var CBoxResizer = function(){
    this.border.r.t = document.getElementById("border_rt");
    this.border.r.l = document.getElementById("border_rl");
    this.border.r.r = document.getElementById("border_rr");
    this.border.r.b = document.getElementById("border_rb");
    this.border.e.t = document.getElementById("border_et");
    this.border.e.l = document.getElementById("border_el");
    this.border.e.r = document.getElementById("border_er");
    this.border.e.b = document.getElementById("border_eb");
    this.border_v = document.getElementById("resizer_border_v");
    this.border_h = document.getElementById("resizer_border_h");
    this.okng = document.getElementById("resizer_ok_ng");
    this.btnResizer = document.getElementById("btn_resizer");
    this.pixelStepSelector = document.getElementById("resize_pixel_step_selector");
}
CBoxResizer.prototype = {
    targetY: "",
    targetX: "",
    startLeft: 0,
    startTop : 0,
    endRight : 0,
    endBottom : 0,
    pixelStep: 1,
    isBorderActive : false,
    border : {"r":{}, "e":{}},
    TLRB : ["t","l","r","b"],
    isbuttonShowing : false,

    showButton : function(Y, X){
        this.btnResizer.style.left = px(cond.getColX(X) + MAP_PADDING + bxu.getBoxWidth(Y, X) - 20);
        this.btnResizer.style.top = px(cond.getRowY(Y) + MAP_PADDING + bxu.getBoxHeight(Y, X) - 20);
        if (this.isButtonShowing) return;
        this.isButtonShowing = true;
        this.btnResizer.style.position = "absolute";
        btnBlur(this.btnResizer);
        cond.mountToMapWorld(this.btnResizer);
    },

    hideButton : function(){
        this.isButtonShowing = false;
        cond.unmountFromMapWorld(this.btnResizer);
    },

    startResize : function (){
        var yx = cond.getBoxYXP();
        this.hideButton();
        cond.hideMenu();
        this.targetY = yx.y;
        this.targetX = yx.x;
        this.startTop = cond.getRowY(yx.y);
        this.startLeft = cond.getColX(yx.x);
        this.dockBorder(true);

        this.border.r.t.style.top =  px(this.startTop + MAP_PADDING);
        this.border.r.t.style.left = px(this.startLeft + MAP_PADDING);
        this.border.r.l.style.top =  px(this.startTop + MAP_PADDING);
        this.border.r.l.style.left = px(this.startLeft + MAP_PADDING);
        this.border.r.r.style.top =  px(this.startTop + MAP_PADDING);
        this.border.r.b.style.left =    px(this.startLeft + MAP_PADDING);
        for (var pos in this.TLRB){
            this.border["e"][this.TLRB[pos]].style.top = this.border["r"][this.TLRB[pos]].style.top;
            this.border["e"][this.TLRB[pos]].style.left = this.border["r"][this.TLRB[pos]].style.left;
            this.border["e"][this.TLRB[pos]].style.width = px(1);
            this.border["e"][this.TLRB[pos]].style.height = px(1);
            this.border["r"][this.TLRB[pos]].style.width = px(1);
            this.border["r"][this.TLRB[pos]].style.height = px(1);
        }

        this.border_v.style.top  =  px(0);
        this.border_v.style.height =    px(getHeight(mapWorld));
        this.border_h.style.left =  px(0);
        this.border_h.style.width =  px(getWidth(mapWorld));

        var mouseYX = cond.getMouseYX();
        this.setBorderPos(mouseYX.y, mouseYX.x);
        this.pixelStepSelector.style.display = "";
    },

    isResizing : function (){
        return (this.targetY || this.targetX) ? true : false;
    },

    execResize : function (){
        if (!this.isSquareableSize()) return;
        var rcaid = YX(this.targetY, this.targetX);
        if (!bxu.isBox(rcaid)) { this.abort(); return; }
        if ((rcaMap[rcaid].w == this.endRight - this.startLeft) && (rcaMap[rcaid].h == (this.endBottom - this.startTop))) {
            this.abort();
            return;
        }
        rcaMap[rcaid].w = this.endRight - this.startLeft;
        rcaMap[rcaid].h = this.endBottom - this.startTop;
        var Y = this.targetY;
        var X = this.targetX;
        this.abort();
        cond.isDrawing = true;
        adjustSpace();
        cond.refreshMapWorldScales(false);
        drawMap(lineRouteCalculator.getResult(), true);
        this.isButtonShowing = false;
        this.showButton(Y, X);
        cond.isDrawing = false;
    },

    dockBorder : function(isShow){
        var which = (isShow ? "" : "u");
        for (var pos in this.TLRB) {
            cond.mountToMapWorld(this.border["r"][this.TLRB[pos]], which);
            cond.mountToMapWorld(this.border["e"][this.TLRB[pos]], which);
        }
        cond.mountToMapWorld(this.okng, which);
        cond.mountToMapWorld(this.border_v, which);
        cond.mountToMapWorld(this.border_h, which);
    },

    abort : function (){
        this.targetY = this.targetX = this.borderMode = "";
        this.startLeft = this.startTop = this.endRight = this.endButton = 0;
        this.dockBorder(false);
        this.pixelStepSelector.style.display = "none";
    },

    isSquareableSize : function(){
        if (this.endRight - this.startLeft < BOX_MIN_WIDTH) return false;
        if (this.endRight - this.startLeft > BOX_MAX_WIDTH) return false;
        if (this.endBottom - this.startTop < BOX_MIN_HEIGHT) return false;
        if (this.endBottom - this.startTop > BOX_MAX_HEIGHT) return false;
        return true;
    },

    isSquare : function(){
        if (this.endRight - this.startLeft < 1) return false;
        if (this.endBottom - this.startTop < 1) return false;
        return true;
    },

    setBorderPos : function(mouseY, mouseX){
        this.endRight  = mouseX + this.pixelStep -  (mouseX % this.pixelStep);
        this.endBottom = mouseY + this.pixelStep - (mouseY % this.pixelStep);
        document.getElementById("txt1").value = this.endBottom + " " + this.endRight;
        var isVisible = this.isSquare();
        var isOkSize = this.isSquareableSize();
        var borderMode = (isVisible?"v":"h") + (isOkSize?"y":"n");
        if (borderMode != this.borderMode) {
            for (var pos in this.TLRB) {
                this.border.r[this.TLRB[pos]].style.display = (isVisible && !isOkSize ? "" : "none");
                this.border.e[this.TLRB[pos]].style.display = (isVisible && isOkSize ? "" : "none");
            }
            this.borderMode = borderMode;
        }

        this.okng.style.left = px(this.endRight - 20 + MAP_PADDING);
        this.okng.style.top = px(this.endBottom + MAP_PADDING);
        this.okng.style.color = (isOkSize ? "#080" :"#f00");
        this.okng.innerHTML = (isOkSize ? "ok" :"ng");;
        this.border_v.style.left =   px(this.endRight + MAP_PADDING);
        this.border_h.style.top =    px(this.endBottom + MAP_PADDING);

        if (!isVisible) return;

        var r_or_e = (isOkSize ? "e" : "r");
        this.border[r_or_e].t.style.width =  px(this.endRight - this.startLeft);
        this.border[r_or_e].l.style.height = px(this.endBottom - this.startTop);
        this.border[r_or_e].r.style.height = px(this.endBottom - this.startTop);
        this.border[r_or_e].b.style.width =  px(this.endRight- this.startLeft);
        this.border[r_or_e].r.style.left =   px(this.endRight + MAP_PADDING);
        this.border[r_or_e].b.style.top =    px(this.endBottom + MAP_PADDING);
    },

    setPixelStep : function(pixel){
        var p = parseInt(pixel);
        if (p!=4 && p!=8 && p!=16) p=1;
        this.pixelStep = p;
    }
}



<? //****************************************************************************** ?>
<? // 印刷オプション                                                                ?>
<? //****************************************************************************** ?>
var CPrintOption = function(){
    this.div = document.getElementById("print_option");
}
CPrintOption.prototype = {

    showAlternate : function(){
        this.div.style.display = (this.div.style.display == "none" ? "" : "none");
    },

    hide : function(){
        this.div.style.display = "none";
    },

    execPrint : function(xls_or_pdf, is_coloring){
        this.hide();
        document.frm_print.print_chart_data.value = bxu.getJsonDump();
        document.frm_print.print_line_data.value = bxu.getRealLineDumpForPrint();
        document.frm_print.print_row_heights_data.value = cond.rowHeights.join(",");
        document.frm_print.print_col_widths_data.value = cond.colWidths.join(",");
        document.frm_print.print_xls_or_pdf.value = xls_or_pdf;
        document.frm_print.print_area_width.value = cond.mapRealAreaWidth;
        document.frm_print.print_area_height.value = cond.mapRealAreaHeight;
        document.frm_print.print_coloring.value = is_coloring;
        document.frm_print.submit();
    }
}


<? //****************************************************************************** ?>
<? // テキストエリア・テキストエディット編集関連                                    ?>
<? //****************************************************************************** ?>
var CTextEditor = function(){
    this.te = document.getElementById("textedit");
    this.dmy = document.getElementById("textedit_dummy");
    this.btnOkContainer = document.getElementById("textedit_ok_container");
    this.btnOk = document.getElementById("textedit_ok");
    this.btnOkMini = document.getElementById("textedit_ok_mini");
    this.btnPasteString = document.getElementById("textedit_paste_string");
    this.afterResizeSelector = document.getElementById("text_editor_auto_resize_selector");
}

CTextEditor.prototype = {
    isActive : false,
    targetY : 0,
    targetX : 0,
    targetTD : null,
    startHeight : "",
    afterAutoResize : false,
    uwagakiTuikiMode : "tuiki",

    setAfterResize : function(bool){
        this.afterResize = (bool ? true : false);
    },

    startEdit : function(td, y, x){
        cond.hideBoxMenu();
        if (this.targetY && this.targetX){
            if ((this.targetY != y || this.targetX != x) && this.targetTD != null) this.applyChange();
        }
        this.isActive = true;
        if (!bxu.isBox(YX(y,x))) { this.isActive = false; return; }
        var rca = rcaMap[YX(y,x)];
        this.targetY = y;
        this.targetX = x;

        this.targetTD = td;

        this.startHeight = bxu.getBoxHeight(y,x)-4;
        this.te.style.width = px(bxu.getBoxWidth(y,x)-4);
        this.te.style.height = px(this.startHeight);
        this.te.style.top = cond.getRowY(y) + MAP_PADDING + 2;
        this.te.style.left = cond.getColX(x) + MAP_PADDING + 2;
        this.targetTD.innerHTML = "";
        this.te.value = bxu.getBoxText(rca);

        if (cond.isUnmountedFromMapWorld(this.te)) cond.mountToMapWorld(this.te);
        if (this.te.style.display!="") this.te.style.display="";
        this.te.style.backgroundColor = bxu.getBoxBehavior(rca.boxtype).tacolor;
        this.te.style.border = "2px solid #fff";
        this.mountOkButton();
        this.setCursorPosition(this.uwagakiTuikiMode);
        this.afterResizeSelector.style.display = "";
    },

    mountOkButton : function(){
        var cliplistIsFeatureing = cliplist.isFeatureing();
        this.btnOkMini.style.display = (cliplistIsFeatureing ? "" : "none");
        this.btnPasteString.style.display = (cliplistIsFeatureing ? "" : "none");
        this.btnOk.style.display = (cliplistIsFeatureing ? "none" : "");
        this.btnOkContainer.style.left = px(cond.getColX(this.targetX) + bxu.getBoxWidth(this.targetY, this.targetX)  / 2 - 48 + MAP_PADDING);
        this.btnOkContainer.style.top = px(cond.getRowY(this.targetY) - 20 + MAP_PADDING);
        cond.mountToMapWorld(this.btnOkContainer);
    },

    isActiveTarget : function(Y,X){
        if (!this.isActive) return false;
        if (this.targetY!=Y) return false;
        if (this.targetX!=X) return false;
        return true;
    },

    isEditing : function(){
        return this.isActive;
    },

    pasteString : function(){
        if(document.selection){
            this.te.focus();
            var r = document.selection.createRange();
            var len = r.text.length;
            var br = document.body.createTextRange();
            br.moveToElementText(this.te);
            var all_len = br.text.length;
            br.setEndPoint("StartToStart", r);
            var s = all_len - br.text.length;
            var e = s + len;
            var pos = { start: s, end: e };
        }
        else if(this.te.setSelectionRange){
            var pos = {start: this.te.selectionStart, end: this.te.selectionEnd };
        }

        var prev = this.te.value.substring(0, pos.start); <? // 選択範囲の前 ?>
        var next = this.te.value.substring(pos.end, this.te.value.length); <? // 選択範囲の次 ?>
        this.te.value = prev + cliplist.getCurrentText() + next;
    },

    applyChange : function(){
        if (!this.isActive) return;
        this.isActive = false;
        btnBlur(this.btnOk);
        btnBlur(this.btnOkMini);
        btnBlur(this.btnPasteString);
        cond.unmountFromMapWorld(this.btnOkContainer);
        var s = this.trim(this.te.value);
        this.te.style.display=="none";
        if (!cond.isUnmountedFromMapWorld(this.te)) cond.unmountFromMapWorld(this.te);

        var es = this.htmlWbrEscape(s);
        if (es=="") es = "<br/>";
        if (this.targetTD) this.targetTD.innerHTML = es;

        var rcaid = YX(this.targetY,this.targetX);
        if (bxu.isBox(rcaid)){
            var oldtext = bxu.getBoxText(rcaMap[rcaid]);
            if (s != oldtext){
                rcaMap[rcaid].text = s;
                if (this.afterAutoResize){
                    this.targetTD.parentNode.parentNode.parentNode.style.height = "";
                    this.targetTD.parentNode.parentNode.parentNode.style.width = "";
                    rcaMap[rcaid].w = getWidth(this.targetTD)+2;
                    rcaMap[rcaid].h = getHeight(this.targetTD)+6;
                    cond.refreshMapWorldScales(true);
                }
                if (this.afterAutoResize){
                    cond.isDrawing = true;
                    drawMap(lineRouteCalculator.getResult(), true);
                    cond.isDrawing = false;
                } else {
                    undo.makeHistory();
                }
            }
        }
        this.targetY = 0;
        this.targetX = 0;
        this.targetTD = null;
        this.afterResizeSelector.style.display = "none";

    },

    abort : function(){
        this.applyChange();
    },

    adjustHeight : function(){
        if (this.te.value=="") return;
        this.dmy.style.width = this.te.style.width;
        this.dmy.value = this.te.value;
        this.dmy.style.height = null;
        this.dmy.rows = 1;
        this.dmy.style.display = "";
        var h = this.dmy.offsetHeight;
        this.dmy.value = this.te.value;

        var margin = 14;
        for (var i=0; i<30; i++){
            this.dmy.scrollTop = 1000;
            if (this.dmy.scrollTop == 0) break;
            this.dmy.style.height = (this.dmy.offsetHeight + this.dmy.scrollTop + margin)+"px";
        }
        if (h < parseInt(this.dmy.style.height,10)) h = parseInt(this.dmy.style.height,10);
        if (h < this.startHeight) h = this.startHeight;
        this.te.style.height = px(h);
        this.dmy.style.display = "none";
    },

    htmlEscape : function(str, nonEscapeCrLf){
        if (!str) return str;
        var ret = str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
        if (nonEscapeCrLf) return ret;
        return ret.replace(/\n/g, '<br/>');
    },

    trim : function(str){
        if (!str) return "";
        return str.replace(/^[ 　]+/g, "").replace(/[ 　]+$/g, "").replace(/^[\r\n]+/g, "").replace(/[\r\n]+$/g, "");
    },

    escapeCrlf : function(str){
        if (!str) return "";
        return str.replace(/\n/g, "\\n");
    },

    htmlWbrEscape : function(str, nonEscapeCrLf) {
        if (!str) return str;
        var out = [];
        var a = str.split("");
        for (var s in a) {
            var es = a[s].replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
            if (!nonEscapeCrLf) es = es.replace(/\n/g, '<br/>');
            out.push(es+"<wbr>");
        }
        return out.join("");
    },

    setAfterAutoResize : function(bool){
        this.afterAutoResize = (bool ? true : false);
    },

    setUwagakiTuiki : function(which){
        this.uwagakiTuikiMode = (which=="uwagaki" ? "uwagaki" : "tuiki");
        this.setCursorPosition(this.uwagakiTuikiMode);
    },

    setCursorPosition : function(which){
        this.te.focus();
        if (which=="uwagaki") this.te.select();
        else {
            if (this.te.createTextRange) {
                var range = this.te.createTextRange();
                range.move('character', this.te.value.length);
                range.select();
            } else if (this.te.setSelectionRange) {
                this.te.setSelectionRange(this.te.value.length, this.te.value.length);
            }
        }
    }
}


<? //****************************************************************************** ?>
<? // ＵＮＤＯ関連                                                                  ?>
<? //****************************************************************************** ?>
var CUndo = function(){
    document.getElementById("undo_container").style.display = "";
}
CUndo.prototype = {

    UNDO_MAX : 50,
    undo_counter : -1,
    undo_list : [],

    makeHistory : function(){
        var cloneMap = {};
        for (var rcaid in rcaMap) cloneMap[rcaid] = bxu.cloneBox(rcaMap[rcaid]);
        this.undo_counter++;
        this.undo_list[this.undo_counter] = cloneMap;
        if (this.undo_counter > this.UNDO_MAX) {
            var rca = this.undo_list.shift();
            delete rca;
            this.undo_counter--;
        }
        for (var nuc = plus1(this.undo_counter); nuc <= this.UNDO_MAX; nuc++) {
            if (!this.undo_list[nuc]) break;
            delete this.undo_list[nuc];
            this.undo_list[nuc] = 0;
        }
    },

    undoMap : function(){
        if (this.undo_counter < 1) return;
        if (!this.undo_list[this.undo_counter-1]) return;
        cond.resetMenu();
        this.applyUndoHistory(this.undo_counter - 1, "undo");
        this.undo_counter--;
    },

    redoMap : function(){
        var uc = plus1(this.undo_counter);
        if (!this.undo_list[uc]) return;
        cond.resetMenu();
        this.applyUndoHistory(uc, "redo");
        this.undo_counter = uc;
    },

    applyUndoHistory : function(idx, undo_or_redo){
        cond.isDrawing = true;
        rcaMap = {};
        cond.abortAllLockedFunctions();
        for (var rcaid in this.undo_list[idx]) rcaMap[rcaid] = bxu.cloneBox(this.undo_list[idx][rcaid]);
        cond.refreshMapWorldScales(false);
        lineRouteCalculator.calculate(true);
        drawMap(lineRouteCalculator.getResult(), true, true);
        cond.isDrawing = false;
    }
}


<? //****************************************************************************** ?>
<? // クリップリスト関連                                                            ?>
<? //****************************************************************************** ?>
var CCliplist = function(initList){
    this.divMainFrame = document.getElementById("cbd_main_frame");
    this.divContent = document.getElementById("cbd_content");
    this.divText = document.getElementById("cbd_text");
    this.divCommand = document.getElementById("cbd_command");
    this.divCommandMessage = document.getElementById("cbd_command_message");
    this.divDelete = document.getElementById("cbd_delete");
    this.divStockOuter = document.getElementById("cbd_stock_outer");
    this.divStockInner = document.getElementById("cbd_stock_inner");
    this.divStock.Y = document.getElementById("cbd_stock_y");
    this.divStock.G = document.getElementById("cbd_stock_g");
    this.divStock.B = document.getElementById("cbd_stock_b");
    this.divStock.R = document.getElementById("cbd_stock_r");
    this.divStock.V = document.getElementById("cbd_stock_v");
    this.divStock.S = document.getElementById("cbd_stock_s");

    this.pasteBox.B.normal = document.getElementById("btn_pasteboxB");
    this.pasteBox.R.normal = document.getElementById("btn_pasteboxR");
    this.pasteBox.Y.normal = document.getElementById("btn_pasteboxY");
    this.pasteBox.V.normal = document.getElementById("btn_pasteboxV");
    this.pasteBox.G.normal = document.getElementById("btn_pasteboxG");

    this.pasteBox.B.ok = document.getElementById("btn_pasteboxBok");
    this.pasteBox.R.ok = document.getElementById("btn_pasteboxRok");
    this.pasteBox.Y.ok = document.getElementById("btn_pasteboxYok");
    this.pasteBox.V.ok = document.getElementById("btn_pasteboxVok");
    this.pasteBox.G.ok = document.getElementById("btn_pasteboxGok");

    this.pasteBox.B.ng = document.getElementById("btn_pasteboxBng");
    this.pasteBox.R.ng = document.getElementById("btn_pasteboxRng");
    this.pasteBox.Y.ng = document.getElementById("btn_pasteboxYng");
    this.pasteBox.V.ng = document.getElementById("btn_pasteboxVng");
    this.pasteBox.G.ng = document.getElementById("btn_pasteboxGng");

    this.btnBalloon = document.getElementById("btn_balloon");

    this.clipPasteSelector = document.getElementById("clip_paste_selector");

    this.noticeMarqueeContainer = document.getElementById("clip_added_notice");
    this.noticeMarqueeContainer.style.display = "";
    this.btnShowCliplist = document.getElementById("btn_show_cliplist");


    for (var n in initList){
        this.entries[n] = {};
        for (var d in initList[n]) this.entries[n][d] = initList[n][d];
    }

    for (var idx in this.entries) this.addEntry(idx, this.entries[idx].boxtype, this.entries[idx].text);
    this.setColor(this.divContent, "白白");
}

CCliplist.prototype = {
    currentId : "",
    prefixSize : 4,
    entries : {},
    divStock : {},
    pasteBox : {"Y":{}, "G":{}, "B":{}, "R":{}, "V":{}},
    tagIndex : ["Y","G","B","R","V","S"],
    currentTagIndex : "",
    currentPasteBox : 0,
    marqueeTop : 0,
    marqueeCloseTime : 0,
    marqueeRequesting : 0,

    isVisible : function(){
        return this.divMainFrame.style.display=="none" ? false : true;
    },

    addEntryFromMap : function(){
        var yx = cond.getBoxYXP();
        var rcaid = YX(yx.y,yx.x);
        if (this.addEntry(0, rcaMap[rcaid].boxtype, bxu.getBoxText(rcaMap[rcaid]))) this.notifyMarquee(1);
    },

    notifyMarquee: function(isNew){
        if (isNew){
            isAlreadyRunning = this.marqueeRequesting;
            this.marqueeRequesting = 1;
            this.marqueeTop = 0;
            if (isAlreadyRunning) return;
        }
        if (this.marqueeRequesting){
            if (this.marqueeTop < 24) {
                this.marqueeTop++;
                this.noticeMarqueeContainer.style.top = (92 + 20 - this.marqueeTop) + "px";
            }
            else if (this.marqueeTop == 24){
                this.marqueeTop++;
                this.marqueeStartWaitTime = (+new Date()) + 3000;
            }
            else if (this.marqueeStartWaitTime < (+new Date())){
                this.marqueeRequesting = 0;
            }
        }
        else if (this.marqueeTop > 0) {
                this.marqueeTop--;
                this.noticeMarqueeContainer.style.top = (92 + 20 - this.marqueeTop) + "px";
        }
        if (this.marqueeTop > 0 || this.marqueeRequesting){
            setTimeout("cliplist.notifyMarquee()", 10);
        }
    },

    addEntry : function(idx, boxtype, text){
        if (!idx){
            idx = 0;
            for (var id in this.entries) {
                if (boxtype != this.entries[id].boxtype) continue;
                if (text != this.entries[id].text) continue;
                idx = id;
                break;
            }
            if (!idx){
                for (var id in this.entries) idx = Math.max(id, idx);
                idx++;
            }
        }
        this.entries[idx] = {"boxtype":boxtype, "text":text};
        text = text.length > this.prefixSize ? text.substring(0, this.prefixSize)+"…" : text;

        var div = document.getElementById("cbd_entry_" + idx);
        if (div) return false;
        if (!div) {
            div = document.createElement("div");
            div.id = "cbd_entry_" + idx;
            div.style.whiteSpace = "nowrap";
            div.innerHTML =
                '<div class="clip ' + bxu.getBoxBehavior(boxtype).divboxClass + '"' +
                ' onmouseover="cliplist.mouseMoveReview(\''+idx+'\');" onclick="cliplist.feature(\''+idx+'\');">'+ textEditor.htmlWbrEscape(text, true) + "</div>";
            if (getWidth(div) > 230){
                div.style.whiteSpace = "normal";
                div.style.width = "230px";
            }
        }
        if (boxtype!="パス") this.divStock[bxu.getBoxBehavior(boxtype).stocktag].appendChild(div);
        this.divCommandMessage.style.display = "none";
        return true;
    },

    changeDisplay : function(show_or_hide){
        this.resetFeature();
        var isLetShowing = (this.divMainFrame.style.display == "none");
        if (show_or_hide) isLetShowing = (show_or_hide=="show");
        this.divMainFrame.style.display = (isLetShowing ? "" : "none");
        cond.resizeAllPanesToFit();
    },

    show : function(){ this.changeDisplay("show"); },

    hide : function(){ this.changeDisplay("hide"); },

    mouseOutEvent : function(){
        if (this.currentId) return;
        this.resetFeature();
    },

    deleteEntry : function(idx){
        delete this.entries[idx];
        if (!bxu.count(this.entries)) this.divCommandMessage.style.display = "";
        for (var t in this.tagIndex){
            var tag = this.tagIndex[t];
            for (var elem in this.divStock[tag].childNodes){
                if (!this.divStock[tag].childNodes[elem]) continue;
                if (this.divStock[tag].childNodes[elem].tagName!="DIV") continue;
                if (this.divStock[tag].childNodes[elem].id == "cbd_entry_" + idx){
                    var de = this.divStock[tag].removeChild(this.divStock[tag].childNodes[elem]);
                    delete de;
                    this.resetFeature();
                    return;
                }
            }
        }
    },

    resetFeature : function(){
        if (this.currentPasteBox) {
            cond.unmountFromMapWorld(this.currentPasteBox.normal);
            cond.unmountFromMapWorld(this.currentPasteBox.ok);
            cond.unmountFromMapWorld(this.currentPasteBox.ng);
        }
        this.currentId = "";
        this.clipPasteSelector.style.display = "none";
        this.currentTagIndex = "";
        this.divText.innerHTML = "";
        this.divCommand.style.display = "none";
        this.currentPasteBox = 0;
        this.divDelete.style.display = "none";
        this.setColor(this.divContent, "白白");
        if (textEditor.isEditing()) textEditor.mountOkButton();
    },

    mouseMoveReview : function(idx){
        var clip = this.entries[idx];
        if (!this.currentId) this.setColor(this.divContent, "白箱");
        if (this.currentId) return;
        this.divText.innerHTML = textEditor.htmlWbrEscape(this.entries[idx].text);
        this.divCommand.style.display = "none";
    },

    feature : function(idx){
        resizer.abort();
        joinner.abort();
        var clip = this.entries[idx];
        this.currentId = idx;
        this.clipPasteSelector.style.display = "";
        this.currentTagIndex = bxu.getBoxBehavior(clip.boxtype).stocktag;
        this.divDelete.style.display = "";
        this.divText.innerHTML = textEditor.htmlWbrEscape(this.entries[idx].text);
        this.setColor(this.divContent, clip.boxtype);
        this.divCommand.style.display = "";
        if (textEditor.isEditing()) {
            textEditor.mountOkButton();
            textEditor.setCursorPosition(textEditor.uwagakiTuikiMode);
        }
    },

    setColor : function(elem, boxtype){
        var behavior = bxu.getBoxBehavior(boxtype);
        elem.className = behavior.divboxClass;
        elem.style.backgroundColor = behavior.bgcolor;
        elem.style.borderLeft = behavior.borderL;
        elem.style.borderRight = behavior.borderR;
        elem.style.borderTop = behavior.borderT;
        elem.style.borderBottom = behavior.borderB;
        elem.style.backgroundImage = behavior.bgimage;
    },

    truncate : function(){
        this.entries = {};
        for (var t in this.tagIndex){
            this.divStock[this.tagIndex[t]].innerHTML = "";
        }
        this.resetFeature();
        this.hide();
    },

    isFeatureing : function(){
        return this.currentId ? true : false;
    },

    isPastableBoxType : function(boxtype){
        if (!boxtype) return;
        if (!this.isFeatureing()) return false;
        var bt = this.entries[this.currentId].boxtype;
        if (boxtype == bt) return true;
        if (boxtype == "質問" || boxtype == "原因" || boxtype == "パス"){
            if (bt == "質問" || bt == "原因" || bt == "パス") return true;
        }
        return false;
    },

    pasteIfPastable : function(Y,X) {
        if (!this.currentId) return;
        var clip = this.entries[this.currentId];
        var rca = rcaMap[YX(Y,X)];
        if (!this.isPastableBoxType(rca.boxtype)) return;
        cond.isDrawing = true;
        rca.boxtype = clip.boxtype;
        rca.text = clip.text;
        this.resetFeature();
        adjustSpace();
        cond.refreshMapWorldScales(true);
        drawMap(lineRouteCalculator.getResult());
        cond.isDrawing = false;
    },

    getCurrentText : function(){
        if (!this.currentId) return "";
        var clip = this.entries[this.currentId];
        return clip.text;
    },

    decPrefixSize : function(){
        if (this.prefixSize <= 1) return;
        this.prefixSize--;
        this.setPrefixSize();
    },
    incPrefixSize : function(){
        if (this.prefixSize >= 30) return;
        this.prefixSize++;
        this.setPrefixSize();
    },

    setPrefixSize : function(){
        var maxStrlen = 0;
        for (var t in this.tagIndex){
            var tag = this.tagIndex[t];
            for (var elem in this.divStock[tag].childNodes){
                if (this.divStock[tag].childNodes[elem].tagName!="DIV") continue;
                var div = this.divStock[tag].childNodes[elem];
                var ids = div.id.split("_");
                var idx = ids[2];
                var text = this.entries[idx].text;
                maxStrlen = Math.max(maxStrlen, text.length);
                text = text.length > this.prefixSize ? text.substring(0, this.prefixSize)+"…" : text;
                div.childNodes[0].innerHTML = textEditor.htmlWbrEscape(text, true);
                div.style.width = "";
                div.style.whteSpace = "nowrap";
                if (getWidth(div) > 210){
                    div.style.width = "210px";
                }
            }
        }
        if (maxStrlen < this.prefixSize && this.prefixSize > 4) this.prefixSize = maxStrlen;
    },

    locatePasteBox : function(pasteBoxType){
        if (!this.currentPasteBox) {
            this.currentPasteBox = this.pasteBox[this.currentTagIndex];
            cond.mountToMapWorld(this.currentPasteBox.normal);
            cond.mountToMapWorld(this.currentPasteBox.ok);
            cond.mountToMapWorld(this.currentPasteBox.ng);
        }
        var myx = cond.getMouseYX();
        var byx = cond.getBoxYXP();

        if (toolmenu.currentHoveringId=="textedit_ok_mini" || toolmenu.currentHoveringId=="textedit_paste_string"){
            pasteBoxType = "";
            cond.unmountFromMapWorld(this.btnBalloon);
        }
        else if (toolmenu.currentHoveringId=="btn_textedit" || (byx.position=="box" && textEditor.isActiveTarget(byx.y, byx.x))){
            cond.mountToMapWorld(this.btnBalloon);
            this.btnBalloon.style.left = px(myx.x + MAP_PADDING - 40);
            this.btnBalloon.style.top = px(myx.y + MAP_PADDING - 40);
            pasteBoxType = "balloon";
        }
        this.currentPasteBox.normal.style.display = (pasteBoxType=="normal" ? "" : "none");
        this.currentPasteBox.ok.style.display =     (pasteBoxType=="ok" ? "" : "none");
        this.currentPasteBox.ng.style.display =     (pasteBoxType=="ng" ? "" : "none");
        if (pasteBoxType != "normal" && pasteBoxType != "ok" && pasteBoxType != "ng") return;
        cond.unmountFromMapWorld(this.btnBalloon);
        this.currentPasteBox[pasteBoxType].style.left = px(myx.x + MAP_PADDING - 40);
        this.currentPasteBox[pasteBoxType].style.top = px(myx.y + MAP_PADDING + 10);
    },

    hidePasteBox : function(){
        if (this.currentPasteBox) {
            cond.unmountFromMapWorld(this.currentPasteBox.normal);
            cond.unmountFromMapWorld(this.currentPasteBox.ok);
            cond.unmountFromMapWorld(this.currentPasteBox.ng);
            cond.unmountFromMapWorld(this.btnBalloon);
            this.currentPasteBox = 0;
        }
    },

    getJsonDump : function(){
        var idx = 0;
        var out = [];
        for (var ent in this.entries){
            idx++;
            if (idx > 1) out.push(",");
            out.push(idx+':{"boxtype":"'+this.entries[ent].boxtype+'", "text":"'+textEditor.trim(this.entries[ent].text)+'"}');
        }
        return out.join("");
    }
}


<? //****************************************************************************** ?>
<? // ツールバー形式のメニューボタン関連                                            ?>
<? //****************************************************************************** ?>
var CToolMenu = function(){
    this.delbox = document.getElementById("delbox");
    this.btnJoinner = document.getElementById("btn_joinner");
    this.btnTextEdit = document.getElementById("btn_textedit");
    this.btnChangecolor = document.getElementById("btn_changecolor");
    this.btnClip = document.getElementById("btn_clip");
    this.btnAddboxY = document.getElementById("btn_addboxY");
    this.btnAddboxB = document.getElementById("btn_addboxB");
    this.btnAddboxBR= document.getElementById("btn_addboxBR");
    this.btnAddboxRB= document.getElementById("btn_addboxRB");
    this.btnAddboxR = document.getElementById("btn_addboxR");
    this.btnAddboxV = document.getElementById("btn_addboxV");
    this.btnCutline = document.getElementById("btn_cutline");
    this.boxname = document.getElementById("boxname");
}
CToolMenu.prototype = {
    currentHoveringId : "",

    configureBoxButtonMenuContainer : function(posy, posx){
        var rca = rcaMap[YX(posy,posx)];
        this.boxname.style.display = textEditor.isActiveTarget(posy, posx) ? "none" : "";
        this.boxname.innerHTML = rca.boxtype;
        this.boxname.style.color = bxu.getBoxBehavior(rca.boxtype).namecolor;
        this.btnTextEdit.style.display = textEditor.isActiveTarget(posy, posx) ? "none" : "";
        this.btnChangecolor.style.display = (posy==1 || textEditor.isActiveTarget(posy, posx)) ? "none" : "";
        this.btnClip.style.display = (bxu.getBoxText(rca) && !textEditor.isActiveTarget(posy, posx) && rca.boxtype!="パス") ? "" : "none";
        this.delbox.style.display = ( ( (posy==1 && posx==1) || rca.boxtype=="結果" || textEditor.isEditing()) ? "none" : "");
        var dmp = "[縦:"+posy+", 横"+posx + "] (親)";
        var parents = bxu.getParentList(YX(posy,posx));
        for (var pid in parents){ var pyx = getyx(pid); dmp += " {縦:"+pyx.y+", 横:"+pyx.x+"}"; }
        dmp += " (子)";
        for (var d in rca.dest) { dyx =getyx(rca.dest[d]); dmp += " {縦:"+dyx.y+", 横:"+dyx.x+"}" };
        document.getElementById("txt1").value = dmp;
    },

    configureLineRY1ButtonMenuContainer : function (posx) {
        var rca = rcaMap[YX(1,posx)];
        var boxtype = (rca ? rca.boxtype : "");

        var currentX = posx;
        while(posx < cond.colCount) {
            if (bxu.isBox(YX(1,plus1(posx)))) break;
            posx++;
        }
        this.btnCutline.style.display = "none";
        this.btnJoinner.style.display = "none";
        this.btnAddboxY.style.display = ((currentX <= posx && boxtype!="結果") ? "" : "none");
        this.btnAddboxB.style.display = "none";
        this.btnAddboxR.style.display = "none";
        this.btnAddboxV.style.display = "none";
        this.btnAddboxBR.style.display = "none";
        this.btnAddboxRB.style.display = "none";
        this.btnCutline.style.display = "none";
        return posx;
    },

    configureLineRButtonMenuContainer : function (posy, posx) {
        var rca = rcaMap[YX(posy,posx)];
        var boxtype = rca.boxtype;
        this.btnCutline.style.display = "none";
        this.btnJoinner.style.display = "none";
        this.btnAddboxY.style.display = "none";
        this.btnAddboxB.style.display = (isOneOf(boxtype, ["質問","パス"]) && !ingaMode ? "" : "none");
        this.btnAddboxR.style.display = (isOneOf(boxtype, ["原因","パス"]) || ingaMode  ? "" : "none");
        this.btnAddboxV.style.display = (isOneOf(boxtype, ["根本原因"]) ? "" : "none");
        this.btnAddboxBR.style.display = "none";
        this.btnAddboxRB.style.display = "none";
        this.btnCutline.style.display = "none";
        cond.linemenuContainer.style.width = (boxtype=="パス") ? px(22) : "";
        if (this.btnAddboxB.style.display=="") this.btnAddboxB.title = "左要素と同じ矢印線を持つ要素を挿入";
        if (this.btnAddboxR.style.display=="") this.btnAddboxR.title = "左要素と同じ矢印線を持つ要素を挿入";
        if (this.btnAddboxV.style.display=="") this.btnAddboxV.title = "左要素と同じ矢印線を持つ要素を挿入";
    },

    configureLineBButtonMenuContainer : function (posy, posx) {
        cond.linemenuContainer.style.width = "";
        var rca = rcaMap[YX(posy,posx)];
        var boxtype = rca.boxtype;
        var nbtype = "";
        var childArray = bxu.getChildArray(YX(posy, posx));
        var nbtype = childArray.length ? childArray[0].boxtype : "";
        this.btnAddboxY.style.display = "none";
        var childCount = 0;
        var underChildCount = 0;
        for (var d in rca.dest){ childCount++; if (getyx(rca.dest[d]).y >1) underChildCount++; }
        this.btnJoinner.style.display = (!cliplist.isFeatureing() && isOneOf(boxtype, ["工程","質問","原因","パス"]) ? "" : "none");
        this.btnCutline.style.display = (!cliplist.isFeatureing() && underChildCount && boxtype!="パス" ? "" : "none");

        this.btnAddboxB.style.display = (!ingaMode && isOneOf(boxtype, ["工程","原因","パス"]) ? "" : "none");
        this.btnAddboxR.style.display = (boxtype!="根本原因" && boxtype != "結果" && (ingaMode || isOneOf(boxtype, ["質問","パス"])) ? "" : "none");
        this.btnAddboxBR.style.display =(!ingaMode && (underChildCount && nbtype!="原因") && isOneOf(boxtype, ["工程","原因","パス"]) ? "" : "none");
        this.btnAddboxRB.style.display =(!ingaMode && (underChildCount && nbtype!="質問") && isOneOf(boxtype, ["質問","パス"]) ? "" : "none");
        this.btnAddboxV.style.display = (!nbtype && childCount < 2 && isOneOf(boxtype, ["工程","質問","原因","パス"]) ? "" : "none");
        if (this.btnAddboxB.style.display=="") this.btnAddboxB.title = "「質問」要素を上要素の直下に挿入";
        if (this.btnAddboxR.style.display=="") this.btnAddboxR.title = "「原因」要素を上要素の直下に挿入";
        if (this.btnAddboxV.style.display=="") this.btnAddboxV.title = "「根本原因」要素を上要素の直下に挿入";
    }
}


<? //****************************************************************************** ?>
<? // グローバルコンディション                                                      ?>
<? //****************************************************************************** ?>
var CGlobalCondition = function(){
    this.mapScrollSquareOuter = document.getElementById("map_scroll_square_outer");
    this.mapScrollSquareInner = document.getElementById("map_scroll_square_inner");
    this.headerMenu = document.getElementById("headermenu_container");
    this.mapScrollSquareInner.innerHTML =
        '<div id="map_world" ondragstart="return false;" onmousedown="mapWorldMouseDownEvent();"></div>';
    this.disabledContainer = document.getElementById("disabled_container");
    mapWorld = document.getElementById("map_world");
    this.resizeAllPanesToFit();
    this.linemenuContainer = document.getElementById("linemenuContainer");
    this.boxmenuContainer = document.getElementById("boxmenuContainer");
    this.mountToMapWorld(this.linemenuContainer);
    this.mountToMapWorld(this.boxmenuContainer);
    this.linkInga = document.getElementById("link_inga");
    this.imgIngaSelector = document.getElementById("img_inga_selector");
    this.ingaModeTitle = document.getElementById("inga_mode_title");
    this.tdLinkDiversion = document.getElementById("td_link_diversion");
    this.tdLinkIngaSelector = document.getElementById("td_link_inga_selector");
}
CGlobalCondition.prototype = {
    isPageInitialLoaded : false,
    mouseY : 0,
    mouseX : 0,
    boxY : 0,
    boxX : 0,
    position : "",
    locatedCounter : 0,
    mapRealAreaWidth : 0,
    mapRealAreaHeight : 0,
    mapWorldWidth : 0,
    mapWorldHeight : 0,
    colWidths : [],
    rowHeights : [],
    colsX : [],
    rowsY : [],
    colMinWidths : [],
    rowMinHeights : [],
    colCount : 0,
    rowCount : 0,
    isMouseInMapRealArea : false,
    isMouseInMapSquare : false,
    isSystemDisabled : false,
    isPrintMode : false,
    isDrawing : false,

    setDefaultMinimumChart : function(){
        rcaMap = {
            "1_1":{"boxtype":"工程", "text":"", "dest":["1_2"]},
            "1_2":{"boxtype":"結果", "text":"", "dest":[]}
        };
    },

    alertIfSystemDisabled : function(){
        if (!this.isSystemDisabled) return false;
        alert("データ読込みから、分析対象を指定してください。");
        return true;
    },

    execWindowMouseMoveEvent : function(e){
        if (!this.isPageInitialLoaded) return;
        if (this.isPageInitialLoaded && this.isSystemDisabled) return;
        if (this.isDrawing) return;

        var counter = this.locatedCounter;
        this.setGlobalPosition(e);
        if (!this.mouseY || !this.mouseX) { this.resetMenu(); return; }
        if (resizer && resizer.isResizing()){ resizer.setBorderPos(this.mouseY, this.mouseX); return; }

        if (cliplist.isFeatureing()) {
            if (this.isMouseInMapSquare){
                var pasteBoxType = "normal";
                if (this.position == "box" && bxu.isBox(YX(this.boxY,this.boxX))){
                    var rca = rcaMap[YX(this.boxY,this.boxX)];
                    pasteBoxType = (cliplist.isPastableBoxType(rca.boxtype) ? "ok" : "ng");
                }
                cliplist.locatePasteBox(pasteBoxType);
            }
            else {
                cliplist.hidePasteBox();
            }
        }
        if (!this.isMouseInMapRealArea || !this.isMouseInMapSquare) this.resetMenu();
        if (!this.boxY || !this.boxX || !this.position) return;
        if (counter == this.locatedCounter) return;


        if (joinner.isJoinning()){
            joinner.setJoinStyle(this.boxY, this.boxX, this.position);
            return;
        }

        if (this.position == "box") {
            if (!this.isMouseInMapSquare) return;
            if (resizer.isResizing()) return;

            var rca = rcaMap[YX(this.boxY,this.boxX)];

            if (!bxu.isBox(YX(this.boxY,this.boxX))) return;
            this.hideLineMenu();

            toolmenu.configureBoxButtonMenuContainer(this.boxY, this.boxX);
            this.showBoxMenu();
            return;
        }

        else if ((this.position == "lineR" || this.position == "lineB") && !joinner.isJoinning() && !textEditor.isEditing()) {
            if (!this.isMouseInMapSquare) return;
            var newX = this.boxX;
            if (this.position=="lineR"){
                if (this.boxY==1){
                    newX = toolmenu.configureLineRY1ButtonMenuContainer(this.boxX);
                } else {
                    if (!bxu.isBox(YX(this.boxY,this.boxX))) return;
                    toolmenu.configureLineRButtonMenuContainer(this.boxY, this.boxX);
                }
            } else {
                if (!bxu.isBox(YX(this.boxY,this.boxX))) return;
                toolmenu.configureLineBButtonMenuContainer(this.boxY, this.boxX);
            }
            this.hideBoxMenu();
            this.showLineMenu(this.boxY, newX);
        }

        else {
            this.hideMenu();
            return;
        }
    },

    setGlobalPosition : function(e){
        var baseX = (e ? e.pageX : event.x + document.body.scrollLeft);
        var baseY = (e ? e.pageY : event.y + document.body.scrollTop);
        var scrL = this.mapScrollSquareInner.scrollLeft;
        var scrT = this.mapScrollSquareInner.scrollTop;
        this.mouseX = baseX + scrL - this.mapScrollSquareOuter.offsetLeft - MAP_PADDING;
        this.mouseY = baseY + scrT - this.mapScrollSquareOuter.offsetTop - MAP_PADDING;

        var pxwok = (this.mouseX + MAP_PADDING < 0 || this.mouseX + MAP_PADDING > parseInt(this.mapScrollSquareInner.style.width) + scrL  ? 0 : 1);
        var pywok = (this.mouseY + MAP_PADDING < 0 || this.mouseY + MAP_PADDING > parseInt(this.mapScrollSquareInner.style.height) + scrT ? 0 : 1);
        this.isMouseInMapSquare = (pxwok && pywok ? true : false);

        var pxok = (this.mouseX < 0 || this.mouseX > this.mapRealAreaWidth  ? 0 : 1);
        var pyok = (this.mouseY < 0 || this.mouseY > this.mapRealAreaHeight ? 0 : 1);
        this.isMouseInMapRealArea = (pxok && pyok ? true : false);

        var boxX = boxY = 0;
        if (pxok) for (var n in this.colsX) { if (this.colsX[n] > this.mouseX) break; boxX = plus1(n); }
        if (pyok) for (var n in this.rowsY) { if (this.rowsY[n] > this.mouseY) break; boxY = plus1(n); }
        if (!boxX || !boxY) return;

        var lr = (this.colsX[boxX-1] + bxu.getBoxWidth(boxY, boxX) > this.mouseX ? "left" : "right");
        var tb = (this.rowsY[boxY-1] + bxu.getBoxHeight(boxY, boxX) > this.mouseY ? "top" : "bottom");
        var position = "";
        if (lr=="left" && tb=="top") position = "box";
        else if (lr=="right" && tb=="top") position = "lineR";
        else if (lr=="left"  && tb=="bottom") position = "lineB";
        else if (lr=="right" && tb=="bottom") position = "lineRB";
        if (!position) return;

        this.boxX = boxX; this.boxY = boxY; this.position = position;
        this.locatedCounter++;
    },

    <? // 各縦横軸の位置、サイズ、などの目盛りを作成する ?>
    refreshMapWorldScales : function(isCalculateOnly){
        this.colWidths = [];
        this.rowHeights = [];
        this.colMinWidths = [];
        this.rowMinHeights = [];
        this.colCount = this.rowCount = 0;
        this.colsX = [0];
        this.rowsY = [0];
        for (var rcaid in rcaMap){
            if (!bxu.isBox(rcaid)) { alert("error refreshMapWorldScales(1) " + rcaid); continue; }
            var yx = getyx(rcaid);
            if (yx.y > MAX_YX_ENTRY_LENGTH || yx.x > MAX_YX_ENTRY_LENGTH) continue;
            this.rowCount = Math.max(this.rowCount, yx.y);
            this.colCount = Math.max(this.colCount, yx.x);
            var rca = rcaMap[rcaid];
            if (!rcaMap[rcaid].dest) rcaMap[rcaid].dest = [];
            if (!rca.h || rca.h=="undefined") rcaMap[rcaid].h = BOX_DEFAULT_HEIGHT;
            if (!rca.w || rca.w=="undefined") rcaMap[rcaid].w = BOX_DEFAULT_WIDTH;
            rca.h = Math.min(Math.max(int(rca.h), BOX_MIN_HEIGHT), BOX_MAX_HEIGHT);
            rca.w = Math.min(Math.max(int(rca.w), BOX_MIN_WIDTH), BOX_MAX_HEIGHT);

            while (this.rowHeights.length < this.rowCount) { this.rowHeights.push(0); this.rowMinHeights.push(BOX_DEFAULT_HEIGHT); }
            while (this.colWidths.length < this.colCount) { this.colWidths.push(0); this.colMinWidths.push(BOX_DEFAULT_WIDTH); }
            this.rowHeights[yx.y-1] = Math.max(this.rowHeights[yx.y-1], rca.h);
            this.colWidths[yx.x-1] = Math.max(this.colWidths[yx.x-1], rca.w);
            this.rowMinHeights[yx.y-1] = Math.min(this.rowMinHeights[yx.y-1], rca.h);
            this.colMinWidths[yx.x-1] = Math.min(this.colMinWidths[yx.x-1], rca.w);
        }

        for (var i=1; i<this.rowHeights.length; i++) {
            if (!this.rowHeights[i-1]) this.rowHeights[i-1] = BOX_DEFAULT_HEIGHT;
            this.rowsY[i] = this.rowsY[i-1] + this.rowHeights[i-1] + 30;
        }
        for (var i=1; i<this.colWidths.length; i++) {
            if (!this.colWidths[i-1]) this.colWidths[i-1] = BOX_DEFAULT_WIDTH;
            this.colsX[i] = this.colsX[i-1] + this.colWidths[i-1] + 30;
        }

        this.mapRealAreaHeight = this.rowsY[this.rowCount-1] + this.rowHeights[this.rowHeights.length-1] + 30;
        this.mapRealAreaWidth = this.colsX[this.colCount-1] + this.colWidths[this.colWidths.length-1] + 30;
        if (isCalculateOnly) return;
        this.resizeAllPanesToFit();
    },

    resizeAllPanesToFit : function(firstHeight, firstWidth){
        var rawHeight = getClientHeight();
        var rawWidth = getClientWidth();
        if (rawHeight==firstHeight && rawWidth==firstWidth) return;
        var rcaHeight = Math.max(350, rawHeight - 120);
        var rcaWidth = Math.max(600, rawWidth - 20);
        this.mapScrollSquareInner.style.height = px(rcaHeight-2);
        this.mapScrollSquareOuter.style.height = px(rcaHeight);

        if (cliplist && cliplist.isVisible()){
            this.headerMenu.style.width = px(rcaWidth - 260);
            this.mapScrollSquareInner.style.width = px(rcaWidth - 260 - 2); <? // 250-pad12 - border2 ?>
            this.mapScrollSquareOuter.style.width = px(rcaWidth - 260); <? // 250-pad12 - border2 ?>
            cliplist.divMainFrame.style.height = px(rcaHeight - 12); <? // padding:6px ?>
            cliplist.divMainFrame.style.left = px(8 + rcaWidth - 252 + 4);
            cliplist.divStockInner.style.height = px(rcaHeight - 212);
            cliplist.divStockOuter.style.height = px(rcaHeight - 210);
        } else {
            this.headerMenu.style.width = px(rcaWidth);
            this.mapScrollSquareInner.style.width = px(rcaWidth-2);
            this.mapScrollSquareOuter.style.width = px(rcaWidth);
        }
        this.mapWorldHeight = Math.max(parseInt(this.mapScrollSquareInner.style.height), this.mapRealAreaHeight+100);
        this.mapWorldWidth = Math.max(parseInt(this.mapScrollSquareInner.style.width), this.mapRealAreaWidth+100);
        mapWorld.style.width = px(this.mapWorldWidth);
        mapWorld.style.height = px(this.mapWorldHeight);

        if (int(firstHeight) && int(firstWidth)) return;
        setTimeout("cond.resizeAllPanesToFit("+rawHeight+","+rawWidth+")", 100);
    },

    mountToMapWorld : function(elem, which){
        if (which=="u") this.unmountFromMapWorld(elem);
        else if (elem.parentNode!=mapWorld) mapWorld.appendChild(elem);
    },

    unmountFromMapWorld : function(elem){
        if (elem.parentNode!=this.disabledContainer) this.disabledContainer.appendChild(elem);
    },

    isUnmountedFromMapWorld :function(elem){
        return (elem.parentNode == this.disabledContainer ? true : false);
    },

    getMouseYX : function(){ return {"y":this.mouseY,"x":this.mouseX}; },

    getBoxYXP : function(){ return {"y":this.boxY,"x":this.boxX, "position":this.position}; },

    getColWidth : function(x){ return this.colWidths[x-1]; },
    getRowHeight : function(y){ return this.rowHeights[y-1]; },
    getColMinWidth : function(x){ return this.colMinWidths[x-1]; },
    getRowMinHeight : function(y){ return this.rowMinHeights[y-1]; },
    getColX : function(x){ return this.colsX[x-1]; },
    getRowY : function(y){ return this.rowsY[y-1]; },

    clearGlobalPositions : function() {
        this.mouseX = this.mouseY = this.boxX = this.boxY = 0;
        this.position = "";
        this.locatedCounter++;
    },

    abortAllLockedFunctions : function(){
        resizer.abort();
        cliplist.resetFeature();
        joinner.abort();
        textEditor.abort();
    },

    resetMenu : function(){
        this.hideMenu();
        this.clearGlobalPositions();
    },

    hideMenu : function(){ this.hideBoxMenu(); this.hideLineMenu(); },
    hideBoxMenu : function (){
        this.boxmenuContainer.style.display = "none";
        resizer.hideButton();
        this.unmountFromMapWorld(toolmenu.boxname);
    },
    hideLineMenu : function (){ this.linemenuContainer.style.display = "none"; },

    showBoxMenu : function () {
        this.boxmenuContainer.style.left = px(this.getColX(this.boxX) + MAP_PADDING);
        this.boxmenuContainer.style.top = px(this.getRowY(this.boxY) + MAP_PADDING);
        this.boxmenuContainer.style.display = "";
        if (!cliplist.isFeatureing() && !textEditor.isEditing()) resizer.showButton(this.boxY, this.boxX);
        toolmenu.boxname.style.left = this.boxmenuContainer.style.left;
        toolmenu.boxname.style.top = px(this.getRowY(this.boxY) + MAP_PADDING - 14);
        this.mountToMapWorld(toolmenu.boxname);
    },

    showLineMenu : function (boxY, boxX) {
        this.linemenuContainer.style.paddingLeft = (this.position=="lineR" ? "5px" : "0");
        this.linemenuContainer.style.paddingTop = (this.position=="lineR" ? "0" : "5px");
        this.linemenuContainer.style.whiteSpace = (this.position=="lineR" ? "normal" : "nowrap");

        var bleft = this.getColX(boxX) + MAP_PADDING;
        if (this.position=="lineR") { if (boxY == 1) bleft += this.getColWidth(boxX); else bleft += bxu.getBoxWidth(boxY, boxX); }
        var btop = this.getRowY(boxY) + MAP_PADDING;
        if (this.position=="lineB") btop += bxu.getBoxHeight(boxY, boxX);

        this.linemenuContainer.style.left = px(bleft);
        this.linemenuContainer.style.top = px(btop);
        this.linemenuContainer.style.display = "";
    },

    setIngaModeOrNormalMode : function(im) {
        ingaMode = im;
//      this.linkInga.innerHTML = im ? "ﾁｬｰﾄ参照" : "因果図";
        this.linkInga.innerHTML = im ? "RCA図読み込み" : "因果連鎖検証";
//      this.imgIngaSelector.title = im ? "ﾁｬｰﾄ参照" : "因果図";
        this.imgIngaSelector.title = im ? "RCA図読み込み" : "因果連鎖検証";
        this.imgIngaSelector.src = im ? "img/color_15.gif" : "img/color_9.gif";
        this.ingaModeTitle.style.display = im ? "" : "none";
        this.tdLinkDiversion.style.display = im ? "none" : "";
        if (im) this.tdLinkIngaSelector.appendChild(document.getElementById("print_option"));

        if (im && bxu) {
            bxu.eliminateAllSitumonBox();
            if (ingaDataSwapFromNormalData) {
                alert("通常図をもとに因果図の構成を行いました。\n\nこの因果図は、まだデータベースに保存されていません。\n「最新状態」ボタンを押すことで、\n因果図を最終登録状態に戻すことができます。");

            }
        }
    },

    reverseNormalOrIngaMode : function(){
        var renewIngaMode = "";
        if (ingaMode) {
            renewIngaMode = "&renew_inga_mode=yes";
            if(!confirm("画面のリロードを行い、\nデータベース上の通常図データをもとに、因果図を再生成します。\n\n現在編集中の因果図データは、クリアされます。\nデータベースに通常図の情報がない場合は、データベース上の因果図を表示します。\n\n実行してよろしいですか？")) return;
        }
        else {
            if(!confirm("因果図の編集モードに切り替えます。\n現在編集中の通常図データは、クリアされます。\n\nこの通常図に対する因果図が登録されていなければ、\nこの通常図の登録情報から、因果図を自動作成します。\n\n実行してよろしいですか？")) return;
        }
        window.location.href = "hiyari_analysis_rca_chart.php?a_id="+analysisId+"&session=<?=$session?>&inga_mode=yes"+renewIngaMode;
    }
}

<? //------------------------------------------------------------------------------ ?>
<? // イベント                                                                      ?>
<? //------------------------------------------------------------------------------ ?>
window.document.onmousemove = function(e){ windowMouseMoveEvent(e); }
function windowMouseMoveEvent(e) { if (cond) cond.execWindowMouseMoveEvent(e); }

function mapWorldMouseDownEvent(){
    if (resizer.isResizing()){ resizer.execResize(); }
}

function windowResizeEvent(){
    if (cond) cond.resizeAllPanesToFit();
}

function boxClickEvent(td){
    var id = td.parentNode.parentNode.parentNode.parentNode.id.split("_");
    var Y = id[1];
    var X = id[2];
    if (cond.isSystemDisabled) return;
    if (resizer.isResizing()) return;
    if (cliplist.isFeatureing()) { cliplist.pasteIfPastable(Y,X); return; }
    if (joinner.isJoinning()) { joinner.execJoin(Y,X); return; }
}

function btnAddBoxClickEvent(command){
    var yxp = cond.getBoxYXP();
    addBox(yxp.y, yxp.x, yxp.position, command);
}

function windowLoadEvent(){
    cond = new CGlobalCondition();
    if (!int(analysisId) && !int(chartSeq)) cond.isSystemDisabled = true;
    bxu = new CBoxUtil();

    if (!bxu.count(rcaMap)) cond.setDefaultMinimumChart();

    textEditor = new CTextEditor();
    toolmenu = new CToolMenu();
    cliplist = new CCliplist(clipInitData);
    undo = new CUndo();
    joinner = new CLineJoinner();
    resizer = new CBoxResizer();
    printOption = new CPrintOption();
    lineRouteCalculator = new CLineRouteCalculator();

    cond.isPageInitialLoaded = true;

    adjustSpace();
    cond.refreshMapWorldScales(true);
    lineRouteCalculator.calculate(true);
    drawMap(lineRouteCalculator.getResult());

    cond.setIngaModeOrNormalMode(ingaMode);
}

function doCloseTask(){
    if (diversion_window && !diversion_window.closed) diversion_window.close();
    if (guidance_window && !guidance_window.closed) guidance_window.close();
    window.location.href = "hiyari_analysis_rca_chart.php?session=<?=$session?>&self_unlock=yes";
}

</script>

<style type="text/css">
body, table, input, textarea, select { font-family:"Hiragino Kaku Gothic Pro W3","ヒラギノ角ゴ Pro W3",Osaka,"MS P Gothic","ＭＳ Ｐゴシック",sans-serif; }
body { padding:0; margin:0; background:url(inci/rca/img/bg_titleback.png) repeat-x #eaebea; }
.ptr { cursor:pointer }
.btnborder { border:1px solid #dadada; border-left:1px solid #eaeaea; border-top:1px solid #fafafa; }
.abs { position:absolute }
table { font-size:13px }
.entity { border-collapse:collapse; border:0;}
.entity td { padding:5px; text-align:center; vertical-align:middle; cursor:default;}
.clip { margin:2px; float:left; border:1px solid #aaa; padding:3px; cursor:pointer; font-size:13px; }
label { cursor:pointer; font-size:13px; }
.fs14px { font-size:14px; }
.fs13px { font-size:13px; }
.fs12px { font-size:12px; }
.collapse { border-collapse:collapse }
.collapse td { padding:0 }
.always_green { color:#89b988; text-decoration:none; }
.always_green:hover { color:#ff5809; text-decoration:none; }
.always_green2 { color:#35b341; text-decoration:none; }
.always_green2:hover { color:#ff5809; text-decoration:none; }
.a_help { color:#050; }
.a_help:hover { color:#fff; }


#map_scroll_square_outer { border:1px solid #777; position:absolute; left:8px; top:108px; z-index:800 }
#map_scroll_square_inner { background-color:#fff; position:absolute; border:1px solid #ccc; overflow:scroll; left:0px; top:0px }
#map_world { background-image:url(inci/rca/img/bg_world.jpg); }

.boxdiv_koutei { border-left:1px solid #eeee99; border-right:1px solid #c7cb2e; border-top:1px solid #f0f0aa; border-bottom:1px solid #8b8f36; background-image:url(inci/rca/img/bg_box_yellow.png); background-color:#ff7; }
.boxdiv_kekka { border-left:1px solid #bbbbbb; border-right:1px solid #999999; border-top:1px solid #eee; border-bottom:1px solid #777777; background-image:url(inci/rca/img/bg_box_gray.png); background-color:#ccc; }
.boxdiv_situmon { border-left:1px solid #4b9ae9; border-right:1px solid #508ac5; border-top:1px solid #9bc8ff; border-bottom:1px solid #366ca3; background-image:url(inci/rca/img/bg_box_blue.png); background-color:#7bf; }
.boxdiv_genin { border-left:1px solid #ea84b7; border-right:1px solid #bd6096; border-top:1px solid #ffc4dd; border-bottom:1px solid #964776; background-image:url(inci/rca/img/bg_box_red.png); background-color:#f9c }
.boxdiv_konpon { border-left:1px solid #d97762; border-right:1px solid #bc5b43; border-top:1px solid #ffbab0; border-bottom:1px solid #8f5143; background-image:url(inci/rca/img/bg_box_orange.png); background-color:#f87 }
#print_title { display:none }
</style>
<style type="text/css" media="print">
#map_scroll_square_outer { border:0; position:relative; left:0; top:0; overflow:visible }
#map_scroll_square_inner { background:0; position:relative; border:0; overflow:visible; left:0; top:0 }
#map_world { background:0; }
#pre_chart { display:none }
#post_chart { display:none }
body { padding:0; margin:0; background:0; background-color:#fff }
.entity { background-color:#fff; }
.boxdiv_pass { border:0; background:0; background-color:transparent; }
.boxdiv_koutei { border:1px solid #888; background:0; }
.boxdiv_kekka { border:1px solid #888; background:0; }
.boxdiv_situmon { border:1px solid #888; background:0; }
.boxdiv_genin { border:1px solid #888; background:0; }
.boxdiv_konpon { border:1px solid #888; background:0; }
.boxdiv_pass { border:0; background:0; background-color:transparent; }
#print_title { display:block }
</style>

</head>
<title>CoMedix RCA分析チャート | ファントルくん | 出来事分析</title>
<body onload="windowLoadEvent(); windowResizeEvent();" onresize="windowResizeEvent();">

<? // 印刷用タイトル ?>
<div id="print_title">
    <table><tr>
        <td>
            <table class="collapse">
                <tr><td style="border:1px solid #888; background-color:#ddd; text-align:center; padding:3px; white-space:nowrap">分析番号</td></tr>
                <tr><td style="border:1px solid #888; background-color:#fff; text-align:center; padding:3px; white-space:nowrap" id="rca_analysis_no"><?=h($analysis_no)?><br/></td></tr>
            </table>
        </td>
        <td style="font-size:18px; padding-left:16px"><?=h($title)?><br/></td>
    </tr></table>
</div>



<div id="pre_chart">

<input type="text" id="txt1" value="(debug message)" style="width:500px; position:absolute; left:200; top:0; background-color:transparent; border:0; display:none" />

<? // タイトルヘッダ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #3f7844">
    <tr height="32" bgcolor="#35B341">
        <td style="color:#fff; padding-left:4px; font-size:14px; width:50%">
            <div style="float:left; border:1px solid #35B341; padding-top:2px"><b>ＲＣＡ分析チャート</b></div>
            <div style="float:left; margin-left:20px; padding:2px 6px; border:1px solid #ff0; background-color:#aa0; color:#fff; display:none" id="inga_mode_title">因果図</div>
            <br style="clear:both" />
        </td>
        <td style="width:50%; text-align:right; color:#fff;">
        <span id="edit_lock_user_name"><?=@$editing_emp_name ? "編集ロック中：": ""?><?=@$editing_emp_name?></span>
            <input type="button" id="btn_edit_unlock" value="ロック強制解除" onclick="editUnlock();" <?=@$editing_emp_name ? '': 'style="display:none"'?> class="ptr" />
            <input type="button" id="btn_edit_lock" value="編集ロックの実施" onclick="editLock();" <?=@$editing_emp_name ? 'style="display:none"' : ''?> class="ptr" />


        </td>
        <td width="50" align="center" style="padding-left:20px"><!--a href="inci/rca/help/rca_help_part1.xls" target="_blank" class="a_help"><nobr>ヘルプ</nobr></a--></td>
        <td width="50px" style="text-align:right; padding-left:10px; padding-right:4px"><a href="" onclick="doCloseTask(); return false;"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
</table>


<? // ＲＣＡタイトル・更新・印刷 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td style="width:95%;">
            <div style="padding:8px"><span style="font-size:18px" id="rca_title"><?=h($title)?><br/></span></div>
        </td>
        <td style="text-align:center; width:150px; padding:4px 20px; white-space:nowrap">
            <table class="collapse">
                <tr><td style="border:1px solid #35b341; background-color:#dfffdc; text-align:center; padding:3px; white-space:nowrap">分析番号</td></tr>
                <tr><td style="border:1px solid #35b341; text-align:center; padding:3px; white-space:nowrap" id="rca_analysis_no"><?=h($analysis_no)?><br/></td></tr>
            </table>
        </td>
        <td style="text-align:center; color:#13781D; padding:4px 6px;" id="td_link_inga_selector">
            <a href="" onclick="this.blur(); cond.reverseNormalOrIngaMode(); return false;" class="always_green fs12px">
            <img id="img_inga_selector" src="img/spacer.gif" width="32" height="32" style="border:0; margin:1px; margin-bottom:3px" title="" /><br/><nobr id="link_inga"></nobr></a>
        </td>
        <td style="text-align:center; color:#13781D; padding:4px 6px;">
            <a href="" onclick="this.blur(); dataReload(); return false;" class="always_green fs12px">
            <img src="inci/rca/img/mm_refresh.gif?a=d" style="border:0; margin:1px; margin-bottom:3px" title="最新状態" /><br/><nobr>最新状態</nobr></a>
            <? // 印刷オプション ?>
            <div id="print_option" style="position:absolute; z-index:7000; padding-top:4px; width:200px; display:none">
                <div style="background-color:#fff; border:1px solid #888; text-align:left; width:250px">
                    <div style="background-color:#35b341; padding:3px">
                        <div style="float:right"><a href="" onclick="printOption.hide(); return false;" class="a_help fs13px">閉じる</a></div>
                        <br style="clear:both" />
                    </div>
                    <div style="border:1px solid #ccc; padding:4px; padding-left:8px;">
                        <a href="" onclick="printOption.execPrint('xls','y'); return false;" class="always_green2 fs13px" style="display:block; width:100%; padding:4px; padding-left:20px; background:url(img/icon/file.gif) no-repeat;">Excelファイル出力（カラー）（.xls）</a>
                        <a href="" onclick="printOption.execPrint('pdf','y'); return false;" class="always_green2 fs13px" style="display:block; width:100%; padding:4px; padding-left:20px; background:url(img/icon/file.gif) no-repeat;">PDFファイル出力（カラー）</a>
                        <a href="" onclick="printOption.execPrint('pdf',''); return false;" class="always_green2 fs13px" style="display:block; width:100%; padding:4px; padding-left:20px; background:url(img/icon/file.gif) no-repeat;">PDFファイル出力（白黒）</a>
                    </div>
                </div>
            </div>
        </td>
        <td style="text-align:center; color:#13781D; padding:4px 6px;">
            <a href="" onclick="this.blur(); if (cond.alertIfSystemDisabled()) return false; cliplist.changeDisplay(); return false;" class="always_green fs12px">
            <img src="inci/rca/img/mm_cliplist.gif?a=c" style="border:0; margin:1px; margin-bottom:3px" title="ｸﾘｯﾌﾟﾘｽﾄ" /><br/><nobr>ｸﾘｯﾌﾟﾘｽﾄ</nobr></a>
        </td>
        <td style="text-align:center; color:#13781D; padding:4px 6px;" id="td_link_diversion">
            <a href="" onclick="this.blur(); diversionWindowPopup(); return false;" class="always_green fs12px">
            <img src="inci/rca/img/mm_load.gif" style="border:0; margin:1px; margin-bottom:3px" title="ﾃﾞｰﾀ読込" /><br/><nobr>ﾃﾞｰﾀ読込</nobr></a>
        </td>
        <?php if ( $update_flg===true ) { ?>
        <td style="text-align:center; color:#13781D; padding:4px 6px;">
            <a href="" onclick="this.blur(); if (cond.alertIfSystemDisabled()) return false; inquiryRegist(); return false;" class="always_green fs12px">
            <img src="inci/rca/img/mm_save.gif" style="border:0; margin:1px; margin-bottom:3px" title="保存" /><br/><nobr>保存</nobr></a>
        </td>
        <?php } ?>
        <td style="text-align:center; color:#13781D; padding:4px 6px;">
            <a href="" onclick="this.blur(); if (cond.alertIfSystemDisabled()) return false; printOption.showAlternate(); return false;" class="always_green fs12px">
            <img src="inci/rca/img/mm_print.gif" style="border:0; margin:1px; margin-bottom:3px;" title="印刷" /><br/><nobr>印刷</nobr></a>
        </td>
        <td style="text-align:center; color:#13781D; padding:4px 6px;">
            <a href="" onclick="this.blur(); guidanceWindowPopup(); return false;" class="always_green fs12px">
            <img src="inci/rca/img/mm_guidance.gif" style="border:0; margin:1px; margin-bottom:3px;" title="ｶﾞｲﾀﾞﾝｽ" /><br/><nobr>ｶﾞｲﾀﾞﾝｽ</nobr></a>
        </td>
    </tr>
</table>




<? // ヘッダメニュー ?>
<div id="headermenu_container" style="position:absolute; left:8px; top:88px; height:18px; font-size:13px; z-index:600">

    <? // アンドゥリンク、クリップリスト表示リンク ?>
    <div style="float:left; padding-right:20px; display:none" id="undo_container">
        <a href="" id="map_undo" onclick="undo.undoMap(); return false;"><img src="inci/rca/img/gb_undo.png" style="border:0" onmouseover="btnSwap(this, 'gb_undo_on.png');" onmouseout="btnSwap(this, 'gb_undo.png');" title="変更を一段階戻す（アンドゥ）" /></a>
        <a href="" id="map_redo" onclick="undo.redoMap(); return false;"><img src="inci/rca/img/gb_redo.png" onmouseover="btnSwap(this, 'gb_redo_on.png');" onmouseout="btnSwap(this, 'gb_redo.png');" style="border:0" title="変更を一段階進める（リドゥ）" /> </a>
    </div>

    <? // クリップ貼り付け作業 ?>
    <div id="clip_paste_selector" style="display:none; float:left">
        <img src="inci/rca/img/yy_clip.gif"/>
        <img src="inci/rca/img/cc_cancel_task.gif" alt="" class="ptr btnborder" onclick="cliplist.resetFeature();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />
    </div>

    <? // 要素のジョイント作業 ?>
    <div id="line_joinner_selector" style="display:none; float:left">
        <img src="inci/rca/img/yy_join.gif" style="float:left" />
        <img src="inci/rca/img/cc_cancel_task.gif" alt="" class="ptr btnborder" onclick="joinner.abort();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" style="float:left" />
        <div id="join_message" style="float:left; margin-left:6px; padding:2px; color:#999; background-color:#ffffbb"></div>
        <br style="clear:both"/>
    </div>

    <? // リサイズ時の目盛り選択ラジオボタン ?>
    <div id="resize_pixel_step_selector" style="display:none; float:left">
        <div style="float:left">
            <img src="inci/rca/img/yy_resize.gif"/>
            <img src="inci/rca/img/cc_cancel_task.gif" alt="" class="ptr btnborder" onclick="resizer.abort();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />
        </div>
        <div style="float:left; margin-left:6px; padding:2px; background-color:#ffffbb">
            リサイズ単位：
            <label><input type="radio" name="resize_pixel_step" onchange="if(this.checked) resizer.setPixelStep(1);" style="padding:0; margin:0; border:0; vertical-align:baseline" checked />
            1</label>
            <label><input type="radio" name="resize_pixel_step" onchange="if(this.checked) resizer.setPixelStep(4);" style="padding:0; margin:0; border:0; vertical-align:baseline" />
            4</label>
            <label><input type="radio" name="resize_pixel_step" onchange="if(this.checked) resizer.setPixelStep(8);" style="padding:0; margin:0; border:0; vertical-align:baseline" />
            8</label>
            <label><input type="radio" name="resize_pixel_step" onchange="if(this.checked) resizer.setPixelStep(16);" style="padding:0; margin:0; border:0; vertical-align:baseline" />
            16</label>
        </div>
    </div>

    <? // テキストエディタの編集後のオートリサイズ選択 ?>
    <div id="text_editor_auto_resize_selector" style="display:none; float:left;">
        <div style="float:left"><img src="inci/rca/img/yy_textedit.gif"/></div>
        <div style="float:left; margin-left:6px; padding:2px; background-color:#ffffbb">
        <label><input type="checkbox" name="text_editor_auto_resize" style="padding:0; margin:0; border:0; vertical-align:baseline"
            onchange="textEditor.setAfterAutoResize(this.checked);" />
            変更時自動リサイズ</label>
        </div>

        <div style="float:left; margin-left:6px; padding:2px; background-color:#ffffbb">
        <label><input type="radio" name="text_editor_uwagaki_tuiki" style="padding:0; margin:0; border:0; vertical-align:baseline"
            onchange="textEditor.setUwagakiTuiki('uwagaki');" />
            上書き</label>
        <label><input type="radio" name="text_editor_uwagaki_tuiki" style="padding:0; margin:0; border:0; vertical-align:baseline"
            onchange="textEditor.setUwagakiTuiki('tuiki');" checked />
            追記</label>
        </div>
        <br style="clear:both"/>
    </div>

    <br style="clear:both"/>
</div>

<? // 擬似マーキー クリップ追加通知 ?>
<div id="clip_added_notice" style="position:absolute; top:112px; left:200px; z-index:610; display:none">
    <img src="inci/rca/img/yy_addclip_notice.gif"/><!--
    --><img id="btn_show_cliplist" src="inci/rca/img/cc_show_cliplist.gif" alt="" class="ptr btnborder"
        onclick="cliplist.show();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />
</div>

</div>
<? // end of "pre_chart" ?>


<? // チャート図 メイン ?>
<div id="map_scroll_square_outer"><div id="map_scroll_square_inner"></div></div>


<div id="post_chart">

<? // テキストエディタの自動改行対策 ?>
<textarea id="textedit_dummy" style="overflow:hidden; position:absolute; left:0; top:-1000px; display:none; font-size:12px; letter-spacing:1px"></textarea>


<? // ツールバー・ボックスメニュー ?>
<div id="boxmenuContainer" class="abs" style="display:none; z-index:3000; white-space:nowrap">
    <img src="inci/rca/img/cb_delbox.png" alt="" class="ptr btnborder" id="delbox" title="この要素を削除"
    onclick="btnDelBoxClick(cond.getBoxYXP());" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_textedit.png" alt="" class="ptr btnborder" id="btn_textedit" title="この要素のテキストを編集"
    onclick="btnTextEditClick();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_changecolor.png" alt="" class="ptr btnborder" id="btn_changecolor" title="この要素の色（属性）の変更"
    onclick="btnChangeColorClick();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_clip.png" alt="" class="ptr btnborder" id="btn_clip" title="クリップリストに記憶"
    onclick="cliplist.addEntryFromMap();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />
</div>


<? // ツールバー・線用メニュー ?>
<div id="linemenuContainer" class="abs" style="display:none; z-index:3000; white-space:nowrap">
    <img src="inci/rca/img/cb_addboxY.png" alt="" class="ptr btnborder" id="btn_addboxY" title="「工程」要素を挿入"
    onclick="btnAddBoxClickEvent('Y');" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_addboxB.png" alt="" class="ptr btnborder" id="btn_addboxB" title="「質問」要素を上要素の直下に挿入"
    onclick="btnAddBoxClickEvent('B');" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_addboxR.png" alt="" class="ptr btnborder" id="btn_addboxR" title="「原因」要素を上要素の直下に挿入"
    onclick="btnAddBoxClickEvent('R');" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_addboxBR.png" alt="" class="ptr btnborder" id="btn_addboxBR" title="「質問」と「原因」要素を上要素の直下に連続挿入"
    onclick="btnAddBoxClickEvent('BR');" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_addboxRB.png" alt="" class="ptr btnborder" id="btn_addboxRB" title="「原因」と「質問」要素を上要素の直下に連続挿入"
    onclick="btnAddBoxClickEvent('RB');" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_addboxV.png" alt="" class="ptr btnborder" id="btn_addboxV" title="「根本原因」要素を上要素の直下に挿入"
    onclick="btnAddBoxClickEvent('V');" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_cutline.png" alt="" class="ptr btnborder" id="btn_cutline" title="上の要素から伸びる矢印線の削除"
    onclick="cutLine();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
    --><img src="inci/rca/img/cb_joinner.png" alt="" class="ptr btnborder" id="btn_joinner" title="上の要素から矢印線を作成"
    onclick="joinner.startJoin();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />
</div>


<? // パーツストック ?>
<div id="disabled_container" class="abs" style="display:none;">
    <img src="inci/rca/img/dot_r.gif" alt="" class="abs" id="border_rt" style="z-index:6020" />
    <img src="inci/rca/img/dot_r.gif" alt="" class="abs" id="border_rl" style="z-index:6020" />
    <img src="inci/rca/img/dot_r.gif" alt="" class="abs" id="border_rr" style="z-index:6020" />
    <img src="inci/rca/img/dot_r.gif" alt="" class="abs" id="border_rb" style="z-index:6020" />
    <img src="inci/rca/img/dot_s.gif" alt="" class="abs" id="border_st" style="z-index:6020" />
    <img src="inci/rca/img/dot_s.gif" alt="" class="abs" id="border_sl" style="z-index:6020" />
    <img src="inci/rca/img/dot_s.gif" alt="" class="abs" id="border_sr" style="z-index:6020" />
    <img src="inci/rca/img/dot_s.gif" alt="" class="abs" id="border_sb" style="z-index:6020" />
    <img src="inci/rca/img/dot_e.gif" alt="" class="abs" id="border_et" style="z-index:6020" />
    <img src="inci/rca/img/dot_e.gif" alt="" class="abs" id="border_el" style="z-index:6020" />
    <img src="inci/rca/img/dot_e.gif" alt="" class="abs" id="border_er" style="z-index:6020" />
    <img src="inci/rca/img/dot_e.gif" alt="" class="abs" id="border_eb" style="z-index:6020" />

    <img src="inci/rca/img/dot_f.gif" alt="" class="abs" id="border_ft" style="z-index:6020" />
    <img src="inci/rca/img/dot_f.gif" alt="" class="abs" id="border_fl" style="z-index:6020" />
    <img src="inci/rca/img/dot_f.gif" alt="" class="abs" id="border_fr" style="z-index:6020" />
    <img src="inci/rca/img/dot_f.gif" alt="" class="abs" id="border_fb" style="z-index:6020" />
    <img src="inci/rca/img/dot_h.gif" alt="" class="abs" id="border_ht" style="z-index:6020" />
    <img src="inci/rca/img/dot_h.gif" alt="" class="abs" id="border_hl" style="z-index:6020" />
    <img src="inci/rca/img/dot_h.gif" alt="" class="abs" id="border_hr" style="z-index:6020" />
    <img src="inci/rca/img/dot_h.gif" alt="" class="abs" id="border_hb" style="z-index:6020" />

    <img src="inci/rca/img/cp_B.png" alt="" class="abs" id="btn_pasteboxB" style="z-index:6020" />
    <img src="inci/rca/img/cp_R.png" alt="" class="abs" id="btn_pasteboxR" style="z-index:6020" />
    <img src="inci/rca/img/cp_Y.png" alt="" class="abs" id="btn_pasteboxY" style="z-index:6020" />
    <img src="inci/rca/img/cp_V.png" alt="" class="abs" id="btn_pasteboxV" style="z-index:6020" />
    <img src="inci/rca/img/cp_G.png" alt="" class="abs" id="btn_pasteboxG" style="z-index:6020" />
    <img src="inci/rca/img/cp_Bok.png" alt="" class="abs" id="btn_pasteboxBok" style="z-index:6020" />
    <img src="inci/rca/img/cp_Rok.png" alt="" class="abs" id="btn_pasteboxRok" style="z-index:6020" />
    <img src="inci/rca/img/cp_Yok.png" alt="" class="abs" id="btn_pasteboxYok" style="z-index:6020" />
    <img src="inci/rca/img/cp_Vok.png" alt="" class="abs" id="btn_pasteboxVok" style="z-index:6020" />
    <img src="inci/rca/img/cp_Gok.png" alt="" class="abs" id="btn_pasteboxGok" style="z-index:6020" />
    <img src="inci/rca/img/cp_Bng.png" alt="" class="abs" id="btn_pasteboxBng" style="z-index:6020" />
    <img src="inci/rca/img/cp_Rng.png" alt="" class="abs" id="btn_pasteboxRng" style="z-index:6020" />
    <img src="inci/rca/img/cp_Yng.png" alt="" class="abs" id="btn_pasteboxYng" style="z-index:6020" />
    <img src="inci/rca/img/cp_Vng.png" alt="" class="abs" id="btn_pasteboxVng" style="z-index:6020" />
    <img src="inci/rca/img/cp_Gng.png" alt="" class="abs" id="btn_pasteboxGng" style="z-index:6020" />

    <img src="inci/rca/img/cp_balloon.png" alt="" class="abs" id="btn_balloon" style="z-index:6020" />


    <div style="width:30px; white-space:nowrap" class="abs fs12px" id="boxname"></div>

    <img src="inci/rca/img/cb_resize.png" alt="" class="ptr btnborder" id="btn_resizer" title="この要素のサイズ調整"
    onclick="resizer.startResize();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" style="position:absolute; z-index:3010" />

    <img src="inci/rca/img/dot_g.gif" alt="" class="abs" id="resizer_border_v" style="width:1px; z-index:6010" />
    <img src="inci/rca/img/dot_g.gif" alt="" class="abs" id="resizer_border_h" style="height:1px; z-index:6010" />

    <div class="abs" id="resizer_ok_ng" style="z-index:6000; cursor:default;"></div>

    <textarea id="textedit" style="display:none; position:absolute; border:0; font-size:12px; overflow:hidden; letter-spacing:1px; z-index:4000" onkeyup="textEditor.adjustHeight();"></textarea>

    <? // テキストエディット ＯＫボタン ?>
    <div id="textedit_ok_container" style="position:absolute; z-index:6010; white-space:nowrap">
        <input type="image" src="inci/rca/img/cc_textedit_ok.gif" class="ptr btnborder"
            id="textedit_ok" onclick="textEditor.applyChange();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />

        <input type="image" src="inci/rca/img/cc_textedit_ok_mini.gif" class="ptr btnborder"
            id="textedit_ok_mini" onclick="textEditor.applyChange();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /><!--
--><input type="image" src="inci/rca/img/cc_paste_string.gif" class="ptr btnborder"
            id="textedit_paste_string" onclick="textEditor.pasteString();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />
    </div>
</div>






<? // クリップリスト ?>
<div id="cbd_main_frame" class="abs fs14px" style="z-index:5000; height:300px; width:250px; position:absolute; left:8px; top:108px; display:none">


    <? // クリップリストペイン タイトル ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="height:24px">
        <tr bgcolor="#ffffff">
            <td style="width:210px; border:1px solid #13ca25; background-image:url(inci/rca/img/bn_cliptitle.gif)"><img src="img/spacer.gif"/></td>
            <td width="10%" style="text-align:right;"><a href="" onclick="cliplist.hide(); return false;"><img src="inci/rca/img/gb_clip_hide.png" alt="閉じる" width="24" height="24" border="0" onmouseover="btnSwap(this, 'gb_clip_hide_on.png');" onmouseout="btnSwap(this, 'gb_clip_hide.png');" title="クリップリストを隠す" /></a></td>
        </tr>
    </table>


    <? // クリップリストペイン プレビュー ?>
    <div id="cbd_content" style="margin-top:4px; height:120px; background-image:url(inci/rca/img/bg_preview1.gif?);">
        <div id="cbd_delete" style="float:right; text-align:right; display:none"><img src="inci/rca/img/cc_delclip.gif?" alt="" class="ptr btnborder" id="delclip" onclick="cliplist.deleteEntry(cliplist.currentId);" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" /></div>
        <br style="clear:both"/>
        <div id="cbd_text" style="padding:8px; text-align:center;" class="fs13px"></div>
    </div>


    <? // クリップリストキャンセルボタン ?>
    <div style="padding:4px; height:32px; text-align:center; color:#3489df" class="fs12px">
        <img id="cbd_command" src="inci/rca/img/cc_cancel_clip.gif" alt="" class="ptr btnborder"
        onclick="cliplist.resetFeature();" onmouseover="btnHover(this)" onmouseout="btnBlur(this);" />
        <div id="cbd_command_message">現在、クリップは登録されていません。</div>
    </div>


    <? // クリップリストペイン クリップリスト スクロールフレーム ?>
    <div id="cbd_stock_outer" style="height:100px; border:1px solid #777">
        <div id="cbd_stock_inner" style="height:98px; overflow:scroll; border:1px solid #ccc; background-color:#fff; background-image:url(inci/rca/img/bg_cliplist_stock.png)" onmouseout="cliplist.mouseOutEvent();">
            <table class="collapse" style="width:100%;">
                <tr><td style="height:24px; background-image:url(inci/rca/img/bn_clipsubheadY.gif);"></td></tr>
                <tr><td id="cbd_stock_y" style="padding:4px; padding-bottom:20px;"></td></tr>
                <tr><td style="height:24px; background-image:url(inci/rca/img/bn_clipsubheadG.gif);"></td></tr>
                <tr><td id="cbd_stock_g" style="padding:4px; padding-bottom:20px;"></td></tr>
                <tr><td style="height:24px; background-image:url(inci/rca/img/bn_clipsubheadB.gif);"></td></tr>
                <tr><td id="cbd_stock_b" style="padding:4px; padding-bottom:20px;"></td></tr>
                <tr><td style="height:24px; background-image:url(inci/rca/img/bn_clipsubheadR.gif);"></td></tr>
                <tr><td id="cbd_stock_r" style="padding:4px; padding-bottom:20px;"></td></tr>
                <tr><td style="height:24px; background-image:url(inci/rca/img/bn_clipsubheadV.gif);"></td></tr>
                <tr><td id="cbd_stock_v" style="padding:4px; padding-bottom:20px;"></td></tr>
                <!-- tr><td style="height:24px; background-image:url(inci/rca/img/bn_clipsubheadS.gif);"></td></tr>
                <tr><td id="cbd_stock_s" style="padding:4px;"></td></tr -->
            </table>
            <br style="clear:both" />
        </div>
    </div>


    <? // クリップリストペイン サービスコマンド ?>
    <div style="padding-top:2px; height:20px; float:right">
        <img src="inci/rca/img/gb_clip_dec.png" alt="" class="ptr btnborder" title="表示文字数を減らす"
            onclick="cliplist.decPrefixSize();" onmouseover="btnSwap(this, 'gb_clip_dec_on.png')" onmouseout="btnSwap(this, 'gb_clip_dec.png')" /><!--
 --><img src="inci/rca/img/gb_clip_inc.png" alt="" class="ptr btnborder" title="表示文字数を増やす"
            onclick="cliplist.incPrefixSize();" onmouseover="btnSwap(this, 'gb_clip_inc_on.png')" onmouseout="btnSwap(this, 'gb_clip_inc.png')" />
    </div>
    <br style="clear:both"/>

</div>
<? // クリップリスト終了 ?>


</div><? // end of "post_chart" ?>


<form name="frm_print" target="hiyari_analysis_rca_chart_print_window" action="hiyari_analysis_rca_chart_print.php?session=<?=$session?>" method="post">
    <input type="hidden" name="session" value="<?=$session?>"/>
    <input type="hidden" name="print_chart_data" value=""/>
    <input type="hidden" name="print_line_data" value=""/>
    <input type="hidden" name="print_row_heights_data" value=""/>
    <input type="hidden" name="print_col_widths_data" value=""/>
    <input type="hidden" name="print_title" value="<?=h($title)?>"/>
    <input type="hidden" name="print_analysis_no" value="<?=h($analysis_no)?>"/>
    <input type="hidden" name="print_xls_or_pdf" value=""/>
    <input type="hidden" name="print_area_width" value=""/>
    <input type="hidden" name="print_area_height" value=""/>
    <input type="hidden" name="print_coloring" value=""/>
</form>

</body>
</html>
