<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 入力値の編集・チェック

//// モード
$mode = trim($mode);
if ($mode != "reserve" && $mode != "register") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('処理モードが正しくないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//// 患者登録権限
$pt_reg_auth = check_authority($session, 22, $fname);
if ($pt_reg_auth != "1") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('患者登録権限がないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//// 患者ID
$ptif_id = trim($patient_id);
if ($ptif_id == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$_label_by_profile["PATIENT_ID"][$profile_type]}が取得できないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (preg_match("/[^0-9a-zA-Z]/", $ptif_id) > 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$_label_by_profile["PATIENT_ID"][$profile_type]}に半角英数字以外の文字が含まれるため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($ptif_id) > 12) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$_label_by_profile["PATIENT_ID"][$profile_type]}が長すぎるため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//// 患者氏名（漢字）
$pt_nm = trim(mb_convert_kana($kanji_name, "s"));
$pt_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_nm);
list($lt_nm, $ft_nm) = explode(" ", $pt_nm);
if ($lt_nm == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('苗字（漢字）が取得できないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($lt_nm) > 20) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎるため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($ft_nm == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('名前（漢字）が取得できないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($ft_nm) > 20) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎるため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//// 患者氏名（かな）
$pt_kana_nm = trim(mb_convert_kana($kana_name, "HVcs"));
$pt_kana_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_kana_nm);
list($lt_kana_nm, $ft_kana_nm) = explode(" ", $pt_kana_nm);
if ($lt_kana_nm == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('苗字（かな）が取得できないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($lt_kana_nm) > 20) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('苗字（かな）が長すぎるため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($ft_kana_nm == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('名前（かな）が取得できないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($ft_kana_nm) > 20) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('名前（かな）が長すぎるため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$keyword = get_keyword($lt_kana_nm);

//// 性別
$sex = trim($sex);
if ($sex != "0" && $sex != "1" && $sex != "2") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('性別コードが正しくないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//// 生年月日
$birth = trim($birth);
if (preg_match("/^\d{8}$/", $birth) == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('生年月日が正しくないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$birth_yr = substr($birth, 0, 4);
$birth_mon = substr($birth, 4, 2);
$birth_day = substr($birth, 6, 2);
if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('生年月日が正しくないため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//// 郵便番号
$zip = isset($zip) ? trim($zip) : "";
if ($zip != "") {
	if (preg_match("/^\d{7}$/", $zip) == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('郵便番号が正しくないため、処理を中断しました。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$zip1 = substr($zip, 0, 3);
	$zip2 = substr($zip, 3, 4);
} else {
	$zip1 = "";
	$zip2 = "";
}

//// 住所
$address = isset($address) ? trim($address) : "";
$prv = null;
$prefectures = CoMedix_Prefecture::getList();
foreach ($prefectures as $pref_id => $prefecture) {
	if (strpos($address, $prefecture) === 0) {
		$prv = $pref_id;
		$address = substr($address, strlen($prefecture));
		break;
	}
}
if (strlen($address) > 100) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('住所が長すぎるため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//// 電話番号
$tel = isset($tel) ? trim($tel) : "";
if ($tel != "") {
	if (preg_match("/^(\d{1,6})-(\d{1,6})-(\d{1,6})$/", $tel, $matches)) {
		$tel1 = $matches[1];
		$tel2 = $matches[2];
		$tel3 = $matches[3];
	} else {
		$tel = str_replace("-", "", $tel);
		if (!preg_match("/^\d{0,12}$/", $tel)) {
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('電話番号が正しくないため、処理を中断しました。');</script>\n");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		$tel1 = substr($tel, 0, 4);
		$tel2 = substr($tel, 4, 4);
		$tel3 = substr($tel, 8);
	}
} else {
	$tel1 = "";
	$tel2 = "";
	$tel3 = "";
}

//// 勤務先電話番号
$biz_tel = isset($biz_tel) ? trim($biz_tel) : "";
if ($biz_tel != "") {
	if (preg_match("/^(\d{1,6})-(\d{1,6})-(\d{1,6})$/", $biz_tel, $matches)) {
		$biz_tel1 = $matches[1];
		$biz_tel2 = $matches[2];
		$biz_tel3 = $matches[3];
	} else {
		$biz_tel = str_replace("-", "", $biz_tel);
		if (!preg_match("/^\d{0,12}$/", $biz_tel)) {
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('電話番号が正しくないため、処理を中断しました。');</script>\n");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		$biz_tel1 = substr($biz_tel, 0, 4);
		$biz_tel2 = substr($biz_tel, 4, 4);
		$biz_tel3 = substr($biz_tel, 8);
	}
} else {
	$biz_tel1 = "";
	$biz_tel2 = "";
	$biz_tel3 = "";
}

// トランザクションの開始
pg_query($con, "begin");

// 入院済みチェック

//// 入院予定登録の場合
if ($mode == "reserve") {

	// 入院予定済みかどうかチェック
	$sql = "select count(*) from inptres";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院予約済み患者のため、処理を中断しました。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// 入院予定済みかどうかチェック
$sql = "select count(*) from inptmst";
$cond = "where ptif_id = '$ptif_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('入院登録済み患者のため、処理を中断しました。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 患者が登録済みかどうかチェック
$sql = "select count(*) from ptifmst";
$cond = "where ptif_id = '$ptif_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 未登録の場合は登録
if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
	$sql = "insert into ptifmst (ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_birth, ptif_sex, ptif_keywd, ptif_in_out, ptif_zip1, ptif_zip2, ptif_prv, ptif_addr1, ptif_tel1, ptif_tel2, ptif_tel3, ptif_bus_tel1, ptif_bus_tel2, ptif_bus_tel3) values (";
	$content = array($ptif_id, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $birth, $sex, $keyword, "1", $zip1, $zip2, $prv, $address, $tel1, $tel2, $tel3, $biz_tel1, $biz_tel2, $biz_tel3);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

// 登録済みの場合は更新
} else {
	$sql = "update ptifmst set";
	$set = array("ptif_lt_kaj_nm", "ptif_ft_kaj_nm", "ptif_lt_kana_nm", "ptif_ft_kana_nm", "ptif_birth", "ptif_sex", "ptif_keywd", "ptif_in_out", "ptif_zip1", "ptif_zip2", "ptif_prv", "ptif_addr1", "ptif_tel1", "ptif_tel2", "ptif_tel3", "ptif_bus_tel1", "ptif_bus_tel2", "ptif_bus_tel3");
	$setvalue = array($lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $birth, $sex, $keyword, "1", $zip1, $zip2, $prv, $address, $tel1, $tel2, $tel3, $biz_tel1, $biz_tel2, $biz_tel3);
	$cond = "where ptif_id = '$ptif_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
//pg_query($con, "rollback");
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面遷移
if ($mode == "reserve") {
	echo("<script type=\"text/javascript\">parent.reserveIn('$ptif_id');</script>");
} else {
	echo("<script type=\"text/javascript\">parent.registerIn('$ptif_id');</script>");
}

class CoMedix_Prefecture
{
	function getList() {
		$list = array();
		$list["0"] = "北海道";
		$list["1"] = "青森県";
		$list["2"] = "岩手県";
		$list["3"] = "宮城県";
		$list["4"] = "秋田県";
		$list["5"] = "山形県";
		$list["6"] = "福島県";
		$list["7"] = "茨城県";
		$list["8"] = "栃木県";
		$list["9"] = "群馬県";
		$list["10"] = "埼玉県";
		$list["11"] = "千葉県";
		$list["12"] = "東京都";
		$list["13"] = "神奈川県";
		$list["14"] = "新潟県";
		$list["15"] = "富山県";
		$list["16"] = "石川県";
		$list["17"] = "福井県";
		$list["18"] = "山梨県";
		$list["19"] = "長野県";
		$list["20"] = "岐阜県";
		$list["21"] = "静岡県";
		$list["22"] = "愛知県";
		$list["23"] = "三重県";
		$list["24"] = "滋賀県";
		$list["25"] = "京都府";
		$list["26"] = "大阪府";
		$list["27"] = "兵庫県";
		$list["28"] = "奈良県";
		$list["29"] = "和歌山県";
		$list["30"] = "鳥取県";
		$list["31"] = "島根県";
		$list["32"] = "岡山県";
		$list["33"] = "広島県";
		$list["34"] = "山口県";
		$list["35"] = "徳島県";
		$list["36"] = "香川県";
		$list["37"] = "愛媛県";
		$list["38"] = "高知県";
		$list["39"] = "福岡県";
		$list["40"] = "佐賀県";
		$list["41"] = "長崎県";
		$list["42"] = "熊本県";
		$list["43"] = "大分県";
		$list["44"] = "宮崎県";
		$list["45"] = "鹿児島県";
		$list["46"] = "沖縄県";
		return $list;
	}
}
?>
