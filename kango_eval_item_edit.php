<?
require_once("about_comedix.php");
require_once("kango_common.ini");


//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	//==============================
	//送信パラメータ解析
	//==============================
	
	$cate_info = null;
	for($eval_cate_no = 1;$eval_cate_no <= 5;$eval_cate_no++ )
	{
		$p_eval_point1 = "cate{$eval_cate_no}_eval_point1";
		$p_eval_point2 = "cate{$eval_cate_no}_eval_point2";
		$p_eval_point3 = "cate{$eval_cate_no}_eval_point3";
		$p_use_flg = "cate{$eval_cate_no}_use_flg";
		$p_eval_cate_name = "cate{$eval_cate_no}_eval_cate_name";
		
		$cate_info[$eval_cate_no]['eval_point1'] = $$p_eval_point1;
		$cate_info[$eval_cate_no]['eval_point2'] = $$p_eval_point2;
		$cate_info[$eval_cate_no]['eval_point3'] = $$p_eval_point3;
		$cate_info[$eval_cate_no]['use_flg'] = $$p_use_flg;
		$cate_info[$eval_cate_no]['eval_cate_name'] = $$p_eval_cate_name;
	}
	//==============================
	//入力チェック
	//==============================
	
	$is_ok = true;
	$error_message = "";
	if($eval_item_name == "")
	{
		$is_ok = false;
		$error_message = "項目名を入力してください。";
	}
	
	
	if(mb_strlen($eval_item_name)>150)
	{
		$is_ok = false;
		$error_message = "項目名が長すぎます。";
	}
	
		
	//==============================
	//エラーメッセージ表示
	//==============================
	if(!$is_ok)
	{
		?>
		<script type="text/javascript">
		alert("<?=$error_message?>");
		history.back();
		</script>
		<?
		exit;
	}
	
	//==============================
	//トランザクション開始
	//==============================
	pg_query($con,"begin transaction");

	//==============================
	//DB更新
	//==============================
	
	//追加の場合
	if($mode == "add")
	{
		//==============================
		//評価項目IDの採番
		//==============================
		$sql = "select max(eval_item_id)+1 as new_eval_item_id from nlcs_eval_item where eval_form_id = {$eval_form_id}";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$eval_item_id = pg_fetch_result($sel,0,'new_eval_item_id');
		
		//==============================
		//評価項目情報の登録
		//==============================
		$disp_index = $caller_disp_index;  //呼び出し元の表示順と同じにする。
		$default_flg = "f";
		$delete_flg = "f";
		$eval_item_name_sql = pg_escape_string($eval_item_name);
		
		$sql  = " insert into nlcs_eval_item";
		$sql .= " (eval_form_id, eval_item_id, eval_item_name, disp_index, default_flg, delete_flg)";
		$sql .= " values({$eval_form_id}, {$eval_item_id}, '{$eval_item_name_sql}', {$disp_index}, '{$default_flg}', '{$delete_flg}')";
		$result = pg_exec_with_log($con, $sql, $fname);
		if ($result == 0)
		{
			pg_query($con,"rollback");
			pg_close($db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//==============================
		//評価カテゴリ情報の登録
		//==============================
		for($eval_cate_no = 1;$eval_cate_no <= 5;$eval_cate_no++ )
		{
			$eval_point1 = $cate_info[$eval_cate_no]['eval_point1'];
			$eval_point2 = $cate_info[$eval_cate_no]['eval_point2'];
			$eval_point3 = $cate_info[$eval_cate_no]['eval_point3'];
			$use_flg = $cate_info[$eval_cate_no]['use_flg'];
			$eval_cate_name = $cate_info[$eval_cate_no]['eval_cate_name'];
			$eval_cate_name_sql = pg_escape_string($eval_cate_name);
			
			$use_flg = ($use_flg == "t") ? "t" : "f";
			
			//使用しない＋評価項目名無しの場合はレコードを作成しない。
			if($use_flg == "f" && $eval_cate_name == "")
			{
				continue;
			}
			
			$sql  = " insert into nlcs_eval_cate";
			$sql .= " (eval_form_id, eval_item_id, eval_cate_no, eval_cate_name, eval_point1, eval_point2, eval_point3, use_flg)";
			$sql .= " values({$eval_form_id}, {$eval_item_id}, {$eval_cate_no}, '{$eval_cate_name_sql}', {$eval_point1}, {$eval_point2}, {$eval_point3}, '{$use_flg}')";
			$result = pg_exec_with_log($con, $sql, $fname);
			if ($result == 0)
			{
				pg_query($con,"rollback");
				pg_close($db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
	//更新の場合
	else
	{
		//==============================
		//評価項目情報の更新
		//==============================
		
		//評価項目名のみ更新
		$eval_item_name_sql = pg_escape_string($eval_item_name);


		$sql  = " update nlcs_eval_item set eval_item_name = '{$eval_item_name_sql}' where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id}";
		$result = pg_exec_with_log($con, $sql, $fname);
		if ($result == 0)
		{
			pg_query($con,"rollback");
			pg_close($db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//==============================
		//評価カテゴリ更新前の情報を取得
		//==============================
		$sql = "select * from nlcs_eval_cate where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id}";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$cate_list_before_update = pg_fetch_all($sel);
		$cate_info_before_update = null;
		foreach($cate_list_before_update as $tmp_cate)
		{
			$tmp_eval_cate_no = $tmp_cate['eval_cate_no'];
			$cate_info_before_update[$tmp_eval_cate_no]['eval_point1'] = $tmp_cate['eval_point1'];
			$cate_info_before_update[$tmp_eval_cate_no]['eval_point2'] = $tmp_cate['eval_point2'];
			$cate_info_before_update[$tmp_eval_cate_no]['eval_point3'] = $tmp_cate['eval_point3'];
			$cate_info_before_update[$tmp_eval_cate_no]['use_flg'] = $tmp_cate['use_flg'];
			$cate_info_before_update[$tmp_eval_cate_no]['eval_cate_name'] = $tmp_cate['eval_cate_name'];
		}



		//==============================
		//評価カテゴリ情報の更新
		//==============================

		//評価カテゴリ情報の削除
		$sql  = " delete from nlcs_eval_cate where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id}";
		$result = pg_exec_with_log($con, $sql, $fname);
		if ($result == 0)
		{
			pg_query($con,"rollback");
			pg_close($db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		//評価カテゴリ情報の登録 ※登録の時と同じロジック。
		for($eval_cate_no = 1;$eval_cate_no <= 5;$eval_cate_no++ )
		{
			$eval_point1 = $cate_info[$eval_cate_no]['eval_point1'];
			$eval_point2 = $cate_info[$eval_cate_no]['eval_point2'];
			$eval_point3 = $cate_info[$eval_cate_no]['eval_point3'];
			$use_flg = $cate_info[$eval_cate_no]['use_flg'];
			$eval_cate_name = $cate_info[$eval_cate_no]['eval_cate_name'];
			$eval_cate_name_sql = pg_escape_string($eval_cate_name);
			
			$use_flg = ($use_flg == "t") ? "t" : "f";
			
			//使用しない＋項目名無しの場合はレコードを作成しない。
			if($use_flg == "f" && $eval_cate_name == "")
			{
				continue;
			}
			
			$sql  = " insert into nlcs_eval_cate";
			$sql .= " (eval_form_id, eval_item_id, eval_cate_no, eval_cate_name, eval_point1, eval_point2, eval_point3, use_flg)";
			$sql .= " values({$eval_form_id}, {$eval_item_id}, {$eval_cate_no}, '{$eval_cate_name_sql}', {$eval_point1}, {$eval_point2}, {$eval_point3}, '{$use_flg}')";
			$result = pg_exec_with_log($con, $sql, $fname);
			if ($result == 0)
			{
				pg_query($con,"rollback");
				pg_close($db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		
		//==============================
		//評価カテゴリ変更に伴う再計算有無判定
		//==============================
		$recalc_7_1_cate_no_list = null;
		$recalc_level_cate_no_list = null;
		for($eval_cate_no = 1;$eval_cate_no <= 5;$eval_cate_no++ )
		{
			$eval_point1 = $cate_info[$eval_cate_no]['eval_point1'];
			$eval_point2 = $cate_info[$eval_cate_no]['eval_point2'];
			$eval_point3 = $cate_info[$eval_cate_no]['eval_point3'];
			$use_flg = $cate_info[$eval_cate_no]['use_flg'];
			
			$eval_point1_b = $cate_list_before_update[$eval_cate_no]['eval_point1'];
			$eval_point2_b = $cate_list_before_update[$eval_cate_no]['eval_point2'];
			$eval_point3_b = $cate_list_before_update[$eval_cate_no]['eval_point3'];
			$use_flg_b = $cate_list_before_update[$eval_cate_no]['use_flg'];
			
			$is_7_1_recalc = false;
			$is_level_recalc = false;
			if($use_flg_b == "t")//必ず$use_flg == "t"が成り立つ。
			{
				if($eval_point1 != $eval_point1_b)
				{
					$is_7_1_recalc = true;//評価票1だけ。
					$is_level_recalc = true;
				}
				if($eval_point2 != $eval_point2_b)
				{
					$is_level_recalc = true;
				}
				if($eval_point3 != $eval_point3_b)
				{
					$is_level_recalc = true;
				}
			}
			
			if($is_7_1_recalc)
			{
				$recalc_7_1_cate_no_list[] = $eval_cate_no;
			}
			if($is_level_recalc)
			{
				$recalc_level_cate_no_list[] = $eval_cate_no;
			}
		}
		
		//==============================
		//７対１入院基準該当フラグの再計算
		//==============================
		if($recalc_7_1_cate_no_list != null)
		{
			$target_eval_cate_no = join($recalc_7_1_cate_no_list,',');
			//再計算対象データの特定
			$sql  = " select pt_id,input_date,input_time from";
			$sql .= " (";
			$sql .= " select pt_id,input_date,input_time,eval_data_id from nlcs_eval_record where not delete_flg and input_state = 1";
			$sql .= " ) record";
			$sql .= " natural inner join";
			$sql .= " (";
			$sql .= " select eval_data_id from nlcs_eval_record_data where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id} and eval_cate_no in ({$target_eval_cate_no})";
			$sql .= " ) recalc_data_id";
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$update_target_record_info = pg_fetch_all($sel);

			//再計算
			foreach($update_target_record_info as $row)
			{
				set_pt_7_1_flg($con,$fname,$row['pt_id'],$row['input_date'],$row['input_time']);
			}
		}
		
		//==============================
		//患者分類レベルの再計算
		//==============================
		if($recalc_level_cate_no_list != null)
		{
			$target_eval_cate_no = join($recalc_level_cate_no_list,',');
			//再計算対象データの特定
			$sql  = " select pt_id,input_date,input_time,bldg_cd,ward_cd from";
			$sql .= " (";
			$sql .= " select pt_id,input_date,input_time,bldg_cd,ward_cd,eval_data_id from nlcs_eval_record where not delete_flg and input_state = 1";
			$sql .= " ) record";
			$sql .= " natural inner join";
			$sql .= " (";
			$sql .= " select eval_data_id from nlcs_eval_record_data where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id} and eval_cate_no in ({$target_eval_cate_no})";
			$sql .= " ) recalc_data_id";
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$update_target_record_info = pg_fetch_all($sel);

			//再計算
			foreach($update_target_record_info as $row)
			{
				$pt_class_level_setting = get_pt_class_level_setting($con, $fname, $row['bldg_cd'], $row['ward_cd']);
				set_pt_class_level($con,$fname,$row['pt_id'],$row['input_date'],$row['input_time'],$pt_class_level_setting);
			}
		}
		
	}
	
	//==============================
	//コミット
	//==============================
	pg_query($con, "commit");

	//==============================
	//子画面を更新して、画面終了
	//==============================
	?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<script language='javascript'>
	if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}
	window.close();
	</script>
	</head>
	<body>
	</body>
	</html>
	<?
	exit;

}




//==============================
//表示データ取得
//==============================
if($mode == "update")
{
	//==============================
	//項目名取得
	//==============================
	$sql = "select eval_item_name from nlcs_eval_item where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id}";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$eval_item_name = pg_fetch_result($sel,0,'eval_item_name');
	
	
	//==============================
	//カテゴリ情報取得
	//==============================
	$sql = "select * from nlcs_eval_cate where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id}";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$cate_list = pg_fetch_all_in_array($sel);
	
	$cate_info = array();
	foreach($cate_list as $tmp_cate)
	{
		$tmp_eval_cate_no = $tmp_cate['eval_cate_no'];
		$cate_info[$tmp_eval_cate_no]['eval_point1'] = $tmp_cate['eval_point1'];
		$cate_info[$tmp_eval_cate_no]['eval_point2'] = $tmp_cate['eval_point2'];
		$cate_info[$tmp_eval_cate_no]['eval_point3'] = $tmp_cate['eval_point3'];
		$cate_info[$tmp_eval_cate_no]['use_flg'] = $tmp_cate['use_flg'];
		$cate_info[$tmp_eval_cate_no]['eval_cate_name'] = $tmp_cate['eval_cate_name'];
	}
	
	//==============================
	//既に使用されているカテゴリを取得
	//==============================
	//使用済みの場合、以下が不可能。
	//・「利用する」を未選択
	//使用済みの場合、以下が特殊
	//・点数変更時に使用している評価記録を再計算。
	$sql  = " select distinct eval_cate_no from";
	$sql .= " (";
	$sql .= " select eval_data_id,eval_cate_no from nlcs_eval_record_data where eval_form_id = {$eval_form_id} and eval_item_id = {$eval_item_id}";
	$sql .= " ) form_use_item_date";
	$sql .= " natural inner join";
	$sql .= " (";
	$sql .= " select eval_data_id from nlcs_eval_record where not delete_flg";
	$sql .= " ) enable_data_id";
	$sql .= " order by eval_cate_no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$wk_data = pg_fetch_all_in_array($sel);
	$used_cate_no_list = array();
	foreach($wk_data as $wk_data1)
	{
		$used_cate_no_list[] = $wk_data1['eval_cate_no'];
	}
}
else
{
	$eval_item_name = "";
	$cate_info = array();
	$used_cate_no_list = null;
}


//==============================
//その他
//==============================

//画面名
if($mode == "add")
{
	$PAGE_TITLE = "評価項目登録";
	$button_name = "追加";
}
else
{
	$PAGE_TITLE = "評価項目変更";
	$button_name = "更新";
}

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<? write_resizing_textarea_script(); ?>
<script type="text/javascript">

function initPage()
{
	loadResizingTextArea();
}

function save()
{
	//評価項目名の必須チェック
	var eval_item_name = document.mainform.eval_item_name.value;
	if(eval_item_name == "")
	{
		alert("評価項目名を入力してください。");
		return;
	}
	
	//利用カテゴリ数のチェック
	var use_flg1 = document.mainform.cate1_use_flg.checked;
	var use_flg2 = document.mainform.cate2_use_flg.checked;
	var use_flg3 = document.mainform.cate3_use_flg.checked;
	var use_flg4 = document.mainform.cate4_use_flg.checked;
	var use_flg5 = document.mainform.cate5_use_flg.checked;
	
	var use_count = 0;
	if(use_flg1){use_count++;}
	if(use_flg2){use_count++;}
	if(use_flg3){use_count++;}
	if(use_flg4){use_count++;}
	if(use_flg5){use_count++;}
	if(use_count < 2)
	{
		alert("利用する評価カテゴリを2つ以上指定してください。");
		return;
	}
	
	//評価カテゴリ名の必須チェック
	var eval_cate_name1 = document.mainform.cate1_eval_cate_name.value;
	var eval_cate_name2 = document.mainform.cate2_eval_cate_name.value;
	var eval_cate_name3 = document.mainform.cate3_eval_cate_name.value;
	var eval_cate_name4 = document.mainform.cate4_eval_cate_name.value;
	var eval_cate_name5 = document.mainform.cate5_eval_cate_name.value;
	if(
		   (use_flg1 && eval_cate_name1 == "")
		|| (use_flg2 && eval_cate_name2 == "")
		|| (use_flg3 && eval_cate_name3 == "")
		|| (use_flg4 && eval_cate_name4 == "")
		|| (use_flg5 && eval_cate_name5 == "")
	  )
	{
		alert("評価カテゴリ名を入力してください。");
		return;
	}
	
	document.mainform.submit();
}

function use_flg_changed(obj,eval_cate_no)
{
	if(!obj.checked)
	{
		<?
		foreach($used_cate_no_list as $used_cate_no)
		{
			?>
			if(eval_cate_no == <?=$used_cate_no?>)
			{
				alert("このカテゴリは既に記録で選択されているため利用不可能にできません。\n項目変更に対して新たに適用開始日を割り当てるか、このカテゴリを選択している記録を全て修正してから実行してください。");
				obj.checked = true;
			}
			<?
		}
		?>
	}
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
show_kango_header_for_sub_window($PAGE_TITLE);
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>


<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="mode" value="<?=$mode?>">
<input type="hidden" name="eval_form_id" value="<?=$eval_form_id?>">
<input type="hidden" name="caller_disp_index" value="<?=$caller_disp_index?>">
<input type="hidden" name="eval_item_id" value="<?=$eval_item_id?>">




<!-- 項目名・実効ボタン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width='600' border='0' cellspacing='0' cellpadding='1' class="list">
<tr>
<td bgcolor='#bdd1e7' align='right' width="100"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価項目名</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><textarea name="eval_item_name" cols="50" rows="1" class="ResizingTextArea"><?=h($eval_item_name)?></textarea></font></td>
</tr>
</table>

</td>
<td width="100" align="right" valign="top">
<input type="button" value="<?=$button_name?>" onclick="save()">
</td>
</tr>
</table>
<!-- 項目名・実効ボタン END -->


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="5" alt=""></td>
</tr>
</table>


<!-- カテゴリ一覧 START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
<tr>
<td bgcolor='#bdd1e7' align='center' width="60"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>利用する</font></td>
<td bgcolor='#bdd1e7' align='center' width="80"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票1得点</font></td>
<td bgcolor='#bdd1e7' align='center' width="80"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票2得点</font></td>
<td bgcolor='#bdd1e7' align='center' width="80"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票3得点</font></td>
<td bgcolor='#bdd1e7' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価カテゴリ</font></td>
</tr>


<?
for($eval_cate_no = 1;$eval_cate_no <= 5;$eval_cate_no++ )
{
	if (isset($cate_info[$eval_cate_no])) {
		$eval_point1 = $cate_info[$eval_cate_no]['eval_point1'];
		$eval_point2 = $cate_info[$eval_cate_no]['eval_point2'];
		$eval_point3 = $cate_info[$eval_cate_no]['eval_point3'];
		$use_flg = $cate_info[$eval_cate_no]['use_flg'];
		$eval_cate_name = $cate_info[$eval_cate_no]['eval_cate_name'];
	}
	else {
		$eval_point1 = null;
		$eval_point2 = null;
		$eval_point3 = null;
		$use_flg = null;
		$eval_cate_name = null;
	}
	?>
	<tr>
	<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><input type="checkbox" name="cate<?=$eval_cate_no?>_use_flg" value="t" onclick="use_flg_changed(this,<?=$eval_cate_no?>)" <?if($use_flg == "t"){echo("checked");}?>></font></td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<select name="cate<?=$eval_cate_no?>_eval_point1">
		<?
		for($p = 0; $p <=9; $p++)
		{
			?>
			<option value="<?=$p?>" <? if($eval_point1 == $p){echo("selected");} ?>><?=$p?></option>
			<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<select name="cate<?=$eval_cate_no?>_eval_point2">
		<?
		for($p = 0; $p <=9; $p++)
		{
			?>
			<option value="<?=$p?>" <? if($eval_point2 == $p){echo("selected");} ?>><?=$p?></option>
			<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<select name="cate<?=$eval_cate_no?>_eval_point3">
		<?
		for($p = 0; $p <=9; $p++)
		{
			?>
			<option value="<?=$p?>" <? if($eval_point3 == $p){echo("selected");} ?>><?=$p?></option>
			<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><input type="text" name="cate<?=$eval_cate_no?>_eval_cate_name" value="<?=h($eval_cate_name)?>" maxlength="200" style="width:100%"></font></td>
	</tr>
	<?
}
?>


</table>
<!-- カテゴリ一覧 END -->



</form>
<!-- 全体 END -->



</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
?>
