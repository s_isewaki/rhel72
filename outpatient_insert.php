<?
$enti = 1;
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="outpatient_register.php">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="ty" value="<? echo($ty); ?>">
<input type="hidden" name="tm" value="<? echo($tm); ?>">
<input type="hidden" name="td" value="<? echo($td); ?>">
<input type="hidden" name="hr" value="<? echo($hr); ?>">
<input type="hidden" name="mi" value="<? echo($mi); ?>">
<input type="hidden" name="sect" value="<? echo($sect); ?>">
<input type="hidden" name="doc" value="<? echo($doc); ?>">
<input type="hidden" name="enti" value="<? echo($enti); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
<?

//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("get_values.ini");
require("./conf/sql.inf");
require("label_by_profile_type.ini");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$outreg = check_authority($session, 35, $fname);
if( $outreg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限の取得（画面遷移制御用）
$inout = check_authority($session, 33, $fname);

$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

//form
//正規表現チェック

if($pt_id ==""){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"不正なアクセスです\");</script>\n");
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

if (!checkdate($tm, $td, $ty)) {
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"{$_label_by_profile["INOUT_DATETIME"][$profile_type]}が不正です\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if($sect == "0"){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"{$_label_by_profile["SECTION"][$profile_type]}を選択してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if($doc == "0"){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"{$_label_by_profile["DOCTOR"][$profile_type]}を選択してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if($enti == "0"){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"事業所を選択してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//----------Transaction begin----------

pg_exec($con,"begin transaction");

// 登録値の編集
$out_dt = $ty . $tm . $td;  // 来院日付
$out_tm = $hr . $mi;  // 来院時刻
$reg_dt = date("Ymd");  // 登録日付
$reg_tm = date("Hi");  // 登録時刻
$reg_emp_id = get_emp_id($con, $session, $fname);  // 登録職員ID

//----------来院履歴テーブルへの挿入----------
$sql = "insert into outhist values(";
$content = array($pt_id, $out_dt, $out_tm, $enti, $sect, null, $doc, $reg_dt, $reg_tm, $reg_emp_id);
$in = insert_into_table($con, $sql, $content, $fname);
if ($in == 0) {
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//----------来院履歴テーブルより最終来院日を取得----------
$sql = "select max(outpt_out_dt) from outhist";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$last = pg_fetch_result($sel, 0, 0);

//----------患者マスタの更新----------
$sql = "update ptifmst set";
$set = array("ptif_last", "ptif_up_dt", "ptif_up_tm");
$setvalue = array($last, $reg_dt, $reg_tm);
$cond = "where ptif_id = '$pt_id'";
$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($up == 0) {
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

pg_exec($con, "commit");
pg_close($con);

/*
$key1 = urlencode($key1);
$key2 = urlencode($key2);
if ($inout == 1) {
	echo("<script language='javascript'>location.href = 'inoutpatient_detail.php?session=$session&pt_id=$pt_id&key1=$key1&key2=$key2&page=$page';</script>");
} else {
	echo("<script language='javascript'>location.href = 'outpatient_register.php?session=$session&pt_id=$pt_id&key1=$key1&key2=$key2&page=$page';</script>");
}
*/

echo("<script language='javascript'>opener.location.reload();self.close();</script>");
?>
</body>
