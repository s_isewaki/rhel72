<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 勤務分担表</title>

<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

require_once("show_attendance_pattern.ini");
require_once("atdbk_menu_common.ini");

require_once("duty_shift_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 勤務管理権限の取得
///-----------------------------------------------------------------------------
$work_admin_auth = check_authority($session, 42, $fname);
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------

	$err_flg_1 = "";

	// 期間
	if ($start_yr == "") {
		$start_yr = date('Y');
	}
	if ($start_mon == "") {
		$start_mon = date('m');
	}
	if ($start_day == "") {
		$start_day = date('d');
	}

	//duty_shift_diary_common.iniから一覧の見出しと幅情報取得
	list($arr_title, $arr_width) = get_title();

	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	///-----------------------------------------------------------------------------
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
//	$data_emp = $obj->get_empmst_array("");
	$data_emp = array();
	$data_wktmgrp = $obj->get_wktmgrp_array();
	///-----------------------------------------------------------------------------
	// 勤務シフトグループ情報を取得
	///-----------------------------------------------------------------------------
	$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
	// 更新権限あり、職員設定されているグループ取得
	$group_array = $obj->get_valid_group_array($group_array, $emp_id, "", "");
	if (count($group_array) <= 0) {
		$err_flg_1 = "1";
	} else {

		///-----------------------------------------------------------------------------
		//勤務シフトグループ情報ＤＢより情報取得
		///-----------------------------------------------------------------------------
		if ($group_id == ""){
			$group_id = $group_array[0]["group_id"];
		}
	}

	if ($act == "表示") {
		$start_date = $start_yr.$start_mon.$start_day;
		$data_atdptn_all = $obj->get_atdptn_array("");
		$data_pattern_all = $obj->get_duty_shift_pattern_array("", $data_atdptn_all);

		$reason_2_array = $obj->get_reason_2("1");

		//下書き有無確認用期間
		$arr_date = $obj->get_term_from_group_date($group_id, $group_array, $start_date);
//print_r($arr_date);
		$proc_flg = 1; //処理フラグ 1:下書き有無確認する
		//以下の関数で下書きの有無を確認
		$data = get_allotment_data($con, $fname, $group_id, $group_array, $start_date, $data_pattern_all, $reason_2_array, $data_atdptn_all, $proc_flg, $arr_date);
	}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	//表示
	///-----------------------------------------------------------------------------
	function dataDisp() {
		document.mainform.action = "atdbk_duty_shift_allotment.php";
		document.mainform.target = "";
	}
	///-----------------------------------------------------------------------------
	//印刷
	///-----------------------------------------------------------------------------
	function dataPrint() {
		//印刷ＷＩＮＤＯＷをＯＰＥＮ
		dx = screen.width;
		dy = screen.top;
		base_left = 0;
		base = 0;
		wx = 670;
		wy = 700;
		var url = 'duty_shift_diary_allotment_print.php';
		url += '?session=<?=$session?>';
		url += '&group_id=<?=$group_id?>';
		url += '&start_yr=<?=$start_yr?>';
		url += '&start_mon=<?=$start_mon?>';
		url += '&start_day=<?=$start_day?>';
		window.open(url, 'printFormChild', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	}


	///-----------------------------------------------------------------------------
	//Excel出力
	///-----------------------------------------------------------------------------
	function makeExcel() {
		document.mainform.action = "duty_shift_diary_allotment_excel.php";
		document.mainform.target = "download";
		document.mainform.submit();
	}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
		<!-- 画面遷移 -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr bgcolor="#f6f9ff">
		<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
		<? if ($work_admin_auth == "1") { ?>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
		<? } ?>
		</tr></table>
		<!-- タブ -->
		<? show_atdbk_menuitem($session, $fname, ""); ?>
		<!-- 下線 -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr></table><img src="img/spacer.gif" alt="" width="1" height="2"><br>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
	<?
	if ($err_flg_1 == "1") {
		echo("登録されているシフトグループがありません。");
	} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post" action="atdbk_duty_shift_allotment.php">
		<table width="680" border="0" cellspacing="0" cellpadding="2" class="">
		<tr>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="atdbk_duty_shift_list.php?session=<?=$session?>">勤務予定表</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="atdbk_duty_shift_list2.php?session=<?=$session?>">勤務報告書</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務分担表</b></font></td>
		<td align="left" width=""></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務シフトグループ名 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="680" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
		<!-- 勤務シフトグループ名 -->
		<td width="250" align="left" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ</font>
		<select name="group_id">
		<?
		for($i=0;$i<count($group_array);$i++) {
			$wk_id= $group_array[$i]["group_id"];
			$wk_name = $group_array[$i]["group_name"];
			echo("<option value=\"$wk_id\"");
			if ($group_id == $wk_id) {
				echo(" selected");
			}
			echo(">$wk_name\n");
		}
		?>
		</select>&nbsp;</td>
		<td width="280" align="left" nowrap>
		<?
		// 期間
		?>
		<select id='start_yr' name='start_yr'><?
		if (date("m") == 12) {
			$next_yr = date("Y") + 1;
			echo("<option value=\"$next_yr\">$next_yr</option>");
		}
		show_select_years(15, $start_yr, false);
		?></select>年
		<select id='start_mon' name='start_mon'><?
		show_select_months($start_mon, false);
		?></select>月
		<select id='start_day' name='start_day'><?
		show_select_days($start_day, false);
		?></select>日
		<!-- 表示ボタン -->
		<input type="submit" name="act" value="表示" onclick="dataDisp();"></td>
		<td align="right" width="">
		<? if (count($data) > 0) { ?>
		<input type="button" value="Excel出力" onclick="makeExcel();">
		<input type="button" value="印刷" onclick="dataPrint();">
		<? } ?>
		</td>
		</tr>
		</table>
<?
// 年月日
//
// 表示、検索後
if ($act == "表示") {
?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="680" border="0" cellspacing="0" cellpadding="2" class="list">
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（見出し） -->
			<!-- ------------------------------------------------------------------------ -->
			<tr height="22" bgcolor="#f6f9ff">
			<?
			for ($i=0; $i<count($arr_title); $i++) {
				echo("<td align=\"left\" width=\"{$arr_width[$i]}\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j10\"> \n");
				echo($arr_title[$i]);
				echo("</font></td> \n");
			}
			?>

				</font></td>
			</tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（データ） -->
			<!-- ------------------------------------------------------------------------ -->
			<?
			for ($i=0; $i<count($data); $i++) {
				echo("<tr height=\"22\"> \n");
				for ($j=0; $j<count($arr_title); $j++) {
					//氏名以外は小さいフォント
					if ($j == 3) {
						$fontclass = "j12";
					} else {
						$fontclass = "j10";
					}
					echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$fontclass\"> \n");
					echo($data[$i][$j]);
					echo("</font></td> \n");
				}
				echo("</tr>\n");
			}
			?>
		</table>
<? } ?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">

	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>

	<? } ?>

</body>
<? pg_close($con); ?>
</html>
