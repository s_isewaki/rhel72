<?php
// 勤務シフト作成｜管理 |管理帳票設定更新
// duty_shift_mprint_setting_update.php

require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_mprint_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_mprint = new duty_shift_mprint_common_class($con, $fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
// 管理者情報を全削除
///-----------------------------------------------------------------------------
$sql = "delete from duty_shift_mprint_staff";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// スタッフ情報を登録
///-----------------------------------------------------------------------------
if ($emp_id_array != "") {
	//職員データ分を登録
	$emp_id_array = split(",", $emp_id_array);
	$no = 1;
	foreach ($emp_id_array as $emp_id) {
		///-----------------------------------------------------------------------------
		// スタッフ情報を登録
		///-----------------------------------------------------------------------------
		$sql = "insert into duty_shift_mprint_staff (emp_id, no) values (";
		$content = array($emp_id, $no);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$no++;
	}
}
//帳票一覧
//既存データの取得
$arr_old_id = array();
$arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst();
foreach ($arr_layout_mst as $i => $layout_mst) {
    $arr_old_id[] = $layout_mst["layout_id"];
}
//登録、更新
for ($i=0; $i<count($layout_id); $i++) {
    //テーブルにないため登録 -1
    if (!in_array($layout_id[$i], $arr_old_id)) {
        $sql = "insert into duty_shift_mprint_layout_mst (layout_name, no) values (";
        $content = array($layout_name[$i], $i+1);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
    //テーブルにあるため更新
    else if (in_array($layout_id[$i], $arr_old_id)) {
        $sql = "update duty_shift_mprint_layout_mst set";
        $set = array("layout_name", "no");
        $setvalue = array($layout_name[$i], $i+1);
        $cond = "where layout_id = $layout_id[$i] ";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        
    }

}

//画面上で削除されたデータ
for ($i=0; $i<count($arr_old_id); $i++) {
    if (!in_array($arr_old_id[$i], $layout_id)) {
        //関連テーブルから削除
        $arr_table = array("duty_shift_mprint_layout_mst"
                ,"duty_shift_mprint_config"
                ,"duty_shift_mprint_hol_except_ptn"
                ,"duty_shift_mprint_holwkcnt_job_id"
                ,"duty_shift_mprint_layout_item"
                ,"duty_shift_mprint_total_item"
                ,"duty_shift_mprint_total_item2"
                ,"duty_shift_mprint_total_setting"
                ,"duty_shift_mprint_total_tra"
                );
        
        foreach ($arr_table as $tablename) {
            $sql = "delete from $tablename";
            $cond = "where layout_id = $arr_old_id[$i]";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}


///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query($con, "commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
$back_show = "<script type=\"text/javascript\">location.href = 'duty_shift_mprint_setting.php?session=$session'</script>";
echo($back_show);


