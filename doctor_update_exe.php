<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="doctor_update.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="enti_id" value="<? echo($enti_id); ?>">
<input type="hidden" name="st_id" value="<? echo($st_id); ?>">
<input type="hidden" name="dr_id" value="<? echo($dr_id); ?>">
<input type="hidden" name="dr_nm" value="<? echo($dr_nm); ?>">
<input type="hidden" name="dr_emp_personal_id" value="<? echo($dr_emp_personal_id); ?>">
<input type="hidden" name="dr_emp_kensakekka_nm" value="<? echo($dr_emp_kensakekka_nm); ?>">
<input type="hidden" name="dr_emp_id" value="<? echo($dr_emp_id); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 28, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 担当医名/担当者１
$doctor_name_title = $_label_by_profile["DOCTOR_NAME"][$profile_type];

// 担当医/担当者１
$doctor_title = $_label_by_profile["DOCTOR"][$profile_type];

// 担当医名未入力チェック
if ($dr_nm == "") {
	echo("<script type=\"text/javascript\">alert('{$doctor_name_title}を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($dr_emp_id == "") {
	$dr_emp_id = null;
}

// トランザクションを開始
pg_query($con, "begin transaction");

// 関連チェック
$sql = "select count(*) from drmst";
$cond = "where (not (enti_id = $enti_id and sect_id = $st_id and dr_id = $dr_id)) and (dr_emp_id = '$dr_emp_id') and dr_del_flg = 'f' and exists (select * from sectmst where sectmst.enti_id = drmst.enti_id and sectmst.sect_id = drmst.sect_id and sectmst.sect_del_flg = 'f')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, 0) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('指定された職員は{$doctor_title}登録済みです。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 担当医情報を更新
$sql = "update drmst set";
$set = array("dr_nm", "dr_emp_id", "dr_nm_kensa");
$setvalue = array($dr_nm, $dr_emp_id, trim($dr_emp_kensakekka_nm));
$cond = "where enti_id = $enti_id and sect_id = $st_id and dr_id = $dr_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 担当医一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'doctor_list.php?session=$session&st_id=$st_id';</script>\n");
?>
</body>
