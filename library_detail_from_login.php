<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 文書管理｜文書詳細</title>
<?php
require("about_postgres.php");
require("get_values.ini");
require("library_common.php");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// 文書情報を取得
$sql = "select libinfo.*, libcate.*, classmst.class_nm from (((libinfo inner join libcate on libinfo.lib_cate_id = libcate.lib_cate_id) inner join empmst on libinfo.emp_id = empmst.emp_id) inner join classmst on empmst.emp_class = classmst.class_id)";
$cond = "where libinfo.lib_id = '$lib_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$cate_nm = pg_fetch_result($sel, 0, "lib_cate_nm");
$folder_id = pg_fetch_result($sel, 0, "folder_id");
$lib_nm = pg_fetch_result($sel, 0, "lib_nm");
$document_type = pg_fetch_result($sel, 0, "lib_type");
$keyword = pg_fetch_result($sel, 0, "lib_keyword");
$lib_no = pg_fetch_result($sel, 0, "lib_no");
$summary = pg_fetch_result($sel, 0, "lib_summary");
$class_nm = pg_fetch_result($sel, 0, "class_nm");
$show_login_begin = pg_fetch_result($sel, 0, "show_login_begin");
$show_login_end = pg_fetch_result($sel, 0, "show_login_end");
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$upd_time = pg_fetch_result($sel, 0, "lib_up_date");
$apply_id = pg_fetch_result($sel, 0, "apply_id"); 		// 申請ID
$lib_archive = pg_fetch_result($sel, 0, "lib_archive");

// フォルダパスを取得
$folder_path = lib_get_folder_path($con, $folder_id, $fname, $lib_archive);

// 表示内容の編集
switch ($document_type) {
case "1":
	$type_name = "Word";
	break;
case "2":
	$type_name = "Excel";
	break;
case "3":
	$type_name = "PowerPoint";
	break;
case "4":
	$type_name = "PDF";
	break;
case "5":
	$type_name = "テキスト";
	break;
case "6":
	$type_name = "JPEG";
	break;
case "7":
	$type_name = "GIF";
	break;
case "99":
	$type_name = "その他";
	break;
default:
	$type_name = "";
	break;
}
$show_login_begin = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $show_login_begin);
$show_login_end = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $show_login_end);
if ($show_login_begin != "" || $show_login_end != "") {
	$show_login_span = join(" 〜 ", array($show_login_begin, $show_login_end));
} else {
	$show_login_span = "";
}
$emp_name = get_emp_kanji_name($con, $emp_id, $fname);
if (strlen($upd_time) == 14) {
	$upd_time = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $upd_time);
} else {
	$upd_time = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $upd_time);
}

// 2015.06.30 isewaki 保存先の書庫を正しく表示する。
// 使用可能な書庫一覧を取得
switch ($lib_archive) {
    case 2:
        $archive_name = "全体共有";
        break;
    case 3:
        $archive_name = "部署共有";
        break;
    case 4:
        $archive_name = "委員会・WG共有";
        break;
    default :
        // 2015.06.30 isewaki ここを通ることはないが念のため
        $archive_name = "不明な共有";
        break;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>文書詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">保存先</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo $archive_name ?> &gt; <?php echo($cate_nm); ?><?php lib_write_folder_path($folder_path, true); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">文書名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($lib_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">文書タイプ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($type_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">キーワード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($keyword); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">文書番号</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($lib_no); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">説明</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo(str_replace("\n", "<br>", $summary)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">表示期間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($show_login_span); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo("{$emp_name}（{$class_nm}）"); ?></font></td>
</tr>

<?php
	if(!empty($apply_id)){

		// 申請情報取得
		require_once("application_workflow_common_class.php");
		$obj = new application_workflow_common_class($con, $fname);

		$arr_apply_wkfwmst		= $obj->get_apply_wkfwmst($apply_id);
		$short_wkfw_name		= $arr_apply_wkfwmst[0]["short_wkfw_name"];
		$apply_no 				= $arr_apply_wkfwmst[0]["apply_no"];
		$apply_date      		= $arr_apply_wkfwmst[0]["apply_date"];
		$apply_delete_flg      	= $arr_apply_wkfwmst[0]["delete_flg"];


		if($apply_delete_flg == "f" ){

			// 申請番号作成
			$year = substr($apply_date, 0, 4);
			$md   = substr($apply_date, 4, 4);
			if($md >= "0101" and $md <= "0331")
			{
				$year = $year - 1;
			}

			$apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);


			// 申請ルート表示
			echo("<tr height='22'>\n");
			echo("<td align='right' bgcolor='#f6f9ff'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j18'>申請番号</font></td>\n");
			echo("<td ><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j18'>{$apply_no}</font>\n");
			echo("</font></td>\n");
			echo("</tr>\n\n");
		}

	}
?>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">更新日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($upd_time); ?></font></td>
</tr>
</table>
</center>
</body>
</html>
