<?
ob_start();
require_once("about_session.php");
require_once("about_postgres.php");
require_once("hiyari_report_class.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("hiyari_rm_stats_util.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//====================================
//ファイル名
//====================================
$filename = 'intelligence_gathering.xls';

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}

$filename = mb_convert_encoding(
                $filename,
                $encoding,
                mb_internal_encoding());


$arr_total_item_count = explode(",", $total_item_count);
$arr_total_show_count = explode(",", $total_show_count);
$arr_total_vertical_sum_count = explode(",", $total_vertical_sum_count);
$arr_total_vertical2_sum_count = explode(",", $total_vertical2_sum_count);
$arr_total_horizontal_sum_count = explode(",", $total_horizontal_sum_count);
$arr_total_sum_count = explode(",", $total_sum_count);

$item[0] = '(1) 薬剤';
$item[1] = '(2) 輸血';
$item[2] = '(3) 治療・処置';
$item[3] = '(4) 医療機器等';
$item[4] = '(5) ドレーン・チューブ';
$item[5] = '(6) 検査';
$item[6] = '(7) 療養上の世話';
$item[7] = '(8) その他';

$show[0] = '�〔�剤の名称や形状に関連<BR>する事例';
$show[1] = '�¬�剤に由来する事例';
$show[2] = '�０緡典ヾ鐡�に由来する事例';
$show[3] = '�ず４�のテーマ';


// ダウンロードデータ（Excel用）
$download_data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
//集計条件
$download_data .= "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
// 集計月
if($mode == 'all') { 
	$year_text  = $year.'年';
	$month_text = '1月〜12月';
} else if($mode == 'quarter') { 
	$year_text  = $year.'年';
	$next_month = $month + 2;
	$month_text = $month.'月〜'.$next_month.'月';
}
$download_data .= "<tr><td><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$year_text}{$month_text}</font></td></tr>";
$download_data .= "</table>";

$download_data .= "<table  border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";

$download_data .= '<tr bgcolor="#C8C8C8">';
$download_data .= '<td rowspan="5" align="center" valign="middle"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目</font></td>';
$download_data .= '<td colspan="4" align="center" valign="middle"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">誤った医療の実施の有無</font></td>';
$download_data .= '<td rowspan="5" align="center" valign="middle"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>';
$download_data .= '</tr>';

$download_data .= '<tr bgcolor="#C8C8C8">';
$download_data .= '<td colspan="3" align="center" valign="middle"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">実施なし</font></td>';
$download_data .= '<td rowspan="4" align="center" valign="middle"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">実施あり</font></td>';
$download_data .= '</tr>';

$download_data .= '<tr bgcolor="#C8C8C8">';
$download_data .= '<td colspan="3" align="center" valign="middle"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">影響度</font></td>';
$download_data .= '</tr>';

$download_data .= '<tr bgcolor="#C8C8C8">';
$download_data .= '<td colspan="3" align="center" valign="middle"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">当該事例の内容が仮に実施された場合、</font></td>';
$download_data .= '</tr>';

$download_data .= '<tr bgcolor="#C8C8C8">';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">死亡もしく<BR>は重篤な状<BR>況に至った<BR>と考えられ<BR>る</font></td>';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">濃厚な処<BR>置・治療が<BR>必要である<BR>と考えら<BR>れる</font></td>';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">軽微な処置・治<BR>療が必要もしく<BR>は処置・治療が<BR>不要と考えられ<BR>る</font></td>';
$download_data .= '</tr>';

foreach($item as $value) { 
	$download_data .= '<tr>';
	$download_data .= '<td bgcolor="#C8C8C8"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.$value.'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_item_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_item_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_item_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_item_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_vertical_sum_count).'</font></td>';
	$download_data .= '</tr>';
}

$download_data .= '<tr>';
$download_data .= '<td bgcolor="#C8C8C8"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_horizontal_sum_count).'</font></td>';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_horizontal_sum_count).'</font></td>';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_horizontal_sum_count).'</font></td>';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_horizontal_sum_count).'</font></td>';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_sum_count).'</font></td>';
$download_data .= '</tr>';

$download_data .= '<tr bgcolor="#C8C8C8">';
$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">再掲</font></td>';
$download_data .= '<td colspan="5"></td>';
$download_data .= '</tr>';

foreach($show as $value) { 
	$download_data .= '<tr>';
	$download_data .= '<td bgcolor="#C8C8C8"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.$value.'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_show_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_show_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_show_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_show_count).'</font></td>';
	$download_data .= '<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.array_shift($arr_total_vertical2_sum_count).'</font></td>';
	$download_data .= '</tr>';
}

$download_data .= "</table>";








//====================================
//統計分析データEXCEL出力
//====================================
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding($download_data, 'sjis', mb_internal_encoding());
ob_end_flush();



pg_close($con);

?>