#!/bin/sh
#=====================================================================
# SFC CSV Auto Output
#=====================================================================

# Data Directory 
out_dir='/tmp'

# sfc_set_dir (cmxsfc-convert.txt file wo oku directory)
sfc_set_dir='/tmp'

# sfc_group_code(if 'all' then all group) or (csv format, rei '01,02')
sfc_group_code='all'

# yyyymm year month (if '' then this month)
yyyymm=''

# CoMedix Directory
comedix_dir='/var/www/html/comedix'
#---------------------------------------------------------------------

# PHP Program CALL
php -q -c /etc/php.ini -f $comedix_dir/duty_shift_sfc_csv.php $out_dir $sfc_set_dir $sfc_group_code $yyyymm
#---------------------------------------------------------------------
