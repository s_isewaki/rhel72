<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<? if ($path == "A" || $path == "B") { ?>
<form name="items" method="post" action="bed_menu_inpatient_sub.php">
<? } else if ($path == "M") { ?>
<form name="items" method="post" action="summary_patient_sub.php">
<? } else if ($path == "S") { ?>
<form name="items" method="post" action="inpatient_sub_menu.php">
<? } else { ?>
<form name="items" method="post" action="inpatient_patient_sub.php">
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="height1" value="<? echo($height1); ?>">
<input type="hidden" name="height2" value="<? echo($height2); ?>">
<input type="hidden" name="weight1" value="<? echo($weight1); ?>">
<input type="hidden" name="weight2" value="<? echo($weight2); ?>">
<input type="hidden" name="ptsubif_move" value="<? echo($ptsubif_move); ?>">
<input type="hidden" name="ptsubif_move6" value="<? echo($ptsubif_move6); ?>">
<input type="hidden" name="ptsubif_tool_flg" value="<? echo($ptsubif_tool_flg); ?>">
<?
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_tool$i";
	echo("<input type=\"hidden\" name=\"ptsubif_tool$i\" value=\"{$$var_name}\">\n");
}
?>
<input type="hidden" name="blood_abo" value="<? echo($blood_abo); ?>">
<input type="hidden" name="blood_rh" value="<? echo($blood_rh); ?>">
<input type="hidden" name="bronchotomy" value="<? echo($bronchotomy); ?>">
<input type="hidden" name="ventilator" value="<? echo($ventilator); ?>">
<input type="hidden" name="experiment" value="<? echo($experiment); ?>">
<input type="hidden" name="dialysis_flg" value="<? echo($dialysis_flg); ?>">
<input type="hidden" name="ptsubif_algy_flg" value="<? echo($ptsubif_algy_flg); ?>">
<?
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_allergy$i";
	echo("<input type=\"hidden\" name=\"ptsubif_allergy$i\" value=\"{$$var_name}\">\n");
}
?>
<input type="hidden" name="allergy_note" value="<? echo($allergy_note); ?>">
<input type="hidden" name="meal_note" value="<? echo($meal_note); ?>">
<input type="hidden" name="ptsubif_infect_flg" value="<? echo($ptsubif_infect_flg); ?>">
<?
for ($i = 1; $i <= 10; $i++) {
	$var_name = "ptsubif_infection$i";
	echo("<input type=\"hidden\" name=\"ptsubif_infection$i\" value=\"{$$var_name}\">\n");
}
?>
<input type="hidden" name="epidemic_note" value="<? echo($epidemic_note); ?>">
<input type="hidden" name="note1" value="<? echo($note1); ?>">
<input type="hidden" name="medical_div" value="<? echo($medical_div); ?>">
<input type="hidden" name="adl_div" value="<? echo($adl_div); ?>">
<input type="hidden" name="key1" value="<? echo(@$key1); ?>">
<input type="hidden" name="key2" value="<? echo(@$key2); ?>">
<input type="hidden" name="ptwait_id" value="<? echo(@$ptwait_id); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="ward" value="<? echo(@$ward); ?>">
<input type="hidden" name="ptrm" value="<? echo(@$ptrm); ?>">
<input type="hidden" name="bed" value="<? echo(@$bed); ?>">
<input type="hidden" name="bedundec1" value="<? echo(@$bedundec1); ?>">
<input type="hidden" name="bedundec2" value="<? echo(@$bedundec2); ?>">
<input type="hidden" name="bedundec3" value="<? echo(@$bedundec3); ?>">
<input type="hidden" name="in_dt" value="<? echo(@$in_dt); ?>">
<input type="hidden" name="in_tm" value="<? echo(@$in_tm); ?>">
<input type="hidden" name="ccl_id" value="<? echo(@$ccl_id); ?>">
<input type="hidden" name="pt_nm" value="<? echo(@$pt_nm); ?>">
<input type="hidden" name="search_sect" value="<? echo(@$search_sect); ?>">
<input type="hidden" name="search_doc" value="<? echo(@$search_doc); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($height1 == "") {$height1 = "0";}
if ($height2 == "") {$height2 = "0";}
$height = "$height1.$height2";
if (!is_numeric($height)) {
	echo("<script type=\"text/javascript\">alert('身長が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($weight1 == "") {$weight1 = "0";}
if ($weight2 == "") {$weight2 = "0";}
$weight = "$weight1.$weight2";
if (!is_numeric($weight)) {
	echo("<script type=\"text/javascript\">alert('体重が無効です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 登録値の編集
$ventilator = ($ventilator != "t") ? "f" : "t";
$experiment = ($experiment != "t") ? "f" : "t";
$dialysis_flg = ($dialysis_flg != "t") ? "f" : "t";
$note_flg = ($note1 == "") ? "f" : "t";
$ptsubif_move_flg = ($ptsubif_move === "0") ? "t" : "f";
$ptsubif_move1 = ($ptsubif_move === "1") ? "t" : "f";
$ptsubif_move2 = ($ptsubif_move === "2") ? "t" : "f";
$ptsubif_move3 = ($ptsubif_move === "3") ? "t" : "f";
$ptsubif_move4 = ($ptsubif_move === "4") ? "t" : "f";
$ptsubif_move5 = ($ptsubif_move === "5") ? "t" : "f";
$ptsubif_move6 = ($ptsubif_move6 == "t") ? "t" : "f";
$bronchotomy = ($bronchotomy == "t") ? "t" : "f";
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_tool$i";
	$$var_name = ($$var_name != "t") ? "f" : "t";
}
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_allergy$i";
	$$var_name = ($$var_name != "t") ? "f" : "t";
}
for ($i = 1; $i <= 10; $i++) {
	$var_name = "ptsubif_infection$i";
	$$var_name = ($$var_name != "t") ? "f" : "t";;
}
if ($medical_div == "") {$medical_div = null;}
if ($adl_div == "") {$adl_div = null;}

// 装具が1件も選択されていない場合は「なし」が選択されたものとする
if ($ptsubif_tool1 == "f" && $ptsubif_tool2 == "f" && $ptsubif_tool3 == "f" && $ptsubif_tool4 == "f" && $ptsubif_tool5 == "f" && $ptsubif_tool6 == "f" && $ptsubif_tool7 == "f") {
	$ptsubif_tool_flg = "t";
}

// アレルギーが1件も選択されていない場合は「なし」が選択されたものとする
if ($ptsubif_allergy1 == "f" && $ptsubif_allergy2 == "f" && $ptsubif_allergy3 == "f" && $ptsubif_allergy4 == "f" && $ptsubif_allergy5 == "f" && $ptsubif_allergy6 == "f" && $ptsubif_allergy7 == "f") {
	$ptsubif_algy_flg = "t";
}

// 感染症が1件も選択されていない場合は「なし」が選択されたものとする
if ($ptsubif_infection1 == "f" && $ptsubif_infection2 == "f" && $ptsubif_infection3 == "f" && $ptsubif_infection4 == "f" && $ptsubif_infection5 == "f" && $ptsubif_infection6 == "f" && $ptsubif_infection7 == "f" && $ptsubif_infection8 == "f" && $ptsubif_infection9 == "f" && $ptsubif_infection10 == "f") {
	$ptsubif_infect_flg = "t";
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 患者詳細情報をDELETE〜INSERT
$sql = "delete from ptsubif";
$cond = "where ptif_id = '$pt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "insert into ptsubif (ptif_id, ptsubif_height, ptsubif_weight, ptsubif_blood_abo, ptsubif_blood_rh, ptsubif_ventilator, ptsubif_dialysis, ptsubif_algy_flg, ptsubif_allergy1, ptsubif_allergy2, ptsubif_allergy3, ptsubif_allergy4, ptsubif_allergy5, ptsubif_allergy6, ptsubif_allergy7, ptsubif_algy_com, ptsubif_meal_com, ptsubif_infect_flg, ptsubif_infection1, ptsubif_infection2, ptsubif_infection3, ptsubif_infection4, ptsubif_infection5, ptsubif_infection6, ptsubif_infection7, ptsubif_infection8, ptsubif_infection9, ptsubif_infection10, ptsubif_infection_com, ptsubif_move_flg, ptsubif_move1, ptsubif_move2, ptsubif_move3, ptsubif_move4, ptsubif_move5, ptsubif_move6, ptsubif_tool_flg, ptsubif_tool1, ptsubif_tool2, ptsubif_tool3, ptsubif_tool4, ptsubif_tool5, ptsubif_tool6, ptsubif_tool7, ptsubif_experiment_flg, ptsubif_note_flg, ptsubif_note, ptsubif_medical_div, ptsubif_adl_div, ptsubif_bronchotomy) values (";
$content = array($pt_id, $height, $weight, $blood_abo, $blood_rh, $ventilator, $dialysis_flg, $ptsubif_algy_flg, $ptsubif_allergy1, $ptsubif_allergy2, $ptsubif_allergy3, $ptsubif_allergy4, $ptsubif_allergy5, $ptsubif_allergy6, $ptsubif_allergy7, $allergy_note, $meal_note, $ptsubif_infect_flg, $ptsubif_infection1, $ptsubif_infection2, $ptsubif_infection3, $ptsubif_infection4, $ptsubif_infection5, $ptsubif_infection6, $ptsubif_infection7, $ptsubif_infection8, $ptsubif_infection9, $ptsubif_infection10, $epidemic_note, $ptsubif_move_flg, $ptsubif_move1, $ptsubif_move2, $ptsubif_move3, $ptsubif_move4, $ptsubif_move5, $ptsubif_move6, $ptsubif_tool_flg, $ptsubif_tool1, $ptsubif_tool2, $ptsubif_tool3, $ptsubif_tool4, $ptsubif_tool5, $ptsubif_tool6, $ptsubif_tool7, $experiment, $note_flg, $note1, $medical_div, $adl_div, $bronchotomy);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);

// 親画面をリロードする
echo "<script type=\"text/javascript\">if (window.opener) window.opener.location.reload();</script>";

if ($path == "A" || $path == "B") {
	$url_pt_nm = urlencode($pt_nm);
	echo("<script type=\"text/javascript\">location.href = 'bed_menu_inpatient_sub.php?session=$session&pt_id=$pt_id&path=$path&pt_nm=$url_pt_nm&search_sect=$search_sect&search_doc=$search_doc';</script>");
} else if ($path == "M") {
	echo("<script type=\"text/javascript\">location.href = 'summary_patient_sub.php?session=$session&pt_id=$pt_id';</script>");
} else if ($path == "S") {
	echo("<script type=\"text/javascript\">location.href = 'inpatient_sub_menu.php?session=$session&pt_id=$pt_id';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'inpatient_patient_sub.php?session=$session&pt_id=$pt_id&key1=$url_key1&key2=$url_key2&path=$path&ward=$ward&ptrm=".@$ptrm."&bed=$bed&bedundec1=$bedundec1&bedundec2=$bedundec2&bedundec3=$bedundec3&in_dt=$in_dt&in_tm=$in_tm&ccl_id=$ccl_id&ptwait_id=$ptwait_id'</script>");
}
?>
</body>
