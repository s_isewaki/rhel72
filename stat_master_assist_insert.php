<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="stat_master_assist_register.php" method="post">
<input type="hidden" name="name" value="<? echo($name); ?>">
<input type="hidden" name="item_id" value="<? echo($item_id); ?>">
<input type="hidden" name="assist_type" value="<? echo($assist_type); ?>">
<input type="hidden" name="var1_type" value="<? echo($var1_type); ?>">
<input type="hidden" name="var1_const_id" value="<? echo($var1_const_id); ?>">
<input type="hidden" name="var1_item_id" value="<? echo($var1_item_id); ?>">
<input type="hidden" name="var1_assist_id" value="<? echo($var1_assist_id); ?>">
<input type="hidden" name="operator" value="<? echo($operator); ?>">
<input type="hidden" name="var2_type" value="<? echo($var2_type); ?>">
<input type="hidden" name="var2_const_id" value="<? echo($var2_const_id); ?>">
<input type="hidden" name="var2_item_id" value="<? echo($var2_item_id); ?>">
<input type="hidden" name="var2_assist_id" value="<? echo($var2_assist_id); ?>">
<input type="hidden" name="format" value="<? echo($format); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($name == "") {
	echo("<script type=\"text/javascript\">alert('補助項目名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($name) > 80) {
	echo("<script type=\"text/javascript\">alert('補助項目名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($item_id == "") {
	echo("<script type=\"text/javascript\">alert('管理項目が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 項目属性が「自動計算」の場合
if ($assist_type == "2") {

	// 項目1が「定数」の場合
	if ($var1_type == "1") {
		if ($var1_const_id == "") {
			echo("<script type=\"text/javascript\">alert('項目1の定数が選択されていません。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}
	} else {
		$var1_const_id = null;
	}

	// 項目1が「項目指定」の場合
	if ($var1_type == "2") {
		if ($var1_item_id == "") {
			echo("<script type=\"text/javascript\">alert('項目1の管理項目が選択されていません。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}

		if ($var1_assist_id == "") {
			$var1_assist_id = null;
		}
	} else {
		$var1_item_id = null;
		$var1_assist_id = null;
	}

	// 項目2が「定数」の場合
	if ($var2_type == "1") {
		if ($var2_const_id == "") {
			echo("<script type=\"text/javascript\">alert('項目2の定数が選択されていません。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}
	} else {
		$var2_const_id = null;
	}

	// 項目2が「項目指定」の場合
	if ($var2_type == "2") {
		if ($var2_item_id == "") {
			echo("<script type=\"text/javascript\">alert('項目2の管理項目が選択されていません。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}

		if ($var2_assist_id == "") {
			$var2_assist_id = null;
		}
	} else {
		$var2_item_id = null;
		$var2_assist_id = null;
	}

} else {
	$var1_type = null;
	$var1_const_id = null;
	$var1_item_id = null;
	$var1_assist_id = null;
	$operator = null;
	$var2_type = null;
	$var2_const_id = null;
	$var2_item_id = null;
	$var2_assist_id = null;
	$format = null;
}

// データベースへ接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 補助項目IDを採番
$sql = "select max(statassist_id) from statassist";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 補助項目情報を登録
$sql = "insert into statassist (statassist_id, statassist_name, statitem_id, assist_type, calc_var1_type, calc_var1_const_id, calc_var1_item_id, calc_var1_assist_id, calc_operator, calc_var2_type, calc_var2_const_id, calc_var2_item_id, calc_var2_assist_id, calc_format) values (";
$content = array($id, $name, $item_id, $assist_type, $var1_type, $var1_const_id, $var1_item_id, $var1_assist_id, $operator, $var2_type, $var2_const_id, $var2_item_id, $var2_assist_id, $format);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 親の管理項目自体が他の計算式で使用されていたらエラーとする
$sql = "select count(*) from statassist";
$cond = "where ((calc_var1_item_id = $item_id and calc_var1_assist_id is null) or (calc_var2_item_id = $item_id and calc_var2_assist_id is null)) and del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
	$sql = "select name from statitem";
	$cond = "where statitem_id = $item_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$item_name = pg_fetch_result($sel, 0, "name");

	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('管理項目「{$item_name}」は、\\n他の計算式に使用されているため補助項目を登録できません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 管理項目一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'stat_master_item_list.php?session=$session&item_id=$item_id'</script>");
?>
</body>
