<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// スケジュール管理権限のチェック
$checkauth = check_authority($session,13,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


// 行先名：未入力チェック
if($place_name == ""){
	echo("<script language=\"javascript\">alert(\"行先名が入力されていません。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// 行先名：文字数チェック
if(strlen($place_name) > 40){
	echo("<script language=\"javascript\">alert(\"行先名が長すぎます。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// 行先情報を更新
$sql = "update scheduleplace set";
$set = array("place_name");
$setvalue = array($place_name);
$cond = "where place_id = '$place_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// データベース接続を切断
pg_close($con);

// 行先一覧画面に遷移
echo("<script language=\"javascript\">location.href = 'schedule_place_list.php?session=$session&page=$page';</script>");
?>
