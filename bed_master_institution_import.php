<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | マスタ管理 | 医療機関・施設 | CSVインポート</title>
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

if ($action == "import") {
	ini_set("max_execution_time", 0);
	list($result, $message) = import_csv($session, $fname);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	return true;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>CSVインポート</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<form name="inpt" action="bed_master_institution_import.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "" || $encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="submit" value="インポート"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="action" value="import">
</form>
<? if (!$result && $message != "") { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? echo($message); ?></font></font></td>
</tr>
</table>
<? } else if ($result) { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue">インポートしました。</font></font></td>
</tr>
</table>
<script type="text/javascript">
opener.location.href = 'bed_master_institution.php?session=<? echo($session); ?>';
</script>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。医療機関・施設名称と医療機関・施設コードは必須です。</p>
<ul style="margin:0;">
<li style="margin-left:40px;">医療機関・施設名称……ユニークである必要があります</li>
<li style="margin-left:40px;">種別……「診療所」「有床診療所」「病院」「老人保健施設」「特別養護老人ホーム」「グループホーム」「高齢者専用賃貸住宅」のいずれか</li>
<li style="margin-left:40px;">医療機関・施設コード……ユニークである必要があります。英数以外の文字は削除されます。英数字が9桁以内であればOKです</li>
<li style="margin-left:40px;">郵便番号……数字以外の文字は削除されます。数字が7桁含まれればOKです</li>
<li style="margin-left:40px;">都道府県……「北海道」〜「沖縄県」のいずれか</li>
<li style="margin-left:40px;">住所</li>
<li style="margin-left:40px;">電話番号……ハイフン区切り</li>
<li style="margin-left:40px;">FAX番号……ハイフン区切り</li>
</ul>
<dl style="margin:0;">
<dt>例：</dt>
<dd style="margin-left:20px;font-family:monospace;">医療法人　○○会　メディシステム病院,病院,CD00009,〒123-4567,東京都,千代田区神田司町2-13,03-1111-1111,03-2222-2222</dd>
<dd style="margin-left:20px;font-family:monospace;">△△医院,,123.4567,,,,,</dd>
</dl>
<dl style="margin:0;">
<dt>注：</dt>
<dd style="margin-left:20px;">「崎」「高」などの異体字はエラーの原因となります。一般的な字形をお使いください。</dd>
</dl>
</font></td>
</tr>
</table>
</center>
</body>
</html>
<?
function import_csv($session, $fname) {
	if (!array_key_exists("csvfile", $_FILES) || $_FILES["csvfile"]["error"] != 0 || $_FILES["csvfile"]["size"] == 0) {
		return array(false, "ファイルのアップロードに失敗しました。");
	}

	switch ($_POST["$encoding"]) {
	case "2":
		$file_encoding = "EUC-JP";
		break;
	case "3":
		$file_encoding = "UTF-8";
		break;
	default:
		$file_encoding = "SJIS";
		break;
	}

	// EUC-JPで扱えない文字列は「・」に変換するよう設定
	mb_substitute_character(0x30FB);

	$fp = fopen($_FILES["csvfile"]["tmp_name"], "r");
	if (!$fp) {
		return array(false, "アップロードされたファイルの読み込みに失敗しました。");
	}

	$con = connect2db($fname);
	pg_query($con, "begin");

	// ログインユーザの職員IDを取得
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");

	$line_no = 0;
	while (!feof($fp)) {
		$line_no++;
		$line = trim(mb_convert_encoding(fgets($fp), "EUC-JP", $file_encoding));

		// EOFを削除
		$line = str_replace(chr(0x1A), "", $line);

		// 空行はスキップ
		if ($line == "") {continue;}

		$columns = split(",", $line);
		if (count($columns) < 8) {
			pg_query($con, "rollback");
			pg_close($con);
			return array(false, "{$line_no}行目の列数が正しくないためインポートできませんでした。");
		}

		$mst_name					= $columns[0];
		$type						= get_type_by_string($columns[1]);
		$mst_cd						= extract_number($columns[2]);
		list($zip1, $zip2)			= format_zip($columns[3]);
		$province					= get_province_by_string($columns[4]);
		$address1					= $columns[5];
		list($tel1, $tel2, $tel3)	= format_tel($columns[6]);
		list($fax1, $fax2, $fax3)	= format_tel($columns[7]);

		list($result, $message) = validate_values($con, $mst_name, $mst_cd, $zip1, $zip2, $province, $address, $tel1, $tel2, $tel3, $fax1, $fax2, $fax3, $fname);

		if (!$result) {
			return array(false, "{$message}（{$line_no}行目）");
		}

		// 医療機関情報情報を登録
		$now = date("YmdHis");
		$sql = "insert into institemmst (mst_cd, mst_name, type, zip1, zip2, province, address1, address2, tel1, tel2, tel3, fax1, fax2, fax3, mst_content, create_time, create_emp_id, update_time, update_emp_id) values (";
		$content = array($mst_cd, $mst_name, $type, $zip1, $zip2, $province, $address1, "", $tel1, $tel2, $tel3, $fax1, $fax2, $fax3, "", $now, $emp_id, $now, $emp_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	fclose($fp);

	pg_query($con, "commit");
	pg_close($con);

	return array(true, "");
}

function get_type_by_string($string) {
	switch ($string) {
	case "診療所":
		return "1";
	case "有床診療所":
		return "2";
	case "病院":
		return "3";
	case "老人保健施設":
		return "4";
	case "特別養護老人ホーム":
		return "5";
	case "グループホーム":
		return "6";
	case "高齢者専用賃貸住宅":
		return "7";
	default:
		return "";
	}
}

function extract_number($string) {
	$buf = mb_convert_kana($string, "a");
	return preg_replace("/[^0-9a-zA-Z]/", "", $buf);
}

function format_zip($string) {
	$buf = mb_convert_kana($string, "a");
	$buf = extract_number($buf);
	if (strlen($buf) != 7) return array("", "");
	return array(substr($buf, 0, 3), substr($buf, 3, 4));
}

function get_province_by_string($string) {
	switch ($string) {
	case "北海道":
		return "0";
	case "青森県":
		return "1";
	case "岩手県":
		return "2";
	case "宮城県":
		return "3";
	case "秋田県":
		return "4";
	case "山形県":
		return "5";
	case "福島県":
		return "6";
	case "茨城県":
		return "7";
	case "栃木県":
		return "8";
	case "群馬県":
		return "9";
	case "埼玉県":
		return "10";
	case "千葉県":
		return "11";
	case "東京都":
		return "12";
	case "神奈川県":
		return "13";
	case "新潟県":
		return "14";
	case "富山県":
		return "15";
	case "石川県":
		return "16";
	case "福井県":
		return "17";
	case "山梨県":
		return "18";
	case "長野県":
		return "19";
	case "岐阜県":
		return "20";
	case "静岡県":
		return "21";
	case "愛知県":
		return "22";
	case "三重県":
		return "23";
	case "滋賀県":
		return "24";
	case "京都府":
		return "25";
	case "大阪府":
		return "26";
	case "兵庫県":
		return "27";
	case "奈良県":
		return "28";
	case "和歌山県":
		return "29";
	case "鳥取県":
		return "30";
	case "島根県":
		return "31";
	case "岡山県":
		return "32";
	case "広島県":
		return "33";
	case "山口県":
		return "34";
	case "徳島県":
		return "35";
	case "香川県":
		return "36";
	case "愛媛県":
		return "37";
	case "高知県":
		return "38";
	case "福岡県":
		return "39";
	case "佐賀県":
		return "40";
	case "長崎県":
		return "41";
	case "熊本県":
		return "42";
	case "大分県":
		return "43";
	case "宮崎県":
		return "44";
	case "鹿児島県":
		return "45";
	case "沖縄県":
		return "46";
	default:
		return "";
	}
}

function format_tel($string) {
	$buf = mb_convert_kana($string, "a");
	return split("-", $buf);
}

function validate_values($con, $mst_name, $mst_cd, $zip1, $zip2, $province, $address, $tel1, $tel2, $tel3, $fax1, $fax2, $fax3, $fname) {
	if ($mst_name == "") {
		return array(false, "医療機関・施設名称は必須です。");
	}

	if (strlen($mst_name) > 100) {
		return array(false, "医療機関・施設名称は全角50文字以内にしてください。");
	}

	$sql  = "select count(*) from institemmst";
	$cond = "where mst_name = '$mst_name'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		return array(false, "医療機関・施設名称「{$mst_name}」は既に使用されています。");
	}

	if ($mst_cd == "") {
		return array(false, "医療機関・施設コードは必須です。");
	}

	if (!preg_match("/^[a-zA-Z0-9]{1,9}$/", $mst_cd)) {
		return array(false, "医療機関・施設コードは半角英数9桁以内にしてください。");
	}

	$sql  = "select count(*) from institemmst";
	$cond = "where mst_cd = '$mst_cd'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		return array(false, "医療機関・施設コード「{$mst_cd}」は既に使用されています。");
	}

	if (strlen($zip1) > 3) {
		return array(false, "郵便番号1は半角3文字以内にしてください。");
	}

	if (strlen($zip2) > 4) {
		return array(false, "郵便番号2は半角4文字以内にしてください。");
	}

	if (strlen($address) > 100) {
		return array(false, "住所は全角50文字以内にしてください。");
	}

	if (strlen($tel1) > 6) {
		return array(false, "電話番号1は半角6文字以内にしてください。");
	}

	if (strlen($tel2) > 6) {
		return array(false, "電話番号2は半角6文字以内にしてください。");
	}

	if (strlen($tel3) > 6) {
		return array(false, "電話番号3は半角6文字以内にしてください。");
	}

	return array(true, "");
}
?>
