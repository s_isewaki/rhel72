<?
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 患者情報を検索します。
//
//インターフェース
//  リクエストデータ
//　  session   セッションID
//　  pt_id     患者ID
//  レスポンスデータ
//　  患者情報が得られた場合
//　    ajax=success
//　    patient_id={患者ID}
//　    patient_name={患者氏名}
//　    sex={性別(1:男性2:女性90:不明)}
//　    age_year={年齢(年数) ※0埋めで3桁表示。例:２９歳→029}
//　    age_month={年齢(月数) ※0埋めで2桁表示。例:９ヶ月→09}
//
//　  患者が１件もヒットしなかった場合
//　    ajax=0hit
//
//　  患者検索に失敗した場合
//　    ajax=error
//
//注意:このモジュールはリクエストデータがシングルバイトであることを想定します。
//
//////////////////////////////////////////////////////////////////////////////////////////////////////

//共通で出力されるmetaタグを消す。
ob_start();
require("about_session.php");
require("about_authority.php");
ob_end_clean();

//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	print "a";
	ajax_server_erorr();
	exit;
}

//患者情報参照権限のチェック
$pt_auth = check_authority($session, 15, $fname);
if ($pt_auth == "0") {
	print "b";
	ajax_server_erorr();
	exit;
}

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	print "c";
	ajax_server_erorr();
	exit;
}

//患者ID完全一致検索
$sql = "select ptifmst.ptif_sex, ptifmst.ptif_birth, drmst.dr_nm, inptmst.inpt_disease, ptifmst.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, inptmst.inpt_in_dt, bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name, inptmst.inpt_bed_no from ptifmst left join inptmst on inptmst.ptif_id = ptifmst.ptif_id left join bldgmst on bldgmst.bldg_cd = inptmst.bldg_cd left join wdmst on wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd left join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no left join drmst on inptmst.inpt_enti_id = drmst.enti_id and inptmst.inpt_sect_id = drmst.sect_id and inptmst.dr_id = drmst.dr_id";
$cond = "where ptifmst.ptif_del_flg = 'f' and '$pt_id' in (ptifmst.ptif_id, ltrim(ptifmst.ptif_id, '0'))";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	print "d";
	pg_close($con);
	ajax_server_erorr();
	exit;
}

//レスポンスデータ作成
if(pg_num_rows($sel) != 1)
{
	//検索にヒットしない。
	$response_data = "ajax=0hit\n";
}
else
{
	$row = pg_fetch_array($sel,0);

	//患者情報作成(※hiyari_patient_search.phpと同等)
	$tmp_patient_id = $row["ptif_id"];
	$tmp_patient_name = $row[ptif_lt_kaj_nm] . " " . $row[ptif_ft_kaj_nm];
	switch ($row["ptif_sex"]) {
		case "1":
			$tmp_sex = "1";
			$tmp_sex_string = "男性";
			break;
		case "2":
			$tmp_sex = "2";
			$tmp_sex_string = "女性";
			break;
		default:
			$tmp_sex = "90";
			$tmp_sex_string = "不明";
			break;
	}

	$tmp_in_dt = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $row["inpt_in_dt"]);
	$tmp_bed = $row["bldg_name"] . " " . $row["ward_name"] . " " . $row["ptrm_name"] . " " . $row["inpt_bed_no"];
	$tmp_disease = $row["inpt_disease"];
	$tmp_dr_nm = $row["dr_nm"];

	//$date = $_POST['date'];

	// 患者区分の計算
	if($tmp_in_dt && $date != 'false') {
		list($tmp_inpt_year, $tmp_inpt_month, $tmp_inpt_day) = split("/", $tmp_in_dt);
		list($tmp_year, $tmp_month, $tmp_day) = split("/", $date);
		//患者区分1:入院中
		$tmp_patient_class1 = "1";
		//入院31日目の日付
		$tmp_inpt_31_date_time = mktime(0, 0, 0, $tmp_inpt_month, $tmp_inpt_day+30, $tmp_inpt_year);
		$tmp_inpt_31_date = date("Y/m/d",$tmp_inpt_31_date_time);
		//患者区分2:入院日数
 		if($date <= $tmp_inpt_31_date) {
			//入院(0-31日)
			$tmp_patient_class2 = "1";
		} else {
			//入院(32日-)
			$tmp_patient_class2 = "2";
		}
	} else if($tmp_in_dt == "") {
		$tmp_patient_class1 = "3";
	}


	$tmp_birthday = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $row["ptif_birth"]);

	list($tmp_birth_year, $tmp_birth_month, $tmp_birth_day) = split("/", $tmp_birthday);
	$tmp_today_year = date("Y");
	$tmp_today_month = date("m");
	$tmp_today_day = date("d");

	$tmp_age_year = ($tmp_today_year - $tmp_birth_year);
	if ("$tmp_today_month/$tmp_today_day" < "$tmp_birth_month/$tmp_birth_day") {
		$tmp_age_year--;
	}
	$tmp_age_year = sprintf("%03d", $tmp_age_year);

	$tmp_age_month = ($tmp_today_month - $tmp_birth_month);
	if ("$tmp_today_day" < "$tmp_birth_day") {
		$tmp_age_month--;
	}
	if ($tmp_age_month < 0) {
		$tmp_age_month += 12;
	}
	$tmp_age_month = sprintf("%02d", $tmp_age_month);


	//レスポンスデータ作成
	$response_data = "ajax=success\n";

	$p_key = "patient_id";
	$p_value = $tmp_patient_id;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "patient_name";
	$p_value = $tmp_patient_name;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "sex";
	$p_value = $tmp_sex;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "age_year";
	$p_value = $tmp_age_year;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "age_month";
	$p_value = $tmp_age_month;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "in_dt";
	$p_value = $tmp_in_dt;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "disease";
	$p_value = $tmp_disease;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "class1";
	$p_value = $tmp_patient_class1;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "class2";
	$p_value = $tmp_patient_class2;
	$response_data .= "$p_key=$p_value\n";

}

//レスポンスデータを出力
print $response_data;

//DB切断
pg_close($con);





/**
 * 当モジュール内でエラーが発生した場合の共通処理
 */
function ajax_server_erorr()
{
	print "ajax=error\n";
	exit;
}


?>

