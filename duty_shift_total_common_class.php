<!-- ************************************************************************ -->
<!-- 勤務シフト作成　集計表用共通ＣＬＡＳＳ -->
<!-- ************************************************************************ -->

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");


class duty_shift_total_common_class
{
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_total_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（ＤＢ読み込み）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 集計表設定情報取得
	// @param	$group_id			グループID
	//
	// @return	$data	取得情報
	/*************************************************************************/
	function get_duty_shift_total_config($group_id) {
		$sql = "select * from duty_shift_total_config";
		$cond = "where group_id = '$group_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		$data = array();
		if ($num > 0) {
			$data = pg_fetch_array($sel);
		}
		//デフォルト設定
		//普通残業時間 1:深夜、休日残業を含まない
		if ($data["normal_overtime_flg"] == "") {
			$data["normal_overtime_flg"] = "1";
		}
		//超勤休日深夜 t:表示する
		if ($data["hol_late_night_disp_flg"] == "") {
			$data["hol_late_night_disp_flg"] = "t";
		}
		return $data;
	}

	/*************************************************************************/
	// 集計表設定情報取得
	// @param	$group_id			グループID
	// @param	$flg				表示パターン
	//
	// @return	$data	取得情報
	/*************************************************************************/
	function get_duty_shift_total_ptn($group_id, $flg) {
	
		switch ($flg) {
		case 1:
			$db_name = "duty_shift_total_ptn_full";
			break;
		case 2:
			$db_name = "duty_shift_total_ptn_part";
			break;
		case 3:
			$db_name = "duty_shift_total_tra_ptn_part";
			break;
		}
		$sql = "select * from $db_name";
		$cond = "where group_id = '$group_id' order by atdptn_ptn_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		$data = array();
		while ($row = pg_fetch_array($sel)) {
			$data[] = $row["atdptn_ptn_id"];
		}
		return $data;
	}

	/*************************************************************************/
	// 集計表設定交通費該当勤務パターン情報取得
	// @param	$group_id			グループID
	//
	// @return	$data	取得情報
	/*************************************************************************/
	function get_duty_shift_total_tra_ptn($group_id) {
	
		$sql = "select * from duty_shift_total_tra_ptn_full";
		$cond = "where group_id = '$group_id' order by atdptn_ptn_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		$data = array();
		while ($row = pg_fetch_array($sel)) {
			$ptn = $row["atdptn_ptn_id"];
			$cnt = $row["cnt"];
			$data[$ptn] = $cnt;
		}
		return $data;
	}

	/*************************************************************************/
	// 集計表設定手当情報取得
	// @param	$group_id			グループID
	// @param	$flg				表示パターン
	//
	// @return	$data	取得情報 allow_idをキー、disp_flgをデータとした配列
	/*************************************************************************/
	function get_duty_shift_total_allow($group_id, $flg) {
		
		switch ($flg) {
			case 1:
				$db_name = "duty_shift_total_allow_full";
				break;
			case 2:
				$db_name = "duty_shift_total_allow_part";
				break;
		}
		$sql = "select * from $db_name";
		$cond = "where group_id = '$group_id' order by allow_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		$data = array();
		while ($row = pg_fetch_array($sel)) {
			$data[$row["allow_id"]] = $row["disp_flg"];
		}
		return $data;
	}
	/*************************************************************************/
	// 休日出勤時間数除外勤務パターン情報取得
	// @param	$group_id			グループID
	//
	// @return	$data	取得情報
	/*************************************************************************/
	function get_duty_shift_hol_except_ptn($group_id) {
		
		$sql = "select * from duty_shift_total_hol_except_ptn";
		$cond = "where group_id = '$group_id' order by atdptn_ptn_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		$data = array();
		while ($row = pg_fetch_array($sel)) {
			$data[] = $row["atdptn_ptn_id"];
		}
		return $data;
	}
	
}

?>
