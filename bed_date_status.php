<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 入退院稼働状況（区分別）</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_calc_bed_by_type.ini");
require_once("show_select_values.ini");
require_once("get_ward_div.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 病棟区分名を取得
$ward_types = get_ward_div($con, $fname, true);

// 棟一覧を取得
$sql = "select bldg_cd, bldg_name from bldgmst";
$cond = "where bldg_del_flg = 'f' order by bldg_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$buildings = array();
while ($row = pg_fetch_array($sel)) {
	$buildings[$row["bldg_cd"]] = $row["bldg_name"];
}

// 病棟区分が指定されている場合
if ($ward_type != "") {

	// 病棟一覧に表示するリストを取得
	if ($bldg_cd == "" && count($buildings) > 0) {
		$bldg_cd = key($buildings);
	}
	$ward_ptrms = get_ward_ptrms($con, $buildings, $ward_type, $bldg_cd, $fname);
}

// デフォルト値の設定
if ($year == "") {$year = date("Y");}
if ($month == "") {$month = date("n");}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7">
<a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#5279a5"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>在院日数管理</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#15467c"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="25%" align="center"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数表示</font></a></td>
<td width="25%" align="center"><a href="bed_date_exception.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">計算除外患者指定</font></a></td>
<td width="25%" align="center"><a href="bed_date_exception_room.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">計算除外部屋指定</font></a></td>
<td width="25%" align="center"><a href="bed_date_inpatient_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">長期入院患者検索</font></a></td>
</tr>
<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院稼働状況</font></td>
<td align="center"><a href="bed_date_simulation.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シミュレーション</font></a></td>
<td align="center"><a href="bed_date_target.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">目標値設定</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<table border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<a href="bed_date_status.php?session=<? echo($session); ?>&year=<? echo($year - 1); ?>&month=<? echo($month); ?>&ward_type=<? echo($ward_type); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_ptrm=<? echo($ward_ptrm); ?>">←</a>
<img src="img/spacer.gif" width="10" height="1" alt=""><? echo($year); ?>年
<img src="img/spacer.gif" width="10" height="1" alt=""><a href="bed_date_status.php?session=<? echo($session); ?>&year=<? echo($year + 1); ?>&month=<? echo($month); ?>&ward_type=<? echo($ward_type); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_ptrm=<? echo($ward_ptrm); ?>">→</a>
</font></td>
<td width="30"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
病棟区分：<select name="ward_type" onchange="location.href = 'bed_date_status.php?session=<? echo($session); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_type=' + this.value;">
<option value="">
<option value="all"<? if ($ward_type == "all") {echo(" selected");} ?>>すべて
<?
foreach ($ward_types as $tmp_ward_type => $tmp_ward_name) {
	if ($tmp_ward_type > 5) continue;
	echo("<option value=\"$tmp_ward_type\"");
	if ($tmp_ward_type == $ward_type) {
		echo(" selected");
	}
	echo(">$tmp_ward_name");
}
?>
</select>
</font></td>
<td width="20"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
事業所（棟）：<select name="bldg_cd" onchange="location.href = 'bed_date_status.php?session=<? echo($session); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&ward_type=<? echo($ward_type); ?>&bldg_cd=' + this.value;">
<?
foreach ($buildings as $tmp_bldg_cd => $tmp_bldg_name) {
	echo("<option value=\"$tmp_bldg_cd\"");
	if ($tmp_bldg_cd == $bldg_cd) {
		echo(" selected");
	}
	echo(">$tmp_bldg_name");
}
?>
</select>
</font></td>
<td width="15"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
病棟：<select name="ward_ptrm" onchange="location.href = 'bed_date_status.php?session=<? echo($session); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&ward_type=<? echo($ward_type); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_ptrm=' + this.value;">
<? if ($ward_type == "" || $ward_type == "all") { ?>
<option value="">　　　　　</option>
<? } else { ?>
<option value="">すべて
<?
foreach ($ward_ptrms as $tmp_id => $tmp_name) {
	echo("<option value=\"$tmp_id\"");
	if ($tmp_id == $ward_ptrm) {
		echo(" selected");
	}
	echo(">$tmp_name");
}
?>
<? } ?>
</select>
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($month > 1) { ?><a href="bed_date_status.php?session=<? echo($session); ?>&year=<? echo($year); ?>&month=<? echo($month - 1); ?>&ward_type=<? echo($ward_type); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_ptrm=<? echo($ward_ptrm); ?>"><? } ?>←<? if ($month > 1) { ?></a><? } ?>
<?
for ($i = 1; $i <= 12; $i++) {
	echo("<img src=\"img/spacer.gif\" width=\"10\" height=\"1\" alt=\"\">");
	if ($i != $month) {
		echo("<a href=\"bed_date_status.php?session=$session&year=$year&month=$i&ward_type=$ward_type&bldg_cd=$bldg_cd&ward_ptrm=$ward_ptrm\">");
	}
	echo("{$i}月");
	if ($i != $month) {
		echo("</a>");
	}
}
?>
<img src="img/spacer.gif" width="10" height="1" alt=""><? if ($month < 12) { ?><a href="bed_date_status.php?session=<? echo($session); ?>&year=<? echo($year); ?>&month=<? echo($month + 1); ?>&ward_type=<? echo($ward_type); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_ptrm=<? echo($ward_ptrm); ?>"><? } ?>→<? if ($month < 12) { ?></a><? } ?>
</font></td>
<td align="right"><input type="button" value="Excel出力" onclick="document.excelform.submit();"<? if ($ward_type == "" || $bldg_cd == "") {echo(" disabled");} ?>></td>
</tr>
</table>

<? if ($ward_type != "" && $ward_type != "all" && $bldg_cd != "") { ?>

<!-- 病棟区分個別指定 -->
<table width="600" cellspacing="0" cellpadding="2" class="block">
<tr height="22" align="center" bgcolor="#f6f9ff">
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">繰越</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入棟</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退棟</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">実数</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">同日退院</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">延数</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象外</font></td>
</tr>
<tr height="22" align="center" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">再</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">再</font></td>
</tr>
<?
$ward_ptrm_ids = ($ward_ptrm == "") ? array_keys($ward_ptrms) : array($ward_ptrm);
$status = get_status($con, $year, $month, $bldg_cd, $ward_ptrm_ids, $fname);
foreach ($status["main"] as $tmp_day => $tmp_data) {
	if ($tmp_day == 0) {
		continue;
	}
	echo("<tr height=\"22\" align=\"right\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . sprintf("%02d", $tmp_day) . "</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["carrying_over"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["inpatient"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["outpatient"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["new_in_ward"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["re_in_ward"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["new_out_ward"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["re_out_ward"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["actual_number"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["same_day"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["total"]}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data["except"]}</font></td>\n");
	echo("</tr>\n");
}

echo("<tr height=\"22\" align=\"right\">\n");
echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">計</font></td>\n");
echo("<td></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["inpatient"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["outpatient"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["new_in_ward"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["re_in_ward"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["new_out_ward"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["re_out_ward"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["actual_number"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["same_day"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["total"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["total"]["except"]}</font></td>\n");
echo("</tr>\n");

echo("<tr height=\"22\" align=\"right\">\n");
echo("<td colspan=\"2\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">除外</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["except"]["inpatient"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["except"]["outpatient"]}</font></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["except"]["new_in_ward"]}</font></td>\n");
echo("<td style=\"border-bottom-style:none;\"></td>\n");
echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$status["except"]["new_out_ward"]}</font></td>\n");
echo("</tr>\n");
?>
</table>

<? } else if ($ward_type == "all" && $bldg_cd != "") { ?>

<?
// 指定棟の病棟一覧を取得
$sql = "select ward_cd, ward_name from wdmst";
$cond = "where bldg_cd = $bldg_cd and (not ward_del_flg) order by ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$wards = array();
while ($row = pg_fetch_array($sel)) {
	$wards[$row["ward_cd"]] = $row["ward_name"];
}
?>

<!-- 病棟区分「すべて」 -->
<table width="*" cellspacing="0" cellpadding="2" class="block">
<tr height="22" align="center" bgcolor="#f6f9ff">
<td rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
<? foreach ($wards as $tmp_ward_name) { ?>
<td colspan="7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ward_name); ?></font></td>
<? } ?>
</tr>
<tr height="22" align="center" bgcolor="#f6f9ff">
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者数</font></td>
<? for ($i = 1, $j = count($wards); $i <= $j; $i++) { ?>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入棟</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退棟</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者数</font></td>
<? } ?>
</tr>
<tr height="22" align="center" bgcolor="#f6f9ff">
<? for ($i = 1, $j = count($wards); $i <= $j; $i++) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">再</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">再</font></td>
<? } ?>
</tr>
<?
$status = get_status_all($con, $year, $month, $bldg_cd, $wards, $fname);
foreach ($status["main"] as $tmp_day => $tmp_data) {
	if ($tmp_day == 0) {
		continue;
	}
	echo("<tr height=\"22\" align=\"right\">\n");
	echo("<td>");
	echo_with_font_tag(sprintf("%02d", $tmp_day), $tmp_data["future"]);
	echo("</td>\n");
	echo("<td>");
	echo_with_font_tag($tmp_data["all"]["inpatient"], $tmp_data["future"]);
	echo("</td>\n");
	echo("<td>");
	echo_with_font_tag($tmp_data["all"]["outpatient"], $tmp_data["future"]);
	echo("</td>\n");
	echo("<td>");
	echo_with_font_tag($tmp_data["all"]["actual_number"], $tmp_data["future"]);
	echo("</td>\n");
	foreach ($wards as $tmp_ward_cd => $tmp_ward_name) {
		echo("<td>");
		echo_with_font_tag($tmp_data["wards"][$tmp_ward_cd]["inpatient"], $tmp_data["future"]);
		echo("</td>\n");
		echo("<td>");
		echo_with_font_tag($tmp_data["wards"][$tmp_ward_cd]["outpatient"], $tmp_data["future"]);
		echo("</td>\n");
		echo("<td>");
		echo_with_font_tag($tmp_data["wards"][$tmp_ward_cd]["new_in_ward"], $tmp_data["future"]);
		echo("</td>\n");
		echo("<td>");
		echo_with_font_tag($tmp_data["wards"][$tmp_ward_cd]["re_in_ward"], $tmp_data["future"]);
		echo("</td>\n");
		echo("<td>");
		echo_with_font_tag($tmp_data["wards"][$tmp_ward_cd]["new_out_ward"], $tmp_data["future"]);
		echo("</td>\n");
		echo("<td>");
		echo_with_font_tag($tmp_data["wards"][$tmp_ward_cd]["re_out_ward"], $tmp_data["future"]);
		echo("</td>\n");
		echo("<td>");
		echo_with_font_tag($tmp_data["wards"][$tmp_ward_cd]["actual_number"], $tmp_data["future"]);
		echo("</td>\n");
	}
	echo("</tr>\n");
}
?>
<tr height="22" align="right">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["all"]["inpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["all"]["outpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["all"]["actual_number"]); ?></font></td>
<? foreach ($wards as $tmp_ward_cd => $tmp_ward_name) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["wards"][$tmp_ward_cd]["inpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["wards"][$tmp_ward_cd]["outpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["wards"][$tmp_ward_cd]["new_in_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["wards"][$tmp_ward_cd]["re_in_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["wards"][$tmp_ward_cd]["new_out_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["wards"][$tmp_ward_cd]["re_out_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status["total"]["wards"][$tmp_ward_cd]["actual_number"]); ?></font></td>
<? } ?>
</tr>
<tr height="22" align="right">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["all"]["inpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["all"]["outpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["all"]["actual_number"]); ?></font></td>
<? foreach ($wards as $tmp_ward_cd => $tmp_ward_name) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["wards"][$tmp_ward_cd]["inpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["wards"][$tmp_ward_cd]["outpatient"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["wards"][$tmp_ward_cd]["new_in_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["wards"][$tmp_ward_cd]["re_in_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["wards"][$tmp_ward_cd]["new_out_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["wards"][$tmp_ward_cd]["re_out_ward"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo_average($status["average"]["wards"][$tmp_ward_cd]["actual_number"]); ?></font></td>
<? } ?>
</tr>
</table>
<? } ?>
</td>
</tr>
</table>
<form name="excelform" action="bed_date_status_excel.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="year" value="<? echo($year); ?>">
<input type="hidden" name="month" value="<? echo($month); ?>">
<input type="hidden" name="ward_type" value="<? echo($ward_type); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_ptrm_ids" value="<? echo(join("|", $ward_ptrm_ids)); ?>">
<input type="hidden" name="ward_ptrm" value="<? echo($ward_ptrm); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
<?
function get_ward_ptrms($con, $buildings, $ward_type, $bldg_cd, $fname) {

	// 棟が未指定の場合は棟が未登録なので空配列を返す
	if ($bldg_cd == "") {
		return array();
	}

	// 病棟一覧を取得
	$sql = "select ward_cd, ward_name, ward_type from wdmst";
	$cond = "where bldg_cd = $bldg_cd and ward_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$wards = array();
	while ($row = pg_fetch_array($sel)) {
		$wards[$row["ward_cd"]] = array("name" => $row["ward_name"],
		                                "type" => $row["ward_type"]);
	}

	// 病室一覧を取得
	$sql = "select ward_cd, ptrm_room_no, ptrm_name, ward_type, case when ward_type = '0' then '0' else '1' end from ptrmmst";
	$cond = "where bldg_cd = $bldg_cd and ptrm_del_flg = 'f' order by ward_cd, 5, ptrm_room_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 病室一覧をリスト形式に加工
	$rooms = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_ptrm_room_no = $row["ptrm_room_no"];
		$tmp_ptrm_name = $row["ptrm_name"];
		$tmp_ward_name = $row["ward_name"];
		$tmp_ptrm_ward_type = $row["ward_type"];

		// 病棟区分指定ありの場合
		if ($ward_type != "") {

			// 病室に病棟区分の指定があり、指定された病棟区分と異なる場合はスキップ
			if ($tmp_ptrm_ward_type != "0" && $tmp_ptrm_ward_type != $ward_type) {
				continue;
			}

			// 病室に病棟区分の指定がなく、病棟の病棟区分が指定されたものと異なる場合はスキップ
			if ($tmp_ptrm_ward_type == "0" && $wards[$tmp_ward_cd]["type"] != $ward_type) {
				continue;
			}
		}

		if (!isset($rooms[$tmp_ward_cd])) {
			$rooms[$tmp_ward_cd] = array();
		}

		// 病室の病棟区分が未設定の場合：1行にまとめる
		if ($tmp_ptrm_ward_type == "0") {
			if (!isset($rooms[$tmp_ward_cd][0])) {
				$rooms[$tmp_ward_cd][0] = array(
					"title" => $buildings[$bldg_cd] . $wards[$tmp_ward_cd]["name"],
					"ptrm_room_no_list" => array()
				);
			}
			$rooms[$tmp_ward_cd][0]["ptrm_room_no_list"][] = $tmp_ptrm_room_no;

		// 病室の病棟区分が設定ありの場合：1行1病室
		} else {
			$rooms[$tmp_ward_cd][] = array(
				"title" => $buildings[$bldg_cd] . $wards[$tmp_ward_cd]["name"] . $tmp_ptrm_name,
				"ptrm_room_no_list" => array($tmp_ptrm_room_no)
			);
		}
	}
	unset($wards);

	$ward_ptrms = array();
	foreach ($rooms as $tmp_ward_cd => $tmp_rows) {
		foreach ($tmp_rows as $tmp_row) {
			$ward_ptrms["{$tmp_ward_cd}-" . join(",", $tmp_row["ptrm_room_no_list"])] = $tmp_row["title"];
		}
	}
	return $ward_ptrms;
}

function get_status($con, $year, $month, $bldg_cd, $ward_ptrm_ids, $fname) {

	// 未来年月の場合は空の稼働状況配列を返す
	$days_in_month = days_in_month($year, $month);
	if (intval($year) > intval(date("Y")) || (intval($year) == intval(date("Y")) && intval($month) > intval(date("m")))) {
		for ($i = 1; $i <= $days_in_month; $i++) {
			$status[$i] = array();
		}

		return array("main" => $status, "total" => array(), "except" => array());
	}

	// 稼働状況配列を初期化
	for ($i = 0; $i <= $days_in_month; $i++) {
		$status[$i] = array(
			"inpatient"     => 0,
			"outpatient"    => 0,
			"new_in_ward"   => 0,
			"re_in_ward"    => 0,
			"new_out_ward"  => 0,
			"re_out_ward"   => 0,
			"actual_number" => 0,
			"same_day"      => 0,
			"except"        => 0
		);
	}
	$except_status["inpatient"]    = 0;
	$except_status["outpatient"]   = 0;
	$except_status["new_in_ward"]  = 0;
	$except_status["new_out_ward"] = 0;

	// 除外対象部屋一覧を取得
	$except_rooms = get_except_rooms($con, $fname);

	// 期間内在院患者を取得（繰越計算のために指定年月1日の前日を含める）
	$from_date = date("Ymd", strtotime("-1day", mktime(0, 0, 0, $month, 1, $year)));
	$to_date = sprintf("%04d%02d%02d", $year, $month, $days_in_month);
	$sel = get_patient_list($con, $from_date, $to_date, false, $fname);

	// 指定年月を変数に保存
	$ym = sprintf("%04d%02d", $year, $month);

	// 同日退院カウント用の配列を初期化
	$same_day_counter = array();

	// 患者情報をループ
	while ($row = pg_fetch_array($sel)) {
		$tmp_ptif_id = $row["ptif_id"];
		$tmp_pt_type = $row["pt_type"];
		$tmp_in_dt = $row["inpt_in_dt"];
		$tmp_in_tm = $row["inpt_in_tm"];
		$tmp_in_dttm = "$tmp_in_dt$tmp_in_tm";
		$tmp_out_dt = $row["inpt_out_dt"];
		$tmp_out_tm = $row["inpt_out_tm"];
		$tmp_bldg_cd = $row["bldg_cd"];
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_ptrm_room_no = $row["ptrm_room_no"];
		$tmp_except_flg = $row["inpt_except_flg"];
		$tmp_except_terms = array();
		for ($i = 1; $i <= 5; $i++) {
			$tmp_except_terms[$i] = array("from" => $row["inpt_except_from_date$i"],
			                              "to"   => $row["inpt_except_to_date$i"]);
		}

		// 在院患者の場合、退院日を「9999/12/31」に仮設定
		if ($tmp_pt_type == "in") {
			$tmp_out_dt = "99991231";
			$tmp_out_tm = "2330";
		}
		$tmp_out_dttm = "$tmp_out_dt$tmp_out_tm";

		// 入院期間内の移転確定情報を取得
		$sql_move = "select * from inptmove";
		$cond_move = "where ptif_id = '$tmp_ptif_id' and move_cfm_flg = 't' and ((move_dt = '$tmp_in_dt' and move_tm > '$tmp_in_tm') or (move_dt > '$tmp_in_dt')) and ((move_dt = '$tmp_out_dt' and move_tm < '$tmp_out_tm') or (move_dt < '$tmp_out_dt')) order by move_dt, move_tm";
		$sel_move = select_from_table($con, $sql_move, $cond_move, $fname);
		if ($sel_move == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$movings = (pg_num_rows($sel_move) > 0) ? pg_fetch_all($sel_move) : array();

		// 入院日時時点の病室を求める
		$tmp_current_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";
		$tmp_in_ptrm = get_initial_room($movings, $tmp_current_ptrm);

		// 入院日が期間内の場合
		if ($tmp_in_dt >= $from_date && $tmp_in_dt <= $to_date) {

			// 入院日時時点の病室がカウント対象であれば
			if (is_countable_ptrm($tmp_in_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms)) {

				// 除外患者であればサマリを増やす
				if (is_except_patient($tmp_except_flg, $tmp_in_dt, $tmp_except_terms)) {

					// ただし指定年月前日の場合は増やさない
					if ($tmp_in_dt != $from_date) {
						$except_status["inpatient"]++;
					}

				// 除外患者でなければ「入院」を増やす
				} else {
					$tmp_day = ($tmp_in_dt == $from_date) ? 0 : intval(substr($tmp_in_dt, 6, 2));
					$status[$tmp_day]["inpatient"]++;

					// 同日退院カウント用データをセット
					if (!isset($same_day_counter[$tmp_ptif_id]["in"])) {
						$same_day_counter[$tmp_ptif_id]["in"] = array();
					}
					if (!in_array($tmp_in_dt, $same_day_counter[$tmp_ptif_id]["in"])) {
						$same_day_counter[$tmp_ptif_id]["in"][] = $tmp_in_dt;
					}
				}
			}
		}

		// 退院日が期間内の場合
		if ($tmp_out_dt >= $from_date && $tmp_out_dt <= $to_date) {

			// 退院日時時点の病室を求める
			$tmp_out_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";

			// カウント対象の病室であれば
			if (is_countable_ptrm($tmp_out_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms)) {

				// 除外患者であればサマリを増やす
				if (is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {

					// ただし指定年月前日の場合は増やさない
					if ($tmp_out_dt != $from_date) {
						$except_status["outpatient"]++;
					}

				// 除外患者でなければ「退院」を増やす
				} else {
					$tmp_day = ($tmp_out_dt == $from_date) ? 0 : intval(substr($tmp_out_dt, 6, 2));
					$status[$tmp_day]["outpatient"]++;

					// 同日退院カウント用データをセット
					if (!isset($same_day_counter[$tmp_ptif_id]["out"])) {
						$same_day_counter[$tmp_ptif_id]["out"] = array();
					}
					if (!in_array($tmp_out_dt, $same_day_counter[$tmp_ptif_id]["out"])) {
						$same_day_counter[$tmp_ptif_id]["out"][] = $tmp_out_dt;
					}
				}
			}
		}

		// 転棟情報をループ
		$tmp_new_in_ward = true;
		$tmp_new_out_ward = true;
		foreach ($movings as $tmp_moving) {
			$tmp_move_dt = $tmp_moving["move_dt"];
			$tmp_move_ym = substr($tmp_move_dt, 0, 6);
			$tmp_from_bldg_cd = $tmp_moving["from_bldg_cd"];
			$tmp_from_ward_cd = $tmp_moving["from_ward_cd"];
			$tmp_from_ptrm_room_no = $tmp_moving["from_ptrm_room_no"];
			$tmp_to_bldg_cd = $tmp_moving["to_bldg_cd"];
			$tmp_to_ward_cd = $tmp_moving["to_ward_cd"];
			$tmp_to_ptrm_room_no = $tmp_moving["to_ptrm_room_no"];

			// 年月が指定年月より未来であればカウント処理に無関係になるのでループを抜ける
			if ($tmp_move_ym > $ym) {
				break;
			}

			$tmp_from_ptrm = "{$tmp_from_bldg_cd}-{$tmp_from_ward_cd}-{$tmp_from_ptrm_room_no}";
			$tmp_to_ptrm = "{$tmp_to_bldg_cd}-{$tmp_to_ward_cd}-{$tmp_to_ptrm_room_no}";

			// 入棟カウント対象の場合
			if (must_count_as_in_ward($tmp_from_ptrm, $tmp_to_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms)) {

				// 期間内であれば、除外患者を考慮しつつカウント処理
				if ($tmp_move_dt >= $from_date && $tmp_move_dt <= $to_date) {
					$tmp_key = ($tmp_new_in_ward) ? "new_in_ward" : "re_in_ward";
					if (is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
						if ($tmp_move_dt != $from_date) {
							$except_status[$tmp_key]++;
						}
					} else {
						$tmp_day = ($tmp_move_dt == $from_date) ? 0 : intval(substr($tmp_move_dt, 6, 2));
						$status[$tmp_day][$tmp_key]++;
					}
				}

				// 次以降を再入棟とする
				if ($tmp_new_in_ward) {
					$tmp_new_in_ward = false;
				}
			}

			// 退棟カウント対象の場合
			if (must_count_as_out_ward($tmp_from_ptrm, $tmp_to_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms)) {

				// 期間内であれば、除外患者を考慮しつつカウント処理
				if ($tmp_move_dt >= $from_date && $tmp_move_dt <= $to_date) {
					$tmp_key = ($tmp_new_out_ward) ? "new_out_ward" : "re_out_ward";
					if (is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
						if ($tmp_move_dt != $from_date) {
							$except_status[$tmp_key]++;
						}
					} else {
						$tmp_day = ($tmp_move_dt == $from_date) ? 0 : intval(substr($tmp_move_dt, 6, 2));
						$status[$tmp_day][$tmp_key]++;
					}
				}

				// 次以降を再退棟とする
				if ($tmp_new_out_ward) {
					$tmp_new_out_ward = false;
				}
			}
		}

		// 指定年月の日付をループ（24時のため次月の1日まで含める）
		for ($i = 1; $i <= $days_in_month + 1; $i++) {

			// 日時文字列を変数に格納（次月を考慮）
			if ($i <= $days_in_month) {
				$tmp_dt = $ym . sprintf("%02d", $i);
			} else {
				$tmp_dt = date("Ymd", strtotime("+1 day", mktime(0, 0, 0, $month, $days_in_month, $year)));
			}
			$tmp_dttm = $tmp_dt . "0000";

			// 在院期間外の場合、次の日へ
			if ($tmp_dttm < $tmp_in_dttm || $tmp_dttm >= $tmp_out_dttm) {
				continue;
			}

			// 0時時点の在院病室を取得
			$tmp_ptrm = get_room_by_datetime($tmp_dttm, $tmp_in_dttm, $tmp_out_dttm, $movings, $tmp_current_ptrm);

			// 除外病室の場合、次の日へ
			if (!is_countable_ptrm($tmp_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms)) {
				continue;
			}

			// 除外患者を考慮しつつカウント処理（前日の24時とみなす）
			if (is_except_patient($tmp_except_flg, $tmp_dt, $tmp_except_terms)) {
				$status[$i - 1]["except"]++;
			} else {
				$status[$i - 1]["actual_number"]++;
			}
		}
	}

	// 同日退院カウント用配列（患者単位）をループ
	foreach ($same_day_counter as $tmp_inout) {

		// 入院→退院または退院→入院のあった日をカウント
		$tmp_dates = array_intersect($tmp_inout["in"], $tmp_inout["out"]);
		foreach ($tmp_dates as $tmp_date) {
			$tmp_day = ($tmp_date == $from_date) ? 0 : intval(substr($tmp_date, 6, 2));
			$status[$tmp_day]["same_day"]++;
		}
	}

	// 繰越をセット・延数を計算
	for ($i = 0; $i <= $days_in_month; $i++) {
		if ($i > 0) {
			$status[$i]["carrying_over"] = $status[$i - 1]["actual_number"];
		}
		$status[$i]["total"] = $status[$i]["actual_number"] + $status[$i]["new_out_ward"] + $status[$i]["same_day"];
	}

	// 総計配列を作成
	$total = array(
		"inpatient"     => 0,
		"outpatient"    => 0,
		"new_in_ward"   => 0,
		"re_in_ward"    => 0,
		"new_out_ward"  => 0,
		"re_out_ward"   => 0,
		"actual_number" => 0,
		"same_day"      => 0,
		"total"         => 0,
		"except"        => 0
	);
	for ($i = 1; $i <= $days_in_month; $i++) {
		$total["inpatient"]     += $status[$i]["inpatient"];
		$total["outpatient"]    += $status[$i]["outpatient"];
		$total["new_in_ward"]   += $status[$i]["new_in_ward"];
		$total["re_in_ward"]    += $status[$i]["re_in_ward"];
		$total["new_out_ward"]  += $status[$i]["new_out_ward"];
		$total["re_out_ward"]   += $status[$i]["re_out_ward"];
		$total["actual_number"] += $status[$i]["actual_number"];
		$total["same_day"]      += $status[$i]["same_day"];
		$total["total"]         += $status[$i]["total"];
		$total["except"]        += $status[$i]["except"];
	}

	return array("main" => $status, "total" => $total, "except" => $except_status);
}

function get_status_all($con, $year, $month, $bldg_cd, $wards, $fname) {

	// 稼働状況配列を初期化
	$days_in_month = days_in_month($year, $month);
	$today = date("Ymd");
	$tomorrow = date("Ymd", strtotime("+1 day"));
	$is_today_in_month = false;
	for ($i = 0; $i <= $days_in_month; $i++) {
		$tmp_date = sprintf("%04d%02d%02d", $year, $month, $i);

		// 当日の予定はxを足して表現する（例：今日が5日なら5x）
		// その予定は未来扱いとなる
		if ($tmp_date == $today) {
			$tmp_keys = array($i, "{$i}x");
			$is_today_in_month = true;
		} else {
			$tmp_keys = array($i);
		}
		foreach ($tmp_keys as $j => $tmp_key) {
			$status[$tmp_key] = array(
				"future" => ($tmp_date > $today || $j == 1),
				"all" => array(
					"inpatient" => 0,
					"outpatient" => 0,
					"actual_number" => 0
				),
				"wards" => array()
			);
			foreach ($wards as $tmp_ward_cd => $tmp_ward_name) {
				$status[$tmp_key]["wards"][$tmp_ward_cd] = array(
					"inpatient" => 0,
					"outpatient" => 0,
					"new_in_ward" => 0,
					"re_in_ward" => 0,
					"new_out_ward" => 0,
					"re_out_ward" => 0,
					"actual_number" => 0
				);
			}
		}
	}
	$total = array(
		"all" => array(
			"inpatient" => 0,
			"outpatient" => 0,
			"actual_number" => 0
		),
		"wards" => array()
	);
	foreach ($wards as $tmp_ward_cd => $tmp_ward_name) {
		$total["wards"][$tmp_ward_cd] = array(
			"inpatient" => 0,
			"outpatient" => 0,
			"new_in_ward" => 0,
			"re_in_ward" => 0,
			"new_out_ward" => 0,
			"re_out_ward" => 0,
			"actual_number" => 0
		);
	}

	// 除外対象部屋一覧を取得
	$except_rooms = get_except_rooms($con, $fname);

	// 期間内在院患者を取得（繰越計算のために指定年月1日の前日を含める）
	$last_date_of_month = sprintf("%04d%02d%02d", $year, $month, $days_in_month);
	$from_date = date("Ymd", strtotime("-1day", mktime(0, 0, 0, $month, 1, $year)));
	$to_date = $last_date_of_month;
	if ($from_date <= $to_date) {
		$sel = get_patient_list($con, $from_date, $today, false, $fname);

		// 指定年月を変数に保存
		$ym = sprintf("%04d%02d", $year, $month);

		// 患者情報をループ
		while ($row = pg_fetch_array($sel)) {
			$tmp_ptif_id = $row["ptif_id"];
			$tmp_pt_type = $row["pt_type"];
			$tmp_in_dt = $row["inpt_in_dt"];
			$tmp_in_tm = $row["inpt_in_tm"];
			$tmp_in_dttm = "$tmp_in_dt$tmp_in_tm";
			$tmp_out_dt = $row["inpt_out_dt"];
			$tmp_out_tm = $row["inpt_out_tm"];
			$tmp_bldg_cd = $row["bldg_cd"];
			$tmp_ward_cd = $row["ward_cd"];
			$tmp_ptrm_room_no = $row["ptrm_room_no"];
			$tmp_except_flg = $row["inpt_except_flg"];
			$tmp_except_terms = array();
			for ($i = 1; $i <= 5; $i++) {
				$tmp_except_terms[$i] = array("from" => $row["inpt_except_from_date$i"],
				                              "to"   => $row["inpt_except_to_date$i"]);
			}

			// 在院患者の場合、退院日を「9999/12/31」に仮設定
			if ($tmp_pt_type == "in") {
				$tmp_out_dt = "99991231";
				$tmp_out_tm = "2330";
			}
			$tmp_out_dttm = "$tmp_out_dt$tmp_out_tm";

			// 入院期間内の移転確定情報を取得
			$sql_move = "select * from inptmove";
			$cond_move = "where ptif_id = '$tmp_ptif_id' and move_cfm_flg = 't' and ((move_dt = '$tmp_in_dt' and move_tm > '$tmp_in_tm') or (move_dt > '$tmp_in_dt')) and ((move_dt = '$tmp_out_dt' and move_tm < '$tmp_out_tm') or (move_dt < '$tmp_out_dt')) order by move_dt, move_tm";
			$sel_move = select_from_table($con, $sql_move, $cond_move, $fname);
			if ($sel_move == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$movings = (pg_num_rows($sel_move) > 0) ? pg_fetch_all($sel_move) : array();

			// 入院日時時点の病室を求める
			$tmp_current_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";
			$tmp_in_ptrm = get_initial_room($movings, $tmp_current_ptrm);

			// 入院日が期間内の場合
			if ($tmp_in_dt >= $from_date && $tmp_in_dt <= $to_date) {

				// 入院日時時点の病室が除外病室でなければ
				if (!in_array($tmp_in_ptrm, $except_rooms)) {

					// 除外患者でなければ「入院」を増やす
					if (!is_except_patient($tmp_except_flg, $tmp_in_dt, $tmp_except_terms)) {
						$tmp_day = ($tmp_in_dt == $from_date) ? 0 : intval(substr($tmp_in_dt, 6, 2));
						$status[$tmp_day]["all"]["inpatient"]++;
						$tmp_in_bldg_cd = bldg_cd_of_ptrm($tmp_in_ptrm);
						if ($tmp_in_bldg_cd == $bldg_cd) {
							$tmp_in_ward_cd = ward_cd_of_ptrm($tmp_in_ptrm);
							if (isset($status[$tmp_day]["wards"][$tmp_in_ward_cd])) {
								$status[$tmp_day]["wards"][$tmp_in_ward_cd]["inpatient"]++;
							}
						}
					}
				}
			}

			// 退院日が期間内の場合
			if ($tmp_out_dt >= $from_date && $tmp_out_dt <= $to_date) {

				// 退院日時時点の病室を求める
				$tmp_out_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";

				// 退院日時時点の病室が除外病室でなければ
				if (!in_array($tmp_out_ptrm, $except_rooms)) {

					// 除外患者でなければ「退院」を増やす
					if (!is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
						$tmp_day = ($tmp_out_dt == $from_date) ? 0 : intval(substr($tmp_out_dt, 6, 2));
						$status[$tmp_day]["all"]["outpatient"]++;
						if ($tmp_bldg_cd == $bldg_cd) {
							if (isset($status[$tmp_day]["wards"][$tmp_ward_cd])) {
								$status[$tmp_day]["wards"][$tmp_ward_cd]["outpatient"]++;
							}
						}
					}
				}
			}

			// 転棟情報をループ
			$tmp_re_in_wards = array();
			$tmp_re_out_wards = array();
			foreach ($movings as $tmp_moving) {
				$tmp_move_dt = $tmp_moving["move_dt"];
				$tmp_move_ym = substr($tmp_move_dt, 0, 6);
				$tmp_from_bldg_cd = $tmp_moving["from_bldg_cd"];
				$tmp_from_ward_cd = $tmp_moving["from_ward_cd"];
				$tmp_from_ptrm_room_no = $tmp_moving["from_ptrm_room_no"];
				$tmp_to_bldg_cd = $tmp_moving["to_bldg_cd"];
				$tmp_to_ward_cd = $tmp_moving["to_ward_cd"];
				$tmp_to_ptrm_room_no = $tmp_moving["to_ptrm_room_no"];

				// 年月が指定年月より未来であればカウント処理に無関係になるのでループを抜ける
				if ($tmp_move_ym > $ym) {
					break;
				}

				$tmp_from_ptrm = "{$tmp_from_bldg_cd}-{$tmp_from_ward_cd}-{$tmp_from_ptrm_room_no}";
				$tmp_to_ptrm = "{$tmp_to_bldg_cd}-{$tmp_to_ward_cd}-{$tmp_to_ptrm_room_no}";

				// 入棟カウント対象の場合
				if (must_count_as_in_ward_all($tmp_from_ptrm, $tmp_to_ptrm, $bldg_cd, $except_rooms)) {

					// 期間内であれば、除外患者を考慮しつつカウント処理
					if ($tmp_move_dt >= $from_date && $tmp_move_dt <= $to_date) {
						if (!is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
							$tmp_day = ($tmp_move_dt == $from_date) ? 0 : intval(substr($tmp_move_dt, 6, 2));
							if (isset($status[$tmp_day]["wards"][$tmp_to_ward_cd])) {
								$tmp_key = (!in_array($tmp_to_ward_cd, $tmp_re_in_wards)) ? "new_in_ward" : "re_in_ward";
								$status[$tmp_day]["wards"][$tmp_to_ward_cd][$tmp_key]++;
							}
						}
					}

					// 次以降を再入棟とする
					if (!in_array($tmp_to_ward_cd, $tmp_re_in_wards)) {
						$tmp_re_in_wards[] = $tmp_to_ward_cd;
					}
				}

				// 退棟カウント対象の場合
				if (must_count_as_out_ward_all($tmp_from_ptrm, $tmp_to_ptrm, $bldg_cd, $except_rooms)) {

					// 期間内であれば、除外患者を考慮しつつカウント処理
					if ($tmp_move_dt >= $from_date && $tmp_move_dt <= $to_date) {
						if (!is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
							$tmp_day = ($tmp_move_dt == $from_date) ? 0 : intval(substr($tmp_move_dt, 6, 2));
							if (isset($status[$tmp_day]["wards"][$tmp_from_ward_cd])) {
								$tmp_key = (!in_array($tmp_from_ward_cd, $tmp_re_out_wards)) ? "new_out_ward" : "re_out_ward";
								$status[$tmp_day]["wards"][$tmp_from_ward_cd][$tmp_key]++;
							}
						}
					}

					// 次以降を再退棟とする
					if (!in_array($tmp_from_ward_cd, $tmp_re_out_wards)) {
						$tmp_re_out_wards[] = $tmp_from_ward_cd;
					}
				}
			}

			// 指定年月の日付をループ（24時のため次月の1日まで含める）
			for ($i = 1; $i <= $days_in_month + 1; $i++) {

				// 日時文字列を変数に格納（次月を考慮）
				if ($i <= $days_in_month) {
					$tmp_dt = $ym . sprintf("%02d", $i);
				} else {
					$tmp_dt = date("Ymd", strtotime("+1 day", mktime(0, 0, 0, $month, $days_in_month, $year)));
				}
				$tmp_dttm = $tmp_dt . "0000";

				// 在院期間外の場合、次の日へ
				if ($tmp_dttm < $tmp_in_dttm || $tmp_dttm >= $tmp_out_dttm) {
					continue;
				}

				// 0時時点の在院病室を取得
				$tmp_ptrm = get_room_by_datetime($tmp_dttm, $tmp_in_dttm, $tmp_out_dttm, $movings, $tmp_current_ptrm);

				// 0時時点の在院病室が除外病室の場合、次の日へ
				if (in_array($tmp_ptrm, $except_rooms)) {
					continue;
				}

				// 除外患者を考慮しつつカウント処理（前日の24時とみなす）
				if (!is_except_patient($tmp_except_flg, $tmp_dt, $tmp_except_terms)) {
					$status[$i - 1]["all"]["actual_number"]++;
					$tmp_bldg_cd = bldg_cd_of_ptrm($tmp_ptrm);
					if ($tmp_bldg_cd == $bldg_cd) {
						$tmp_ward_cd = ward_cd_of_ptrm($tmp_ptrm);
						if (isset($status[$i - 1]["wards"][$tmp_ward_cd])) {
							$status[$i - 1]["wards"][$tmp_ward_cd]["actual_number"]++;
						}
					}
				}
			}
		}
	}

	// 表示期間内に未来日付が存在したら
	$future_date_exists = false;
	foreach ($status as $tmp_day => $tmp_status) {
		if ($tmp_status["future"]) {
			$future_date_exists = true;
			break;
		}
	}
	if ($future_date_exists) {

		// 当日の実績患者数を当日の予定値にコピー
		if ($is_today_in_month) {
			$today_d = intval(date("d"));
			$status["{$today_d}x"]["all"]["actual_number"] = $status[$today_d]["all"]["actual_number"];
			foreach ($status[$today_d]["wards"] as $tmp_ward_cd => $tmp_data) {
				$status["{$today_d}x"]["wards"][$tmp_ward_cd]["actual_number"] = $tmp_data["actual_number"];
			}
		}

		$future_patients = array();

		// 未来分の入院予定を「入院」にカウント（入院予定患者分）
		$from_date = $today;
		$to_date = $last_date_of_month;
		$sql = "select ptif_id, inpt_in_res_dt, inpt_in_res_tm, bldg_cd, ward_cd, ptrm_room_no from inptres";
		$cond = "where inpt_in_res_dt_flg and inpt_in_res_dt between '$from_date' and '$to_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$tmp_in_dt = $row["inpt_in_res_dt"];
			$tmp_in_tm = $row["inpt_in_res_tm"];
			$tmp_bldg_cd = $row["bldg_cd"];
			$tmp_ward_cd = $row["ward_cd"];
			$tmp_ptrm_room_no = $row["ptrm_room_no"];

			// 入院予定の病室が除外病室であれば次の患者へ
			$tmp_in_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";
			if (in_array($tmp_in_ptrm, $except_rooms)) {
				continue;
			}

			$future_patients[] = array(
				"type" => "in",
				"in_dttm" => $tmp_in_dt . $tmp_in_tm,
				"ptrm" => $tmp_in_ptrm,
				"bldg_cd" => $tmp_bldg_cd,
				"ward_cd" => $tmp_ward_cd
			);

			// 入院数にカウント
			$tmp_day = (substr($tmp_in_dt, 0, 6) != $ym) ? 0 : intval(substr($tmp_in_dt, 6, 2));
			if ($tmp_in_dt == $today) $tmp_day .= "x";
			$status[$tmp_day]["all"]["inpatient"]++;
			if ($tmp_bldg_cd == $bldg_cd) {
				if (isset($status[$tmp_day]["wards"][$tmp_ward_cd])) {
					$status[$tmp_day]["wards"][$tmp_ward_cd]["inpatient"]++;
				}
			}
		}

		// 未来分の戻り入院予定を「入院」にカウント（退院予定患者分）
		$sql = "select inpt_back_in_dt, inpt_back_in_tm, inpt_back_bldg_cd, inpt_back_ward_cd, inpt_back_ptrm_room_no from inptmst";
		$cond = "where inpt_out_res_flg and inpt_back_in_dt between '$from_date' and '$to_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$tmp_in_dt = $row["inpt_back_in_dt"];
			$tmp_in_tm = $row["inpt_back_in_tm"];
			$tmp_bldg_cd = $row["inpt_back_bldg_cd"];
			$tmp_ward_cd = $row["inpt_back_ward_cd"];
			$tmp_ptrm_room_no = $row["inpt_back_ptrm_room_no"];

			// 入院予定の病室が除外病室であれば次の患者へ
			$tmp_in_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";
			if (in_array($tmp_in_ptrm, $except_rooms)) {
				continue;
			}

			$future_patients[] = array(
				"type" => "in",
				"in_dttm" => $tmp_in_dt . $tmp_in_tm,
				"ptrm" => $tmp_in_ptrm,
				"bldg_cd" => $tmp_bldg_cd,
				"ward_cd" => $tmp_ward_cd
			);

			// 入院数にカウント
			$tmp_day = (substr($tmp_in_dt, 0, 6) != $ym) ? 0 : intval(substr($tmp_in_dt, 6, 2));
			if ($tmp_in_dt == $today) $tmp_day .= "x";
			$status[$tmp_day]["all"]["inpatient"]++;
			if ($tmp_bldg_cd == $bldg_cd) {
				if (isset($status[$tmp_day]["wards"][$tmp_ward_cd])) {
					$status[$tmp_day]["wards"][$tmp_ward_cd]["inpatient"]++;
				}
			}
		}

		// 未来分の戻り入院予定を「入院」にカウント（退院患者分）
		$sql = "select inpt_back_in_dt, inpt_back_in_tm, inpt_back_bldg_cd, inpt_back_ward_cd, inpt_back_ptrm_room_no from inpthist";
		$cond = "where inpt_back_in_dt between '$from_date' and '$to_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$tmp_in_dt = $row["inpt_back_in_dt"];
			$tmp_in_tm = $row["inpt_back_in_tm"];
			$tmp_bldg_cd = $row["inpt_back_bldg_cd"];
			$tmp_ward_cd = $row["inpt_back_ward_cd"];
			$tmp_ptrm_room_no = $row["inpt_back_ptrm_room_no"];

			// 入院予定の病室が除外病室であれば次の患者へ
			$tmp_in_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";
			if (in_array($tmp_in_ptrm, $except_rooms)) {
				continue;
			}

			$future_patients[] = array(
				"type" => "in",
				"in_dttm" => $tmp_in_dt . $tmp_in_tm,
				"ptrm" => $tmp_in_ptrm,
				"bldg_cd" => $tmp_bldg_cd,
				"ward_cd" => $tmp_ward_cd
			);

			// 入院数にカウント
			$tmp_day = (substr($tmp_in_dt, 0, 6) != $ym) ? 0 : intval(substr($tmp_in_dt, 6, 2));
			if ($tmp_in_dt == $today) $tmp_day .= "x";
			$status[$tmp_day]["all"]["inpatient"]++;
			if ($tmp_bldg_cd == $bldg_cd) {
				if (isset($status[$tmp_day]["wards"][$tmp_ward_cd])) {
					$status[$tmp_day]["wards"][$tmp_ward_cd]["inpatient"]++;
				}
			}
		}

		// 未来日付の退院予定を「退院」にカウント
		$sql = "select inpt_in_dt, inpt_in_tm, inpt_out_res_dt, inpt_out_res_tm, bldg_cd, ward_cd, ptrm_room_no, inpt_except_flg, inpt_except_from_date1, inpt_except_to_date1, inpt_except_from_date2, inpt_except_to_date2, inpt_except_from_date3, inpt_except_to_date3, inpt_except_from_date4, inpt_except_to_date4, inpt_except_from_date5, inpt_except_to_date5 from inptmst";
		$cond = "where inpt_out_res_flg and inpt_out_res_dt between '$from_date' and '$to_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$tmp_in_dt = $row["inpt_in_dt"];
			$tmp_in_tm = $row["inpt_in_tm"];
			$tmp_out_dt = $row["inpt_out_res_dt"];
			$tmp_out_tm = $row["inpt_out_res_tm"];
			$tmp_bldg_cd = $row["bldg_cd"];
			$tmp_ward_cd = $row["ward_cd"];
			$tmp_ptrm_room_no = $row["ptrm_room_no"];
			$tmp_except_flg = $row["inpt_except_flg"];
			$tmp_except_terms = array();
			for ($i = 1; $i <= 5; $i++) {
				$tmp_except_terms[$i] = array("from" => $row["inpt_except_from_date$i"],
				                              "to"   => $row["inpt_except_to_date$i"]);
			}

			// 現在入院中の病室が除外病室であれば次の患者へ
			$tmp_out_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";
			if (in_array($tmp_out_ptrm, $except_rooms)) {
				continue;
			}

			$future_patients[] = array(
				"type" => "out",
				"out_dttm" => $tmp_out_dt . $tmp_out_tm,
				"ptrm" => $tmp_out_ptrm,
				"bldg_cd" => $tmp_bldg_cd,
				"ward_cd" => $tmp_ward_cd,
				"except_flg" => $tmp_except_flg,
				"except_terms" => $tmp_except_terms
			);

			// 除外患者であれば次の患者へ
			if (is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
				continue;
			}

			// 退院数にカウント
			$tmp_day = (substr($tmp_out_dt, 0, 6) != $ym) ? 0 : intval(substr($tmp_out_dt, 6, 2));
			if ($tmp_out_dt == $today) $tmp_day .= "x";
			$status[$tmp_day]["all"]["outpatient"]++;
			if ($tmp_bldg_cd == $bldg_cd) {
				if (isset($status[$tmp_day]["wards"][$tmp_ward_cd])) {
					$status[$tmp_day]["wards"][$tmp_ward_cd]["outpatient"]++;
				}
			}
		}

		foreach ($future_patients as $tmp_patient) {

			// 指定年月の日付をループ（24時のため次月の1日まで含める）
			for ($i = 1; $i <= $days_in_month + 1; $i++) {

				// 日時文字列を変数に格納（次月を考慮）
				if ($i <= $days_in_month) {
					$tmp_dt = $ym . sprintf("%02d", $i);
				} else {
					$tmp_dt = date("Ymd", strtotime("+1 day", mktime(0, 0, 0, $month, $days_in_month, $year)));
				}
				$tmp_dttm = $tmp_dt . "0000";

				// 稼働状況配列のキー
				$tmp_day = $i - 1;
				if ($tmp_dt == $tomorrow) $tmp_day .= "x";

				// 入院予定の場合
				if ($tmp_patient["type"] == "in") {

					// 入院予定日前の場合、次の日へ
					if ($tmp_dttm < $tmp_patient["in_dttm"]) {
						continue;
					}

					$status[$tmp_day]["all"]["actual_number"]++;
					if ($tmp_patient["bldg_cd"] == $bldg_cd) {
						if (isset($status[$tmp_day]["wards"][$tmp_patient["ward_cd"]])) {
							$status[$tmp_day]["wards"][$tmp_patient["ward_cd"]]["actual_number"]++;
						}
					}

				// 退院予定の場合
				} else if ($tmp_patient["type"] == "out") {

					// 退院予定日前の場合、次の日へ
					if ($tmp_dttm < $tmp_patient["out_dttm"]) {
						continue;
					}

					// 除外病室の場合、次の日へ
					if (in_array($tmp_patient["ptrm"], $tmp_ptrm["except_rooms"])) {
						continue;
					}

					// 除外患者を考慮しつつカウント処理（前日の24時とみなす）
					if (!is_except_patient($tmp_patient["except_flg"], $tmp_dt, $tmp_patient["except_terms"])) {
						$status[$tmp_day]["all"]["actual_number"]--;
						$tmp_bldg_cd = bldg_cd_of_ptrm($tmp_patient["ptrm"]);
						if ($tmp_patient["bldg_cd"] == $bldg_cd) {
							if (isset($status[$tmp_day]["wards"][$tmp_patient["ward_cd"]])) {
								$status[$tmp_day]["wards"][$tmp_patient["ward_cd"]]["actual_number"]--;
							}
						}
					}
				}
			}
		}
	}

	// 合計行の計算
	foreach ($status as $tmp_day => $tmp_data) {
		if ($tmp_day == 0) {
			continue;
		}

		$total["all"]["inpatient"] += $tmp_data["all"]["inpatient"];
		$total["all"]["outpatient"] += $tmp_data["all"]["outpatient"];
		$total["all"]["actual_number"] += $tmp_data["all"]["actual_number"];

		// 当日の患者数は予定のみを見る
		$is_today = (strpos($tmp_day, "x") !== false);
		if ($is_today) {
			$today_key = str_replace("x", "", $tmp_day);
			$total["all"]["actual_number"] -= $status[$today_key]["all"]["actual_number"];
		}

		foreach ($tmp_data["wards"] as $tmp_ward_cd => $tmp_ward_data) {
			$total["wards"][$tmp_ward_cd]["inpatient"] += $tmp_ward_data["inpatient"];
			$total["wards"][$tmp_ward_cd]["outpatient"] += $tmp_ward_data["outpatient"];
			$total["wards"][$tmp_ward_cd]["new_in_ward"] += $tmp_ward_data["new_in_ward"];
			$total["wards"][$tmp_ward_cd]["re_in_ward"] += $tmp_ward_data["re_in_ward"];
			$total["wards"][$tmp_ward_cd]["new_out_ward"] += $tmp_ward_data["new_out_ward"];
			$total["wards"][$tmp_ward_cd]["re_out_ward"] += $tmp_ward_data["re_out_ward"];
			$total["wards"][$tmp_ward_cd]["actual_number"] += $tmp_ward_data["actual_number"];
			if ($is_today) {
				$total["wards"][$tmp_ward_cd]["actual_number"] -= $status[$today_key]["wards"][$tmp_ward_cd]["actual_number"];
			}
		}
	}

	// 平均行の計算
	$average["all"]["inpatient"] = round($total["all"]["inpatient"] / $days_in_month, 1);
	$average["all"]["outpatient"] = round($total["all"]["outpatient"] / $days_in_month, 1);
	$average["all"]["actual_number"] = round($total["all"]["actual_number"] / $days_in_month, 1);
	foreach ($total["wards"] as $tmp_ward_cd => $tmp_data) {
		$average["wards"][$tmp_ward_cd]["inpatient"] = round($tmp_data["inpatient"] / $days_in_month, 1);
		$average["wards"][$tmp_ward_cd]["outpatient"] = round($tmp_data["outpatient"] / $days_in_month, 1);
		$average["wards"][$tmp_ward_cd]["new_in_ward"] = round($tmp_data["new_in_ward"] / $days_in_month, 1);
		$average["wards"][$tmp_ward_cd]["re_in_ward"] = round($tmp_data["re_in_ward"] / $days_in_month, 1);
		$average["wards"][$tmp_ward_cd]["new_out_ward"] = round($tmp_data["new_out_ward"] / $days_in_month, 1);
		$average["wards"][$tmp_ward_cd]["re_out_ward"] = round($tmp_data["re_out_ward"] / $days_in_month, 1);
		$average["wards"][$tmp_ward_cd]["actual_number"] = round($tmp_data["actual_number"] / $days_in_month, 1);
	}

	return array("main" => $status, "total" => $total, "average" => $average);
}

// 入院日時時点の病室を返す
function get_initial_room($movings, $cur_ptrm) {
	if (count($movings) == 0) {
		return $cur_ptrm;
	} else {
		return "{$movings[0]["from_bldg_cd"]}-{$movings[0]["from_ward_cd"]}-{$movings[0]["from_ptrm_room_no"]}";
	}
}

// 指定日時時点の病室を返す
function get_room_by_datetime($datetime, $in_dttm, $out_dttm, $movings, $cur_ptrm) {

	// 入院期間外であれば、falseを返す
	if ($datetime < $in_dttm || $datetime >= $out_dttm) {
		return false;
	}

	// 指定日時直後の転棟データを取得
	$post_moving = get_post_moving($datetime, $movings);

	// 転棟データが見つかれば、そのfrom病室を返す
	if ($post_moving) {
		return "{$post_moving["from_bldg_cd"]}-{$post_moving["from_ward_cd"]}-{$post_moving["from_ptrm_room_no"]}";
	}

	// 現在入院中の病室を返す
	return $cur_ptrm;
}

// 指定時刻直後の転棟情報を返す
function get_post_moving($datetime, $movings) {
	for ($i = 0, $j = count($movings); $i < $j; $i++) {
		if ("{$movings[$i]["move_dt"]}{$movings[$i]["move_tm"]}" >= $datetime) {
			return $movings[$i];
		}
	}
	return false;
}

function is_countable_ptrm($ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms) {
	if (in_array($ptrm, $except_rooms)) {
		return false;
	}

	return is_ptrm_in_options($ptrm, $bldg_cd, $ward_ptrm_ids);
}

function is_ptrm_in_options($ptrm, $bldg_cd, $ward_ptrm_ids) {
	list($target_bldg_cd, $target_ward_cd, $target_ptrm_room_no) = split("-", $ptrm);
	if ($target_bldg_cd != $bldg_cd) {
		return false;
	}

	foreach ($ward_ptrm_ids as $tmp_ward_ptrm_id) {
		list($tmp_ward_cd, $tmp_ptrm_room_no_list) = split("-", $tmp_ward_ptrm_id);
		if ($tmp_ward_cd != $target_ward_cd) {
			continue;
		}

		$tmp_ptrm_room_nos = split(",", $tmp_ptrm_room_no_list);
		if (in_array($target_ptrm_room_no, $tmp_ptrm_room_nos)) {
			return true;
		}
	}

	return false;
}

function is_except_patient($except_flg, $date, $except_terms) {
	if ($except_flg == "f") {
		return false;
	}

	if ($except_terms[1]["from"] == "") {
		return true;
	}

	foreach ($except_terms as $tmp_except_term) {
		if ($tmp_except_term["from"] == "") {
			continue;
		}

		if ($tmp_except_term["from"] <= $date && $date <= $tmp_except_term["to"]) {
			return true;
		}
	}

	return false;
}

function must_count_as_in_ward($from_ptrm, $to_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms) {

	// 転棟元が病棟オプションの選択値に含まれる場合はカウント対象外
	if (is_ptrm_in_options($from_ptrm, $bldg_cd, $ward_ptrm_ids)) {
		return false;
	}

	// 転棟先がカウント対象外の場合はカウント対象外
	if (!is_countable_ptrm($to_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms)) {
		return false;
	}

	return true;
}

function must_count_as_out_ward($from_ptrm, $to_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms) {

	// 転棟先が病棟オプションの選択値に含まれる場合はカウント対象外
	if (is_ptrm_in_options($to_ptrm, $bldg_cd, $ward_ptrm_ids)) {
		return false;
	}

	// 転棟元がカウント対象外の場合はカウント対象外
	if (!is_countable_ptrm($from_ptrm, $bldg_cd, $ward_ptrm_ids, $except_rooms)) {
		return false;
	}

	return true;
}

function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}

function echo_with_font_tag($value, $is_future) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"");
	if ($is_future) echo(" color=\"red\"");
	echo(">$value</font>");
}

function echo_average($average) {
//	echo ($average == 0.0) ? "0" : sprintf("%.1f", $average);
	echo $average;
}

function bldg_cd_of_ptrm($ptrm) {
	list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ptrm);
	return $bldg_cd;
}

function ward_cd_of_ptrm($ptrm) {
	list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ptrm);
	return $ward_cd;
}

function must_count_as_in_ward_all($from_ptrm, $to_ptrm, $bldg_cd, $except_rooms) {
	list($to_bldg_cd, $to_ward_cd, $to_ptrm_room_no) = split("-", $to_ptrm);
	if ($to_bldg_cd != $bldg_cd) {
		return false;
	}

	list($from_bldg_cd, $from_ward_cd, $from_ptrm_room_no) = split("-", $from_ptrm);
	if ($from_bldg_cd == $to_bldg_cd && $from_ward_cd == $to_ward_cd) {
		return false;
	}

	return !(in_array($to_ptrm, $except_rooms));
}

function must_count_as_out_ward_all($from_ptrm, $to_ptrm, $bldg_cd, $except_rooms) {
	list($from_bldg_cd, $from_ward_cd, $from_ptrm_room_no) = split("-", $from_ptrm);
	if ($from_bldg_cd != $bldg_cd) {
		return false;
	}

	list($to_bldg_cd, $to_ward_cd, $to_ptrm_room_no) = split("-", $to_ptrm);
	if ($from_bldg_cd == $to_bldg_cd && $from_ward_cd == $to_ward_cd) {
		return false;
	}

	return !(in_array($from_ptrm, $except_rooms));
}
