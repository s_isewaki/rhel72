<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
require("about_session.php");
require("about_authority.php");
require("show_class_name.ini");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 53, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2 = pg_fetch_result($sel, 0, "menu2");
$menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 社会福祉法人の場合、「・外来」を削除
if ($profile_type == PROFILE_TYPE_WELFARE) {
	$menu2_4 = mbereg_replace("・外来", "", $menu2_4);
}

// エリア一覧を取得
$sql = "select area_id, area_name from area";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$areas = (pg_num_rows($sel) > 0) ? pg_fetch_all($sel) : array();

// ログインユーザの職員IDを取得
$sql = "select emp_id, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_name");
// 初期表示時は当該職員を対象者に
if ($back != "t") {
	$target_id_list1 = $emp_id;
}

// メンバー情報を配列に格納
$arr_target['1'] = array();
if ($target_id_list1 != "") {
	$arr_target_id = split(",", $target_id_list1);
	for ($i = 0; $i < count($arr_target_id); $i++) {
		$tmp_emp_id = $arr_target_id[$i];
		$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
		}
		$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
		array_push($arr_target['1'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
	}
}

?>
<title>イントラネット | <?php echo($menu2_4); ?>設定 | 施設登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<?php
require("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=11';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?php
	for ($i=1; $i<=1; $i++) {
		$script = "m_target_list['$i'] = new Array(";
		$is_first = true;
		foreach($arr_target["$i"] as $row)
		{
			if($is_first)
			{
				$is_first = false;
			}
			else
			{
				$script .= ",";
			}
			$tmp_emp_id = $row["id"];
			$tmp_emp_name = $row["name"];
			$script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
		}
		$script .= ");\n";
		print $script;
	}
?>

// クリア
function clear_target(item_id, emp_id,emp_name) {
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		var is_exist_flg = false;
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			if(emp_id == m_target_list[item_id][i].emp_id)
			{
				is_exist_flg = true;
				break;
			}
		}
		m_target_list[item_id] = new Array();
		if (is_exist_flg == true) {
			m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
		}
		update_target_html(item_id);
	}
}

function submitForm() {
	var frm = document.getElementById("regform");
	closeEmployeeList();
	frm.submit();
}

function initPage() {
	//登録対象者を設定する。
	update_target_html("1");
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<?php echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<?php echo($session); ?>"><b><?php echo($menu2); ?></b></a> &gt; <a href="intra_info_allot.php?session=<?php echo($session); ?>"><b><?php echo($menu2_4); ?></b></a> &gt; <a href="allot_master_menu.php?session=<?php echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_info_allot.php?session=<?php echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="allot_master_menu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#5279a5"><a href="allot_master_fcl_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>施設登録</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form id="regform" name="regform" action="allot_master_fcl_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設名</font></td>
<td><input type="text" name="name" value="<?php echo($name); ?>" size="30" maxlength="80" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア</font></td>
<td><select name="area_id">
<option value="">（設定しない）
<?php
foreach ($areas as $tmp_area) {
	echo("<option value=\"{$tmp_area["area_id"]}\"");
	if ($tmp_area["area_id"] == $area_id) {
		echo(" selected");
	}
	echo(">{$tmp_area["area_name"]}\n");
}
?>
</select></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録可能職員</font><br>
<input type="button" name="emplist1" value="職員名簿" style="margin-left: 2em; width: 5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" value="クリア" style="margin-left: 2em; width: 5.5em;" onclick="clear_target('1','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
<td>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="60" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area1"></span>
</font>

</td>
</tr>
</table>

</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="button" value="登録" onclick="submitForm();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
</form>
</td>
</tr>
</table>
</body>
</html>
<?php pg_close($con);
