<?
ob_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 15, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$sql = "select * from bedcheckwait";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 11; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}

$sql = "select w.ptwait_id, w.ptif_id, (p.ptif_lt_kaj_nm || ' ' || p.ptif_ft_kaj_nm) as ptif_nm, p.ptif_birth, ptif_sex, (c.emp_lt_nm || ' ' || c.emp_ft_nm) as charge_emp_nm, w.progress, m.mst_name, s.item_name as intro_sect_name, w.intro_sect_text, case w.intro_doctor_no when 1 then s.doctor_name1 when 2 then s.doctor_name2 when 3 then s.doctor_name3 when 4 then s.doctor_name4 when 5 then s.doctor_name5 when 6 then s.doctor_name6 when 7 then s.doctor_name7 when 8 then s.doctor_name8 when 9 then s.doctor_name9 when 10 then s.doctor_name10 else null end as intro_doctor_name, w.intro_doctor_text, w.msw, w.disease, w.patho_from, w.patho_to, w.note, w.dementia, substr(w.reception_time, 1, 8) as reception_date, substr(w.verified_time, 1, 8) as verified_date, w.status1, w.status2, r.item_name as ccl_rsn_name, w.ccl_rsn as ccl_rsn_detail, i.inpt_in_dt from ptwait w";
$sql .= " inner join ptifmst p on w.ptif_id = p.ptif_id";
$sql .= " left join empmst c on w.charge_emp_id = c.emp_id";
$sql .= " left join institemmst m on w.intro_inst_cd = m.mst_cd";
$sql .= " left join institemrireki s on w.intro_inst_cd = s.mst_cd and w.intro_sect_rireki = s.rireki_index and w.intro_sect_cd = s.item_cd";
$sql .= " left join inptmst i on w.ptif_id = i.ptif_id and w.reception_time <= cast(i.inpt_in_dt || i.inpt_in_tm as varchar)";
$sql .= " left join (select rireki_index, item_cd, item_name from tmplitemrireki where mst_cd = 'A309') r on w.ccl_rsn_rireki = r.rireki_index and w.ccl_rsn_cd = r.item_cd";
$cond = ptwait_cond($from_date, $to_date, $progress_filter, $cond_id, $cond_name);
$cond .= " order by w.reception_time desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
?>
<style type="text/css">
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body>

<p>
<?
if ($cond_id !== "") echo("患者ID：{$cond_id}");
if ($cond_id !== "" && $cond_name !== "") echo("　");
if ($cond_name !== "") echo("患者氏名：{$cond_name}");
if ($cond_id !== "" || $cond_name !== "") echo("<br>");
?>
受付日：<? echo(format_date($from_date)); ?> 〜 <? echo(format_date($to_date)); ?>　進捗：<? echo(format_progress_filter($progress_filter)); ?></p>

<table class="block">
<tr bgcolor="#CCFFFF">
<td>患者ID</td>
<td>患者氏名</td>
<td align="right">年齢</td>
<td>性別</td>
<? if ($display1 == "t") { ?>
<td>担当</td>
<? } ?>
<td>進捗</td>
<? if ($display2 == "t") { ?>
<td>紹介元医療機関</td>
<? } ?>
<? if ($display3 == "t") { ?>
<td>診療科</td>
<? } ?>
<? if ($display4 == "t") { ?>
<td>前医</td>
<? } ?>
<? if ($display11 == "t") { ?>
<td>MSW</td>
<? } ?>
<? if ($display6 == "t") { ?>
<td>傷病名</td>
<? } ?>
<? if ($display7 == "t") { ?>
<td>発症日</td>
<? } ?>
<? if ($display8 == "t") { ?>
<td>備考</td>
<? } ?>
<? if ($display5 == "t") { ?>
<td>認知症</td>
<? } ?>
<td>受付日</td>
<td>最終確認日</td>
<? if ($display9 == "t") { ?>
<td>調整状況1</td>
<? } ?>
<? if ($display10 == "t") { ?>
<td>調整状況2</td>
<? } ?>
<td>キャンセル</td>
<td>入院日</td>
</tr>
<?
while ($row = pg_fetch_array($sel)) {
	$tmp_ptwait_id = $row["ptwait_id"];
	$tmp_ptif_id = $row["ptif_id"];
	$tmp_ptif_nm = $row["ptif_nm"];
	$tmp_age = calc_age($row["ptif_birth"]);
	$tmp_sex = format_sex($row["ptif_sex"]);
	$tmp_charge_emp_nm = $row["charge_emp_nm"];
	$tmp_progress = format_progress($row["progress"]);
	$tmp_intro_inst_name = $row["mst_name"];
	$tmp_intro_sect_name = format_name_detail($row["intro_sect_name"], $row["intro_sect_text"]);
	$tmp_intro_doctor_name = format_name_detail($row["intro_doctor_name"], $row["intro_doctor_text"]);
	$tmp_msw = $row["msw"];
	$tmp_disease = $row["disease"];
	$tmp_patho = format_patho($row["patho_from"], $row["patho_to"]);
	$tmp_note = $row["note"];
	$tmp_dementia = format_dementia($row["dementia"]);
	$tmp_reception_date = format_date($row["reception_date"]);
	$tmp_verified_date = format_date($row["verified_date"]);
	$tmp_status1 = $row["status1"];
	$tmp_status2 = $row["status2"];
	$tmp_ccl_rsn = format_name_detail($row["ccl_rsn_name"], $row["ccl_rsn_detail"]);
	$tmp_inpt_in_dt = format_date($row["inpt_in_dt"]);
?>
<tr>
<td><? echo($tmp_ptif_id); ?></td>
<td><? echo($tmp_ptif_nm); ?></td>
<td align="right"><? echo($tmp_age); ?></td>
<td><? echo($tmp_sex); ?></td>
<? if ($display1 == "t") { ?>
<td><? echo($tmp_charge_emp_nm); ?></td>
<? } ?>
<td><? echo($tmp_progress); ?></td>
<? if ($display2 == "t") { ?>
<td><? echo($tmp_intro_inst_name); ?></td>
<? } ?>
<? if ($display3 == "t") { ?>
<td><? echo($tmp_intro_sect_name); ?></td>
<? } ?>
<? if ($display4 == "t") { ?>
<td><? echo($tmp_intro_doctor_name); ?></td>
<? } ?>
<? if ($display11 == "t") { ?>
<td><? echo($tmp_msw); ?></td>
<? } ?>
<? if ($display6 == "t") { ?>
<td><? echo($tmp_disease); ?></td>
<? } ?>
<? if ($display7 == "t") { ?>
<td><? echo($tmp_patho); ?></td>
<? } ?>
<? if ($display8 == "t") { ?>
<td><? echo($tmp_note); ?></td>
<? } ?>
<? if ($display5 == "t") { ?>
<td><? echo($tmp_dementia); ?></td>
<? } ?>
<td><? echo($tmp_reception_date); ?></td>
<td><? echo($tmp_verified_date); ?></td>
<? if ($display9 == "t") { ?>
<td><? echo($tmp_status1); ?></td>
<? } ?>
<? if ($display10 == "t") { ?>
<td><? echo($tmp_status2); ?></td>
<? } ?>
<td><? echo($tmp_ccl_rsn); ?></td>
<td><? echo($tmp_inpt_in_dt); ?></td>
</tr>
<?
}
?>
</table>

</body>
<? pg_close($con); ?>
</html>
<?
// Excelを出力
$excel_data = mb_convert_encoding(ob_get_contents(), "Shift_JIS", 'EUC-JP');
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=waiting_list.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
echo($excel_data);
ob_end_flush();

function ptwait_cond($from_date, $to_date, $progress_filter, $cond_id, $cond_name) {
	$cond = "where w.reception_time between '{$from_date}0000' and '{$to_date}9999'";
	switch ($progress_filter) {
	case "2":  // 待機中
		$cond .= " and w.progress = '1'";
		break;
	case "3":  // キャンセル
		$cond .= " and w.progress = '2'";
		break;
	case "4":  // その他
		$cond .= " and w.progress = '5'";
		break;
	}

	mb_regex_encoding("EUC-JP");
	$cond_id = mb_ereg_replace("[ 　]", "", $cond_id);
	$cond_name = mb_ereg_replace("[ 　]", "", $cond_name);
	$pt_conds = array();
	if ($cond_id !== "") {
		$pt_conds[] = "(p.ptif_id = '$cond_id')";
	}
	if ($cond_name !== "") {
		$pt_conds[] = "(p.ptif_lt_kaj_nm || p.ptif_ft_kaj_nm like '%$cond_name%') or (p.ptif_lt_kana_nm || p.ptif_ft_kana_nm like '%$cond_name%')";
	}
	if (count($pt_conds) > 0) {
		$cond .= " and (" . join(" or ", $pt_conds) . ")";
	}

	return $cond;
}

function calc_age($birth) {
	$age = date("Y") - substr($birth, 0, 4);
	if (date("md") < substr($birth, 4, 8)) $age--;
	return $age;
}

function format_sex($sex) {
	switch ($sex) {
	case "1":
		return "男";
	case "2":
		return "女";
	default:
		return "不";
	}
}

function format_progress($progress) {
	switch ($progress) {
	case "1":
		return "待機中";
	case "2":
		return "待機キャンセル";
	case "3":
		return "入院予定";
	case "4":
		return "入院済み";
	case "5":
		return "その他";
	}
}

function format_progress_filter($progress_filter) {
	switch ($progress_filter) {
	case "1":
		return "全て";
	case "2":
		return "待機中";
	case "3":
		return "キャンセル";
	case "4":
		return "その他";
	}
}

function format_patho($from, $to) {
	if ($from == "" && $to == "") return "";
	$days = array(format_date($from), format_date($to));
	return join(" 〜 ", $days);
}

function format_date($date) {
	return ($date === "00000000") ? "" : preg_replace("/\A(\d{4})(\d{2})(\d{2})\z/", "$1/$2/$3", $date);
}

function format_dementia($dementia) {
	switch ($dementia) {
	case "t":
		return "有り";
	case "f":
		return "無し";
	default:
		return "";
	}
}

function format_name_detail($name, $detail) {
	$detail = str_replace("\n", " ", $detail);
	if ($name != "" && $detail != "") return "{$name} {$detail}";
	return "{$name}{$detail}";
}
