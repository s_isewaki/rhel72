<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("hiyari_report_auth_models.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
	showLoginPage();
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//更新処理
//====================================
if($mode == "update") {

	// トランザクションの開始
	pg_query($con, "begin transaction");

	$auth_class = "";
	$arr_auths = split(",", $inci_auths);
	foreach($arr_auths as $auth)
	{
		//当設定画面表示形式→DB形式(旧画面仕様)に変換
		$setting_ref_name = "setting_ref_".$auth;
		$setting_update_name = "setting_update_".$auth;
		$setting_torey_update_name = "setting_torey_update_".$auth;


		// 参照権限設定
		$report_db_use_flg = "t";
		$report_db_read_div = $$setting_ref_name;
		$report_db_read_auth_flg = "f";
		if($$setting_ref_name == "non")
		{
			$report_db_use_flg = "f";
			$report_db_read_div = "";
		}
		elseif($$setting_ref_name == "auth")
		{
			$report_db_read_auth_flg = "t";
		}
		
		// 報告ファイル更新権限設定
		$report_db_update_flg = "t";
		$report_db_update_div = $$setting_update_name;
		if($$setting_update_name == "non")
		{
			$report_db_update_flg = "f";
			$report_db_update_div = "";
		}
		if($auth == "GU")
		{
			$report_db_update_flg = "f";
			$report_db_update_div = "";
		}
		

//		// 送受信トレイ更新権限設定
//		$report_torey_update_flg = 't';
//		if($$setting_torey_update_name == "non")
//		{
//			$report_torey_update_flg = 'f';
//		}

		if($report_db_read_auth_flg == "t" || $report_db_update_div == "1")
		{
			$sql = "select max(target_class) as target_class from inci_auth_emp_post where auth = '$auth'";
			$sel = select_from_table($con, $sql, "", $fname);
			if($sel == 0){
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$target_class = pg_fetch_result($sel,0,"target_class");

			if($target_class == "")
			{
				//ダミー設定。このケースの場合、担当範囲=システム全体であるため効果はない。
				$target_class = "1";
			}

			if($report_db_read_auth_flg == "t")
			{
				//if($target_class == 4) $target_class = 3;
				$report_db_read_div = $target_class;
			}
			if($report_db_update_div == "1")
			{
				$report_db_update_div = $target_class;
			}
		}

		//更新
		update_inci_auth_case(
			$con,                       // データベースコネクションオブジェクト
			$fname,                     // ファイル名
			$auth,                      // 担当者（RM、所属長、部門長 ・・・）
			$report_db_use_flg,         // 報告ファイル利用フラグ
			$report_db_read_div,        // 報告書参照階層
			$report_db_read_auth_flg,   // 報告書参照権限フラグ
			$report_db_update_flg,      // 【報告ファイル】更新権限フラグ
			$report_db_update_div      // 更新可能階層
		);
	}
    
    // 送受信トレイの編集権限を設定
    setToreyUpdateFlg($con, $setting_torey_update_flg);    

	// トランザクションをコミット
	pg_query($con, "commit");
}

//==============================
//インシデント権限取得
//==============================
$arr_inci = get_inci_auth_case($con, $fname);
$inci_auths = "";
for($i=0; $i<count($arr_inci); $i++)
{
	if($arr_inci[$i]["auth"] != 'GU' && !is_usable_auth($arr_inci[$i]["auth"],$fname,$con))
	{
		continue;
	}

	if($inci_auths != "") {
		$inci_auths .= ",";
	}
	$inci_auths .= $arr_inci[$i]["auth"];
}

$report_torey_update_flg = getToreyUpdateFlg( $con );

if ( $report_torey_update_flg == 't' ){
    $checked = 'checked';
}

//DB形式(旧画面仕様)→当設定画面表示形式に変換
for($i=0; $i<count($arr_inci); $i++)
{
	$setting_ref_name = "setting_ref_".$arr_inci[$i]["auth"];
	$setting_update_name = "setting_update_".$arr_inci[$i]["auth"];
	$setting_torey_update_name = "setting_torey_update_".$arr_inci[$i]["auth"];

	$$setting_ref_name = "non";
	if($arr_inci[$i]["report_db_use_flg"] == "t")
	{
		$$setting_ref_name = $arr_inci[$i]["report_db_read_div"];
		if($arr_inci[$i]["report_db_read_auth_flg"] == "t")
		{
			$$setting_ref_name = "auth";
		}
	}

	$$setting_update_name = "non";
	if($arr_inci[$i]["report_db_update_flg"] == "t")
	{
		$$setting_update_name = $arr_inci[$i]["report_db_update_div"];
		if($$setting_update_name != "0")
		{
			$$setting_update_name = "1";
		}
	}
	
//	$$setting_torey_update_name = "f";
//	if($arr_inci[$i]["report_torey_update_flg"] == "t")
//	{
//            $$setting_torey_update_name = "t";
//	}
}

//==============================
// 組織階層情報取得
//==============================
$arr_class_name = get_class_name_array($con, $fname);



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 報告ファイル権限設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

	function init_page()
	{
	}

	function update()
	{
		document.mainform.mode.value="update";
		document.mainform.submit();
	}

	function ref_update_changed(action_obj,auth)
	{
//制御しない
//		if(auth == "GU")
//		{
//			return;
//		}
//
//		var r0_name = "setting_ref_" + auth + "_0";
//		var ra_name = "setting_ref_" + auth + "_auth";
//		var u0_name = "setting_update_" + auth + "_0";
//		var u1_name = "setting_update_" + auth + "_1";
//		var un_name = "setting_update_" + auth + "_non";
//
//		var r0_obj = document.getElementById(r0_name);
//		var ra_obj = document.getElementById(ra_name);
//		var u0_obj = document.getElementById(u0_name);
//		var u1_obj = document.getElementById(u1_name);
//		var un_obj = document.getElementById(un_name);
//
//		//全更新なら全参照
//		if(action_obj.id == u0_name)
//		{
//			r0_obj.checked = true;
//		}
//		//担当範囲更新なら(全参照or)担当範囲参照
//		if(action_obj.id == u1_name)
//		{
//			if(!r0_obj.checked)
//			{
//				ra_obj.checked = true;
//			}
//		}
//
//		//参照側を変更した場合
//		if(action_obj.id != u0_name && action_obj.id != u1_name && action_obj.id != un_name)
//		{
//			//担当範囲参照への変更の場合
//			if(action_obj.id == ra_name )
//			{
//				//全更新→担当範囲更新
//				if(u0_obj.checked)
//				{
//					u1_obj.checked = true;
//				}
//			}
//
//			//更新不可となる参照設定の場合
//			if(action_obj.id != r0_name && action_obj.id != ra_name)
//			{
//				//全更新/担当範囲更新→更新不可
//				un_obj.checked = true;
//			}
//		}
	}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="hiyari_report_db_auth.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="inci_auths" value="<?=$inci_auths?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top" align="left" bgcolor="#F5FFE5">




<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>





<!-- 参照 START -->
<?
if($arr_class_name["class_cnt"] == 4)
{
	$input_col_count = 7;
	$input_col_width = 80*7;
}
else
{
	$input_col_count = 6;
	$input_col_width = 80*6;
}

?>
<table border="0" cellspacing="1" cellpadding="0" class="list">

<tr>
<td width="200" bgcolor="#DFFFDC" align="center" rowspan="2" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font>
</td>
<td width="<?=$input_col_width?>" bgcolor="#DFFFDC" align="center" colspan="<?=$input_col_count?>" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照</font>
</td>
</tr>


<tr>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全て</font>
</td>
<?
for($j=0; $j<$arr_class_name["class_cnt"]; $j++)
{
?>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_class_name[$j]?></font>
</td>
<?
}
?>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当範囲</font>
</td>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不可</font>
</td>
</tr>






<?
for($i=0; $i<count($arr_inci); $i++)
{
	$setting_ref_name = "setting_ref_".$arr_inci[$i]["auth"];
?>



<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_inci[$i]["auth_name"]?></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="<?=$setting_ref_name?>_0" name="<?=$setting_ref_name?>" value="0" <?   if($$setting_ref_name == "0"){?>checked<?}   ?>  onclick="ref_update_changed(this,'<?=$arr_inci[$i]["auth"]?>')"></font>
</td>
<?
for($j=0; $j<$arr_class_name["class_cnt"]; $j++)
{
?>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="<?=$setting_ref_name?>_<?=$i+1?>" name="<?=$setting_ref_name?>" value="<?=$j+1?>" <?   if($$setting_ref_name == $j+1){?>checked<?}   ?>  onclick="ref_update_changed(this,'<?=$arr_inci[$i]["auth"]?>')"></font>
</td>
<?
}
?>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
<?
if($arr_inci[$i]["auth"] == 'GU')
{
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">-</font>
<?
}
else
{
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="<?=$setting_ref_name?>_auth" name="<?=$setting_ref_name?>" value="auth" <?   if($$setting_ref_name == "auth"){?>checked<?}   ?>  onclick="ref_update_changed(this,'<?=$arr_inci[$i]["auth"]?>')"></font>
<?
}
?>

</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="<?=$setting_ref_name?>_non" name="<?=$setting_ref_name?>" value="non" <?   if($$setting_ref_name == "non"){?>checked<?}   ?>  onclick="ref_update_changed(this,'<?=$arr_inci[$i]["auth"]?>')"></font>
</td>
</tr>
<?
}
?>



</table>
<!-- 参照 END -->




<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>




<!-- 更新 START -->
<table border="0" cellspacing="1" cellpadding="0" class="list">

<tr>
<td width="200" bgcolor="#DFFFDC" align="center" rowspan="2" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font>
</td>
<td width="240" bgcolor="#DFFFDC" align="center" colspan="3" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新</font>
</td>
</tr>


<tr>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全て</font>
</td>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当範囲</font>
</td>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不可</font>
</td>
</tr>


<?
for($i=0; $i<count($arr_inci); $i++)
{
	if($arr_inci[$i]["auth"] == 'GU')
	{
		continue;
	}
	$setting_update_name = "setting_update_".$arr_inci[$i]["auth"];

?>
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_inci[$i]["auth_name"]?></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="setting_update_<?=$arr_inci[$i]["auth"]?>_0" name="setting_update_<?=$arr_inci[$i]["auth"]?>" value="0" <?   if($$setting_update_name == "0"){?>checked<?}   ?> onclick="ref_update_changed(this,'<?=$arr_inci[$i]["auth"]?>')"></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="setting_update_<?=$arr_inci[$i]["auth"]?>_1" name="setting_update_<?=$arr_inci[$i]["auth"]?>" value="1" <?   if($$setting_update_name == "1"){?>checked<?}   ?> onclick="ref_update_changed(this,'<?=$arr_inci[$i]["auth"]?>')"></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="setting_update_<?=$arr_inci[$i]["auth"]?>_non" name="setting_update_<?=$arr_inci[$i]["auth"]?>" value="non" <?   if($$setting_update_name == "non"){?>checked<?}   ?> onclick="ref_update_changed(this,'<?=$arr_inci[$i]["auth"]?>')"></font>
</td>

</tr>
<?
}
?>
<tr>
<td bgcolor="#FFFFFF" align="left" colspan="4" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<input type="checkbox" name="setting_torey_update_flg" value="t" <?=$checked?>>
    送受信トレイの編集権限は、報告ファイルの更新権限に準ずる。
	</font>
</td>
</tr>

<tr>
<td bgcolor="#FFFFFF" align="left" colspan="4" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	担当範囲をチェックした場合は、担当者設定の条件に準じます。<br>
	更新権限がある場合でも参照権限がなければ報告書を開けません。
	</font>
</td>
</tr>

</table>
<!-- 更新 END -->

<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>


<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td align="right">
<input type="button" value="登録" onclick="update();">
</td>
</tr>
</table>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>

</form>
</body>
</html>
<?php
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>