<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("about_postgres.php"); ?>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<?
$fname = $PHP_SELF;

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$ward = check_authority($session,11,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
?>
<?
$reservation_title = trim($reservation_title);
$remarks = trim($remarks);

if(!$reservation_title){
echo("<script language='javascript'>alert(\"内容を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen(reservation_title) > 100){
echo("<script language='javascript'>alert(\"内容を50文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($remarks) > 500){
echo("<script language='javascript'>alert(\"備考を250文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$reservation_begin = "$reservation_begin1"."$reservation_begin2"."$reservation_begin3"."$reservation_begin4"."$reservation_begin5";
$reservation_end = "$reservation_end1"."$reservation_end2"."$reservation_end3"."$reservation_end4"."$reservation_end5";

if($reservation_begin >= $reservation_end){
echo("<script language='javascript'>alert(\"正しい開始日・終了日を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if($reservation_begin1 != $reservation_end1 || $reservation_begin2 != $reservation_end2 ||  $reservation_begin3 != $reservation_end3){
echo("<script language='javascript'>alert(\"日をまたいで入力することはできません。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$con = connect2db($fname);

$ymd=date(Ymd);
if($reservation_begin1.$reservation_begin2.$reservation_begin3==$ymd){
$select_reservation1="select * from reservation where facility_id='$facility_id' and reservation_begin=$reservation_begin";
$result_reservation1=pg_exec($con,$select_reservation1);
if($result_reservation1==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$num1=pg_numrows($result_reservation1);
if($num1>0){
echo("<script language='javascript'>alert(\"以下の施設・設備はすでに予約されています。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$select_reservation2="select * from reservation where facility_id='$facility_id' and reservation_begin<$reservation_begin and reservation_end>$reservation_begin";
$result_reservation2=pg_exec($con,$select_reservation2);
if($result_reservation2==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$num2=pg_numrows($result_reservation2);
if($num2>0){
echo("<script language='javascript'>alert(\"以下の施設・設備はすでに予約されています。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$select_reservation3="select * from reservation where facility_id='$facility_id' and reservation_begin>$reservation_begin and reservation_begin<$reservation_end";
$result_reservation3=pg_exec($con,$select_reservation3);
if($result_reservation3==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$num3=pg_numrows($result_reservation3);
if($num3>0){
echo("<script language='javascript'>alert(\"以下の施設・設備はすでに予約されています。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}
}

$date = date(YmdHi);

if($reservation_begin < $date){
echo("<script language='javascript'>alert(\"過去の施設・設備予約はできません。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$select_id = "select emp_id from login where emp_id in (select emp_id from session where session_id = '$session')";
//echo($select_id);
$result_select = pg_exec($con, $select_id);
if($result_select == false){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_result($result_select,0,"emp_id");


$select_max = "select max(reservation_id) from reservation";
$result_max = pg_exec($con,$select_max);
$count = pg_numrows($result_max);
if($count > 0){
	$reservation_id = pg_result($result_max,0,"max");
	$reservation_id = $reservation_id + 1;
}else{
	$reservation_id = "1";
}

$insert_reservation = "insert into reservation (emp_id, reservation_id, date, facility_id, reservation_begin, reservation_end, reservation_title, remarks, reservation_del_flg) values ('$emp_id', '$reservation_id', '$date','$facility_id', '$reservation_begin', '$reservation_end', '$reservation_title', '$remarks', 'f')";
//echo($insert_reservation);
$result_insert = pg_exec($con,$insert_reservation);
pg_close($con);
if($result_insert == true){
   echo("<script language='javascript'>window.opener.location.reload();</script>");	
	echo("<script language='javascript'>self.close();</script>");
	exit;
}else{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
?>