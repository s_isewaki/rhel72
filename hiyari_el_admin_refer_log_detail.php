<?
//メモ：この画面は管理画面以外に、通常画面でも管理者権限がある場合に呼び出されるケースがある。

require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
if (check_authority($session, 48, $fname) == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


// デフォルト値の設定
if ($page == "") {$page = 1;}  // ページ数：1


// コンテンツ名を取得
$sql = "select lib_nm from el_info";
$cond = "where lib_id = $lib_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lib_nm = pg_fetch_result($sel, 0, "lib_nm");



//画面名
$PAGE_TITLE = "参照履歴詳細";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.list td td {border-width:0;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5" align="center">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=$lib_nm?></b></font></td>
</tr>
</table>
<?
show_library_list($con, $lib_id, $page, $session, $fname, $list_order);
?>


		</td>
		<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/r_3.gif" width="10" height="10"></td>
		<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td><img src="img/r_4.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- 本体 END -->


</td>
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
<?



//参照ログの詳細一覧を表示します。
function show_library_list($con, $lib_id, $page, $session, $fname,$list_order="ref_date_desc")
{
	// 表示限界ログ数
	$limit = 20;

	// ログ数を取得（ページング処理のため）
	$sql = "select count(*) from el_reflog";
	$cond = "where lib_id = $lib_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$log_count = intval(pg_fetch_result($sel, 0, 0));
	if ($log_count == 0)
	{
		return;
	}

	// ログ情報の取得
	$sql  = " select * from";
	$sql .= " (select ref_date,emp_id from el_reflog where lib_id = $lib_id) reflog_list";
	$sql .= " natural inner join";
	$sql .= " (select emp_id, emp_lt_nm, emp_ft_nm, emp_class as class_id, emp_attribute as atrb_id, emp_dept as dept_id, emp_room as room_id from empmst) emp_list";
	$sql .= " natural left join";
	$sql .= " (select class_id,class_nm,order_no as c_order_no from classmst where not class_del_flg) class_list";
	$sql .= " natural left join";
	$sql .= " (select atrb_id,atrb_nm,order_no as a_order_no from atrbmst where not atrb_del_flg) atrb_list";
	$sql .= " natural left join";
	$sql .= " (select dept_id,dept_nm,order_no as d_order_no from deptmst where not dept_del_flg) dept_list";
	$sql .= " natural left join";
	$sql .= " (select room_id,room_nm,order_no as r_order_no from classroom where not room_del_flg) room_list";
	
	$cond = "";
	switch($list_order)
	{
		case "emp_name":
			$cond = " order by emp_lt_nm,emp_ft_nm,ref_date";
			break;
		case "emp_name_desc":
			$cond = " order by emp_lt_nm desc,emp_ft_nm desc,ref_date";
			break;
		case "emp_class":
			$cond = " order by c_order_no,a_order_no,d_order_no,r_order_no,emp_lt_nm,emp_ft_nm,ref_date";
			break;
		case "emp_class_desc":
			$cond = " order by c_order_no desc,a_order_no desc,d_order_no desc,r_order_no desc,emp_lt_nm,emp_ft_nm,ref_date";
			break;
		case "ref_date":
			$cond = " order by ref_date";
			break;
		case "ref_date_desc":
		default://case "":
			$cond = " order by ref_date desc";
			$list_order = "ref_date_desc";
			break;
	}

	$offset = $limit * ($page - 1);
	$cond .= " offset $offset limit $limit";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// ページ番号の表示
	if ($log_count > $limit)
	{
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:5px 0 2px;\">\n");
		echo("<tr>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");

		if ($page > 1)
		{
			echo("<a href=\"$fname?session=$session&lib_id=$lib_id&page=" . ($page - 1) . "\">＜前</a>　\n");
		}
		else
		{
			echo("＜前　\n");
		}

		$page_count = ceil($log_count / $limit);
		for ($i = 1; $i <= $page_count; $i++)
		{
			if ($i != $page)
			{
				echo("<a href=\"$fname?session=$session&lib_id=$lib_id&page=$i\">$i</a>\n");
			}
			else
			{
				echo("<b>$i</b>\n");
			}
		}

		if ($page < $page_count)
		{
			echo("　<a href=\"$fname?session=$session&lib_id=$lib_id&page=" . ($page + 1) . "\">次＞</a>\n");
		}
		else
		{
			echo("　次＞\n");
		}

		echo("</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
	}
	else
	{
		echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
	}
	
	//ソート情報
	$list_order_change1 = "ref_date_desc";
	$list_order_gif1 = 'img/down.gif';
	$list_order_change2 = "emp_class_desc";
	$list_order_gif2 = 'img/down.gif';
	$list_order_change3 = "emp_name_desc";
	$list_order_gif3 = 'img/down.gif';
	if($list_order == "ref_date_desc")
	{
		$list_order_change1 = "ref_date";
		$list_order_gif1 = 'img/up.gif';
	}
	if($list_order == "emp_class_desc")
	{
		$list_order_change2 = "emp_class";
		$list_order_gif2 = 'img/up.gif';
	}
	if($list_order == "emp_name_desc")
	{
		$list_order_change3 = "emp_name";
		$list_order_gif3 = 'img/up.gif';
	}
	
	// ログ一覧の表示
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22" bgcolor="#DFFFDC">
	<td width="30%">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="<?=$fname?>?session=<?=$session?>&lib_id=<?=$lib_id?>&list_order=<?=$list_order_change1?>">
		<img src="<?=$list_order_gif1?>" alt="" width="17" height="17" border="0" style="vertical-align:middle;">
		参照日時
		</a>
		</font>
		</td>
	<td width="40%">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="<?=$fname?>?session=<?=$session?>&lib_id=<?=$lib_id?>&list_order=<?=$list_order_change2?>">
		<img src="<?=$list_order_gif2?>" alt="" width="17" height="17" border="0" style="vertical-align:middle;">
		部署
		</a>
		</font>
		</td>
	<td width="30%" style="padding-right:4px;">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="<?=$fname?>?session=<?=$session?>&lib_id=<?=$lib_id?>&list_order=<?=$list_order_change3?>">
		<img src="<?=$list_order_gif3?>" alt="" width="17" height="17" border="0" style="vertical-align:middle;">
		参照者
		</a>
		</font>
		</td>
	</tr>
	<?
	while ($row = pg_fetch_array($sel))
	{
		$tmp_ref_date = format_datetime($row["ref_date"]);
		$tmp_emp_nm   = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

		//部署名(部署表示設定に依存)
		$tmp_classes_nm = "";
		$profile_use = get_inci_profile($fname, $con);
		if($profile_use["class_flg"] == "t")
		{
			$tmp_classes_nm .= $row["class_nm"];
		}
		if($profile_use["attribute_flg"] == "t")
		{
			$tmp_classes_nm .= $row["atrb_nm"];
		}
		if($profile_use["dept_flg"] == "t")
		{
			$tmp_classes_nm .= $row["dept_nm"];
		}
		if($profile_use["room_flg"] == "t")
		{
			$tmp_classes_nm .= $row["room_nm"];
		}
		$tmp_classes_nm = h($tmp_classes_nm);



		echo("<tr height=\"22\" bgcolor=\"#FFFFFF\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_ref_date</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_classes_nm</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_emp_nm</font></td>\n");
		echo("</tr>\n");
	}
	echo("</table>\n");
}

//参照日時を表示用のフォーマットに変換します。
function format_datetime($datetime)
{
	return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3　$4:$5", $datetime);
}
?>
