<?php

ob_start();

require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("./conf/sql.inf");
require_once("show_class_name.ini");
require_once("fplus_common.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,79,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);


if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
}
else {
	$encoding = mb_http_output();   // Firefox
}

$stamp = time();

$date = date('YmdHis', $stamp);


$fileName = "MRSA報告一覧_".$date.".xls";

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
} else {
	$encoding = mb_http_output();   // Firefox
}
$fileName = mb_convert_encoding(
		$fileName,
		$encoding,
		mb_internal_encoding());

ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');


$s_date = sprintf("%04d/%02d/%02d",$sel_d1_y,$sel_d1_m,$sel_d1_d); // 開始日
$e_date = sprintf("%04d/%02d/%02d",$sel_d2_y,$sel_d2_m,$sel_d2_d); // 終了日


$download_data = setData($fname,$con,$s_date,$e_date);

echo mb_convert_encoding(
		nl2br($download_data),
		'sjis',
		mb_internal_encoding()
		);

ob_end_flush();

pg_close($con); 


/**
 * /エクセル出力用データセット
 *
 * @param mixed $fname 画面名
 * @param mixed $con 接続情報
 * @param mixed $s_date 開始年月日
 * @param mixed $e_date 終了年月日
 * @return mixed This is the return value description
 *
 */
function setData
($fname,$con,$s_date,$e_date)
{
	require_once("l4pWClass.php");
	$lfp = new l4pWClass("FPLUS");
	
	
	
	
	$sql = "select 
			bbb.item_name as ward, data_1_7_ward, 
			ccc.item_name as depart, data_1_6_depart,
			data_1_1_pid, data_1_3_name, data_1_51_sex, data_1_4_years,data_1_8_hospital, data_4_1_mrsa, data_1_14_pick_day,
			ddd.record_caption as specimen, data_2_1_specimen, 
			eee.record_caption as pcg, data_3_1_pcg, 
			fff.record_caption as mpipc, data_3_2_mpipc, 
			ggg.record_caption as cez, data_3_3_cez, 
			hhh.record_caption as ctm, data_3_4_ctm, 
			iii.record_caption as gm, data_3_5_gm, 
			jjj.record_caption as em, data_3_6_em, 
			kkk.record_caption as mino, data_3_7_mino, 
			lll.record_caption as ipm, data_3_8_ipm, 
			mmm.record_caption as cldm, data_3_9_cldm, 
			nnn.record_caption as vcm, data_3_10_vcm, 
			ooo.record_caption as lvfx, data_3_11_lvfx, 
			ppp.record_caption as teic, data_3_12_teic, 
			qqq.record_caption as etc, data_3_13_etc, 
			sss.record_caption as disease, data_1_9_base_disease, 
			ttt.record_caption as infectious, data_4_2_infectious, 
			uuu.record_caption as kroot, data_4_5_root, 
			vvv.record_caption as antimicrobial, data_4_7_antimicrobial, 
			www.record_caption as immunity, data_4_9_immunity, 
			xxx.record_caption as kcontrol, data_4_10_control, 
			rrr.item_name as germ, data_1_13_find_germ, 
			yyy.item_name as diagnosis, data_4_3_diagnosis 
			from fplus_infection_mrsa aaa
			inner join fplusapply on ( fplusapply.apply_id = aaa.apply_id and fplusapply.delete_flg = 'f' and fplusapply.apply_stat in ('0','1') and fplusapply.draft_flg = 'f' )
			left join tmplitem bbb on (bbb.mst_cd='A404' and bbb.item_cd=aaa.data_1_7_ward) 
			left join tmplitem ccc on (ccc.mst_cd='A409' and ccc.item_cd=aaa.data_1_6_depart)
			left join fplus_001_master ddd on (ddd.field_name='data_2_1_specimen' and ddd.record_id=aaa.data_2_1_specimen)
			left join fplus_001_master eee on (eee.field_name='data_3_1_pcg' and eee.record_id=aaa.data_3_1_pcg)
			left join fplus_001_master fff on (fff.field_name='data_3_2_mpipc' and fff.record_id=aaa.data_3_2_mpipc)
			left join fplus_001_master ggg on (ggg.field_name='data_3_3_cez' and ggg.record_id=aaa.data_3_3_cez)
			left join fplus_001_master hhh on (hhh.field_name='data_3_4_ctm' and hhh.record_id=aaa.data_3_4_ctm)
			left join fplus_001_master iii on (iii.field_name='data_3_5_gm' and iii.record_id=aaa.data_3_5_gm)
			left join fplus_001_master jjj on (jjj.field_name='data_3_6_em' and jjj.record_id=aaa.data_3_6_em)
			left join fplus_001_master kkk on (kkk.field_name='data_3_7_mino' and kkk.record_id=aaa.data_3_7_mino)
			left join fplus_001_master lll on (lll.field_name='data_3_8_ipm' and lll.record_id=aaa.data_3_8_ipm)
			left join fplus_001_master mmm on (mmm.field_name='data_3_9_cldm' and mmm.record_id=aaa.data_3_9_cldm)
			left join fplus_001_master nnn on (nnn.field_name='data_3_10_vcm' and nnn.record_id=aaa.data_3_10_vcm)
			left join fplus_001_master ooo on (ooo.field_name='data_3_11_lvfx' and ooo.record_id=aaa.data_3_11_lvfx)
			left join fplus_001_master ppp on (ppp.field_name='data_3_12_teic' and ppp.record_id=aaa.data_3_12_teic)
			left join fplus_001_master qqq on (qqq.field_name='data_3_13_etc' and qqq.record_id=aaa.data_3_13_etc)
			left join fplus_001_master sss on (sss.field_name='data_1_11_base_disease' and sss.record_id=aaa.data_1_9_base_disease)
			left join fplus_001_master ttt on (ttt.field_name='data_4_2_infectious' and ttt.record_id=aaa.data_4_2_infectious)
			left join fplus_001_master uuu on (uuu.field_name='data_4_5_root' and uuu.record_id=aaa.data_4_5_root)
			left join fplus_001_master vvv on (vvv.field_name='data_4_7_antimicrobial' and vvv.record_id=aaa.data_4_7_antimicrobial)
			left join fplus_001_master www on (www.field_name='data_4_9_immunity' and www.record_id=aaa.data_4_9_immunity)
			left join fplus_001_master xxx on (xxx.field_name='data_4_10_control' and xxx.record_id=aaa.data_4_10_control)
			left join tmplitem rrr on (rrr.mst_cd='A405' and rrr.item_cd=aaa.data_1_13_find_germ)
			left join tmplitem yyy on (yyy.mst_cd='A407' and yyy.item_cd=aaa.data_4_3_diagnosis)";

	$sql .= " where data_1_14_pick_day >= '".$s_date."'"; // 開始日
	$sql .= " and data_1_14_pick_day <= '".$e_date."'"; // 終了日
	
	$sql .= " order by data_1_13_find_germ, data_1_7_ward, data_1_6_depart";

	$lfp->debugRst("FPLUS","SQL:".$sql);

	
	$sel = @select_from_table($con, $sql, "", $fname);
	$row = @pg_fetch_all($sel);
	
	
	$str_disp = "期間：".$s_date."〜".$e_date;
	
	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
	
	$data .= "<table border=1 >";
	
	$data .= "<tr align=\"center\">";
	$data .= "<td align=\"left\" colspan=\"22\" ><font size=\"4\">".$str_disp."</font></td>";
	$data .= "</tr>";
	
	//ヘッダ行表示
	$data .= "<tr align=\"center\">";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">検出菌名/微生物名</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">病棟</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">診療科</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">患者ID</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">名前</font></td>";	
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">性別</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">入院日</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">検出検体</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">検体採取日</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">病原体の型(MRSAの場合)</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" colspan=\"13\"><font size=\"4\">検査結果</font></td>";
	
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">基礎疾患</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">感染/保菌状態</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">診断名</font></td>";
	
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">感染経路</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">一カ月以内投薬抗菌剤</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">患者の免疫能</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">免疫抑制剤使用の有無</font></td>";
	
	$data .= "</tr>";

	$tdset = "<td style=\"background-color:#ffdfff;\"><font size=\"4\">";

	//表示しやすいように取得データから配列を加工しました
	foreach($row as $val )
	{

		$data .= "</tr>";

		$data .= $tdset.$val["germ"]."</font></td>";
		$data .= $tdset.$val["ward"]."</font></td>";
		$data .= $tdset.$val["depart"]."</font></td>";
		$data .= $tdset.$val["data_1_1_pid"]."</font></td>";
		$data .= $tdset.$val["data_1_3_name"]."</font></td>";
		$data .= $tdset.$val["data_1_51_sex"]."</font></td>";
		$data .= $tdset.$val["data_1_4_years,data_1_8_hospital"]."</font></td>";
		$data .= $tdset.$val["specimen"]."</font></td>";
		$data .= $tdset.$val["data_1_14_pick_day"]."</font></td>";
		$data .= $tdset.$val["data_4_1_mrsa"]."</font></td>";
		
		$data .= $tdset.$val["pcg"]."</font></td>";
		$data .= $tdset.$val["mpipc"]."</font></td>";
		$data .= $tdset.$val["cez"]."</font></td>";
		$data .= $tdset.$val["ctm"]."</font></td>";
		$data .= $tdset.$val["gm"]."</font></td>";
		$data .= $tdset.$val["em"]."</font></td>";
		$data .= $tdset.$val["mino"]."</font></td>";
		$data .= $tdset.$val["ipm"]."</font></td>";
		$data .= $tdset.$val["cldm"]."</font></td>";
		$data .= $tdset.$val["vcm"]."</font></td>";
		$data .= $tdset.$val["lvfx"]."</font></td>";
		$data .= $tdset.$val["teic"]."</font></td>";
		$data .= $tdset.$val["etc"]."</font></td>";

		$data .= $tdset.$val["disease"]."</font></td>";
		$data .= $tdset.$val["infectious"]."</font></td>";
		$data .= $tdset.$val["diagnosis"]."</font></td>";		
		$data .= $tdset.$val["kroot"]."</font></td>";
		$data .= $tdset.$val["antimicrobial"]."</font></td>";
		$data .= $tdset.$val["immunity"]."</font></td>";
		$data .= $tdset.$val["kcontrol"]."</font></td>";
		

		$data .= "</tr>";
	}
	
	$data .= "</table>";
	
	return $data;	

	
}
?>
