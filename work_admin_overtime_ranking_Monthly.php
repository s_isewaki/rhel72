<?php
ob_start();
require_once("about_comedix.php");
require_once("show_timecard_common.ini");
require_once("show_attendance_pattern.ini");
require_once("holiday.php");
require_once("timecard_common_class.php");
require_once("date_utils.php");
require_once("atdbk_common_class.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("atdbk_info_common.php");
require_once("ovtm_class.php");
require_once("work_admin_timecard_common.php");
require_once("settings_common.php");
require_once("get_values.ini");
require_once("timecard_tantou_class.php");
require_once("atdbk_close_class.php");

define("SIXTY_TIME", 3600);

/**
 * 時間外統計資料　残業理由別CSVの生成と出力.
 *
 */
$fname = $PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
if ($mmode == "usr") {
	$emp_id = get_emp_id($con, $session, $fname);
	$timecard_tantou_class = new timecard_tantou_class($con, $fname);
	$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
}

$duty_type_flag = get_settings_value("employment_working_other_flg", "f"); // 雇用区分表示

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

//ワークフロー関連共通クラス
$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

//タイムカードデータ取得
$arr_legal_hol_cnt = array();
$arr_legal_hol_cnt_part = array();
$arr_holwk = get_timecard_holwk_day($con, $fname);
$legal_hol_cnt_flg = false;

//残業申請関連クラス
$ovtm_class = new ovtm_class($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

// 常勤/非常勤ごとの締め日情報を取得
$sql = "select closing, closing_parttime, closing_month_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0){
	$tmp_full = pg_fetch_result($sel, 0, "closing");
	$tmp_part = pg_fetch_result($sel, 0, "closing_parttime");
	$tmp_flag = pg_fetch_result($sel, 0, "closing_month_flg");
	//常勤
	$closing_full = (empty($tmp_full)) ? "6" : $tmp_full;
	//非常勤
	$closing_part = (empty($tmp_part)) ? $closing_full : $tmp_part;
	
	$closing_month_flg = (empty($tmp_flag)) ? "1" : $tmp_flag;
}else{
	$closing_full = "6";
	$closing_part = $closing_jyoukin; 
	$closing_month_flg = "1";
}

$tmp_yyyymm = $_POST["start_yr"] . $_POST["start_mon"];
$yyyymm = (empty($tmp_yyyymm)) ? date("Ym") : $tmp_yyyymm;
$overtime = ($_POST["base_one"]) ? intval($_POST["base_one"]) : 0; // 基準残業時間

/*
$full_time = getClosingDateFromTo($yyyymm, $closing_full, $closing_month_flg);
$part_time = getClosingDateFromTo($yyyymm, $closing_part, $closing_month_flg);
$search_start = min(intval($full_time["start_date"]), intval($part_time["start_date"]));
$search_end = max(intval($full_time["end_date"]), intval($part_time["end_date"]));
*/
$atdbk_close_class = new atdbk_close_class($con, $fname);
$arr_csv_closing_info = $atdbk_close_class->get_csv_closing_info();
$arr_closing = array();
$i=0;
$arr_closing[$i] = $closing_full;
$i++;
if ($closing_part != $closing_full) {
	$arr_closing[$i] = $closing_part;
	$i++;
}
foreach ($arr_csv_closing_info as $csv_id => $wk_closing) {
	if ($wk_closing != $closing_full) {
		$arr_closing[$i] = $wk_closing;
		$i++;
	}
}
$arr_term = $atdbk_close_class->get_term_multi_closing($yyyymm, $arr_closing, $closing_month_flg);
$search_start = $arr_term["start"];
$search_end = $arr_term["end"];

/*
 * 残業対象者情報を取得
 */
$sql  = "SELECT DISTINCT o.emp_id, f.emp_personal_id, f.emp_lt_nm, f.emp_ft_nm, ";
$sql .= "deptmst.link_key1, deptmst.dept_nm, ec.duty_form, ec.duty_form_type ";
$sql .= "FROM ovtmapply o ";
$sql .= "INNER JOIN  empmst f on o.emp_id = f.emp_id ";
$sql .= "LEFT JOIN deptmst ON f.emp_dept = deptmst.dept_id AND deptmst.dept_del_flg = 'f' ";
$sql .= "LEFT JOIN empcond ec ON f.emp_id = ec.emp_id ";
$sql .= "LEFT JOIN atdbkrslt r ON o.emp_id = r.emp_id AND o.target_date = CAST(r.date AS varchar) ";
$cond  = "WHERE o.target_date >= '$search_start' AND o.target_date <= '$search_end' AND o.apply_status = '1' AND o.delete_flg = 'f' ";
$cond .= "AND o.over_start_time <> '' AND o.over_end_time <> '' ";
//担当所属
if ($mmode == "usr") {
    $cond .= " and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond .= " or (";
        if ($r["class_id"]) $cond .= "     f.emp_class=".$r["class_id"];
        if ($r["atrb_id"])  $cond .= "     and f.emp_attribute=".$r["atrb_id"];
        if ($r["dept_id"])  $cond .= "     and f.emp_dept=".$r["dept_id"];
        if ($r["class_id"]) $cond .= " )";
    }
    //兼務
    $cond_ccr = " or exists (select * from concurrent where concurrent.emp_id = f.emp_id and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond_ccr .= " or (";
        if ($r["class_id"]) $cond_ccr .= "     concurrent.emp_class=".$r["class_id"];
        if ($r["atrb_id"])  $cond_ccr .= "     and concurrent.emp_attribute=".$r["atrb_id"];
        if ($r["dept_id"])  $cond_ccr .= "     and concurrent.emp_dept=".$r["dept_id"];
        if ($r["class_id"]) $cond_ccr .= " )";
    }
    $cond_ccr .= "))";
    $cond .= $cond_ccr;
    $cond .= ") ";
}
$cond .= "ORDER BY o.emp_id, f.emp_personal_id, f.emp_lt_nm, f.emp_ft_nm, deptmst.link_key1, deptmst.dept_nm, ec.duty_form, ec.duty_form_type ";
$sel = select_from_table($con, $sql, $cond, $fname);
if($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
while($row = pg_fetch_array($sel)){
	$emp_id = $row["emp_id"];
	
	$array_info[$emp_id]["id"] = $row["emp_personal_id"]; // 職員ID
	$array_info[$emp_id]["name"] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"] ; // 職員名
	$array_info[$emp_id]["code"] = $row["link_key1"]; // 所属コード
	$array_info[$emp_id]["dept_nm"] = $row["dept_nm"];  // 科
	$array_info[$emp_id]["duty"] = $row["duty_form"];  // 雇用勤務形態
	$array_info[$emp_id]["duty_type"] = $row["duty_form_type"]; // 雇用区分
}

$arr_duty = array("1"=>"常勤", "2"=>"非常勤", "3"=>"短時間正職員");

if ($duty_type_flag == "t"){
	$arr_duty_type = array("1"=>"正職員", "2"=>"再雇用職員", "3"=>"嘱託職員", "4"=>"臨時職員", "5"=>"パート職員");
}

// 職員別月別の勤務実績から時間外を集計
// 勤務形態別の開始日・終了日のセット
$ovtmRecord = array();
foreach ($array_info as $key_id=>$item) {
	/*
	if ($item["duty"] == "2") {
		// 非常勤
		$start_date = $part_time["start_date"];
		$end_date   = $part_time["end_date"];
	}else{
		// 常勤
		$start_date = $full_time["start_date"];
		$end_date   = $full_time["end_date"];
	}
	*/
	$wk_closing = $atdbk_close_class->get_empcond_closing($key_id);
	$arr_term = $atdbk_close_class->get_term($yyyymm, $wk_closing, $closing_month_flg);
	$start_date = $arr_term["start"];
	$end_date = $arr_term["end"];
	
	// 職員別タイムカード情報の取得
	$arr_atdbk_info = get_atdbk_info($con, $fname, $session, $key_id, 
									 $yyyymm, $atdbk_common_class, $start_date, $end_date, 
									 $atdbk_workflow_common_class, $timecard_bean, false, $arr_legal_hol_cnt, 
									 $arr_legal_hol_cnt_part, $arr_holwk, false, 0, $obj_hol_hour,
									 "", "", "", "", $calendar_name_class, true, "", "",$ovtm_class);
	
	// 指定した期間の就業時間の集計結果から編集
	$summary = $arr_atdbk_info["sums"];

	// 普通残業（深夜・休日残業除外）
	$week_over = ($summary["30"] == "")? "0:00" : $summary["30"]; //○

	// 平日時間外(深夜)
	$week_night_over = ($summary[15] == "")? "0:00" : $summary[15]; // 法定外深夜残業（休日除外）
	
	// 休日時間外
	$hol_over = ($summary[31] == "")? "0:00" : $summary[31]; // 法定外休日残業（深夜除外）
	
	// 休日時間外(深夜)
	$hol_night_over = ($summary[40] == "")? "0:00" : $summary[40]; // 法定外休日深夜残業
	
	// 平日時間外 + 休日時間外
	$count_over = hmm_to_minute($summary["30"]) + hmm_to_minute($summary["31"]);
	$sixty_over = $count_over - SIXTY_TIME;
	
	// 平日時間深夜 + 休日深夜
	$count_night_over = hmm_to_minute($summary["15"]) + hmm_to_minute($summary["40"]);
	$sixty_night_over = $count_night_over - SIXTY_TIME;
	
	// 時間外合計 
	$total_over = $count_over + $count_night_over; 
	
	if ($total_over >= ($overtime * 60)){
		$ovtmRecord[$key_id][] = $item["id"];
		$ovtmRecord[$key_id][] = $item["name"];
		$ovtmRecord[$key_id][] = $item["code"];
		$ovtmRecord[$key_id][] = $item["dept_nm"];
		$ovtmRecord[$key_id][] = $arr_duty[$item["duty"]];
		if ($duty_type_flag == "t"){
			$ovtmRecord[$key_id][] = $arr_duty_type[$item["duty_type"]];
		}
		$ovtmRecord[$key_id][] = $week_over;
		$ovtmRecord[$key_id][] = $week_night_over;
		$ovtmRecord[$key_id][] = $hol_over;
		$ovtmRecord[$key_id][] = $hol_night_over;
		
		$ovtmRecord[$key_id][] = ($sixty_over > 0) ? minute_to_hmm($sixty_over) : "0:00";
		$ovtmRecord[$key_id][] = ($sixty_night_over > 0) ? minute_to_hmm($sixty_night_over) : "0:00";
		$ovtmRecord[$key_id][] = $total_over;
	}
}

// 残業時間数ソート順
$key_total = array();
foreach ($ovtmRecord as $key=>$item){
	$key_total[$key] = $item[12];
}
array_multisort($key_total, SORT_DESC, $ovtmRecord);


// 情報をCSV形式で出力
$csv = get_list_csv($ovtmRecord, $yyyymm, $overtime, $duty_type_flag);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "OvertimeMonths" . "_" . $yyyymm . "_" . $overtime . "Hour.csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

/**
 * CSVデータの生成.
 * @param array $array_info 職員別月別残業時間集計結果(連想配列) 時間外合計で降順に並び替え済
 * @param string 集計対象期間 基準月
 * @param int $overtime 基準値
 * @return string $duty_type_flag　雇用形態区分フラグ
 * @return string CSV形式に整形された閾値以上の残業者リスト
 */
function get_list_csv($ovtmRecord, $yyyymm, $overtime, $duty_type_flag) {

	$titles = array(
			"職員ID",
			"氏名",
			"所属コード",
			"所属名",
			"雇用勤務形態",
			"雇用区分",
			"平日時間外",
			"平日時間外(深夜)",
			"休日時間外",
			"休日時間外(深夜)",
			"60時間超(法定外残業)",
			"60時間超(深夜残業)",
			"時間外合計"
	);
	if ($duty_type_flag != "t"){
		array_splice($titles, 5, 1);
	}

	$item_num = count($titles);
	$last_num = $item_num -1;
	$num =count($ovtmRecord);


	$buf = "";
	$buf .= $yyyymm . "," . $overtime;
	$buf .= "\r\n";

	// タイトル
	for ($j=0;$j<$item_num;$j++) {
		if ($j != 0) {
			$buf .= ",";
		}
		$buf .= $titles[$j];
	}
	$buf .= "\r\n";

	// 内容
	foreach ($ovtmRecord as $key=>$info){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$buf .= ($last_num == $j) ?  minute_to_hmm($ovtmRecord[$key][$j]) : $ovtmRecord[$key][$j];
		}
		$buf .= "\r\n";
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}

/**
 * 締め日を考慮した月度の開始日、終了日の算出.
 * @param String $yyyymm
 * @param String $closing
 * @param String $closing_month_flg
 * @return array:string 月度に応じた月の開始日, 終了日
 */
function getClosingDateFromTo($yyyymm, $closing, $closing_month_flg){

	$arr_date = array();
	
	// 締め日が登録済みの場合
	if ($closing != "") {

		if ($yyyymm != "") {
			$year = substr($yyyymm, 0, 4);
			$month = intval(substr($yyyymm, 4, 2));
			$month_set_flg = true; //月が指定された場合
		} else {
			$year = date("Y");
			$month = date("n");
			$yyyymm = $year . date("m");
			$month_set_flg = false; //月が未指定の場合
		}

		// 開始日を算出
		$calced = false;
		switch ($closing) {
			case "1":  // 1日
				$closing_day = 1;
				break;
			case "2":  // 5日
				$closing_day = 5;
				break;
			case "3":  // 10日
				$closing_day = 10;
				break;
			case "4":  // 15日
				$closing_day = 15;
				break;
			case "5":  // 20日
				$closing_day = 20;
				break;
			case "6":  // 末日
				$start_year = $year;
				$start_month = $month;
				$start_day = 1;
				$end_year = $start_year;
				$end_month = $start_month;
				$end_day = days_in_month($end_year, $end_month);
				$calced = true;
				break;
		}
		if (!$calced) {
			$day = date("j");
			$start_day = $closing_day + 1;
			$end_day = $closing_day;

			//月が指定された場合
			if ($month_set_flg) {
				if ($closing_month_flg != "2") {
					$start_year = $year;
					$start_month = $month;

					$end_year = $year;
					$end_month = $month + 1;
					if ($end_month == 13) {
						$end_year++;
						$end_month = 1;
					}
				}
				else {
					$start_year = $year;
					$start_month = $month - 1;
					if ($start_month == 0) {
						$start_year--;
						$start_month = 12;
					}

					$end_year = $year;
					$end_month = $month;
				}

			} else {
				if ($day <= $closing_day) {
					$start_year = $year;
					$start_month = $month - 1;
					if ($start_month == 0) {
						$start_year--;
						$start_month = 12;
					}

					$end_year = $year;
					$end_month = $month;
				} else {
					$start_year = $year;
					$start_month = $month;

					$end_year = $year;
					$end_month = $month + 1;
					if ($end_month == 13) {
						$end_year++;
						$end_month = 1;
					}
				}
			}
		}
		$arr_date["start_date"] = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
		$arr_date["end_date"] = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
	}
	return $arr_date;
}
?>
