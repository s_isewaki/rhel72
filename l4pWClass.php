<?

class l4pWClass
{

	/** ロガーオブジェクト */
	var $log;

	/** ログ出力設定 */

	/** ウェブメール */
	var $WEBMAIL;

	/** スケジュール */
	var $SCHEDULE;

	/** 委員会・ＷＧ */
	var $PROJECT;

	/** お知らせ・回覧板 */
	var $NEWS;

	/** タスク */
	var $TASK;

	/** 伝言メモ */
	var $MESSAGE;

	/** 設備予約 */
	var $FACILITY;

	/** 出勤表 */
	var $TIMECARD;

	/** 決裁・申請 */
	var $WORKFLOW;

	/** ネットカンファレンス */
	var $NETCF;

	/** 掲示板・電子会議室 */
	var $BBS;

	/** Ｑ＆Ａ */
	var $QA;

	/** 文書管理 */
	var $LIBRARY;

	/** アドレス帳 */
	var $ADDRESS;

	/** 内線電話帳 */
	var $EXT;

	/** リンクライブラリ */
	var $LINK;

	/** パスワード・本人情報 */
	var $PASSWD;

	/** コミュニティサイト */
	var $COMM;

	/** 備忘録 */
	var $MEMO;

	/** 院内行事 */
	var $EVENT;

	/** 職員登録 */
	var $EMPLOYEE;

	/** マスターメンテナンス */
	var $MASTER;

	/** 環境設定 */
	var $CONFIG;

	/** ライセンス管理 */
	var $LICENSE;

	/** 患者管理 */
	var $PATIENT;

	/** 検索ちゃん */
	var $KENSAKU;

	/** 看護支援 */
	var $OUTPT;

	/** 病床管理 */
	var $BED;

	/** 診療科 */
	var $HPDEPT;

	/** 看護観察記録 */
	var $NURSE_REC;

	/** ファントルくん */
	var $FANTOL;

	/** ファントルくん＋ */
	var $FPLUS;

	/** バリテス */
	var $E_LEARN;

	/** メドレポート */
	var $SUMMARY;

	/** カルテビュー */
	var $KVIEW;

	/** 経営支援 */
	var $BIZ;

	/** 勤務シフト */
	var $SHIFT;

	/** ＣＡＳ */
	var $CAS;

	/** 人事管理 */
	var $PERSONNEL;

	/** 日報・月報 */
	var $JOURNAL;

	/** イントラネット */
	var $INTRA;

	/** データベース */
	var $SQL;

	/** 共通 */
	var $COMMON;

	/** スタッフ・ポートフォリオ */
	var $SPF;



    /**
     * コンストラクタ
     * @param string $function_id 機能ＩＤ
     */
	function l4pWClass($function_id="COMMON")
	{

		//ログ設定
		$str_docRoot = getcwd();
		define('LOG4PHP_DIR', $str_docRoot.'/libs/log4php');

		//→デフォルトのプロパティファイルを使用する
		define('LOG4PHP_CONFIGURATION', $str_docRoot . '/conf/CMX.properties');

		require_once( LOG4PHP_DIR . '/LoggerManager.php' );

		$this->log =& LoggerManager::getLogger();

		// データベースに接続
		require_once('Cmx/Model/SystemConfig.php');

		// ログ用system_config
		$conf = new Cmx_SystemConfig();

		$this->$function_id = $conf->get("l4p.".$function_id);

		if($this->$function_id == null)
		{
			$this->log->info("UNDEFINED FUNCTION:".$function_id);
		}

		//一カ月前のログは削除
		$this->l4p_logrotate($str_docRoot);

	}


	function l4p_logrotate($str_docRoot)
	{
		//20120501_CMX.log

		$old_path = $str_docRoot . '/log/' . date('Ymd', strtotime("-30 days")) . '_CMX.log';
		$paths = glob($str_docRoot . '/log/*_CMX.log');
		foreach ($paths as $tmp_path)
		{
			if ($tmp_path <= $old_path)
			{
				unlink($tmp_path);
				//if (DEBUG)
				//{
				//	echo('REMOVE: ' . $tmp_path . "\n");
				//} else {
				//	unlink($tmp_path);
				//}
			}
		}
	}

	/**
	 * infoメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function infoRst($function_id,$msg)
	{

		//救済措置ー＞ファンクションＩＤを指定し忘れた場合に引数がずれるので、$msgへ第一引数から再代入する
		if($msg == null)
		{
			$msg = $function_id;
			$function_id = "";
		}

		$data = debug_backtrace();
		$file = $data[0]["file"];
		$line = $data[0]["line"];

		if(1< count($data))
		{
			//呼び元が関数内から呼ばれている
			$func = $data[1]["function"];
		}
		else
		{
			//呼び元が関数内から呼ばれていない
			$func = "";
		}

		if($this->$function_id === "1" || $this->$function_id === "2")
		{
			//ログレベルを表示させる
			$mode="INFO";

			$msg=$this->editmsg($mode,$msg,$file,$line,$func,$function_id);
			$this->log->info($msg);
		}
	}


	/**
	 * debugメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function debugRst($function_id,$msg)
	{
		//救済措置ー＞ファンクションＩＤを指定し忘れた場合に引数がずれるので、$msgへ第一引数から再代入する
		if($msg == null)
		{
			$msg = $function_id;
			$function_id = "";
		}

		$data = debug_backtrace();
		$file = $data[0]["file"];
		$line = $data[0]["line"];

		if(1< count($data))
		{
			//呼び元が関数内から呼ばれている
			$func = $data[1]["function"];
		}
		else
		{
			//呼び元が関数内から呼ばれていない
			$func = "";
		}

		if($this->$function_id === "2")
		{
			//ログレベルを表示させる
			$mode="DEBUG";

			$msg=$this->editmsg($mode,$msg,$file,$line,$func,$function_id);
			$this->log->info($msg);
		}
	}


	/**
	 * errorメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function errorRst($function_id,$msg)
	{
		//救済措置ー＞ファンクションＩＤを指定し忘れた場合に引数がずれるので、$msgへ第一引数から再代入する
		if($msg == null)
		{
			$msg = $function_id;
			$function_id = "";
		}

		$data = debug_backtrace();
		$file = $data[0]["file"];
		$line = $data[0]["line"];

		if(1< count($data))
		{
			//呼び元が関数内から呼ばれている
			$func = $data[1]["function"];
		}
		else
		{
			//呼び元が関数内から呼ばれていない
			$func = "";
		}

		//ログレベルを表示させる
		$mode="ERROR";

		$msg=$this->editmsg($mode,$msg,$file,$line,$func,$function_id);
		$this->log->info($msg);
	}
	/**
	 * warnメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function warnRst($function_id,$msg)
	{
		//救済措置ー＞ファンクションＩＤを指定し忘れた場合に引数がずれるので、$msgへ第一引数から再代入する
		if($msg == null)
		{
			$msg = $function_id;
			$function_id = "";
		}

		$data = debug_backtrace();
		$file = $data[0]["file"];
		$line = $data[0]["line"];

		if(1< count($data))
		{
			//呼び元が関数内から呼ばれている
			$func = $data[1]["function"];
		}
		else
		{
			//呼び元が関数内から呼ばれていない
			$func = "";
		}

		//ログレベルを表示させる
		$mode="WARN";

		$msg=$this->editmsg($mode,$msg,$file,$line,$func,$function_id);
		$this->log->info($msg);
	}
	/**
	 * fatalメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function fatalRst($function_id,$msg)
	{

		//救済措置ー＞ファンクションＩＤを指定し忘れた場合に引数がずれるので、$msgへ第一引数から再代入する
		if($msg == null)
		{
			$msg = $function_id;
			$function_id = "";
		}

		$data = debug_backtrace();
		$file = $data[0]["file"];
		$line = $data[0]["line"];

		if(1< count($data))
		{
			//呼び元が関数内から呼ばれている
			$func = $data[1]["function"];
		}
		else
		{
			//呼び元が関数内から呼ばれていない
			$func = "";
		}

		//ログレベルを表示させる
		$mode="FATAL";

		$msg=$this->editmsg($mode,$msg,$file,$line,$func,$function_id);
		$this->log->info($msg);
	}

	/**
	 * メッセージ編集
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function editmsg($mode,$msg,$file=null,$line=null,$function=null,$function_id=null)
	{
		$prestr="";

		if($mode!=null){
			$prestr=$prestr."[".$mode."]";
		}
		if($function_id!=null){
			$prestr=$prestr."[".$function_id."]";
		}
		if($this->without_dir){
			$file=basename($file);
		}
		if($file!=null){
			$prestr=$prestr."[".$file."]";
		}
		if($line!=null){
			$prestr=$prestr."[".$line."]";
		}
		if($function!=null){
			$prestr=$prestr."[".$function."]";
		}

		return $prestr.$msg;
	}

	/**
	 * ロガーシャットダウン
	 */
	function shutdown()
	{

		/**
		* シャットダウン
		*/
		LoggerManager::shutdown();
	}
}



?>