<?
//------------------------------------------------------------------------------
// Title     :  ネット会議終了処理
// File Name :  net_conference_end.php
// History   :  2004/07/16 - リリース（岩本隆史）
// Copyright :
//------------------------------------------------------------------------------

require("about_session.php");
require("about_authority.php");

//==============================================================================
// 共通処理
//==============================================================================

$fname = $PHP_SELF;  // スクリプトパス

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ネット会議権限のチェック
$auth_nc = check_authority($session, 10, $fname);
if ($auth_nc == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================================================================
// 削除処理
//==============================================================================

// データベースに接続
$con = connect2db($fname);

// ネット会議終了SQLの実行
$sql = "update nmrmmst set";
$set = array("nmrm_end_flg");
$setvalue = array("t");
$cond = "where nmrm_id = $nmrm_id";
$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);

// データベース接続をクローズ
pg_close($con);

// SQL実行失敗ならエラー画面に、成功ならネット会議一覧画面に遷移
if ($up == 0) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
} else {
	echo("<script language='javascript'>location.href = 'net_conference_menu.php?session=$session';</script>");
	exit;
}
?>
