<?php
require_once('about_comedix.php');
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("aclg_set.php");

// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

//====================================
// 登録ボタン押下時DB更新
//====================================

if($postback_mode == "send_change")
{
	// トランザクションの開始
	pg_query($con, "begin transaction");

	//進捗管理対象の設定保存
	$arr_auths = split(",", $auths);
	foreach($arr_auths as $auth) {
		regist_dflt_flg($con, $fname, $auth, $$auth);
	}
	
	//確認コメントの利用設定保存
	$is_progress_comment_use = $progress_comment_use == 't';
	set_progress_comment_use($con,$fname,$is_progress_comment_use);
	

	//確認コメントの印刷設定保存
	$is_progress_comment_print = $progress_comment_print == 't';
	set_progress_comment_print($con,$fname,$is_progress_comment_print);
	
	//進捗登録調整の設定保存
	$is_progress_adjust = $progress_adjust == 't';
	set_progress_adjust($con,$fname,$is_progress_adjust);
    
    //SM確認時にロックの設定保存
    $is_progress_auto_lock = $progress_auto_lock == 't';
    set_progress_auto_lock($con, $fname, $is_progress_auto_lock);
    
    //他の部署の報告書を評価可能にする設定を保存
    //$is_progress_eval == 't';
    $is_progress_eval = $other_progress_eval == 't';
    //set_progress_eval($con, $fname, $is_progress_eval);
    $sysConf->set('fantol.evaluation.common.use.flg', $is_progress_eval == 't' ? 't' : 'f');
    
    //評価機能利用フラグ
    $sysConf->set('fantol.evaluation.use.sm.flg', $evaluation_use_SM == 't' ? 't' : 'f');
    $sysConf->set('fantol.evaluation.use.rm.flg', $evaluation_use_RM == 't' ? 't' : 'f');
    $sysConf->set('fantol.evaluation.use.ra.flg', $evaluation_use_RA == 't' ? 't' : 'f');
    $sysConf->set('fantol.evaluation.use.sd.flg', $evaluation_use_SD == 't' ? 't' : 'f');
    $sysConf->set('fantol.evaluation.use.md.flg', $evaluation_use_MD == 't' ? 't' : 'f');
    $sysConf->set('fantol.evaluation.use.hd.flg', $evaluation_use_HD == 't' ? 't' : 'f');
    
    
    
    //山本@静岡県立総合 20150730

    $sysConf->set('fantol.progress.sm.uketsuke.flg', $p_uketsuke_flg_SM == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.rm.uketsuke.flg', $p_uketsuke_flg_RM == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.ra.uketsuke.flg', $p_uketsuke_flg_RA == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.sd.uketsuke.flg', $p_uketsuke_flg_SD == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.md.uketsuke.flg', $p_uketsuke_flg_MD == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.hd.uketsuke.flg', $p_uketsuke_flg_HD == 't' ? 't' : 'f');
    
    
    
    
    $sysConf->set('fantol.progress.sm.confirm.flg', $p_confirm_flg_SM == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.rm.confirm.flg', $p_confirm_flg_RM == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.ra.confirm.flg', $p_confirm_flg_RA == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.sd.confirm.flg', $p_confirm_flg_SD == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.md.confirm.flg', $p_confirm_flg_MD == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.hd.confirm.flg', $p_confirm_flg_HD == 't' ? 't' : 'f');
    
    
    
    $sysConf->set('fantol.progress.sm.eval.flg', $p_eval_flg_SM == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.rm.eval.flg', $p_eval_flg_RM == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.ra.eval.flg', $p_eval_flg_RA == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.sd.eval.flg', $p_eval_flg_SD == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.md.eval.flg', $p_eval_flg_MD == 't' ? 't' : 'f');
    $sysConf->set('fantol.progress.hd.eval.flg', $p_eval_flg_HD == 't' ? 't' : 'f');
    
    
    
    
    

	// トランザクションをコミット
	pg_query($con, "commit");
}

//====================================
// 初期化処理
//====================================
$arr_inci_progress = search_inci_report_progress_dflt_flg($con, $fname);
$is_progress_comment_use = get_progress_comment_use($con,$fname);

//印刷設定を取得する(print_flg)
$is_progress_comment_print = get_progress_comment_print($con,$fname);

// 進捗登録の調整設定
$is_progress_adjust = get_progress_adjust($con, $fname);

//SM確認時にロックの設定
$is_progress_auto_lock = get_progress_auto_lock($con, $fname);
$arr_inci = get_inci_mst($con, $fname, array('SM'));
$auth_short_name = $arr_inci['SM']['auth_short_name'];


//受信トレイにある他の部署の報告書を評価可能にするフラグを取得
//$is_progress_eval = get_progress_eval($con, $fname);
$is_progress_eval = $sysConf->get('fantol.evaluation.common.use.flg');

//受付済みなどのチェック
$progress_uketsuke['SM'] = $sysConf->get('fantol.progress.sm.uketsuke.flg');
$progress_uketsuke['RM'] = $sysConf->get('fantol.progress.rm.uketsuke.flg');
$progress_uketsuke['RA'] = $sysConf->get('fantol.progress.ra.uketsuke.flg');
$progress_uketsuke['SD'] = $sysConf->get('fantol.progress.sd.uketsuke.flg');
$progress_uketsuke['MD'] = $sysConf->get('fantol.progress.md.uketsuke.flg');
$progress_uketsuke['HD'] = $sysConf->get('fantol.progress.hd.uketsuke.flg');

//
$progress_confirm['SM'] = $sysConf->get('fantol.progress.sm.confirm.flg');
$progress_confirm['RM'] = $sysConf->get('fantol.progress.rm.confirm.flg');
$progress_confirm['RA'] = $sysConf->get('fantol.progress.ra.confirm.flg');
$progress_confirm['SD'] = $sysConf->get('fantol.progress.sd.confirm.flg');
$progress_confirm['MD'] = $sysConf->get('fantol.progress.md.confirm.flg');
$progress_confirm['HD'] = $sysConf->get('fantol.progress.hd.confirm.flg');

//評価                                
$progress_eval['SM'] = $sysConf->get('fantol.progress.sm.eval.flg');
$progress_eval['RM'] = $sysConf->get('fantol.progress.rm.eval.flg');
$progress_eval['RA'] = $sysConf->get('fantol.progress.ra.eval.flg');
$progress_eval['SD'] = $sysConf->get('fantol.progress.sd.eval.flg');
$progress_eval['MD'] = $sysConf->get('fantol.progress.md.eval.flg');
$progress_eval['HD'] = $sysConf->get('fantol.progress.hd.eval.flg');


//評価機能利用フラグ
$evaluation_use['SM'] = $sysConf->get('fantol.evaluation.use.sm.flg');
$evaluation_use['RM'] = $sysConf->get('fantol.evaluation.use.rm.flg');
$evaluation_use['RA'] = $sysConf->get('fantol.evaluation.use.ra.flg');
$evaluation_use['SD'] = $sysConf->get('fantol.evaluation.use.sd.flg');
$evaluation_use['MD'] = $sysConf->get('fantol.evaluation.use.md.flg');
$evaluation_use['HD'] = $sysConf->get('fantol.evaluation.use.hd.flg');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 進捗設定初期登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
.list_2 {border-collapse:collapse;}
.list_2 td {border:#999999 solid 1px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td bgcolor="#F5FFE5">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<form name="mainform" action='' mode="post">
	<input type="hidden" name="postback_mode" value="send_change">
	<input type="hidden" name="session" value="<?=$session?>">


	<table width="50%" height="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
		<tr>
			<td width="10%" align="center" bgcolor="#FFDDFD" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">工程(担当)</font></td>
			<td width="10%" align="center" bgcolor="#FFDDFD" rowspan="2">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定</font>
			</td>
			<td width="10%" align="center" bgcolor="#FFDDFD" rowspan="2">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書の評価を行う</font>
			</td>
                        
                       
               <!--山本@静岡県立総合 20150730 ここから-->
<td width="10%" bgcolor="#FFDDFD" align="center"  colspan="3" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全員が処理したときにチェックが入る</font>
</td>
                
</tr>
<tr>

                        <td width="10%" align="center" bgcolor="#FFDDFD">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付済</font>
			</td>
                        <td width="10%" align="center" bgcolor="#FFDDFD">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認済</font>
			</td>
                        <td width="10%" align="center" bgcolor="#FFDDFD">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価済</font>
			</td>
		</tr>
                <!--山本@静岡県立総合 20150730 ここまで-->
		<?
			$num = pg_numrows($arr_inci_progress);
			$auths = "";	
			for($i=0; $i<$num; $i++) { 

				$auth_name = pg_result($arr_inci_progress,$i,"auth_name");
				$auth = pg_result($arr_inci_progress,$i,"auth");
				$use_flg = pg_result($arr_inci_progress,$i,"use_flg");
				$eval_flag = $evaluation_use[$auth];

                                $p_uketsuke_flg =  $progress_uketsuke[$auth];
                                $p_confirm_flg =  $progress_confirm[$auth];
                                $p_eval_flg =  $progress_eval[$auth];
				if($auths != "") {
					$auths .= ",";
				}

				$auths .= "$auth"; 
		?>
		<tr>
			<td align="left" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo h($auth_name); ?><font></td>
			<td align="center" bgcolor="#FFFFFF">
				<input type="checkbox" name="<? echo $auth ?>"                value="t" id="progress_<? echo $auth ?>"   onclick="on_progress_check(this, '<? echo $auth ?>')"   <?if($use_flg == "t")   {echo(" checked");} ?>>
			</td>
			<td align="center" bgcolor="#FFFFFF">
				<input type="checkbox" name="evaluation_use_<? echo $auth ?>" value="t" id="evaluation_<? echo $auth ?>" onclick="on_evaluation_check(this, '<? echo $auth ?>')" <?if($eval_flag == "t") {echo(" checked");} ?>>
			</td>
                        
                        <!--山本@静岡県立総合 20150730 ここから-->
                        <td align="center" bgcolor="#FFFFFF">
				<input type="checkbox" name="p_uketsuke_flg_<? echo $auth ?>" value="t" id="uketsuke_<? echo $auth ?>" onclick="on_evaluation_check(this, '<? echo $auth ?>')" <?if($p_uketsuke_flg == "t") {echo(" checked");} ?>>
			</td>
                        
                        <td align="center" bgcolor="#FFFFFF">
				<input type="checkbox" name="p_confirm_flg_<? echo $auth ?>" value="t" id="confirm_<? echo $auth ?>" onclick="on_evaluation_check(this, '<? echo $auth ?>')" <?if($p_confirm_flg == "t") {echo(" checked");} ?>>
			</td>
                        
                        <td align="center" bgcolor="#FFFFFF">
				<input type="checkbox" name="p_eval_flg_<? echo $auth ?>" value="t" id="eval_<? echo $auth ?>" onclick="on_evaluation_check(this, '<? echo $auth ?>')" <?if($p_eval_flg == "t") {echo(" checked");} ?>>
			</td>
                        <!--山本@静岡県立総合 20150730　ここまで -->
		</tr>
		<? } ?>
		<input type="hidden" name="auths" value="<?=$auths?>">
	</table>
</td></tr>
<tr><td>

<script language="javascript">
function on_progress_check(obj, auth)
{
    //設定のチェックを外したら、評価を行うのチェックも外す
    if (!obj.checked){
        document.getElementById("evaluation_" + auth).checked = false;
        $("#evaluation_" + auth).prop('checked', false);
    }
}

function on_evaluation_check(obj, auth)
{
    //評価にチェックを入れたら、設定のチェックも入れる
    if (obj.checked){
        document.getElementById("progress_" + auth).checked = true;
        $("#progress_" + auth).prop('checked', true);
    }
}

function auto_out()
{
	for(i = 0; i < document.mainform.elements.length; i++)
	{
		if(document.mainform.elements[i].name == "progress_comment_use")
		{
			if(!document.mainform.elements[i].checked)
			{
				//「確認コメントを使用する。」がはずれていれば、「確認コメントを報告書印刷時に印刷する。 」をはずす
				document.mainform.progress_comment_print.checked = false;
			}
		}
	}
}
	

function auto_check()
{
	for(i = 0; i < document.mainform.elements.length; i++)
	{
		if(document.mainform.elements[i].name == "progress_comment_print")
		{
			if(document.mainform.elements[i].checked)
			{
				//「確認コメントを報告書印刷時に印刷する。」がチェックされていれば、「確認コメントを使用する。 」をチェックする
				document.mainform.progress_comment_use.checked = true;
			}
		}
	}
}

</script>

	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<input type="checkbox" value="t" name="progress_comment_use" <?if($is_progress_comment_use){?>checked<?} ?> onclick="auto_out();">
	確認コメントを使用する。
	</font>
</td></tr>
<tr><td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<input type="checkbox" value="t" name="progress_comment_print" <?if($is_progress_comment_print){?>checked<?} ?>  onclick=auto_check();>
	確認コメントを報告書印刷時に印刷する。
	</font>
</td></tr>
<tr><td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<input type="checkbox" value="t" name="progress_adjust" <?if($is_progress_adjust){?>checked<?} ?> >
	送信先に進捗登録設定した担当者がいなければ、進捗管理を行わない。
	</font>
</td></tr>
<tr><td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<input type="checkbox" value="t" name="progress_auto_lock" <?if($is_progress_auto_lock){?>checked<?} ?> >
	<?=$auth_short_name?>確認時に対象の報告書をロックする。
	</font>
</td></tr>

<tr><td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<input type="checkbox" value="t" name="other_progress_eval" <?if($is_progress_eval=='t'){?>checked<?} ?> >
	受信トレイにある他の部署の報告書を評価可能にする。
	</font>
</td></tr>

<tr><td>
	<table width="40%" border="0" cellspacing="0" cellpadding="0">
		<tr><td align="right">
			<input type="submit" value="登録">
		</td></tr>
	</table>


</form>


</td>
</tr>
</table>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>
</body>
</html>
<?
//========================================================================
// 進捗設定（工程）登録処理
//========================================================================
function regist_dflt_flg($con, $fname, $auth, $use_flg) {

	// 更新処理
	$sql = "update inci_report_progress_dflt_flg set ";
	$set = array("use_flg");
	$setvalue = array($use_flg);
	$cond = "where auth = '$auth'";
	$up = update_set_table($con,$sql,$set,$setvalue,$cond,$fname);

	if($up == 0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

pg_close($con);
?>
