<?php

// セッションの開始
session_start();

// ==================================================
// ファイルの読込み
// ==================================================
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("hiyari_common.ini");
require_once("hiyari_post_select_box.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_wf_utils.php");

require_once("get_values.php");

// ==================================================
// 初期処理
// ==================================================

// 画面名
$fname = $PHP_SELF;

// ==================================================
// セッションのチェック
// qualify_session() from about_session.php
// ==================================================
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// 権限チェック
// check_authority() from about_authority.php
// ==================================================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

// ==================================================
// DBコネクション取得
// connect2db() from about_postgres.php
// ==================================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// 職員ID取得
// get_emp_id() from get_values.php
// ==================================================
$emp_id = get_emp_id($con, $session, $fname);

// ==================================================
// HTML出力 START
// ==================================================
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?= $INCIDENT_TITLE ?> | ヒヤリハット提出</title>
<script type="text/javascript" src="js/fontsize.js"></script>

<link rel="stylesheet" type="text/css" href="css/incident.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.list_2 {border-collapse:collapse;}
.list_2 td {border:#999999 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}

</style>

<script language="javascript" type="text/javascript">
	function start_auto_session_update()
	{
	    //1分(60*1000ms)後に開始
	    setTimeout(auto_sesson_update, 60000);
	}
	function auto_sesson_update()
	{
	    //セッション更新
	    document.session_update_form.submit();

	    //1分(60*1000ms)後に最呼び出し
	    setTimeout(auto_sesson_update, 60000);
	}
</script>
</head>

<?
$target_xml_index = "";
$target_db_index = "";

$target_xml_index = $_GET["target_xml_index"];
if ($target_xml_index == "") {
	$target_xml_index = $_POST["target_xml_index"];
	$target_db_index = $_POST["target_db_index"];
}

if ($target_db_index == "") {
	$col_num = 1;
} else {
	$col_num = 2;
}

// 表題、データ表示部の幅：1174(全体) / 2(XMLデータ、重複データ) = 587(片方の幅)
$th_width = 180;
$td_width = 587 - $th_width;

// データ表示部の背景色
$normal_td_color = "#ffffff";
$unmatched_td_color = "#ffcccc";
?>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- BODY全体 START -->

<!-- セッションタイムアウト防止 START -->
<form name="session_update_form" action="hiyari_session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="<?= $session ?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->


<form name="import_sub_form" method="post" action="hiyari_hiyari_xml_import_sub.php?session=<?= $session ?>">

<!-- 本体 START -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<?
			// XMLデータ：最後のインデックスは親画面で選択した行番号にする
			$report_xml = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_REPORT"][$target_xml_index];
			
			// 比較対象(データベース）
			if ($target_db_index != "") {
				$target_data = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_ID_ARRAY"][$target_xml_index][$target_db_index];
				$target_report_id = $target_data["report_id"];
				$target_eid_id = $target_data["eid_id"];
				
				// ==================================================
				// レポート取得
				// get_report_array() from self
				// ==================================================
				$db_report_arr = get_report_array($con, $fname, $target_report_id);
				$db_report = $db_report_arr[0];
				
				// ==================================================
				// レポート取得
				// get_report_data_array() from self
				// ==================================================
				$db_data_arr = get_report_data_array($con, $fname, $target_eid_id);
			}
			?>
			<table border="0" cellspacing="0" cellpadding="3" class="list">
				<tr height="25">
					<?
					// ==================================================
					// 報告書ID
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "<font color='#55aaff'>";
								$disp .= "置換する重複データの報告書IDになります";
								$disp .= "</font>";
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["report_id"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書ID</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 表題
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = mb_substr($report_xml["DATCONTENTTEXT"][0]["_value"], 0, 10);
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["report_title"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// (下書き)様式の職種ID
					// ==================================================
					// ==================================================
					// 職種取得
					// get_job_array() from self
					// ==================================================
					$job_sel_arr = get_job_array($con, $fname);
					
					$xml_job_id = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]["sel_job_id"];
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach($job_sel_arr as $job_data) {
									$val = $job_data["job_id"];
									$txt = $job_data["job_nm"];
									
									if ($xml_job_id == $val) {
										$disp = $txt;
										
										$compare = $disp;
										
										break;
									}
								}
								break;
							case "1":
								foreach($job_sel_arr as $job_data) {
									$val = $job_data["job_id"];
									$txt = $job_data["job_nm"];
									
									if ($db_report["job_id"] == $val) {
										$disp = $txt;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">(下書き)様式の職種ID</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告者ID
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $emp_id;
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["registrant_id"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者ID</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告者氏名
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								// ==================================================
								// 報告者氏名取得
								// get_emp_kanji_name() from get_values.php
								// ==================================================
								$disp = get_emp_kanji_name($con, $emp_id, $fname);
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["registrant_name"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者氏名</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告日
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = date("Y/m/d");
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["registration_date"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// (下書き)様式ID
					// ==================================================
					// ==================================================
					// 様式取得
					// get_eis_array() from self
					// ==================================================
					$eis_sel_arr = get_eis_array($con, $fname);
					
					$xml_eis_id = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]["sel_eis_id"];
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach($eis_sel_arr as $eis_data) {
									$job = $eis_data["job_id"];
									$eis = $eis_data["eis_no"];
									$name = $eis_data["eis_name"];
									
									if ($xml_job_id == $job && $xml_eis_id == $eis) {
										$disp = $name;
										
										$compare = $disp;
										
										break;
									}
								}
								break;
							case "1":
								foreach($eis_sel_arr as $eis_data) {
									$job = $eis_data["job_id"];
									$eis = $eis_data["eis_no"];
									$name = $eis_data["eis_name"];
									
									if ($db_report["job_id"] == $job && $db_report["eis_id"] == $eis) {
										$disp = $name;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">(下書き)様式ID</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// データID
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "<font color='#55aaff'>";
								$disp .= "登録時に新たに採番されます";
								$disp .= "</font>";
								break;
							case "1":
								$disp = $db_report["eid_id"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">データID</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 削除フラグ
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "削除されていません";
								
								$compare = $disp;
								
								break;
							case "1":
								if ($db_report["del_flag"] == "t") {
									$disp = "削除されています";
								} else {
									$disp = "削除されていません";
								}
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除フラグ</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 事案番号
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "<font color='#55aaff'>";
								$disp .= "置換する重複データの事案番号になります";
								$disp .= "</font>";
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["report_no"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 下書きフラグ
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "下書きではありません";
								
								$compare = $disp;
								
								break;
							case "1":
								if ($db_report["shitagaki_flg"] == "t") {
									$disp = "下書きです";
								} else {
									$disp = "下書きではありません";
								}
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下書きフラグ</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// コメント
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "";
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["report_comment"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コメント</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// SMロックフラグ
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "ロックされていません";
								
								$compare = $disp;
								
								break;
							case "1":
								if ($db_report["edit_lock_flg"] == "t") {
									$disp = "ロックされています";
								} else {
									$disp = "ロックされていません";
								}
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">SMロックフラグ</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 匿名報告フラグ
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "匿名ではありません";
								
								$compare = $disp;
								
								break;
							case "1":
								if ($db_report["anonymous_report_flg"] == "t") {
									$disp = "匿名です";
								} else {
									$disp = "匿名ではありません";
								}
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">匿名報告フラグ</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告部署1
					// ==================================================
					// ==================================================
					// 部門取得
					// get_class_array() from self
					// ==================================================
					$class_arr = get_class_array($con, $fname);
					
					$xml_class_id = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]["search_emp_class"];
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($class_arr as $class_data) {
									$val = $class_data["class_id"];
									$txt = $class_data["class_nm"];
									
									if ($xml_class_id == $val) {
										$disp = $txt;
										
										$compare = $disp;
										
										break;
									}
								}
								break;
							case "1":
								foreach ($class_arr as $class_data) {
									$val = $class_data["class_id"];
									$txt = $class_data["class_nm"];
									
									if ($db_report["registrant_class"] == $val) {
										$disp = $txt;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署1</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告部署2
					// ==================================================
					// ==================================================
					// 課取得
					// get_atrb_array() from self
					// ==================================================
					$atrb_arr = get_atrb_array($con, $fname);
					
					$xml_atrb_id = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]["search_emp_attribute"];
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($atrb_arr as $atrb_data) {
									$upp = $atrb_data["class_id"];
									$val = $atrb_data["atrb_id"];
									$txt = $atrb_data["atrb_nm"];
									
									if ($xml_class_id == $upp && $xml_atrb_id == $val) {
										$disp = $txt;
										
										$compare = $disp;
										
										break;
									}
								}
								break;
							case "1":
								foreach ($atrb_arr as $atrb_data) {
									$upp = $atrb_data["class_id"];
									$val = $atrb_data["atrb_id"];
									$txt = $atrb_data["atrb_nm"];
									
									if ($db_report["registrant_class"] == $upp && $db_report["registrant_attribute"] == $val) {
										$disp = $txt;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署2</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告部署3
					// ==================================================
					// ==================================================
					// 科取得
					// get_dept_array() from self
					// ==================================================
					$dept_arr = get_dept_array($con, $fname);
					
					$xml_dept_id = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]["search_emp_dept"];
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($dept_arr as $dept_data) {
									$upp = $dept_data["atrb_id"];
									$val = $dept_data["dept_id"];
									$txt = $dept_data["dept_nm"];
									
									if ($xml_atrb_id == $upp && $xml_dept_id == $val) {
										$disp = $txt;
										
										$compare = $disp;
										
										break;
									}
								}
								break;
							case "1":
								foreach ($dept_arr as $dept_data) {
									$upp = $dept_data["atrb_id"];
									$val = $dept_data["dept_id"];
									$txt = $dept_data["dept_nm"];
									
									if ($db_report["registrant_attribute"] == $upp && $db_report["registrant_dept"] == $val) {
										$disp = $txt;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署3</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告部署4
					// ==================================================
					// ==================================================
					// 室取得
					// get_room_array() from self
					// ==================================================
					$room_arr = get_room_array($con, $fname);
					
					$xml_room_id = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]["search_emp_room"];
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($room_arr as $room_data) {
									$upp = $room_data["dept_id"];
									$val = $room_data["room_id"];
									$txt = $room_data["room_nm"];
									
									if ($xml_dept_id == $upp && $xml_room_id == $val) {
										$disp = $txt;
										
										$compare = $disp;
										
										break;
									}
								}
								break;
							case "1":
								foreach ($room_arr as $room_data) {
									$upp = $room_data["dept_id"];
									$val = $room_data["room_id"];
									$txt = $room_data["room_nm"];
									
									if ($db_report["registrant_dept"] == $upp && $db_report["registrant_room"] == $val) {
										$disp = $txt;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署4</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 報告者職種
					// ==================================================
					// ==================================================
					// 報告者職種
					// get_own_job() from get_values.php
					// ==================================================
					$xml_own_job = get_own_job($con, $emp_id, $fname);
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach($job_sel_arr as $job_data) {
									$val = $job_data["job_id"];
									$txt = $job_data["job_nm"];
									
									if ($xml_own_job == $val) {
										$disp = $txt;
										
										$compare = $disp;
										
										break;
									}
								}
								break;
							case "1":
								foreach($job_sel_arr as $job_data) {
									$val = $job_data["job_id"];
									$txt = $job_data["job_nm"];
									
									if ($db_report["registrant_job"] == $val) {
										$disp = $txt;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者職種</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 主治医ID
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "";
								
								$compare = $disp;
								
								break;
							case "1":
								$disp = $db_report["doctor_emp_id"];
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主治医ID</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 完全削除フラグ
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = "完全削除されていません";
								
								$compare = $disp;
								
								break;
							case "1":
								if ($db_report["kill_flg"] == "t") {
									$disp = "完全削除されています";
								} else {
									$disp = "完全削除されていません";
								}
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">完全削除フラグ</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				
				<!-- 以下 inci_easyinput_data //-->
				
				<?
				// ==================================================
				// 固定データ配列を取得
				// get_fix_xml_data_array() from self
				// ==================================================
				$fix_arr = get_fix_xml_data_array();
				?>
				
				<tr height="25">
					<?
					// ==================================================
					// 発生年月日
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $report_xml["DATYEAR"][0]["_value"] . "/" . substr("00" . $report_xml["DATMONTH"][0]["_attr"]["CODE"], -2) . "/00";
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 100 && $item == 5) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生年月日</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 発生月
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATMONTH"][$report_xml["DATMONTH"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 100 && $item == 10) {
										if ($input != "") {
											$disp = $fix_arr["DATMONTH"][substr("00" . $input, -2)];
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生月</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 発生曜日
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATDAY"][$report_xml["DATDAY"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 100 && $item == 20) {
										if ($input != "") {
											$disp = $fix_arr["DATDAY"][substr("00" . $input, -2)];
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生曜日</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 曜日区分
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATHOLIDAY"][$report_xml["DATHOLIDAY"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 100 && $item == 30) {
										if ($input != "") {
											$disp = $fix_arr["DATHOLIDAY"][substr("00" . $input, -2)];
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日区分</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 発生時間帯
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATTIMEZONE"][$report_xml["DATTIMEZONE"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 100 && $item == 40) {
										if ($input != "") {
											$disp = $fix_arr["DATTIMEZONE"][substr("00" . $input, -2)];
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生時間帯</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 医療の実施の有無
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATEXECUTION"][$report_xml["DATEXECUTION"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 1100 && $item == 10) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療の実施の有無</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 治療の程度
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATLEVEL"][$report_xml["DATLEVEL"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 1100 && $item == 20) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">治療の程度</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 事故の程度
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATSEQUELAE"][$report_xml["DATSEQUELAE"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 1100 && $item == 30) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故の程度</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 影響度
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATINFLUENCE"][$report_xml["DATINFLUENCE"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 1100 && $item == 50) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">影響度</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 発生場所
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($report_xml["LSTSITE"][0]["DATSITE"] as $datsite) {
									if ($disp != "") {
										$disp .= "、";
									}
									$disp .= $fix_arr["DATSITE"][$datsite["_attr"]["CODE"]];
								}
								
								$compare = $disp;
								
								break;
							case "1":
								// ==================================================
								// DEFデータの取得
								// get_def_array() from self
								// ==================================================
								$def_arr = get_def_array($con, $fname, 110, 60);
								
								$tmp_arr = array();
								
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 110 && $item == 60) {
										$tmp_arr = explode("\t", $input);
										
										foreach($tmp_arr as $tmp) {
											foreach ($def_arr as $def) {
												if ($tmp == $def["easy_code"]) {
													if ($disp != "") {
														$disp .= "、";
													}
													$disp .= $fix_arr["DATSITE"][trim($def["def_code"])];
													
													break;
												}
											}
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場所</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 概要
					// ==================================================
					// 項目選択の為、保持
					$xml_summary = "";
					$data_summary = "";
					$db_summary = "";
					
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$xml_summary = $report_xml["DATSUMMARY"][0]["_attr"]["CODE"];
								$disp = $fix_arr["DATSUMMARY"][$xml_summary];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 900 && $item == 10) {
										$db_summary = $input;
										
										// ==================================================
										// CODEデータの取得
										// convert_super_item_id_to_code() from self
										// ==================================================
										$data_summary = convert_super_item_id_to_code($con, $fname, $input);
										$disp = $fix_arr["DATSUMMARY"][$data_summary];
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 特に報告を求める事例
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATSPECIALLY"][$report_xml["DATSPECIALLY"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 1110 && $item == 10) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特に報告を求める事例</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 関連診療科
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($report_xml["LSTDEPARTMENT"][0]["DATDEPARTMENT"] as $datdepartment) {
									if ($disp != "") {
										$disp .= "、";
									}
									$disp .= $fix_arr["DATDEPARTMENT"][$datdepartment["_attr"]["CODE"]];
								}
								
								$compare = $disp;
								
								break;
							case "1":
								// ==================================================
								// DEFデータの取得
								// get_def_array() from self
								// ==================================================
								$def_arr = get_def_array($con, $fname, 130, 90);
								
								$tmp_arr = array();
								
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 130 && $item == 90) {
										$tmp_arr = explode("\t", $input);
										
										foreach ($tmp_arr as $tmp) {
											foreach ($def_arr as $def) {
												if ($tmp == $def["easy_code"]) {
													if ($disp != "") {
														$disp .= "、";
													}
													$disp .= $fix_arr["DATDEPARTMENT"][trim($def["def_code"])];
													
													break;
												}
											}
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">関連診療科</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 患者の数
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATPATIENT"][$report_xml["GRPPATIENT"][0]["DATPATIENT"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 200 && $item == 10) {
										$disp = $fix_arr["DATPATIENT"][$input];
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者の数</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 患者の年齢
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$years = "";
								$years_str = "";
								$months = "";
								$months_str = "";
								foreach ($report_xml["GRPPATIENT"][0]["LSTPATIENTAGE"][0]["DATPATIENTAGE"] as $datpatientage) {
									switch ($datpatientage["_attr"]["KBN"]) {
										// 歳
										case "years":
											$years = (int) $datpatientage["_value"];
											$years_str =  $fix_arr["DATPATIENTAGE"][$datpatientage["_attr"]["KBN"]];
											break;
										// ヶ月
										case "months":
											$months = (int) $datpatientage["_value"];
											$months_str = $fix_arr["DATPATIENTAGE"][$datpatientage["_attr"]["KBN"]];
											break;
									}
								}
								if ($years != "" && $months != "") {
									$disp = $years . $years_str . " " . $months . $months_str;
								}
								
								$compare = $disp;
								
								break;
							case "1":
								$years = "";
								$months = "";
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 210 && $item == 40) {
										$years = (int) $input;
									}
									
									if ($grp == 210 && $item == 50) {
										$months = (int) $input;
									}
								}
								if ($years != "" && $months != "") {
									$disp = $years . $years_str . " " . $months . $months_str;
								}
								
								if ($compare != $disp) {
									$td_color = $unmatched_td_color;
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者の年齢</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 患者の性別
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATPATIENTSEX"][$report_xml["GRPPATIENT"][0]["DATPATIENTSEX"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 210 && $item == 30) {
										if ($input != "") {
											$disp = $fix_arr["DATPATIENTSEX"][substr("00" . $input, -2)];
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者の性別</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 患者区分
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATPATIENTDIV"][$report_xml["GRPPATIENT"][0]["DATPATIENTDIV"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								// ==================================================
								// DEFデータの取得
								// get_def_array() from self
								// ==================================================
								$def_arr = get_def_array($con, $fname, 130, 90);
								
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 230 && $item == 60) {
										foreach ($def_arr as $def) {
											if ($input == $def["easy_code"]) {
												
												$disp = $fix_arr["DATPATIENTDIV"][trim($def["def_code"])];
												
												break;
											}
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者区分</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<?
				// ==================================================
				// 疾患名
				// ==================================================
				for ($i = 0; $i < $col_num; $i++) {
					switch ($i) {
						case "0":
							$xml_disp = array();
							$disp_title = array();
							
							foreach ($report_xml["LSTDISEASE"][0]["DATDISEASE"] as $datdisease) {
								switch ($datdisease["_attr"]["KBN"]) {
									// 事故(事例)に直接関連する疾患名
									case "disease0":
										$disp_title[0] = "事故(事例)に<br>直接関連する疾患名";
										$xml_disp[0] = $datdisease["_value"];
										break;
									// 関連診療科する疾患名1
									case "disease1":
										$disp_title[1] = "関連する疾患名1";
										$xml_disp[1] = $datdisease["_value"];
										break;
									// 関連診療科する疾患名2
									case "disease2":
										$disp_title[2] = "関連する疾患名2";
										$xml_disp[2] = $datdisease["_value"];
										break;
									// 関連診療科する疾患名3
									case "disease3":
										$disp_title[3] = "関連する疾患名3";
										$xml_disp[3] = $datdisease["_value"];
										break;
								}
							}
							
							
							break;
						case "1":
							$data_disp = array();
							foreach ($db_data_arr as $db_data) {
								$grp = $db_data["grp_code"];
								$item = $db_data["easy_item_code"];
								$input = $db_data["input_item"];
								
								// 事故(事例)に直接関連する疾患名
								if ($grp == 250 && $item == 80) {
									$disp_title[0] = "事故(事例)に<br>直接関連する疾患名";
									$data_disp[0] = $input;
								}
								
								// 関連診療科する疾患名1
								if ($grp == 260 && $item == 90) {
									$disp_title[1] = "関連する疾患名1";
									$data_disp[1] = $input;
								}
								
								// 関連診療科する疾患名2
								if ($grp == 270 && $item == 100) {
									$disp_title[2] = "関連する疾患名2";
									$data_disp[2] = $input;
								}
								
								// 関連診療科する疾患名3
								if ($grp == 280 && $item == 110) {
									$disp_title[3] = "関連する疾患名3";
									$data_disp[3] = $input;
								}
							}
							
							break;
					}
				}
				
				for ($i = 0; $i < 4; $i++) {
					$td_color = $normal_td_color;
					if ($disp_title[$i] != "") {
					?>
						<tr height="25">
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $disp_title[$i] ?></font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $xml_disp[$i] ?>
							</td>
							<?
							if ($col_num == 2) {
								$td_color = $normal_td_color;
								if ($xml_disp[$i] != $data_disp[$i]) {
									$td_color = $unmatched_td_color;
								}
								?>
								<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $disp_title[$i] ?></font>
								</td>
								<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
									<?= $data_disp[$i] ?>
								</td>
							<?
							}
							?>
						</tr>
					<?
					}
				}
				?>
				<tr height="25">
					<?
					// ==================================================
					// 直前の患者の状態
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($report_xml["LSTPATIENTSTATE"][0]["DATPATIENTSTATE"] as $datpatientstate) {
									if ($disp != "") {
										$disp .= "、";
									}
									$disp .= $fix_arr["DATPATIENTSTATE"][$datpatientstate["_attr"]["CODE"]];
								}
								
								$compare = $disp;
								
								break;
							case "1":
								// ==================================================
								// DEFデータの取得
								// get_def_array() from self
								// ==================================================
								$def_arr = get_def_array($con, $fname, 290, 120);
								
								$tmp_arr = array();
								
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 290 && $item == 120) {
										$tmp_arr = explode("\t", $input);
										
										foreach ($tmp_arr as $tmp) {
											foreach ($def_arr as $def) {
												if ($tmp == $def["easy_code"]) {
													if ($disp != "") {
														$disp .= "、";
													}
													$disp .= $fix_arr["DATPATIENTSTATE"][trim($def["def_code"])];
													
													break;
												}
											}
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">直前の患者の状態</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 発見者
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATDISCOVERER"][$report_xml["DATDISCOVERER"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 3000 && $item == 10) {
										if ($input != "") {
											$disp = $fix_arr["DATDISCOVERER"][substr("00" . $input, -2)];
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発見者</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<?
				// ==================================================
				// 当事者
				// ==================================================
				// 比較用
				$compare = "";
				
				$disp_title = array();
				$xml_disp = array();
				$data_disp = array();
				
				$disp_title[0] = "当事者職種";
				$disp_title[1] = "専門医・認定医及び<br>その他の医療従事者の<br>専門・認定資格";
				$disp_title[2] = "当事者職種経験";
				$disp_title[3] = "当事者部署配属期間";
				$disp_title[4] = "直前１週間の<br>当直・夜勤回数";
				$disp_title[5] = "勤務形態";
				$disp_title[6] = "直前１週間の勤務時間";
				
				for ($i = 0; $i < $col_num; $i++) {
					
					switch ($i) {
						case "0":
							$j = 0;
							foreach ($report_xml["LSTCONCERNED"][0]["GRPCONCERNED"] as $grpconcerned) {
								// 当事者職種
								$xml_disp[$j][0] = $fix_arr["DATCONCERNED"][$grpconcerned["DATCONCERNED"][0]["_attr"]["CODE"]];
								
								// 専門医・認定医及び<br>その他の医療従事者の<br>専門・認定資格
								$xml_disp[$j][1] = $grpconcerned["DATQUALIFICATION"][0]["_value"];
								
								$years = "";
								$months = "";
								foreach ($grpconcerned["LSTEXPERIENCE"][0]["DATEXPERIENCE"] as $datexperience) {
									switch ($datexperience["_attr"]["KBN"]) {
										case "years":
											$years = $datexperience["_value"];
											
											break;
										case "months" :
											$months = $datexperience["_value"];
											break;
									}
								}
								
								// 当事者職種経験
								$xml_disp[$j][2] = (int) $years . "年";
								if ($months != "") {
									$xml_disp[$j][2] .= " " . (int) $months . "ヶ月";
								}
								
								$years = "";
								$months = "";
								foreach ($grpconcerned["LSTLENGTH"][0]["DATLENGTH"] as $datexperience) {
									switch ($datexperience["_attr"]["KBN"]) {
										case "years":
											$years = $datexperience["_value"];
											
											break;
										case "months" :
											$months = $datexperience["_value"];
											break;
									}
								}
								
								// 当事者部署配属期間
								$xml_disp[$j][3] = (int) $years . "年";
								if ($months != "") {
									$xml_disp[$j][3] .= " " . (int) $months . "ヶ月";
								}
								
								// 直前１週間の<br>当直・夜勤回数
								$xml_disp[$j][4] = $fix_arr["DATDUTY"][$grpconcerned["DATDUTY"][0]["_attr"]["CODE"]];
								
								// 勤務形態
								$xml_disp[$j][5] = $fix_arr["DATSHIFT"][$grpconcerned["DATSHIFT"][0]["_attr"]["CODE"]];
								
								// 直前１週間の勤務時間
								$xml_disp[$j][6] = $grpconcerned["DATHOURS"][0]["_value"];
								
								$j++;
							}
							
							break;
						case "1":
							
							$tmp_arr = array();
							
							foreach ($db_data_arr as $db_data) {
								$grp = $db_data["grp_code"];
								$item = $db_data["easy_item_code"];
								$input = $db_data["input_item"];
								
								// 当事者職種
								if (strlen($grp) == 4 && substr($grp, 0, 3) == 305 && $item == 30) {
									// ==================================================
									// DEFデータの取得
									// get_def_array() from self
									// ==================================================
									$def_arr = get_def_array($con, $fname, 305 . substr($grp, -1), 30);
									
									foreach ($def_arr as $def) {
										if ($input == $def["easy_code"]) {
											$data_disp[substr($grp, -1)][0] = $fix_arr["DATCONCERNED"][trim($def["def_code"])];
											
											break;
										}
									}
								}
								
								// 専門医・認定医及びその他の医療従事者の専門・認定資格
								if (strlen($grp) == 4 && substr($grp, 0, 3) == 320 && $item == 60) {
									$data_disp[substr($grp, -1)][1] = $input;
								}
								
								// 当事者職種経験
								$years = "";
								$months = "";
								if (strlen($grp) == 4 && substr($grp, 0, 3) == 325) {
									// 年
									if ($item == 70) {
										$data_disp[substr($grp, -1)][2] = (int) $input . "年";
									}
									
									// ヶ月
									if ($item == 80) {
										$data_disp[substr($grp, -1)][2] .= " " . (int) $input . "ヶ月";
									}
								}
								
								// 当事者部署配属期間
								$years = "";
								$months = "";
								if (strlen($grp) == 4 && substr($grp, 0, 3) == 330) {
									// 年
									if ($item == 90) {
										$data_disp[substr($grp, -1)][3] = (int) $input . "年";
									}
									
									// ヶ月
									if ($item == 100) {
										$data_disp[substr($grp, -1)][3] .= " " . (int) $input . "ヶ月";
									}
								}
								
								// 直前１週間の当直・夜勤回数
								if (strlen($grp) == 4 && substr($grp, 0, 3) == 335 && $item == 110) {
									if ($input != "") {
										$data_disp[substr($grp, -1)][4] = $fix_arr["DATDUTY"][substr("00" . $input, -2)];
									}
								}
								
								// 勤務形態
								if (strlen($grp) == 4 && substr($grp, 0, 3) == 340 && $item == 120) {
									if ($input != "") {
										$data_disp[substr($grp, -1)][5] = $fix_arr["DATSHIFT"][substr("00" . $input, -2)];
									}
								}
								
								// 直前１週間の勤務時間
								if (strlen($grp) == 4 && substr($grp, 0, 3) == 345 && $item == 140) {
									$data_disp[substr($grp, -1)][6] = $input;
								}
							}
							
							break;
					}
				}
				$cnt = count($xml_disp);
				if ($cnt < count($data_disp)) {
					$cnt = count($data_disp);
				}
				
				for ($i = 0; $i < $cnt; $i++) {
					for ($j = 0; $j < 7; $j++) {
						$td_color = $normal_td_color;
						?>
						<tr height="25">
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $disp_title[$j] ?></font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $xml_disp[$i][$j] ?>
							</td>
							<?
							if ($col_num == 2) {
								if ($xml_disp[$i][$j] != $data_disp[$i][$j]) {
									$td_color = $unmatched_td_color;
								}
								?>
								<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $disp_title[$j] ?></font>
								</td>
								<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
									<?= $data_disp[$i][$j] ?>
								</td>
							<?
							}
							?>
						</tr>
					<?
					}
				}
				?>
				<tr height="25">
					<?
					// ==================================================
					// 当事者以外の関連職種
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($report_xml["LSTRELCATEGORY"][0]["DATRELCATEGORY"] as $datrelcategory) {
									if ($disp != "") {
										$disp .= "、";
									}
									$disp .= $fix_arr["DATRELCATEGORY"][$datrelcategory["_attr"]["CODE"]];
								}
								
								$compare = $disp;
								
								break;
							case "1":
								// ==================================================
								// DEFデータの取得
								// get_def_array() from self
								// ==================================================
								$def_arr = get_def_array($con, $fname, 3500, 150);
								
								$tmp_arr = array();
								
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 3500 && $item == 150) {
										$tmp_arr = explode("\t", $input);
										
										foreach ($tmp_arr as $tmp) {
											foreach ($def_arr as $def) {
												if ($tmp == $def["easy_code"]) {
													if ($disp != "") {
														$disp .= "、";
													}
													$disp .= $fix_arr["DATRELCATEGORY"][trim($def["def_code"])];
													
													break;
												}
											}
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当事者以外の関連職種</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<?
				// ==================================================
				// 概要によって選択される項目
				// ==================================================
				// 薬剤
				if ($xml_summary == "02" || $data_summary == "02") {
				?>
					<tr height="25">
						<?
						// ==================================================
						// 種類
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATTYPE"]["02"][$report_xml["GRPMEDICINE"][0]["DATTYPE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "02" && $grp == 920 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATTYPE"]["02"][convert_sub_item_id_to_code($con, $fname, $db_summary, "kind", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 関連医薬品：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPMEDICINE"][0]["GRPMEDICALSUPPLIES"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "02" && $grp == 1000 && $item == 5) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 関連医薬品：コード
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPMEDICINE"][0]["GRPMEDICALSUPPLIES"][0]["DATCODE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "02" && $grp == 1000 && $item == 10) {
											$disp = $input;
											
											if ($compare != $disp) {
												$td_color = $unmatched_td_color;
											}
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コード</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 関連医薬品：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPMEDICINE"][0]["GRPMEDICALSUPPLIES"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "02" && $grp == 1000 && $item == 15) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 発生場面
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATSCENE"]["02"][$report_xml["GRPMEDICINE"][0]["DATSCENE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "02" && $grp == 950 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATSCENE"]["02"][convert_sub_item_id_to_code($con, $fname, $db_summary, "scene", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場面</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 事故(事例)の内容
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATDETAIL"]["02"][$report_xml["GRPMEDICINE"][0]["DATDETAIL"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "02" && $grp == 980 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATDETAIL"]["02"][convert_sub_item_id_to_code($con, $fname, $db_summary, "content", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故(事例)の内容</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
				<?
				// 輸血
				} else if ($xml_summary == "03" || $data_summary == "03") {
				?>
					<tr height="25">
						<?
						// ==================================================
						// 発生場面
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATSCENE"]["03"][$report_xml["GRPTRANSFUSION"][0]["DATSCENE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "03" && $grp == 950 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATSCENE"]["03"][convert_sub_item_id_to_code($con, $fname, $db_summary, "scene", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場面</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 事故(事例)の内容
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATDETAIL"]["03"][$report_xml["GRPTRANSFUSION"][0]["DATDETAIL"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "03" && $grp == 980 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATDETAIL"]["03"][convert_sub_item_id_to_code($con, $fname, $db_summary, "content", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故(事例)の内容</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
				<?
				// 治療・処置
				} else if ($xml_summary == "04" || $data_summary == "04") {
				?>
					<tr height="25">
						<?
						// ==================================================
						// 種類
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATTYPE"]["04"][$report_xml["GRPTREATMENT"][0]["DATTYPE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "04" && $grp == 920 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATTYPE"]["04"][convert_sub_item_id_to_code($con, $fname, $db_summary, "kind", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPTREATMENT"][0]["GRPMATERIAL"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "04" && $grp == 1000 && $item == 20) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPTREATMENT"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "04" && $grp == 1000 && $item == 25) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：購入年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPTREATMENT"][0]["GRPMATERIAL"][0]["DATPURCHASE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "04" && $grp == 1000 && $item == 30) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">購入年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 発生場面
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATSCENE"]["04"][$report_xml["GRPTREATMENT"][0]["DATSCENE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "04" && $grp == 950 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATSCENE"]["04"][convert_sub_item_id_to_code($con, $fname, $db_summary, "scene", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場面</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 事故(事例)の内容
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATDETAIL"]["04"][$report_xml["GRPTREATMENT"][0]["DATDETAIL"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "04" && $grp == 980 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATDETAIL"]["04"][convert_sub_item_id_to_code($con, $fname, $db_summary, "content", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故(事例)の内容</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
				<?
				// 医療機器等
				} else if ($xml_summary == "10" || $data_summary == "10") {
				?>
					<tr height="25">
						<?
						// ==================================================
						// 種類
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATTYPE"]["10"][$report_xml["GRPAPPARATUS"][0]["DATTYPE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 920 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATTYPE"]["10"][convert_sub_item_id_to_code($con, $fname, $db_summary, "kind", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 35) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 40) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：製造年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATDATEOFMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 45) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：購入年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATPURCHASE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 50) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">購入年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：直近の保守・点検年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATMAINTENANCE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 55) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">直近の保守・点検年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPMATERIAL"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 20) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 25) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：購入年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPAPPARATUS"][0]["GRPMATERIAL"][0]["DATPURCHASE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 1000 && $item == 30) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">購入年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 発生場面
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATSCENE"]["10"][$report_xml["GRPAPPARATUS"][0]["DATSCENE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 950 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATSCENE"]["10"][convert_sub_item_id_to_code($con, $fname, $db_summary, "scene", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場面</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 事故(事例)の内容
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATDETAIL"]["10"][$report_xml["GRPAPPARATUS"][0]["DATDETAIL"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "10" && $grp == 980 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATDETAIL"]["10"][convert_sub_item_id_to_code($con, $fname, $db_summary, "content", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故(事例)の内容</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
				<?
				// ドレーン・チューブ
				} else if ($xml_summary == "06" || $data_summary == "06") {
				?>
					<tr height="25">
						<?
						// ==================================================
						// 種類
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATTYPE"]["06"][$report_xml["GRPDRAINTUBE"][0]["DATTYPE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "06" && $grp == 920 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATTYPE"]["06"][convert_sub_item_id_to_code($con, $fname, $db_summary, "kind", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPDRAINTUBE"][0]["GRPMATERIAL"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "06" && $grp == 1000 && $item == 20) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPDRAINTUBE"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "06" && $grp == 1000 && $item == 25) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：購入年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPDRAINTUBE"][0]["GRPMATERIAL"][0]["DATPURCHASE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "06" && $grp == 1000 && $item == 30) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">購入年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 発生場面
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATSCENE"]["06"][$report_xml["GRPDRAINTUBE"][0]["DATSCENE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "06" && $grp == 950 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATSCENE"]["06"][convert_sub_item_id_to_code($con, $fname, $db_summary, "scene", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場面</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 事故(事例)の内容
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATDETAIL"]["06"][$report_xml["GRPDRAINTUBE"][0]["DATDETAIL"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "06" && $grp == 980 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATDETAIL"]["06"][convert_sub_item_id_to_code($con, $fname, $db_summary, "content", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故(事例)の内容</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
				<?
				// 検査
				} else if ($xml_summary == "08" || $data_summary == "08") {
				?>
					<tr height="25">
						<?
						// ==================================================
						// 種類
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATTYPE"]["08"][$report_xml["GRPINSPECTION"][0]["DATTYPE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 920 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATTYPE"]["08"][convert_sub_item_id_to_code($con, $fname, $db_summary, "kind", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 35) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 40) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：製造年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATDATEOFMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 45) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：購入年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATPURCHASE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 50) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">購入年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療機器等：直近の保守・点検年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATMAINTENANCE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 55) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">直近の保守・点検年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPMATERIAL"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 20) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 25) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：購入年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPINSPECTION"][0]["GRPMATERIAL"][0]["DATPURCHASE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 1000 && $item == 30) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">購入年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 発生場面
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATSCENE"]["08"][$report_xml["GRPINSPECTION"][0]["DATSCENE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 950 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATSCENE"]["08"][convert_sub_item_id_to_code($con, $fname, $db_summary, "scene", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場面</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 事故(事例)の内容
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATDETAIL"]["08"][$report_xml["GRPINSPECTION"][0]["DATDETAIL"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "08" && $grp == 980 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATDETAIL"]["08"][convert_sub_item_id_to_code($con, $fname, $db_summary, "content", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故(事例)の内容</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
				<?
				// 療養上の世話
				} else if ($xml_summary == "09" || $data_summary == "09") {
				?>
					<tr height="25">
						<?
						// ==================================================
						// 種類
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATTYPE"]["09"][$report_xml["GRPRECUPERATING"][0]["DATTYPE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "09" && $grp == 920 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATTYPE"]["09"][convert_sub_item_id_to_code($con, $fname, $db_summary, "kind", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：販売名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPRECUPERATING"][0]["GRPMATERIAL"][0]["DATNAME"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "09" && $grp == 1000 && $item == 20) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">販売名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：製造販売業者名
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPRECUPERATING"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "09" && $grp == 1000 && $item == 25) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">製造販売業者名</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 医療材料・諸物品等：購入年月
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $report_xml["GRPRECUPERATING"][0]["GRPMATERIAL"][0]["DATPURCHASE"][0]["_value"];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "09" && $grp == 1000 && $item == 30) {
											$disp = $input;
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">購入年月</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 発生場面
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATSCENE"]["09"][$report_xml["GRPRECUPERATING"][0]["DATSCENE"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "09" && $grp == 950 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATSCENE"]["09"][convert_sub_item_id_to_code($con, $fname, $db_summary, "scene", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場面</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
					<tr height="25">
						<?
						// ==================================================
						// 事故(事例)の内容
						// ==================================================
						// 比較用
						$compare = "";
						
						for ($i = 0; $i < $col_num; $i++) {
							$disp = "";
							
							$td_color = $normal_td_color;
							
							switch ($i) {
								case "0":
									$disp = $fix_arr["DATDETAIL"]["09"][$report_xml["GRPRECUPERATING"][0]["DATDETAIL"][0]["_attr"]["CODE"]];
									
									$compare = $disp;
									
									break;
								case "1":
									foreach ($db_data_arr as $db_data) {
										$grp = $db_data["grp_code"];
										$item = $db_data["easy_item_code"];
										$input = $db_data["input_item"];
										
										if ($data_summary == "09" && $grp == 980 && $item == 10) {
											// ==================================================
											// CODEデータの取得
											// convert_sub_item_id_to_code() from self
											// ==================================================
											$disp = $fix_arr["DATDETAIL"]["09"][convert_sub_item_id_to_code($con, $fname, $db_summary, "content", $input)];
											
											break;
										}
									}
									
									if ($compare != $disp) {
										$td_color = $unmatched_td_color;
									}
									
									break;
							}
							?>
							<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故(事例)の内容</font>
							</td>
							<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
								<?= $disp ?>
							</td>
						<?
						}
						?>
					</tr>
				<?
				}
				?>
				<tr height="25">
					<?
					// ==================================================
					// 実施した医療行為の目的
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $report_xml["DATPURPOSE"][0]["_value"];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 500 && $item == 10) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">実施した医療行為の目的</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 事故（事例）の内容
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $report_xml["DATCONTENTTEXT"][0]["_value"];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 520 && $item == 30) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故（事例）の内容</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// インスタンス作成
					// hiyari_report_class() from hiyari_report_class.php
					// ==================================================
					$rep_obj = new hiyari_report_class($con, $fname);
					
					// ==================================================
					// インシレベルに対する情報を取得
					// get_inci_level_infos() from hiyari_report_class.php
					// ==================================================
					$level_infos = $rep_obj->get_inci_level_infos();
					
					// ==================================================
					// 患者影響レベル
					// ==================================================
					// 比較用
					$compare = "";
					
					$xml_patient_level = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]["patient_level"];
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($level_infos as $info) {
									if ($xml_patient_level == $info["easy_code"]) {
										$disp = $info["easy_name"];
										break;
									}
								}
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 90 && $item == 10) {
										foreach ($level_infos as $info) {
											if ($input == $info["easy_code"]) {
												$disp = $info["easy_name"];
												break;
											}
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者影響レベル</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 発生要因
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								foreach ($report_xml["LSTFACTOR"][0]["DATFACTOR"] as $datrelcategory) {
									if ($disp != "") {
										$disp .= "、";
									}
									$disp .= $fix_arr["DATFACTOR"][$datrelcategory["_attr"]["CODE"]];
								}
								
								$compare = $disp;
								
								break;
							case "1":
								// ==================================================
								// DEFデータの取得
								// get_def_array() from self
								// ==================================================
								$def_arr = get_def_array($con, $fname, 600, 10);
								
								$tmp_arr = array();
								
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 600 && $item == 10) {
										$tmp_arr = explode("\t", $input);
										
										foreach ($tmp_arr as $tmp) {
											foreach ($def_arr as $def) {
												if ($tmp == $def["easy_code"]) {
													if ($disp != "") {
														$disp .= "、";
													}
													$disp .= $fix_arr["DATFACTOR"][trim($def["def_code"])];
													
													break;
												}
											}
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生要因</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 事故（事例）の背景要因の概要
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $report_xml["DATBACKGROUND"][0]["_value"];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 610 && $item == 20) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故（事例）の背景要因の概要</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 事故調査委員会設置の有無
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $fix_arr["DATCOMMITTEE"][$report_xml["DATCOMMITTEE"][0]["_attr"]["CODE"]];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 590 && $item == 90) {
										if ($input != "") {
											$disp = $fix_arr["DATCOMMITTEE"][substr("00" . $input, -2)];
										}
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事故調査委員会設置の有無</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
				<tr height="25">
					<?
					// ==================================================
					// 改善策
					// ==================================================
					// 比較用
					$compare = "";
					
					for ($i = 0; $i < $col_num; $i++) {
						$disp = "";
						
						$td_color = $normal_td_color;
						
						switch ($i) {
							case "0":
								$disp = $report_xml["DATIMPROVEMENTTEXT"][0]["_value"];
								
								$compare = $disp;
								
								break;
							case "1":
								foreach ($db_data_arr as $db_data) {
									$grp = $db_data["grp_code"];
									$item = $db_data["easy_item_code"];
									$input = $db_data["input_item"];
									
									if ($grp == 620 && $item == 30) {
										$disp = $input;
										
										if ($compare != $disp) {
											$td_color = $unmatched_td_color;
										}
										
										break;
									}
								}
								
								break;
						}
						?>
						<td align="center" bgcolor="#DFFFDC" width="<?= $th_width ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">改善策</font>
						</td>
						<td align="center" width="<?= $td_width ?>" bgcolor="<?= $td_color ?>" class="j12">
							<?= $disp ?>
						</td>
					<?
					}
					?>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- 本体 END -->

<?
// ==================================================
// フッターの表示
// show_hiyari_footer() from hiyari_common.ini
// ==================================================
show_hiyari_footer($session, $fname, false);
?>

<!-- BODY全体 END -->

</form>

</body>

<?
// DB接続の切断
pg_close($con);
?>

</html>

<?
// ==================================================
// HTML出力 END
// ==================================================

/**
 * 職種データの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @return array         職種データ配列
 */
function get_job_array($con, $fname)
{
	// 職種データ取得
	$sql = "select";
		$sql .= " a.job_id";
		$sql .= ", b.job_nm";
	$sql .= " from";
		$sql .= " (";
			$sql .= " select distinct";
				$sql .= " job_id";
			$sql .= " from";
				$sql .= " inci_set_job_link";
		$sql .= ") a";
		$sql .= " inner join";
		$sql .= " jobmst b";
		$sql .= " on";
		$sql .= " a.job_id = b.job_id";
	$sql .= " order by";
		$sql .= " a.job_id";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}

/**
 * 様式データの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @return array         様式データ配列
 */
function get_eis_array($con, $fname)
{
	$sql  = " select";
		$sql .= " jt.job_id";
		$sql .= ", et.eis_no";
		$sql .= ", et.eis_name";
		$sql .= ", lt.default_flg";
	$sql .= " from";
		$sql .= " inci_set_job_link lt";
		$sql .= " left join jobmst jt";
		$sql .= " on";
		$sql .= " lt.job_id = jt.job_id";
		$sql .= " left join inci_easyinput_set et";
		$sql .= " on";
		$sql .= " lt.eis_no = et.eis_no";
	$sql .= " where";
		$sql .= " not et.del_flag";			//jobmstの論理削除は判定しない。(論理削除済み職種でもその職種でレポートが作成されている可能性があるため)
	$sql .= " order by";
		$sql .= " jt.order_no";
		$sql .= ", et.eis_no";
	$cond = "";
	
	$result = select_from_table($con, $sql, $cond, $fname);
	if ($result == 0) {
		pg_close($con);
		showErrorPage();
		exit;
	}

	if (pg_numrows($result) == 0) {
		return false;
	} else {
		return pg_fetch_all($result);
	}
}

/**
 * 部門データの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @return array         部門データ配列
 */
function get_class_array($con, $fname) {
	$sql = "select";
		$sql .= " class_id";
		$sql .= ", class_nm";
	$sql .= " from";
		$sql .= " classmst";
	
	$cond = "where";
		$cond .= " class_del_flg = 'f'";
	$cond .= " order by";
		$cond .= " class_id";
	
	$sel_class = select_from_table($con, $sql, $cond, $fname);
	if ($sel_class == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_all($sel_class);
}

/**
 * 課データの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @return array         課データ配列
 */
function get_atrb_array($con, $fname) {
	$sql = "select";
		$sql .= " class_id";
		$sql .= ", atrb_id";
		$sql .= ", atrb_nm";
	$sql .= " from";
		$sql .= " atrbmst";
	
	$cond = "where";
		$cond .= " atrb_del_flg = 'f'";
	$cond .= " order by";
		$cond .= " class_id";
		$cond .= ", atrb_id";
	
	$sel_atrb = select_from_table($con, $sql, $cond, $fname);
	if ($sel_atrb == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_all($sel_atrb);
}

/**
 * 科データの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @return array         科データ配列
 */
function get_dept_array($con, $fname) {
	$sql = "select";
		$sql .= " atrb_id";
		$sql .= ", dept_id";
		$sql .= ", dept_nm";
	$sql .= " from";
		$sql .= " deptmst";
	
	$cond = "where";
		$cond .= " dept_del_flg = 'f'";
	$cond .= " order by";
		$cond .= " atrb_id";
		$cond .= ", dept_id";
	
	$sel_dept = select_from_table($con, $sql, $cond, $fname);
	if ($sel_dept == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_all($sel_dept);
}

/**
 * 室データの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @return array         室データ配列
 */
function get_room_array($con, $fname) {
	$sql = "select";
		$sql .= " dept_id";
		$sql .= ", room_id";
		$sql .= ", room_nm";
	$sql .= " from";
		$sql .= " classroom";
	
	$cond = "where";
		$cond .= " room_del_flg = 'f'";
	$cond .= " order b";
		$cond .= "y dept_id";
		$cond .= ", room_id";
	
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_all($sel_room);
}

/**
 * レポートの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @param  string $id    レポートID
 * @return array         室データ配列
 */
function get_report_array($con, $fname, $id) {
	$sql = "select";
		$sql .= " *";
	$sql .= " from";
		$sql .= " inci_report";
	
	$cond = "where";
		$cond .= " report_id = " . $id;
	
	$result = select_from_table($con, $sql, $cond, $fname);
	if ($result == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_all($result);
}

/**
 * レポートデータの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @param  string $id    データID
 * @return array         室データ配列
 */
function get_report_data_array($con, $fname, $id) {
	$sql = "select";
		$sql .= " grp_code";
		$sql .= ", easy_item_code";
		$sql .= ", input_item";
	$sql .= " from";
		$sql .= " inci_easyinput_data";
	
	$cond = "where";
		$cond .= " eid_id = " . $id;
	$cond .= " order by";
		$cond .= " grp_code";
		$cond .= ", easy_item_code";
	
	$result = select_from_table($con, $sql, $cond, $fname);
	if ($result == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_all($result);
}

/**
 * DEFデータの取得
 * 
 * @param  onject $con            接続コネクション
 * @param  string $fname          画面名
 * @param  int    $grp_code       グループコード
 * @param  int    $easy_item_code 項目コード
 * @return array                  DEFデータ配列
 */
function get_def_array($con, $fname, $grp_code, $easy_item_code)
{
	// データ取得
	$sql = "select";
		$sql .= " easy_code";
		$sql .= ", def_code";
	$sql .= " from";
		$sql .= " inci_report_materials";
	$cond = " where";
		$cond .= " grp_code = " . $grp_code;
		$cond .= " and easy_item_code = " . $easy_item_code;
	$cond .= " order by";
		$cond .= " def_code";
		$cond .= ", easy_code";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}

/**
 * CODEデータの取得(概要)
 * 
 * @param  onject $con     接続コネクション
 * @param  string $fname   画面名
 * @param  string $db_code データベースコード
 * @return string          CODE
 */
function convert_super_item_id_to_code($con, $fname, $db_code)
{
	// db_codeが指定されない場合は空文字を返す
	if ($db_code == "") {
		return "";
	}
	
	// 職種データ取得
	$sql = "select";
		$sql .= " code";
	$sql .= " from";
		$sql .= " inci_super_item_2010";
	
	$cond = " where";
		$cond .= " super_item_id = '" . $db_code . "'";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$row = pg_fetch_row($sel);
	
	return trim($row[0]);
}

/**
 * CODEデータの取得(TYPE、SCENE、DETAIL)
 * 
 * @param  onject $con     接続コネクション
 * @param  string $fname   画面名
 * @param  string $super   概要
 * @param  string $div     種類［DATTYPE="kind"、DATSCENE="scene"、DATDETAIL="content"］
 * @param  string $db_code データベースコード
 * @return string          CODE
 */
function convert_sub_item_id_to_code($con, $fname, $super, $div, $db_code)
{
	// xml_codeが指定されない場合は空文字を返す
	if ($db_code == "") {
		return "";
	}
	
	// 職種データ取得
	$sql = "select";
		$sql .= " code";
	$sql .= " from";
		$sql .= " inci_sub_item_2010";
	
	$cond = " where";
		$cond .= " super_item_id = " . $super;
		$cond .= " and item_code = '" . $div . "'";
		$cond .= " and sub_item_id = " . $db_code;
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$row = pg_fetch_row($sel);
	
	return trim($row[0]);
}

/**
 * XMLの固定内容データを連想配列に格納
 *
 * @return array 固定内容データ配列
 */
function get_fix_xml_data_array()
{
	$ret = array();
	
	$ret = array(
		"DATMONTH" => array(
			"01" => "１月"
			, "02" => "２月"
			, "03" => "３月"
			, "04" => "４月"
			, "05" => "５月"
			, "06" => "６月"
			, "07" => "７月"
			, "08" => "８月"
			, "09" => "９月"
			, "10" => "１０月"
			, "11" => "１１月"
			, "12" => "１２月"
		)
		, "DATDAY" => array(
			"01" => "日曜日"
			, "02" => "月曜日"
			, "03" => "火曜日"
			, "04" => "水曜日"
			, "05" => "木曜日"
			, "06" => "金曜日"
			, "07" => "土曜日"
		)
		, "DATHOLIDAY" => array(
			"01" => "平日"
			, "02" => "休日・祝日"
		)
		, "DATTIMEZONE" => array(
			"01" => "0:00〜1:59"
			, "02" => "2:00〜3:59"
			, "03" => "4:00〜5:59"
			, "04" => "6:00〜7:59"
			, "05" => "8:00〜9:59"
			, "06" => "10:00〜11:59"
			, "07" => "12:00〜13:59"
			, "08" => "14:00〜15:59"
			, "09" => "16:00〜17:59"
			, "10" => "18:00〜19:59"
			, "11" => "20:00〜21:59"
			, "12" => "22:00〜23:59"
			, "90" => "不明"
		)
		, "DATEXECUTION" => array(
			"01" => "実施あり"
			, "02" => "実施なし"
		)
		, "DATLEVEL" => array(
			"01" => "濃厚な治療"
			, "02" => "軽微な治療"
			, "03" => "治療なし"
			, "90" => "不明"
		)
		, "DATSEQUELAE" => array(
			"01" => "死亡"
			, "02" => "障害残存の可能性がある(高い)"
			, "03" => "障害残存の可能性がある(低い)"
			, "04" => "障害残存の可能性なし"
			, "05" => "障害なし"
			, "90" => "不明"
		)
		, "DATINFLUENCE" => array(
			"01" => "死亡もしくは重篤な状況に至ったと考えられる"
			, "02" => "濃厚な処置・治療が必要であると考えられる"
			, "03" => "軽微な処置・治療が必要もしくは処置・治療が不要と考えられる"
		)
		, "DATSITE" => array(
			"01" => "外来診察室"
			, "02" => "外来処置室"
			, "03" => "外来待合室"
			, "04" => "救急外来"
			, "05" => "救命救急センター"
			, "06" => "病室"
			, "07" => "病棟処置室"
			, "08" => "手術室"
			, "09" => "ICU"
			, "10" => "CCU"
			, "11" => "NICU"
			, "12" => "検査室"
			, "13" => "カテーテル検査室"
			, "14" => "放射線治療室"
			, "15" => "放射線撮影室"
			, "16" => "核医学検査室"
			, "17" => "透析室"
			, "18" => "分娩室"
			, "19" => "機能訓練室"
			, "20" => "トイレ"
			, "21" => "廊下"
			, "22" => "浴室"
			, "23" => "階段"
			, "24" => "薬局(調剤所)"
			, "90" => "不明"
			, "99" => "その他"
		)
		, "DATSUMMARY" => array(
			"02" => "薬剤"
			, "03" => "輸血"
			, "04" => "治療・処置"
			, "10" => "医療機器等"
			, "06" => "ドレーン・チューブ"
			, "08" => "検査"
			, "09" => "療養上の世話"
			, "99" => "その他"
		)
		, "DATSPECIALLY" => array(
			"090105" => "汚染された薬剤・材料・生体由来材料等の使用による事故"
			, "090107" => "院内感染による死亡や障害"
			, "090201" => "患者の自殺又は自殺企図"
			, "090202" => "入院患者の失踪"
			, "090204" => "患者の熱傷"
			, "090205" => "患者の感電"
			, "090206" => "医療施設内の火災による患者の死亡や障害"
			, "090207" => "間違った保護者の許への新生児の引渡し"
			, "999999" => "本事例は選択肢には該当しない"
		)
		, "DATDEPARTMENT" => array(
			"01" => "内科"
			, "02" => "麻酔科"
			, "03" => "循環器内科"
			, "04" => "神経科"
			, "05" => "呼吸器内科"
			, "06" => "消化器科"
			, "07" => "血液内科"
			, "08" => "循環器外科"
			, "09" => "アレルギー科"
			, "10" => "リウマチ科"
			, "11" => "小児科"
			, "12" => "外科"
			, "13" => "整形外科"
			, "14" => "形成外科"
			, "15" => "美容外科"
			, "16" => "脳神経外科"
			, "17" => "呼吸器外科"
			, "18" => "心臓血管外科"
			, "19" => "小児外科"
			, "20" => "ペインクリニック"
			, "21" => "皮膚科"
			, "22" => "泌尿器科"
			, "23" => "性病科"
			, "24" => "肛門科"
			, "25" => "産婦人科"
			, "26" => "産科"
			, "27" => "婦人科"
			, "28" => "眼科"
			, "29" => "耳鼻咽喉科"
			, "30" => "心療内科"
			, "31" => "精神科"
			, "32" => "リハビリテーション科"
			, "33" => "放射線科"
			, "34" => "歯科"
			, "35" => "矯正歯科"
			, "36" => "小児歯科"
			, "37" => "歯科口腔外科"
			, "90" => "不明"
			, "99" => "その他"
		)
		, "DATPATIENT" => array(
			"1" => "１人"
			, "2" => "複数人"
		)
		, "DATPATIENTAGE" => array(
			"years" => "歳"
			, "months" => "ヶ月"
		)
		, "DATPATIENTSEX" => array(
			"01" => "男性"
			, "02" => "女性"
		)
		, "DATPATIENTDIV" => array(
			"01" => "入院"
			, "04" => "外来"
		)
		, "DATDISEASE" => array(
			"disease0" => "事故(事例)に直接関連する疾患名"
			, "disease1" => "関連する疾患名１"
			, "disease2" => "関連する疾患名２"
			, "disease3" => "関連する疾患名３"
		)
		, "DATPATIENTSTATE" => array(
			"01" => "意識障害"
			, "02" => "視覚障害"
			, "03" => "聴覚障害"
			, "04" => "構音障害"
			, "05" => "精神障害"
			, "06" => "認知症・健忘"
			, "07" => "上肢障害"
			, "08" => "下肢障害"
			, "09" => "歩行障害"
			, "10" => "床上安静"
			, "11" => "睡眠中"
			, "13" => "薬剤の影響下"
			, "14" => "麻酔中・麻酔前後"
			, "99" => "その他特記する心身状態あり"
		)
		, "DATDISCOVERER" => array(
			"01" => "当事者本人"
			, "02" => "同職種者"
			, "03" => "他職種者"
			, "04" => "患者本人"
			, "05" => "家族・付き添い"
			, "06" => "他患者"
			, "99" => "その他"
		)
		, "LSTCONCERNED" => array(
			"01" => "１人"
			, "02" => "２人"
			, "03" => "３人"
			, "04" => "４人"
			, "05" => "５人"
			, "06" => "６人"
			, "07" => "７人"
			, "08" => "８人"
			, "09" => "９人"
			, "10" => "１０人"
			, "90" => "１１人以上"
		)
		, "DATCONCERNED" => array(
			"01" => "医師"
			, "02" => "歯科医師"
			, "03" => "看護師"
			, "04" => "准看護師"
			, "05" => "薬剤師"
			, "06" => "臨床工学技士"
			, "07" => "助産師"
			, "08" => "看護助手"
			, "09" => "診療放射線技師"
			, "10" => "臨床検査技師"
			, "12" => "管理栄養士"
			, "13" => "栄養士"
			, "14" => "調理師・調理従事者"
			, "11" => "理学療法士(PT)"
			, "16" => "作業療法士(OT)"
			, "15" => "言語聴覚士(ST)"
			, "17" => "衛生検査技師"
			, "18" => "歯科衛生士"
			, "19" => "歯科技工士"
			, "99" => "その他"
		)
		, "DATEXPERIENCE" => array(
			"years" => "年"
			, "months" => "ヶ月"
		)
		, "DATLENGTH" => array(
			"years" => "年"
			, "months" => "ヶ月"
		)
		, "DATDUTY" => array(
			"00" => "０回"
			, "01" => "１回"
			, "02" => "２回"
			, "03" => "３回"
			, "04" => "４回"
			, "05" => "５回"
			, "06" => "６回"
			, "07" => "７回"
			, "90" => "不明"
		)
		, "DATSHIFT" => array(
			"02" => "２交替"
			, "03" => "３交替"
			, "04" => "４交替"
			, "08" => "交替勤務なし"
			, "99" => "その他"
		)
		, "DATRELCATEGORY" => array(
			"01" => "医師"
			, "02" => "歯科医師"
			, "03" => "看護師"
			, "04" => "准看護師"
			, "05" => "薬剤師"
			, "06" => "臨床工学技士"
			, "07" => "助産師"
			, "08" => "看護助手"
			, "09" => "診療放射線技師"
			, "10" => "臨床検査技師"
			, "12" => "管理栄養士"
			, "13" => "栄養士"
			, "14" => "調理師・調理従事者"
			, "11" => "理学療法士(PT)"
			, "16" => "作業療法士(OT)"
			, "15" => "言語聴覚士(ST)"
			, "17" => "衛生検査技師"
			, "18" => "歯科衛生士"
			, "19" => "歯科技工士"
			, "99" => "その他"
		)
		, "DATTYPE" => array(
			// 薬剤
			"02" => array(
				"020401" => "血液製剤"
				, "020402" => "麻薬"
				, "020403" => "抗腫瘍剤"
				, "020404" => "循環器用薬"
				, "020405" => "抗糖尿病薬"
				, "020406" => "抗不安剤"
				, "020407" => "睡眠導入剤"
				, "020408" => "抗凝固剤"
				, "020499" => "その他"
			)
			// 治療・処置
			, "04" => array(
				"040101" => "開頭"
				, "040102" => "開胸"
				, "040103" => "開心"
				, "040104" => "開腹"
				, "040105" => "四肢"
				, "040106" => "鏡視下手術"
				, "040199" => "その他の手術"
				, "040201" => "全身麻酔(吸入麻酔+静脈麻酔)"
				, "040202" => "局所麻酔"
				, "040203" => "吸引麻酔"
				, "040204" => "静脈麻酔"
				, "040205" => "脊椎・硬膜外麻酔"
				, "040299" => "その他の麻酔"
				, "040301" => "経膣分娩"
				, "040302" => "帝王切開"
				, "040303" => "人工妊娠中絶"
				, "040399" => "その他の分娩・人工妊娠中絶等"
				, "040401" => "血液浄化療法(血液透析含む)"
				, "040402" => "IVR(血液カテーテル治療等)"
				, "040403" => "放射線治療"
				, "040404" => "ペインクリニック"
				, "040405" => "リハビリテーション"
				, "040406" => "観血的歯科治療"
				, "040407" => "非観血的歯科治療"
				, "040408" => "内視鏡的治療"
				, "040499" => "その他の治療"
				, "040501" => "中心静脈ライン"
				, "040502" => "末梢静脈ライン"
				, "040503" => "動脈ライン"
				, "040504" => "血液浄化用カテーテル"
				, "040505" => "栄養チューブ(NG・ED)"
				, "040506" => "尿道カテーテル"
				, "040509" => "ドレーンに関する処置"
				, "040508" => "創傷処置"
				, "040599" => "その他の一般的処置"
				, "040601" => "気管挿管"
				, "040602" => "気管切開"
				, "040603" => "心臓マッサージ"
				, "040604" => "酸素療法"
				, "040605" => "血管確保"
				, "040699" => "その他の救急措置"
			)
			// 医療機器等
			, "10" => array(
				"050101" => "人工呼吸器"
				, "050102" => "酸素療法機器"
				, "050103" => "麻酔器"
				, "050104" => "人工心肺"
				, "050105" => "除細動器"
				, "050106" => "IABP"
				, "050107" => "ペースメーカー"
				, "050108" => "輸血・輸注ポンプ"
				, "050109" => "血液浄化用機器"
				, "050110" => "保育器"
				, "050111" => "内視鏡"
				, "050112" => "低圧持続吸引器"
				, "050113" => "心電図・血液モニター"
				, "050114" => "パルスオキシメーター"
				, "050115" => "高気圧酸素療法装置"
				, "050116" => "歯科用回転研削器具"
				, "050117" => "歯科用根管治療用器具"
				, "050118" => "歯科補綴物・充填物"
				, "050199" => "その他の医療機器等"
			)
			// ドレーン・チューブ
			, "06" => array(
				"060101" => "中心静脈ライン"
				, "060102" => "末梢静脈ライン"
				, "060103" => "動脈ライン"
				, "060104" => "気管チューブ"
				, "060105" => "気管カニューレ"
				, "060106" => "栄養チューブ(NG・ED)"
				, "060107" => "尿道カテーテル"
				, "060108" => "胸腔ドレーン"
				, "060109" => "腹腔ドレーン"
				, "060110" => "脳室・脳槽ドレーン"
				, "060111" => "皮下持続吸引ドレーン"
				, "060112" => "硬膜外カテーテル"
				, "060113" => "血液浄化用カテーテル・回路"
				, "060114" => "三方活栓"
				, "060199" => "その他のドレーン・チューブ類"
			)
			// 検査
			, "08" => array(
				"080101" => "採血"
				, "080102" => "採尿"
				, "080103" => "採便"
				, "080104" => "採痰"
				, "080105" => "穿刺液"
				, "080199" => "その他の検体採取"
				, "080201" => "超音波検査"
				, "080202" => "心電図検査"
				, "080203" => "トレッドミル検査"
				, "080204" => "ホルター負荷心電図"
				, "080205" => "脳波検査"
				, "080206" => "筋電図検査"
				, "080207" => "肺機能検査"
				, "080299" => "その他の生理検査"
				, "080301" => "一般撮影"
				, "080302" => "ポータブル撮影"
				, "080303" => "CT"
				, "080304" => "MRI"
				, "080305" => "血管カテーテル撮影"
				, "080306" => "上部消化管撮影"
				, "080307" => "下部消化管撮影"
				, "080399" => "その他の画像検査"
				, "080401" => "上部消化管"
				, "080402" => "下部消化管"
				, "080403" => "気管支鏡"
				, "080499" => "その他の内視鏡検査"
				, "080501" => "耳鼻科検査"
				, "080502" => "眼科検査"
				, "080503" => "歯科検査"
				, "080505" => "検体検査"
				, "080506" => "血糖測定(病棟で実施したもの)"
				, "080507" => "病理検査"
				, "080508" => "核医学検査"
				, "080599" => "その他の機能検査"
			)
			// 療養上の世話
			, "09" => array(
				"090101" => "気管内・口腔内吸引"
				, "090102" => "体位交換"
				, "090103" => "清拭"
				, "090104" => "更衣介助"
				, "090106" => "入浴介助"
				, "090107" => "排泄介助"
				, "090109" => "移動介助"
				, "090110" => "搬送・移送"
				, "090115" => "罨法"
				, "090114" => "患者観察"
				, "090112" => "患者周辺物品管理"
				, "090111" => "体温管理"
				, "090113" => "配膳"
				, "090199" => "その他の療養上の世話"
				, "090201" => "経口摂取"
				, "090202" => "経管栄養"
				, "090203" => "食事介助"
				, "090301" => "散歩中"
				, "090302" => "移動中"
				, "090303" => "外出・外泊中"
				, "090304" => "食事中"
				, "090305" => "入浴中"
				, "090306" => "着替え中"
				, "090307" => "排泄中"
				, "090308" => "就寝中"
				, "090399" => "その他の療養上の場面"
			)
		)
		, "DATSCENE" => array(
			// 薬剤
			"02" => array(
				"010201" => "手書きによる処方箋の作成"
				, "010202" => "オーダリングによる処方箋の作成"
				, "010203" => "口頭による処方指示"
				, "010204" => "手書きによる処方の変更"
				, "010205" => "オーダリングによる処方の変更"
				, "010206" => "口頭による処方の変更"
				, "010299" => "その他の処方に関する場面"
				, "020401" => "内服薬調剤"
				, "020402" => "注射薬調剤"
				, "020403" => "血液製剤調剤"
				, "020404" => "外用薬調剤"
				, "020499" => "その他の調剤に関する場面"
				, "020301" => "内服薬製剤管理"
				, "020302" => "注射薬製剤管理"
				, "020303" => "血液製剤管理"
				, "020304" => "外用薬製剤管理"
				, "020399" => "その他の製剤管理に関する場面"
				, "020101" => "与薬準備"
				, "020201" => "皮下・筋肉注射"
				, "020202" => "静脈注射"
				, "020203" => "動脈注射"
				, "020204" => "抹消静脈点滴"
				, "020205" => "中心静脈注射"
				, "020206" => "内服"
				, "020207" => "外用"
				, "020208" => "坐剤"
				, "020209" => "吸入"
				, "020210" => "点鼻・点耳・点眼"
				, "020299" => "その他の与薬に関する場面"
			)
			// 輸血
			, "03" => array(
				"010201" => "手書きによる処方箋の作成"
				, "010202" => "オーダリングによる処方箋の作成"
				, "010203" => "口頭による処方指示"
				, "010204" => "手書きによる処方の変更"
				, "010205" => "オーダリングによる処方の変更"
				, "010206" => "口頭による処方の変更"
				, "010299" => "その他の処方に関する場面"
				, "030101" => "準備"
				, "030102" => "実施"
				, "030199" => "その他の輸血検査に関する場面"
				, "030201" => "準備"
				, "030202" => "実施"
				, "030299" => "その他の放射線照射に関する場面"
				, "030301" => "製剤の交付"
				, "030399" => "その他の輸血準備に関する場面"
				, "030401" => "実施"
				, "030499" => "その他の輸血実施に関する場面"
			)
			// 治療・処置
			, "04" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040199" => "その他の管理に関する場面"
				, "040201" => "準備"
				, "040299" => "その他の準備に関する場面"
				, "040301" => "実施"
				, "040399" => "その他の治療・処置に関する場面"
			)
			// 医療機器等
			, "10" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// ドレーン・チューブ
			, "06" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// 検査
			, "08" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// 療養上の世話
			, "09" => array(
				"010301" => "手書きによる計画又は指示の作成"
				, "010302" => "オーダリングによる計画又は指示の作成"
				, "010303" => "口頭による計画又は指示"
				, "010304" => "手書きによる計画又は指示の変更"
				, "010305" => "オーダリングによる計画又は指示の変更"
				, "010306" => "口頭による計画又は指示の変更"
				, "010399" => "その他の計画又は指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "実施中"
			)
		)
		, "DATDETAIL" => array(
			// 薬剤
			"02" => array(
				"010201" => "処方忘れ"
				, "010202" => "処方遅延"
				, "010203" => "処方量間違い"
				, "010204" => "重複処方"
				, "010205" => "禁忌薬剤の処方"
				, "010206" => "対象患者処方間違い"
				, "010207" => "処方薬剤間違い"
				, "010208" => "処方単位間違い"
				, "010209" => "投与方法処方間違い"
				, "010299" => "その他の処方に関する内容"
				
				, "020212" => "調剤忘れ"
				, "020201" => "処方箋・注射箋鑑査間違い"
				, "020202" => "秤量間違い調剤"
				, "020203" => "数量間違い"
				, "020204" => "分包間違い"
				, "020205" => "規格間違い調剤"
				, "020206" => "単位間違い調剤"
				, "020207" => "薬剤取り違え調剤"
				, "020208" => "説明文書の取り違え"
				, "020209" => "交付患者間違い"
				, "020210" => "薬剤・製剤の取り違え交付"
				, "020211" => "期限切れ製剤の交付"
				, "020299" => "その他の調剤に関する内容"
				
				, "020302" => "薬袋・ボトルの記載間違い"
				, "020305" => "異物混入"
				, "020306" => "細菌汚染"
				, "020307" => "期限切れ製剤"
				, "020399" => "その他の製剤管理に関する内容"
				
				, "020114" => "過剰与薬準備"
				, "020115" => "過少与薬準備"
				, "020104" => "与薬時間・日付間違い"
				, "020105" => "重複与薬"
				, "020116" => "禁忌薬剤の与薬"
				, "020107" => "投与速度速すぎ"
				, "020108" => "投与速度遅すぎ"
				, "020109" => "患者間違い"
				, "020110" => "薬剤間違い"
				, "020111" => "単位間違い"
				, "020112" => "投与方法間違い"
				, "020113" => "無投薬"
				, "020117" => "混合間違い"
				, "020199" => "その他の与薬準備に関する内容"
				
				, "020414" => "過剰投与"
				, "020415" => "過少投与"
				, "020404" => "投与時間・日付間違い"
				, "020405" => "重複投与"
				, "020416" => "禁忌薬剤の投与"
				, "020407" => "投与速度速すぎ"
				, "020408" => "投与速度遅すぎ"
				, "020409" => "患者間違い"
				, "020410" => "薬剤間違い"
				, "020411" => "単位間違い"
				, "020412" => "投与方法間違い"
				, "020413" => "無投薬"
				, "020499" => "その他の与薬に関する内容"
			)
			// 輸血
			, "03" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010110" => "指示量間違い"
				, "010111" => "重複指示"
				, "010112" => "禁忌薬剤の指示"
				, "010113" => "対象患者指示間違い"
				, "010114" => "指示薬剤間違い"
				, "010115" => "指示単位間違い"
				, "010116" => "投与方法指示間違い"
				, "010199" => "その他の指示出しに関する内容"
				
				, "030101" => "未実施"
				, "030102" => "検体取り違え"
				, "030105" => "判定間違い"
				, "030104" => "結果記入・入力間違い"
				, "030199" => "その他の輸血検査に関する内容"
				
				, "030201" => "未実施"
				, "030202" => "過剰照射"
				, "030205" => "過少照射"
				, "030203" => "患者間違い"
				, "030204" => "製剤間違い"
				, "030299" => "その他の放射線照射に関する内容"
				
				, "020302" => "薬袋・ボトルの記載間違い"
				, "020305" => "異物混入"
				, "020306" => "細菌汚染"
				, "020307" => "期限切れ製剤"
				, "020399" => "その他の輸血管理に関する内容"
				
				, "020114" => "過剰与薬準備"
				, "020115" => "過少与薬準備"
				, "020104" => "与薬時間・日付間違い"
				, "020105" => "重複与薬"
				, "020116" => "禁忌薬剤の与薬"
				, "020107" => "投与速度速すぎ"
				, "020108" => "投与速度遅すぎ"
				, "020109" => "患者間違い"
				, "020110" => "薬剤間違い"
				, "020111" => "単位間違い"
				, "020112" => "投与方法間違い"
				, "020113" => "無投薬"
				, "020199" => "その他の輸血準備に関する内容"
				
				, "020214" => "過剰投与"
				, "020215" => "過少投与"
				, "020204" => "投与時間・日付間違い"
				, "020205" => "重複投与"
				, "020216" => "禁忌薬剤の投与"
				, "020207" => "投与速度速すぎ"
				, "020208" => "投与速度遅すぎ"
				, "020209" => "患者間違い"
				, "020210" => "薬剤間違い"
				, "020211" => "単位間違い"
				, "020212" => "投与方法間違い"
				, "020213" => "無投薬"
				, "020299" => "その他の輸血実施に関する内容"
			)
			// 治療・処置
			, "04" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010116" => "治療・処置指示間違い"
				, "010117" => "日程間違い"
				, "010118" => "時間間違い"
				, "010199" => "その他の治療・処置の指示に関する内容"
				
				, "040301" => "治療・処置の管理"
				, "040399" => "その他の治療・処置の管理に関する内容"
				
				, "040201" => "医療材料取り違え"
				, "040202" => "患者体位の誤り"
				, "040203" => "消毒・清潔操作の誤り"
				, "040299" => "その他の治療・処置の準備に関する内容"
				
				, "040101" => "患者間違い"
				, "040102" => "部位取違え"
				, "040105" => "方法(手技)の誤り"
				, "040106" => "未実施・忘れ"
				, "040107" => "中止・延期"
				, "040108" => "日程・時間の誤り"
				, "040109" => "順番の誤り"
				, "040110" => "不必要行為の実施"
				, "040114" => "誤嚥"
				, "040115" => "誤飲"
				, "040116" => "異物の体内残存"
				, "040104" => "診察・治療・処置等その他の取違え"
				, "040199" => "その他の治療・処置の実施に関する内容"
			)
			// 医療機器等
			, "10" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "使用方法指示間違い"
				, "010199" => "その他の医療機器等・医療材料の使用に関する内容"
				
				, "050301" => "保守・点検不良"
				, "050302" => "保守・点検忘れ"
				, "050303" => "使用中の点検・管理ミス"
				, "050304" => "破損"
				, "050399" => "その他の医療機器等・医療材料の管理に関する内容"
				
				, "050201" => "組み立て"
				, "050202" => "設定条件間違い"
				, "050203" => "設定忘れ"
				, "050204" => "電源入れ忘れ"
				, "050205" => "警報設定忘れ"
				, "050206" => "警報設定範囲間違い"
				, "050207" => "便宜上の警報解除後の再設定忘れ"
				, "050208" => "消毒・清潔操作の誤り"
				, "050209" => "使用前の点検・管理ミス"
				, "050210" => "破損"
				, "050299" => "その他の医療機器等・医療材料の準備に関する内容"
				
				, "050105" => "医療機器等・医療材料の不適切使用"
				, "050104" => "誤作動"
				, "050106" => "故障"
				, "050116" => "破損"
				, "050199" => "その他の医療機器等・医療材料の使用に関する内容"
			)
			// ドレーン・チューブ
			, "06" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "使用方法指示間違い"
				, "010199" => "その他のドレーン・チューブ類の使用・管理の指示に関する内容"
				
				, "060301" => "点検忘れ"
				, "060302" => "点検不良"
				, "060303" => "使用中の点検・管理ミス"
				, "060304" => "破損"
				, "060399" => "その他のドレーン・チューブ類の管理に関する内容"
				
				, "060201" => "組み立て"
				, "060202" => "設定条件間違い"
				, "060203" => "設定忘れ"
				, "060204" => "消毒・清潔操作の誤り"
				, "060205" => "使用前の点検・管理ミス"
				, "060299" => "その他のドレーン・チュ−ブ類の準備に関する内容"
				
				, "060101" => "点滴漏れ"
				, "060102" => "自己抜去"
				, "060103" => "自然抜去"
				, "060104" => "接続はずれ"
				, "060105" => "未接続"
				, "060106" => "閉塞"
				, "060107" => "切断・破損"
				, "060108" => "接続間違い"
				, "060109" => "三方活栓操作間違い"
				, "060110" => "ルートクランプエラー"
				, "060111" => "空気混入"
				, "060112" => "誤作動"
				, "060113" => "故障"
				, "060114" => "ドレーン・チューブ類の不適切使用"
				, "060199" => "その他のドレーン・チューブ類の使用に関する内容"
			)
			// 検査
			, "08" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "指示検査の間違い"
				, "010199" => "その他の検査の指示に関する内容"
				
				, "070301" => "分析機器・器具管理"
				, "070302" => "試薬管理"
				, "070303" => "データ紛失"
				, "070304" => "計算・入力・暗記"
				, "070399" => "その他の検査の管理に関する内容"
				
				, "070201" => "患者取違え"
				, "070204" => "検体取違え"
				, "070205" => "検体紛失"
				, "070210" => "検査機器・器具の準備"
				, "070206" => "検体破損"
				, "070299" => "その他の検査の準備に関する内容"
				
				, "070101" => "患者取違え"
				, "070104" => "検体取違え"
				, "070115" => "試薬の間違い"
				, "070105" => "検体紛失"
				, "070102" => "検査の手技・判定技術の間違い"
				, "070103" => "検体採取時のミス"
				, "070106" => "検体破損"
				, "070107" => "検体のコンタミネーション"
				, "070111" => "データ取違え"
				, "070114" => "結果報告"
				, "070199" => "その他の検査の実施に関する内容"
			)
			// 療養上の世話
			, "09" => array(
				"010101" => "計画忘れ又は指示出し忘れ"
				, "010102" => "計画又は指示の遅延"
				, "010111" => "計画又は指示の対象患者間違い"
				, "010119" => "計画又は指示内容間違い"
				, "010199" => "その他の療養上の世話の計画又は指示に関する内容"
				
				, "080104" => "拘束・抑制"
				, "080505" => "給食の内容の間違い"
				, "080109" => "安静指示"
				, "080110" => "禁食指示"
				, "080306" => "外出・外泊許可"
				, "080405" => "異物混入"
				, "080101" => "転倒"
				, "080102" => "転落"
				, "080103" => "衝突"
				, "080106" => "誤嚥"
				, "080107" => "誤飲"
				, "080108" => "誤配膳"
				, "080202" => "遅延"
				, "080203" => "実施忘れ"
				, "080204" => "搬送先間違い"
				, "080205" => "患者間違い"
				, "080404" => "延食忘れ"
				, "080403" => "中止の忘れ"
				, "080301" => "自己管理薬飲み忘れ・注射忘れ"
				, "080305" => "自己管理薬注入忘れ"
				, "080303" => "自己管理薬取違え摂取"
				, "080406" => "不必要行為の実施"
				, "089999" => "その他の療養上の世話の管理・準備・実施に関する内容"
			)
		)
		, "DATFACTOR" => array(
			"010101" => "確認を怠った"
			, "010102" => "観察を怠った"
			, "010103" => "報告が遅れた(怠った)"
			, "010104" => "記録などに不備があった"
			, "010105" => "連携ができていなかった"
			, "010106" => "患者への説明が不十分であった(怠った)"
			, "010107" => "判断を誤った"
			, "010201" => "知識が不足していた"
			, "010202" => "技術・手技が未熟だった"
			, "010203" => "勤務状況が繁忙だった"
			, "010204" => "通常とは異なる身体的条件下にあった"
			, "010205" => "通常とは異なる心理的条件下にあった"
			, "010299" => "その他"
			, "010301" => "コンピュータシステム"
			, "010302" => "医薬品"
			, "010303" => "医療機器"
			, "010304" => "施設・設備"
			, "010305" => "諸物品"
			, "010306" => "患者側"
			, "010399" => "その他"
			, "010401" => "教育・訓練"
			, "010402" => "仕組み"
			, "010403" => "ルールの不備"
			, "010499" => "その他"
		)
		, "DATCOMMITTEE" => array(
			"05" => "既設の医療安全に関する委員会等で対応"
			, "07" => "内部調査委員会設置(予定も含む)"
			, "08" => "外部調査委員会設置(予定も含む)"
			, "06" => "現在顕著宇宙で対応は未定"
			, "99" => "その他"
		)
	);
	
	return $ret;
}
?>