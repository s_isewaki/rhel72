<?
// ┌────────────────────────────────────────────────┐
// │ ★★★ これは非利用の可能性があります                                                          │
// └────────────────────────────────────────────────┘
// ┌────────────────────────────────────────────────┐
// │ ★★★ comedix/password_change_from_launcher.phpから呼ばれます                                 │
// │ ★★★ comedix/password_change_from_launcher.php自体、非利用の可能性があります                 │
// └────────────────────────────────────────────────┘
require_once("common.php"); // 読込みと同時にセッションチェックあり


$id = $_REQUEST["id"]; // ログインIDはリクエストから。恐らくGETで来る

$errmsg = "";
$_emp_id = c2dbGetOne("select emp_id from login where emp_login_id = ".c2dbStr($id));
if (!$id) $errmsg = "パスワード変更権限がありません。(ログインID不明)"; // 同じ
else if (!$_emp_id) $errmsg = "パスワード変更権限がありません。(職員不明)"; // 同じ
else if (!$emp_pass_flg) $errmsg = "パスワード変更権限がありません。"; // 同じ。


c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");
?>
<title>パスワード変更</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="pass/css/style.css">
</head>
<body style="margin:5px; padding:0">



<table class="dialog_title" cellspacing="0" cellpadding="0">
    <tr><th>パスワード変更</th><td class="closebtn"><a href="javascript:void(0);" onclick="window.close();"></a></td></tr>
</table>



<?if ($errmsg){ ?><div style="padding:20px; text-align:center"><?=$errmsg?></div><? } ?>


<?if ($_REQUEST["succeed"]=="1"){ ?><div style="padding:5px; text-align:center">パスワード更新に成功しました。</div><? } // これが表示されることは無い ?>



<? if (!$errmsg) { ?>
<form action="passwd_change_from_launcher_exe.php" method="post">
<input type="hidden" name="id" value="<?=hh($id)?>" />

	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
		<tr>
			<th>ID</th>
			<td><?=hh($id)?></td>
		</tr>
		<tr>
			<th>旧パスワード</th>
			<td><input type="password" name="cur_passwd" style="ime-mode:inactive;" autocomplete="off"></td>
		</tr>
		<tr>
			<th>新パスワード</th>
			<td><input type="password" name="new_passwd" style="ime-mode:inactive;" autocomplete="off"></td>
		</tr>
		<tr>
			<th>新パスワード確認</th>
			<td><input type="password" name="cfm_passwd" style="ime-mode:inactive;" autocomplete="off"></td>
		</tr>
	</table>


	<div style="text-align:right; padding:2px">
		<input type="submit" value="更新">
	</div>

</form>
<? } ?>



</body>
</html>
