<?
// ┌────────────────────────────────────────────────┐
// │ ★★★ ランチャー(cmxlauncher.exe)から呼ばれます                                               │
// └────────────────────────────────────────────────┘
// ┌────────────────────────────────────────────────┐
// │ ★★★ comedix/passwd_change_from_launcher_exe.phpから呼ばれます                               │
// └────────────────────────────────────────────────┘

ob_start();
require_once("about_postgres.php");
require_once("about_authority.php");
//XX require_once("community/mainfile.php");
require_once("webmail/config/config.php");
require_once("passwd_change_common.ini");

$fname = $PHP_SELF;
$result = change_password($fname, $_POST["id"], $_POST["cur_passwd"], $_POST["new_passwd"], $_POST["cfm_passwd"], $imapServerAddress);

ob_end_clean();
show_result($result);

function change_password($fname, $id, $cur_passwd, $new_passwd, $cfm_passwd, $imapServerAddress) {
	$con = connect2db($fname);

	// 権限チェック
	$accessible = false;
	$sql = "select emp_id from login";
	$cond = "where emp_login_id = '$id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		return error_result("システムエラーが発生しました。");
	}
	if (pg_num_rows($sel) > 0) {
		$emp_id = pg_fetch_result($sel, 0, "emp_id");
		$authorities = get_emp_authority($con, $emp_id, $fname);
		$accessible = ($authorities[8] != "f");
	}
	if (!$accessible) {
		pg_close($con);
		return error_result("パスワード変更権限がありません。");
	}

	// 入力チェック
	if ($cur_passwd == "") {
		pg_close($con);
		return error_result("旧パスワードを入力してください。");
	}
	if ($new_passwd == "") {
		pg_close($con);
		return error_result("新パスワードを入力してください。");
	}
	if ($cfm_passwd != $new_passwd) {
		pg_close($con);
		return error_result("確認用と一致しておりません。正しい新パスワードを入力してください。");
	}
	$pass_length = get_pass_length($con, $fname);
	if (strlen($new_passwd) > $pass_length) {
		pg_close($con);
		return error_result("新パスワードが長すぎます。");
	}
	if (preg_match("/[^0-9a-zA-Z_-]/", $new_passwd) > 0) {
		pg_close($con);
		return error_result("新パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。");
	}
	if (substr($new_passwd, 0, 1) == "-") {
		pg_close($con);
		return error_result("新パスワードの先頭に「-」は使えません。");
	}

	// トランザクションの開始
	pg_query($con, "begin");

	// ログイン情報を取得
	$sql = "select emp_login_id, emp_login_mail, emp_login_pass from login";
	$cond = " where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		return error_result("システムエラーが発生しました。");
	}
	$login_id = pg_fetch_result($sel, 0, "emp_login_id");
	$mail_id = pg_fetch_result($sel, 0, "emp_login_mail");
	$login_pass = pg_fetch_result($sel, 0, "emp_login_pass");

	// 環境設定情報を取得
	$sql = "select site_id, use_cyrus from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		return error_result("システムエラーが発生しました。");
	}
	$site_id = pg_fetch_result($sel, 0, "site_id");
	$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

	// 入力された既存パスワードと異なる場合、入力エラーとする
	if ($cur_passwd != $login_pass) {
		pg_query($con, "rollback");
		pg_close($con);
		return error_result("正しい旧パスワードを入力してください。");
	}

	// ログインIDと新パスワードが同じ場合
	if ($login_id == $new_passwd) {

		// 環境設定情報を取得
		$sql = "select weak_pwd_flg from config";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			return error_result("システムエラーが発生しました。");
		}
		$weak_pwd_flg = pg_fetch_result($sel, 0, "weak_pwd_flg");

		// 許可しない設定の場合はエラーとする
		if ($weak_pwd_flg == "f") {
			pg_query($con, "rollback");
			pg_close($con);
			return error_result("ログインIDと同じパスワードは使用できません。");
		}
	}

	// 新パスワードと旧パスワードが同じ場合
	if ($new_passwd === $login_pass) {
		pg_query($con, "rollback");
		pg_close($con);
		return error_result("旧パスワードと同じパスワードは使用できません。");
	}

	// パスワードを更新
	$pass_change_date = date("Ymd");
	$sql = "update login set";
	$set = array("emp_login_pass", "pass_change_date");
	$setvalue = array($new_passwd, $pass_change_date);
	$cond = "where emp_id = '$emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		return error_result("システムエラーが発生しました。");
	}

//XX	// XOOPSアカウントの更新
//XX	$con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX	mysql_select_db(XOOPS_DB_NAME, $con_mysql);
//XX	$sql = "update " . XOOPS_DB_PREFIX . "_users set pass = '" . md5($new_passwd) . "' where uname = '$login_id'";
//XX	if (!mysql_query($sql, $con_mysql)) {
//XX		pg_query($con, "rollback");
//XX		pg_close($con);
//XX		mysql_close($con_mysql);
//XX		return error_result("システムエラーが発生しました。");
//XX	}

	// トランザクションをコミット
	pg_query($con, "commit");
	pg_close($con);
//XX	mysql_close($con_mysql);

	// CoMedixのCyrusを使う場合
	if ($use_cyrus == "t") {

		// メールアカウント情報を更新（IDにアルファベットが含まれる場合）
		$account = ($site_id == "") ? $login_id : "{$mail_id}_{$site_id}";
		if (preg_match("/[a-z]/", $account) > 0) {
			$dir = getcwd();
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/updmailbox $account $account $new_passwd 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/updmailbox.exp $account $account $new_passwd");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncupdmailbox.exp $account $account $new_passwd $imapServerAddress $dir");
			}
		}
	}

	return success_result();
}

function show_result($result) {
	header("Content-Type: text/plain; charset=Shift_JIS");
	$flag_str = ($result["flag"]) ? "OK" : "NG";
	echo($flag_str . "," . mb_convert_encoding($result["message"], "Shift_JIS", "EUC-JP"));
}

function success_result() {
	return make_result(true, "パスワード更新に成功しました。");
}

function error_result($message) {
	return make_result(false, $message);
}

function make_result($flag, $message) {
	return array("flag" => $flag, "message" => $message);
}
?>
