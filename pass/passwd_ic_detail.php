<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$emp_pass_flg && !c2Bool($configRow["user_belong_change"]) && !c2Bool($configRow["user_job_change"]) && !c2Bool($configRow["user_st_change"])) {
	c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。");
}

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

define("SUCCEED_MSG", "passwd_ic_detail_succeed_msg");

//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
	$empmstRow["emp_idm"] = $_REQUEST["emp_idm"];

	// 入力チェック
	if (strlen($empmstRow["emp_idm"]) > 32) $errmsg[]= 'ICカードIDmが長すぎます。';

	if ($empmstRow["emp_idm"]!="") {
		$sql =
		" select count(*) from empmst".
		" where emp_idm = ".c2dbStr($empmstRow["emp_idm"]).
		" and emp_id <> ".c2dbStr(C2_LOGIN_EMP_ID).
		" and exists (select * from authmst where authmst.emp_id = empmst.emp_id and emp_del_flg = 'f')";
		if ((int)c2dbGetOne($sql)) $errmsg[]= '入力されたICカードIDmは、すでに使われています。';
	}

	if (!count($errmsg)) {
		$sql = "update empmst set emp_idm = ".c2dbStr($empmstRow["emp_idm"])." where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID);
		c2dbExec($sql);

		// 変更完了表示
		ob_clean();
		c2Env::setGlobalSession(SUCCEED_MSG, 'ICカードIDmを更新しました。');
		header("Location: passwd_ic_detail.php");
		die;
	}
}
$succeed_msg = c2Env::getGlobalSession(SUCCEED_MSG);
if (!count($errmsg) && $succeed_msg) $errmsg[]= $succeed_msg;
c2Env::unsetGlobalSession(SUCCEED_MSG);

//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | ICカード登録</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
function readFelica() {
	var felica_anchor = document.getElementById('felica_anchor');
	if (!felica_anchor) {
		alert('プラグインがインストールされていないため利用できません。');
		return;
	}
	felica_anchor.click();
	document.frm.emp_idm.value = document.frm.idm_hid.value;
}
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">


<?=$title_and_pankuzu?>


<?=$basic_tab_menu?>



<form name="mainform" method="post">
<input type="hidden" name="try_update" value="1" />


<div style="width:600px; padding-top:2px">


<table width="100%" cellspacing="0" cellpadding="2" class="list prop_list">
	<tr>
		<th width="140">ICカードIDm</th>
		<td>
			<input type="text" name="emp_idm" value="<?=hh($empmstRow["emp_idm"])?>" size="30">
			<input type="button" value="カード読み込み" onclick="readFelica();">
			<span id="FeliCa">
			<!--
				<a id="felica_anchor" href="felica:">
					<input type="hidden" name="idm_hid" />
					<polling target="cTafzB6PR9ZGl7MmdfBDm37wfzoBABhLg6mKuzDiXFAH" result="idm_hid" />
				</a>
			-->
			</span>
		</td>
	</tr>
</table>


<div style="padding-top:2px; text-align:right"><input type="submit" value="更新"></div>


<div style="color:red">
	注意：<br>
	・ICカードを読み込むためには，felica対応のカードリーダを接続する必要があります。<br>
	・カード読み込みができるのは、IEブラウザエクステンションのプラグインをインストールしたIEブラウザのみです。
</div>

</div>

</form>


</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>

