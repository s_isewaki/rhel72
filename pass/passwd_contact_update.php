<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$emp_pass_flg && !c2Bool($configRow["user_belong_change"]) && !c2Bool($configRow["user_job_change"]) && !c2Bool($configRow["user_st_change"])) {
	c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。");
}

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

// 拡張empmst項目1から5のデータ
$spftEmpmstRow = c2dbGetTopRow("select * from spft_empmst where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID));

// 拡張empmst項目1から5のラベル
$_rows = c2dbGetRows("select field_id, field_label from spfm_field_struct where table_id = 'spft_empmst' and field_id like 'empmst_ext%'");
$fieldLabels = array();
foreach ($_rows as $row) $fieldLabels[$row["field_id"]] = $row["field_label"];


// 拡張empmst項目1から5の表示可能判定と編集可能判定を取得
// ・利用判定：disp_empがall_okか、そうでないかを判定
// ・編集判定：edit_empがall_okか、そうでないかを判定
$sql =
" select field_id, disp_emp, edit_emp from spfm_field_struct_auth".
" where table_id = 'spft_empmst'".
" and field_id like 'empmst_ext%'".
" and target_place = 'all'".
" and disp_emp = 'all_ok'";
$rows = c2dbGetRows($sql);
$fieldStructAuthRows = array();
$ret = array();
foreach ($rows as $row) $fieldStructAuthRows[$row["field_id"]] = $row;


//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
	foreach ($_REQUEST as $k => $v) {
		// リクエストキーをアンスコ２つで区切ったとき、２つに分かれるなら、それは拡張フィールド。
		$ids = explode("__",$k);
		if (count($ids==2) && $ids[0]=="spft_empmst") $spftEmpmstRow[$ids[1]] = $v;
		else $empmstRow[$k] = $v; // そうでなければ、それはempmstのフィールドとしておく
	}

	if (strlen($empmstRow["emp_zip1"]) > 3) $errmsg[]= '郵便番号1が長すぎます。';
	if (strlen($empmstRow["emp_zip2"]) > 4) $errmsg[]= '郵便番号2が長すぎます。';
	if (strlen($empmstRow["emp_addr1"]) > 100) $errmsg[]= '住所1が長すぎます。';
	if (strlen($empmstRow["emp_addr2"]) > 100) $errmsg[]= '住所2が長すぎます。';
	if (strlen($empmstRow["emp_tel1"]) > 6) $errmsg[]= '電話番号1が長すぎます。';
	if (strlen($empmstRow["emp_tel2"]) > 6) $errmsg[]= '電話番号2が長すぎます。';
	if (strlen($empmstRow["emp_tel3"]) > 6) $errmsg[]= '電話番号3が長すぎます。';
	if (strlen($empmstRow["emp_mobile1"]) > 6) $errmsg[]= '携帯端末1が長すぎます。';
	if (strlen($empmstRow["emp_mobile2"]) > 6) $errmsg[]= '携帯端末2が長すぎます。';
	if (strlen($empmstRow["emp_mobile3"]) > 6) $errmsg[]= '携帯端末3が長すぎます。';
	if (strlen($empmstRow["emp_m_email"]) > 120) $errmsg[]= '携帯Mailが長すぎます。';
	if (strlen($empmstRow["emp_ext"]) > 10) $errmsg[]= '内線番号が長すぎます。';
	if (strlen($empmstRow["emp_phs"]) > 6) $errmsg[]= getPhsName().'が長すぎます。';

	if (!count($errmsg)) {
		$sql =
		" update empmst set".
		" emp_zip1 = ".c2dbStr($empmstRow["emp_zip1"]).
		",emp_zip2 = ".c2dbStr($empmstRow["emp_zip2"]).
		",emp_prv = ".c2dbStr($empmstRow["emp_prv"]).
		",emp_addr1 = ".c2dbStr($empmstRow["emp_addr1"]).
		",emp_addr2 = ".c2dbStr($empmstRow["emp_addr2"]).
		",emp_ext = ".c2dbStr($empmstRow["emp_ext"]).
		",emp_tel1 = ".c2dbStr($empmstRow["emp_tel1"]).
		",emp_tel2 = ".c2dbStr($empmstRow["emp_tel2"]).
		",emp_tel3 = ".c2dbStr($empmstRow["emp_tel3"]).
		",emp_mobile1 = ".c2dbStr($empmstRow["emp_mobile1"]).
		",emp_mobile2 = ".c2dbStr($empmstRow["emp_mobile2"]).
		",emp_mobile3 = ".c2dbStr($empmstRow["emp_mobile3"]).
		",emp_m_email = ".c2dbStr($empmstRow["emp_m_email"]).
		",emp_phs = ".c2dbStr($empmstRow["emp_phs"]).
		" where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID);
		c2dbExec($sql);

		// 拡張職員マスタ。なければインサート
		if (!(int)c2dbGetOne("select count(emp_id) from spft_empmst where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID))) {
			c2dbExec("insert into spft_empmst (emp_id) values (".c2dbStr(C2_LOGIN_EMP_ID).")");
		}
		// 拡張職員マスタ、更新対象フィールドがあれば更新
		$upd_ext = array();
		for ($idx=1; $idx<=5; $idx++) {
			$row = $fieldStructAuthRows["empmst_ext".$idx];
			if (!$row || $row["edit_emp"]!="all_ok") continue;
			c2dbExec("update spft_empmst set empmst_ext".$idx." = ".c2dbStr($_REQUEST["empmst_ext".$idx])." where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID));
		}
		ob_clean();
		header("Location: passwd_contact_update.php");
		die;
	}
}

//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 連絡先変更</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<?=$title_and_pankuzu?>


<?=$basic_tab_menu?>



<form name="frm" method="post">
<input type="hidden" name="try_update" value="1" />




<div style="width:600px; padding-top:2px">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
	<tr>
		<th width="120">登録先</th>
		<td>職員名簿</td>
	</tr>
	<tr>
		<th>職員ID</th>
		<td><?=hh($empmstRow["emp_personal_id"])?></td>
	</tr>
	<tr>
		<th>名前（漢字）</th>
		<td><?=hh($empmstRow["emp_lt_nm"]." ".$empmstRow["emp_ft_nm"])?></td>
	</tr>
	<tr>
		<th>名前（ひらがな）</td>
		<td><?=hh($empmstRow["emp_kn_lt_nm"]." ".$empmstRow["emp_kn_ft_nm"])?></td>
	</tr>
	<tr>
		<th>郵便番号</th>
		<td>
			<input name="emp_zip1" type="text" size="5" maxlength="3" value="<?=hh($empmstRow["emp_zip1"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_zip2" type="text" size="5" maxlength="4" value="<?=hh($empmstRow["emp_zip2"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>都道府県</th>
		<td>
			<select name="emp_prv">
				<? $pref_list = c2GetPrefList(); ?>
				<option value=""></option>
				<? foreach($pref_list as $idx=>$pref) { ?>
				<option value="<?=$idx?>"<?=(trim($idx)===$empmstRow["emp_prv"]?" selected":"")?>><?=$pref?></option>
				<? } ?>
			</select>
		</td>
	</tr>
	<tr>
		<th>住所1</th>
		<td><input name="emp_addr1" type="text" size="50" maxlength="50" value="<?=hh($empmstRow["emp_addr1"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>住所2</th>
		<td><input name="emp_addr2" type="text" size="50" maxlength="50" value="<?=hh($empmstRow["emp_addr2"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td>
			<input name="emp_tel1" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_tel1"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_tel2" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_tel2"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_tel3" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_tel3"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>E-Mail</th>
		<td><input type="text" size="40" maxlength="100" value="<?=hh($empmstRow["emp_email2"])?>" style="ime-mode:inactive;" readonly></td>
	</tr>
	<tr>
		<th>携帯端末・外線</th>
		<td>
			<input name="emp_mobile1" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_mobile1"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_mobile2" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_mobile2"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_mobile3" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_mobile3"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>携帯Mail</th>
		<td><input name="emp_m_email" type="text" size="40" maxlength="100" value="<?=hh($empmstRow["emp_m_email"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>内線番号</th>
		<td><input name="emp_ext" type="text" size="10" maxlength="10" value="<?=hh($empmstRow["emp_ext"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th><?=getPhsName()?></th>
		<td><input name="emp_phs" type="text" size="10" maxlength="6" value="<?=hh($empmstRow["emp_phs"])?>" style="ime-mode:inactive;"></td>
	</tr>

	<?
		for ($idx=1; $idx<=5; $idx++) {
			$row = $fieldStructAuthRows["empmst_ext".$idx];
			if (!$row) continue;
	?>
		<tr>
			<th><?=hh($fieldLabels["empmst_ext".$idx])?></th>
			<td>
				<?if ($row["edit_emp"]=="all_ok") { ?>
				<input name="empmst_ext<?=$idx?>" type="text" size="20"
					value="<?=hh($spftEmpmstRow["empmst_ext".$idx])?>" />
				<? } else { ?>
				<span style="padding-left:2px"><?=hh($spftEmpmstRow["empmst_ext".$idx])?></span>
				<? } ?>
			</td>
		</tr>
	<? } ?>
</table>




<div style="padding-top:2px; text-align:right"><input type="submit" value="更新"></div>


</div>



</form>

</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>
