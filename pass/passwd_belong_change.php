<?
require_once("common.php"); // 読込みと同時にログイン判別
//if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にpass1権限(8)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

if($configRow["user_belong_change"] != "t" && $configRow["user_job_change"] != "t" && $configRow["user_st_change"] != "t" ) {
	//権限チェック
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

define("SUCCEED_MSG",  "passwd_belong_change_succeed_msg");

//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
$update_type = $_REQUEST["update_type"];
if ($update_type=="busyo" || $update_type=="emp_st" || $update_type=="emp_job") {
	foreach ($_REQUEST as $k => $v) $empmstRow[$k] = $v;

	if ($update_type=="busyo" && !$empmstRow["emp_class"]) $errmsg[]= "所属を選択してください。";
	else if ($update_type=="busyo" && !$empmstRow["emp_attribute"]) $errmsg[]= $classnameRow["atrb_nm"]."を選択してください。";
	else if ($update_type=="busyo" && !$empmstRow["emp_dept"]) $errmsg[]= $classnameRow["dept_nm"]."を選択してください。";
	else if ($update_type=="emp_st" && !$empmstRow["emp_st"]) $errmsg[]= "役職を選択してください。";
	else if ($update_type=="syozoku" && !$empmstRow["emp_job"]) $errmsg[]= "職種を選択してください。";

	if (!count($errmsg)) {
		$succeed_msg = "";
		$upd_field = array();
		if ($update_type == "busyo") {
			$succeed_msg = "所属を変更しました。";
			$upd_field[]= "emp_class = ".($empmstRow["emp_class"] ? (int)$empmstRow["emp_class"] : "null");
			$upd_field[]= "emp_attribute = ".($empmstRow["emp_attribute"] ? (int)$empmstRow["emp_attribute"] : "null");
			$upd_field[]= "emp_dept = ".($empmstRow["emp_dept"] ? (int)$empmstRow["emp_dept"] : "null");
			$upd_field[]= "emp_room = ".($empmstRow["emp_room"] ? (int)$empmstRow["emp_room"] : "null");
		} else if($update_type == "emp_st") {
			$succeed_msg = "役職を変更しました。";
			$upd_field[]= "emp_st = ".c2dbStr($empmstRow["emp_st"]);
		} else if($update_type == "emp_job") {
			$succeed_msg = "職種を変更しました。";
			$upd_field[]= "emp_job = ".c2dbStr($empmstRow["emp_job"]);
		}

		c2dbExec("update empmst set ".implode(", ", $upd_field)." where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID));

		ob_clean();
		c2Env::setGlobalSession(SUCCEED_MSG, $succeed_msg);
		header("Location: passwd_belong_change.php");
		die;
	}
}


$succeed_msg = c2Env::getGlobalSession(SUCCEED_MSG); // 更新直後なら値がある。
if (!count($errmsg) && $succeed_msg) $errmsg[]= $succeed_msg; // errmsgに成功メッセージを一時的に入れる
c2Env::unsetGlobalSession(SUCCEED_MSG); // リダイレクト明けのワンタイムメッセージをクリア


$classmstRows = c2dbGetRows("select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no");// 部門一覧
$atrbmstRows = c2dbGetRows("select class_id as pid, atrb_id as cid, atrb_nm as nm from atrbmst where atrb_del_flg = 'f' order by class_id, order_no");// 課一覧
$deptmstRows = c2dbGetRows("select atrb_id as pid, dept_id as cid, dept_nm as nm from deptmst where dept_del_flg = 'f' order by atrb_id, order_no");// 科一覧
$classroomRows = c2dbGetRows("select dept_id as pid, room_id as cid, room_nm as nm from classroom where room_del_flg = 'f' order by order_no");// 室一覧
$jobmstRows = c2dbGetRows("select job_id, job_nm from jobmst where job_del_flg = 'f' order by job_id");// 職種一覧
$stmstRows = c2dbGetRows("select st_id, st_nm from stmst where st_del_flg = 'f' order by st_id");// 役職一覧



//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | マスタ変更</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">

var busyoObj = {};
busyoObj["emp_attribute"] = <?=c2ToJson($atrbmstRows)?>;
busyoObj["emp_dept"] = <?=c2ToJson($deptmstRows)?>;
busyoObj["emp_room"] = <?=c2ToJson($roomsmstRows)?>;

<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>

function busyoChanged(childId, selectValue) {
	var cmb = document.getElementById(childId);
	cmb.options.length = 0;
	for ( var idx=0; idx<busyoObj[childId].length; idx++) {
		var obj = busyoObj[childId][idx];
		if (obj.pid!=selectValue) continue;
		cmb.options[cmb.options.length] = new Option(obj.nm, obj.cid);
	}
	if (cmb.options.length==0) {
		cmb.options[0] = new Option("（未登録）", "");
	}
	var magoId = "";
	if (childId=="emp_attribute") magoId = "emp_dept";
	if (childId=="emp_dept") magoId = "emp_room";
	if (magoId) {
		busyoChanged(magoId, cmb.options[0].value);
	}
}

function act_update(up_type) {
	document.frm.update_type.value = up_type;
	document.frm.submit();
}
function initPage() {
	var cmb = document.getElementById("emp_class");
	busyoChanged("emp_attribute", cmb.options[cmb.selectedIndex].value);
}
</script>
</head>
<body onload="initPage();"><div id="CLIENT_FRAME_CONTENT">




<?=$title_and_pankuzu?>


<?=$basic_tab_menu?>



<form name="frm" method="post" action="passwd_belong_change.php">
<input type="hidden" name="update_type" value="" />



<div style="width:800px; padding-top:2px">





<? if($configRow["user_belong_change"] == "t") { ?>
	<div style="padding-bottom:12px">
	<div style="border:1px solid #5179a5; background-color:#f6f6ff; padding:2px"><nobr>
		【組織】<br>
		所属<?=hh($classnameRow["class_nm"])?>
		<select id="emp_class" name="emp_class" onchange="busyoChanged('emp_attribute', this.value);">
			<option value=""></option>
			<? foreach ($classmstRows as $row) { ?>
			<option value="<?=$row["class_id"]?>"<?=($row["class_id"]==$empmstRow["emp_class"]?" selected":"")?>><?=hh($row["class_nm"])?></option>
			<? } ?>
		</select>
		所属<?=hh($classnameRow["atrb_nm"])?>
		<select id="emp_attribute" name="emp_attribute" onchange="busyoChanged('emp_dept', this.value);">
			<? foreach ($atrbmstRows as $row) { ?>
			<? if ($empmstRow["emp_class"]!=$row["pid"]) continue; ?>
			<option value="<?=$row["cid"]?>"<?=($row["cid"]==$empmstRow["emp_attribute"]?" selected":"")?>><?=hh($row["nm"])?></option>
			<? } ?>
		</select>
		所属<?=hh($classnameRow["dept_nm"])?>
		<select id="emp_dept" name="emp_dept" onchange="busyoChanged('emp_room', this.value);">
			<? foreach ($deptmstRows as $row) { ?>
			<? if ($empmstRow["emp_attribute"]!=$row["pid"]) continue; ?>
			<option value="<?=$row["cid"]?>"<?=($row["cid"]==$empmstRow["emp_dept"]?" selected":"")?>><?=hh($row["nm"])?></option>
			<? } ?>
		</select>
		<span style="<?=($classnameRow["class_cnt"]==4 ? "":"display:none")?>">
			所属<?=hh($classnameRow["room_nm"])?>
			<select id="emp_room" name="emp_room">
				<? foreach ($classroomRows as $row) { ?>
				<? if ($empmstRow["emp_dept"]!=$row["pid"]) continue; ?>
				<option value="<?=$row["cid"]?>"<?=($row["cid"]==$empmstRow["emp_room"]?" selected":"")?>><?=hh($row["nm"])?></option>
				<? } ?>
			</select>
		</span><br>
		<input type="button" value="更新" onclick="act_update('busyo');">
	</nobr></div>
	</div>
<? } ?>



<? if($configRow["user_st_change"] == "t" ) { ?>
	<div style="padding-bottom:12px">
	<div style="border:1px solid #5179a5; background-color:#f6f6ff; padding:2px"><nobr>
		【役職】<br>
		<select name="emp_st">
			<option value=""></option>
			<? foreach ($stmstRows as $row) { ?>
			<option value="<?=$row["st_id"]?>"<?=($row["st_id"]==$empmstRow["emp_st"]?" selected":"")?>><?=hh($row["st_nm"])?></option>
			<? } ?>
			</select><br>
			<input type="button" value="更新" onclick="act_update('emp_st');">
	</div>
	</div>
<? } ?>




<? if($configRow["user_job_change"] == "t") { ?>
	<div style="border:1px solid #5179a5; background-color:#f6f6ff; padding:2px"><nobr>
		【職種】<br>
		<select name="emp_job">
			<option value=""></option>
			<? foreach ($jobmstRows as $row) { ?>
			<option value="<?=$row["job_id"]?>"<?=($row["job_id"]==$empmstRow["emp_job"]?" selected":"")?>><?=hh($row["job_nm"])?></option>
			<? } ?>
		</select><br>
		<input type="button" value="更新" onclick="act_update('emp_job');">
	</div>
<? } ?>




</div>

</form>


</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>
