<?
define("SELF_APPID",   "pass1");
define("C2_SESSION_GROUP_NAME", "pass1");


if ($_REQUEST["c2trace"]=="direct" || $_REQUEST["c2trace"]=="intelligent") {
    $_COOKIE["C2TRACE"] = $_REQUEST["c2trace"];
}
if ($_REQUEST["c2trace"]=="off" || $_REQUEST["c2trace"]=="exit") {
    $_COOKIE["C2TRACE"] = "";
}


// このファイルのひとつ上の階層に上ってから、Core2呼び出し
require_once(dirname(dirname(__FILE__))."/class/Cmx/Core2/C2FW.php"); // 呼出しと同時にセッションチェックあり
require_once(dirname(dirname(__FILE__))."/about_postgres.php"); // $conのために必要

// アプリ名
$c2app = c2GetC2App();
//define("SELF_APPNAME",   $c2app->getApplicationName("pass1"));
define("SELF_APPNAME",   "パスワード変更");

$loginEmpmstRow = c2login::validateSession(); // クッキーのセッション値からログイン者判別
$EAL = $c2app->getEmpAuthority(C2_LOGIN_EMP_ID); // ログイン者の機能権限有無リスト

$con = connect2db($_SERVER["PHP_SELF"]); // DB接続

$classnameRow = c2dbGetTopRow("select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname");


$emp_pass_flg  = c2Bool($EAL["emp_pass_flg"]);  // パスワード変更権限（pass1権限）(8)
$emp_cauth_flg = c2Bool($EAL["emp_cauth_flg"]); // 端末認証権限（cauth権限）(91)

$configRow = $c2app->getConfigRow();

$empmstRow = $c2fw->loginEmpmstRow;



$title_and_pankuzu =
'<div class="window_title"><table cellspacing="0" cellpadding="0"><tr><th>'.
'<a class="pankuzu" href="'.
($emp_pass_flg ? 'passwd_change.php">パスワード・本人情報' : 'passwd_contact_update.php">本人情報').
'</a></th></tr></table></div>';


$_fn = basename($_SERVER["PHP_SELF"]);

$_abtm = array();
$_abtm[]= '<div class="basic_tab_menu">';
$_abtm[]= '<table cellspacing="0" cellpadding="0"><tr>';
if($emp_pass_flg) {
	$_abtm[]= '<th class="'.($_fn=="passwd_change.php"?"current":"selectable").'"><a href="passwd_change.php"><nobr>パスワード変更</nobr></a></th>';
}
	$_abtm[]= '<th class="'.($_fn=="passwd_contact_update.php"?"current":"selectable").'"><a href="passwd_contact_update.php">連絡先変更</a></th>';
if (c2Bool($configRow["user_belong_change"]) || c2Bool($configRow["user_job_change"]) || c2Bool($configRow["user_st_change"])) {
	$_abtm[]= '<th class="'.($_fn=="passwd_belong_change.php"?"current":"selectable").'"><a href="passwd_belong_change.php">マスタ変更</a></th>';
}
if (c2Bool($configRow["ic_read_flg"])) {
	$_abtm[]= '<th class="'.($_fn=="passwd_ic_detail.php"?"current":"selectable").'"><a href="passwd_ic_detail.php">ICカード登録</a></th>';
}
if ($emp_cauth_flg) {
	$_abtm[]= '<th class="'.($_fn=="passwd_client_auth.php"?"current":"selectable").'"><a href="passwd_client_auth.php">端末認証</a></th>';
}
$_abtm[]= '</tr></table>';
$_abtm[]= '</div>';
$basic_tab_menu = implode("\n", $_abtm);


function getPhsName() {
    global $c2app;
    $LBP = $c2app->getLabelByProfile();
    return $LBP["PHS"][$c2app->getPrfType()];
}
