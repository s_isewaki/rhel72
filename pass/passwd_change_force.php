<?
require_once("common.php"); // 読込みと同時にログイン判別
if (!$emp_pass_flg) {
    if ($_REQUEST["try_update"]) {
        echo "操作権限がありません。";
        die;
    }
    else {
        c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にpass1権限(8)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行
    }
}
$cur_passwd = $_REQUEST["cur_passwd"];
$new_passwd = $_REQUEST["new_passwd"];
$cfm_passwd = $_REQUEST["cfm_passwd"];


//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//――――――――――――――――――――――――――――――――――――――――――――――――
if ($_REQUEST["try_update"]) {
    if ($cur_passwd == "") {
        echo "旧パスワードを入力してください。";
        die;
    }
    if ($new_passwd == "") {
        echo "新パスワードを入力してください。";
        die;
    }
    if ($cfm_passwd != $new_passwd) {
        echo "確認用と一致しておりません。正しい新パスワードを入力してください。";
        die;
    }
    if (!$configRow["pass_length"]) $configRow["pass_length"] = 20; // nullかゼロなら20文字とみなす
    if (strlen($new_passwd) > $configRow["pass_length"]) {
        echo "新パスワードが長すぎます。";
        die;
    }
    if (preg_match("/[^0-9a-zA-Z_-]/", $new_passwd) > 0) {
        echo "新パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。";
        die;
    }
    if (substr($new_passwd, 0, 1) == "-") {
        echo "新パスワードの先頭に「-」は使えません。";
        die;
    }

    // ログイン情報を取得
    $loginRow = c2dbGetTopRow("select * from login where emp_id = " . c2dbStr(C2_LOGIN_EMP_ID));
    if ($cur_passwd && $cur_passwd != $loginRow["emp_login_pass"]) {
        echo "正しい旧パスワードを入力してください。";
        die;
    }

    // ログインIDと新パスワードが同じ状態を許可しない場合エラー
    if ($loginRow["emp_login_id"] == $loginRow["emp_login_pass"]) {
        if (!c2Bool($configRow["weak_pwd_flg"])) {
            echo 'ログインIDと同じパスワードは使用できません。';
            die;
        }
    }

    // 新パスワードと旧パスワードが同じ場合
    if ($new_passwd === $loginRow["emp_login_pass"]) {
        echo '旧パスワードと同じパスワードは使用できません。';
        die;
    }

    //----------loginテーブルのパスワード列を更新----------
    $sql = "update login set emp_login_pass = " . c2dbStr($new_passwd) . ", pass_change_date = '" . date("Ymd") . "' where emp_id = " . c2dbStr(C2_LOGIN_EMP_ID);
    c2dbExec($sql);

    // XOOPSアカウントの更新
    require_once("webmail/config/config.php");

    // CoMedixのCyrusを使う場合
    if (c2Bool($configRow["use_cyrus"])) {

        // メールアカウント情報を更新（IDにアルファベットが含まれる場合）
        $account = ($configRow["site_id"] == "" ? $loginRow["emp_login_id"] : $loginRow["emp_login_mail"] . "_" . $configRow["site_id"]);
        if (preg_match("/[a-z]/", $account) > 0) {
            $dir = getcwd();
            if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
                exec("/usr/bin/perl $dir/mboxadm/updmailbox $account $account $new_passwd 2>&1 1> /dev/null", $output, $exit_status);
                if ($exit_status !== 0) {
                    exec("/usr/bin/expect -f $dir/expect/updmailbox.exp $account $account $new_passwd");
                }
            }
            else {
                exec("/usr/bin/expect -f $dir/expect/rsyncupdmailbox.exp $account $account $new_passwd $imapServerAddress $dir");
            }
        }
    }
    echo "ok"; // 成功
    die;
}

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


if (!$_REQUEST["next_url"]) $_REQUEST["next_url"] = "passwd_change_force.php";

// アクセスログ
require_once("aclg_set.php");
aclg_regist($session, $_SERVER["PHP_SELF"], $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | パスワード変更</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript" src="../js/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
    $.ajaxSettings.type = "POST";
    $.ajaxSettings.async = false;
    function tryUpdate() {
        var postData =
            "cur_passwd=" + encodeURIComponent($("#cur_passwd").val()) +
            "&new_passwd=" + encodeURIComponent($("#new_passwd").val()) +
            "&cfm_passwd=" + encodeURIComponent($("#cfm_passwd").val());
        $.ajax({
            url: "passwd_change_force.php?try_update=1&emp_id=<?=C2_LOGIN_EMP_ID?>", data: postData, success: function(msg) {
                if (msg === "ok") {
                    alert("パスワードを変更しました。");
                    $('<form method="post" action="../main_menu.php"></form>')
                        .appendTo(document.body)
                        .append($('<input>', {type: 'hidden', name: 'id', value: '<?=$_REQUEST["id"]?>'}))
                        .append($('<input>', {type: 'hidden', name: 'pass', value: $("#new_passwd").val()}))
                        .append($('<input>', {type: 'hidden', name: 'launcher', value: '1'})) // 同時ログイン警告を回避するためにlauncher=1を付ける
                        .submit();
                    return;
                }
                alert(msg);
            }});

    }
</script>
</head>
<body onload="document.getElementById('cur_passwd').focus();"><div id="CLIENT_FRAME_CONTENT">

        <div style="margin:50px auto; padding: 20px; border: 1px solid black; width:640px;">
            <h1 style="background-color:#f6f9ff;">パスワード変更</h1>
            <p>初めてのログインの場合、必ず別のパスワードに変更してください。変更後はこの画面は表示されません。</p>

            <div style="padding-top:2px">
                <table width="600" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
                    <tr>
                        <th width="120">旧パスワード</th>
                        <td width="480"><input type="password" id="cur_passwd" style="ime-mode: inactive;" autocomplete="off"></td>
                    </tr>
                    <tr>
                        <th>新パスワード</th>
                        <td><input type="password" id="new_passwd" style="ime-mode: inactive;" autocomplete="off"></td>
                    </tr>
                    <tr>
                        <th>新パスワード</th>
                        <td><input type="password" id="cfm_passwd" style="ime-mode: inactive;" autocomplete="off">&nbsp;（確認用）</td>
                    </tr>
                </table>
            </div>

            <table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td height="22" align="right"><button type="button" onclick="tryUpdate()">更新</button></td>
                </tr>
            </table>

        </div>

    </div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>
