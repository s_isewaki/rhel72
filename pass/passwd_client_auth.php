<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$emp_cauth_flg) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にcauth権限(91)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

define("SUCCEED_MSG",  "passwd_client_auth_succeed_msg");

$clientAuthRow = c2dbGetTopRow("select * from client_auth where issuer_emp_id = ".c2dbStr(C2_LOGIN_EMP_ID));

//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
	$clientAuthRow["client_no"] = $_REQUEST["client_no"];
	$clientAuthRow["note"] = $_REQUEST["note"];

	// 入力チェック
	if (strlen($clientAuthRow["client_no"]) > 50) $errmsg[]= '端末番号が長すぎます。';
	if (strlen($clientAuthRow["note"]) > 200) $errmsg[]= '備考が長すぎます。';

	if (!count($errmsg)) {
		// 認証キーの生成
		$_caid = md5(uniqid(mt_rand(), true));
		$contentRoot = c2GetContentsRootPath();
		setcookie("caid", $_caid, mktime(0, 0, 0, 1, 1, 2030), $contentRoot, "", true); // 端末認証キーをCOOKIE(/comedix/パス)に格納する。HTTPSのみ。

		// 端末認証履歴情報の登録
		$sql =
		" insert into client_auth (".
		"     client_no, note, issuer_emp_id, issue_datetime, auth_key".
		" ) values (".
		"     ".c2dbStr($clientAuthRow["client_no"]).
		"    ,".c2dbStr($clientAuthRow["note"]).
		"    ,".c2dbStr(C2_LOGIN_EMP_ID).
		"    ,'".date("YmdHi")."'".
		"    ,".c2dbStr($_caid).
		" )";
		c2dbExec($sql);

		c2Env::setGlobalSession(SUCCEED_MSG, '端末認証キーを発行しました。');
//		c2Env::setGlobalSession(SUCCEED_CAID, $_caid);

		ob_clean();
		header("Location: passwd_client_auth.php");
		die;
	}

}


$succeed_msg = c2Env::getGlobalSession(SUCCEED_MSG); // 更新直後なら値がある。
if (!count($errmsg) && $succeed_msg) $errmsg[]= $succeed_msg; // errmsgに成功メッセージを一時的に入れる
c2Env::unsetGlobalSession(SUCCEED_MSG); // リダイレクト明けのワンタイムメッセージをクリア



// 認証キーが未設定の場合は、最大値で「仮」採番
$message = "本端末は認証キー未発行またはクリアーされています。";
if ($_COOKIE["caid"] == "") {
	$issue_id = 1 + (int)c2dbGetOne("select max(issue_id) from client_auth");
}
// 認証キーが設定されている場合は、認証キーが有効かどうか確認する
else {
	$message = "本端末は認証キー発行済みです。";
	$cnt = (int)c2dbGetOne("select count(issue_id) from client_auth where auth_key = ".c2dbStr($_COOKIE["caid"])." and not disabled");
	if (!$cnt) $message = "本端末の認証キーは無効です。有効にするためには、管理者に連絡してください。";
}


// 発行者＝ログイン者の情報を取得
$sql =
" select e.emp_lt_nm, e.emp_ft_nm, c.class_nm, a.atrb_nm, d.dept_nm, r.room_nm".
" from empmst e".
" inner join classmst c on e.emp_class = c.class_id".
" inner join atrbmst a on e.emp_attribute = a.atrb_id".
" inner join deptmst d on e.emp_dept = d.dept_id".
" left join classroom r on e.emp_room = r.room_id".
" where e.emp_id = ".c2dbStr(C2_LOGIN_EMP_ID);
$leRow = c2dbGetTopRow($sql);

$busyo = $leRow["class_nm"] . "＞" . $leRow["atrb_nm"] . "＞" . $leRow["dept_nm"];
if (strlen($leRow["room_nm"]) > 0) $busyo .= "＞" . $leRow["room_nm"];
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 端末認証</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<? if (count($errmsg)) {?><script type="text/javascript"> alert("<?=implode("\\n", $errmsg)?>"); </script><? } ?>
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<?=$title_and_pankuzu?>


<?=$basic_tab_menu?>



<div style="padding:6px 0 2px 0">
<?=$message?>
</div>



<? if ($_COOKIE["caid"]=="") { ?>
<form method="post" action="passwd_client_auth.php">
<input type="hidden" name="try_update" value="1" />


<div style="width:600px; padding-top:2px;">

<table width="100%" cellspacing="0" cellpadding="2" class="list prop_list">
	<tr>
		<th width="20%">発行No</th>
		<td><?=hh($issue_id)?></td>
	</tr>
	<tr>
		<th>端末番号</th>
		<td><input type="text" name="client_no" value="" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>備考</th>
		<td><input type="text" name="note" value="<?=hh($clientAuthRow["note"])?>" size="50" style="ime-mode:active;"></td>
	</tr>
	<tr>
		<th>発行者</th>
		<td><?=hh($leRow["emp_lt_nm"]." ".$leRow["emp_ft_nm"])?></td>
	</tr>
	<tr>
		<th>部署</th>
		<td><?=hh($busyo)?></td>
	</tr>
</table>


<div style="padding-top:6px; text-align:right"><input type="submit" value="認証キー発行"></div>

</div>

</form>
<? } ?>





</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>
