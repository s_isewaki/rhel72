<?php
require_once("about_comedix.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

function certify($id, $password, $session, $fname) {

	// セッションを保持しているかチェック
	$checksession = check_session($session);

	// ない場合はログイン画面からのアクセスとみなす
	if ($checksession == "0") {
		$con = connect2db($fname);

		// 端末認証キーチェックがonの場合
		$conf = new Cmx_SystemConfig();
		$client_auth = $conf->get('security.client_auth');
		if ($client_auth == 1) {

			// 環境設定権限も端末認証権限もなければ端末認証キーをチェックする
			$sql = "select a.emp_config_flg, a.emp_cauth_flg from login l inner join authmst a on l.emp_id = a.emp_id";
			$cond = q("where l.emp_login_id = '%s'", $id);
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == "0") {
				echo("<script type='text/javascript' src='js/showpage.js'></script>");
				echo("<script type='text/javascript'>showErrorPage(window);</script>");
				exit;
			}
			if (pg_num_rows($sel) == 0 || (pg_fetch_result($sel, 0, "emp_config_flg") != "t" && pg_fetch_result($sel, 0, "emp_cauth_flg") != "t")) {
				$auth_key = $_COOKIE["caid"];
				if ($auth_key == "") {
					echo("<script type='text/javascript'>alert('本端末でのログインは許可されていません。');</script>");
					echo("<script type='text/javascript' src='js/showpage.js'></script>");
					echo("<script type='text/javascript'>showLoginPage(window);</script>");
					exit;
				}

				$sql = "select issue_id from client_auth";
				$cond = q("where auth_key = '%s' and (not disabled)", $auth_key);
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == "0") {
					echo("<script type='text/javascript' src='js/showpage.js'></script>");
					echo("<script type='text/javascript'>showErrorPage(window);</script>");
					exit;
				}
				if (pg_num_rows($sel) == 0) {
					echo("<script type='text/javascript'>alert('本端末でのログインは許可されていません。');</script>");
					echo("<script type='text/javascript' src='js/showpage.js'></script>");
					echo("<script type='text/javascript'>showLoginPage(window);</script>");
					exit;
				}
			}
		}

		if ($id == "" || $password == "") {
			echo("<script type='text/javascript'>alert('IDとパスワードを入力してください。');</script>");
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script type='text/javascript'>showLoginPage(window);</script>");
			exit;
		}

		// idとpasswordがある場合にはDBと照合
		$sql = "select l.emp_id from login l inner join authmst a on l.emp_id = a.emp_id";
		$cond = q("where l.emp_login_id = '%s' and l.emp_login_pass = '%s' and (not a.emp_del_flg)", $id, $password);
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == "0") {
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script type='text/javascript'>showErrorPage(window);</script>");
			exit;
		}

		// レコード数が1以外ならログイン画面へ戻す
		$record = check_record($sel);
		if ($record != 1) {
			echo("<script type='text/javascript'>alert('IDとPasswordが間違っています');</script>");
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script type='text/javascript'>showLoginPage(window);</script>");
			exit;
		}

		// 新しいセッションIDを作成する
		$emp_id = pg_fetch_result($sel, 0, "emp_id");
		$session = start_session($emp_id, $id, $session, $fname, true);
		if ($session == "0") {
			echo("<script type='text/javascript'>alert('長時間利用されませんでしたのでタイムアウトしました。\\n再度ログインしてください。');</script>");
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script type='text/javascript'>showLoginPage(window);</script>");
			exit;
		}
		return $session;

	// セッションを保持している場合はセッションの更新をする
	} else {
		$session = qualify_session($session, $fname);
		if ($session == "0") {
			echo("<script type='text/javascript'>alert('長時間利用されませんでしたのでタイムアウトしました。\\n再度ログインしてください。');</script>");
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script type='text/javascript'>showLoginPage(window);</script>");
			exit;
		}
		return $session;
	}
}
