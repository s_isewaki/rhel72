<?/*

画面パラメータ
	$session セッションID
	$default_tmpl_of_div**** デフォルトテンプレートID(****=診療区分ID)

*/?><meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("get_values.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,59,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

//入力チェック
//特に無し。

//診療区分一覧の取得(論理未削除データのみ)
$emp_id = get_emp_id($con, $session, $fname);
//$tbl = "select distinct tmpl_div_link.div_id from tmpl_div_link";
$tbl = " select" .
       "   dm.div_id" .
       " from" .
       "   divmst dm" .
       "  ,(select distinct div_id from tmpl_div_link) dl" .
       " where" .
       "       dm.div_del_flg = 'f'" .
       "   and dm.div_id = dl.div_id" ;
$cond = "";
$kubun_data_set = select_from_table($con,$tbl,$cond,$fname);//データリスト
$kubun_data_len = pg_numrows($kubun_data_set);//データ件数

//診療区分ループ
for($i=0;$i<$kubun_data_len;$i++){

	//診療区分ID
	$div_id      = pg_result($kubun_data_set,$i,"div_id");

	//デフォルトテンプレートID(画面パラメータより取得)
	$parmname = "default_tmpl_of_div".$div_id;
	$default_tmpl_id  = $$parmname;

	//デフォルトフラグをクリア
	$sql = "update tmpl_div_link set";
	$set = array("default_flg");
	$setvalue = array("f");
	$cond = "where tmpl_div_link.div_id = '$div_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		//echo("デフォルトフラグのクリアに失敗"."<br>");
		//echo($div_id."<br>");
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//デフォルトフラグを設定
	$sql = "update tmpl_div_link set";
	$set = array("default_flg");
	$setvalue = array("t");
	$cond = "where tmpl_div_link.div_id = '$div_id' and tmpl_div_link.tmpl_id = '$default_tmpl_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		//echo("デフォルトフラグの設定に失敗"."<br>");
		//echo($div_id."<br>");
		//echo($default_tmpl_id."<br>");
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

}

// トランザクションのコミット
pg_query($con, "commit");

pg_close($con);
//echo("<script language=\"javascript\">alert('更新しました。');location.href=\"./summary_kanri_tmpl.php?session=$session\";</script>\n");
echo("<script language=\"javascript\">location.href=\"./summary_kanri_tmpl.php?session=$session\";</script>\n");
exit;
?>