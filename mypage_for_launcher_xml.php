<?php
//===================================================================
// 新着情報XML
//===================================================================
ob_start();
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once('about_comedix.php');
define('SM_PATH', './webmail/');
require_once('webmail/config/config.php');
require_once('webmail/functions/i18n.php');
require_once('webmail/functions/prefs.php');
require_once('webmail/functions/imap.php');
require_once('webmail/plugins/newmail/setup.php');

require_once("hiyari_menu_news.php");

// SystemConfig
$conf = new Cmx_SystemConfig();
$schedule_alarm = $conf->get('schedule.alarm');
$notice_popup = $conf->get('news.notice_popup');
$notice_company_use = $conf->get('shift.notice.company_use');
$notice_company_url = $conf->get('shift.notice.company_url');
$notice_company_timeout = $conf->get('shift.notice.company_timeout');
$launcher_dakoku = $conf->get('shift.launcher_dakoku');

$notice_approval_confirmed_display = $conf->get('shift.notice.approval_confirmed');

// DB
$fname = $_SERVER["PHP_SELF"];
$con = connect2db($fname);

// パラメータ
$id = @$_POST["id"];
$passwd = @$_POST["passwd"];
$systemid = @$_POST["systemid"];
$output = @$_POST["output"];
$fnum = @$_POST["fnum"];
$noinfo = @$_POST["noinfo"];

// エラーコード
$errorcode = array(
    '101' => 'ユーザーIDが指定されていません。',
    '102' => '指定のユーザーIDはありません。',
    '103' => 'パスワードとシステムIDの両方が指定されていません。',
    '104' => '指定のユーザーIDはあるがパスワードが一致しません。',
    '105' => '指定のユーザーIDはあるがシステムIDが一致しません。',
    '106' => '指定のユーザーIDは利用停止中です。',
    '201' => '何らかのエラーが発生し情報が取得できません。'
);

// URL
$script_names = explode("/", $_SERVER["SCRIPT_NAME"]);
$url = sprintf("http://%s/%s/", $_SERVER["SERVER_NAME"], $script_names[1]);

//-------------------------------------------------------------------
// 認証
//-------------------------------------------------------------------
// ユーザIDが指定されているか
if (empty($id)) {
    doc_error_output('101');
}

// ユーザIDが存在するか
$sql = "SELECT *, get_mail_login_id(emp_id) as username FROM login JOIN empmst USING (emp_id) JOIN authmst USING (emp_id) LEFT JOIN option USING (emp_id) WHERE emp_login_id='$id'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    doc_error_output('201');
}
if (pg_num_rows($sel) != 1) {
    doc_error_output('102');
}
$emp = pg_fetch_assoc($sel);

// パスワード、システムIDが指定されているか
if (empty($passwd) and empty($systemid)) {
    doc_error_output('103');
}

// 利用停止か
if ($emp["emp_del_flg"] == 't') {
    doc_error_output('106');
}

// システムIDが一致するか
if (!empty($systemid)) {
    $sql = "SELECT prf_org_cd FROM profile";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        doc_error_output('201');
    }
    if ($systemid != md5(pg_fetch_result($sel, 0, 'prf_org_cd'))) {
        doc_error_output('105');
    }

    // ID, パスワード、システムIDが指定されていればパスワード変更
    // パスワードが'systemid='で始まる文字列だった場合はパスワード変更処理には入らない
    if (!empty($passwd) and $passwd != $emp["emp_login_pass"] and preg_match('/^systemid=/', $passwd) === 0) {
        change_passwd($id, $passwd, $emp['username']);
        $emp["emp_login_pass"] = $passwd;
    }
}

// パスワードが一致するか
else {
    if ($passwd != $emp["emp_login_pass"]) {
        doc_error_output('104');
    }
}


//-------------------------------------------------------------------
// DOM
//-------------------------------------------------------------------
$doc = domxml_new_doc("1.0");
$root = $doc->create_element("comedix");
$root = $doc->append_child($root);
$root->set_attribute("url", $url);
$root->set_attribute("version", "1.0");
$modules = $root->append_child($doc->create_element("modules"));
$modules_count = 0;

//--------------------------------------------------------------
// スケジュール（アラーム）
//--------------------------------------------------------------
if ($schedule_alarm and $emp["emp_schd_flg"] == 't' and $emp["top_schd_flg"] == 't') {

    list($num, $mes_text, $feed_array) = notice_schedule_alarm($emp["emp_id"]);

    if ($num > 0 or $noinfo != 1) {
        $modules_count++;

        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'schedule');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('スケジュール', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $infos->set_attribute("number", 1);
        $new = $infos->append_child($doc->create_element("information"));
        $new->set_attribute("name", 'new');

        $mes = $new->append_child($doc->create_element("message"));
        $mes->append_child($doc->create_text_node(mb_convert_encoding($mes_text, "UTF-8", "eucjp-win")));
        $feeds = $new->append_child($doc->create_element("feeds"));
        $feeds->set_attribute("number", $num);
        foreach ($feed_array as $feed_text) {
            $feed = $feeds->append_child($doc->create_element("feed"));
            $feed->append_child($doc->create_text_node(mb_convert_encoding($feed_text, "UTF-8", "eucjp-win")));
        }
    }
}

//--------------------------------------------------------------
// お知らせ・回覧板
//--------------------------------------------------------------
if ($emp["emp_newsuser_flg"] == 't' and $emp["top_info_flg"] == 't') {

    $return = notice_news($emp, $notice_popup);

    foreach ($return as $ret) {
        if ($ret['num'] > 0 or $noinfo != 1) {
            $modules_count++;

            $module = $modules->append_child($doc->create_element("module"));
            $module->set_attribute("name", 'newsuser');
            $modname = $module->append_child($doc->create_element("name"));
            $modname->append_child($doc->create_text_node(mb_convert_encoding($ret['title'], "UTF-8", "eucjp-win")));

            $infos = $module->append_child($doc->create_element("informations"));
            $infos->set_attribute("number", 1);
            $new = $infos->append_child($doc->create_element("information"));
            $new->set_attribute("name", 'new');

            foreach ($ret['mes'] as $message) {
                $mes = $new->append_child($doc->create_element("message"));
                $mes->append_child($doc->create_text_node(mb_convert_encoding($message, "UTF-8", "eucjp-win")));
            }
            $feeds = $new->append_child($doc->create_element("feeds"));
            $feeds->set_attribute("number", $ret['num']);
            foreach ($ret['feed'] as $feed_text) {
                $feed = $feeds->append_child($doc->create_element("feed"));
                $feed->append_child($doc->create_text_node(mb_convert_encoding($feed_text, "UTF-8", "eucjp-win")));
            }
        }
    }
}

//--------------------------------------------------------------
// 決裁・申請
//--------------------------------------------------------------
if (($emp["emp_aprv_flg"] === 't' && $emp["top_aprv_flg"] === 't') || ($emp["emp_aprv_flg"] === 't' && $emp["top_workflow_flg"] === 't')) {

    if(!$notice_approval_confirmed_display){ 
        list($num1, $mes_text1, $feed_array1) = notice_application_apply($emp["emp_id"]);
    }
    
    
    list($num2, $mes_text2, $feed_array2) = notice_application_approve($emp["emp_id"]);
    if ($num1 > 0 or $num2 > 0 or $noinfo != 1) {
        $modules_count++;

        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'application');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('決裁・申請', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $informations_number = 0;

        // 承認確定
        if ($num1 > 0 or $noinfo != 1) {
            $informations_number++;

            $apply = $infos->append_child($doc->create_element("information"));
            $apply->set_attribute("name", 'apply');

            $mes1 = $apply->append_child($doc->create_element("message"));
            $mes1->append_child($doc->create_text_node(mb_convert_encoding($mes_text1, "UTF-8", "eucjp-win")));
            $feeds1 = $apply->append_child($doc->create_element("feeds"));
            $feeds1->set_attribute("number", $num1);
        }

        // 承認待ち
        if ($num2 > 0 or $noinfo != 1) {
            $informations_number++;

            $approve = $infos->append_child($doc->create_element("information"));
            $approve->set_attribute("name", 'approve');

            $mes2 = $approve->append_child($doc->create_element("message"));
            $mes2->append_child($doc->create_text_node(mb_convert_encoding($mes_text2, "UTF-8", "eucjp-win")));
            $feeds2 = $approve->append_child($doc->create_element("feeds"));
            $feeds2->set_attribute("number", $num2);
        }

        // informations number
        $infos->set_attribute("number", $informations_number);
    }
}
//--------------------------------------------------------------
// ウェブメール
//--------------------------------------------------------------
if ($emp["emp_webml_flg"] == 't' and $emp["top_mail_flg"] == 't') {

    list($num, $mes_text, $feed_array) = notice_webmail($emp["username"], $emp["emp_login_pass"]);

    if ($num > 0 or $noinfo != 1) {
        $modules_count++;

        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'webmail');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('ウェブメール', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $infos->set_attribute("number", 1);

        $new = $infos->append_child($doc->create_element("information"));
        $new->set_attribute("name", 'new');

        $mes = $new->append_child($doc->create_element("message"));
        $mes->append_child($doc->create_text_node(mb_convert_encoding($mes_text, "UTF-8", "eucjp-win")));
        $feeds = $new->append_child($doc->create_element("feeds"));
        $feeds->set_attribute("number", $num);
    }
}

//--------------------------------------------------------------
// 伝言メモ
//--------------------------------------------------------------
if ($emp["emp_phone_flg"] == 't' and $emp["top_msg_flg"] == 't') {

    list($num, $mes_text, $feed_array) = notice_message($emp["emp_id"]);

    if ($num > 0 or $noinfo != 1) {
        $modules_count++;

        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'message');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('伝言メモ', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $infos->set_attribute("number", 1);

        // 新着
        $new = $infos->append_child($doc->create_element("information"));
        $new->set_attribute("name", 'new');

        $mes = $new->append_child($doc->create_element("message"));
        $mes->append_child($doc->create_text_node(mb_convert_encoding($mes_text, "UTF-8", "eucjp-win")));
        $feeds = $new->append_child($doc->create_element("feeds"));
        $feeds->set_attribute("number", $num);
        foreach ($feed_array as $feed_text) {
            $feed = $feeds->append_child($doc->create_element("feed"));
            $feed->append_child($doc->create_text_node(mb_convert_encoding($feed_text, "UTF-8", "eucjp-win")));
        }
    }
}
//--------------------------------------------------------------
// ファントルくん
//--------------------------------------------------------------
if ($emp["emp_inci_flg"] == 't' and $emp["top_inci_flg"] == 't') {
    list($num, $mes_text, $feed_array) = notice_hiyari_new($emp["emp_id"]);
    list($num2, $mes_text2, $feed_array2) = notice_hiyari_info($emp["emp_id"], $emp["emp_class"], $emp["emp_job"]);

    if ($num > 0 or $num2 > 0 or $noinfo != 1) {
        $modules_count++;

        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'hiyari');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('ファントルくん', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $informations_number = 0;

        // 新しいメッセージ
        if ($num > 0 or $noinfo != 1) {
            $informations_number++;

            $new = $infos->append_child($doc->create_element("information"));
            $new->set_attribute("name", 'new');

            $mes = $new->append_child($doc->create_element("message"));
            $mes->append_child($doc->create_text_node(mb_convert_encoding($mes_text, "UTF-8", "eucjp-win")));
            $feeds = $new->append_child($doc->create_element("feeds"));
            $feeds->set_attribute("number", $num);
            foreach ($feed_array as $feed_text) {
                $feed = $feeds->append_child($doc->create_element("feed"));
                $feed->append_child($doc->create_text_node(mb_convert_encoding($feed_text, "UTF-8", "eucjp-win")));
            }
        }

        // 新しいお知らせ
        if ($num2 > 0 or $noinfo != 1) {
            $informations_number++;

            $info = $infos->append_child($doc->create_element("information"));
            $info->set_attribute("name", 'info');

            $mes2 = $info->append_child($doc->create_element("message"));
            $mes2->append_child($doc->create_text_node(mb_convert_encoding($mes_text2, "UTF-8", "eucjp-win")));
            $feeds2 = $info->append_child($doc->create_element("feeds"));
            $feeds2->set_attribute("number", $num2);
            foreach ($feed_array2 as $feed_text) {
                $feed = $feeds2->append_child($doc->create_element("feed"));
                $feed->append_child($doc->create_text_node(mb_convert_encoding($feed_text, "UTF-8", "eucjp-win")));
            }
        }

        // informations number
        $infos->set_attribute("number", $informations_number);
    }
}
//--------------------------------------------------------------
// ファントルくん＋
//--------------------------------------------------------------
if ($emp["emp_fplus_flg"] == 't' and $emp["top_fplus_flg"] == 't') {

//     list($num1, $mes_text1, $feed_array1) = notice_fplus_apply($emp["emp_id"]);
    list($num2, $mes_text2, $feed_array2) = notice_fplus_approve($emp["emp_id"]);
    list($num3, $mes_text3, $feed_array3) = notice_fplus_apply_bak($emp["emp_id"]);

//     if ($num1 > 0 or $num2 > 0 or $num3 > 0 or $noinfo != 1) {
    if ($num2 > 0 or $num3 > 0 or $noinfo != 1) {
        $modules_count++;

        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'fplus');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('ファントルくん＋', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $informations_number = 0;

        // 受付済みの報告書
//         if ($num1 > 0 or $noinfo != 1) {
//             $informations_number++;

//             $info = $infos->append_child($doc->create_element("information"));
//             $info->set_attribute("name", 'apply');

//             $mes = $info->append_child($doc->create_element("message"));
//             $mes->append_child($doc->create_text_node(mb_convert_encoding($mes_text1, "UTF-8", "eucjp-win")));
//             $feeds = $info->append_child($doc->create_element("feeds"));
//             $feeds->set_attribute("number", $num1);
//         }

        // 受付待ちの報告書
        if ($num2 > 0 or $noinfo != 1) {
            $informations_number++;

            $info = $infos->append_child($doc->create_element("information"));
            $info->set_attribute("name", 'approve');

            $mes = $info->append_child($doc->create_element("message"));
            $mes->append_child($doc->create_text_node(mb_convert_encoding($mes_text2, "UTF-8", "eucjp-win")));
            $feeds = $info->append_child($doc->create_element("feeds"));
            $feeds->set_attribute("number", $num2);
        }

        // 差戻しの報告書
        if ($num3 > 0 or $noinfo != 1) {
            $informations_number++;

            $info = $infos->append_child($doc->create_element("information"));
            $info->set_attribute("name", 'back');

            $mes = $info->append_child($doc->create_element("message"));
            $mes->append_child($doc->create_text_node(mb_convert_encoding($mes_text3, "UTF-8", "eucjp-win")));
            $feeds = $info->append_child($doc->create_element("feeds"));
            $feeds->set_attribute("number", $num3);
        }

        // informations number
        $infos->set_attribute("number", $informations_number);
    }
}

//--------------------------------------------------------------
// バリテス
//--------------------------------------------------------------
if ($emp["emp_manabu_flg"] == 't' and $emp["top_manabu_flg"] == 't') {

    require_once("show_mypage_manabu.ini");

    $counts = get_baritess_counts($con, $emp["emp_id"], $fname);
    if ($counts["is_error"]) doc_error_output('201'); // エラーなら終了
    $notif_cnt = $counts["notification_count"];
    $mijukou_cnt = $counts["mijukou_count"];

    if ($notif_cnt or $noinfo != 1) {
        $modules_count++;
        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'baritess_oshirase');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('バリテス', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $infos->set_attribute("number", 1);

        $child = $infos->append_child($doc->create_element("information"));
        $child->set_attribute("name", 'new');

        $mes = $child->append_child($doc->create_element("message"));
        $mes->append_child($doc->create_text_node(mb_convert_encoding("お知らせが " . $notif_cnt . " 件あります。", "UTF-8", "eucjp-win")));
        $feeds = $child->append_child($doc->create_element("feeds"));
        $feeds->set_attribute("number", $notif_cnt);
    }

    if ($mijukou_cnt or $noinfo != 1) {
        $modules_count++;
        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'baritess_mijukou');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node(mb_convert_encoding('バリテス', "UTF-8", "eucjp-win")));

        $infos = $module->append_child($doc->create_element("informations"));
        $infos->set_attribute("number", 1);

        $child = $infos->append_child($doc->create_element("information"));
        $child->set_attribute("name", 'new');

        $mes = $child->append_child($doc->create_element("message"));
        $mes->append_child($doc->create_text_node(mb_convert_encoding("未受講の研修・講義が  " . $mijukou_cnt . " 件あります。", "UTF-8", "eucjp-win")));
        $feeds = $child->append_child($doc->create_element("feeds"));
        $feeds->set_attribute("number", $mijukou_cnt);
    }
}

//--------------------------------------------------------------
// COMPANY連携
//--------------------------------------------------------------

if ($notice_company_use === "1" && isset($notice_company_url) && isset($notice_company_timeout) && $emp["top_workflow_flg"] === 't' && $emp["emp_aprv_flg"] === 't') {

    list($company_num, $company_mes_text) = notice_company_approve($emp["emp_login_id"], $notice_company_url, $notice_company_timeout);
    if ($company_num > 0 || $noinfo != 1) {
        $modules_count++;

        $module = $modules->append_child($doc->create_element("module"));
        $module->set_attribute("name", 'company');
        $modname = $module->append_child($doc->create_element("name"));
        $modname->append_child($doc->create_text_node('COMPANY'));

        $infos = $module->append_child($doc->create_element("informations"));
        $informations_number = 0;

        $informations_number++;

        $apply = $infos->append_child($doc->create_element("information"));
        $apply->set_attribute("name", 'company');

        $mes1 = $apply->append_child($doc->create_element("message"));
        $mes1->append_child($doc->create_text_node(mb_convert_encoding($company_mes_text, "UTF-8", "eucjp-win")));
        $feeds1 = $apply->append_child($doc->create_element("feeds"));
        $feeds1->set_attribute("number", $company_num);

        // informations number
        $infos->set_attribute("number", $informations_number);
    }
}

//--------------------------------------------------------------
// 打刻
//--------------------------------------------------------------
if ($launcher_dakoku) {
    require_once 'Cmx/Model/Shift/DutyDate.php';
    // 打刻実行
    $dutydate = new Cmx_Shift_DutyDate('launcher_open');
    $dakoku_result = $dutydate->dakoku($emp['emp_personal_id'], date("Ymd"));
    if ($dakoku_result) {
        $dutydate->exec();
    }
}

//-------------------------------------------------------------------
// output
//-------------------------------------------------------------------
$modules->set_attribute("number", $modules_count);

ob_end_clean();
header("Content-Type: text/xml; charset=UTF-8");
print $doc->dump_mem(true, "UTF-8");
exit;

//-------------------------------------------------------------------
// エラーXML出力
//-------------------------------------------------------------------
function doc_error_output($errorid)
{
    global $url, $errorcode;
    $doc = domxml_new_doc("1.0");
    $root = $doc->create_element("comedix");
    $root = $doc->append_child($root);
    $root->set_attribute("url", $url);
    $root->set_attribute("version", "1.0");
    $error = $root->append_child($doc->create_element("error"));
    $error->set_attribute("code", $errorid);
    $mes = $error->append_child($doc->create_element("message"));
    $mes->append_child($doc->create_text_node(mb_convert_encoding($errorcode[$errorid], "UTF-8", "eucjp-win")));

    ob_end_clean();
    header("Content-Type: text/xml; charset=UTF-8");
    print $doc->dump_mem(true, "UTF-8");
    exit;
}

//-------------------------------------------------------------------
// 伝言メモ新着取得
//-------------------------------------------------------------------
function notice_message($emp_id)
{
    global $fnum, $con, $fname;

    $sql = <<<__SQL_END__
SELECT m.*, e.emp_lt_nm, e.emp_ft_nm
FROM message m JOIN empmst e USING (emp_id)
WHERE m.message_towhich_id='$emp_id'
  AND m.msg_receiver_flg <> '2'
  AND m.msg_receiver_flg <> '3'
ORDER BY m.message_id DESC
__SQL_END__;

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        doc_error_output('201');
    }
    if (pg_num_rows($sel) == 0) {
        return array(0, '伝言はありません。', array());
    }
    $num = pg_num_rows($sel);
    $feed = array();
    if ($fnum != "0") {
        $cnt = 0;
        while ($r = pg_fetch_assoc($sel)) {
            if (empty($r["message_company"]) and empty($r["message_name"])) {
                $feed[] = sprintf("%s %sさんより", $r["emp_lt_nm"], $r["emp_ft_nm"]);
            }
            else {
                $feed[] = sprintf("%s様より", trim("{$r["message_company"]} {$r["message_name"]}"));
            }
            $cnt++;
            if ($fnum != "" and $cnt >= $fnum) {
                break;
            }
        }
    }
    return array($num, "伝言が{$num}件あります。", $feed);
}

//-------------------------------------------------------------------
// スケジュールアラーム取得
//-------------------------------------------------------------------
function notice_schedule_alarm($emp_id)
{
    global $schedule_alarm, $fnum, $con, $fname;

    if ($schedule_alarm === "1") {
        $sql = "
            select schd_id, schd_title, schd_type, schd_start_time_v, schd_dur_v, schd_start_date, schd_imprt, schd_status
            from schdmst
            where emp_id = '$emp_id'
              and alarm_confirm=0
              and alarm>0
              and now() between schd_start_date+(substring(schd_start_time_v from 1 for 2)||':'||substring(schd_start_time_v from 3 for 2))::time - (alarm||'m')::interval and schd_start_date+(substring(schd_start_time_v from 1 for 2)||':'||substring(schd_start_time_v from 3 for 2))::time
            union
            select schd_id, schd_title, schd_type, null as schd_start_time_v, null, schd_start_date, schd_imprt, schd_status
            from schdmst2
            where emp_id = '$emp_id'
              and alarm_confirm=0
              and alarm>0
              and now() between schd_start_date - (alarm||'m')::interval and schd_start_date
            order by schd_start_date, schd_start_time_v
        ";
    }
    else if ($schedule_alarm == "2") {
        $sql = "
            select schd_id, schd_title, schd_type, schd_start_time_v, schd_dur_v, schd_start_date, schd_imprt, schd_status
            from schdmst
            where emp_id = '$emp_id'
              and alarm_confirm=0
              and alarm>0
              and now() <= schd_start_date+(substring(schd_start_time_v from 1 for 2)||':'||substring(schd_start_time_v from 3 for 2))::time
            union
            select schd_id, schd_title, schd_type, null as schd_start_time_v, null, schd_start_date, schd_imprt, schd_status
            from schdmst2
            where emp_id = '$emp_id'
              and alarm_confirm=0
              and alarm>0
              and now() <= schd_start_date
            order by schd_start_date, schd_start_time_v
            limit 5
        ";
    }

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        doc_error_output('201');
    }
    if (pg_num_rows($sel) == 0) {
        return array(0, 'スケジュールはありません。', array());
    }
    $num = pg_num_rows($sel);
    $feed = array();

    $cnt = 0;
    while ($r = pg_fetch_assoc($sel)) {
        $status = ($r["schd_status"] == "2") ? "[未承認]" : "";

        $dt = '';
        if ($r["schd_start_date"] != date('Y-m-d') or ! $r["schd_start_time_v"]) {
            $dt .= preg_replace("/\d\d\d\d\-(\d\d)\-(\d\d)/", "$1/$2", $r["schd_start_date"]);
        }
        if ($r["schd_start_time_v"]) {
            if ($dt) $dt .= " ";
            $dt .= preg_replace("/(\d\d)(\d\d)/", "$1:$2", $r["schd_start_time_v"]);
        }
        $feed[] = "$dt {$r['schd_title']}$status";
        $cnt++;
    }

    return array($num, "スケジュールが{$num}件あります。", $feed);
}

//-------------------------------------------------------------------
// 決裁・申請(承認確定)新着取得
//-------------------------------------------------------------------
function notice_application_apply($emp_id)
{
    global $fnum, $con, $fname;

    require_once("application_workflow_common_class.php");
    $obj = new application_workflow_common_class($con, $fname);
    $apply_fix_cnt = $obj->get_apply_fix($emp_id);
    if ($apply_fix_cnt > 0) {
        return array($apply_fix_cnt, "承認確定された申請書が{$apply_fix_cnt}件あります。", array());
    }
    else {
        return array(0, '承認確定された申請書はありません。', array());
    }
}

//-------------------------------------------------------------------
// 決裁・申請(承認待ち)新着取得
//-------------------------------------------------------------------
function notice_application_approve($emp_id)
{
    global $fnum, $con, $fname;

    require_once("application_workflow_common_class.php");
    $obj = new application_workflow_common_class($con, $fname);
    $approve_wait_cnt = $obj->get_approve_wait($emp_id);
    if ($approve_wait_cnt > 0) {
        return array($approve_wait_cnt, "承認待ち(未承認）の申請書が{$approve_wait_cnt}件あります。", array());
    }
    else {
        return array(0, '承認待ち(未承認）の申請書はありません。', array());
    }
}

//-------------------------------------------------------------------
// ファントルくん＋(承認確定)新着取得
//-------------------------------------------------------------------
// function notice_fplus_apply($emp_id) {
//     global $fnum, $con, $fname;

//     require_once("fplus_common_class.php");
//     $obj = new fplus_common_class($con, $fname);
//     $cnt = $obj->get_apply_fix($emp_id);
//     if($cnt > 0) {
//         return array($cnt, "受付済みの報告書が{$cnt}件あります。", array());
//     }
//     else {
//         return array(0, '受付済みの報告書はありません。', array());
//     }
// }

//-------------------------------------------------------------------
// ファントルくん＋(承認待ち)新着取得
//-------------------------------------------------------------------
function notice_fplus_approve($emp_id)
{
    global $fnum, $con, $fname;

    require_once("fplus_common_class.php");
    $obj = new fplus_common_class($con, $fname);
    $cnt = $obj->get_approve_wait($emp_id);
    if ($cnt > 0) {
        return array($cnt, "受付待ちの報告書が{$cnt}件あります。", array());
    }
    else {
        return array(0, '受付待ちの報告書はありません。', array());
    }
}

//-------------------------------------------------------------------
// ファントルくん＋(差戻し)新着取得
//-------------------------------------------------------------------
function notice_fplus_apply_bak($emp_id)
{
    global $fnum, $con, $fname;

    require_once("fplus_common_class.php");
    $obj = new fplus_common_class($con, $fname);
    $cnt = $obj->get_apply_bak($emp_id);
    if ($cnt > 0) {
        return array($cnt, "差戻しの報告書が{$cnt}件あります。", array());
    }
    else {
        return array(0, '差戻しの報告書はありません。', array());
    }
}

//-------------------------------------------------------------------
// ファントルくん・メッセージ新着取得
//-------------------------------------------------------------------
function notice_hiyari_new($emp_id) {
    global $fnum, $con, $fname;

    $sql = <<<__SQL_END__
SELECT
    rp.report_title
FROM
    (
        SELECT
            mail_id
        FROM
            inci_mail_recv_info
        WHERE
                recv_emp_id = '$emp_id'
            AND recv_read_flg = 'f'
            AND recv_del_flg = 'f'
    ) mr natural
INNER JOIN
    (
        SELECT
            mail_id,
            report_id
        FROM
            inci_mail_send_info
    ) ms natural
INNER JOIN
    (
        SELECT
            report_id,
            report_title
        FROM
            inci_report
        WHERE
                NOT kill_flg
    ) rp
ORDER BY mr.mail_id DESC
__SQL_END__;

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        doc_error_output('201');
    }
    if (pg_num_rows($sel) == 0) {
//         return array(0, '新着メッセージはありません。', array());
        return array(0, '未読のメッセージはありません。', array());
    }
    $num = pg_num_rows($sel);
    $feed = array();
    if ($fnum != "0") {
        $cnt = 0;
        while ($r = pg_fetch_assoc($sel)) {
            $feed[] = $r["report_title"];
            $cnt++;
            if ($fnum != "" and $cnt >= $fnum) {
                break;
            }
        }
    }
//     return array($num, "新着メッセージが{$num}件あります。", $feed);
    return array($num, "未読のメッセージが{$num}件あります。", $feed);
}

//-------------------------------------------------------------------
// ファントルくん・お知らせ新着取得
//-------------------------------------------------------------------
function notice_hiyari_info($emp_id, $class_id, $job_id)
{
    global $fnum, $con, $fname;

    //システム日付
    $today = date("Ymd");

    $sql = <<<__SQL_END__
SELECT inci_news.news_title FROM
(
    SELECT news_id,news_title
    FROM inci_news
    WHERE news_begin <= '$today'
      AND news_end >= '$today'
      AND (news_category='1' or (news_category='2' and class_id=$class_id) or (news_category='3' and job_id=$job_id))
) inci_news
LEFT JOIN (
    SELECT news_id
     FROM inci_accessed_news
     WHERE emp_id = '$emp_id'
) inci_accessed_news ON inci_news.news_id=inci_accessed_news.news_id
WHERE inci_accessed_news.news_id isnull
ORDER BY inci_news.news_id DESC
__SQL_END__;

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        doc_error_output('201');
    }
    if (pg_num_rows($sel) == 0) {
        return array(0, '新しいお知らせはありません。', array());
    }
    $num = pg_num_rows($sel);
    $feed = array();
    if ($fnum != "0") {
        $cnt = 0;
        while ($r = pg_fetch_assoc($sel)) {
            $feed[] = $r["news_title"];
            $cnt++;
            if ($fnum != "" and $cnt >= $fnum) {
                break;
            }
        }
    }
    return array($num, "新しいお知らせが{$num}件あります。", $feed);
}

//-------------------------------------------------------------------
// ウェブメール新着取得
// menu_webmail.iniを参考に作成
//-------------------------------------------------------------------
function notice_webmail($username, $secretkey)
{
    global $fnum, $con, $fname;
    global $color, $data_dir, $default_folder_prefix, $default_move_to_sent,
    $default_move_to_trash, $default_unseen_notify, $default_unseen_type,
    $folder_prefix, $imap_auth_mech, $imapConnection, $imapPort,
    $imapServerAddress, $key, $move_to_sent, $move_to_trash,
    $newmail_allbox, $newmail_changetitle, $newmail_enable,
    $newmail_media, $newmail_popup, $newmail_recent, $onetimepad,
    $PHP_SELF, $sent_folder, $squirrelmail_language, $trash_folder,
    $unseen_notify, $unseen_type, $use_imap_tls;

    // IMAPサーバにログイン
    $onetimepad = OneTimePadCreate(strlen($secretkey));
    $key = OneTimePadEncrypt($secretkey, $onetimepad);
    $imapConnection = sqimap_login($username, $key, $imapServerAddress, $imapPort, 1);

    // ログインできた場合
    if (is_resource($imapConnection)) {

        sqsession_register(true, 'user_is_logged_in');

        // オプション設定値を取得
        $unseen_type = getPref($data_dir, $username, 'unseen_type', $default_unseen_type);
        $unseen_notify = getPref($data_dir, $username, 'unseen_notify', $default_unseen_notify);
        $folder_prefix = getPref($data_dir, $username, 'folder_prefix', $default_folder_prefix);
        $move_to_sent = getPref($data_dir, $username, 'move_to_sent', $default_move_to_sent);
        $move_to_trash = getPref($data_dir, $username, 'move_to_trash', $default_move_to_trash);
        newmail_pref();

        // 新着メールの数を取得
        $totalNew = get_newmail_count($imapConnection, $newmail_recent, $newmail_allbox, $sent_folder, $trash_folder);
    }
    else {
        $totalNew = 0;
    }

    // IMAPサーバからログアウト
    sqimap_logout($imapConnection);

    if ($totalNew > 0) {
        return array($totalNew, "新着メッセージが{$totalNew}通あります。", array());
    }
    else {
        return array(0, '新着メッセージはありません。', array());
    }
}

//-------------------------------------------------------------------
// お知らせ・回覧板新着取得
//-------------------------------------------------------------------
function notice_news($emp, $notice)
{
    global $fnum, $con, $fname, $conf;

    // emp
    $emp_id = $emp['emp_id'];
    $job_id = $emp['emp_job'];

    //システム日付
    $today = date("Ymd");

    //入職日以前のお知らせはポップアップしない
    if ($emp['emp_join']) {
        $join_sql = "and news.news_date >= '{$emp['emp_join']}'";
    }

    //通達区分ポップアップ
    if ($notice) {
        $notice_sql = "and ((newsnotice.popup_flg and newsnotice.notice_id is not null) ";

        $global_popup_flg = $conf->get('news.global_popup_flg');
        if ($global_popup_flg === '1') {
            $notice_sql .= "or newsnotice.notice_id is null ";
        }
        $notice_sql .= ")";
        $notice_order = 'newsnotice.order_no, newsnotice2.order_no,';
    }

    $sql = "
select
    news.*,
    newsref.ref_time,
    newsref.read_flg,
    newsnotice.notice_name,
    newsnotice2.notice2_name,
    newsnotice.order_no as n1_order,
    newsnotice2.notice2_name as n2_order
from
    news
    left join newsref on  news.news_id = newsref.news_id and newsref.emp_id = '$emp_id'
    left join newsnotice on  news.notice_id = newsnotice.notice_id
    left join newsnotice2 on  news.notice2_id = newsnotice2.notice2_id
    left join newscate on  news.news_add_category = newscate.newscate_id
where
    news.news_begin <= '$today'
and news.news_end >= '$today'
and news.news_del_flg = 'f'
and newsref.ref_time is null
and (newsref.read_flg is null or newsref.read_flg = 'f')
$join_sql
$notice_sql
and (
        (
            (
                news.news_add_category = 1
            or  (
                    news.news_add_category = 2
                and exists (
                        select * from newsdept
                        where
                            newsdept.news_id = news.news_id
                        and (newsdept.class_id in (
                                    select emp_class from empmst where emp_id = '$emp_id'
                                    union
                                    select emp_class from concurrent where concurrent.emp_id = '$emp_id'
                                ))
                        and (
                                newsdept.atrb_id in (
                                    select emp_attribute from empmst where empmst.emp_id = '$emp_id'
                                    union
                                    select emp_attribute from concurrent where concurrent.emp_id = '$emp_id'
                                )
                            or  newsdept.atrb_id is null
                            )
                        and (
                                newsdept.dept_id in (
                                    select emp_dept from empmst where empmst.emp_id = '$emp_id'
                                    union
                                    select emp_dept from concurrent where concurrent.emp_id = '$emp_id'
                                )
                            or  newsdept.dept_id is null
                            )
                    )
                )
            or  (
                    news.news_add_category = 3
                and (
                        news.job_id = $job_id
                    or  (
                            news.job_id is null
                        and exists (
                                select * from newsjob where newsjob.news_id = news.news_id and newsjob.job_id = $job_id
                            )
                        )
                    )
                )
            )
        and (
                news.st_id_cnt = 0
            or  (
                    news.st_id_cnt > 0
                and exists (
                        select * from newsst
                        where
                            newsst.news_id = news.news_id
                        and newsst.st_id in (
                                select emp_st from empmst where empmst.emp_id = '$emp_id'
                                union
                                select emp_st from concurrent where concurrent.emp_id = '$emp_id'
                            )
                    )
                )
            )
        )
    or  (
            news.news_add_category >= 4
        and news.news_add_category < 10000
        and (
                (
                    newscate.standard_cate_id = 1
                or  (
                        newscate.standard_cate_id = 2
                    and exists (
                            select * from newscatedept
                            where
                                newscatedept.newscate_id = news.news_add_category
                            and (newscatedept.class_id in (
                                        select emp_class from empmst where empmst.emp_id = '$emp_id'
                                        union
                                        select emp_class from concurrent where concurrent.emp_id = '$emp_id'
                                    ))
                            and (
                                    (newscatedept.atrb_id in (
                                            select emp_attribute from empmst where empmst.emp_id = '$emp_id'
                                            union
                                            select emp_attribute from concurrent where concurrent.emp_id = '$emp_id'
                                        ))
                                or  (newscatedept.atrb_id is null)
                                )
                            and (
                                    (newscatedept.dept_id in (
                                            select emp_dept from empmst where empmst.emp_id = '$emp_id'
                                            union
                                            select emp_dept from concurrent where concurrent.emp_id = '$emp_id'
                                        ))
                                or  (newscatedept.dept_id is null)
                                )
                        )
                    )
                or  (
                        newscate.standard_cate_id = 3
                    and (
                            newscate.job_id = $job_id
                        or  (
                                newscate.job_id is null
                            and exists (
                                    select * from newscatejob
                                    where
                                        newscatejob.newscate_id = newscate.newscate_id
                                    and newscatejob.job_id = $job_id
                                )
                            )
                        )
                    )
                )
            and (
                    newscate.st_id_cnt = 0
                or  (
                        newscate.st_id_cnt > 0
                    and exists (
                            select
                                *
                            from
                                newscatest
                            where
                                newscatest.newscate_id = news.news_add_category
                            and newscatest.st_id in (
                                    select emp_st from empmst where empmst.emp_id = '$emp_id'
                                    union
                                    select emp_st from concurrent where concurrent.emp_id = '$emp_id'
                                )
                        )
                    )
                )
            )
        )
    or  (
            news.news_add_category = 10000
        and exists (
                select * from newscomment
                where
                    news.news_id = newscomment.news_id
                and newscomment.emp_id = '$emp_id'
            )
        )
    )
order by
    $notice_order
    news.news_date desc,
    news.news_id desc
    ";

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        doc_error_output('201');
    }

    // 1件も新着なし
    if (pg_num_rows($sel) == 0) {
        return array(array('num' => 0, 'title' => 'お知らせ・回覧板', 'mes' => array('新しいお知らせはありません。'), 'feed' => array()));
    }

    // 通達区分ポップアップあり
    if ($notice) {
        $cnt = array();
        $feed = array();

        while ($r = pg_fetch_assoc($sel)) {
            $id = !empty($r['notice_id']) ? $r['notice_id'] : 0;
            $id2 = !empty($r['notice2_id']) ? $r['notice2_id'] : 0;

            $cnt[$id][$id2] ++;
            if ($fnum != "0") {
                $feed[$id][] = $r["news_title"];
            }

            if ($r['notice_id']) {
                $n1name[$r['notice_id']] = $r['notice_name'];
            }
            if ($r['notice2_id']) {
                $n2name[$r['notice2_id']] = $r['notice2_name'];
            }
        }

        foreach ($cnt as $id => $cnt1) {
            $num = 0;
            $mes = array();

            foreach ($cnt1 as $id2 => $cnt2) {
                $name = $id2 !== 0 ? "({$n2name[$id2]}) " : "";
                $mes[] = "{$name}新しいお知らせが{$cnt2}件あります。";
                $num += $cnt2;
            }

            if ($fnum != "0" or $fnum != "") {
                $feed[$id] = array_slice($feed[$id], 0, $fnum);
            }

            $return[] = array(
                'num' => $num,
                'title' => "[{$n1name[$id]}] お知らせ",
                'mes' => $mes,
                'feed' => $feed[$id],
            );
        }
        return $return;
    }

    // 通達区分ポップアップなし
    else {
        $num = pg_num_rows($sel);
        $feed = array();
        if ($fnum != "0") {
            $cnt = 0;
            while ($r = pg_fetch_assoc($sel)) {
                $feed[] = $r["news_title"];
                $cnt++;
                if ($fnum != "" and $cnt >= $fnum) {
                    break;
                }
            }
        }
        return array(array('num' => $num, 'title' => 'お知らせ・回覧板', 'mes' => array("新しいお知らせが{$num}件あります。"), 'feed' => $feed));
    }
}

// COMPANYのURLから件数取得
// 1以上の数値で有れば通知、0で有れば通知しない
// それ以外の時はアラート
function notice_company_approve($emp_login_id, $notice_company_url, $notice_company_timeout)
{
    // 件数取得用URL
    $url = $notice_company_url . "&empno=" . $emp_login_id;
    $approve_wait_cnt = trim(shell_exec('wget -q -T ' . escapeshellarg($notice_company_timeout) . ' -t 1 -O - ' . escapeshellarg($url)));
    if ($approve_wait_cnt > 0) {
        return array($approve_wait_cnt, "承認待ちの申請書が{$approve_wait_cnt}件あります。", array());
    }
    else if ($approve_wait_cnt === "0") {
        return array(0, '承認待ちの申請書はありません。', array());
    }
    else {
        return array(1, "新着情報の取得に失敗しました。", array());
    }
}

//-------------------------------------------------------------------
// パスワード変更
//-------------------------------------------------------------------
function change_passwd($id, $passwd, $username)
{
    global $con, $fname, $imapServerAddress;

    // 環境設定情報を取得
    $sel = select_from_table($con, "SELECT use_cyrus FROM config", "", $fname);
    if ($sel == 0) {
        pg_close($con);
        doc_error_output('201');
    }
    $use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

    // loginテーブルupdate
    $data = array(
        "emp_login_pass" => $passwd,
        "pass_change_date" => date("Ymd"),
    );
    $cond = "WHERE emp_login_id='{$id}'";
    $upd = update_set_table($con, "UPDATE login SET", array_keys($data), array_values($data), $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        doc_error_output('201');
    }

    // コミット処理
    pg_query($con, "commit");

    // CoMedixのCyrusを使う場合
    if ($use_cyrus == "t") {
        if (preg_match("/[a-z]/", $username) > 0) {
            $dir = getcwd();
            if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
                exec("/usr/bin/expect -f $dir/expect/updmailbox.exp $username $username $passwd");
            }
            else {
                exec("/usr/bin/expect -f $dir/expect/rsyncupdmailbox.exp $username $username $passwd $imapServerAddress $dir");
            }
        }
    }
}
