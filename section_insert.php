<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");
require("label_by_profile_type.ini");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,28,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

//正規表現チェック
if($sect_name ==""){
	echo("<script language=\"javascript\">alert(\"{$section_title}名を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

/*
$check = special_char_check($sect_name);
if($check == 0){
	echo("<script language=\"javascript\">alert(\"特殊な文字は使用しないでください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
*/

//ＤＢへ診療科名を登録する

$cond = "";
$sel = select_from_table($con,$SQL30,$cond,$fname);	//診療科ＩＤの最大値を取得
$rows = pg_numrows($sel);
if($rows == 0){
	$next_id = "1";
}else{
	if($sel == 0){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	$max = pg_result($sel,0,"max");
	$next_id = $max + 1;
}

$content = array(1,"$next_id","$sect_name","f");

$in = insert_into_table($con,$SQL31,$content,$fname);	//診療科情報を挿入
pg_close($con);											//ＤＢとのコネクションを閉じる

if($in == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}else{
	echo("<script language=\"javascript\">location.href=\"./entity_menu.php?session=$session\";</script>\n");
	exit;
}

?>