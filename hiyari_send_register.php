<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

if($postback_mode == "update")
{
	// トランザクションの開始
	pg_query($con, "begin transaction");

	set_send_input_items($con, $fname, $_POST);

	// トランザクションをコミット
	pg_query($con, "commit");
}


$arr_usable_auth = array();
$arr_inci = get_inci_mst($con, $fname);
foreach($arr_inci as $auth => $arr_val)
{
	if(is_usable_auth($auth,$fname,$con))
	{
		$arr_usable_auth[$auth] = $arr_val["auth_name"];
	}
}

$auths = "";
$arr_key = array_keys($arr_usable_auth);
for($i=0; $i<count($arr_key); $i++)
{
	if($auths != "")
	{
		$auths .= ",";
	}
	$auths .= $arr_key[$i];
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | 送信先設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function init_page()
{

	<?
	for($i=0; $i<count($arr_key); $i++)
	{
		$auth = strtolower($arr_key[$i]);
	?>
	obj = document.mainform.elements['<?=$auth?>_is_send'];
	change_radio(obj, '<?=$auth?>_recv_class');
	<?
	}
	?>
	obj = document.mainform.elements['doctor_is_send'];
	change_radio(obj, 'doctor_recv_class');
}

function change_radio(obj, radio)
{
	
	if (!obj) //利用できない場合
	{
		return;
	}
	
	if (obj.checked) {
		flg = false;
	} else {
		flg = true;
	}
	var rd_btn = document.mainform.elements[radio];
	if(rd_btn.length) {
		for (var i = 0; i < rd_btn.length; i++) {
			rd_btn[i].disabled = flg;
		}
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="#" method="post">
<input type="hidden" name="postback_mode" value="update">
<input type="hidden" name="auths" value="<?=$auths?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">
<font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j14">

<?
foreach($arr_usable_auth as $auth => $auth_name)
{
	$checked = "";

	$to_cc_div = get_new_report_mail_recv_default($con,$fname,$auth);
	if($to_cc_div != "")
	{
		$checked = "checked";
	}
	else
	{
		$to_cc_div = 'TO';
	}

	$auth = strtolower($auth);
?>
<nobr><input type="checkbox" name="<?=$auth?>_is_send" value="t" onclick="change_radio(this, '<?=$auth?>_recv_class');" <?=$checked?>><?=htmlspecialchars($auth_name)?>に送信する&nbsp;&nbsp;(<input type="radio" name="<?=$auth?>_recv_class" value="TO" <?if($to_cc_div == 'TO') {echo(" checked");}?>>TO&nbsp;&nbsp;<input type="radio" name="<?=$auth?>_recv_class" value="CC"  <?if($to_cc_div == 'CC') {echo(" checked");}?>>CC)</nobr><br>
<?
}
?>

<?
	$checked = "";

	$to_cc_div = get_new_report_mail_recv_default_doctor($con,$fname);
	if($to_cc_div != "")
	{
		$checked = "checked";
	}
	else
	{
		$to_cc_div = 'TO';
	}
?>

<nobr><input type="checkbox" name="doctor_is_send" value="t" onclick="change_radio(this, 'doctor_recv_class');" <?=$checked?>>主治医に送信する&nbsp;&nbsp;(<input type="radio" name="doctor_recv_class" value="TO" <?if($to_cc_div == 'TO') {echo(" checked");}?>>TO&nbsp;&nbsp;<input type="radio" name="doctor_recv_class" value="CC"  <?if($to_cc_div == 'CC') {echo(" checked");}?>>CC)</nobr><br>



<table width="550" border="0" cellspacing="1" cellpadding="0">
<tr>
<td align="right">
<input type="submit" value="更新" onclick="">
</td>
</tr>
</table>

</font>
</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>

</form>
</body>
</html>

<?

/**
 * 送信先設定情報登録処理
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param object $inputs 画面入力情報
 */
function set_send_input_items($con, $fname, $inputs)
{

	$auths = $inputs["auths"];
	$arr_auth = split(",", $auths);

	// 送信宛先
	foreach($arr_auth as $val)
	{
		$recv_class = $inputs[strtolower($val)."_recv_class"];
		update_new_report_mail_recv_default($con,$fname,$val,$recv_class);
	}
	
	//主治医
	$doctor_recv_class = $inputs["doctor_recv_class"];
	update_new_report_mail_recv_default_doctor($con,$fname,$doctor_recv_class);
}




//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>