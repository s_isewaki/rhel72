<?php

function get_fplus_hiyari_total_level($occurences_year, $hospital_id){

	if (!$fname) global $fname;
	if (!$fname) $fname = $_SERVER["PHP_SELF"];
	if (!$con) global $con;
	if (!$con) $con = connect2db($fname);

	$occurences_next_year = (int)$occurences_year+1;
	//**************************
	// 検索
	//**************************
	$sql = " SELECT ".
		" 	hyr.level ".
		" 	, count(hyr.report_id) as count ".
		" FROM ".
		" 	(SELECT ".
		" 		data.eid_id ".
		" 		, data.eis_id ".
		" 		, data.report_id ".
		" 		, repo.registration_date ".
		" 		, max(case data.grp_code  ".
		" 			WHEN '90' THEN case data.easy_item_code ".
		" 						WHEN '10' THEN data.input_item ".
		" 						ELSE NULL ".
		" 					END ".
		" 			ELSE NULL ".
		" 		  END) as level ".
		" 		, repo.registrant_class ".
		" 		, repo.registrant_attribute ".
		" 		, repo.registrant_dept ".
		" 		, repo.registrant_room ".
		" 	FROM ".
		" 		inci_report repo ".
		" 		,inci_easyinput_data data ".
		" 		,inci_easyinput_item_mst item ".
		" 		,fplus_hospital_master hosp ".
		" 	WHERE ".
		" 		repo.eid_id = data.eid_id ".
		" 		AND repo.eis_id = data.eis_id ".
		" 		AND repo.report_id = data.report_id ".
		" 		AND data.grp_code = item.grp_code ".
		" 		AND data.easy_item_code = item.easy_item_code ".
		" 		AND COALESCE(repo.registrant_class,0) = COALESCE(hosp.class_id,0) ".
		" 		AND COALESCE(repo.registrant_attribute,0) = COALESCE(hosp.attribute_id,0) ".
		" 		AND COALESCE(repo.registrant_dept,0) = COALESCE(hosp.dept_id,0) ".
		" 		AND COALESCE(repo.registrant_room,0) = COALESCE(hosp.room_id,0) ".
		" 		AND hosp.hospital_id = '$hospital_id' ".
		" 		AND repo.registration_date between '$occurences_year'||'/04/01' and '$occurences_next_year'||'/3/31' ".
		" 	group by ".
		" 		data.eid_id ".
		" 		, data.eis_id ".
		" 		, data.report_id ".
		" 		, repo.registration_date ".
		" 		, repo.registrant_class ".
		" 		, repo.registrant_attribute ".
		" 		, repo.registrant_dept ".
		" 		, repo.registrant_room) hyr ".
		" WHERE ".
		" 	hyr.level != 'LVh' ".
		" 	AND hyr.level != 'LV3' ".
		" 	AND hyr.level != 'L4a' ".
		" 	AND hyr.level != 'L4b' ".
		" 	AND hyr.level != 'LVo' ".
		" group by ".
		" 	hyr.level ".
		" order by ".
		" 	level ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;
}

function get_fplus_hiyari_total($occurences_year, $hospital_id){

	if (!$fname) global $fname;
	if (!$fname) $fname = $_SERVER["PHP_SELF"];
	if (!$con) global $con;
	if (!$con) $con = connect2db($fname);

	$occurences_next_year = (int)$occurences_year+1;
	//**************************
	// 検索
	//**************************
	$sql = " SELECT ".
		" 	hyr.level ".
		" 	, hyr.job ".
		" 	, count(hyr.report_id) as count ".
		" FROM ".
		" 	(SELECT ".
		" 		data.eid_id ".
		" 		, data.eis_id ".
		" 		, data.report_id ".
		" 		, repo.registration_date ".
		" 		, max(case data.grp_code  ".
		" 			WHEN '90' THEN case data.easy_item_code ".
		" 						WHEN '10' THEN data.input_item ".
		" 						ELSE NULL ".
		" 					END ".
		" 			ELSE NULL ".
		" 		  END) as level ".
		" 		, max(case data.grp_code  ".
		" 			WHEN '3050' THEN case data.easy_item_code ".
		" 						WHEN '30' THEN lpad(data.input_item,2,'0') ".
		" 						ELSE NULL ".
		" 					END ".
		" 			ELSE NULL ".
		" 		  END) as job ".
		" 		, repo.registrant_class ".
		" 		, repo.registrant_attribute ".
		" 		, repo.registrant_dept ".
		" 		, repo.registrant_room ".
		" 	FROM ".
		" 		inci_report repo ".
		" 		,inci_easyinput_data data ".
		" 		,inci_easyinput_item_mst item ".
		" 		,fplus_hospital_master hosp ".
		" 	WHERE ".
		" 		repo.eid_id = data.eid_id ".
		" 		AND repo.eis_id = data.eis_id ".
		" 		AND repo.report_id = data.report_id ".
		" 		AND data.grp_code = item.grp_code ".
		" 		AND data.easy_item_code = item.easy_item_code ".
		" 		AND COALESCE(repo.registrant_class,0) = COALESCE(hosp.class_id,0) ".
		" 		AND COALESCE(repo.registrant_attribute,0) = COALESCE(hosp.attribute_id,0) ".
		" 		AND COALESCE(repo.registrant_dept,0) = COALESCE(hosp.dept_id,0) ".
		" 		AND COALESCE(repo.registrant_room,0) = COALESCE(hosp.room_id,0) ".
		" 		AND hosp.hospital_id = '$hospital_id' ".
		" 		AND repo.registration_date between '$occurences_year'||'/04/01' and '$occurences_next_year'||'/3/31' ".
		" 	group by ".
		" 		data.eid_id ".
		" 		, data.eis_id ".
		" 		, data.report_id ".
		" 		, repo.registration_date ".
		" 		, repo.registrant_class ".
		" 		, repo.registrant_attribute ".
		" 		, repo.registrant_dept ".
		" 		, repo.registrant_room) hyr ".
		" WHERE ".
		" 	hyr.level != 'LVh' ".
		" 	AND hyr.level != 'LV3' ".
		" 	AND hyr.level != 'L4a' ".
		" 	AND hyr.level != 'L4b' ".
		" 	AND hyr.level != 'LVo' ".
		" group by ".
		" 	hyr.level ".
		" 	, hyr.job ".
		" order by ".
		" 	job ".
		" 	, level ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;
}

function get_fplus_hiyari_total_fall($occurences_year, $hospital_id){

	if (!$fname) global $fname;
	if (!$fname) $fname = $_SERVER["PHP_SELF"];
	if (!$con) global $con;
	if (!$con) $con = connect2db($fname);

	$occurences_next_year = (int)$occurences_year+1;
	//**************************
	// 検索
	//**************************
	$sql = " SELECT ".
		" 	hyr.level ".
		" 	, count(hyr.report_id) as count ".
		" FROM ".
		" 	(SELECT ".
		" 		data.eid_id ".
		" 		, data.eis_id ".
		" 		, data.report_id ".
		" 		, repo.registration_date ".
		" 		, max(case data.grp_code  ".
		" 			WHEN '90' THEN case data.easy_item_code ".
		" 						WHEN '10' THEN data.input_item ".
		" 						ELSE NULL ".
		" 					END ".
		" 			ELSE NULL ".
		" 		  END) as level ".
		" 		, max(case data.grp_code  ".
		" 			WHEN '980' THEN case data.easy_item_code ".
		" 						WHEN '10' THEN data.input_item ".
		" 						ELSE NULL ".
		" 					END ".
		" 			ELSE NULL ".
		" 		  END) as fall ".
		" 		, repo.registrant_class ".
		" 		, repo.registrant_attribute ".
		" 		, repo.registrant_dept ".
		" 		, repo.registrant_room ".
		" 	FROM ".
		" 		inci_report repo ".
		" 		,inci_easyinput_data data ".
		" 		,inci_easyinput_item_mst item ".
		" 		,fplus_hospital_master hosp ".
		" 	WHERE ".
		" 		repo.eid_id = data.eid_id ".
		" 		AND repo.eis_id = data.eis_id ".
		" 		AND repo.report_id = data.report_id ".
		" 		AND data.grp_code = item.grp_code ".
		" 		AND data.easy_item_code = item.easy_item_code ".
		" 		AND COALESCE(repo.registrant_class,0) = COALESCE(hosp.class_id,0) ".
		" 		AND COALESCE(repo.registrant_attribute,0) = COALESCE(hosp.attribute_id,0) ".
		" 		AND COALESCE(repo.registrant_dept,0) = COALESCE(hosp.dept_id,0) ".
		" 		AND COALESCE(repo.registrant_room,0) = COALESCE(hosp.room_id,0) ".
		" 		AND hosp.hospital_id = '$hospital_id' ".
		" 		AND repo.registration_date between '$occurences_year'||'/04/01' and '$occurences_next_year'||'/3/31' ".
		" 	group by ".
		" 		data.eid_id ".
		" 		, data.eis_id ".
		" 		, data.report_id ".
		" 		, repo.registration_date ".
		" 		, repo.registrant_class ".
		" 		, repo.registrant_attribute ".
		" 		, repo.registrant_dept ".
		" 		, repo.registrant_room) hyr ".
		" WHERE ".
		" 	hyr.level in ('L3b','LV4','LV5') ".
		" 	AND hyr.fall is not null ".
		" group by ".
		" 	hyr.level ".
		" order by ".
		" 	level ";


	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;
}

function convert_japanese_year($year, $month, $day) {
	$date = (int)sprintf("%04d%02d%02d", $year, $month, $day);
	
	if ($date >= 19890108) { //平成元年（1989年1月8日以降）
		$name = "平成";
		$year -= 1988;
	} else if ($date >= 19261225) { //昭和元年（1926年12月25日以降）
		$name = "昭和";
		$year -= 1925;
	} else if ($date >= 19120730) { //大正元年（1912年7月30日以降）
		$name = "大正";
		$year -= 1911;
	} else if ($date >= 18680125) { //明治元年（1868年1月25日以降）
		$name = "明治";
		$year -= 1867;
	}
	return $name.(string)$year;
}
?>
