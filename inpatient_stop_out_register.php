<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($subsys == "med") ? 57 : 14;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付のデフォルト値を設定
$from_yr = date("Y");
$from_mon = date("m");
$from_day = date("d");
$to_yr = $from_yr;
$to_mon = $from_mon;
$to_day = $from_day;

// データベースに接続
$con = connect2db($fname);

// 患者情報を取得
$sql = "select * from ptifmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");
$sex = get_sex_string(pg_fetch_result($sel, 0, "ptif_sex"));
$age = get_age(pg_fetch_result($sel, 0, "ptif_birth"));

// 入院日を取得
$sql = "select inpt_in_dt from inptmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql ,$cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$inpt_in_dt = pg_fetch_result($sel, 0, "inpt_in_dt");

// 外出履歴を取得
$sql = "select out_date, ret_date, place from inptgoout";
$cond = "where ptif_id = '$pt_id' and out_type = '2' and out_date >= '$inpt_in_dt' order by out_date desc";
$sel_history = select_from_table($con, $sql, $cond, $fname);
if ($sel_history == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
?>
<title>病床管理｜外泊<? if ($subsys != "med") { ?>登録<? } else { ?>参照<? } ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function openDetail(date) {
	window.open('inpatient_stop_out_update.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&date='.concat(date), 'newwin2', 'width=640,height=480,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="860" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>外泊<? if ($subsys != "med") { ?>登録<? } else { ?>参照<? } ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="inpt" action="inpatient_stop_out_insert.php" method="post">
<table width="860" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($pt_id); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">性別</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($sex); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">年齢</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($age); ?>歳</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="860" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="180">
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border:#5279a5 solid 1px; background-color:#f6f9ff;">
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_sub_menu.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">基礎データ</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_detail_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">入院情報</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_move_reserve_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">転棟・転科</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_go_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">外出</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><b>外泊</b></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="outpatient_out_reserve_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">退院予定</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="outpatient_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">退院</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_family_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">家族付き添い</a></font></td>
</tr>
</table>
</td>
<td width="5"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">開始日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><select name="from_yr"><? show_select_years_future(2, $from_yr); ?></select>/<select name="from_mon"><? show_select_months($from_mon); ?></select>/<select name="from_day"><? show_select_days($from_day); ?></select>　食止め：<select name="stop_out_fd1"><option value="1">朝から<option value="2">昼から<option value="3">夕から</select></font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">終了日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><select name="to_yr"><? show_select_years_future(2, $to_yr); ?></select>/<select name="to_mon"><? show_select_months($to_mon); ?></select>/<select name="to_day"><? show_select_days($to_day); ?></select>　食止め：<select name="stop_out_fd2"><option value="1">朝まで<option value="2">昼まで<option value="3">夕まで</select></font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">外泊先</font></td>
<td colspan="3"><input type="text" name="place" size="40" maxlength="30" style="ime-mode:active;"></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">コメント</font></td>
<td colspan="3"><textarea name="comment" cols="40" rows="5" style="ime-mode:active;"></textarea></td>
</tr>
</table>
<? if ($subsys != "med") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>登録履歴</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="list">
<tr bgcolor="#f6f9ff" valign="top">
<td width="290"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">期間</font></td>
<td width="250"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">外泊先</font></td>
<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">詳細</font></td>
</tr>
<?
while ($row = pg_fetch_array($sel_history)) {
	$tmp_out_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["out_date"]);
	$tmp_ret_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["ret_date"]);

	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">{$tmp_out_date}〜{$tmp_ret_date}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">{$row["place"]}</font></td>\n");
	echo("<td align=\"center\"><input type=\"button\" value=\"詳細\" onclick=\"openDetail('{$row["out_date"]}');\"></td>\n");
	echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function get_sex_string($sex) {
	switch ($sex) {
	case "1":
		return "男";
	case "2":
		return "女";
	default:
		return "不明";
	}
}

function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}
?>
