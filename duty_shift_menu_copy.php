
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 過去月コピー「子画面」</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
//require_once("show_select_values.ini");

require_once("yui_calendar_util.ini");

require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
	///-----------------------------------------------------------------------------
	//現在日付の取得
	///-----------------------------------------------------------------------------
	$date = getdate();
	$now_yyyy = $date["year"];
	$now_mm = $date["mon"];
	$now_dd = $date["mday"];
	///-----------------------------------------------------------------------------
	//各変数値の初期値設定
	///-----------------------------------------------------------------------------
	if ($date_y1 == "") { $date_y1 = $duty_yyyy; };
	if ($date_m1 == "") { $date_m1 = $duty_mm - 1; };
	if ((int)$date_m1 < 1) {
		$date_y1--;
		$date_m1 = 12;
	}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<?
	///-----------------------------------------------------------------------------
	// 外部ファイルを読み込む
	// カレンダー作成、関数出力
	///-----------------------------------------------------------------------------
//	write_yui_calendar_use_file_read_0_12_2();
//	write_yui_calendar_script2(1);
?>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 「ＯＫ」開始年月を設定する
	///-----------------------------------------------------------------------------
	function OkBtn(duty_yyyy, duty_mm) {
		var start_yyyy = document.mainform.date_y1.value;
		var start_mm = document.mainform.date_m1.value;

		//開始年月を指定して過去コピーを実行
		if(parent.opener && !parent.opener.closed && parent.opener.add_copy_yyyymm){
			parent.opener.add_copy_yyyymm(start_yyyy, start_mm);
		}

		window.close();
	}
	///-----------------------------------------------------------------------------
	// 「キャンセル」
	///-----------------------------------------------------------------------------
	function CancelBtn() {
		window.close();
	}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<!-- <body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();"> -->
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>コピー元の過去月設定</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr height="22">
		<td width="20%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>コピー月</b></font></td>
		<td width="80%" align=""><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<?
			///-----------------------------------------------------------------------------
			// 年
			///-----------------------------------------------------------------------------
			echo("<select id=\"date_y1\" name=\"date_y1\">\n");
			$num = 10;
			$fix = $date_y1;
			$now = date("Y");

			if ($fix > 0) {
				$num = ($now - $num + 1 > $fix) ? $now - $fix + 1 : $num;
			}

			for ($i = 0; $i < $num; $i++) {
				$yr = $now - $i;
				echo("<option value=\"$yr\"");
				if ($fix == $yr) {
					echo(" selected");
				}
				echo(">$yr</option>\n");
			}
			echo("</select>年\n");
			///-----------------------------------------------------------------------------
			// 月
			///-----------------------------------------------------------------------------
			echo("<select id=\"date_m1\" name=\"date_m1\">\n");
			for ($i = 1; $i <= 12; $i++) {
				$val = sprintf("%02d", $i);
				echo("<option value=\"$val\"");
				if ($i == $date_m1) {
					echo(" selected");
				}
				echo(">$i</option>\n");
			}
			echo("</select>月\n");
			///-----------------------------------------------------------------------------
			// カレンダー
			///-----------------------------------------------------------------------------
//			echo("<div id=\"cal1Container\" style=\"position:absolute;display:none;z-index:10000;\"></div>\n");
			echo("</font>\n");
//		   	echo("<img src=\"img/calendar_link.gif\" style=\"cursor:pointer;\" onclick=\"show_cal1()\">\n");
		?>
		</td>
		</tr>
		</table>

		<br>
		<br>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「ＯＫ」「キャンセル」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="100%" align="center">
			<input type="button" value="　　ＯＫ　　" onclick="OkBtn(<? echo($duty_yyyy); ?>, <? echo($duty_mm); ?>);">
			<input type="button" value="キャンセル" onclick="CancelBtn();">
		</td>
		</tr>
	</table>
	</form>
</center>
</body>
<? pg_close($con); ?>
</html>

