<?php
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');

require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;

///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo('<script type="text/javascript" src="js/showpage.js"></script>');
    echo('<script type="text/javascript">showLoginPage(window);</script>');
    exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if ($con == "0") {
    echo('<script type="text/javascript" src="js/showpage.js"></script>');
    echo('<script type="text/javascript">showLoginPage(window);</script>');
    exit;
}

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo('<script type="text/javascript" src="js/showpage.js"></script>');
    echo('<script type="text/javascript">showLoginPage(window);</script>');
    exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);

///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
// system_config
$conf = new Cmx_SystemConfig();

// 希望一致
$match_color = $conf->get('shift.hope.match.color');
if (empty($match_color)) {
    $match_color = '#000000';
}
$match_bgcolor = $conf->get('shift.hope.match.bgcolor');
if (empty($match_bgcolor)) {
    $match_bgcolor = '#00ff00';
}

// 希望不一致
$unmatch_color = $conf->get('shift.hope.unmatch.color');
if (empty($unmatch_color)) {
    $unmatch_color = '#000000';
}
$unmatch_bgcolor = $conf->get('shift.hope.unmatch.bgcolor');
if (empty($unmatch_bgcolor)) {
    $unmatch_bgcolor = '#ffff00';
}

// ハイライト
$highlight_color = $conf->get('shift.highlight.color');
if (empty($highlight_color)) {
    $highlight_color = 'noset';
}
$highlight_bordercolor = $conf->get('shift.highlight.bordercolor');
if (empty($highlight_bordercolor)) {
    $highlight_bordercolor = '#ff8080'; //初期値は淡い赤
}
if ($highlight_bordercolor == 'noset') {
    $highlight_bordercolor = '#5279a5';
    $wk_bdstyle = '3px solid '.$highlight_bordercolor;
}
else {
    $wk_bdstyle = '3px solid '.$highlight_bordercolor;
}
///-----------------------------------------------------------------------------
//追加オプションを取得
///-----------------------------------------------------------------------------
$tosYotei_button_flg = $conf->get('shift.tosYotei_button_flg');
$year_paid_hol_btn_flg = $conf->get('shift.year_paid_hol_btn_flg');
$timecard_error_btn_flg =  $conf->get('shift.timecard_error_btn_flg');

///-----------------------------------------------------------------------------
//カラー情報（色）を取得
///-----------------------------------------------------------------------------
$font_color_array = $obj->get_font_color_array();
$back_color_array = $obj->get_back_color_array();
$high_color_array = $obj->get_high_color_array();

pg_close($con);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <!-- <title>CoMedix 勤務シフト作成｜管理｜色指定</title> -->
        <title>CoMedix 勤務シフト作成｜管理｜オプション</title>
        <script type="text/javascript" src="js/fontsize.js"></script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript">
            $(function() {
                $('select.font_color_id').bind('click change', change_preview);
                $('select.back_color_id').bind('click change', change_preview);
            });
            function change_preview() {
                var base = $(this).parent().parent().parent();
                var color   = base.find('select.font_color_id').val();
                var bgcolor = base.find('select.back_color_id').val();
                var preview = base.find('td.preview');
                preview.css({color: color, backgroundColor: bgcolor});
            }
            function change_bordercolor(colorcode) {

                var wk_style = '3px solid '+colorcode;
                if (colorcode == 'noset') {
                    wk_style = '3px solid #5279a5';
                }
                var element1 = document.getElementById("line1");
                element1.style.borderRight= wk_style;
                element1.style.borderBottom = wk_style;
                var element2 = document.getElementById("line2");
                element2.style.borderLeft= wk_style;
                element2.style.borderBottom = wk_style;
                var element3 = document.getElementById("line3");
                element3.style.borderRight= wk_style;
                element3.style.borderTop = wk_style;
                var element4 = document.getElementById("line4");
                element4.style.borderLeft= wk_style;
                element4.style.borderTop = wk_style;

            }
        </script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <style type="text/css">
            .listHeader {
                font-size: 14px;
                font-weight: bold;
                margin: 10px 10px 5px 10px;
            }
            .list {
                border: 1px solid black;
                margin: 5px 10px 20px 10px;
                border-collapse: collapse;
            }
            .list td {
                border: 1px solid black;
                padding: 2px 3px;
                font-size: 13px;
            }
            .list thead td {
                font-weight: bold;
                text-align: center;
            }
            button.submit {
                margin: 5px 10px;
            }
        </style>
    </head>
    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <? show_manage_title($session, $section_admin_auth); ?>
                                </table>

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <?
                                    $arr_option = "";
                                    show_manage_tab_menuitem($session, $fname, $arr_option);
                                    ?>
                                </table>

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                                    </tr>
                                </table>
                                <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                            </td>
                        </tr>
                    </table>

                    <form action="duty_shift_entity_color_update.php" method="post">
                        <input type="hidden" name="session" value="<? eh($session); ?>">
                        <div class="listHeader">■勤務希望の色設定</div>
                        <table class="list">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>文字色</td>
                                    <td>背景色</td>
                                    <td>プレビュー</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>勤務希望と一致</td>
                                    <td align="center">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <select class="font_color_id" name="match_color">
                                                <option value="noset">(色変更しない)</option>
                                                <?php
                                                foreach ($font_color_array as $c) {
                                                    printf('<option %s value="%s" style="background-color:%s;color:%s;">%s</option>',
                                                        ($match_color === $c['color'] ? 'selected' : ''),
                                                        $c['color'], $c['color'], $c['fcolor'], $c['name']);
                                                }
                                                ?>
                                            </select>
                                        </font>
                                    </td>
                                    <td align="center">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <select class="back_color_id" name="match_bgcolor">
                                                <option value="noset">(色変更しない)</option>
                                                <?php
                                                foreach ($back_color_array as $c) {
                                                    printf('<option %s value="%s" style="background-color:%s;color:%s;">%s</option>',
                                                        ($match_bgcolor === $c['color'] ? 'selected' : ''),
                                                        $c['color'], $c['color'], $c['fcolor'], $c['name']);
                                                }
                                                ?>
                                            </select>
                                        </font>
                                    </td>
                                    <td class="preview" style="color:<? eh($match_color);?>; background-color:<? eh($match_bgcolor);?>;">プレビュー</td>
                                </tr>
                                <tr>
                                    <td>勤務希望と不一致</td>
                                    <td align="center">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <select class="font_color_id" name="unmatch_color">
                                                <option value="noset">(色変更しない)</option>
                                                <?php
                                                foreach ($font_color_array as $c) {
                                                    printf('<option %s value="%s" style="background-color:%s;color:%s;">%s</option>',
                                                        ($unmatch_color === $c['color'] ? 'selected' : ''),
                                                        $c['color'], $c['color'], $c['fcolor'], $c['name']);
                                                }
                                                ?>
                                            </select>
                                        </font>
                                    </td>
                                    <td align="center">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <select class="back_color_id" name="unmatch_bgcolor">
                                                <option value="noset">(色変更しない)</option>
                                                <?php
                                                foreach ($back_color_array as $c) {
                                                    printf('<option %s value="%s" style="background-color:%s;color:%s;">%s</option>',
                                                        ($unmatch_bgcolor === $c['color'] ? 'selected' : ''),
                                                        $c['color'], $c['color'], $c['fcolor'], $c['name']);
                                                }
                                                ?>
                                            </select>
                                        </font>
                                    </td>
                                    <td class="preview" style="color:<? eh($unmatch_color);?>; background-color:<? eh($unmatch_bgcolor);?>;">プレビュー</td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="listHeader">■カーソルガイドラインの色設定</div>
                        <table class="list">
                            <thead>
                                <tr>
                                    <td>罫線色</td>
                                    <td colspan="2" width="">プレビュー</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr height="">
                                    <td rowspan="2" align="center">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <select class="back_color_id" name="highlight_bordercolor" onChange="change_bordercolor(this.value);">
                                                <option value="noset">(色変更しない)</option>
                                                <?php
                                                foreach ($high_color_array as $c) {
                                                    printf('<option %s value="%s" style="background-color:%s;color:%s;">%s</option>',
                                                        ($highlight_bordercolor === $c['color'] ? 'selected' : ''),
                                                        $c['color'], $c['color'], $c['fcolor'], $c['name']);
                                                }
                                                ?>
                                            </select>
                                        </font>
                                    </td>
                                    <td id="line1" class="preview_line" style="border-right: <?php eh($wk_bdstyle);?>;border-bottom: <?php eh($wk_bdstyle);?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td id="line2" class="preview_line" style="border-left: <?php eh($wk_bdstyle);?>;border-bottom: <?php eh($wk_bdstyle);?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td id="line3" class="preview_line" style="border-right: <?php eh($wk_bdstyle);?>;border-top: <?php eh($wk_bdstyle);?>;">&nbsp;</td>
                                    <td id="line4" class="preview_line" style="border-left: <?php eh($wk_bdstyle);?>;border-top: <?php eh($wk_bdstyle);?>;">&nbsp;</td>

                                </tr>
                            </tbody>
                        </table>
                        <!-- 新規テーブルフォーム追加 START-->
                        <div class="listHeader">■勤務実績入力画面ボタン表示</div>
                        <table class="check">
                            <tbody>
                                <tr><td>　</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <label><input type="checkbox" name="tosYotei_button_flg" value="t" <?echo($tosYotei_button_flg=='t')?"checked":"";?>> 当初予定ボタンを表示する</label>
                                </font></td></tr>
                                <tr><td>　</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <label><input type="checkbox" name="year_paid_hol_btn_flg" value="t" <?echo($year_paid_hol_btn_flg=='t')?"checked":"";?>> 年次有給休暇簿表示ボタンを表示する</label>
                                </font></td></tr>
                                <tr><td>　</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <label><input type="checkbox" name="timecard_error_btn_flg" value="t" <?echo($timecard_error_btn_flg=='t')?"checked":"";?>> 打刻チェックボタンを表示する</label>
                                </font></td></tr>
                            </tbody>
                        </table>
                        <!-- 新規テーブルフォーム追加 END-->
                        <button class="submit" type="submit">更新する</button>
                    </form>
                </td>
            </tr>
        </table>
    </body>
</html>