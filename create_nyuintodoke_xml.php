<?php
$doc = domxml_new_doc("1.0");
$root = $doc->create_element("template");
$root = $doc->append_child($root);
$head = $doc->create_element("医療保護入院者の入院届け");
$head = $root->append_child($head);

//入院年月日
$category1 = $doc->create_element("入院年月日");
$category1 = $head->append_child($category1);
$sub1 = $doc->create_element("保護者の同意により入院した年月日");
$sub1 = $category1->append_child($sub1);
$date1 = $doc->create_text_node($year1."/".$month1."/".$day1);
$date1 = $sub1->append_child($date1);
$sub1->set_attribute("main", "year1");
$sub2 = $doc->create_element("今回の入院年月日");
$sub2 = $category1->append_child($sub2);
$date2 = $doc->create_text_node($year2."/".$month2."/".$day2);
$date2 = $sub2->append_child($date2);
$sub2->set_attribute("main", "year2");
$sub3 = $doc->create_element("入院形態");
$sub3 = $category1->append_child($sub3);
$sub3->append_child($doc->create_text_node(mb_convert_encoding($Text1, "UTF-8", "EUC-JP")));
$sub3->set_attribute("main", "Text1");

//第34条による移送の有無
$i = 1;
$category2[$i] = $doc->create_element("第34条による移送の有無");
$category2[$i] = $head->append_child($category2[$i]);
if (is_array($Isou)) {
	while(list($key, $val) = each($Isou)) {
		$val = mb_convert_encoding($val, "UTF-8", "EUC-JP");
		$category2[$i]->append_child($doc->create_text_node($val));
		if($val == "あり"){
			$category2[$i]->set_attribute("id", "0");
		} else {
			$category2[$i]->set_attribute("id", "1");
		}
		$category2[$i]->set_attribute("main", "Isou");
	}
}

//病名
$category3 = $doc->create_element("病名");
$category3 = $head->append_child($category3);
$i = 1;
$sub4[$i] = $doc->create_element("主たる精神障害");
$sub4[$i] = $category3->append_child($sub4[$i]);
if (is_array($CheckShogai)) {
	while(list($key, $val) = each($CheckShogai)) {
		if($i > 1) {
			$sub4[$i] = $doc->create_element("主たる精神障害");
			$sub4[$i] = $category3->append_child($sub4[$i]);
		}

		if($key == 11){
			$sub4[$i]->set_attribute("other", mb_convert_encoding($Text2, "UTF-8", "EUC-JP"));
			$sub4[$i]->set_attribute("sub", "Text2");
		}

		$sub4[$i]->set_attribute("id", $key);
		$sub4[$i]->append_child($doc->create_text_node(mb_convert_encoding($val, "UTF-8", "EUC-JP")));
		$sub4[$i]->set_attribute("main", "CheckShogai[".$key."]");
		$i = $i + 1;
	}
}

$sub5 = $doc->create_element("従たる精神障害");
$sub5 = $category3->append_child($sub5);
$sub5->append_child($doc->create_text_node(mb_convert_encoding($Text3, "UTF-8", "EUC-JP")));
$sub5->set_attribute("main", "Text3");
$sub6 = $doc->create_element("身体合併症");
$sub6 = $category3->append_child($sub6);
$sub6->append_child($doc->create_text_node(mb_convert_encoding($Text4, "UTF-8", "EUC-JP")));
$sub6->set_attribute("main", "Text4");

//生活暦及び現病歴
$category4 = $doc->create_element("生活暦及び現病歴");
$category4 = $head->append_child($category4);
$category4->append_child($doc->create_text_node(mb_convert_encoding($Textarea1, "UTF-8", "EUC-JP")));
$category4->set_attribute("main", "Textarea1");

//入院期間
$category5 = $doc->create_element("入院期間");
$category5 = $head->append_child($category5);
$sub7 = $doc->create_element("初回入院期間");
$sub7 = $category5->append_child($sub7);
$kaisi1 = $doc->create_element("開始");
$kaisi1 = $sub7->append_child($kaisi1);
$date3 = $doc->create_text_node($year3."/".$month3."/".$day3);
$date3 = $kaisi1->append_child($date3);
$kaisi1->set_attribute("main", "year3");
$syuryou1 = $doc->create_element("終了");
$syuryou1 = $sub7->append_child($syuryou1);
$date4 = $doc->create_text_node($year4."/".$month4."/".$day4);
$date4 = $syuryou1->append_child($date4);
$syuryou1->set_attribute("main", "year4");
$keitai2 = $doc->create_element("入院形態");
$keitai2 = $sub7->append_child($keitai2);
$keitai2->append_child($doc->create_text_node(mb_convert_encoding($Text5, "UTF-8", "EUC-JP")));
$keitai2->set_attribute("main", "Text5");
$sub8 = $doc->create_element("前回入院期間");
$sub8 = $category5->append_child($sub8);
$kaisi2 = $doc->create_element("開始");
$kaisi2 = $sub8->append_child($kaisi2);
$date5 = $doc->create_text_node($year5."/".$month5."/".$day5);
$date5 = $kaisi2->append_child($date5);
$kaisi2->set_attribute("main", "year5");
$syuryou2 = $doc->create_element("終了");
$syuryou2 = $sub8->append_child($syuryou2);
$date6 = $doc->create_text_node($year6."/".$month6."/".$day6);
$date6 = $syuryou2->append_child($date6);
$syuryou2->set_attribute("main", "year6");
$keitai3 = $doc->create_element("入院形態");
$keitai3 = $sub8->append_child($keitai3);
$keitai3->append_child($doc->create_text_node(mb_convert_encoding($Text6, "UTF-8", "EUC-JP")));
$keitai3->set_attribute("main", "Text6");
$sub9 = $doc->create_element("初回から前回までの入院回数");
$sub9 = $category5->append_child($sub9);
$kaisu = $doc->create_text_node(mb_convert_encoding($Text7, "UTF-8", "EUC-JP"));
$kaisu = $sub9->append_child($kaisu);
$sub9->set_attribute("main", "Text7");

//現在の症状又は状態像
$i = 1;
$category6[$i] = $doc->create_element("現在の症状又は状態像");
$category6[$i] = $head->append_child($category6[$i]);
if (is_array($CheckShojo)) {
	while(list($key, $val) = each($CheckShojo)) {
		if($i > 1) {
			$category6[$i] = $doc->create_element("現在の症状又は状態像");
			$category6[$i] = $head->append_child($category6[$i]);
		}

		$category6[$i]->set_attribute("id", $key);
		switch($key) {
			case 8:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text8, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text8");
				break;
			case 16:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text9, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text9");
				break;
			case 22:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text10, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text10");
				break;
			case 26:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text11, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text11");
				break;
			case 30:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text12, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text12");
				break;
			case 40:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text13, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text13");
				break;
			case 45:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text14, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text14");
				break;
			case 49:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text15, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text15");
				break;
			case 53:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text16, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text16");
				break;
			case 57:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text17, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text17");
				break;
			case 59:
				$category6[$i]->set_attribute("other", mb_convert_encoding($Text18, "UTF-8", "EUC-JP"));
				$category6[$i]->set_attribute("sub", "Text18");
				break;
		}
		$category6[$i]->append_child($doc->create_text_node(mb_convert_encoding($val, "UTF-8", "EUC-JP")));
		$category6[$i]->set_attribute("main", "CheckShojo[".$key."]");
		$i = $i + 1;
	}
}

//XML文章
$text = $doc->dump_mem(true, "UTF-8");

?>