<?
//この画面の呼び出し項目
//発生場所 110 60
//当事者職種 3050 30 +
//勤務形態 3400 120 +  20100127
//当事者以外の関連職種 3500 150
//インシデント直前の患者の状態 290 120
//関連診療科 130 90
//発生要因 600 10
//再発防止に資する警鐘的事例 510 20
//ヒヤリ・ハット分類 125 85
//関係者・物・システム(事例詳細内容) 4000 4
//ユーザー定義項目 >=10000 10


//==============================
//本処理
//==============================

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ( $hiyari == "0" ) {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//医療事故収集連携の判定
//==============================
//発生場所 110 60
//当事者職種 3050 30 +
//当事者以外の関連職種 3500 150
//インシデント直前の患者の状態 290 120
//関連診療科 130 90
//発生要因 600 10
//再発防止に資する警鐘的事例 510 20

$is_acci_use = (
        ($grp_code ==  110 && $easy_item_code ==  60)
    || ($grp_code == 3050 && $easy_item_code ==  30)
    || ($grp_code == 3400 && $easy_item_code == 120)
    || ($grp_code == 3500 && $easy_item_code == 150)
    || ($grp_code ==  200 && $easy_item_code == 10)
    || ($grp_code ==  290 && $easy_item_code == 120) 
    || ($grp_code ==  130 && $easy_item_code ==  90)
    || ($grp_code ==  600 && $easy_item_code ==  10)
    || ($grp_code ==  510 && $easy_item_code ==  20)
    || ($grp_code ==  230 && $easy_item_code ==  60)
    || ($grp_code ==  590 && $easy_item_code ==  90)
);

//==============================
//ヒヤリハット連携の判定
//==============================
//発生場所 110 60
//当事者職種 3050 30 +
//インシデント直前の患者の状態 290 120
//関連診療科 130 90
//ヒヤリ・ハット分類 125 85

$is_inci_use = (
      ($grp_code ==  110 && $easy_item_code ==  60)
   || ($grp_code == 3050 && $easy_item_code ==  30)
   || ($grp_code ==  200 && $easy_item_code == 10)
   || ($grp_code ==  290 && $easy_item_code == 120)
   || ($grp_code ==  130 && $easy_item_code ==  90)
   || ($grp_code ==  125 && $easy_item_code ==  85)
   || ($grp_code ==  230 && $easy_item_code ==  60)
   || ($grp_code == 3000 && $easy_item_code ==  10)
);

//==============================
//項目名の取得
//==============================
$easy_item_name = get_easy_item_name($con,$fname,$grp_code,$easy_item_code);

//==============================
//医療事故収集コードの取得
//==============================
if($is_acci_use)
{
    $acci_item_list = get_acci_item_list($con,$fname,$grp_code,$easy_item_code);
    $def_item_list = get_def_item_list($con,$fname,$grp_code,$easy_item_code);
}
else
{
    $acci_item_list = "";
    $def_item_list = "";
}

//==============================
//ヒヤリハットコードの取得
//==============================
if($is_inci_use)
{
    $inci_item_list = get_inci_item_list($con,$fname,$grp_code,$easy_item_code);
}
else
{
    $inci_item_list = "";
}

//==============================
//利用済み選択項目コードの取得
//==============================
$used_easy_code_list = get_used_easy_code_list($con,$fname,$grp_code,$easy_item_code);

//==============================
//選択項目情報の取得
//==============================
if($is_postback != "true")
{
    $edit_item_list = get_edit_item_list($con,$fname,$grp_code,$easy_item_code);
    $edit_item_list_cnt = count($edit_item_list);
    $line_count = $edit_item_list_cnt > 0 ? $edit_item_list_cnt : 1;

    if ($grp_code >= 10000){
        if ($other_code_cnt == null){
            $other_code_cnt=0;
        }
    } else {
        $other_code_cnt = get_other_code($grp_code)==null ? 0 : 1;
        if ($grp_code==600){
            $other_code_cnt=3;
        }
    }

    $other_flgs = array();
    if ($other_code_cnt>0){
        for ($i=0; $i<$other_code_cnt; $i++){
            $other_flgs[$i] = -1;
        }
        for($line_no=0;$line_no<$edit_item_list_cnt;$line_no++){
            if ($edit_item_list[$line_no]["other_code"]>0){
                if ($grp_code==600){
                    switch ($edit_item_list[$line_no]["other_code"]){
                        case 30:
                            $other_flgs[0] = $edit_item_list[$line_no]['easy_num'];
                            break;
                        case 40:
                            $other_flgs[1] = $edit_item_list[$line_no]['easy_num'];
                            break;
                        case 20:
                            $other_flgs[2] = $edit_item_list[$line_no]['easy_num'];
                            break;
                    }
                }
                else{
                    $other_flgs[0] = $edit_item_list[$line_no]['easy_num'];
                }
            }
        }
    }
    if ($edit_item_list_cnt==0){
        $edit_item_list[0]["disp_flg"] = 't';
    }
}

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
    //==============================
    //ポストバックデータより現在の表示内容を作成
    //==============================
    $edit_item_list = "";
    $tmp_other_flgs = array();
    for($line_no=0;$line_no<$line_count;$line_no++)
    {
        $p_easy_code = "easy_code_".$line_no;
        $p_easy_name = "easy_name_".$line_no;
        $p_pause_flg = "pause_flg_".$line_no;
        $p_disp_flg  = "disp_flg_".$line_no;
        $p_easy_num  = "easy_num_".$line_no;
        $p_def_code  = "def_code_".$line_no;
        $p_acci_code = "acci_code_".$line_no;
        $p_inci_code = "inci_code_".$line_no;
        $p_default_item_flg = "default_item_flg_".$line_no;
        $p_item_group_code  = "item_group_code_".$line_no;

        $tmp_array = array();

        $tmp_array["easy_code"]        = $$p_easy_code;
        $tmp_array["easy_name"]        = $$p_easy_name;
        $tmp_array["pause_flg"]        = $$p_pause_flg == 'true' ? 't' : 'f';
        $tmp_array["disp_flg"]         = $$p_disp_flg != 'true' ? 't' : 'f';
        $tmp_array["easy_num"]         = $line_no+1;
        $tmp_array["def_code"]         = $$p_def_code;//使用しない場合、空文字となる。
        $tmp_array["acci_code"]        = $$p_acci_code;//使用しない場合、空文字となる。
        $tmp_array["inci_code"]        = $$p_inci_code;//使用しない場合、空文字となる。
        $tmp_array["default_item_flg"] = $$p_default_item_flg == 'true' ? 't' : 'f';
        $tmp_array["item_group_code"]  = $$p_item_group_code;

        $edit_item_list[$line_no] = $tmp_array;

        for ($i=0; $i<$other_code_cnt; $i++){
            $other_flg = "other_flg".$i;
            if ($$other_flg!==null && $$other_flg==$$p_easy_num){
                $tmp_other_flgs[$i] = $tmp_array["easy_num"];
            }
        }
    }
    for ($i=0; $i<$other_code_cnt; $i++){
        $other_flgs[$i]=$tmp_other_flgs[$i];
    }

    //==============================
    //行追加
    //==============================
    if($postback_mode == "add_line")
    {
        $tmp_array = array();

        $tmp_array["easy_code"]        = '';
        $tmp_array["easy_name"]        = '';
        $tmp_array["pause_flg"]        = 'f';
        $tmp_array["disp_flg"]         = 't';
        $tmp_array["easy_num"]         = $line_count+1;
        $tmp_array["def_code"]         = '';
        $tmp_array["acci_code"]        = '';
        $tmp_array["inci_code"]        = '';
        $tmp_array["default_item_flg"] = 'f';
        $tmp_array["item_group_code"]  = '';

        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            //既存行に割り込み追加の場合
            if($add_line_no == $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $tmp_array;
                $new_line_no++;
            }

            $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
            $new_line_no++;
        }

        //最終行に追加の場合
        if($add_line_no == $line_count)
        {
            $tmp_edit_item_list[$add_line_no] = $tmp_array;
            $new_line_no++;
        }

        $edit_item_list = $tmp_edit_item_list;
        $line_count++;

    }
    //==============================
    //行削除
    //==============================
    elseif($postback_mode == "delete_line")
    {
        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            if($delete_line_no != $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                $new_line_no++;
            }
        }
        $edit_item_list = $tmp_edit_item_list;
        $line_count--;
    }
    //==============================
    //順番変更
    //==============================
    elseif($postback_mode == "change_line_no")
    {
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            //移動するデータを先に割り当てる。
            $tmp_edit_item_list[$change_line_no_after_line_no] = $edit_item_list[$change_line_no_target_line_no];

            //移動するデータ以外を割り当てる
            if($line_no != $change_line_no_target_line_no)
            {
                //先頭方向に移動する場合
                if($change_line_no_target_line_no > $change_line_no_after_line_no)
                {
                    //移動元-移動先間は末尾方向へ１移動。それ以外は移動なし。
                    $new_line_no = $line_no;
                    if($change_line_no_target_line_no > $line_no && $change_line_no_after_line_no <= $line_no)
                    {
                        $new_line_no++;
                    }
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }
                //末尾方向に移動する場合
                elseif($change_line_no_target_line_no < $change_line_no_after_line_no)
                {
                    //移動元-移動先間は先頭方向へ１移動。それ以外は移動なし。
                    $new_line_no = $line_no;
                    if($change_line_no_target_line_no < $line_no && $change_line_no_after_line_no >= $line_no)
                    {
                        $new_line_no--;
                    }
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }
                //移動しない場合(ありえない)
                else
                {
                    //移動しない。
                    $new_line_no = $line_no;
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }

            }
        }
        $edit_item_list = $tmp_edit_item_list;
    }
    //==============================
    //ＤＢ更新
    //==============================
    elseif($postback_mode == "update")
    {
        //==============================
        //入力チェック
        //==============================
        //いくつかは全てJavaScriptにて制限しているため、実際にここにかかるケースは通常はないものもある。

        $all_no_disp_flg = true;
        $tmp_used_easy_code_list_count = 0;
        $used_pause_name_array = array();
        $used_easy_name_array = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            $check_item = $edit_item_list[$line_no];

            //使用済み項目数のカウント
            if(in_array($check_item["easy_code"],$used_easy_code_list))
            {
                $tmp_used_easy_code_list_count++;
            }

            //表示内容は空欄ではならない。
            if($check_item["easy_name"] == "")
            {
                $error_message = "表示内容が入力されていません。";
                break;
            }

            //表示内容が重複してはならない。
            if ($check_item["pause_flg"]=="t"){
                if(in_array($check_item["easy_name"],$used_pause_name_array))
                {
                    $error_message = "区切りの表示内容｢{$check_item["easy_name"]}｣が重複しています。\\n区切りの表示内容を重複して設定することはできません。";
                    break;
                }
                $used_pause_name_array[] = $check_item["easy_name"];
            }
            else{
                if(in_array($check_item["easy_name"],$used_easy_name_array))
                {
                    $error_message = "表示内容｢{$check_item["easy_name"]}｣が重複しています。\\n表示内容を重複して設定することはできません。";
                    break;
                }
                $used_easy_name_array[] = $check_item["easy_name"];                
            }

            //表示項目が１つでもあればOK
            if($check_item["disp_flg"]=='t')
            {
                $all_no_disp_flg = false;
            }

        }

        //表示項目が１つもないのはNG
        if($error_message == "")
        {
            if($all_no_disp_flg)
            {
                $error_message = "全てを非表示に設定することはできません。";
            }
        }

        //使用済みの選択項目は削除してはならない。
        if($error_message == "")
        {
            if($used_easy_code_list != "" && $tmp_used_easy_code_list_count != count($used_easy_code_list))
            {
                $error_message = "既に使用されている項目は削除できません。";
            }
        }

        //==============================
        //入力チェック結果による処理分岐
        //==============================

        //エラーがある場合の処理
        if($error_message != "")
        {
            //ＤＢ更新を行わない。
        }

        //エラーが無い場合の処理
        else
        {
            //==============================
            //トランザクション開始
            //==============================
            pg_query($con,"begin transaction");

            //============================================================
            //項目定義の更新
            //============================================================
            
            //==============================
            //マルチグループ対応のループ
            //==============================
            $update_grp_code_list = array($grp_code);
            if($grp_code == 3050) {
                $update_grp_code_list = array(3050,3051,3052,3053,3054,3055,3056,3057,3058,3059);
            }
            if($grp_code == 3400) {
                $update_grp_code_list = array(3400,3401,3402,3403,3404,3405,3406,3407,3408,3409);
            }
            if($grp_code == 4000) {
                $update_grp_code_list = range(4000,4099);
            }
            if($grp_code == 230) {
                for ($code=350; $code<=359; $code++){
                    $update_grp_code_list[] = $code;
                }
            }
            if($grp_code == 240) {
                for ($code=360; $code<=369; $code++){
                    $update_grp_code_list[] = $code;
                }
            }
            foreach($update_grp_code_list as $update_grp_code)
            {
                if ($update_grp_code>=350 and $update_grp_code<=369){
                    $update_easy_item_code = 10;
                }
                else{
                    $update_easy_item_code = $easy_item_code;
                }

                //==============================
                //easy_codeの採番開始値の取得
                //==============================
                $new_easy_code = get_new_easy_code($con,$fname,$update_grp_code,$update_easy_item_code);

                //==============================
                //デリート処理
                //==============================
                $sql  = "delete from inci_report_materials";
                $cond = "where grp_code = $update_grp_code and easy_item_code = $update_easy_item_code";

                $result = delete_from_table($con, $sql, $cond, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }


                //==============================
                //カテゴリコードの取得
                //==============================
                if ($grp_code < 10000){
                    $cate_code = get_cate_code($con,$fname,$grp_code);
                } else {
                    $cate_code = 10;
                }

                //==============================
                //インサート処理
                //==============================

                for($line_no=0;$line_no<$line_count;$line_no++)
                {
                    //==============================
                    //インサートデータの作成
                    //==============================
                    $update_item = $edit_item_list[$line_no];
                    $easy_name        = $update_item["easy_name"];
                    $easy_code        = $update_item["easy_code"];
                    $easy_num         = $update_item["easy_num"];
                    $def_code         = $update_item["def_code"];
                    $acci_code        = $update_item["acci_code"];
                    $inci_code        = $update_item["inci_code"];
                    $default_item_flg = $update_item["default_item_flg"];
                    $item_group_code  = $update_item["item_group_code"];
                    if ($item_group_code == '') $item_group_code = null;
                    $other_code       = null;
                    if ($other_code_cnt>0){
                        for ($i=0; $i<$other_code_cnt; $i++){
                            if ($other_flgs[$i]==$update_item['easy_num']){
                                $other_code = get_other_code($grp_code, $i);
                                break;
                            }
                        }
                    }
                    $pause_flg = $update_item["pause_flg"];

                    $easy_flag      = "1";
                    $def_name       = "";
                    $acci_item_code = "0";//※特殊な項目が追加となった場合は別途取得方法を検討する必要あり。
                    $acci_name      = "";
                    $acci_flag      = "0";
                    $acci_num       = "0";//固定値
                    $inci_item_code = "0";//※特殊な項目が追加となった場合は別途取得方法を検討する必要あり。
                    $inci_name      = "";
                    $inci_flag      = "0";
                    $inci_num       = "0";//固定値


                    //=================================
                    // 詳細項目のタイトル追加 20100426
                    //=================================
                    if($update_grp_code == 290 && $easy_item_code == 120 && $easy_code) {
                        $sql = "UPDATE inci_easyinput_item_mst SET ";
                        $set = array('easy_item_name');
                        $setvalue = array("「{$easy_name}」の詳細");
                        $cond = "WHERE grp_code = 295 AND easy_item_code = {$easy_code}";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }
                    } else if($update_grp_code == 600 && $easy_item_code == 10 && $easy_code) {
                        $sql = "UPDATE inci_easyinput_item_mst SET ";
                        $set = array('easy_item_name');
                        $setvalue = array("「{$easy_name}」の詳細");
                        $cond = "WHERE grp_code = 605 AND easy_item_code = {$easy_code}";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }
                    }
                    //=================================






                    //def_codeが選択されている場合の値設定
                    if($is_acci_use)
                    {
                        foreach($def_item_list as $def_item)
                        {
                            $a_def_code = $def_item["def_code"];
                            $a_def_name = $def_item["def_name"];
                            if($a_def_code == $def_code)
                            {
                                $def_name = $a_def_name;
                                $def_flag = "1";
                            }
                        }
                    }
                    else
                    {
                        //(空文字送信されるが、念のため)
                        $def_code = "";
                    }

                    //acci_codeが選択されている場合の値設定
                    if($is_acci_use)
                    {
                        foreach($acci_item_list as $acci_item)
                        {
                            $a_acci_code = $acci_item["acci_code"];
                            $a_acci_name = $acci_item["acci_name"];
                            if($a_acci_code == $acci_code)
                            {
                                $acci_name = $a_acci_name;
                                $acci_flag = "1";
                            }
                        }
                    }
                    else
                    {
                        //(空文字送信されるが、念のため)
                        $acci_code = "";
                    }

                    //inci_codeが選択されている場合の値設定
                    if($is_inci_use)
                    {
                        foreach($inci_item_list as $inci_item)
                        {
                            $a_inci_code = $inci_item["inci_code"];
                            $a_inci_name = $inci_item["inci_name"];
                            if($a_inci_code == $inci_code)
                            {
                                $inci_name = $a_inci_name;
                                $inci_flag = "1";
                            }
                        }
                    }
                    else
                    {
                        //(空文字送信されるが、念のため)
                        $inci_code = "";
                    }

                    //新規追加のeasy_codeを設定
                    if($easy_code == "")
                    {
                        $easy_code = $new_easy_code;
                        $new_easy_code++;
                        $edit_item_list[$line_no]["easy_code"] = $easy_code;//表示情報保存のためにセット
                    }

                    //SQLエスケープ
                    $easy_name = pg_escape_string($easy_name);
                    $def_name  = pg_escape_string($def_name);
                    $acci_name = pg_escape_string($acci_name);
                    $inci_name = pg_escape_string($inci_name);

                    //==============================
                    //項目定義のインサート
                    //==============================
                    $sql = "";
                    $sql .= " insert into inci_report_materials(";
                    $sql .= " cate_code,";
                    $sql .= " grp_code,";
                    $sql .= " easy_item_code,";
                    $sql .= " easy_name,";
                    $sql .= " easy_code,";
                    $sql .= " easy_flag,";
                    $sql .= " easy_num,";
                    $sql .= " def_name,";
                    $sql .= " def_code,";
                    $sql .= " acci_item_code,";
                    $sql .= " acci_name,";
                    $sql .= " acci_code,";
                    $sql .= " acci_flag,";
                    $sql .= " acci_num,";
                    $sql .= " inci_item_code,";
                    $sql .= " inci_name,";
                    $sql .= " inci_code,";
                    $sql .= " inci_flag,";
                    $sql .= " inci_num,";
                    $sql .= " available,";
                    $sql .= " other_code,";
                    $sql .= " pause_flg,";
                    $sql .= " item_group_code";
                    $sql .= " ) values(";
                    $registry_data = array(
                        $cate_code,
                        $update_grp_code,
                        $update_easy_item_code,
                        $easy_name,
                        $easy_code,
                        $easy_flag,
                        $easy_num,
                        $def_name,
                        $def_code,
                        $acci_item_code,
                        $acci_name,
                        $acci_code,
                        $acci_flag,
                        $acci_num,
                        $inci_item_code,
                        $inci_name,
                        $inci_code,
                        $inci_flag,
                        $inci_num,
                        't',
                        $other_code,
                        $pause_flg,
                        $item_group_code
                    );

                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                }
            }

            //============================================================
            //項目非表示定義の更新
            //============================================================
            
            //==============================
            //項目非表示定義のデリート
            //==============================
                $sql  = "delete from inci_easyinput_item_element_no_disp";
                $cond = "where grp_code = $grp_code and easy_item_code = $easy_item_code and class_id isnull and attribute_id isnull and dept_id is null and room_id is null";

                $result = delete_from_table($con, $sql, $cond, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

            //==============================
            //項目非表示定義のインサート
            //==============================
            for($line_no=0;$line_no<$line_count;$line_no++)
            {
                $update_item = $edit_item_list[$line_no];
                $easy_code        = $update_item["easy_code"];
                $disp_flg         = $update_item["disp_flg"];

                if ($update_item["pause_flg"] == 't'){
                    $disp_flg = 'f';
                }

                if($disp_flg == 'f')
                {
                    $sql = "";
                    $sql .= " insert into inci_easyinput_item_element_no_disp(";
                    $sql .= " grp_code,";
                    $sql .= " easy_item_code,";
                    $sql .= " easy_code";
                    $sql .= " ) values(";
                    $registry_data = array(
                        $grp_code,
                        $easy_item_code,
                        $easy_code
                    );

                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                }
            }

            //==============================
            //表示制限で設定している非表示項目を削除した場合は非表示項目側も削除する。(部署ごとの設定に対して)
            //==============================
            $sql = "";
            $sql .= " delete from inci_easyinput_item_element_no_disp";
            $sql .= " where grp_code = $grp_code and easy_item_code = $easy_item_code";
            $sql .= " and not easy_code in(";
            $sql .= " select easy_code from inci_report_materials where grp_code = $grp_code and easy_item_code = $easy_item_code";
            $sql .= " )";
            $cond = "";

            $result = delete_from_table($con, $sql, $cond, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //==============================
            //当事者職種の場合、発見者職種も更新
            //==============================
            if ($grp_code == 3050 && $easy_item_code ==  30){
                //項目定義のデリート
                $sql = "DELETE FROM inci_report_materials";
                $cond = "WHERE grp_code=3020 AND easy_item_code=10;";
                $result = delete_from_table($con, $sql, $cond, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                
                //項目定義のインサート
                $sql = "INSERT INTO inci_report_materials".
                       "(cate_code, grp_code, easy_item_code, easy_name, easy_code, easy_flag, easy_num, other_code, available)".
                       " SELECT 2, 3020, 10, easy_name, easy_code, easy_flag, easy_num, CASE other_code WHEN 35 THEN 20 ELSE NULL END, true".
                       " FROM inci_report_materials WHERE grp_code=3050 AND easy_item_code=30 AND available;";
                $result = insert_into_table_no_content($con, $sql, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                
                //項目非表示定義のデリート
                $sql  = "DELETE FROM inci_easyinput_item_element_no_disp";
                $cond = "where grp_code = 3020 and easy_item_code = 10";
                $result = delete_from_table($con, $sql, $cond, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                //項目非表示定義のインサート
                $sql = "INSERT INTO inci_easyinput_item_element_no_disp(class_id, attribute_id, dept_id, room_id, grp_code, easy_item_code, easy_code)".
                       " SELECT class_id, attribute_id, dept_id, room_id, 3020, 10, easy_code FROM inci_easyinput_item_element_no_disp".
                       " WHERE grp_code=3050 AND easy_item_code=30;";
                $result = insert_into_table_no_content($con, $sql, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }

            //==============================
            //トランザクション終了
            //==============================
            pg_query($con, "commit");

            //==============================
            //自画面を閉じる。
            //==============================
            echo("<script language='javascript'>");
            echo("window.close();");
            echo("</script>");

        }
    }
}


//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
var other_cnt= <?php echo $other_code_cnt; ?>;
var other_no = new Array();
//起動時の処理
function loadaction()
{
    <?
    //更新時エラーがある場合
    if($error_message != "")
    {
    ?>
    alert("<?=$error_message?>");
    <?
    }
    ?>

    <?
    if($postback_mode == "add_line")
    {
    ?>
    var focus_obj_name = "easy_name_<?=$add_line_no?>";
    var focus_obj = document.getElementsByName(focus_obj_name)[0];
    focus_obj.focus();
    <?
    }
    ?>

    //その他の選択を保存
    for (var i=0; i<other_cnt; i++){
        other_no[i] = $("input:radio[name='other_flg"+i+"']:checked").val();
    }

    <?php if ($grp_code==600){?>
    //発生要因の区切りフラグ
    for (var i=0; i<<?php echo $line_count; ?>; i++){
        pause_flg_change_check(i);
    }
    <?php } ?>
}

<?php if ($other_code_cnt>0){ ?>
//その他の変更
function other_check(obj, other_id){
<? if ($other_code_cnt > 1){ ?>
    var isDuplicate = false;
    for (var i=0; i<other_cnt && !isDuplicate; i++){
        for (var j=i+1; j<other_cnt; j++){
            var v1 = $("input:radio[name='other_flg"+i+"']:checked").val();
            var v2 = $("input:radio[name='other_flg"+j+"']:checked").val();
            if (v1!=null && v1==v2){
                isDuplicate=true;
                break;
            }
        }
    }
    if (isDuplicate){
        alert("1つの項目に設定できるその他は１つのみです。");
        $("input:radio[name='other_flg"+other_id+"']").val([other_no[other_id]]);
        return false;
    }
<? } ?>
<? if ($grp_code >= 10000){ ?>
    var val = $("input:radio[name='other_flg"+other_id+"']:checked").val();
    if (other_no[other_id] == val && obj.checked){
        obj.checked = false;
        other_no[other_id]=null;
    } else {
        other_no[other_id]=val;
    }
<? } else { ?>
    other_no[other_id]=$("input:radio[name='other_flg"+other_id+"']:checked").val();
<? } ?>
}
<?php } ?>

<?php if ($grp_code==600){?>
//発生要因の区切りフラグ変更処理
function pause_flg_change_check(no){
    var pause_obj = $("input[name='pause_flg_"+no+"']");

    //その他チェック
    if (pause_obj.attr("checked")){
        for (var i=0; i<other_cnt;i++){
            if ($("#other_flg"+i+"_"+no).attr("checked")){
                alert("その他に設定されているため、区切り項目にできません。");
                pause_obj.attr("checked","");
                return;
            };
        }
    }
    
    //その他・非表示・標準コードの有効・無効設定
    var disable_text = "";
    if (pause_obj.attr("checked")){
        disable_text = "disabled";
    }
    
    for (var i=0; i<other_cnt;i++){
        $("#other_flg"+i+"_"+no).attr("disabled", disable_text);
    }
    $("input[name='disp_flg_"+no+"']").attr("disabled", disable_text);
    var def_code_obj = $("select[name='def_code_"+no+"']");
    if (def_code_obj){
        def_code_obj.attr("disabled", disable_text);
    }
    var acci_code_obj = $("select[name='acci_code_"+no+"']");
    if (acci_code_obj){
        acci_code_obj.attr("disabled", disable_text);
    }
}
<?php }?>

//ＤＢ更新処理
function update_data()
{
    if(update_check())
    {
        document.form1.postback_mode.value = "update";
        document.form1.submit();
    }
}

//順番変更処理
function list_change_line_no(change_line_no_target_line_no,change_line_no_after_line_no)
{
    document.form1.postback_mode.value = "change_line_no";
    document.form1.change_line_no_target_line_no.value = change_line_no_target_line_no;
    document.form1.change_line_no_after_line_no.value = change_line_no_after_line_no;
    document.form1.submit();
}

//行削除処理
function list_delete_line(line_no)
{
    document.form1.postback_mode.value = "delete_line";
    document.form1.delete_line_no.value = line_no;
    document.form1.submit();
}

//行追加処理
function list_add_line(line_no)
{
    document.form1.postback_mode.value = "add_line";
    document.form1.add_line_no.value = line_no;
    document.form1.submit();
}

//削除-使用済みチェック/全非表示チェック
function delete_check(line_no,used_item_flg)
{
    if(used_item_flg)
    {
        alert("既に使用されている項目は削除できません。");
        return false;
    }

    //行削除後の全項目非表示チェック
    var all_no_disp_flg = true;
    var objs = document.getElementsByTagName("input");
    for (var i = 0, j = objs.length; i < j; i++)
    {
        var obj = objs[i];

        if(obj.name == "disp_flg_" + line_no)
        {
            continue;
        }

        if(obj.name.indexOf("disp_flg_") != -1)
        {
            if(!obj.checked)
            {
                all_no_disp_flg = false;
                break;
            }
        }
    }
    if(all_no_disp_flg)
    {
        alert("全ての項目を非表示にすることはできません。");
        return false;
    }

    return true;
}
//非表示-全非表示チェック
function disp_flg_change_check(disp_flg_obj)
{
    var all_no_disp_flg = true;
    var objs = document.getElementsByTagName("input");
    for (var i = 0, j = objs.length; i < j; i++)
    {
        var obj = objs[i];
        if(obj.name.indexOf("disp_flg_") != -1)
        {
            if(!obj.checked)
            {
                all_no_disp_flg = false;
                break;
            }
        }
    }

    if(all_no_disp_flg)
    {
        disp_flg_obj.checked = false;
        alert("全ての項目を非表示にすることはできません。");
        return false;
    }
    return true;
}
function update_check()
{
<?
for($line_no=0;$line_no<$line_count;$line_no++)
{
?>
    if(document.form1.easy_name_<?=$line_no?>.value == "")
    {
        alert("表示内容が未入力です。");
        return false;
    }
    if (document.form1.item_group_code_<?=$line_no?> != null){
        if (document.form1.item_group_code_<?=$line_no?>.value != ""){
            if (!check_number("グループ", document.form1.item_group_code_<?=$line_no?>.value)){
                return false;
            }
        }
    }
<?
}
?>
    return true;
}

function check_number(name, value){
    //入力値に 0〜9 以外があれば
    if(value.match(/[^0-9]+/)){
        alert(name+"は半角数字のみを入力してください。");
        return false;
    }
    return true;
}


</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadaction()">
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">

<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="grp_code" value="<?=$grp_code?>">
<input type="hidden" name="easy_item_code" value="<?=$easy_item_code?>">

<input type="hidden" name="line_count" value="<?=$line_count?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="change_line_no_after_line_no" value="">
<input type="hidden" name="change_line_no_target_line_no" value="">
<input type="hidden" name="delete_line_no" value="">
<input type="hidden" name="add_line_no" value="">
<input type="hidden" name="other_code_cnt" value="<?=$other_code_cnt?>">


<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?
    $PAGE_TITLE = "項目管理";
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<? // ヘッダータブ表示
show_item_edit_header_tab($session, $fname, $grp_code, $easy_item_code);
?>

<!-- メイン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>


<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="800" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">


<!-- 上部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" nowrap>
    <? if($grp_code == 600 && $easy_item_code == 10) { ?>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_item_edit.php?session=<?=$session?>&grp_code=<?=$grp_code?>&easy_item_code=<?=$easy_item_code?>"><b><?=h($easy_item_name)?></b></a></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_detail_factor.php?session=<?=$session?>&grp_code=605&easy_item_code=1"><b>発生要因詳細</b></a></font>
    <? } else if($grp_code == 110 && $easy_item_code == 60) { ?>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_item_edit.php?session=<?=$session?>&grp_code=110&easy_item_code=60"><b>発生場所</b></a></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_detail_place.php?session=<?=$session?>&grp_code=115&easy_item_code=1"><b>発生場所詳細</b></a></font>
    <? } else if($grp_code == 290 && $easy_item_code == 120) { ?>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_item_edit.php?session=<?=$session?>&grp_code=290&easy_item_code=120"><b>インシデント直前の患者の状態</b></a></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_patient_status.php?session=<?=$session?>&grp_code=295&easy_item_code=1"><b>インシデント直前の患者の状態詳細</b></a></font>
    <? } else { ?>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b><?=h($easy_item_name)?></b></font>
    <? } ?>
</td>
<td>
    &nbsp
</td>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 上部更新ボタン等 START-->



<!-- 一覧 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
    <td width="300" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示内容</font>
    </td>
    <?php for ($i=0; $i<$other_code_cnt; $i++){ ?>
        <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他<?php if ($other_code_cnt>1){echo $i+1;}?></font>
        </td>
    <?php } ?>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font>
    </td>
    <?php if ($grp_code==600){ ?>
        <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">区切り</font>
        </td>
        <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font>
        </td>
    <?php } ?>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font>
    </td>

<?
if($is_acci_use)
{
?>
    <td align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">標準コード(2010)</font>
    </td>
<?
}
?>

<?
if($is_acci_use)
{
?>
    <td align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">標準コード(医療事故収集)</font>
    </td>
<?
}
?>

<?
if($is_inci_use)
{
?>
    <td align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">標準コード(ヒヤリ・ハット)</font>
    </td>
<?
}
?>

    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行削除</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行追加</font>
    </td>
</tr>

<?
for($line_no=0;$line_no<$line_count;$line_no++)
{
    $edit_item = $edit_item_list[$line_no];

    $easy_code        = $edit_item["easy_code"];
    $easy_name        = $edit_item["easy_name"];
    $pause_flg        = $edit_item["pause_flg"] == 't';
    $disp_flg         = $edit_item["disp_flg"] == 't';
    $easy_num         = $edit_item["easy_num"];
    $def_code         = $edit_item["def_code"];
    $acci_code        = $edit_item["acci_code"];
    $inci_code        = $edit_item["inci_code"];
    $default_item_flg = $edit_item["default_item_flg"] == 't';
    $item_group_code  = $edit_item["item_group_code"];

    $used_item_flg     = in_array($easy_code,$used_easy_code_list);

    //区切り欄のチェック状態のHTML
    $no_pause_checked_html = "";
    if($pause_flg)
    {
        $no_pause_checked_html = "checked";
    }

    //非表示欄のチェック状態のHTML
    $no_disp_checked_html = "";
    if(!$disp_flg)
    {
        $no_disp_checked_html = "checked";
    }

    //初期項目フラグの値のHTML
    $default_item_flg_value_html = "false";
    if($default_item_flg)
    {
        $default_item_flg_value_html = "true";
    }

    $used_item_flg_script = "false";
    if($used_item_flg)
    {
        $used_item_flg_script = "true";
    }

    //その他のチェック状態
    $other_flg_checked_htmls = array();
    for ($i=0; $i<$other_code_cnt; $i++){
        if ($other_flgs[$i]==$easy_num) {
            $other_flg_checked_htmls[$i] = 'checked';
        }
    }
?>
<input type="hidden" name="easy_code_<?=$line_no?>" value="<?=$easy_code?>">
<input type="hidden" name="default_item_flg_<?=$line_no?>" value="<?=$default_item_flg_value_html?>">
<input type="hidden" name="easy_num_<?=$line_no?>" value="<?=$easy_num?>">
<tr height="22">

<td align="left" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="text" name="easy_name_<?=$line_no?>" value="<?=h($easy_name)?>" style="width:290px" maxlength="50">
    </font>
</td>

<?php for ($i=0; $i<$other_code_cnt; $i++){ ?>
    <td align="center" bgcolor="#FFFFFF" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <input type="radio" name="other_flg<?php echo $i;?>" id="other_flg<?php echo $i."_".$line_no?>"
                value="<?php echo $easy_num;?>"
                onclick="other_check(this, <?=$i?>)"
                <?=$other_flg_checked_htmls[$i]?>>
        </font>
    </td>
<?php } ?>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="checkbox" name="disp_flg_<?=$line_no?>" value="true" <?=$no_disp_checked_html?> onclick="disp_flg_change_check(this);" >
    </font>
</td>

<?php if ($grp_code==600){ ?>
    <td align="center" bgcolor="#FFFFFF" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="checkbox" name="pause_flg_<?=$line_no?>" value="true" <?=$no_pause_checked_html?>
               onclick="pause_flg_change_check(<?=$line_no?>);" >
        </font>
    </td>
    <td align="center" bgcolor="#FFFFFF" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="text" name="item_group_code_<?=$line_no?>" value="<?=h($item_group_code)?>" style="text-align:right;width:40px" maxlength="3">
        </font>
    </td>
<?php } ?>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if($line_no-1 > -1)
{
?>
    <img src="img/up.gif" onclick="list_change_line_no('<?=$line_no?>','<?=$line_no-1?>');" style="cursor: pointer;">
<?
}
?>
<?
if($line_no+1 < $line_count)
{
?>
    <img src="img/down.gif" onclick="list_change_line_no('<?=$line_no?>','<?=$line_no+1?>');" style="cursor: pointer;">
<?
}
?>
    </font>
</td>


<?
if($is_acci_use)
{
?>
<td align="left" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if($def_item_list != "" && !$default_item_flg)
{
?>
    <select name="def_code_<?=$line_no?>">]
<!--
        <option value="">-----
-->
<?
foreach($def_item_list as $def_item)
{
    $a_def_code = $def_item["def_code"];
    $a_def_name = $def_item["def_name"];

    //医療事故収集コード選択状態のHTML
    $def_selected_html = "";
    if($a_def_code == $def_code)
    {
        $def_selected_html = "selected";
    }
//  $acci_code

?>
        <option value="<?=$a_def_code?>" <?=$def_selected_html?> ><?=h("$a_def_code:$a_def_name")?>
<?
}
?>
    </select>
<?
}
else
{
foreach($def_item_list as $def_item)
{
    $a_def_code = $def_item["def_code"];
    $a_def_name = $def_item["def_name"];
    if($a_def_code == $def_code)
    {
?>
        <input type="hidden" name="def_code_<?=$line_no?>" value="<?=$a_def_code?>">
        <?=h("$a_def_code:$a_def_name")?>
<?
    }
}
}
?>


    </font>
</td>
<?
}
?>



<?
if($is_acci_use)
{
?>
<td align="left" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if($acci_item_list != "" && !$default_item_flg)
{
?>
    <select name="acci_code_<?=$line_no?>">]
<!--
        <option value="">-----
-->
<?
foreach($acci_item_list as $acci_item)
{
    $a_acci_code = $acci_item["acci_code"];
    $a_acci_name = $acci_item["acci_name"];

    //医療事故収集コード選択状態のHTML
    $acci_selected_html = "";
    if($a_acci_code == $acci_code)
    {
        $acci_selected_html = "selected";
    }
//  $acci_code

?>
        <option value="<?=$a_acci_code?>" <?=$acci_selected_html?> ><?=h("$a_acci_code:$a_acci_name")?>
<?
}
?>
    </select>
<?
}
else
{
foreach($acci_item_list as $acci_item)
{
    $a_acci_code = $acci_item["acci_code"];
    $a_acci_name = $acci_item["acci_name"];
    if($a_acci_code == $acci_code)
    {
?>
        <input type="hidden" name="acci_code_<?=$line_no?>" value="<?=$a_acci_code?>">
        <?=h("$a_acci_code:$a_acci_name")?>
<?
    }
}
}
?>


    </font>
</td>
<?
}
?>





<?
if($is_inci_use)
{
?>
<td align="left" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if($inci_item_list != "" && !$default_item_flg)
{
?>
    <select name="inci_code_<?=$line_no?>">]
<!--
        <option value="">-----
-->
<?
foreach($inci_item_list as $inci_item)
{
    $a_inci_code = $inci_item["inci_code"];
    $a_inci_name = $inci_item["inci_name"];

    //医療事故収集コード選択状態のHTML
    $inci_selected_html = "";
    if($a_inci_code == $inci_code)
    {
        $inci_selected_html = "selected";
    }
//  $inci_code

?>
        <option value="<?=$a_inci_code?>" <?=$inci_selected_html?> ><?=h("$a_inci_code:$a_inci_name")?>
<?
}
?>
    </select>
<?
}
else
{
foreach($inci_item_list as $inci_item)
{
    $a_inci_code = $inci_item["inci_code"];
    $a_inci_name = $inci_item["inci_name"];
    if($a_inci_code == $inci_code)
    {
?>
        <input type="hidden" name="inci_code_<?=$line_no?>" value="<?=$a_inci_code?>">
        <?=h("$a_inci_code:$a_inci_name")?>
<?
    }
}
}
?>
    </font>
</td>
<?
}
?>









<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if(!$default_item_flg)
{
?>
    <input type="button" value="行削除" onclick="if(delete_check(<?=$line_no?>,<?=$used_item_flg_script?>)){list_delete_line('<?=$line_no?>');}" >
<?
}
?>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="button" name="add_line" value="行追加" onclick="list_add_line('<?=$line_no+1?>');" >
    </font>
</td>

</tr>
<?
}
?>


</table>
<!-- 一覧 END -->


<!-- 下部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 下部更新ボタン等 START-->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->



</td>
</tr>
</table>
<!-- メイン END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?php
//==============================
//DBコネクション終了
//==============================
pg_close($con);


//========================================================================================================================
//内部関数
//========================================================================================================================

/**
 * 当画面の一覧表示データを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return array 当画面の一覧表示データの配列。
 */
function get_edit_item_list($con,$fname,$grp_code,$easy_item_code)
{
    $sql="";
    $sql.=" select";

    $sql.=" m.easy_code,";
    $sql.=" m.easy_name,";
    $sql.=" m.easy_num,";
    $sql.=" m.def_code,";
    $sql.=" m.acci_code,";
    $sql.=" m.inci_code,";
    $sql.=" m.other_code,";
    $sql.=" m.pause_flg,";
    $sql.=" m.item_group_code,";
    $sql.=" case when m_default.grp_code isnull then false else true end as default_item_flg,";
    $sql.=" case when e_disp.grp_code isnull then true else false end as disp_flg";

    /* 選択項目情報 */
    $sql.=" from ";
    $sql.=" (";
    $sql.=" select * ";
    $sql.=" from inci_report_materials";
    $sql.=" where grp_code = $grp_code and easy_item_code = $easy_item_code";
    $sql.=" ) m";

    /* デフォルトの選択項目情報 */
    $sql.=" left join";
    $sql.=" (";
    $sql.=" select distinct";
    $sql.=" grp_code,easy_item_code,easy_code";
    $sql.=" from inci_report_materials_mst";
    $sql.=" where easy_flag = '1'";
    $sql.=" ) m_default";
    $sql.=" on  m_default.grp_code = m.grp_code";
    $sql.=" and m_default.easy_item_code = m.easy_item_code";
    $sql.=" and m_default.easy_code = m.easy_code";

    /* 選択項目の表示情報 */
    $sql.=" left join";
    $sql.=" (";
    $sql.=" select * from inci_easyinput_item_element_no_disp where class_id isnull and attribute_id isnull and dept_id is null and room_id is null";
    $sql.=" ) e_disp";
    $sql.=" on  e_disp.grp_code = m.grp_code";
    $sql.=" and e_disp.easy_item_code = m.easy_item_code";
    $sql.=" and e_disp.easy_code = m.easy_code";

    $sql.=" order by easy_num";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $tmp_list = pg_fetch_all($sel);

    $result_array = array();
    $line_no = 0;
    foreach($tmp_list as $tmp_data)
    {
        $result_array[$line_no] = $tmp_data;
        $line_no++;
    }

    return $result_array;
}


/**
 * グループコード、項目コードより、医療事故収集コードを取得します。
 * ※この関数はファントルくんの項目と医療事故収集の項目が１対１の関係になっていることを前提とします。(項目要素は他対他でも可)
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return array 医療事故収集コードと名称の配列。
 */
function get_acci_item_list($con,$fname,$grp_code,$easy_item_code)
{
    $sql="";
    $sql.=" select distinct";
    $sql.=" acci_code,acci_name";
    $sql.=" from inci_report_materials_mst";
    $sql.=" where acci_flag = '1'";
    $sql.=" and grp_code = $grp_code and easy_item_code = $easy_item_code";
    $sql.=" order by acci_code";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_all($sel);
}


/**
 * グループコード、項目コードより、ヒヤリハットコードを取得します。
 * ※この関数はファントルくんの項目とヒヤリハットの項目が１対１の関係になっていることを前提とします。(項目要素は他対他でも可)
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return array ヒヤリハットコードと名称の配列。
 */
function get_inci_item_list($con,$fname,$grp_code,$easy_item_code)
{
    $sql="";
    $sql.=" select distinct";
    $sql.=" inci_code,inci_name";
    $sql.=" from inci_report_materials_mst";
    $sql.=" where inci_flag = '1'";
    $sql.=" and grp_code = $grp_code and easy_item_code = $easy_item_code";
    $sql.=" order by inci_code";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_all($sel);
}


/**
 * グループコード、項目コードより、2010年版標準コードを取得します。
 * ※この関数はファントルくんの項目と2010年版の項目が１対１の関係になっていることを前提とします。(項目要素は他対他でも可)
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return array ヒヤリハットコードと名称の配列。
 */
function get_def_item_list($con,$fname,$grp_code,$easy_item_code)
{
    $sql="";
    $sql.=" select distinct";
    $sql.=" def_code,def_name";
    $sql.=" from inci_report_materials_mst";
    $sql.=" where acci_flag = '1'";
    $sql.=" and grp_code = $grp_code and easy_item_code = $easy_item_code";
    $sql.=" order by def_code";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_all($sel);
}


/**
 * 使用済みの選択項目コードを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return array 使用済み選択コードの配列。
 */
function get_used_easy_code_list($con,$fname,$grp_code,$easy_item_code)
{
    $sql="";
    $sql.=" select easy_code from (";
    $sql.=" select distinct easy_code";

    $sql.=" from";

    $sql.=" (";
    $sql.=" select distinct input_item from inci_easyinput_data";
    $sql.=" where grp_code = $grp_code";
    $sql.=" and   easy_item_code = $easy_item_code";
    $sql.=" ) used_easy_code_tsv";

    $sql.=" left join";

    $sql.=" (";
    $sql.=" select easy_code from inci_report_materials";
    $sql.=" where grp_code = $grp_code and easy_item_code = $easy_item_code";
    $sql.=" and easy_flag = '1'";
    $sql.=" ) all_easy_code";

    $sql.=" on '\\t' || used_easy_code_tsv.input_item || '\\t' like '%\\t' || all_easy_code.easy_code || '\\t%'";

    //当事者職種の場合は、プロフィールと発見者職種で使用しているかもチェックする。
    if($grp_code == 3050 && $easy_item_code == 30 )
    {
    $sql.=" union (select distinct exp_code as easy_code from inci_profile where not exp_code isnull and exp_code <> '')";
        $sql.=" union (select distinct input_item from inci_easyinput_data where grp_code = 3020 and easy_item_code = 10)";
    }

    //ヒヤリ・ハット分類の場合は分類フォルダ、ヒヤリハット分類フォルダで使用しているかもチェックする。
    if($grp_code == 125 && $easy_item_code == 85 )
    {
    $sql.=" union (";

    $sql.=" select distinct easy_code from";
    $sql.=" (";
    $sql.=" select easy_code from inci_report_materials where grp_code = 125 and easy_item_code = 85";
    $sql.=" ) all_easy_code";
    $sql.=" inner join";
    $sql.=" (";
    $sql.=" select hiyari_summary as easy_code_csv from inci_hiyari_folder";
    $sql.=" union";
    $sql.=" select use_hiyari_summary_list as easy_code_csv from inci_classification_folder";
    $sql.=" ) used_easy_code";
    $sql.=" on ',' || used_easy_code.easy_code_csv || ',' like '%,' || all_easy_code.easy_code || ',%'";

    $sql.=" )";
    }
    $sql.=" ) used_easy_code where not easy_code isnull";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $tmp_array = pg_fetch_all($sel);

    $result_list = "";
    foreach($tmp_array as $tmp_data)
    {
        $result_list[] = $tmp_data["easy_code"];
    }


    return $result_list;
}


/**
 * カテゴリコードを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @return カテゴリコード
 */
function get_cate_code($con,$fname,$grp_code)
{
    $sql="";
    $sql.=" select distinct";
    $sql.=" cate_code";
    $sql.=" from inci_report_materials_mst";
    $sql.=" where grp_code = $grp_code";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_result($sel,0,"cate_code");
}



/**
 * 新しい選択項目コードを採番します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return integer 新しい選択項目コード。
 */
function get_new_easy_code($con,$fname,$grp_code,$easy_item_code)
{
    //1000、inci_report_materials.easy_code、inci_report_materials_mst.easy_codeのうち最大の値+1。
    //つまり、初期項目以外は1000番代が割り当てられる。

    $sql="";
    $sql.=" select max(to_number(code1::text,'999999'))+1 as new_easy_code from";
    $sql.=" (";
    $sql.=" select max(to_number(easy_code,'999999')) as code1 from inci_report_materials";
    $sql.=" where grp_code = $grp_code and easy_item_code = $easy_item_code and not easy_code isnull and easy_code != ''";
    $sql.=" union all";
    $sql.=" select max(to_number(easy_code,'999999')) as code1 from inci_report_materials_mst";
    $sql.=" where grp_code = $grp_code and easy_item_code = $easy_item_code and not easy_code isnull and easy_code != ''";
    $sql.=" union all";
    $sql.=" select 1000 as code1";
    $sql.=" ) t";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_result($sel,0,"new_easy_code");
}


/**
 * 項目名を取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return integer 新しい選択項目コード。
 */
function get_easy_item_name($con,$fname,$grp_code,$easy_item_code)
{
    $sql="";
    $sql.=" select easy_item_name from inci_easyinput_item_mst";
    $sql.=" where grp_code = $grp_code and easy_item_code = $easy_item_code";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_result($sel,0,"easy_item_name");
}

/**
 * グループに関連付けられるその他のeasy_item_codeを取得します。
 * 例：発生要因の場合
 * 　grp_code=600、項目のeasy_item_code=10 その他のeasy_item_code=20,30,40
 * @param integer $grp_code グループコード
 * @param integer $other_id その他のコードが複数ある場合の識別ID
 * @return integer その他のeasy_item_code。
 */
function get_other_code($grp_code, $other_id=0){
    switch ($grp_code){
        case 116:   //発生部署
            $other_code=15;	
            break;
        case 110:   //発生場所
            $other_code=70;	
            break;
        case 3000:  //発見者
            $other_code=20;	
            break;
        case 3050:  //当事者職種
            $other_code=35;	
            break;
        case 3400:  //勤務形態
            $other_code=130;	
            break;
        case 3500:  //当事者以外の関連職種
            $other_code=160;	
            break;
        case 200:   //患者の数
            $other_code=20;   
            break;
        case 290:   //直前の患者の状態
            $other_code=130;	
            break;
        case 125:   //ヒヤリハット分類
            $other_code=86;   
            break;
        case 130:   //関連診療科
            $other_code=100;	
            break;
        case 600:   //発生要因
            switch ($other_id){
            case 0:
                $other_code=30;
                break;
            case 1:
                $other_code=40;
                break;
            case 2:
                $other_code=20;
                break;
            }
            break;
        case 590:   //事故調査委員会の設置有無
            $other_code=91;	
            break;
        default:
            $other_code=null;
    }
    if ($grp_code >= 10000){ // ユーザー定義項目
        $other_code=20;      
    }
    return $other_code;
}
?>