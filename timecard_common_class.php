<?
require_once("date_utils.php");
require_once("holiday.php");

class timecard_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $hours = array(); //
	var $timecard_array = array(); //timecardテーブル
	var $duty_end_time_array = array(); //当直終了時刻 key:tmcd_group_id value:1:終了時刻１、2:終了時刻２、""の場合は終了時刻１とする。
	var $empcond_officehours_array = array(); //個人別の所定時間
    var $empcond_officehours_hist = array(); //個人別の所定時間履歴 []["histdate"](昇順),["data"]:基本、曜日別のデータ配列 20130220
    
	/** コンストラクタ
	 * @param $con          DBコネクション
	 * @param $fname        画面名
	 * @param $emp_id       対象emp_id   文字列または、配列
	 * @param $start_date	取得範囲開始日
	 * @param $end_date     取得範囲終了日
	 *
	 */
	function timecard_common_class($con, $fname, $emp_id, $start_date, $end_date)
	{

		global $hours;
		global $timecard_array;
		global $duty_end_time_array;

		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;

		//対象期間に存在するグループ,パターンを取得する
		$sql =	"SELECT ".
					"tmcd_group_id, ".
					"pattern ".
				"FROM ".
					"atdbk ".
				"WHERE ";
		if(empty($emp_id) == false){
			//emp_idが配列で来た場合、INで指定する
			if (is_array($emp_id)){
				$sql .= "emp_id IN ('";
				$sql .= implode("','", $emp_id);
				$sql .= "') AND ";
			}
			else {
				$sql .= "emp_id = '$emp_id' AND ";
			}
		}
		if (empty($start_date) == false && empty($end_date)){
			//終了が指定してない場合、１日指定で取得
			$sql .= "date = '$start_date' AND ";
		}
		else if(empty($start_date) == false && empty($end_date) == false){
			$sql .= "date >= '$start_date' AND ";
			$sql .= "date <= '$end_date' AND ";
		}
		$sql .=	"LENGTH(pattern) > 0 ".
				"UNION  ".
				"SELECT ".
					"tmcd_group_id, ".
					"pattern ".
				"FROM ".
					"atdbkrslt ".
				"WHERE ";
		//emp_idが配列で来た場合、INで指定する
		if (is_array($emp_id)){
			$sql .= "emp_id IN ('";
			$sql .= implode("','", $emp_id);
			$sql .= "') AND ";
		}
		else {
			$sql .= "emp_id = '$emp_id' AND ";
		}
		if($start_date != null && $start_date != ""){
			$sql .= "date >= '$start_date' AND ";
		}
		if($end_date != null && $end_date != ""){
			$sql .= "date <= '$end_date' AND ";
		}
		$sql .=	"LENGTH(pattern) > 0 ".
				"ORDER BY ".
				"tmcd_group_id, pattern";

		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$work_list = array();
		$pattern_array = array();
		$work_group_id = "";

		//groupId毎にパターンを保持
		while ($row = pg_fetch_array($sel)) {

			if ($work_group_id != $row["tmcd_group_id"] && $work_group_id != ""){
				//追加
				$work_list[] = array("group_id" => $work_group_id, "pattern_array" => $pattern_array);

				$pattern_array = array();
			}

			$work_group_id = $row["tmcd_group_id"];
			//SQLで使用する為シングルクォートで囲んで保持
			$pattern = $row["pattern"];
			$pattern_array[] = "'$pattern'";

		}
		if ($work_group_id != null){
			//追加
			$work_list[] = array("group_id" => $work_group_id, "pattern_array" => $pattern_array);
		}

		//対象期間に存在するグループｘパターンのofficehours情報を取得する
		foreach($work_list as $group_pattern){
			$tmcd_group_id = $group_pattern["group_id"];
			$comma_split_pattern = join(', ', $group_pattern["pattern_array"]);
			$officehours_table_name = "officehours";

			$sql =	"SELECT ".
				"tmcd_group_id, ".
				"pattern, ".
				"officehours_id, ".
				"officehours2_start, ".
				"officehours2_end, ".
				"officehours4_start, ".
				"officehours4_end, ".
				"officehours5_start, ".
				"officehours5_end ".
				"FROM ".
				"$officehours_table_name ".
				"WHERE ".
				"tmcd_group_id = $tmcd_group_id and pattern IN ($comma_split_pattern) ";
			$cond = "";
			$sel2 = select_from_table($con, $sql, $cond, $fname);

			if ($sel2 == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			while ($row = pg_fetch_array($sel2)) {
				$officehours = array();
				$tmcd_group_id         = $row["tmcd_group_id"];
				$pattern               = $row["pattern"];
				$officehours_id        = $row["officehours_id"];
				$officehours["start2"] = $row["officehours2_start"];
				$officehours["end2"]   = $row["officehours2_end"];
				$officehours["start4"] = $row["officehours4_start"];
				$officehours["end4"]   = $row["officehours4_end"];
				$officehours["start5"] = $row["officehours5_start"];
				$officehours["end5"]   = $row["officehours5_end"];

				$array_key = $tmcd_group_id."_".$pattern."_".$officehours_id;
				$hours[$array_key] = $officehours;
			}

		}

		// タイムカード設定情報を取得
		$sql = "select * from timecard";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			for ($i = 1; $i <= 5; $i++) {
				$timecard_array["fraction$i"]       = pg_fetch_result($sel, 0, "fraction$i");
				if ($i != 3) {
					$timecard_array["fraction{$i}_min"] = pg_fetch_result($sel, 0, "fraction{$i}_min");
				}
				$timecard_array["others$i"]         = pg_fetch_result($sel, 0, "others$i");
			}

			$timecard_array["rest1"]                    = pg_fetch_result($sel, 0, "rest1");
			$timecard_array["rest2"]                    = pg_fetch_result($sel, 0, "rest2");
			$timecard_array["rest3"]                    = pg_fetch_result($sel, 0, "rest3");

			//各当直情報をまとめて入れる
			$duty_info["night_start_time"]                      = pg_fetch_result($sel, 0, "night_start_time");
			$duty_info["night_start_next_day_flag"]             = pg_fetch_result($sel, 0, "night_start_next_day_flag");
			$duty_info["night_end_time"]                        = pg_fetch_result($sel, 0, "night_end_time");
			$duty_info["night_end_next_day_flag"]               = pg_fetch_result($sel, 0, "night_end_next_day_flag");
			$duty_info["night_workday_count"]                   = pg_fetch_result($sel, 0, "night_workday_count");
			$duty_info["night_end_time2"]                       = pg_fetch_result($sel, 0, "night_end_time2");
			$duty_info["night_end_next_day_flag2"]              = pg_fetch_result($sel, 0, "night_end_next_day_flag2");
			$duty_info["night_workday_count2"]                  = pg_fetch_result($sel, 0, "night_workday_count2");

			$duty_info_sat["night_start_time"]                  = pg_fetch_result($sel, 0, "night_start_time_sat");
			$duty_info_sat["night_start_next_day_flag"]         = pg_fetch_result($sel, 0, "night_start_next_day_flag_sat");
			$duty_info_sat["night_end_time"]                    = pg_fetch_result($sel, 0, "night_end_time_sat");
			$duty_info_sat["night_end_next_day_flag"]           = pg_fetch_result($sel, 0, "night_end_next_day_flag_sat");
			$duty_info_sat["night_workday_count"]               = pg_fetch_result($sel, 0, "night_workday_count_sat");
			$duty_info_sat["night_end_time2"]                   = pg_fetch_result($sel, 0, "night_end_time2_sat");
			$duty_info_sat["night_end_next_day_flag2"]          = pg_fetch_result($sel, 0, "night_end_next_day_flag2_sat");
			$duty_info_sat["night_workday_count2"]              = pg_fetch_result($sel, 0, "night_workday_count2_sat");

			$duty_info_hol["night_start_time"]                  = pg_fetch_result($sel, 0, "night_start_time_hol");
			$duty_info_hol["night_start_next_day_flag"]         = pg_fetch_result($sel, 0, "night_start_next_day_flag_hol");;
			$duty_info_hol["night_end_time"]                    = pg_fetch_result($sel, 0, "night_end_time_hol");
			$duty_info_hol["night_end_next_day_flag"]           = pg_fetch_result($sel, 0, "night_end_next_day_flag_hol");
			$duty_info_hol["night_workday_count"]               = pg_fetch_result($sel, 0, "night_workday_count_hol");
			$duty_info_hol["night_end_time2"]                   = pg_fetch_result($sel, 0, "night_end_time2_hol");
			$duty_info_hol["night_end_next_day_flag2"]          = pg_fetch_result($sel, 0, "night_end_next_day_flag2_hol");
			$duty_info_hol["night_workday_count2"]              = pg_fetch_result($sel, 0, "night_workday_count2_hol");

			$timecard_array["duty_info"]     = $duty_info;
			$timecard_array["duty_info_sat"] = $duty_info_sat;
			$timecard_array["duty_info_hol"] = $duty_info_hol;

			$timecard_array["ovtmscr_tm"]               = pg_fetch_result($sel, 0, "ovtmscr_tm");
			$timecard_array["over_time_apply_type"]     = pg_fetch_result($sel, 0, "over_time_apply_type");
			//残業時刻の申請単位
			$timecard_array["fraction_over_unit"]     = pg_fetch_result($sel, 0, "fraction_over_unit");
			$timecard_array["criteria_months"]     = pg_fetch_result($sel, 0, "criteria_months");
		}

		// カレンダー名情報取得
		$sql = "select day1_time, day2, day3, day4, day5 from calendarname";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$calendarname_info = array();
		if (pg_num_rows($sel) > 0) {
			$calendarname_info["day2"] = pg_fetch_result($sel, 0, "day2");
			$calendarname_info["day3"] = pg_fetch_result($sel, 0, "day3");
			$calendarname_info["day4"] = pg_fetch_result($sel, 0, "day4");
			$calendarname_info["day5"] = pg_fetch_result($sel, 0, "day5");
			$calendarname_info["day1_time"] = pg_fetch_result($sel, 0, "day1_time");
		}
		else{
			$calendarname_info["day1_time"] = "";
		}
		if (empty($calendarname_info["day2"])) {
			$calendarname_info["day2"] = "法定休日";
		}
		if (empty($calendarname_info["day3"])) {
			$calendarname_info["day3"] = "所定休日";
		}
		if (empty($calendarname_info["day4"])) {
			$calendarname_info["day4"] = "勤務日1";
		}
		if (empty($calendarname_info["day5"])) {
			$calendarname_info["day5"] = "勤務日2";
		}
		$calendarname_info["day1"] = "通常勤務日";
		$calendarname_info["day6"] = "祝日";
		$calendarname_info["day7"] = "年末年始休暇";
        $calendarname_info["day8"] = "買取日";
        
		$timecard_array["calendarname_info"] = $calendarname_info;

		//当直終了時刻
		$sql = "select group_id, night_end_time from tmcd_night_end_time ";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$duty_end_time_array[$row["group_id"]] = $row["night_end_time"];
		}
	
	}

	function get_officehours($tmcd_group_id, $pattern, $cond, $fname){

		global $hours;
		$array_key = $tmcd_group_id."_".$pattern;

		//キーが存在しない場合は取得する
		if (array_key_exists($array_key, $hours) == false){
			$officehours_table_name = "officehours"; //.$tmcd_group_id;
            $sql =	"SELECT ".
                "    officehours_id, ".
                "    officehours2_start, ".
                "    officehours2_end, ".
                "    officehours4_start, ".
                "    officehours4_end, ".
                "    officehours5_start, ".
                "    officehours5_end ".
                "FROM ".
                "    $officehours_table_name ".
                "WHERE ".
                "    tmcd_group_id = $tmcd_group_id and pattern = '$pattern'";
					//"ORDER BY ".
					//"    tmcd_group_id, pattern";
			$cond = "";
            $sel = select_from_table($this->_db_con, $sql, $cond, $fname);
			if ($sel == 0) {
                pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			$officehours = array();
			$officehours["start2"] = pg_fetch_result($sel, 0, "officehours2_start");
			$officehours["end2"]   = pg_fetch_result($sel, 0, "officehours2_end");
			$officehours["start4"] = pg_fetch_result($sel, 0, "officehours4_start");
			$officehours["end4"]   = pg_fetch_result($sel, 0, "officehours4_end");
			$officehours["start5"] = pg_fetch_result($sel, 0, "officehours5_start");
			$officehours["end5"]   = pg_fetch_result($sel, 0, "officehours5_end");
			$hours[$array_key] = $officehours;
		}

		return $officehours;
	}

	function get_officehours_info($tmcd_group_id, $pattern, $officehours_id, $column_name){

		global $hours;
		$result_value = "";
		$array_key = $this->create_hours_array_key($tmcd_group_id, $pattern, $officehours_id);

		//キーが存在する場合のみ取得
		if (array_key_exists($array_key, $hours)){
			$result_value = $hours[$array_key][$column_name];
		}

		return $result_value;
	}

	function create_hours_array_key($tmcd_group_id, $pattern, $officehours_id){

		return $tmcd_group_id."_".$pattern."_".$officehours_id;

	}


	/**
	 * 登録対象日のタイムカード情報を取得する（前・翌日対応　※仕変があった為使用は要注意）
	 *
	 * $today_flg "1":当日のデータを返す
	 */
	function get_clock_in_atdbkrslt_array($con, $emp_id, $fname, $today_flg="", $date="") {
		$result_array       = array();
		$previous_day_array = array();
		$today_array        = array();
		$next_day_array     = array();
		$now_time           = (int)(date(H).date(i));
		if ($date != "") {
			$timestamp_today = date_utils::to_timestamp_from_ymd($date);
			$previous_day       = date("Ymd", strtotime("-1 day", $timestamp_today));
			$today              = $date;
			$next_day           = date("Ymd", strtotime("1 day", $timestamp_today));
		} else {
			$previous_day       = date("Ymd", strtotime("-1 day"));
			$today              = date("Ymd");
			$next_day           = date("Ymd", strtotime("1 day"));
		}

		$previous_day_start = "";
		$previous_day_end   = "";

		$today_start           = "";
		$today_end             = "";
		$today_group_id        = "";
		$today_pattern         = "";
		$today_type            = "";
		$today_office_end_time = 0;

		$next_day_start             = "";
		$next_day_end               = "";

		$today_data_exist = false;
		$next_day_data_exist = false;

        //個人別所定時間を優先するための対応 20120309
        $arr_empcond_officehours_ptn = $this->get_empcond_officehours_pattern();
        $arr_weekday_flg = $this->get_arr_weekday_flag($previous_day, $next_day);
        $arr_empcond_officehours = $this->get_arr_empcond_officehours($emp_id,$previous_day); //個人別勤務時間帯履歴対応 20130220

        //前日・当日・翌日の情報取得
		$sql =	"SELECT ".
			"atdbkrslt.*, ".
			"atdptn.previous_day_possible_flag, ".
			"atdptn.meeting_start_time as a_meeting_start_time, ".
			"atdptn.meeting_end_time as a_meeting_end_time, ".
			"atdptn.over_24hour_flag, ".
			"calendar.type ".
				"FROM ".
					"atdbkrslt  ".
						"LEFT JOIN atdptn ON (atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id as varchar)) ".
						"LEFT JOIN calendar ON (atdbkrslt.date = calendar.date) ".
				"WHERE ".
					"atdbkrslt.emp_id = '$emp_id' AND ".
					"atdbkrslt.date IN ('$previous_day', '$today', '$next_day') ";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		for($i = 0; $i < 3; $i++){
			if (($row = pg_fetch_array($sel)) == false) {
				break;
			}

			$work_date = $row["date"];
			//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
			$tmp_type = ($row["type"] != "2" && $row["type"] != "3") ? "1" : $row["type"];
			$row["office_start_time"]   = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "start2" );
			$row["office_end_time"]     = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "end2" );
            //休憩追加 20140626
            $row["office_rest_start_time"]   = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "start4" );
            $row["office_rest_end_time"]     = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "end4" );
            //個人別所定時間を優先する場合 20120309
            $tmcd_group_id = $row["tmcd_group_id"];
            $pattern = $row["pattern"];
            if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
                $wk_weekday = $arr_weekday_flg[$work_date];
                $arr_empcond_officehours = $this->get_empcond_officehours($wk_weekday, $work_date); //個人別勤務時間帯履歴対応 20130220
                if ($arr_empcond_officehours["officehours2_start"] != "") {
                    $row["office_start_time"] = $arr_empcond_officehours["officehours2_start"];
                    $row["office_end_time"] = $arr_empcond_officehours["officehours2_end"];
                    //休憩追加 20140626
                    $row["office_rest_start_time"] = $arr_empcond_officehours["officehours4_start"];
                    $row["office_rest_end_time"] = $arr_empcond_officehours["officehours4_end"];
                }
                
            }
            if($previous_day == $work_date){
				//前日の情報保持
				$previous_day_array     = $row;
				$previous_day_start     = $row["start_time"];
				$previous_day_end       = $row["end_time"];
				$previous_day_previous_day_flag = $row["previous_day_flag"];
			}
			else if($today == $work_date){
				//当日の情報保持
				$today_array    = $row;
				$today_start    = $row["start_time"];
				$today_end      = $row["end_time"];
				$today_type     = $row["type"];
				$today_group_id = $row["tmcd_group_id"];
				$today_pattern  = $row["pattern"];
				$today_data_exist = true;
			}
			else if($next_day == $work_date){
				//翌日の情報保持
				$next_day_array    = $row;
				$next_day_start    = $row["start_time"];
				$next_day_end      = $row["end_time"];
				$next_day_previous_day_possible_flag = $row["previous_day_possible_flag"];
				$next_day_data_exist = true; 
			}
		}

		$arr_atdbk_today = $this->get_atdbk_array($con, $today, $emp_id, $fname);
		//実績に当日のレコードがない場合
		if (!$today_data_exist) {
			$today_group_id = $arr_atdbk_today["tmcd_group_id"];
			$today_pattern = $arr_atdbk_today["pattern"];
			$today_type = $arr_atdbk_today["type"];
		}
		//実績に翌日のレコードがない場合、予定のパターンで「勤務開始時刻が前日になる場合がある」フラグを取得する
		if (!$next_day_data_exist) {
			$arr_atdbk_next = $this->get_atdbk_array($con, $next_day, $emp_id, $fname);
			$next_day_previous_day_possible_flag = $arr_atdbk_next["previous_day_possible_flag"];
		}

		//当日の所定退勤時刻取得
		if (strlen($today_group_id) > 0 && strlen($today_pattern) > 0){
			// 各時間帯の開始時刻・終了時刻を変数に格納
			//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
			$tmp_type = ($today_type != "2" && $today_type != "3") ? "1" : $today_type;

			$work_end_time      = $this->get_officehours_info( $today_group_id, $today_pattern, $tmp_type, "end2" );

			//時刻はHH:mmで保存されているHHmmの数値に変換
			if (strlen($work_end_time) == 5){
				$today_office_end_time = (int) (substr($work_end_time, 0, 2).substr($work_end_time, 3, 4));
			}

		}

		//前日に退勤後復帰後、復帰後退勤していないか判定
		$prev_no_end_flg = false;
		for ($i=0; $i<10; $i++) {
			if ($previous_day_array["o_start_time$i"] != "" && $previous_day_array["o_end_time$i"] == "") {
				$prev_no_end_flg = true;
				break;
			}
		}

        //当日のデータを返す 20140820
        if ($today_flg == "1") {
            $today_array["date"] = $today;
            $result_array = $today_array;
            return $result_array;
        }
        
		//当日の出勤が入っていないかつ、前日退勤後復帰後、復帰後退勤していない場合は前日の情報を返す 20090623
		if(empty($today_start) && $prev_no_end_flg) {
			$previous_day_array["date"] = $previous_day;
			$result_array = $previous_day_array;
		}
		//当日の出勤が入っていないかつ、前日の出勤が入力されているかつ、前日の退勤が入力されていない場合は前日の情報を返す
		else if(empty($today_start) && strlen($previous_day_start) > 0 && strlen($previous_day_end) == 0 && $previous_day_previous_day_flag != 1){
			$previous_day_array["date"] = $previous_day;
			$result_array = $previous_day_array;
		}
		//翌日の出勤パターンに勤務開始時刻が前日になる場合があるにチェックがあるかつ、
		//当日の勤務シフトの退勤時刻を過ぎているかつ、
		//当日の出勤が入力されていない or 当日の退勤が入力されている場合は、翌日の情報を返す
		else if($next_day_previous_day_possible_flag == "1" && $today_office_end_time < $now_time && (empty($today_start) || empty($today_end) == false)){
			$next_day_array["date"] = $next_day;
			$result_array = $next_day_array;
		}
		else{
			$today_array["date"] = $today;
			$result_array = $today_array;
		}


		return $result_array;
	}


	/**
	 * 本日のタイムカード情報を取得する
	 *
	 */
	function get_clock_in_atdbkrslt_array_today($con, $emp_id, $fname) {

		$today_array        = array();
		$today              = date("Ymd");

		$today_group_id        = "";
		$today_pattern         = "";
		$today_type            = "";

		//当日の情報取得
		$sql =	"SELECT ".
					"atdbkrslt.*, ".
					"atdptn.previous_day_possible_flag, ".
					"atdptn.meeting_start_time as a_meeting_start_time, ".
					"atdptn.meeting_end_time as a_meeting_end_time, ".
					"calendar.type ".
				"FROM ".
					"atdbkrslt  ".
						"LEFT JOIN atdptn ON (atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id as varchar)) ".
						"LEFT JOIN calendar ON (atdbkrslt.date = calendar.date) ".
				"WHERE ".
					"atdbkrslt.emp_id = '$emp_id' AND ".
					"atdbkrslt.date = '$today'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		if (($row = pg_fetch_array($sel)) == true) {
			//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
			$tmp_type = ($row["type"] != "2" && $row["type"] != "3") ? "1" : $row["type"];
			$row["office_start_time"]   = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "start2" );
			$row["office_end_time"]     = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "end2" );

			//当日の情報保持
			$today_array    = $row;
			$today_type     = $row["type"];
			$today_group_id = $row["tmcd_group_id"];
			$today_pattern  = $row["pattern"];
		}

		//当日の所定退勤時刻取得
		if (strlen($today_group_id) > 0 && strlen($today_pattern) > 0){
			// 各時間帯の開始時刻・終了時刻を変数に格納
			//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
			$tmp_type = ($row["type"] != "2" && $row["type"] != "3") ? "1" : $row["type"];
			$today_array["office_end_time"] = $this->get_officehours_info( $today_group_id, $today_pattern, $tmp_type, "end2" );
		}
		$today_array["date"] = $today;

		return $today_array;
	}
	/**
	 * 指定日の予定情報を取得する
	 *
	 */
	function get_atdbk_array($con, $duty_date, $emp_id, $fname) {

		$atdbk_array = array();

		$sql =	"SELECT ".
					"atdbk.*,".
					"atdptn.previous_day_possible_flag, ".
					"atdptn.previous_day_flag, ".
					"calendar.type ".
				"FROM ".
					"atdbk ".
						"LEFT JOIN atdptn ON (atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id as varchar)) ".
						"LEFT JOIN calendar ON (atdbk.date = calendar.date) ".
				"WHERE ".
					"atdbk.emp_id = '$emp_id' AND ".
					"atdbk.date = '$duty_date'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		if (($row = pg_fetch_array($sel)) == true) {
			$atdbk_array    = $row;
		}
		return $atdbk_array;
	}

	/**
	 * 指定日の実績情報を取得する
	 *
	 */
	function get_atdbkrslt_array($con, $duty_date, $emp_id, $fname) {

		$atdbk_array = array();

		$sql =	"SELECT ".
					"atdbkrslt.*,".
					"atdptn.previous_day_possible_flag, ".
					"atdptn.meeting_start_time as a_meeting_start_time, ".
					"atdptn.meeting_end_time as a_meeting_end_time, ".
					"atdptn.workday_count, ".
					"calendar.type ".
				"FROM ".
					"atdbkrslt ".
						"LEFT JOIN atdptn ON (atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id as varchar)) ".
						"LEFT JOIN calendar ON (atdbkrslt.date = calendar.date) ".
				"WHERE ".
					"atdbkrslt.emp_id = '$emp_id' AND ".
					"atdbkrslt.date = '$duty_date'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		if (($row = pg_fetch_array($sel)) == true) {
			//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
			$tmp_type = ($row["type"] != "2" && $row["type"] != "3") ? "1" : $row["type"];
			$row["office_start_time"]   = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "start2" );
			$row["office_end_time"]     = $this->get_officehours_info( $row["tmcd_group_id"], $row["pattern"], $tmp_type, "end2" );
            $wk_officehours = $this->get_officehours_emp($emp_id, $duty_date, $row["tmcd_group_id"], $row["pattern"]);
            if ($wk_officehours["office_start_time"] != "") {
                $row["office_start_time"] = $wk_officehours["office_start_time"];
                $row["office_end_time"] = $wk_officehours["office_end_time"];
            }

			$atdbk_array    = $row;
		}
		return $atdbk_array;
	}

	/**
	 * 曜日に対応した当直時間を分単位のarrayで取得する
	 * @param $date 対象日付yyyymmddの文字列
	 * @param $end_time 退勤時刻
	 * @param $next_day_flag 翌日フラグ
	 * @param $tmcd_group_id グループID
	 */
	function get_duty_time_array($date, $end_time, $next_day_flag, $tmcd_group_id){

		//デフォルトは空文字
		$result_workday_time_array = array();

		//日付に対応する当直時間の取得
		$duty_start_date_time   = $this->get_duty_date_time_start($date);
		$duty_end_date_time     = $this->get_duty_date_time_end($date, $end_time, $next_day_flag, $tmcd_group_id);

		if(empty($duty_start_date_time) == false && empty($duty_end_date_time) == false){
			$result_workday_time_array = $this->get_minute_time_array($duty_start_date_time, $duty_end_date_time);
		}

		return $result_workday_time_array;
	}


	/**
	 * 勤務時間が当直にあたるか判定する
	 * @param $night_duty           当直フラグ 1以外は当直ではない
	 * @param $tmp_date             対象日付
	 * @param $start_time           出勤時間
	 * @param $previous_day_flag    前日フラグ
	 * @param $end_time             退勤時間
	 * @param $next_day_flag        翌日フラグ
	 * @param $tmcd_group_id グループID
	 * 
	 */
	function is_valid_duty($night_duty, $date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id){

		$valid_duty_flag = false;

		if ($night_duty == "1"){
			//日付に対応する当直時間の取得
			$duty_start_date_time = $this->get_duty_date_time_start($date);
			$duty_end_date_time   = $this->get_duty_date_time_end($date, $end_time, $next_day_flag, $tmcd_group_id);

			if(empty($duty_start_date_time) == false && empty($duty_end_date_time) == false &&
				empty($start_time) == false && empty($end_time) == false
			){
				$start_date_time = $this->get_timecard_start_date_time($date, $start_time, $previous_day_flag);
				$end_date_time   = $this->get_timecard_end_date_time($date, $end_time, $next_day_flag);

				//当直時間に出勤していたか確認
				$start_timestamp        = date_utils::to_timestamp_from_ymdhi($start_date_time);
				$end_timestamp          = date_utils::to_timestamp_from_ymdhi($end_date_time);
				$duty_start_timestamp   = date_utils::to_timestamp_from_ymdhi($duty_start_date_time);
				$duty_end_timestamp     = date_utils::to_timestamp_from_ymdhi($duty_end_date_time);

				//当直終了前に出勤かつ、当直開始後に退勤時に当直有効
				$valid_duty_flag = ($start_timestamp < $duty_end_timestamp && $end_timestamp > $duty_start_timestamp);
			}

		}

		return $valid_duty_flag;
	}

	/**
	 * 日付に対応する当直情報を取得する
	 * @param $date             対象日付
	 */
	function get_duty_info($date){
		global $timecard_array;

		$duty_info = array();

		//通常・土・祝日で変更
		if(date_utils::is_holiday($date) && array_key_exists("duty_info_hol", $timecard_array)){
			//祝日
			$duty_info = $timecard_array["duty_info_hol"];
		}
		else if(date_utils::is_Saturday($date) && array_key_exists("duty_info_sat", $timecard_array)){
			//土曜日
			$duty_info = $timecard_array["duty_info_sat"];
		}
		else if(array_key_exists("duty_info", $timecard_array)){
			//通常日
			$duty_info = $timecard_array["duty_info"];
		}

		return $duty_info;
	}

	/**
	 * 日付に対応する当直終了時刻が翌日か
	 * @param $date             対象日付
	 * @return 翌日:1 翌日ではない:0
	 */
	function get_duty_start_next_day_flag($date){
		//当直情報取得
		$duty_info = $this->get_duty_info($date);
		return $duty_info["night_start_next_day_flag"];
	}

	/**
	 * 日付に対応する当直開始時刻を取得する
	 * @param $date 対象日付
	 */
	function get_duty_time_start($date){
		//当直情報取得
		$duty_info = $this->get_duty_info($date);
		return $duty_info["night_start_time"];
	}

	/**
	 * 日付に対応する当直開始日時を取得する
	 * @param $date 対象日付
	 */
	function get_duty_date_time_start($date){

		$duty_start_time = "";

		//当直情報取得
		$duty_info = $this->get_duty_info($date);
		$duty_start_time = $duty_info["night_start_time"];

		if (empty($duty_start_time) == false){
			if ($duty_info["night_start_next_day_flag"] == 1){
				$duty_start_time = date_utils::add_day_ymdhi($date.$duty_start_time, 1);
			}
			else{
				$duty_start_time = $date.$duty_info["night_start_time"];
			}
		}

		return $duty_start_time;
	}

	/**
	 * 日付に対応する当直終了時刻が翌日か
	 * @param $date             対象日付
	 * @param $end_time         退勤時刻
	 * @param $next_day_flag    翌日フラグ
	 * @param $tmcd_group_id グループID
	 * @return 翌日:1 翌日ではない:0
	 */
	function get_duty_end_next_day_flag($date, $end_time, $next_day_flag, $tmcd_group_id){
		$duty_end_info = $this->get_duty_end_info($date, $end_time, $next_day_flag, $tmcd_group_id);
		return $duty_end_info["night_end_next_day_flag"];
	}

	/**
	 * 日付に対応する当直終了時刻を取得する
	 * @param $date             対象日付
	 * @param $end_time         退勤時刻
	 * @param $next_day_flag    翌日フラグ
	 * @param $tmcd_group_id グループID
	 */
	function get_duty_time_end($date, $end_time, $next_day_flag, $tmcd_group_id){
		$duty_end_info = $this->get_duty_end_info($date, $end_time, $next_day_flag, $tmcd_group_id);
		return $duty_end_info["night_end_time"];
	}

	/**
	 * 日付に対応する当直終了日時を取得する
	 * @param $date             対象日付
	 * @param $end_time         退勤時刻
	 * @param $next_day_flag    翌日フラグ
	 * @param $tmcd_group_id グループID
	 */
	function get_duty_date_time_end($date, $end_time, $next_day_flag, $tmcd_group_id){
		$duty_end_info = $this->get_duty_end_info($date, $end_time, $next_day_flag, $tmcd_group_id);
		return $duty_end_info["night_end_date_time"];
	}

	/**
	 * 曜日・退勤時刻に対応したworkday_countを取得する
	 * @param date yyyymmddの文字列
	 * @param end_time hhmm
	 * @param next_day_flag 翌日フラグ
	 * @param $tmcd_group_id グループID
	 * @return 対応する日数
	 */
	function get_night_workday_count($date, $end_time, $next_day_flag, $tmcd_group_id){

		$duty_end_info = $this->get_duty_end_info($date, $end_time, $next_day_flag, $tmcd_group_id);

		//退勤時刻から対応する日数換算を取得
		return $duty_end_info["night_workday_count"];
	}

	/**
	 * ※現在は未使用
	 * 曜日に対応したworkday_countを取得する(要勤務日数取得用)
	 * @param yyyymmddの文字列
	 */
	function get_prov_night_workday_count($date){

		$duty_info = $this->get_duty_info($date);

		//当直終了[１]の日数換算を取得
		return $duty_info["night_workday_count"];
	}

	/**
	 * 退勤時刻に対応する当直終了情報を取得する
	 * @param $date             対象日付
	 * @param $end_time         退勤時刻
	 * @param $next_day_flag    翌日フラグ
	 * @param $tmcd_group_id グループID
	 * @return 退勤時刻に対応する当直終了情報のarray(
	 *												night_end_date_time     :終了日時, 
	 *												night_end_time          :終了時刻, 
	 *												night_end_next_day_flag :翌日フラグ, 
	 *												night_workday_count     :日数換算)
	 */
	function get_duty_end_info($date, $end_time, $next_day_flag, $tmcd_group_id){

		global $duty_end_time_array;

		$duty_end_info   = array();
		$duty_end_date_time1    = "";
		$duty_end_date_time2    = "";

		//退勤日付時刻
		$end_date_time = $date.$end_time;
		if ($next_day_flag == 1){
			$end_date_time = date_utils::add_day_ymdhi($end_date_time, 1);
		}

		//当直情報取得
		$duty_info = $this->get_duty_info($date);
		$night_end_time1            = $duty_info["night_end_time"];
		$night_end_time2            = $duty_info["night_end_time2"];
		$night_workday_count1       = $duty_info["night_workday_count"];
		$night_workday_count2       = $duty_info["night_workday_count2"];
		$night_end_next_day_flag1   = $duty_info["night_end_next_day_flag"];
		$night_end_next_day_flag2   = $duty_info["night_end_next_day_flag2"];

		if ($night_end_next_day_flag1 == 1){
			$duty_end_date_time1 = date_utils::add_day_ymdhi($date.$night_end_time1, 1);
		}
		else{
			$duty_end_date_time1 = $date.$night_end_time1;
		}

		if ($night_end_next_day_flag2 == 1){
			$duty_end_date_time2 = date_utils::add_day_ymdhi($date.$night_end_time2, 1);
		}
		else{
			$duty_end_date_time2 = $date.$night_end_time2;
		}


		//1,2共に入力がない場合時間を返さない
		if (empty($night_end_time1) && empty($night_end_time2)){
			$duty_end_info = array();
		}
//グループ別に指定された終了時刻を使用するよう改修20090619
		$duty_end_time = $duty_end_time_array[$tmcd_group_id];
		if ($duty_end_time == "" || $duty_end_time == "1") {
			$duty_end_info["night_end_date_time"]     = $duty_end_date_time1;
			$duty_end_info["night_end_time"]          = $night_end_time1;
			$duty_end_info["night_end_next_day_flag"] = $night_end_next_day_flag1;
			$duty_end_info["night_workday_count"]     = $night_workday_count1;
		} else if ($duty_end_time == "2") {
			$duty_end_info["night_end_date_time"]     = $duty_end_date_time2;
			$duty_end_info["night_end_time"]          = $night_end_time2;
			$duty_end_info["night_end_next_day_flag"] = $night_end_next_day_flag2;
			$duty_end_info["night_workday_count"]     = $night_workday_count2;
		}
		return $duty_end_info;
	}

	/**
	 * apply_statusからリンクタイプの判定を行い残業申請情報を返す
	 *                                  マスタテーブルがない
	 * @param apply_status      申請ステータス
	 */
	function get_link_type_modify($apply_status){
		$link_type = "";

		switch ($apply_status) {
			case "0":  // 申請中
				$link_type = "2";
				break;
			case "1":  // 承認済
				$link_type = "3";
				break;
			case "2":  // 否認済
				$link_type = "4";
				break;
			case "3":  // 差戻済
				$link_type = "5";
				break;
			default:   // 未申請
				$link_type = "0";
				break;
		}

		return $link_type;
	}

	/**
	 * apply_statusからリンクタイプの判定を行い残業申請情報を返す
	 * @param $date                 対象日
	 * @param $start_time           出勤時刻
	 * @param $previous_day_flag    出勤時刻が前日フラグ	1:前日出勤、その他:当日退勤
	 * @param $end_time             退勤時刻
	 * @param $next_day_flag        退勤時刻が翌日フラグ	1:翌日退勤、その他:当日退勤
	 * @param $night_duty           当直フラグ              1:当直      その他:当直ではない
	 * @param $office_start_time    勤務開始時刻
	 * @param $office_end_time      勤務終了時刻
	 * @param $apply_status         残業の申請ステータス
	 * @param $early_over_time		 早出残業時間
	 * @param $tmcd_group_id		 グループID
	 * @param $over_start_time		 残業開始時刻
	 * @param $over_end_time		 残業終了時刻
	 * @param $over_24hour_flag	 終了時刻は、開始時刻から24時間を超えている 
	 * @param $tmcd_group_id グループID
	 * @param $over_start_time2	 残業開始時刻2
	 * @param $over_end_time2		 残業終了時刻2
	 * @param $$over_start_next_day_flag2	 残業開始時刻2翌日フラグ
	 * @param $$over_end_next_day_flag2	 残業終了時刻2翌日フラグ
	 * @return $result_flag 残業申請リンクタイプ 0:申請不要 1:要申請（未申請） 2:申請中 3:承認済 4:否認済 5:差戻済 6:残業申請不要
	 */
	function get_link_type_overtime($date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $office_start_time, $office_end_time, $apply_status, $tmcd_group_id, $over_start_time = "", $over_end_time = "", $early_over_time="", $over_24hour_flag="", $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2){
		$link_type = "";

		switch ($apply_status) {
			case "0":  // 申請中
				$link_type =  "2";
				break;
			case "1":  // 承認済
				$link_type =  "3";
				break;
			case "2":  // 否認済
				$link_type =  "4";
				break;
			case "3":  // 差戻済
				$link_type =  "5";
				break;
			case "4":  // 申請不要
				$link_type =  "6";
				break;
		}

		//申請が行われていない場合
		if ($link_type == ""){
			if($this->is_overtime_apply($date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $office_start_time, $office_end_time, true, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2)){
				//設定時間を超過時は、要申請
				$link_type = "1";
			}
			else{
				//設定時間以下は、申請不要
				$link_type = "0";
			}
		}

		return $link_type;
	}

	/**
	 * 残業申請が必要か判定する
	 *
	 * @param $date                 対象日
	 * @param $start_time           出勤時刻
	 * @param $previous_day_flag    出勤時刻が前日フラグ	1:前日出勤、その他:当日退勤
	 * @param $end_time             退勤時刻
	 * @param $next_day_flag        退勤時刻が翌日フラグ	1:翌日退勤、その他:当日退勤
	 * @param $night_duty           当直フラグ             1:当直      その他:当直ではない
	 * @param $office_start_time    勤務開始時刻
	 * @param $office_end_time      勤務終了時刻
	 * @param $link_type_flag       リンク表示から呼ばれた場合は、事後申請時もtrueで返す
	 * @param $tmcd_group_id		 グループID
	 * @param $over_start_time		 残業開始時刻
	 * @param $over_end_time		 残業終了時刻
	 * @param $early_over_time		 早出残業時間
	 * @param $over_24hour_flag	 終了時刻は、開始時刻から24時間を超えている 
	 * @param $over_start_time2	 残業開始時刻2
	 * @param $over_end_time2		 残業終了時刻2
	 * @param $over_start_next_day_flag2	 残業開始時刻2翌日フラグ
	 * @param $over_end_next_day_flag2	 残業終了時刻2翌日フラグ
	 * @return $result_flag true:残業あり false:残業なし
	 */
	function is_overtime_apply($date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $office_start_time, $office_end_time, $link_type_flag = false, $tmcd_group_id, $over_start_time = "", $over_end_time = "", $early_over_time="", $over_24hour_flag, $over_start_time2 = "", $over_end_time2 = "", $over_start_next_day_flag2, $over_end_next_day_flag2
	) {
		global $timecard_array;

		$result_flag = false;

		//残業時刻追加 20100114
		//残業開始時刻があれば勤務終了時刻（所定）とする
		$tmp_office_end_time = ($over_start_time != "") ? $over_start_time : $office_end_time;
		//残業終了時刻があれば退勤時刻とする 
		if ($over_end_time != "") {
			$tmp_end_time = $over_end_time;
		} 
		//残業時刻がない場合は残業計算をしない 20110825
		else {
			//所定時刻がある場合は設定する
			if ($tmp_office_end_time != "" && $link_type_flag == false) { //20110905 タイムカード入力画面で残業申請リンクが表示されない不具合対応
				$tmp_end_time = $tmp_office_end_time;
			} else {
				$tmp_end_time = $end_time;
			}
		}
		
		//当直が有効か判定
		$night_duty_flag = $this->is_valid_duty($night_duty, $date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

		//実際の作業終了日と所定勤務終了日を取得
		$end_date_time      = $date.$tmp_end_time;
		$work_end_date_time = "";
		if ($next_day_flag == 1) {
			$end_date_time = date_utils::add_day_ymdhi($end_date_time, 1);
		}

		//当直有効時は当直時間で残業申請有無を判定する
		if ($night_duty_flag){
			$work_end_date_time = $this->get_duty_date_time_end($date, $tmp_end_time, $next_day_flag, $tmcd_group_id);
		}
		else if (empty($tmp_office_end_time) == false){
			$work_end_date_time = $date.$tmp_office_end_time;
			if (($previous_day_flag == 0 && //前日フラグがついていない場合
					$office_start_time > $office_end_time)|| //開始終了時刻 以上(>=)をより大きい(>)へ 休日残業時不具合20101228
					$over_24hour_flag == "1") { //終了時刻は、開始時刻から24時間を超えている
				$work_end_date_time = date_utils::add_day_ymdhi($work_end_date_time, 1);
			}
		}

		//実際の作業開始日と所定勤務開始日を取得
		$start_date_time      = $date.$start_time;
		$work_start_date_time = "";
		if ($previous_day_flag == 1) {
			$start_date_time = date_utils::add_day_ymdhi($start_date_time, -1);
		}
		if (empty($office_start_time) == false){
			$work_start_date_time = $date.$office_start_time;
			if ($office_start_time > $tmp_office_end_time) {
				$work_start_date_time = date_utils::add_day_ymdhi($work_start_date_time, -1);
			}
		}

		//残業申請が必要な時間か判定
		$over_time_apply_type = $timecard_array["over_time_apply_type"];
		$ovtmscr_tm = $timecard_array["ovtmscr_tm"];

		//早出残業時間
		$over_time_minute2 = 0;
		if ($early_over_time != "") {
			$over_time_minute2 = date_utils::hi_to_minute($early_over_time);
		} else {
			//早出残業時間を集計する場合
			if ($timecard_array["others5"] == "1") {
				$over_time_minute2 = date_utils::get_diff_minute($work_start_date_time, $start_date_time);
			}
		}
		
		
		//残業２
		$over_time_minute3 = 0; // minute2は早出で使用しているためminute3とする
		//設定が有る場合
		if ($over_start_time2 != "" && $over_end_time2 != "") {
			
			//日時設定
			$start_date_time2 = $date.$over_start_time2;
			if ($over_start_next_day_flag2 == 1) {
				$start_date_time2 = date_utils::add_day_ymdhi($start_date_time2, 1);
			}
			
			$end_date_time2 = $date.$over_end_time2;
			if ($over_end_next_day_flag2 == 1) {
				$end_date_time2 = date_utils::add_day_ymdhi($end_date_time2, 1);
			}
			
			//時間計算
			$over_time_minute3 = date_utils::get_diff_minute($end_date_time2, $start_date_time2);
			
		}
		if ($over_time_apply_type == 1){
			//残業申請指定時間を超えていたら要申請
			$over_time_minute = date_utils::get_diff_minute($end_date_time, $work_end_date_time);

			$result_flag = (empty($ovtmscr_tm) == false && (($over_time_minute + $over_time_minute3) > $ovtmscr_tm || $over_time_minute2 > $ovtmscr_tm));
		}
		//事後申請はリンク表示時のみ
		else if($over_time_apply_type && $link_type_flag){
			//１分以上残業がある場合申請
			$over_time_minute = date_utils::get_diff_minute($end_date_time, $work_end_date_time);
			$result_flag = ($over_time_minute + $over_time_minute2 + $over_time_minute3 > 0);
		}

		return $result_flag;
	}
	
	/**
	 *早出残業か判定する
	 *
	 * @param $date                 対象日
	 * @param $start_time           出勤時刻
	 * @param $previous_day_flag    出勤時刻が前日フラグ	1:前日出勤、その他:当日退勤
	 * @param $office_start_time    勤務開始時刻
	 * @param $office_end_time      勤務終了時刻
	 * @param $tmcd_group_id グループID
	 * @return $result_flag true:残業あり false:残業なし
	 */
	function is_overtime_early($date, $start_time, $previous_day_flag, $office_start_time, $office_end_time, $tmcd_group_id){
		global $timecard_array;

		$result_flag = false;

		//実際の作業開始日と所定勤務開始日を取得
		$start_date_time      = $date.$start_time;
		$work_start_date_time = "";
		if ($previous_day_flag == 1) {
			$start_date_time = date_utils::add_day_ymdhi($start_date_time, -1);
		}
		if (empty($office_start_time) == false){
			$work_start_date_time = $date.$office_start_time;
			if ($office_start_time >= $office_end_time) {
				$work_start_date_time = date_utils::add_day_ymdhi($work_start_date_time, -1);
			}
		}

		//残業申請が必要な時間か判定
		$over_time_apply_type = $timecard_array["over_time_apply_type"];
		$ovtmscr_tm = $timecard_array["ovtmscr_tm"];
		//早出残業時間
		$over_time_minute2 = 0;
		if ($over_time_apply_type == 1){
			//残業申請指定時間を超えていたら要申請
			//早出残業時間
			if ($timecard_array["others5"] == "1") {
				$over_time_minute2 = date_utils::get_diff_minute($work_start_date_time, $start_date_time);
			}

			$result_flag = (empty($ovtmscr_tm) == false && ($over_time_minute2 > $ovtmscr_tm));
		}

		return $result_flag;
	}

	/**
	 * apply_statusからリンクタイプの判定を行い残業申請情報を返す
	 *                                  マスタテーブルがない
	 * @param apply_status      申請ステータス
	 */
	function get_link_type_return($ret_btn_flg, $o_start_time1, $apply_status){
		$link_type = "";

		switch ($apply_status) {
			case "0":  // 申請中
				$link_type = "2";
				break;
			case "1":  // 承認済
				$link_type = "3";
				break;
			case "2":  // 否認済
				$link_type = "4";
				break;
			case "3":  // 差戻済
				$link_type = "5";
				break;
			default:   // 未申請
				$link_type = "1";
				break;
		}

		// 退勤後復帰ボタンが非表示または退勤後未復帰の場合は申請不要
		if ($ret_btn_flg != "t" || $o_start_time1 == "") {
			$link_type = "0";
		}

		// 未申請の場合は要申請
		return $link_type;

	}

	/**
	 * fraction1_min,fraction2_min等から
	 * 繰り下げ・繰り上げ時間を取得する
	 *                                  マスタテーブルがない
	 * @param fraction_min      申請ステータス
	 */
	function get_moving_minutes($fraction_min){
		$moving_minutes = 0;

		switch ($fraction_min) {
			case "A":
				$moving_minutes = 5;
				break;
			case "B":
				$moving_minutes = 10;
				break;
			case "1":
				$moving_minutes = 15;
				break;
			case "C":
				$moving_minutes = 20;
				break;
			case "2":
				$moving_minutes = 30;
				break;
			case "D":
				$moving_minutes = 40;
				break;
			case "E":
				$moving_minutes = 45;
				break;
			case "3":
				$moving_minutes = 60;
				break;
		}

		return $moving_minutes;
	}

	/**
	 * 確定出勤時刻／遅刻分数を取得
	 * @param $arr_tmp_work_time	実動時間 出勤〜退勤までの時間を１分単位の文字列の配列で持つ
	 * @param $arr_time2			所定労働 午前・午後休などの休憩時間を除いた所定労働時間
	 * @param $arr_time4			休憩時間 休憩開始〜終了
	 * @param $fraction1			timecard.fraction1			繰り上げ・下げ判定（出勤用）
	 * @param $fraction1_min		timecard.fraction1_min		繰り上げ間隔判定（出勤用）
	 * @param $fraction3			timecard.fraction3			1の場合、端数処理を行うらしい（遅刻じゃないときは行ってる
	 * @param $others4				timecard.others4			2の場合、休憩中の遅刻を遅刻時間に加算
	 * @param $others5				timecard.others5			
	 */
	function get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5) {

		$ret = array();

        // 実績が所定労働時間として使われた場合は遅刻なしとする
		if ($arr_tmp_work_time == $arr_time2) {
			$ret["fixed_time"] = $arr_tmp_work_time[0];
			$ret["diff_minutes"] = 0;
			$ret["diff_count"] = 0;
            //echo "time[0]=$arr_tmp_work_time[0] <br>\n";
            return $ret;
		}

		$start_time = $arr_tmp_work_time[0];
		$end_time = date_utils::add_minute_ymdhi($arr_tmp_work_time[count($arr_tmp_work_time) - 1], 1);
		$office_start_time = $arr_time2[0];

		// 遅刻の判定
		$arr_tmp = array_diff($arr_time2, $arr_tmp_work_time);
		// 所定労働時間開始時にいないフラグ（遅刻）
		$flg = in_array($office_start_time, $arr_tmp);
        
		// 遅刻かつ端数処理を行わない場合
		if ($flg && $fraction3 != "1") {

			// 遅刻時間を算出
			$arr_tmp = $this->get_minute_time_array($office_start_time, $start_time);
			if ($others4 == "2") {  // 休憩中の遅刻を遅刻時間に加算
				$diff_minutes = count($arr_tmp);
			} else {
				$diff_minutes = count(array_diff($arr_tmp, $arr_time4));
			}

			// 出勤時刻はそのまま
			$fixed_start_time = $start_time;
		} else {
			//端数処理// 出勤時刻を確定
			$moving_minutes = $this->get_moving_minutes($fraction1_min);
			$fixed_start_time = date_utils::move_time($office_start_time, $start_time, $moving_minutes, $fraction1);
            //echo "moving_minutes=$moving_minutes start_time=$start_time fixed_start_time=$fixed_start_time<br>\n";
			// 遅刻の再判定（端数処理の結果、遅刻でなくなることもあり得る）
			$arr_tmp = $this->get_minute_time_array($fixed_start_time, $end_time);
			$arr_tmp = array_diff($arr_time2, $arr_tmp);
			$flg = in_array($office_start_time, $arr_tmp);

			// 遅刻時間を算出
			$diff_minutes = 0;
			if ($flg) {
				$arr_tmp = $this->get_minute_time_array($office_start_time, $fixed_start_time);
				if ($others4 == "2") {  // 休憩中の遅刻を遅刻時間に加算
					$diff_minutes = count($arr_tmp);
				} else {
					$diff_minutes = count(array_diff($arr_tmp, $arr_time4));
				}
			}
		}

		// 早出残業なし設定かつ遅刻でない場合は定時出勤とみなす
		if ($others5 == "2" && $diff_minutes == 0) {
			$ret["fixed_time"] = $arr_time2[0];
			$ret["diff_minutes"] = 0;
			$ret["diff_count"] = 0;
			return $ret;
		}

		$ret["fixed_time"] = $fixed_start_time;
		$ret["diff_minutes"] = $diff_minutes;
		$ret["diff_count"] = ($diff_minutes > 0) ? 1 : 0;

		return $ret;
	}

	/**
	 * 確定退勤時刻／早退分数を取得
	 * @param $arr_tmp_work_time	実動時間 出勤〜退勤までの時間を１分単位の文字列の配列で持つ
	 * @param $arr_time2			所定労働 午前・午後休などの休憩時間を除いた所定労働時間
	 * @param $arr_time4			休憩時間 休憩開始〜終了
	 * @param $fraction1			timecard.fraction2			繰り上げ・下げ判定（退勤用）
	 * @param $fraction1_min		timecard.fraction1_min		繰り上げ間隔判定（退勤用）
	 * @param $fraction3			timecard.fraction3			1の場合、端数処理を行うらしい（遅刻じゃないときは行ってる
	 * @param $others4				timecard.others4			2の場合、休憩中の早退を早退時間に加算
	 */
	function get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4) {

		$ret = array();
		$start_time = $arr_tmp_work_time[0];
		$end_time   = date_utils::add_minute_ymdhi($arr_tmp_work_time[count($arr_tmp_work_time) - 1], 1);

		// 実績が所定労働時間として使われた場合は早退なしとする
		if ($arr_tmp_work_time == $arr_time2) {
			$ret["fixed_time"] = $end_time;
			$ret["diff_minutes"] = 0;
			$ret["diff_count"] = 0;
			return $ret;
		}

		$check_time = $arr_time2[count($arr_time2) - 1];
		$office_end_time = date_utils::add_minute_ymdhi($check_time, 1);

		// 早退の判定
		$arr_tmp = array_diff($arr_time2, $arr_tmp_work_time);
		$flg = in_array($check_time, $arr_tmp);

		// 早退かつ端数処理を行わない場合
		if ($flg && $fraction3 != "1") {

			// 早退時間を算出
			$arr_tmp = $this->get_minute_time_array($end_time, $office_end_time);
			if ($others4 == "2") {  // 休憩中の早退を早退時間に加算
				$diff_minutes = count($arr_tmp);
			} else {
				$diff_minutes = count(array_diff($arr_tmp, $arr_time4));
			}

			// 退勤時刻はそのまま
			$fixed_end_time = $end_time;
		} else {
			//端数処理// 退勤時刻を確定
			$moving_minutes = $this->get_moving_minutes($fraction2_min);
			$fixed_end_time = date_utils::move_time($office_end_time, $end_time, $moving_minutes, $fraction2);

			// 早退の再判定（端数処理の結果、早退でなくなることもあり得る）
			$arr_tmp = $this->get_minute_time_array($start_time, $fixed_end_time);
			$arr_tmp = array_diff($arr_time2, $arr_tmp);
			$flg = in_array($check_time, $arr_tmp);

			// 早退時間を算出
			$diff_minutes = 0;
			if ($flg) {
				$arr_tmp = $this->get_minute_time_array($fixed_end_time, $office_end_time);
				if ($others4 == "2") {  // 休憩中の早退を早退時間に加算
					$diff_minutes = count($arr_tmp);
				} else {
					$diff_minutes = count(array_diff($arr_tmp, $arr_time4));
				}
			}
		}

		$ret["fixed_time"] = $fixed_end_time;
		$ret["diff_minutes"] = $diff_minutes;
		$ret["diff_count"] = ($diff_minutes > 0) ? 1 : 0;

		return $ret;
	}

	/**
	 * 確定出勤時刻／遅刻分数を取得
	 *  時間の配列ではなく時刻を引数とする
	 * @param $start_date_time	出勤時刻
	 * @param $end_date_time	退勤時刻
	 * @param $start2_date_time	所定開始時刻
	 * @param $end2_date_time	所定終了時刻
	 * @param $start4_date_time	休憩開始時刻
	 * @param $end4_date_time	休憩終了時刻
	 * @param $fraction1			timecard.fraction1			繰り上げ・下げ判定（出勤用）
	 * @param $fraction1_min		timecard.fraction1_min		繰り上げ間隔判定（出勤用）
	 * @param $fraction3			timecard.fraction3			1の場合、端数処理を行う
	 */
	function get_start_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction1, $fraction1_min, $fraction3) {
		$ret = array();

		//時間にコロン(:)がある場合消す
		$wk_start_date_time = str_replace(":", "", $start_date_time);
		$wk_end_date_time = str_replace(":", "", $end_date_time);
		$wk_start2_date_time = str_replace(":", "", $start2_date_time);
		$wk_end2_date_time = str_replace(":", "", $end2_date_time);
		$wk_start4_date_time = str_replace(":", "", $start4_date_time);
		$wk_end4_date_time = str_replace(":", "", $end4_date_time);
		
		// 実績が所定労働時間として使われた場合は遅刻なしとする
		if ($wk_start_date_time == $wk_start2_date_time && $wk_end_date_time == $wk_end2_date_time) {
			$ret["fixed_time"] = $wk_start_date_time;
			$ret["diff_minutes"] = 0;
			$ret["diff_count"] = 0;
			return $ret;
		}
		
		// 遅刻の判定
		// 所定労働時間開始時にいないフラグ（遅刻）
		$flg = ($wk_start_date_time > $wk_start2_date_time) ? true : false;
		// 遅刻かつ端数処理を行わない場合
		if ($flg && $fraction3 != "1") {

			// 遅刻時間を算出、休憩の重複分は除く
			$diff_minutes = $this->get_diff_times($wk_start2_date_time, $wk_start_date_time, $wk_start4_date_time, $wk_end4_date_time);

			// 出勤時刻はそのまま
			$fixed_start_time = $wk_start_date_time;
		} else {
			//端数処理// 出勤時刻を確定
			$moving_minutes = $this->get_moving_minutes($fraction1_min);
			$fixed_start_time = date_utils::move_time($wk_start2_date_time, $wk_start_date_time, $moving_minutes, $fraction1);

			// 遅刻の再判定（端数処理の結果、遅刻でなくなることもあり得る）
			$flg = ($fixed_start_time > $wk_start2_date_time) ? true : false;
			// 遅刻時間を算出
			$diff_minutes = 0;
			if ($flg) {
				// 遅刻時間を算出、休憩の重複分は除く
				$diff_minutes = $this->get_diff_times($wk_start2_date_time, $fixed_start_time, $wk_start4_date_time, $wk_end4_date_time);
			}
		
		}

		// 遅刻でない場合は定時出勤とみなす
		if ($diff_minutes == 0) {
			$ret["fixed_time"] = $wk_start2_date_time;
			$ret["diff_minutes"] = 0;
			$ret["diff_count"] = 0;
			return $ret;
		}

		$ret["fixed_time"] = $fixed_start_time;
		$ret["diff_minutes"] = $diff_minutes;
		$ret["diff_count"] = ($diff_minutes > 0) ? 1 : 0;

		return $ret;
	}

	/**
	 * 確定退勤時刻／早退分数を取得
	 *  時間の配列ではなく時刻を引数とする
	 * @param $start_date_time	出勤時刻
	 * @param $end_date_time	退勤時刻
	 * @param $start2_date_time	所定開始時刻
	 * @param $end2_date_time	所定終了時刻
	 * @param $start4_date_time	休憩開始時刻
	 * @param $end4_date_time	休憩終了時刻
	 * @param $fraction1			timecard.fraction2			繰り上げ・下げ判定（退勤用）
	 * @param $fraction1_min		timecard.fraction1_min		繰り上げ間隔判定（退勤用）
	 * @param $fraction3			timecard.fraction3			1の場合、端数処理を行う
	 */
	function get_end_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction2, $fraction2_min, $fraction3) {

		$ret = array();

		//時間にコロン(:)がある場合消す
		$wk_start_date_time = str_replace(":", "", $start_date_time);
		$wk_end_date_time = str_replace(":", "", $end_date_time);
		$wk_start2_date_time = str_replace(":", "", $start2_date_time);
		$wk_end2_date_time = str_replace(":", "", $end2_date_time);
		$wk_start4_date_time = str_replace(":", "", $start4_date_time);
		$wk_end4_date_time = str_replace(":", "", $end4_date_time);
		
		// 実績が所定労働時間として使われた場合は早退なしとする
		if ($wk_start_date_time == $wk_start2_date_time && $wk_end_date_time == $wk_end2_date_time) {
			$ret["fixed_time"] = $wk_end_date_time;
			$ret["diff_minutes"] = 0;
			$ret["diff_count"] = 0;
			return $ret;
		}
		// 早退の判定
		$flg = ($wk_end_date_time < $wk_end2_date_time) ? true : false;
		
		// 早退かつ端数処理を行わない場合
		if ($flg && $fraction3 != "1") {
			// 遅刻時間を算出、休憩の重複分は除く
			$diff_minutes = $this->get_diff_times($wk_end_date_time, $wk_end2_date_time, $wk_start4_date_time, $wk_end4_date_time);
			
			// 退勤時刻はそのまま
			$fixed_end_time = $wk_end_date_time;
		} else {
			//端数処理// 退勤時刻を確定
			$moving_minutes = $this->get_moving_minutes($fraction2_min);
			$fixed_end_time = date_utils::move_time($wk_end2_date_time, $wk_end_date_time, $moving_minutes, $fraction2);
			
			// 早退の再判定（端数処理の結果、早退でなくなることもあり得る）
			$flg = ($fixed_end_time < $wk_end2_date_time) ? true : false;
			// 早退時間を算出
			$diff_minutes = 0;
			if ($flg) {
				// 遅刻時間を算出、休憩の重複分は除く
				$diff_minutes = $this->get_diff_times($fixed_end_time, $wk_end2_date_time, $wk_start4_date_time, $wk_end4_date_time);
			}
		}

		$ret["fixed_time"] = $fixed_end_time;
		$ret["diff_minutes"] = $diff_minutes;
		$ret["diff_count"] = ($diff_minutes > 0) ? 1 : 0;
		
		return $ret;
	}

	/**
	 * show_timecard_commoin.ini create_time_array($start_time, $end_time)のyyyyMMdd hhmm版
	 * 年月日を追加することで、２４時間を超える作業時間に対応させる
	 * @param $start_date 開始日時 yyyyMMddhhmm または yyyyMMddhh:mm
	 * @param $end_date   終了日時 yyyyMMddhhmm または yyyyMMddhh:mm
	 */
	function get_minute_time_array($start_date_time, $end_date_time){

		$result_array = array();

		if ($start_date_time != "" && $end_date_time != "") {
			$start_timestamp = date_utils::to_timestamp_from_ymdhi($start_date_time);
			$end_timestamp   = date_utils::to_timestamp_from_ymdhi($end_date_time);

			//分単位で開始から終了時間をarrayに保持する
			$work_timestamp = $start_timestamp;
			while ($work_timestamp < $end_timestamp && empty($work_timestamp) == false) {
				array_push($result_array, date("YmdHi", $work_timestamp));
				$work_timestamp = strtotime("+1 minute", $work_timestamp);
			}
		}

		return $result_array;
	}

	/**
	 * 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列
	 * @param $start_date 開始日時 yyyyMMddhhmm または yyyyMMddhh:mm
	 * @param $end_date   終了日時 yyyyMMddhhmm または yyyyMMddhh:mm
	 * @param $previous_day_flag 前日出勤フラグ
	 * @param $next_day_flag 翌日出勤フラグ
	 */
	function get_minute_time_array2($start_date_time, $end_date_time, $previous_day_flag, $next_day_flag){

		$result_array = array();

		if ($start_date_time != "" && $end_date_time != "") {
			$start_timestamp = date_utils::to_timestamp_from_ymdhi($start_date_time);
			$end_timestamp   = date_utils::to_timestamp_from_ymdhi($end_date_time);

			//分単位で開始から終了時間をarrayに保持する
			$work_timestamp = $start_timestamp;
			while ($work_timestamp < $end_timestamp && empty($work_timestamp) == false) {
				array_push($result_array, date("YmdHi", $work_timestamp));
				$work_timestamp = strtotime("+1 minute", $work_timestamp);
			}
			// 前日出勤フラグ
			if ($previous_day_flag) {
				$work_timestamp = strtotime("-1 day", $start_timestamp);
				$prev_end_timestamp = strtotime("-1 day", $end_timestamp);

				while ($work_timestamp < $prev_end_timestamp && empty($work_timestamp) == false) {

					array_push($result_array, date("YmdHi", $work_timestamp));
					$work_timestamp = strtotime("+1 minute", $work_timestamp);
				}


			}
			// 翌日退勤フラグ
			if ($next_day_flag) {
				$work_timestamp = strtotime("+1 day", $start_timestamp);
				$next_end_timestamp = strtotime("+1 day", $end_timestamp);

				while ($work_timestamp < $next_end_timestamp && empty($work_timestamp) == false) {

					array_push($result_array, date("YmdHi", $work_timestamp));
					$work_timestamp = strtotime("+1 minute", $work_timestamp);
				}


			}
		}

		return $result_array;
	}

	/**
	 * 夜勤のarrayを取得する
	 * @param $day
	 * @param $start_time
	 * @param $end_time
	 * @return 前日・当日・翌日の深夜勤務時間
	 */
	function get_late_night_shift_array($date, $start_time, $end_time){
		$result_array = array();

		if (empty($date) == false && empty($start_time) == false && empty($end_time) == false ){
			$start_date_time            = $date.$start_time;
			$end_date_time              = $this->get_schedule_end_date_time($date, $start_time, $end_time);
			$next_start_date_time       = date_utils::add_day_ymdhi($start_date_time, 1);
			$next_end_date_time         = date_utils::add_day_ymdhi($end_date_time, 1);
			$previous_start_date_time   = date_utils::add_day_ymdhi($start_date_time, -1);
			$previous_end_date_time     = date_utils::add_day_ymdhi($end_date_time, -1);
			$result_array               = $this->get_minute_time_array($previous_start_date_time, $previous_end_date_time);
			$result_array               = array_merge($result_array, $this->get_minute_time_array($start_date_time, $end_date_time));
			$result_array               = array_merge($result_array, $this->get_minute_time_array($next_start_date_time, $next_end_date_time));
		}

		return $result_array;
	}
	/**
	 * 夜勤のtimes_infoを取得する
	 * @param $day
	 * @param $start_time
	 * @param $end_time
	 * @return 前日・当日・翌日の深夜勤務時間情報
	 */
	function get_late_night_shift_info($date, $start_time, $end_time){
		$times_info = array();
		
		if (empty($date) == false && empty($start_time) == false && empty($end_time) == false ){
			$start_date_time            = $date.$start_time;
			$end_date_time              = $this->get_schedule_end_date_time($date, $start_time, $end_time);
			$next_start_date_time       = date_utils::add_day_ymdhi($start_date_time, 1);
			$next_end_date_time         = date_utils::add_day_ymdhi($end_date_time, 1);
			$previous_start_date_time   = date_utils::add_day_ymdhi($start_date_time, -1);
			$previous_end_date_time     = date_utils::add_day_ymdhi($end_date_time, -1);

			$times_info[0]["start_time"] = $previous_start_date_time;
			$times_info[0]["end_time"] = $previous_end_date_time;
			$times_info[1]["start_time"] = $start_date_time;
			$times_info[1]["end_time"] = $end_date_time;
			$times_info[2]["start_time"] = $next_start_date_time;
			$times_info[2]["end_time"] = $next_end_date_time;
			$times_info["data_cnt"] = 3;
		}
		
		return $times_info;
	}
	
	/**
	 * 翌日フラグを持たない時間は開始終了が矛盾時に翌日と判断し
	 * 判定した日時を返す
	 * @param day        対象日付 yyyymmdd
	 * @param start_time 開始時間 hhmm またはhh:mm
	 * @param end_time   終了時間 hhmm またはhh:mm
	 * @return yyyymmddhhmm 
	 */
	function get_schedule_end_date_time($day, $start_time, $end_time){

		$reslt_date = "";

		if (empty($day) == false && empty($start_time) == false && empty($end_time) == false){
			if ($start_time > $end_time){
				$reslt_date = date("YmdHi", strtotime("+1 day", date_utils::to_timestamp_from_hhmm($end_time, $day)));
			}
			else{
				$reslt_date = date("YmdHi", date_utils::to_timestamp_from_hhmm($end_time, $day));
			}
		}

		return $reslt_date;
	}

	/**
	 * タイムカードの開始時間は前日フラグで前日と判断し
	 * 判定した日時を返す
	 * @param day        対象日付 yyyymmdd
	 * @param start_time 開始時間 hhmm またはhh:mm
	 * @param end_time   終了時間 hhmm またはhh:mm
	 * @return yyyymmddhhmm 
	 */
	function get_timecard_start_date_time($day, $start_time, $previous_day_flag){

		$reslt_date_time = "";

		if (empty($day) == false &&  empty($start_time) == false){
			if ($previous_day_flag == 1){
				$reslt_date_time = date("YmdHi", strtotime("-1 day", date_utils::to_timestamp_from_hhmm($start_time, $day)));
			}
			else{
				$reslt_date_time = date("YmdHi", date_utils::to_timestamp_from_hhmm($start_time, $day));
			}
		}

		return $reslt_date_time;
	}

	/**
	 * タイムカードの時間は翌実フラグで翌日と判断し
	 * 判定した日時を返す
	 * @param day        対象日付 yyyymmdd
	 * @param start_time 開始時間 hhmm またはhh:mm
	 * @param end_time   終了時間 hhmm またはhh:mm
	 * @return yyyymmddhhmm 
	 */
	function get_timecard_end_date_time($day, $end_time, $next_day_flag){

		$reslt_date = "";

		if (empty($day) == false &&  empty($end_time) == false){
			if ($next_day_flag == 1){
				$reslt_date = date("YmdHi", strtotime("+1 day", date_utils::to_timestamp_from_hhmm($end_time, $day)));
			}
			else{
				$reslt_date = date("YmdHi", date_utils::to_timestamp_from_hhmm($end_time, $day));
			}
		}

		return $reslt_date;
	}

	/**
	 * 退勤後復帰の時間を分で返す
	 * @param $end_date_time     退勤日時 yyyymmddhhmm
	 * @param $return_start_time 退勤後復帰開始時間 hhmm またはhh:mm
	 * @param $return_end_time   退勤後復帰終了時間 hhmm またはhh:mm
	 * @return 退勤後復帰時間（分）
	 */
	function get_return_array($end_date_time, $return_start_time, $return_end_time){

		$reslt_array = array();

		if (empty($end_date_time) == false && empty($return_start_time) == false && empty($return_end_time) == false){
			//退勤日時から退勤日付取得
			$end_date = date_utils::get_format_ymdhi($end_date_time, "Ymd");
			$ret_start_date_time    = $end_date.$return_start_time;

			//退勤日時より退勤後復日時が小さい場合、退勤後復帰日時は退勤日の翌日
			if ($end_date > $ret_start_date_time){
				$ret_start_date_time = date_utils::add_day_ymdhi($ret_start_date_time, 1);
			}

			//退勤後復日時から退勤後復日時取得
			$end_date = date_utils::get_format_ymdhi($ret_start_date_time, "Ymd");
			$ret_end_date_time    = $end_date.$return_end_time;

			//退勤日時より退勤後復日時が小さい場合、退勤後復帰日時は退勤日の翌日
			if ($ret_start_date_time > $ret_end_date_time){
				$ret_end_date_time = date_utils::add_day_ymdhi($ret_end_date_time, 1);
			}

			//array
			$reslt_array = $this->get_minute_time_array($ret_start_date_time, $ret_end_date_time);
		}

		return $reslt_array;
	}


	/**
	 * calendar_nameテーブルのday1_time項目、所定労働時間を返す
	 * @return calendar_name.day1_time
	 */
	function get_calendarname_day1_time(){
		global $timecard_array;
		$calendarname_info = $timecard_array["calendarname_info"];
		return $calendarname_info["day1_time"];
	}

	/**
	 * calendar_nameテーブル情報を配列で返す
	 * @return calendar_nameの配列
	 */
	function get_calendarname_info(){
		global $timecard_array;
		return $timecard_array["calendarname_info"];
	}

	/**
	 * カレンダー名を返す
	 * @param  $type     
	 * @param  $date     
	 * @return $calendarname （通常勤務日、法定休日、祝日名等）
	 */
	function get_calendarname($type, $date){
		global $timecard_array;

		switch ($type) {
		case "2":	// 勤務日1
			$var = "day4";
			break;
		case "3":	// 勤務日2
			$var = "day5";
			break;
		case "4":	// 法定休日
			$var = "day2";
			break;
		case "5":	// 所定休日
			$var = "day3";
			break;
		default:	// 1:通常勤務日 6:祝日 7:年末年始休暇
			$var = "day".$type;
		}

		$calendarname_info = $timecard_array["calendarname_info"];

		//祝日、法定休日
		if ($type == "6" || $type == "4") {
			$calendarname = get_holiday_name($date);
			// 振替休日以外は法定休日
			if ($type == "4" && $calendarname == "") {
				$calendarname = $calendarname_info[$var];
			}
			
		} else {
			$calendarname = $calendarname_info[$var];
		}
		return $calendarname;
	}
	
	/**
	 * カレンダーの属性をもとに要勤務日数を返す
	 * @param  $type  属性
	 * @return $day   日数
	 */
	function get_you_kinmu_nissu($type){

		switch ($type) {
		case "1":
		case "2":
		case "3":
			$day = 1;
			break;
		case "":
		case "4":
		case "5":
		case "6":
		case "7":
			$day = 0;
			break;
		default:
			$day = 0;
		}
		return $day;
	}

	/**
	 * 休日勤務時間（分）と休日出勤した日を配列にして返す
	 * @param  $arr_type  属性
	 * @param  $today     
	 * @param  $arr_effective_time  
	 * @param  $start_time  
	 * @param  $previous_day_flag  
	 * @param  $end_time  
	 * @param  $next_day_flag  
	 * @param  $reason
	 * @return $arr_hol_time  配列 0:前日 1:当日 2:翌日 3:合計 
	 *			4:前日が休出なら前日日付
	 *			5:当日が休出なら当日日付
	 *			6:翌日が休出なら翌日日付
	 */
	function get_holiday_work($arr_type, $today, $arr_effective_time, $start_time, $previous_day_flag, $end_time, $next_day_flag, $reason) {
		
		$arr_hol_time = array();
		for ($i=0; $i<5; $i++) {
			$arr_hol_time[$i] = 0;
		}

		//稼動時間確認
		if (count($arr_effective_time) == 0) {
			return $arr_hol_time;
		}
		
		$is_today_holiday = ($this->get_you_kinmu_nissu($arr_type["$today"]) == 0);
		//当日が休日以外、事由が休日出勤以外、かつ、前日フラグなし、翌日フラグなし
		if ($is_today_holiday == false && $reason != "16" &&
			$previous_day_flag == false && $next_day_flag == false) {
			return $arr_hol_time;
		}
		//事由が振替休日の場合を除く 20100602
		if ($reason == "15") {
			return $arr_hol_time;
		}

		$prev_date = last_date($today);
		$next_date = next_date($today);
		//前日フラグあり、かつ、前日が休日
		if ($previous_day_flag == true &&
			$this->get_you_kinmu_nissu($arr_type["$prev_date"]) == 0) {
			$start_date_time = $prev_date."0000";
			$end_date_time = $today."0000";
			//時間帯データを配列に格納
			$arr_prevday_time = $this->get_minute_time_array($start_date_time, $end_date_time);
			//前日分稼動時間
			$arr_hol_time[0] = count(array_intersect($arr_effective_time, $arr_prevday_time));
		}
		//当日が休日、または、事由が休日出勤
		if ($is_today_holiday == true || $reason == "16") {
			$start_date_time = $today."0000";
			$end_date_time = $next_date."0000";
			//時間帯データを配列に格納
			$arr_today_time = $this->get_minute_time_array($start_date_time, $end_date_time);
			//当日分稼動時間
			$arr_hol_time[1] = count(array_intersect($arr_effective_time, $arr_today_time));
		}
		//翌日フラグあり、かつ、翌日が休日
		if ($next_day_flag == true &&
			$this->get_you_kinmu_nissu($arr_type["$next_date"]) == 0) {
			$next2_date = next_date($next_date);
			$start_date_time = $next_date."0000";
			$end_date_time = $next2_date."0000";
			//時間帯データを配列に格納
			$arr_nextday_time = $this->get_minute_time_array($start_date_time, $end_date_time);
			//翌日分稼動時間
			$arr_hol_time[2] = count(array_intersect($arr_effective_time, $arr_nextday_time));
		}
		//合計
		for ($i=0; $i<3; $i++) {
			$arr_hol_time[3] += $arr_hol_time[$i];
		}
		//休出
		$tmp_date = $prev_date;
		for ($i=0; $i<3; $i++) {
			if ($arr_hol_time[$i] > 0) {
				$arr_hol_time[$i+4] = $tmp_date;
			} else {
				$arr_hol_time[$i+4] = "";
			}
			$tmp_date = next_date($tmp_date);
		}
		
		return $arr_hol_time;
	}

	/**
	 * 休日勤務時間（分）と休日出勤した日を配列にして返す
	 *  時間帯配列を使用しない
	 * @param  $arr_type  属性
	 * @param  $today     日付
	 * @param  $start_date_time 開始日時
	 * @param  $previous_day_flag 前日フラグ
	 * @param  $end_date_time  終了日時
	 * @param  $next_day_flag  翌日フラグ
	 * @param  $reason 事由
	 * @param  $work_times_info 時間情報
	 * @param  $out_time 外出時刻
	 * @param  $ret_time 戻り時刻
	 * @return $arr_hol_time  配列 0:前日 1:当日 2:翌日 3:合計 
	 *			4:前日が休出なら前日日付
	 *			5:当日が休出なら当日日付
	 *			6:翌日が休出なら翌日日付
	 */
	function get_holiday_work2($arr_type, $today, $start_date_time, $previous_day_flag, $end_date_time, $next_day_flag, $reason, $work_times_info, $out_time, $ret_time) {
		
		$arr_hol_time = array();
		for ($i=0; $i<5; $i++) {
			$arr_hol_time[$i] = 0;
		}
		
		//稼動時間確認
		$wk_time = date_utils::get_diff_minute($end_date_time, $start_date_time);
		if ($wk_time == 0) {
			return $arr_hol_time;
		}
		
		$is_today_holiday = ($this->get_you_kinmu_nissu($arr_type["$today"]) == 0);
		//当日が休日以外、事由が休日出勤以外、かつ、前日フラグなし、翌日フラグなし
		if ($is_today_holiday == false && $reason != "16" &&
				$previous_day_flag == false && $next_day_flag == false) {
			return $arr_hol_time;
		}
		//事由が振替休日の場合を除く 20100602
		if ($reason == "15") {
			return $arr_hol_time;
		}
		
		$wk_rest_time = 0;
		$wk_out_time = 0;
		$prev_date = last_date($today);
		$next_date = next_date($today);
		//前日フラグあり、かつ、前日が休日
		if ($previous_day_flag == true &&
				$this->get_you_kinmu_nissu($arr_type["$prev_date"]) == 0) {
			//前日分稼動時間
			$wk_prev_time = $this->get_intersect_times($start_date_time, $end_date_time, $work_times_info[0]["date_start"], $work_times_info[0]["date_end"]);
			//休憩時間確認
			$wk_rest_time = $this->get_intersect_times($start_date_time, $end_date_time, $work_times_info[0]["rest_start"], $work_times_info[0]["rest_end"]);
			//外出時間確認
			if ($wk_prev_time > 0) {
				$wk_out_time = $this->get_intersect_times($start_date_time, $end_date_time, $prev_date.$out_time, $prev_date.$ret_time);
			}
			$arr_hol_time[0] = $wk_prev_time - $wk_rest_time - $wk_out_time;
			
		}
		//当日が休日、または、事由が休日出勤
		if ($is_today_holiday == true || $reason == "16") {
			//当日分稼動時間
			$wk_today_time = $this->get_intersect_times($start_date_time, $end_date_time, $work_times_info[1]["date_start"], $work_times_info[1]["date_end"]);
			//休憩時間確認 24時間以上の時の休憩を減算しないため
			if ($wk_rest_time == 0) {
				$wk_rest_time = $this->get_intersect_times($start_date_time, $end_date_time, $work_times_info[1]["rest_start"], $work_times_info[1]["rest_end"]);
				$wk_today_time -= $wk_rest_time;
			}
			//外出時間確認
			if ($wk_today_time > 0 && $wk_out_time == 0) {
				$wk_out_time = $this->get_intersect_times($start_date_time, $end_date_time, $today.$out_time, $today.$ret_time);
				$wk_today_time -= $wk_out_time;
			}
			$arr_hol_time[1] = $wk_today_time;
		}
		//翌日フラグあり、かつ、翌日が休日
		if ($next_day_flag == true &&
				$this->get_you_kinmu_nissu($arr_type["$next_date"]) == 0) {
			//翌日分稼動時間
			$wk_nextday_time = $this->get_intersect_times($start_date_time, $end_date_time, $work_times_info[2]["date_start"], $work_times_info[2]["date_end"]);
			//休憩時間確認
			if ($wk_rest_time == 0) {
				$wk_rest_time = $this->get_intersect_times($start_date_time, $end_date_time, $work_times_info[2]["rest_start"], $work_times_info[2]["rest_end"]);
				$wk_nextday_time -= $wk_rest_time;
			}
			//外出時間確認
			if ($wk_nextday_time > 0 && $wk_out_time == 0) {
				$wk_out_time = $this->get_intersect_times($start_date_time, $end_date_time, $next_date.$out_time, $next_date.$ret_time);
				$wk_nextday_time -= $wk_out_time;
			}
			$arr_hol_time[2] = $wk_nextday_time;
		}
		//合計
		for ($i=0; $i<3; $i++) {
			$arr_hol_time[3] += $arr_hol_time[$i];
		}
		//休出
		$tmp_date = $prev_date;
		for ($i=0; $i<3; $i++) {
			if ($arr_hol_time[$i] > 0) {
				$arr_hol_time[$i+4] = $tmp_date;
			} else {
				$arr_hol_time[$i+4] = "";
			}
			$tmp_date = next_date($tmp_date);
		}
		
		return $arr_hol_time;
	}
	
	/**
	 * 翌日以降の休暇を実績へ登録
	 * @param  $duty_date  当日YYYYMMDD形式
	 * @param  $emp_id     職員ID
	 */
	function next_hol_set($duty_date, $emp_id) {

		$tmp_date = $duty_date;

		while (1) {
			$tmp_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($tmp_date)));
			//翌日予定の勤務パターンを確認
			//勤務日数換算が0日の場合を条件追加 20091201
			$sql =	"SELECT ".
						"a.pattern, a.reason, a.night_duty, a.tmcd_group_id, a.allow_id, b.pattern as rslt_pattern, b.date as rslt_date, c.workday_count, c.after_night_duty_flag ".
					"FROM ".
						"atdbk a ".
						"LEFT JOIN atdbkrslt b ON (a.emp_id = b.emp_id and a.date = b.date) ".
						"LEFT JOIN atdptn c ON a.tmcd_group_id = c.group_id AND a.pattern = CAST(c.atdptn_id AS varchar) ";
				$cond = "WHERE a.emp_id = '$emp_id' and a.date = '$tmp_date'";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			if (pg_num_rows($sel) > 0) {
				$pattern       = pg_fetch_result($sel, 0, "pattern");
				$rslt_pattern  = pg_fetch_result($sel, 0, "rslt_pattern");
				$rslt_date     = pg_fetch_result($sel, 0, "rslt_date");
				$workday_count = pg_fetch_result($sel, 0, "workday_count");
				$after_night_duty_flag = pg_fetch_result($sel, 0, "after_night_duty_flag");
			} else {
				break;
			}
			//（予定が休暇か勤務日数換算が0日）の場合で実績が未設定の場合、実績を更新
			if (($pattern == "10" || $workday_count == 0 || $after_night_duty_flag == 1) && $rslt_pattern == "") {
				$reason       = pg_fetch_result($sel, 0, "reason");
				$night_duty       = pg_fetch_result($sel, 0, "night_duty");
				$tmcd_group_id       = pg_fetch_result($sel, 0, "tmcd_group_id");
				$allow_id       = pg_fetch_result($sel, 0, "allow_id");
				//実績レコードがない場合、登録
				if ($rslt_date == "") {
					
					$sql = "insert into atdbkrslt (emp_id, date, pattern, reason,night_duty, allow_id, tmcd_group_id ";
					//, reg_prg_flg, "1"を除く 20100406 打刻時以外は、出勤表側、勤務シフト側のどちらからも更新可とする
					$sql .= ") values (";

					$content = array($emp_id, $tmp_date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id);
					$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
					if ($ins == 0) {
						pg_query($this->_db_con, "rollback");
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				} else {
				//更新
					$sql = "update atdbkrslt set";
					$set = array("pattern", "reason", "night_duty", "tmcd_group_id", "allow_id");
					//, "reg_prg_flg", "1"を除く 20100406 打刻時以外は、出勤表側、勤務シフト側のどちらからも更新可とする
					$setvalue = array($pattern, $reason, $night_duty, $tmcd_group_id, $allow_id);
					$cond = "WHERE emp_id = '$emp_id' and date = '$tmp_date'";

					$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0) {
						pg_query($this->_db_con, "rollback");
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			} else {
			//休暇以外になるまで繰り返す
				break;
			}

		}

	}
	/**
	 * 翌日の勤務開始時刻が前日であるフラグを取得
	 * @param  $emp_id     職員ID
     * @return $previous_day_flag 1:true "":false
	 */
	function get_previous_day_flag($emp_id) {

		$previous_day_flag = "";

		//翌日開始時刻確認
		$office_start_time = "";
		$tmp_type = "";
		$tmp_group_id = "";
		$tmp_pattern = "";

        $next_day           = date("Ymd", strtotime("1 day"));
		$arr_next_day_rslt = $this->get_atdbkrslt_array($this->_db_con, $next_day, $emp_id, $this->file_name);
		
		//実績から取得
		if ($arr_next_day_rslt["pattern"] != "") {
			$previous_day_flag = $arr_next_day_rslt["previous_day_flag"];
			$tmp_type = $arr_next_day_rslt["type"];
			$tmp_group_id = $arr_next_day_rslt["tmcd_group_id"];
			$tmp_pattern = $arr_next_day_rslt["pattern"];
		} else {
		//予定から取得
			$arr_next_day = $this->get_atdbk_array($this->_db_con, $next_day, $emp_id, $this->file_name);
			$previous_day_flag = $arr_next_day["previous_day_flag"];
			$tmp_type = $arr_next_day["type"];
			$tmp_group_id = $arr_next_day["tmcd_group_id"];
			$tmp_pattern = $arr_next_day["pattern"];
		}
		if ($tmp_pattern != "") {
			$tmp_type = ($tmp_type != "2" && $tmp_type != "3") ? "1" : $tmp_type;
			$office_start_time = $this->get_officehours_info( $tmp_group_id, $tmp_pattern, $tmp_type, "start2" );
			//システム時刻が翌日シフト開始の２時間以前の場合は当日とする
			if (date("H:i", strtotime("2 hours")) <= $office_start_time) {
				return "";
			}
		}

		return $previous_day_flag;
	}

	/**
	 * 残業時間計算用に残業申請中等の未承認か承認済みかどうかの状態を返す
	 *
	 * @param $overtime_link_type 残業申請リンクタイプ
	 * @param $modify_flg 本人による修正 "t":可 "f":不可
	 * @param $modify_link_type 修正申請リンクタイプ
	 * @return 0:不要 1:未承認 2:承認済
	 */
	function get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type) {
		$ret_flg = "0";

		//未承認となる条件
		// 残業申請リンクタイプ 1:要申請（未申請） 2:申請中 3:承認済 4:否認済 5:差戻済
		// 修正申請(本人による修正不可　更新リンクタイプ 2:申請中 4:否認済 5:差戻済
		if ($overtime_link_type == "1" ||
			$overtime_link_type == "2" ||
			$overtime_link_type == "4" ||
			$overtime_link_type == "5" ||
			$overtime_link_type == "6" ||
			(($overtime_link_type == "0" || $overtime_link_type == "3") && 
			 ($modify_flg == "f" && 
			  ($modify_link_type == "2" ||
			   $modify_link_type == "4" ||
			   $modify_link_type == "5")))) {
			$ret_flg = "1";
		} else if ($overtime_link_type == "0") {
			$ret_flg = "0";
		} else {
			$ret_flg = "2";
		}
		return $ret_flg;
	}
    
	/**
	 * This is function minute_to_h_hh
	 * 分をh.hh形式に変換（400→6.67）
	 * @param mixed $minute 分
	 * @return h.hh形式の時間、小数点以下は時間換算表示 
	 *
	 */
	function minute_to_h_hh($minutes) {
		$wk_hmm = sprintf("%1.2f", $minutes / 60);
		$arr_hmm = split("\.", $wk_hmm);
		if ($arr_hmm[1] == "00") {
			return $arr_hmm[0];
		}
		if (substr($arr_hmm[1], 1, 1) == "0") {
			$wk_mm = substr($arr_hmm[1], 0, 1);
		} else {
			$wk_mm = $arr_hmm[1];
		}
		$wk = $arr_hmm[0].".".$wk_mm;
		return $wk;
		
	}

    
    /**
     * This is method minute_to_hmm
     * 分をH:MM形式に変換（400→6:40）
     * @param mixed $minutes This is a description
     * @return mixed This is the return value description
     *
     */
    function minute_to_hmm($minutes) {
        if ($minutes == 0) {
            return "";
        }
        //分がプラスの場合
        if ($minutes >= 0) {
            $hour = floor($minutes / 60);
        } else {
            //分がマイナスの場合
            $hour = ceil($minutes / 60);
        }
        $min = $minutes - ($hour * 60);
        //時分変換後の分がマイナスの場合、プラスにする
        if ($min < 0) {
            $min = $min * (-1);
        }
        return "$hour:" . sprintf("%02d", $min);
    }
    
    /**
     * This is method minute_to_time_format
     * 時間形式に合う文字列に変換する
     * @param mixed $minutes 分
     * @param mixed $time_format 時間形式 1,"":"hh:mm" 2:"hh.hh" 3:"hh.mm"
     * @return mixed 変換後の時間
     *
     */
    function minute_to_time_format($minutes, $time_format) {
        switch ($time_format) {
            case "":
            case "1":
                if ($minutes == 0) {
                    $str = "0:00";
                }
                else {
                    $str = $this->minute_to_hmm($minutes);
                }
                break;
            case "2":
                if ($minutes == 0) {
                    $str = "0";
                }
                else {
                    $str = $this->minute_to_h_hh($minutes);
                }
                break;
            case "3":
                if ($minutes == 0) {
                    $str = "0.00";
                }
                else {
                    $str = $this->minute_to_hmm($minutes);
                    $str = str_replace(":", ".", $str);
                }
                break;
            default :
                $str = "";
                break;
        }
        return $str;
    }
    
    /**
	 * 残業時刻のデフォルト取得
	 *
	 * @param string $start_time 出勤時刻
	 * @param string $end_time 退勤時刻
	 * @param string $next_day_flag 翌日フラグ
	 * @param string $tmcd_group_id 出勤パターングループID
	 * @param string $pattern 出勤パターン
	 * @param string $type カレンダーの種別
	 * @param string $previous_day_flag 前日フラグ
     * @param string $emp_office_end_time 個人別勤務時間の終了 20130220
	 * @return array 残業時刻のデフォルトデータの配列
	 *
	 */
    function get_default_overtime($start_time, $end_time, $next_day_flag, $tmcd_group_id, $pattern, $type, $previous_day_flag, $emp_office_end_time) {

		global $timecard_array;
		//カレンダーの種別
		$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
		//予定の開始時刻
		$start2   = $this->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
		//予定の終了時刻
        if ($emp_office_end_time != "") { //20130220
            $end2 = $emp_office_end_time;
        }
        else {
            $end2   = $this->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
        }
		$office_end_time = substr($end2, 0, 2).substr($end2, 3, 2);
		//24時間以上勤務の場合 20100203
		$over_24hour_flag = $this->get_over_24hour_flag($tmcd_group_id, $pattern);
		if ($over_24hour_flag == "1") {
			$office_end_date_time = date("Ymd", strtotime("1 day")).$office_end_time;
		} else {
			$office_end_date_time = date("Ymd").$office_end_time;
		}

		//残業時間
		//退勤時刻が翌日の場合
		if ($next_day_flag == "1") {
			$wk_date = date("Ymd", strtotime("1 day"));
		} else {
			$wk_date = date("Ymd");
		}
		$wk_end_date_time = $wk_date.$end_time;
		
		$over_time_minute = date_utils::get_diff_minute($wk_end_date_time, $office_end_date_time);
		
		//デフォルト設定する判断用基準分
		//残業申請画面を表示する分
		if ($timecard_array["over_time_apply_type"] == "1") {
			$std_min = $timecard_array["ovtmscr_tm"];
		}
		//退勤時刻を切り上げする端数
		elseif ($timecard_array["fraction2"] == "3") {
			$std_min = $this->get_moving_minutes($timecard_array["fraction2_min"]);
		}
		//上記以外
		else {
			$std_min = 0;
		}

		$data = array();		
		if ($over_time_minute > $std_min) {
			//開始
			$data["over_start_time"] = $office_end_time;
			$data["over_start_hour"] = substr($office_end_time, 0, 2);
			$data["over_start_min"] = substr($office_end_time, 2, 2);
			
			//終了
			//退勤時刻を切り上げ、切り下げ
			if ($timecard_array["fraction2"] >= "2") {
				$moving_minutes = $this->get_moving_minutes($timecard_array["fraction2_min"]);
				$fixed_end_time = date_utils::move_time(date("Ymd").$office_end_time, $wk_date.$end_time, $moving_minutes, $timecard_array["fraction2"]);
			} else {
				$fixed_end_time = $wk_date.$end_time; //端数処理がない場合、時刻が設定されない不具合対応 20131125
			}
			$data["over_end_time"] = substr($fixed_end_time, 8, 4);
			$data["over_end_hour"] = substr($fixed_end_time, 8, 2);
			$data["over_end_min"] = substr($fixed_end_time, 10, 2);
			
			//翌日フラグ設定、前日フラグなしで日をまたがっているか判断
			if (($start2 > $end2 && $previous_day_flag == 0) ||
					$over_24hour_flag == "1") {
				$data["over_start_next_day_flag"] = 1;
			} else {
				$data["over_start_next_day_flag"] = 0;
			}
			if ($next_day_flag == "1" || ($start_time > $end_time && $previous_day_flag == 0) ||
					$over_24hour_flag == "1") {
				$data["over_end_next_day_flag"] = 1;
			} else {
				$data["over_end_next_day_flag"] = 0;
			}
			
		}

		return $data;
	}
	
	
	/**
	 * 24時間以上勤務フラグ取得
	 *
	 * @param mixed $tmcd_group_id 勤務シフトグループID
	 * @param mixed $pattern 勤務パターンID
	 * @return mixed '1':ON  '':OFF
	 *
	 */
	function get_over_24hour_flag($tmcd_group_id, $pattern) {
		
		$over_24hour_flag = "";
		if ($tmcd_group_id == "" || $pattern == "") {
			return $over_24hour_flag;
		}
		
		$sql =	"SELECT over_24hour_flag FROM atdptn ";
		$cond = "WHERE group_id = $tmcd_group_id and atdptn_id = $pattern";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$over_24hour_flag       = pg_fetch_result($sel, 0, "over_24hour_flag");
		}
		
		return $over_24hour_flag;
	
	}
	
	/**
	 * 日曜か祝日かを確認する
	 *
	 * @param mixed $date 対象日付
	 * @return true:日曜祝日 false:それ以外
	 *
	 */
	function check_sunday_or_holiday($date) {
		// 処理日付の曜日を取得
        $tmp_weekday = date("w", date_utils::to_timestamp_from_ymd($date)); //to_timestamp関数変更 20130219
        if ($tmp_weekday == "0") {
			return true;
		} else {
			$holiday_name = get_holiday_name($date);
			if ($holiday_name != "") {
				return true;
			}
		}
		return false;
	}

//年休残取得
	function get_nenkyu_zan($year, $month, $emp_id, $closing_paid_holiday, $start_date, $end_date) {
		
		global $timecard_array;

		$criteria_months = $timecard_array["criteria_months"];
		$wk_month = ($criteria_months == "1") ? "3" : "6";
		
		$sql = "select empmst.emp_id, emp_lt_nm, emp_ft_nm, emppaid.days1, emppaid.days2, emppaid.adjust_day ";
		$sql .= " , g.days1 as prev_days1 ,g.days2 as prev_days2, g.adjust_day as prev_adjust_day, emp_join, to_char(to_timestamp(case when emppaid.paid_hol_start_date is not null and  emppaid.paid_hol_start_date != '' then emppaid.paid_hol_start_date when emppaid.paid_hol_emp_join is not null and emppaid.paid_hol_emp_join != '' then emppaid.paid_hol_emp_join when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '$wk_month months', 'yyyymmdd') as paid_hol_add_date, to_char(to_timestamp(case when emppaid.paid_hol_start_date is not null and  emppaid.paid_hol_start_date != '' then emppaid.paid_hol_start_date when emppaid.paid_hol_emp_join is not null and emppaid.paid_hol_emp_join != '' then emppaid.paid_hol_emp_join when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '12 months', 'yyyymmdd') as year_after_join, h.curr_use, h.data_migration_date, h.last_remain, h.curr_carry, h.curr_add, h.last_add_date, emppaid.year as emppaid_year, emppaid.paid_hol_add_mmdd ";
		$sql .= " from empmst ";
		//年範囲調整 20110208
		if ($closing_paid_holiday != "3") { // 1:4/1, 2:1/1
			$sql .= " left join emppaid on emppaid.emp_id = empmst.emp_id and emppaid.year = '$year' ";
		}
		//3:採用日
		else {
			$wk_yr = substr($start_date, 0, 4);
			$start_ymd = ($wk_yr-1).substr($start_date, 4, 4);
			$end_ymd = $end_date;
			//2件有り得るので1件に絞り込む
			$sql .= " left join (select * from emppaid where emppaid.emp_id = '$emp_id' and  ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd >= '$start_ymd' and ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd <= '$end_ymd' ";
			$sql .= " order by emppaid.year desc, emppaid.paid_hol_add_mmdd desc limit 1) emppaid on emppaid.emp_id = empmst.emp_id ";
		}
		$sql .= " left join empcond f on f.emp_id = empmst.emp_id ";
		if ($closing_paid_holiday != "3") { // 1:4/1, 2:1/1
            $wk_year = $year - 1;
            $sql .= " left join emppaid g on g.emp_id = empmst.emp_id and g.year = '$wk_year' ";
		}
		//3:採用日
		else {
			$wk_yr = substr($start_date, 0, 4);
			$start_ymd = ($wk_yr-2).substr($start_date, 4, 4);
			$wk_yr = substr($end_date, 0, 4);
			$end_ymd = ($wk_yr-1).substr($end_date, 4, 4);
			//2件有り得るので1件に絞り込む
			$sql .= " left join (select * from emppaid where emppaid.emp_id = '$emp_id' and  ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd >= '$start_ymd' and ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd <= '$end_ymd' ";
			$sql .= " order by emppaid.year desc, emppaid.paid_hol_add_mmdd desc limit 1) g on g.emp_id = empmst.emp_id ";
		}
		$sql .= " left join timecard_paid_hol_import h on h.emp_personal_id = empmst.emp_personal_id ";
		$cond = "where empmst.emp_id = '$emp_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		$tmp_days3 = 0;
		$num = pg_numrows($sel);
		if ($num>0) {
			$emppaid_year = pg_fetch_result($sel, 0, "emppaid_year");//直近付与データの年
			$paid_hol_add_mmdd = pg_fetch_result($sel, 0, "paid_hol_add_mmdd");//直近付与データの月日
			$tmp_days1 = pg_fetch_result($sel, 0, "days1"); //繰越
			$tmp_days2 = pg_fetch_result($sel, 0, "days2");
			$tmp_adjust_day = pg_fetch_result($sel, 0, "adjust_day");
			$tmp_emp_join = pg_fetch_result($sel, 0, "emp_join");
			$tmp_prev_days1 = pg_fetch_result($sel, 0, "prev_days1"); //前年分
			$tmp_prev_days2 = pg_fetch_result($sel, 0, "prev_days2");
			$tmp_prev_adjust_day = pg_fetch_result($sel, 0, "prev_adjust_day");
//移行データ
			$data_migration_date = pg_fetch_result($sel,0,"data_migration_date");
			$curr_use = pg_fetch_result($sel,0,"curr_use");
			$last_remain = pg_fetch_result($sel,0,"last_remain");
			$curr_carry = pg_fetch_result($sel,0,"curr_carry");
			$curr_add = pg_fetch_result($sel,0,"curr_add");
			$last_add_date = pg_fetch_result($sel,0,"last_add_date");
			//移行データの前回付与日
			$import_last_add_date = "";
			if ($last_add_date != "") {
				$wk_ymd = split("/", $last_add_date);
				$import_last_add_date = $wk_ymd[0].sprintf("%02d%02d", $wk_ymd[1], $wk_ymd[2]);
			}
			
		}
		//付与開始日
		//付与データが有る場合、年休使用数を求める開始日に設定 20110422
		if ($emppaid_year != "" && $paid_hol_add_mmdd != "") {
			$fuyo_start_date = $emppaid_year.$paid_hol_add_mmdd;
			$chk_ymd1 = $fuyo_start_date; //20110810
		}
		else {
			switch ($closing_paid_holiday) {
				case "1":
					//年度調整
					$wk_year = ($month < "04") ? $year - 1 : $year;
					$fuyo_start_date = $wk_year."0401";
					$start_mm = 4;
					break;
				case "2":
					$fuyo_start_date = $year."0101";
					$wk_year = $year;
					$start_mm = 1;
					break;
				case "3":
					
					//6ヶ月基準
					if ($criteria_months == "2" ) {
						$paid_hol_add_date = pg_fetch_result($sel, 0, "paid_hol_add_date");
					}
					//3ヶ月基準
					else {
						$today = date("Ymd");
						$year_after_join = pg_fetch_result($sel, 0, "year_after_join");
						//入職後1年以上か確認
						if (substr($year_after_join, 0, 4) <= $year) {
							$paid_hol_add_date = $year_after_join;
						}
						else {
							$paid_hol_add_date = pg_fetch_result($sel, 0, "paid_hol_add_date");
						}
					}
					
					$wk_mon = substr($paid_hol_add_date, 4, 2);
					$start_mm = intval($wk_mon);
					//年度調整 締め日？付与日　例1001 0401 1001
					$wk_fuyo_mmdd = substr($paid_hol_add_date, 4, 4);
					$wk_start_mmdd = substr($start_date, 4, 4);
					$wk_end_mmdd = substr($end_date, 4, 4);
					$wk_year = ($wk_end_mmdd < $wk_fuyo_mmdd) ? $year - 1 : $year;
					$fuyo_start_date = $wk_year.substr($paid_hol_add_date, 4, 4); //年度
					break;
			}
			$chk_ymd1 = $wk_year.sprintf("%02d",$start_mm)."01"; //20110810
		}
		
		//処理年月範囲確認
		$chk_ymd1_timestamp = date_utils::to_timestamp_from_ymd($chk_ymd1);
		$chk_ymd2 =  date("Ymd", strtotime("+12 month", $chk_ymd1_timestamp));

		//移行データがある場合、移行データの繰越、付与から年休残を計算
		//付与データがないことを確認 20120710
		if ($tmp_days2 == "" &&
			$chk_ymd1 <= $import_last_add_date &&
				$import_last_add_date < $chk_ymd2) {
			$tmp_days3 = $curr_carry + $curr_add;
		} else {
			//年度に応じた年休残を計算
			if (($closing_paid_holiday == "1" && $month < "04") ) {
				$tmp_days3 = $tmp_prev_days1 + $tmp_prev_days2 + $tmp_prev_adjust_day;
			} else {
				$tmp_days3 = $tmp_days1 + $tmp_days2 + $tmp_adjust_day;
			}
		}
		
		//年休使用数取得
		//移行データがある場合、年月確認、移行データの使用数、移行日後の使用数
		if ($data_migration_date != "") {

			if ($chk_ymd1 <= $data_migration_date && $data_migration_date < $chk_ymd2) {
				if ($data_migration_date <= $end_date) {
					//20110414 移行日の翌日からとする
                    $tmp_data_migration_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($data_migration_date))); //to_timestamp関数変更 20130219
                    $nenkyu_siyo = $this->get_nenkyu_siyo($emp_id, $tmp_data_migration_date, $end_date);
					$nenkyu_siyo += $curr_use;
				} else {
					$nenkyu_siyo = 0;
				}
			}
			else {
				$nenkyu_siyo = $this->get_nenkyu_siyo($emp_id, $fuyo_start_date, $end_date);
			}
		}
		else {
			$nenkyu_siyo = $this->get_nenkyu_siyo($emp_id, $fuyo_start_date, $end_date);
		}
		
		return $tmp_days3 - $nenkyu_siyo;
	}
	
	//年休使用数取得
	function get_nenkyu_siyo($emp_id, $start_date, $end_date) {
		//付与開始日
		//データ取得 reason:44半有半公は0.5日とする 55半夏半有 57半有半欠 58半特半有 62半正半有 2午前有休 3午後有休 38午前年休 39午後年休
		$sql = " select atdbkrslt.emp_id,sum(case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end) as cnt from atdbkrslt ";
		$cond = " where (emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date') ";
        $cond .= " and ((reason='2' or reason='3' or reason='38' or reason ='39' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (pattern = '10' and reason='1') or (pattern = '10' and reason='37')) ";
		$cond .= " group by atdbkrslt.emp_id ";
		
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_result($sel,0,"cnt");
		
		return $cnt;

		//※月のデータを表示する場合、モードを用意。年間、月別配列
	}
	
	//残業時刻の申請単位取得
	function get_fraction_over_unit() {
		global $timecard_array;

		return $timecard_array["fraction_over_unit"];
	}

	/**
	 * 2つの時間帯の重複する時間を取得する
	 *
	 * @param mixed $start1 開始時刻１
	 * @param mixed $end1 終了時刻１
	 * @param mixed $start2 開始時刻２
	 * @param mixed $end2 終了時刻２
	 * @return mixed 分
	 *
	 */
	function get_intersect_times($start1, $end1, $start2, $end2) {
		
		//時間にコロン(:)がある場合消す
		$s1 = str_replace(":", "", $start1);
		$e1 = str_replace(":", "", $end1);
		$s2 = str_replace(":", "", $start2);
		$e2 = str_replace(":", "", $end2);
		
		//入力チェック
		//開始終了時刻が逆の場合、0とする
		if ($s1 >= $e1 || $s2 >= $e2) {
			return 0;
		}
		//範囲外
		//      s1----e1
		//s2--e2      s2--e2
		if ($e2 <= $s1 || $s2 >= $e1) {
			return 0;
		}
		
		//     s1----e1  s1----e1
		//    s2--e2      s2--e2
		$wk_s = max($s1, $s2);
		$wk_e = min($e1, $e2);
		
		$minutes = date_utils::get_diff_minute($wk_e, $wk_s);
		
		return $minutes;
		
	}
	/**
	 * 2つの時間帯で重複分を除外した時間を取得する
	 *
	 * @param mixed $start1 開始時刻１
	 * @param mixed $end1 終了時刻１
	 * @param mixed $start2 開始時刻２
	 * @param mixed $end2 終了時刻２
	 * @return mixed 分
	 *
	 */
	function get_diff_times($start1, $end1, $start2, $end2) {
		
		//時間にコロン(:)がある場合消す
		$s1 = str_replace(":", "", $start1);
		$e1 = str_replace(":", "", $end1);
		$s2 = str_replace(":", "", $start2);
		$e2 = str_replace(":", "", $end2);
		
		//入力チェック
		//開始時刻２か終了時刻２が空白の場合
		if ($s2 == "" || $e2 == "") {
			$minutes = date_utils::get_diff_minute($e1, $s1);
			return $minutes;
		}
		
		//開始終了時刻が逆の場合、0とする
		if ($s1 > $e1 || $s2 > $e2) { // ">=" -> ">" 同じ場合は0を返さず処理続行する 20131021
			return 0;
		}
		//範囲外
		//      s1----e1
		//s2--e2      s2--e2
		if ($e2 <= $s1 || $s2 >= $e1) {
			$minutes = date_utils::get_diff_minute($e1, $s1);
			return $minutes;
		}
		//  s1----e1  全体除外
		// s2------e2
		if ($s2 <= $s1 && $e2 >= $e1) {
			return 0;
		}
		//  s1----e1  前方除外
		// s2--e2   
		if ($s2 <= $s1 && $e2 > $s1 && $e2 < $e1) {
			$minutes = date_utils::get_diff_minute($e1, $e2);
			return $minutes;
		}
		//  s1----e1  後方除外
		//     s2--e2 
		if ($e2 >= $e1 && $s2 < $e1 && $s2 > $s1) {
			$minutes = date_utils::get_diff_minute($s2, $s1);
			return $minutes;
		}
		//  s1----e1  中央除外
		//   s2--e2 
		if ($s2 > $s1 && $e2 < $e1) {
			$minutes = date_utils::get_diff_minute($s2, $s1);
			$minutes += date_utils::get_diff_minute($e1, $e2);
			return $minutes;
		}
		
	}
	
	/**
	 * 複数時間情報と１時間帯の重複時間を取得する
	 *
	 * @param mixed $times_info 時間情報
	 * @param mixed $start_time 開始日時
	 * @param mixed $end_time 終了日時
	 * @return mixed 時間（分）
	 *
	 */
	function get_times_info_calc($times_info, $start_time, $end_time) {
		
		$ret_time = 0;
		$cnt = $times_info["data_cnt"];
		for ($i=0; $i<$cnt; $i++) {
			$ret_time += $this->get_intersect_times($times_info[$i]["start_time"], $times_info[$i]["end_time"], $start_time, $end_time);
		}
		
		return $ret_time; 
	}


	/**
	 * 時間情報を取得する
	 * 　休日勤務を計算するため、各時間帯を保持する
	 * 日と開始終了時刻の2次元配列
	[$i] 0:前日 1:当日 2:翌日
	前日当日翌日の日時
	　date_start
	　date_end
　　休憩
	　rest_start
	　rest_end
	日勤（深夜以外）
	　day_start
	　day_end
	深夜
	0:前々日 1:前日 2:当日 3:翌日、日またがりのため他より1回多い
	　night_start
	　night_end
	
	"night_data_cnt":深夜情報の件数
	"day_data_cnt":日勤情報の件数
	 *
	 * @param mixed $today This is a description
	 * @param mixed $start4 This is a description
	 * @param mixed $end4 This is a description
	 * @param mixed $start5 This is a description
	 * @param mixed $end5 This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function get_work_times_info($today, $start4, $end4,	$start5, $end5) {
		
		$times_info = array();
		//日
		$start_timestamp = date_utils::to_timestamp_from_ymdhi($today."0000");
		
		$times_info[0]["date_start"] = date("YmdHi", strtotime("-1 day", $start_timestamp));
		$times_info[0]["date_end"] = date("YmdHi", $start_timestamp);
		$times_info[1]["date_start"] = date("YmdHi", $start_timestamp);
		$times_info[1]["date_end"] = date("YmdHi", strtotime("+1 day", $start_timestamp));
		$times_info[2]["date_start"] = date("YmdHi", strtotime("+1 day", $start_timestamp));
		$times_info[2]["date_end"] = date("YmdHi", strtotime("+2 day", $start_timestamp));

		//休憩
		if ($start4 != "" && $end4 != "") {
			$start4_timestamp = date_utils::to_timestamp_from_ymdhi($today.$start4);
			$end4_timestamp   = date_utils::to_timestamp_from_ymdhi($today.$end4);
			
			$times_info[0]["rest_start"] = date("YmdHi", strtotime("-1 day", $start4_timestamp));
			$times_info[0]["rest_end"] = date("YmdHi", strtotime("-1 day", $end4_timestamp));
			$times_info[1]["rest_start"] = date("YmdHi", $start4_timestamp);
			$times_info[1]["rest_end"] = date("YmdHi", $end4_timestamp);
			$times_info[2]["rest_start"] = date("YmdHi", strtotime("+1 day", $start4_timestamp));
			$times_info[2]["rest_end"] = date("YmdHi", strtotime("+1 day", $end4_timestamp));
		}
		
		if ($start5 == "") {
			$start5 = "2200";
		}
		if ($end5 == "") {
			$end5 = "0500";
		}
		//深夜
		$start5_timestamp = date_utils::to_timestamp_from_ymdhi($today.$start5);
		$end5_timestamp   = date_utils::to_timestamp_from_ymdhi($today.$end5);
		//翌日から前日にかけてデータ設定
		$start5_timestamp = strtotime("+1 day", $start5_timestamp);
		$end5_timestamp   = strtotime("+1 day", $end5_timestamp);
		$max_cnt = 2;
		//翌日にまたがる 2200-0500
		if ($start5 > $end5) {
			$end5_timestamp  = strtotime("+1 day", $end5_timestamp);
			$max_cnt = 3;
		}
		
		for ($i=$max_cnt; $i>=0; $i--) {
			$times_info[$i]["night_start"] = date("YmdHi", $start5_timestamp);
			$times_info[$i]["night_end"] = date("YmdHi", $end5_timestamp);
			
			$start5_timestamp   =strtotime("-1 day", $start5_timestamp);
			$end5_timestamp   =strtotime("-1 day", $end5_timestamp);
		}
		if ($max_cnt == 2) {
			$times_info[3]["night_start"] = "";
			$times_info[3]["night_end"] = "";
		}
		$times_info["night_data_cnt"] = $max_cnt+1;
		
		//日勤(深夜の逆）0500-2200
		$start6_timestamp = date_utils::to_timestamp_from_ymdhi($today.$end5);
		$end6_timestamp   = date_utils::to_timestamp_from_ymdhi($today.$start5);
		//翌日から前日にかけてデータ設定
		$start6_timestamp = strtotime("+1 day", $start6_timestamp);
		$end6_timestamp   = strtotime("+1 day", $end6_timestamp);
		$max_cnt = 2;
		//翌日にまたがる場合は考慮外。必要がでた場合対応
		//if ($start5 < $end5) {
		//	$end6_timestamp  = strtotime("+1 day", $end6_timestamp);
		//	$max_cnt = 3;
		//}
		
		for ($i=$max_cnt; $i>=0; $i--) {
			$times_info[$i]["day_start"] = date("YmdHi", $start6_timestamp);
			$times_info[$i]["day_end"] = date("YmdHi", $end6_timestamp);
			
			$start6_timestamp   =strtotime("-1 day", $start6_timestamp);
			$end6_timestamp   =strtotime("-1 day", $end6_timestamp);
		}
		$times_info["day_data_cnt"] = $max_cnt+1;
		
		return $times_info;
	}

	/**
	 * 深夜残業時間取得
	 *
	 * @param mixed $work_times_info 時間情報
	 * @param mixed $over_start_date_time 残業開始日時
	 * @param mixed $over_end_date_time 残業終了日時
	 * @param mixed $date_flg 日付フラグ "":全体 0:前日 1:当日 2:翌日
	 * @return mixed 分
	 *
	 */
	function get_late_night($work_times_info, $over_start_date_time, $over_end_date_time, $date_flg="") {
	
		$cnt = $work_times_info["night_data_cnt"];
	
		$wk_time = 0;
		$wk_rest = 0;
		
		if ($date_flg == "") { //全体
			for ($i=0; $i<$cnt; $i++) {
				
				$wk_time += $this->get_intersect_times($work_times_info[$i]["night_start"], $work_times_info[$i]["night_end"], $over_start_date_time, $over_end_date_time);
				
			}
		} elseif ($date_flg == 0) { //前日
			$wk_time += $this->get_intersect_times($work_times_info[0]["date_start"], $work_times_info[0]["night_end"], $over_start_date_time, $over_end_date_time);
			$wk_time += $this->get_intersect_times($work_times_info[1]["night_start"], $work_times_info[0]["date_end"], $over_start_date_time, $over_end_date_time);
			
		} elseif ($date_flg == 1) { //当日
			$wk_time += $this->get_intersect_times($work_times_info[1]["date_start"], $work_times_info[1]["night_end"], $over_start_date_time, $over_end_date_time);
			$wk_time += $this->get_intersect_times($work_times_info[2]["night_start"], $work_times_info[1]["date_end"], $over_start_date_time, $over_end_date_time);
			
		} elseif ($date_flg == 2) { //翌日
			$wk_time += $this->get_intersect_times($work_times_info[2]["date_start"], $work_times_info[2]["night_end"], $over_start_date_time, $over_end_date_time);
			$wk_time += $this->get_intersect_times($work_times_info[3]["night_start"], $work_times_info[2]["date_end"], $over_start_date_time, $over_end_date_time);
			
		}
		
		return $wk_time;
	}

	/**
	 * 深夜時間帯以外の残業時間取得
	 *
	 * @param mixed $work_times_info 時間情報
	 * @param mixed $over_start_date_time 残業開始日時
	 * @param mixed $over_end_date_time 残業終了日時
	 * @param mixed $date_flg 日付フラグ "":全体 0:前日 1:当日 2:翌日
	 * @return mixed 分
	 *
	 */
	function get_day_time($work_times_info, $over_start_date_time, $over_end_date_time, $date_flg="") {
		
		$cnt = $work_times_info["day_data_cnt"];
		
		$wk_time = 0;
		$wk_rest = 0;
		
		for ($i=0; $i<$cnt; $i++) {
			
			if ($date_flg != "" && $date_flg != $i) {
				continue;
			}
			
			$wk_time += $this->get_intersect_times($work_times_info[$i]["day_start"], $work_times_info[$i]["day_end"], $over_start_date_time, $over_end_date_time);
			
		}
		
		return $wk_time;
	}
	
	/**
	 * 休日勤務時間取得、所定時間がある場合は、所定時間内とする
	 *　深夜を除く
	 * @param mixed $work_times_info 時間情報
	 * @param mixed $fixed_start_time 開始日時
	 * @param mixed $fixed_end_time 終了日時
	 * @param mixed $start2_date_time 所定開始日時
	 * @param mixed $end2_date_time 所定終了日時
	 * @param mixed $date_flg 日付フラグ "":全体 0:前日 1:当日 2:翌日
	 * @return mixed 分
	 *
	 */
	function get_hol_work($work_times_info, $fixed_start_time, $fixed_end_time, $start2_date_time, $end2_date_time, $date_flg) {
		
		//実働時刻
		//時間にコロン(:)がある場合消す
		$start_date_time = str_replace(":", "", $fixed_start_time);
		$end_date_time = str_replace(":", "", $fixed_end_time);
		//所定時刻がある場合は重複分とする（所定時間ない場合は実働）
		if ($start2_date_time != "" && $end2_date_time != "") {
			$s2 = str_replace(":", "", $start2_date_time);
			$e2 = str_replace(":", "", $end2_date_time);
			$start_date_time = max($start_date_time, $s2);
			$end_date_time = min($end_date_time, $e2);
		}
		
		$wk_time = 0;
		$wk_rest = 0;

		$wk_time = $this->get_intersect_times($work_times_info[$date_flg]["day_start"], $work_times_info[$date_flg]["day_end"], $start_date_time, $end_date_time);
		
		$first_rest_flg = "";
		
        //休憩が日勤時間帯と重なるか確認
        $wk_start_date_time = max($work_times_info[$date_flg]["day_start"], $start_date_time);
        $wk_end_date_time = min($work_times_info[$date_flg]["day_end"], $end_date_time);
        
		//24時間以上の場合の対応
		for ($i=0; $i<3; $i++) {
			if ($work_times_info[$i]["rest_start"] != "" && $work_times_info[$i]["rest_end"] != "") {
                $wk_rest = $this->get_intersect_times($work_times_info[$i]["rest_start"], $work_times_info[$i]["rest_end"], $wk_start_date_time, $wk_end_date_time);
				if ($wk_rest > 0) {
					$first_rest_flg = $i;
					break;
				}
			}
		}
		
		//休憩がある場合は減算する
		if ($first_rest_flg != "" && $first_rest_flg == $date_flg) {
			$wk_time -= $wk_rest;
		}
		
		return $wk_time;
	}
	
	/**
	 * 明け設定パターン取得
	 *
	 * @return array 明けパターン配列[group_id][atdptn_id]
	 *
	 */
	function get_after_night_duty_pattern() {
		
		$arr_pattern = array();
		
		$sql =	"SELECT group_id, atdptn_id, atdptn_nm  FROM atdptn ";
		$cond = "WHERE after_night_duty_flag = 1";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num =pg_num_rows($sel);
		for ($i=0; $i<$num; $i++) {
			$group_id = pg_fetch_result($sel, $i, "group_id");
			$atdptn_id = pg_fetch_result($sel, $i, "atdptn_id");
			$atdptn_nm = pg_fetch_result($sel, $i, "atdptn_nm");
			$arr_pattern[$group_id][$atdptn_id] = "1";
		}
		
		return $arr_pattern;
		
	}
	
	/**
	 * 非常勤の場合の分割前後の時間取得
	 *
	 * @param mixed $split_time 分割時刻
	 * @param mixed $date 日付
	 * @param mixed $over_start_time2 残業開始時刻
	 * @param mixed $over_start_next_day_flag2 残業開始時刻翌日フラグ
	 * @param mixed $over_end_time2 残業終了時刻
	 * @param mixed $over_end_next_day_flag2 残業終了時刻翌日フラグ
	 * @return mixed 時間の配列 [0]分割前の時間、[1]分割後の時間
	 *
	 */
	function get_parttime_split($split_time, $date, $over_start_time2, $over_start_next_day_flag2, $over_end_time2, $over_end_next_day_flag2
		) {
		
		$ret_time = array();
		
		//日時を求める
		$wk_start_date = ($over_start_next_day_flag2 == "1") ? next_date($date) : $date;
		$start_date_time = $wk_start_date.$over_start_time2;
		$wk_end_date = ($over_end_next_day_flag2 == "1") ? next_date($date) : $date;
		$end_date_time = $wk_end_date.$over_end_time2;
		
		//分割(18:00)あり
		if ($split_time != "") {
			//時間1,18:00までの重なりを取得し集計
			$wk_start_date_time1 = $date."0000";
			$wk_end_date_time1 = $date.$split_time."00";
			$ret_time[0] = $this->get_intersect_times($wk_start_date_time1, $wk_end_date_time1, $start_date_time, $end_date_time);
			//時間2,18:00からの重なりを取得し集計
			$wk_start_date_time2 = $date.$split_time."00";
			$wk_end_date_time2 = next_date($date)."2359";
			$ret_time[1] = $this->get_intersect_times($wk_start_date_time2, $wk_end_date_time2, $start_date_time, $end_date_time);
		}
		else {
			//分割なし
			//時間1へ集計
			$ret_time[0] = date_utils::get_diff_minute($end_date_time, $start_date_time);
			$ret_time[1] = 0;
		}
		//配列で返す
		return $ret_time;
		
	}
	
	
	/**
	 * 日またがりの場合の休日勤務時間を取得する
	 *
	 * @param mixed $date 日付
	 * @param mixed $over_start_time2 残業開始時刻
	 * @param mixed $over_start_next_day_flag2 残業開始時刻翌日フラグ
	 * @param mixed $over_end_time2 残業終了時刻
	 * @param mixed $over_end_next_day_flag2 残業終了時刻翌日フラグ
	 * @param mixed $today_hol_flag 当日休日フラグ
	 * @param mixed $nextday_hol_flag 翌日休日フラグ
	 * @return mixed 時間
	 *
	 */
	function get_holtime_today_nextday($date, $over_start_time2, $over_start_next_day_flag2, $over_end_time2, $over_end_next_day_flag2, $today_hol_flag, $nextday_hol_flag
		) {
		
		$ret_time = 0;
		
		//日時を求める
		$wk_start_date = ($over_start_next_day_flag2 == "1") ? next_date($date) : $date;
		$start_date_time = $wk_start_date.$over_start_time2;
		$wk_end_date = ($over_end_next_day_flag2 == "1") ? next_date($date) : $date;
		$end_date_time = $wk_end_date.$over_end_time2;
		
		//当日
		if ($today_hol_flag != "") {
			//当日の重なりを取得し集計
			$wk_start_date_time1 = $date."0000";
			$wk_end_date_time1 = next_date($date)."0000";
			$ret_time += $this->get_intersect_times($wk_start_date_time1, $wk_end_date_time1, $start_date_time, $end_date_time);
		}
		//翌日
		if ($nextday_hol_flag != "") {
			//翌日の重なりを取得し集計
			$wk_start_date_time1 = next_date($date)."0000";
			$wk_end_date_time1 = next_date($date)."2359";
			$ret_time += $this->get_intersect_times($wk_start_date_time1, $wk_end_date_time1, $start_date_time, $end_date_time);
			
		}

		return $ret_time;
		
	}

	/**
	 * 個人別の所定時間を優先する設定パターン取得
	 *
	 * @return array パターン配列[group_id][atdptn_id]
	 *
	 */
	function get_empcond_officehours_pattern() {
		
		$arr_pattern = array();
		
		$sql =	"SELECT group_id, atdptn_id, atdptn_nm  FROM atdptn ";
		$cond = "WHERE empcond_officehours_flag = 1";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num =pg_num_rows($sel);
		for ($i=0; $i<$num; $i++) {
			$group_id = pg_fetch_result($sel, $i, "group_id");
			$atdptn_id = pg_fetch_result($sel, $i, "atdptn_id");
			$atdptn_nm = pg_fetch_result($sel, $i, "atdptn_nm");
			$arr_pattern[$group_id][$atdptn_id] = "1";
		}
		
		return $arr_pattern;
		
	}
    /**
     * 指定パターンが個人別の所定時間を優先するパターンか確認
     * @param string $group_id 勤務パターングループID
     * @param string $atdptn_id 勤務パターンID
     * @return boolean true:優先する false:優先しない
     *
     */
    function check_empcond_officehours_pattern($group_id, $atdptn_id) {
        
        
        $sql =	"SELECT count(*) as cnt  FROM atdptn ";
        $cond = "WHERE empcond_officehours_flag = 1 and group_id = $group_id and atdptn_id = $atdptn_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $cnt = pg_fetch_result($sel, 0, "cnt");
        
        return ($cnt > 0);
        
    }
    
	/**
     * 曜日情報取得
	 *
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @return mixed 曜日の配列、キーはYYYYMMDD型日付、0:日 6:土 7:祝日
	 *
	 */
	function get_arr_weekday_flag($start_date, $end_date) {
		
		$arr_weekday_flag = array();
		
		$wk_date = $start_date;
		while ($wk_date <= $end_date) {
			$holday_name = get_holiday_name($wk_date);
			$timestamp = date_utils::to_timestamp_from_ymd($wk_date);
			//祝日
			if ($holday_name != "") {
				$arr_weekday_flag[$wk_date] = "7";
			}
			else {
				$arr_weekday_flag[$wk_date] = date("w", $timestamp);
			}
			$wk_date = date("Ymd", strtotime("+1 day", $timestamp));
			
		}
		return $arr_weekday_flag;
	}
	
	/**
     * 個人別勤務時間帯取得
	 *
     * @param string $emp_id 職員ID
     * @param string $start_date 開始日付、履歴取得用、処理する期間の最初の日を設定、省略の場合は履歴を取得しない 20130220
     * @return array 個人別勤務時間帯の配列、曜日別に所定時刻、休憩時刻を設定
	 *
	 */
	function get_arr_empcond_officehours($emp_id, $start_date="") {
		
		$sql = "select * from empcond_officehours";
		$cond = "where emp_id = '$emp_id' order by weekday";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);
		
		$arr_empcond_officehours = array();
		if ($num > 0) {
			
			for ($i=0; $i<$num; $i++) {
				$weekday = pg_fetch_result($sel, $i, "weekday");
				$officehours2_start = pg_fetch_result($sel, $i, "officehours2_start");
				$officehours2_end = pg_fetch_result($sel, $i, "officehours2_end");
				$officehours4_start = pg_fetch_result($sel, $i, "officehours4_start");
				$officehours4_end = pg_fetch_result($sel, $i, "officehours4_end");
				
				$arr_empcond_officehours[$weekday]["officehours2_start"] = $officehours2_start;
				$arr_empcond_officehours[$weekday]["officehours2_end"] = $officehours2_end;
				$arr_empcond_officehours[$weekday]["officehours4_start"] = $officehours4_start;
				$arr_empcond_officehours[$weekday]["officehours4_end"] = $officehours4_end;
				
			}
		}
		$this->empcond_officehours_array = $arr_empcond_officehours;
        //履歴取得 20130220
        $arr_hist = array();
        if ($start_date != "") {
            $wk_histdate = substr($start_date, 0, 4)."-".substr($start_date, 4, 2)."-".substr($start_date, 6, 2)." 00:00:00";
            $sql = "SELECT * FROM empcond_officehours_history ";
            $cond = "WHERE emp_id='$emp_id' and histdate >= '$wk_histdate' ORDER BY histdate ";
            $sel_hrhist = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel_hrhist == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $h_idx = 0;
            while ($r = pg_fetch_assoc($sel_hrhist)) {
                $arr_hist[$h_idx]["histdate"] = $r["histdate"];
                $arr_data = array();
                //基本
                $arr_data[8] = $r;
                //曜日別
                if ($r["sub_rec_cnt"] > 0) {
                    
                    $sql = "select * from empcond_officehours_history_sub";
                    $cond = "where empcond_officehours_history_sub_id={$r["empcond_officehours_history_id"]}";
                    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                    if ($sel == 0) {
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                    
                    $num = pg_num_rows($sel);
                    if ($num > 0) {
                        
                        for ($i=0; $i<$num; $i++) {
                            $weekday = pg_fetch_result($sel, $i, "weekday");
                            $arr_data[$weekday]["officehours2_start"] = pg_fetch_result($sel, $i, "officehours2_start");
                            $arr_data[$weekday]["officehours2_end"] = pg_fetch_result($sel, $i, "officehours2_end");
                            $arr_data[$weekday]["officehours4_start"] = pg_fetch_result($sel, $i, "officehours4_start");
                            $arr_data[$weekday]["officehours4_end"] = pg_fetch_result($sel, $i, "officehours4_end");
                        }
                    }
                }
                $arr_hist[$h_idx]["data"] = $arr_data;
                $h_idx++;
            }
            
        }
        $this->empcond_officehours_hist = $arr_hist;
		return $arr_empcond_officehours;
	}

	/**
     * 個人別勤務時間帯取得
     *　※get_arr_empcond_officehours個人別勤務時間帯取得を呼んだあとに使用する
     * @param string $weekday 曜日 0:日 6:土 7:祝日
     * @param string $date 対象日、履歴対応用 20130220 省略時は履歴から検索しない
     * @return array 時間帯の配列 所定時刻、休憩時刻を設定、曜日別の時刻が未設定の時は基本時間
	 *
	 */
	function get_empcond_officehours($weekday, $date="") {

		$ret_data = array();
        //履歴から検索
        if ($date != "") {
            $num = count($this->empcond_officehours_hist);
            for ($i=0; $i<$num; $i++) {
                //変更日確認、指定日以降で近い日から履歴として呼出元へ返す。ただし時刻設定済みの場合
                $wk_histdate = str_replace("-", "", substr($this->empcond_officehours_hist[$i]["histdate"], 0, 10));
                if ($wk_histdate >= $date) {
                    //曜日別確認
                    $arr_data = $this->empcond_officehours_hist[$i]["data"];
                    if ($arr_data[$weekday]["officehours2_start"] != "") {
                        $ret_data = $arr_data[$weekday];
                    }
                    else {
                        //基本
                        if ($arr_data[8]["officehours2_start"] != "") {
                            $ret_data = $arr_data[8];
                        }
                    }
                    break;
                }
            }
        }
        //履歴にない場合、最新から取得
        if (count($ret_data) == 0) {
            
            //曜日別時間
            if ($this->empcond_officehours_array[$weekday]["officehours2_start"] != "") {
                $ret_data = $this->empcond_officehours_array[$weekday];
            }
            else {
                //基本時間
                if ($this->empcond_officehours_array[8]["officehours2_start"] != "") {
                    $ret_data = $this->empcond_officehours_array[8];
                }
            }
        }
        //時刻がある場合、所定時間計算設定
        if ($ret_data["officehours2_start"] != "") {
            $today = date("Ymd");
            $start_datetime = $today.$ret_data["officehours2_start"];
            $end_datetime = $today.$ret_data["officehours2_end"];
            $wk_time = date_utils::get_time_difference($end_datetime, $start_datetime);
            //休憩
            if ($ret_data["officehours4_start"] != "") {
                $rest_start_datetime = $today.$ret_data["officehours4_start"];
                $rest_end_datetime = $today.$ret_data["officehours4_end"];
                $rest_time = date_utils::get_time_difference($rest_end_datetime, $rest_start_datetime);
                $wk_time -= $rest_time;
            }
            $time_str = sprintf("%02d%02d", $wk_time / 60, $wk_time % 60);
            $ret_data["part_specified_time"] = $time_str;
        }
        
		return $ret_data;
	}
    
    
    /**
     * get_officehours_emp 指定職員の勤務時間帯を取得する
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $date 対象日
     * @param mixed $tmcd_group_id 勤務パターングループID
     * @param mixed $pattern 勤務パターン
     * @return mixed 時間帯の配列
     *
     */
    function get_officehours_emp($emp_id, $date, $tmcd_group_id, $pattern) {
        $arr_ret = array();
        if ($tmcd_group_id >= 1 && $pattern >= 1) {
            $empcond_officehours_ptn_flg = $this->check_empcond_officehours_pattern($tmcd_group_id, $pattern);
            if ($empcond_officehours_ptn_flg) {
                $arr_empcond_officehours = $this->get_arr_empcond_officehours($emp_id, $date); //個人別勤務時間帯履歴対応 20130220
                $arr_weekday_flg = $this->get_arr_weekday_flag($date, $date);
                $wk_weekday = $arr_weekday_flg[$date];
                $arr_empcond_officehours = $this->get_empcond_officehours($wk_weekday, $date); //個人別勤務時間帯履歴対応 20130220
                if ($arr_empcond_officehours["officehours2_start"] != "") {
                    $arr_ret = $arr_empcond_officehours;
                    $arr_ret["office_start_time"] = $arr_empcond_officehours["officehours2_start"];
                    $arr_ret["office_end_time"] = $arr_empcond_officehours["officehours2_end"];
                }
            }
            
        }
        return $arr_ret;
    }
    /**
     * 指定パターンが勤務時間を計算しない ターンか確認
     * @param string $group_id 勤務パターングループID
     * @param string $atdptn_id 勤務パターンID
     * @return boolean true:計算しない false:計算する
     *
     */
    function check_no_count_pattern($group_id, $atdptn_id) {
        
        
        $sql =	"SELECT count(*) as cnt  FROM atdptn ";
        $cond = "WHERE no_count_flag = 1 and group_id = $group_id and atdptn_id = $atdptn_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $cnt = pg_fetch_result($sel, 0, "cnt");
        
        return ($cnt > 0);
        
    }
    
    //休憩時間チェック
    //休憩時間が不足している場合にfalseを返す、OKの場合はtrue
    //勤務時間＞８時間、かつ、休憩時間＜６０分
    //または
    //勤務時間＞６時間、かつ、休憩時間＜４５分
    //param 職員ID、日付、実績データ
    function check_rest_time($emp_id, $date, $atdbkrslt) {
        //実績データ取得
        if (count($atdbkrslt) == 0) {
            $arr_result = $this->get_clock_in_atdbkrslt_array($this->_db_con, $emp_id, $this->file_name, "1", $date);   
        }
        else {
            $arr_result = $atdbkrslt;
        }
        
        //計算と比較
        $next_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($date)));
        //所定労働時間、個人勤務条件があれば優先、次に所定（時間帯画面）（取得関数内で対応）
        $office_start_time = $arr_result["office_start_time"];
        $office_end_time = $arr_result["office_end_time"];
        
       
        //日またがり確認、24時間超over_24hour_flag
        $office_start_date_time = $date.$office_start_time;
        $office_end_date = $date;
        if ($arr_result["over_24hour_flag"] == 1 ||
                ($arr_result["over_24hour_flag"] != 1 && ($office_end_time < $office_start_time))) {
            $office_end_date = $next_date;
        }
        $office_end_date_time = $office_end_date.$office_end_time;
        
        $office_time_min = date_utils::get_diff_minute($office_end_date_time, $office_start_date_time);
        
        //休憩時間、実績にあれば優先、次に個人勤務条件があれば優先、次に所定（時間帯画面）（取得関数内で対応）
        $rest_start_time = ($arr_result["office_rest_start_time"] != "") ? $arr_result["office_rest_start_time"] : $arr_result["rest_start_time"];
        $rest_end_time = ($arr_result["office_rest_end_time"] != "") ? $arr_result["office_rest_end_time"] : $arr_result["rest_end_time"];
        $rest_time_min = 0;
        if ($rest_start_time != "" && $rest_end_time != "") {
            $rest_start_date = $date;
            $rest_end_date = $date;
            //日またがり確認
            $wk_office_end_time = ($office_end_time == "0000") ? "2400" : $office_end_time;
            if ($rest_start_date < $wk_office_end_time) {
                $rest_start_date = $next_date;
            }
            if ($rest_end_date < $wk_office_end_time) {
                $rest_end_date = $next_date;
            }
            
            $rest_time_min = date_utils::get_diff_minute($rest_end_date.$rest_end_time, $rest_start_date.$rest_start_time);
        }
        //所定の中の休憩を除外
        $office_time_min -= $rest_time_min;
        
        //残業時間５回分集計
        $ovtm_min = 0;
        $ovtm_rest_min = 0;
        for ($i=1; $i<=5; $i++) {
            $idx = ($i == 1) ? "" : $i;
            $s_t = "over_start_time".$idx;
            $e_t = "over_end_time".$idx;
            if ($arr_result[$s_t] != "" && $arr_result[$e_t] != "") {
                $s_check_nm = "over_start_next_day_flag".$idx;
                $e_check_nm = "over_end_next_day_flag".$idx;
                $o_s_date = ($arr_result[$s_check_nm] == 1) ? $next_date : $date;
                $o_e_date = ($arr_result[$e_check_nm] == 1) ? $next_date : $date;
                $wk_min = date_utils::get_diff_minute($o_e_date.$arr_result[$e_t], $o_s_date.$arr_result[$s_t]);
                $ovtm_min += $wk_min;
                
                $s_t = "rest_start_time".$i;
                $e_t = "rest_end_time".$i;
                if ($arr_result[$s_t] != "" && $arr_result[$e_t] != "") {
                    $s_check_nm = "rest_start_next_day_flag".$i;
                    $e_check_nm = "rest_end_next_day_flag".$i;
                    $o_s_date = ($arr_result[$s_check_nm] == 1) ? $next_date : $date;
                    $o_e_date = ($arr_result[$e_check_nm] == 1) ? $next_date : $date;
                    $o_rest_min = date_utils::get_diff_minute($o_e_date.$arr_result[$e_t], $o_s_date.$arr_result[$s_t]);
                    $ovtm_rest_min += $o_rest_min;
                    
                }
            }
            
        }
        //勤務時間＝所定労働時間＋残業時間
        $kinmu_total_min = $office_time_min + $ovtm_min;
        
        //休憩時間＝所定内休憩時間＋残業時間内の休憩時間
        $rest_total_min = $rest_time_min + $ovtm_rest_min;
        
        //echo "kinmu_total_min=$kinmu_total_min <br>\n";
        //echo "rest_time_min$rest_time_min <br>\n";
        //echo "rest_total_min=$rest_total_min <br>\n";
        //勤務時間＞８時間、かつ、休憩時間＜６０分
        //または
        //勤務時間＞６時間、かつ、休憩時間＜４５分
        if (($kinmu_total_min > 8 * 60 && $rest_total_min < 60) ||
                ($kinmu_total_min > 6 * 60 && $rest_total_min < 45)) {
            $flg = false;
        }
        else {
            $flg = true;
        }
        
        return $flg;
    }

    //休憩時間チェック用に、所定時間"office_time_min"と所定内の休憩時間"rest_time_min"を配列で返す
    function get_office_rest_data($emp_id, $date, $atdbkrslt) {
        //実績データ取得
        //if (count($atdbkrslt) == 0) {
            $arr_result = $this->get_clock_in_atdbkrslt_array($this->_db_con, $emp_id, $this->file_name, "1", $date);   
        //}
        //else {
        //    $arr_result = $atdbkrslt;
        //}
        
        //計算と比較
        $next_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($date)));
        //所定労働時間、個人勤務条件があれば優先、次に所定（時間帯画面）（取得関数内で対応）
        $office_start_time = $arr_result["office_start_time"];
        $office_end_time = $arr_result["office_end_time"];
        
        
        //日またがり確認、24時間超over_24hour_flag
        $office_start_date_time = $date.$office_start_time;
        $office_end_date = $date;
        if ($arr_result["over_24hour_flag"] == 1 ||
                ($arr_result["over_24hour_flag"] != 1 && ($office_end_time < $office_start_time))) {
            $office_end_date = $next_date;
        }
        $office_end_date_time = $office_end_date.$office_end_time;
        
        $office_time_min = date_utils::get_diff_minute($office_end_date_time, $office_start_date_time);
        
        //休憩時間、実績にあれば優先、次に個人勤務条件があれば優先、次に所定（時間帯画面）（取得関数内で対応）
        $rest_start_time = ($arr_result["office_rest_start_time"] != "") ? $arr_result["office_rest_start_time"] : $arr_result["rest_start_time"];
        $rest_end_time = ($arr_result["office_rest_end_time"] != "") ? $arr_result["office_rest_end_time"] : $arr_result["rest_end_time"];
        $rest_time_min = 0;
        if ($rest_start_time != "" && $rest_end_time != "") {
            $rest_start_date = $date;
            $rest_end_date = $date;
            //日またがり確認
            $wk_office_end_time = ($office_end_time == "0000") ? "2400" : $office_end_time;
            if ($rest_start_time < $wk_office_end_time) {
                $rest_start_date = $next_date;
            }
            if ($rest_end_time < $wk_office_end_time) {
                $rest_end_date = $next_date;
            }
            
            $rest_time_min = date_utils::get_diff_minute($rest_end_date.$rest_end_time, $rest_start_date.$rest_start_time);
        }
        //所定の中の休憩を除外
        $office_time_min -= $rest_time_min;
    
        return array("office_time_min" => $office_time_min, "rest_time_min" => $rest_time_min);
    }
       
    function get_atdbk_term($emp_id, $date) {
    
        // 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
        $sql = "select duty_form from empcond";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";

        $sql = "select closing, closing_parttime, closing_month_flg from timecard";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        if ($duty_form == 2) {
            //非常勤
            $wk = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
            //非常勤が未設定時は常勤を使用する
            if ($wk == "") {
                $wk = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
            }
        } else {
            //常勤
            $wk = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
        }
        $closing = $wk;
        $closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";
     
        $year = substr($date, 0, 4);
        $month = intval(substr($date, 4, 2), 10);

        // 開始日・締め日を算出
    	$calced = false;
        switch ($closing) {
            case "1":  // 1日
                $closing_day = 1;
                break;
            case "2":  // 5日
                $closing_day = 5;
                break;
            case "3":  // 10日
                $closing_day = 10;
                break;
            case "4":  // 15日
                $closing_day = 15;
                break;
            case "5":  // 20日
                $closing_day = 20;
                break;
            case "6":  // 末日
                $start_year = $year;
                $start_month = $month;
                $start_day = 1;
                $end_year = $start_year;
                $end_month = $start_month;
                $end_day = days_in_month($end_year, $end_month);
                $calced = true;
                break;
        }
        if (!$calced) {
            //$day = date("j");
            $day = intval(substr($date, 6, 2), 10);
            $start_day = $closing_day + 1;
            $end_day = $closing_day;
            if ($day <= $closing_day) {
                $start_year = $year;
                $start_month = $month - 1;
                if ($start_month == 0) {
                    $start_year--;
                    $start_month = 12;
                }
                
                $end_year = $year;
                $end_month = $month;
            } else {
                $start_year = $year;
                $start_month = $month;
                
                $end_year = $year;
                $end_month = $month + 1;
                if ($end_month == 13) {
                    $end_year++;
                    $end_month = 1;
                }
            }
        }
        $start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
        $end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
     
        return array($start_date, $end_date); 
    }
    
    //$input_refer_flg "refer":参照（当日分除かない） "input":入力（当日分除く、変更されるかもしれないため）
    function get_ovtm_month_total($emp_id, $target_date, $input_or_refer_flg) {
        
        //承認済み
        $approve_min = 0;
        //申請中
        $apply_min = 0;

        //入力の場合、当日分を別に集計し、ovtm_approve_time_all等に入れて返す
        $today_approve_min = 0;
        $today_apply_min = 0;
        
        list($start_date, $end_date) = $this->get_atdbk_term($emp_id, $target_date);
        //echo "$emp_id $target_date $start_date $end_date";        
        // 処理日付の勤務実績を取得
        $sql =	"SELECT ".
        "atdbkrslt.*, ".
        "atdptn.workday_count, ".
        "atdptn.base_time, ".
        "atdptn.over_24hour_flag, ".
        "atdptn.out_time_nosubtract_flag, ".
        "atdptn.after_night_duty_flag, ".
        "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
        "atdptn.no_count_flag, ".
	"ovtmapply.apply_id     AS ovtm_apply_id, ".
		"ovtmapply.apply_status AS ovtm_apply_status, ".
		"ovtmapply.reason AS other_reason, ".
		"ovtmapply.reason_id AS ovtm_reason_id, ".
		"rtnapply.apply_id      AS rtn_apply_id, ".
		"rtnapply.apply_status  AS rtn_apply_status ".
		"FROM ".
		"atdbkrslt  ".
		"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
		"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
		"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
		"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ".
		"LEFT JOIN ovtmrsn on ovtmapply.reason_id = ovtmrsn.reason_id";
        $cond = "where atdbkrslt.emp_id = '$emp_id' and atdbkrslt.date >= '$start_date' and atdbkrslt.date <= '$end_date'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            $date = $row["date"];
            $arr_atdbkrslt[$date] = $row;
        }
        //普通残業
        $normal_over_total = 0;
        //深夜
        $late_night_total = 0;
        //休日残業
        $hol_over_total = 0;
        //休日深夜
        $hol_late_night_total = 0;
        
        $sixty_value = 3600;
        //60時間超えた普通残業
        $sixty_normal_over_total = 0;
        //60時間超えた深夜残業
        $sixty_late_night_total = 0;

        // 処理日付の勤務日種別を取得
        $sql = "select date, type from calendar";
        $cond = "where date >= '$start_date' and date <= '$end_date'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            $date = $row["date"];
            $arr_type[$date] = $row["type"];
        }

        $tmp_date = $start_date;
        while ($tmp_date <= $end_date) {
            
            //登録対象日は除く（変更される分かもしれないため）
            //if ($input_or_refer_flg == "input" && $tmp_date == $target_date) {
            //    $tmp_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($tmp_date)));
            //    continue;
            //}

           
            $ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
            $ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
            $rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
            $rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];
            
            $next_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($tmp_date)));
            //echo "tmp_date=$tmp_date <br>";
            //残業時間
            $ovtm_min = 0;
            $ovtm_rest_min = 0;
            //5回繰り返す 
            for ($i=1; $i<=5; $i++) {
                $idx = ($i == 1) ? "" : $i;
                $s_t = "over_start_time".$idx;
                $e_t = "over_end_time".$idx;
                $o_s_time = $arr_atdbkrslt[$tmp_date]["$s_t"];
                $o_e_time = $arr_atdbkrslt[$tmp_date]["$e_t"];
                
                if ($o_s_time != "" && $o_e_time != "") {
                    //echo "o_s_time=$o_s_time <br>";
                    //echo "o_e_time=$o_e_time <br>";
                    //時間計算
                    $s_f = "over_start_next_day_flag".$idx;
                    $e_f = "over_end_next_day_flag".$idx;
                    $o_s_date = ($arr_atdbkrslt[$tmp_date]["$s_f"] == 1) ? $next_date : $tmp_date;
                    $o_e_date = ($arr_atdbkrslt[$tmp_date]["$e_f"] == 1) ? $next_date : $tmp_date;
                    $wk_min = date_utils::get_diff_minute($o_e_date.$o_e_time, $o_s_date.$o_s_time);
                    //echo "wk_min=$wk_min <br>";
                    $ovtm_min += $wk_min;
                    
                    //休憩時刻
                    $s_t = "rest_start_time".$i;
                    $e_t = "rest_end_time".$i;
                    $o_s_time = $arr_atdbkrslt[$tmp_date]["$s_t"];
                    $o_e_time = $arr_atdbkrslt[$tmp_date]["$e_t"];
                    if ($o_s_time != "" && $o_e_time != "") {
                        $s_f = "rest_start_next_day_flag".$i;
                        $e_f = "rest_end_next_day_flag".$i;
                        $o_s_date = ($arr_atdbkrslt[$tmp_date]["$s_f"] == 1) ? $next_date : $tmp_date;
                        $o_e_date = ($arr_atdbkrslt[$tmp_date]["$e_f"] == 1) ? $next_date : $tmp_date;
                        $wk_rest_min = date_utils::get_diff_minute($o_e_date.$o_e_time, $o_s_date.$o_s_time);
                        $ovtm_rest_min += $wk_rest_min;
                        
                    }
                    
                }
            }
            $ovtm_min -= $ovtm_rest_min;
            
            //echo("$tmp_date $ovtm_min <br>\n");
            //入力時の当日分
            if ($input_or_refer_flg == "input" && $tmp_date == $target_date) {
                //承認済み、申請中を分けて集計
                //承認済み
                if ($ovtm_apply_status == "1") {
                    $today_approve_min += $ovtm_min;
                }
                //申請中
                if ($ovtm_apply_status == "0") {
                    $today_apply_min += $ovtm_min;
                }
            }
            else {
                //承認済み
                if ($ovtm_apply_status == "1") {
                    $approve_min += $ovtm_min;
                }
                //申請中
                if ($ovtm_apply_status == "0") {
                    $apply_min += $ovtm_min;
                }
            }
            $tmp_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($tmp_date)));
        }
        $arr_data = array();
        $arr_data["ovtm_approve_min"] = $approve_min;
        $arr_data["ovtm_apply_min"] = $apply_min;
        $arr_data["ovtm_approve_apply_min"] = $approve_min + $apply_min;
        $arr_data["ovtm_approve_time"] = ($approve_min==0) ? " 0:00": $this->minute_to_hmm($approve_min);
        $arr_data["ovtm_apply_time"] = ($apply_min==0) ? " 0:00": $this->minute_to_hmm($apply_min);
        $arr_data["ovtm_approve_apply_time"] = ($approve_min + $apply_min==0) ? " 0:00": $this->minute_to_hmm($approve_min + $apply_min);
        //当日分も入れた全て        
        $arr_data["ovtm_approve_min_all"] = $approve_min + $today_approve_min;
        $arr_data["ovtm_apply_min_all"] = $apply_min + $today_apply_min;
        $arr_data["ovtm_approve_apply_min_all"] = $approve_min + $apply_min + $today_approve_min + $today_apply_min;
        $arr_data["ovtm_approve_time_all"] = ($approve_min + $today_approve_min==0) ? " 0:00": $this->minute_to_hmm($approve_min + $today_approve_min);
        $arr_data["ovtm_apply_time_all"] = ($apply_min + $today_apply_min==0) ? " 0:00": $this->minute_to_hmm($apply_min + $today_apply_min);
        $arr_data["ovtm_approve_apply_time_all"] = ($approve_min + $apply_min + $today_apply_min + $today_approve_min==0) ? " 0:00": $this->minute_to_hmm($approve_min + $apply_min + $today_apply_min + $today_approve_min);
        
        
        return $arr_data;
    }
    /**
     * タイムカード修正画面のチェック用情報取得
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $tmcd_group_id 勤務パターングループID
     * @param mixed $pattern 勤務パターン
     * @param mixed $date 対象日付
     * @return mixed グループ名
     *
     */
    function get_officetime_info($emp_id, $tmcd_group_id, $pattern, $date) {
        $arr_info = array();
        //休暇
        if ($pattern == "10" || $pattern == "--") {
            $arr_info["office_start_time"] = "";
            $arr_info["office_end_time"] = "";
            $arr_info["rest_start_time"] = "";
            $arr_info["rest_end_time"] = "";
            $arr_info["office_time_min"] = 0;
            $arr_info["rest_time_min"] = 0;
            $arr_info["base_time"] = "";
            return $arr_info;
        }
        
        //個人別所定時間を優先するための対応
        $arr_empcond_officehours_ptn = $this->get_empcond_officehours_pattern();
        $arr_weekday_flg = $this->get_arr_weekday_flag($date, $date);
        $arr_empcond_officehours = $this->get_arr_empcond_officehours($emp_id, $date); //個人別勤務時間帯履歴対応
        
        $sql = "select date, type from calendar";
        $cond = "where date = '$date'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_type = array();
        while ($row = pg_fetch_array($sel)) {
            $date = $row["date"];
            $arr_type[$date] = $row["type"];
        }
        $type = $arr_type[$date];
        $tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
        $start2 = $this->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
        if ($start2 != "") {
            $end2 =   $this->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
            $start4 = $this->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
            $end4 =   $this->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
        }
        else {
            $wk_officehours = $this->get_officehours( $tmcd_group_id, $pattern, $tmp_type, "start2" );
            $start2 = $wk_officehours["start2"];
            $end2 = $wk_officehours["end2"];
            $start4 = $wk_officehours["start4"];
            $end4 = $wk_officehours["end4"];
        }
        
        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
            $wk_weekday = $arr_weekday_flg[$date];
            $arr_empcond_officehours = $this->get_empcond_officehours($wk_weekday, $date); //個人別勤務時間帯履歴対応
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $start2 = $arr_empcond_officehours["officehours2_start"];
                $end2 = $arr_empcond_officehours["officehours2_end"];
                $start4 = $arr_empcond_officehours["officehours4_start"];
                $end4 = $arr_empcond_officehours["officehours4_end"];
            }
        }
        
        $over_24hour_flag = $this->get_over_24hour_flag($tmcd_group_id, $pattern);
        $next_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($date)));
        $wk_start2 = str_replace(":", "", $start2);
        $wk_end2 = str_replace(":", "", $end2);
        $office_start_date_time = $date.$wk_start2;
        $office_end_date = $date;
        if ($over_24hour_flag == 1 ||
                ($over_24hour_flag != 1 && ($wk_end2 < $wk_start2))) {
            $office_end_date = $next_date;
        }
        $office_end_date_time = $office_end_date.$wk_end2;
        
        $office_time_min = date_utils::get_diff_minute($office_end_date_time, $office_start_date_time);
        
        $rest_time_min = 0;
        $rest_start_time = "";
        $rest_end_time = "";
        if ($start4 != "" && $end4 != "") {
            $rest_start_date = $date;
            $rest_end_date = $date;
            $rest_start_time = str_replace(":", "", $start4);
            $rest_end_time = str_replace(":", "", $end4);
            //日またがり確認
            $wk_office_end_time = str_replace(":", "", $end2);
            $wk_office_end_time = ($wk_office_end_time == "0000") ? "2400" : $wk_office_end_time;
            if ($rest_start_time < $wk_office_end_time) {
                $rest_start_date = $next_date;
            }
            if ($rest_end_time < $wk_office_end_time) {
                $rest_end_date = $next_date;
            }
            
            $rest_time_min = date_utils::get_diff_minute($rest_end_date.$end4, $rest_start_date.$start4);
        }
        //所定の中の休憩を除外
        $office_time_min -= $rest_time_min;
        
        $base_time = $wk_end2;
        if ($wk_end2 < $wk_start2) {
            $base_time = $wk_start2;  
        }

        $arr_info["office_start_time"] = $wk_start2;
        $arr_info["office_end_time"] = $wk_end2;
        $arr_info["rest_start_time"] = $rest_start_time;
        $arr_info["rest_end_time"] = $rest_end_time;
        $arr_info["office_time_min"] = $office_time_min;
        $arr_info["rest_time_min"] = $rest_time_min;
        $arr_info["base_time"] = $base_time;
        return $arr_info;
    }	
}
?>