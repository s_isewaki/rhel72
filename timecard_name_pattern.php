	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 出勤パターン名</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_attendance_pattern.ini");
require("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);

// 初期表示時
if ($pattern_count == "") {

	// 出勤パターン名を取得
	//$arr_attendance_pattern = get_attendance_pattern_array($con, $fname, $group_id);

	$arr_attendance_pattern = get_attendance_pattern_array_order($con, $fname, $group_id);
	$pattern_count = count($arr_attendance_pattern);

// パターン数変更時
} else {
	//途中のIDがない場合、エラーとなるので対処 20110610
	//出勤パターンIDの最大
	$max_id = "";
	for ($i = 0; $i < $pattern_count; $i++) {
		$var = "atdptn_id_".$i;
		if ($$var != "") {
			//最大ID
			$max_id = max($max_id, $$var);
		}
	}
	//通番、空白時用
	$idx = 1;
	$arr_attendance_pattern = array();
	for ($i = 0; $i < $pattern_count; $i++) {
		$var = "pattern_names_".$i;
		$arr_attendance_pattern[$i]["atdptn_nm"] = $$var;
		$var = "atdptn_id_".$i;
		//空白時、最大IDから通番
		if ($$var == "") {
			$wk_id =  $max_id + $idx;
			$idx++;
		} else {
			$wk_id = $$var;
		}
		$arr_attendance_pattern[$i]["atdptn_id"] = $wk_id;
		$var = "order_no_".$i;
		$arr_attendance_pattern[$i]["atdptn_order_no"] = $$var;
		$var = "display_flag_".$i;
		$wk_flag = ($$var == "f") ? "f" : "t";
		$arr_attendance_pattern[$i]["display_flag"] = $wk_flag;
	}
}
//休暇(10)がない場合には後ろに追加する
$exist_flg = false;
for ($i = 0; $i < $pattern_count; $i++) {
	if ($arr_attendance_pattern[$i]["atdptn_id"] == "10") {
		$exist_flg = true;
		break;
	}
}
if (!$exist_flg) {
	$arr_attendance_pattern[$pattern_count]["atdptn_id"] = "10";
	$arr_attendance_pattern[$pattern_count]["atdptn_nm"] = "休暇";
	$arr_attendance_pattern[$pattern_count]["atdptn_order_no"] = $pattern_count+1; //表示順
	$arr_attendance_pattern[$pattern_count]["display_flag"] = "t";
	$pattern_count++;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="50%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="timecard_name.php?session=<? echo($session); ?>">グループ名</a></font></td>
<td width="50%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターン名</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="update" action="timecard_name_pattern_update_exe.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td width="30%"><select name="group_id" onchange="location.href = '<? echo($fname . "?session=$session&group_id="); ?>' + this.value;">
<?
foreach ($group_names as $tmp_group_id => $group_name) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $group_id) {
		echo(" selected");
	}
	echo(">$group_name\n");
}
?>
</select></td>
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターン数</font></td>
<td width="30%"><select name="pattern_count" onchange="this.form.action = '<? echo($fname); ?>'; this.form.submit();">
<?
for ($i = 10; $i <= 100; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $pattern_count) {
		echo(" selected");
	}
	echo(">$i\n");
}
?>
</select></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td colspan="2" align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<? /* 表示順、非表示追加 20101006 */ ?>
<tr height="22">
<td width="8%" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
<td width="8%" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font></td>
<td width="20%" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターン</font></td>
<td width="64%" align="left" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;&nbsp;出勤パターン名</font></td>
</tr>
<? for ($i = 0; $i < $pattern_count; $i++) { ?>
<tr height="22">
<td width="8%" align="center">
<?
	echo("<select id=\"order_no_$i\" name=\"order_no_$i\">\n");

	for ($k=0; $k<=$pattern_count; $k++) {
		// selected
		if ($i + 1 == $k) {
			$selected = " selected";
		} else {
			$selected = "";
		}
		$order_val = $k;
		echo("<option value=\"$order_val\" $selected>$order_val</option>\n");
	}

	echo("</select>");
?>
</td>
<td width="8%" align="center">
<?
	$checked = ($arr_attendance_pattern[$i]["display_flag"] == "f") ? " checked" : "";
	echo("<input type=\"checkbox\" id=\"display_flag_$i\" name=\"display_flag_$i\" value=\"f\" $checked>\n");
?>
</td>
<td width="20%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターン<? echo($arr_attendance_pattern[$i]["atdptn_id"]); ?></font>
<input type="hidden" name="atdptn_id_<?=$i?>" value="<? echo($arr_attendance_pattern[$i]["atdptn_id"]); ?>">
</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="pattern_names_<?=$i?>" value="<? echo($arr_attendance_pattern[$i]["atdptn_nm"]); ?>" size="20" maxlength="10" style="ime-mode:active;"><? if ($arr_attendance_pattern[$i]["atdptn_id"] == 10) {echo(" （このパターンのみ、時間帯は登録できません）");} ?></font></td>
</tr>
<? } ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td colspan="2" align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
