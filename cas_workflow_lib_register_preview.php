<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require_once("cas_yui_calendar_util.ini");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, $CAS_MANAGE_AUTH, $fname);
if($summary == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

$cas_title = get_cas_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<TITLE><?=$cas_title?>｜テンプレートプレビュー</TITLE>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
<!--
// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>
<?
$num = get_number_of_calendar($session);

if ($num > 0) {
	// 外部ファイルを読み込む
	write_yui_calendar_use_file_read_0_12_2();

}
// カレンダー作成、関数出力
write_yui_calendar_script2($num);
?>
</HEAD>
<BODY  bgcolor="#ffffff" text="#000000" onload="initcal();
<? if($iframe != ""){ ?>
if(window.parent.document.all('<?=$iframe ?>'))
{
	var pfrm = window.parent.document.all('<?=$iframe ?>');
	var h = pfrm.contentWindow.document.body.scrollHeight;
	if(h < pfrm.contentWindow.document.body.offsetHeight)
	{
		h = pfrm.contentWindow.document.body.offsetHeight;
	}
}
<? } ?>
if (window.OnloadSub) { OnloadSub(); }">
<?

//========================================
//閉じるヘッダーを表示
//========================================
if($sansho_flg == false){
	?>
	<table width="560" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#A0D25A">
	<td width="480" height="32" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>テンプレートプレビュー</b></font></td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	</table>
	<img src="img/spacer.gif" width="10" height="10" alt=""><br>
	<?
}

?>
<?
// テンプレート読み込み
$filename = "cas_workflow/tmp/{$session}_t.php";
if (is_file($filename)) {
	if (!$fp = fopen($filename, "r")) {
		echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	$content = fread($fp, filesize($filename));

	fclose($fp);

	$content_prev = ereg_replace("<\? \/\/ XML.*\?>", "", $content);
	$content_prev = eregi_replace("_(textarea)", "/\\1", $content_prev);
	$content_prev = eregi_replace("_(script)", "/\\1", $content_prev);

	if (strlen($content_prev) > 0) {
		// プレビュー用ファイル保存
		$savefilename = "cas_workflow/tmp/{$session}_p.php";

		if (!$fp = fopen($savefilename, "w")) {
			echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
		if (!fwrite($fp, $content_prev, 2000000)) {
			echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		fclose($fp);

		echo("<form name=\"apply\">");

		// プレビュー用テンプレートファイルをインクルード
		$filename = "cas_workflow/tmp/{$session}_p.php";
		$a = include($filename);

		echo("</form>");
	}

}

?>
</BODY>
</HTML>
