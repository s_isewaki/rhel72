<?
require_once("about_comedix.php");
require_once("application_workflow_common_class.php");
require_once('Cmx/Model/SystemConfig.php');

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,7,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new application_workflow_common_class($con, $fname);

//一画面内の最大表示件数
$disp_max_page = 15;

if($page == "")
{
	$page = 1;
}

$approved_apply_list_count = $obj->get_approved_apply_list_count($session, $wkfw_id);

$arr_approved_apply = $obj->get_approved_apply_list($session, $wkfw_id, $page, $disp_max_page);

$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 決裁・申請 | 承認確定申請一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function setProcondApply(apply_id, apply_no, apply_title)
{
    if(window.opener && !window.opener.closed && window.opener.setPrecond)
    {
  	    window.opener.setPrecond('<?=$precond_order?>', apply_id, apply_no, apply_title);
		self.close();
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>承認確定申請一覧</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="apply" action="#" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr bgcolor="#f6f9ff">
<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号</font></td>
<td width="130" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($title_label); ?></font></td>
<td width="80" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
</tr>

<?
if($approved_apply_list_count > 0)
{
	foreach($arr_approved_apply as $approved_apply)
	{
		$apply_id = $approved_apply["apply_id"];
		$apply_title = $approved_apply["apply_title"];

		// 申請番号
		$apply_date = $approved_apply["apply_date"];
		$short_wkfw_name = $approved_apply["short_wkfw_name"];
		$apply_no        = $approved_apply["apply_no"];
		$year = substr($apply_date, 0, 4);
		$md   = substr($apply_date, 4, 4);
		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}
		$apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

		// 申請日
		$apply_ymd = preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $apply_date);


?>
<tr>
<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="button" value="設定" onclick="setProcondApply('<?=$apply_id?>', '<?=$apply_no?>', '<?=h($apply_title)?>')"></font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_no?></font></td>
<td width="130"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($approved_apply["wkfw_title"])?></font></td>
<td width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($approved_apply["apply_title"])?></font></td>
<td width="80" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_ymd?></font></td>
</tr>
<?
	}
}
?>

</table>

<?
if($approved_apply_list_count == 0)
{
?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認確定の申請書がございません。</font>
<?
}
//最大ページ数
if($approved_apply_list_count == 0)
{
	$page_max  = 1;
}
else
{
	$page_max  = floor( ($approved_apply_list_count-1) / $disp_max_page ) + 1;
}

show_page_area($page_max,$page);


?>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</form>
</center>
</body>
</html>

<?

function show_page_area($page_max,$page)
{
	?>
	<input type="hidden" name="page" value="">
	<script type="text/javascript">

	//ページ遷移します。
	function page_change(page)
	{
		document.apply.page.value=page;
		document.apply.submit();
	}

	</script>

	<?
	if($page_max != 1)
	{
	?>
		<table width="100%">
		<tr>
		<td>
		  <table>
		  <tr>
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    先頭へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(1);">
		    先頭へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    前へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page-1?>);">
		    前へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		<?

		for($i=1;$i<=$page_max;$i++)
		{
			if($i == $page)
			{
		?>
		    [<?=$i?>]
		<?
			}
			else
			{
		?>
		    <a href="javascript:page_change(<?=$i?>);">[<?=$i?>]</a>
		<?
			}
		}
		?>
		    </font>
		  </td>
		  <td>
		<?


		if($page == $page_max)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    次へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page+1?>);">
		    次へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  </tr>
		  </table>
		</td>
		</tr>
		</table>
	<?
	}
	?>
	<?
}

pg_close($con);
?>
