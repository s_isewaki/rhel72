<?
ini_set("max_execution_time", 0);
ob_start();
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("work_admin_menu_common.ini");
require_once("show_attendance_pattern.ini");
require_once("date_utils.php");
require_once("show_select_values.ini");
require_once("atdbk_common_class.php");
require_once("work_admin_paid_holiday_common.php");
require_once("show_timecard_common.ini");

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
// 締め日を取得
$sql = "select closing, closing_parttime, closing_paid_holiday, criteria_months from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
$closing_paid_holiday = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_paid_holiday") : "1";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}
//基準月数
$criteria_months = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "criteria_months") : "2";
$wk_month = ($criteria_months == "1") ? "3" : "6";

//年設定
//$default_start_year = 2006;


//付与年月日入力の場合
if ($select_add_yr != "-" && $select_add_yr != "") {
	$year = $select_add_yr;
} elseif ($select_close_yr != "") {
	//
	$cur_year = $select_close_yr;
	$md = $select_close_mon.$select_close_day;
	//年度にする
	if ($closing_paid_holiday == "1") {
		if($md >= "0101" and $md <= "0331") {
			$cur_year--;
		}
	}
	//指定年
	$year = $cur_year;
	//当年を元に開始年を求める
	$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
}
else {
	$cur_year = date("Y");
	$md = date("md");;
	//年度にする
	if ($closing_paid_holiday == "1") {
		if($md >= "0101" and $md <= "0331") {
			$cur_year--;
		}
	}
}

//年一覧開始年
if ($start_year == "") {
	//当年を元に開始年を求める
	$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
}
$end_year = $start_year + 6;

$arr_class_name = get_class_name_array($con, $fname);

// 組織情報を取得し、配列に格納
$arr_cls = array();
$arr_clsatrb = array();
$arr_atrbdept = array();
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	array_push($arr_cls, array("id" => $tmp_class_id, "name" => $tmp_class_nm));
	
	$sql2 = "select atrb_id, atrb_nm from atrbmst";
    $cond2 = "where class_id = '$tmp_class_id' and atrb_del_flg = 'f' order by order_no";
	$sel2 = select_from_table($con, $sql2, $cond2, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_atrb = array();
	while ($row2 = pg_fetch_array($sel2)) {
		$tmp_atrb_id = $row2["atrb_id"];
		$tmp_atrb_nm = $row2["atrb_nm"];
		array_push($arr_atrb, array("id" => $tmp_atrb_id, "name" => $tmp_atrb_nm));
		
		$sql3 = "select dept_id, dept_nm from deptmst";
        $cond3 = "where atrb_id = '$tmp_atrb_id' and dept_del_flg = 'f' order by order_no";
		$sel3 = select_from_table($con, $sql3, $cond3, $fname);
		if ($sel3 == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr_dept = array();
		while ($row3 = pg_fetch_array($sel3)) {
			$tmp_dept_id = $row3["dept_id"];
			$tmp_dept_nm = $row3["dept_nm"];
			array_push($arr_dept, array("id" => $tmp_dept_id, "name" => $tmp_dept_nm));
		}
		array_push($arr_atrbdept, array("atrb_id" => $tmp_atrb_id, "depts" => $arr_dept));
	}
	array_push($arr_clsatrb, array("class_id" => $tmp_class_id, "atrbs" => $arr_atrb));
}

// 該当する職員を検索
$sel_emp = false;
$arr_data = array();
if ($srch_flg == "1") {

	$select_close_date = $select_close_yr.$select_close_mon.$select_close_day;
    $cond = get_paid_srch_cond($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_date, $srch_id);
	
	//付与年月日未設定時、プルダウンの"-"を""にする
	if ($select_add_yr == "-") {
		$select_add_yr = "";
	}
	if ($select_add_mon == "-") {
		$select_add_mon = "";
	}
	if ($select_add_day == "-") {
		$select_add_day = "";
	}
	//sql文取得
	$sql = get_paid_srch_sql($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_yr, $select_close_mon, $select_close_day, $sort);
	
	$offset = $page * $limit;
	//ソート順
	if ($sort == "") {
		$sort = "1";
	}
	switch ($sort) {
		case "1":
			$orderby = "empmst.emp_personal_id ";
			break;
		case "2":
			$orderby = "empmst.emp_personal_id desc ";
			break;
		case "3":
			$orderby = "empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id ";
			break;
		case "4":
			$orderby = "empmst.emp_kn_lt_nm desc, empmst.emp_kn_ft_nm desc, empmst.emp_personal_id ";
			break;
		case "5":
            $orderby = "c.order_no, d.order_no, e.order_no, empmst.emp_personal_id ";
            break;
		case "6":
            $orderby = "c.order_no desc, d.order_no desc, e.order_no desc, empmst.emp_personal_id ";
            break;
	}
	$cond .= " order by $orderby ";
	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
}
$sql = "select closing_paid_holiday from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$closing_paid_holiday = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_paid_holiday") : "1";

// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by order_no";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_paid_hol = array();
while($row = pg_fetch_array($sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
	for ($i=1; $i<=8; $i++) {
		$varname = "add_day".$i;
        //未設定時の対応、""は0とする、8番目が""の時は7番目の数値を使用 20120329
        $wk_days = $row["$varname"];
        if ($i == 8 && $wk_days == "") {
            $wk_days = $arr_paid_hol[$paid_hol_tbl_id][7];
        }
        if ($wk_days == "") {
            $wk_days = "0";
        }
        $arr_paid_hol[$paid_hol_tbl_id][$i] = $wk_days;
	}
}

$select_add_date = $select_add_yr.$select_add_mon.$select_add_day;
//データ取得
$data = get_emp_list_data($con, $fname, $sel_emp, $closing_paid_holiday, $arr_paid_hol, $year, $criteria_months, $atdbk_common_class, $select_close_date, $sort, $select_add_date, "2");
$download_data_1 = $data;


//見出し
$data = "";
// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
$data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";
$data .= "<tr height=\"22\">\n";
$wk_title = "有休繰越・付与日数一覧　";
if ($select_close_yr != "" && $select_close_yr != "-") {
	$wk_title .= "有休残締め日:";
	$wk_title .= $select_close_yr."年";
	$wk_title .= intval($select_close_mon)."月";
	$wk_title .= intval($select_close_day)."日";
}

$data .= "<td nowrap align=\"left\" colspan=\"11\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $wk_title . "</font></td>\n";
//$data .= "<td nowrap align=\"right\" colspan=\"7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成年月日:";
//$data .= date("Y") . "年";
//$data .= date("m") . "月";
//$data .= date("d") . "日";
//$data .= "</font></td>\n";
$data .= "</tr>\n";
$data .= "</table>\n";

$download_data_0 = $data;


$filename = "yuukyu_";
if ($select_close_date != "---") {
	$filename .= $select_close_date;
} else {
	$filename .= date("Ymd");
}
$filename .= ".xls";

///-----------------------------------------------------------------------------
//データEXCEL出力
///-----------------------------------------------------------------------------
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding(nl2br($download_data_0), 'sjis', mb_internal_encoding());
echo mb_convert_encoding(nl2br($download_data_1), 'sjis', mb_internal_encoding());
ob_end_flush();

pg_close($con);

?>