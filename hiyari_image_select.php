<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//テスト用。
//TODO リリース時はコメントアウト必須。
//ini_set("display_errors","1");

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//処理モードごとの処理
//==============================
if(! empty($mode))
{
    switch ($mode)
    {
        case "postback":

            //==============================
            //自己画像を更新
            //==============================
            $sql = "delete from inci_emp_image where emp_id = '$emp_id'";
            $result = pg_exec($con,$sql);
            if($result == false){
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }

            $sql = "insert into inci_emp_image(emp_id,img_id) values(";
            $registry_data = array(
                $emp_id,
                $select_img
            );
            $result = insert_into_table($con, $sql, $registry_data, $fname);
            if ($result == 0) {
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }

            //==============================
            //自分画面を閉じる。
            //==============================
            echo("<script language='javascript'>");
            echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
            echo("window.close();");
            echo("</script>");
            exit;

            break;
        default:
            break;
    }
}

//==============================
//画像一覧取得
//==============================
$sql = "select inci_image_mst.*, cnt_tbl.cnt from inci_image_mst ";
$sql .= "left join ";
$sql .= "(select img_id , count(*) as cnt from inci_emp_image group by img_id) cnt_tbl ";
$sql .= "on inci_image_mst.img_id = cnt_tbl.img_id ";
$cond = "order by inci_image_mst.list_index";
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$img_list = pg_fetch_all($sel);


//==============================
//自己選択画像を取得
//==============================
/*
$sql = "select inci_image_mst.* from inci_emp_image left join inci_image_mst on inci_emp_image.img_id = inci_image_mst.img_id";
$cond = "where inci_emp_image.emp_id = '$emp_id'";
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$my_img = pg_fetch_all($sel);
if(count($my_img) < 1)
{
    $my_img = array();
    $my_img_id = "";
}
else
{
    $my_img = $my_img[0];
    $my_img_id = $my_img["img_id"];
}
*/

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | Myファントルくん</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list_2 {border-collapse:collapse;}
.list_2 td {border:#999999 solid 1px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="hiyari_image_select.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="postback">
<input type="hidden" name="select_img" value="">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
    show_hiyari_header_for_sub_window("Myファントルくん");
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr><td>
                    <font size="4" face="ＭＳ Ｐゴシック, Osaka">あなたのファントルくんを選んでください。</font>
                </td></tr>
                <tr><td>
                    <table width="200" border="0" cellspacing="0" cellpadding="3" class="list_2">
                    <tr>
                    <?
                    foreach($img_list as $img_data)
                    {
                        $img_id = $img_data["img_id"];
                        $img_name = $img_data["img_name"];
                        $img_url = $img_data["img_url"];
                        $default_flg = $img_data["default_flg"];
                        $default_flg = ($default_flg == "t");
                        $use_cnt = $img_data["cnt"];
                        if($use_cnt == "")
                        {
                            $use_cnt = 0;
                        }

                        $p_default = "";
                        if($default_flg)
                        {
                            $p_default = "(+デフォルト)";
                        }
                    ?>
                    <td>
                        <table width="200" border="0" cellspacing="0" cellpadding="3" class="list_2">
                            <tr>
                                <td align="right" bgcolor="#FFDDFD" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka">画像名</font></td>
                                <td align="left" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><?=$img_name?><font></td>
                            </tr>
                            <tr>
                                <td align="right" bgcolor="#FFDDFD" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka">設定者</font></td>
                                <td align="left" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><?=$use_cnt?>人<?=$p_default?><font></td>
                            </tr>
                            <tr height="250">
                                <td align="center" bgcolor="#FFFFFF" colspan="2">
                                    <img src="<?=$img_url?>" width="180">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#FFFFFF" colspan="2">
                                    <input type="button" value="これに決定" onclick="mainform.select_img.value='<?=$img_id?>';mainform.submit();">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <?
                    }
                    ?>
                    </tr>
                    </table>
                </td></tr>
            </table>
        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
</table>
<!-- 本体 END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?



//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

