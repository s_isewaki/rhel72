<?
$forPDF = false ;
$wkst ;
if ($_REQUEST["direct_template_pdf_print_requested"])
{
  $forPDF = true ;
  $wkst   = new Wkst() ;
  ob_start() ;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">

<style type="text/css" media="all">
  textarea {overflow:hidden; }
  .date_header { background-color:#ffdddb; border:1px solid #ff9a95; padding:2px; margin:2px; margin-bottom:0px; line-height:1.1; color:#600; }
  .c_red { color:#f05 }
  .apply_header { border: 1px solid #c9db71; background-color:#f0f8c9; }
  .timecalcullator table{ border:1px solid #aaa; background-color:#ddd; }
  .timecalcullator td { border:1px solid #aaa; text-align:center; width:20px; padding:2px 4px; background-color:#fff; cursor:pointer; color:#00f }
  .plane { border:0; border-collapse:collapse; }
  .plane td { border:0; }
<? if (@$_REQUEST["print_preview"]) {?>
  .print_preview_hidden { display:none; }
<? } ?>
</style>
<? if (@$_REQUEST["print_preview"]) {?>
<style type="text/css" media="print">
  html { font-size:12px; }
  .print_color_black { color:#000; }
  .print_target_table td { font-size:60% }
  .print_target_table th { font-size:60% }
  .print_hidden { display:none; }
  .list td {border:#000 solid 1px;}
  .date_header { border: 1px solid #777; background-color:#fff; }
  .apply_header { border: 1px solid #777; background-color:#fff; }
</style>
<? } ?>

<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

//ログイン職員ＩＤ
$emp_id = get_emp_id($con, $session, $fname);

// ログ
summary_common_write_operate_log("access", $con, $fname, $session, $emp_id, "", "");

//=================================================
// 初期表示準備１  対象日付がない場合は本日を設定
//=================================================
if(@$_REQUEST["apply_date_default"] == "on") list($date_y, $date_m, $date_d) = split("/", date("Y/m/d", strtotime("today")));
$date_m = (int)@$date_m <10 ? "0".(int)@$date_m : $date_m;
$date_d = (int)@$date_d <10 ? "0".(int)@$date_d : $date_d;
$target_yyyymmdd = $date_y. $date_m . $date_d;
if ($forPDF) $wkst->dtStr = $date_y . "年" . (int)$date_m . "月" . (int)$date_d . "日" ;

//=================================================
// 初期表示準備２  病棟などが指定されていなければデフォルトを設定する
//=================================================
if (@$_REQUEST["apply_date_default"] && @$sel_bldg_cd=="" && @$sel_ward_cd==""){
  $sql =
    " select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
    " inner join empmst emp on (".
    "   emp.emp_class = swer.class_id".
    "   and emp.emp_attribute = swer.atrb_id".
    "   and emp.emp_dept = swer.dept_id".
    "   and emp.emp_id = '".pg_escape_string($emp_id)."'".
    " )";
  $emp_ward = @$c_sot_util->select_from_table($sql);
  $_REQUEST["sel_bldg_cd"] = @pg_fetch_result($emp_ward, 0, "bldg_cd");
  $_REQUEST["sel_ward_cd"] = @pg_fetch_result($emp_ward, 0, "ward_cd");
  $sel_bldg_cd = $_REQUEST["sel_bldg_cd"];
  $sel_ward_cd = $_REQUEST["sel_ward_cd"];
}
$sel_team_id = (int)@$_REQUEST["sel_team_id"];



$THIS_WORKSHEET_ID = "1"; // [sotmst][sot_id]の固定値


$sql = "select bldg_name from bldgmst where bldg_cd = " . (int)@$_REQUEST["sel_bldg_cd"];
$sel = $c_sot_util->select_from_table($sql);
$bldg_name = @pg_fetch_result($sel, 0, "bldg_name");

$sql =
  " select ward_name from wdmst".
  " where bldg_cd = " . (int)@$_REQUEST["sel_bldg_cd"].
  " and ward_cd = " . (int)@$_REQUEST["sel_ward_cd"];
$sel = $c_sot_util->select_from_table($sql);
$ward_name = @pg_fetch_result($sel, 0, "ward_name");

?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | ワークシート</title>
<? write_yui_calendar_use_file_read_0_12_2();// 外部ファイルを読み込む ?>
<? write_yui_calendar_script2(1);// カレンダー作成、関数出力 ?>


<script type="text/javascript">
<?//**************************************************************************?>
<?// JavaScript１  オーダ関連                                                 ?>
<?//**************************************************************************?>
function cellMouseOver(cell) {
  if (cell.getElementsByTagName('div').length > 0) {
    return;
  }
  cell.style.backgroundColor = "#fefc8f";
  cell.style.cursor = "pointer";
}

function cellMouseOut(cell) {
  if (cell.getElementsByTagName('div').length > 0) {
    return;
  }
  cell.style.backgroundColor = "#fff";
  cell.style.cursor = "default";
}

function popupRegisterWindow(cell, tmpl_id, ptif_id, ymd) {
  if (PopupRegisterCancel) {
    PopupRegisterCancel = false;
    return;
  }
  if (cell.getElementsByTagName('div').length > 0) {
    return;
  }
  window.open('sum_application_apply_new.php?session=<? echo($session); ?>&tmpl_id=' + tmpl_id + '&ptif_id=' + ptif_id + '&ymd=' + ymd, '', 'directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=600,height=700');
}

function divMouseOver(div) {
  div.style.backgroundColor = "#fefc8f";
  div.style.cursor = "pointer";
}

function divMouseOut(div) {
  div.style.backgroundColor = "#fff";
  div.style.cursor = "default";
}

/****************************************************************
 * popupRegisterWindowWithParam()
 * 入力済内容をコピーして、修正登録画面を開く
 *
 * 引　数：tmpl_id テンプレートID
 *         ptif_id 患者ID
 *         ymd 対象日付
 *         apply_id 承認連番ID
 * 戻り値：なし
 * 
 * 2012/04/18 追加
 ****************************************************************/
function popupRegisterWindowWithParam(tmpl_id, ptif_id, ymd, apply_id) {
    window.open('sum_application_apply_new.php?session=<? echo($session); ?>&tmpl_id=' + tmpl_id + '&ptif_id=' + ptif_id + '&ymd=' + ymd + '&apply_id=' + apply_id + "&worksheet_date=<? echo $target_yyyymmdd; ?>",
             '', 'directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=600,height=700');
}

function popupDetailWindow(apply_id){
  if (PopupDetailCancel) {PopupDetailCancel = false; return; }
  window.open(
    "sum_application_approve_detail.php?session=<?=$session?>&apply_id="+apply_id+
    "&worksheet_date=<?=@$target_yyyymmdd?>",
    "approvewin",
    "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1024,height=700"
  );
  cancelpopupRegister();
}
var PopupDetailCancel = false;
function cancelPopupDetail(){
  PopupDetailCancel = true;
}

var PopupRegisterCancel = false;    // 登録画面のポップアップ抑止

/****************************************************************
 * cancelpopupRegister()
 * 登録画面のポップアップを抑止する
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/05/21 追加
 ****************************************************************/
function cancelpopupRegister() {
  PopupRegisterCancel = true;
}

/****************************************************************
 * onMealCommentUpdateOKClick()
 * 食事コメントリンククリック時の処理
 *
 * 引　数：tmpl_id テンプレートID
 *         ptif_id 患者ID
 * 戻り値：なし
 * 
 * 2012/05/21 追加
 ****************************************************************/
function onMealCommentUpdateOKClick(ptif_id)
{
  var comment = document.getElementById('meal_comment_text').value;
  document.getElementById('ptif_id').value = ptif_id;
  document.getElementById('meal_comment').value = comment;
  document.getElementById('mode').value = 'update_meal_comment';
  document.search.submit();
}
 
<? // 印刷ダイアログ用にチームリレーションをJavaScriptで出力、事業所と病棟は固定 ?>
var teams = {};
<? $relations = array(); ?>
<? $sql = "select * from sot_ward_team_relation where bldg_cd = ".(int)@$_REQUEST["sel_bldg_cd"]." and ward_cd = ".(int)@$_REQUEST["sel_ward_cd"]; ?>
<? $sel = $c_sot_util->select_from_table($sql); ?>
<? while($row = pg_fetch_array($sel)) $relations[$row["team_id"]][] = $row["ptrm_room_no"]; ?>
<? foreach ($relations as $team_id => $rows) echo 'teams["'.$team_id.'"] = ["'.implode('","',$rows).'"];'."\n"; ?>

function showPrintTargetDialog(){
  <? if ($ward_name==""){ ?>
  alert("病棟を選択してください。");return;
  <? } ?>

  var out = [];
  var idx = 0;
  for(var i in room_selections){
    if (room_selections[i].bldg_cd != "<?=@$sel_bldg_cd?>") continue;
    if (room_selections[i].ward_cd != "<?=@$sel_ward_cd?>") continue;
    idx++;
    out[out.length] = "<label style='font-size:14px;'>";
    out[out.length] = "<input type='checkbox' style='padding:0; vertical-align:middle; margin-bottom:4px; margin-right:2px;' id='print_room_no_"+idx+"' name='print_room_no_"+idx+"'";
    out[out.length] = " value='"+room_selections[i].code+"' checked>";
    out[out.length] = room_selections[i].name+"</label>";
    out[out.length] = "\n<wbr>";
  }
  document.getElementById("print_target_selections").innerHTML = out.join("");

  var dlg = document.getElementById("print_target_dialog");
  dlg.style.left = (getClientWidth() / 2 - 150) + "px";
  dlg.style.top = ((window.parent.document.body.scrollTop || window.parent.document.documentElement.scrollTop) + 10) + "px";
  dlg.style.display = "";

  var team_idx = document.getElementById("sel_team_id").selectedIndex;
  document.getElementById("sel_print_team_id").selectedIndex = team_idx;
  if (team_idx > 0) teamSelected2(document.getElementById("sel_print_team_id").options[team_idx].value);
}

function setPrintTargetAll(is_check){
  document.getElementById("sel_print_team_id").selectedIndex = 0;
  var idx = 0;
  for (;;){
    idx++;
    var obj = document.getElementById("print_room_no_"+idx);
    if (!obj) break;
    obj.checked = is_check;
  }
}

function teamSelected2(v){
  var team = teams[v];
  if (!team) team = {};
  var i = 0;
  for (;;){
    i++;
    var obj = document.getElementById("print_room_no_"+i);
    if (!obj) break;
    var chk = false;
    for (var j in team) if (team[j] == obj.value) { chk = true; break; }
    obj.checked = chk;
  }
}

function popupPrintPreview(){
  var room_no_list = [];
  var idx = 0;
  for (;;){
    idx++;
    var obj = document.getElementById("print_room_no_"+idx);
    if (!obj) break;
    if (!obj.checked) continue;
    room_no_list[room_no_list.length] = obj.value;
  }
  if (!room_no_list.length) {
    alert("病室を選択してください。");
    return;
  }
//  window.open(
//    "sot_worksheet.php?session=<?=$session?>&date_y=<?=@$date_y?>&date_m=<?=$date_m?>&date_d=<?=$date_d?>&sel_bldg_cd=<?=@$sel_bldg_cd?>&sel_ward_cd=<?=@$sel_ward_cd?>&print_room_no_list="+room_no_list.join(",")+"&print_preview=y",
//    "worksheet_print",
//    "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1024,height=700"
//  );

  document.frm_pdf_print.print_room_no_list.value = room_no_list.join(",") ;
  document.frm_pdf_print.submit() ;
}


<?//**************************************************************************?>
<?// JavaScript２  受付実施と電卓関連                                         ?>
<?//**************************************************************************?>
var apply_dialog_calobj = null;<?//受付・実施ダイアログの電卓を押したときの入力先 ?>
var apply_dialog_calc_refreshed = false;<?//受付・実施ダイアログの中の電卓が表示されたときたときtrue、電卓を押したらfalse?>
<?//--------------------------------------------?>
<?// 受付実施ダイアログを隠す                   ?>
<?//--------------------------------------------?>
function hideApplyDialog(){
  document.getElementById('apply_dialog').style.display = "none";
}

<?//--------------------------------------------
  // 指定されたidのダイアログを隠す
  //--------------------------------------------?>
function hideDialog(id){
  document.getElementById(id).style.display = "none";
}

<?//--------------------------------------------?>
<?// 時分秒のクリックイベント、電卓を表示する   ?>
<?//--------------------------------------------?>
function adjcalc(hms){
  document.getElementById("apply_dialog_td"+hms).appendChild(document.getElementById('apply_dialog_calculator'));
  var cap="時"; if(hms=="m")cap="分"; if(hms=="s")cap="秒";
  document.getElementById("apply_dialog_caption").innerHTML = cap;
  apply_dialog_calc_refreshed = true;
  apply_dialog_calobj = document.getElementById("apply_dialog_"+hms);
}
function calcf(hms){
  document.getElementById("apply_dialog_calculator").style.display = "";
  adjcalc(hms);
  apply_dialog_calobj.select();
}

<?//--------------------------------------------?>
<?// 電卓を隠す                                 ?>
<?//--------------------------------------------?>
function hidecalc(){document.getElementById('apply_dialog_calculator').style.display='none';}
<?//--------------------------------------------?>
<?// 電卓のマウスオーバー、セル色をつける       ?>
<?//--------------------------------------------?>
function calc1(obj){ obj.style.backgroundColor = "#f90"; obj.style.color = "#fff"; }
<?//--------------------------------------------?>
<?// 電卓のマウスアウト、セル色を戻す           ?>
<?//--------------------------------------------?>
function calc2(obj){ obj.style.backgroundColor = "#fff"; obj.style.color = "#00f"; }
<?//--------------------------------------------?>
<?// 電卓のボタンクリック                       ?>
<?//--------------------------------------------?>
function calc(num){
  var ref = apply_dialog_calc_refreshed;
  var len = apply_dialog_calobj.value.length;
  if (num=="C"){ apply_dialog_calobj.value = ""; return; }
  if (num=="D"){
    if (len == 0) return;
    apply_dialog_calobj.value = apply_dialog_calobj.value.substring(0,len-1);
    apply_dialog_calc_refreshed = false;
    return;
  }
  if (ref){
    apply_dialog_calobj.value = num;
  } else {
    apply_dialog_calobj.value = apply_dialog_calobj.value + "" + num;
    if (len > 0) hidecalc();
  }
  apply_dialog_calc_refreshed = false;
}
<?//--------------------------------------------?>
<?// 受付実施ダイアログを開く                   ?>
<?//--------------------------------------------?>
var apply_dialog_mode = "";
var apply_dialog_global_element_index = "";
function getClientWidth(){
    if (document.documentElement && document.documentElement.clientWidth) return document.documentElement.clientWidth;
    if (document.body.clientWidth) return document.body.clientWidth;
    if (window.innerWidth) return window.innerWidth;
    return 0;
}
function showApplyDialog(mode, id){
  apply_dialog_mode = mode;
  apply_dialog_global_element_index = id;
  var dialog = document.getElementById("apply_dialog");
  var msg = (mode == "accept" ? "受付" : "実施");
  document.getElementById("apply_dialog_titlebar").innerHTML = msg;
  document.getElementById("apply_dialog_description").innerHTML = "上記を"+msg+"時刻として、"+msg+"を登録します。";
  var date = new Date();
  document.getElementById("apply_dialog_h").value = date.getHours();
  document.getElementById("apply_dialog_m").value = date.getMinutes();
  document.getElementById("apply_dialog_s").value = date.getSeconds();
  dialog.style.left = (getClientWidth() / 2 - 125) + "px";
  dialog.style.top = ((window.parent.document.body.scrollTop || window.parent.document.documentElement.scrollTop) + 10) + "px";
  dialog.style.display = "";
  document.getElementById("apply_dialog_ok").focus();
}

function cancelApply(mode, id){
  var msg = (mode == "accept" ? "受付" : "実施");
  if (!confirm(msg + "の登録を解除します。よろしいですか？")) return;
  inquiryRegist(mode, id, 1);
}

<?//--------------------------------------------?>
<?// 受付実施をサーバに登録する                 ?>
<?//--------------------------------------------?>
function registAcceptOperate(mode, global_element_index){
  inquiryRegist(mode, global_element_index, 0);
}

<?//**************************************************************************?>
<?// JavaScript３  AJAXサーバ登録関連（受付実施と評価メモ用                   ?>
<?//**************************************************************************?>
var ajaxObj = null;
function cnvtime(v, limit){
  if (!v.match(/^\s*[0-9]+\s*$/g)) return "";
  v = parseInt(v);
  if (isNaN(v)) return "";
  if (v > limit) return "";
  if (v < 0) return "";
  return v;
}
<?//--------------------------------------------?>
<?// サーバへ登録送信                           ?>
<?//--------------------------------------------?>
function inquiryRegist(mode, global_element_index, is_delete){
  var apply_id = document.getElementById("global_element_"+global_element_index+"_apply_id").value;
  var summary_seq = document.getElementById("global_element_"+global_element_index+"_summary_seq").value;
  var memo = "";
  var hms = "";
  if (!is_delete){
    if (mode=="accept" || mode=="operate"){
      var elemh = document.getElementById("apply_dialog_h");
      var elemm = document.getElementById("apply_dialog_m");
      var elems = document.getElementById("apply_dialog_s");
      elemh.value = cnvtime(elemh.value, 23);
      elemm.value = cnvtime(elemm.value, 59);
      elems.value = cnvtime(elems.value, 59);
      if (elemh.value=="" || elemm.value=="" || elems.value=="") {alert("有効な時刻を指定してください。"); return; }
      hms = elemh.value+":"+elemm.value+":"+elems.value;
      hideApplyDialog();
    }
    if (mode == "memo") {
      memo = document.getElementById("global_memo_"+global_element_index+"_cur").value;
    }
  }

  var postval =
    "ymd=<?=$target_yyyymmdd?>&apply_id="+apply_id+"&summary_seq="+summary_seq+"&mode="+mode+"&hms="+hms+
    "&global_element_index="+global_element_index+"&session=<?=$session?>"+
    "&is_delete="+is_delete+
    "&memo="+encodeURIComponent(encodeURIComponent(memo)); //文字化け回避のため２重掛け
  if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
  else {
    try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
    catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
  }
  ajaxObj.onreadystatechange = returnInquiryRegist;
  ajaxObj.open("POST", "sot_worksheet_apply.php", true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval);
}
<?//--------------------------------------------?>
<?// サーバからの戻りを取得して画面表示を変更   ?>
<?//--------------------------------------------?>
function returnInquiryRegist(){
  if (ajaxObj == null) return;
  if (ajaxObj.readyState != 4) return;
  if (typeof(ajaxObj.status) != "number") return;
  if (ajaxObj.status != 200) return;
  if (!ajaxObj.responseText) return;
  try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
  catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
  if (!ret) return;
  if (!ret.global_element_index) return;
  var elem = null;
  if (ret.is_accepted) {
    elem = document.getElementById("workflow_element_accept_"+ret.global_element_index);
    if (elem) {
      if (parseInt(ret.is_delete,10)>0){
        elem.innerHTML =
          '<input type="button" onclick="showApplyDialog(\'accept\',\''+ret.global_element_index+'\');"'+
          ' class="print_hidden" value="受付">';
      } else {
        elem.innerHTML =
          '<a href="" onclick="cancelApply(\'accept\',\''+ret.global_element_index+'\'); return false;"'+
          ' style="color:#880" class="print_color_black">[受]'+ret.emp_name+'</a>';
      }
    }
  }
  if (ret.is_operated) {
    elem = document.getElementById("workflow_element_operate_"+ret.global_element_index);
    if (elem) {
      if (parseInt(ret.is_delete,10)>0){
        elem.innerHTML =
          '<input type="button" onclick="showApplyDialog(\'operate\',\''+ret.global_element_index+'\');"'+
          ' class="print_hidden" value="実施">';
      } else {
        elem.innerHTML =
          '<a href="" onclick="cancelApply(\'operate\',\''+ret.global_element_index+'\'); return false;"'+
          ' style="color:#880" class="print_color_black">[施]'+ret.emp_name+'</a>';
      }
    }
  }
  if(ret.is_memo_registed){
    var cur = document.getElementById("global_memo_"+ret.global_element_index+"_cur").value;
    document.getElementById("global_memo_"+ret.global_element_index+"_org").value = cur;
    checkTextareaDiffAndColoring(ret.global_element_index);
    document.getElementById("global_memo_emp_name_"+ret.global_element_index).innerHTML = "[更]"+ret.emp_name;
    alert("保存しました。");
  }
  ajaxObj = null;
}

<?//--------------------------------------------
  // 食事コメント入力ダイアログを開く
  //--------------------------------------------?>
function showMealCommentDialog(ptif_id, id) {
  var dialog = document.getElementById("meal_comment_dialog");
  dialog.style.left = (getClientWidth() / 2 - 125) + "px";
  dialog.style.top = ((window.parent.document.body.scrollTop || window.parent.document.documentElement.scrollTop) + 10) + "px";
  dialog.style.display = "";
  comment = document.getElementById(id).innerHTML;
  document.getElementById("meal_comment_text").innerHTML = comment;
  document.getElementById("meal_comment_dialog_ok").onclick = function() { onMealCommentUpdateOKClick(ptif_id); };
  document.getElementById("meal_comment_dialog_ok").focus();
}
</script>
</head>
<body onload="document.getElementById('page_contents').style.display=''; <? if (@$_REQUEST["print_preview"]) {?>print();close();<?}else{ ?>initcal();<?}?>"<? if (@$_REQUEST["print_preview"]) {?>style="padding:1px"<? } ?>>

<div class="print_preview_hidden">

<? $param = "&date_y=".@$date_y."&date_m=".$date_m."&date_d=".$date_d."&sel_bldg_cd=".@$sel_bldg_cd."&sel_ward_cd=".@$sel_ward_cd."&sel_ptrm_room_no=".@$sel_ptrm_room_no."&sel_team_id=".$sel_team_id; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "ワークシート", $param); ?>

<? summary_common_show_main_tab_menu($con, $fname, "看護支援", 1); ?>


<script type="text/javascript">
<?//**************************************************************************?>
<?// JavaScript４  病棟病室ドロップダウン制御                                 ?>
<?//**************************************************************************?>
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
  combobox[1] = document.getElementById("sel_bldg_cd");
  combobox[2] = document.getElementById("sel_ward_cd");
  combobox[3] = document.getElementById("sel_ptrm_room_no");
  if (node < 3) combobox[3].options.length = 1;
  if (node < 2) combobox[2].options.length = 1;

  <? // 初期化用 自分コンボを再選択?>
  var cmb = combobox[node];
  if (node > 0 && cmb.value != selval) {
    for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
  }

  var idx = 1;
  if (node == 1){
    for(var i in ward_selections){
      if (ward_selections[i].bldg_cd != selval) continue;
      combobox[2].options.length = combobox[2].options.length + 1;
      combobox[2].options[idx].text  = ward_selections[i].name;
      combobox[2].options[idx].value = ward_selections[i].code;
      idx++;
    }
  }
  if (node == 2){
    for(var i in room_selections){
      if (room_selections[i].bldg_cd != combobox[1].value) continue;
      if (room_selections[i].ward_cd != selval) continue;
      combobox[3].options.length = combobox[3].options.length + 1;
      combobox[3].options[idx].text  = room_selections[i].name;
      combobox[3].options[idx].value = room_selections[i].code;
      idx++;
    }
  }
}

function searchByDay(day_incriment){
  var yy = document.search.date_y;
  var mm = document.search.date_m;
  var dd = document.search.date_d;
  var y = (parseInt(yy.value,10) ? yy.value : yy.options[1].value);
  var m = (parseInt(mm.value,10) ? mm.value : 1)*1;
  var d = (parseInt(dd.value,10) ? dd.value : 1)*1;
  var dt = new Date(y, m-1, d);
  dt.setTime(dt.getTime() + day_incriment * 86400000);

  var idx_y = yy.options[1].value - dt.getFullYear() + 1;
  if (idx_y < 1) return;
  yy.selectedIndex = idx_y;
  mm.selectedIndex = dt.getMonth()+1;
  dd.selectedIndex = dt.getDate();
  document.search.submit();
}

function searchByToday(){
  var dt = new Date();
  document.search.date_y.selectedIndex = document.search.date_y.options[1].value - dt.getFullYear() + 1;
  document.search.date_m.selectedIndex = dt.getMonth()+1;
  document.search.date_d.selectedIndex = dt.getDate();
  document.search.submit();
}


</script>
<?
$EMPTY_OPTION = '<option value="">　　　　</option>';





// チーム一覧
$team_options = array();
$sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel)){
  $team_options[] =
  $selected = ($row["team_id"] == $sel_team_id ? " selected" : "");
  $team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}


// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) {
  $selected = ($row["bldg_cd"] == @$_REQUEST["sel_bldg_cd"] ? " selected" : "");
  $bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$mst = $c_sot_util->getPatientsRoomInfo();
$ary = array();
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";

//=================================================
// 食事コメントの更新
//=================================================
if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'update_meal_comment') {
    $meal_comment = isset($_REQUEST['meal_comment']) ? $_REQUEST['meal_comment'] : '';
    $ptif_id = isset($_REQUEST['ptif_id']) ? $_REQUEST['ptif_id'] : '';
    if (!updateMealComment($con, $fname, $ptif_id, $meal_comment, $emp_id)) {
        // エラー処理
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script type='text/javascript'>showErrorPage(window);</script>");
        exit;
    }
}
?>

<form name="search" action="sot_worksheet.php?session=<? echo $session; ?>" method="get">

  <?//==========================================================================?>
  <?// 病棟などの検索条件                                                       ?>
  <?//==========================================================================?>
  <input type="hidden" name="session" value="<? echo($session); ?>">
  <table class="list_table" style="margin-bottom:8px; margin-top:4px" cellspacing="1">
    <tr>
      <th bgcolor="#fefcdf">事業所(棟)</th>
      <td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
      <th bgcolor="#fefcdf">病棟</th>
      <td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
      <th bgcolor="#fefcdf">病室</th>
      <td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no" onchange="document.search.submit();"><?=$EMPTY_OPTION?></select></td>
      <script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
      <script type="text/javascript">sel_changed(2, "<?=@$_REQUEST["sel_ward_cd"]?>");</script>
      <script type="text/javascript">sel_changed(3, "<?=@$_REQUEST["sel_ptrm_room_no"]?>");</script>
      <th bgcolor="#fefcdf">チーム</th>
      <td>
        <select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
      </td>
    </tr>
    <tr>
      <th bgcolor="#fefcdf">日付</th>
      <td colspan="7">
        <div style="float:left">
          <div style="float:left">
            <select id="date_y1" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>&nbsp;/
            <select id="date_m1" name="date_m"><? show_select_months($date_m, true); ?></select>&nbsp;/
            <select id="date_d1" name="date_d"><? show_select_days($date_d, true); ?></select>
          </div>
          <div style="float:left">
            <img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
          </div>
          <div style="float:left; padding-left:4px">
            <input type="button" onclick="searchByDay('-7');" value="≪" title="１週間前"/><!--
          --><input type="button" onclick="searchByDay('-1');" value="<" title="１日前"/><!--
          --><input type="button" onclick="searchByDay('1');" value=">" title="１日後"/><!--
          --><input type="button" onclick="searchByDay('7');" value="≫" title="１週間後"/>
          </div>
          <div style="float:left;">
            <input type="button" onclick="searchByToday();" value="本日"/>
          </div>
          <div style="clear:both"></div>
          <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
        </div>
        <div style="float:right">
          <input type="button" onclick="document.search.submit();" value="検索"/>
          <input type="button" onclick="showPrintTargetDialog();" value="印刷"/>
        </div>
        <div style="clear:both"></div>
      </td>
    </tr>
  </table>
  <? // 食事コメント欄更新時に利用するパラメータ ?>
  <input type="hidden" name="meal_comment" id="meal_comment">
  <input type="hidden" name="mode" id="mode">
  <input type="hidden" name="ptif_id" id="ptif_id">
  
<? summary_common_show_worksheet_tab_menu("ワークシート", $param); ?>


  <?//==========================================================================?>
  <?// 印刷対象選択ダイアログ                                                   ?>
  <?//==========================================================================?>
  <div id="print_target_dialog" style="position:absolute; width:300px; border:1px solid #1f97f5; border-top:1px solid #2b7cc6; background-color:#eee; font-size:12px; margin-top:4px; text-align:center; display:none">
    <div style="color:#fff; padding:3px; background-color:#1f97f5;">印刷対象病室</div>
    <div style="background-color:#eee; border:1px solid #999">
      <div style="margin:4px">
        <table class="list_table" cellspacing="1">
          <tr>
            <th style="width:25%; white-space:nowrap; text-align:center">事業所(棟)</th>
            <td style="width:25%;"> <?=$bldg_name ?></td>
            <th style="width:25%; white-space:nowrap; text-align:center">病棟</th>
            <td style="width:25%;"> <?=$ward_name ?></td>
          </tr>
        </table>
        <table class="list_table" style="margin-top:4px" cellspacing="1"><tr><td style="text-align:left">
          <div style="width:280px; padding:3px" id="print_target_selections"></div>
        </td></tr></table>
      </div>
      <div>
        <select onchange="teamSelected2(this.value)" name="sel_print_team_id" id="sel_print_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
        <a href="" onclick="setPrintTargetAll(1); return false;" class="always_blue">すべて選択</a>
        <a href="" onclick="setPrintTargetAll(0); return false;" class="always_blue">すべて解除</a>
      </div>
      <div style="background-color:#eee; margin-top:4px">
        <div style="padding:6px; padding-top:10px">
          <input type="button" value="OK" style="width:80px" onclick="popupPrintPreview();" />
          <input type="button" value="キャンセル" style="width:80px" onclick="document.getElementById('print_target_dialog').style.display='none';"/>
        </div>
      </div>
    </div>
  </div>
</form>

<form id="frm_pdf_print" name="frm_pdf_print" action="sot_worksheet.php" method="post" target="_blank">
 <input type="hidden" name="session" value="<?=$session?>" />
 <input type="hidden" name="date_y" value="<?=$date_y?>" />
 <input type="hidden" name="date_m" value="<?=$date_m?>" />
 <input type="hidden" name="date_d" value="<?=$date_d?>" />
 <input type="hidden" name="sel_bldg_cd" value="<?=@$sel_bldg_cd?>" />
 <input type="hidden" name="sel_ward_cd" value="<?=@$sel_ward_cd?>" />
 <input type="hidden" name="print_preview" value="y" />
 <input type="hidden" name="print_room_no_list" value="" />
 <input type="hidden" name="direct_template_pdf_print_requested" value="1" />
</form>

</div><!-- end of [class="print_preview_hidden"] -->

<div id="page_contents" style="display:none">

<?

//====================================
// スケジュール中の患者と病室を取得
// ・患者ごとに、どの期間を取得すべきかを求める
//   ・指定日に入院しているなら、入院開始予定日と終了予定日の間のスケジュールをとる
//   ・指定日に入院していないなら、直近の退院日からのスケジュールをとる。
//     ・直近の退院日もない場合は、システム稼動日からが求める期間となる。
//====================================
$sel = select_from_table($con, "select emp_id, emp_lt_nm, emp_ft_nm from empmst", "", $fname);
$empmst = array();
while ($row = pg_fetch_array($sel)){
  $empmst[$row["emp_id"]] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
}

$target_week = "";
if (checkdate((int)@$date_m, (int)$date_d, (int)$date_y)) {
  $target_ymd_time = mktime(0, 0, 0, (int)$date_m, (int)$date_d, (int)$date_y);
  $weeks_en = array("sun","mon","tue","wed","thu","fri","sat");
  $target_week = $weeks_en[date("w", $target_ymd_time)];
}

$where = "";

$team_inner_join = "";
if ($sel_team_id) {
  $team_inner_join =
    " inner join sot_ward_team_relation t on (".
    "   t.bldg_cd = i.bldg_cd and t.ward_cd = i.ward_cd and t.ptrm_room_no = i.ptrm_room_no".
    "   and team_id = " . $sel_team_id.
    " )";
}

if ((int)@$_REQUEST["sel_bldg_cd"] || $sel_team_id==0) { // チームがなければ必須指定
  $where .= " and i.bldg_cd = " .      (int)@$_REQUEST["sel_bldg_cd"];
}
if ((int)@$_REQUEST["sel_ward_cd"] || $sel_team_id==0) { // チームがなければ必須指定
  $where .= " and i.ward_cd = " .      (int)@$_REQUEST["sel_ward_cd"];
}
if (!@$_REQUEST["print_preview"]) {
  if (@$_REQUEST["sel_ptrm_room_no"]!="") $where .= " and i.ptrm_room_no = " . (int)$_REQUEST["sel_ptrm_room_no"];
} else {
  $print_room_no_list = explode(",", @$_REQUEST["print_room_no_list"]);
  for($i = 0; $i < count($print_room_no_list); $i++){
    $print_room_no_list[$i] = (int)$print_room_no_list[$i];
  }
  if (count($print_room_no_list)){
    $where .= " and i.ptrm_room_no in (" . implode(",", $print_room_no_list) . ")";
  }
}



// 対象日の入院中患者情報
$sql_nyuin_kanja_list =
  " select i.ptif_id, i.bldg_cd, i.ward_cd, i.ptrm_room_no, i.inpt_bed_no, i.inpt_sect_id".
  ",max(i.inpt_in_dt) as inpt_in_dt".
  ",'' as inpt_out_dt".
  " from (".
  " select i.ptif_id, i.bldg_cd, i.ward_cd, i.ptrm_room_no, i.inpt_bed_no, i.inpt_sect_id, i.inpt_in_dt, '' as inpt_out_dt".
  " from inptmst i".
  $team_inner_join.
  " where i.inpt_in_dt <= '" . $target_yyyymmdd . "'"." " . $where.
  " ) i".
  " group by i.ptif_id, i.bldg_cd, i.ward_cd, i.ptrm_room_no, i.inpt_bed_no, i.inpt_sect_id";

// 期間の取得
// 入院なら、スケジュール探索開始は入院日から後に登録されたもの
// 外来なら、スケジュール探索は、前回の退院から後に登録されたもの
$sql_schedule_term =
  " select".
  " pt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
  ",case when inpt.ptif_id is not null then 1 else 0 end as is_nyuin".
  ",inpt.inpt_in_dt as schedule_start_day".
  " from ptifmst pt".
  " left join (". $sql_nyuin_kanja_list . ") inpt on (inpt.ptif_id = pt.ptif_id)";

// 全スケジュール(小テンプレート単位)
$sql_schedule_all =
  " select".
  " str.sot_col_index".
  ",str.sot_tmpl_index".
  ",str.tmpl_file_name".
  ",sss.apply_id".
  ",sss.start_ymd".
  ",sss.update_seq".
  ",sss.schedule_seq".
  ",sss.tmpl_chapter_code".
  ",sss.tmpl_id".
  ",sss.summary_id".
  ",sss.summary_seq".
  ",sss.ptif_id as d_ptif_id".
  ",sss.xml_file_name".
  ",sss.assign_pattern".
  ",sss.assign_sun_flg".
  ",sss.assign_mon_flg".
  ",sss.assign_tue_flg".
  ",sss.assign_wed_flg".
  ",sss.assign_thu_flg".
  ",sss.assign_fri_flg".
  ",sss.assign_sat_flg".
  ",sss.times_per_ymd".
  ",sss.is_hide_worksheet_date".
  ",sss.is_hide_worksheet_apply".
  ",date_part('month',sss.regist_timestamp)||'/'||date_part('day',sss.regist_timestamp)||' '||".
  " date_part('hour',sss.regist_timestamp)||':'||to_char(sss.regist_timestamp, 'MI') as regist_timestamp".
  ",d.schedule_start_day".
  ",d.ptif_id".
  ",d.bldg_cd as d_bldg_cd".
  ",d.ward_cd as d_ward_cd".
  ",d.ptrm_room_no as d_ptrm_room_no".
  ",d.inpt_bed_no as d_inpt_bed_no".
  ",d.inpt_sect_id".
  " from sot_summary_schedule sss".
  " inner join sot_tmpl_relation str".
  " on (str.sot_id = ".$THIS_WORKSHEET_ID." and str.tmpl_file_name = sss.tmpl_file_name)".
  " inner join (". $sql_schedule_term .") d".
  " on (d.ptif_id = sss.ptif_id and d.schedule_start_day <= sss.start_ymd::text)".
  " where sss.start_ymd <= ". $target_yyyymmdd.
  " and sss.end_ymd >= ".$target_yyyymmdd.
  " and sss.temporary_update_flg = 0".
  " and sss.delete_flg = false";

$sql_schedule_all_with_master =
  " select".
  " d.*".
  ",ptif.ptif_lt_kaj_nm".
  ",ptif.ptif_ft_kaj_nm".
  ",bldg.bldg_name".
  ",wd.ward_name".
  ",ptrm.ptrm_name".
  ",tmpl.tmpl_name".
  ",sae.accepted_emp_id".
  ",sae.operated_emp_id".
  " from (". $sql_schedule_all .") d".
  " left join ptifmst ptif on (ptif.ptif_id = d.d_ptif_id)".
  " left join bldgmst bldg on (bldg.bldg_cd = d.d_bldg_cd)".
  " left join wdmst wd on (wd.bldg_cd = d.d_bldg_cd and wd.ward_cd = d.d_ward_cd)".
  " left join ptrmmst ptrm on (ptrm.bldg_cd = d.d_bldg_cd and ptrm.ward_cd = d.d_ward_cd and ptrm.ptrm_room_no = d.d_ptrm_room_no)".
  " left join sot_worksheet_apply sae on (sae.worksheet_date = ".$target_yyyymmdd." and sae.summary_seq = d.summary_seq)".
  " left join tmplmst tmpl on (tmpl.tmpl_file = d.tmpl_file_name)".
  " order by d_bldg_cd, d_ward_cd, d_ptrm_room_no, d_inpt_bed_no, d_ptif_id, sot_col_index, summary_seq, schedule_seq";


//$sql = "select i.bldg_cd, b.bldg_name, i.ward_cd, w.ward_name, i.ptrm_room_no, r.ptrm_name, i.ptif_id, p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm from inptmst i inner join bldgmst b on b.bldg_cd = i.bldg_cd inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no inner join ptifmst p on p.ptif_id = i.ptif_id $team_inner_join where i.ptif_id is not null and i.ptif_id <> '' $where order by bldg_cd, ward_cd, ptrm_room_no, ptif_id ";
//$sql = "select i.bldg_cd, b.bldg_name, i.ward_cd, w.ward_name, i.ptrm_room_no, r.ptrm_name, i.inpt_bed_no, i.ptif_id, p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm from inptmst i inner join bldgmst b on b.bldg_cd = i.bldg_cd inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no inner join ptifmst p on p.ptif_id = i.ptif_id $team_inner_join where i.ptif_id is not null and i.ptif_id <> '' $where order by bldg_cd, ward_cd, ptrm_name, ptrm_room_no, inpt_bed_no, ptif_id ";
$sql =
  " select" .
  " i.bldg_cd" .
  ",b.bldg_name" .
  ",i.ward_cd" .
  ",w.ward_name" .
  ",i.ptrm_room_no" .
  ",r.ptrm_name" .
  ",i.inpt_bed_no" .
  ",i.ptif_id" .
  ",p.ptif_lt_kaj_nm" .
  ",p.ptif_ft_kaj_nm" .
  ",ptmealcmt.comment" .
  " from inptmst i" .
  " inner join bldgmst b on b.bldg_cd = i.bldg_cd" .
  " inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd" .
  " inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no" .
  " inner join ptifmst p on p.ptif_id = i.ptif_id $team_inner_join" .
  " left join ptmealcmt on ptmealcmt.ptif_id = p.ptif_id" .
  " where i.ptif_id is not null and i.ptif_id <> '' $where" .
  " order by bldg_cd, ward_cd, ptrm_name, ptrm_room_no, inpt_bed_no, ptif_id ";
$sel = $c_sot_util->select_from_table($sql);
$sel_inpt_all = (array)pg_fetch_all($sel);
$schedule = array();
foreach ($sel_inpt_all as $key => $row) {
  $f1_bldg = $row["bldg_cd"];
  $f2_ward = $row["ward_cd"];
  $f3_room = $row["ptrm_room_no"];
  $f4_ptif = $row["ptif_id"];

  // [事業所]の名前一覧
  $schedule[$f1_bldg]["name"] = $row["bldg_name"];

  // [事業所ID]一覧に紐つく、[病棟]の名前一覧
  $schedule[$f1_bldg]["list"][$f2_ward]["name"] = $row["ward_name"];

  // [事業所ID]-[病棟ID]に紐つく、[病室]の名前一覧
  $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["name"] = $row["ptrm_name"];

  // [事業所ID]-[病棟ID]-[病室ID]に紐つく、[患者]の名前一覧
  $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["name"] = $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"];
//  $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["name"] = $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"]." (".$row["inpt_bed_no"].")";

  // [事業所ID]-[病棟ID]-[病室ID]に紐つく、[患者]の食事コメント一覧
  $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["meal_comment"] = $row["comment"];
}
unset($sel_inpt_all);

$sel = $c_sot_util->select_from_table($sql_schedule_all_with_master);
$sel_schedule_all = (array)pg_fetch_all($sel);
foreach($sel_schedule_all as $key => $row){
  $f1_bldg = $row["d_bldg_cd"];
  $f2_ward = $row["d_ward_cd"];
  $f3_room = $row["d_ptrm_room_no"];
  $f4_ptif = $row["d_ptif_id"];
  $f5_cols = $row["sot_col_index"];
  $f6_smry = $row["summary_seq"];
  $f7_wkfw = trim($row["tmpl_chapter_code"]);

  // [事業所ID]-[病棟ID]-[病室ID]-[患者ID]に紐つく、[指示オーダ]の初回登録日時の一覧
  $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["list"][$f5_cols]["list"][$f6_smry]["date_header"] = $row["regist_timestamp"] . "&nbsp;" . $row["tmpl_name"];

  // [事業所ID]-[病棟ID]-[病室ID]-[患者ID]に紐つく、[指示オーダ]の初回登録日の一覧
  $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["list"][$f5_cols]["list"][$f6_smry]["start_ymd"] = $row["start_ymd"];

  // [事業所ID]-[病棟ID]-[病室ID]-[患者ID]-[指示オーダ]に含まれる、チャプターデータ一覧
  $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["list"][$f5_cols]["list"][$f6_smry]["list"][$f7_wkfw]["data"] = $row;

  // 対象日より過去の登録データ（継続データ）は、ひとつだけ表示する。
  // 対象日のデータは、すべて表示させる。
  // [事業所ID]-[病棟ID]-[病室ID]-[患者ID]に紐つく、有効な最新のチャプターのユニークID
  if ((int)$row["start_ymd"] < (int)$target_yyyymmdd) {
    $schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["list"][$f5_cols]["latest_idx"][$f7_wkfw] = $row["schedule_seq"];
  }
  if($f7_wkfw == "mstp" && $row["times_per_ymd"] == 1){
	$schedule[$f1_bldg]["list"][$f2_ward]["list"][$f3_room]["list"][$f4_ptif]["list"][$f5_cols]["last_idx"]["mstp"] = $row["schedule_seq"];
  }
}
unset($sel_schedule_all);

$global_element_index = 0;

//====================================
// ワークシート・テンプレート構造取得
//====================================
//$sql = "select sot_cols.*, (select tmpllastuse.tmpl_id from tmpllastuse where tmpllastuse.emp_id = '$emp_id' and tmpllastuse.div_id = divmst.div_id limit 1) as tmpl_id1, (select tmpl_div_link.tmpl_id from tmpl_div_link where tmpl_div_link.div_id = divmst.div_id order by tmpl_div_link.tmpl_id limit 1) as tmpl_id2 from sot_cols left join divmst on sot_cols.diag_div = divmst.diag_div where sot_cols.sot_id = $THIS_WORKSHEET_ID order by sot_cols.sot_col_index";
//$sql = "select sot_cols.*, tmpl_div_link.tmpl_id from sot_cols left join divmst on sot_cols.diag_div = divmst.diag_div left join (select tmpl_id, div_id from tmpl_div_link where default_flg) tmpl_div_link on divmst.div_id = tmpl_div_link.div_id where sot_cols.sot_id = $THIS_WORKSHEET_ID order by sot_cols.sot_col_index";
$sql =  "select sot_cols.*, tmpl_div_link.tmpl_id, tmplmst.tmpl_file" .
        " from sot_cols" .
        " left join divmst on sot_cols.diag_div = divmst.diag_div" .
        " left join (select tmpl_id, div_id from tmpl_div_link where default_flg) tmpl_div_link on divmst.div_id = tmpl_div_link.div_id" .
        " left join tmplmst on tmplmst.tmpl_id = tmpl_div_link.tmpl_id" .
        " where sot_cols.sot_id = $THIS_WORKSHEET_ID" .
        " order by sot_cols.sot_col_index";
$sel = $c_sot_util->select_from_table($sql);
$sot_cols_count = pg_num_rows($sel);
$sot_cols_width = floor((100-10) / $sot_cols_count); // 幅100%から最初の列の7%を差し引いて、個別の列幅を算出
$sot_cols = pg_fetch_all($sel);
?>

<? if (!@$_REQUEST["sel_ward_cd"] && !$sel_team_id) { ?>
  <div style="padding:10px; color:#aaa; text-align:center">病棟またはチームを指定してください。</div>
<? } ?>
<? //-------------------------------------------------- ?>
<? // 事業所・病棟・病室・患者のループ                  ?>
<? //-------------------------------------------------- ?>
<? foreach ($schedule as $bldg_cd => $bldg_data){ ?><? // 事業所のループ ?>
<? $page_count = count($bldg_data["list"]); ?><? // 改ページ位置確認用カウンタ ?>
<?
if ($forPDF)
{
  $bldg = new Bldg() ;
  $bldg->name = $bldg_data["name"] ;
}
?>
<? foreach ($bldg_data["list"] as $ward_cd => $ward_data){ ?><? // 病棟のループ ?>
<? $paging_style = ' style="page-break-after:always;"'; ?><? // 病棟の最初の行は後続ページを改ページさせる ?>
<? $page_count--; if($page_count==0) $paging_style = ""; ?><? // 改ページカウントのデクリメント、最後の病棟は改ページなし ?>
<?
if ($forPDF)
{
  $ward = new Ward() ;
  $ward->name = $ward_data["name"] ;
}
?>
<? foreach ($ward_data["list"] as $room_cd => $room_data){ ?><? // 病室のループ ?>
<table class="listp_table print_target_table" cellspacing="1" <?=$paging_style?>>
<? $paging_style = ""; ?>
<?
if ($forPDF)
{
  $room = new Room() ;
  $room->name = $room_data["name"] ;
}
?>

<? //----------------------------------- ?>
<? // ヘッダ１、部屋番号と日付の行       ?>
<? //----------------------------------- ?>
  <tr bgcolor="#f6f9ff">
    <th colspan="<?= $sot_cols_count + 1 ?>" style="padding:0">
      <div style="float:left; width:150px; padding:2px"><?= $room_data["name"]?></div>
      <div style="float:left; padding:2px;"><?=(int)$date_m?>&nbsp;月&nbsp;&nbsp;<?=(int)$date_d?>&nbsp;日</div>
      <span style="clear:both"></span>
    </th>
  </tr>

<? $echo_header = true; ?>
<? foreach ($room_data["list"] as $ptif_id => $ptif_data){ ?><? // 患者のループ ?>
<?   if (!$ptif_id) continue; ?>
<?
if ($forPDF)
{
  $ptif = new Ptif() ;
}
?>

<? //----------------------------------- ?>
<? // ヘッダ２、診療区分名の行           ?>
<? //----------------------------------- ?>
<?   if ($echo_header) { ?>
  <tr bgcolor="#f6f9ff">
    <th width="10%" align="center" style="padding:2px 1px">氏名</th>
    <? foreach($sot_cols as $key => $row){ ?>
    <th width="<?=$sot_cols_width?>%" style="text-align:center; padding:2px 1px"><?=$row["diag_div"]?></th>
    <? } ?>
  </tr>
<?     $echo_header = false; ?>
<?   } ?>

<? //----------------------------------- ?>
<? // テンプレートの実体の表示領域行     ?>
<? //----------------------------------- ?>
  <tr>

    <? // 一列目は患者情報 ?>
    <td style="vertical-align:top"><?=$ptif_data["name"]?></td>
<?
if (function_exists("curl_multi_init")) {
	$params = array();
	$col_index = 0;
	$tmp_global_element_index = $global_element_index;
	foreach ($sot_cols as $key => $row) {
		$col_index++;
		if ($row["sot_col_index"] != $col_index) continue;

		$cols_data = (array)@$ptif_data["list"][$col_index];
		if (!$cols_data) continue;

		$cols_latest_template_idx_list = (array)@$cols_data["latest_idx"];

		foreach ($cols_data["list"] as $summary_seq => $smry_data) {
			$enable_tmpl_chapter_codes = "";
			$daihyou_wkfw = null;
			$tmp_global_element_index++;

			foreach ($smry_data["list"] as $wkfw_idx => $wkfw_data) {
				$wkfw = $wkfw_data["data"];
				$tmpl_chapter_code = trim($wkfw["tmpl_chapter_code"]);

				if ($tmpl_chapter_code == "mstp") {
					if (@$cols_data["last_idx"]["mstp"] != $wkfw["schedule_seq"]) continue;
				}

				if ((int)$wkfw["start_ymd"] < (int)$target_yyyymmdd) {
					if ($wkfw["assign_pattern"] == "WEEK_OVERRIDE" || $wkfw["assign_pattern"] == "OVERRIDE") {
						if (@$cols_latest_template_idx_list[trim($tmpl_chapter_code)] != $wkfw["schedule_seq"]) continue;
					}
				}

				if (!$daihyou_wkfw) $daihyou_wkfw = $wkfw;
				if ($wkfw["times_per_ymd"]) {
					if ($wkfw["assign_pattern"] == "WEEK_OVERRIDE") {
						if ($wkfw["assign_".$target_week."_flg"]) {
							$enable_tmpl_chapter_codes .= "&enable_tmpl_chapter_codes[]=".trim($wkfw["tmpl_chapter_code"]);
						}
					} else {
						$enable_tmpl_chapter_codes .= "&enable_tmpl_chapter_codes[]=".trim($wkfw["tmpl_chapter_code"]);
					}
				}
			}

			if ($enable_tmpl_chapter_codes == "") continue;

			if ($daihyou_wkfw["tmpl_file_name"] && $daihyou_wkfw["xml_file_name"]) {
				$param =
					"wrap_template_file_name=".$daihyou_wkfw["tmpl_file_name"].
					"&view_worksheet=1".
					"&xml_file=".$daihyou_wkfw["xml_file_name"].
					"&target_ymd=".@$date_y."/".$date_m."/".$date_d.
					"&sot_id=".$THIS_WORKSHEET_ID.
					"&sot_col_index=".$col_index.
					"&summary_seq=".$summary_seq.
					"&apply_id=".$daihyou_wkfw["apply_id"].
					"&global_element_index=".$tmp_global_element_index.
					$enable_tmpl_chapter_codes;
				$params[] = $param;
			}
		}
	}

	if (!isset($htmls)) $htmls = array();
	if (count($params) > 0) {
		$htmls = array_merge($htmls, $c_sot_util->url_get_template_contents_multi($params));
	}
}
?>
    <? $col_index = 0; ?>
<?
if ($forPDF)
{
  $ptif->diagDivs[0] = $ptif_data["name"] ;
}
?>
    <? foreach($sot_cols as $key => $row){ ?>
    <?    $col_index++; ?>
    <?    if ($row["sot_col_index"] != $col_index){ echo "<td></td>"; continue; } ?>

<?
          // データが存在しない場合、新規登録できるようにする
          echo("<td style=\"vertical-align:top\" onmouseover=\"cellMouseOver(this);\" onmouseout=\"cellMouseOut(this);\" onclick=\"popupRegisterWindow(this, '{$row["tmpl_id"]}', '$ptif_id', '$target_yyyymmdd');\">");

          // 列データ、すなわち指示オーダの一覧情報
          $cols_data = (array)@$ptif_data["list"][$col_index];
          
          if (!$cols_data) {
            $diagDiv = '';

            // 食事コメント
            if (isMealTmpl($row['tmpl_file'])) {
              $meal_comment = $ptif_data['meal_comment'];
              dispMealCommentText($ptif_id, $meal_comment);
              $diagDiv = '[コメント]' . $meal_comment;
            }
if ($forPDF)
{
  $ptif->diagDivs[count($ptif->diagDivs)] = $diagDiv;
}
            echo '</td>';
            continue;
          }

          // その列の、チャプターごとの最新ID情報
          $cols_latest_template_idx_list = (array)@$cols_data["latest_idx"];

          $enable_tmpl_chapter_codes = "";
          $is_first_data = true;

if ($forPDF)
{
  $html2 = "" ;
}         
          // 対象データの全サマリシーケンス番号から、最新のサマリシーケンス番号を取得
          $summary_seq_latest = max(array_keys($cols_data["list"]));    // サマリシーケンス番号の最大値 = 最新のサマリシーケンス番号
          foreach ($cols_data["list"] as $summary_seq => $smry_data){

            // サマリオーダ単位で、テンプレートは登録されている。このため、サマリ単位でテンプレートを呼び出す。ただし、
            // １つのテンプレートには、複数のワークフローが含まれているかもしれない。
            // テンプレートに対し、どのワークフロー部分を表示するかを判断、抽出する。
            $enable_tmpl_chapter_codes = "";
            $daihyou_wkfw = null;
            $is_exist_latest_past_data = false;
            $global_element_index++;
            foreach ($smry_data["list"] as $wkfw_idx => $wkfw_data){
              $wkfw = $wkfw_data["data"];
              $tmpl_chapter_code = trim($wkfw["tmpl_chapter_code"]);

            //テンプレートが過去の最新でなければスキップする。
            if($tmpl_chapter_code == "mstp"){
                if (@$cols_data["last_idx"]["mstp"] != $wkfw["schedule_seq"]) continue;
            }
              // テンプレートが本日発行なら、表示させる。
              // テンプレートが対象日を除いて過去に発行されているが、過去の最新でなければスキップする。
              // そうでなければ、表示対象候補である。
              if ((int)$wkfw["start_ymd"] < (int)$target_yyyymmdd) {
                if ($wkfw["assign_pattern"] == "WEEK_OVERRIDE" || $wkfw["assign_pattern"] == "OVERRIDE"){
                  if (@$cols_latest_template_idx_list[trim($tmpl_chapter_code)] != $wkfw["schedule_seq"]) continue;
                  $is_exist_latest_past_data = true;
                }
              }
              // 区分が"WEEK_OVERRIDE"なら、対象日がその曜日かどうかを確認する。それ以外は表示。
              if (!$daihyou_wkfw) $daihyou_wkfw = $wkfw;
              if ($wkfw["times_per_ymd"]){
                if ($wkfw["assign_pattern"] == "WEEK_OVERRIDE") {
                  if ($wkfw["assign_".$target_week."_flg"]) {
                    $enable_tmpl_chapter_codes .= "&enable_tmpl_chapter_codes[]=".trim($wkfw["tmpl_chapter_code"]);
                  }
                } else {
                  $enable_tmpl_chapter_codes .= "&enable_tmpl_chapter_codes[]=".trim($wkfw["tmpl_chapter_code"]);
                }
              }
            }

            if ($enable_tmpl_chapter_codes == "") continue;

            // テンプレートスクリプトHTMLを、URL越しに取得
            if ($daihyou_wkfw["tmpl_file_name"] && $daihyou_wkfw["xml_file_name"]) {
              $param =
                "wrap_template_file_name=".$daihyou_wkfw["tmpl_file_name"].
                "&view_worksheet=1".
                "&xml_file=".$daihyou_wkfw["xml_file_name"].
                "&target_ymd=".@$date_y."/".$date_m."/".$date_d.
                "&sot_id=".$THIS_WORKSHEET_ID.
                "&sot_col_index=".$col_index.
                "&summary_seq=".$summary_seq.
                "&apply_id=".$daihyou_wkfw["apply_id"].
                "&global_element_index=".$global_element_index.
                $enable_tmpl_chapter_codes;
              if (function_exists("curl_multi_init")) {
                $html = trim($htmls[$param]);
              } else {
                $html = trim($c_sot_util->url_get_template_contents($param));
              }
            } else {
              $html = "";
            }

            // コメントだけのデータはデータがないものとみなす
            if (mb_ereg_match('\A<!--[^<]*-->\z', $html)) $html = "";

if ($forPDF)
{
  if ($is_first_data) $html2 .= $html ;
  else                $html2 .= ("@hr@" . $html) ;
}

            //--------------------------
            // ここから描画
            //--------------------------
            if ($daihyou_wkfw["apply_id"] && !@$_REQUEST["print_preview"]) {
              if ($html){
                echo "<div onmouseover=\"divMouseOver(this);\" onmouseout=\"divMouseOut(this);\" onclick=\"popupDetailWindow('".$daihyou_wkfw["apply_id"]."');\">";
              }
            }

            //--------------------------
            // 初回登録日とテンプレート名
            //--------------------------
            if (!$is_first_data) {
              if (!$daihyou_wkfw["is_hide_worksheet_date"]){
                echo '<div class="date_header">'.$smry_data["date_header"]."</div>";

if ($forPDF)
{
  $html2 = str_replace("@hr@", $smry_data["date_header"] . "@hr@", $html2) ;
}

              } else if ($html !="") {
                echo '<div style="border-top:1px solid #BAD9F2; margin-left:4px; margin-right:4px"><img src="img/spacer.gif" height="4" /></div>';
              }
            }
            echo '<input type="hidden" id="global_element_'.$global_element_index.'_apply_id" value="'.$daihyou_wkfw["apply_id"].'"/>';
            echo '<input type="hidden" id="global_element_'.$global_element_index.'_summary_seq" value="'.$summary_seq.'"/>';

            //--------------------------
            //受付実施状態
            //--------------------------
            if (!$daihyou_wkfw["is_hide_worksheet_apply"]){
              $print_hidden = "";
              if (!$daihyou_wkfw["accepted_emp_id"] && !$daihyou_wkfw["operated_emp_id"]) $print_hidden = " print_hidden";
              echo '<div style="padding-top:2px;">';
              echo '<div style="text-align:right; padding:1px; margin:2px; cursor:default;" class="apply_header'.$print_hidden.'"';
              echo ' onclick="cancelPopupDetail(); cancelpopupRegister()">';

              // 新規ボタン描画(ケア・処置/排泄/移動・移乗/環境・備考)
              if (isCreateNewButton($daihyou_wkfw['tmpl_file_name'], $summary_seq, $summary_seq_latest)) {
                  createNewButton($global_element_index, $daihyou_wkfw["tmpl_id"], $ptif_id, $target_yyyymmdd, $daihyou_wkfw["apply_id"]);
              }
              
              echo '<span id="workflow_element_accept_'.$global_element_index.'">';
              if ($daihyou_wkfw["accepted_emp_id"]){
                if ($emp_id == $daihyou_wkfw["accepted_emp_id"]){
                  echo '<a href="" onclick="cancelApply(\'accept\',\''.$global_element_index.'\'); return false;"';
                  echo ' style="color:#880" class="print_color_black">[受]'.@$empmst[$daihyou_wkfw["accepted_emp_id"]].'</a>';
                } else {
                  echo '<span style="color:#880" class="print_color_black">[受]'.@$empmst[$daihyou_wkfw["accepted_emp_id"]].'</span>';
                }
              } else {
                echo '<input type="button" onclick="showApplyDialog(\'accept\',\''.$global_element_index.'\');"';
                echo ' class="print_hidden" value="受付"/>';
              }


              echo "</span><wbr>";
              echo '&nbsp;<span id="workflow_element_operate_'.$global_element_index.'">';
              if ($daihyou_wkfw["operated_emp_id"]){
                if ($emp_id == $daihyou_wkfw["operated_emp_id"]){
                  echo '<a href="" onclick="cancelApply(\'operate\',\''.$global_element_index.'\'); return false;"';
                  echo ' style="color:#880" class="print_color_black">[施]'.@$empmst[$daihyou_wkfw["operated_emp_id"]].'</a>';
                } else {
                  echo '<span style="color:#880" class="print_color_black">[施]'.@$empmst[$daihyou_wkfw["operated_emp_id"]].'</span>';
                }
              } else {
                echo '<input type="button" onclick="showApplyDialog(\'operate\',\''.$global_element_index.'\');"';
                echo ' class="print_hidden" value="実施"/>';
              }
              echo "</span>";
              echo "</div></div>";
            } else {
              // 新規ボタン描画(ケア・処置/排泄/移動・移乗/環境・備考)
              if (isCreateNewButton($daihyou_wkfw['tmpl_file_name'], $summary_seq, $summary_seq_latest)) {
                echo '<div style="padding-top:2px;">';
                echo '<div style="text-align:right; padding:1px; margin:2px; cursor:default;" class="apply_header'.$print_hidden.'"';
                echo ' onclick="cancelPopupDetail(); cancelpopupRegister();">';
                createNewButton($global_element_index, $daihyou_wkfw["tmpl_id"], $ptif_id, $target_yyyymmdd, $daihyou_wkfw["apply_id"]);
                echo '</div></div>';
              }
            }
            echo $html;

            // 少しでもデータが書き出されたマーク
            if ($html!="") $is_first_data = false;

            // もし表示内容がなければ・・・
            // 過去データなら、無視
            // 本日データなら、空登録である。指定なしを表示する。⇒やめた
//            if ($html=="" && !$is_exist_latest_past_data) echo "(指示内容クリア)";

            if ($daihyou_wkfw["apply_id"] && !@$_REQUEST["print_preview"]) {
              if ($html) {
                echo '</div>';
              }
            }
          }

          // 食事コメント
          if (isMealTmpl($daihyou_wkfw['tmpl_file_name'])) {
              $meal_comment = $ptif_data['meal_comment'];
              dispMealCommentText($ptif_id, $meal_comment);
              $html2 .= '[コメント]' . $meal_comment;
          }
          
if ($forPDF)
{
  $ptif->diagDivs[count($ptif->diagDivs)] = $html2;
}
?>
    </td>
    <? } ?>
  </tr>

<?
if ($forPDF)
{
  $room->ptifs[count($room->ptifs)] = $ptif ;
}
?>
<? } ?><? // 患者ループおわり ?>
</table>
<br/>

<?
if ($forPDF)
{
  $ward->rooms[count($ward->rooms)] = $room ;
}
?>
<? } ?><? // 病室ループおわり ?>
<?
if ($forPDF)
{
  $bldg->wards[count($bldg->wards)] = $ward ;
}
?>
<? } ?><? // 病棟ループおわり ?>
<?
if ($forPDF)
{
  $wkst->bldgs[count($wkst->bldgs)] = $bldg ;
}
?>
<? } ?><? // 事業所ループおわり ?>


<?//==========================================================================?>
<?// 承認ダイアログ                                                           ?>
<?//==========================================================================?>
<div id="apply_dialog" style="position:absolute; width:250px; border:1px solid #1484dc; border-top:1px solid #1273c2; background-color:#f4f4f4; font-size:12px; margin-top:4px; text-align:center; display:none;">
  <div style="color:#fff; padding:3px; background-color:#2a9cf4;" id="apply_dialog_titlebar">受付</div>
  <div style="background-color:#f4f4f4; border:1px solid #999">
    <div style="margin:4px">
      <div style="background:url(css/img/bg-b-gura.gif) top repeat-x; border:1px solid #bad9f2">
        <div style="padding:2px">
          <b><?=$date_y?></b><span style="padding:2px">年</span><b><?=(int)$date_m?></b><span style="padding:2px">月</span><b><?=(int)$date_d?></b><span style="padding:2px">日</span>
        </div>
        <div style="padding:4px;">
          <table style="border:0; border-collapse:collapse" class="plane"><tr>
            <td style="width:20%"></td>
            <td style="width:10%"><input type="text" style="width:30px; ime-mode:disabled" maxlength="2" id="apply_dialog_h" onfocus="adjcalc('h')"></td>
            <td style="width:10%"><a href="" onclick="calcf('h'); return false;" class="always_blue">時</a><div id="apply_dialog_tdh"></div></td>
            <td style="width:10%"><input type="text" style="width:30px; ime-mode:disabled" maxlength="2" id="apply_dialog_m" onfocus="adjcalc('m')"></td>
            <td style="width:10%"><a href="" onclick="calcf('m'); return false;" class="always_blue">分</a><div id="apply_dialog_tdm"></div></td>
            <td style="width:10%"><input type="text" style="width:30px; ime-mode:disabled" maxlength="2" id="apply_dialog_s" onfocus="adjcalc('s')"></td>
            <td style="width:10%"><a href="" onclick="calcf('s'); return false;" class="always_blue">秒</a><div id="apply_dialog_tds"></div></td>
            <td style="width:20%"></td>
          </tr></table>
        </div>
      </div>
    </div>
    <div style="background-color:#f4f4f4; margin-top:4px">
      <div style="padding:2px" id="apply_dialog_description">上記を受付時刻として、受付を登録します。</div>
      <div>よろしいですか？</div>
      <div style="padding:6px; padding-top:10px">
        <input type="button" value="OK" style="width:80px" onclick="registAcceptOperate(apply_dialog_mode, apply_dialog_global_element_index);" id="apply_dialog_ok" />
        <input type="button" value="キャンセル" style="width:80px" onclick="hideApplyDialog();"/>
      </div>
    </div>
  </div>
</div>
<?//==========================================================================?>
<?// 承認ダイアログの電卓                                                     ?>
<?//==========================================================================?>
<div style="position:absolute; border:1px solid #05c; width:100px; display:none" id="apply_dialog_calculator">
  <div style="border:1px solid #1484dc; background-color:#2a9cf4;" class="timecalcullator">
    <div style="padding:2px;">
      <div style="float:left; color:#fff"><span id="apply_dialog_caption"></span>の選択</div>
      <div style="float:right; background-color:#f4f4f4"><input type="image" src="js/yui_0.12.2/build/calendar/assets/calx.gif" onclick="hidecalc(); return false;"></div>
      <div style="clear:both"></div>
    </div>
    <table style="width:100%">
      <tr>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(1)">1</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(2)">2</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(3)">3</td>
      </tr>
      <tr>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(4)">4</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(5)">5</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(6)">6</td>
      </tr>
      <tr>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(7)">7</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(8)">8</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(9)">9</td>
      </tr>
      <tr>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc('D')">D</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(0)">0</td>
        <td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc('C')">C</td>
      </tr>
    </table>
  </div>
</div>

<?//==========================================================================
  // 食事コメント入力ダイアログ
  //==========================================================================?>
<div id="meal_comment_dialog" style="position:absolute; width:250px; border:1px solid #1484dc; border-top:1px solid #1273c2; background-color:#f4f4f4; font-size:12px; margin-top:4px; text-align:center; display:none;">
<div style="color:#fff; padding:3px; background-color:#2a9cf4;" id="meal_comment_dialog_titlebar">食事コメント</div>
  <div style="background-color:#f4f4f4; border:1px solid #999">
    <div style="background-color:#f4f4f4; margin-top:4px">
      <div style="padding:4px;">
        <textarea cols="36" rows="3" id="meal_comment_text"></textarea>
      </div>
      <div style="padding:2px" id="apply_dialog_description">食事コメントを登録します。</div>
      <div>よろしいですか？</div>
      <div style="padding:6px; padding-top:10px">
        <input type="button" value="OK" style="width:80px" onclick="" id="meal_comment_dialog_ok" />
        <input type="button" value="キャンセル" style="width:80px" onclick="hideDialog('meal_comment_dialog');"/>
      </div>
    </div>
  </div>
</div>
</div><!-- end of "page_contents"-->

</body>
<? pg_close($con); ?>
</html>


<? //-------------------------------------------------------------------- ?>
<? // PDF印刷用クラス定義                                                 ?>
<? //-------------------------------------------------------------------- ?>
<?
class Wkst
{
  var $TITLE = "看護ワークシート" ;
  var $dtStr ;
  var $bldgs = array() ;
}
class Bldg
{
  var $name ;
  var $wards = array() ;
}
class Ward
{
  var $name ;
  var $rooms = array() ;
}
class Room
{
  var $name ;
  var $ptifs = array() ;
}
class Ptif
{
  var $diagDivs = array() ;
}
?>

<? //-------------------------------------------------------------------- ?>
<? // PDF印刷スクリプトを出力する                                         ?>
<? //-------------------------------------------------------------------- ?>
<?
if ($forPDF)
{
  ob_end_clean() ;

  require_once('fpdf153/mbfpdf.php') ;

  //=================================================
  // PDF生成ライブラリの拡張クラス定義
  // ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //=================================================
  class CustomMBFPDF extends MBFPDF
  {
    // A4横：297 x 210
    var $A4W = 297 ;
    var $A4H = 210 ;

//    var $M          = 0.3 ;
    var $MOST_LEFT  =   7 ;
    var $MOST_RIGHT = 290 ;
    var $MOST_TOP   =   7 ;
    var $MOST_BTM   = 203  ;
    var $HDR_CELL_H = 5.5 ; // ←セル内のテキストの高さを調整する
    var $CNT_LINE_H = 5.0 ; // ←セル内のテキストの高さを調整する
    var $LINE_W_1   = 0.2 ;
    var $LINE_W_2   = 0.5 ;

    var $ROOM_CELL_W = 13 ;
    var $NAME_CELL_W = 23 ;

    var $sotCols   ;
    var $cellW     ;
    var $mostRight ;

    var $wsTitle ;

    var $dtStr1 ;
    var $dtStr2 ;

    var $saveY ;
    var $currY ;

    var $currRoomData ; // 現在処理している病室データ
    var $currPtifData ; // 現在処理している患者データ

    var $pageRoomNum ;

    var $pno = 0;

    var $PRINT_JS = "(print\\('true'\\);)" ; // 印刷ダイアログボックスを表示するためにPDF文書内に埋め込むJavaScript
    var $printJs ;
    var $cin ; // 現在までに生成されたのCustomMBFPDFインスタンス数

    // コンストラクタ(PHP5可)
    function CustomMBFPDF($sot_cols, $wsTitle, $dtStr)
    {
      $this->MBFPDF("L", "mm", "A4") ; // PHPでは継承元クラスのコンストラクタは明示的に呼び出す必要がある

      $this->sotCols   = $sot_cols ;
      $this->cellW     = ($this->MOST_RIGHT - $this->MOST_LEFT - $this->ROOM_CELL_W - $this->NAME_CELL_W) / count($sot_cols) ;
      $this->mostRight = $this->MOST_LEFT + $this->ROOM_CELL_W + $this->NAME_CELL_W + ($this->cellW * count($sot_cols)) ;

      $this->wsTitle = $wsTitle ;
      $this->dtStr1  = date("y/m/d h:i") ;
      $this->dtStr2  = $dtStr ;
    }

    function AddPage0()
    {
      $this->AddPage() ;
      $this->currY = $this->getY() ;
      $this->pageRoomNum = 0 ;
      $this->pno++ ;
    }

    function AddPage1()
    {
      $this->AddPage0() ;
      $this->writeTitleHeader() ;
      $this->writeClmnsHeader() ;
    }

    function AddPage2($idxp)
    {
      $this->fillWhite($this->MOST_LEFT - 2, $this->saveY - 0.1, $this->MOST_RIGHT - $this->MOST_LEFT + 4, $this->A4H - $this->saveY) ;
      $lineW = ($idxp > 0) ? $this->LINE_W_1 : $this->LINE_W_2 ;
      $add   = ($idxp > 0) ? 0               : 0.15            ;
      $this->SetLineWidth($lineW) ;
      $this->Line($this->MOST_LEFT + $add, $this->saveY - 0.15, $this->mostRight - $add, $this->saveY - 0.15) ;
      $this->SetLineWidth(0.2) ;
      $this->writePageNo() ;
      $this->AddPage1() ;
      $this->writeRoomDataConts() ;
      $this->writePtifDataConts() ;
    }

    // 指定範囲を白で塗りつぶす
    function fillWhite($x, $y, $w, $h)
    {
      $this->SetFillColor(255, 255, 255) ;
      $this->Rect($x, $y, $w, $h, "F") ;
    }

    // タイトルヘッダーを出力する
    function writeTitleHeader()
    {
      $this->setY($this->currY) ;
      $this->SetFont(GOTHIC, "",  8.0) ;
      $this->Cell(0, 3.0, $this->dtStr1,  "", 1, "R") ;
      $this->SetFont(GOTHIC, "", 16.0) ;
      $this->Cell(0, 5.0, $this->wsTitle, "", 1, "C") ;
      $this->SetFont(GOTHIC, "",  9.5) ;
      $this->Cell(0, 6.0, $this->dtStr2,  "", 1, "L") ;
      $this->currY = $this->GetY() - 1.5 ;
    }

    // 項目ヘッダーを出力する
    function writeClmnsHeader()
    {
      $this->setY($this->currY + 1) ;
      $this->SetFont(GOTHIC, "", 9.5) ;
      $this->SetFillColor(210);
      $this->Cell($this->ROOM_CELL_W, $this->HDR_CELL_H, "病室", "1",   0, "C", 1) ;
      $this->Cell($this->NAME_CELL_W, $this->HDR_CELL_H, "氏名", "TRB", 0, "C", 1) ;
      $lastIdx = count($this->sotCols) - 1 ;
      foreach($this->sotCols as $key => $row)
      {
        $lineEnd = ($key < $lastIdx) ? 0 : 1 ;
        $this->Cell($this->cellW, $this->HDR_CELL_H, $row["diag_div"], "TRB", $lineEnd, "C", 1) ;
      }
      $this->currY = $this->GetY() ;
      $lineNum = count($this->sotCols) + 3 ;
      $sX = $this->MOST_LEFT ;
      $this->SetLineWidth($this->LINE_W_1) ;
      for ($i = 0 ; $i < $lineNum ; $i++)
      {
        switch ($i)
        {
          case 0  : $add = 0                  ; break ;
          case 1  : $add = $this->ROOM_CELL_W ; break ;
          case 2  : $add = $this->NAME_CELL_W ; break ;
          default : $add = $this->cellW       ;
        }
        $sX += $add ;
        $this->Line($sX, $this->currY - $this->HDR_CELL_H, $sX, $this->MOST_BTM) ;
      }
    }

    function writePageNo()
    {
      $this->SetFont(GOTHIC, '' , 11) ;
      $this->setXY($this->MOST_LEFT, $this->MOST_BTM - 1) ;
      $this->Cell(0, 3, "- " . $this->pno . " -", '', 0, 'C') ;
    }

    function writeRoomDataConts()
    {
      $roomName = $this->currRoomData->name ;
      if ($roomName == null) return ;

      if ($this->pageRoomNum > 0)
      {
        $this->SetLineWidth($this->LINE_W_2) ;
        $this->Line($this->MOST_LEFT + 0.3, $this->currY - 0.15, $this->mostRight - 0.3, $this->currY - 0.15) ;
      }
      $this->setXY($this->MOST_LEFT, $this->currY) ;
      $this->Cell($this->ROOM_CELL_W, $this->HDR_CELL_H, $roomName, "0", 0, "C") ;
      $this->pageRoomNum++ ;
    }

    function writePtifDataConts()
    {
      $this->saveY = $this->currY ;
      $sX = $this->MOST_LEFT + $this->ROOM_CELL_W ;
      $maxX = $sX ;
      $maxY = $this->currY ;
      $this->SetLineWidth($this->LINE_W_1) ;
      foreach ($this->currPtifData->diagDivs as $idx1 => $conts)
      {
//if ((count($this->currPtifData->diagDivs) - 1 > count($this->sotCols)) && ($idx1 == 1) && (strlen($conts) < 1)) continue ;
        $this->setY($this->currY) ;
        $cW = ($idx1 < 1) ? $this->NAME_CELL_W : $this->cellW ;

        $contsArray = $this->getInnerText($conts) ;
        foreach ($contsArray as $idx2 => $cont)
        {
          $this->setX($maxX) ;
          $ruled = ($idx2 > 0) ? "T" : "0" ;
          $this->MultiCell($cW, $this->CNT_LINE_H, $cont, $ruled, "L") ;
        }

        $cY = $this->GetY() ;
        $maxX += $cW ;
        if ($cY > $maxY) $maxY = $cY ;
      }
      $this->SetLineWidth($this->LINE_W_1) ;
      $this->Line($sX, $maxY, $maxX, $maxY) ;
      $this->currY = $maxY ;
    }

    // 指定されたHTML文書からタグ以外のテキスト内容のみを取得します(行の配列で返します)
    function getInnerText($html)
    {
      $retArray = array() ;

      // テンプレートごとに分ける
      $itemArray = explode("@hr@", $html) ;
      // それぞれの内容について繰り返す
      foreach ($itemArray as $idx => $cont)
      {
        if (($cont == null) || (strlen(trim($cont)) < 1)) continue ;
        // \r → ""、 \n → ""、 \t → ""
        $cont = @ereg_replace("\r|\n|\t", "", $cont) ;
        // 複数" " → 単一" "
        $cont = @preg_replace("/\s+/", " ", $cont) ;
        // &nbsp;  → " "
        $cont = @str_replace("&nbsp;", " ", $cont) ;
        // h() → html_entity_decode()
        $cont = @html_entity_decode($cont) ;
        // <div> → ""、</div> → \n、<br/> → \n
//        $cont = @str_replace("<br/>", "\n", str_replace("</div>", "\n", str_replace("<div>", "", $cont))) ;
        $cont = @str_replace("<br/>", "\n", str_replace("</div>", "\n", $cont)) ;
        // < 〜 > → ""
        $cont = @strip_tags($cont) ;
        // "\n " → "\n"
        $cont = @str_replace("\n ", "\n", $cont) ;

        $retArray[count($retArray)] = trim($cont) ;
      }

      return $retArray ;
    }

    // 印刷ダイアログボックスを表示するためにオーバーライドします
    function Output($showPrintDialog)
    {
      $this->printJs = ($showPrintDialog) ? $this->PRINT_JS : "" ;
      parent::Output() ;
    }
    // 印刷ダイアログボックスを表示するためにオーバーライドします
    function _putresources()
    {
      parent::_putresources() ;
      if (strlen($this->printJs) < 1) return ;
      $this->_newobj() ;
      $this->cin = $this->n ;
      $this->_out("<<\n/Names [(EmbeddedJS) " . ($this->cin + 1) . " 0 R]\n>>\nendobj\n") ;
      $this->_newobj() ;
      $this->_out("<<\n/S /JavaScript\n/JS " . $this->printJs . "\n>>\nendobj\n") ;
    }
    function _putcatalog()
    {
      parent::_putcatalog() ;
      if (strlen($this->printJs) < 1) return ;
      $this->_out("/Names <</JavaScript " . $this->cin . " 0 R>>") ;
    }

  }
  //=================================================
  // ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
  // PDF生成ライブラリの拡張クラス定義
  //=================================================

  $wsTitle = $wkst->TITLE ;
  $dtStr   = $date_y . "年" . (int)$date_m . "月" . (int)$date_d . "日" ;

  //  $pdf = new MBFPDF() ;
  $pdf = new CustomMBFPDF($sot_cols, $wsTitle, $dtStr) ;
  $pdf->SetAutoPageBreak(false) ;
  $pdf->AddMBFont(GOTHIC, 'EUC-JP') ;
  $pdf->SetMargins($pdf->MOST_LEFT, $pdf->MOST_TOP) ;
  $pdf->Open() ;

  $pdf->AddPage1() ;

  foreach ($wkst->bldgs as $idxb => $bldg) { // 事業所(棟)ループ 開始
  foreach ($bldg->wards as $idxw => $ward) { // 病棟ループ 開始

    foreach ($ward->rooms as $idxr => $room) // 病室ループ 開始
    {
      $pdf->currRoomData = $room ;
      $pdf->writeRoomDataConts() ;
      foreach ($room->ptifs as $idxp => $ptif) // 患者ループ 開始
      {
        $pdf->currPtifData = $ptif ;
        $pdf->writePtifDataConts() ;
        // ページ下端を超えたら改ページ処理(2)を行う
        if ($pdf->currY > $pdf->MOST_BTM) $pdf->AddPage2($idxp) ;
      } // 患者ループ 終了
    } // 病室ループ 終了

  } // 病棟ループ 終了
  } // 事業所(棟)ループ 終了

  $pdf->fillWhite($pdf->MOST_LEFT - 2, $pdf->currY - 0.1, $pdf->MOST_RIGHT - $pdf->MOST_LEFT + 4, $pdf->A4H - $pdf->currY) ;
  $pdf->SetLineWidth($pdf->LINE_W_1) ;
  $pdf->Line($pdf->MOST_LEFT, $pdf->currY - 0.15, $pdf->mostRight, $pdf->currY - 0.15) ;
  $pdf->writePageNo() ;

  $pdf->Output(true) ;
  die ;

}
else
{
  ob_end_flush() ;
}

/**
 * 「新規」ボタンを追加する
 * 
 * @param integer $global_element_index 要素番号
 * @param integer $tmpl_id              テンプレートID
 * @param string  $ptif_id              患者ID
 * @param string  $target_yyyymmdd      対象日付
 * @param integer $apply_id             承認連番ID
 * 
 * @return void
 */
function createNewButton($global_element_index, $tmpl_id, $ptif_id, $target_yyyymmdd, $apply_id)
{
    echo "<span id=\"workflow_element_new_$global_element_index\">";
    echo "<input type=\"button\" onclick=\"popupRegisterWindowWithParam('$tmpl_id', '$ptif_id', '$target_yyyymmdd', $apply_id);\"";
    echo ' class="print_hidden" value="新規" />';
    echo '</span><wbr>&nbsp;';
}

/**
 * 食事コメントを表示する
 * 
 * @param resource $con     データベース接続リソース
 * @param string   $fname   画面名
 * @param string   $ptif_id 患者ID
 * 
 * @return void
 */
function dispMealCommentText($ptif_id, $comment)
{
    $id = "meal_comment_$ptif_id";
    echo '<a href="" onclick="showMealCommentDialog(\'' . $ptif_id . '\', \'' . $id  . '\'); cancelpopupRegister(); return false;">[コメント]</a>';
    echo '<span id="' . $id . '">';
    echo h($comment, ENT_QUOTES);
    echo '</span>';
}

/**
 * 食事コメントを更新する
 * 
 * @param resource $con     データベース接続リソース
 * @param string   $fname   画面名
 * @param string   $ptif_id 患者ID
 * @param string   $comment 食事コメント
 * @param string   $emp_id  更新者ID
 * 
 * @return true:成功/false:失敗
 */
function updateMealComment($con, $fname, $ptif_id, $comment, $emp_id)
{
    // コメントのエスケープ処理
    $comment = pg_escape_string($comment);
    
    // レコードの有無を確認
    $sql = "select count(*) from ptmealcmt where ptif_id = '$ptif_id'";
    $sel = select_from_table($con, $sql, "", $fname);
    $res = pg_fetch_result($sel, 0, 0);
    
    $date = date('Ymd');
    if ($res == 0) {
        // レコードが存在していなければ、insert
        $sql = 'insert into ptmealcmt (ptif_id, comment, upd_date, upd_emp_id) values (';
        $content = array($ptif_id, $comment, $date, $emp_id);
        $ret = insert_into_table($con, $sql, $content, $fname);
    } else {
        // レコードが存在していれば、update
        $sql = 'update ptmealcmt set';
        $set = array('comment', 'upd_date', 'upd_emp_id');
        $setvalue = array($comment, $date, $emp_id);
        $cond = "where ptif_id = '$ptif_id'";
        $ret = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    }
    
    if ($ret === 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * 食事欄に内容が表示されるテンプレートか判定する
 * 
 * @param string $tmplFileName テンプレートファイル名
 * 
 * @return true:該当する/false:該当しない
 */
function isMealTmpl($tmplFileName)
{
    $mealTmplNames = array('tmpl_MealStart.php', 'tmpl_MealJoken.php', 'tmpl_MealStop.php');
    $key = array_search($tmplFileName, $mealTmplNames);
    if ($key === false) {
        return false;
    } else {
        return true;
    }
}

/**
 * 「新規」ボタンを作成するか判定する
 * 
 * @param string  $tmplFileName       テンプレートファイル名
 * @param integer $summary_seq        処理中のサマリシーケンス番号
 * @param integer $summary_seq_latest 最新のサマリシーケンス番号
 * 
 * @return true:作成する/false:作成しない
 */
function isCreateNewButton($tmplFileName, $summary_seq, $summary_seq_latest)
{
    // 「新規」ボタン作成対象のテンプレート
    $templates = array('tmpl_Worksheet2.php', 'tmpl_Haisetu.php', 'tmpl_IdouIjou.php', 'tmpl_Kankyou.php');
    if (!in_array($tmplFileName, $templates)) {    // 対象テンプレートに該当しない
        return false;
    }
    
    // 処置中のサマリシーケンス番号が最新のサマリシーケンス番号でなければ、ボタンを表示しない
    if ($summary_seq != $summary_seq_latest) {
        return false;
    }
    
    return true;

}
?>
