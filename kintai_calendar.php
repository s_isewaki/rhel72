<?php
require_once 'about_certify.php';
require_once 'about_authority.php';
require_once 'conf/conf.inf';

require_once './kintai/common/mdb_wrapper.php';
require_once './kintai/common/mdb.php';
require_once './kintai/common/user_info.php';
require_once './kintai/common/date_util.php';
require_once './kintai/common/code_util.php';

require_once './kintai/individual/model/overtime_calc_util.php';

//====================================
//ロガー設定
//====================================
require_once("./common_log_class.ini");
$log = new common_log_class(basename(__FILE__));

$fname = $_SERVER['PHP_SELF'];

//====================================
//セッションのチェック
//====================================
$session = certify("", "", $_REQUEST["session"], $fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$checkauth = check_authority($session,7,$fname);
if($checkauth == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

// ページ制御を決定
$ref_date = $_GET["ref_date"];
$from_date; // from
$to_date;   // to
$prev_allow = true;
$next_allow = true;
switch ($_GET["page"]) {
    case "prev":
        $to_date = date('Y/m/d', strtotime($ref_date) - (60*60*24*10)); // toを10日前
        $from_date = date('Y/m/d', strtotime($to_date) - (60*60*24*9)); // fromをさらに9日前
        break;
    case "next":
        $to_date = date('Y/m/d', strtotime($ref_date) + (60*60*24*10)); // toを10日前
        $from_date = date('Y/m/d', strtotime($to_date) - (60*60*24*9)); // fromをさらに9日前
        break;
    default:
        if ($init_date_flag == "1") {
            $to_date = date('Y/m/d', time() + (60*60*24*9)); // toを9日後
            $from_date = date('Y/m/d', time()); // fromを当日
        }
        else {
            $to_date = date('Y/m/d', time()); // toを当日
            $from_date = date('Y/m/d', time() - (60*60*24*9)); // fromを9日前
        }
}

try {
    // コネクション初期化
    MDBWrapper::init();
    // MDB
    $mdb = new MDB($log);
    
    // ユーザ情報を取得
    $emp_id = $_GET["emp_id"];
    
    $sql = 
        "select ".
        "date_key, ".
        "atdptn_nm, ".
        "default_name, ".
        "display_name, ".
        "previous_day_flag, ".
        "start_time, ".
        "next_day_flag, ".
        "end_time ".
        "from ".
        "( ".
        "( ".
        "( ".
        "select emp_id as emp_key, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key, previous_day_flag, start_time, next_day_flag, end_time from atdbkrslt ".
        ") rslt left outer join ".
        "( ".
        "select atdptn_id, group_id, atdptn_nm from atdptn ".
        ") ptn on rslt.ptn_key = CAST(ptn.atdptn_id AS varchar) and rslt.group_key = ptn.group_id ".
        ") a left outer join atdbk_reason_mst mst on a.reason_key = mst.reason_id ".
        ") b  ".
        "where emp_key = :emp_id and date_key >= :from_date and date_key <= :to_date order by date_key";
    
    $types = array("text","text","text");
    $values = array(
            "emp_id" => $emp_id,
            "from_date" => str_replace('/', '', $from_date),
            "to_date" => str_replace('/', '', $to_date)
            );
    // SQLを実行して結果を取得
    $result = $mdb->find($sql, $types, $values);
    //予定取得
    $sql = 
        "select ".
        "date_key, ".
        "atdptn_nm, ".
        "default_name, ".
        "display_name ".
        "from ".
        "( ".
        "( ".
        "( ".
        "select emp_id as emp_key, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key from atdbk ".
        ") rslt left outer join ".
        "( ".
        "select atdptn_id, group_id, atdptn_nm from atdptn ".
        ") ptn on rslt.ptn_key = CAST(ptn.atdptn_id AS varchar) and rslt.group_key = ptn.group_id ".
        ") a left outer join atdbk_reason_mst mst on a.reason_key = mst.reason_id ".
        ") b  ".
        "where emp_key = :emp_id and date_key >= :from_date and date_key <= :to_date order by date_key";
    
    $types = array("text","text","text");
    $values = array(
            "emp_id" => $emp_id,
            "from_date" => str_replace('/', '', $from_date),
            "to_date" => str_replace('/', '', $to_date)
            );
    // SQLを実行して結果を取得
    $plan = $mdb->find($sql, $types, $values);
} catch (FatalException $fex) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

$title = "カレンダー";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー｜<?php echo $title; ?></TITLE>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {
	border-collapse:collapse;
}
.list td {
	border:#5279a5 solid 1px;padding:1px;
	border-width:1;
}
.holiday {
	background-color:#fadede;
}
.paging {
	text-decoration: none;
}
</style>
<script type="text/javascript">
<!-- 親画面のfunctionをcallする -->
$(document).ready(function() {
	$("a[action=set]").click(function() {
		window.opener.set_hol_date($(this).attr("value"), <?php echo $item_id; ?>);
		window.close();
	});
});
</script>

</HEAD>
<BODY  bgcolor="#ffffff" text="#000000">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr bgcolor="#5279a5">
			<td width="480" height="32" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?php echo $title; ?></b></font></td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>
	<img src="img/spacer.gif" width="10" height="10" alt=""><br>
	<table>
		<tr class="j12">
			<!-- ページ移動リンク切替 -->
			<td>
<?php 
$url = "kintai_calendar.php?session=$session&emp_id=$emp_id&ref_date=$to_date&page=prev&item_id=$item_id";
				?>
				<a class="paging" href="<?php echo $url; ?>">&lt;前へ</a>
			</td>
			<td width="50"></td>
			<td>
<?php 
$url = "kintai_calendar.php?session=$session&emp_id=$emp_id&ref_date=$to_date&page=next&item_id=$item_id";
				?>
				<a class="paging" href="<?php echo $url; ?>">後へ&gt;</a>
			</td>
		<tr>
	</table>
	<table cellspacing="0" cellpadding="2" class="list">
		<tr class="j12" bgcolor="#f6f9ff">
			<td width="60" align="center">日付</td>
			<td width="30" align="center">曜日</td>
			<td width="80" align="center">勤務実績</td>
			<td width="80" align="center">事由</td>
			<td width="30" align="center">前</td>
			<td width="50" align="center">出勤</td>
			<td width="30" align="center">翌</td>
			<td width="50" align="center">退勤</td>
		</tr>
<?php
$tr_date = $from_date;
for ($i = 0; $i < 10; $i++) {
    // 年月に変換
    $col_date = date('n月j日', strtotime($tr_date));
    $day_of_week = DateUtil::day_of_week($tr_date, true);
    
    // 実績の表示可否
    $rslt = "";
    $reason = "";
    $prev_day_str = "";
    $next_day_str = "";
    $start_time = "";
    $end_time = "";
    $fetch_rec = get_rec($result, $tr_date);
    $setting_action = false;
    if (!empty($fetch_rec)) { // あったら値編集
        $setting_action = true;
        $rslt = $fetch_rec["atdptn_nm"]; // 勤務実績
        $reason = $fetch_rec["display_name"]; // 事由(表示) 
        if (empty($reason)) $reason = $fetch_rec["default_name"]; // 事由(デフォ)
        if (empty($rslt) && empty($reason)) {
            $plan_rec = get_rec($plan, $tr_date);
            if (!empty($plan_rec)) { // あったら値編集
                $rslt = $plan_rec["atdptn_nm"]; // 勤務実績
                $reason = $plan_rec["display_name"]; // 事由(表示) 
                if (empty($reason)) $reason = $plan_rec["default_name"]; // 事由(デフォ)
            }
        }
        if ($fetch_rec["previous_day_flag"] == "1") {
            $prev_day_str = "前";
        }
        if ($fetch_rec["next_day_flag"] == "1") {
            $next_day_str = "翌";
        }
        $start_time = $fetch_rec["start_time"];
        if ($start_time != "") {
            $start_time = DateUtil::convert_time(DateUtil::convert_min($start_time)); //hhmm->h:mm
        }
        $end_time = $fetch_rec["end_time"];
        if ($end_time != "") {
            $end_time = DateUtil::convert_time(DateUtil::convert_min($end_time));
        }
          
    }
    //実績がない場合、予定から取得
    else {
        $plan_rec = get_rec($plan, $tr_date);
        if (!empty($plan_rec)) { // あったら値編集
            $rslt = $plan_rec["atdptn_nm"]; // 勤務実績
            $reason = $plan_rec["display_name"]; // 事由(表示) 
            if (empty($reason)) $reason = $plan_rec["default_name"]; // 事由(デフォ)
        }
    }
    $is_cal_holiday = OvertimeCalcUtil::is_cal_holiday(date('Ymd', strtotime($tr_date)));
    $tr_class = "";
    if ($is_cal_holiday) {
        $tr_class = "holiday";
    }
		?>
		<tr class="j12 <?php echo $tr_class; ?>">
			<td align="right">
				<a href="#" action="set" value="<?php echo $tr_date; ?>">
					<?php echo $col_date; ?>
				</a> 
			</td>
			<td align="center"><?php echo $day_of_week; ?></td>
			<td><?php echo $rslt; ?></td>
			<td><?php echo $reason; ?></td>
			<td align="center"><?php echo $prev_day_str; ?></td>
			<td align="right"><?php echo $start_time; ?></td>
			<td align="center"><?php echo $next_day_str; ?></td>
			<td align="right"><?php echo $end_time; ?></td>
		</tr>
    <?
    $tr_date = date('Y/m/d', strtotime("$tr_date +1 day"));
}
		?>
	</table>
</BODY>
</HTML>


<?php
// 画面内function
// 該当する日付のレコードを取得する
function get_rec($list, $date) {
    $target = date('Ymd', strtotime($date));
    foreach ($list as $rec) {
        if ($rec["date_key"] == $target) return $rec;
    }
    return null;
}

// 時間外の表示を編集
function get_over_time($data, $num) {
    
    if ($data["mdfy_cancel_flag".$num] == 1) {
        return "";
    }
    
    $mdfy_start_time = $data["mdfy_start_time".$num];
    $rslt_start_time = $data["rslt_start_time".$num];
    $cmmd_start_time = $data["cmmd_start_time".$num];
    
    $from;
    $end;
    $flg1;
    $flg2;
    if (!empty($mdfy_start_time )) {
        $from = $mdfy_start_time;
        $end  = $data["mdfy_end_time".$num];
        $flg1 = $data["mdfy_start_next_flag".$num];
        $flg2 = $data["mdfy_end_next_flag".$num];
    } else if (!empty($rslt_start_time )) {
        $from = $rslt_start_time;
        $end  = $data["rslt_end_time".$num];
        $flg1 = $data["rslt_start_next_flag".$num];
        $flg2 = $data["rslt_end_next_flag".$num];
    } else if (!empty($cmmd_start_time )) {
        $from = $cmmd_start_time;
        $end  = $data["cmmd_end_time".$num];
        $flg1 = $data["cmmd_start_next_flag".$num];
        $flg2 = $data["cmmd_end_next_flag".$num];
    } else {
        return "";
    }
    
    $str;
    
    if ($flg1 == 1) {
        $str .= "翌";
    } else {
        $str .= "&nbsp";
    }
    $from_hm_map = DateUtil::split_hi($from);
    $str .= $from_hm_map['h'].":".$from_hm_map['i'];
    $str .= "〜";
    if ($flg2 == 1) {
        $str .= "翌";
    } else {
        $str .= "&nbsp";
    }
    $to_hm_map = DateUtil::split_hi($end);
    $str .= $to_hm_map['h'].":".$to_hm_map['i'];
    
    return $str;
}
?>