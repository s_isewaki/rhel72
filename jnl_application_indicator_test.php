<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜管理指標</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");

require_once("jnl_application_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);
echo 'emp_id = '.$emp_id."<br />";

?>



<script type="text/javascript">

</script>
<script type="text/javascript" src="js/jnl/jnl_indicator_menu_list.js"></script>
<script type="text/javascript" src="js/jnl/jnl_indicator_ajax.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="決裁・管理指標"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_indicator_menu.php?session=<? echo($session); ?>"><b>管理指標</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form id="wkfw" name="wkfw" action="jnl_application_imprint.php" method="post" enctype="multipart/form-data">
    <input type="hidden" id="category" name="category" value="<?= $category?>" />
    <input type="hidden" id="sub_category" name="sub_category" value="<?= $sub_category?>" />
<?php
if (!empty($plant)) {
?>
    <!--<input type="hidden" id="plant" name="plant" value="<?= $plant?>" />
    --><input type="hidden" id="type" name="type" value="<?= $type?>" />
    <input type="hidden" id="hidden_type" name="hidden_type" value="<?= $category?>" />
<?php
}
?>
    <input type="hidden" id="no" name="no" value="<?= $no?>" />
<a href="http://create_test_20100122/comedix/jnl_application_indicator_test.php?session=<?=$session?>">link</a>
<?php
error_reporting(-1);

require_once('jnl_indicator_data.php');
include_once('jnl_indicator_display.php');
//require_once('jnl_indicator_display.php');
require_once('jnl_indicator_ajax.php');

$data    = new IndicatorData(array('fname' => $fname, 'con' => $con));
$display = new IndicatorDisplay(array('con' => $con, 'session' => $session, 'display_year' => $display_year, 'display_month' => $display_month));
$ajax    = new IndicatorAjax(array('fname' => $fname, 'con' => $con));

/* menu list */

echo $display->getMenuList(
    array('category' => $category, 'sub_category' => $sub_category, 'no' => $no, 'type' => $type, 'plant' => $plant),
    null,
    null,
    $emp_id
);

$noArr = $data->getNoForNo($no);
//var_dump($noArr);
//var_dump($ajax->getFacility(null, null, 2));
//var_dump(array($emp_id, 0, 1));
//var_dump($ajax->getFacility($emp_id, 0, 2));
//var_dump($ajax->getDisplayNoNameArr(1, 2, null, '100100000002'));
//var_dump($ajax->getReportNo($emp_id, 1, 2));
//var_dump($ajax->jnlInspectorList->getSql());
//var_dump($category.' : '. $sub_category);
//var_dump($display->getNoForCategory($category, $sub_category));
//echo $display->sc(9);
//echo $display->sc(10);
//echo $display->sc(11);
//echo '$plant = '.$plant."<br />";
//echo '$_plant = '.$_plant."<br />";


$a ="あ";
$a = mb_convert_encoding($a, "UTF-16" ,"SJIS");//Shift_JISからUTF-16へ変換
$x = bin2hex("$a");//16進数の数値へ変換
echo $x;//"3042"と表示される
echo "<br />";
/*--- 16進数のUnicodeを"あ"へ再変換 ---*/
$a = pack("H*","$x");//16進数の数値を文字コードへ変換
$a = mb_convert_encoding($a,"SJIS", "UTF-16" );//UTF-16からShift_JISへ変換
echo $a;//"あ"に変換表示される

?>

<input type="hidden" name="session" value="<? echo($session); ?>">



</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>
