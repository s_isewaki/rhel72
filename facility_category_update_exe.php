<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備権限チェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($cate_name == "") {
	echo("<script type=\"text/javascript\">alert('カテゴリ名を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($cate_name) > 100) {
	echo("<script type=\"text/javascript\">alert('カテゴリ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$chart_start = "$chart_start_hour$chart_start_min";
$chart_end = "$chart_end_hour$chart_end_min";
if ($chart_start >= $chart_end) {
	echo("<script type=\"text/javascript\">alert('タイムチャート表示時間が不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
// 予約開始日妥当性チェック
$start_date = $start_date1.$start_date2.$start_date3;
$start_date = str_replace("-", "", $start_date);
if ($start_date != "" &&
	!checkdate($start_date2, $start_date3, $start_date1)) {
	echo("<script language=\"javascript\">alert(\"予約開始日が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 日付指定の場合
if ($end_date_flg == "1") {
	// 予約期限日妥当性チェック
	$end_date = $end_date1.$end_date2.$end_date3;
	$end_date = str_replace("-", "", $end_date);
	if ($end_date != "" &&
		!checkdate($end_date2, $end_date3, $end_date1)) {
		echo("<script language=\"javascript\">alert(\"予約期限日が正しくありません\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
	// 開始日、期限日の関連チェック
	if ($start_date != "" && $end_date != "" && $start_date > $end_date) {
		echo("<script language=\"javascript\">alert(\"予約開始日は予約期限日以前にしてください\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}

	// 月をクリア
	$end_month = "";
} else {
// 期間指定の場合
	// 月が未入力の場合
	if ($end_month == "-") {
		echo("<script language=\"javascript\">alert(\"期間指定の場合は月数を指定してください\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
	// 予約期限日をクリア
	$end_date = "";
}

// 登録値の編集
if ($pt_flg != "t") {$pt_flg = "f";}

$emp_id = get_emp_id($con, $session, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 更新前の権限設定・管理者設定フラグを取得
$sql = "select auth_flg, has_admin_flg from fclcate";
$cond = "where fclcate_id = $cate_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pre_auth_flg = pg_fetch_result($sel, 0, "auth_flg");
$pre_has_admin_flg = pg_fetch_result($sel, 0, "has_admin_flg");

// カテゴリレコードを更新
$sql = "update fclcate set";
$set = array("fclcate_name", "base_min", "pt_flg", "time_chart_start", "time_chart_end", "bg_color", "show_content", "auth_flg", "has_admin_flg", "start_date", "end_date_flg", "end_date", "end_month", "double_flg");
$setvalue = array($cate_name, $base_min, $pt_flg, $chart_start, $chart_end, $bg_color, $show_content, $auth_flg, $has_admin_flg, $start_date, $end_date_flg, $end_date, $end_month, $double_flg);
if ($pre_auth_flg != $auth_flg) {
	array_push($set, "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg");
	array_push($setvalue, "f", "1", "1", "f", "1", "1");
}
$cond = "where fclcate_id = $cate_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 管理者設定「する」→「しない」の場合、施設・設備管理者情報を削除
if ($pre_has_admin_flg == "t" && $has_admin_flg != "t") {
	$sql = "delete from fclcateadmin";
	$cond = "where fclcate_id = $cate_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

// 管理者設定「しない」→「する」の場合、登録者を施設・設備管理者情報として登録
} else if ($pre_has_admin_flg != "t" && $has_admin_flg == "t") {
	$sql = "insert into fclcateadmin (fclcate_id, emp_id) values (";
	$content = array($cate_id, $emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 画面遷移
echo("<script type=\"text/javascript\">location.href = 'facility_master_menu.php?session=$session';</script>");
?>
