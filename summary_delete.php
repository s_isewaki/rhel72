<?/*

画面パラメータ
$session
	セッションID
$sum_id
	削除対象の診療記録ID
その他
	summary_rireki_naiyo.phpに必要となるパラメータ
		$pt_id
		$emp_id
		$mode
		$summary_id
		$diag_div
		$problem
		$date
		$from_date
		$to_date
		$return_url

*/?><meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("get_values.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// サマリー削除権限チェック
$check_auth = check_authority($session, 57, $fname);
if ($check_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

$emp_id = get_emp_id($con, $session, $fname);

// 作成者か、代理者は削除可能
// 代理者については、最新の有効データの代理者が有効であれば削除可能
// (dairi_kakunin_ymdhmsに値があるものは、代理無効になったものである）
$sql =
	" select su.emp_id, su.summary_seq, ap.dairi_emp_id from summary su".
	" left join sum_apply ap on (".
	"   ap.summary_id = su.summary_id".
	"   and ap.ptif_id = su.ptif_id".
	"   and ap.dairi_kakunin_ymdhms is null".
	"   and ap.delete_flg = 'f'".
	" )".
	" where su.summary_id = ".$sum_id.
	" and su.ptif_id = '".$pt_id."'".
	" order by ap.apply_id desc limit 1";
$summary = select_from_table($con, $sql, "", $fname);
$summary_seq = (int)@pg_fetch_result($summary, 0, "summary_seq");
$db_emp_id = @pg_fetch_result($summary, 0, "emp_id");
$dairi_emp_id = pg_fetch_result($summary, 0, "dairi_emp_id");


if ($db_emp_id == $emp_id || $dairi_emp_id == $emp_id){
	pg_query($con, "begin transaction");

	$sql = "delete from summary";
	$cond = "where (emp_id = '".pg_escape_string($db_emp_id)."') and (ptif_id = '".pg_escape_string($pt_id)."') and (summary_id = '$sum_id')";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}

	// 申請情報の物理削除
//	$sel = select_from_table($con, "select apply_id from sum_apply where summary_id = " . $sum_id . " and (ptif_id = '".pg_escape_string($pt_id)."' or ptif_id is null)", "", $fname);
	$sel = select_from_table($con, "select apply_id from sum_apply where summary_id = " . $sum_id . " and ptif_id = '".pg_escape_string($pt_id)."'", "", $fname);
	while ($row = pg_fetch_array($sel)){
		$apply_id = $row["apply_id"];
		if (!$apply_id) continue;
		$upd = delete_from_table($con, "delete from sum_apply where apply_id = ".$apply_id, "", $fname);
		$upd = delete_from_table($con, "delete from sum_applyapv where apply_id = ".$apply_id, "", $fname);
		$upd = delete_from_table($con, "delete from sum_applyapvemp where apply_id = ".$apply_id, "", $fname);
		$upd = delete_from_table($con, "delete from sum_applyfile where apply_id = ".$apply_id, "", $fname);
		$upd = delete_from_table($con, "delete from sum_applynotice where apply_id = ".$apply_id, "", $fname);
		$upd = delete_from_table($con, "delete from sum_applyasyncrecv where apply_id = ".$apply_id, "", $fname);
		$upd = delete_from_table($con, "delete from sum_apply_hinin_sasimodosi where apply_id = ".$apply_id, "", $fname);
		$upd = delete_from_table($con, "delete from sum_apply_sasimodosi_target where apply_id = ".$apply_id, "", $fname);
	}
	$upd = delete_from_table($con, "delete from sum_xml where summary_id = ".$sum_id." and ptif_id = '".$pt_id."'", "", $fname); // 旧(2009/12時点の消し方)
	$upd = delete_from_table($con, "delete from sum_xml where summary_seq = ".$summary_seq, "", $fname);                         // 新(2010/01以降の消し方)

	// ワークシートスケジュールの物理削除
	$sel = select_from_table($con, "select schedule_seq from sot_summary_schedule where summary_seq = ".$summary_seq, "", $fname);
	while($row = pg_fetch_array($sel)){
		$upd = delete_from_table($con, "delete from sot_summary_schedule_various where schedule_seq = ".$row["schedule_seq"], "", $fname);
	}
	$upd = delete_from_table($con, "delete from sot_summary_schedule where summary_seq = ".$summary_seq, "", $fname);
	$upd = delete_from_table($con, "delete from sot_worksheet_apply where summary_seq = ".$summary_seq, "", $fname);
	pg_query($con, "commit");

	$filename = "./docArchive/xml/".$emp_id."_".$pt_id."_".$sum_id.".xml"; // 2009/12以降は、これは不要である
	@unlink($filename); // 2009/12以降は、これは不要である

	// 添付ファイルを消していないことに注意

}
echo("<script language=\"javascript\">location.href=\"./summary_rireki_naiyo.php?session=$session&pt_id=$pt_id&emp_id=$emp_id&mode=$mode&summary_id=$sum_id&diag_div=$diag_div&problem=$problem&date=$date&from_date=$from_date&to_date=$to_date&return_url=$return_url\";</script>\n");
exit;
