<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("show_select_values.ini");
require("summary_common.ini");
//require_once("./about_error.php");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

require_once("yui_calendar_util.ini");
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(1);
?>
<?
//****************************************************************************
// 画面描画
//****************************************************************************
// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

$def_y      = trim($_REQUEST["uketuke_date_y"], "-") ; // 受付日−西暦 (例："2011", "-")
$def_m      = trim($_REQUEST["uketuke_date_m"], "-") ; // 受付日−月   (例："03", "-")
$def_d      = trim($_REQUEST["uketuke_date_d"], "-") ; // 受付日−日   (例："08", "-")
$uketuke_no =      $_REQUEST["uketuke_no"    ]       ; // 受付番号
$pt_id      =      $_REQUEST["pt_id"         ]       ; // 患者ID
if (@$_REQUEST["mode"] != "search")
{
  $def_y = ($def_y) ? (int)$def_y : (int)date("Y") ;
  $def_m = ($def_m) ? (int)$def_m : (int)date("m") ;
  $def_d = ($def_d) ? (int)$def_d : (int)date("d") ;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><? echo($med_report_title); ?>管理 | 検査結果</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list     { border:#5279a5 solid 1px; border-collapse:collapse; padding:3px; }
.klist    { font-size:12px; }
.klist th { text-align:center; white-space:nowrap; background-color:#bdd1e7; }
.klist td { text-align:left;   white-space:nowrap; }
select    { font-size:13px; font-family:"Hiragino Kaku Gothic Pro W3","ヒラギノ角ゴ Pro W3",Osaka,"MS P Gothic","ＭＳ Ｐゴシック",sans-serif; padding:1px; margin:0; }
</style>
<script type="text/javascript">
function startSearch()
{
  var yy=document.getElementById("date_y1").value;
  var mm=document.getElementById("date_m1").value;
  var dd=document.getElementById("date_d1").value;
  var no=document.getElementById("uketuke_no").value;
  var id=document.getElementById("pt_id").value;
  var ymd=yy+mm+dd;
  var len=ymd.length;
  var derr=false;
  var perr=false;
  if ((3<len)&&(len<6))
  {
    derr=true;
  }
  else
  {
    if ((len==7)&&(mm.length<2))
    {
      derr=true;
    }
    else
    {
      if ((len<8)&&(id.length<1)) perr=true;
    }
  }
  var dmsg=(derr)?"受付日を入力してください。":"";
  var pmsg=(perr)?"患者IDを入力してください。":"";
  var msg=dmsg+pmsg;
  if (msg.length>0)
  {
    alert(msg); return;
  }
  document.getElementById("uketuke_date").value=ymd;
  document.getElementById("mode").value="search";
  document.frm.submit();
}
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_KENSAKEKKA"); ?>
<table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td width="120" valign="top">

   <table width="120" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
    </tr>
    <tr>
     <td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
     <td width="118" valign="top"><?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?></td>
     <td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
    </tr>
    <tr>
     <td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
    </tr>
   </table>
  </td>
  <td width="5">
   <img src="img/spacer.gif" width="5" height="1" alt="">
  </td>
  <td valign="top">
   <?//****************************************************************************?>
   <?// 検査結果メニュー                                                           ?>
   <?//****************************************************************************?>
   <table border="0" cellspacing="0" cellpadding="0">
    <tr height="22">
     <td width="110" align="center" bgcolor="#bdd1e7"><a href="./summary_kensakekka_import.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インポート</font></a></td>
     <td width="5">&nbsp;</td>
     <td width="110" align="center" bgcolor="#5279a5"><a href="./summary_kensakekka_sansyo.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>検査結果参照</b></font></a></td>
    </tr>
   </table>
   <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
    </tr>
   </table>
<form action="summary_kensakekka_sansyo.php" method="post" name="frm" style="margin-top:4px" enctype="multipart/form-data">
<input type="hidden" name="session" value="<?=$session ?>">
<input type="hidden" name="mode" id="mode" value="">
   <table style="width:100%;" border="1" cellspacing="0" cellpadding="2" class="list">
    <tr>
     <td style="padding:5px;text-align:left;">
      <table>
       <tr>
        <td>
         受付日
         <select name="uketuke_date_y" id="date_y1"><?= show_select_years(10, (int)$def_y, true); ?></select>&nbsp;年
         <select name="uketuke_date_m" id="date_m1"><?= show_select_months((int)$def_m, true); ?></select>&nbsp;月
         <select name="uketuke_date_d" id="date_d1"><?= show_select_days((int)$def_d, true); ?></select>&nbsp;日
         <input type="hidden" name="uketuke_date" id="uketuke_date" value="">
         <img src="img/calendar_link.gif" style="position:relative;top:3px;left:0px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>&nbsp;&nbsp;
         受付番号
         <input type="text" name="uketuke_no" id="uketuke_no" value="<?=$uketuke_no?>" style="width:70px;">&nbsp;&nbsp;&nbsp;
         患者ID
         <input type="text" name="pt_id" id="pt_id" value="<?=$pt_id?>" style="width:110px;">
         <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
        </td>
       </tr>
       <tr>
        <td style="text-align:right;">
         <input type="button" name="btn_search" value=" 検索 " onclick="startSearch();" />
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
</form>
<?
//****************************************************************************
// 検査結果検索
//****************************************************************************
if (@$_REQUEST["mode"] == "search")
{
  // 画面入力値より検索条件を作成する
  $uketuke_date      = rtrim($_REQUEST["uketuke_date"], "-") ; // 受付日 (例："20110308", "201103-", "2011--", "---")
  $uketuke_date_cond = ($uketuke_date) ? " and hdr_uketuke_date like '" . $uketuke_date . "%'" : "" ; 
  $uketuke_no_cond   = ($uketuke_no  ) ? " and hdr_uketuke_no = '" . $uketuke_no . "'"         : "" ; 
  $pt_id_cond        = ($pt_id       ) ? " and ptif_id = '" . $pt_id . "'"                     : "" ; 

  $sql =
    " select" .
    "   hdr_uketuke_date" .
    "  ,hdr_uketuke_no" .
    "  ,ptif_id" .
    "  ,ptr_name_kanji" .
    "  ,ptr_sex" .
    "  ,itm_item_no" .
    "  ,itm_item_name" .
    "  ,itm_ijouti_kbn" .
    "  ,itm_kensa_kekka" .
    "  ,itm_seijou_low_m" .
    "  ,itm_seijou_high_m" .
    "  ,itm_seijou_low_f" .
    "  ,itm_seijou_high_f" .
    "  ,itm_tani" .
    "  ,itm_ryouritu_kbn" .
    "  ,itm_hoken_pt" .
    "  ,itm_kekka_cmt_1" .
    "  ,itm_kekka_cmt_2" .
    " from" .
    "   sum_kensakekka" .
    " where" .
    "   hdr_update_kbn <> '9'" .
    $uketuke_date_cond .
    $uketuke_no_cond .
    $pt_id_cond .
    " order by" .
    "   hdr_uketuke_date" .
    "  ,hdr_uketuke_no" .
    "  ,ptif_id" .
    "  ,itm_item_sno" .
    "  ,itm_item_no" ;
  
  $sel = select_from_table($con, $sql, "" , $fname);
?>
   <div style="margin:5px;"></div>
   <table class="list klist" border="1" cellspacing="0" cellpadding="2" style="width:100%;">
    <tr>
     <th>受付日</th>
     <th>受付番号</th>
     <th>患者ID</th>
     <th>患者名</th>
     <th>項目コード</th>
     <th>項目名</th>
     <th>成績</th>
     <th>LH</th>
     <th>基準値</th>
     <th>単位</th>
     <th>料率区分</th>
     <th>保険点数</th>
     <th>コメント1</th>
     <th>コメント2</th>
    </tr>
<?
  while ($row = pg_fetch_array($sel))
  {
    // 性別によって基準値を決定する
    $seijou_low  = "" ;
    $seijou_high = "" ;
    $sex = strtolower($row["ptr_sex"]) ; // 小文字に変換 (例："m", "f", その他)
    if (($sex == "m") || ($sex == "f"))
    {
      $seijou_low  = $row["itm_seijou_low_"  . $sex] ;
      $seijou_high = $row["itm_seijou_high_" . $sex] ;
    }
    $kijunti = $seijou_low . " - " . $seijou_high ;
?>
    <tr>
     <td style="text-align:center;"><?=$c_sot_util->slash_yyyymmdd($row["hdr_uketuke_date"])?></td>
     <td><?=$row["hdr_uketuke_no"]?></td>
     <td><?=$row["ptif_id"]?></td>
     <td><?=$row["ptr_name_kanji"]?></td>
     <td><?=$row["itm_item_no"]?></td>
     <td><?=$row["itm_item_name"]?></td>
     <td style="text-align:right;" ><?=$row["itm_kensa_kekka"]?></td>
     <td style="text-align:center;"><?=$row["itm_ijouti_kbn"]?></td>
     <td><?=$kijunti?></td>
     <td style="text-align:center;"><?=$row["itm_tani"]?></td>
     <td style="text-align:center;"><?=$row["itm_ryouritu_kbn"]?></td>
     <td style="text-align:right;" ><?=$row["itm_hoken_pt"]?></td>
     <td><?=$row["itm_kekka_cmt_1"]?></td>
     <td><?=$row["itm_kekka_cmt_2"]?></td>
    </tr>
<?
  }
?>
   </table>
<?
}
?>
  </td>
 </tr>
</table>
</body>
<script type="text/javascript">initcal();</script>
</html>
<? pg_close($con); ?>
