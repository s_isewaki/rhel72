<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<?
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟登録権限チェック
$auth_id = ($subsys == "med") ? 57 : 14;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// チェック情報を取得
$sql = "select display1 from bedcheck";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$display1 = pg_fetch_result($sel, 0, "display1");

//--------------------患者基本情報をptifmstから取得--------------------------
$sql = "select * from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con,$sql,$cond,$fname);		//ptifmstから取得
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");
$sex = get_sex_string(pg_fetch_result($sel, 0, "ptif_sex"));
$age = get_age(pg_fetch_result($sel, 0, "ptif_birth"));

// 事業所一覧の取得
$cond = "where enti_del_flg = 'f' order by enti_id";
$sel_enti = select_from_table($con, $SQL108, $cond, $fname);
if ($sel_enti == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$arr_entiward = array();
$arr_wardptrm = array();
$arr_ptrmbed = array();
$arr_entisect = array();
$arr_sectdr = array();
$arr_sectnurse = array();

// 全事業所をループ
while ($row = pg_fetch_array($sel_enti)) {
	$enti_id = $row["enti_id"];

	// 病棟一覧の取得
	$sql = "select * from wdmst";
	$cond = "where enti_id = '$enti_id' and ward_del_flg = 'f' order by bldg_cd, ward_cd";
	$sel_ward = select_from_table($con, $sql, $cond, $fname);
	if ($sel_ward == 0) {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_ward = array();

	// 全病棟をループ
	while ($row = pg_fetch_array($sel_ward)) {
		$bldg_cd = $row["bldg_cd"];
		$ward_cd = $row["ward_cd"];
		$bldgwd = "$bldg_cd-$ward_cd";
		$ward_nm = $row["ward_name"];
		array_push($arr_ward, array("bldgwd" => $bldgwd, "ward_nm" => $ward_nm));

		// 病室一覧の取得
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_del_flg = 'f' order by ptrm_room_no";
		$sel_ptrm = select_from_table($con, $SQL72, $cond, $fname);
		if ($sel_ptrm == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr_ptrm = array();

		// 全病室をループ
		while ($row = pg_fetch_array($sel_ptrm)) {
			$ptrm_no = $row["ptrm_room_no"];
			$ptrm_nm = $row["ptrm_name"];
			$bed_chg = $row["ptrm_bed_chg"];
			if ($bed_chg > 0) {
				$ptrm_nm .= "（差額" . number_format($bed_chg) . "円）";
			}
			$ptrm_bed_cur = $row["ptrm_bed_cur"];
			array_push($arr_ptrm, array("ptrm_no" => $ptrm_no, "ptrm_nm" => $ptrm_nm));
			array_push($arr_ptrmbed, array("bldgwd" => $bldgwd, "ptrm_no" => $ptrm_no, "ptrm_bed_cur" => $ptrm_bed_cur));
		}

		array_push($arr_wardptrm, array("bldgwd" => $bldgwd, "ptrms" => $arr_ptrm));
	}

	// 診療科一覧の取得
	$cond = "where enti_id = '$enti_id' and sect_del_flg = 'f' order by sect_id";
	$sel_sect = select_from_table($con, $SQL109, $cond, $fname);
	if ($sel_sect == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_sect = array();

	// 全診療科をループ
	while ($row = pg_fetch_array($sel_sect)) {
		$sect_id = $row["sect_id"];
		$sect_nm = $row["sect_nm"];
		array_push($arr_sect, array("sect_id" => $sect_id, "sect_nm" => $sect_nm));

		// 主治医一覧の取得
		$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id' and dr_del_flg = 'f' order by dr_id";
		$sel_dr = select_from_table($con, $SQL90, $cond, $fname);
		if ($sel_dr == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr_dr = array();

		// 全主治医をループ
		while ($row = pg_fetch_array($sel_dr)) {
			$dr_id = $row["dr_id"];
			$dr_nm = $row["dr_nm"];
			array_push($arr_dr, array("dr_id" => $dr_id, "dr_nm" => $dr_nm));
		}

		array_push($arr_sectdr, array("enti_id" => $enti_id, "sect_id" => $sect_id, "drs" => $arr_dr));

		// 看護師一覧の取得
		$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id' and nurse_del_flg = 'f' order by nurse_id";
		$sel_nurse = select_from_table($con, $SQL107, $cond, $fname);
		if ($sel_nurse == 0) {
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr_nurse = array();

		// 全看護師をループ
		while ($row = pg_fetch_array($sel_nurse)) {
			$nurse_id = $row["nurse_id"];
			$nurse_nm = $row["nurse_nm"];
			array_push($arr_nurse, array("nurse_id" => $nurse_id, "nurse_nm" => $nurse_nm));
		}

		array_push($arr_sectnurse, array("enti_id" => $enti_id, "sect_id" => $sect_id, "nurses" => $arr_nurse));
	}

	array_push($arr_entiward, array("enti_id" => $enti_id, "wards" => $arr_ward));
	array_push($arr_entisect, array("enti_id" => $enti_id, "sects" => $arr_sect));
}

/*
print_r($arr_entiward);
print_r($arr_wardptrm);
print_r($arr_ptrmbed);
print_r($arr_entisect);
print_r($arr_sectdr);
print_r($arr_sectnurse);
exit;
*/

if ($back != "t") {
	//--------------------患者入院情報をinptmstから取得--------------------------
	$cond_in = "where ptif_id = '$pt_id'";
	$sel_in = select_from_table($con,$SQL111,$cond_in,$fname);		//inptmstから取得
	if($sel_in == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$enti =  pg_result($sel_in,0,"inpt_enti_id");
	$tmp_bldg_cd = pg_result($sel_in, 0, "bldg_cd");
	$tmp_ward_cd = pg_result($sel_in, 0, "ward_cd");
	$ward = ($ward == "") ? "$tmp_bldg_cd-$tmp_ward_cd" : $ward;
	$ptrm = ($ptrm == "") ? pg_result($sel_in, 0, "ptrm_room_no") : $ptrm;
	$bed = ($bed == "") ? pg_result($sel_in, 0, "inpt_bed_no") : $bed;
	$sect =  pg_result($sel_in,0,"inpt_sect_id");
	$doc =  pg_result($sel_in,0,"dr_id");
	$nurse =  pg_result($sel_in,0,"nurse_id");
	$in_dt = pg_result($sel_in,0,"inpt_in_dt");

	// 移転日時の初期値
	if (preg_match("/(\d{4})(\d{2})(\d{2})/", $date, $matches) == 1) {
		$mv_yr = $matches[1];
		$mv_mon = $matches[2];
		$mv_day = $matches[3];
	} else {
		$mv_yr = date("Y");
		$mv_mon = date("n");
		$mv_day = date("j");
	}

	// 移転日時のデフォルト
	if ($mv_hr == "") {
		$mv_hr = date("G");
	}
	if ($mv_min == "") {
		$mv_min = date("i");
	}
}

// 当該患者の転床履歴を取得
$sql = "select m.move_cfm_flg, m.move_dt, m.move_tm, fw.ward_name as from_ward_name, fr.ptrm_name as from_ptrm_name, m.from_bed_no, tw.ward_name as to_ward_name, tr.ptrm_name as to_ptrm_name, m.to_bed_no from inptmove m inner join wdmst fw on fw.bldg_cd = m.from_bldg_cd and fw.ward_cd = m.from_ward_cd inner join ptrmmst fr on fr.bldg_cd = m.from_bldg_cd and fr.ward_cd = m.from_ward_cd and fr.ptrm_room_no = m.from_ptrm_room_no inner join wdmst tw on tw.bldg_cd = m.to_bldg_cd and tw.ward_cd = m.to_ward_cd inner join ptrmmst tr on tr.bldg_cd = m.to_bldg_cd and tr.ward_cd = m.to_ward_cd and tr.ptrm_room_no = m.to_ptrm_room_no";
$cond = "where m.ptif_id = '$pt_id' and m.move_del_flg = 'f' order by m.move_dt desc, m.move_tm desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$move_records = array();
while ($row = pg_fetch_array($sel)) {
	$move_records[] = array(
		"cfm_flg" => $row["move_cfm_flg"],
		"date" => $row["move_dt"],
		"time" => $row["move_tm"],
		"from_ward_name" => $row["from_ward_name"],
		"from_ptrm_name" => $row["from_ptrm_name"],
		"from_bed_no" => $row["from_bed_no"],
		"to_ward_name" => $row["to_ward_name"],
		"to_ptrm_name" => $row["to_ptrm_name"],
		"to_bed_no" => $row["to_bed_no"]
	);
}
?>
<title>CoMedix 病床管理｜転床予定<? if ($subsys != "med") { ?>登録<? } else { ?>参照<? } ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">

function initPage() {

<? if ($enti != "") { ?>
	document.inpt.enti.value = '<? echo($enti); ?>';
<? } ?>

	entiOnChange('<? echo($ward); ?>', '<? echo($ptrm); ?>', '<? echo($bed); ?>', '<? echo($sect); ?>', '<? echo($doc); ?>', '<? echo($nurse); ?>');

}

function entiOnChange(ward, ptrm, bed, sect, doc, nurse) {

	var enti_id = document.inpt.enti.value;

	// 病棟セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.ward);

	// 病棟セレクトボックスのオプションを作成
<? foreach ($arr_entiward as $entiward) { ?>
	if (enti_id == '<? echo $entiward["enti_id"]; ?>') {
	<? foreach($entiward["wards"] as $warditem) { ?>
		addOption(document.inpt.ward, '<? echo $warditem["bldgwd"]; ?>', '<? echo $warditem["ward_nm"]; ?>', ward);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.ward.options.length == 0) {
		addOption(document.inpt.ward, '0', '（未登録）', ward);
	}

	wardOnChange(ptrm, bed);

	// 診療科セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.sect);

	// 診療科セレクトボックスのオプションを作成
<? foreach ($arr_entisect as $entisect) { ?>
	if (enti_id == '<? echo $entisect["enti_id"]; ?>') {
	<? foreach($entisect["sects"] as $sectitem) { ?>
		addOption(document.inpt.sect, '<? echo $sectitem["sect_id"]; ?>', '<? echo $sectitem["sect_nm"]; ?>', sect);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.sect.options.length == 0) {
		addOption(document.inpt.sect, '0', '（未登録）', sect);
	}

	sectOnChange(doc, nurse);

}

function wardOnChange(ptrm, bed) {

	var bldgwd = document.inpt.ward.value;

	// 病室セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.ptrm);

	// 病室セレクトボックスのオプションを作成
<? foreach ($arr_wardptrm as $wardptrm) { ?>
	if (bldgwd == '<? echo $wardptrm["bldgwd"]; ?>') {
	<? foreach($wardptrm["ptrms"] as $ptrmitem) { ?>
		addOption(document.inpt.ptrm, '<? echo $ptrmitem["ptrm_no"]; ?>', '<? echo $ptrmitem["ptrm_nm"]; ?>', ptrm);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.ptrm.options.length == 0) {
		addOption(document.inpt.ptrm, '0', '（未登録）', ptrm);
	}

	// 病室が未登録の場合、ボタンを押下不可にする
	var btnDisabled = (document.inpt.ptrm.value == '0');
	document.inpt.btnDetail.disabled = btnDisabled;

	ptrmOnChange(bed);

}

function ptrmOnChange(bed) {

	var bldgwd = document.inpt.ward.value;
	var ptrm_no = document.inpt.ptrm.value;

	// ベット番号セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.bed);

	// ベット番号セレクトボックスのオプションを作成
<? foreach ($arr_ptrmbed as $ptrmbed) { ?>
	if (bldgwd == '<? echo $ptrmbed["bldgwd"]; ?>' && ptrm_no == '<? echo $ptrmbed["ptrm_no"]; ?>') {
		for (var i = 1; i <= <? echo $ptrmbed["ptrm_bed_cur"]; ?>; i++) {
			addOption(document.inpt.bed, i.toString(), 'ベットNo.' + i, bed);
		}
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.bed.options.length == 0) {
		addOption(document.inpt.bed, '0', '（未登録）', bed);
	}
}

function sectOnChange(doc, nurse) {

	var enti_id = document.inpt.enti.value;
	var sect_id = document.inpt.sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.doc);

	// 主治医セレクトボックスのオプションを作成
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (enti_id == '<? echo $sectdr["enti_id"]; ?>' && sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.inpt.doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.doc.options.length == 0) {
		addOption(document.inpt.doc, '0', '（未登録）', doc);
	} else {
		addOption(document.inpt.doc, '0', '', doc);
	}

	if (!document.inpt.nurse) {
		return;
	}

	// 担当看護師セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.nurse);

	// 担当看護師セレクトボックスのオプションを作成
<? foreach ($arr_sectnurse as $sectnurse) { ?>
	if (enti_id == '<? echo $sectnurse["enti_id"]; ?>' && sect_id == '<? echo $sectnurse["sect_id"]; ?>') {
	<? foreach($sectnurse["nurses"] as $nurseitem) { ?>
		addOption(document.inpt.nurse, '<? echo $nurseitem["nurse_id"]; ?>', '<? echo $nurseitem["nurse_nm"]; ?>', nurse);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.nurse.options.length == 0) {
		addOption(document.inpt.nurse, '0', '（未登録）', nurse);
	} else {
		addOption(document.inpt.nurse, '0', '', nurse);
	}

}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}

}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';

}

function openEquipWin() {

	var arr_bldgwd = document.inpt.ward.value.split('-');

	var url = 'room_equipment_detail.php';
	url += '?session=<? echo $session; ?>';
	url += '&bldg_cd=' + arr_bldgwd[0];
	url += '&ward_cd=' + arr_bldgwd[1];
	url += '&rm_no=' + document.inpt.ptrm.value;

	window.open(url, 'newwin2', 'width=640,height=480,scrollbars=yes');

}

function openSearchWindow(){

	var ptrm = document.inpt.ptrm.value;
	var ward = document.inpt.ward.value;
	var in_yr = document.inpt.mv_yr.value;
	var in_mon = document.inpt.mv_mon.value;
	var in_day = document.inpt.mv_day.value;
	var in_hr = document.inpt.mv_hr.value;
	var in_min = document.inpt.mv_min.value;
	var url = "sub_vacant_room_list.php?session=<? echo($session); ?>&bldgwd="+ward+"&ptrm="+ptrm+"&in_yr="+in_yr+"&in_mon="+in_mon+"&in_day="+in_day+"&in_hr="+in_hr+"&in_min="+in_min;
	window.open(url, "vacant", "width=840,height=640,scrollbars=yes");

}

function insertDirectly() {
	document.inpt.action = 'inpatient_move_insert.php';
	document.inpt.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="860" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>転床予定<? if ($subsys != "med") { ?>登録<? } else { ?>参照<? } ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="inpt" action="inpatient_move_reserve_insert.php" method="post">
<table width="860" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($pt_id); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">性別</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($sex); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">年齢</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($age); ?>歳</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="860" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="180">
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border:#5279a5 solid 1px; background-color:#f6f9ff;">
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_sub_menu.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">基礎データ</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_detail_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">入院情報</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><b>転棟・転科</b></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_go_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">外出</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_stop_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">外泊</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="outpatient_out_reserve_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">退院予定</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="outpatient_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">退院</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_family_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">家族付き添い</a></font></td>
</tr>
</table>
</td>
<td width="5"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病棟</font></td>
<td>
<select name="ward" onChange="wardOnChange();">
</select><input value="空床検索" type="button" onclick="openSearchWindow()">
</td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病室</font></td>
<td>
<select name="ptrm" onChange="ptrmOnChange();">
</select><input type="button" name="btnDetail" value="詳細" onclick="openEquipWin();">
</td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">ベット番号</font></td>
<td colspan="3">
<select name="bed">
<option value="0">選択してください</option>
</select>
</td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">転床日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="mv_yr">
<? show_select_years_span(date("Y") - 1, date("Y") + 1, $mv_yr); ?>
</select>/<select name="mv_mon">
<? show_select_months($mv_mon); ?>
</select>/<select name="mv_day">
<? show_select_days($mv_day); ?>
</select>&nbsp;<select name="mv_hr">
<? show_select_hrs_0_23($mv_hr); ?>
</select>：<select name="mv_min">
<? show_select_min($mv_min); ?>
</select>
</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">診療科</font></td>
<td>
<select name="sect" onChange="sectOnChange();">
</select>
</td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">主治医</font></td>
<td>
<select name="doc">
</select>
</td>
</tr>
<? if ($display1 == "t") { ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">担当看護師</font></td>
<td colspan="3"><select name="nurse"></select></td>
</tr>
<? } ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">転床理由</font></td>
<td colspan="3"><textarea name="reason" cols="50" rows="4" wrap="virtual" style="ime-mode:active;"><? echo($reason); ?></textarea></td>
</tr>
</table>
<? if ($subsys != "med") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td align="right">
<input type="submit" value="予定登録">
<input type="button" value="転床登録" onclick="insertDirectly();">
</td>
</tr>
</table>
<? } ?>
<? if (count($move_records) > 0) { ?>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr bgcolor="#f6f9ff">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">履歴</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転床日時</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転床元</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転床先</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
</tr>
<?
foreach ($move_records as $tmp_move_record) {
	$tmp_cfm_flg = ($tmp_move_record["cfm_flg"] == "t") ? "転床" : "予定";
	$tmp_year = substr($tmp_move_record["date"], 0, 4);
	$tmp_month = substr($tmp_move_record["date"], 4, 2);
	$tmp_day = substr($tmp_move_record["date"], 6, 2);
	$tmp_hour = substr($tmp_move_record["time"], 0, 2);
	$tmp_minute = substr($tmp_move_record["time"], 2, 2);
?>
<tr>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_cfm_flg); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_year); ?>年<? echo($tmp_month); ?>月<? echo($tmp_day); ?>日 <? echo($tmp_hour); ?>時<? echo($tmp_minute); ?>分</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_move_record["from_ward_name"]); ?> <? echo($tmp_move_record["from_ptrm_name"]); ?> No.<? echo($tmp_move_record["from_bed_no"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_move_record["to_ward_name"]); ?> <? echo($tmp_move_record["to_ptrm_name"]); ?> No.<? echo($tmp_move_record["to_bed_no"]); ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('inpatient_move_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&date=<? echo($tmp_move_record["date"]); ?>&time=<? echo($tmp_move_record["time"]); ?>', 'rmdtlsub', 'width=640,height=480,scrollbars=yes');">詳細</a></font></td>
</tr>
<?
}
?>
</table>
<? } ?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="in_dt" value="<? echo($in_dt); ?>">
<input type="hidden" name="enti" value="1">
<input type="hidden" name="direct" value="t">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function get_sex_string($sex) {
	switch ($sex) {
	case "1":
		return "男";
	case "2":
		return "女";
	default:
		return "不明";
	}
}

function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}
?>
