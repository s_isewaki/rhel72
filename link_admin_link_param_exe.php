<?php
/*************************************************************************
リンクライブラリ管理
  パラメータ設定・更新
*************************************************************************/
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$check_auth = check_authority($session, 61, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 登録情報を配列化
$params = array();
$param_ids = array();
$len_alert_mess = "";
for ($i = 0, $j = count($param_name); $i < $j; $i++) {
	if ($param_name[$i] == "") {
		continue;
	}
	
	$params[] = array(
			"param_id"	=> $param_id[$i],
			"param_type"	=> $param_type[$i],
			"param_name"	=> p($param_name[$i]),
			"param_value"	=> ($param_type[$i] == 1) ? p($param_value[$i]) : ""
			);
	
	if (!empty($param_id[$i])) {
		$param_ids[] = $param_id[$i];
	}
        if(strlen($params[$i]["param_name"]) > 99){
                echo("<script type=\"text/javascript\">alert('".($i+1)."行目のパラメータ名が長すぎます。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
        }
        if(strlen($params[$i]["param_value"]) > 99){
                echo("<script type=\"text/javascript\">alert('".($i+1)."行目の固定値が長すぎます。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
        }
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 登録／更新対象パラメータに所属しないリンク情報を削除
$sql = "delete from link_param";
$cond = "where link_id = $link_id";
$cond .= (count($param_ids) > 0) ? " and param_id not in (" . implode(",", $param_ids) . ")" : "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// パラメータ情報を登録
foreach ($params as $param) {
	
	// パラメータID未採番なら登録処理を実行
	if (empty($param["param_id"])) {
		$sql = "select max(param_id) from link_param";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$param["param_id"] = intval(pg_fetch_result($sel, 0, 0)) + 1;
		
		$sql = <<<_SQL_END_
INSERT INTO link_param (
	param_id, 
	link_id, 
	param_type, 
	param_name, 
	param_value
) values (
_SQL_END_;
		$content = array(
				$param["param_id"], 
				$link_id, 
				$param["param_type"], 
				$param["param_name"], 
				$param["param_value"]
				);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// パラメータID採番済みなら更新処理を実行
	} else {
		$sql = "update link_param set";
		$set = array(
				"param_type", 
				"param_name", 
				"param_value"
				);
		$setvalue = array(
				$param["param_type"], 
				$param["param_name"], 
				$param["param_value"], 
				);
		$cond = "where param_id = {$param["param_id"]}";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// パラメータ区切り文字の更新
if (strlen($value_sep) > 1 || strlen($param_sep) > 1 || strlen($query_sep) > 1) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('区切り文字が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$sql = "update link set";
$set = array(
		"value_sep", 
		"param_sep", 
		"query_sep"
		);
$setvalue = array(
		p($value_sep), 
		p($param_sep), 
		p($query_sep), 
		);
$cond = "where link_id = {$link_id}";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);
?>
<script type="text/javascript">
location.href = 'link_admin_link_param.php?session=<? eh($session);?>&link_id=<? eh($link_id);?>&exe=1';
</script>