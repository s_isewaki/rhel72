<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require_once("get_values.php");
require_once("show_sinryo_top_header.ini");
require_once("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
    js_login_exit();
}

$mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
$therapy_type = isset($_REQUEST["therapy_type"]) ? $_REQUEST["therapy_type"] : "PT";
$group_code = isset($_REQUEST["group_code"]) ? $_REQUEST["group_code"] : "";
$button_name = $group_code ? "更新" : "登録";    // グループコードが設定されていれば、更新
if ($group_code) {
    $button_name = "更新";
    $tab_name    = "更新";
    $next_mode   = "update"; 
} else {
    $button_name = "登録";
    $tab_name    = "新規登録";
    $next_mode   = "insert"; 
}

//==========================================================================
// 更新
//==========================================================================
if ($mode == "insert" || $mode == "update") {
    $ret = insertGoalGroupMst($con, $mode, $therapy_type, $group_code, $fname);
    pg_close($con);
    
    if ($ret === true) {
        header("Location: summary_goal_group_list.php?session=$session&therapy_type=$therapy_type");
        die;
    } else {
        js_error_exit();
    }
}

//==========================================================================
// 以下、通常処理
//==========================================================================

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 選択した療法に該当するリハビリ目標マスタデータを取得
$rehabili_goal_mst = getRehabiliGoalMst($con, $therapy_type, $fname);
if ($rehabili_goal_mst === false) {
    pg_close($con);
    js_error_exit();
}

// 選択した療法に該当する治療プログラムマスタデータを取得
$cure_program_mst = getCureProgramMst($con, $therapy_type, $fname);
if ($cure_program_mst === false) {
    pg_close($con);
    js_error_exit();
}

$rehabili_goal_selected = array();
$cure_program_selected = array();
if ($group_code) {    // 更新時
    $rehabili_goal_selected = $c_sot_util->getGoalGroupMst($con, $therapy_type, $group_code, 'G', $fname);
    if ($rehabili_goal_selected === false) {
        pg_close($con);
        js_error_exit();
    }
    $cure_program_selected = $c_sot_util->getGoalGroupMst($con, $therapy_type, $group_code, 'P', $fname);
    if ($cure_program_selected === false) {
        pg_close($con);
        js_error_exit();
    }
    
    // 治療プログラムマスタと選択済み治療プログラムデータを、重複せずにマージ
    $cure_program_data = array_diff($cure_program_mst, $cure_program_selected);
} else {
    $cure_program_data = $cure_program_mst;
}

/**
 * 選択した療法に該当するリハビリ目標マスタデータを取得
 * 
 * @param resource $con          データベース接続リソース
 * @param string   $therapy_type 療法種別('PT':理学療法 'OT':作業療法 'ST':言語聴覚療法 'PST':心理療法)
 * @param string   $fname        ページ名
 * 
 * @return mixed 成功時:障害名一覧/失敗時:false
 */
function getRehabiliGoalMst($con, $therapy_type, $fname)
{
    // 選択済みのデータは除いて選択する
    $sql  =  'select * from sum_med_rehabili_goal_mst';
    $sql .= " where therapy_type = '$therapy_type' and is_disabled = 0";
    $sql .= " and code not in";
    $sql .= " (select code from sum_med_goal_group_mst where therapy_type = '$therapy_type' and data_type = 'G')";
    $sql .= " order by sort_order";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    
    $data = array();
    while ($row = pg_fetch_array($sel)) {
        $data[$row['code']] = $row['caption'];
    }
    return $data;
}

/**
 * 選択した療法に該当する治療プログラムマスタデータを取得
 * 
 * @param resource $con          データベース接続リソース
 * @param string   $therapy_type 療法種別('PT':理学療法 'OT':作業療法 'ST':言語聴覚療法 'PST':心理療法)
 * @param string   $fname        ページ名
 * 
 * @return mixed 成功時:障害名一覧/失敗時:false
 */
function getCureProgramMst($con, $therapy_type, $fname)
{
    $sql  = 'select * from sum_med_cure_program_mst';
    $sql .= " where therapy_type = '$therapy_type' and is_disabled = 0";
    $sql .= " order by sort_order";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    
    $data = array();
    while ($row = pg_fetch_array($sel)) {
        $data[$row['code']] = $row['caption'];
    }
    return $data;
}

/**
 * 目標グループマスタへデータを登録する
 * 
 * @param resource $con          データベース接続リソース
 * @param string   $mode         処理状態
 * @param string   $therapy_type 療法種別('PT':理学療法 'OT':作業療法 'ST':言語聴覚療法 'PST':心理療法)
 * @param string   $group_code   グループコード
 * @param string   $fname        ページ名
 * 
 * @return mixed 成功時:true/失敗時:false
 */
function insertGoalGroupMst($con, $mode, $therapy_type, $group_code, $fname)
{
    if ($mode == 'insert') {
        // 新規グループコードを取得
        $sel = select_from_table($con, "select max(group_code) from sum_med_goal_group_mst where therapy_type = '$therapy_type'", "", $fname);
        $group_code = ((int)pg_fetch_result($sel, 0, 0)) + 1;
    }
    
    // トランザクションの開始
    pg_query($con, "begin");
    
    if ($mode == 'update') {
        // 指定された療法種別とグループコードに該当するリハビリ目標データと治療プログラムデータをいったん削除
        $sql = "delete from sum_med_goal_group_mst where therapy_type='$therapy_type' and group_code = $group_code";
        $upd = update_set_table($con, $sql, array(), null, "", $fname);
        if ($upd == 0) {
            pg_query($con,"rollback");
            return false;
        }
    }

    // リハビリ目標を登録
    foreach ($_REQUEST['rehabili_goal'] as $code) {
        if (insertGoalGroupMstEach($con, $therapy_type, $group_code, 'G', $code, $fname) === false) {
            pg_query($con,"rollback");
            return false;
        }
    }
    
    // 治療プログラムを登録
    foreach ($_REQUEST['cure_program'] as $code) {
        if (insertGoalGroupMstEach($con, $therapy_type, $group_code, 'P', $code, $fname) === false) {
            pg_query($con,"rollback");
            return false;
        }
    }
    
    // トランザクションのコミット
    pg_query($con, "commit");
    return true;
}

/**
 * 目標グループマスタへ各データを登録する
 * 
 * @param resource $con          データベース接続リソース
 * @param string   $therapy_type 療法種別('PT':理学療法 'OT':作業療法 'ST':言語聴覚療法 'PST':心理療法)
 * @param string   $group_code   グループコード
 * @param string   $data_type    データ種別('G':リハビリ目標 'P'治療プログラム)
 * @param string   $code         コード
 * @param string   $fname        ページ名
 * 
 * @return mixed 成功時:true/失敗時:false
 */
function insertGoalGroupMstEach($con, $therapy_type, $group_code, $data_type, $code, $fname)
{
    
    $sql  =  "insert into sum_med_goal_group_mst (";
    $sql .=  "therapy_type";
    $sql .= ",group_code";
    $sql .= ",data_type";
    $sql .= ",code";
    $sql .= ") values (";
    $sql .= "'$therapy_type'";
    $sql .= ",$group_code";
    $sql .= ",'$data_type'";
    $sql .= ",$code)";
    
    $upd = update_set_table($con, $sql, array(), null, "", $fname);
    if ($upd == 0) {
        return false;
    }
    return true;
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | マスタ管理</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.td_border { border:#5279a5 solid 1px; }
.subtitle { font-weight:bold; margin-top:10px }
.list table { font-family: "ＭＳ Ｐゴシック", Osaka; font-size: 8px; }
.list th { border: #5279a5 solid 1px; bgcolor: #f6f9ff; font-family: "ＭＳ Ｐゴシック", "Osaka"; font-size: 13px; }
.list td { border: #5279a5 solid 1px; font-family: "ＭＳ Ｐゴシック", "Osaka"; font-size: 13px; vertical-align: top; line-height: 18px; }
</style>
<script type="text/javascript">
    // データを登録
    function registData(mode, therapy_type, group_code) {
        $('#mode').val(mode);
        if (mode == 'update') {
            $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'group_code', 'value':group_code}));
            $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'therapy_type', 'value':therapy_type}));
        }

        // リハビリ目標リストの項目と治療プログラムリストの項目をリセット
        $('input[name="rehabili_goal[]"]').remove();
        $('input[name="cure_program[]"]').remove;
        
        // リハビリ目標リストの項目をフォームに追加
        $('#rehabili_goal_dst option').each(function() {
            // selectタグのname属性を[]付きで定義すればPHP側で配列として受け取れるが、
            // 全optionを選択状態にしないと値がセットされないので、別のhidden要素にセットする。
            // (全optionを送信直前に選択状態にすると、一瞬色が変わってしまう為。)
            $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'rehabili_goal' + '[]', 'value':$(this).val()}));
        });
        
        if (!$('input[name="rehabili_goal[]"]').size()) {
            alert("リハビリ目標が選択されていません。");
            return;
        }

        // 治療プログラムリストの項目をフォームに追加
        $('#cure_program_dst option').each(function() {
            $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'cure_program' + '[]', 'value':$(this).val()}));
        });
        
        if (!$('input[name="cure_program[]"]').size()) {
            alert("治療プログラムが選択されていません。");
            return;
        }
        
        $('#mainform').submit();
    }

    // 要素の移動
    function moveItem(src, dst) {
        // 移動先へ追加
        $('#' + dst).append($('#' + src + ' option:selected'));

        // 選択を解除
        $('#' + dst + ' option:selected').width();  // IE6
        $('#' + dst + ' option:selected').attr('selected', false);
        
        // 昇順にソート
        $('#' + dst + ' option').sort(cmp).appendTo('#' + dst);
    }

    // 値の比較
    function cmp(a, b)  {
        return parseInt($(a).val()) > parseInt($(b).val()) ? 1 : -1;
    }

    // 療法選択時の処理
    function selectType() {
        document.mainform.submit();
    }
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session, $fname, "KANRI_MST"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<? echo summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">

<?
//==========================================================================
// マスタ管理メニュー
//==========================================================================
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<? summary_common_show_mst_tab_menu($session, '目標グループ'); ?>
<td>&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?
//==========================================================================
// サブメニュー
//==========================================================================
?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
  <tr height="22">
<?
    $therapy_types = array('PT' => '理学療法', 'OT' => '作業療法', 'ST' => '言語聴覚療法', 'PST' => '心理療法');
    foreach ($therapy_types as $key => $value) {
        echo "<td width=\"130\" align=\"center\" bgcolor=\"#e8e4bd\">\n";
        echo "<a href=\"summary_goal_group_list.php?session=$session&therapy_type=$key\">";
        echo "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
        echo "<b>{$value}一覧</b></font></a>\n";
        echo "</td>";
        echo "<td width=\"5\">&nbsp;</td>";
    }
?>    
    <td width="130" align="center" bgcolor="#958f28">
      <a href="summary_goal_group_regist.php?session=<? echo $session; echo $group_code ? "&therapy_type=$therapy_type&group_code=$group_code" : ''; ?>">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo $tab_name; ?></b></font>
      </a>
    </td>
    <td width="5">&nbsp;</td>
    <td></td>
  </tr>
</table>

<?
//==========================================================================
// 一覧
//==========================================================================
?>
<form action="summary_goal_group_regist.php" method="get" id="mainform" name="mainform" style="margin-top:10px">
<input type="hidden" id="session" name="session" value="<? echo $session; ?>">
<input type="hidden" id="mode" name="mode" value="<? echo $mode; ?>">

<div style="font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;">
  <div style="padding-bottom: 2px;"><b>療法</b></div>
  <div>
    <select name="therapy_type" id="therapy_type" onchange="selectType();"<? echo $group_code ? ' disabled' : ''; ?>>
    <?
        foreach ($therapy_types as $key => $value) {
            $selected = ($key == $therapy_type) ? ' selected' : '';
            echo "<option value=\"{$key}\"{$selected}>{$value}：{$key}</option>\n";
        }
    ?>
    </select>
  </div>
<?
    if ($group_code) {
        echo "<div style=\"margin-top: 15px;\"><b>No.$group_code</b></div>\n";
    }
?>
</div>


<div style="width: 740px; font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;">
  <div style="padding-bottom: 2px; margin-top: 20px;"><b>リハビリ目標</b></div>
  <div style="float: left;">
    <select id="rehabili_goal_dst" size="15" multiple style="width: 320px;">
<?
    foreach ($rehabili_goal_selected as $code => $caption) {
        echo "<option value=\"$code\">$caption</option>\n";
    }
?>
    </select>
  </div>
  <div style="float: left; width: 100px; text-align: center; padding-top: 75px;">
    <input type="button" value="≪追加" onclick="moveItem('rehabili_goal_src', 'rehabili_goal_dst');"><br><br>
    <input type="button" value="削除≫" onclick="moveItem('rehabili_goal_dst', 'rehabili_goal_src');">
  </div>
  <div style="float: left;">
    <select id="rehabili_goal_src" size="15" multiple style="width: 320px;">
<?
    foreach ($rehabili_goal_mst as $code => $caption) {
        echo "<option value=\"$code\">$caption</option>\n";
    }
?>
    </select>
  </div>
  <div style="clear: both;"></div>

  <div style="padding-bottom: 2px; margin-top: 20px;"><b>治療プログラム</b></div>
  <div style="float: left;">
    <select id="cure_program_dst" size="15" multiple style="width: 320px;">
<?
    foreach ($cure_program_selected as $code => $caption) {
        echo "<option value=\"$code\">$caption</option>\n";
    }
?>
    </select>
  </div>
  <div style="float: left; width: 100px; text-align: center; padding-top: 75px;">
    <input type="button" value="≪追加" onclick="moveItem('cure_program_src', 'cure_program_dst');"><br><br>
    <input type="button" value="削除≫" onclick="moveItem('cure_program_dst', 'cure_program_src');">
  </div>
  <div style="float: left;">
    <select id="cure_program_src" size="15" multiple style="width: 320px;">
<?
    foreach ($cure_program_data as $code => $caption) {
        echo "<option value=\"$code\">$caption</option>\n";
    }
?>
    </select>
  </div>
  <div style="clear: both;"></div>
  <div style="margin-left: 690px; margin-top: 20px;">
    <input type="button" value="<? echo $button_name; ?>" onclick="registData('<? echo $next_mode; ?>', '<? echo $therapy_type; ?>', <? echo $group_code ? $group_code : '0'; ?>);">
  </div>
</div>
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
