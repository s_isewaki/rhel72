<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理 | 詳細</title>
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$ptif = check_authority($session, 15, $fname);
if ($ptif == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者管理各権限の取得
$ptreg = check_authority($session, 22, $fname);
$outreg = check_authority($session, 35, $fname);
$inout = check_authority($session, 33, $fname);
$disreg = check_authority($session, 36, $fname);
$dishist = check_authority($session, 34, $fname);

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 患者情報を取得
$sql = "select * from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");;
	exit;
}
$lt_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 患者詳細情報を取得
if ($back != "t") {
	$sql = "select * from ptsubif";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");;
		exit;
	}

	if (pg_num_rows($sel) > 0) {
		list($height1, $height2) = explode(".", pg_fetch_result($sel, 0, "ptsubif_height"));
		if ($height2 == "") {$height2 = "0";}

		list($weight1, $weight2) = explode(".", pg_fetch_result($sel, 0, "ptsubif_weight"));
		if ($weight2 == "") {$weight2 = "0";}

		if (pg_fetch_result($sel, 0, "ptsubif_move_flg") == "t") {
			$ptsubif_move = "0";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move1") == "t") {
			$ptsubif_move = "1";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move2") == "t") {
			$ptsubif_move = "2";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move3") == "t") {
			$ptsubif_move = "3";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move4") == "t") {
			$ptsubif_move = "4";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move5") == "t") {
			$ptsubif_move = "5";
		}

		$ptsubif_move6 = pg_fetch_result($sel, 0, "ptsubif_move6");

		$ptsubif_tool_flg = pg_fetch_result($sel, 0, "ptsubif_tool_flg");
		for ($i = 1; $i <= 7; $i++) {
			$var_name = "ptsubif_tool$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_tool$i");
		}

		$blood_abo = pg_fetch_result($sel, 0, "ptsubif_blood_abo");
		$blood_rh = pg_fetch_result($sel, 0, "ptsubif_blood_rh");
		$bronchotomy = pg_fetch_result($sel, 0, "ptsubif_bronchotomy");
		$ventilator = pg_fetch_result($sel, 0, "ptsubif_ventilator");
		$experiment = pg_fetch_result($sel, 0, "ptsubif_experiment_flg");
		$dialysis_flg = pg_fetch_result($sel, 0, "ptsubif_dialysis");

		$ptsubif_algy_flg = pg_fetch_result($sel, 0, "ptsubif_algy_flg");
		for ($i = 1; $i <= 7; $i++) {
			$var_name = "ptsubif_allergy$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_allergy$i");
		}

		$allergy_note = pg_fetch_result($sel, 0, "ptsubif_algy_com");
		$meal_note = pg_fetch_result($sel, 0, "ptsubif_meal_com");

		$ptsubif_infect_flg = pg_fetch_result($sel, 0, "ptsubif_infect_flg");
		for ($i = 1; $i <= 10; $i++) {
			$var_name = "ptsubif_infection$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_infection$i");
		}

		$epidemic_note = pg_fetch_result($sel, 0, "ptsubif_infection_com");
		$note1 = pg_fetch_result($sel, 0, "ptsubif_note");

		$medical_div = pg_fetch_result($sel, 0, "ptsubif_medical_div");
		$adl_div = pg_fetch_result($sel, 0, "ptsubif_adl_div");
	}
}

$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
?>
<script type="text/javascript">
function setItemsLabel() {
	var tools = new Array();
	if (document.mainform.ptsubif_tool_flg.value == 't') {
		tools.push('なし');
	}
	if (document.mainform.ptsubif_tool1.value == 't') {
		tools.push('上肢装具');
	}
	if (document.mainform.ptsubif_tool2.value == 't') {
		tools.push('下肢装具');
	}
	if (document.mainform.ptsubif_tool3.value == 't') {
		tools.push('義歯');
	}
	if (document.mainform.ptsubif_tool4.value == 't') {
		tools.push('義眼');
	}
	if (document.mainform.ptsubif_tool5.value == 't') {
		tools.push('補聴器');
	}
	if (document.mainform.ptsubif_tool6.value == 't') {
		tools.push('車椅子');
	}
	if (document.mainform.ptsubif_tool7.value == 't') {
		tools.push('歩行器');
	}

	var algys = new Array();
	if (document.mainform.ptsubif_algy_flg.value == 't') {
		algys.push('なし');
	}
	if (document.mainform.ptsubif_allergy1.value == 't') {
		algys.push('抗生物質');
	}
	if (document.mainform.ptsubif_allergy2.value == 't') {
		algys.push('解熱鎮静剤');
	}
	if (document.mainform.ptsubif_allergy3.value == 't') {
		algys.push('局所麻酔薬');
	}
	if (document.mainform.ptsubif_allergy4.value == 't') {
		algys.push('液製剤');
	}
	if (document.mainform.ptsubif_allergy5.value == 't') {
		algys.push('ワクチン');
	}
	if (document.mainform.ptsubif_allergy6.value == 't') {
		algys.push('造影剤');
	}
	if (document.mainform.ptsubif_allergy7.value == 't') {
		algys.push('その他');
	}

	var infects = new Array();
	if (document.mainform.ptsubif_infect_flg.value == 't') {
		infects.push('なし');
	}
	if (document.mainform.ptsubif_infection1.value == 't') {
		infects.push('未検査');
	}
	if (document.mainform.ptsubif_infection2.value == 't') {
		infects.push('梅毒');
	}
	if (document.mainform.ptsubif_infection3.value == 't') {
		infects.push('Ｂ型肝炎');
	}
	if (document.mainform.ptsubif_infection4.value == 't') {
		infects.push('Ｃ型肝炎');
	}
	if (document.mainform.ptsubif_infection5.value == 't') {
		infects.push('ＨＩＶ');
	}
	if (document.mainform.ptsubif_infection6.value == 't') {
		infects.push('ＭＲＳＡ');
	}
	if (document.mainform.ptsubif_infection7.value == 't') {
		infects.push('緑膿菌');
	}
	if (document.mainform.ptsubif_infection8.value == 't') {
		infects.push('疥癬');
	}
	if (document.mainform.ptsubif_infection9.value == 't') {
		infects.push('ＡＴＬ');
	}
	if (document.mainform.ptsubif_infection10.value == 't') {
		infects.push('その他');
	}

	document.getElementById('tool').innerHTML = tools.join(', ');
	document.getElementById('algy').innerHTML = algys.join(', ');
	document.getElementById('infect').innerHTML = infects.join(', ');
}

function openItemSetting() {
	window.open('patient_item_update.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>', 'newwin', 'width=640,height=600,scrollbars=yes');
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setItemsLabel();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_check_setting.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<form name="mainform" action="patient_sub_setting_confirm.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<? if ($ptif == 1) { ?>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&page=<? echo($page); ?>&key1=<? echo($key1); ?>&key2=<? echo($ekey2); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="85" align="center" bgcolor="#bdd1e7"><a href="patient_insurance.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">健康保険</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_genogram.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">家族構成図</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="patient_sub_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="patient_contact_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_claim_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<? } ?>
<? if ($inout == 1) { ?>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="inoutpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["IN_OUT"][$profile_type]); ?></font></a></td>
<? } ?>
<? if ($dishist == 1) { ?>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="disease_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></a></td>
<? } ?>
<? if ($ptif == 1) { ?>
<td width="5">&nbsp;</td>
<td width="135" align="center" bgcolor="#bdd1e7"><a href="patient_karte.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?></font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_NAME"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_nm $ft_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">身長</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="height1" value="<? echo($height1); ?>" size="3" maxlength="3" style="ime-mode:inactive;">.<input type="text" name="height2" value="<? echo($height2); ?>" size="3" maxlength="1" style="ime-mode:inactive;">cm</font></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">体重</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="weight1" value="<? echo($weight1); ?>" size="3" maxlength="3" style="ime-mode:inactive;">.<input type="text" name="weight2" value="<? echo($weight2); ?>" size="3" maxlength="1" style="ime-mode:inactive;">kg</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">救護区分・移動手段</font></td>
<td><select name="ptsubif_move">
<option value="">
<option value="0"<? if ($ptsubif_move == "0") {echo(" selected");} ?>>独歩行
<option value="1"<? if ($ptsubif_move == "1") {echo(" selected");} ?>>杖歩行
<option value="2"<? if ($ptsubif_move == "2") {echo(" selected");} ?>>介助歩行
<option value="3"<? if ($ptsubif_move == "3") {echo(" selected");} ?>>車椅子（自己駆動）
<option value="4"<? if ($ptsubif_move == "4") {echo(" selected");} ?>>車椅子（介助）
<option value="5"<? if ($ptsubif_move == "5") {echo(" selected");} ?>>担送
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">装具・補助具</font></td>
<td style="padding:0;">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="tool"></span></font>
<input type="hidden" name="ptsubif_tool_flg" value="<? echo($ptsubif_tool_flg); ?>">
<?
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_tool$i";
	echo("<input type=\"hidden\" name=\"ptsubif_tool$i\" value=\"{$$var_name}\">\n");
}
?>
</td>
<? if ($ptreg == 1) { ?>
<td align="right" valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">酸素必要</font></td>
<td><input type="checkbox" name="ptsubif_move6" value="t"<? if ($ptsubif_move6 == "t") {echo " checked";} ?>></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人工呼吸器</font></td>
<td><input type="checkbox" name="ventilator" value="t"<? if ($ventilator == "t") {echo " checked";} ?>></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">血液型</font></td>
<td>
<select name="blood_abo">
<option value="0"<? if ($blood_abo == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($blood_abo == "1") {echo " selected";} ?>>A型
<option value="2"<? if ($blood_abo == "2") {echo " selected";} ?>>B型
<option value="3"<? if ($blood_abo == "3") {echo " selected";} ?>>AB型
<option value="4"<? if ($blood_abo == "4") {echo " selected";} ?>>O型
</select>
<select name="blood_rh">
<option value="0"<? if ($blood_rh == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($blood_rh == "1") {echo " selected";} ?>>Rh+
<option value="2"<? if ($blood_rh == "2") {echo " selected";} ?>>Rh-
</select>
</td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">気管切開</font></td>
<td><input type="checkbox" name="bronchotomy" value="t"<? if ($bronchotomy == "t") {echo " checked";} ?>></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">治験<? echo($_label_by_profile["PATIENT"][$profile_type]); ?></font></td>
<td><input type="checkbox" name="experiment" value="t"<? if ($experiment == "t") {echo " checked";} ?>></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">透析<? echo($_label_by_profile["PATIENT"][$profile_type]); ?></font></td>
<td><input type="checkbox" name="dialysis_flg" value="t"<? if ($dialysis_flg == "t") {echo " checked";} ?>></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アレルギー</font></td>
<td colspan="3" style="padding:0;">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="algy"></span></font>
<input type="hidden" name="ptsubif_algy_flg" value="<? echo($ptsubif_algy_flg); ?>">
<?
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_allergy$i";
	echo("<input type=\"hidden\" name=\"ptsubif_allergy$i\" value=\"{$$var_name}\">\n");
}
?>
</td>
<? if ($ptreg == 1) { ?>
<td align="right" valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アレルギーコメント</font></td>
<td colspan="3"><textarea cols="50" rows="5" name="allergy_note" style="ime-mode:active;"><? echo($allergy_note); ?></textarea></td>
</tr>
<!--
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事コメント</font></td>
<td colspan="3"><textarea cols="50" rows="5" name="meal_note" style="ime-mode:active;"><? echo($meal_note); ?></textarea></td>
</tr>
-->
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感染症</font></td>
<td colspan="3" style="padding:0;">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="infect"></span></font>
<input type="hidden" name="ptsubif_infect_flg" value="<? echo($ptsubif_infect_flg); ?>">
<?
for ($i = 1; $i <= 10; $i++) {
	$var_name = "ptsubif_infection$i";
	echo("<input type=\"hidden\" name=\"ptsubif_infection$i\" value=\"{$$var_name}\">\n");
}
?>
</td>
<? if ($ptreg == 1) { ?>
<td align="right" valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感染症コメント</font></td>
<td colspan="3"><textarea cols="50" rows="5" name="epidemic_note" style="ime-mode:active;"><? echo($epidemic_note); ?></textarea></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特記事項</font></td>
<td colspan="3"><textarea rows="5" cols="50" name="note1" style="ime-mode:active;"><? echo($note1); ?></textarea></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療区分</font></td>
<td><select name="medical_div">
<option value="">
<option value="1"<? if ($medical_div == "1") {echo(" selected");} ?>>1
<option value="2"<? if ($medical_div == "2") {echo(" selected");} ?>>2
<option value="3"<? if ($medical_div == "3") {echo(" selected");} ?>>3
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ADL区分</font></td>
<td><select name="adl_div">
<option value="">
<option value="1"<? if ($adl_div == "1") {echo(" selected");} ?>>1
<option value="2"<? if ($adl_div == "2") {echo(" selected");} ?>>2
<option value="3"<? if ($adl_div == "3") {echo(" selected");} ?>>3
</select></td>
</tr>
</table>
<? if ($ptreg == 1) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
