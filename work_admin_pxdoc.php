<?
//A4サイズ
define("SIZE_A4_HEIGHT", 2970);
define("SIZE_A4_WIDTH",  2100);

//表の文字開始位置
define("PRINT_TEXT_ROW_START", 260);
define("PRINT_LIST_LINE_TOP",  150);

//文字の大きさ
define("FONT_SIZE", 26);
//行間
define("INTERLINEAR_SPACE", 10);


//ini_set("display_errors", "1");
ob_start();
ini_set("max_execution_time", 0);

require_once("about_session.php");
require_once("about_authority.php");
require_once("show_attendance_pattern.ini");
require_once("show_timecard_common.ini");
require_once("holiday.php");
require_once("timecard_common_class.php");
require_once("atdbk_common_class.php");
require_once("atdbk_duty_shift_common_class.php");
require_once("pxdoc_utils.php");
require_once("work_admin_pxdoc.ini");
require_once("date_utils.php");
require_once("timecard_bean.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_paid_hol_hour_class.php");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

// 決裁欄役職情報を取得
$st_name_array = get_timecard_st_name_array($con, $fname);

// pxd名指定(yyyyMM.pxd)
$year  = substr($yyyymm, 0, 4);
$month = substr($yyyymm, 4, 2);
$file_name = sprintf("%04d%02d", $year, $month) . ".pxd";
//改ページ毎に表示する対象期間のテキストタグ
ob_clean();


//header("Content-Disposition: attachment; filename=$file_name");
//header("Content-Type: application/octet-stream; name=$file_name");

header("Content-type: text/pxd; charset=EUC-JP");
header("Content-Disposition:inline;filename=\"".$file_name."\"");

echo("<?xml version=\"1.0\" encoding=\"EUC-JP\"?>\n");
echo("<pxd paper-type=\"a4\" title=\"タイムカード\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n");

//keisen_test();
//exit;

//追加表示する事由IDを取得
$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
$arr_disp_flg = array();
$disp_cnt = 0;
for ($i=0; $i<count($arr_reason_id); $i++) {
	//休暇種別等画面の非表示フラグ
	$tmp_display_flag = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
	$arr_disp_flg[$i] = $tmp_display_flag;
	if ($tmp_display_flag == 't') {
		$disp_cnt++;
	}
}
//タイムカード集計項目出力設定からデータ取得 20091104
$arr_total_flg = get_timecard_total_flg($con, $fname); //show_timecard_common.ini
$arr_total_id = get_timecard_total_id($con, $fname);; //出力項目のid

//期間指定
$start_date = $select_start_yr.$select_start_mon.$select_start_day;
$end_date = $select_end_yr.$select_end_mon.$select_end_day;

//ソート順対応
$arr_sortkey = array();
for($i=1; $i<=5; $i++) {
	$varname = "sortkey".$i;
	$arr_sortkey[$varname] = $$varname;
}
$emp_ids = get_arr_emp_id_by_sortkey($con, $fname, $srch_name, $arr_sortkey, $emp_id_list);

// 選択された全職員をループ
//$emp_ids = explode(",", $emp_id_list);
foreach ($emp_ids as $tmp_emp_id) {
	show_attendance_book($con, $tmp_emp_id, $fname, $session, $yyyymm, $atdbk_common_class, $disp_cnt, $arr_disp_flg, $st_name_array, $arr_total_id, $arr_total_flg, $closing, $start_date, $end_date, $obj_hol_hour);
}
echo("</pxd>");

pg_query($con, "begin transaction");
//ソート順の保存
$sql = "update timecard set";
$set = array("sortkey1", "sortkey2", "sortkey3", "sortkey4", "sortkey5");
$setvalue = array($sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
pg_query("commit");

pg_close($con);
exit;


/**
 * 出勤予定を出力
 *
 * @param mixed $con DBコネクション
 * @param mixed $emp_id 職員ID
 * @param mixed $fname 画面名
 * @param mixed $session セッションID
 * @param mixed $yyyymm 年月
 * @param mixed $atdbk_common_class 出勤表共通クラス
 * @param mixed $disp_cnt 追加表示事由表示件数
 * @param mixed $arr_disp_flg 追加表示事由出力フラグ
 * @param mixed $st_name_array 役職名
 * @param mixed $arr_total_id タイムカード集計項目出力設定id
 * @param mixed $arr_total_flg タイムカード集計項目出力設定(id,name,flgの配列)
 * @param mixed $closing 締め日
 * @param mixed $start_date 開始日
 * @param mixed $end_date 終了日
 * @param mixed $obj_hol_hour 時間有休クラス
 * @return mixed 合計の配列sums
 *
 */
function show_attendance_book($con, $emp_id, $fname, $session, $yyyymm, $atdbk_common_class, $disp_cnt, $arr_disp_flg, $st_name_array, $arr_total_id, $arr_total_flg, $closing, $start_date, $end_date, $obj_hol_hour) {

	//ワークフロー関連共通クラス
	$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

    //会議研修病棟外の列を常に表示とする 20120319
    $meeting_display_flag = true;
    
    // 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理を取得
	$sql = "select duty_form, no_overtime from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
	$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
	// ************ oose add end *****************

	// ************ oose update start *****************
	$sql = "select closing, closing_parttime, closing_month_flg from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if ($duty_form == 2) {
		//非常勤
		$wk = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
		//非常勤が未設定時は常勤を使用する 20091214
		if ($wk == "") {
			$wk = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
		}
	} else {
		//常勤
		$wk = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
	}
	$closing = $wk;

	// ************ oose update end *****************
	$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";
	
    // ヘッダ・データ・罫線の開始位置情報取得
    $arr_col_list = get_col_list($con, $fname, $meeting_display_flag);
	
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}

	$start_month = intval(substr($start_date, 4, 2));
	$start_day = intval(substr($start_date, 6, 2));
	$end_month = intval(substr($end_date, 4, 2));
	$end_day = intval(substr($end_date, 6, 2));
	//一括修正申請情報
	$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($emp_id, $start_date, $end_date);

	// 氏名を取得
	$sql = "select emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
	// 職員氏名の後ろに勤務条件を追加 2008/09/18
	$str_empcond = get_employee_empcond2($con, $fname, $emp_id);
	$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date);
	//所属表示
	//タイムカード情報の取得
	$timecard_bean = new timecard_bean();
	$timecard_bean->select($con, $fname);
	$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
	$emp_name .= "  （".$str_empcond."  ".$str_shiftname."）$str_orgname";

	// 変則労働適用情報を配列で取得
	$arr_irrg_info = get_irrg_info_array($con, $emp_id, $fname);
	// 対象年月の締め状況を取得
//	$tmp_yyyymm = substr($start_date, 0, 6);
	//月度対応 20100415
	$tmp_yyyymm = $year.sprintf("%02d", $month);
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

	// 締め済みの場合、締めデータから表示対象期間を取得
	if ($atdbk_closed) {
		$sql = "select min(date), max(date) from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$start_date = pg_fetch_result($sel, 0, 0);
		$end_date = pg_fetch_result($sel, 0, 1);

		$start_year = intval(substr($start_date, 0, 4));
		$start_month = intval(substr($start_date, 4, 2));
		$start_day = intval(substr($start_date, 6, 2));
		$end_year = intval(substr($end_date, 0, 4));
		$end_month = intval(substr($end_date, 4, 2));
		$end_day = intval(substr($end_date, 6, 2));
	}

	// 給与支給区分・祝日計算方法を取得
	$sql = "select wage, hol_minus from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$wage = pg_fetch_result($sel, 0, "wage");
		$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
	} else {
		$wage = "";
		$hol_minus = "";
	}

	// 端数処理情報を取得
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = pg_fetch_result($sel, 0, "fraction$i");
			$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = pg_fetch_result($sel, 0, "others$i");
		}
		$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
		$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
		$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
		//時間給者の休憩時間追加 20100929
		$rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
		$rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
		$rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
		$rest7 = intval(pg_fetch_result($sel, 0, "rest7"));
		
		$night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
		$delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
		$early_leave_time_flg = pg_fetch_result($sel, 0, "early_leave_time_flg");
		//$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
		$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
		$timecard_title = pg_fetch_result($sel, 0, "timecard_title");
		$timecard_appr_disp_flg = pg_fetch_result($sel, 0, "timecard_appr_disp_flg");
	} else {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = "";
			$$var_name_min = "";
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = "";
		}
		$rest1 = 0;
		$rest2 = 0;
		$rest3 = 0;
		//時間給者の休憩時間追加 20100929
		$rest4 = 0;
		$rest5 = 0;
		$rest6 = 0;
		$rest7 = 0;
		
		$night_workday_count = "";
		$delay_overtime_flg = "1";
		$early_leave_time_flg = "2";
		$modify_flg = "t";
		$timecard_appr_disp_flg = "f";
	}
	//週40時間超過分は計算しないを固定とする 20100525
	$overtime_40_flg = "2";

	//集計処理の設定を「集計しない」の固定とする 20100910
	$others1 = "1";	//勤務時間内に外出した場合
	$others2 = "1";	//勤務時間外に外出した場合
	$others3 = "1";	//時間給者の休憩の集計
	$others4 = "1";	//遅刻・早退に休憩が含まれた場合
	$others5 = "2";	//早出残業時間の集計
	
	if ($timecard_title == "") {
		$timecard_title = "タイムカード";
	}
	//タイトル、決裁欄追加後の開始位置調整
	if ($timecard_appr_disp_flg == "t") {
		$line_offset = 270;
	} else {
		$line_offset = 50;
	}

	//ページ数（タイトル、表見出しの出力調整用）
	$page_cnt = 1;

	header_out2($emp_id, $emp_name, $start_year, $start_month, $start_day, $end_month, $end_day, true, true, $arr_col_list, $emp_personal_id, $timecard_appr_disp_flg, $line_offset, $st_name_array, $page_cnt, $timecard_title, $year, $month);

	//ヘッダー出力
	header_out($emp_id, $emp_name, true, true, $arr_col_list, $line_offset, $page_cnt, $timecard_bean);

	//休日出勤と判定する日設定取得 20091222
	$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $emp_id);
	
	//公休と判定する日設定 20100810
	$arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");
	
	// 集計結果格納用配列を初期化
	$sums = array_fill(0, 41, 0);

	// 勤務日・休日の名称を取得
	$sql = "select day1_time ,day2, day3, day4, day5 from calendarname";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$day2 = pg_fetch_result($sel, 0, "day2");
		$day3 = pg_fetch_result($sel, 0, "day3");
		$day4 = pg_fetch_result($sel, 0, "day4");
		$day5 = pg_fetch_result($sel, 0, "day5");
		$day1_time = pg_fetch_result($sel, 0, "day1_time");
	}
	else{
		$day1_time = "";
	}
	if ($day2 == "") {
		$day2 = "法定休日";
	}
	if ($day3 == "") {
		$day3 = "所定休日";
	}
	if ($day4 == "") {
		$day4 = "勤務日1";
	}
	if ($day5 == "") {
		$day5 = "勤務日2";
	}

	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);


	// 「退勤後復帰」ボタン表示フラグを取得
	$sql = "select ret_btn_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

	// 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
	$tmp_date = $start_date;
	$wd = date("w", to_timestamp($tmp_date));
	if ($arr_irrg_info["irrg_type"] == "1") {
		while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	} else {
	//変則労働時間・週以外の場合、日曜から
		while ($wd != 0) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	}
	//データ検索用開始日
	$wk_start_date = $tmp_date;

	//グループ毎のテーブルから勤務時間を取得する
	$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $tmp_date, $end_date);

    //個人別所定時間を優先するための対応 20120309
    $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $end_date);
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id);
    
    $counter_for_week = 1;
	$holiday_count = 0;

	//行間
	$kaigyo_size = 0;
	$line_count  = 0;
	$last_line_row_index = 0;

	// 処理日付の勤務日種別を取得
	$sql = "select date, type from calendar";
	$cond = "where date >= '$wk_start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}

	// 残業理由一覧を取得
	$sql = "select reason_id, reason, over_no_apply_flag from ovtmrsn";
	$cond = "where del_flg = 'f' order by reason_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$arr_over_no_apply_flag = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_reason_id = $row["reason_id"];
		$tmp_reason = $row["reason"];
		$tmp_over_no_apply_flag = $row["over_no_apply_flag"];
		
		$arr_over_no_apply_flag[$tmp_reason_id] = $tmp_over_no_apply_flag;
		
	}

	// 処理日付の勤務予定情報を取得
	$sql =	"SELECT ".
				"atdbk.date, ".
				"atdbk.pattern, ".
				"atdbk.reason, ".
				"atdbk.night_duty, ".
				"atdbk.allow_id, ".
				"atdbk.prov_start_time, ".
				"atdbk.prov_end_time, ".
				"atdbk.tmcd_group_id, ".
				"atdptn.workday_count ".
			"FROM ".
				"atdbk ".
					"LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
	$cond = "where emp_id = '$emp_id' and date >= '$wk_start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbk[$date]["pattern"] = $row["pattern"];
		$arr_atdbk[$date]["reason"] = $row["reason"];
		$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
		$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
		$arr_atdbk[$date]["workday_count"] = $row["workday_count"];
	}
	// 処理日付の勤務実績を取得
	$sql =	"SELECT ".
				"atdbkrslt.*, ".
				"atdptn.workday_count, ".
				"atdptn.base_time, ".
				"atdptn.over_24hour_flag, ".
				"atdptn.out_time_nosubtract_flag, ".
				"atdptn.after_night_duty_flag, ".
				"tmmdapply.apply_id     AS tmmd_apply_id, ".
				"tmmdapply.apply_status AS tmmd_apply_status, ".
				"ovtmapply.apply_id     AS ovtm_apply_id, ".
				"ovtmapply.apply_status AS ovtm_apply_status, ".
				"ovtmapply.reason AS other_reason, ".
				"ovtmapply.reason_id AS ovtm_reason_id, ".
				"ovtmrsn.reason AS ovtm_reason, ".
				"rtnapply.apply_id      AS rtn_apply_id, ".
				"rtnapply.apply_status  AS rtn_apply_status ".
			"FROM ".
				"atdbkrslt  ".
					"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
					"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
					"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
					"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ".
					"LEFT JOIN ovtmrsn on ovtmapply.reason_id = ovtmrsn.reason_id";
	$cond = "where atdbkrslt.emp_id = '$emp_id' and atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
		$arr_atdbkrslt[$date]["reason"] = $row["reason"];
		$arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
		$arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
		$arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
		$arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$arr_atdbkrslt[$date][$start_var] = $row[$start_var];
			$arr_atdbkrslt[$date][$end_var] = $row[$end_var];
		}
		$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
		$arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
		$arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
		$arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
		$arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
		$arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
		$arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
		$arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
		$arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
		$arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
		$arr_atdbkrslt[$date]["meeting_start_time"] = $row["meeting_start_time"];
		$arr_atdbkrslt[$date]["meeting_end_time"] = $row["meeting_end_time"];
		$arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
		//残業時刻追加 20100114
		$arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
		$arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
		$arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
		$arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
		//残業時刻2追加 20110616
		$arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
		$arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
		//法定内残業
		$arr_atdbkrslt[$date]["legal_in_over_time"] = $row["legal_in_over_time"];
		//早出残業 20100601
		$arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];
		//休憩時刻追加 20100921
		$arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
		$arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
		$arr_atdbkrslt[$date]["ovtm_reason"] = $row["ovtm_reason"];
		$arr_atdbkrslt[$date]["other_reason"] = $row["other_reason"];
		$arr_atdbkrslt[$date]["ovtm_reason_id"] = $row["ovtm_reason_id"];
		//外出時間を差し引かないフラグ 20110727
		$arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
		//明け追加 20110819
		$arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
	}
	//時間有休追加 20111207 
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//時間有休データをまとめて取得
		$arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($emp_id, $wk_start_date, $end_date);
		
	}	
	
	//法定内勤務、法定内残業、法定外残業
	$hoteinai = 0;
	$hoteinai_zan = 0;
	$hoteigai = 0;

	//早退時間計
	$early_leave_time = 0;

	//支給換算日数 20090908
	//$paid_day_count = 0;

	//休日出勤テーブル 2008/10/23
	$arr_hol_work_date = array();
	while ($tmp_date <= $end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		if ($counter_for_week >= 8) {
			$counter_for_week = 1;
		}
		if ($counter_for_week == 1) {
			//前日までの計
			$last_day_total = 0;
			$work_time_for_week = 0;
			if ($arr_irrg_info["irrg_type"] == "1") {
				$holiday_count = 0;
			}
			//1日毎の法定外残を週単位に集計 20090708
			$wk_week_hoteigai = 0;
		}
		$work_time = 0;
		$wk_return_time = 0;

		$start_time_info = array();
		$end_time_info = array(); //退勤時刻情報 20100910
		
		$time3 = 0; //外出時間（所定時間内） 20100910
		$time4 = 0; //外出時間（所定時間外） 20100910
		$tmp_rest2 = 0; //休憩時間 20100910
		$time3_rest = 0; //休憩時間（所定時間内）20100910
		$time4_rest = 0; //休憩時間（所定時間外）20100910
		//$arr_effective_time_wk = array(); //休憩を引く前の実働時間 20100915 20101217
		$arr_hol_time = array(); // 休日勤務情報 20101224
		
		$time_rest = "";

		$holiday_name = get_holiday_name($tmp_date);
		if ($holiday_name != "") {
			$holiday_count++;
		}

		// 処理日付の勤務日種別を取得
		$type = $arr_type[$tmp_date];

		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);
/*
		switch ($type) {
		case "1":  // 通常勤務日
			$type_name = "通常勤務日";
			break;
		case "2":  // 勤務日1
			$type_name = $day4;
			break;
		case "3":  // 勤務日2
			$type_name = $day5;
			break;
		case "4":  // 法定休日
			$type_name = $day2;
			break;
		case "5":  // 所定休日
			$type_name = $day3;
			break;
		default:
			$type_name = "";
			break;
		}
*/
		// 処理日付の勤務予定情報を取得
		$prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
		$prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
		$prov_reason = $arr_atdbk[$tmp_date]["reason"];
		$prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
		$prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

		$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
		$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
		$prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
		$prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
		$prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];
		// 要勤務日数をカウント
//		if ($tmp_date >= $start_date) {

			// カレンダーの属性を元にカウントする 2008/10/21
//			$sums[0] += $timecard_common_class->get_you_kinmu_nissu($type);
/*
			// 予定が未入力でも「休暇」でもなければカウント
			if ($prov_pattern != "" && $prov_pattern != "10") {

                // 当直あり
                if($prov_night_duty == "1")
                {
					//曜日に対応したworkday_countを取得する
					$prov_workday_count = $prov_workday_count + $timecard_common_class->get_prov_night_workday_count($tmp_date);

                }
                $sums[0] = $sums[0] + $prov_workday_count;
			}
*/
//		}

		// 処理日付の勤務実績を取得
		$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
		$reason = $arr_atdbkrslt[$tmp_date]["reason"];
        $night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
		$allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
		$start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
		$end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
		$out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
		$ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
			$$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

			if ($$start_var != "") {
				$$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
			}
			if ($$end_var != "") {
				$$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
			}
		}

		$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
		$meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

		$workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

		$previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
		$next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

		$tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
		$tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
		$ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
		$ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
		$rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
		$rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];

		$meeting_start_time = $arr_atdbkrslt[$tmp_date]["meeting_start_time"];
		$meeting_end_time = $arr_atdbkrslt[$tmp_date]["meeting_end_time"];
		$base_time = $arr_atdbkrslt[$tmp_date]["base_time"];
		//hhmmをhh:mmに変換している
		if ($start_time != "") {
			$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
		}
		if ($end_time != "") {
			$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
		}
		//表示用に退避 20100802
		$disp_start_time = $start_time;
		$disp_end_time = $end_time;

		if ($out_time != "") {
			$out_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time);
		}
		if ($ret_time != "") {
			$ret_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
		}
		if ($meeting_start_time != "") {
			$meeting_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_start_time);
		}
		if ($meeting_end_time != "") {
			$meeting_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_end_time);
		}
		if ($previous_day_flag == "") {
			$previous_day_flag = 0;
		}
		if ($next_day_flag == "") {
			$next_day_flag = 0;
		}
		//表示用翌日フラグ
		$show_next_day_flag = $next_day_flag;
		//残業時刻追加 20100114
		$over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
		$over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
		$over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
		$over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
		if ($over_start_time != "") {
			$over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
		}
		if ($over_end_time != "") {
			$over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
		}
		//残業時刻2追加 20110616
		$over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
		$over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
		$over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
		$over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
		if ($over_start_time2 != "") {
			$over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
		}
		if ($over_end_time2 != "") {
			$over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
		}
		$over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
		//法定内残業
		$legal_in_over_time = $arr_atdbkrslt[$tmp_date]["legal_in_over_time"];
		if ($legal_in_over_time != "") {
			$legal_in_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $legal_in_over_time);
		}
		//早出残業 20100601
		$early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
		if ($early_over_time != "") {
			$early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
		}
		//休憩時刻追加 20100921
		$rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
		$rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
		if ($rest_start_time != "") {
			$rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
		}
		if ($rest_end_time != "") {
			$rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
		}
		//残業理由
		$ovtm_reason = $arr_atdbkrslt[$tmp_date]["ovtm_reason"];
		$other_reason = $arr_atdbkrslt[$tmp_date]["other_reason"];
		$ovtm_reason_id = $arr_atdbkrslt[$tmp_date]["ovtm_reason_id"];
		//外出時間を差し引かないフラグ 20110727
		$out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];
		//明け追加 20110819
		$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
		
		// 残業表示用に退勤時刻を退避
		$show_end_time = $end_time;
		$show_next_day_flag = $next_day_flag;	//残業時刻日またがり対応 20100114
		// 表示用出勤時刻 20100802
		$show_start_time = $start_time;
		
		$work_workday_count = "";

		//当直の有効判定
		$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

		if ($tmp_date >= $start_date) {

			//公休か事由なし休暇で残業時刻がありの場合 20100802
			//事由に関わらず休暇で残業時刻がありの場合へ変更 20100916
			if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
				$over_start_time != "" && $over_end_time != "") {
				$legal_hol_over_flg = true;
				//残業時刻を設定
				$start_time = $over_start_time;
				$end_time = $over_end_time;
				
				//法定外残業の基準
				$wk_base_time = 0;
			} else {
				$legal_hol_over_flg = false;
			}

			// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
			if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {
				
				//日またがり対応 20101027
				if ($start_time > $end_time && 
						$previous_day_flag == "0") {
					$next_day_flag = "1";
				}
				
				//休暇時は計算しない 20090806
				if ($pattern != "10") {
					$work_workday_count = $workday_count;
				}
				if($night_duty_flag){
					//曜日に対応したworkday_countを取得する
					$work_workday_count = $work_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}
				$sums[1] = $sums[1] + $work_workday_count;
				//休日出勤のカウント方法変更 20091222
				if (check_timecard_holwk(
							$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason) == true) {
					$sums[2] += 1;
				}
			}

			// 事由ベースの日数計算
			$reason_day_count = $atdbk_common_class->get_reason_day_count($reason);
			//reason_day_countは支給換算日数となる 20090908
			if ($reason != "") {
				//$paid_day_count += $reason_day_count;
				//半日有休を区別して集計 20110204
				switch ($reason) {
					case "2": //午前有休
					case "38": //午前年休
					case "67": //午前特別
					case "50": //午前夏休
					case "69": //午前正休
						$sums[38] += $reason_day_count;
						break;
					case "3": //午後有休
					case "39": //午後年休
					case "68": //午後特別
					case "51": //午後夏休
					case "70": //午後正休
						$sums[39] += $reason_day_count;
						break;
					default: //上記以外	
						$sums[34] += $reason_day_count;
				}
			}
			//時間有休追加 20111207
			$paid_time_hour = 0;
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				$paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
				$paid_time_hour = $paid_hol_min;
				$sums[40] += $paid_hol_min; //時間有休計
			}

			switch ($reason) {
//			case "16":  // 休日出勤
//				$sums[2] += $reason_day_count;
//				break;
			case "14":  // 代替出勤
				$sums[3] += 1;
				break;
			case "15":  // 振替出勤
				$sums[4] += 1;
				break;
			case "65":  // 午前振出
			case "66":  // 午後振出
				$sums[4] += 0.5;
				break;
			case "4":  // 代替休暇
				$sums[5] += 1;
				break;
			case "18":  // 半前代替休
			case "19":  // 半後代替休
				$sums[5] += 0.5;
				break;
			case "17":  // 振替休暇
				$sums[6] += 1;
				break;
			case "20":  // 半前振替休
			case "21":  // 半後振替休
				$sums[6] += 0.5;
				break;
			case "1":  // 有休休暇
			case "37": // 年休
				$sums[7] += 1;
				break;
			case "2":  // 午前有休
			case "3":  // 午後有休
			case "38": // 午前年休
			case "39": // 午後年休
				$sums[7] += 0.5;
				break;
			case "57":  // 半有半欠
				$sums[7] += 0.5;
				$sums[8] += 0.5;
				break;
			case "6":  // 一般欠勤
			case "7":  // 病傷欠勤
				$sums[8] += 1;
				break;
			case "48": // 午前欠勤
			case "49": // 午後欠勤
				$sums[8] += 0.5;
				break;
			case "8":  // その他休
				$sums[9] += 1;
				break;
			case "52":  // 午前その他休
			case "53":  // 午後その他休
				$sums[9] += 0.5;
				break;
			case "24":  // 公休
			//case "22":  // 法定休暇
			//case "23":  // 所定休暇
			case "45":  // 希望(公休)
			case "46":  // 待機(公休)
			case "47":  // 管理当直前(公休)
				$sums[16] += 1;
				break;
			case "35":  // 午前公休
			case "36":  // 午後公休
				$sums[16] += 0.5;
				break;
			case "5":   // 特別休暇
				$sums[17] += 1;
				break;
			case "67":  // 午前特別
			case "68":  // 午後特別
				$sums[17] += 0.5;
				break;
			case "58":  // 半特半有
				$sums[17] += 0.5;
				$sums[7] += 0.5;
				break;
			case "59":  // 半特半公
				$sums[17] += 0.5;
				$sums[16] += 0.5;
				break;
			case "60":  // 半特半欠
				$sums[17] += 0.5;
				$sums[8] += 0.5;
				break;
			case "25":  // 産休
				$sums[18] += 1;
				break;
			case "26":  // 育児休業
				$sums[19] += 1;
				break;
			case "27":  // 介護休業
				$sums[20] += 1;
				break;
			case "28":  // 傷病休職
				$sums[21] += 1;
				break;
			case "29":  // 学業休職
				$sums[22] += 1;
				break;
			case "30":  // 忌引
				$sums[23] += 1;
				break;
			case "31":  // 夏休
				$sums[24] += 1;
				break;
			case "50":  // 午前夏休
			case "51":  // 午後夏休
				$sums[24] += 0.5;
				break;
			case "54":  // 半夏半公
				$sums[24] += 0.5;
				$sums[16] += 0.5;
				break;
			case "55":  // 半夏半有
				$sums[24] += 0.5;
				$sums[7] += 0.5;
				break;
			case "56":  // 半夏半欠
				$sums[24] += 0.5;
				$sums[8] += 0.5;
				break;
			case "72":  // 半夏半特
				$sums[24] += 0.5;
				$sums[17] += 0.5;
				break;
			case "61":  // 年末年始
				$sums[33] += 1;
				break;
			case "69":  // 午前正休
			case "70":  // 午後正休
				$sums[33] += 0.5;
				break;
			case "62":  // 半正半有
				$sums[33] += 0.5;
				$sums[7] += 0.5;
				break;
			case "63":  // 半正半公
				$sums[33] += 0.5;
				$sums[16] += 0.5;
				break;
			case "64":  // 半正半欠
				$sums[33] += 0.5;
				$sums[8] += 0.5;
				break;
			case "32":   // 結婚休
				$sums[25] += 1;
				break;
			case "40":   // リフレッシュ休暇
				$sums[26] += 1;
				break;
			case "42":   // 午前リフレッシュ休暇
			case "43":   // 午後リフレッシュ休暇
				$sums[26] += 0.5;
				break;
			case "41":  // 初盆休暇
				$sums[27] += 1;
				break;
			case "44":  // 半有半公
				$sums[7] += 0.5;
				$sums[16] += 0.5;
				break;
			}
			// 休日となる設定と事由があう場合に公休に計算する 20100810
			// 法定休日
			if ($arr_type[$tmp_date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
				if ($reason == "22") {
					$sums[16] += 1;
				}
			}
			// 集計行に法定休日追加 20110121
			if ($reason == "22") {
				$sums[36] += 1;
			}
			// 所定休日
			if ($arr_type[$tmp_date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
				if ($reason == "23") {
					$sums[16] += 1;
				}
			}
			// 集計行に所定休日追加 20110121
			if ($reason == "23") {
				$sums[37] += 1;
			}
			// 年末年始
			if ($arr_type[$tmp_date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
				if ($reason == "61") {
					$sums[16] += 1;
				}
				if ($reason == "62" || $reason == "63" || $reason == "64") {
					$sums[16] += 0.5;
				}
			}
		}

		// 各時間帯の開始時刻・終了時刻を変数に格納
//		$tmp_type = ($type == "4" || $type == "5") ? "1" : $type;
		//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
		$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
		$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
		$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
		$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
		$end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
		$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
		$end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

        //個人別所定時間を優先する場合 20120309
        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
            $wk_weekday = $arr_weekday_flg[$tmp_date];
            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday);
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $start2 = $arr_empcond_officehours["officehours2_start"];
                $end2 = $arr_empcond_officehours["officehours2_end"];
                $start4 = $arr_empcond_officehours["officehours4_start"];
                $end4 = $arr_empcond_officehours["officehours4_end"];
            }
        }
        
        // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
		if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
			$start2 = $prov_start_time;
			$end2 = $prov_end_time;
		}

		$over_calc_flg = false;	//残業時刻 20100525
		//残業時刻追加 20100114
		//残業開始が予定終了時刻より前の場合に設定
		if ($over_start_time != "") {
			$tmp_end2_date_time = ($start2 > $end2 && $previous_day_flag == 0) ? next_date($tmp_date).$end2 : $tmp_date.$end2;
			//残業開始日時
			if ($over_start_next_day_flag == "1") {
				$over_start_date = next_date($tmp_date);
			} else {
				$over_start_date = $tmp_date;
			}
			
			$over_start_date_time = $over_start_date.$over_start_time;
			//残業開始が予定終了時刻より前の場合に設定
			//if ($over_start_date_time < $tmp_end2_date_time) {
// 残業開始が開始予定より後の場合 20100525
			//if ($over_start_date_time < $tmp_end2_date_time && $over_start_time > $start2) { //20121019
			//	$end2   = $over_start_time;
			//}
			$over_calc_flg = true;
			//残業終了時刻 20100705
			$over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
			$over_end_date_time = $over_end_date.$over_end_time;
		}
		// 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
		$effective_time = 0;
		
		//一括修正のステータスがある場合は設定
		$tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
		if ($tmmd_apply_status == "" && $tmp_status != "") {
			$tmmd_apply_status = $tmp_status;
			$tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
		}
		//tmmd_apply_statusからlink_type取得
		$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
		$modify_apply_id  = $tmmd_apply_id;

		// 残業情報を取得
		//ovtm_apply_statusからlink_type取得
		$night_duty_time_array  = array();
		$duty_work_time         = 0;
		if ($night_duty_flag){
			//当直時間の取得
			//$night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id); //20101217

			//日数換算の数値ｘ通常勤務日の労働時間の時間
			$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
		}
		$overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
		$overtime_apply_id  = $ovtm_apply_id;

		//rtn_apply_statusからlink_type取得
		$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
		$return_apply_id  = $rtn_apply_id;

		// 所定労働・休憩開始終了日時の取得
		// 勤務開始時刻が前日の場合
		if ($previous_day_flag == "1") {
			$tmp_prev_date = last_date($tmp_date);
			$start2_date_time = $tmp_prev_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_prev_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
		} else {
			$start2_date_time = $tmp_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_end_date = next_date($tmp_date);
			} else {
				$tmp_end_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
		}

		//休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
		if ($rest_start_time != "" && $rest_end_time != "") {
			$start4 = $rest_start_time;
			$end4 = $rest_end_time;
		}
		$start4_date_time = $tmp_date.$start4;
		$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);

		//外出復帰日時の取得
		$out_date_time = $tmp_date.$out_time;
		$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

		/* 20101217
		// 時間帯データを配列に格納
		$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
		// 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
		// 24時間超える場合の翌日の休憩は配列にいれない 20101027
		if ($start2 < $start4) {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, "");  // 休憩
		} else {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
		}
		*/
		//時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持 20101217
		if ($tmp_date >= $start_date) {
			$work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
		}
		
		// 処理当日の所定労働時間を求める
		if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
			$specified_time = 0;
			//		} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算 集計処理の設定を「集計しない」の固定とする 20100910
			//			$specified_time = count($arr_time2);
		} else {
			//$specified_time = count(array_diff($arr_time2, $arr_time4));
			//時間計算変更 20101217
			$specified_time = date_utils::get_diff_minute($end2_date_time, $start2_date_time);
			if ($start4 != "" && $end4 != "") {
				$wk_rest_time = date_utils::get_diff_minute($end4_date_time, $start4_date_time);
				$specified_time -= $wk_rest_time;
			}
		}

// 基準時間の計算を変更 2008/10/21
		// 変則労働期間が月でない場合、所定労働時間を基準時間としてカウント
//		if ($tmp_date >= $start_date && $reason != "16") {
//			if ($arr_irrg_info["irrg_type"] != "2") {
//				$sums[12] += $specified_time;
				//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加
//				if ($night_duty_flag){
//					$sums[12] += $duty_work_time;
//				}
//			}
//		}

		// 出退勤とも打刻済みの場合
		if ($start_time != "" && $end_time != "") {

			// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
			$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
			$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
			if (empty($start2_date_time) || empty($end2_date_time)) {
				$start2_date_time = $start_date_time;
				$end2_date_time    = $end_date_time;
			}

			$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);
			// 残業管理をする場合、（しない場合には退勤時刻を変えずに稼動時間の計算をする）
			if ($no_overtime != "t") {
				// 残業申請中の場合は定時退勤時刻を退勤時刻として計算
//				if (!($overtime_link_type == "0" || $overtime_link_type == "3" || $overtime_link_type == "4")) {
				// 残業未承認か確認
				if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
					if (date_utils::to_timestamp_from_ymdhi($start2_date_time) > date_utils::to_timestamp_from_ymdhi($start_date_time)) { //所定より出勤が早い場合には所定にする 20100914 時刻に:があるため遅刻時間の表示されない不具合 20101005
						$start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
					}
					if ($end_date_time > $end2_date_time) {
						$end_date_time = $end2_date_time;
					}
				}
			}

			// 出勤〜退勤までの時間を配列に格納
			//$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);
			//時間計算変更 20101217
			$wk_work_time = date_utils::get_diff_minute($end_date_time, $start_date_time);
			
			//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
			//if (count($arr_tmp_work_time) > 0){
			if ($wk_work_time > 0){
				// 時間帯データを配列に格納
/*
				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
					break;
				default:
*/
					//時間計算変更 20101217
					//$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
//					break;
//				}
				//休日で残業がある場合 20101028
				if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
					//深夜残業の開始終了時間
					$start5 = "2200";
					$end5 = "0500";
				}
				//前日・翌日の深夜帯も追加する
				//$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯
				//時間計算変更 20101217
				$late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報
				
				// 確定出勤時刻／遅刻回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
					$start_time_info = array();
				} else {
					//時間計算変更 20101217
					$start_time_info = $timecard_common_class->get_start_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction1, $fraction1_min, $fraction3);
					$fixed_start_time = $start_time_info["fixed_time"];
				}

				// 確定退勤時刻／早退回数を取得
				if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
//echo("fixed_end_time=$fixed_end_time");
					$end_time_info = array();
				} else {
					//残業時刻が入っている場合は、端数処理しない 20100806
					$wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
					if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time) { //時間が所定終了よりあとか確認20111128
						$fixed_end_time = $wk_over_end_date_time;
					} else {
					//時間計算変更 20101217
						$end_time_info = $timecard_common_class->get_end_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction2, $fraction2_min, $fraction3);
						$fixed_end_time = $end_time_info["fixed_time"];
					}
					//※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
					//残業申請画面を表示しないは除く 20100811
					if ($timecard_bean->over_time_apply_type != "0") {
						if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
							//退勤時刻が所定よりあとの場合設定 20101224
							if ($fixed_end_time > $end2_date_time) {
								$fixed_end_time = $end2_date_time;
							}
						}
					}
				}
				//時間有休がある場合は、遅刻早退を計算しない 20111207
				if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
					//遅刻
					$tikoku_time = 0;
					$start_time_info["diff_minutes"] = 0;
					$start_time_info["diff_count"] = 0;
					//早退
					$sotai_time = 0;
					$end_time_info["diff_minutes"] = 0;
					$end_time_info["diff_count"] = 0;
				}
				if ($tmp_date >= $start_date) {
					//遅刻
					$sums[10] += $start_time_info["diff_count"];
					$delay_time += $start_time_info["diff_minutes"]; //遅刻時間 20100907
					//早退
					$sums[11] += $end_time_info["diff_count"];
					$early_leave_time += $end_time_info["diff_minutes"];
				}
//				if ($tmp_date >= $start_date) {
//					$sums[11] += $end_time_info["diff_count"];
//					$early_leave_time += $end_time_info["diff_minutes"];
//				}

				// 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
						// 残業時間が正しく計算されないため、休憩終了時間設定をしない 20100330
						//if (in_array($fixed_start_time, $arr_time2)) {
						$tmp_start_time = $fixed_start_time;
					//} else {
					//	$tmp_start_time = $end4_date_time;
					//}
					$tmp_end_time = $fixed_end_time;
						//時間計算変更 20101217
						//$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

					//$arr_specified_time = $arr_time2;

					//$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);
						//休憩を引く前の実働時間
						//$arr_effective_time_wk = $arr_effective_time;
						
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
					$tmp_start_time = $fixed_start_time;
						// 残業時間が正しく計算されないため、休憩開始時間設定をしない 20100330
						//if (in_array($fixed_end_time, $arr_time2)) {
						$tmp_end_time = $fixed_end_time;
					//} else {
					//	$tmp_end_time = $start4_date_time;
					//}
						//時間計算変更 20101217
						//$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

					//$arr_specified_time = $arr_time2;

					//$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);
						//休憩を引く前の実働時間
						//$arr_effective_time_wk = $arr_effective_time;
						
					break;
				default:
					$tmp_start_time = $fixed_start_time;
					$tmp_end_time = $fixed_end_time;
						//時間計算変更 20101217
						//$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
						//休憩を引く前の実働時間
						//$arr_effective_time_wk = $arr_effective_time;
					//if ($wage != "4" || ($start4 != "" && $end4 != "")) { //休憩時刻がある場合を追加 20100925
					//		$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
					//}

					//$arr_specified_time = array_diff($arr_time2, $arr_time4);

					//$paid_time = 0;

					break;
				}

				//時間計算変更 20101217
				//残業時刻追加対応 20100114
				//予定終了時刻から残業開始時刻の間を除外する
				//if ($over_start_time != "") {
					//$wk_end_date_time = $end2_date_time;
					
					//$arr_jogai = array();
					/* 20100713
					//基準日時
					$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
					}
					*/
					//残業開始時刻があとの場合
					//if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
						//時間配列を取得する（休憩扱い）
						//$arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);  						
						//休憩扱いの時間配列を除外する
						//$arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
					//}
					
				//}
				//休日勤務時間（分）と休日出勤を取得 2008/10/23
				if ($tmp_date >= $start_date) {
					//休暇時は計算しない 20090806
					//if ($pattern != "10") {
					//休日出勤の判定方法変更 20100721
					if (check_timecard_holwk(
								$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason) == true) {
						//$arr_hol_time = $timecard_common_class->get_holiday_work($arr_type, $tmp_date, $arr_effective_time, $start_time, $previous_day_flag, $end_time, $next_day_flag, $reason);
						//時間計算変更 20101217
						//所定開始時刻と残業開始時刻が一致している場合
						if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {
							$wk_end_date_time = $over_end_date_time;
						} else {
							$wk_end_date_time = $tmp_end_time;
						}
						$arr_hol_time = $timecard_common_class->get_holiday_work2($arr_type, $tmp_date, $start_date_time, $previous_day_flag, $wk_end_date_time, $next_day_flag, $reason, $work_times_info, $out_time, $ret_time);

						//$sums[31] += $arr_hol_time[3]; //20110126

						// 休日出勤した日付を配列へ設定
						for ($i=4; $i<7; $i++) {
							if ($arr_hol_time[$i] != "") {
								$arr_hol_work_date[$arr_hol_time[$i]] = 1;
							}
						}
					}
				}
				//時間計算変更 20101217
				//当直の場合当直時間を計算から除外する
				//if ($night_duty_flag){
					//稼働 - (当直 - 所定労働)
					//$arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
				//}

				// 普外時間を算出（分単位）
				// 外出時間を差し引かないフラグ対応 20110727
				if ($out_time_nosubtract_flag != "1") {
					if ($out_time != "" && $ret_time != "") { //外出 20101109
						$time3 = date_utils::get_diff_minute($ret_date_time, $out_date_time);
					}
				}
				
				// 実働時間を算出（分単位）
				//$effective_time = count($arr_effective_time);
				//時間計算変更 20101217
				$effective_time = date_utils::get_diff_minute($fixed_end_time, $fixed_start_time);
				$tmp_start_time = str_replace(":", "", $start_time);
				$time3_rest = 0;
				$time4_rest = 0;
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					if ($effective_time > 1200) {  // 20時間を超える場合 20100929
						$effective_time -= $rest7;
						$time3_rest = 0;
						$time4_rest = $rest7;
					} else if ($effective_time > 720) {  // 12時間を超える場合
						$effective_time -= $rest6;
						$time3_rest = 0;
						$time4_rest = $rest6;
					} else if ($effective_time > 540) {  // 9時間を超える場合
						$effective_time -= $rest3;
						$time3_rest = 0;
						$time4_rest = $rest3;
					} else if ($effective_time > 480) {  // 8時間を超える場合
						$effective_time -= $rest2;
						$time3_rest = $rest2;
						$time4_rest = 0;
					} else if ($effective_time > 360) {  // 6時間を超える場合
						$effective_time -= $rest1;
						$time3_rest = $rest1;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time < "1200") {  // 4時間超え午前開始の場合
						$effective_time -= $rest4;
						$time3_rest = $rest4;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time >= "1200") {  // 4時間超え午後開始の場合
						$effective_time -= $rest5;
						$time3_rest = $rest5;
						$time4_rest = 0;
					}
					//休日残業で残業申請中は休憩を表示しない 20100916
					if ($legal_hol_over_flg && $overtime_approve_flg == "1") {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//休日で残業がない場合,出勤退勤がある場合 20100917
					if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//開始した日が休日の場合に休日勤務から休憩を減算 2008/10/23
					//if (($previous_day_flag == true && $arr_hol_time[0] > 0) ||
					//	($previous_day_flag == false && $arr_hol_time[1] > 0)) {
					//	$sums[31] = $sums[31] - $time3_rest - $time4_rest;
					//}
				} else {
					//休憩時間 20101217
					if ($start4 != "" && $end4 != "") {
						$tmp_rest2 = date_utils::get_diff_minute($end4_date_time, $start4_date_time);
					}
				}

				//古い仕様のため廃止 20100330
				/*
				// 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
				if ($reason == "14" || $reason == "15") {
					$effective_time -= count($arr_specified_time);
				}
				*/
				// 稼働時間を算出（分単位）
				//$work_time = $effective_time + $paid_time - $time3 - $time4;
				// 半年休の時間をプラスしない 20100330
				$work_time = $effective_time - $time3 - $time4;
				
				//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
				if ($pattern == "10" && !$legal_hol_over_flg) {
					$work_time = 0;
				}

				// 退勤後復帰回数・時間を算出
				$return_count = 0;
				$return_time = 0;
				$return_late_time = 0; //20100715
				//申請状態 "0":申請不要 "3":承認済
				if (($return_link_type == "0" || $return_link_type == "3")) { 
					for ($i = 1; $i <= 10; $i++) {
						$start_var = "o_start_time$i";
						if ($$start_var == "") {
							break;
						}
						$return_count++;
						
						$end_var = "o_end_time$i";
						if ($$end_var == "") {
							break;
						}
						
						//端数処理 20090709
						if ($fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							//開始日時
							$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
							$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_start_time = str_replace(":", "", $$start_var);
						}
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//終了日時
							$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);
							
							$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_end_time = str_replace(":", "", $$end_var);
						}
						//$arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);
						//				$arr_ret = $timecard_common_class->get_return_array($end_date_time, $$start_var, $$end_var);
						//$return_time += count($arr_ret);
						//時間計算変更 20101217
						$wk_ret_start = $tmp_date.$tmp_ret_start_time;
						$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
						$wk_ret_time = date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
						$return_time += $wk_ret_time;
						
						// 月の深夜勤務時間に退勤後復帰の深夜分を追加
						//$return_late_time += count(array_intersect($arr_ret, $arr_time5)); //20100715
						//時間計算変更 20101217
						//$return_late_time += $timecard_common_class->get_times_info_calc($late_night_info, $wk_ret_start, $wk_ret_end);
						$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");
						$return_late_time += $wk_late_night;
						//当日が休日の場合、休日に合計。
						if (($pattern != "10" && check_timecard_holwk(
										$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "") == true) ||
								$reason == "16" ||
								$pattern == "10") {
							//当日の残業
							$today_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
							$sums[31] += $today_over;
						}
						//翌日の残業
						$next_date = next_date($tmp_date);
						if (check_timecard_holwk(
									$arr_timecard_holwk_day, $arr_type[$next_date], $pattern, $reason, "1") == true) {
							$nextday_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "2");
							$sums[31] += $nextday_over;
						}
					}
				}
				$wk_return_time = $return_time;
				//残業時刻が入力された場合、重複するため、退勤後復帰時間を加算しない。 20100714 不要 20100910
				//if ($over_calc_flg) {
				//	$wk_return_time = 0; //集計用 $return_time 表示用
				//	$return_late_time = 0; //20100715
				//}
				
				// 前日までの計 = 週計
				$last_day_total = $work_time_for_week;
				$work_time_for_week += $work_time;
				$work_time_for_week += $wk_return_time;
				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
				// 深夜勤務時間を算出（分単位）
				$time2 = 0;
				//計算方法変更 20110126
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0") {
					//残業時刻がない場合で、所定終了、退勤を比較後残業計算
					if ($over_start_time == "" || $over_end_time == "") {
                        //if ($end2_date_time < $fixed_end_time) { //退勤時刻を使用しない 20120406
						//	$over_start_date_time = $end2_date_time;
						//	$over_end_date_time = $fixed_end_time;
						//} else {
							$over_start_date_time = ""; //20101109
							$over_end_date_time = "";
						//}
					}
					//深夜残業
					$time2 = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");
					//休日残業
					//当日
					if (($pattern != "10" && check_timecard_holwk(
										$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "") == true) ||
								$reason == "16" ||
								($pattern == "10" && $over_start_time != "" && $over_end_time != "" && 
								$overtime_approve_flg != "1") //休暇で残業時刻がある場合
						) {
						$today_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "1");
						//休憩時間
						if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合
							//休憩を減算
							$today_over = $today_over - $time3_rest - $time4_rest;
						} else {
							//残業開始時刻が所定開始時刻と同じ場合で休憩がある場合は減算
							if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1"	
									&& $tmp_rest2 > 0) {
								$today_over -= $tmp_rest2;
							}
							//休憩時間確認
							else if ($start4 != "" && $end4 != "") {
								$wk_kyukei = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
								$today_over -= $wk_kyukei;
								
							}
						}
						$sums[31] += $today_over;
					}
					//翌日フラグ
					if ($next_day_flag == "1" || $over_end_next_day_flag == "1") {
						$next_date = next_date($tmp_date);
						if (check_timecard_holwk(
									$arr_timecard_holwk_day, $arr_type[$next_date], $pattern, $reason, "1") == true) {
							$nextday_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "2");
							$sums[31] += $nextday_over;
						}
					}
					//残業２
					if ($over_start_time2 != "" && $over_end_time2 != "") {
						//開始日時
						$over_start_date_time2 = ($over_start_next_day_flag2 == 1) ? next_date($tmp_date).$over_start_time2 : $tmp_date.$over_start_time2;
						//終了日時
						$over_end_date_time2 = ($over_end_next_day_flag2 == 1) ? next_date($tmp_date).$over_end_time2 : $tmp_date.$over_end_time2;
						//深夜時間帯の時間取得
						$over_time2_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time2, $over_end_date_time2, "");
						$time2 += $over_time2_late_night;
					}				
				}
				$time2 += $return_late_time; //20100715
				$sums[15] += $time2;

				// 月の稼働時間・残業時間に退勤後復帰分の残業時間を追加
				//$sums[13] += $wk_return_time; //20100714 20100910
				//				$sums[14] += $return_time;

				// 表示値の編集
				$time1 = minute_to_hmm($time1);
				$time2 = minute_to_hmm($time2);
				// 普外−＞外出に変更 20090603
				//$time3 = minute_to_hmm($time3 + $time3_rest);
				$time_outtime = minute_to_hmm($time3);
				if ($time4 > 0) {
					$time_outtime .= "(".minute_to_hmm($time4).")";
				}
				// 残外−＞休憩に変更 20090603
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					$time_rest = minute_to_hmm($time3_rest);
					if ($time4_rest > 0) {
						$time_rest .= "(".minute_to_hmm($time4_rest).")";
					}
				} else {
					//時間有休対応 20111207 休憩時間と出勤退勤時刻の重なりがあるか確認
					$tmp_rest_disp_flg = 0;
					if ($obj_hol_hour->paid_hol_hour_flag == "t") {
						$tmp_rest_disp_flg = $timecard_common_class->get_intersect_times($start_date_time, $end_date_time, $start4_date_time, $end4_date_time);
					}
					// 休憩時間
					if ($tmp_rest_disp_flg != 0) {
						$time_rest = minute_to_hmm($tmp_rest2);
					}
				}

//				$time4 = minute_to_hmm($time4 + $time4_rest);
//				if ($return_count == 0) {$return_count = "";}
				$return_time = minute_to_hmm($return_time);
			} else {
			// 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
			// 月の開始日前は処理しない
				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
			}
		} else {
			$time1 = "";
			$time2 = "";
			$time3 = "";
			$time4 = "";
			$time_outtime = "";
			$time_rest = "";
			$return_count = "";
			$return_time = "";

			// 事由が「代替休暇」「振替休暇」の場合、
			// 所定労働時間を稼働時間にプラス
			// 「有給休暇」を除く $reason == "1" || 2008/10/14 
			if ($reason == "4" || $reason == "17") {
				if ($tmp_date >= $start_date) {
					$sums[13] += $specified_time;
					$work_time_for_week += $specified_time;
				}
			}
/*
			if ($arr_irrg_info["irrg_type"] == "1" && $counter_for_week == 7) {
				$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
				if ($hol_minus == "t") {
					$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
				}
				$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
				$sums[14] += $time1;
				$time1 = minute_to_hmm($time1);
			}
*/
			// 退勤後復帰回数を算出
			$return_count = 0;
					// 退勤後復帰時間を算出 20100413
					$return_time = 0;
					//申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない
					if ($return_link_type == "0" || $return_link_type == "3") { 
						for ($i = 1; $i <= 10; $i++) {
							$start_var = "o_start_time$i";
							if ($$start_var == "") {
								break;
							}
							$return_count++;
							
							$end_var = "o_end_time$i";
					if ($$end_var == "") {
						break;
					}
					//端数処理
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						//開始日時
						$tmp_start_date_time = $tmp_date.$$start_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
						$tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_start_time = str_replace(":", "", $$start_var);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						//終了日時
						$tmp_end_date_time = $tmp_date.$$end_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);
						
						$tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_end_time = str_replace(":", "", $$end_var);
					}
					//$arr_ret = $timecard_common_class->get_return_array($tmp_date.$tmp_ret_start_time, $tmp_ret_start_time, $tmp_ret_end_time);
					
					//$return_time += count($arr_ret);
					//時間計算変更 20101217
					$wk_ret_start = $tmp_date.$tmp_ret_start_time;
					$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
					$wk_ret_time = date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
					$return_time += $wk_ret_time;
					
					//前日・翌日の深夜帯
					//$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯
					// 月の深夜勤務時間に退勤後復帰の深夜分を追加
					//$sums[15] += count(array_intersect($arr_ret, $arr_time5));
					// 月の深夜勤務時間に退勤後復帰の深夜分を追加 20101217
					//$return_late_time += $timecard_common_class->get_times_info_calc($late_night_info, $wk_ret_start, $wk_ret_end);
					//$sums[15] += $return_late_time;
				}
			}
			$wk_return_time = $return_time;
			//分を時分に変換
			$return_time = minute_to_hmm($return_time);
			
			// 前日までの計 = 週計
			$last_day_total = $work_time_for_week;
			
			$work_time_for_week += $work_time;
			$work_time_for_week += $wk_return_time;
			
			if ($tmp_date < $start_date) {
				$tmp_date = next_date($tmp_date);
				$counter_for_week++;
				continue;
			}
		}

		// 残業時間の処理を変更 20090629
		$wk_hoteigai = 0;
		$wk_hoteinai_zan = 0;
		$wk_hoteinai = 0;

		//日数換算
		$total_workday_count = "";
		if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
		{
			//休暇時は計算しない 20090806
			if ($pattern != "10") {
				if($workday_count != "")
				{
					$total_workday_count = $workday_count;
				}
			}

			if($night_duty_flag)
			{
				//曜日に対応したworkday_countを取得する
				$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

			}
		}
		else if ($workday_count == 0){
			//日数換算に0が指定された場合のみ0を入れる
			$total_workday_count = $workday_count;
		}
		//法定外残業の基準時間
		if ($base_time == "") {
			$wk_base_time = 8 * 60;
		} else {
			$base_hour = intval(substr($base_time, 0, 2));
			$base_min = intval(substr($base_time, 2, 2));
			$wk_base_time = $base_hour * 60 + $base_min;
		}
		//公休か事由なし休暇で残業時刻がありの場合 20100802
		if ($legal_hol_over_flg) {
			$wk_base_time = 0;
		}
		//残業時間 20100209 変更 20100910
		$wk_zangyo = 0;
		$kinmu_time = 0;
		//出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
		if ($start_time != "" && $end_time != "") {
			//残業承認時に計算、申請画面を表示しない場合は無条件に計算
			if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
					$timecard_bean->over_time_apply_type == "0"
				) {
				
				//残業開始、終了時刻がある場合
				if ($over_start_time != "" && $over_end_time != "") {
					$wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
				//残業時刻未設定は計算しない 20110825	
				//} else {
				//	//残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
				//	if ($end2_date_time < $fixed_end_time) {
				//		$wk_zangyo = date_utils::get_time_difference($fixed_end_time,$end2_date_time);
				//	}
				}
				//残業２
				if ($over_start_time2 != "" && $over_end_time2 != "") {
					$over_start_date_time2 = $tmp_date.$over_start_time2;
					if ($over_start_next_day_flag2 == 1) {
						$over_start_date_time2 = next_date($tmp_date).$over_start_time2;
					}
					$over_end_date_time2 = $tmp_date.$over_end_time2;
					if ($over_end_next_day_flag2 == 1) {
						$over_end_date_time2 = next_date($tmp_date).$over_end_time2;
					}
					
					$wk_zangyo2 = date_utils::get_time_difference($over_end_date_time2,$over_start_date_time2);
					$wk_zangyo += $wk_zangyo2;
				}				
				//早出残業
				if ($early_over_time != "") {
					$wk_early_over_time = hmm_to_minute($early_over_time);
					$wk_zangyo += $wk_early_over_time;
				}				
			}
			//呼出
			$wk_zangyo += $wk_return_time;
			
			if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
				//休日残業時は、休憩減算 20100916
				if ($legal_hol_over_flg == true) {
					if ($wk_zangyo >= ($time3_rest + $time4_rest)) {
						$wk_zangyo -= ($time3_rest + $time4_rest);
					}
				}
			}		
			//休日残業で休憩時刻がある場合 20100921
			if ($legal_hol_over_flg == true && $tmp_rest2 > 0) {
				$wk_zangyo -= $tmp_rest2;
			}
			//所定開始時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
			if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
				$overtime_only_flg = true;	
				//遅刻
				$tikoku_time = 0;
				//早退
				$sotai_time = 0;
				$wk_zangyo -= $tmp_rest2;
			} else {
				$overtime_only_flg = false;	
				//遅刻
				$tikoku_time = $start_time_info["diff_minutes"];
				//早退
				$sotai_time = $end_time_info["diff_minutes"];
			}		
			// 遅刻時間を残業時間から減算する場合
			if ($timecard_bean->delay_overtime_flg == "1") {
				if ($wk_zangyo >= $tikoku_time) { //20100913
					$wk_zangyo -= $tikoku_time;
					$tikoku_time2 = 0;
				} else { // 残業時間より多い遅刻時間対応 20100925 
					$tikoku_time2 = $tikoku_time - $wk_zangyo;
					$wk_zangyo = 0;
				}
			}
			
			//勤務時間＝所定労働時間−休憩時間−外出時間−早退−遅刻＋残業時間 20100907
			//　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
			//　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
			//所定
			if ($pattern == "10" || //休暇は所定を0とする 20100916
					$overtime_only_flg == true ||	//残業のみ計算する 20100917
					$after_night_duty_flag == "1"  //明けの場合 20110819
				) {
				$shotei_time = 0;
			} elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
				//$shotei_time = count($arr_effective_time_wk); //休憩を引く前の実働時間
				$shotei_time = date_utils::get_diff_minute($fixed_end_time, $fixed_start_time); // 20101217
			} else {
				//$shotei_time = count($arr_time2);
				$shotei_time = date_utils::get_diff_minute($end2_date_time, $start2_date_time); // 20101217
			}
			//時間有休追加 20111207 
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				$shotei_time -= $paid_time_hour;
			}
			//休憩 $tmp_rest2 $time3_rest $time4_rest
			//外出 $time3 $time4
			$kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
			// 遅刻時間を残業時間から減算しない場合
			if ($timecard_bean->delay_overtime_flg == "2") {
				$kinmu_time -= $tikoku_time;
			}			
			// 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
			else {
				if ($overtime_approve_flg == "1") {
					$kinmu_time -= $tikoku_time;
				} else { //遅刻時間不具合対応 20101005
					// 残業時間より多い遅刻時間対応 20100925
					$kinmu_time -= $tikoku_time2;
				}
			}
			// 早退時間を残業時間から減算しない場合
			if ($timecard_bean->early_leave_time_flg == "2") {
				$kinmu_time -= $sotai_time;
			}			
			//時間給者 20100915
			if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
				//休日残業時は、休憩を減算しない 20100916
				if ($legal_hol_over_flg != true) {
					$kinmu_time -= ($time3_rest + $time4_rest);
				}
			} else {
				if ($overtime_only_flg != true && $legal_hol_over_flg != true) {	//残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
					//月給制日給制、実働時間内の休憩時間
					$kinmu_time -= $tmp_rest2;
				}
			}
			
			//法定内、法定外残業の計算
			if ($wk_zangyo > 0) {
				//法定内入力有無確認
				if ($legal_in_over_time != "") {
					$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
					if ($wk_hoteinai_zan <= $wk_zangyo) {
						$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
					} else {
						$wk_hoteigai = 0;
					}
					
				} else {
					//法定外残業の基準時間を超える分を法定外残業とする
					if ($kinmu_time > $wk_base_time) {
						$wk_hoteigai = $kinmu_time - $wk_base_time;
					}
					//
					if ($wk_zangyo - $wk_hoteigai > 0) {
						$wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
					} else {
						//マイナスになる場合
						$wk_hoteinai_zan = 0;
						$wk_hoteigai = $wk_zangyo;
					}
				}
			}
			//合計
			//法定内
			$wk_hoteinai = $kinmu_time - $wk_zangyo;
			$hoteinai += $wk_hoteinai;
			//法定内残業
			$hoteinai_zan += $wk_hoteinai_zan;
			//法定外残
			$hoteigai += $wk_hoteigai;
			//勤務時間
			$sums[13] += $kinmu_time;
			//残業時間
			$sums[14] += $wk_zangyo;
		}
		
//comment化
if ($dummy == "1") {		
		//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100608
		if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
				$timecard_bean->over_time_apply_type == "0"
				|| $wk_return_time > 0 // 退勤後復帰がある場合 20100714
				|| $legal_hol_over_flg) { // 公休か残業時刻ありを条件追加 20100802
			//休暇時は計算しない 20090806
			if ($pattern != "10" ||
					$legal_hol_over_flg) { // 公休で残業時刻ありを条件追加 20100802
				
				//法定内勤務、法定内残業、法定外残業
				// 前日までの累計 >= 40時間
				//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
				if ($last_day_total >= 40 * 60 && $overtime_40_flg == "1") {
					//  法定外 += 稼動時間
					//下の処理で合計するためここはコメント化
					//			$hoteigai += $work_time + $wk_return_time;
				} else if ($work_time + $wk_return_time > 0){
					// 上記以外、かつ、 稼動時間がある場合
					
					//法定内勤務と法定外残業
					//法定内勤務のうち週40時間を超える分から法定外残業へ移動
					//wk法定外
					//残業時刻から計算 20100525
					if ($over_calc_flg) {
						/* 20100713
						// 端数処理する
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//基準日時
							if ($end2_date_time != "") {
								$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
							} else {
								$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							}
							$tmp_over_end_date_time = date_utils::move_time($tmp_office_start_time, $over_end_date_time, $moving_minutes, $fraction2);
						} else {
						*/
							$tmp_over_end_date_time = $over_end_date_time;
						//}
						$arr_over_time = $timecard_common_class->get_minute_time_array($over_start_date_time,$tmp_over_end_date_time);
						//20100712
						$wk_hoteigai = count(array_diff($arr_over_time, $arr_time4));
						$wk_hoteigai_org = $wk_hoteigai;
						if ($wk_hoteigai <= $effective_time) {
							$wk_hoteigai = $effective_time - $wk_base_time;
							if ($wk_hoteigai < 0) {
								$wk_hoteigai = 0;
							}
						}
						
						// 所定労働内の休憩時間 20100706
						$tmp_rest1 = count(array_intersect(array_diff($arr_time2, $arr_over_time), $arr_time4));
						// 所定労働外（残業時間内）の休憩時間
						$tmp_rest3 = count(array_intersect($arr_over_time, $arr_time4));
						$time_rest = minute_to_hmm($tmp_rest1);
						if ($tmp_rest3 > 0) {
							$time_rest .= "(".minute_to_hmm($tmp_rest3).")";
						}
						
					}else {
						$wk_hoteigai = $work_time + $wk_return_time - $wk_base_time;
					}
					if ($wk_hoteigai > 0) {
						//wk法定内 = (稼動時間 - wk法定外)
						$wk_hoteinai = ($work_time + $wk_return_time - $wk_hoteigai);
						
						// 40時間を超えた法定外を除外する
						if ($overtime_40_flg == "1") {
							$wk_jogai = $last_day_total + $work_time + $wk_return_time - (40 * 60);
							if ($wk_jogai > 0) {
								$wk_hoteigai -= $wk_jogai;
							}
						}
						
						//$arr_week_hoteigai[$counter_for_week] = $wk_hoteigai;
					} else {
						//wk法定内 = (稼動時間)
						$wk_hoteinai = ($work_time + $wk_return_time);
						
						//マイナスを0とする
						$wk_hoteigai = 0;
					}
					//echo("$tmp_date n=$wk_hoteinai g=$wk_hoteigai "); //debug
					
					//前日までの累計 ＋ wk法定内　＞ 40時間 の場合
					if ($last_day_total + $wk_hoteinai > 40 * 60) {
						if ($overtime_40_flg == "1") {
							$wk_hoteigai = 0;
							//法定内 += (40時間 - 前日までの累計) 20090722 変更
							$hoteinai += (40 * 60) - $last_day_total;
						} else {
							//法定内 += wk法定内 20090722 変更
							$hoteinai += $wk_hoteinai;
						}
					} else {
						//上記以外
						//法定内 += wk法定内
						$hoteinai += $wk_hoteinai;
					}
					
					// 法定内残業算出の所定労働時間を勤務パターン毎の時間とする 20090715
					// 法定内残業、所定労働時間から8時間まで間を加算
					// 稼動時間が所定労働時間より大きい場合
					//			$wk_minute = date_utils::hi_to_minute($day1_time); //カレンダーの所定労働時間（間違い）
					$wk_minute = $specified_time;
					// 時間帯の法定外残業の計算を使用する 480 -> $wk_base_time 20091218 20100713
					if (($wk_minute != 0 && $wk_minute < $wk_base_time && ($work_time + $wk_return_time) > $wk_minute) || $over_calc_flg) {
						
						//稼動時間が法定外残業の計算時間以上
						if (($work_time + $wk_return_time) >= $wk_base_time) {
							//基準法定内残業
							$wk_std_hoteinai_zan = $wk_base_time - $wk_minute;
						} else {
							//8時間未満の場合、稼動時間-所定労働時間
							$wk_std_hoteinai_zan = ($work_time + $wk_return_time) - $wk_minute;
							//20100713
							if ($over_calc_flg && $wk_hoteigai_org > ($wk_base_time - $specified_time)) {
								$wk_std_hoteinai_zan = $wk_hoteinai;
							}
						}
						
						//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
						if ($overtime_40_flg == "1") {
							//前日までの累計 + 所定時間 ＜ 40時間 の場合
							if ($last_day_total + $wk_minute < 40 * 60) {
								//wk法定内残業 = 40時間 - (前日までの累計 + 所定時間)
								$wk_hoteinai_zan = 40 * 60 - ($last_day_total + $wk_minute);
								//wk法定内残業 > 基準法定内残業
								if ($wk_hoteinai_zan > $wk_std_hoteinai_zan) {
									// wk法定内残業 = 基準法定内残業
									$wk_hoteinai_zan = $wk_std_hoteinai_zan;
								}
								//法定内勤務 -= wk法定内残業
								$hoteinai -= $wk_hoteinai_zan;
								
								//法定内残業 += wk法定内残業
								$hoteinai_zan += $wk_hoteinai_zan;
								
								$sums[14] += $wk_hoteinai_zan; // 変更 20090722
							}
						} else {
							// wk法定内残業 = 基準法定内残業
							$wk_hoteinai_zan = $wk_std_hoteinai_zan;
							//法定内勤務 -= wk法定内残業
							$hoteinai -= $wk_hoteinai_zan;
							
							//法定内残業 += wk法定内残業
							$hoteinai_zan += $wk_hoteinai_zan;
							
							$sums[14] += $wk_hoteinai_zan; // 変更 20090722
						}
					}
				}
				
			}
			
			//1日毎の法定外残を週単位に集計
			$wk_week_hoteigai += $wk_hoteigai;
			
			//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
			if ($overtime_40_flg == "1") {
				if ($counter_for_week == 7) {
					if ($arr_irrg_info["irrg_type"] == "1") {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
					} else {
						$tmp_irrg_minutes = 40 * 60; // 変則労働期間以外の場合40時間
					}
					if ($hol_minus == "t") {
						$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
					}
					$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
					$wk_hoteigai = $time1 + $wk_week_hoteigai;
					$hoteigai += $wk_hoteigai;
					
				} else {
					$wk_hoteigai = "";
				}
				//20090722 前日までの累計+稼働時間 >= 40時間、かつ、半日の場合、所定時間を基準とする
				if ($last_day_total + $work_time + $wk_return_time >= 40 * 60 && $workday_count == 0.5) {
					$wk_base_time = $specified_time;
				}
				$wk_hoteigai_yoteigai = $work_time + $wk_return_time - $wk_base_time;
				if ($wk_hoteigai_yoteigai > 0) {
					if ($others1 != "4") { //勤務時間内に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time3;
					}
					if ($others2 != "4") { //勤務時間外に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time4;
					}
					$sums[14] += $wk_hoteigai_yoteigai;
				}
			} else {
				
				$time1 = 0;
				// 出退勤とも打刻済みの場合
				if ($start_time != "" && $end_time != "") {
					//				$tmp_specified_time = 480; //法定内8時間を所定として処理 20090717
					$tmp_specified_time = $wk_base_time; //日数換算対応 20090803
					// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
					if ($delay_overtime_flg == "2") {
						$tmp_specified_time = $tmp_specified_time - $start_time_info["diff_minutes"];
						if ($tmp_specified_time < 0) {
							$tmp_specified_time = 0;
						}
					}
					//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
					if ($pattern != "10" || $legal_hol_over_flg) {
						//残業時刻から計算 20100525
						if ($over_calc_flg) {
							//法定外 20100705
							$time1 = $wk_hoteigai;
							
						} else {
							if ($effective_time > $tmp_specified_time) {
								$time1 = $effective_time - $tmp_specified_time;
							} else {
								$time1 = $wk_hoteigai; //20100714
							}
						}
						
						if ($others1 == "4") { //勤務時間内に外出した場合
							if ($time1 > $time3) {
								$time1 -= $time3;
							}
						}
						if ($others2 == "4") { //勤務時間外に外出した場合
							if ($time1 > $time4) {
								$time1 -= $time4;
							}
						}
					}
				}
				$wk_hoteigai = $time1; //20100714
				
				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;
				
			}
			//法定内残業が入力された場合 20100531
			if ($legal_in_over_time != "") {
				//合計を戻す
				$sums[14] -= $wk_hoteinai_zan;
				$hoteinai_zan -= $wk_hoteinai_zan;
				$sums[14] -= $wk_hoteigai;
				$hoteigai -= $wk_hoteigai;
				
				$wk_zangyo = $wk_hoteinai_zan + $wk_hoteigai;
				
				//法定内残業と法定外残業に分ける
				$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
				if ($wk_hoteinai_zan <= $wk_zangyo) {
					$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
				} else {
					$wk_hoteigai = 0;
				}
				//再度、合計する
				$sums[14] += $wk_hoteinai_zan;
				$hoteinai_zan += $wk_hoteinai_zan;
				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;
				
			}
			//早出残業が入力された場合 20100601
			if ($early_over_time != "") {
				//申請中の場合は計算しない	// 残業未承認か確認
				if (!($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3"))) {
					
					$wk_early_over_time = hmm_to_minute($early_over_time);
					$work_time += $wk_early_over_time;
					$wk_zangyo += $wk_early_over_time;
					$wk_hoteinai_zan += $wk_early_over_time;
					//合計する
					$sums[13] += $wk_early_over_time;
					$sums[14] += $wk_early_over_time;
					$hoteinai_zan += $wk_early_over_time;
				}
				
			}
		}
} //comment化		
		
		//グループ・パターン・事由の日本語名取得
		$group_name   = $atdbk_common_class->get_group_name($tmcd_group_id);
		$pattern_name = $atdbk_common_class->get_pattern_name($tmcd_group_id, $pattern);
		$reason_name  = $atdbk_common_class->get_reason_name($reason);
		$allow_value = "";
		if ($allow_id != ""){
			foreach($arr_allowance as $allowance){
				if($allow_id == $allowance["allow_id"]){
					$allow_value = $allowance["allow_contents"];
					break;
				}
			}
		}
		//残業申請不要の場合で、残業申請不要理由区分がない場合は表示しないよう空白にする
		if ($overtime_link_type == "6" && $arr_over_no_apply_flag[$ovtm_reason_id] != "t") {
			$ovtm_reason = "";
		}
		//手当てに、残業理由を連結
		if ($ovtm_reason != "" || $other_reason != "") {
			$allow_value .= "/";
			$allow_value .= ($other_reason != "") ? mb_substr($other_reason, 0, 10) : $ovtm_reason;
		}
		
		//改行が有り得る項目の中で最も長い文字列(-1した値)を取得
		$max_value_length = strlen($group_name) - 1;
		if ($max_value_length < strlen($pattern_name) - 1){
			$max_value_length = strlen($pattern_name) - 1;
		}
		if ($max_value_length < strlen($reason_name) - 1){
			$max_value_length = strlen($reason_name) - 1;
		}
		if ($max_value_length < strlen($allow_value) - 1){
			$max_value_length = strlen($allow_value) - 1;
		}

		// 項目の幅を取得
		if($arr_col_list["time_move_flag"])
		{
			$pattern_size = get_line_width($arr_col_list, 12); // 出勤予定
			$reason_size = get_line_width($arr_col_list, 13); // 事由
			$allow_size = get_line_width($arr_col_list, 15); // 手当
		}
		else
		{
			$pattern_size = get_line_width($arr_col_list, 5); // 出勤予定
			$reason_size = get_line_width($arr_col_list, 6); // 事由
			$allow_size = get_line_width($arr_col_list, 8); // 手当
		}
		$max_line_width = $pattern_size;
		if($max_line_width > $reason_size)
		{
			$max_line_width = $reason_size;
		}
		if($max_line_width > $allow_size)
		{
			$max_line_width = $allow_size;
		}

		//文字縦位置の計算(1行目の開始位置 + (文字サイズ x 行数) + (行間 x 行数))
		$text_row_index = PRINT_TEXT_ROW_START + (FONT_SIZE * $line_count) + (INTERLINEAR_SPACE * $line_count) + $kaigyo_size;
		if ($page_cnt == 1) {
			$text_row_index +=  $line_offset;
		} else {
			$text_row_index +=  50;
		}
		//text_areaは−２０する
		$textarea_row_index = $text_row_index - 20;

		//文字数から改行時の高さを取得
		$kaigyo_size += ((($max_value_length - ($max_value_length % ($max_line_width / 15)))) / ($max_line_width / 15)) * FONT_SIZE;

		//外出・休憩の改行の高さ20090727
		if ($kaigyo_size == 0 && (strlen($time_outtime) > 6 || strlen($time_rest) > 6)) {
			$kaigyo_size = FONT_SIZE;
		}

		//横線位置の計算(1行目の開始位置 + (文字サイズ x 行数) + (行間 x 行数) + 行間 )
		$line_row_index = PRINT_TEXT_ROW_START + (FONT_SIZE * $line_count) + (INTERLINEAR_SPACE * $line_count) + $kaigyo_size + INTERLINEAR_SPACE;
		if ($page_cnt == 1) {
			$line_row_index +=  $line_offset;
		} else {
			$line_row_index +=  50;
		}
		$list_frame_height = $line_row_index;

//debug
//		    pxdoc_utils::out_text($arr_col_list["col_1"]["data_start_pos"], $line_row_index, $line_row_index); // 

		//A4のサイズを超えた場合（フルで印刷出来ないため-100しとく
		if ($line_row_index > SIZE_A4_HEIGHT - 100){
			//座標リセット
			$kaigyo_size = 0;
			$line_count  = 0;

			//再計算
			//文字縦位置の計算(1行目の開始位置 + (文字サイズ x 行数) + (行間 x 行数))
			$text_row_index = PRINT_TEXT_ROW_START + 50 + (FONT_SIZE * $line_count) + (INTERLINEAR_SPACE * $line_count) + $kaigyo_size;
			//text_areaは−２０する
			$textarea_row_index = $text_row_index - 20;

			//文字数から改行時の高さを取得
//			$kaigyo_size += ((($max_value_length - ($max_value_length % 12))) / 12) * FONT_SIZE;
			$kaigyo_size += ((($max_value_length - ($max_value_length % ($max_line_width / 15)))) / ($max_line_width / 15)) * FONT_SIZE;

			//横線位置の計算(1行目の開始位置 + (文字サイズ x 行数) + (行間 x 行数) + 行間 )
			$line_row_index = PRINT_TEXT_ROW_START + 50 + (FONT_SIZE * $line_count) + (INTERLINEAR_SPACE * $line_count) + $kaigyo_size + INTERLINEAR_SPACE;
			$list_frame_height = $line_row_index;

			$page_cnt++;
			new_page($last_line_row_index, $emp_id, $emp_name, $arr_col_list, true, $line_offset, $page_cnt);
		}

		//カレンダー属性による背景色設定
		if ($type >= "4" && $type <= "7") {
			//先頭の場合、タイトル分位置調整
			if ($last_line_row_index == 0) {
				if ($page_cnt == 1) {
					$wk_line_offset = $line_offset;
				} else {
					$wk_line_offset = 50;
				}
				$wk_y = 230 + $wk_line_offset;
			} else {
				$wk_y = $last_line_row_index + 1;	//罫線が薄くなるのを防ぐため位置をずらす
			}
			$wk_x = 60 + COL_OFFSET;
			$wk_height = $line_row_index - $wk_y;
			$wk_width = $arr_col_list["col_last"]["line_start_pos"] - (60 + COL_OFFSET);
			//背景色取得
			$bgcolor = get_timecard_bgcolor($type);
			//行に背景色設定
			echo("<rect x=\"$wk_x\" y=\"$wk_y\" width=\"$wk_width\" height=\"$wk_height\" fill=\"$bgcolor\" ></rect>\n");
		}
        $tmp_night_duty = "";
        if($night_duty == "1")
        {
            $tmp_night_duty = "有";
        }
        else if($night_duty == "2")
        {
            $tmp_night_duty = "無";
        }

		// 前日になる場合があるフラグ
		$previous_day_value = "";
		if ($previous_day_flag == 1){
			$previous_day_value = "前";
		}

        // 翌
        $next_day = "";
		if ($show_next_day_flag == 1){
			$next_day = "翌";
		}

        if($arr_col_list["col_1"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_1"]["data_start_pos"], $text_row_index, $tmp_month."/".$tmp_day); // 日付
        }
        if($arr_col_list["col_2"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_2"]["data_start_pos"], $text_row_index, get_weekday(to_timestamp($tmp_date)), true); // 曜日
        }
        if($arr_col_list["col_3"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                pxdoc_utils::out_text($arr_col_list["col_3"]["data_start_pos"], $text_row_index, $previous_day_value, true); // 前
            }
            else
            {
                if($arr_col_list["col_4"]["disp_flg"] != "")
                {
                    $width = $arr_col_list["col_4"]["line_start_pos"] - $arr_col_list["col_3"]["line_start_pos"] -10;
                }
                else
                {
                    $width = $arr_col_list["col_5"]["line_start_pos"] - $arr_col_list["col_3"]["line_start_pos"] -10;
                }
		        pxdoc_utils::out_textarea($arr_col_list["col_3"]["data_start_pos"], $textarea_row_index, $width, $type_name); // 種別
            }
        }
        if($arr_col_list["col_4"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                pxdoc_utils::out_text($arr_col_list["col_4"]["data_start_pos"], $text_row_index, hhmm_to_hmm($show_start_time), false, true, $arr_col_list["col_4"]["bold_flg"]); // 出勤
            }
            else
            {
                $width = $arr_col_list["col_5"]["line_start_pos"] - $arr_col_list["col_4"]["line_start_pos"] -10;
		        pxdoc_utils::out_textarea($arr_col_list["col_4"]["data_start_pos"], $textarea_row_index, $width, $group_name); // グループ
            }
        }
        if($arr_col_list["col_5"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_5"]["data_start_pos"], $text_row_index, $next_day, true); // 翌
            }
            else
            {
               $width = $arr_col_list["col_6"]["line_start_pos"] - $arr_col_list["col_5"]["line_start_pos"] -10;
		       pxdoc_utils::out_textarea($arr_col_list["col_5"]["data_start_pos"], $textarea_row_index, $width, $pattern_name); // 出勤予定
            }
        }
        if($arr_col_list["col_6"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_6"]["data_start_pos"], $text_row_index, hhmm_to_hmm($show_end_time), false, true, $arr_col_list["col_6"]["bold_flg"]); // 退勤
            }
            else
            {
                $width = $arr_col_list["col_7"]["line_start_pos"] - $arr_col_list["col_6"]["line_start_pos"] -10;
		        pxdoc_utils::out_textarea($arr_col_list["col_6"]["data_start_pos"], $textarea_row_index, $width, $reason_name); // 事由
            }
        }
        if($arr_col_list["col_7"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
				if($timecard_bean->over_disp_split_flg != "f"){
					$tmp_add_pos = 0;
				} else {
					$tmp_add_pos = 20;
				}
				pxdoc_utils::out_text($arr_col_list["col_7"]["data_start_pos"]+$tmp_add_pos, $text_row_index, minute_to_hmm($kinmu_time), false, true); // 勤務 //$work_time + $wk_return_timeから変更 20100910
				//遅刻早退
				if ($arr_col_list["time_move_flag"] && !$arr_col_list["col_10"]["disp_flg"]) {
					pxdoc_utils::out_text($arr_col_list["col_7"]["line_start_pos"]+162+$tmp_add_pos, $text_row_index, minute_to_hmm($start_time_info["diff_minutes"]), false,       true, false, 0, "red");
					pxdoc_utils::out_text($arr_col_list["col_7"]["line_start_pos"]+250+$tmp_add_pos+$tmp_add_pos, $text_row_index, minute_to_hmm($end_time_info["diff_minutes"]), false,       true, false, 0, "red");
				}
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_7"]["data_start_pos"], $text_row_index, $tmp_night_duty, true); // 当直
            }

        }
        if($arr_col_list["col_8"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
				// 残業管理をする場合
				if ($no_overtime != "t") {
					if($timecard_bean->over_disp_split_flg != "f"){
					    pxdoc_utils::out_text($arr_col_list["col_8"]["data_start_pos"], $text_row_index, minute_to_hmm($wk_hoteinai_zan), false, true); // 法内残
					} else {
						pxdoc_utils::out_text($arr_col_list["col_8"]["data_start_pos"]+70, $text_row_index, minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai), false, true); // 残業
					}
				}
            }
            else
            {
                //前日フラグcol_9が非表示の場合、出勤時刻の位置を使用
				$wk_line_start_pos = ($arr_col_list["col_9"]["disp_flg"]) ? $arr_col_list["col_9"]["line_start_pos"] : $arr_col_list["col_10"]["line_start_pos"];
				$width = $wk_line_start_pos - $arr_col_list["col_8"]["line_start_pos"] -10;
				pxdoc_utils::out_textarea($arr_col_list["col_8"]["data_start_pos"], $textarea_row_index, $width, $allow_value); // 手当
            }
        }
        if($arr_col_list["col_9"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
				// 残業管理をする場合
				if ($no_overtime != "t") {
					if($timecard_bean->over_disp_split_flg != "f"){
						pxdoc_utils::out_text($arr_col_list["col_9"]["data_start_pos"], $text_row_index, minute_to_hmm($wk_hoteigai), false, true); // 法外残
					}
				}
            }
            else
            {
 		        pxdoc_utils::out_text($arr_col_list["col_9"]["data_start_pos"], $text_row_index, $previous_day_value, true); // 前
//                $width = $arr_col_list["col_10"]["line_start_pos"] - $arr_col_list["col_9"]["line_start_pos"] -10;
// 		        pxdoc_utils::out_textarea($arr_col_list["col_9"]["data_start_pos"], $textarea_row_index, $width, $pattern_name); // 出勤予定
            }
        }
        if($arr_col_list["col_10"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                if($arr_col_list["col_11"]["disp_flg"] != "")
                {
                    $width = $arr_col_list["col_11"]["line_start_pos"] - $arr_col_list["col_10"]["line_start_pos"] -10;
                }
                else
                {
                    $width = $arr_col_list["col_12"]["line_start_pos"] - $arr_col_list["col_10"]["line_start_pos"] -10;
                }
 		        pxdoc_utils::out_textarea($arr_col_list["col_10"]["data_start_pos"], $textarea_row_index, $width, $type_name); // 種別
            }
            else
            {
 		        pxdoc_utils::out_text($arr_col_list["col_10"]["data_start_pos"], $text_row_index, hhmm_to_hmm($show_start_time), false, true, $arr_col_list["col_10"]["bold_flg"]); // 出勤
            }
        }
        if($arr_col_list["col_11"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                $width = $arr_col_list["col_12"]["line_start_pos"] - $arr_col_list["col_11"]["line_start_pos"] -10;
		        pxdoc_utils::out_textarea($arr_col_list["col_11"]["data_start_pos"], $textarea_row_index, $width, $group_name); // グループ
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_11"]["data_start_pos"], $text_row_index, $next_day, true); // 翌
            }
        }
        if($arr_col_list["col_12"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                $width = $arr_col_list["col_13"]["line_start_pos"] - $arr_col_list["col_12"]["line_start_pos"] -10;
 		        pxdoc_utils::out_textarea($arr_col_list["col_12"]["data_start_pos"], $textarea_row_index, $width, $pattern_name); // 出勤予定
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_12"]["data_start_pos"], $text_row_index, hhmm_to_hmm($show_end_time), false, true, $arr_col_list["col_12"]["bold_flg"]); // 退勤
            }
        }
        if($arr_col_list["col_13"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                $width = $arr_col_list["col_14"]["line_start_pos"] - $arr_col_list["col_13"]["line_start_pos"] -10;
		        pxdoc_utils::out_textarea($arr_col_list["col_13"]["data_start_pos"], $textarea_row_index, $width, $reason_name); // 事由
            }
            else
            {
				if($timecard_bean->over_disp_split_flg != "f"){
					$tmp_add_pos = 0;
				} else {
					$tmp_add_pos = 20;
				}
				pxdoc_utils::out_text($arr_col_list["col_13"]["data_start_pos"]+$tmp_add_pos, $text_row_index, minute_to_hmm($kinmu_time), false, true); // 勤務 //$work_time + $wk_return_timeから変更 20100910
				//遅刻早退
				if (!$arr_col_list["time_move_flag"] && !$arr_col_list["col_3"]["disp_flg"]) {
					pxdoc_utils::out_text($arr_col_list["col_13"]["line_start_pos"]+162+$tmp_add_pos, $text_row_index, minute_to_hmm($start_time_info["diff_minutes"]), false,       true, false, 0, "red");
					pxdoc_utils::out_text($arr_col_list["col_13"]["line_start_pos"]+250+$tmp_add_pos+$tmp_add_pos, $text_row_index, minute_to_hmm($end_time_info["diff_minutes"]), false,       true, false, 0, "red");
				}
			}
        }
        if($arr_col_list["col_14"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_14"]["data_start_pos"], $text_row_index, $tmp_night_duty, true); // 当直
            }
            else
            {
				// 残業管理をする場合
				if ($no_overtime != "t") {
					if($timecard_bean->over_disp_split_flg != "f"){
						pxdoc_utils::out_text($arr_col_list["col_14"]["data_start_pos"], $text_row_index, minute_to_hmm($wk_hoteinai_zan), false, true); // 法内残
					} else {
						pxdoc_utils::out_text($arr_col_list["col_14"]["data_start_pos"]+70, $text_row_index, minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai), false, true); // 残業
					}
				}
			}
        }
        if($arr_col_list["col_15"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                $width = $arr_col_list["col_16"]["line_start_pos"] - $arr_col_list["col_15"]["line_start_pos"] -10;
				pxdoc_utils::out_textarea($arr_col_list["col_15"]["data_start_pos"], $textarea_row_index, $width, $allow_value); // 手当
            }
            else
            {
				// 残業管理をする場合
				if ($no_overtime != "t") {
					if($timecard_bean->over_disp_split_flg != "f"){
						pxdoc_utils::out_text($arr_col_list["col_15"]["data_start_pos"], $text_row_index, minute_to_hmm($wk_hoteigai), false, true); // 法外残
					}
				}
			}
        }
        if($arr_col_list["col_16"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_16"]["data_start_pos"], $text_row_index, $time2, false, true); // 深勤
        }
        if($arr_col_list["col_17"]["disp_flg"])
        {
//text -> text_area 20090727
			$width = $arr_col_list["col_18"]["line_start_pos"] - $arr_col_list["col_17"]["line_start_pos"];
			pxdoc_utils::out_textarea($arr_col_list["col_17"]["data_start_pos"], $textarea_row_index, $width, $time_outtime); // 外出
        }
        if($arr_col_list["col_18"]["disp_flg"])
        {
//text -> text_area 20090727
			$width = $arr_col_list["col_19"]["line_start_pos"] - $arr_col_list["col_18"]["line_start_pos"];
			pxdoc_utils::out_textarea($arr_col_list["col_18"]["data_start_pos"], $textarea_row_index, $width, $time_rest); // 休憩

        }
		if ($return_count == "0"){
			$return_count = "";
		}
        if($arr_col_list["col_19"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_19"]["data_start_pos"], $text_row_index, $return_time, false, true); // 退勤後復帰(時間)
        }

		if($arr_col_list["col_20"]["disp_flg"] == true)
		{
			// 出勤・退勤時刻がある場合のみ表示する 20090702 //明けを追加 20110819
			if (!(($start_time != "" && $end_time != "") || $after_night_duty_flag == "1")) {
				$total_workday_count = "";
			}
			pxdoc_utils::out_text($arr_col_list["col_20"]["data_start_pos"], $text_row_index, $total_workday_count, false, true); // 日数
		}
		if ($meeting_display_flag){
			//会議研修
			$meeting_time_hh = "";
			$meeting_time_mm = "";
			$meeting_time_value = "";
			// 開始終了時刻から時間を計算する 20091008
			if ($meeting_start_time != "" && $meeting_end_time != "") {
				$tmp_meeting_start_date_time = $tmp_date.$meeting_start_time;
				if ($meeting_start_time <= $meeting_end_time) {
					$tmp_meeting_end_date_time = $tmp_date.$meeting_end_time;
				} else {
					// 日またがりの場合
					$tmp_meeting_end_date_time = next_date($tmp_date).$meeting_end_time;
				}
				$tmp_meeting_time = date_utils::get_time_difference($tmp_meeting_end_date_time, $tmp_meeting_start_date_time);
				$tmp_meeting_hhmm = $timecard_common_class->minute_to_h_hh($tmp_meeting_time);
				$meeting_time_value = $tmp_meeting_hhmm;
			} else 
			if ($meeting_time != null){
				$meeting_time_hh = (int)substr($meeting_time, 0, 2);
				$meeting_time_mm = (int)substr($meeting_time, 2, 4);
				$meeting_time_value = $meeting_time_hh;

				if ($meeting_time_mm == 15){
					$meeting_time_value = $meeting_time_value.".25";
				} else if ($meeting_time_mm == 30){
					$meeting_time_value = $meeting_time_value.".5";
				} else if ($meeting_time_mm == 45){
					$meeting_time_value = $meeting_time_value.".75";
				}

			}
			if($arr_col_list["col_21"]["disp_flg"] == true)
			{
 				pxdoc_utils::out_text($arr_col_list["col_21"]["data_start_pos"], $text_row_index, $meeting_time_value, false, true); // 会議研修
			}
		}

		//罫線
		pxdoc_utils::out_horizontal_line(60 + COL_OFFSET, $line_row_index, $arr_col_list["col_last"]["line_start_pos"]);

		$last_line_row_index = $line_row_index;
		$line_count++;

		$tmp_date = next_date($tmp_date);
		$counter_for_week++;
	} // end of while

	// 早退時間を残業時間から減算する設定の場合、法定内残業、法定外残業の順に減算する
	if ($early_leave_time_flg == "1") {

		list($hoteinai_zan, $hoteigai) = $atdbk_common_class->get_overtime_early_leave($hoteinai_zan, $hoteigai, $early_leave_time);
		//残業時間 20100713
		$sums[14] = $hoteinai_zan + $hoteigai;
	}
	// 要勤務日数を取得 20091222
	$wk_year = substr($yyyymm, 0, 4);
	$wk_mon = substr($yyyymm, 4, 2);
	//当月日数
	if ($closing_month_flg == "2") { //締め日の月度が終了日の月の場合
		$wk_year2 = substr($start_date, 0, 4);
		$wk_mon2 = substr($start_date, 4, 2);
	} else {
		$wk_year2 = $wk_year;
		$wk_mon2 = $wk_mon;
	}
	$days_in_month = days_in_month($wk_year2, $wk_mon2);
	//対象年月の公休数を取得 20100615
	$legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $wk_year, $wk_mon, $closing, $closing_month_flg, $tmcd_group_id);
	$sums[0] = $days_in_month - $legal_hol_cnt;
	
	// 法定内 = 勤務時間 - 残業時間 20100713
	$sums[28] = $sums[13] - $sums[14];
	// 法定内残業
	$sums[29] = $hoteinai_zan;
	// 法定外残業
	$sums[30] = $hoteigai;

	// 変則労働期間が月の場合
	if ($arr_irrg_info["irrg_type"] == "2") {

		// 所定労働時間を基準時間とする
		$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
		if ($hol_minus == "t") {
			$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
		}
		$sums[12] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

		// 稼働時間−基準時間を残業時間とする
		$sums[14] = $sums[13] - $sums[12];
	}
	else {
	// 基準時間の計算を変更 2008/10/21
	// 要勤務日数 × 所定労働時間 
		$sums[12] = $sums[0] * date_utils::hi_to_minute($day1_time);
	}

	// 月集計値の稼働時間・残業時間・深夜勤務を端数処理
	for ($i = 13; $i <= 15; $i++) {
		if ($sums[$i] < 0) {
			$sums[$i] = 0;
		}
		$sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
	}

	// 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
	if ($no_overtime == "t") {
		$sums[14] = 0;
		$sums[28] = 0;
		$sums[29] = 0;
		$sums[30] = 0;
	}
	//時間有休 20111207
	$paid_hol_hour_total = 0;
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//１日分の所定労働時間(分)
		$specified_time_per_day = $obj_hol_hour->get_specified_time($emp_id, $day1_time);
		//当月分
		//$wk_day = $sums[40] / $specified_time_per_day;
		//$sums[7] += $obj_hol_hour->edit_nenkyu_zan($wk_day);
		
		//時間有休合計取得
		$paid_hol_hour_total = $obj_hol_hour->get_paid_hol_hour_total($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $start_date, $end_date, $specified_time_per_day, 1);
	}
	// 月集計値を表示用に編集
	for ($i = 0; $i <= 9; $i++) {
		$sums[$i] .= "日";
	}
    //当月分の時間有休を有休年休欄に追加出力 20120604
    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        if ($sums[40] > 0) {
            $wk_hour = $sums[40] / 60;
            $sums[7] .= $wk_hour; //."時間"
        }
    }
    for ($i = 10; $i <= 11; $i++) {
		$sums[$i] .= "回";
	}
	for ($i = 12; $i <= 15; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	for ($i = 16; $i <= 27; $i++) {
		$sums[$i] .= "日";
	}
	for ($i = 28; $i <= 31; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	//年末年始追加 20090908
	$sums[33] .= "日";
	//支給換算日数追加 20090908
	//$sums[34] = $paid_day_count;
	//年休残追加 20100625
	$sums[35] = $timecard_common_class->get_nenkyu_zan($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $start_date, $end_date);//."日";
	//時間有休 20111207
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        //時間有休がある場合の年休残文字列取得 20120604
        $ret_str = $obj_hol_hour->get_nenkyu_zan_str($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $start_date, $end_date, $specified_time_per_day, $sums[35], $duty_form, "2", $timecard_common_class);
        $sums[35] = $ret_str;
    }
    else {
        $sums[35] .= "日";
    }
    
	//法定追加 20110121
	$sums[36] .= "日";
	//所定追加 20110121
	$sums[37] .= "日";
	
	echo("			</g>\n");
	line_out($line_row_index, $arr_col_list, $line_offset, true, $page_cnt, $timecard_bean);

	total_table_out($sums, $line_row_index, $emp_id, $emp_name, $con, $fname, $start_date, $end_date, false, $arr_col_list, $disp_cnt, $arr_disp_flg, $no_overtime, $atdbk_common_class, $line_offset, $page_cnt, $arr_total_id, $arr_total_flg, $timecard_bean, $obj_hol_hour);

	echo("		</svg>\n");
	echo("	</page>\n");

	return $sums;
}


function total_table_out($sums, $last_line_row_index, $emp_id, $emp_name, $con, $fname, $start_date, $end_date, $atdbk_closed, $arr_col_list, $disp_cnt, $arr_disp_flg, $no_overtime, $atdbk_common_class, $line_offset, $page_cnt, $arr_total_id, $arr_total_flg, $timecard_bean, $obj_hol_hour){

	//集計項目出力設定 20091104
	$wk_disp_cnt = 0;
	for ($i=0; $i<18; $i++) {
		if ($arr_total_flg[$i]["flg"] == "t") {
			$wk_disp_cnt++;
		}
	}
	//最終行から２つ分行間空ける
	if ($wk_disp_cnt > 0) {
		$table_index_top      = $last_line_row_index + (INTERLINEAR_SPACE * 2);
		$table_index_left     = 60 + COL_OFFSET;
		$table_index_right    = $arr_col_list["col_last"]["line_start_pos"];
		//タイトル2行分 + タイトル上部の行間スペース確保
		$table_index_separate = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 2) + INTERLINEAR_SPACE;
		//タイトル2行＋データ1行分 + タイトル上部の行間スペース確保
		$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;
		
		echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
		
		//A4のサイズを超えた場合（フルで印刷出来ないため-100しとく
		if ($table_index_bottom > SIZE_A4_HEIGHT - 100){
			//座標リセット
			$table_index_top      = PRINT_LIST_LINE_TOP;
			$table_index_left     = 60 + COL_OFFSET;
			$table_index_right    = $arr_col_list["col_last"]["line_start_pos"];
			//タイトル2行分 + タイトル上部の行間スペース確保
			$table_index_separate = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 2) + INTERLINEAR_SPACE;
			//タイトル2行＋データ1行分 + タイトル上部の行間スペース確保
			$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;
			
			new_page($last_line_row_index, $emp_id, $emp_name, $arr_col_list, false, $line_offset, $page_cnt);
		}
		
		//枠線
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_top,       $table_index_right,  2, 3);
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_separate,  $table_index_right,  2, 3);
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_bottom,    $table_index_right,  2, 3);
		pxdoc_utils::out_vertical_line(  $table_index_left,     $table_index_top,       $table_index_bottom, 2, 3);
		pxdoc_utils::out_vertical_line(  $table_index_right,    $table_index_top,       $table_index_bottom, 2, 3);

		//テキスト位置（タイトル下段）
		$text_top    = $table_index_top  + ((INTERLINEAR_SPACE + FONT_SIZE) * 2);
		//テキスト位置（タイトル上段）
		$text_top2   = $table_index_top  + INTERLINEAR_SPACE + FONT_SIZE;
		//テキスト位置（タイトル中段）
		$text_top3   = $table_index_top  + (((INTERLINEAR_SPACE + FONT_SIZE) * 3) / 2);
		//テキスト位置（値）
		$text_top4   = $text_top + INTERLINEAR_SPACE + FONT_SIZE;
		//テキスト位置（左）
		$left_index  = $table_index_left;

		//出力項目数により幅を計算
		$add_pos = ceil($arr_col_list["col_last"]["line_start_pos"] - $table_index_left) / ($wk_disp_cnt*2);
		
		//見出し配列
		$arr_total_text = array();
		$arr_total_text[] = array("top2" => "要勤務", "top" => "日数", "top3" => "");	//0
		$arr_total_text[] = array("top2" => "出勤", "top" => "日数", "top3" => "");
		$arr_total_text[] = array("top2" => "休日", "top" => "出勤", "top3" => "");
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "代出");
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "振出");
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "代休");			//5
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "振休");
		$arr_total_text[] = array("top2" => "年休", "top" => "有休", "top3" => "");
		$arr_total_text[] = array("top2" => "年残", "top" => "有残", "top3" => "");
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "欠勤");
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "その他");			//10
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "遅刻");
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "早退");
		$arr_total_text[] = array("top2" => "基準", "top" => "時間", "top3" => "");
		$arr_total_text[] = array("top2" => "勤務", "top" => "時間", "top3" => "");
		$arr_total_text[] = array("top2" => "支給", "top" => "時間", "top3" => "");	//15
//		$arr_total_text[] = array("top2" => "予定外", "top" => "時間", "top3" => "");	//15
		$arr_total_text[] = array("top2" => "", "top" => "", "top3" => "残業");
		$arr_total_text[] = array("top2" => "深夜", "top" => "時間", "top3" => "");
		
		for ($i=0; $i<count($arr_total_text); $i++) {
			//出力設定確認
			if ($arr_total_flg[$i]["flg"] != "t") {
				continue;
			}

			$left_index = $left_index + $add_pos;
			if ($arr_total_text[$i]["top2"] != "") {
				pxdoc_utils::out_text($left_index, $text_top2, $arr_total_text[$i]["top2"], true);
			}
			if ($arr_total_text[$i]["top"] != "") {
				pxdoc_utils::out_text($left_index, $text_top,  $arr_total_text[$i]["top"],   true);
			}
			if ($arr_total_text[$i]["top3"] != "") {
				pxdoc_utils::out_text($left_index, $text_top3, $arr_total_text[$i]["top3"], true);
			}
			if ($i < 8) {
				$wk_str = $sums[$i];
			} elseif ($i== 8) {
				$wk_str = $sums[35]; //年残
			} elseif ($i == 15) { //
				$paid_hol_days = $sums[34]; //支給換算日数
				//$paid_time = $atdbk_common_class->get_paid_time($sums[13], $paid_hol_days, $emp_id);
				//半日有休対応 20110204
				$paid_time = $atdbk_common_class->get_paid_time($sums[13], $paid_hol_days, $sums[38], $sums[39], $emp_id);
				//時間有休追加 20111207 
				if ($obj_hol_hour->paid_hol_hour_flag == "t") {
					$paid_time += $sums[40];
				}
				$wk_str = ($paid_time == 0) ? "0:00" : minute_to_hmm($paid_time);
			} elseif ($i > 8 && $i < 15) {
				$wk_idx = $i - 1;
				$wk_str = $sums[$wk_idx];
			} else {
				$wk_idx = $i - 2;
				$wk_str = $sums[$wk_idx];
			}
			//遅刻2_10、早退2_11で1回以上あったら、赤で表示する 20101210
			if (($arr_total_flg[$i]["id"] == "2_10" ||
						$arr_total_flg[$i]["id"] == "2_11") && $sums[$wk_idx] > 0) {
				$wk_color = "#ff0000";
			} else {
				$wk_color = "#000000";
			}
			pxdoc_utils::out_text($left_index, $text_top4, $wk_str, true, false, false, 0, $wk_color);
			$left_index = $left_index + $add_pos;
			pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
		}
		echo("			</g>\n");
	} else {
		//表示件数がない場合、下側位置に前の行位置を設定
		$table_index_bottom   = $last_line_row_index;
	}
	
	
/*
	//０：要勤務日数
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "要勤務", true);
	pxdoc_utils::out_text($left_index, $text_top,  "日数",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[0], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１：出勤日数
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "出勤",   true);
	pxdoc_utils::out_text($left_index, $text_top,  "日数",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[1], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//２：休日出勤
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "休日",   true);
	pxdoc_utils::out_text($left_index, $text_top,  "出勤",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[2], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//３：代出
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "代出",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[3], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//４：振出
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "振出",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[4], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//５：代休
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "代休",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[5], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//６：振休
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "振休",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[6], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//７：有給
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "年休", true);
	pxdoc_utils::out_text($left_index, $text_top,  "有休",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[7], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//８：欠勤
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "欠勤",   true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[8], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//９：その他
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "その他", true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[9], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１０：遅刻
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "遅刻",    true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[10], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１１：早退
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top3, "早退",    true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[11], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１２：基準時間
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "基準",    true);
	pxdoc_utils::out_text($left_index, $text_top,  "時間",    true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[12], true);

	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１３：稼働時間->勤務時間
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "勤務",    true);
	pxdoc_utils::out_text($left_index, $text_top,  "時間",    true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[13], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１４：支給時間
	// 支給時間＝勤務時間＋有給取得日数×所定労働時間
//	$paid_hol_days = substr($sums[7], 0, strlen($sums[7])-2); //"日"を除く
	// 有給取得日数を支給換算日数に変更 20090908
	$paid_hol_days = $sums[34]; //支給換算日数
	$paid_time = $atdbk_common_class->get_paid_time($sums[13], $paid_hol_days, $emp_id);
	$paid_time = ($paid_time == 0) ? "0:00" : minute_to_hmm($paid_time);

	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "支給",    true);
	pxdoc_utils::out_text($left_index, $text_top,  "時間",    true);
	pxdoc_utils::out_text($left_index, $text_top4, $paid_time, true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１５：残業時間->予定外残業
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "予定外",    true);
	pxdoc_utils::out_text($left_index, $text_top,  "残業",    true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[14], true);
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	//１６：深夜勤務
	$left_index = $left_index + $add_pos;
	pxdoc_utils::out_text($left_index, $text_top2, "深夜", true);
	pxdoc_utils::out_text($left_index, $text_top,  "勤務", true);
	pxdoc_utils::out_text($left_index, $text_top4, $sums[15], 4, true);
	$left_index = $left_index + $add_pos;
*/

//	$last_line_row_index = employee_empcond_out($con, $fname, $emp_id, $table_index_bottom, $arr_col_list);

// 法定内勤務〜法定外残業、追加事由分
	//法定内等件数
	$hotei_disp_cnt = 0;
	for ($i=18; $i<22; $i++) {
		if ($arr_total_flg[$i]["flg"] == "t") {
			$hotei_disp_cnt++;
		}
	}
	//休暇日数が非表示の場合は件数を０とする
	if (in_array("10_0", $arr_total_id)==false) {
		$disp_cnt = 0;
	}
	
	echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
	
	//法定内等と休暇の件数がある場合
	if ($disp_cnt + $hotei_disp_cnt > 0) {	
		//集計表から2つ分行間空ける
		$table_index_top      = $table_index_bottom + (INTERLINEAR_SPACE * 2);
		$table_index_left     = 60 + COL_OFFSET;
		$table_index_right    = $arr_col_list["col_last"]["line_start_pos"];
		//タイトル2行分 + タイトル上部の行間スペース確保
		$table_index_separate = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 2) + INTERLINEAR_SPACE;
		//タイトル2行＋データ1行分 + タイトル上部の行間スペース確保
		$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;

		//A4のサイズを超えた場合（フルで印刷出来ないため-100しとく
		if ($table_index_bottom > SIZE_A4_HEIGHT - 100){
			//座標リセット
			$table_index_top      = PRINT_LIST_LINE_TOP;
			$table_index_left     = 60 + COL_OFFSET;
			$table_index_right    = $arr_col_list["col_last"]["line_start_pos"];
			//タイトル2行分 + タイトル上部の行間スペース確保
			$table_index_separate = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 2) + INTERLINEAR_SPACE;
			//タイトル2行＋データ1行分 + タイトル上部の行間スペース確保
			$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;

			$page_cnt++;
			new_page($last_line_row_index, $emp_id, $emp_name, $arr_col_list, false, $line_offset, $page_cnt, $timecard_bean);
		}

		//枠線
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_top,       $table_index_right,  2, 3);
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_separate,  $table_index_right,  2, 3);
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_bottom,    $table_index_right,  2, 3);
		pxdoc_utils::out_vertical_line(  $table_index_left,     $table_index_top,       $table_index_bottom, 2, 3);
		pxdoc_utils::out_vertical_line(  $table_index_right,    $table_index_top,       $table_index_bottom, 2, 3);
	}

	//テキスト位置（タイトル下段）
	$text_top    = $table_index_top  + ((INTERLINEAR_SPACE + FONT_SIZE) * 2);
	//テキスト位置（タイトル上段）
	$text_top2   = $table_index_top  + INTERLINEAR_SPACE + FONT_SIZE;
	//テキスト位置（タイトル中段）
	$text_top3   = $table_index_top  + (((INTERLINEAR_SPACE + FONT_SIZE) * 3) / 2);
	//テキスト位置（値）
	$text_top4   = $text_top + INTERLINEAR_SPACE + FONT_SIZE;
	//テキスト位置（左）
	$left_index  = $table_index_left;

	$add_pos = ceil($arr_col_list["col_last"]["line_start_pos"] - $table_index_left) / (($hotei_disp_cnt + $disp_cnt) * 2) ;

	//集計行見出しを休暇種別等画面の表示変更にする2009/02/03
	$arr_reason_name = $atdbk_common_class->get_add_disp_reason_name2();

	$arr_text = array();
	$arr_text[] = array("top2" => "法定内", "top" => "勤務", "top3" => "");
	$arr_text[] = array("top2" => "法定内", "top" => "残業", "top3" => "");
	$arr_text[] = array("top2" => "法定外", "top" => "残業", "top3" => "");
	$arr_text[] = array("top2" => "休日", "top" => "勤務", "top3" => "");
	for ($i=0; $i<11; $i++) {
		$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[$i]);
	}
/*
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[0]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[1]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[2]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[3]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[4]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[5]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[6]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[7]);
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[8]);
*/
//	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[9]);
	if ($arr_reason_name[11] == "年末<br>年始") {
		$arr_text[] = array("top2" => "年末", "top" => "年始", "top3" => "");
	} else {
		if (strlen($arr_reason_name[11]) >= 9) {
			$arr_text[] = array("top2" => substr($arr_reason_name[11], 0, 8), "top" => substr($arr_reason_name[11], 8, 8), "top3" => "");
		} else {
			$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[11]);
		}
	}
	$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[12]);
	if ($arr_reason_name[13] == "リフレッ<br>シュ") {
		$arr_text[] = array("top2" => "リフレッ", "top" => "シュ", "top3" => "");
	} else {
		if (strlen($arr_reason_name[13]) >= 9) {
			$arr_text[] = array("top2" => substr($arr_reason_name[13], 0, 8), "top" => substr($arr_reason_name[13], 8, 8), "top3" => "");
		} else {
			$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[13]);
		}
	}
	if ($arr_reason_name[14] == "初盆<br>休暇") {
		$arr_text[] = array("top2" => "初盆", "top" => "休暇", "top3" => "");
	} else {
		if (strlen($arr_reason_name[14]) >= 9) {
			$arr_text[] = array("top2" => substr($arr_reason_name[14], 0, 8), "top" => substr($arr_reason_name[14], 8, 8), "top3" => "");
		} else {
			$arr_text[] = array("top2" => "", "top" => "", "top3" => $arr_reason_name[14]);
		}
	}
/*
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "公休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "特休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "産休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "育休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "介休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "傷休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "学休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "忌引");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "夏休");
	$arr_text[] = array("top2" => "", "top" => "", "top3" => "結休");
	$arr_text[] = array("top2" => "リフレッ", "top" => "シュ", "top3" => "");
	$arr_text[] = array("top2" => "初盆", "top" => "休暇", "top3" => "");
*/
	for ($i=0; $i<count($arr_text); $i++) {
		//法定内勤務、法定内残業、法定外残業、休日勤務
		if ($i < 4 && $arr_total_flg[$i+18]["flg"] != 't') {
			continue;
		}		
		//非表示設定確認
		if ($i >= 4 && ($arr_disp_flg[$i-4] != 't' || in_array("10_0", $arr_total_id)==false)) {
			continue;
		}
		$left_index = $left_index + $add_pos;
		if ($arr_text[$i]["top2"] != "") {
			pxdoc_utils::out_text($left_index, $text_top2, $arr_text[$i]["top2"], true);
		}
		if ($arr_text[$i]["top"] != "") {
			pxdoc_utils::out_text($left_index, $text_top,  $arr_text[$i]["top"],   true);
		}
		if ($arr_text[$i]["top3"] != "") {
			pxdoc_utils::out_text($left_index, $text_top3, $arr_text[$i]["top3"], true);
		}
		// $sumsのindexを調整、4から追加事由分、4まで法定分
		//法定所定追加 20110121
		if ($i == 5) { //法定
			$wk_idx = 36;
		} elseif ($i == 6) { //法定
			$wk_idx = 37;
		} elseif ($i > 6 && $i < 15) { 
			$wk_idx = $i - 4 + 16 - 2;
		} elseif ($i == 15) { //年末年始
			$wk_idx = 33;
		} elseif ($i < 4) {
			$wk_idx = $i + 28;
		} elseif ($i > 15) {
			$wk_idx = $i - 4 + 16 - 3;
		} else {
			$wk_idx = $i - 4 + 16;
		}
		pxdoc_utils::out_text($left_index, $text_top4, $sums[$wk_idx], true);
		$left_index = $left_index + $add_pos;
		pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
	}

	$last_line_row_index = $table_index_bottom;

	shift_summary_out2($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date, $last_line_row_index, $arr_col_list, $page_cnt, $arr_total_id);

	echo("			</g>\n");

}

function line_out($last_line_row_index, $arr_col_list, $line_offset, $title_out_flag, $page_cnt, $timecard_bean){

//	$line_offset = 40;
if ($page_cnt == 1) {
	$wk_line_offset = $line_offset;
} else {
	$wk_line_offset = 50;
}

  	echo("			<!-- タイトル部の横線 --> ");
    pxdoc_utils::out_horizontal_line(60 + COL_OFFSET,   150 + $wk_line_offset, $arr_col_list["col_last"]["line_start_pos"], 2, 3);
    pxdoc_utils::out_horizontal_line(60 + COL_OFFSET,   230 + $wk_line_offset, $arr_col_list["col_last"]["line_start_pos"], 2, 3);
//    if($arr_col_list["col_17"]["disp_flg"] == true)
//    {
//        pxdoc_utils::out_horizontal_line($arr_col_list["col_17"]["line_start_pos"], 190, $arr_col_list["col_17"]["line_start_pos"] + 154, 2, 3);
//    }

	//縦線の長さ(終端位置 - 開始位置)
	echo("			<!-- 縦線 --> ");
    if($arr_col_list["col_1"]["disp_flg"])
    {
        pxdoc_utils::out_vertical_line($arr_col_list["col_1"]["line_start_pos"],   150 + $wk_line_offset, $last_line_row_index, 2, 3); // 日付
    }
    if($arr_col_list["col_2"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_2"]["line_start_pos"],  150 + $wk_line_offset, $last_line_row_index, 2, 3); // 曜日
    }
    if($arr_col_list["col_3"]["disp_flg"])
    {
        pxdoc_utils::out_vertical_line($arr_col_list["col_3"]["line_start_pos"],  150 + $wk_line_offset, $last_line_row_index, 2, 3); // 種別 or 前
    }
    if($arr_col_list["col_4"]["disp_flg"])
    {
        pxdoc_utils::out_vertical_line($arr_col_list["col_4"]["line_start_pos"],  150 + $wk_line_offset, $last_line_row_index, 2, 3); // グループ or 出勤
    }
    if($arr_col_list["col_5"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_5"]["line_start_pos"],  150 + $wk_line_offset, $last_line_row_index, 2, 3); // 出勤予定 or 翌
    }
    if($arr_col_list["col_6"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_6"]["line_start_pos"],  150 + $wk_line_offset, $last_line_row_index, 2, 3); // 事由 or 退勤
    }
    if($arr_col_list["col_7"]["disp_flg"])
    {
   	    pxdoc_utils::out_vertical_line($arr_col_list["col_7"]["line_start_pos"],  150 + $wk_line_offset, $last_line_row_index, 2, 3); // 当直 or 種別
		if($timecard_bean->over_disp_split_flg != "f"){
			$tmp_add_pos = 0;
		} else {
			if ($arr_col_list["time_move_flag"] && !$arr_col_list["col_10"]["disp_flg"]) {
				$tmp_add_pos = 20;
			} else {
				$tmp_add_pos = 40;
			}
		}
		//遅刻早退
		if ($arr_col_list["time_move_flag"] && !$arr_col_list["col_10"]["disp_flg"]) {
			
			pxdoc_utils::out_vertical_line($arr_col_list["col_7"]["line_start_pos"]+80+$tmp_add_pos, 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 
			pxdoc_utils::out_vertical_line($arr_col_list["col_7"]["line_start_pos"]+168+$tmp_add_pos+$tmp_add_pos, 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 
		}
    }
	if($arr_col_list["col_8"]["disp_flg"])
	{
		if(!$arr_col_list["time_move_flag"] || $timecard_bean->over_disp_split_flg != "f"){
			$tmp_add_pos = 0;
		} else {
			if ($arr_col_list["time_move_flag"] && !$arr_col_list["col_10"]["disp_flg"]) {
				$tmp_add_pos = 60;
			} else {
				$tmp_add_pos = 40;
			}
		}
		pxdoc_utils::out_vertical_line($arr_col_list["col_8"]["line_start_pos"]+$tmp_add_pos,  150 + $wk_line_offset, $last_line_row_index, 2, 3); // 手当 or グループ
	}
	if($arr_col_list["col_9"]["disp_flg"])
	{
		if(!$arr_col_list["time_move_flag"] || $timecard_bean->over_disp_split_flg != "f"){
			pxdoc_utils::out_vertical_line($arr_col_list["col_9"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 前 or 出勤予定
		}
	}
    if($arr_col_list["col_10"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_10"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 出勤 or 事由
    }
    if($arr_col_list["col_11"]["disp_flg"])
    {
        pxdoc_utils::out_vertical_line($arr_col_list["col_11"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 翌 or 当直
    }
    if($arr_col_list["col_12"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_12"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 退勤 or 手当
    }
    if($arr_col_list["col_13"]["disp_flg"])
    {
		if($timecard_bean->over_disp_split_flg != "f"){
			$tmp_add_pos = 0;
		} else {
			if (!$arr_col_list["time_move_flag"] && !$arr_col_list["col_3"]["disp_flg"]) {
				$tmp_add_pos = 20;
			} else {
				$tmp_add_pos = 40;
			}
		}
		pxdoc_utils::out_vertical_line($arr_col_list["col_13"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 勤務
		//遅刻早退
		if (!$arr_col_list["time_move_flag"] && !$arr_col_list["col_3"]["disp_flg"]) {
			
			pxdoc_utils::out_vertical_line($arr_col_list["col_13"]["line_start_pos"]+80+$tmp_add_pos, 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 
			pxdoc_utils::out_vertical_line($arr_col_list["col_13"]["line_start_pos"]+168+$tmp_add_pos+$tmp_add_pos, 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 
		}
    }
    if($arr_col_list["col_14"]["disp_flg"])
    {
		if($arr_col_list["time_move_flag"] || $timecard_bean->over_disp_split_flg != "f"){
			$tmp_add_pos = 0;
		} else {
			if (!$arr_col_list["time_move_flag"] && !$arr_col_list["col_3"]["disp_flg"]) {
				$tmp_add_pos = 60;
			} else {
				$tmp_add_pos = 40;
			}
		}
		pxdoc_utils::out_vertical_line($arr_col_list["col_14"]["line_start_pos"]+$tmp_add_pos, 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 法内残
    }
	
	if($arr_col_list["col_15"]["disp_flg"])
	{
		if($arr_col_list["time_move_flag"] || $timecard_bean->over_disp_split_flg != "f"){
			pxdoc_utils::out_vertical_line($arr_col_list["col_15"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 法外残
		}
	}
		
    if($arr_col_list["col_16"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_16"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 深勤
    }
    if($arr_col_list["col_17"]["disp_flg"])
    {
    	pxdoc_utils::out_vertical_line($arr_col_list["col_17"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 外出
    }
    if($arr_col_list["col_18"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_18"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 休憩
    }
    if($arr_col_list["col_19"]["disp_flg"])
    {
	    pxdoc_utils::out_vertical_line($arr_col_list["col_19"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 退復
    }
    if($arr_col_list["col_20"]["disp_flg"])
    {
	   pxdoc_utils::out_vertical_line($arr_col_list["col_20"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 日数
    }
    if($arr_col_list["col_21"]["disp_flg"])
    {
	   pxdoc_utils::out_vertical_line($arr_col_list["col_21"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 会議研修
    }

	pxdoc_utils::out_vertical_line($arr_col_list["col_last"]["line_start_pos"], 150 + $wk_line_offset, $last_line_row_index, 2, 3); // 最終罫線

}

//改ページ
function new_page($last_line_row_index, $emp_id, $emp_name, $arr_col_list, $title_out_flag = true, $line_offset, $page_cnt, $timecard_bean){
	echo("			</g>\n");

	if ($title_out_flag){
		line_out($last_line_row_index, $arr_col_list, $line_offset, $title_out_flag, 1, $timecard_bean);
	}

	echo("		</svg>\n");
	echo("	</page>\n");
	header_out($emp_id, $emp_name, $title_out_flag, false, $arr_col_list, $page_cnt, $timecard_bean);
}

//ヘッダー出力
function header_out2($emp_id, $emp_name, $start_year, $start_month, $start_day, $end_month, $end_day, $title_out_flag = true, $chapter_flag = false, $arr_col_list, $emp_personal_id, $timecard_appr_disp_flg, $line_offset, $st_name_array, $page_cnt, $timecard_title, $year, $month){

	echo("<page type=\"hidden\" id=\"".$emp_id."\">");
	echo("	<svg x=\"0\" y=\"0\" width=\"21cm\" height=\"29.7cm\" viewBox=\"0 0 2100 2970\">\n");
	echo("		<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
	
	pxdoc_utils::out_text(60 + COL_OFFSET,   50 + $line_offset, $timecard_title);
	$kaishibi_shuryobi = "開始日 ".$start_month."月".$start_day."日  〜  締め日 ".$end_month."月".$end_day."日";
	pxdoc_utils::out_text(60 + COL_OFFSET,   90 + $line_offset, "対象期間 ".$year."年".$month."月度");
	pxdoc_utils::out_text($arr_col_list["col_last"]["line_start_pos"], 90 + $line_offset, $kaishibi_shuryobi, false, true);
	pxdoc_utils::out_text(60 + COL_OFFSET,  130 + $line_offset, "職員ID ".$emp_personal_id." 職員氏名 ".$emp_name);
	
	//決裁欄
	if ($timecard_appr_disp_flg == "t") {
		$appr_wd = 50;	//決裁欄の幅
		$wd = 210;		//決裁者欄の幅
		//左側位置
		$x1 = LINE_COL_LAST - $appr_wd - (count($st_name_array) * $wd);
		$abox_top = 70;		//上側位置
		$abox_bottom = 320;	//下側位置
		pxdoc_utils::out_horizontal_line($x1, $abox_top,  LINE_COL_LAST,  2, 3);
		pxdoc_utils::out_horizontal_line($x1+$appr_wd, $abox_top+40,  LINE_COL_LAST,  2, 3);
		pxdoc_utils::out_horizontal_line($x1, $abox_bottom,  LINE_COL_LAST,  2, 3);

		pxdoc_utils::out_vertical_line($x1, $abox_top, $abox_bottom, 2, 3);
		pxdoc_utils::out_text($x1 + 10,  $abox_top + 100, "決");
		pxdoc_utils::out_text($x1 + 10,  $abox_top + 180, "裁");
		pxdoc_utils::out_vertical_line(LINE_COL_LAST, $abox_top, $abox_bottom, 2, 3);
		for ($i=0; $i<count($st_name_array); $i++) {
			$xpos = $x1 + $appr_wd + ($wd * $i);
			pxdoc_utils::out_vertical_line($xpos, $abox_top, $abox_bottom, 2, 3);
			pxdoc_utils::out_text($xpos + ($wd/2),  $abox_top + 30, $st_name_array[$i]["st_name"], true);
		}
	}

	echo("		</g>");
	echo("	</svg>");
	echo("</page>");

	$wk_line_offset = 50;
	echo("<page type=\"hidden\" id=\"".$emp_id."_2\">");
	echo("	<svg x=\"0\" y=\"0\" width=\"21cm\" height=\"29.7cm\" viewBox=\"0 0 2100 2970\">\n");
	echo("		<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
	
	pxdoc_utils::out_text(60 + COL_OFFSET,   50 + $wk_line_offset, $timecard_title);
	$kaishibi_shuryobi = "開始日 ".$start_month."月".$start_day."日  〜  締め日 ".$end_month."月".$end_day."日";
	pxdoc_utils::out_text(60 + COL_OFFSET,   90 + $wk_line_offset, "対象期間 ".$start_year."年".$start_month."月度");
	pxdoc_utils::out_text($arr_col_list["col_last"]["line_start_pos"], 90 + $wk_line_offset, $kaishibi_shuryobi, false, true);
	pxdoc_utils::out_text(60 + COL_OFFSET,  130 + $wk_line_offset, "職員ID ".$emp_personal_id." 職員氏名 ".$emp_name);

	echo("		</g>");
	echo("	</svg>");
	echo("</page>");

}

//ヘッダー出力
function header_out($emp_id, $emp_name, $title_out_flag = true, $chapter_flag = false, $arr_col_list, $line_offset, $page_cnt, $timecard_bean){

	echo("	<page>\n");
	if ($chapter_flag){
		echo("	<chapter name=\"".$emp_name."\" />\n");
	}
	if ($page_cnt == 1){
		echo("	<background id=\"".$emp_id."\" />");
	} else {
		echo("	<background id=\"".$emp_id."_2\" />");
	}
	echo("		<svg x=\"0\" y=\"0\" width=\"21cm\" height=\"29.7cm\" viewBox=\"0 0 2100 2970\">\n");
	echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");

	if ($title_out_flag){

		if ($page_cnt == 1) {
			$head_pos1 = 182 + $line_offset;
			$head_pos2 = 222 + $line_offset;
		} else {
			$head_pos1 = 182 + 50;
			$head_pos2 = 222 + 50;
		}

        if($arr_col_list["col_1"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_1"]["head_start_pos"],  $head_pos2, "日付",       true);
        }
        if($arr_col_list["col_2"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_2"]["head_start_pos"],  $head_pos1, "曜",         true);
		    pxdoc_utils::out_text($arr_col_list["col_2"]["head_start_pos"],  $head_pos2, "日",         true);
        }
        if($arr_col_list["col_3"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_3"]["head_start_pos"],  $head_pos2, "前",       true);
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_3"]["head_start_pos"],  $head_pos2, "種別",       true);
            }
        }
        if($arr_col_list["col_4"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_4"]["head_start_pos"],  $head_pos2, "出勤",   true);
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_4"]["head_start_pos"],  $head_pos2, "グループ",   true);
            }
        }
        if($arr_col_list["col_5"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                pxdoc_utils::out_text($arr_col_list["col_5"]["head_start_pos"],  $head_pos2, "翌",   true);
            }
            else
            {
                pxdoc_utils::out_text($arr_col_list["col_5"]["head_start_pos"],  $head_pos2, "勤務実績",   true);
            }
        }
        if($arr_col_list["col_6"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                pxdoc_utils::out_text($arr_col_list["col_6"]["head_start_pos"],  $head_pos2, "退勤",       true);
            }
            else
            {
                pxdoc_utils::out_text($arr_col_list["col_6"]["head_start_pos"],  $head_pos2, "事由",       true);
            }
        }
        if($arr_col_list["col_7"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
//				pxdoc_utils::out_text($arr_col_list["col_7"]["head_start_pos"]+$tmp_add_pos,  $head_pos2, "勤務",         true);
				$tmp_add_pos = ($timecard_bean->over_disp_split_flg != "f") ? 0 : 10;
				pxdoc_utils::out_text($arr_col_list["col_7"]["line_start_pos"]+40+$tmp_add_pos, $head_pos2, "勤務",       true);
 				//遅刻早退
				if ($arr_col_list["time_move_flag"] && !$arr_col_list["col_10"]["disp_flg"]) {
					$tmp_add_pos = ($timecard_bean->over_disp_split_flg != "f") ? 0 : 30;
					pxdoc_utils::out_text($arr_col_list["col_7"]["line_start_pos"]+124+$tmp_add_pos, $head_pos2, "遅刻",       true);
					$tmp_add_pos = ($timecard_bean->over_disp_split_flg != "f") ? 0 : 50;
					pxdoc_utils::out_text($arr_col_list["col_7"]["line_start_pos"]+212+$tmp_add_pos, $head_pos2, "早退",       true);
				}
            }
            else
            {
                pxdoc_utils::out_text($arr_col_list["col_7"]["head_start_pos"],  $head_pos1, "当",         true);
                pxdoc_utils::out_text($arr_col_list["col_7"]["head_start_pos"],  $head_pos2, "直",         true);
            }
        }
        if($arr_col_list["col_8"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
				if($timecard_bean->over_disp_split_flg != "f"){
					pxdoc_utils::out_text($arr_col_list["col_8"]["head_start_pos"], $head_pos2, "法内残",       true);
				} else {
					pxdoc_utils::out_text($arr_col_list["col_8"]["head_start_pos"]+70, $head_pos2, "残業",       true);
				}
            }
            else
            {
                //pxdoc_utils::out_text($arr_col_list["col_8"]["head_start_pos"], $head_pos2, "手当/残業理由",       true);
                pxdoc_utils::out_text($arr_col_list["col_8"]["head_start_pos"], $head_pos1, "手当",       true);
                pxdoc_utils::out_text($arr_col_list["col_8"]["head_start_pos"], $head_pos2, "残業理由",       true);
            }
        }
        if($arr_col_list["col_9"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
				if($timecard_bean->over_disp_split_flg != "f"){
					pxdoc_utils::out_text($arr_col_list["col_9"]["head_start_pos"], $head_pos2, "法外残",         true);
				}
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_9"]["head_start_pos"], $head_pos2, "前",         true);
            }
        }
        if($arr_col_list["col_10"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_10"]["head_start_pos"], $head_pos2, "種別",       true);
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_10"]["head_start_pos"], $head_pos2, "出勤",       true);
            }
        }
        if($arr_col_list["col_11"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_11"]["head_start_pos"], $head_pos2, "グループ",       true);
            }
            else
            {
                pxdoc_utils::out_text($arr_col_list["col_11"]["head_start_pos"], $head_pos2, "翌",         true);
            }
        }
        if($arr_col_list["col_12"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
		        pxdoc_utils::out_text($arr_col_list["col_12"]["head_start_pos"], $head_pos2, "勤務実績",       true);
            }
            else
            {
		        pxdoc_utils::out_text($arr_col_list["col_12"]["head_start_pos"], $head_pos2, "退勤",       true);
            }
        }
        if($arr_col_list["col_13"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
			    pxdoc_utils::out_text($arr_col_list["col_13"]["head_start_pos"], $head_pos2, "事由",       true);
            }
            else
            {
//				pxdoc_utils::out_text($arr_col_list["col_13"]["head_start_pos"]+$tmp_add_pos, $head_pos2, "勤務",       true);
				$tmp_add_pos = ($timecard_bean->over_disp_split_flg != "f") ? 0 : 10;
				pxdoc_utils::out_text($arr_col_list["col_13"]["line_start_pos"]+40+$tmp_add_pos, $head_pos2, "勤務",       true);
 				//遅刻早退
				if (!$arr_col_list["time_move_flag"] && !$arr_col_list["col_3"]["disp_flg"]) {
					$tmp_add_pos = ($timecard_bean->over_disp_split_flg != "f") ? 0 : 30;
					pxdoc_utils::out_text($arr_col_list["col_13"]["line_start_pos"]+124+$tmp_add_pos, $head_pos2, "遅刻",       true, false, 0, "red");
					$tmp_add_pos = ($timecard_bean->over_disp_split_flg != "f") ? 0 : 50;
					pxdoc_utils::out_text($arr_col_list["col_13"]["line_start_pos"]+212+$tmp_add_pos, $head_pos2, "早退",       true, false, 0, "red");
				}
            }
        }
        if($arr_col_list["col_14"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
                pxdoc_utils::out_text($arr_col_list["col_14"]["head_start_pos"],  $head_pos1, "当",         true);
                pxdoc_utils::out_text($arr_col_list["col_14"]["head_start_pos"],  $head_pos2, "直",         true);
			} else {
				if($timecard_bean->over_disp_split_flg != "f"){
				    pxdoc_utils::out_text($arr_col_list["col_14"]["head_start_pos"], $head_pos2, "法内残",       true);
				} else {
					pxdoc_utils::out_text($arr_col_list["col_14"]["head_start_pos"]+60, $head_pos2, "残業",       true);
				}
			}
        }
        if($arr_col_list["col_15"]["disp_flg"])
        {
            if($arr_col_list["time_move_flag"])
            {
				//pxdoc_utils::out_text($arr_col_list["col_15"]["head_start_pos"], $head_pos2, "手当/残業理由",       true);
				pxdoc_utils::out_text($arr_col_list["col_15"]["head_start_pos"], $head_pos1, "手当",       true);
				pxdoc_utils::out_text($arr_col_list["col_15"]["head_start_pos"], $head_pos2, "残業理由",       true);
			} else {
				if($timecard_bean->over_disp_split_flg != "f"){
					pxdoc_utils::out_text($arr_col_list["col_15"]["head_start_pos"], $head_pos2, "法外残",       true);
				}
			}
        }
        if($arr_col_list["col_16"]["disp_flg"])
        {
			pxdoc_utils::out_text($arr_col_list["col_16"]["head_start_pos"], $head_pos2, "深残",       true);
        }
        if($arr_col_list["col_17"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_17"]["head_start_pos"], $head_pos2, "外出",       true);
        }
        if($arr_col_list["col_18"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_18"]["head_start_pos"], $head_pos2, "休憩",       true);
        }
        if($arr_col_list["col_19"]["disp_flg"])
        {
			$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退復" : "呼出";
			pxdoc_utils::out_text($arr_col_list["col_19"]["head_start_pos"], $head_pos2, $ret_str,       true);
        }

        if($arr_col_list["col_20"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_20"]["head_start_pos"], $head_pos1, "日",         true);
		    pxdoc_utils::out_text($arr_col_list["col_20"]["head_start_pos"], $head_pos2, "数",         true);
        }

        if($arr_col_list["col_21"]["disp_flg"])
        {
		    pxdoc_utils::out_text($arr_col_list["col_21"]["head_start_pos"], $head_pos1, "会議",       true);
		    pxdoc_utils::out_text($arr_col_list["col_21"]["head_start_pos"], $head_pos2, "研修",       true);
        }

	}
}

/**
 * ここから下show_timecard_common.iniのほぼコピー
 *
 */
// 職員の勤務条件表示
function employee_empcond_out($con, $fname, $emp_id, $table_index_bottom, $arr_col_list){

	//勤務条件表示
	$sql = "select * from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$duty_form = pg_fetch_result($sel, 0, "duty_form");
	$wage      = pg_fetch_result($sel, 0, "wage");
    $irrg_type = pg_fetch_result($sel, 0, "irrg_type");

    if($duty_form == "1")
    {
        $duty_form = "常勤";
    }
    else if($duty_form == "2")
    {
        $duty_form = "非常勤";
    }

    if($wage == "1")
    {
        $wage_name = "月給制";
    }
    else if($wage == "3")
    {
        $wage_name = "日給制";
    }
    else if($wage == "4")
    {
        $wage_name = "時間給制";
    }
	else if($wage == "5")
	{
		$wage_name = "年俸制";
	}
	
    if($irrg_type == "")
    {
        $irrg_name = "変則労働適用無し";
    }
    else if($irrg_type == "1")
    {
        $irrg_name = "変則労働適用有り(週)";
    }
    else if($irrg_type == "2")
    {
        $irrg_name = "変則労働適用有り(月)";
    }

	//集計表から4つ分行間空ける
	$text_top = $table_index_bottom + (INTERLINEAR_SPACE * 4);
	echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");

	//A4のサイズを超えた場合（フルで印刷出来ないため-100しとく
	if ($text_top > SIZE_A4_HEIGHT - 100){
		//座標リセット
		$text_top      = PRINT_LIST_LINE_TOP + (INTERLINEAR_SPACE * 4);
		$text_left     = 60 + COL_OFFSET;

		$page_cnt++;
		new_page($last_line_row_index, $emp_id,  $emp_name, $arr_col_list, $false, $page_cnt);

		//ヘッダーとデータ区切りのための横線
		pxdoc_utils::out_horizontal_line(60 + COL_OFFSET,   150, $arr_col_list["col_last"]["line_start_pos"], 2, 3);
	}

	pxdoc_utils::out_text(60 + COL_OFFSET,  $text_top, $duty_form, false, false, true);
	pxdoc_utils::out_text(280, $text_top, $wage_name, false, false, true);
	pxdoc_utils::out_text(500, $text_top, $irrg_name, false, false, true);
	echo("			</g>\n");

	return $text_top;
}


// シフト集計表示
function shift_summary_out($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date, $last_line_row_index, $arr_col_list)
{
    require_once("holiday.php");

    $arr_pattern    = array();
    $arr_night_duty = array();
    $arr_allow      = array();
	$arr_group_pattern = array();
	$work_arr_pattern = array();

    // 締め済みではない場合
	$table_name = "atdbkrslt";
	if($atdbk_closed){
		// 締め済みの場合
		$table_name = "wktotald";
	}

    //////////////////////////////////////////
    // 勤務シフト集計
    //////////////////////////////////////////
	$sql   ="SELECT ".
				"c.group_id, ".
				"c.group_name, ".
				"b.atdptn_id, ".
				"b.atdptn_nm, ".
				"count(a.pattern) as pattern_cnt ".
			"FROM ".
				"$table_name a, ".
				"atdptn b, ".
				"wktmgrp c ".
			"WHERE ".
				"a.tmcd_group_id = b.group_id AND ".
				"a.pattern = cast(b.atdptn_id as varchar) AND ".
				"a.emp_id = '$emp_id' AND ".
				"a.date >= '$start_date' AND ".
				"a.date <= '$end_date' AND ".
				"b.atdptn_nm <> '' AND ".
				"a.tmcd_group_id = c.group_id ".
			"GROUP BY ".
				"c.group_id, ".
				"c.group_name, ".
				"b.atdptn_id, ".
				"b.atdptn_nm ".
			"ORDER BY ".
				"c.group_id, ".
				"b.atdptn_id ";
	$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	$work_group_id   = "";
	$work_group_name = "";
	while ($row = pg_fetch_array($sel))
	{
		if ($work_group_id != $row["group_id"] && $work_group_id != ""){
			$arr_group_pattern[] = array("group_id" => $work_group_id, "group_name" => $work_group_name, "arr_pattern" => $work_arr_pattern);
			$work_arr_pattern = array();
		}
		$work_group_id = $row["group_id"];
		$work_group_name = $row["group_name"];
	    $work_arr_pattern[] = array("atdptn_nm" => $row["atdptn_nm"], "pattern_cnt" => $row["pattern_cnt"]);
	}
	if (count($work_arr_pattern) > 0){
		$arr_group_pattern[] = array("group_id" => $work_group_id, "group_name" => $work_group_name, "arr_pattern" => $work_arr_pattern);
	}

    //////////////////////////////////////////
    // 当直集計
    //////////////////////////////////////////
    $arr_night_duty_date = get_night_duty_date($con, $fname, $emp_id, $start_date, $end_date, $table_name);
    $sql   = "select a.date from $table_name a ";
    $cond  = "where a.emp_id = '$emp_id' and a.date >= '$start_date' and a.date <= '$end_date' ";
    $cond .= "and a.night_duty = '1'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $arr_night_duty_date = array();
    while ($row = pg_fetch_array($sel))
    {
        $arr[] = array("date" => $row["date"]);
    }
	$arr_night_duty_date = $arr;

    $holiday_cnt = 0;
    $satday_cnt = 0;
    $weekday_cnt = 0;

    foreach($arr_night_duty_date as $night_duty_date)
    {
        $date = $night_duty_date["date"];
        $timestamp = to_timestamp($date);

        // 日付から休日かを求める
        $holiday = ktHolidayName($timestamp);
        // 日付から曜日を求める
        $wd = get_weekday($timestamp);

        if($holiday != "" || $wd == "日")
        {
            $holiday_cnt++;
        }
        else if($wd == "土")
        {
            $satday_cnt++;
        }
        else
        {
            $weekday_cnt++;
        }
    }
    $arr_night_duty["holiday_cnt"] = $holiday_cnt;
    $arr_night_duty["satday_cnt"]  = $satday_cnt;
    $arr_night_duty["weekday_cnt"] = $weekday_cnt;

    //////////////////////////////////////////
    // 手当集計
    //////////////////////////////////////////
    $arr_allow = get_allow_summary($con, $fname, $emp_id, $start_date, $end_date, $table_name);

	$text_top  = $last_line_row_index + (INTERLINEAR_SPACE * 4);
	$text_left = 60 + COL_OFFSET;

//	echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
	//表示する集計情報がA4に収まるかチェック
	$group_pattern_count = 0;
	foreach($arr_group_pattern as $group){
		$group_pattern_count++;
		$group_pattern_count += count($group["arr_pattern"]);
	}

	//勤務シフト最終行
	$max_bottom_index = $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * $group_pattern_count);
	//当直最終行
	if ($max_bottom_index < $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3)){
		$max_bottom_index = $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3);
	}
	//手当最終行
	if ($max_bottom_index < $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * count($arr_allow))){
		$max_bottom_index = $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * count($arr_allow));
	}

	//A4のサイズを超えた場合（フルで印刷出来ないため-100しとく
	if ($max_bottom_index > SIZE_A4_HEIGHT - 100){
		//座標リセット
		$text_top      = PRINT_LIST_LINE_TOP + (INTERLINEAR_SPACE * 4);
		$last_line_row_index = $text_top;
		$page_cnt++;
		new_page($last_line_row_index, $emp_id, $emp_name, $arr_col_list, false, $page_cnt);

		//ヘッダーとデータ区切りのための横線
		pxdoc_utils::out_horizontal_line(60 + COL_OFFSET,   150+40, $arr_col_list["col_last"]["line_start_pos"], 2, 3);
		$new_page_flg = true;
	} else {
	//改ページなし
		echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
		$new_page_flg = false;
	}

    // 勤務シフト
	pxdoc_utils::out_text($text_left,  $text_top, "勤務シフト", false, false, true);
	$max_length = strlen("勤務シフト");
	$work_replace = "";
	$replace_key = "%replace_key".$emp_id;
	foreach($arr_group_pattern as $group){
		$group_id    = $group["group_id"];
		$group_name  = $group["group_name"];
		$arr_pattern = $group["arr_pattern"];

		if ($max_length < strlen($group_name)){
			$max_length = strlen($group_name);
		}

		//文字縦位置の計算(1行目の開始位置 + (文字サイズ x 行数) + (行間 x 行数))
		$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;

		pxdoc_utils::out_text($text_left, $text_top, $group_name, false, false, true);

		foreach($arr_pattern as $pattern)
		{
			$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
			$atdptn_nm   = $pattern["atdptn_nm"];
			$pattern_cnt = $pattern["pattern_cnt"];
			pxdoc_utils::out_text($text_left + INTERLINEAR_SPACE, $text_top, $atdptn_nm);
			$work_replace = $work_replace.pxdoc_utils::get_text($replace_key, $text_top, $pattern_cnt."回");
			if ($max_length < strlen($atdptn_nm)){
				$max_length = strlen($atdptn_nm);
			}
		}
	}

	//回数書き込み位置をグループ内の最大文字数分右にずらす
	$count_text_left = $text_left + (($max_length / 2) * FONT_SIZE) + INTERLINEAR_SPACE + INTERLINEAR_SPACE;

	//位置置換
	echo(str_replace($replace_key, $count_text_left, $work_replace));

	//上位置初期化
	$text_top  = $last_line_row_index + (INTERLINEAR_SPACE * 4);

	//当直書き込み位置をパターン回数の最大文字数(２文字)分右にずらす
	$text_left = $count_text_left + (2 * FONT_SIZE) + INTERLINEAR_SPACE;
	//回数書き込み位置を曜日種別内の最大文字数(５文字)分右にずらす
	$count_text_left = $text_left + (5 * FONT_SIZE) + INTERLINEAR_SPACE;

	pxdoc_utils::out_text($text_left, $text_top, "当直", false, false, true);

	$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
	pxdoc_utils::out_text($text_left, $text_top, "平日");
	pxdoc_utils::out_text($count_text_left, $text_top, $weekday_cnt."回");

	$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
	pxdoc_utils::out_text($text_left, $text_top, "土曜日");
	pxdoc_utils::out_text($count_text_left, $text_top, $satday_cnt."回");

	$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
	pxdoc_utils::out_text($text_left, $text_top, "日曜・休日");
	pxdoc_utils::out_text($count_text_left, $text_top, $holiday_cnt."回");

	//上位置初期化
	$text_top  = $last_line_row_index + (INTERLINEAR_SPACE * 4);
	//当直書き込み位置をパターン回数の最大文字数(２文字)分右にずらす
	$text_left = $count_text_left + (2 * FONT_SIZE) + INTERLINEAR_SPACE;

	pxdoc_utils::out_text($text_left, $text_top, "手当", false, false, true);

    // 手当
	$max_length = 0;
	$work_replace = "";
    foreach($arr_allow as $allow){
        $allow_contents = $allow["allow_contents"];
        $allow_cnt      = $allow["allow_cnt"];

		$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
		pxdoc_utils::out_text($text_left + INTERLINEAR_SPACE, $text_top, $allow_contents);
		$work_replace = $work_replace.pxdoc_utils::get_text($replace_key, $text_top, $allow_cnt."回");
		if ($max_length < strlen($allow_contents)){
			$max_length = strlen($allow_contents);
		}
    }

	//回数書き込み位置をグループ内の最大文字数分右にずらす
	$count_text_left = $text_left + (($max_length / 2) * FONT_SIZE) + INTERLINEAR_SPACE + INTERLINEAR_SPACE;

	//位置置換
	echo(str_replace($replace_key, $count_text_left, $work_replace));

	//改ページなしの場合
	if ($new_page_flg == false) {
		echo("</g>\n");
	}

}

// シフト集計表示（表形式）
function shift_summary_out2($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date, $last_line_row_index, $arr_col_list, $page_cnt, $arr_total_id)
{
    require_once("holiday.php");

    $arr_pattern    = array();
    $arr_night_duty = array();
    $arr_allow      = array();
	$arr_group_pattern = array();
	$work_arr_pattern = array();

    // 締め済みではない場合
	$table_name = "atdbkrslt";
	if($atdbk_closed){
		// 締め済みの場合
		$table_name = "wktotald";
	}

	$today = date("Ymd");
    //////////////////////////////////////////
    // 勤務シフト集計
    //////////////////////////////////////////
	$sql   ="SELECT ".
				"c.group_id, ".
				"c.group_name, ".
				"b.atdptn_id, ".
				"b.atdptn_nm, ".
				"count(a.pattern) as pattern_cnt ".
			"FROM ".
				"$table_name a, ".
				"atdptn b, ".
				"wktmgrp c ".
			"WHERE ".
				"a.tmcd_group_id = b.group_id AND ".
				"a.pattern = cast(b.atdptn_id as varchar) AND ".
				"a.emp_id = '$emp_id' AND ".
				"a.date >= '$start_date' AND ".
				"a.date <= '$end_date' AND ".
				"b.atdptn_nm <> '' AND ".
				"a.tmcd_group_id = c.group_id ".
				/* パターンがあればカウントへ変更 20121018
				"AND ( ". //打刻済みをカウントする 20090729
				"((b.atdptn_id = 10 OR ".
				"b.workday_count = 0) AND ".
				"a.date <= '$today') OR ".
				"(b.atdptn_id != 10 AND ".
				"a.start_time is not null ".
				"and a.end_time is not null) ) ".
        */
        "GROUP BY ".
				"c.group_id, ".
				"c.group_name, ".
				"b.atdptn_id, ".
				"b.atdptn_nm ".
			"ORDER BY ".
				"c.group_id, ".
				"b.atdptn_id ";
	$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	$work_group_id   = "";
	$work_group_name = "";
	while ($row = pg_fetch_array($sel))
	{
		if ($work_group_id != $row["group_id"] && $work_group_id != ""){
			$arr_group_pattern[] = array("group_id" => $work_group_id, "group_name" => $work_group_name, "arr_pattern" => $work_arr_pattern);
			$work_arr_pattern = array();
		}
		$work_group_id = $row["group_id"];
		$work_group_name = $row["group_name"];
	    $work_arr_pattern[] = array("atdptn_nm" => $row["atdptn_nm"], "pattern_cnt" => $row["pattern_cnt"]);
	}
	if (count($work_arr_pattern) > 0){
		$arr_group_pattern[] = array("group_id" => $work_group_id, "group_name" => $work_group_name, "arr_pattern" => $work_arr_pattern);
	}

    //////////////////////////////////////////
    // 当直集計
    //////////////////////////////////////////
    $arr_night_duty_date = get_night_duty_date($con, $fname, $emp_id, $start_date, $end_date, $table_name);
    $sql   = "select a.date from $table_name a ";
    $cond  = "where a.emp_id = '$emp_id' and a.date >= '$start_date' and a.date <= '$end_date' ";
    $cond .= "and a.night_duty = '1'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $arr_night_duty_date = array();
    while ($row = pg_fetch_array($sel))
    {
        $arr[] = array("date" => $row["date"]);
    }
	$arr_night_duty_date = $arr;

    $holiday_cnt = 0;
    $satday_cnt = 0;
    $weekday_cnt = 0;

    foreach($arr_night_duty_date as $night_duty_date)
    {
        $date = $night_duty_date["date"];
        $timestamp = to_timestamp($date);

        // 日付から休日かを求める
        $holiday = ktHolidayName($timestamp);
        // 日付から曜日を求める
        $wd = get_weekday($timestamp);

        if($holiday != "" || $wd == "日")
        {
            $holiday_cnt++;
        }
        else if($wd == "土")
        {
            $satday_cnt++;
        }
        else
        {
            $weekday_cnt++;
        }
    }
    $arr_night_duty["holiday_cnt"] = $holiday_cnt;
    $arr_night_duty["satday_cnt"]  = $satday_cnt;
    $arr_night_duty["weekday_cnt"] = $weekday_cnt;

    //////////////////////////////////////////
    // 手当集計
    //////////////////////////////////////////
    $arr_allow = get_allow_summary($con, $fname, $emp_id, $start_date, $end_date, $table_name);

	//所属グループ
	$sql = "select a.group_id,b.pattern_id from duty_shift_staff a inner join duty_shift_group b on b.group_id = a.group_id ";
	$cond = "where a.emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	if ($num > 0) {
		$belong_group_id = pg_result($sel,0,"pattern_id");
	}
// 名称と回数のテーブル
// $arr_summary name total
	$arr_summary = array();
// 勤務シフト
	$i = 0;
	if (in_array("10_3", $arr_total_id)) {
		foreach($arr_group_pattern as $group){
			
			//所属グループ
			$group_id    = $group["group_id"]; 
			if ($group_id == $belong_group_id) {
				$group_name  = $group["group_name"];
				$arr_pattern = $group["arr_pattern"];
				foreach($arr_pattern as $pattern)
				{
					$arr_summary[$i]["top3"] = $pattern["atdptn_nm"]; 
					$arr_summary[$i]["cnt"] = $pattern["pattern_cnt"]; 
					$i++;
				}
			}
		}
	}
// 当直
	if (in_array("10_1", $arr_total_id)) {
		$arr_summary[$i]["top2"] = "当直";
		$arr_summary[$i]["top"] = "平日";
		$arr_summary[$i]["cnt"] = $weekday_cnt;
		$i++;
		$arr_summary[$i]["top2"] = "当直";
		$arr_summary[$i]["top"] = "土曜日";
		$arr_summary[$i]["cnt"] = $satday_cnt;
		$i++;
		$arr_summary[$i]["top2"] = "当直";
		$arr_summary[$i]["top"] = "日曜・休日";
		$arr_summary[$i]["cnt"] = $holiday_cnt;
		$i++;
	}
// 手当て
	if (in_array("10_2", $arr_total_id)) {
		foreach($arr_allow as $allow)
		{
			$arr_summary[$i]["top3"] = $allow["allow_contents"]; 
			$arr_summary[$i]["cnt"] = $allow["allow_cnt"]; 
			$i++;
		}
	}

//	$text_top  = $last_line_row_index + (INTERLINEAR_SPACE * 4);
//	$text_left = 60 + COL_OFFSET;

//	echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
	//表示する集計情報がA4に収まるかチェック
	$group_pattern_count = 0;
	foreach($arr_group_pattern as $group){
		$group_pattern_count++;
//		$group_pattern_count += count($group["arr_pattern"]);
//表形式になったためカウント方法を変更
		$group_pattern_count += 4;
	}

	//勤務シフト最終行
	$max_bottom_index = $last_line_row_index + ((FONT_SIZE + INTERLINEAR_SPACE) * $group_pattern_count);
	//当直最終行
//	if ($max_bottom_index < $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3)){
//		$max_bottom_index = $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3);
//	}
	//手当最終行
//	if ($max_bottom_index < $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * count($arr_allow))){
//		$max_bottom_index = $text_top + ((FONT_SIZE + INTERLINEAR_SPACE) * count($arr_allow));
//	}

	$table_index_top      = $last_line_row_index + (INTERLINEAR_SPACE * 2);
	$table_index_left     = 60 + COL_OFFSET;
	$table_index_right    = $arr_col_list["col_last"]["line_start_pos"];
	//タイトル2行分 + タイトル上部の行間スペース確保
	$table_index_separate = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 2) + INTERLINEAR_SPACE;
	//タイトル2行＋データ1行分 + タイトル上部の行間スペース確保
	$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;
	//A4のサイズを超えた場合（フルで印刷出来ないため-100しとく
	if ($max_bottom_index > SIZE_A4_HEIGHT - 100){
		//座標リセット
		$text_top      = PRINT_LIST_LINE_TOP + (INTERLINEAR_SPACE * 4);
		$last_line_row_index = $text_top;
		$page_cnt++;
		new_page($last_line_row_index, $emp_id, $emp_name, $arr_col_list, false, $page_cnt);

		//ヘッダーとデータ区切りのための横線
		pxdoc_utils::out_horizontal_line(60 + COL_OFFSET,   150+40, $arr_col_list["col_last"]["line_start_pos"], 2, 3);
		$new_page_flg = true;
		$table_index_top      = PRINT_LIST_LINE_TOP + 40 + (INTERLINEAR_SPACE * 2);
		//タイトル2行分 + タイトル上部の行間スペース確保
		$table_index_separate = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 2) + INTERLINEAR_SPACE;
		//タイトル2行＋データ1行分 + タイトル上部の行間スペース確保
		$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;
	} else {
	//改ページなし
		echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");
		$new_page_flg = false;
	} 

	// 勤務シフト
//	$table_index_top =  $text_top;
	//	件数 シフト＋３（当直）＋手当て
	$wk_disp_cnt = count($arr_summary);
	if ($wk_disp_cnt > 0) {
		//枠線
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_top,       $table_index_right,  2, 3);
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_separate,  $table_index_right,  2, 3);
		pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_bottom,    $table_index_right,  2, 3);
		pxdoc_utils::out_vertical_line(  $table_index_left,     $table_index_top,       $table_index_bottom, 2, 3);
		pxdoc_utils::out_vertical_line(  $table_index_right,    $table_index_top,       $table_index_bottom, 2, 3);
		
		//テキスト位置（タイトル下段）
		$text_top    = $table_index_top  + ((INTERLINEAR_SPACE + FONT_SIZE) * 2);
		//テキスト位置（タイトル上段）
		$text_top2   = $table_index_top  + INTERLINEAR_SPACE + FONT_SIZE;
		//テキスト位置（タイトル中段）
		$text_top3   = $table_index_top  + (((INTERLINEAR_SPACE + FONT_SIZE) * 3) / 2);
		//テキスト位置（値）
		$text_top4   = $text_top + INTERLINEAR_SPACE + FONT_SIZE;
		//テキスト位置（左）
		$left_index  = $table_index_left;
		
		$add_pos = ceil($arr_col_list["col_last"]["line_start_pos"] - $table_index_left) / ($wk_disp_cnt * 2) ;
		
		for ($i=0; $i<$wk_disp_cnt; $i++) {
			$left_index = $left_index + $add_pos;
			if ($arr_summary[$i]["top2"] != "") {
				pxdoc_utils::out_text($left_index, $text_top2, $arr_summary[$i]["top2"], true);
			}
			if ($arr_summary[$i]["top"] != "") {
				pxdoc_utils::out_text($left_index, $text_top,  $arr_summary[$i]["top"],   true);
			}
			if ($arr_summary[$i]["top3"] != "") {
				pxdoc_utils::out_text($left_index, $text_top3, $arr_summary[$i]["top3"], true);
			}
			
			pxdoc_utils::out_text($left_index, $text_top4, $arr_summary[$i]["cnt"]."回", true);
			$left_index = $left_index + $add_pos;
			pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
		}
	}

	//応援追加分
	if (in_array("10_3", $arr_total_id)) {
		foreach($arr_group_pattern as $group){
			//所属グループ以外
			$group_id    = $group["group_id"]; 
			//echo("g $group_id:".$belong_group_id);
			if ($group_id == $belong_group_id) {
				continue;
			}
			
			$group_name  = $group["group_name"];
			$arr_pattern = $group["arr_pattern"];
			$arr_summary = array();
			$i = 0;
			foreach($arr_pattern as $pattern)
			{
				$arr_summary[$i]["top3"] = $pattern["atdptn_nm"]; 
				$arr_summary[$i]["cnt"] = $pattern["pattern_cnt"]; 
				$i++;
			}
			
			// グループ名
			$table_index_left     = 60 + COL_OFFSET;
			$group_text_top = $table_index_bottom + (INTERLINEAR_SPACE * 4);
			$group_text_bottom = $table_index_bottom +(FONT_SIZE + INTERLINEAR_SPACE);
			pxdoc_utils::out_text($table_index_left+(3 * FONT_SIZE), $group_text_top, $group_name, true);
			// 枠線
			$table_index_top      = $group_text_bottom + (INTERLINEAR_SPACE * 2);
			//keisan		$table_index_right    = $arr_col_list["col_last"]["line_start_pos"];
			$wk_width = $arr_col_list["col_last"]["line_start_pos"] - $table_index_left;
			$wk_right = $table_index_left + ($wk_width * (10 + 10 * $i) / 100);
			//		$table_index_right    = $arr_col_list["col_last"]["line_start_pos"];
			$table_index_right    = $wk_right;
			$add_pos = ceil($wk_right - $table_index_left) / ($i * 2) ;
			//タイトル2行分 + タイトル上部の行間スペース確保
			$table_index_separate = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 2) + INTERLINEAR_SPACE;
			//タイトル2行＋データ1行分 + タイトル上部の行間スペース確保
			$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;
			//枠線
			pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_top,       $table_index_right,  2, 3);
			pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_separate,  $table_index_right,  2, 3);
			pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_bottom,    $table_index_right,  2, 3);
			pxdoc_utils::out_vertical_line(  $table_index_left,     $table_index_top,       $table_index_bottom, 2, 3);
			pxdoc_utils::out_vertical_line(  $table_index_right,    $table_index_top,       $table_index_bottom, 2, 3);
			
			//テキスト位置（タイトル下段）
			$text_top    = $table_index_top  + ((INTERLINEAR_SPACE + FONT_SIZE) * 2);
			//テキスト位置（タイトル上段）
			$text_top2   = $table_index_top  + INTERLINEAR_SPACE + FONT_SIZE;
			//テキスト位置（タイトル中段）
			$text_top3   = $table_index_top  + (((INTERLINEAR_SPACE + FONT_SIZE) * 3) / 2);
			//テキスト位置（値）
			$text_top4   = $text_top + INTERLINEAR_SPACE + FONT_SIZE;
			//テキスト位置（左）
			$left_index  = $table_index_left;
			//
			for ($i=0; $i<count($arr_summary); $i++) {
				$left_index = $left_index + $add_pos;
				if ($arr_summary[$i]["top3"] != "") {
					pxdoc_utils::out_text($left_index, $text_top3, $arr_summary[$i]["top3"], true);
				}
				
				pxdoc_utils::out_text($left_index, $text_top4, $arr_summary[$i]["cnt"]."回", true);
				$left_index = $left_index + $add_pos;
				pxdoc_utils::out_vertical_line($left_index, $table_index_top, $table_index_bottom, 2, 3);
			}
		}
	}


	// 勤務シフト
/*
	pxdoc_utils::out_text($text_left,  $text_top, "勤務シフト", false, false, true);
	$max_length = strlen("勤務シフト");
	$work_replace = "";
	$replace_key = "%replace_key".$emp_id;
	foreach($arr_group_pattern as $group){
		$group_id    = $group["group_id"];
		$group_name  = $group["group_name"];
		$arr_pattern = $group["arr_pattern"];

		if ($max_length < strlen($group_name)){
			$max_length = strlen($group_name);
		}

		//文字縦位置の計算(1行目の開始位置 + (文字サイズ x 行数) + (行間 x 行数))
		$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;

//		pxdoc_utils::out_text($text_left, $text_top, $group_name, false, false, true);

		foreach($arr_pattern as $pattern)
		{
			$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
			$atdptn_nm   = $pattern["atdptn_nm"];
			$pattern_cnt = $pattern["pattern_cnt"];
??			pxdoc_utils::out_text($text_left + INTERLINEAR_SPACE, $text_top, $atdptn_nm);
			$work_replace = $work_replace.pxdoc_utils::get_text($replace_key, $text_top, $pattern_cnt."回");
			if ($max_length < strlen($atdptn_nm)){
				$max_length = strlen($atdptn_nm);
			}
		}
	}

	//回数書き込み位置をグループ内の最大文字数分右にずらす
	$count_text_left = $text_left + (($max_length / 2) * FONT_SIZE) + INTERLINEAR_SPACE + INTERLINEAR_SPACE;

	//位置置換
	echo(str_replace($replace_key, $count_text_left, $work_replace));

	//上位置初期化
	$text_top  = $last_line_row_index + (INTERLINEAR_SPACE * 4);

	//当直書き込み位置をパターン回数の最大文字数(２文字)分右にずらす
	$text_left = $count_text_left + (2 * FONT_SIZE) + INTERLINEAR_SPACE;
	//回数書き込み位置を曜日種別内の最大文字数(５文字)分右にずらす
	$count_text_left = $text_left + (5 * FONT_SIZE) + INTERLINEAR_SPACE;

	pxdoc_utils::out_text($text_left, $text_top, "当直", false, false, true);

	$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
	pxdoc_utils::out_text($text_left, $text_top, "平日");
	pxdoc_utils::out_text($count_text_left, $text_top, $weekday_cnt."回");

	$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
	pxdoc_utils::out_text($text_left, $text_top, "土曜日");
	pxdoc_utils::out_text($count_text_left, $text_top, $satday_cnt."回");

	$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
	pxdoc_utils::out_text($text_left, $text_top, "日曜・休日");
	pxdoc_utils::out_text($count_text_left, $text_top, $holiday_cnt."回");

	//上位置初期化
	$text_top  = $last_line_row_index + (INTERLINEAR_SPACE * 4);
	//当直書き込み位置をパターン回数の最大文字数(２文字)分右にずらす
	$text_left = $count_text_left + (2 * FONT_SIZE) + INTERLINEAR_SPACE;

	pxdoc_utils::out_text($text_left, $text_top, "手当", false, false, true);

    // 手当
	$max_length = 0;
	$work_replace = "";
    foreach($arr_allow as $allow){
        $allow_contents = $allow["allow_contents"];
        $allow_cnt      = $allow["allow_cnt"];

		$text_top = $text_top + FONT_SIZE + INTERLINEAR_SPACE;
		pxdoc_utils::out_text($text_left + INTERLINEAR_SPACE, $text_top, $allow_contents);
		$work_replace = $work_replace.pxdoc_utils::get_text($replace_key, $text_top, $allow_cnt."回");
		if ($max_length < strlen($allow_contents)){
			$max_length = strlen($allow_contents);
		}
    }

	//回数書き込み位置をグループ内の最大文字数分右にずらす
	$count_text_left = $text_left + (($max_length / 2) * FONT_SIZE) + INTERLINEAR_SPACE + INTERLINEAR_SPACE;

	//位置置換
	echo(str_replace($replace_key, $count_text_left, $work_replace));
*/
	//改ページなしの場合
	if ($new_page_flg == false) {
		echo("</g>\n");
	}

}

function get_line_width($arr_col_list, $idx)
{
    $tmp_size1 = "";
    for($j=$idx + 1; $j<=21; $j++)
    {
        if($arr_col_list["col_".$j]["disp_flg"])
        {
            $tmp_size1 = $arr_col_list["col_".$j]["line_start_pos"];
            break;
        }
    }
    $tmp_size2 = $tmp_size1 - $arr_col_list["col_".$idx]["line_start_pos"];
    return $tmp_size2;
}

//罫線位置確認用
function keisen_test() {

	$tate_offset = 40;
/* 改修前の位置
	$arr_tate = array(
		60,
		140,
		190,
		365,
		540,
		715,
		885,
		935,
		1115,
		1163,
		1248,
		1299,
		1384,
		1469,
		1554,
		1637,
		1720,
		1790,
		1873,
		1931
	);
*/
	$arr_tate = array(
		60,		//1
		140,
		190,
		365,
		540,	//5
		715,
		885,
		935,
		1115,
		1163,	//10
		1248,
		1299,
		1384,
		1469,
		1554,	//15
		1637,
		1720,
		1790,
		1873,
		1931	//20
	);
	//罫線位置、列毎の幅を指定する方式にする
	$idx = 0;

	$h_pos = 60;
	$arr_tate[$idx] = $h_pos;
//日付
	$idx++;	$h_pos += 80;
	$arr_tate[$idx] = $h_pos;
//曜日
	$idx++;	$h_pos += 50;
	$arr_tate[$idx] = $h_pos;
//種別
	$idx++;	$h_pos += 175;
	$arr_tate[$idx] = $h_pos;
//グループ
	$idx++;	$h_pos += 175;
	$arr_tate[$idx] = $h_pos;
//勤務実績
	$idx++;	$h_pos += 175;
	$arr_tate[$idx] = $h_pos;
//事由
	$idx++;	$h_pos += 170;
	$arr_tate[$idx] = $h_pos;
//当直
	$idx++;	$h_pos += 50;
	$arr_tate[$idx] = $h_pos;
//手当
	$idx++;	$h_pos += 180;
	$arr_tate[$idx] = $h_pos;
//前
	$idx++;	$h_pos += 48;
	$arr_tate[$idx] = $h_pos;
//出勤
	$idx++;	$h_pos += 85;
	$arr_tate[$idx] = $h_pos;
//翌
	$idx++;	$h_pos += 51;
	$arr_tate[$idx] = $h_pos;
//退勤
	$idx++;	$h_pos += 85;
	$arr_tate[$idx] = $h_pos;
//勤務
	$idx++;	$h_pos += 70;
	$arr_tate[$idx] = $h_pos;
//法内残法外残
	$idx++;	$h_pos += 85;
	$arr_tate[$idx] = $h_pos;
//深勤
	$idx++;	$h_pos += 85;
	$arr_tate[$idx] = $h_pos;
//外出
	$idx++;	$h_pos += 83;
	$arr_tate[$idx] = $h_pos;
//休憩
	$idx++;	$h_pos += 83;
	$arr_tate[$idx] = $h_pos;
//復退
	$idx++;	$h_pos += 83;
	$arr_tate[$idx] = $h_pos;
//日数
	$idx++;	$h_pos += 58;
	$arr_tate[$idx] = $h_pos;
//会議研修(h) 右端までの残り 79

	//ヘッダ位置、罫線位置から相対とする
	//                     0                   5                  10                  15              19
	$arr_head_pos = array(43, 27, 90, 90, 90, 87, 25, 92, 24, 42, 25, 43, 35, 43, 43, 42, 42, 41, 29, 39);

	//データ位置、罫線位置から相対とする、センタリング、右寄せ等あるため位置調整をする
	//                    0               5                 10                  15              19
	$arr_data_pos = array(8, 27, 6, 6, 6, 4, 25, 6, 24, 75, 25, 76, 60, 76, 76, 74, 74, 73, 48, 69);

	echo("	<page>\n");
	if ($chapter_flag){
		echo("	<chapter name=\"".$emp_name."\" />\n");
	}
	echo("	<background id=\"".$emp_id."\" />");
	echo("		<svg x=\"0\" y=\"0\" width=\"21cm\" height=\"29.7cm\" viewBox=\"0 0 2100 2970\">\n");
	echo("			<g font-size=\"".FONT_SIZE."\" fill=\"black\" font-family=\"'ＭＳ ゴシック'\">\n");

	$table_index_top      = 0 + (INTERLINEAR_SPACE * 2);
	$table_index_left     = 60 + $tate_offset;
	$table_index_right    = LINE_COL_LAST;
	$table_index_bottom   = $table_index_top + ((FONT_SIZE + INTERLINEAR_SPACE) * 3) + INTERLINEAR_SPACE;

	pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_top,       $table_index_right,  2, 3);
	for ($i=0; $i<count($arr_tate); $i++) {
	
		pxdoc_utils::out_vertical_line(  $arr_tate[$i] + $tate_offset,     $table_index_top,       $table_index_bottom, 2, 3);
		if ($i <count($arr_tate)-1) {
			$wk_width = $arr_tate[$i+1] - $arr_tate[$i];
		} else {
			$wk_width = LINE_COL_LAST - $tate_offset - $arr_tate[$i];
		}
		pxdoc_utils::out_text($arr_tate[$i] + $arr_head_pos[$i] + $tate_offset, $table_index_top + 32, $i);
		$center_flg = false;
		if ($i == 1) { 
			$center_flg = true;
		}
		pxdoc_utils::out_text($arr_tate[$i] + $arr_data_pos[$i] + $tate_offset, $table_index_top + 52, $wk_width, $center_flg);
	}
	pxdoc_utils::out_vertical_line(  LINE_COL_LAST,     $table_index_top,       $table_index_bottom, 2, 3);
	pxdoc_utils::out_horizontal_line($table_index_left,     $table_index_bottom,       $table_index_right,  2, 3);

	echo("			</g>\n");
	echo("		</svg>\n");
	echo("	</page>\n");
	echo("</pxd>");
}

/**
 * 指定職員をソートする
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $arr_sortkey 表示順設定
 * @param mixed $emp_id_list 指定職員ID
 * @return mixed 職員ID
 *
 */
function get_arr_emp_id_by_sortkey($con, $fname, $srch_name, $arr_sortkey, $emp_id_list) {
	
	$sql = "select empmst.emp_id, emp_personal_id, emp_lt_nm, emp_ft_nm from empmst ";
	$sql .= "left join classmst on classmst.class_id = empmst.emp_class ";
	$sql .= "left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute ";
	$sql .= "left join deptmst on deptmst.dept_id = empmst.emp_dept ";
	$sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";
	
	$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') ";

	//職員ID
	if ($emp_id_list != "") {
		$emp_ids = explode(",", $emp_id_list);
		$emp_id_str = "";
		for ($i=0; $i<count($emp_ids); $i++) {
			if ($i > 0) {
				$emp_id_str .= ",";
			}
			$emp_id_str .= "'".$emp_ids[$i]."'";
		}
		$cond .= "and empmst.emp_id in ($emp_id_str) ";
	}
	
	//ソートキー指定数
	$sort_num = 0;
	for ($i=1; $i<=5; $i++) {
		$varname = "sortkey".$i;
		if ($arr_sortkey[$varname] > 0) {
			$sort_num++;
		}
	}
	if ($sort_num == 0) {
		$cond .= " order by emp_personal_id";
	} else {
		//出力順項目
		$arr_sortitem = array("", "emp_personal_id", "emp_kn_lt_nm, emp_kn_ft_nm", "classmst.link_key, empmst.emp_class", "atrbmst.link_key, empmst.emp_attribute", "deptmst.link_key1, empmst.emp_dept", "jobmst.link_key, jobmst.order_no");
		$cond .= " order by ";
		$first_flg = true;
		for ($i=1; $i<=5; $i++) {
			$varname = "sortkey".$i;
			$idx = $arr_sortkey[$varname];
			$wk_sortitem = $arr_sortitem[$idx];
			if ($idx > 0) {
				if ($first_flg) {
					$first_flg = false;
				} else {
					$cond .= ", ";
				}
				$cond .= $wk_sortitem;
			}
		}		
	}
	
	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	$arr_emp_id = array();
	while ($row = pg_fetch_array($sel_emp)) {
		$arr_emp_id[] = $row["emp_id"];
	}	
	return $arr_emp_id;
}


?>
