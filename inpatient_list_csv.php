<?
ob_start();

require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// �¾��������¥����å�
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// �إå��Ԥ����
$header_csv = inpatient_csv_get_header($type);

// �ǡ����١�������³
$con = connect2db($fname);

// �ǡ����Ԥ����
$body_csv = inpatient_csv_get_data($con, $type, $initial, $bldgwd, $order, $list_sect_id, $list_year, $list_month, $list_day, $list_year2, $list_month2, $list_day2, $term, $fname);
$csv = mb_convert_encoding($header_csv . $body_csv, "SJIS", "EUC-JP");

// CSV�����
$file_name = "patient_list.csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

// �إå��Ԥ����
function inpatient_csv_get_header($type) {
	switch ($type) {

	case "2":  // ��������
	case "7":  // �������ꥹ��
	case "8":  // ����󥻥봵��
	case "9":  // ���д���
	case "10":  // ���񴵼�
	case "12":  // ž��ͽ�괵��
	case "13":  // ž������
		return "����ID,���Ի�̾,ǯ��,���Ų�,����,�¼�\n";

	case "1":  // ����ͽ�괵��
		return "����ID,���Ի�̾,ǯ��,���Ų�,�缣��,����,�¼�,����ͽ������,����̾(����̾)\n";

	case "3":  // ������������
		return "����ID,���Ի�̾,ǯ��,���Ų�,�缣��,����,�¼�,��������,����̾(����̾)\n";

	case "5":  // �ౡͽ�괵��
		return "����ID,���Ի�̾,ǯ��,���Ų�,����,�¼�,�ౡͽ������\n";

	case "4":  // �����ౡ����
	case "6":  // 3��������ౡ����
	case "11":  // �ౡ����
		return "����ID,���Ի�̾,ǯ��,���Ų�,����,�¼�,�ౡ����\n";
	}
}

// �ǡ����Ԥ����
function inpatient_csv_get_data($con, $type, $initial, $bldgwd, $order, $list_sect_id, $list_year, $list_month, $list_day, $list_year2, $list_month2, $list_day2, $term, $fname) {
	$data = "";

	switch ($type) {

	case "1":  // ����ͽ�괵��
		$sql = "select inptres.ptif_id, inptres.inpt_lt_kj_nm, inptres.inpt_ft_kj_nm, inptres.inpt_in_res_dt_flg, inptres.inpt_in_res_dt, inptres.inpt_in_res_tm, inptres.inpt_in_res_ym, inptres.inpt_in_res_td, case when inptres.inpt_in_res_dt_flg = true then inptres.inpt_in_res_dt || inptres.inpt_in_res_tm else inptres.inpt_in_res_ym || case when inptres.inpt_in_res_td = '1' then '01' when inptres.inpt_in_res_td = '2' then '11' else '21' end || '0000' end as in_res_dt_for_order, ptifmst.ptif_birth, sectmst.sect_nm, bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name, drmst.dr_nm, inptres.inpt_disease from inptres left join ptifmst on ptifmst.ptif_id = inptres.ptif_id left join sectmst on sectmst.enti_id = inptres.inpt_enti_id and sectmst.sect_id = inptres.inpt_sect_id left join bldgmst on bldgmst.bldg_cd = inptres.bldg_cd left join wdmst on wdmst.bldg_cd = inptres.bldg_cd and wdmst.ward_cd = inptres.ward_cd left join ptrmmst on ptrmmst.bldg_cd = inptres.bldg_cd and ptrmmst.ward_cd = inptres.ward_cd and ptrmmst.ptrm_room_no = inptres.ptrm_room_no left join drmst on inptres.inpt_sect_id = drmst.sect_id and inptres.dr_id = drmst.dr_id";
		if ($list_day <= 10) {
			$from_day_for_td = "01";
		} else if ($list_day <= 20) {
			$from_day_for_td = "11";
		} else {
			$from_day_for_td = "21";
		}
		if ($list_day2 <= 10) {
			$to_day_for_td = "10";
		} else if ($list_day2 <= 20) {
			$to_day_for_td = "20";
		} else {
			$to_day_for_td = "31";
		}
		$cond = "where ((inptres.inpt_in_res_dt_flg = 't' and inptres.inpt_in_res_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2') or (inptres.inpt_in_res_dt_flg = 'f' and ((inptres.inpt_in_res_td = '1' and (inptres.inpt_in_res_ym || '01') between '$list_year$list_month$from_day_for_td' and '$list_year2$list_month2$to_day_for_td') or (inptres.inpt_in_res_td = '2' and (inptres.inpt_in_res_ym || '11') between '$list_year$list_month$from_day_for_td' and '$list_year2$list_month2$to_day_for_td') or (inptres.inpt_in_res_td = '3' and (inptres.inpt_in_res_ym || '21') between '$list_year$list_month$from_day_for_td' and '$list_year2$list_month2$to_day_for_td') or (inptres.inpt_in_res_td = '9' and inptres.inpt_in_res_ym between '$list_year$list_month' and '$list_year2$list_month2'))))";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and inptres.bldg_cd = $bldg_cd and inptres.ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inptres.inpt_sect_id = $list_sect_id";
		}
		switch ($order) {
		case "1":
			$cond .= " order by inptres.inpt_keywd";
			break;
		case "2":
			$cond .= " order by inptres.inpt_keywd desc";
			break;
		case "3":
			$cond .= " order by ptrmmst.ptrm_name";
			break;
		case "4":
			$cond .= " order by ptrmmst.ptrm_name desc";
			break;
		case "5":
			$cond .= " order by in_res_dt_for_order";
			break;
		case "6":
			$cond .= " order by in_res_dt_for_order desc";
			break;
		case "7":
			$cond .= " order by drmst.dr_nm";
			break;
		case "8":
			$cond .= " order by drmst.dr_nm desc";
			break;
		}
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			if ($row["inpt_in_res_dt_flg"] == "t") {
				$res_dt = $row["inpt_in_res_dt"];
				$res_yr = substr($res_dt, 0, 4);
				$res_mon = substr($res_dt, 4, 2);
				$res_day = substr($res_dt, 6, 2);
				$res_tm = $row["inpt_in_res_tm"];
				$res_hr = substr($res_tm, 0, 2);
				$res_min = substr($res_tm, 2, 2);
				$in_res_dt = $res_yr . "/" . $res_mon . "/" . $res_day . " " . $res_hr . ":" . $res_min;
			} else {
				$res_ym = $row["inpt_in_res_ym"];
				$res_y = substr($res_ym, 0, 4);
				$res_m = substr($res_ym, 4, 2);
				switch ($row["inpt_in_res_td"]) {
				case "1":
					$res_td = "/��";
					break;
				case "2":
					$res_td = "/��";
					break;
				case "3":
					$res_td = "/��";
					break;
				default:
					$res_td = "";
					break;
				}
				$in_res_dt = $res_y . "/" . $res_m . $res_td;
			}

			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["dr_nm"]},";
			$data .= "{$row["bldg_name"]}{$row["ward_name"]},";
			$data .= "{$row["ptrm_name"]},";
			$data .= "{$in_res_dt},";
			$data .= "{$row["inpt_disease"]}\n";
		}
		break;

	case "2":  // ��������
	case "12":  // ž��ͽ�괵��
	case "13":  // ž������
		$sql = "select inptmst.ptif_id, inptmst.inpt_lt_kj_nm, inptmst.inpt_ft_kj_nm, ptifmst.ptif_birth, sectmst.sect_nm, bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id inner join sectmst on sectmst.enti_id = inptmst.inpt_enti_id and sectmst.sect_id = inptmst.inpt_sect_id inner join bldgmst on bldgmst.bldg_cd = inptmst.bldg_cd inner join wdmst on wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd inner join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no";
		$cond = "where inptmst.inpt_in_flg = 't'";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and inptmst.bldg_cd = $bldg_cd and inptmst.ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inptmst.inpt_sect_id = $list_sect_id";
		}
		if ($type == "12") {
			$cond .= " and exists (select * from inptmove where inptmove.ptif_id = inptmst.ptif_id and inptmove.move_cfm_flg = 'f' and inptmove.move_del_flg = 'f')";
		} else if ($type == "13") {
			$cond .= " and exists (select * from inptmove where inptmove.ptif_id = inptmst.ptif_id and inptmove.move_cfm_flg = 't' and inptmove.move_del_flg = 'f' and inptmove.move_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2')";
		}
		switch ($order) {
		case "1":
			$cond .= " order by inptmst.inpt_keywd";
			break;
		case "2":
			$cond .= " order by inptmst.inpt_keywd desc";
			break;
		case "3":
			$cond .= " order by ptrmmst.ptrm_name";
			break;
		case "4":
			$cond .= " order by ptrmmst.ptrm_name desc";
			break;
		}
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_name"]}{$row["ward_name"]},";
			$data .= "{$row["ptrm_name"]}\n";
		}
		break;

	case "3":  // ������������
		$sql = "(select char(1) 'i' as pt_type, inptmst.ptif_id, inptmst.inpt_lt_kj_nm, inptmst.inpt_ft_kj_nm, inptmst.inpt_in_dt, inptmst.inpt_in_tm, ptifmst.ptif_birth, sectmst.sect_nm, drmst.dr_nm, bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name, inptmst.inpt_keywd, inptmst.inpt_disease from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id inner join sectmst on sectmst.enti_id = inptmst.inpt_enti_id and sectmst.sect_id = inptmst.inpt_sect_id inner join bldgmst on bldgmst.bldg_cd = inptmst.bldg_cd inner join wdmst on wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd inner join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no left join drmst on inptmst.inpt_sect_id = drmst.sect_id and inptmst.dr_id = drmst.dr_id where inptmst.inpt_in_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2'";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$sql .= " and inptmst.bldg_cd = $bldg_cd and inptmst.ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$sql .= " and inptmst.inpt_sect_id = $list_sect_id";
		}
		$sql .= ") union all (select char(1) 'o' as pt_type, inpthist.ptif_id, inpthist.inpt_lt_kj_nm, inpthist.inpt_ft_kj_nm, inpthist.inpt_in_dt, inpthist.inpt_in_tm, ptifmst.ptif_birth, sectmst.sect_nm, drmst.dr_nm, bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name, inpthist.inpt_keywd, inpthist.inpt_disease from inpthist inner join ptifmst on ptifmst.ptif_id = inpthist.ptif_id inner join sectmst on sectmst.enti_id = inpthist.inpt_enti_id and sectmst.sect_id = inpthist.inpt_sect_id inner join bldgmst on bldgmst.bldg_cd = inpthist.bldg_cd inner join wdmst on wdmst.bldg_cd = inpthist.bldg_cd and wdmst.ward_cd = inpthist.ward_cd inner join ptrmmst on ptrmmst.bldg_cd = inpthist.bldg_cd and ptrmmst.ward_cd = inpthist.ward_cd and ptrmmst.ptrm_room_no = inpthist.ptrm_room_no left join drmst on inpthist.inpt_sect_id = drmst.sect_id and inpthist.dr_id = drmst.dr_id where inpthist.inpt_in_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2'";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$sql .= " and inpthist.bldg_cd = $bldg_cd and inpthist.ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$sql .= " and inpthist.inpt_sect_id = $list_sect_id";
		}
		$sql .= ")";
		$cond = "";
		switch ($order) {
		case "1":
			$cond .= " order by 13, 12, 5, 6";
			break;
		case "2":
			$cond .= " order by 13 desc, 12, 5, 6";
			break;
		case "3":
			$cond .= " order by 12, 13, 5, 6";
			break;
		case "4":
			$cond .= " order by 12 desc, 13, 5, 6";
			break;
		case "5":
			$cond .= " order by 5, 6, 13, 12";
			break;
		case "6":
			$cond .= " order by 5 desc, 6 desc, 13, 12";
			break;
		}
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["dr_nm"]},";
			$data .= "{$row["bldg_name"]}{$row["ward_name"]},";
			$data .= "{$row["ptrm_name"]},";
			$data .= format_date($row["inpt_in_dt"]) . " " . format_time($row["inpt_in_tm"]) . ",";
			$data .= "{$row["inpt_disease"]}\n";
		}
		break;

	case "4":  // �����ౡ����
		$sql = "select *, (select ptif_birth from ptifmst where ptifmst.ptif_id = inpthist.ptif_id) as ptif_birth, (select sect_nm from sectmst where sectmst.enti_id = inpthist.inpt_enti_id and sectmst.sect_id = inpthist.inpt_sect_id) as sect_nm, (select bldg_name from bldgmst where bldgmst.bldg_cd = inpthist.bldg_cd) as bldg_nm, (select ward_name from wdmst where wdmst.bldg_cd = inpthist.bldg_cd and wdmst.ward_cd = inpthist.ward_cd) as ward_nm, (select ptrm_name from ptrmmst where ptrmmst.bldg_cd = inpthist.bldg_cd and ptrmmst.ward_cd = inpthist.ward_cd and ptrmmst.ptrm_room_no = inpthist.ptrm_room_no) as rm_nm from inpthist";
		$cond = "where inpthist.inpt_out_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2'";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and inpthist.bldg_cd = $bldg_cd and inpthist.ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inpthist.inpt_sect_id = $list_sect_id";
		}
		switch ($order) {
		case "1":
			$cond .= " order by inpthist.inpt_keywd";
			break;
		case "2":
			$cond .= " order by inpthist.inpt_keywd desc";
			break;
		case "3":
			$cond .= " order by inpthist.inpt_out_dt desc, inpthist.inpt_out_tm desc";
			break;
		case "4":
			$cond .= " order by inpthist.inpt_out_dt, inpthist.inpt_out_tm";
			break;
		}
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_nm"]}{$row["ward_nm"]},";
			$data .= "{$row["rm_nm"]},";
			$data .= format_date($row["inpt_out_dt"]) . " " . format_time($row["inpt_out_tm"]) . "\n";
		}
		break;

	case "5":  // �ౡͽ�괵��
		$sql = "select *, (select ptif_birth from ptifmst where ptifmst.ptif_id = inptmst.ptif_id) as ptif_birth, (select sect_nm from sectmst where sectmst.enti_id = inptmst.inpt_enti_id and sectmst.sect_id = inptmst.inpt_sect_id) as sect_nm, (select bldg_name from bldgmst where bldgmst.bldg_cd = inptmst.bldg_cd) as bldg_nm, (select ward_name from wdmst where wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd) as ward_nm, (select ptrm_name from ptrmmst where ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no) as rm_nm from inptmst";
		$cond = "where inpt_out_res_flg = 't'";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and bldg_cd = $bldg_cd and ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inpt_sect_id = $list_sect_id";
		}
		switch ($order) {
		case "1":
			$cond .= " order by inpt_keywd";
			break;
		case "2":
			$cond .= " order by inpt_keywd desc";
			break;
		case "3":
			$cond .= " order by rm_nm";
			break;
		case "4":
			$cond .= " order by rm_nm desc";
			break;
		case "5":
			$cond .= " order by inpt_out_res_dt desc, inpt_out_res_tm desc";
			break;
		case "6":
			$cond .= " order by inpt_out_res_dt, inpt_out_res_tm";
			break;
		}
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_nm"]}{$row["ward_nm"]},";
			$data .= "{$row["rm_nm"]},";
			$data .= format_date($row["inpt_out_res_dt"]) . " " . format_time($row["inpt_out_res_tm"]) . "\n";
		}
		break;

	case "6":  // 3��������ౡ����
		$today = date("Ymd");
		$limit_date = date("Ymd", strtotime("-3 month"));
		$sql = "select inpthist.ptif_id, inpthist.inpt_in_dt, inpthist.inpt_in_tm, inpthist.inpt_lt_kj_nm, inpthist.inpt_ft_kj_nm, inpthist.inpt_out_dt, inpthist.inpt_out_tm, ptifmst.ptif_birth, sectmst.sect_nm, bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name from inpthist inner join ptifmst on ptifmst.ptif_id = inpthist.ptif_id inner join sectmst on sectmst.enti_id = inpthist.inpt_enti_id and sectmst.sect_id = inpthist.inpt_sect_id inner join bldgmst on bldgmst.bldg_cd = inpthist.bldg_cd inner join wdmst on wdmst.bldg_cd = inpthist.bldg_cd and wdmst.ward_cd = inpthist.ward_cd inner join ptrmmst on ptrmmst.bldg_cd = inpthist.bldg_cd and ptrmmst.ward_cd = inpthist.ward_cd and ptrmmst.ptrm_room_no = inpthist.ptrm_room_no";
		$cond = "where inpthist.inpt_out_dt between '$limit_date' and '$today'";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and inpthist.bldg_cd = $bldg_cd and inpthist.ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inpthist.inpt_sect_id = $list_sect_id";
		}
		switch ($order) {
		case "1":
			$cond .= " order by inpthist.inpt_keywd";
			break;
		case "2":
			$cond .= " order by inpthist.inpt_keywd desc";
			break;
		case "3":
			$cond .= " order by inpthist.inpt_out_dt desc, inpthist.inpt_out_tm desc";
			break;
		case "4":
			$cond .= " order by inpthist.inpt_out_dt, inpthist.inpt_out_tm";
			break;
		}
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_name"]}{$row["ward_name"]},";
			$data .= "{$row["ptrm_name"]},";
			$data .= format_date($row["inpt_out_dt"]) . " " . format_time($row["inpt_out_tm"]) . "\n";
		}
		break;

	case "7":  // �������ꥹ��
		require_once("show_in_current_list.ini");
		$current_patients = get_current_patients($con, $bldgwd, $fname, $list_sect_id, $list_year, $list_month, $list_day);
		foreach ($current_patients as $tmp_ptif_id => $tmp_patient) {
			$data .= "{$tmp_ptif_id},";
			$data .= "{$tmp_patient["name"]},";
			$data .= "" . get_age($tmp_patient["birth"]) . "��,";
			$data .= "{$tmp_patient["sect_nm"]},";
			$data .= "{$tmp_patient["bldg_name"]}{$tmp_patient["ward_name"]},";
			$data .= "{$tmp_patient["ptrm_name"]}\n";
		}
		break;

	case "8":  // ����󥻥봵��
		$sql = "select *, (select ptif_birth from ptifmst where ptifmst.ptif_id = inptcancel.ptif_id) as ptif_birth, (select sect_nm from sectmst where sectmst.enti_id = inptcancel.inpt_enti_id and sectmst.sect_id = inptcancel.inpt_sect_id) as sect_nm, (select bldg_name from bldgmst where bldgmst.bldg_cd = inptcancel.bldg_cd) as bldg_nm, (select ward_name from wdmst where wdmst.bldg_cd = inptcancel.bldg_cd and wdmst.ward_cd = inptcancel.ward_cd) as ward_nm, (select ptrm_name from ptrmmst where ptrmmst.bldg_cd = inptcancel.bldg_cd and ptrmmst.ward_cd = inptcancel.ward_cd and ptrmmst.ptrm_room_no = inptcancel.ptrm_room_no) as rm_nm from inptcancel";
		$arr_keywd = array();
		switch ($initial) {
		case "1":  // ����
			array_push($arr_keywd, "01", "02", "03", "04", "05");
			break;
		case "2":  // ����
			array_push($arr_keywd, "06", "07", "08", "09", "10");
			array_push($arr_keywd, "11", "12", "13", "14", "15");
			break;
		case "3":  // ����
			array_push($arr_keywd, "16", "17", "18", "19", "20");
			array_push($arr_keywd, "21", "22", "23", "24", "25");
			break;
		case "4":  // ����
			array_push($arr_keywd, "26", "27", "28", "29", "30");
			array_push($arr_keywd, "31", "32", "33", "34", "35");
			break;
		case "5":  // �ʹ�
			array_push($arr_keywd, "36", "37", "38", "39", "40");
			break;
		case "6":  // �Ϲ�
			array_push($arr_keywd, "41", "42", "43", "44", "45");
			array_push($arr_keywd, "46", "47", "48", "49", "50");
			array_push($arr_keywd, "51", "52", "53", "54", "55");
			break;
		case "7":  // �޹�
			array_push($arr_keywd, "56", "57", "58", "59", "60");
			break;
		case "8":  // ���
			array_push($arr_keywd, "61", "62", "63");
			break;
		case "9":  // ���
			array_push($arr_keywd, "64", "65", "66", "67", "68");
			break;
		case "10":  // ���
			array_push($arr_keywd, "69", "70", "71", "72", "73");
			array_push($arr_keywd, "99");
			break;
		}
		$cond = "where (inpt_keywd like '";
		$cond .= join("%' or inpt_keywd like '", $arr_keywd);
		$cond .= "%')";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and bldg_cd = $bldg_cd and ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inpt_sect_id = $list_sect_id";
		}
		$cond .= " order by inpt_keywd, ptif_id, inpt_res_ccl_id desc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_nm"]}{$row["ward_nm"]},";
			$data .= "{$row["rm_nm"]}\n";
		}
		break;

	case "9":  // ���д���
		$sql = "select *, (select ptif_birth from ptifmst where ptifmst.ptif_id = inptmst.ptif_id) as ptif_birth, (select sect_nm from sectmst where sectmst.enti_id = inptmst.inpt_enti_id and sectmst.sect_id = inptmst.inpt_sect_id) as sect_nm, (select bldg_name from bldgmst where bldgmst.bldg_cd = inptmst.bldg_cd) as bldg_nm, (select ward_name from wdmst where wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd) as ward_nm, (select ptrm_name from ptrmmst where ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no) as rm_nm from inptmst";
		$cond = "where exists (select * from inptgoout where inptgoout.ptif_id = inptmst.ptif_id and inptgoout.out_type = '1' and to_date(inptgoout.out_date, 'YYYYMMDD') >= to_date(inptmst.inpt_in_dt, 'YYYYMMDD')";
		if ($term == "1") {
			$cond .= " and to_timestamp('" . date("YmdHi") . "', 'YYYYMMDDHH24MI') > to_timestamp(out_date || ret_time, 'YYYYMMDDHH24MI')";
		} else if ($term == "2") {
			$cond .= " and to_timestamp('" . date("YmdHi") . "', 'YYYYMMDDHH24MI') between to_timestamp(out_date || out_time, 'YYYYMMDDHH24MI') and to_timestamp(out_date || ret_time, 'YYYYMMDDHH24MI')";
		} else if ($term == "3") {
			$cond .= " and to_timestamp('" . date("YmdHi") . "', 'YYYYMMDDHH24MI') < to_timestamp(out_date || out_time, 'YYYYMMDDHH24MI')";
		}
		$cond .= ")";
		$cond .= " and (inpt_in_flg = 'true' or inpt_out_res_flg = 'true' or inpt_out_flg = 'true')";
		if ($term == "1") {
			$arr_keywd = array();
			switch ($initial) {
			case "1":  // ����
				array_push($arr_keywd, "01", "02", "03", "04", "05");
				break;
			case "2":  // ����
				array_push($arr_keywd, "06", "07", "08", "09", "10");
				array_push($arr_keywd, "11", "12", "13", "14", "15");
				break;
			case "3":  // ����
				array_push($arr_keywd, "16", "17", "18", "19", "20");
				array_push($arr_keywd, "21", "22", "23", "24", "25");
				break;
			case "4":  // ����
				array_push($arr_keywd, "26", "27", "28", "29", "30");
				array_push($arr_keywd, "31", "32", "33", "34", "35");
				break;
			case "5":  // �ʹ�
				array_push($arr_keywd, "36", "37", "38", "39", "40");
				break;
			case "6":  // �Ϲ�
				array_push($arr_keywd, "41", "42", "43", "44", "45");
				array_push($arr_keywd, "46", "47", "48", "49", "50");
				array_push($arr_keywd, "51", "52", "53", "54", "55");
				break;
			case "7":  // �޹�
				array_push($arr_keywd, "56", "57", "58", "59", "60");
				break;
			case "8":  // ���
				array_push($arr_keywd, "61", "62", "63");
				break;
			case "9":  // ���
				array_push($arr_keywd, "64", "65", "66", "67", "68");
				break;
			case "10":  // ���
				array_push($arr_keywd, "69", "70", "71", "72", "73");
				array_push($arr_keywd, "99");
				break;
			}
			$cond .= " and (inpt_keywd like '";
			$cond .= join("%' or inpt_keywd like '", $arr_keywd);
			$cond .= "%')";
		}
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and bldg_cd = $bldg_cd and ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inpt_sect_id = $list_sect_id";
		}
		$cond .= " order by inpt_keywd, ptif_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_nm"]}{$row["ward_nm"]},";
			$data .= "{$row["rm_nm"]}\n";
		}
		break;

	case "10":  // ���񴵼�
		$sql = "select *, (select ptif_birth from ptifmst where ptifmst.ptif_id = inptmst.ptif_id) as ptif_birth, (select sect_nm from sectmst where sectmst.enti_id = inptmst.inpt_enti_id and sectmst.sect_id = inptmst.inpt_sect_id) as sect_nm, (select bldg_name from bldgmst where bldgmst.bldg_cd = inptmst.bldg_cd) as bldg_nm, (select ward_name from wdmst where wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd) as ward_nm, (select ptrm_name from ptrmmst where ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no) as rm_nm from inptmst";
		$cond = "where exists (select * from inptgoout where inptgoout.ptif_id = inptmst.ptif_id and inptgoout.out_type = '2' and to_date(inptgoout.out_date, 'YYYYMMDD') >= to_date(inptmst.inpt_in_dt, 'YYYYMMDD') and ";
		if ($term == "1") {
			$cond .= "to_date('" . date("Ymd") . "', 'YYYYMMDD') > to_date(ret_date, 'YYYYMMDD')";
		} else if ($term == "2") {
			$cond .= "to_date('" . date("Ymd") . "', 'YYYYMMDD') between to_date(out_date, 'YYYYMMDD') and to_date(ret_date, 'YYYYMMDD')";
		} else if ($term == "3") {
			$cond .= "to_date('" . date("Ymd") . "', 'YYYYMMDD') < to_date(out_date, 'YYYYMMDD')";
		}
		$cond .= ")";
		$cond .= " and (inpt_in_flg = 'true' or inpt_out_res_flg = 'true' or inpt_out_flg = 'true')";
		if ($term == "1") {
			$arr_keywd = array();
			switch ($initial) {
			case "1":  // ����
				array_push($arr_keywd, "01", "02", "03", "04", "05");
				break;
			case "2":  // ����
				array_push($arr_keywd, "06", "07", "08", "09", "10");
				array_push($arr_keywd, "11", "12", "13", "14", "15");
				break;
			case "3":  // ����
				array_push($arr_keywd, "16", "17", "18", "19", "20");
				array_push($arr_keywd, "21", "22", "23", "24", "25");
				break;
			case "4":  // ����
				array_push($arr_keywd, "26", "27", "28", "29", "30");
				array_push($arr_keywd, "31", "32", "33", "34", "35");
				break;
			case "5":  // �ʹ�
				array_push($arr_keywd, "36", "37", "38", "39", "40");
				break;
			case "6":  // �Ϲ�
				array_push($arr_keywd, "41", "42", "43", "44", "45");
				array_push($arr_keywd, "46", "47", "48", "49", "50");
				array_push($arr_keywd, "51", "52", "53", "54", "55");
				break;
			case "7":  // �޹�
				array_push($arr_keywd, "56", "57", "58", "59", "60");
				break;
			case "8":  // ���
				array_push($arr_keywd, "61", "62", "63");
				break;
			case "9":  // ���
				array_push($arr_keywd, "64", "65", "66", "67", "68");
				break;
			case "10":  // ���
				array_push($arr_keywd, "69", "70", "71", "72", "73");
				array_push($arr_keywd, "99");
				break;
			}
			$cond .= " and (inpt_keywd like '";
			$cond .= join("%' or inpt_keywd like '", $arr_keywd);
			$cond .= "%')";
		}
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and bldg_cd = $bldg_cd and ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inpt_sect_id = $list_sect_id";
		}
		$cond .= " order by inpt_keywd, ptif_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_nm"]}{$row["ward_nm"]},";
			$data .= "{$row["rm_nm"]}\n";
		}
		break;

	case "11":  // �ౡ����
		$sql = "select *, (select ptif_birth from ptifmst where ptifmst.ptif_id = inpthist.ptif_id) as ptif_birth, (select sect_nm from sectmst where sectmst.enti_id = inpthist.inpt_enti_id and sectmst.sect_id = inpthist.inpt_sect_id) as sect_nm, (select bldg_name from bldgmst where bldgmst.bldg_cd = inpthist.bldg_cd) as bldg_nm, (select ward_name from wdmst where wdmst.bldg_cd = inpthist.bldg_cd and wdmst.ward_cd = inpthist.ward_cd) as ward_nm, (select ptrm_name from ptrmmst where ptrmmst.bldg_cd = inpthist.bldg_cd and ptrmmst.ward_cd = inpthist.ward_cd and ptrmmst.ptrm_room_no = inpthist.ptrm_room_no) as rm_nm from inpthist";
		$arr_keywd = array();
		switch ($initial) {
		case "1":  // ����
			array_push($arr_keywd, "01", "02", "03", "04", "05");
			break;
		case "2":  // ����
			array_push($arr_keywd, "06", "07", "08", "09", "10");
			array_push($arr_keywd, "11", "12", "13", "14", "15");
			break;
		case "3":  // ����
			array_push($arr_keywd, "16", "17", "18", "19", "20");
			array_push($arr_keywd, "21", "22", "23", "24", "25");
			break;
		case "4":  // ����
			array_push($arr_keywd, "26", "27", "28", "29", "30");
			array_push($arr_keywd, "31", "32", "33", "34", "35");
			break;
		case "5":  // �ʹ�
			array_push($arr_keywd, "36", "37", "38", "39", "40");
			break;
		case "6":  // �Ϲ�
			array_push($arr_keywd, "41", "42", "43", "44", "45");
			array_push($arr_keywd, "46", "47", "48", "49", "50");
			array_push($arr_keywd, "51", "52", "53", "54", "55");
			break;
		case "7":  // �޹�
			array_push($arr_keywd, "56", "57", "58", "59", "60");
			break;
		case "8":  // ���
			array_push($arr_keywd, "61", "62", "63");
			break;
		case "9":  // ���
			array_push($arr_keywd, "64", "65", "66", "67", "68");
			break;
		case "10":  // ���
			array_push($arr_keywd, "69", "70", "71", "72", "73");
			array_push($arr_keywd, "99");
			break;
		}
		$cond = "where (inpt_keywd like '";
		$cond .= join("%' or inpt_keywd like '", $arr_keywd);
		$cond .= "%')";
		if ($bldgwd != "") {
			list($bldg_cd, $ward_cd) = split("-", $bldgwd);
			$cond .= " and bldg_cd = $bldg_cd and ward_cd = $ward_cd";
		}
		if ($list_sect_id != "") {
			$cond .= " and inpt_sect_id = $list_sect_id";
		}
		switch ($order) {
		case "1":
			$cond .= " order by inpt_keywd";
			break;
		case "2":
			$cond .= " order by inpt_keywd desc";
			break;
		case "3":
			$cond .= " order by inpt_out_dt desc, inpt_out_tm desc";
			break;
		case "4":
			$cond .= " order by inpt_out_dt, inpt_out_tm";
			break;
		}
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$data .= "{$row["ptif_id"]},";
			$data .= "{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]},";
			$data .= "" . get_age($row["ptif_birth"]) . "��,";
			$data .= "{$row["sect_nm"]},";
			$data .= "{$row["bldg_nm"]}{$row["ward_nm"]},";
			$data .= "{$row["rm_nm"]},";
			$data .= format_date($row["inpt_out_dt"]) . " " . format_time($row["inpt_out_tm"]) . "\n";
		}
		break;
	}

	return $data;
}

function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}

function format_date($dt) {
	$pattern = '/^(\d{4})(\d{2})(\d{2})$/';
	$replacement = '$1/$2/$3';
	return preg_replace($pattern, $replacement, $dt);
}

function format_time($tm) {
	$pattern = '/^(\d{2})(\d{2})$/';
	$replacement = '$1:$2';
	return preg_replace($pattern, $replacement, $tm);
}
?>
