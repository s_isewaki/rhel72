<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | メール転送設定</title>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("webmail_alias_functions.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
	js_login_exit();
	exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

// データベースに接続
$con = connect2db($fname);

// 職員情報を取得
$infos = get_empmst($con, $emp_id, $fname);

// 転送先リストを取得
$sql = "select alias from webmail_alias";
$cond = "where emp_id = '$emp_id' order by alias";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
$aliases = array();
while ($row = pg_fetch_array($sel)) {
	$aliases[] = $row["alias"];
}

// データベース接続を閉じる
pg_close($con);
?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteAlias(alias) {
	if (confirm(alias + ' を削除してもよろしいでしょうか？')) {
		document.delform.del_alias.value = alias;
		document.delform.submit();
	}
}
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>&key1=<? echo(urlencode($key1)); ?>&key2=<? echo(urlencode($key2)); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_detail.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="employee_contact_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_condition_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_auth_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_display_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#5279a5"><a href="employee_alias_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>メール転送設定</b></font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infos[1]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infos[7]); ?>&nbsp;<? echo($infos[8]); ?></font></td>
</tr>
</table>
<? if ($reg_auth == 1) { ?>
<form name="mainform" action="option_alias_update.php" method="post">
<p><input type="text" name="alias" value="" size="50" style="ime-mode:disabled"> <input type="submit" value="転送先アドレスの登録"></p>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
<input type="hidden" name="wherefrom" value="employee">
</form>
<? } ?>
<? if (count($aliases) > 0) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>転送先アドレス</b></font></td>
</tr>
</table>
<form name="delform" action="option_alias_update.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2">
<? foreach ($aliases as $alias) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? eh($alias); ?></font></td>
<? if ($reg_auth == 1) { ?>
<td><input type="button" value="アドレスを削除" onclick="deleteAlias('<? eh_jsparam($alias); ?>');"></td>
<? } ?>
</tr>
<? } ?>
</table>
<input type="hidden" name="del_alias" value="">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
<input type="hidden" name="wherefrom" value="employee">
</form>
<? } ?>
</td>
</tr>
</table>
</body>
</html>
