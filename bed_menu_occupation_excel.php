<?
ob_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_occupation_detail_excel.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$checkauth = check_authority($session, 14, $fname);
if($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 棟名を取得
$sql = "select bldg_name from bldgmst";
$cond = "where bldg_cd = $bldg_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_name = pg_fetch_result($sel, 0, "bldg_name");

if ($ward_cd != 0) {

	// 病棟名の取得
	$sql = "select ward_name from wdmst";
	$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ward_name = pg_fetch_result($sel, 0, "ward_name");
} else {
	$ward_cd = "0";
	$ward_name = "すべて";
}
?>
<style type="text/css">
table.bed {border-collapse:collapse;}
table.bed td {border:solid #5279a5 1px;}
</style>
</head>
<body>
<p>事業所（棟）：<? echo($bldg_name); ?> 病棟：<? echo($ward_name); ?></p>
<?
if ($ward_cd == "0") {
	show_bldg_report($con, $bldg_cd, $fname);
	$ward_cd = "";
}

show_ward_layout($con, $bldg_cd, $ward_cd, $session, $fname);

if ($ward_cd != "") {
	show_ward_report($con, $bldg_cd, $ward_cd, $fname);
}
?>
</body>
<? pg_close($con); ?>
</html>
<?
$excel_data = mb_convert_encoding(ob_get_clean(), "Shift_JIS", 'EUC-JP');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=bed_layout.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
echo($excel_data);
?>
