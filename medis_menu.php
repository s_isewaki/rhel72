<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>MEDIS標準マスタ | MEDIS標準マスタ</title>
<?
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0 0 0.8em;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="medis_menu.php?session=<? echo($session); ?>"><img src="img/icon/b31.gif" width="32" height="32" border="0" alt="MEDIS標準マスタ"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="medis_menu.php?session=<? echo($session); ?>"><b>MEDIS標準マスタ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-top:10px;">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p>「ICD10対応電子カルテ標準病名マスタ」</p>
<p>当システムで使用しているICD10対応標準病名マスタは厚生労働省の委託を受けて作成されたもので、財団法人医療情報システム開発センター、標準病名マスター作業班、社会診療報酬支払基金が著作権を有します。</p>
<p style="padding-left:20px;">使用許諾番号　第19-1009号</p>
</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
