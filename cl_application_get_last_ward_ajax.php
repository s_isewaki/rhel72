<?
ob_start();
require("about_authority.php");
require("about_session.php");
require_once("cl_common.ini");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	ajax_server_erorr();
	exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($check_auth == "0") {
	pg_close($con);
	ajax_server_erorr();
	exit;
}


// データベースに接続
$con = connect2db($fname);


$emp_id = get_emp_id($con, $fname, $session);

$apply_id = get_max_apply_id_for_lastcopy($con, $fname, $emp_id, $short_wkfw_name);

if($apply_id != "")
{
    $arr_cl_wkfw = get_cl_wkfw($con, $fname, $short_wkfw_name, $apply_id);
}

//レスポンスデータ作成
if(count($arr_cl_wkfw) == 0)
{
	//検索にヒットしない。
	$response_data = "ajax=0hit\n";
}
else
{

	$response_data = "ajax=success\n";

    for($i=0; $i<count($arr_cl_wkfw); $i++)
    {
        $p_key = $arr_cl_wkfw[$i]["var_name"];
	    $p_value = $arr_cl_wkfw[$i]["value"];
        $response_data .= "$p_key=$p_value\n";
    }
	
	$p_key = "eval_count";
	$p_value = count($arr_cl_wkfw);
	$response_data .= "$p_key=$p_value\n";
}	
	
print $response_data;

function get_emp_id($con, $fname, $session)
{
	$sql  = "select * from empmst";
	$cond = "where emp_id in (select emp_id from session where session_id='$session')";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
	    pg_close($con);
	    ajax_server_erorr();
	    exit;
    }
	return pg_fetch_result($sel, 0, "emp_id");
}



function get_max_apply_id_for_lastcopy($con, $fname, $emp_id, $short_wkfw_name)
{
	$sql  = "select max(a.apply_id) as apply_id from cl_apply a ";
	$sql .= "inner join (select wkfw_id from cl_wkfwmst where short_wkfw_name = '$short_wkfw_name') b on a.wkfw_id = b.wkfw_id ";
	$cond = "where a.emp_id = '$emp_id' and a.apply_stat = '1' "; 

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
	    pg_close($con);
	    ajax_server_erorr();
	    exit;
    }

	return pg_fetch_result($sel, 0, "apply_id");
}

function get_cl_wkfw($con, $fname, $short_wkfw_name, $apply_id)
{
    $cl_wkfw = "cl_wkfw_".$short_wkfw_name;
    $sql  = "select var_name, value from $cl_wkfw ";
    $cond = "where apply_id = $apply_id order by cl_val_id";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
	    pg_close($con);
	    ajax_server_erorr();
	    exit;
    }

	$arr = array();
	while($row = pg_fetch_array($sel))
	{
        $var_name = $row["var_name"];
        $len = strlen($var_name);

        if(substr($var_name, 0, 5) != "kadai" && substr($var_name, $len - 1, 1) != "2")
        {
	        $arr[] = array("var_name" => $row["var_name"], "value" => $row["value"]);
        }
	}
	return $arr;
}

function ajax_server_erorr()
{
	print "ajax=error\n";
	exit;
}


// データベース切断
pg_close($con);
?>
