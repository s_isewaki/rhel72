<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟情報権限チェック
$auth_id = ($path == "3") ? 63 : 32;
$share = check_authority($session,$auth_id,$fname);
if($share == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//----------正規表現チェック----------
	if($category_name == ""){
		echo("<script language=\"javascript\">alert(\"カテゴリ名が入力されていません。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	if(strlen($category_name) > 20){
		echo("<script language=\"javascript\">alert(\"カテゴリ名が長すぎます。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

//**********DB処理**********
//----------ＤＢのコネクション作成----------
$con = connect2db($fname);

//----------Transaction begin----------
pg_exec($con,"begin transaction");

//----------ＳＱＬの処理-------
//----------職員ＩＤの取得----------
$cond = " where session_id='$session'";
$sel = select_from_table($con,$SQL1,$cond,$fname);
if($sel==0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id=pg_result($sel,0,"emp_id");

//----------lib_cate_idの最大値取得----------
$cond_max = "";
$sel_max = select_from_table($con,$SQL183,$cond_max,$fname);
if($sel_max==0){
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$max = pg_result($sel_max,0,"max");
if($max == ""){
	$lib_cate_id = 1;
}else{
	$lib_cate_id = $max+1;
}

$content = array($archive,$lib_cate_id,$emp_id,$category_name);

$in_category = insert_into_table($con,$SQL184,$content,$fname);

if($in_category==0){
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// 情報共有用のディレクトリを作成
if (!is_dir("docArchive")) {
	mkdir("docArchive", 0755);
}
if($archive == 1){
	if (!is_dir("docArchive/private")) {
		mkdir("docArchive/private", 0755);
	}
	mkdir("docArchive/private/cate$lib_cate_id", 0755);
}elseif($archive == 2){
	if (!is_dir("docArchive/all")) {
		mkdir("docArchive/all", 0755);
	}
	mkdir("docArchive/all/cate$lib_cate_id", 0755);
}

pg_exec($con, "commit");
pg_close($con);

if ($path == "3") {
	echo("<script language='javascript'>location.href = 'library_admin_menu.php?session=$session&a=$archive&o=$o';</script>");
} else {
	echo("<script language='javascript'>location.href = 'library_list_all.php?session=$session&a=$archive&o=$o';</script>");
}
?>
