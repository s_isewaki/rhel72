<html>
<head>
<title>病床管理 | 感染詳細</title>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<?
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟登録権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 感染情報の取得
$arr_infect = get_infect($con, $pt_id, $fname);
$infect = $arr_infect[0];
$comment = $arr_infect[1];
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5"> 
<td width="520" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>感染詳細</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感染症：</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infect); ?></font></td>
</tr>
<tr bgcolor="#f6f9ff">
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感染症コメント：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($comment); ?></font></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

function get_infect($con, $pt_id, $fname) {

require("./conf/sql.inf");

	// 感染情報を格納する配列の初期化
	$arr_infect = array();

	// 患者詳細情報の取得
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $SQL86, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// 各種感染症について、感染していたら配列に保存
	//// 梅毒
	$infection2 = pg_result($sel, 0, "ptsubif_infection2");
	if ($infection2 == "t") {
		array_push($arr_infect, "梅毒");
	}
	//// Ｂ型肝炎
	$infection3 = pg_result($sel, 0, "ptsubif_infection3");
	if ($infection3 == "t") {
		array_push($arr_infect, "Ｂ型肝炎");
	}
	//// Ｃ型肝炎
	$infection4 = pg_result($sel, 0, "ptsubif_infection4");
	if ($infection4 == "t") {
		array_push($arr_infect, "Ｃ型肝炎");
	}
	//// ＨＩＶ
	$infection5 = pg_result($sel, 0, "ptsubif_infection5");
	if ($infection5 == "t") {
		array_push($arr_infect, "ＨＩＶ");
	}
	//// ＭＲＳＡ
	$infection6 = pg_result($sel, 0, "ptsubif_infection6");
	if ($infection6 == "t") {
		array_push($arr_infect, "ＭＲＳＡ");
	}
	//// 緑膿菌
	$infection7 = pg_result($sel, 0, "ptsubif_infection7");
	if ($infection7 == "t") {
		array_push($arr_infect, "緑膿菌");
	}
	//// 疥癬
	$infection8 = pg_result($sel, 0, "ptsubif_infection8");
	if ($infection8 == "t") {
		array_push($arr_infect, "疥癬");
	}
	//// ＡＴＬ
	$infection9 = pg_result($sel, 0, "ptsubif_infection9");
	if ($infection9 == "t") {
		array_push($arr_infect, "ＡＴＬ");
	}
	//// その他
	$infection10 = pg_result($sel, 0, "ptsubif_infection10");
	if ($infection10 == "t") {
		array_push($arr_infect, "その他");
	}

	// 配列をカンマで結合
	$infect = join(", ", $arr_infect);

	// コメントの取得
	$comment = pg_result($sel, 0, "ptsubif_infection_com");

	// 戻り値の作成
	$arr_ret = array();
	array_push($arr_ret, $infect, $comment);

	return $arr_ret;

}
?>
