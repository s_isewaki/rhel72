<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
require_once("tmpl_common.ini"); 
ob_end_clean();

$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$session_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}



//==========================================================================
// エディタの表示、現在の最新情報を送信する
//==========================================================================
if ($_REQUEST["mode"] == "ajax_show_editor"){
	$sel = select_from_table($con, "select * from sum_med_saiyou_mst where seq = ".(int)$_REQUEST["seq"],"",$fname);
	$row = pg_fetch_array($sel);
?>
{
	"seq":"<?=@$row["seq"]?>",
	"med_name":"<?=@$row["med_name"]?>",
	"is_visible_at_selector":"<?=@$row["is_visible_at_selector"]?>",
	"used_counter":<?=(int)@$row["used_counter"]?>,
	"units_separated_tab":"<?=@$row["units_separated_tab"]?>",
	"remark":"<?=str_replace('"','\"',@$row["remark"])?>",
	"hot13_cd":"<?=@$row["hot13_cd"]?>",
	"yj_cd":"<?=@$row["yj_cd"]?>"
}
<?
	die;
}

//==========================================================================
// エディタからの更新命令を処理する
//==========================================================================
if ($_REQUEST["mode"] == "ajax_regist_editor"){
	$med_name = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["edit_med_name"])), "EUC-JP", "UTF-8");
	$units_separated_tab = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["edit_units_separated_tab"])), "EUC-JP", "UTF-8");
	$remark = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["edit_remark"])), "EUC-JP", "UTF-8");
	$hot13_cd = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["edit_hot13_cd"])), "EUC-JP", "UTF-8");
	$yj_cd = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["edit_yj_cd"])), "EUC-JP", "UTF-8");
	$sql = "select hot13_cd from sum_med_saiyou_mst";
	$cond = "where seq = " . (int)@$_REQUEST["edit_seq"];
	$sel = select_from_table($con, $sql, $cond, $fname);
	$org_hot13_cd = pg_fetch_result($sel, 0, "hot13_cd");

	// HOTコードが変わる場合は重複チェック
	if ($hot13_cd !== $org_hot13_cd) {
		$sql = "select seq from sum_med_saiyou_mst";
		$cond = "where hot13_cd = " . "'".pg_escape_string($hot13_cd)."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if (pg_num_rows($sel) > 0) {
			echo "{'status':'hot_dup'}\n";
			die;
		}
		$sql = "select hot13_cd from sum_medmst";
		$cond = "where hot13_cd = " . "'".pg_escape_string($hot13_cd)."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if (pg_num_rows($sel) > 0) {
			echo "{'status':'hot_dup'}\n";
			die;
		}
	}

	$sql =
		" update sum_medmst set".
		" hot13_cd = " . "'".pg_escape_string($hot13_cd)."'".
		",yj_cd = "    . "'".pg_escape_string($yj_cd)."'".
		" where hot13_cd = " . "'".pg_escape_string($org_hot13_cd)."'";
	update_set_table($con, $sql, array(), null, "", $fname);

	$sql =
		" update sum_med_saiyou_mst set".
		" med_name = "               . "'".pg_escape_string($med_name)."'".
		",is_visible_at_selector = " . (int)@$_REQUEST["edit_is_visible_at_selector"].
		",used_counter = "           . (int)@$_REQUEST["edit_used_counter"].
		",units_separated_tab = "    . "'".pg_escape_string($units_separated_tab)."'".
		",remark = "                 . "'".pg_escape_string($remark)."'".
		",hot13_cd = "               . "'".pg_escape_string($hot13_cd)."'".
		",yj_cd = "                  . "'".pg_escape_string($yj_cd)."'".
		" where seq = " . (int)@$_REQUEST["edit_seq"];
	update_set_table($con, $sql, array(), null, "", $fname);

?>
{"status":"ok"}
<?
	die;
}

//==========================================================================
// エディタからの削除命令を処理する
//==========================================================================
if ($_REQUEST["mode"] == "ajax_delete_editor"){
	$hot13_cd = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["edit_hot13_cd"])), "EUC-JP", "UTF-8");

	$sql = "delete from sum_medmst where hot13_cd = '" . $hot13_cd . "'";
	update_set_table($con, $sql, array(), null, "", $fname);

	$sql = "delete from sum_med_saiyou_mst where seq = " . (int)@$_REQUEST["edit_seq"];
	update_set_table($con, $sql, array(), null, "", $fname);
?>
{"status":"ok"}
<?
	die;
}

//==========================================================================
// 以下、通常処理
//==========================================================================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';


// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | 処方・注射オーダ</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td { border:#5279a5 solid 1px; text-align:center; white-space:nowrap }
.list1 th {  border:#5279a5 solid 1px; text-align:center; font-weight:normal; white-space:nowrap; background-color:#e3ecf4 }

.list2 {border-collapse:collapse;}
.list2 td { border:#5279a5 solid 1px; text-align:center; white-space:nowrap; background-color:#fff; }
.list2 th {  border:#5279a5 solid 1px; text-align:center; font-weight:normal; white-space:nowrap; background-color:#7495ba; color:#fff }

a.always_blue:link { color:#05d; }
a.always_blue:visited { color:#05d; }
a.always_blue:hover { color:#f50; }

.editbox { background-color:#fff; }

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="adjustListWidth()" onresize="adjustListWidth()">
<? show_sinryo_top_header($session,$fname,"KANRI_ORDSC"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">


<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<?//==========================================================================?>
<?// 処方・注射メニュー                                                       ?>
<?//==========================================================================?>
<?= summary_common_show_ordsc_right_menu($session, "薬品")?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?//==========================================================================?>
<?// サブメニュー                                                             ?>
<?//==========================================================================?>

<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
	<tr height="22">
		<td width="100" align="center" bgcolor="#958f28">
			<a href="sum_ordsc_med_list.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>薬品名一覧</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_regist.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>新規登録/編集</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_import.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>インポート</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_saiyou_import.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>採用薬一括登録</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td></td>
	</tr>
</table>



<?//==========================================================================?>
<?// 更新・削除サーバ通信                                                     ?>
<?//==========================================================================?>
<script type="text/javascript">
	var ajaxObj = null;

	//==========================
	// 薬品選択画面をポップアップ
	//==========================
	var med_selector_window = null;
	function medSelectorWindowPopup(){
		if (med_selector_window && !med_selector_window.closed) med_selector_window.focus();
		var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=850,height=650";
		var url = "sum_ordsc_med_selector.php?session=<?=$session?>&js_callback=medSelectorCallback&encoded_sel_med_name="+encodeURIComponent(encodeURIComponent(document.getElementById("edit_med_name").value));
		med_selector_window = window.open(url, 'med_selector_window', option);
		setTimeout("if (med_selector_window && !med_selector_window.closed) med_selector_window.focus();", 100);
	}

	function medSelectorCallback(hot13_cd){
		document.getElementById("edit_hot13_cd").value = hot13_cd;
	}



	//==========================
	// サーバ送信共通関数
	//==========================
	function inquiryRegist(receiver, param){
		if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
		else {
			try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
			catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
		}
	  ajaxObj.onreadystatechange = receiver;
	  ajaxObj.open("POST", "sum_ordsc_med_list.php", true);
	  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	  ajaxObj.send("session=<?=$session?>" + param);
	}

	//==========================
	// サーバ受信共通関数
	//==========================
	function checkReceiveAjax(){
		if (ajaxObj == null) return false;
		if (ajaxObj.readyState != 4) return false;
		if (typeof(ajaxObj.status) != "number") return false;
		if (ajaxObj.status != 200) return false;
		if (!ajaxObj.responseText) return false;
		try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return false; }}
		catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return false; }
		if (!ret) return false;
		return ret;
	}

	//==========================
	// エディタを表示するようにサーバへ送信
	//==========================
  function showEditor(seq){
		inquiryRegist(returnShowEditor, "&mode=ajax_show_editor&seq="+seq);
  }

	//==========================
	// データの変更内容をサーバへ送信
	//==========================
  function registEditor(command){
		var param = "" +
			"&edit_seq="                   + encodeURIComponent(encodeURIComponent(document.getElementById("edit_seq").value)) +
			"&edit_med_name="              + encodeURIComponent(encodeURIComponent(document.getElementById("edit_med_name").value)) +
			"&edit_is_visible_at_selector="+ (document.getElementById("edit_is_visible_at_selector").checked ? "1" : "0") +
			"&edit_used_counter="          + (document.getElementById("edit_used_counter").checked ? "1" : "0") +
			"&edit_units_separated_tab="   + encodeURIComponent(encodeURIComponent(document.getElementById("edit_units_separated_tab").value)) +
			"&edit_remark="                + encodeURIComponent(encodeURIComponent(document.getElementById("edit_remark").value)) +
			"&edit_hot13_cd="              + encodeURIComponent(encodeURIComponent(document.getElementById("edit_hot13_cd").value)) +
			"&edit_yj_cd="                 + encodeURIComponent(encodeURIComponent(document.getElementById("edit_yj_cd").value));
		inquiryRegist(returnRegistEditor, "&mode="+command+param);
  }

	//==========================
	// エディタの表示命令の、サーバからのコールバック
	//==========================
	function returnShowEditor(){
		var ret = checkReceiveAjax();
		if (!ret) return;
		document.getElementById("search_div").style.display = 'none';
		document.getElementById("editor_div").style.display = '';
		document.getElementById("edit_med_name").value = ret.med_name;
		document.getElementById("edit_is_visible_at_selector").checked = parseInt(ret.is_visible_at_selector);
		document.getElementById("edit_used_counter").checked = parseInt(ret.used_counter);
		document.getElementById("edit_units_separated_tab").value = ret.units_separated_tab;
		document.getElementById("edit_remark").value = ret.remark;
		document.getElementById("edit_hot13_cd").value = ret.hot13_cd;
		document.getElementById("edit_yj_cd").value = ret.yj_cd;
		document.getElementById("edit_seq").value = ret.seq;
		ajaxObj = null;
	}

	//==========================
	// データの変更命令の、サーバからのコールバック
	//==========================
	function returnRegistEditor(){
		var ret = checkReceiveAjax();
		if (!ret) return;
		if (ret.status == 'hot_dup') {
			alert('HOTコードが重複しています。');
			return;
		}
		window.location.reload();
	}

	//==========================
	// エディタを閉じる
	//==========================
	function closeEditor(){
		document.getElementById('search_div').style.display = '';
		document.getElementById('editor_div').style.display = 'none';
	}

</script>





<?//==========================================================================?>
<?// 更新用フィールド                                                         ?>
<?//==========================================================================?>
<?=$font?>
<div id="editor_div" style="margin-top:4px; height:90px; background-color:#a4c0db; display:none">
	<input type="hidden" name="edit_seq" id="edit_seq" value="">
	<div style="background-color:#5279a5; height:20px">
		<div style="float:left; color:#fff; padding:3px">付加情報編集 / 基準番号割当</div>
		<div style="float:right; padding:3px">
			<a href="" onclick="closeEditor(); return false;" style="color:#fff">閉じる</span></a>
		</div>
		<br style="clear:both"/>
	</div>
	<div style="float:left; padding:4px;">
	<table class="list2">
		<tr>
			<th><?=$font?>薬剤名(販売名)</font></th>
			<th><?=$font?>採用薬</font></th>
			<th><?=$font?>選択可</font></th>
			<th><?=$font?>単位</font></th>
			<th><?=$font?>備考</font></th>
			<th><?=$font?>HOTコード</font></th>
			<th><?=$font?>YJコード</font></th>
		</tr>
		<tr>
			<td><input class="editbox" type="text" name="edit_med_name" id="edit_med_name" style="width:200px" value="" /></td>
			<td><input type="checkbox" name="edit_used_counter" id="edit_used_counter"></td>
			<td><input type="checkbox" name="edit_is_visible_at_selector" id="edit_is_visible_at_selector"></td>
			<td><input class="editbox" type="text" name="edit_units_separated_tab" id="edit_units_separated_tab" style="width:80px" value="" /></td>
			<td><input class="editbox" type="text" name="edit_remark" id="edit_remark" style="width:200px" value="" /></td>
			<td><input class="editbox" type="text" name="edit_hot13_cd" id="edit_hot13_cd" style="width:110px" value="" /></td>
			<td><input class="editbox" type="text" name="edit_yj_cd" id="edit_yj_cd" style="width:110px" value="" /></td>
		</tr>
	</table>
	</div>
	<div style="float:left; padding-top:12px; padding-left:10px"><input type="button" value="更新" onclick="registEditor('ajax_regist_editor')" /></div>
	<br style="clear:both"/>
</div>
</font>










<?//==========================================================================?>
<?// 検索条件                                                                 ?>
<?//==========================================================================?>
<?
	$page = max(1 ,(int)@$_REQUEST["summary_current_page"]);
	if (@$_REQUEST["rows_per_page"] != @$_REQUEST["rows_per_page_old"]) $page = 1;
  $rows_per_page = max(8, (int)@$_REQUEST["rows_per_page"]);


?>
<div><?=$font?>
<script type="text/javascript">
  function summary_page_change(page){
    document.getElementById("summary_current_page").value = page;
    document.frm.submit();
    return false;
  }
</script>
<form action="sum_ordsc_med_list.php" method="get" name="frm">
<div id="search_div" style="background-color:#e3ecf4; margin-top:4px; height:90px">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="search">
  <input type="hidden" name="summary_current_page" id="summary_current_page" value="<?=$page?>">
	<div style="padding:4px">
		<span style="padding-right:3px">基準番号</span>
		<input type="text" name="sel_hot13_cd" style="width:120px; ime-mode:disabled" value="<?=h(@$_REQUEST["sel_hot13_cd"])?>" />
		<span style="padding-right:3px; padding-left:10px">薬品名（販売名）</span>
		<input type="text" name="sel_med_name" style="width:200px" value="<?=h(@$_REQUEST["sel_med_name"])?>"/>
	</div>
	<div style="padding:4px">
		<span style="padding-right:3px">処方用番号</span>
		<input type="text" name="sel_hot7_cd" style="width:80px; ime-mode:disabled" value="<?=h(@$_REQUEST["sel_hot7_cd"])?>" />
		<span style="padding-right:3px; padding-left:10px">JANコード</span>
		<input type="text" name="sel_jan_cd" style="width:120px; ime-mode:disabled" value="<?=h(@$_REQUEST["sel_jan_cd"])?>" />
		<span style="padding-right:3px; padding-left:10px">薬価基準収蔵医薬品コード</span>
		<input type="text" name="sel_unitary_cd" style="width:120px; ime-mode:disabled" value="<?=h(@$_REQUEST["sel_unitary_cd"])?>" />
		<span style="padding-right:3px; padding-left:10px">個別医薬品コード</span>
		<input type="text" name="sel_yj_cd" style="width:120px; ime-mode:disabled" value="<?=h(@$_REQUEST["sel_yj_cd"])?>" />
	</div>
  <div style="padding:4px; text-align:right">
  	<label><input type="checkbox" name="sel_used_counter" value="1" <?=@$_REQUEST["sel_used_counter"]?"checked":""?> />非採用薬を隠す</label>
  	<label><input type="checkbox" name="sel_is_visible_at_selector" value="1" <?=@$_REQUEST["sel_is_visible_at_selector"]?"checked":""?> />選択不可薬品を隠す</label>
  	<label><input type="checkbox" name="show_detail" value="1" <?=@$_REQUEST["show_detail"]?"checked":""?> />詳細を表示</label>
		<span style="padding-left:10px">
			<input type="text" name="rows_per_page" value="<?=$rows_per_page?>" style="width:40px" />行ずつ
			<input type="hidden" name="rows_per_page_old" value="<?=$rows_per_page?>" />
			<input type="submit" value="検索"/>
		</span>
  </div>
</div>
</form>
</font></div>






<?//==========================================================================?>
<?// 一覧                                                                     ?>
<?//==========================================================================?>
<?
  $offset = ($page - 1) * $rows_per_page;

	if (@$_REQUEST["show_detail"]){
		$sql  = " select mst.*, sai.* from (";
	} else {
		$sql  = " select sai.* from (";
	}

	$sql .= "   select * from sum_med_saiyou_mst where 1=1";
	if (@$_REQUEST["sel_used_counter"]) $sql .= " and used_counter > 0";
	if (@$_REQUEST["sel_is_visible_at_selector"]) $sql .= " and is_visible_at_selector = 1";
	if (@$_REQUEST["sel_hot13_cd"])   $sql .= " and hot13_cd = '".pg_escape_string($_REQUEST["sel_hot13_cd"])."'";
	if (@$_REQUEST["sel_med_name"]) $sql .= " and  med_name like '%".pg_escape_string($_REQUEST["sel_med_name"])."%'";
	if (@$_REQUEST["sel_yj_cd"]) $sql .= " and yj_cd = '".pg_escape_string($_REQUEST["sel_yj_cd"])."'";
	$sql .= " ) sai";
	if (@$_REQUEST["show_detail"]){
		$sql .= " inner join (";
		$sql .= "   select * from sum_medmst where 1=1";
		if (@$_REQUEST["sel_hot7_cd"])    $sql .= " and hot7_cd = '".pg_escape_string($_REQUEST["sel_hot7_cd"])."'";
		if (@$_REQUEST["sel_jan_cd"])     $sql .= " and jan_cd = '".pg_escape_string($_REQUEST["sel_jan_cd"])."'";
		if (@$_REQUEST["sel_unitary_cd"]) $sql .= " and unitary_cd = '".pg_escape_string($_REQUEST["sel_unitary_cd"])."'";
		$sql .= " ) mst on (mst.hot13_cd = sai.hot13_cd)";
	}

	// 全レコード数の取得、ページの割り出し
	$sel = select_from_table($con, "select count(*) from (".$sql.") d", "", $fname);
	$total_record = pg_fetch_result($sel, 0, 0);
  $total_page_count = floor( ($total_record-1) / $rows_per_page ) + 1;

	$sql .= " order by seq, sai.hot13_cd offset ".$offset." limit ".$rows_per_page;
//	$sql .= " offset ".$offset." limit ".$rows_per_page;
	$sel = select_from_table($con, $sql, "", $fname);
?>



<?//==========================================================================?>
<?//ページャ(上)                                                              ?>
<?//==========================================================================?>
<table width="100%"><tr><td>
  <table><tr>
    <td style="white-space:nowrap"><?=$font?>全<?=$total_record?>行</font></td>
		<? if ($total_page_count > 1){ ?>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1){?><a href="" onclick="return summary_page_change(1);"><? } ?>先頭へ</a></span>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1) { ?><a href="" onclick="return summary_page_change(<?=$page-1?>);"><? } ?>前へ</a></span>
    </font></td>
    <td><?=$font?>
			<? for($i=1;$i<=$total_page_count;$i++) { ?>
				<? if ($page > $i && $page - $i > 10) continue; ?>
				<? if ($page < $i && $i - $page > 10) continue; ?>
				<? if($i != $page) { ?><a href="" onclick="return summary_page_change(<?=$i?>);">[<?=$i?>]</a><? }else{ ?>[<?=$i?>]<? } ?>
			<? } ?>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page<$total_page_count){ ?><a href="" onclick="return summary_page_change(<?=$page+1?>);"><? } ?>次へ</a></span>
    </font></td>
    <? } ?>
  </tr></table>
</td></tr></table>


<? if ($total_record > 0) { ?>
<div id="scroll_div" style="overflow-x:scroll; border:1px solid #aaa">
<table border="0" cellspacing="0" cellpadding="2" class="list1">
	<tr style="background-color:#f6f9ff">
		<th><?=$font?>薬剤名(販売名)</font></th>

		<th><?=$font?>採用薬</font></th>
		<th><?=$font?>選択可</font></th>
		<th><?=$font?>単位</font></th>
		<th><?=$font?>備考</font></th>

		<th><?=$font?>基準番号（ＨＯＴコード）</font></th>

		<? if (@$_REQUEST["show_detail"]){ ?>
		<th><?=$font?>処方用番号(HOT7)</font></th>
		<th><?=$font?>会社識別用番号</font></th>
		<th><?=$font?>調剤用番号</font></th>
		<th><?=$font?>物流用番号</font></th>
		<th><?=$font?>JANコード</font></th>
		<th><?=$font?>薬価基準収蔵医薬品コード</font></th>
		<th><?=$font?>個別医薬品コード(YJコード）</font></th>
		<th><?=$font?>レセプト電算処理システムコード（１）</font></th>
		<th><?=$font?>レセプト電算処理システムコード（２）</font></th>
		<? } ?>
		<th><?=$font?>告示名称</font></th>
		<? if (@$_REQUEST["show_detail"]){ ?>
		<th><?=$font?>販売名</font></th>
		<th><?=$font?>レセプト電算処理システム医薬品名</font></th>
		<th><?=$font?>規格単位</font></th>
		<th><?=$font?>包装形態</font></th>
		<th><?=$font?>包装単位数</font></th>
		<th><?=$font?>包装単位単位</font></th>
		<th><?=$font?>包装総量数</font></th>
		<th><?=$font?>包装総量単位</font></th>
		<th><?=$font?>区分</font></th>
		<th><?=$font?>製造会社</font></th>
		<th><?=$font?>販売会社</font></th>
		<th><?=$font?>更新区分</font></th>
		<th><?=$font?>更新年月日(yyyymmdd)</font></th>
		<th><?=$font?>剤形分類コード</font></th>
		<? } ?>
		<th><?=$font?>剤形分類</font></th>
		<? if (@$_REQUEST["show_detail"]){ ?>
		<th><?=$font?>成分分類コード</font></th>
		<? } ?>
		<th><?=$font?>成分分類</font></th>
		<th><?=$font?>薬効分類コード</font></th>
	</tr>
<? while($row = pg_fetch_array($sel)){ ?>
<?   $d = $row["op_time"]; ?>
<?   $ymdhms = substr($d,0,4)."/".substr($d,4,2)."/".substr($d,6,2)." ".substr($d,8,2).":".substr($d,10,2).":".substr($d,12,2); ?>
	<tr style="<?= (@$row["update_kubun"]=="4" ? "background-color:#999" : "")?>" >
		<td style="text-align:left"><?=$font?><a class="always_blue" href="" onclick="showEditor(<?=$row["seq"]?>); return false;"><?=$row["med_name"]?></a></font></td>
		<td><?=$font?><?=$row["used_counter"]?"●":""?></font></td>
		<td><?=$font?><?=$row["is_visible_at_selector"]?"●":""?></font></td>
		<td><?=$font?><?=$row["units_separated_tab"]?></font></td>
		<td><?=$font?><?=$row["remark"]?></font></td>

		<td><?=$font?><a class="always_blue" href="sum_ordsc_med_regist.php?session=<?=$session?>&seq=<?=$row["seq"]?>&mode=edit"><?=$row["hot13_cd"]?$row["hot13_cd"]:"(未割当)"?></a></font></td>
		<? if (@$_REQUEST["show_detail"]){ ?>
		<td><?=$font?><?=$row["hot7_cd"]?></font></td>
		<td><?=$font?><?=$row["company_cd"]?></font></td>
		<td><?=$font?><?=$row["cyouzai_cd"]?></font></td>
		<td><?=$font?><?=$row["buturyu_cd"]?></font></td>
		<td><?=$font?><?=$row["jan_cd"]?></font></td>
		<td><?=$font?><?=$row["unitary_cd"]?></font></td>
		<td><?=$font?><?=$row["hanbai_name"]?></font></td>
		<td><?=$font?><?=$row["yj_cd"]?></font></td>
		<td><?=$font?><?=$row["receipt1_cd"]?></font></td>
		<td><?=$font?><?=$row["receipt2_cd"]?></font></td>
		<? } ?>
		<td style="text-align:left"><?=$font?><?=$row["kokuji_name"]?></font></td>
		<? if (@$_REQUEST["show_detail"]){ ?>
		<td style="text-align:left"><?=$font?><?=$row["hanbai_name"]?></font></td>
		<td style="text-align:left"><?=$font?><?=$row["receipt_name"]?></font></td>
		<td style="text-align:left"><?=$font?><?=$row["kikaku_unit_name"]?></font></td>
		<td><?=$font?><?=$row["wrap_keitai"]?></font></td>
		<td><?=$font?><?=$row["wrap_unit_num"]?></font></td>
		<td><?=$font?><?=$row["wrap_unit_unit"]?></font></td>
		<td><?=$font?><?=$row["wrap_amount_num"]?></font></td>
		<td><?=$font?><?=$row["wrap_amount_unit"]?></font></td>
		<td><?=$font?><?=$row["kubun"]?></font></td>
		<td><?=$font?><?=$row["maker_name"]?></font></td>
		<td><?=$font?><?=$row["distributor_name"]?></font></td>
		<td><?=$font?><?=$row["update_kubun"]?></font></td>
		<td><?=$font?><?=$row["update_ymd"]?></font></td>
		<td><?=$font?><?=$row["zaikei_bunrui_cd"]?></font></td>
		<? } ?>
		<td><?=$font?><?=$row["zaikei_bunrui"]?></font></td>
		<? if (@$_REQUEST["show_detail"]){ ?>
		<td><?=$font?><?=$row["seibun_bunrui_cd"]?></font></td>
		<? } ?>
		<td><?=$font?><?=$row["seibun_bunrui"]?></font></td>
		<td><?=$font?><?=$row["yakkou_bunrui_cd"]?></font></td>
	</tr>
<? } ?>
</table>
</div>
<? } ?>


<?//==========================================================================?>
<?//ページャ(下)                                                              ?>
<?//==========================================================================?>
<? if ($total_page_count > 1){ ?>
<table width="100%"><tr><td>
  <table><tr>
    <td style="white-space:nowrap"><?=$font?>全<?=$total_record?>行</font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1){?><a href="" onclick="return summary_page_change(1);"><? } ?>先頭へ</a></span>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1) { ?><a href="" onclick="return summary_page_change(<?=$page-1?>);"><? } ?>前へ</a></span>
    </font></td>
    <td><?=$font?>
			<? for($i=1;$i<=$total_page_count;$i++) { ?>
				<? if ($page > $i && $page - $i > 10) continue; ?>
				<? if ($page < $i && $i - $page > 10) continue; ?>
				<? if($i != $page) { ?><a href="" onclick="return summary_page_change(<?=$i?>);">[<?=$i?>]</a><? }else{ ?>[<?=$i?>]<? } ?>
			<? } ?>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page<$total_page_count){ ?><a href="" onclick="return summary_page_change(<?=$page+1?>);"><? } ?>次へ</a></span>
    </font></td>
  </tr></table>
</td></tr></table>
<? } ?>


<script type="text/javascript">
	function adjustListWidth(){
		if (!document.getElementById("scroll_div")) return;
		document.getElementById("scroll_div").style.width =
			document.getElementById("header_menu_table").clientWidth - document.getElementById("left_sub_menu").clientWidth - 20;
	}
</script>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
