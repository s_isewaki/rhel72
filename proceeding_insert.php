<?
require_once("Cmx.php");
require_once("about_comedix.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="proceeding_register.php" method="post">
<input type="hidden" name="prcd_subject" value="<? echo($prcd_subject); ?>">
<input type="hidden" name="prcd_place_id" value="<? echo($prcd_place_id); ?>">
<input type="hidden" name="prcd_place_detail" value="<? echo($prcd_place_detail); ?>">
<input type="hidden" name="prcd_year" value="<? echo($prcd_year); ?>">
<input type="hidden" name="prcd_month" value="<? echo($prcd_month); ?>">
<input type="hidden" name="prcd_day" value="<? echo($prcd_day); ?>">
<input type="hidden" name="prcd_start_hour" value="<? echo($prcd_start_hour); ?>">
<input type="hidden" name="prcd_start_min" value="<? echo($prcd_start_min); ?>">
<input type="hidden" name="prcd_end_hour" value="<? echo($prcd_end_hour); ?>">
<input type="hidden" name="prcd_end_min" value="<? echo($prcd_end_min); ?>">
<input type="hidden" name="prcd_person" value="<? echo($prcd_person); ?>">
<input type="hidden" name="prcd_atnd" value="<? echo($prcd_atnd); ?>">
<input type="hidden" name="prcd_absentee" value="<? echo($prcd_absentee); ?>">
<input type="hidden" name="prcd_summary" value="<? echo(h($prcd_summary)); ?>">
<input type="hidden" name="aprv_cnt" value="<? echo($aprv_cnt); ?>">

<input type="hidden" name="ovtm_approve_num" value="<? echo($ovtm_approve_num); ?>">
<?
for ($i = 0; $i < $ovtm_approve_num; $i++) {
	$j = $i + 1;

	$str_emp_id = "ovtm_emp_id".$j;
	$str_emp_nm = "ovtm_emp_nm".$j;
	$str_content = "ovtm_approve_content".$j;
	$str_notice = "ovtm_next_notice_div".$j;
	$default_str_emp_id = "ovtm_emp_id_def".$j;

	echo("<input type=\"hidden\" name=\"ovtm_approve_content" . $j . "\" value=\"".$$str_content."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_id" . $j . "\" value=\"".$$str_emp_id."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_nm" . $j . "\" value=\"".$$str_emp_nm."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div" . $j . "\" value=\"".$$str_notice."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_id_def" . $j . "\" value=\"".$$default_str_emp_id."\">\n");
}
?>
<?


foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

// デフォルトで設定する承認者を更新
$arr_target1 = array();
if ($target_id_list1 != "") {
	$arr_target_id1 = explode(",", $target_id_list1);

	for ($i = 0; $i < count($arr_target_id1); $i++) {
		array_push($arr_target1, array("id" => $arr_target_id1[$i]));
	}

}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_schd_id" value="<? echo($pjt_schd_id); ?>">
<input type="hidden" name="timeless" value="<? echo($timeless); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
<input type="hidden" name="force_aprv_flag" value="<? echo($force_aprv_flag); ?>">
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
</form>
<?
// セッションのチェック
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($prcd_subject == "") {
	echo("<script type=\"text/javascript\">alert('議題を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($prcd_subject) > 200) {
	echo("<script type=\"text/javascript\">alert('議題が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($prcd_place_detail) > 100) {
	echo("<script type=\"text/javascript\">alert('場所（詳細）が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!checkdate($prcd_month, $prcd_day, $prcd_year)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ("$prcd_start_hour$prcd_start_min" >= "$prcd_end_hour$prcd_end_min") {
	echo("<script type=\"text/javascript\">alert('終了時刻は開始時刻より前にしてください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

for ($i = 1; $i <= (int) $ovtm_approve_num; $i++) {
    $str_chk_level = "ovtm_emp_id" . $i;
    if ($$str_chk_level == "") {
        echo("<script type=\"text/javascript\">alert('承認者が設定されていない階層があります。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}

// 添付ファイルの確認
if (!is_dir("proceeding")) {
	mkdir("proceeding", 0755);
}
if (!is_dir("proceeding/tmp")) {
	mkdir("proceeding/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "proceeding/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 議事録情報を作成

if ($draft_flg == "t") {
    //下書き保存のステータスを設定
    $prcd_status = "4";
}
else {
    //下書き保存でなく登録処理の際のステータス
    //1:申請中（未承認）　2:承認済（承認者なし）
    //$prcd_status = ($ch_ap > 0) ? "1" : "2";
    $prcd_status = ($ovtm_approve_num > 0) ? "1" : "2";
}

$prcd_summary = str_replace(array("\n", "\r"), "", $prcd_summary);
$sql = "insert into proceeding (pjt_schd_id, prcd_subject, prcd_date, prcd_start_time, prcd_end_time, prcd_place_id, prcd_place_detail, prcd_person, prcd_absentee, prcd_summary, prcd_create_time, prcd_create_emp_id, prcd_status) values (";
$content = array($pjt_schd_id, $prcd_subject, "$prcd_year$prcd_month$prcd_day", "$prcd_start_hour$prcd_start_min", "$prcd_end_hour$prcd_end_min", $prcd_place_id, $prcd_place_detail, $prcd_person, $prcd_absentee, $prcd_summary, date("YmdHis"), $emp_id, $prcd_status);
$ins = insert_into_table($con, $sql, q($content), $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 添付ファイル情報を作成
$no = 1;
foreach ($filename as $tmp_filename) {
	$sql = "insert into prcdfile (pjt_schd_id, prcdfile_no, prcdfile_name) values (";
	$content = array($pjt_schd_id, $no, pg_escape_string($tmp_filename));
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$no++;
}

//→承認者を登録する
for ($i = 1; $i <= $ovtm_approve_num; $i++) {

    $str_ap_emp_id = "ovtm_emp_id" . $i;
    $str_ap_notice_id = "ovtm_next_notice_div" . $i;


    //承認階層の種別を登録
    $sql = "insert into prcdaprv_mng (pjt_schd_id, prcdaprv_order, next_notice_div) values(";
    $content = array($pjt_schd_id, $i, $$str_ap_notice_id);
    $in = insert_into_table($con, $sql, $content, $fname);
    if ($in == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $arr_app_id = explode(",", $$str_ap_emp_id);
    $set_arr_app = array();

    for ($cnt = 0; $cnt < count($arr_app_id); $cnt++) {
        array_push($set_arr_app, array("prcdaprv_emp_id" => $arr_app_id[$cnt]));
    }

    //→承認者を登録する
    $no_cnt = 1;
    foreach ($set_arr_app as $row) {

        $tmp_deci_flg = "t";
        $sql = "insert into prcdaprv (pjt_schd_id, prcdaprv_no, prcdaprv_emp_id, prcdaprv_decide_flg, prcdaprv_order) values (";
        $content = array($pjt_schd_id, $no_cnt, $row["prcdaprv_emp_id"], $tmp_deci_flg, $i);

        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $no_cnt++;
    }
}

// 出席者情報を作成
if ($prcd_atnd != "") {
	$sql = "select pjt_id from proschd";
	$cond = "where pjt_schd_id = '$pjt_schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		$sql = "select pjt_id from proschd2";
		$cond = "where pjt_schd_id = '$pjt_schd_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	$pjt_id = pg_fetch_result($sel, 0, "pjt_id");

	$atnd_emp_ids = explode(",", $prcd_atnd);
	for ($i = 0, $j = count($atnd_emp_ids); $i < $j; $i++) {
		$sql = "insert into prcdatnd (pjt_schd_id, emp_id, pjt_id, order_no) values (";
		$content = array($pjt_schd_id, $atnd_emp_ids[$i], $pjt_id, $i + 1);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "proceeding/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "proceeding/{$pjt_schd_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("proceeding/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
?>
</body>
