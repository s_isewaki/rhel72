<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($system == "") {
	echo("<script type=\"text/javascript\">alert('制度が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($system) > 100) {
	echo("<script type=\"text/javascript\">alert('制度が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

for ($i = 1; $i <= 3; $i++) {
	if ($_FILES["file$i"]["name"] != "") {
		switch ($_FILES["file$i"]["error"]) {
		case 1:  // UPLOAD_ERR_INI_SIZE
		case 2:  // UPLOAD_ERR_FORM_SIZE
			echo("<script type=\"text/javascript\">alert('ファイルサイズが大きすぎます。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		case 3:  // UPLOAD_ERR_PARTIAL:
		case 4:  // UPLOAD_ERR_NO_FILE:
			echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}
}

// データベースに接続
$con = connect2db($fname);

// 文書情報を更新
$sql = "update welfare set";
$set = array("lifestage_id", "system", "description", "update_time");
$setvalue = array($stage_id, $system, $description, date("YmdHis"));
$cond = "where welfare_id = $welfare_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 必要に応じて既存ファイルを削除
if ($del1 == "t" || $_FILES["file1"]["name"] != "") {
	exec("rm -f " . getcwd() . "/intra/welfare/{$welfare_id}_1.*");
}
if ($del2 == "t" || $_FILES["file2"]["name"] != "") {
	exec("rm -f " . getcwd() . "/intra/welfare/{$welfare_id}_2.*");
}
if ($del3 == "t" || $_FILES["file3"]["name"] != "") {
	exec("rm -f " . getcwd() . "/intra/welfare/{$welfare_id}_3.*");
}

// アップロードされたファイルを保存
for ($i = 1; $i <= 3; $i++) {
	if ($_FILES["file$i"]["name"] != "") {
		copy($_FILES["file$i"]["tmp_name"], "intra/welfare/{$welfare_id}_$i" . strrchr($_FILES["file$i"]["name"], "."));
	}
}

// 自画面を閉じ、親画面をリフレッシュ
echo("<script type=\"text/javascript\">self.close(); opener.location.href = 'intra_life_welfare.php?session=$session&default_stage_id=$stage_id';</script>");
?>
