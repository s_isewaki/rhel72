<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//ポストバック時の処理
//==============================
if(! empty($is_postback))
{
    //TODO
}

//==============================
//情報取得
//==============================
$PAGE_TITLE = "確認コメント一覧";

//既に入力済みのコメントの一覧で上書き(※特殊ケース：昔、権限を持っていた場合はこれにヒット)
$sql = "select * from inci_report_progress_comment where report_id = $report_id and auth = '$auth' order by comment_date";
$sel = select_from_table($con,$sql,"",$fname);
if($sel == 0){
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$comment_info_arr = pg_fetch_all($sel);

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">
function init_page()
{


}



</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#35B341 solid 0px;}
table.block_in td td {border-width:1;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0" width="700">
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">

<?
if($comment_info_arr == "")
{
?>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">確認コメントはありません。</font>
<?
}
else
{
?>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr>
                    <td align="center" width="100" bgcolor="#DFFFDC" nowrap>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">入力日付</font>
                    </td>
                    <td align="center" width="100" bgcolor="#DFFFDC" nowrap>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">担当者</font>
                    </td>
                    <td align="center" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">コメント</font>
                    </td>
                </tr>
    <?
    foreach($comment_info_arr as $comment_info)
    {
        $comment_emp_id = $comment_info['emp_id'];
        $comment        = $comment_info['progress_comment'];
        $comment_date   = $comment_info['comment_date'];

        $comment_emp_name = get_emp_kanji_name($con,$comment_emp_id,$fname);

    ?>
                <tr>
                    <td align="center" bgcolor="#FFFFFF">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$comment_date ?></font>
                    </td>
                    <td align="center" bgcolor="#FFFFFF">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=h($comment_emp_name)?></font>
                    </td>
                    <td align="left" bgcolor="#FFFFFF">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=nl2br(h($comment))?></font>
                    </td>
                </tr>
    <?
    }
    ?>
            </table>
<?
}
?>

            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td align="right">
                        <input type="button" value="閉じる" onclick="javascript:window.close();">
                    </td>
                </tr>
            </table>


        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
</table>
<!-- 本体 END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?

//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

