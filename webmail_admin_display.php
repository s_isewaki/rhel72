<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ウェブメール | 表示設定</title>
<?
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("webmail_quota_functions.php");
require_once("webmail_alias_functions.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 66, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 環境設定情報を取得
$sql = "select webmail_download_link, webmail_message_id, webmail_destination_mygroup_myself, webmail_init_disp_address, use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$webmail_download_link = pg_fetch_result($sel, 0, "webmail_download_link");
$webmail_message_id = pg_fetch_result($sel, 0, "webmail_message_id");
$webmail_destination_mygroup_myself = pg_fetch_result($sel, 0, "webmail_destination_mygroup_myself");
$webmail_init_disp_address = pg_fetch_result($sel, 0, "webmail_init_disp_address");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

$conf = new Cmx_SystemConfig();
$webmail_use_secret_checkbox = ($conf->get('webmail.use_secret_checkbox') == "t");

$webmail_alias_enabled = (webmail_alias_enabled()) ? "t" : "f";

$webmail_default_quota = webmail_quota_get_default();
$webmail_use_quota = ($webmail_default_quota > 0) ? "t" : "f";
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setDefaultQuotaDisabled() {
	if (document.mainform.webmail_default_quota) {
		document.mainform.webmail_default_quota.disabled = document.mainform.webmail_use_quota[1].checked;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
	<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setDefaultQuotaDisabled();">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="webmail_menu.php?session=<? echo($session); ?>"><img src="img/icon/b01.gif" width="32" height="32" border="0" alt="ウェブメール"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="webmail_menu.php?session=<? echo($session); ?>"><b>ウェブメール</b></a> &gt; <a href="webmail_admin_menu.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><b>管理画面</b></a></font></td>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="webmail_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr height="22">
					<td width="150" align="center" bgcolor="#bdd1e7"><a href="webmail_admin_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インターネット接続</font></a></td>
					<td width="5"></td>
					<td width="150" align="center" bgcolor="#5279a5"><a href="webmail_admin_display.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション設定</b></font></a></td>
					<td width="5"></td>
					<td width="150" align="center" bgcolor="#bdd1e7"><a href="webmail_admin_shoot.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">トラブルシュート</font></a></td>
					<td width="">&nbsp;</td>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>

				<img src="img/spacer.gif" width="1" height="2" alt=""><br>
				<form name="mainform" action="webmail_admin_display_update.php" method="post">
				<table width="660" border="0" cellspacing="0" cellpadding="2">
				<tr height="22" valign="bottom">
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>メール本文</b></font></td>
				</tr>
			</table>

			<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
			<tr height="30">
				<td width="60%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「このファイルをダウンロード」リンクの表示</font></td>
				<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input name="webmail_download_link" type="radio" value="t"<? if ($webmail_download_link == "t") {echo(" checked");} ?>>する
				<input name="webmail_download_link" type="radio" value="f"<? if ($webmail_download_link == "f") {echo(" checked");} ?>>しない
				</font></td>
			</tr>
			</table>

			<table width="660" border="0" cellspacing="0" cellpadding="2">
				<tr height="22" valign="bottom">
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>メールヘッダ</b></font></td>
				</tr>
			</table>

			<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr height="30">
					<td width="60%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メッセージIDにクライアントのIPアドレスを含める</font></td>
					<td>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input name="webmail_message_id" type="radio" value="t"<? if ($webmail_message_id == "t") {echo(" checked");} ?>>含める
						<input name="webmail_message_id" type="radio" value="f"<? if ($webmail_message_id == "f") {echo(" checked");} ?>>含めない
					</font>
					</td>
				</tr>
			</table>

			<table width="660" border="0" cellspacing="0" cellpadding="2">
				<tr height="22" valign="bottom">
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>送信先</b></font></td>
				</tr>
			</table>

			<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr height="30">
					<td width="60%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">送信先でマイグループを選択する際、自分のアドレスも送信先に含める</font></td>
					<td>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input name="webmail_destination_mygroup_myself" type="radio" value="t"<? if ($webmail_destination_mygroup_myself == "t") {echo(" checked");} ?>>含める
						<input name="webmail_destination_mygroup_myself" type="radio" value="f"<? if ($webmail_destination_mygroup_myself == "f") {echo(" checked");} ?>>含めない
					</font>
					</td>
				</tr>
			</table>

			<table width="660" border="0" cellspacing="0" cellpadding="2">
				<tr height="22" valign="bottom">
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>メールアドレス表示</b></font></td>
				</tr>
			</table>

<?
global $_label_by_profile;
$profile_type = get_profile_type($con, $fname);
// 院内/法人内
$in_hospital_title = $_label_by_profile["IN_HOSPITAL"][$profile_type];
?>

			<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr height="30">
					<td width="60%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名簿検索画面メールアドレス表示設定</font></td>
					<td>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input name="webmail_init_disp_address" type="radio" value="t"<? if ($webmail_init_disp_address == "t") {echo(" checked");} ?>><? echo($in_hospital_title); ?>アドレス
						<input name="webmail_init_disp_address" type="radio" value="f"<? if ($webmail_init_disp_address == "f") {echo(" checked");} ?>>Emailアドレス
					</font>
					</td>
				</tr>
			</table>

<? if (webmail_quota_plugin_enabled()) { ?>
			<table width="660" border="0" cellspacing="0" cellpadding="2">
				<tr height="22" valign="bottom">
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>容量制限</b></font></td>
				</tr>
			</table>

			<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr height="30">
					<td width="60%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員のメールボックスの容量制限</font></td>
					<td>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input name="webmail_use_quota" type="radio" value="t"<? if ($webmail_use_quota == "t") {echo(" checked");} ?> onclick="setDefaultQuotaDisabled();">する
						<input name="webmail_use_quota" type="radio" value="f"<? if ($webmail_use_quota == "f") {echo(" checked");} ?> onclick="setDefaultQuotaDisabled();">しない<br>
						<input type="text" name="webmail_default_quota" value="<? echo($webmail_default_quota); ?>" size="4" style="ime-mode:disabled"> MB
					</font>
					</td>
				</tr>
			</table>
<? } ?>

			<table width="660" border="0" cellspacing="0" cellpadding="2">
				<tr height="22" valign="bottom">
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>その他</b></font></td>
				</tr>
			</table>

			<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr height="30">
					<td width="60%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">機密機能 <font size="2" class="j10" title="有効にすると、メッセージ作成画面に「機密」チェックボックスが表示されます。チェックすると、件名の先頭に「【機密】」が追加され、外部のメールアドレスに送信できなくなります。">(?)</font></font></td>
					<td>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input name="webmail_use_secret_checkbox" type="radio" value="t"<? if ($webmail_use_secret_checkbox == "t") {echo(" checked");} ?>>有効にする
						<input name="webmail_use_secret_checkbox" type="radio" value="f"<? if ($webmail_use_secret_checkbox != "t") {echo(" checked");} ?>>無効にする
					</font>
					</td>
				</tr>

<? if ($use_cyrus == "t" && webmail_alias_key_exists()) { ?>
				<tr height="30">
					<td width="60%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送機能 <font size="2" class="j10" title="有効にすると、ユーザ自身と管理者がメールの転送先を設定できるようになります。無効にすると、既存の転送設定がすべて削除されます。">(?)</font></font></td>
					<td>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input name="webmail_alias_enabled" type="radio" value="t"<? if ($webmail_alias_enabled == "t") {echo(" checked");} ?>>有効にする
						<input name="webmail_alias_enabled" type="radio" value="f"<? if ($webmail_alias_enabled != "t") {echo(" checked");} ?>>無効にする
					</font>
					</td>
				</tr>
<? } ?>

			</table>

			<table width="650" border="0" cellspacing="0" cellpadding="2">
				<tr height="30">
					<td align="right"><input type="submit" value="更新"></td>
				</tr>
			</table>

			<input type="hidden" name="session" value="<? echo($session); ?>">

				</form>
			</td>
			</tr>
		</table>
	</body>
<? pg_close($con); ?>
</html>
