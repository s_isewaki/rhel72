<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require_once("about_comedix.php");
require_once("about_validation.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,79,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 選択されたマスタをループ
foreach ($hospital_ids as $tmp_hospital_id)
{
	//病院を削除（論理削除）
	$sql=	"update fplus_hospital_master set";
	$set =	array("hospital_id","update_time","del_flg");
	$cond = "where hospital_id = $tmp_hospital_id";
	$setvalue = array("$tmp_hospital_id",date("Y-m-d H:i:s"),"t");
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 選択されている場合は選択解除
	if($tmp_hospital_id == $hospital_id)
	{
		$hospital_id = "";
	}
}

// トランザクションのコミット
pg_query($con, "commit");

pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'fplusadm_hospital_mst.php?session=$session&hospital_id=$hospital_id';</script>");

?>