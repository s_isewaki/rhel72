<?

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_cas_application_apply_history.ini");
require_once("show_cas_application_approve_detail.ini");
require_once("show_select_values.ini");
require_once("cas_application_imprint_common.ini");
require_once("cas_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

$imprint_flg = get_imprint_flg($con, $emp_id, $fname);

$imgsrc="img/approved.gif";
if ($imprint_flg == "t") {
	$imgsrc = "cas_application_imprint_image.php?session=".$session."&emp_id=".$emp_id."&apv_flg=1&imprint_flg=".$imprint_flg."&t=".date('YmdHis');
}

$dirname = "cas_workflow/imprint/";
$img_file = "img/approved.gif";
if ($imprint_flg == "t") {
	$img_file1 = "$dirname$emp_id.gif";
	$img_file2 = "$dirname$emp_id.jpg";
	// 当該職員の画像が登録済みか確認
	if (is_file($img_file1)) {
		$img_file = $img_file1;
	} else if (is_file($img_file2)) {
		$img_file = $img_file2;
	}
}
// 画像サイズ取得
list($w_frame1, $h_frame1) = get_imprint_imagesize($img_file);
list($w_frame2, $h_frame2) = get_imprint_imagesize("img/approve_ng.gif");
list($w_frame3, $h_frame3) = get_imprint_imagesize("img/returned.gif");

$cas_title = get_cas_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cas_title?> | 承認詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>


<?
// 履歴フラグ true:履歴なので登録ボタン等を無効化する
$history_flg = false;
if($target_apply_id == "") {
	$target_apply_id = $apply_id;
} else {
	// $target_apply_idが異なる場合は再申請された以前のデータ
	if ($target_apply_id != $apply_id) {
		$history_flg = true;
	}
}

// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
$short_wkfw_name   = $arr_apply_wkfwmst[0]["short_wkfw_name"];

/*
$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
$wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
$wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
$apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");
*/

// 本文形式タイプのデフォルトを「テキスト」とする
if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

// 形式をテキストからテンプレートに変更した場合の古いデータ対応
// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
if ($wkfw_content_type == "2") {
	if (strpos($apply_content, "<?xml") === false) {
		$wkfw_content_type = "1";
	}
}

if($wkfw_history_no != "")
{
	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
}

$num = 0;
if ($wkfw_content_type == "2") {
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}
}



// 異動チェック
$changes_flg = 0;	// 異動フラグ 0:なし 1:申請者所属異動 2:承認者所属異動 3:承認者役職異動
$apply_stat = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_stat");

$emp_class=pg_fetch_result($sel_apply_wkfwmst, 0, "apply_emp_class");
if ($emp_class != "") {
	$emp_attribute=pg_fetch_result($sel_apply_wkfwmst, 0, "apply_emp_attribute");
	$emp_dept=pg_fetch_result($sel_apply_wkfwmst, 0, "apply_emp_dept");
	$emp_room=pg_fetch_result($sel_apply_wkfwmst, 0, "apply_emp_room");

	$emp_mst_class=pg_fetch_result($sel_apply_wkfwmst, 0, "emp_cls");
	$emp_mst_attribute=pg_fetch_result($sel_apply_wkfwmst, 0, "emp_atr");
	$emp_mst_dept=pg_fetch_result($sel_apply_wkfwmst, 0, "emp_dpt");
	$emp_mst_room=pg_fetch_result($sel_apply_wkfwmst, 0, "emp_mst_room");

	if (($emp_class != $emp_mst_class) ||
		($emp_attribute != $emp_mst_attribute) ||
		($emp_dept != $emp_mst_dept) ||
		(($emp_room != $emp_mst_room &&
		  $emp_room != "0"))) {
		$changes_flg = 1;
	}

}


if ($changes_flg == 0) {
	// 承認者情報取得
	$sel_applyapv = search_applyapv($con, $fname, $target_apply_id);
	$approve_num = pg_numrows($sel_applyapv);    // 承認者数

	for($i=0; $i<$approve_num; $i++) {
		// 2012/04/09 Yamagawa upd(s)
		/*
		$apv_emp_id = pg_fetch_result($sel_applyapv, $i, "emp_id");
		if($emp_id == $apv_emp_id) {
			// 承認者所属異動チェック
			$apply_emp_class = pg_fetch_result($sel_applyapv, $i, "apply_emp_class");
			// 未設定時はチェックしない
			if ($apply_emp_class == "") {
				break;
			}
			$apply_emp_attribute = pg_fetch_result($sel_applyapv, $i, "apply_emp_attribute");
			$apply_emp_dept = pg_fetch_result($sel_applyapv, $i, "apply_emp_dept");
			$apply_emp_room = pg_fetch_result($sel_applyapv, $i, "apply_emp_room");
			$emp_cls = pg_fetch_result($sel_applyapv, $i, "emp_cls");
			$emp_atr = pg_fetch_result($sel_applyapv, $i, "emp_atr");
			$emp_dpt = pg_fetch_result($sel_applyapv, $i, "emp_dpt");
			$emp_mst_room = pg_fetch_result($sel_applyapv, $i, "emp_mst_room");

			if ($apply_emp_class != $emp_cls ||
				$apply_emp_attribute != $emp_atr ||
				$apply_emp_dept != $emp_dpt ||
				($apply_emp_room != $emp_mst_room &&
				 $apply_emp_room != "0")) {

				$changes_flg = 2;
			} else {
			// 承認者役職異動チェック
				$apply_emp_st = pg_fetch_result($sel_applyapv, $i, "apply_emp_st");
				$emp_stat = pg_fetch_result($sel_applyapv, $i, "emp_stat");
				if ($apply_emp_st != $emp_stat) {
					$changes_flg = 3;
				}
			}
			break;
		}
		*/
		$apv_emp_id = pg_fetch_result($sel_applyapv, $i, "emp_id");
		$apv_order = pg_fetch_result($sel_applyapv, $i, "apv_order");
		if($emp_id == $apv_emp_id && $apv_order_fix == $apv_order) {
			$st_div = pg_fetch_result($sel_applyapv, $i, "st_div");
			$class_id = pg_fetch_result($sel_applyapv, $i, "apply_emp_class");
			$atrb_id = pg_fetch_result($sel_applyapv, $i, "apply_emp_attribute");
			$dept_id = pg_fetch_result($sel_applyapv, $i, "apply_emp_dept");
			$room_id = pg_fetch_result($sel_applyapv, $i, "apply_emp_room");
			$st_id = pg_fetch_result($sel_applyapv, $i, "apply_emp_st");
			$parent_pjt_id = pg_fetch_result($sel_applyapv, $i, "parent_pjt_id");
			$child_pjt_id = pg_fetch_result($sel_applyapv, $i, "child_pjt_id");
			switch($st_div){
				case "0":
				case "4":
					// 所属先チェック
/*
					if (!$obj->check_applyapv_affiliation($emp_id, $class_id, $atrb_id, $dept_id, $room_id)) {
						$changes_flg = 2;
					}
*/
					if ($changes_flg == 0 && !$obj->check_applyapv_position($emp_id, $class_id, $atrb_id, $dept_id, $room_id, $st_id)) {
						$changes_flg = 3;
					}
					break;
				case "1":
				case "2":
					// 職員存在チェック
					if (!$obj->check_applyapv_staff($emp_id)) {
						$changes_flg = 4;
					}
					break;
				case "3":
					// 委員会・ＷＧ所属チェック
					if (!$obj->check_applyapv_committee($emp_id, $parent_pjt_id, $child_pjt_id)) {
						$changes_flg = 5;
					}
					break;
			}
		}
		// 2012/04/09 Yamagawa upd(e)
	}
}


// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();
?>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>

<?
// カレンダー作成、関数出力
write_yui_calendar_script2($num);
?>

<script type="text/javascript">

var apply1_pos;
var apply2_pos;
var apply3_pos;

function initPage() {
	YUI().use('dd-drag', 'dd-drop', 'dd-scroll', function(Y){
		if (document.getElementById('apply1')) {
			var dd1 = new Y.DD.Drag({node: '#apply1'}).plug(Y.Plugin.DDWinScroll);
			dd1.startPos = YAHOO.util.Dom.getXY("apply1");
			apply1_pos = dd1.startPos;
			dd1.on('drag:start', function(e){
				var el = document.getElementById('apply1');
				el.style.zIndex = 999;
			});
			dd1.on('drag:drophit', function(e){
				var el = document.getElementById('apply1');
				el.style.zIndex = 0;
				set_target('1');
			});
			dd1.on('drag:dropmiss', function(e){
				var el = document.getElementById('apply1');
				YAHOO.util.Dom.setXY(el, this.startPos);
				el.style.backgroundColor = '';
				el.style.zIndex = 0;
			});
		}

		if (document.getElementById('apply2')) {
			var dd2 = new Y.DD.Drag({node: '#apply2'}).plug(Y.Plugin.DDWinScroll);
			dd2.startPos = YAHOO.util.Dom.getXY("apply2");
			apply2_pos = dd2.startPos;
			dd2.on('drag:start', function(e){
				var el = document.getElementById('apply2');
				el.style.zIndex = 999;
			});
			dd2.on('drag:drophit', function(e){
				var el = document.getElementById('apply2');
				el.style.zIndex = 0;
				set_target('2');
			});
			dd2.on('drag:dropmiss', function(e){
				var el = document.getElementById('apply2');
				YAHOO.util.Dom.setXY(el, this.startPos);
				el.style.backgroundColor = '';
				el.style.zIndex = 0;
			});
		}

		if (document.getElementById('apply3')) {
			var dd3 = new Y.DD.Drag({node: '#apply3'}).plug(Y.Plugin.DDWinScroll);
			dd3.startPos = YAHOO.util.Dom.getXY("apply3");
			apply3_pos = dd3.startPos;
			dd3.on('drag:start', function(e){
				var el = document.getElementById('apply3');
				el.style.zIndex = 999;
			});
			dd3.on('drag:drophit', function(e){
				var el = document.getElementById('apply3');
				el.style.zIndex = 0;
				set_target('3');
			});
			dd3.on('drag:dropmiss', function(e){
				var el = document.getElementById('apply3');
				YAHOO.util.Dom.setXY(el, this.startPos);
				el.style.backgroundColor = '';
				el.style.zIndex = 0;
			});
		}

		if (document.getElementById('target1')) {
			ddtarget1 = new Y.DD.Drop({node: '#target1'});
		}
	});
}

function set_target(flg) {

// drag元を消す
	document.getElementById("apply"+flg).style.display = "none";
// 白い背景を表示
	document.getElementById("empty"+flg).style.display = "";

// ターゲットに画像を表示
	var imgsrc = "";
	if (flg == 1) {
		imgsrc = "<?=$imgsrc?>";
		document.getElementById("img1").width = <?=$w_frame1?>;
		document.getElementById("img1").height = <?=$h_frame1?>;
	} else if (flg == 2) {
		imgsrc = "img/approve_ng.gif";
		document.getElementById("img1").width = <?=$w_frame2?>;
		document.getElementById("img1").height = <?=$h_frame2?>;
	} else if (flg == 3) {
		imgsrc = "img/returned.gif";
		document.getElementById("img1").width = <?=$w_frame3?>;
		document.getElementById("img1").height = <?=$h_frame3?>;
	}
	document.getElementById("img1").src = imgsrc;
// 背景を白に
	document.getElementById("target1").style.backgroundColor = "#ffffff";
	document.getElementById("message1").style.backgroundColor = "#E5F6CD";
// プルダウンを設定
	document.apply.approve.value = flg;
	// 2012/04/09 Yamagawa add(s)
	document.apply.hdn_approve.value = flg;
	// 2012/04/09 Yamagawa add(e)

	document.apply.drag_flg.value = "t";

}

function reset_imprint()
{
	document.getElementById("apply1").style.display = "";
	document.getElementById("empty1").style.display = "none";
	YAHOO.util.Dom.setXY(document.getElementById("apply1"), apply1_pos);

	document.getElementById("apply2").style.display = "";
	document.getElementById("empty2").style.display = "none";
	YAHOO.util.Dom.setXY(document.getElementById("apply2"), apply2_pos);

	document.getElementById("apply3").style.display = "";
	document.getElementById("empty3").style.display = "none";
	YAHOO.util.Dom.setXY(document.getElementById("apply3"), apply3_pos);

	document.getElementById("target1").style.backgroundColor = "#ffcccc";
	document.getElementById("img1").src = "img/spacer.gif";
	document.getElementById("message1").style.backgroundColor = "#ffcccc";

	document.apply.drag_flg.value = "f";

}

// 2012/04/09 Yamagawa add(s)

function compel_remand(){

	document.getElementById("apply1").style.display = "none";
	document.getElementById("empty1").style.display = "";

	document.getElementById("apply2").style.display = "none";
	document.getElementById("empty2").style.display = "";

	document.getElementById("apply3").style.display = "none";
	document.getElementById("empty3").style.display = "";

	document.getElementById("target1").style.backgroundColor = "#ffffff";
	document.getElementById("img1").src = "img/returned.gif";
	document.getElementById("message1").style.backgroundColor = "#E5F6CD";
	document.apply.approve.value = "3";
	document.apply.hdn_approve.value = "3";
	document.apply.approve.disabled = true;
	document.apply.stat_reset.disabled = true;
	document.apply.apv_comment.readOnly = true;

	document.apply.drag_flg.value = "t";
}

// 2012/04/09 Yamagawa add(e)

function history_select(apply_id, target_apply_id) {

	document.apply.apply_id.value = apply_id;
	document.apply.target_apply_id.value = target_apply_id;
	document.apply.action="cas_application_approve_detail.php?session=<?=$session?>&apv_order_fix=<?=$apv_order_fix?>&apv_sub_order_fix=<?=$apv_sub_order_fix?>&send_apved_order_fix=<?=$send_apved_order_fix?>";
	document.apply.submit();
}

/**
 * 登録ボタン押下時の処理
 * @author 2011.12.09 Edit by matsuura
 */
function approve_regist() {

<?
// 評価申請レベル　No1,No3,No10 レベル　No2,No4,No11で2階層目の場合、差戻し、否認の不可メッセージ
if (in_array($short_wkfw_name, array("c201","c202","c207","c209","c210","c215","c218","c221")) &&
	$apv_order_fix == "2") {
?>
	if (document.apply.approve.value == "2" ||
		document.apply.approve.value == "3") {
		alert("この様式では、差戻しと否認を実行できません");
		return;
	}
<?
}
?>

    <?show_js_eval_approve($short_wkfw_name);?>
	if (confirm('登録します。よろしいですか？')) {
		// 2011.12.09 Edit by matsuura
		//document.apply.action="cas_application_approve_detail_regist.php?session=<?=$session?>&approve_mode=REGIST";
		document.apply.action="cas_application_approve_detail_regist.php";
		document.apply.approve_mode.value="REGIST";
		document.apply.submit();
	}
}

/**
 * 更新ボタン押下時の処理
 * @author 2011.12.09 Edit by matsuura
 */
function approve_update() {

    <?show_js_eval_approve($short_wkfw_name);?>
	if (confirm('更新します。よろしいですか？')) {
		// 2011.12.09 Edit by matsuura
//		document.apply.action="cas_application_approve_detail_regist.php?session=<?=$session?>&approve_mode=UPDATE";
		document.apply.action="cas_application_approve_detail_regist.php";
		document.apply.approve_mode.value="UPDATE";
		document.apply.submit();
	}

}

/**
 * 下書き保存ボタン押下時の処理
 * @author 2011.12.09 Edit by matsuura
 */
function approve_draft_regist() {

	if (confirm('下書き保存します。よろしいですか？')) {
		// 2011.12.09 Edit by matsuura
//		document.apply.action="cas_application_approve_detail_regist.php?session=<?=$session?>&approve_mode=DRAFT";
		document.apply.action="cas_application_approve_detail_regist.php";
		document.apply.approve_mode.value="DRAFT";
		document.apply.submit();
	}
}


// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;

        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

        // 履歴表示table高さ調整
        flg = resize_history_tbl();
        if(flg)
        {
            // 承認画像のY軸座標位置の再設定
            var apply1 = YAHOO.util.Dom.getXY("apply1");
            var empty1 = YAHOO.util.Dom.getXY("empty1");

            y_position = '';
            if(apply1 != false)
            {
                y_position = apply1[1];
            }
            else
            {
                y_position = empty1[1];
            }

            apply1_pos[1] = y_position;
            apply2_pos[1] = y_position;
            apply3_pos[1] = y_position;
        }
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}

// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    var flg = false;
    heigh1 = document.getElementById('history_tbl').style.height;
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
    heigh2 = document.getElementById('history_tbl').style.height;

    if(heigh1 != heigh2)
    {
        flg = true;
    }
    return flg
}



</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();if(window.cas_load){cas_load();};if(window.no_7_load){no_7_load();};
<?



$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];

// 承認画像イメージ表示・非表示
$approve_flg = false;
$arr_applyapv = $obj->get_applyapv($apply_id);
foreach($arr_applyapv as $applyapv)
{
	$apv_stat       = $applyapv["apv_stat"];
	$apv_order      = $applyapv["apv_order"];
	$apv_sub_order  = $applyapv["apv_sub_order"];

	if(($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix) && $apv_stat == 0)
	{
		$approve_flg = true;
		break;
	}
}

/*
$approve_flg = false;

$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
$apply_stat = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_stat");


// 承認者情報取得
$sel_applyapv = search_applyapv($con, $fname, $target_apply_id);
$approve_num = pg_numrows($sel_applyapv);    // 承認者数

for ($i=0; $i<$approve_num; $i++) {
	$apv_stat = pg_fetch_result($sel_applyapv, $i, "apv_stat");
	$apv_emp_id = pg_fetch_result($sel_applyapv, $i, "emp_id");

	if(($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix) && $apv_stat == 0)
	{
		$approve_flg = true;
		break;
	}
}
*/

// 異動がある場合は差戻し画像を設定
// 決定権者が未承認、印刷ではないこと、履歴ではないこと
if ($apply_stat != 1 && $apv_stat == 0 && $mode != "approve_print" && $history_flg == false) {
	if ($changes_flg != 0) {
		// 2012/04/09 Yamagawa upd(s)
		//echo("set_target('3');");
		echo("compel_remand();");
		// 2012/04/09 Yamagawa upd(e)
	}
}
?>
if(window.OnloadSub) { OnloadSub(); };resizeAllTextArea();resize_history_tbl();initPage();">
<form id="apply" name="apply" action="#" method="post">
<!-- 2012/01/06 Yamagawa upd(s) idを追加 -->
<input type="hidden" id="session" name="session" value="<? echo($session); ?>">
<input type="hidden" id="apply_id" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" id="target_apply_id" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" id="send_apved_order_fix" name="send_apved_order_fix" value="<? echo($send_apved_order_fix); ?>">
<input type="hidden" id="approve_mode" name="approve_mode" value="">
<!-- 2012/01/06 Yamagawa upd(e) -->

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>承認詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" width="25%">
<?
show_application_history_for_approve($con, $session, $fname, $apply_id, $target_apply_id, $approve_flg, $history_flg);
?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="75%">
<?
$mode = "";
show_application_approve_detail($con, $session, $fname, $target_apply_id, $mode, $apv_comment, $approve, $drag_flg, $changes_flg, $history_flg, $apv_order_fix, $apv_sub_order_fix, $send_apved_order_fix); ?>
</td>
</center>
</form>

<!-- 2012/01/06 Yamagawa upd(s) idを追加 -->
<form id="approve_print_form" name="approve_print_form" action="cas_application_approve_detail_print.php" method="post" target="approve_detail_print_window">
<input type="hidden" id="session" name="session" value="<?=$session?>">
<input type="hidden" id="fname" name="fname" value="<?=$fname?>">
<input type="hidden" id="target_apply_id" name="target_apply_id" value="<?=$target_apply_id?>">

<input type="hidden" id="back" name="back" value="t">
<input type="hidden" id="mode" name="mode" value="approve_print">
<input type="hidden" id="simple" name="simple" value="">

<input type="hidden" id="history_flg" name="history_flg" value="">
<input type="hidden" id="apv_order_fix" name="apv_order_fix" value="">
<input type="hidden" id="apv_sub_order_fix" name="apv_sub_order_fix" value="">
<input type="hidden" id="send_apved_order_fix" name="send_apved_order_fix" value="">

<input type="hidden" id="approve" name="approve" value="">
<input type="hidden" id="drag_flg" name="drag_flg" value="">

<input type="hidden" id="apv_print_comment" name="apv_print_comment" value="">
</form>

<form id="recognize_schedule_form" name="recognize_schedule_form" action="cas_recognize_schedule_regist.php" method="post" target="recognize_schedule_window">
<input type="hidden" id="session" name="session" value="<?=$session?>">
<input type="hidden" id="level" name="level" value="">
<input type="hidden" id="apply_id" name="apply_id" value="<?=$apply_id?>">
<!-- 2012/01/06 Yamagawa upd(e) -->
</form>


</body>
<script type="text/javascript">
<?
	if ($apply_stat != 1 && $apv_stat == 0 && $mode != "approve_print" && $history_flg == false) {
		if ($changes_flg == 1) {
			echo("alert('申請者の所属が変更されているため差戻ししてください。');");
		} else if ($changes_flg == 2) {
			echo("alert('所属が変更されているため差戻ししてください。');");
		} else if ($changes_flg == 3) {
			echo("alert('役職が変更されているため差戻ししてください。');");
// 2012/04/10 Yamagawa add(s)
		} else if ($changes_flg == 4) {
			echo("alert('承認者が削除されているため差戻ししてください。');");
		} else if ($changes_flg == 5) {
			echo("alert('承認者が委員会・ＷＧから離脱されているため差戻ししてください。');");
// 2012/04/10 Yamagawa add(e)
		}
	}
?>

</script>
</html>

<?
// 申請・ワークフロー情報取得
function search_apply_wkfwmst($con, $fname, $apply_id) {

	$sql = "select a.apply_stat, a.apply_date, a.apply_content, a.apply_title, a.re_apply_id, a.emp_class as apply_emp_class, a.emp_attribute as apply_emp_attribute, a.emp_dept as apply_emp_dept, a.emp_room as apply_emp_room, b.wkfw_id, b.wkfw_title, b.wkfw_start_date, b.wkfw_end_date, b.wkfw_appr, b.wkfw_content, b.wkfw_content_type, c.wkfw_nm, d.emp_lt_nm, d.emp_ft_nm, d.emp_class as emp_cls, d.emp_attribute as emp_atr, d.emp_dept as emp_dpt, d.emp_room as emp_mst_room ";
	$sql .= "from cas_apply a ";
	$sql .= "inner join cas_wkfwmst b on a.wkfw_id = b.wkfw_id ";
	$sql .= "inner join cas_wkfwcatemst c on b.wkfw_type = c.wkfw_type ";
	$sql .= "inner join empmst d on a.emp_id = d.emp_id";
	$cond = "where a.apply_id = $apply_id";

	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// 承認者情報取得
function search_applyapv($con, $fname, $apply_id) {

	$sql = "select a.apply_id, a.wkfw_id, a.apv_order, a.emp_id, a.apv_stat, a.apv_date, a.st_div, a.deci_flg, a.apv_comment, a.emp_class as apply_emp_class, a.emp_attribute as apply_emp_attribute, a.emp_dept as apply_emp_dept, a.emp_room as apply_emp_room, a.emp_st as apply_emp_st, b.emp_lt_nm, b.emp_ft_nm, b.emp_class as emp_cls, b.emp_attribute as emp_atr, b.emp_dept as emp_dpt, b.emp_room as emp_mst_room, b.emp_st as emp_stat, c.st_nm as apply_st_nm, d.st_nm as empmst_st_nm ";
	// 2012/04/10 Yamagawa add(s)
	$sql .= ",a.parent_pjt_id ,a.child_pjt_id ";
	// 2012/04/10 Yamagawa add(e)
	$sql .= "from ";
	$sql .= "cas_applyapv a ";
	$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
	$sql .= "left outer join stmst c on a.emp_st = c.st_id ";
	$sql .= "inner join stmst d on b.emp_st = d.st_id ";

	$cond = "where a.apply_id = $apply_id order by a.apv_order asc";

	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}
pg_close($con);
?>