<?
ob_start();

require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("show_address.ini");

$fname = $PHP_SELF;

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ���¤Υ����å�
$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// �ǡ����١�������³
$con = connect2db($fname);

// �����CSV�����Ǽ���
$csv = get_list_csv($con, $session, $search_shared_flg, $search_item_flg, $search_name, $admin_flg);

// �ǡ����١�����³���Ĥ���
pg_close($con);

// CSV�����
$file_name = "address.csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// �ؿ�
//------------------------------------------------------------------------------

// �����CSV�����Ǽ���
function get_list_csv($con, $session, $search_shared_flg, $search_item_flg, $search_name, $admin_flg) {

/* ����̾
*/
	$items = array(
		"name1",							// 0
		"name2",
		"name_kana1",
		"name_kana2",
		"company",
		"department1",						// 5
		"department2",
		"post1",
		"post2",
		"postcode1||'-'||postcode2",
		"prefectures",						// 10
		"address1",
		"address2",
		"address3",
		"tel1||'-'||tel2||'-'||tel3",
		"fax1||'-'||fax2||'-'||fax3",		// 15
		"mobile1||'-'||mobile2||'-'||mobile3",
		"email_pc",
		"email_mobile",
		"memo"								// 19
	);

	$titles = array(
		"���ʴ�����",				// 0
		"̾�ʴ�����",
		"���ʥ��ʡ�",
		"̾�ʥ��ʡ�",
		"���̾",
		"����̾��",					// 5
		"����̾��",
		"�򿦣�",
		"�򿦣�",
		"͹���ֹ�",
		"��ƻ�ܸ�̾",				// 10
		"���꣱",
		"���ꣲ",
		"���ꣳ",
		"����",
		"FAX",						// 15
		"���ӡ�PHS",
		"E-MAIL��PC��",
		"E-MAIL�ʷ��ӡ�",
		"����"
	);

	$item_num = count($items);

	// show_address.ini��SQLʸ����������
	$cond = get_sql_address($session, $search_shared_flg, "����", $admin_flg, $search_name, $search_item_flg, 0, 0, 2);

	$cond .= " order by name_kana1 asc, name_kana2 asc ";

	$sql = "select ";

	for ($i=0;$i<$item_num;$i++) {
		if ($i != 0) {
			$sql .= ", ";
		}
		$sql .= $items[$i];
	}

	$sql .= " from address a ";

	$sel = select_from_table($con,$sql,$cond,$fname);

	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$num = pg_numrows($sel);

	$title_flg = "t";

	$buf = "";
	if ($title_flg == "t") {
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$buf .= $titles[$j];
		}
		$buf .= "\r\n";
	}

	for($i=0;$i<$num;$i++){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$tmp = pg_result($sel,$i,$j);
			// ͹���ֹ�
			if ($j == 9 && strlen($tmp) <= 1) {
				$tmp = "";
			}
			// ��ƻ�ܸ�
			if ($j == 10) {
				$tmp = get_province($tmp);
			}
			// ��������̤���Ϥ�-����Ϥ��ʤ�
			if ($j >= 14 && $j <= 16) {
				$tmp_split = split("-", $tmp);
				if (strlen($tmp_split[0]) == 0 || strlen($tmp_split[1]) == 0 || strlen($tmp_split[2]) == 0) { 
					$tmp = "";
				}
			}
			// ���ͤ�����ԥ����ɤ���
			if ($j == 19) {
				$tmp = str_replace("\r","", $tmp);
				$tmp = str_replace("\n","", $tmp);
			}
			$buf .= $tmp;
		}
		$buf .= "\r\n";
	}

	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}

//**********��ƻ�ܸ�̾����**********
function get_province($province_id){

	$provinces = array("�̳�ƻ","�Ŀ���","��긩","�ܾ븩","���ĸ�","������","ʡ�縩","��븩","���ڸ�","���ϸ�","��̸�","���ո�","�����","�����","���㸩","�ٻ���","���","ʡ�温","������","Ĺ�","���츩"	,"�Ų���","���θ�","���Ÿ�","���츩","������","�����","ʼ�˸�","���ɸ�","�²λ���","Ļ�踩","�纬��","������","���縩","������","���縩","���","��ɲ��","���θ�","ʡ����","���츩","Ĺ�긩","���ܸ�","��ʬ��","�ܺ긩","�����縩","���츩");

	if ($province_id == "") {
		return "";
	}

	$i = intval($province_id);
	if ($i >= 0 && $i <= 46) {
		return $provinces[$i];
	} else {
		return "";
	}

}
?>
