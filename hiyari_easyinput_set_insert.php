<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

require_once("about_comedix.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("smarty_setting.ini");
require_once("hiyari_report_class.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
// セッション開始
//==============================
session_name("hyr_sid");
session_start();

//==============================
//ポストバック時の処理
//==============================
if( $postback != "")
{
    //==============================
    //様式名の必須チェック
    //==============================

    if($eis_name == "")
    {
        echo("<script language='javascript'>alert(\"様式名称を入力してください。\");</script>");
        echo("<script language='javascript'>history.back();</script>");
        pg_close($con);
        exit;
    }

    //==============================
    //様式名の重複チェック
    //==============================

    $sql = "select * from inci_easyinput_set where not del_flag and eis_name = '". pg_escape_string($eis_name) ."'";
    $sel_chk = select_from_table($con,$sql,"",$fname);
    if($sel_chk == 0)
    {
        pg_close($con);
        showErrorPage();
        exit;
    }
    $chk_count = pg_numrows($sel_chk);
    if($chk_count != 0)
    {
        echo("<script language='javascript'>alert(\"様式名称が重複しています。様式名称を変更して下さい。\");</script>");
        echo("<script language='javascript'>history.back();</script>");
        pg_close($con);
        exit;
    }

    //==============================
    //カテゴリ名の必須チェック
    //==============================
    if(isset($user_cate_name) && $user_cate_name == "")
    {
        echo("<script language='javascript'>alert(\"カテゴリ名称を入力してください。\");</script>");
        echo("<script language='javascript'>history.back();</script>");
        pg_close($con);
        exit;
    }

    //==============================
    //様式の更新(論理削除＆インサート)
    //==============================

    //トランザクション開始
    pg_query($con,"begin transaction");

    //様式情報の更新
    $rep_obj = new hiyari_report_class($con, $fname);
    if(count($patient_use_easy_item_code) > 0) {
        $use_grp_code[] = "210";
    }
    if(count($summary_use_item_grp_code) > 0) {
        $use_grp_code[] = "400";
    }
    if(count($asses_use_easy_item_code) > 0) {
        $use_grp_code[] = "140";
    }
    if(count($place_use_easy_item_code) > 0) {
        $use_grp_code[] = "110";
    }
    $get_eis_id = $rep_obj->set_report_flags($eis_name,$use_grp_code,$style_code,$must_grp_code,$user_cate_name);

    //患者プロフィール・項目使用および必須設定登録
    $rep_obj->set_report_flags_for_easy_item_code($get_eis_id, 210, $patient_use_easy_item_code);
    $rep_obj->set_item_must_for_easy_item_code($get_eis_id, 210, $patient_must_easy_item_code);
    $rep_obj->set_report_flags_for_easy_item_code($get_eis_id, 150, $asses_use_easy_item_code);
    $rep_obj->set_item_must_for_easy_item_code($get_eis_id, 150, $asses_must_easy_item_code);

    //発生場所詳細
    $rep_obj->set_report_flags_for_easy_item_code($get_eis_id, 110, $place_use_easy_item_code);

    //患者影響レベル 20140423 R.C
    $rep_obj->insert_inci_easyinput_other_disp($get_eis_id, 90, 10, $patient_level_names);
    
    $get_eis_no = $rep_obj->get_eis_no_from_eis_id($get_eis_id);

    //インシデントの概要の項目コード・項目使用および必須設定登録
    $rep_obj->set_eis_400_use($get_eis_id,$summary_use_item_grp_code);
    $rep_obj->set_eis_400_must($get_eis_id,$summary_must_item_grp_code);

    // デフォルト値
    $rep_obj->set_default_value($get_eis_id, $_POST);
    
    // 項目名の変更
    foreach ($_POST as $param_name => $value){
        if (starts_with($param_name, "name_")){
            $eis_column = substr($param_name, strlen("name_"));
            if (!empty($value)){
                set_item_title($con, $fname, $get_eis_id, $value, $eis_column);
            }
            continue;
        }
        if (starts_with($param_name, "letter_")){
            $eis_column = substr($param_name, strlen("letter_"));
            if (!empty($value)){
                set_item_title($con, $fname, $get_eis_id, $value, $eis_column);
            }
            continue;
        }
    }
    
    // 表題入力設定
    if($_POST['subject']!="") set_subject_mode($con, $fname, $get_eis_id, $_POST['subject']);

    // 今期のテーマを挿入
    $rep_obj->set_this_term_theme($get_eis_no, $_POST['this_term_theme']);

    // 今期のテーマを取得する
    $theme = $rep_obj->get_this_term_theme($get_eis_no);

    // ユーザー定義カテゴリ項目(グループ)の並び順設定
    set_group_order($con, $fname, $get_eis_id, $use_grp_code);
    
    //トランザクション終了
    pg_query($con, "commit");

    //==============================
    //更新画面に遷移
    //==============================
    echo("<script type='text/javascript'>location.href='hiyari_easyinput_set_update.php?session={$session}&eis_no={$get_eis_no}';</script>");
    exit;
}

//========================================
//カテゴリグループ項目情報の取得
//========================================
$rep_obj = new hiyari_report_class($con, $fname);
$cate_list = $rep_obj->get_report_category_list();

//========================================
//スマーティ設定
//========================================
$smarty = new Cmx_View();
$smarty->assign("session", $session);
$smarty->assign("fname", $fname);
$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("post_back_url", "hiyari_easyinput_set_insert.php");
$smarty->assign("hyr_sid", session_id());
$smarty->assign("cate_list", $cate_list);

//報告書デザイン：1=旧デザイン、2=新デザイン
//報告書スタイル：旧デザインの場合はスタイル1、新デザインの場合はスタイル2
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');
$smarty->assign("design_mode", $design_mode);
$smarty->assign("style_code", $design_mode);

//患者プロフィール情報
$smarty->assign("item_mst_patient", $rep_obj->get_inci_easyinput_item_mst(210));

//アセスメント・患者の状態情報 20090309
// 各項目の並び順は、並び順のフィールドを持っていないため、SQLでコントロールできない
// コード上で、明示的に順番の入れ替えを行っている
$item_mst_asses = $rep_obj->get_inci_easyinput_item_mst(150);
$item_mst_asses_buf = $item_mst_asses[1];
$item_mst_asses[1]  = $item_mst_asses[5];
$item_mst_asses[5]  = $item_mst_asses_buf;
$smarty->assign("item_mst_asses", $item_mst_asses);

//発生場所詳細
$smarty->assign("is_place_detail_use", false);

//様式データ情報
$smarty->assign("eis_no", "*");
$smarty->assign("eis_name", "");

$theme = empty($theme[0]) ? $_POST['this_term_theme'] : $theme[0];
$smarty->assign("theme", $theme);

//概要2010年版
$method = $rep_obj->get_all_super_item_2010();
$kind_2010=array();
$kind_item_2010=array();
$scene_2010=array();
$scene_item_2010=array();
$content_2010=array();
$content_item_2010=array();
foreach($method as $value) {
    // 種類
    $param = $rep_obj->get_kind_2010($value['super_item_id']);
    if ($param) $kind_2010[$value['super_item_id']] = $param;
    if (is_array($param)){
        foreach($param as $value2) {
            $item = $rep_obj->get_kind_item_2010($value2['item_id']);
            if ($item) $kind_item_2010[$value2['item_id']] = $item;
        }
    }

    // 発生場面
    $param = $rep_obj->get_scene_2010($value['super_item_id']);
    if ($param) $scene_2010[$value['super_item_id']] = $param;
    if (is_array($param)){
        foreach($param as $value2) {
            $item = $rep_obj->get_scene_item_2010($value2['item_id']);
            if ($item) $scene_item_2010[$value2['item_id']] = $item;
        }
    }

    // 事例の内容
    $param = $rep_obj->get_content_2010($value['super_item_id']);
    if ($param) $content_2010[$value['super_item_id']] = $param;
    if (is_array($param)){
        foreach($param as $value2) {
            $item = $rep_obj->get_content_item_2010($value2['item_id']);
            if ($item) $content_item_2010[$value2['item_id']] = $item;
        }
    }
}
$smarty->assign("method", $method);
$smarty->assign("kind_2010", $kind_2010);
$smarty->assign("kind_item_2010", $kind_item_2010);
$smarty->assign("scene_2010", $scene_2010);
$smarty->assign("scene_item_2010", $scene_item_2010);
$smarty->assign("content_2010", $content_2010);
$smarty->assign("content_item_2010", $content_item_2010);

//患者影響レベル
$level_infos = $rep_obj->get_inci_level_infos("",true);
$smarty->assign("level_infos", $level_infos);

//========================================
//スマーティ表示
//========================================
$smarty->display("hiyari_easyinput_create.tpl");

function set_item_title($con, $fname, $eis_id, $eis_title, $eis_column) {
    $sql = "insert into inci_item_title (eis_id, eis_title, eis_column) values(";
    $content = array(pg_escape_string($eis_id), pg_escape_string($eis_title), $eis_column);
    $result = insert_into_table($con, $sql, $content, $fname);
    if ($result == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }
}

function set_subject_mode($con, $fname, $eis_id, $mode) {
    $sql     = "insert into inci_subject_mode (eis_id, subject_mode) values (";
    $content = array(pg_escape_string($eis_id), pg_escape_string($mode));
    $result  = insert_into_table($con, $sql, $content, $fname);
    if ($result == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }
}

function set_number_of_charcter($con, $fname, $eis_id, $group_code,$number_of_characters) {
    $sql = "insert into inci_item_number_of_character (eis_id, group_code, number_of_characters) values(";
    $content = array(pg_escape_string($eis_id), pg_escape_string($group_code), pg_escape_string($number_of_characters));
    $result = insert_into_table($con, $sql, $content, $fname);
    if ($result == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }
}

function set_group_order($con, $fname, $eis_id, $use_grp_code){
    $order_no = 1;
    foreach ($use_grp_code as $grp_code) {
        if ($grp_code >= 10000){
            $sql = "insert into inci_easyinput_set_group_order (eis_id, grp_code, order_no) values (";
            $content = array($eis_id, $grp_code, $order_no);
            $result = insert_into_table($con, $sql, $content, $fname);
            if ($result == 0) {
                pg_exec($con, "rollback");
                pg_close($con);
                showErrorPage();
                exit;
            }
            $order_no++;
        }
    }
}

function starts_with($str, $prefix){
    return (substr($str,  0, strlen($prefix)) === $prefix);
}