<?php
$fname = $_SERVER["PHP_SELF"];

require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_schedule_to_do.ini");
require_once("time_check.ini");
require_once("project_check.ini");
require_once("show_date_navigation_year.ini");
require_once("show_class_name.ini");
require_once("show_schedule_chart.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

if(!isset($_REQUEST['date'])){
    $date = mktime(0,0,0,date('m'), date('d'), date('Y'));
} else {
    $date = $_REQUEST['date'];
}

$year =$_REQUEST['year'];

$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// 「前年」「翌年」のタイムスタンプを取得
$last_year = get_last_year($date);
$next_year = get_next_year($date);

if ($hr == "") {
    $hr = "9";
}

// オプション設定情報を取得
$sql = "SELECT * FROM option WHERE emp_id='{$emp_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 年</title>
<script type="text/javascript">
function changeDate(dt) {
    var arr_dt = dt.split(',');
    location.href = 'schedule_year_menu.php?session=<?php eh($session); ?>&year=' + arr_dt[0] + '&date=' + arr_dt[1];
}
// CSVダウンロード用
function downloadCSV(){
    document.csv.action = 'schedule_csv.php';
    document.csv.submit();

}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#5279a5"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>年</b></font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール検索</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー連携</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh(date("Y",$last_year)); ?>&date=<?php eh($last_year); ?>">&lt;前年</a>
&nbsp;
<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_y($last_year, $date, $next_year); ?></select>
&nbsp;
<a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh(date("Y",$next_year)); ?>&date=<?php eh($next_year); ?>">翌年&gt;</a>
</font>
</td>
</tr>
</table>

<table width="750" border="0" cellspacing="0" cellpadding="0">
<?php
$day="01";
$month="01";
$set_date=mktime(0,0,0,$month,$day,$year);

// 年間の日ごとの予定件数を取得
$arr_schedule_count = get_arr_schedule_count($con, $fname, $emp_id, $year);

$tp=0;

foreach (range(1,4) as $p) {
    $tp=$tp+1;
    echo('<tr valign="top">');

    foreach (range(1,3) as $z) {

        // 月初のepoch time
        $mm = mktime(0, 0, 0, $month, 1, $year);

        // 月名
        $month_name = date('m',$mm);

        // 日数
        $num_days_current = cal_days_in_month(0,$month,$year);

        // 週数
        $start_week = ($calendar_start == 1) ? 0 : 1;
        $end_week   = ($calendar_start == 1) ? 6 : 0;
?>
<td style="padding:5px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:3px;border:1px solid #5279a5;">
    <tr>
        <td height="22" colspan="7" bgcolor="f6f9ff">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php eh($year);?>/<?php eh($month_name);?></font>
        </td>
    </tr>
    <tr>
<?php if ($calendar_start == 1) {?>
        <td align="center" bgcolor="#fadede"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></td>
<?php }?>
        <td align="center" bgcolor="#bdd1e7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></td>
        <td align="center" bgcolor="#bdd1e7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">火</font></td>
        <td align="center" bgcolor="#bdd1e7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">水</font></td>
        <td align="center" bgcolor="#bdd1e7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">木</font></td>
        <td align="center" bgcolor="#bdd1e7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">金</font></td>
        <td align="center" bgcolor="#defafa"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土</font></td>
<?php if ($calendar_start == 2) {?>
        <td align="center" bgcolor="#fadede"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></td>
<?php }?>
    </tr>
<?php
        foreach (range(1,$num_days_current) as $day) {
            // 曜日
            $week = date('w', mktime(0, 0, 0, $month, $day, $year));

            // 週初めの曜日
            if ($week == $start_week) {
                echo('<tr>');
            }

            // 1日
            if ($day == 1) {
                if ($week != $start_week) {
                    echo('<tr>');
                }
                $repeat = ($calendar_start == 1) ? $week : ($week + 6) % 7;
                echo str_repeat('<td>&nbsp;</td>', $repeat);
            }

            // 日付表示
            $schedule_count = $arr_schedule_count[sprintf('%04d-%02d-%02d',$year,$month,$day)];
            $bgcolor = ($schedule_count > 0) ? "#fbdade" : "#ffffff";
            $day_link = sprintf('<a href="schedule_menu.php?session=%s&date=%s">%s</a>', $session, mktime(0,0,0,$month,$day,$year), $day);
            printf('<td align="center" bgcolor="%s"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">%s</font></td>', $bgcolor, $day_link);


            // 月末
            if ($day == $num_days_current) {
                if ($week != $end_week) {
                    $repeat = ($calendar_start == 1) ? $week : ($week + 6) % 7;
                    echo str_repeat('<td>&nbsp;</td>', 6 - $week);
                }
            }

            // 週終わりの曜日
            if ($week == $end_week) {
                echo('</tr>');
            }
        }
        $month++;
        echo("</table></td>");
    }
    echo("</tr>\n");
}
?>
</table>
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td>
</tr>
</table>
<!-- CSV出力用form -->
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo($session); ?>">
    <input type="hidden" name="start_date" value="<?php echo $year.'01'.'01' ?>">
    <input type="hidden" name="end_date" value="<?php echo $year.'12'.'31' ?>">
</form>

</body>
<? pg_close($con); ?>
</html>
<?
// 年間の日ごとの予定件数をまとめて取得する
function get_arr_schedule_count($con, $fname, $emp_id, $year) {

    // 日付範囲を設定
    $start_day = $year."-01-01";
    $end_day = $year."-12-31";

    $sql = "select count(*) as cnt, schd_start_date from schdmst";
    $cond = "where emp_id='$emp_id' and schd_start_date >= '$start_day' and schd_start_date <= '$end_day' group by schd_start_date";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_schedule_count = array();
    while ($row = pg_fetch_assoc($sel)) {
        $arr_schedule_count[$row["schd_start_date"]] = $row["cnt"];
    }

    $sql = "select count(*) as cnt, schd_start_date from schdmst2";
    $cond = "where emp_id='$emp_id' and schd_start_date >= '$start_day' and schd_start_date <= '$end_day' group by schd_start_date";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_assoc($sel)) {
        $arr_schedule_count[$row["schd_start_date"]] += $row["cnt"];
    }
    return $arr_schedule_count;
}
