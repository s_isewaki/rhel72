<?
//ini_set("display_errors","1");
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require_once("duty_shift_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


$staff_id = split(",", $staff_ids);
$arr_emp_id1 = split(",", $emp_ids1);
$arr_emp_id2 = split(",", $emp_ids2);
// 入力チェック
$wk_str = ($adjust_time_legal_hol_flg != "2") ? "調整時間" : "公休数";
for ($i=0; $i<count($arr_emp_id1); $i++) {
	$j = $i + 1;
	$var_name = "adj_".$j;
	if ($$var_name != "") {
		if (preg_match("/^-?\d$|^-?\d+\.?\d+$/", $$var_name) == 0) {
			$emp_name = get_emp_kanji_name($con, $arr_emp_id1[$i], $fname);
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('{$emp_name}さんの{$wk_str}前繰越を半角数字で入力してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}
}


// トランザクションを開始
pg_query($con, "begin transaction");

$emp_id_str = "";
for ($i=0; $i<count($staff_id); $i++) {
	if ($i>0) {
		$emp_id_str .= ",";
	}
	$emp_id_str .= "'$staff_id[$i]'";
}
$sql = "select * from duty_shift_total_data ";
//$sql .= " left join empcond on empcond.emp_id = emppaid.emp_id ";
$cond = "where group_id = '$group_id' and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm and emp_id in ($emp_id_str)";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_days = array();
$num = pg_num_rows($sel);
for ($i=0; $i<$num; $i++) {
	$emp_id = pg_fetch_result($sel, $i, "emp_id");
	$arr_days["$emp_id"]["adjust_time"] = pg_fetch_result($sel, $i, "adjust_time");
	$arr_days["$emp_id"]["adjust_time_next"] = pg_fetch_result($sel, $i, "adjust_time_next");
	$arr_days["$emp_id"]["memo"] = pg_fetch_result($sel, $i, "memo");
}

for ($i=0; $i<count($arr_emp_id1); $i++) {
	
	$j = $i + 1;
	
	$var_name1 = "adj_".$j;
	$var_name2 = "adjnext_".$j;
	$var_name3 = "memo1_".$j;
	$wk_emp_id = $arr_emp_id1[$i];
	if ($$var_name1 != $arr_days["$wk_emp_id"]["adjust_time"] ||
			$$var_name2 != $arr_days["$wk_emp_id"]["adjust_time_next"] ||
			$$var_name3 != $arr_days["$wk_emp_id"]["memo"]) {
		//削除
		$sql = "delete from duty_shift_total_data";
		$cond = "where group_id = '$group_id' and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm and emp_id = '$wk_emp_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		//追加
		if ($$var_name1 != "" || $$var_name2 != "" || $$var_name3 != "") {
			$sql = "insert into duty_shift_total_data (group_id, duty_yyyy, duty_mm, emp_id, adjust_time, adjust_time_next, memo) values (";
			$adj1 = $$var_name1;
            if ($adj1 != "") { //null以外で00を0とする
                if ($adj1 == 0) {
                    $adj1 = "0";
                }
            }
			$adj2 = $$var_name2;
			$memo = $$var_name3;
			$content = array($group_id, $duty_yyyy, $duty_mm, $wk_emp_id, $adj1, $adj2, $memo);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}
	}
}

//非常勤
for ($i=0; $i<count($arr_emp_id2); $i++) {
	
	$j = $i + 1;
	
	$var_name1 = "memo2_".$j;
	$wk_emp_id = $arr_emp_id2[$i];
	if ($$var_name1 != $arr_days["$wk_emp_id"]["memo"]) {
		//削除
		$sql = "delete from duty_shift_total_data";
		$cond = "where group_id = '$group_id' and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm and emp_id = '$wk_emp_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		//追加
		if ($$var_name1 != "") {
			$sql = "insert into duty_shift_total_data (group_id, duty_yyyy, duty_mm, emp_id, memo) values (";
			$memo = $$var_name1;
			$content = array($group_id, $duty_yyyy, $duty_mm, $wk_emp_id, $memo);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'duty_shift_total.php?session=$session&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&group_id=$group_id&pattern_id=$pattern_id&staff_ids=$staff_ids';</script>");

?>