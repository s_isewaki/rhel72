<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>スケジュール | 種別</title>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<? require_once("Cmx.php"); ?>
<? require_once("aclg_set.php"); ?>

<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);
if ($pjt_admin_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

if($update_act == "up")
{
	//更新処理を実行
	$arr_name2 = array();
	foreach ($_POST as $key => $val) {
		if (preg_match("/^proschd(\d*)$/", $key, $matches)) 
		{
			$tmp_id = $matches[1];
			if ($val == "") 
			{
				echo("<script type=\"text/javascript\">alert('種別{$tmp_id}は必ず入力して下さい。');</script>");
				echo("<script type=\"text/javascript\">history.back();</script>");
				exit;
			}
			
			if (strlen($val) > 14) {
				echo("<script type=\"text/javascript\">alert('種別{$tmp_id}が長すぎます。');</script>");
				echo("<script type=\"text/javascript\">history.back();</script>");
				exit;
			}
			$arr_name2[$tmp_id] = $val;
		}
	}
	
	// 種別レコードを更新（委員会・WGスケジュール）
	foreach ($arr_name2 as $tmp_id => $tmp_name) 
	{
		$sql = "update proschdtype set";
		$set = array("type_name");
		$setvalue = array($tmp_name);
		$cond = "where type_id = '$tmp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) 
		{
			pg_query($con, "rollback");
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
	
	// トランザクションをコミット
	pg_query($con, "commit");
	
	
}



// 種別名称を取得（委員会・WGスケジュール分）
$sql = "select type_id, type_name from proschdtype";
$cond = "order by to_number(type_id, '99')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) 
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_name2 = array();
while ($row = pg_fetch_array($sel)) 
{
	$arr_name2[$row["type_id"]] = $row["type_name"];
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function onAction() 
{
	//登録用のフラグを設定する。
	document.prcd.update_act.value = "up";
	document.prcd.submit();
	
}

</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#5279a5 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<form name="prcd" action="project_schedule_type_update.php" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>
<? if ($pjt_admin_auth == "1") { ?>
<td width="85%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
<? }else{ ?>
<td width="100%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_wg_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WG登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_member_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >委員会・WG名簿</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>種別</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_bulk_set.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>





<table border="0" cellspacing="0" cellpadding="1"><tr valign="top"><td>　</td></tr></table>


<table width="300" cellspacing="0" cellpadding="1">
	
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list1" bgcolor="FFF1E3">
<?
$color_no = 8;
foreach ($arr_name2 as $tmp_id => $tmp_name) {
?>
				<tr height="22" valign="bottom">
					<td width="100" align="right">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別<? echo($tmp_id); ?>&nbsp;<img src="./img/color_<? echo($color_no); ?>.gif" width="10" height="17" style="border:black solid 1px;">：</font>
					</td>
					<td>
						<? 
							if ($tmp_id == "1") 
							{
								echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_name</font>");
							} 
							else 
							{ 
						?>
								<input type="text" name="proschd<? echo($tmp_id); ?>" value="<? echo($tmp_name); ?>" size="20" maxlength="14" style="ime-mode:active;">
						<? 
							} 
						?>
					</td>
				</tr>
<?
	$color_no++;
}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="button" value="更新" onclick="onAction()"></td>
	</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="update_act" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
