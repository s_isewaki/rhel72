<?php
//ini_set( 'display_errors', 1 );
require_once("cl_common_log_class.inc");
require_once("cl_common_apply.inc");
require_once(dirname(__FILE__) . "/Cmx.php");
require_once('MDB2.php');

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cl_application_apply_detail.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

/*
for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "approve_name$i";
	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
}
*/

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

</form>

<?
$fname=$PHP_SELF;
$log->debug('$fname:'.$fname,__FILE__,__LINE__);

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");

//------------------------------------------------------------------------------
// セッションのチェック
//------------------------------------------------------------------------------
$log->debug('セッションのチェック開始',__FILE__,__LINE__);
$session = qualify_session($session, $fname);
if ($session == "0") {
	$log->error('セッションのチェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$log->debug('セッションのチェック終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 決裁・申請権限のチェック
//------------------------------------------------------------------------------
$log->debug('決裁・申請権限のチェック開始',__FILE__,__LINE__);
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	$log->error('決裁・申請権限のチェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$log->debug('決裁・申請権限のチェック終了',__FILE__,__LINE__);


//------------------------------------------------------------------------------
// 入力チェック
//------------------------------------------------------------------------------
$log->debug('入力チェック開始',__FILE__,__LINE__);
if (strlen($apply_title) > 80) {
	$log->error('入力チェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$log->debug('入力チェック終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 添付ファイルの確認
//------------------------------------------------------------------------------
if (!is_dir("cl/apply")) {
	mkdir("cl/apply", 0755);
}
if (!is_dir("cl/apply/tmp")) {
	mkdir("cl/apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "cl/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		$log->warn('添付ファイル削除警告',__FILE__,__LINE__);
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(con)　開始',__FILE__,__LINE__);
$con = connect2db($fname);
$log->debug('データベースに接続(con)　終了',__FILE__,__LINE__);

$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);





$obj = new cl_application_workflow_common_class($con, $fname);

//------------------------------------------------------------------------------
// 職員情報取得
//------------------------------------------------------------------------------
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];
$emp_class     = $arr_empmst[0]["emp_class"];
$emp_attribute = $arr_empmst[0]["emp_attribute"];
$emp_dept      = $arr_empmst[0]["emp_dept"];
$emp_room      = $arr_empmst[0]["emp_room"];

// ログインユーザの職員IDを保持
$login_emp_id = $emp_id;

//------------------------------------------------------------------------------
// トランザクションを開始
//------------------------------------------------------------------------------
$log->debug("トランザクション開始(con) START",__FILE__,__LINE__);
pg_query($con, "begin");
$log->debug("トランザクション開始(con) END",__FILE__,__LINE__);

$log->debug("トランザクション開始(mdb2) START",__FILE__,__LINE__);
$mdb2->beginTransaction();
$log->debug("トランザクション開始(mdb2) END",__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 承認状況チェック
//------------------------------------------------------------------------------
$log->debug('承認状況チェック開始',__FILE__,__LINE__);
$apv_cnt = $obj->get_applyapv_cnt($apply_id);
if($apv_cnt > 0) {
	$log->error("承認状況チェックエラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script language=\"javascript\">alert('他の承認状況が変更されたため、更新できません。');</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
}
$log->debug('承認状況チェック終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 申請更新
//------------------------------------------------------------------------------
$log->debug('申請更新　開始',__FILE__,__LINE__);
//$obj->update_apply($apply_id, $content, $apply_title);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_apply_model.php");
$cl_apply_model = new cl_apply_model($mdb2,$login_emp_id);;
$cl_apply_model->apply_update($apply_title,$apply_id);

$log->debug('申請更新　終了',__FILE__,__LINE__);


//------------------------------------------------------------------------------
// 承認削除・登録
//------------------------------------------------------------------------------
//$obj->delete_applyapv($apply_id);
$log->debug('承認者削除　開始',__FILE__,__LINE__);
$log->debug('$emp_id:'.$emp_id,__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyapv_model.php");
$cl_applyapv_model = new cl_applyapv_model($mdb2,$login_emp_id);;
$cl_applyapv_model->physical_delete($apply_id);
$log->debug('承認者削除　終了',__FILE__,__LINE__);

$log->debug('承認者登録　開始',__FILE__,__LINE__);
$log->debug('approve_num：'.$approve_num,__FILE__,__LINE__);
$apvteacher_num = $_POST['apvteacher_num'];
// 2012/07/19 Yamagawa add(s)
$council_num = $_POST['council_num'];
// 2012/07/19 Yamagawa add(e)
for($i=1; $i<=$approve_num; $i++)
{

	$varname = "apv_order$i";
	$apv_order = ($$varname == "") ? null : $$varname;

	if ($apv_order_key != $apv_order){
		$apv_sub_order_wk = 1;
		$apv_order_key = $apv_order;
	}

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;
	//if ($st_div == 6 && $multi_apv_flg == "t"){
	if ($st_div == 6 && $multi_apv_flg == "t" && $apvteacher_num > 1){
		$varname = "check_emp_id".$i."_";
		$arr_field_name = array_keys($_POST);
		foreach($arr_field_name as $field_name){
			$pos = strpos($field_name,$varname);
			if ($pos === 0){
				$apv_emp_id = $_POST[$field_name];
				// 所属、役職も登録する
				$infos = get_empmst($con, $apv_emp_id, $fname);

				$emp_class     = $infos[2];
				$emp_attribute = $infos[3];
				$emp_dept      = $infos[4];
				$emp_st        = $infos[6];
				$emp_room      = $infos[33];

				$arr = array(
					"wkfw_id"			 =>	$wkfw_id,
					"apply_id"			 =>	$apply_id,
					"apv_order"			 =>	$apv_order,
					"emp_id"			 =>	$apv_emp_id,
					"apv_stat"			 =>	"0",
					"apv_date"			 =>	"",
					"apv_comment"		 =>	"",
					"st_div"			 =>	$st_div,
					"deci_flg"			 =>	"t",
					"emp_class"			 =>	$emp_class,
					"emp_attribute"		 =>	$emp_attribute,
					"emp_dept"			 =>	$emp_dept,
					"emp_st"			 =>	$emp_st,
					"apv_fix_show_flg"	 =>	"t",
					"emp_room"			 =>	$emp_room,
					"apv_sub_order"		 =>	$apv_sub_order_wk,
					"multi_apv_flg"		 =>	$multi_apv_flg,
					"next_notice_div"	 =>	$next_notice_div,
					"parent_pjt_id"		 =>	$parent_pjt_id,
					"child_pjt_id"		 =>	$child_pjt_id,
					"other_apv_flg"		 =>	"f",
					"draft_flg"			 =>	($draft == "on") ? "t" : "f",
					"eval_content"		 => null
				);

				$cl_applyapv_model->insert($arr);
				$apv_sub_order_wk++;
			}
		}
	// 2012/07/18 Yamagawa add(s)
	// レベルＶ　レベルアップ申請　かつ　承認者が審議会の場合
	} else if ($st_div == 11 && $short_wkfw_name == "c440" && $council_num > 1){
		$varname = "check_council".$i."_";
		$arr_field_name = array_keys($_POST);
		foreach($arr_field_name as $field_name){
			$pos = strpos($field_name,$varname);
			if ($pos === 0){
				$apv_emp_id = $_POST[$field_name];
				// 所属、役職も登録する
				$infos = get_empmst($con, $apv_emp_id, $fname);

				$emp_class     = $infos[2];
				$emp_attribute = $infos[3];
				$emp_dept      = $infos[4];
				$emp_st        = $infos[6];
				$emp_room      = $infos[33];

				$arr = array(
					"wkfw_id"			 =>	$wkfw_id,
					"apply_id"			 =>	$apply_id,
					"apv_order"			 =>	$apv_order,
					"emp_id"			 =>	$apv_emp_id,
					"apv_stat"			 =>	"0",
					"apv_date"			 =>	"",
					"apv_comment"		 =>	"",
					"st_div"			 =>	$st_div,
					"deci_flg"			 =>	"t",
					"emp_class"			 =>	$emp_class,
					"emp_attribute"		 =>	$emp_attribute,
					"emp_dept"			 =>	$emp_dept,
					"emp_st"			 =>	$emp_st,
					"apv_fix_show_flg"	 =>	"t",
					"emp_room"			 =>	$emp_room,
					"apv_sub_order"		 =>	$apv_sub_order_wk,
					"multi_apv_flg"		 =>	$multi_apv_flg,
					"next_notice_div"	 =>	$next_notice_div,
					"parent_pjt_id"		 =>	$parent_pjt_id,
					"child_pjt_id"		 =>	$child_pjt_id,
					"other_apv_flg"		 =>	"f",
					"draft_flg"			 =>	($draft == "on") ? "t" : "f",
					"eval_content"		 => null
				);

				$cl_applyapv_model->insert($arr);
				$apv_sub_order_wk++;
			}
		}
	// 2012/07/18 Yamagawa add(e)
	} else {
		$varname = "regist_emp_id$i";
		$apv_emp_id = ($$varname == "") ? null : $$varname;
		//--------------------------------------------------------------------------
		// 所属、役職も登録する
		//--------------------------------------------------------------------------
		$infos = get_empmst($con, $apv_emp_id, $fname);

		$emp_class     = $infos[2];
		$emp_attribute = $infos[3];
		$emp_dept      = $infos[4];
		$emp_st        = $infos[6];
		$emp_room      = $infos[33];

		$arr = array(
			"wkfw_id"			 =>	$wkfw_id,
			"apply_id"			 =>	$apply_id,
			"apv_order"			 =>	$apv_order,
			"emp_id"			 =>	$apv_emp_id,
			"apv_stat"			 =>	"0",
			"apv_date"			 =>	"",
			"apv_comment"		 =>	"",
			"st_div"			 =>	$st_div,
			"deci_flg"			 =>	"t",
			"emp_class"			 =>	$emp_class,
			"emp_attribute"		 =>	$emp_attribute,
			"emp_dept"			 =>	$emp_dept,
			"emp_st"			 =>	$emp_st,
			"apv_fix_show_flg"	 =>	"t",
			"emp_room"			 =>	$emp_room,
			"apv_sub_order"		 =>	$apv_sub_order_wk,
			"multi_apv_flg"		 =>	$multi_apv_flg,
			"next_notice_div"	 =>	$next_notice_div,
			"parent_pjt_id"		 =>	$parent_pjt_id,
			"child_pjt_id"		 =>	$child_pjt_id,
			"other_apv_flg"		 =>	"f",
			"draft_flg"			 =>	($draft == "on") ? "t" : "f",
			"eval_content"		 => null
		);

		$cl_applyapv_model->insert($arr);
		$apv_sub_order_wk++;
	}

}
$log->debug('承認者登録　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 添付ファイル削除・登録
//------------------------------------------------------------------------------
$log->debug('添付ファイル削除　開始',__FILE__,__LINE__);
//$obj->delete_applyfile($apply_id);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyfile_model.php");
$cl_applyfile_model = new cl_applyfile_model($mdb2,$login_emp_id);;
$cl_applyfile_model->physical_delete($apply_id);


$log->debug('添付ファイル削除　終了',__FILE__,__LINE__);

$log->debug('添付ファイル登録　開始',__FILE__,__LINE__);
$no = 1;
foreach ($filename as $tmp_filename)
{
	//$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$param=array(
			"apply_id"			=>	$apply_id		,
			"applyfile_no"		=>	$no				,
			"applyfile_name"	=>	$tmp_filename
	);
	$cl_applyfile_model->insert($param);
	$no++;
}
$log->debug('添付ファイル登録　終了',__FILE__,__LINE__);

// 2012/05/29 Yamagawa add(s)
//------------------------------------------------------------------------------
// ●非同期・同期受信登録
//------------------------------------------------------------------------------
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyasyncrecv_model.php");
$cl_applyasyncrecv_model = new cl_applyasyncrecv_model($mdb2,$login_emp_id);

$log->debug('非同期・同期受信削除　開始',__FILE__,__LINE__);
$cl_applyasyncrecv_model->physical_delete($apply_id);
$log->debug('非同期・同期受信削除　終了',__FILE__,__LINE__);

$log->debug('非同期・同期受信登録　開始',__FILE__,__LINE__);

if($wkfw_appr == "2")
{
	$previous_apv_order = "";
	$arr_apv = array();
	$arr_apv_sub_order = array();
	// 対象研修に対する講師人数
	$apvteacher_num = $_POST['apvteacher_num'];
	// 2012/07/19 Yamagawa add(s)
	$council_num = $_POST['council_num'];
	// 2012/07/19 Yamagawa add(e)
	for($i=1; $i <= $approve_num; $i++)
	{

		$varname = "apv_order$i";
		$apv_order = ($$varname == "") ? null : $$varname;

		$varname = "st_div$i";
		$st_div = $$varname;

		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;

		if($previous_apv_order != $apv_order)
		{
			$arr_apv_sub_order = array();
			$apv_sub_order_wk = 1;
		}

		// 承認者が講師　かつ
		// 複数承認可　かつ　
		// 研修に対する講師が複数人設定されている場合
		// 選択した講師分登録する
		if ($st_div == 6 && $multi_apv_flg == "t" && $apvteacher_num > 1){
			$varname = "check_emp_id".$i."_";
			$arr_field_name = array_keys($_POST);
			foreach($arr_field_name as $field_name){
				$pos = strpos($field_name,$varname);
				if ($pos === 0){
					$arr_apv_sub_order[] = $apv_sub_order_wk;
					$apv_sub_order_wk++;
				}
			}
		// 2012/07/18 Yamagawa add(s)
		// レベルＶ　レベルアップ申請　かつ　承認者が審議会の場合
		} else if ($st_div == 11 && $short_wkfw_name == "c440" && $council_num > 1){
			$varname = "check_council".$i."_";
			$arr_field_name = array_keys($_POST);
			foreach($arr_field_name as $field_name){
				$pos = strpos($field_name,$varname);
				if ($pos === 0){
					$arr_apv_sub_order[] = $apv_sub_order_wk;
					$apv_sub_order_wk++;
				}
			}
		// 2012/07/18 Yamagawa add(e)
		} else {
			$arr_apv_sub_order[] = $apv_sub_order_wk;
			$apv_sub_order_wk++;
		}

		$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);

		$previous_apv_order = $apv_order;
	}

	$arr_send_apv_sub_order = array();
	foreach($arr_apv as $apv_order => $apv_info)
	{
		$multi_apv_flg = $apv_info["multi_apv_flg"];
		$next_notice_div = $apv_info["next_notice_div"];
		$arr_apv_sub_order = $apv_info["apv_sub_order"];

		foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
		{
			// 非同期通知
			if($arr_send_apv_sub != null)
			{
				foreach($arr_send_apv_sub as $send_apv_sub_order)
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						$param=array(
								"apply_id"				=>	$apply_id			,
								"send_apv_order"		=>	$send_apv_order		,
								"send_apv_sub_order"	=>	$send_apv_sub_order	,
								"recv_apv_order"		=>	$apv_order			,
								"recv_apv_sub_order"	=>	$recv_apv_sub_order	,
								"send_apved_order"		=>	null				,
								"apv_show_flg"			=>	"f"
						);
						$cl_applyasyncrecv_model->insert($param);
					}
				}
			}
			// 同期通知
			else
			{
				foreach($arr_apv_sub_order as $recv_apv_sub_order)
				{
					$param=array(
							"apply_id"				=>	$apply_id			,
							"send_apv_order"		=>	$send_apv_order		,
							"send_apv_sub_order"	=>	null				,
							"recv_apv_order"		=>	$apv_order			,
							"recv_apv_sub_order"	=>	$recv_apv_sub_order	,
							"send_apved_order"		=>	null				,
							"apv_show_flg"			=>	"f"
					);
					$cl_applyasyncrecv_model->insert($param);
				}
			}

		}
		$arr_send_apv_sub_order = array();

		// 非同期通知の場合
		if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
		}
		// 同期通知または権限並列通知の場合
		else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = null;
		}
	}
}
$log->debug('非同期・同期受信登録　終了',__FILE__,__LINE__);
// 2012/05/29 Yamagawa add(e)

//------------------------------------------------------------------------------
// 申請結果通知削除登録
//------------------------------------------------------------------------------
$log->debug('申請結果通知削除　開始',__FILE__,__LINE__);
//$obj->delete_applynotice($apply_id);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applynotice_model.php");
$cl_applynotice_model = new cl_applynotice_model($mdb2,$login_emp_id);;
$cl_applynotice_model->physical_delete($apply_id);
$log->debug('申請結果通知削除　終了',__FILE__,__LINE__);

$log->debug('申請結果通知登録　開始',__FILE__,__LINE__);
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		//$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
		$param=array(
			"apply_id"		=>	$apply_id				,
			"recv_emp_id"	=>	$arr_notice_emp_id[$i]	,
			"confirmed_flg"	=>	"f"						,
			"send_emp_id"	=>	null					,
			"send_date"		=>	null					,
			"rslt_ntc_div"	=>	$arr_rslt_ntc_div[$i]
		);
		$cl_applynotice_model->insert($param);

	}
}
$log->debug('申請結果通知登録　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// テンプレートの項目のみをパラメータに設定
//------------------------------------------------------------------------------
$param=template_parameter_pre_proccess($_POST);

//------------------------------------------------------------------------------
// パラメータに申請ＩＤを追加
//------------------------------------------------------------------------------
$param['apply_id'] = $apply_id;
$param['emp_id'] = $login_emp_id;

//------------------------------------------------------------------------------
// テンプレート独自の申請処理を呼び出す
//------------------------------------------------------------------------------
//ハンドラーモジュールロード
$log->debug('ハンドラーモジュールロード　開始　管理番号：'.$short_wkfw_name,__FILE__,__LINE__);
$handler_script=dirname(__FILE__) . "/cl/handler/".$short_wkfw_name."_handler.php";
$log->debug('$handler_script：'.$handler_script,__FILE__,__LINE__);
require_once($handler_script);
$log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

// 2012/04/21 Yamagawa add(s)
// ハンドラー　クラス化対応
$log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
$handler_name = $short_wkfw_name."_handler";
$handler = new $handler_name();
$log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);
// 2012/04/21 Yamagawa add(e)

//------------------------------------------------------------------------------
// 申請詳細　更新の場合
//------------------------------------------------------------------------------
if( $mode == "apply_update"){

	$log->debug('■ハンドラー申請詳細　更新　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//AplDetail_Update($mdb2, $login_emp_id, $param);
	$handler->AplDetail_Update($mdb2, $login_emp_id, $param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー申請詳細　更新　終了',__FILE__,__LINE__);

}
//------------------------------------------------------------------------------
// その他
//------------------------------------------------------------------------------
else{

}

//------------------------------------------------------------------------------
// トランザクションをコミット
//------------------------------------------------------------------------------
$log->debug("トランザクション確定 START",__FILE__,__LINE__);
pg_query($con, "commit");
$mdb2->commit();
$log->debug("トランザクション確定 END",__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベース接続を切断
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
pg_close($con);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 添付ファイルの移動
//------------------------------------------------------------------------------
foreach (glob("cl/apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "cl/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "cl/apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("cl/apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}


echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");

$log->info(basename(__FILE__)." END");
?>
</body>
