<?
//======================================================================================================================================================
// 初期処理
//======================================================================================================================================================
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//======================================================================================================================================================
// 入力エラー時の送信フォーム
//======================================================================================================================================================
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="hiyari_el_folder_register.php">
<input type="hidden" name="folder_name" value="<? echo($folder_name); ?>">
<input type="hidden" name="archive" value="<? echo($archive); ?>">
<input type="hidden" name="arch_id" value="<? echo($arch_id); ?>">
<input type="hidden" name="category" value="<? echo($category); ?>">
<input type="hidden" name="cate_id" value="<? echo($cate_id); ?>">
<input type="hidden" name="folder_id" value="<? echo($folder_id); ?>">
<input type="hidden" name="parent" value="<? echo($parent); ?>">
<input type="hidden" name="ref_dept_st_flg" value="<? echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<? echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<? echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<? echo($ref_atrb_src); ?>">
<?
if (is_array($hid_ref_dept))
{
	foreach ($hid_ref_dept as $tmp_dept_id)
	{
		echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
	}
}
else
{
	$hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<? echo($ref_st_flg); ?>">
<?
if (is_array($ref_st))
{
	foreach ($ref_st as $tmp_st_id)
	{
		echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
	}
}
else
{
	$ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<input type="hidden" name="target_id_list2" value="<? echo($target_id_list2); ?>">
<?
if ($target_id_list1 != "")
{
	$hid_ref_emp = split(",", $target_id_list1);
}
else
{
	$hid_ref_emp = array();
}
?>
<input type="hidden" name="upd_dept_st_flg" value="<? echo($upd_dept_st_flg); ?>">
<input type="hidden" name="upd_dept_flg" value="<? echo($upd_dept_flg); ?>">
<input type="hidden" name="upd_class_src" value="<? echo($upd_class_src); ?>">
<input type="hidden" name="upd_atrb_src" value="<? echo($upd_atrb_src); ?>">
<?
if (is_array($hid_upd_dept))
{
	foreach ($hid_upd_dept as $tmp_dept_id)
	{
		echo("<input type=\"hidden\" name=\"upd_dept[]\" value=\"$tmp_dept_id\">\n");
	}
}
else
{
	$hid_upd_dept = array();
}
?>
<input type="hidden" name="upd_st_flg" value="<? echo($upd_st_flg); ?>">
<?
if (is_array($upd_st))
{
	foreach ($upd_st as $tmp_st_id)
	{
		echo("<input type=\"hidden\" name=\"upd_st[]\" value=\"$tmp_st_id\">\n");
	}
}
else
{
	$upd_st = array();
}
?>
<?
if ($target_id_list2 != "")
{
	$hid_upd_emp = split(",", $target_id_list2);
}
else
{
	$hid_upd_emp = array();
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="ref_toggle_mode" value="<? echo($ref_toggle_mode); ?>">
<input type="hidden" name="upd_toggle_mode" value="<? echo($upd_toggle_mode); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
//======================================================================================================================================================
// 入力チェック
//======================================================================================================================================================

//フォルダ名必須チェック
if ($folder_name == "")
{
	echo("<script type=\"text/javascript\">alert('フォルダ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//フォルダ名長さチェック
if (strlen($folder_name) > 60)
{
	echo("<script type=\"text/javascript\">alert('フォルダ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//参照権限チェック:(部署指定ありで部署が指定されていない、もしくは役職指定ありで役職が指定されていない) かつ (個人指定されていない)場合
if ((($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0)
{
	echo("<script type=\"text/javascript\">alert('参照可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//更新権限チェック:(部署指定ありで部署が指定されていない、もしくは役職指定ありで役職が指定されていない) かつ (個人指定されていない)場合
if ((($upd_dept_flg != "1" && count($hid_upd_dept) == 0) || ($upd_st_flg != "1" && count($upd_st) == 0)) && count($hid_upd_emp) == 0)
{
	echo("<script type=\"text/javascript\">alert('更新可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}


//======================================================================================================================================================
// 画面パラメータ精査
//======================================================================================================================================================



if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}
if ($upd_dept_st_flg != "t") {$upd_dept_st_flg = "f";}
if ($upd_dept_flg == "") {$upd_dept_flg = null;}
if ($upd_st_flg == "") {$upd_st_flg = null;}


//======================================================================================================================================================
// DB処理開始
//======================================================================================================================================================

// トランザクションを開始
pg_query($con, "begin");


//======================================================================================================================================================
// ログインユーザの職員情報を取得
//======================================================================================================================================================

$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");
$emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel_emp, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel_emp, 0, "emp_dept");
$emp_st = pg_fetch_result($sel_emp, 0, "emp_st");


//======================================================================================================================================================
// 更新権限確認
//======================================================================================================================================================

// 管理画面以外、書庫直下にカテゴリを作成する場合
if ($path != "3" && $cate_id != "")
{
	$upd_flg = get_upd_flg($con, $fname, $archive, $cate_id, $parent, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);
	if ($upd_flg == false)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('更新権限がありません。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}


//======================================================================================================================================================
// フォルダ(カテゴリ)情報
//======================================================================================================================================================

// カテゴリを作成する場合
if ($cate_id == "")
{
	// カテゴリIDを採番
	$sql = "select max(lib_cate_id) from el_cate";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$new_cate_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

	// カテゴリ情報を登録
	$private_flg = null;//不使用
	$now = date("YmdHis");
	$sql = "insert into el_cate (lib_archive, lib_cate_id, lib_link_id, lib_cate_nm, reg_emp_id, upd_time, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg) values (";
	$content = array($archive, $new_cate_id, $emp_id, pg_escape_string($folder_name), $emp_id, $now, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// フォルダを作成する場合
else
{
	// フォルダIDを採番
	$sql = "select max(folder_id) from el_folder";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$folder_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

	// フォルダ情報を登録
	$private_flg = null;//不使用
	$now = date("YmdHis");
	$sql = "insert into el_folder (folder_id, lib_archive, lib_cate_id, folder_name, reg_emp_id, upd_time, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg) values (";
	$content = array($folder_id, $archive, $cate_id, pg_escape_string($folder_name), $emp_id, $now, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 親フォルダが指定されている場合
	if ($parent != "")
	{
		// ツリー情報を登録
		$sql = "insert into el_tree (parent_id, child_id) values (";
		$content = array($parent, $folder_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}


//======================================================================================================================================================
// 権限情報
//======================================================================================================================================================
//(部署＋役職＋個人)×(参照＋更新)＝計６権限をデリートインサート

// カテゴリを作成する場合
if ($cate_id == "")
{
	//対象テーブル
	$refdept_tblname = "el_cate_ref_dept";
	$refst_tblname = "el_cate_ref_st";
	$refemp_tblname = "el_cate_ref_emp";
	$upddept_tblname = "el_cate_upd_dept";
	$updst_tblname = "el_cate_upd_st";
	$updemp_tblname = "el_cate_upd_emp";
	//ID
	$id_name = "lib_cate_id";
	$lib_id = $new_cate_id;
}

// フォルダを作成する場合
else
{
	//対象テーブル
	$refdept_tblname = "el_folder_ref_dept";
	$refst_tblname = "el_folder_ref_st";
	$refemp_tblname = "el_folder_ref_emp";
	$upddept_tblname = "el_folder_upd_dept";
	$updst_tblname = "el_folder_upd_st";
	$updemp_tblname = "el_folder_upd_emp";
	//ID
	$id_name = "folder_id";
	$lib_id = $folder_id;
}

// 参照可能部署情報を登録
$sql = "delete from $refdept_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_dept as $tmp_val)
{
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into $refdept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
	$content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能役職情報を登録
$sql = "delete from $refst_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($ref_st as $tmp_st_id)
{
	$sql = "insert into $refst_tblname ($id_name, st_id) values (";
	$content = array($lib_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能職員情報を登録
$sql = "delete from $refemp_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_emp as $tmp_emp_id)
{
	$sql = "insert into $refemp_tblname ($id_name, emp_id) values (";
	$content = array($lib_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能部署情報を登録
$sql = "delete from $upddept_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_dept as $tmp_val)
{
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into $upddept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
	$content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能役職情報を登録
$sql = "delete from $updst_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($upd_st as $tmp_st_id)
{
	$sql = "insert into $updst_tblname ($id_name, st_id) values (";
	$content = array($lib_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能職員情報を登録
$sql = "delete from $updemp_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_emp as $tmp_emp_id)
{
	$sql = "insert into $updemp_tblname ($id_name, emp_id) values (";
	$content = array($lib_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


//======================================================================================================================================================
// DB処理終了
//======================================================================================================================================================

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);


//======================================================================================================================================================
// 物理フォルダ作成
//======================================================================================================================================================

// カテゴリを作成した場合、物理フォルダを作成
if ($cate_id == "")
{
	$cate_id = $new_cate_id;
	create_cate_directory($cate_id);
}


//======================================================================================================================================================
// 処理終了通知・画面クローズ
//======================================================================================================================================================

//※ユーザー画面・管理画面共通
?>
<script type="text/javascript">
if(window.opener && !window.opener.closed && window.opener.reload_page)
{
	window.opener.reload_page();
	window.close();
}
</script>
<?
?>
