<?
define("PNG_GRAPH_VERTEX_NONE", 0);
define("PNG_GRAPH_VERTEX_DOT_B", 1);
define("PNG_GRAPH_VERTEX_CIRCLE_B", 2);
define("PNG_GRAPH_VERTEX_SQUARE_W", 3);
define("PNG_GRAPH_VERTEX_SQUARE_B", 4);

define("PNG_GRAPH_TYPE_ORESEN", 1);
define("PNG_GRAPH_TYPE_BP", 2);
define("PNG_GRAPH_TYPE_DOTONLY", 3);

header("content-type: image/png");


//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// 以下、温度板専用グラフ
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

$border_color = (@$_REQUEST["border"]!="" ? $_REQUEST["border"] : "#5279a5");
$separator_color = "#dddddd";
$dash_color = "#eeeeee";
if (@$_REQUEST["print_mode"]){
	$border_color = "#000000";
	$separator_color = "#000000";
	$dash_color = "#000000";
}

// expansion:拡大率 印刷時は巨大な画像を作成して、縮小印刷することで、画像クオリティアップ
$exp = (int)max(1, @$_REQUEST["expansion"]);
$width = (int)@$_REQUEST["width"];
$height = (int)@$_REQUEST["height"];


//**********************************************************
// ４の１：  温度板左側の「目盛り」ヘッダ部画像
//**********************************************************
if (@$_REQUEST["position"] == "row_header"){

	// 横軸は項目単位で計算する。
	// とすると、グラフの横幅の最小値は0で、最大値は4である。
	$GRAPH_MAX_WIDTH = 5;

	// グラフ画像の作成
	$graph = new SimplePngGraph($width, $height, $exp);

	// 横列の点線ボーダーを描く為の領域用に、グラフ実体を作成する。横線の点線を描画指定する。
	$dots = array($dash_color,$dash_color,$dash_color,$dash_color,$dash_color,$dash_color,"#ffffff","#ffffff");
	$gA = &$graph->generateGraphField("for-dashed_border");
	$gA->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$gA->drawRowSeparator(0.5, $separator_color, $dots);

	// 縦横の実線ボーダーを描く為の領域用に、グラフ実体を作成する。縦横の実線を描画指定する。
	$gB = &$graph->generateGraphField("for-border");
	$gB->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$gB->drawColSeparator(1, $border_color);
	$gB->drawRowSeparator(1, $separator_color);

  // T/P/R/SPO2/BPの直下の横線は、点線ではない。よって実線で上書きする。gA/gBどちらに書いてもよい。
	$gA->setLine(0, 42.5, 5, 42.5, $border_color);

  // 37℃の線
	$gA->setLine(0, 37, 5, 37, "#ff60cf");

	// 固定で文字を書く。gA/gBどちらに書いてもよい。
	$gA->setTextPx(6, 2,   "T",  "#0000ff"); $gA->setTextPx(36, 2,   "P",   "#ff0000");
	$gA->setTextPx(3, 18,  "42", "#0000ff"); $gA->setTextPx(33, 18,  "190", "#ff0000");
	$gA->setTextPx(3, 48,  "41", "#0000ff"); $gA->setTextPx(33, 48,  "170", "#ff0000");
	$gA->setTextPx(3, 78,  "40", "#0000ff"); $gA->setTextPx(33, 78,  "150", "#ff0000");
	$gA->setTextPx(3, 108, "39", "#0000ff"); $gA->setTextPx(33, 108, "130", "#ff0000");
	$gA->setTextPx(3, 138, "38", "#0000ff"); $gA->setTextPx(33, 138, "110", "#ff0000");
	$gA->setTextPx(3, 168, "37", "#0000ff"); $gA->setTextPx(33, 168, "90",  "#ff0000");
	$gA->setTextPx(3, 198, "36", "#0000ff"); $gA->setTextPx(33, 198, "70",  "#ff0000");
	$gA->setTextPx(3, 228, "35", "#0000ff"); $gA->setTextPx(33, 228, "50",  "#ff0000");
	$gA->setTextPx(3, 258, "34", "#0000ff"); $gA->setTextPx(33, 258, "30",  "#ff0000");
	$gA->setTextPx(3, 287, "0",  "#0000ff"); $gA->setTextPx(33, 287, "0",   "#ff0000");

	$gA->setTextPx(66, 2,   "R",  "#008822"); $gA->setTextPx(91, 2,   "SP",  "#1b9cab");
	$gA->setTextPx(63, 18,  "50", "#008822");
	$gA->setTextPx(63, 48,  "45", "#008822");
	$gA->setTextPx(63, 78,  "40", "#008822"); $gA->setTextPx(93, 78,  "100", "#1b9cab");
	$gA->setTextPx(63, 108, "35", "#008822"); $gA->setTextPx(93, 108, "90",  "#1b9cab");
	$gA->setTextPx(63, 138, "30", "#008822"); $gA->setTextPx(93, 138, "80",  "#1b9cab");
	$gA->setTextPx(63, 168, "25", "#008822"); $gA->setTextPx(93, 168, "70",  "#1b9cab");
	$gA->setTextPx(63, 198, "20", "#008822"); $gA->setTextPx(93, 198, "60",  "#1b9cab");
	$gA->setTextPx(63, 228, "15", "#008822"); $gA->setTextPx(93, 228, "50",  "#1b9cab");
	$gA->setTextPx(63, 258, "10", "#008822"); $gA->setTextPx(93, 258, "40",  "#1b9cab");
	$gA->setTextPx(63, 287, "0",  "#008822"); $gA->setTextPx(93, 287, "0",   "#1b9cab");

  $gA->setTextPx(128, 2,  "BP",   "#777722");
  $gA->setTextPx(123, 18,  "220", "#777722");
  $gA->setTextPx(123, 48,  "200", "#777722");
  $gA->setTextPx(123, 78,  "180", "#777722");
  $gA->setTextPx(123, 108, "160", "#777722");
  $gA->setTextPx(123, 138, "140", "#777722");
  $gA->setTextPx(123, 168, "120", "#777722");
  $gA->setTextPx(123, 198, "100", "#777722");
  $gA->setTextPx(123, 228, "80",  "#777722");
  $gA->setTextPx(123, 258, "60",  "#777722");
  $gA->setTextPx(123, 287, "0",   "#777722");



	// 赤い頂点の凡例を描画するだけの為に、グラフを作成する。そして赤い頂点をひとつ書く。
	$g1 = &$graph->generateGraphField("graph_t_blue");
	$g1->setLineColor("#0000ff");
	$g1->setVertexStyle(PNG_GRAPH_VERTEX_SQUARE_W);
	$g1->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$g1->setPoint(0.77, 42.75);
	$g1->setLine(0.55, 42.75, 0.95, 42.75, "#0000ff"); // 頂点に横線を入れておく

	// 緑の頂点の凡例を描画するだけの為に、グラフを作成する。そして緑の頂点をひとつ書く。
	$g2 = &$graph->generateGraphField("graph_p_red");
	$g2->setLineColor("#ff0000");
	$g2->setVertexStyle(PNG_GRAPH_VERTEX_CIRCLE_B);
	$g2->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$g2->setPoint(1.77, 42.75);
	$g2->setLine(1.55, 42.75, 1.95, 42.75, "#ff0000"); // 頂点に横線を入れておく

	// 青い頂点の凡例を描画するだけの為に、グラフを作成する。そして青い頂点をひとつ書く。
	$g3 = &$graph->generateGraphField("graph_r_green");
	$g3->setLineColor("#008822");
	$g3->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$g3->setPoint(2.77, 42.75);
	$g3->setLine(2.55, 42.75, 2.95, 42.75, "#008822"); // 頂点に横線を入れておく

	// シアンの頂点の凡例を描画するだけの為に、グラフを作成する。そしてシアンの頂点をひとつ書く。
	$g4 = &$graph->generateGraphField("graph_s_cyan");
	$g4->setLineColor("#1b9cab");
	$g4->setVertexStyle(PNG_GRAPH_VERTEX_SQUARE_B);
	$g4->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$g4->setPoint(3.77, 42.75);
	$g4->setLine(3.55, 42.75, 3.95, 42.75, "#1b9cab"); // 頂点に横線を入れておく

	// 画像を出力
	$graph->output();
}

//**********************************************************
// ４の２：  温度板折れ線メイングラフの画像
//**********************************************************
if (@$_REQUEST["position"] == "data"){

	// 横軸は分単位で計算する。
	// とすると、グラフの横幅は、60分かける7日間として計算する。
	// グラフの左端Xがゼロとすると、グラフの横幅最大値は、60*24*7=10080である。
	$GRAPH_ONE_DAY = 60*24;
	$GRAPH_MAX_WIDTH = $GRAPH_ONE_DAY*7;

	// グラフ画像の作成
	$graph = new SimplePngGraph($width, $height, $exp);

	// 横列の点線ボーダーを描くだけの為に、グラフ実体を作成する。横線の点線を描画指定する。
	$dots = array($dash_color,$dash_color,$dash_color,$dash_color,$dash_color,$dash_color,"#ffffff","#ffffff");
	$gA = &$graph->generateGraphField("for-dashed_border");
	$gA->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$gA->drawRowSeparator(0.5, $separator_color, $dots);

	// 縦横の実線ボーダーを描くだけの為に、グラフ実体を作成する。縦横の実線を描画指定する。
	$gB = &$graph->generateGraphField("graph-for-border");
	$gB->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);
	$gB->drawColSeparator($GRAPH_ONE_DAY, $border_color);
	$gB->drawRowSeparator(1, $separator_color);

	//============================
	// GRAPH0 : BPグラフ (40から240)
	//============================
	$g0 = &$graph->generateGraphField("graph-for-bp", PNG_GRAPH_TYPE_BP);
	$g0->setLineColor("#aaaa55");
	$g0->setVertexStyle(PNG_GRAPH_VERTEX_CIRCLE_B);
	$g0->setGraphRange(0, $GRAPH_MAX_WIDTH, 40, 240);
	if (@$_REQUEST["gbp"]!=""){
		$points_bp = explode(",", @$_REQUEST["gbp"].",");
		foreach($points_bp as $idx_bp => $point_bp){
			if (!$point_bp || count($point_bp)<1) break;
			$day_and_point = explode("-",$point_bp);
			if (!$day_and_point) break;
			$p1p2 = explode("_", $day_and_point[1]);
			if (!$p1p2 || count($p1p2) < 2) break;
			$g0->setBpPoint((float)$day_and_point[0], max(40, min(240, (float)$p1p2[0])), max(40, min(240, (float)$p1p2[1])));
		}
	}

	//============================
	// GRAPH1 : 折れ線<T>グラフ(青) (33-43)
	//============================
	$g1 = &$graph->generateGraphField("graph-t-red");
	$g1->setLineColor("#0000ff");
	$g1->setVertexStyle(PNG_GRAPH_VERTEX_SQUARE_W);
	$g1->setGraphRange(0, $GRAPH_MAX_WIDTH, 33, 43);

  // 37℃の線
	$g1->setLine(0, 37, $GRAPH_MAX_WIDTH, 37, "#ff60cf");

	if (@$_REQUEST["gt"]!=""){
		$points_t = explode(",", @$_REQUEST["gt"].",");
		foreach($points_t as $idx_t => $point_t){
			$day_and_point = explode("-",$point_t);
			if (!$day_and_point || count($day_and_point)<2) break;
			$g1->setPoint((float)$day_and_point[0], max(33, min(43, (float)$day_and_point[1])));
		}
	}

	//============================
	// GRAPH2 : 折れ線<P>グラフ(赤) (10から200)
	//============================
	$g2 = &$graph->generateGraphField("graph-p-blue");
	$g2->setLineColor("#ff0000");
	$g2->setVertexStyle(PNG_GRAPH_VERTEX_CIRCLE_B);
	$g2->setGraphRange(0, $GRAPH_MAX_WIDTH, 10, 210);

	if (@$_REQUEST["gp"]!=""){
		$points_p = explode(",", @$_REQUEST["gp"].",");
		foreach($points_p as $idx_p => $point_p){
			$day_and_point = explode("-",$point_p);
			if (!$day_and_point || count($day_and_point)<2) break;
			$g2->setPoint((float)$day_and_point[0], max(10, min(210, (float)$day_and_point[1])));
		}
	}

	//============================
	// GRAPH3 : 折れ線<R>グラフ(緑) (5から52.5)
	//============================
	$g3 = &$graph->generateGraphField("graph-r-green");
	$g3->setLineColor("#008822");
	$g3->setGraphRange(0, $GRAPH_MAX_WIDTH, 5, 55);

	if (@$_REQUEST["gr"]!=""){
		$points_r = explode(",", @$_REQUEST["gr"].",");
		foreach($points_r as $idx_r => $point_r){
			$day_and_point = explode("-",$point_r);
			if (!$day_and_point || count($day_and_point)<2) break;
			$g3->setPoint((float)$day_and_point[0], max(5, min(55, (float)$day_and_point[1])));
		}
	}

	//============================
	// GRAPH4 : 折れ線<SP>グラフ(シアン) (30から130)
	//============================
	$g4 = &$graph->generateGraphField("graph-s-cyan");
	$g4->setLineColor("#1b9cab");
	$g4->setVertexStyle(PNG_GRAPH_VERTEX_SQUARE_B);
	$g4->setGraphRange(0, $GRAPH_MAX_WIDTH, 30, 130);

	if (@$_REQUEST["gs"]!=""){
		$points_r = explode(",", @$_REQUEST["gs"].",");
		foreach($points_r as $idx_r => $point_r){
			$day_and_point = explode("-",$point_r);
			if (!$day_and_point || count($day_and_point)<2) break;
			$g4->setPoint((float)$day_and_point[0], max(30, min(130, (float)$day_and_point[1])));
		}
	}
	// 画像を出力
	$graph->output();
}


//**********************************************************
// ４の３：  酸素吸入グラフ・心電図モニター・呼吸器画像
//**********************************************************
if (@$_REQUEST["position"] == "sanso" || @$_REQUEST["position"] == "sinden" || @$_REQUEST["position"] == "kokyuki"){

	$clr = "#8064a2";
	if (@$_REQUEST["position"] == "sinden") $clr = "#9bbb59";
	if (@$_REQUEST["position"] == "kokyuki") $clr = "#ff9155";

	// 横軸の発想はメイングラフと同じ
	$GRAPH_ONE_DAY = 60*24;
	$GRAPH_MAX_WIDTH = $GRAPH_ONE_DAY*7;

	// グラフ画像の作成
	$graph = new SimplePngGraph($width, $height, $exp);

	// 縦の実線ボーダーを描くだけの為に、グラフ実体を作成する。縦の実線を描画指定する。
	// 高さは特に関係ないが、画像ピクセル高(26予定)と同じにしておく
	$g1 = &$graph->generateGraphField("graph-for-border");
	$g1->setGraphRange(0, $GRAPH_MAX_WIDTH, 0, 26);
	$g1->drawColSeparator($GRAPH_ONE_DAY, $border_color);

	// 頂点の無い線のグラフ地
	$gL = &$graph->generateGraphField("graph-for-line");
	$gL->setLineColor($clr);
	$gL->setVertexStyle(PNG_GRAPH_VERTEX_NONE);
	$gL->setGraphRange(0, $GRAPH_MAX_WIDTH, 0, 26);

	// 黒い頂点のみを上書き描画するためのグラフ地
	$gB = &$graph->generateGraphField("graph-for-over-point-b", PNG_GRAPH_TYPE_DOTONLY);
	$gB->setLineColor($clr);
	$gB->setVertexStyle(PNG_GRAPH_VERTEX_SQUARE_B);
	$gB->setGraphRange(0, $GRAPH_MAX_WIDTH, 0, 26);

	// 白い頂点のみを上書き描画するためのグラフ地
	$gW = &$graph->generateGraphField("graph-for-over-point-w", PNG_GRAPH_TYPE_DOTONLY);
	$gW->setLineColor($clr);
	$gW->setVertexStyle(PNG_GRAPH_VERTEX_SQUARE_W);
	$gW->setGraphRange(0, $GRAPH_MAX_WIDTH, 0, 26);

	if (@$_REQUEST["plots"]!=""){
		$points_p = explode(",", @$_REQUEST["plots"].",");
		foreach($points_p as $idx_p => $point_p){
			$day_and_point = explode("-",$point_p);
			if (!$day_and_point || count($day_and_point)<2) break;
			$gL->setLine((float)$day_and_point[2], 8, (float)$day_and_point[3], 8, $clr);
			if ($day_and_point[0]=="n") $gB->setPoint($day_and_point[2], 8);
			if ($day_and_point[1]=="n"){
				if ($day_and_point[4]=="y") $gW->setPoint($day_and_point[3], 8);
				else $gB->setPoint($day_and_point[3], 8);
			}
			if (@$day_and_point[5]){
				$gL->setText((float)$day_and_point[2], 24, $day_and_point[5],  "#000000");
			}
		}
	}

	// 画像を出力
	$graph->output();
}





//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// 以下、グラフジェネレータ
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■



//******************************************************************************
// Simple PNG Graph (FOR PHP4 OR MORE)
// グラフのコンテナ。複数のグラフ実体を搭載し、グラフ実体をコンテナ上に描画する。
//******************************************************************************
class SimplePngGraph {
	var $width = 0;              // 出力画像のピクセル幅
	var $height = 0;             // 出力画像のピクセル高
	var $image = null;           // 出力画像イメージ
	var $border_color = -1;      // 外枠線の色、マイナス値なら外枠線を描画しない
	var $graphFields = array();  // グラフの実体(SimplePngGraphFieldクラス)の配列。複数グラフを重ね合わせて格納。
	var $expansion = 1;

	// コンストラクタ
	function SimplePngGraph($w, $h, $expansion = 1){
		$this->width = $w*$expansion;
		$this->height = $h*$expansion;
		$this->expansion =$expansion;
		$this->image = imagecreatetruecolor($this->width, $this->height);

		$white = imagecolorallocate($this->image, 255, 255, 255); // 透明度を持つ色を作成
		//************************************************
		// 古いGDのバージョンによるバグあり？
		// imagefill()が利かない場合がある。そうすると背景が黒くなってしまう。
		// このためimagefilledrectangle()で回避することにした。
		//************************************************

		imagefilledrectangle($this->image, 0, 0, $this->width-1, $this->height-1, $white);
//		imagealphablending($this->image, false); // アルファブレンディングを無効
//		imageSaveAlpha($this->image, true); // アルファチャンネルを有効
//		$transparent = imagecolorallocatealpha($this->image, 255, 255, 255, 127); // 透明度を持つ色を作成
//		imagefill($this->image, 0, 0, $transparent); // 塗りつぶす

		register_shutdown_function(array(&$this, '_' . __CLASS__));
	}

	// デストラクタ
	function _SimplePngGraph(){
		if ($this->image) imagedestroy($this->image);
	}

	// グラフ実体を作成して、実体のアドレスを返す
	function &generateGraphField($graph_id, $graph_type = PNG_GRAPH_TYPE_ORESEN){
		$this->graphFields[$graph_id] = new SimplePngGraphField($graph_id, $graph_type);
		return $this->graphFields[$graph_id];
	}

	// "#RRGGBB"形式の文字列を、RGBに変換するための汎用関数
	function setWebColorToImage($rrggbb){
		if (!$rrggbb) return 0;
		if (strlen($rrggbb) == 7) $rrggbb = substr($rrggbb, 1);
		if (strlen($rrggbb) != 6) return 0;
		$rgb = array();
		$r = hexdec(substr($rrggbb, 0, 2));
		$g = hexdec(substr($rrggbb, 2, 2));
		$b = hexdec(substr($rrggbb, 4, 2));
		return imagecolorallocate($this->image, $r, $g, $b);
	}

	// これまでに指定されたグラフ設定から、グラフの描画を行い、
	// 標準出力に出力する。
	function output(){
		$exp = $this->expansion;

		//------------------------
		// カラーの作成準備
		//------------------------
		foreach($this->graphFields as $graph_id => $g){
			if ($g->line_color_string) $this->graphFields[$graph_id]->line_color = $this->setWebColorToImage($g->line_color_string);
		}

		//==================================
		// その１ 行区切り線の描画
		//==================================
		imageantialias($this->image, false);
		foreach($this->graphFields as $graph_id => $g){
			if ($g->row_height > 0 && $g->row_separator_color){
				$clr = $this->setWebColorToImage($g->row_separator_color);
				if ($g->row_separator_line_style) {
					$colors = array();
					foreach($g->row_separator_line_style as $rrggbb) {
						$array[] = $this->setWebColorToImage($rrggbb);
					}
					imagesetstyle($this->image, $array);
				}
				$row_count = floor(($g->rowmax - $g->rowmin) / $g->row_height);
				if ($row_count > 1){
					$row_height = floor($this->height / $row_count);
					for ($i = 1; $i < $row_count; $i++){
						$row_top = $row_height * $i;
						imageline($this->image, 0, $row_top,  $this->width-1, $row_top,
							$g->row_separator_line_style ? IMG_COLOR_STYLED : $clr);
						if ($exp > 2){
							imageline($this->image, 0, $row_top-1,  $this->width-1, $row_top-1,
								$g->row_separator_line_style ? IMG_COLOR_STYLED : $clr);
							imageline($this->image, 0, $row_top+1,  $this->width-1, $row_top+1,
								$g->row_separator_line_style ? IMG_COLOR_STYLED : $clr);
						}
					}
				}
			}
		}

		//==================================
		// その２ 列区切り線の描画
		//==================================
		imageantialias($this->image, false);
		foreach($this->graphFields as $graph_id => $g){
			if ($g->col_width > 0 && $g->col_separator_color){
				$clr = $this->setWebColorToImage($g->col_separator_color);
				if ($g->col_separator_line_style) {
					$colors = array();
					foreach($g->col_separator_line_style as $rrggbb) {
						$array[] = $this->setWebColorToImage($rrggbb);
					}
					imagesetstyle($this->image, $array);
				}
				$col_count = floor(($g->colmax - $g->colmin) / $g->col_width);
				if ($col_count > 1){
					$col_width = floor($this->width / $col_count);
					for ($i = 1; $i < $col_count; $i++){
						$col_left = $col_width * $i;
						imageline($this->image, $col_left, 0, $col_left, $this->height-1,
							$g->col_separator_line_style ? IMG_COLOR_STYLED : $clr);
						if ($exp > 2){
							imageline($this->image, $col_left-1, 0, $col_left-1, $this->height-1,
								$g->col_separator_line_style ? IMG_COLOR_STYLED : $clr);
							imageline($this->image, $col_left+1, 0, $col_left+1, $this->height-1,
								$g->col_separator_line_style ? IMG_COLOR_STYLED : $clr);
						}
					}
				}
			}
		}

		//==================================
		// その３ 追加指定された線の描画
		//==================================
		foreach($this->graphFields as $graph_id => $g){
			foreach($g->lines as $idx => $data){
				$colf = $data["col_range_from"];
				$rowf = $data["row_range_from"];
				$colt = $data["col_range_to"];
				$rowt = $data["row_range_to"];
				$clr = $this->setWebColorToImage($data["web_color"]);
				$x1 = floor(($colf - $g->colmin) / ($g->colmax - $g->colmin) * $this->width);
				$y1 = $this->height - floor(($rowf - $g->rowmin) / ($g->rowmax - $g->rowmin) * $this->height);
				$x2 = floor(($colt - $g->colmin) / ($g->colmax - $g->colmin) * $this->width);
				$y2 = $this->height - floor(($rowt - $g->rowmin) / ($g->rowmax - $g->rowmin) * $this->height);
				$cyokusen = ($x1==$x2 || $y1==$y2);
				imageantialias($this->image, !$cyokusen);
				imageline($this->image, $x1, $y1, $x2, $y2, $clr);
				if ($cyokusen && $exp > 2){
					if ($x1==$x2){
						imageline($this->image, $x1-1, $y1, $x2-1, $y2, $clr);
						imageline($this->image, $x1+1, $y1, $x2+1, $y2, $clr);
					}
					if ($y1==$y2){
						imageline($this->image, $x1, $y1-1, $x2, $y2-1, $clr);
						imageline($this->image, $x1, $y1+1, $x2, $y2+1, $clr);
					}
				}
			}
		}

		//==================================
		// その４ BP用の縦長グラフの描画
		//==================================
		imageantialias($this->image, false);
		foreach($this->graphFields as $graph_id => $g){
			if ($g->graph_type != PNG_GRAPH_TYPE_BP) continue;
			if ($g->line_color_string) $g->line_color = $this->setWebColorToImage($g->line_color_string);
			foreach($g->bppoints as $col_range => $data){
				$x = floor(($col_range - $g->colmin) / ($g->colmax - $g->colmin) * $this->width);
				$ymin = $this->height - floor(($data["min"] - $g->rowmin) / ($g->rowmax - $g->rowmin) * $this->height);
				$ymax = $this->height - floor(($data["max"] - $g->rowmin) / ($g->rowmax - $g->rowmin) * $this->height);
				imageline($this->image, $x+0, $ymin, $x+0, $ymax, $g->line_color);
				for ($i = 0; $i < $exp; $i++){
					$m = $i * 2;
					imageline($this->image, $x-2-$m, $ymin, $x-2-$m, $ymax, $g->line_color);
					imageline($this->image, $x-1-$m, $ymin, $x-1-$m, $ymax, $g->line_color);
					imageline($this->image, $x+1+$m, $ymin, $x+1+$m, $ymax, $g->line_color);
					imageline($this->image, $x+2+$m, $ymin, $x+2+$m, $ymax, $g->line_color);
				}
			}
		}

		//==================================
		// その５ 折れ線グラフの描画
		//==================================
		imageantialias($this->image, true);
		foreach($this->graphFields as $graph_id => $g){
			if ($g->graph_type != PNG_GRAPH_TYPE_ORESEN) continue;
			$xo = 0; $yo = 0;
			$is_first_record = 1;
			ksort($g->points);

			foreach($g->points as $col_range => $row_range){
				$x = floor(($col_range - $g->colmin) / ($g->colmax - $g->colmin) * $this->width);
				$y = $this->height - floor(($row_range - $g->rowmin) / ($g->rowmax - $g->rowmin) * $this->height);
				if (!$is_first_record){
					//************************************************
					// GDのバージョンによるバグあり？
					// 線の開始点が高さを超えそうな値、かつ右上がり30〜40度位の線は、落ちる。
					// 具体的には、高さ300pxの画像を作成するときに、
					// 以下の指定はエラーとなる。こまった。
					// 例えば、
					// imageline($this->image, 285, 299, 400, 210, $g->line_color); これは落ちる
					// imageline($this->image, 285, 500, 400, 210, $g->line_color); これはＯＫ
					// imageline($this->image, 285, 298, 400, 210, $g->line_color); これもＯＫ
					// よって、とりあえず最大でも、開始点は高さより1px引くことにする。
					//************************************************
					$xo = max(0, min($this->width-2, $xo));
					$x = max(0, min($this->width-2, $x));
					$yo = max(0, min($this->height-2, $yo));
					$y = max(0, min($this->height-2, $y));
					imageline($this->image, $xo, $yo, $x, $y, $g->line_color);
					if ($exp > 2){
						$xoz = max(0, min($this->width-2,  $xo+1));
						$xz = max(0, min($this->width-2,   $x+1));
						$yoz = max(0, min($this->height-2, $yo+1));
						$yz = max(0, min($this->height-2,  $y+1));
						imageline($this->image, $xoz, $yoz, $xz, $yz, $g->line_color);

						$xoz = max(0, min($this->width-2,  $xo-1));
						$xz = max(0, min($this->width-2,   $x-1));
						$yoz = max(0, min($this->height-2, $yo-1));
						$yz = max(0, min($this->height-2,  $y-1));
						imageline($this->image, $xoz, $yoz, $xz, $yz, $g->line_color);

						$xoz = max(0, min($this->width-2,  $xo+1));
						$xz = max(0, min($this->width-2,   $x+1));
						$yoz = max(0, min($this->height-2, $yo-1));
						$yz = max(0, min($this->height-2,  $y-1));
						imageline($this->image, $xoz, $yoz, $xz, $yz, $g->line_color);

						$xoz = max(0, min($this->width-2,  $xo-1));
						$xz = max(0, min($this->width-2,   $x-1));
						$yoz = max(0, min($this->height-2, $yo+1));
						$yz = max(0, min($this->height-2,  $y+1));
						imageline($this->image, $xoz, $yoz, $xz, $yz, $g->line_color);
					}
				}
				$xo = $x;
				$yo = $y;
				$is_first_record = 0;
			}
		}

		//==================================
		// その６ 頂点の描画
		//==================================
		imageantialias($this->image, false);
		$white = imagecolorallocate($this->image, 255,255,255);
		foreach($this->graphFields as $graph_id => $g){
			if ($g->graph_type != PNG_GRAPH_TYPE_ORESEN && $g->graph_type != PNG_GRAPH_TYPE_DOTONLY) continue;
			$xo = 0; $yo = 0;
			$is_first_record = 1;
			foreach($g->points as $col_range => $row_range){
				$x = floor(($col_range - $g->colmin) / ($g->colmax - $g->colmin) * $this->width);
				$y = $this->height - floor(($row_range - $g->rowmin) / ($g->rowmax - $g->rowmin) * $this->height);
				if ($x >= 0 && $x <= $this->width) {
					switch ($g->vertex_style) {
						case PNG_GRAPH_VERTEX_CIRCLE_B:
							imagefilledellipse($this->image, $x, $y, 6*$exp, 6*$exp, $g->line_color);
							break;
						case PNG_GRAPH_VERTEX_SQUARE_B:
							imagefilledrectangle($this->image, $x-(2*$exp), $y-(2*$exp), $x+(2*$exp), $y+(2*$exp), $g->line_color);
							break;
						case PNG_GRAPH_VERTEX_SQUARE_W:
							imagefilledrectangle($this->image, $x-(3*$exp), $y-(3*$exp), $x+(3*$exp), $y+(3*$exp), $g->line_color);
							imagefilledrectangle($this->image, $x-(2*$exp), $y-(2*$exp), $x+(2*$exp), $y+(2*$exp), $white);
							break;
						case PNG_GRAPH_VERTEX_DOT_B:
							imageline($this->image, $x-(3*$exp), $y, $x+(3*$exp), $y, $g->line_color);
							imageline($this->image, $x, $y-(3*$exp), $x, $y+(3*$exp), $g->line_color);
							if ($exp > 2){
								imageline($this->image, $x-(3*$exp), $y-1, $x+(3*$exp), $y-1, $g->line_color);
								imageline($this->image, $x-1, $y-(3*$exp), $x-1, $y+(3*$exp), $g->line_color);

								imageline($this->image, $x-(3*$exp), $y+1, $x+(3*$exp), $y+1, $g->line_color);
								imageline($this->image, $x+1, $y-(3*$exp), $x+1, $y+(3*$exp), $g->line_color);
							}
					}
				}
				$xo = $x;
				$yo = $y;
				$is_first_record = 0;
			}
		}

		//==================================
		// その７ 追加指定されたテキストの描画
		//==================================
		foreach($this->graphFields as $graph_id => $g){
			foreach($g->texts_p as $idx => $data){
				$clr = $this->setWebColorToImage($data["web_color"]);
				imagettftext ($this->image, 8*$exp, 0, $data["x"]*$exp, ($data["y"]+11)*$exp, $clr, "./verdana.TTF", $data["text"]);
			}
		}
		foreach($this->graphFields as $graph_id => $g){
			foreach($g->texts_r as $idx => $data){
				$x = floor(($data["col_range"] - $g->colmin) / ($g->colmax - $g->colmin) * $this->width);
				$y = $this->height - floor(($data["row_range"] - $g->rowmin) / ($g->rowmax - $g->rowmin) * $this->height);
				$clr = $this->setWebColorToImage($data["web_color"]);
				imagettftext ($this->image, 8*$exp, 0, $x, $y*$exp+11, $clr, "./verdana.TTF", $data["text"]);
			}
		}

		imagepng($this->image);
	}
}

//******************************************************************************
// Field Of Simple PNG Graph
// グラフ実体
//******************************************************************************
class SimplePngGraphField {
	var $id = "";
	var $points = array();               // 折れ線グラフの点情報リスト
	var $bppoints = array();             // BPグラフの点情報リスト
	var $texts_p = array();              // 追加で表示指定されたテキスト、ピクセル指定
	var $texts_r = array();              // 追加で表示指定されたテキスト、グラフ座標指定
	var $lines = array();                // 追加で表示指定された直線
	var $col_width = 0;                  // 列幅(グラフ値。ピクセルではない。）
	var $row_height = 0;                 // 行高(グラフ値。ピクセルではない。）
	var $col_separator_color = 0;        // 列セパレータ線の色(文字列：#RRGGBB)
	var $row_separator_color = 0;        // 行セパレータ線の色(文字列：#RRGGBB)
	var $col_separator_line_style = 0;   // 列セパレータ線のスタイル(点の配列：各点は"#RRGGBB"形式で表現）
	var $row_separator_line_style = 0;   // 行セパレータ線のスタイル(点の配列：各点は"#RRGGBB"形式で表現）
	var $colmin = 0;                     // グラフ値、X軸の左端値
	var $colmax = 0;                     // グラフ値、X軸の右端値
	var $rowmin = 0;                     // グラフ値、Y軸の最小値
	var $rowmax = 0;                     // グラフ値、Y軸の最大値
	var $line_color_string = 0;          // グラフ線の色(文字列：#RRGGBB)
	var $line_color = 0;                 // グラフ線の色(内部計算用)
	var $vertex_style = PNG_GRAPH_VERTEX_DOT_B; // 頂点の形
	var $graph_type = PNG_GRAPH_TYPE_ORESEN;    // グラフのタイプ(折れ線グラフ/BP線グラフなど）

	// コンストラクタ
	function SimplePngGraphField($id, $graph_type){
		$this->id = $id;
		$this->graph_type = $graph_type;
	}

	// グラフのXY軸の最大、最小値のセット。必須。
	function setGraphRange($colmin, $colmax, $rowmin, $rowmax){
		$this->colmin = (double)$colmin;
		$this->colmax = (double)$colmax;
		$this->rowmin = (double)$rowmin;
		$this->rowmax = (double)$rowmax;
	}

	// 列セパレータを描画する際は呼び出す。
	function drawColSeparator($col_width = 0, $web_color = 0, $line_style_colors = 0){
		$this->col_width = (double)$col_width;
		$this->col_separator_color = $web_color;
		$this->col_separator_line_style = $line_style_colors;
	}

	// 行セパレータを描画する際は呼び出す。
	function drawRowSeparator($row_height = 0, $web_color = 0, $line_style_colors = 0){
		$this->row_height = (double)$row_height;
		$this->row_separator_color = $web_color;
		$this->row_separator_line_style = $line_style_colors;
	}

	// 折れ線グラフの頂点をセット
	function setPoint($col_pos, $row_pos){
		$this->points[(string)((double)$col_pos)] = (double)$row_pos;
	}

	// BP線グラフの点情報をセット
	function setBpPoint($col_pos, $row_pos_min, $row_pos_max){
		$this->bppoints[(string)((double)$col_pos)] = array("min"=>(double)$row_pos_min, "max"=>(double)$row_pos_max);
	}

	// 文字列を書く。ピクセル位置で指定する。追加で描画したい文字列がある場合は呼び出す。
	function setTextPx($x, $y, $text, $web_color=0){
		$this->texts_p[] = array("x"=>(int)$x, "y"=>(int)$y, "text"=>$text, "web_color"=>$web_color);
	}

	// 文字列を書く。追加で描画したい文字列がある場合は呼び出す。
	function setText($x, $y, $text, $web_color=0){
		$this->texts_r[] = array("col_range"=>(int)$x, "row_range"=>(int)$y, "text"=>$text, "web_color"=>$web_color);
	}

	// 追加で描画したい線がある場合は呼び出す。
	function setLine($col_range_from, $row_range_from, $col_range_to, $row_range_to, $web_color){
		$this->lines[] = array(
			"col_range_from"=>(double)$col_range_from, "row_range_from"=>(double)$row_range_from,
			"col_range_to"=>(double)$col_range_to, "row_range_to"=>(double)$row_range_to,
			"web_color"=>$web_color
		);
	}

	// グラフ線の色の指定
	function setLineColor($web_color){
		$this->line_color_string = $web_color;
	}

	// 折れ線グラフの頂点の形の指定
	function setVertexStyle($style){
		$this->vertex_style = $style;
	}
}


?>
