<!-- ************************************************************************ -->
<!-- 勤務シフト作成　勤務シフト作成画面　月平均夜勤時間算出ＣＬＡＳＳ -->
<!-- ************************************************************************ -->

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("date_utils.php");
require_once("timecard_paid_hol_hour_class.php"); //時間有休追加 20120213

class duty_shift_time_common_class
{
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	var $arr_hol_hour_info = array(); //時間有休追加 20120213

    var $arr_empcond_officehours_ptn = array(); //個人別の所定時間を優先する勤務パターンの配列、$data[group_id][atdptn_id]
    var $arr_empcond_officehours = array(); //個人別の所定時間
    var $arr_weekday_flg = array(); //曜日
    var $empcond_officehours_hist = array(); //個人別の所定時間履歴 []["histdate"](昇順),["data"]:基本、曜日別のデータ配列 20130220

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_time_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（計算）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 出勤表の勤務時間帯情報を取得
	// @param	$pattern_id		出勤グループＩＤ
	//
	// @return	$data			勤務時間帯情報
	/*************************************************************************/
	function get_officehours_array($pattern_id) {
		///-----------------------------------------------------------------------------
		// 勤務時間帯情報を取得
		///-----------------------------------------------------------------------------
		$sql = "select * from officehours";
		$cond = "where tmcd_group_id = $pattern_id ";
		$coud .= "order by officehours_id, pattern";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		$data = array();
		for($i=0;$i<$num;$i++){
			$officehours_id = pg_result($sel,$i,"officehours_id");
			$atdptn_ptn_id = pg_result($sel,$i,"pattern");

			for ($k=1; $k<=5; $k++){
				//-------------------------------------------------------------------
				//開始時間
				//-------------------------------------------------------------------
				$wk = "officehours" . $k . "_start";
				$wk_hhmm = pg_result($sel,$i,$wk);
				$wk = "start_hhmm_" . $atdptn_ptn_id . "_" . $officehours_id . "_" . $k;
				$data[$wk] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
				//休日に通常勤務日の情報設定
				if ($officehours_id == "1") {
					$wk4 = "start_hhmm_" . $atdptn_ptn_id . "_4_" . $k;
					$wk5 = "start_hhmm_" . $atdptn_ptn_id . "_5_" . $k;
					$data[$wk4] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
					$data[$wk5] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
					//祝日、年末年始
					$wk6 = "start_hhmm_" . $atdptn_ptn_id . "_6_" . $k;
					$wk7 = "start_hhmm_" . $atdptn_ptn_id . "_7_" . $k;
					$data[$wk6] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
					$data[$wk7] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
				}
				//-------------------------------------------------------------------
				//終了時間
				//-------------------------------------------------------------------
				$wk = "officehours" . $k . "_end";
				$wk_hhmm = pg_result($sel,$i,$wk);
				$wk = "end_hhmm_" . $atdptn_ptn_id . "_" . $officehours_id . "_" . $k;
				$data[$wk] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
				//休日に通常勤務日の情報設定
				if ($officehours_id == "1") {
					$wk4 = "end_hhmm_" . $atdptn_ptn_id . "_4_" . $k;
					$wk5 = "end_hhmm_" . $atdptn_ptn_id . "_5_" . $k;
					$data[$wk4] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
					$data[$wk5] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
					//祝日、年末年始
					$wk6 = "end_hhmm_" . $atdptn_ptn_id . "_6_" . $k;
					$wk7 = "end_hhmm_" . $atdptn_ptn_id . "_7_" . $k;
					$data[$wk6] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
					$data[$wk7] = substr($wk_hhmm,0,2) . substr($wk_hhmm,3,2);
				}
			}
		}
		//-------------------------------------------------------------------
		//休日に通常勤務日の情報設定
		//-------------------------------------------------------------------
		for ($i=4; $i<=5; $i++) {


		}

		return $data;
	}
//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（計算）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 時間の減算
	// @param	$base_time		減算元時間（hh.m）
	// @param	$sub_time		減算時間（hhmm)
	//
	// @return	$prov_time			時間
	/*************************************************************************/
	function subtraction_time($base_time, $sub_time) {
		///-----------------------------------------------------------------------------
		//時刻チェック
		///-----------------------------------------------------------------------------
		if ($base_time == "") {
			return 0.0;
		}
		if ($sub_time == "") {
			return $base_time;
		}
		///-----------------------------------------------------------------------------
		//時間変換
		///-----------------------------------------------------------------------------
		$sub_hh = 0;
		$sub_mm = 0;
		if ($sub_time != "") {
			$sub_hh = (int)substr($sub_time, 0, 2);
			$sub_mm = (int)substr($sub_time, 2, 2);
		}
		///-----------------------------------------------------------------------------
		//時間減算
		///-----------------------------------------------------------------------------
		$wk_time = $sub_hh + ($sub_mm / 60);
		if ($base_time > $wk_time) {
			$prov_time = $base_time - $wk_time;
		} else {
			$prov_time = 0;
		}
		return $prov_time;
	}
	/*************************************************************************/
	// 時間の算出
	// @param	$start_time		開始時刻
	// @param	$end_time		終了時刻
	//
	// @return	$prov_time		時間
	/*************************************************************************/
	function get_times($start_time, $end_time) {
		///-----------------------------------------------------------------------------
		//時刻チェック
		///-----------------------------------------------------------------------------
		if (($start_time == "") ||
			($end_time == "") ||
			($start_time == $end_time)) {
			return 0;
		}
		///-----------------------------------------------------------------------------
		//時間算出
		///-----------------------------------------------------------------------------
		$start_hh = (int)substr($start_time, 0, 2);
		$start_mm = (int)substr($start_time, 2, 2);
		$end_hh = (int)substr($end_time, 0, 2);
		$end_mm = (int)substr($end_time, 2, 2);
		if ($end_hh < $start_hh) {
			$end_hh = $end_hh + 24;
		}
		if ($end_mm < $start_mm) {
			$end_hh--;
			$end_mm = $end_mm + 60;
		}
		$prov_hh = $end_hh - $start_hh;
		$prov_mm = $end_mm - $start_mm;
		$prov_time = $prov_hh + ($prov_mm / 60);

		return $prov_time;
	}
	/*************************************************************************/
	// 時間の算出（申し送り時間の減算）
	// @param	$start_time			開始時刻
	// @param	$end_time			終了時刻
	// @param	$data_standard		基準施設情報（グループ指定）
	// @param	$arr_out_time		外出時間の配列
	//
	// @return	$prov_time		時間
	/*************************************************************************/
	function subtract_msg_times($start_time,
								$end_time,
								$data_standard) {
		///-----------------------------------------------------------------------------
		//時刻チェック
		///-----------------------------------------------------------------------------
		if (($start_time == "") ||
			($end_time == "") ||
			($start_time == $end_time)) {
			return 0;
		}
		///-----------------------------------------------------------------------------
		//申し送り時間
		///-----------------------------------------------------------------------------
		for($i=0; $i<3; $i++) {
			$k = $i + 1;
			$send_start_hhmm[$i] = $data_standard["send_start_hhmm_$k"];	//開始時分
			$send_end_hhmm[$i] = $data_standard["send_end_hhmm_$k"];		//終了時分
		}
		///-----------------------------------------------------------------------------
		//時間算出
		///-----------------------------------------------------------------------------
		$prov_time = $this->get_times($start_time, $end_time);
		///-----------------------------------------------------------------------------
		//申し送り時刻の減算
		//豊永さん仕様：その勤務シフトの終了時刻が申し送り時間マスタの時刻と一致している場合は、申し送り時間分を勤務時間から減算する。
		///-----------------------------------------------------------------------------
		for($i=0; $i<3; $i++) {
		if (($send_start_hhmm[$i] != "") &&
			($send_end_hhmm[$i] != "")) {
			//減算対象か判定
			$wk_array = $this->get_pertinence_times($start_time,
													$end_time,
													$send_start_hhmm[$i],
													$send_end_hhmm[$i]);
			if (($wk_array["check_flg"] == "3") ||
				($wk_array["check_flg"] == "6")) {
				$send_time = $this->get_times($send_start_hhmm[$i], $end_time);
				$prov_time -= $send_time;
			}
		}
		}

		return $prov_time;
	}

	/*************************************************************************/
	// 申し送り時間取得 get_msg_times
	// ※外出時間の重複分は除く
	// @param	$start_time			開始時刻
	// @param	$end_time			終了時刻
	// @param	$data_standard		基準施設情報（グループ指定）
	// @param	$arr_out_time		外出時間の配列
	//
	// @return	$prov_time		時間
	/*************************************************************************/
	function get_msg_times($start_time,
								$end_time,
								$data_standard,
								$arr_out_time) {
		///-----------------------------------------------------------------------------
		//時刻チェック
		///-----------------------------------------------------------------------------
		if (($start_time == "") ||
			($end_time == "") ||
			($start_time == $end_time)) {
			return 0;
		}
		///-----------------------------------------------------------------------------
		//申し送り時間
		///-----------------------------------------------------------------------------
		for($i=0; $i<3; $i++) {
			$k = $i + 1;
			$send_start_hhmm[$i] = $data_standard["send_start_hhmm_$k"];	//開始時分
			$send_end_hhmm[$i] = $data_standard["send_end_hhmm_$k"];		//終了時分
		}
		///-----------------------------------------------------------------------------
		//時間算出
		///-----------------------------------------------------------------------------
		///-----------------------------------------------------------------------------
		//申し送り時刻の減算
		//豊永さん仕様：その勤務シフトの終了時刻が申し送り時間マスタの時刻と一致している場合は、申し送り時間分を勤務時間から減算する。
		///-----------------------------------------------------------------------------
		//申し送り時刻の開始終了の範囲内の場合に減算するよう変更 20090930 不具合対応※範囲の判断が正しくなかった 20100825
		for($i=0; $i<3; $i++) {
			if (($send_start_hhmm[$i] != "") &&
				($send_end_hhmm[$i] != "") ) {
				//減算対象か判定
				//所定の日時と申し送りの日時を比較する
				$today = date("Ymd");
				$nextday = date("Ymd", strtotime("+1 days"));
				$prov_start_date_time = $today.$start_time;
				//開始の2時間後から終了時刻までで判断
				$prov_start_date_time = date("YmdHi", strtotime("+2 hours",date_utils::to_timestamp_from_ymdhi($prov_start_date_time))); //hi-> Hi 20110915
				$prov_end_date_time = ($start_time > $end_time) ? $nextday.$end_time : $today.$end_time;
				$send_start_date_time = ($start_time > $send_start_hhmm[$i]) ? $nextday.$send_start_hhmm[$i] : $today.$send_start_hhmm[$i];
				$send_end_date_time = ($start_time > $send_end_hhmm[$i]) ? $nextday.$send_end_hhmm[$i] : $today.$send_end_hhmm[$i];
				//開始終了の間に申し送りの開始が入っている場合。早退の場合に対応するため終了側は判定しない 20120313
                if ($prov_start_date_time <= $send_start_date_time &&
						$send_start_date_time < $prov_end_date_time) {
					//外出時間との重複分を除く
					$arr_send_time = $this->get_minute_time_array($send_start_date_time, $send_end_date_time);
					$send_time = count(array_diff($arr_send_time, $arr_out_time));
					break;
				}
			}
		}

		return $send_time;
	}
	/*************************************************************************/
	// 申し送り時間取得 get_msg_times2
	// ※配列方式ではなく、時刻から時間を計算する
	// @param	$start_time			開始時刻
	// @param	$end_time			終了時刻
	// @param	$data_standard		基準施設情報（グループ指定）
	//
	// @return	$prov_time		時間
	/*************************************************************************/
	function get_msg_times2($start_time,
		$end_time,
		$data_standard) {
		///-----------------------------------------------------------------------------
		//時刻チェック
		///-----------------------------------------------------------------------------
		if (($start_time == "") ||
				($end_time == "") ||
				($start_time == $end_time)) {
			return 0;
		}
		///-----------------------------------------------------------------------------
		//申し送り時間
		///-----------------------------------------------------------------------------
		for($i=0; $i<3; $i++) {
			$k = $i + 1;
			$send_start_hhmm[$i] = $data_standard["send_start_hhmm_$k"];	//開始時分
			$send_end_hhmm[$i] = $data_standard["send_end_hhmm_$k"];		//終了時分
		}
		///-----------------------------------------------------------------------------
		//時間算出
		///-----------------------------------------------------------------------------
		///-----------------------------------------------------------------------------
		//申し送り時刻の減算
		//豊永さん仕様：その勤務シフトの終了時刻が申し送り時間マスタの時刻と一致している場合は、申し送り時間分を勤務時間から減算する。
		///-----------------------------------------------------------------------------
		//申し送り時刻の開始終了の範囲内の場合に減算
		for($i=0; $i<3; $i++) {
			if (($send_start_hhmm[$i] != "") &&
					($send_end_hhmm[$i] != "") ) {
				//減算対象か判定
				//所定の日時と申し送りの日時を比較する
				$today = date("Ymd");
				$nextday = date("Ymd", strtotime("+1 days"));
				$prov_start_date_time = $today.$start_time;
				//開始の2時間後から終了時刻までで判断
				$prov_start_date_time = date("YmdHi", strtotime("+2 hours",date_utils::to_timestamp_from_ymdhi($prov_start_date_time))); //hi-> Hi 20110915
				$prov_end_date_time = ($start_time > $end_time) ? $nextday.$end_time : $today.$end_time;
				$send_start_date_time = ($start_time > $send_start_hhmm[$i]) ? $nextday.$send_start_hhmm[$i] : $today.$send_start_hhmm[$i];
				$send_end_date_time = ($start_time > $send_end_hhmm[$i]) ? $nextday.$send_end_hhmm[$i] : $today.$send_end_hhmm[$i];
				//開始終了の間に申し送りの開始が入っている場合。早退の場合に対応するため終了側は判定しない 20120313
                if ($prov_start_date_time <= $send_start_date_time &&
						$send_start_date_time < $prov_end_date_time) {
					$send_time = date_utils::get_diff_minute($send_end_date_time, $send_start_date_time);
					break;
				}
			}
		}

		return $send_time;
	}
	/*************************************************************************/
	// 基準時刻より対象となる時間帯を算出
	// @param	$duty_start_time		開始時刻
	// @param	$duty_end_time			終了時刻
	// @param	$standard_start_time	開始時刻（基準）
	// @param	$standard_end_time		終了時刻（基準）
	//
	// @return	$data	対象時間
	/*************************************************************************/
	function get_pertinence_times($duty_start_time,
								$duty_end_time,
								$standard_start_time,
								$standard_end_time) {
		///-----------------------------------------------------------------------------
		//時間帯の開始／終了時刻設定
		///-----------------------------------------------------------------------------
		$wk_flg = "";
		$start_time = str_replace(":", "", $standard_start_time); //20130307
        $end_time = str_replace(":", "", $standard_end_time); //20130307
		///-----------------------------------------------------------------------------
		// 「勤務時刻(開始)　＜　勤務時刻(終了)」の場合
		///-----------------------------------------------------------------------------
		if ((int)$standard_start_time < (int)$standard_end_time) {
			///-----------------------------------------------------------------------------
			// 「勤務時刻(開始)　＜　夜勤時間帯　＜　勤務時刻(終了)」の場合
			///-----------------------------------------------------------------------------
			if (((int)$duty_start_time < (int)$standard_start_time) &&
				((int)$duty_end_time > (int)$standard_end_time)) {
				$wk_flg = "1";
			} else {
				///-----------------------------------------------------------------------------
				// 「夜勤時間帯(開始)　＜=　勤務時刻(開始)　＜=　夜勤時間帯(終了)」の場合
				// 時刻算出に「勤務時刻(開始)」を利用
				///-----------------------------------------------------------------------------
				if (((int)$duty_start_time >= (int)$standard_start_time) &&
					((int)$duty_start_time <= (int)$standard_end_time)) {
					$wk_flg = "2";
					$start_time = $duty_start_time;
				}
				///-----------------------------------------------------------------------------
				// 「夜勤時間帯(開始)　＜=　勤務時刻(終了)　＜=　夜勤時間帯(終了)」の場合
				// 時刻算出に「勤務時刻(終了)」を利用
				///-----------------------------------------------------------------------------
				if (((int)$duty_end_time >= (int)$standard_start_time) &&
					((int)$duty_end_time <= (int)$standard_end_time)) {
					$wk_flg = "3";
					$end_time = $duty_end_time;
				}
			}
		} else {
			///-----------------------------------------------------------------------------
			// 「勤務時刻(開始)　＜　夜勤時間帯　＜　勤務時刻(終了)」の場合
			///-----------------------------------------------------------------------------
			if (((int)$duty_start_time < (int)$standard_start_time) &&
				((int)$duty_end_time > (int)$standard_end_time) &&
				((int)$duty_start_time > (int)$duty_end_time)) {
				$wk_flg = "4";
			} else {
				///-----------------------------------------------------------------------------
				// 「夜勤時間帯(開始)　＜=　勤務時刻(開始)　＜=　夜勤時間帯(終了)」の場合
				// 時刻開始算出に「勤務時刻(開始)」を利用
				///-----------------------------------------------------------------------------
				if (((int)$duty_start_time >= (int)$standard_start_time) &&
					((int)$duty_start_time <= 2400)) {
					$wk_flg = "5";
					$start_time = $duty_start_time;
				}
				if (((int)$duty_start_time >= 0) &&
					((int)$duty_start_time <= (int)$standard_end_time)) {
					$wk_flg = "5";
					$start_time = $duty_start_time;
				}
				///-----------------------------------------------------------------------------
				// 「夜勤時間帯(開始)　＜=　勤務時刻(終了)　＜=　夜勤時間帯(終了)」の場合
				// 時刻終了算出に「勤務時刻(終了)」を利用
				///-----------------------------------------------------------------------------
				if (((int)$duty_end_time >= (int)$standard_start_time) &&
					((int)$duty_end_time <= 2400)) {
					$wk_flg = "6";
					$end_time = $duty_end_time;
				}
				if (((int)$duty_end_time >= 0) &&
					((int)$duty_end_time <= (int)$standard_end_time)) {
					$wk_flg = "6";
					$end_time = $duty_end_time;
				}
				///-----------------------------------------------------------------------------
				// 「夜勤時間帯(終了)　＞　勤務時刻(開始)」
				// 「夜勤時間帯(終了)　＜　勤務時刻(終了)」の場合
				// 時刻開始算出に「勤務時刻(開始)」を利用
				///-----------------------------------------------------------------------------
				if (((int)$duty_start_time <= (int)$standard_end_time) &&
					((int)$duty_end_time >= (int)$standard_end_time)) {
					$wk_flg = "7";
					$start_time = $duty_start_time;
					$end_time = $standard_end_time;
					// 「夜勤時間帯(開始)　＜　勤務時刻(終了)」の場合
					if ((int)$duty_end_time >= (int)$duty_end_time) {
						$wk_flg = "8";
						$start_time2 = $standard_start_time;
						$end_time2 = $duty_end_time;
					}
				}
			}
		}
		///-----------------------------------------------------------------------------
		//対象となる開始／終了時刻設定
		///-----------------------------------------------------------------------------
		$data = array();
		$data["start_time"] = "0000";
		$data["end_time"] = "0000";
		if ($wk_flg != "") {
			$data["start_time"] = $start_time;
			$data["end_time"] = $end_time;
			$data["check_flg"] = $wk_flg;
			//終了時刻後の夜勤時間
			if ($wk_flg == "8") {
				$data["start_time2"] = $start_time2;
				$data["end_time2"] = $end_time2;
			}
		}

		return $data;
	}
	/*************************************************************************/
	// 日勤／夜勤時間の算出
	// @param	$officehours_id		勤務日区分（１：通常勤務日／休日、２：勤務日１、３：勤務日２
	// @param	$pattern_id			出勤グループＩＤ
	// @param	$atdptn_ptn_id		出勤パターンＩＤ
	// @param	$duty_start_time	開始時刻
	// @param	$duty_end_time		終了時刻
	// @param	$data_standard		基準施設情報（グループ指定）
	// @param	$officehours_array	出勤表の勤務時間帯情報
	// @param	$meeting_time		会議・研修時間
	// @param	$job_id				職員の職種ID、申し送り時間確認用
	// @param	$arr_no_subtract	申し送り時間を差し引かない職種・勤務パターン 1_1_1形式
	// @param	$out_time			外出時刻
	// @param	$ret_time			復帰時刻
	// @param	$meeting_start_time	会議・研修開始時刻
	// @param	$meeting_end_time	会議・研修終了時刻
	// @param	$hol_hour_start_time	時間有休開始時刻 20120213
	// @param	$hol_hour_end_time		時間有休終了時刻 20120213
    // @param	$date		            日付 20120309
    //
	// @return	$data			日勤／夜勤時間
	/*************************************************************************/
	function get_duty_night_times(
								$officehours_id,
								$pattern_id,
								$atdptn_ptn_id,
								$duty_start_time,
								$duty_end_time,
								$data_standard,
								$officehours_array,
								$meeting_time,
								$job_id="",
								$arr_no_subtract=array(),
								$out_time="",
								$ret_time="",
								$meeting_start_time="",
								$meeting_end_time="",
								$hol_hour_start_time,	//時間有休追加 20120213
								$hol_hour_end_time,		//時間有休追加 20120213
                                $date
								) {
		///-----------------------------------------------------------------------------
		//初期値設定
		///-----------------------------------------------------------------------------
/*
		$duty_time_to = 0.0;				//日勤時間（当日）
		$duty_time_next = 0.0;				//日勤時間（翌日）
		$night_time_to = 0.0;				//夜勤時間（当日）
		$night_time_next = 0.0;				//夜勤時間（翌日）
*/
		$prov_start_hhmm = $data_standard["prov_start_hhmm"];	//夜勤時間帯（開始時刻）
		$prov_end_hhmm = $data_standard["prov_end_hhmm"];		//夜勤時間帯（終了時刻）
		if ($data_standard["prov_start_hhmm"] == "") { $prov_start_hhmm = "0000"; }
		if ($data_standard["prov_end_hhmm"] == "") { $prov_end_hhmm = "0000"; }

		///******************************************************************************
		//出勤グループごとの勤務時間帯を取得
		///******************************************************************************
		///-----------------------------------------------------------------------------
		//出勤表の勤務時間帯情報より対象勤務時間帯を取得
		///-----------------------------------------------------------------------------
		//休日は通常勤務日と同様の扱い
		if ($officehours_id == 2 || $officehours_id == 3) {
			$wk_officehours_id = $officehours_id;
		} else {
			$wk_officehours_id = 1;
		}
//		if (($wk_officehours_id <= 3) && ($wk_officehours_id >= 1)){
		if ($pattern_id >= 1 && $atdptn_ptn_id >= 1) {
//pattern_idは1から順番のため、-1で配列の位置 20120224
//			for ($i=0; $i<count($officehours_array); $i++) {
//			if ($officehours_array[$i]["pattern_id"] == $pattern_id) {
				$i = $pattern_id - 1;
				$wk_start_hhmm = "start_hhmm_" . $atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
				$wk_end_hhmm = "end_hhmm_" . $atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
				$officehours_start_hhmm = $officehours_array[$i][$wk_start_hhmm];
				$officehours_end_hhmm = $officehours_array[$i][$wk_end_hhmm];
//				break;
//			}
//			}
		}
//		}
        //個人別所定時間を優先する場合 20120309
        if ($this->arr_empcond_officehours_ptn[$pattern_id][$atdptn_ptn_id] == 1) {
            $weekday_flg = $this->arr_weekday_flg[$date];
            $arr_empcond_officehours = $this->get_empcond_officehours($weekday_flg,$date); //個人別勤務時間帯履歴対応 20130220
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $officehours_start_hhmm = $arr_empcond_officehours["officehours2_start"];
                $officehours_end_hhmm = $arr_empcond_officehours["officehours2_end"];
            }
        }

        if ($officehours_start_hhmm == "") { $officehours_start_hhmm = "0000"; }	//未設定時のデフォルト開始時刻
		if ($officehours_end_hhmm == "") { $officehours_end_hhmm = "2400"; }		//未設定時のデフォルト終了時刻
		///-----------------------------------------------------------------------------
		//「当日／翌日」の勤務時間帯を算出
		///-----------------------------------------------------------------------------
		if ($duty_start_time <= $duty_end_time) {
			$duty_start_time_to = $duty_start_time;
			$duty_end_time_to = $duty_end_time;
			$duty_start_time_next = "0000";
			$duty_end_time_next = "0000";
		} else {
			$duty_start_time_to = $duty_start_time;
            //所定終了時刻が日またがりか確認 20120319
            if ($officehours_start_hhmm > $officehours_end_hhmm) {
                $duty_end_time_to = "2400";
            }
            //日またがりでなければ退勤時刻ではなく所定終了時刻を使用
            else {
                $duty_end_time_to = $officehours_end_hhmm;
            }
			$duty_start_time_next = "0000";
			$duty_end_time_next = $duty_end_time;
		}
		///-----------------------------------------------------------------------------
		//「当日」の勤務時間帯より対象となる時間算出
		///-----------------------------------------------------------------------------
		$time_array = $this->get_pertinence_times(
												$duty_start_time_to,
												$duty_end_time_to,
												$officehours_start_hhmm,
												$officehours_end_hhmm);
		$start_time_to = $time_array["start_time"];	//勤務開始時間（当日）
		$end_time_to = $time_array["end_time"];		//勤務終了時間（当日）
		///-----------------------------------------------------------------------------
		//「翌日」の勤務時間帯より対象となる時間算出
		///-----------------------------------------------------------------------------
		if ($duty_start_time > $duty_end_time) {
			$time_array = $this->get_pertinence_times(
												$duty_start_time_next,
												$duty_end_time_next,
												$officehours_start_hhmm,
												$officehours_end_hhmm);
			$start_time_next = $time_array["start_time"];	//勤務開始時間（翌日）
			$end_time_next = $time_array["end_time"];		//勤務終了時間（翌日）
		} else {
			$start_time_next = "0000";
			$end_time_next = "0000";
		}

		//計算のための時間帯を設定
		if ($duty_start_time > $duty_end_time) {
			$wk_duty_start_time = $start_time_to;
            //所定終了時刻が日またがりか確認 20120319
            if ($officehours_start_hhmm > $officehours_end_hhmm) {
                $wk_duty_end_time = $end_time_next;
            }
            //日またがりでなければ退勤時刻ではなく所定終了時刻を使用
            else {
                $wk_duty_end_time = $officehours_end_hhmm;
            }
		} else {
			$wk_duty_start_time = $start_time_to;
			$wk_duty_end_time = $end_time_to;
		}

		$chk_ptn_id = $job_id."_".$pattern_id."_".$atdptn_ptn_id;

		//申し送りフラグ、職種、勤務パターンによりしない設定
		$send_flg = !in_array($chk_ptn_id, $arr_no_subtract);
		//時間算出、当日・翌日、日勤・夜勤
		$wk_data = $this->get_day_night_times(
				$wk_duty_start_time,
				$wk_duty_end_time,
				$data_standard,
				$out_time,
				$ret_time,
				$send_flg,
				$meeting_start_time,
				$meeting_end_time,
				$assist_start_time,
				$assist_end_time,
				$assist_group_id,
				"0"
		);
		$duty_time_to = $wk_data["duty_time_to"];
		$duty_time_next = $wk_data["duty_time_next"];
		$night_time_to = $wk_data["night_time_to"];
		$night_time_next = $wk_data["night_time_next"];
		//時間有休追加 start 20120213
		$wk_data = $this->get_day_night_times(
				$hol_hour_start_time,
				$hol_hour_end_time,
				$data_standard,
				"",
				"",
				$send_flg,
				"",
				"",
				"1"
				);
		//時間有休を減算する
		$duty_time_to -= $wk_data["duty_time_to"];
		$duty_time_next -= $wk_data["duty_time_next"];
		$night_time_to -= $wk_data["night_time_to"];
		$night_time_next -= $wk_data["night_time_next"];
		//時間有休追加 end 20120213
		///******************************************************************************
		//研修・会議時間の減算
		///******************************************************************************
		// 開始終了時刻がある場合はget_day_night_times関数内で計算 20091009
		if ($meeting_start_time == "" && $meeting_end_time == "") {
			if ($duty_time_to > 0) {
				$duty_time_to = $this->subtraction_time($duty_time_to, $meeting_time);
			} else {
				if ($night_time_to > 0) {
					$night_time_to = $this->subtraction_time($night_time_to, $meeting_time);
				}
			}
		}

//※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※
//※「当日／翌日」を分る場合、以下をコメントにする。
//※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※※
//$duty_time_to += $duty_time_next;
//$night_time_to += $night_time_next;
//$duty_time_next = 0.0;
//$night_time_next = 0.0;

		///******************************************************************************
		//日勤／夜勤時間を設定
		///******************************************************************************
		///-----------------------------------------------------------------------------
		//日勤／夜勤時間を設定
		///-----------------------------------------------------------------------------
		$data = array();
		$data["duty_time_to"] = 0.0;		//日勤時間（当日）
		$data["duty_time_next"] = 0.0;		//日勤時間（翌日）
		$data["night_time_to"] = 0.0;		//夜勤時間（当日）
		$data["night_time_next"] = 0.0;		//夜勤時間（翌日）
		if ($duty_time_to > 0) {	$data["duty_time_to"] = $duty_time_to; }		//日勤時間（当日）
		if ($duty_time_next> 0) {	$data["duty_time_next"] = $duty_time_next; }	//日勤時間（翌日）
		if ($night_time_to > 0) {	$data["night_time_to"] = $night_time_to; }		//夜勤時間（当日）
		if ($night_time_next > 0) { $data["night_time_next"] = $night_time_next; }	//夜勤時間（翌日）
		///-----------------------------------------------------------------------------
		//デバック用
		///-----------------------------------------------------------------------------
		$data["prov_start_hhmm"] = $prov_start_hhmm;
		$data["prov_end_hhmm"] = $prov_end_hhmm;
		$data["officehours_start_hhmm"] = $officehours_start_hhmm;
		$data["officehours_end_hhmm"] = $officehours_end_hhmm;
		$data["wk_start_hhmm"] = $wk_start_hhmm;
		$data["wk_end_hhmm"] = $wk_end_hhmm;
		$data["start_time_to"] = $start_time_to;
		$data["end_time_to"] = $end_time_to;
		$data["start_time_next"] = $start_time_next;
		$data["end_time_next"] = $end_time_next;

		return $data;
	}
	/*************************************************************************/
	// 月平均夜勤時間の算出（予定／実績）
	// （データは前日より取得している）
	// @param	$rslt_flg			実績フラグ（１：実績）
	// @param	$day_cnt			日数
	// @param	$plan_array			勤務シフト情報
	// @param	$data_standard		施設基準情報（グループ指定）
	// @param	$data_atdptn_all	出勤パターン情報
	// @param	$officehours_array	出勤表の勤務時間帯情報
	// @param	$calendar_array     カレンダー情報情報 20120213
	// @param	$group_id	        グループID（病棟） 20120213
	//
	// @return	$ret			月平均夜勤時間
	/*************************************************************************/
	function get_night_sumtime($rslt_flg,
							$day_cnt,
							$plan_array,
							$data_standard,
							$data_atdptn_all,
							$officehours_array,
							$calendar_array,$group_id) {

		//時間有休追加 start 20120213
		$obj_hol_hour = new timecard_paid_hol_hour_class($this->_db_con, $this->file_name);
		$obj_hol_hour->select();

		if ($obj_hol_hour->paid_hol_hour_flag == "t") {
			//予定の場合
			if ($rslt_flg == "") { //
				$arr_emp_id = array();
				for ($i=0; $i<count($plan_array); $i++) {
					$wk_emp_id = $plan_array[$i]["staff_id"];
					$arr_emp_id[] = $wk_emp_id;
				}
				$start_date = $calendar_array[0]["date"];
				$idx = count($calendar_array) - 1;
				$end_date = $calendar_array[$idx]["date"];
				$this->arr_hol_hour_info = $obj_hol_hour->get_paid_hol_hour_emp_month($arr_emp_id, $start_date, $end_date);
			}
		}
		// 申し送り時間を差し引かない職種、パターン
		$arr_no_subtract = $this->get_arr_no_subtract();
		//時間有休追加 end 20120213
        //個人別所定時間を優先するための対応 20120309
        $start_date = $calendar_array[0]["date"];
        $idx = count($calendar_array) - 1;
        $end_date = $calendar_array[$idx]["date"];
        $this->get_empcond_officehours_pattern();
        $this->get_arr_weekday_flag($start_date, $end_date);

//換算日数の配列、シフトグループID、勤務パターンIDをキーとする
		$workday_count_array = array();
		$after_night_duty_array = array();
		$previous_day_flag_array = array();
		for ($i=0;$i<count($data_atdptn_all);$i++) {
			$tmp_group_id = $data_atdptn_all[$i]["group_id"];
			$tmp_atdptn_id = $data_atdptn_all[$i]["id"];
			$workday_count_array["$tmp_group_id"]["$tmp_atdptn_id"] = $data_atdptn_all[$i]["workday_count"];
			if ($data_atdptn_all[$i]["after_night_duty_flag"] == "1") {
				$after_night_duty_array["$tmp_group_id"]["$tmp_atdptn_id"] = $data_atdptn_all[$i]["after_night_duty_flag"];
			}
			if ($data_atdptn_all[$i]["previous_day_flag"] == "1") {
				$previous_day_flag_array["$tmp_group_id"]["$tmp_atdptn_id"] = $data_atdptn_all[$i]["previous_day_flag"];
			}
		}

//4週か判断 日が01は4週以外
		$second_date = $calendar_array[1]["date"];
		$wk_second_dd = substr($second_date, 6, 2);

		///-----------------------------------------------------------------------------
		//全夜勤スタッフ数／全夜勤時間を算出
		//夜専は除く
		///-----------------------------------------------------------------------------
		$rslt_staff_cnt = 0;
		$plan_staff_cnt = 0;
		$plan_sumtime = 0.0;
		$rslt_sumtime = 0.0;
		for ($i=0; $i<count($plan_array); $i++) {
            //個人別所定時間を優先するための対応 20120309
            $wk_staff_id = $plan_array[$i]["staff_id"];
            $this->get_arr_empcond_officehours($wk_staff_id,$start_date); //個人別勤務時間帯履歴対応 20130220
            //if ($plan_array[$i]["night_duty_flg"] != "2") {
			///-----------------------------------------------------------------------------
			//スタッフ毎の全夜勤時間を算出
			///-----------------------------------------------------------------------------
			$wk_plan_sumtime = 0.0;
			$wk_rslt_sumtime = 0.0;

			$wk_plan_sumtime_day = 0.0; //日勤時間帯計 20120217
			$wk_rslt_sumtime_day = 0.0;
            $wk_plan_time_day_flg = true; //夜勤専従の確認フラグ 20120312
            $wk_rslt_time_day_flg = true;
            for ($k=1; $k<=$day_cnt; $k++) {
				//4週計算かどうかでassist_gourpのデータ位置変更
				if ($wk_second_dd == "01") {
					$wk_k = $k - 1; //assist_groupは1日前のデータ
					$wk_group_id = $plan_array[$i]["assist_group_$wk_k"];
				}
				else {
					$wk_k = $k - 1;
					$wk_date_assist = $calendar_array[$wk_k]["date"];
					$wk_group_id = $plan_array[$i]["assist_group_$wk_date_assist"];
				}
				if ($wk_group_id != "" && $wk_group_id != $group_id) {
					continue;
				}
				//if ($wk_group_id == "" || $wk_group_id == $group_id) {
				$wk_plan_time = 0.0;
				$wk_plan_time_day = 0.0;
                $wk_rslt_time_day = 0.0; //夜勤専従の確認用 20120312
                ///-----------------------------------------------------------------------------
				//予定（夜勤）か判定
				///-----------------------------------------------------------------------------
				$wk_flg = "";
				// 夜勤明け等対応 2008/06/10
				// 勤務パターンの日数換算が0の場合は加算しない
				$wk_pattern_id = $plan_array[$i]["pattern_id_$k"];
				$wk_atdptn_id = $plan_array[$i]["atdptn_ptn_id_$k"];
				$plan_workday_count = $workday_count_array["$wk_pattern_id"]["$wk_atdptn_id"];
				$after_night_duty_flag = $after_night_duty_array["$wk_pattern_id"]["$wk_atdptn_id"];
				$wk_prov_start_hhmm = "";
				$wk_prov_end_hhmm = "";
                //日付
                $wk_date = $calendar_array[$k - 1]["date"];
                //個人別所定時間を優先する場合 20120309
                if ($this->arr_empcond_officehours_ptn[$wk_pattern_id][$wk_atdptn_id] == 1) {
                    $weekday_flg = $this->arr_weekday_flg[$wk_date];
                    $arr_empcond_officehours = $this->get_empcond_officehours($weekday_flg,$wk_date); //個人別勤務時間帯履歴対応 20130220
                    if ($arr_empcond_officehours["officehours2_start"] != "") {
                        $wk_flg = "1";
                        $wk_prov_start_hhmm = $arr_empcond_officehours["officehours2_start"];
                        $wk_prov_end_hhmm = $arr_empcond_officehours["officehours2_end"];
                    }
                }
                else {
                    //休暇以外、明け以外
                    if ($wk_atdptn_id != "10" && $wk_atdptn_id != "" &&
                            $plan_workday_count != 0 && $after_night_duty_flag != 1) {
                        $wk_flg = "1";
                        $wk_officehours_id = $plan_array[$i]["officehours_id_$k"];
                        $wk_officehours_id = ($wk_officehours_id == 2 || $wk_officehours_id == 3) ? $wk_officehours_id : 1;
                        if ($wk_atdptn_id >= 1) {
                            $wk_pattern_idx = $wk_pattern_id - 1;
                            $wk_start_hhmm = "start_hhmm_" . $wk_atdptn_id . "_" . $wk_officehours_id . "_2";
                            $wk_end_hhmm = "end_hhmm_" . $wk_atdptn_id . "_" . $wk_officehours_id . "_2";
                            $wk_prov_start_hhmm = $officehours_array[$wk_pattern_idx][$wk_start_hhmm];
                            $wk_prov_end_hhmm = $officehours_array[$wk_pattern_idx][$wk_end_hhmm];
                        }
                    }
                }
                //時間有休追加 start 20120213
				$wk_hol_hour_start_time = "";
				$wk_hol_hour_end_time = "";
                //時間有休
                if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                    $wk_staff_id = $plan_array[$i]["staff_id"];
                    $wk_hol_hour_start_date = $this->arr_hol_hour_info[$wk_staff_id][$wk_date]["start_date"];
                    if ($wk_hol_hour_start_date != "") {
                        $wk_hol_hour_start_time = $this->arr_hol_hour_info[$wk_staff_id][$wk_date]["start_time"];
                        $wk_hol_hour_start_date_time = $wk_hol_hour_start_date.$wk_hol_hour_start_time;
                        //終了時刻を求める
                        $wk_use_hour = $this->arr_hol_hour_info[$wk_staff_id][$wk_date]["use_hour"];
                        $wk_use_minute = $this->arr_hol_hour_info[$wk_staff_id][$wk_date]["use_minute"];
                        $wk_minute = $wk_use_hour * 60 + $wk_use_minute;

                        $wk_hol_hour_end_date_time = date_utils::add_minute_ymdhi($wk_hol_hour_start_date_time, $wk_minute);
                        $wk_hol_hour_end_time = substr($wk_hol_hour_end_date_time, 8, 4);
                    }
                }
                //時間有休追加 end 20120213

				if ($wk_flg == "1") {
					///-----------------------------------------------------------------------------
					//予定用の夜勤時間算出
					///-----------------------------------------------------------------------------
					$time_array = $this->get_duty_night_times(
							$plan_array[$i]["officehours_id_$k"],
							$plan_array[$i]["pattern_id_$k"],
							$plan_array[$i]["atdptn_ptn_id_$k"],
							$wk_prov_start_hhmm,
							$wk_prov_end_hhmm,
							$data_standard,
							$officehours_array,
							"0000",
							$plan_array[$i]["job_id"],
							$arr_no_subtract,
							"",
							"",
							"",
							"",
							$wk_hol_hour_start_time,
							$wk_hol_hour_end_time,
                            $wk_date
							);
					//当日
					if ($previous_day_flag_array["$wk_pattern_id"]["$wk_atdptn_id"] != "1") {
						if ($k > 1 && $k < $day_cnt) { //月末
                            //様式９に合わせるため小数点以下3位を四捨五入 20120523
                            $wk_plan_time += round($time_array["night_time_to"] * 100) / 100;
                            $wk_plan_time_day += round($time_array["duty_time_to"] * 100) / 100;
						}
						if ($k < $day_cnt-1) {
                            $wk_plan_time += round($time_array["night_time_next"] * 100) / 100;
                            $wk_plan_time_day += round($time_array["duty_time_next"] * 100) / 100;
						}
					}
					//前日
					else {
						if ($k > 2) {
                            $wk_plan_time += round($time_array["night_time_to"] * 100) / 100;
                            $wk_plan_time_day += round($time_array["duty_time_to"] * 100) / 100;
						}
						if ($k > 1 && $k < $day_cnt) {
                            $wk_plan_time += round($time_array["night_time_next"] * 100) / 100;
                            $wk_plan_time_day += round($time_array["duty_time_next"] * 100) / 100;
						}
					}
					$wk_plan_sumtime += $wk_plan_time;
					$wk_plan_sumtime_day += $wk_plan_time_day;
                    //夜勤専従の確認 1時間以上は夜勤専従ではない 20120312
                    if ($wk_plan_time_day >= 1) {
                        $wk_plan_time_day_flg = false;
                    }
					//debug
					//echo("<br> $i $k $wk_date $wk_group_id night to=".round($time_array["night_time_to"]*100)/100);
					//echo(" next=".$time_array["night_time_next"]);
					//echo("<br>$k $wk_date duty to=".$time_array["duty_time_to"]);
					//echo(" next=".$time_array["duty_time_next"]);
				}
				// 夜勤明け等対応 2008/06/10
				// 勤務パターンの日数換算が0の場合は加算しない
				$tmp_pattern_id = $plan_array[$i]["rslt_pattern_id_$k"];
				$tmp_atdptn_id = $plan_array[$i]["rslt_id_$k"];
				$rslt_workday_count = $workday_count_array["$tmp_pattern_id"]["$tmp_atdptn_id"];
				$rslt_after_night_duty_flag = $after_night_duty_array["$tmp_pattern_id"]["$tmp_atdptn_id"];
				///-----------------------------------------------------------------------------
				//実績織り込み
				///-----------------------------------------------------------------------------
				if (($plan_array[$i]["rslt_start_time_$k"] != "") &&
						($plan_array[$i]["rslt_end_time_$k"] != "")
					) {
					///-----------------------------------------------------------------------------
					//実績が存在した間の場合、実績の勤務時間を加算
					///-----------------------------------------------------------------------------
                    //日付
                    $wk_date = $calendar_array[$k - 1]["date"];
                    //時間有休がある場合、所定時刻で計算
					if ($obj_hol_hour->paid_hol_hour_flag == "t") {
						$wk_staff_id = $plan_array[$i]["staff_id"];
						$wk_hol_hour_start_date = $this->arr_hol_hour_info[$wk_staff_id][$wk_date]["start_date"];
						if ($wk_hol_hour_start_date != "") {
							$wk_pattern_id = $plan_array[$i]["pattern_id_$k"];
							$wk_atdptn_ptn_id = $plan_array[$i]["atdptn_ptn_id_$k"];
							if ($wk_pattern_id >= 1 && $wk_atdptn_ptn_id >= 1) {
								$wk_officehours_id = $plan_array[$i]["officehours_id_$k"];
								$wk_officehours_id = ($wk_officehours_id == 2 || $wk_officehours_id == 3) ? $wk_officehours_id : 1;
								$wk_pattern_idx = $wk_pattern_id - 1;
								$wk_start_hhmm = "start_hhmm_" . $wk_atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
								$wk_end_hhmm = "end_hhmm_" . $wk_atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
								$plan_array[$i]["rslt_start_time_$k"] = $officehours_array[$wk_pattern_idx][$wk_start_hhmm];
								$plan_array[$i]["rslt_end_time_$k"] = $officehours_array[$wk_pattern_idx][$wk_end_hhmm];
							}

						}
					}
					$time_array = $this->get_duty_night_times(
							$plan_array[$i]["officehours_id_$k"],
							$plan_array[$i]["rslt_pattern_id_$k"],
							$plan_array[$i]["rslt_id_$k"],
							$plan_array[$i]["rslt_start_time_$k"],
							$plan_array[$i]["rslt_end_time_$k"],
							$data_standard,
							$officehours_array,
							$plan_array[$i]["rslt_meeting_time_$k"],
							$plan_array[$i]["job_id"],
							$arr_no_subtract,
							$plan_array[$i]["rslt_out_time_$k"],
							$plan_array[$i]["rslt_ret_time_$k"],
							$plan_array[$i]["rslt_meeting_start_time_$k"],
							$plan_array[$i]["rslt_meeting_end_time_$k"],
							$wk_hol_hour_start_time,
							$wk_hol_hour_end_time,
                            $wk_date
							);
						//当日
					if ($plan_array[$i]["previous_day_flag_$k"] != "1") {
						if ($k > 1 && $k < $day_cnt) {
                            $wk_rslt_sumtime += round($time_array["night_time_to"] * 100) / 100;
                            $wk_rslt_sumtime_day += round($time_array["duty_time_to"] * 100) / 100;
                            $wk_rslt_time_day += round($time_array["duty_time_to"] * 100) / 100; //夜勤専従の確認用 20120312
                        }
						if ($k < $day_cnt-1) {
                            $wk_rslt_sumtime += round($time_array["night_time_next"] * 100) / 100;
                            $wk_rslt_sumtime_day += round($time_array["duty_time_next"] * 100) / 100;
                            $wk_rslt_time_day += round($time_array["duty_time_next"] * 100) / 100; //夜勤専従の確認用 20120312
                        }
					}
					//前日
					else {
						if ($k > 2) {
                            $wk_rslt_sumtime += round($time_array["night_time_to"] * 100) / 100;
                            $wk_rslt_sumtime_day += round($time_array["duty_time_to"] * 100) / 100;
                            $wk_rslt_time_day += round($time_array["duty_time_to"] * 100) / 100; //夜勤専従の確認用 20120312
                        }
						if ($k > 1 && $k < $day_cnt) {
                            $wk_rslt_sumtime += round($time_array["night_time_next"] * 100) / 100;
                            $wk_rslt_sumtime_day += round($time_array["duty_time_next"] * 100) / 100;
                            $wk_rslt_time_day += round($time_array["duty_time_next"] * 100) / 100; //夜勤専従の確認用 20120312
                        }

					}
					//debug
					//echo("<br>$k $wk_date to=".$time_array["night_time_to"]);
					//echo(" next=".$time_array["night_time_next"]);
					$wk_plan_time = 0.0;
					$wk_plan_time_day = 0.0;
				} else {
					///-----------------------------------------------------------------------------
					//実績（夜勤）か判定
					///-----------------------------------------------------------------------------
					$wk_flg = "";
					//echo(" tmp_pattern_id=$tmp_pattern_id");
					//echo(" tmp_atdptn_id=$tmp_atdptn_id");

					//休暇以外
					if ($tmp_atdptn_id != "10" && $tmp_atdptn_id != "" &&
							$rslt_workday_count != 0 && $rslt_after_night_duty_flag != 1) {
						$wk_flg = "1";
					}
					if ($wk_flg == "1") {
						$wk_pattern_id = $plan_array[$i]["rslt_pattern_id_$k"];
						$wk_atdptn_id = $plan_array[$i]["rslt_id_$k"];
                        //日付
                        $wk_date = $calendar_array[$k - 1]["date"];

                        //個人別所定時間を優先する場合 20120309
                        if ($this->arr_empcond_officehours_ptn[$wk_pattern_id][$wk_atdptn_id] == 1) {
                            $weekday_flg = $this->arr_weekday_flg[$wk_date];
                            $arr_empcond_officehours = $this->get_empcond_officehours($weekday_flg,$wk_date); //個人別勤務時間帯履歴対応 20130220
                            if ($arr_empcond_officehours["officehours2_start"] != "") {
                                $wk_prov_start_hhmm = $arr_empcond_officehours["officehours2_start"];
                                $wk_prov_end_hhmm = $arr_empcond_officehours["officehours2_end"];
                            }
                        }
                        else {
                            $wk_officehours_id = $plan_array[$i]["officehours_id_$k"];
                            $wk_officehours_id = ($wk_officehours_id == 2 || $wk_officehours_id == 3) ? $wk_officehours_id : 1;
                            if ($wk_atdptn_id >= 1) {
                                $wk_pattern_idx = $wk_pattern_id - 1;
                                $wk_start_hhmm = "start_hhmm_" . $wk_atdptn_id . "_" . $wk_officehours_id . "_2";
                                $wk_end_hhmm = "end_hhmm_" . $wk_atdptn_id . "_" . $wk_officehours_id . "_2";
                                $wk_prov_start_hhmm = $officehours_array[$wk_pattern_idx][$wk_start_hhmm];
                                $wk_prov_end_hhmm = $officehours_array[$wk_pattern_idx][$wk_end_hhmm];
                            }
                        }
                        ///-----------------------------------------------------------------------------
						//予定用の夜勤時間算出
						///-----------------------------------------------------------------------------
						$time_array = $this->get_duty_night_times(
								$plan_array[$i]["officehours_id_$k"],
								$plan_array[$i]["rslt_pattern_id_$k"],
								$plan_array[$i]["rslt_id_$k"],
								$wk_prov_start_hhmm,
								$wk_prov_end_hhmm,
								$data_standard,
								$officehours_array,
								"0000",
								$plan_array[$i]["job_id"],
								$arr_no_subtract,
								"",
								"",
								"",
								"",
								$wk_hol_hour_start_time,
								$wk_hol_hour_end_time,
                                $wk_date
								);

						//当日
						if ($previous_day_flag_array["$wk_pattern_id"]["$wk_atdptn_id"] != "1") {
							if ($k > 1 && $k < $day_cnt) { //月末) {
                                $wk_rslt_sumtime += round($time_array["night_time_to"] * 100) / 100;
                                $wk_rslt_sumtime_day += round($time_array["duty_time_to"] * 100) / 100;
                                $wk_rslt_time_day += round($time_array["duty_time_to"] * 100) / 100; //夜勤専従の確認用 20120312
                            }
							if ($k < $day_cnt-1) {
                                $wk_rslt_sumtime += round($time_array["night_time_next"] * 100) / 100;
                                $wk_rslt_sumtime_day += round($time_array["duty_time_next"] * 100) / 100;
                                $wk_rslt_time_day += round($time_array["duty_time_next"] * 100) / 100; //夜勤専従の確認用 20120312
                            }
						}
						//前日
						else {
							if ($k > 2) {
                                $wk_rslt_sumtime += round($time_array["night_time_to"] * 100) / 100;
                                $wk_rslt_sumtime_day += round($time_array["duty_time_to"] * 100) / 100;
                                $wk_rslt_time_day += round($time_array["duty_time_to"] * 100) / 100; //夜勤専従の確認用 20120312
                            }
							if ($k > 1 && $k < $day_cnt) {
                                $wk_rslt_sumtime += round($time_array["night_time_next"] * 100) / 100;
                                $wk_rslt_sumtime_day += round($time_array["duty_time_next"] * 100) / 100;
                                $wk_rslt_time_day += round($time_array["duty_time_next"] * 100) / 100; //夜勤専従の確認用 20120312
                            }
						}
						//debug
						//echo(" wk_rslt_sumtime=$wk_rslt_sumtime");
						$wk_plan_time = 0.0;
						$wk_plan_time_day = 0.0;
					} else {
						///-----------------------------------------------------------------------------
						//実績のシフトが未設定の場合、予定の夜勤を加算
						///-----------------------------------------------------------------------------
						//予定パターンの日数換算が0以外を条件追加 2008/6/12
						if (($plan_array[$i]["atdptn_ptn_id_$k"] != "") &&
								($plan_array[$i]["rslt_id_$k"] == "")
								&&	$plan_workday_count != 0
								&& $after_night_duty_flag != 1
							) {
							$wk_rslt_sumtime += $wk_plan_time;
							$wk_rslt_sumtime_day += $wk_plan_time_day;
                            $wk_rslt_time_day += $wk_plan_time_day; //夜勤専従の確認用 20120312
                            //debug
							//echo(" 2 $wk_date wk_rslt_sumtime=$wk_rslt_sumtime");
						}
					}
				}
				//}
                //夜勤専従の確認 1時間以上は夜勤専従ではない 20120312
                if ($wk_rslt_time_day >= 1) {
                    $wk_rslt_time_day_flg = false;
                }
            }
			//夜勤専従を自動計算 20120223
			//予定
			$wk_plan_night_duty_flg = false;
            if ($wk_plan_time_day_flg) {
				$wk_plan_night_duty_flg = $this->check_night_duty_flg($plan_array[$i]["staff_id"], $calendar_array, $officehours_array, $data_standard, "2");

			}
			//実績
			$wk_rslt_night_duty_flg = false;
            if ($wk_rslt_time_day_flg) {
				$wk_rslt_night_duty_flg = $this->check_night_duty_flg($plan_array[$i]["staff_id"], $calendar_array, $officehours_array, $data_standard, "1");

			}

			///-----------------------------------------------------------------------------
			//夜勤時間対象スタッフ毎の夜勤時間合計算出
			//夜勤合計が１６時間以下は対象外
			//夜勤時間対象スタッフ数をカウント（兼務者、パートは常勤換算（0.5人））、時間数から換算 20120217
			///-----------------------------------------------------------------------------
			//予定
			//夜勤専従以外 20120223
            if ((($plan_array[$i]["duty_form"] != "3" && (int)$wk_plan_sumtime > 16) || //常勤、非常勤で16時間超 20120523
                        ($plan_array[$i]["duty_form"] == "3" && (int)$wk_plan_sumtime >= 12)) && //短時間で12時間以上
                    $wk_plan_night_duty_flg == false) {//夜勤専従以外
				if ($plan_array[$i]["night_staff_cnt"] != "") {
					$plan_staff_cnt += $plan_array[$i]["night_staff_cnt"];
				} else {
					//"other_post_flg"他部署兼務（１：有り）
					//"duty_form"勤務形態（1:常勤、2:非常勤）
                    if (($plan_array[$i]["other_post_flg"] == "1") ||
                            ($plan_array[$i]["duty_form"] == "2") ||
                            ($plan_array[$i]["duty_form"] == "3") || //短時間
                            ($plan_array[$i]["duty_form"] == "")) {
						//$plan_staff_cnt += 0.5;
                        //時間数から率を計算、小数点第3位以下四捨五入へ変更 20120315
						$labor_cnt = $data_standard["labor_cnt"];
                        $wk_rate = round((($wk_plan_sumtime_day + $wk_plan_sumtime) / (($day_cnt-2) * $labor_cnt / 7)) * 100) / 100;
						if ($wk_rate > 1) {
							$wk_rate = 1;
						}
						$plan_staff_cnt += $wk_rate;

					} else {
						$plan_staff_cnt += 1;
					}
				}
				$plan_sumtime += $wk_plan_sumtime;
			}
			//実績
            if ((($plan_array[$i]["duty_form"] != "3" && (int)$wk_rslt_sumtime > 16) || //常勤、非常勤で16時間超 20120523
                        ($plan_array[$i]["duty_form"] == "3" && (int)$wk_rslt_sumtime >= 12)) && //短時間で12時間以上
                     $wk_rslt_night_duty_flg == false) { //夜勤専従以外
				if ($plan_array[$i]["night_staff_cnt"] != "") {
					$rslt_staff_cnt += $plan_array[$i]["night_staff_cnt"];
				} else {
					//"other_post_flg"他部署兼務（１：有り）
					//"duty_form"勤務形態（1:常勤、2:非常勤）
					if (($plan_array[$i]["other_post_flg"] == "1") ||
                            ($plan_array[$i]["duty_form"] == "2") ||
                            ($plan_array[$i]["duty_form"] == "3") || //短時間
                            ($plan_array[$i]["duty_form"] == "")) {
						//$rslt_staff_cnt += 0.5;
                        //時間数から率を計算、小数点第3位以下四捨五入へ変更 20120315
						$labor_cnt = $data_standard["labor_cnt"];
                        $wk_rate = round((($wk_rslt_sumtime_day + $wk_rslt_sumtime) / (($day_cnt-2) * $labor_cnt / 7)) * 100) / 100;
						if ($wk_rate > 1) {
							$wk_rate = 1;
						}
						$rslt_staff_cnt += $wk_rate;
					} else {
						$rslt_staff_cnt += 1;
					}
				}
				$rslt_sumtime += $wk_rslt_sumtime;
			}
			//}
		}
        //夜勤従事者数の計は小数点1桁まで 20120315
        $rslt_staff_cnt = floor($rslt_staff_cnt * 10) / 10;
        $plan_staff_cnt = floor($plan_staff_cnt * 10) / 10;
        ///-----------------------------------------------------------------------------
		//月平均夜勤時間（予定／実績）の設定
		///-----------------------------------------------------------------------------
		if ($rslt_flg == "1") {
			//小数点２桁まで表示（四捨五入->切捨て）
			$wk = $rslt_sumtime / $rslt_staff_cnt;
			$wk *= 100;
			$wk = (int)($wk); // + 0.5
			$wk /= 100;
			$ret = sprintf("%01.2f",$wk);

			//デバック
//			$ret = $rslt_sumtime . "_" . $rslt_staff_cnt;
//			$ret = $rslt_staff_cnt;
		} else {
			//小数点２桁まで表示（四捨五入->切捨て）
			$wk = $plan_sumtime / $plan_staff_cnt;
			$wk *= 100;
			$wk = (int)($wk); // + 0.5
			$wk /= 100;
			$ret = sprintf("%01.2f",$wk);

			//デバック
//			$ret = $plan_sumtime . "_" . $plan_staff_cnt;
		}
		///-----------------------------------------------------------------------------
		//デバック
		///-----------------------------------------------------------------------------
//		$ret = "";
//		$ret .= " prov_start_hhmm=" . $time_array["prov_start_hhmm"];
//		$ret .= " prov_end_hhmm=" . $time_array["prov_end_hhmm"];
//		$ret .= " officehours_start_hhmm=" . $time_array["officehours_start_hhmm"];
//		$ret .= " officehours_end_hhmm=" . $time_array["officehours_end_hhmm"];
//		$ret .= " start_hhmm=" . $time_array["wk_start_hhmm"];
//		$ret .= " end_hhmm=" . $time_array["wk_end_hhmm"];

		return $ret;
	}

	/**
	 * show_timecard_commoin.ini create_time_array($start_time, $end_time)のyyyyMMdd hhmm版
	 * 年月日を追加することで、２４時間を超える作業時間に対応させる
	 * @param $start_date 開始日時 yyyyMMddhhmm または yyyyMMddhh:mm
	 * @param $end_date   終了日時 yyyyMMddhhmm または yyyyMMddhh:mm
	 */
	function get_minute_time_array($start_date_time, $end_date_time){

		$result_array = array();

		if ($start_date_time != "" && $end_date_time != "") {
			$start_timestamp = date_utils::to_timestamp_from_ymdhi($start_date_time);
			$end_timestamp   = date_utils::to_timestamp_from_ymdhi($end_date_time);

			//分単位で開始から終了時間をarrayに保持する
			$work_timestamp = $start_timestamp;
			while ($work_timestamp < $end_timestamp && empty($work_timestamp) == false) {
/*
				//キーと値を同じ値にする
				$ymdhi = date("YmdHi", $work_timestamp);
				$result_array[$ymdhi] = $ymdhi;
*/
				array_push($result_array, date("YmdHi", $work_timestamp));
				$work_timestamp = strtotime("+1 minute", $work_timestamp);
			}
		}

		return $result_array;
	}

	/**
	 * 当日／翌日、日勤／夜勤の時間を算出
	 *
	 * @param mixed $duty_start_time 出勤時刻
	 * @param mixed $duty_end_time 退勤時刻
	 * @param mixed $data_standard 施設基準
	 * @param mixed $out_time 外出時刻
	 * @param mixed $ret_time 復帰時刻
	 * @param mixed $send_flg 申し送りフラグ
	 * @param mixed $meeting_start_time 会議・研修開始時刻
	 * @param mixed $meeting_end_time 会議・研修終了時刻
	 * @param mixed $hol_hour_flag 時間有休フラグ 0:通常 1:時間有休
	 * @return mixed This is the return value description
	 *
	 */
	function get_day_night_times(
		$duty_start_time,
		$duty_end_time,
		$data_standard,
		$out_time,
		$ret_time,
		$send_flg,
		$meeting_start_time,
		$meeting_end_time,
		$hol_hour_flag
	) {
		$duty_time_to = 0.0;				//日勤時間（当日）
		$duty_time_next = 0.0;				//日勤時間（翌日）
		$night_time_to = 0.0;				//夜勤時間（当日）
		$night_time_next = 0.0;				//夜勤時間（翌日）

		$prov_start_hhmm = $data_standard["prov_start_hhmm"];	//夜勤時間帯（開始時刻）
		$prov_end_hhmm = $data_standard["prov_end_hhmm"];		//夜勤時間帯（終了時刻）
		if ($data_standard["prov_start_hhmm"] == "") { $prov_start_hhmm = "0000"; }
		if ($data_standard["prov_end_hhmm"] == "") { $prov_end_hhmm = "0000"; }

		//配列方式時間計算用
		$today = date("Ymd");
		$nextday = date("Ymd", strtotime("+1 days"));
		$twoday = date("Ymd", strtotime("+2 days"));

		//勤務時間の配列
		$start_duty_date_time = $today.$duty_start_time;
		if ($duty_start_time <= $duty_end_time) {
			$end_duty_date_time = $today.$duty_end_time;
		} else {
			$end_duty_date_time = $nextday.$duty_end_time;
		}
		//夜勤時間帯が未設定の場合、全てを日勤とする
		if (($prov_start_hhmm == "0000") && ($prov_end_hhmm == "0000")) {
			$wk_prov_today_start_hhmm = $today."0000";
			$wk_prov_today_end_hhmm = $nextday."0000";
			$wk_prov_nextday_start_hhmm = $nextday."0000";
			$wk_prov_nextday_end_hhmm = $twoday."0000";
		} else {
			if ((int)$prov_start_hhmm <= (int)$prov_end_hhmm) {
				$wk_prov_today_start_hhmm = $today.$prov_start_hhmm;
				$wk_prov_today_end_hhmm = $today.$prov_end_hhmm;
				$wk_prov_nextday_start_hhmm = $nextday.$prov_start_hhmm;
				$wk_prov_nextday_end_hhmm = $nextday.$prov_end_hhmm;
			} else {
				$wk_prov_today_start_hhmm = $today.$prov_end_hhmm;
				$wk_prov_today_end_hhmm = $today.$prov_start_hhmm;
				$wk_prov_nextday_start_hhmm = $nextday.$prov_end_hhmm;
				$wk_prov_nextday_end_hhmm = $nextday.$prov_start_hhmm;
			}
		}
		//外出、会議研修が未入力の場合
		if ($out_time == "" && $ret_time == "" &&
			$meeting_start_time  == "" && $meeting_end_time == "") {
			//退勤日時、申し送り確認用
			if ($duty_start_time > $duty_end_time) {
				$wk_duty_end_date_time = $nextday.$duty_end_time;
			} else {
				$wk_duty_end_date_time = $today.$duty_end_time;
			}
			//時刻から時間計算
			//当日日勤
			$duty_time_to = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $wk_prov_today_start_hhmm, $wk_prov_today_end_hhmm);
			/*
			if ($send_flg &&
					($wk_prov_today_start_hhmm <= $wk_duty_end_date_time &&
						$wk_duty_end_date_time <= $wk_prov_today_end_hhmm)) {
				//申し送り
				$duty_time_to -= $this->get_msg_times2($duty_start_time, $duty_end_time, $data_standard);
			}
			$duty_time_to = $duty_time_to / 60;
			*/
			//翌日日勤
			$duty_time_next = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $wk_prov_nextday_start_hhmm, $wk_prov_nextday_end_hhmm);
/*
			if ($send_flg &&
					($wk_prov_nextday_start_hhmm <= $wk_duty_end_date_time &&
						$wk_duty_end_date_time <= $wk_prov_nextday_end_hhmm)) {
				//申し送り
				$duty_time_next -= $this->get_msg_times2($duty_start_time, $duty_end_time, $data_standard);
			}
			$duty_time_next = $duty_time_next / 60;
			*/
			$today_night_start_hhmm1 = "";
			$today_night_end_hhmm1 = "";
			$today_night_start_hhmm2 = "";
			$today_night_end_hhmm2 = "";
			$nextday_night_start_hhmm1 = "";
			$nextday_night_end_hhmm1 = "";
			$nextday_night_start_hhmm2 = "";
			$nextday_night_end_hhmm2 = "";
			if (($prov_start_hhmm == "0000") && ($prov_end_hhmm == "0000")) {
				$night_time_to = 0;
				$night_time_next = 0;
			} else {
				if ((int)$prov_start_hhmm <= (int)$prov_end_hhmm) {
					$today_night_start_hhmm1 = $today.$prov_start_hhmm;
					$today_night_end_hhmm1 = $today.$prov_end_hhmm;
					if ($duty_start_time > $duty_end_time) {
						$nextday_night_start_hhmm1 = $nextday.$prov_start_hhmm;
						$nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
					}
				} else {
					$today_night_start_hhmm1 = $today."0000";
					$today_night_end_hhmm1 = $today.$prov_end_hhmm;
					$today_night_start_hhmm2 = $today.$prov_start_hhmm;
					$today_night_end_hhmm2 = $nextday."0000";

					if ($duty_start_time > $duty_end_time) {
						$nextday_night_start_hhmm1 = $nextday."0000";
						$nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
						$nextday_night_start_hhmm2 = $nextday.$prov_start_hhmm;
						$nextday_night_end_hhmm2 = $twoday."0000";
					}
				}
				//当日夜勤
				$night_time_to = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $today_night_start_hhmm1, $today_night_end_hhmm1);
				if ($today_night_start_hhmm2 != "" && $today_night_end_hhmm2 != "") {
					$night_time_to += $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $today_night_start_hhmm2, $today_night_end_hhmm2);
				}
/*
				if ($send_flg &&
						(($today_night_start_hhmm1 <= $wk_duty_end_date_time &&
								$wk_duty_end_date_time <= $today_night_end_hhmm1) ||
							($today_night_start_hhmm2 <= $wk_duty_end_date_time &&
								$wk_duty_end_date_time <= $today_night_end_hhmm2)
							)) {
					//申し送り
					$night_time_to -= $this->get_msg_times2($duty_start_time, $duty_end_time, $data_standard);
				}
*/
				//翌日夜勤
				$night_time_next = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $nextday_night_start_hhmm1, $nextday_night_end_hhmm1);
				if ($nextday_night_start_hhmm2 != "" && $nextday_night_end_hhmm2 != "") {
					$night_time_next += $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $nextday_night_start_hhmm2, $nextday_night_end_hhmm2);
				}
/*
				if ($send_flg &&
						(($nextday_night_start_hhmm1 <= $wk_duty_end_date_time &&
								$wk_duty_end_date_time <= $nextday_night_end_hhmm1) ||
							($nextday_night_start_hhmm2 <= $wk_duty_end_date_time &&
								$wk_duty_end_date_time <= $nextday_night_end_hhmm2)
							)) {
					//申し送り
					$night_time_next -= $this->get_msg_times2($duty_start_time, $duty_end_time, $data_standard);
				}
				$night_time_next = $night_time_next / 60;
*/
			}

			//上の処理のコメント化
			//申し送り時間を減算
			if ($send_flg) {
				//申し送り時間を日勤、夜勤時間帯別に取得する
				//日勤夜勤時間帯情報
				$time_info = array();
				$time_info["today_start_hhmm"] = $wk_prov_today_start_hhmm;
				$time_info["today_end_hhmm"] = $wk_prov_today_end_hhmm;
				$time_info["nextday_start_hhmm"] = $wk_prov_nextday_start_hhmm;
				$time_info["nextday_end_hhmm"] = $wk_prov_nextday_end_hhmm;
				$time_info["today_night_start_hhmm1"] = $today_night_start_hhmm1;
				$time_info["today_night_end_hhmm1"] = $today_night_end_hhmm1;
				$time_info["today_night_start_hhmm2"] = $today_night_start_hhmm2;
				$time_info["today_night_end_hhmm2"] = $today_night_end_hhmm2;
				$time_info["nextday_night_start_hhmm1"] = $nextday_night_start_hhmm1;
				$time_info["nextday_night_end_hhmm1"] = $nextday_night_end_hhmm1;
				$time_info["nextday_night_start_hhmm2"] = $nextday_night_start_hhmm2;
				$time_info["nextday_night_end_hhmm2"] = $nextday_night_end_hhmm2;

				$arr_day_night_send_times = $this->get_day_night_send_times($start_duty_date_time, $end_duty_date_time, $time_info, $data_standard, "", "", "", "", $hol_hour_flag);
				//申し送り時間を減算
				$duty_time_to -= $arr_day_night_send_times["duty_time_to"];
				$duty_time_next -= $arr_day_night_send_times["duty_time_next"];
				$night_time_to -= $arr_day_night_send_times["night_time_to"];
				$night_time_next -= $arr_day_night_send_times["night_time_next"];

			}
			$duty_time_to = $duty_time_to / 60;
			$duty_time_next = $duty_time_next / 60;
			$night_time_to = $night_time_to / 60;
			$night_time_next = $night_time_next / 60;

		} else {
			//外出、会議・研修がある場合
			$arr_minute_duty_time = $this->get_minute_time_array($start_duty_date_time, $end_duty_date_time);

			//外出時間を除外する
			$arr_time_out_ret = array();
			if ($out_time != "" && $ret_time != "") {
				$start_out_date_time = $today.$out_time;
				if ($out_time > $ret_time) {
					$end_out_date_time = $nextday.$ret_time;
				} else {
					$end_out_date_time = $today.$ret_time;
				}
				$arr_time_out_ret = $this->get_minute_time_array($start_out_date_time, $end_out_date_time);
				$arr_minute_duty_time = array_diff($arr_minute_duty_time, $arr_time_out_ret);
			}
			//会議・研修時間を除外する
			$arr_meeting_time = array();
			if ($meeting_start_time != "" && $meeting_end_time != "") {
				$meeting_start_date_time = $today.$meeting_start_time;
				if ($meeting_start_time <= $meeting_end_time) {
					$meeting_end_date_time = $today.$meeting_end_time;
				} else {
					$meeting_end_date_time = $nextday.$meeting_end_time;
				}
				$arr_meeting_time = $this->get_minute_time_array($meeting_start_date_time, $meeting_end_date_time);
				$arr_minute_duty_time = array_diff($arr_minute_duty_time, $arr_meeting_time);
			}


			//退勤日時、申し送り確認用
			if ($duty_start_time > $duty_end_time) {
				$wk_duty_end_date_time = $nextday.$duty_end_time;
			} else {
				$wk_duty_end_date_time = $today.$duty_end_time;
			}
			//当日日勤の配列
			$arr_minute_today_time = $this->get_minute_time_array($wk_prov_today_start_hhmm, $wk_prov_today_end_hhmm);
			$duty_time_to = count(array_intersect($arr_minute_duty_time, $arr_minute_today_time));				//日勤時間（当日）
			/*
			if ($send_flg &&
					($wk_prov_today_start_hhmm <= $wk_duty_end_date_time &&
						$wk_duty_end_date_time <= $wk_prov_today_end_hhmm)) {
				//申し送り
				$duty_time_to -= $this->get_msg_times($duty_start_time, $duty_end_time, $data_standard, $arr_time_out_ret);
			}
			$duty_time_to = $duty_time_to / 60;
			*/
			//翌日日勤の配列
			$arr_minute_nextday_time = array();
			if ($duty_start_time > $duty_end_time) {
				$arr_minute_nextday_time = $this->get_minute_time_array($wk_prov_nextday_start_hhmm, $wk_prov_nextday_end_hhmm);
				$duty_time_next = count(array_intersect($arr_minute_duty_time, $arr_minute_nextday_time));				//日勤時間（翌日）
				/*
				if ($send_flg &&
						($wk_prov_nextday_start_hhmm <= $wk_duty_end_date_time &&
							$wk_duty_end_date_time <= $wk_prov_nextday_end_hhmm)) {
					//申し送り
					$duty_time_next -= $this->get_msg_times($duty_start_time, $duty_end_time, $data_standard, $arr_time_out_ret);
				}
				$duty_time_next = $duty_time_next / 60;
				*/
			}
			//夜勤
			$arr_minute_today_night_time = array();
			$arr_minute_nextday_night_time = array();
			$today_night_start_hhmm1 = "";
			$today_night_end_hhmm1 = "";
			$today_night_start_hhmm2 = "";
			$today_night_end_hhmm2 = "";
			$nextday_night_start_hhmm1 = "";
			$nextday_night_end_hhmm1 = "";
			$nextday_night_start_hhmm2 = "";
			$nextday_night_end_hhmm2 = "";
			if (($prov_start_hhmm == "0000") && ($prov_end_hhmm == "0000")) {
			} else {
				if ((int)$prov_start_hhmm <= (int)$prov_end_hhmm) {
					$today_night_start_hhmm1 = $today.$prov_start_hhmm;
					$today_night_end_hhmm1 = $today.$prov_end_hhmm;
					$arr_minute_today_night_time = $this->get_minute_time_array($today_night_start_hhmm1, $today_night_end_hhmm1);
					if ($duty_start_time > $duty_end_time) {
						$nextday_night_start_hhmm1 = $nextday.$prov_start_hhmm;
						$nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
						$arr_minute_nextday_night_time = $this->get_minute_time_array($nextday_night_start_hhmm1, $nextday_night_end_hhmm1);
					}
				} else {
					$today_night_start_hhmm1 = $today."0000";
					$today_night_end_hhmm1 = $today.$prov_end_hhmm;
					$today_night_start_hhmm2 = $today.$prov_start_hhmm;
					$today_night_end_hhmm2 = $nextday."0000";
					$arr_minute_today_night_time1 = $this->get_minute_time_array($today_night_start_hhmm1, $today_night_end_hhmm1);


					$arr_minute_today_night_time2 = $this->get_minute_time_array($today_night_start_hhmm2, $today_night_end_hhmm2);


					$arr_minute_today_night_time = array_merge($arr_minute_today_night_time1, $arr_minute_today_night_time2);

					if ($duty_start_time > $duty_end_time) {
						$nextday_night_start_hhmm1 = $nextday."0000";
						$nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
						$nextday_night_start_hhmm2 = $nextday.$prov_start_hhmm;
						$nextday_night_end_hhmm2 = $twoday."0000";
						$arr_minute_nextday_night_time1 = $this->get_minute_time_array($nextday_night_start_hhmm1, $nextday_night_end_hhmm1);
						$arr_minute_nextday_night_time2 = $this->get_minute_time_array($nextday_night_start_hhmm2, $nextday_night_end_hhmm2);
						$arr_minute_nextday_night_time = array_merge($arr_minute_nextday_night_time1, $arr_minute_nextday_night_time2);
					}
				}
				$night_time_to = count(array_intersect($arr_minute_duty_time, $arr_minute_today_night_time));				//夜勤時間（当日）
				/*
				if ($send_flg &&
						(($today_night_start_hhmm1 <= $wk_duty_end_date_time &&
								$wk_duty_end_date_time <= $today_night_end_hhmm1) ||
							($today_night_start_hhmm2 <= $wk_duty_end_date_time &&
								$wk_duty_end_date_time <= $today_night_end_hhmm2)
							)) {
					//申し送り
					$night_time_to -= $this->get_msg_times($duty_start_time, $duty_end_time, $data_standard, $arr_time_out_ret);
				}
				$night_time_to = $night_time_to / 60;
				*/
				if ($duty_start_time > $duty_end_time) {
					$night_time_next = count(array_intersect($arr_minute_duty_time, $arr_minute_nextday_night_time));				//夜勤時間（翌日）
					/*
					if ($send_flg &&
							(($nextday_night_start_hhmm1 <= $wk_duty_end_date_time &&
									$wk_duty_end_date_time <= $nextday_night_end_hhmm1) ||
								($nextday_night_start_hhmm2 <= $wk_duty_end_date_time &&
									$wk_duty_end_date_time <= $nextday_night_end_hhmm2)
								)) {
						//申し送り
						$night_time_next -= $this->get_msg_times($duty_start_time, $duty_end_time, $data_standard, $arr_time_out_ret);
					}
					$night_time_next = $night_time_next / 60;
					*/
				}
			}
			//申し送り時間を減算
			if ($send_flg) {
				//申し送り時間を日勤、夜勤時間帯別に取得する
				//日勤夜勤時間帯情報
				$time_info = array();
				$time_info["today_start_hhmm"] = $wk_prov_today_start_hhmm;
				$time_info["today_end_hhmm"] = $wk_prov_today_end_hhmm;
				$time_info["nextday_start_hhmm"] = $wk_prov_nextday_start_hhmm;
				$time_info["nextday_end_hhmm"] = $wk_prov_nextday_end_hhmm;
				$time_info["today_night_start_hhmm1"] = $today_night_start_hhmm1;
				$time_info["today_night_end_hhmm1"] = $today_night_end_hhmm1;
				$time_info["today_night_start_hhmm2"] = $today_night_start_hhmm2;
				$time_info["today_night_end_hhmm2"] = $today_night_end_hhmm2;
				$time_info["nextday_night_start_hhmm1"] = $nextday_night_start_hhmm1;
				$time_info["nextday_night_end_hhmm1"] = $nextday_night_end_hhmm1;
				$time_info["nextday_night_start_hhmm2"] = $nextday_night_start_hhmm2;
				$time_info["nextday_night_end_hhmm2"] = $nextday_night_end_hhmm2;

				$arr_day_night_send_times = $this->get_day_night_send_times($start_duty_date_time, $end_duty_date_time, $time_info, $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag);
				//申し送り時間を減算
				$duty_time_to -= $arr_day_night_send_times["duty_time_to"];
				$duty_time_next -= $arr_day_night_send_times["duty_time_next"];
				$night_time_to -= $arr_day_night_send_times["night_time_to"];
				$night_time_next -= $arr_day_night_send_times["night_time_next"];
			}
			$duty_time_to = $duty_time_to / 60;
			$duty_time_next = $duty_time_next / 60;
			$night_time_to = $night_time_to / 60;
			$night_time_next = $night_time_next / 60;
		}

		///-----------------------------------------------------------------------------
		//日勤／夜勤時間を設定
		///-----------------------------------------------------------------------------
		$data = array();
		$data["duty_time_to"] = 0.0;		//日勤時間（当日）
		$data["duty_time_next"] = 0.0;		//日勤時間（翌日）
		$data["night_time_to"] = 0.0;		//夜勤時間（当日）
		$data["night_time_next"] = 0.0;		//夜勤時間（翌日）
		if ($duty_time_to > 0) {	$data["duty_time_to"] = $duty_time_to; }		//日勤時間（当日）
		if ($duty_time_next> 0) {	$data["duty_time_next"] = $duty_time_next; }	//日勤時間（翌日）
		if ($night_time_to > 0) {	$data["night_time_to"] = $night_time_to; }		//夜勤時間（当日）
		if ($night_time_next > 0) { $data["night_time_next"] = $night_time_next; }	//夜勤時間（翌日）
		return $data;
	}

	/**
	 * 申し送り時間を日勤、夜勤時間帯別に取得する
	 *
	 * @param mixed $start_duty_date_time 出勤時刻
	 * @param mixed $end_duty_date_time 退勤時刻
	 * @param mixed $time_info 日勤夜勤時間帯情報
	 * @param mixed $data_standard 基準施設情報（グループ指定）
	 * @param mixed $start_out_date_time 外出開始時刻
	 * @param mixed $end_out_date_time 外出終了時刻
	 * @param mixed $meeting_start_date_time 会議開始時刻
	 * @param mixed $meeting_end_date_time 会議終了時刻
	 * @param mixed $hol_hour_start_date_time 時間有休開始時刻flag
	 * @param mixed $hol_hour_end_date_time 時間有休終了時刻
	 * @return array 申し送り時間の配列
	 *
	 */
	function get_day_night_send_times($start_duty_date_time, $end_duty_date_time, $time_info, $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag){

		$arr_ret = array();
		//時間帯との重なりを求めて返す
		$arr_ret["duty_time_to"] = $this->get_send_time($start_duty_date_time, $end_duty_date_time, $time_info["today_start_hhmm"], $time_info["today_end_hhmm"], $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag);
		$arr_ret["duty_time_next"] = $this->get_send_time($start_duty_date_time, $end_duty_date_time, $time_info["nextday_start_hhmm"], $time_info["nextday_end_hhmm"], $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag);

		$night_time_to = 0;
		if ($time_info["today_night_start_hhmm1"] != "") {
			$night_time_to += $this->get_send_time($start_duty_date_time, $end_duty_date_time, $time_info["today_night_start_hhmm1"], $time_info["today_night_end_hhmm1"], $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag);
		}
		if ($time_info["today_night_start_hhmm2"] != "") {
			$night_time_to += $this->get_send_time($start_duty_date_time, $end_duty_date_time, $time_info["today_night_start_hhmm2"], $time_info["today_night_end_hhmm2"], $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag);
		}
		$arr_ret["night_time_to"] = $night_time_to;

		$night_time_next = 0;
		if ($time_info["nextday_night_start_hhmm1"] != "") {
			$night_time_next += $this->get_send_time($start_duty_date_time, $end_duty_date_time, $time_info["nextday_night_start_hhmm1"], $time_info["nextday_night_end_hhmm1"], $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag);
		}
		if ($time_info["nextday_night_start_hhmm2"] != "") {
			$night_time_next += $this->get_send_time($start_duty_date_time, $end_duty_date_time, $time_info["nextday_night_start_hhmm2"], $time_info["nextday_night_end_hhmm2"], $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag);
		}
		$arr_ret["night_time_next"] = $night_time_next;

		return $arr_ret;
	}

	/**
	 * 申し送り時間取得(日勤夜勤帯対応)
	 *
	 * @param mixed $start1 所定開始時刻
	 * @param mixed $end1 所定終了時刻
	 * @param mixed $start2 時間帯開始時刻
	 * @param mixed $end2 時間帯終了時刻
	 * @param mixed $data_standard 基準施設情報（グループ指定）
	 * @param mixed $start_out_date_time 外出開始時刻
	 * @param mixed $end_out_date_time 外出終了時刻
	 * @param mixed $meeting_start_date_time 会議開始時刻
	 * @param mixed $meeting_end_date_time 会議終了時刻
	 * @param mixed $hol_hour_flag 時間有休フラグ 時間有休対応 20120213
	 * @return mixed 申し送り時間（分）
	 *
	 */
	function get_send_time($start1, $end1, $start2, $end2, $data_standard, $start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time, $hol_hour_flag) {

		//所定時間と申し送りの重なりを確認後、日勤時間、夜勤時間の重なりを取得する

		//時間にコロン(:)がある場合消す
		$s1 = str_replace(":", "", $start1);
		$e1 = str_replace(":", "", $end1);
		$s2 = str_replace(":", "", $start2);
		$e2 = str_replace(":", "", $end2);

		//申し送りの時刻
		$wk_start_time = substr($s1, 8, 4);
		$wk_end_time = substr($e1, 8, 4);
		list($wk_start_date_time, $wk_end_date_time) = $this->get_send_start_end_time($wk_start_time, $wk_end_time, $data_standard, $hol_hour_flag);
		if ($wk_start_date_time == "") {
			return 0;
		}

		//入力チェック
		//開始終了時刻が逆の場合、0とする
		if ($wk_start_date_time >= $wk_end_date_time || $s2 >= $e2) {
			return 0;
		}
		//範囲外
		//      s1----e1
		//s2--e2      s2--e2
		if ($e2 <= $wk_start_date_time || $s2 >= $wk_end_date_time) {
			return 0;
		}

		//     s1----e1  s1----e1
		//    s2--e2      s2--e2
		$wk_s = max($wk_start_date_time, $s2);
		$wk_e = min($wk_end_date_time, $e2);

		//申し送り時間（日勤夜勤時間と重複する）を取得
		$minutes = date_utils::get_diff_minute($wk_e, $wk_s);

		//外出、会議が重複するか確認
		$wk_time = $this->get_intersect_times($start_out_date_time, $end_out_date_time, $meeting_start_date_time, $meeting_end_date_time);
		if ($wk_time > 0) {
			$wk_out_meeting_s = min($start_out_date_time, $meeting_start_date_time);
			$wk_out_meeting_e = max($end_out_date_time, $meeting_end_date_time);
			//外出、会議を含む時刻と申し送りの重複時間を取得
			$wk_time_overlap = $this->get_intersect_times($wk_s, $wk_e, $wk_out_meeting_s, $wk_out_meeting_e);
			$minutes -= $wk_time_overlap;
		}
		else {
			//重複がない場合個別に時間計算
			$wk_time_overlap = 0;
			//外出時刻がある場合
			if ($start_out_date_time != "" && $end_out_date_time != "") {
				$wk_time_overlap += $this->get_intersect_times($wk_s, $wk_e, $start_out_date_time, $end_out_date_time);
			}

			//会議時刻がある場合
			if ($meeting_start_date_time != "" && $meeting_end_date_time != "") {
				$wk_time_overlap += $this->get_intersect_times($wk_s, $wk_e, $meeting_start_date_time, $meeting_end_date_time);

			}
			$minutes -= $wk_time_overlap;
		}


		return $minutes;
	}

	/**
	 * 申し送り時刻取得
	 * ※get_msg_times2を参考にしている
	 * @param mixed $start_time 開始時刻
	 * @param mixed $end_time 終了時刻
	 * @param mixed $data_standard 基準施設情報（グループ指定）
	 * @param mixed $hol_hour_flag 時間有休フラグ 時間有休対応 20120213
	 * @return array($start_date_time, $end_date_time)		時刻の配列
	 *
	 */
	function get_send_start_end_time($start_time,
		$end_time,
		$data_standard,
		$hol_hour_flag) {

		$wk_start_date_time = "";
		$wk_end_date_time = "";
		///-----------------------------------------------------------------------------
		//時刻チェック
		///-----------------------------------------------------------------------------
		if (($start_time == "") ||
				($end_time == "") ||
				($start_time == $end_time)) {
			return array("", "");
		}
		///-----------------------------------------------------------------------------
		//申し送り時間
		///-----------------------------------------------------------------------------
		for($i=0; $i<3; $i++) {
			$k = $i + 1;
			$send_start_hhmm[$i] = $data_standard["send_start_hhmm_$k"];	//開始時分
			$send_end_hhmm[$i] = $data_standard["send_end_hhmm_$k"];		//終了時分
		}
		///-----------------------------------------------------------------------------
		//申し送り時刻の取得
		///-----------------------------------------------------------------------------
		for($i=0; $i<3; $i++) {
			if (($send_start_hhmm[$i] != "") &&
					($send_end_hhmm[$i] != "") ) {
				//減算対象か判定
				//所定の日時と申し送りの日時を比較する
				$today = date("Ymd");
				$nextday = date("Ymd", strtotime("+1 days"));
				$prov_start_date_time = $today.$start_time;
				//時間有休以外 20120213
				if ($hol_hour_flag != "1") {
					//開始の2時間後から終了時刻までで判断
					$prov_start_date_time = date("YmdHi", strtotime("+2 hours",date_utils::to_timestamp_from_ymdhi($prov_start_date_time)));
				}
				$prov_end_date_time = ($start_time > $end_time) ? $nextday.$end_time : $today.$end_time;
				$send_start_date_time = ($start_time > $send_start_hhmm[$i]) ? $nextday.$send_start_hhmm[$i] : $today.$send_start_hhmm[$i];
				$send_end_date_time = ($start_time > $send_end_hhmm[$i]) ? $nextday.$send_end_hhmm[$i] : $today.$send_end_hhmm[$i];
				//開始終了の間に申し送りの開始が入っている場合。早退の場合に対応するため終了側は判定しない 20120313
				if ($prov_start_date_time <= $send_start_date_time &&
						$send_start_date_time < $prov_end_date_time) {
					$wk_start_date_time = $send_start_date_time;
					$wk_end_date_time = $send_end_date_time;
					break;
				}
			}
		}
		return array($wk_start_date_time	, $wk_end_date_time);
	}

	/**
	 * 2つの時間帯の重複する時間を取得する
	 *
	 * @param mixed $start1 開始時刻１
	 * @param mixed $end1 終了時刻１
	 * @param mixed $start2 開始時刻２
	 * @param mixed $end2 終了時刻２
	 * @return mixed 分
	 *
	 */
	function get_intersect_times($start1, $end1, $start2, $end2) {

		//時間にコロン(:)がある場合消す
		$s1 = str_replace(":", "", $start1);
		$e1 = str_replace(":", "", $end1);
		$s2 = str_replace(":", "", $start2);
		$e2 = str_replace(":", "", $end2);

		//入力チェック
		//開始終了時刻が逆の場合、0とする
		if ($s1 >= $e1 || $s2 >= $e2) {
			return 0;
		}
		//範囲外
		//      s1----e1
		//s2--e2      s2--e2
		if ($e2 <= $s1 || $s2 >= $e1) {
			return 0;
		}

		//     s1----e1  s1----e1
		//    s2--e2      s2--e2
		$wk_s = max($s1, $s2);
		$wk_e = min($e1, $e2);

		$minutes = date_utils::get_diff_minute($wk_e, $wk_s);

		return $minutes;

	}
	/**
	 * 2つの時間帯で重複分を除外した時間を取得する
	 *
	 * @param mixed $start1 開始時刻１
	 * @param mixed $end1 終了時刻１
	 * @param mixed $start2 開始時刻２
	 * @param mixed $end2 終了時刻２
	 * @return mixed 分
	 *
	 */
	function get_diff_times($start1, $end1, $start2, $end2) {

		//時間にコロン(:)がある場合消す
		$s1 = str_replace(":", "", $start1);
		$e1 = str_replace(":", "", $end1);
		$s2 = str_replace(":", "", $start2);
		$e2 = str_replace(":", "", $end2);

		//入力チェック
		//開始時刻２か終了時刻２が空白の場合
		if ($s2 == "" || $e2 == "") {
			$minutes = date_utils::get_diff_minute($e1, $s1);
			return $minutes;
		}

		//開始終了時刻が逆の場合、0とする
		if ($s1 >= $e1 || $s2 >= $e2) {
			return 0;
		}
		//範囲外
		//      s1----e1
		//s2--e2      s2--e2
		if ($e2 <= $s1 || $s2 >= $e1) {
			$minutes = date_utils::get_diff_minute($e1, $s1);
			return $minutes;
		}
		//  s1----e1  全体除外
		// s2------e2
		if ($s2 <= $s1 && $e2 >= $e1) {
			return 0;
		}
		//  s1----e1  前方除外
		// s2--e2
		if ($s2 <= $s1 && $e2 > $s1 && $e2 < $e1) {
			$minutes = date_utils::get_diff_minute($e1, $e2);
			return $minutes;
		}
		//  s1----e1  後方除外
		//     s2--e2
		if ($e2 >= $e1 && $s2 < $e1 && $s2 > $s1) {
			$minutes = date_utils::get_diff_minute($s2, $s1);
			return $minutes;
		}
		//  s1----e1  中央除外
		//   s2--e2
		if ($s2 > $s1 && $e2 < $e1) {
			$minutes = date_utils::get_diff_minute($s2, $s1);
			$minutes += date_utils::get_diff_minute($e1, $e2);
			return $minutes;
		}

	}

	// 申し送り時間を差し引かない職種・勤務パターン取得 1_1_1形式（職種_グループID_勤務パターンID）
	function get_arr_no_subtract() {

		$arr_no_subtract = array();

		$sql = "select * from duty_shift_rpt_no_subtract";
		$cond = "order by no";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);

		for ($i=0; $i<$num; $i++) {
			$arr_no_subtract[$i] = pg_result($sel,$i,"job_id")."_".pg_result($sel,$i,"pattern_id")."_".pg_result($sel,$i,"atdptn_ptn_id");
		}

		return $arr_no_subtract;
	}
	/**
	 * 夜勤専従の確認用サブルーチン 20120220
	 * 実績、予定それぞれの時間確認、1時間以上、日勤がある場合、false:夜勤専従以外とする
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $calendar_array カレンダー情報
	 * @param mixed $officehours_array 時間帯情報
	 * @param mixed $standard_array 施設情報
	 * @param mixed $plan_result_flag 1:実績 2:予定 3:実績予定の組合せ
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @return mixed true:夜勤専従 false:夜勤専従以外
	 *
	 */
	function check_night_duty_flg_sub($emp_id, $calendar_array, $officehours_array, $standard_array, $plan_result_flag, $start_date, $end_date) {
		$wk_flg = true;
		//1時間以上、日勤がある場合、false:夜勤専従以外とする
		//別病棟での勤務の可能性があるため、再度テーブルからデータ取得し確認する

		//実績
		if ($plan_result_flag == "1") {
			$sql = "select ";
			$sql .= "emp_id, date, pattern, tmcd_group_id, start_time, end_time, previous_day_flag ";
			$sql .= "from atdbkrslt ";
			$cond = "where emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date' and pattern != '' and pattern != '10'";
			$cond .= "order by date";
		}
		//予定
		else if ($plan_result_flag == "2") {
			$sql = "select ";
			$sql .= "a.emp_id, a.date, a.pattern, a.reason, a.night_duty, a.tmcd_group_id, replace(k.officehours2_start, ':', '') as start_time, replace(k.officehours2_end, ':', '') as end_time, l.previous_day_flag, j.type, l.over_24hour_flag ";
			$sql .= "from atdbk a ";
			$sql .= "left join calendar j on j.date = a.date ";
			$sql .= "left join officehours k on k.tmcd_group_id = a.tmcd_group_id and k.pattern = a.pattern and k.officehours_id = CAST(case when j.type = '2' or j.type = '3' then j.type else '1' end AS varchar) ";
			$sql .= "left join atdptn l on l.group_id = a.tmcd_group_id and a.pattern = CAST(l.atdptn_id AS varchar) ";
			$sql .= " ";
			$cond = "where a.emp_id = '$emp_id' and (a.date >= '$start_date' and a.date <= '$end_date' and k.officehours2_start != '' and k.officehours2_end != '') ";
			$cond .= " order by a.date";
		}

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		//カレンダーの開始日
		$cal_start_date = $calendar_array[0]["date"];

		//翌月初日確認
		$day_max = count($calendar_array) - 1;

		$num = pg_numrows($sel);
		for ($i=0; $i<$num; $i++) {
			$wk_date = pg_result($sel,$i,"date");
			$wk_pattern_id = pg_result($sel,$i,"tmcd_group_id");
			$wk_pattern = pg_result($sel,$i,"pattern");
			$wk_start_time = pg_result($sel,$i,"start_time");
			$wk_end_time = pg_result($sel,$i,"end_time");
			$wk_previous_day_flag = pg_result($sel,$i,"previous_day_flag");
			//type
			$day_idx = ((date_utils::to_timestamp_from_ymd($wk_date) - date_utils::to_timestamp_from_ymd($cal_start_date)) / (24 * 3600));
			$wk_type = $calendar_array[$day_idx]["type"];
			$wk_officehours_id = ($wk_type == "2" || $wk_type == "3") ? $wk_type : "1";

			if (($wk_start_time != "") &&
					($wk_end_time != "")) {
				//時間取得
				$time_array = $this->get_duty_night_times(
						$wk_officehours_id,
						$wk_pattern_id,
						$wk_pattern,
						$wk_start_time,
						$wk_end_time,
						$standard_array,
						$officehours_array,
						"",
						"",
						array(),
						"",
						"",
						"",
						"",
                        $wk_date
						);
                $wk_duty_time = 0;
				//前日フラグ確認、当日
				if ($wk_previous_day_flag != "1") {
					//前月末除外
					if ($day_idx > 0 && $day_idx < $day_max) {
						$wk_duty_time += sprintf("%01.2f", $time_array["duty_time_to"]);
					}
					if ($day_idx < $day_max) {
						$wk_duty_time += sprintf("%01.2f", $time_array["duty_time_next"]);
					}
				}
				//前日
				else {
					//前月末除外
					if ($day_idx > 1) {
						$wk_duty_time += sprintf("%01.2f", $time_array["duty_time_to"]);
					}
					if ($day_idx > 0 && $day_idx < $day_max) {
						$wk_duty_time += sprintf("%01.2f", $time_array["duty_time_next"]);
					}
				}
                //夜勤専従の確認 1時間以上は夜勤専従ではない
                if ($wk_duty_time >= 1) {
					$wk_flg = false;
					break;
				}
			}

		}
		return $wk_flg;

	}

	/**
	 * 夜勤専従の確認 20120220
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $calendar_array カレンダー情報
	 * @param mixed $officehours_array 時間帯情報
	 * @param mixed $standard_array 施設情報
	 * @param mixed $plan_result_flag 1:実績 2:予定 3:実績予定の組合せ
	 * @return mixed true:夜勤専従 false:夜勤専従以外
	 *
	 */
	function check_night_duty_flg($emp_id, $calendar_array, $officehours_array, $standard_array, $plan_result_flag) {
		$wk_flg = true;

		$start_date = $calendar_array[0]["date"];
		$end_date = $calendar_array[count($calendar_array)-1]["date"];


		//実績、予定
		if ($plan_result_flag == "1" || $plan_result_flag == "2") {
			$wk_flg = $this->check_night_duty_flg_sub($emp_id, $calendar_array, $officehours_array, $standard_array, $plan_result_flag, $start_date, $end_date);
		}
		//予定実績組合せ
		else if ($plan_result_flag == "3") {
			//対象期間と当日の判断
			$today = date("Ymd");

			//過去、実績
			if ($today > $end_date) {
				$wk_flg = $this->check_night_duty_flg_sub($emp_id, $calendar_array, $officehours_array, $standard_array, "1", $start_date, $end_date);
			}
			//未来、予定
			else if ($today <= $start_date) {
				$wk_flg = $this->check_night_duty_flg_sub($emp_id, $calendar_array, $officehours_array, $standard_array, "2", $start_date, $end_date);
			}
			else {
				//前日までの実績
				$work_timestamp = date_utils::to_timestamp_from_ymd($today);
				$last_date =  date("Ymd", strtotime("-1 day", $work_timestamp));
				$wk_flg = $this->check_night_duty_flg_sub($emp_id, $calendar_array, $officehours_array, $standard_array, "1", $start_date, $last_date);
				if ($wk_flg) {
					//当日以降の予定
					$wk_flg = $this->check_night_duty_flg_sub($emp_id, $calendar_array, $officehours_array, $standard_array, "2", $today, $end_date);
				}
			}

		}

		return $wk_flg;

	}

    /**
     * 個人別の所定時間を優先する設定パターン取得
     *
     * @return array パターン配列[group_id][atdptn_id]
     *
     */
    function get_empcond_officehours_pattern() {

        $arr_pattern = array();

        $sql =	"SELECT group_id, atdptn_id, atdptn_nm  FROM atdptn ";
        $cond = "WHERE empcond_officehours_flag = 1";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num =pg_num_rows($sel);
        for ($i=0; $i<$num; $i++) {
            $group_id = pg_fetch_result($sel, $i, "group_id");
            $atdptn_id = pg_fetch_result($sel, $i, "atdptn_id");
            $atdptn_nm = pg_fetch_result($sel, $i, "atdptn_nm");
            $arr_pattern[$group_id][$atdptn_id] = "1";
        }

        $this->arr_empcond_officehours_ptn = $arr_pattern;
        return $arr_pattern;

    }

    /**
        * 曜日情報取得
     *
     * @param mixed $start_date 開始日
     * @param mixed $end_date 終了日
     * @return mixed 曜日の配列、キーはYYYYMMDD型日付、0:日 6:土 7:祝日
     *
     */
    function get_arr_weekday_flag($start_date, $end_date) {

        $arr_weekday_flag = array();

        $wk_date = $start_date;
        while ($wk_date < $end_date) {
            $holday_name = get_holiday_name($wk_date);
            $timestamp = date_utils::to_timestamp_from_ymd($wk_date);
            //祝日
            if ($holday_name != "") {
                $arr_weekday_flag[$wk_date] = "7";
            }
            else {
                $arr_weekday_flag[$wk_date] = date("w", $timestamp);
            }
            $wk_date = date("Ymd", strtotime("+1 day", $timestamp));

        }
        $this->arr_weekday_flg = $arr_weekday_flag;
        return $arr_weekday_flag;
    }

    /**
     * 個人別勤務時間帯取得
     *
     * @param string $emp_id 職員ID
     * @param string $start_date 開始日付、履歴取得用、処理する期間の最初の日を設定、省略の場合は履歴を取得しない 20130220
     * @return array 個人別勤務時間帯の配列、曜日別に所定時刻、休憩時刻を設定
     * ※時刻の形式(呼出元の使用方法による)が、出勤表側はhh:mm、勤務シフト作成はhhmm
     */
    function get_arr_empcond_officehours($emp_id, $start_date="") {

        $sql = "select * from empcond_officehours";
        $cond = "where emp_id = '$emp_id' order by weekday";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con, "rollback");
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_num_rows($sel);

        $arr_empcond_officehours = array();
        if ($num > 0) {

            for ($i=0; $i<$num; $i++) {
                $weekday = pg_fetch_result($sel, $i, "weekday");
                $officehours2_start = pg_fetch_result($sel, $i, "officehours2_start");
                $officehours2_end = pg_fetch_result($sel, $i, "officehours2_end");
                $officehours4_start = pg_fetch_result($sel, $i, "officehours4_start");
                $officehours4_end = pg_fetch_result($sel, $i, "officehours4_end");
                //:を除く
                $arr_empcond_officehours[$weekday]["officehours2_start"] = str_replace(":", "", $officehours2_start);
                $arr_empcond_officehours[$weekday]["officehours2_end"] = str_replace(":", "", $officehours2_end);
                $arr_empcond_officehours[$weekday]["officehours4_start"] = str_replace(":", "", $officehours4_start);
                $arr_empcond_officehours[$weekday]["officehours4_end"] = str_replace(":", "", $officehours4_end);

            }
        }
        $this->arr_empcond_officehours = $arr_empcond_officehours;
        //履歴取得 20130220
        $arr_hist = array();
        if ($start_date != "") {
            $wk_histdate = substr($start_date, 0, 4)."-".substr($start_date, 4, 2)."-".substr($start_date, 6, 2)." 00:00:00";
            $sql = "SELECT * FROM empcond_officehours_history ";
            $cond = "WHERE emp_id='$emp_id' and histdate >= '$wk_histdate' ORDER BY histdate ";
            $sel_hrhist = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel_hrhist == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $h_idx = 0;
            while ($r = pg_fetch_assoc($sel_hrhist)) {
                $arr_hist[$h_idx]["histdate"] = $r["histdate"];
                $arr_data = array();
                //基本
                //20140417 ":"が出力される不具合対応
                $r["officehours2_start"] = str_replace(":", "", $r["officehours2_start"]);
                $r["officehours2_end"] = str_replace(":", "", $r["officehours2_end"]);
                $r["officehours4_start"] = str_replace(":", "", $r["officehours4_start"]);
                $r["officehours4_end"] = str_replace(":", "", $r["officehours4_end"]);
                $arr_data[8] = $r;
                //曜日別
                if ($r["sub_rec_cnt"] > 0) {

                    $sql = "select * from empcond_officehours_history_sub";
                    $cond = "where empcond_officehours_history_sub_id={$r["empcond_officehours_history_id"]}";
                    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                    if ($sel == 0) {
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    $num = pg_num_rows($sel);
                    if ($num > 0) {

                        for ($i=0; $i<$num; $i++) {
                            $weekday = pg_fetch_result($sel, $i, "weekday");
                            $arr_data[$weekday]["officehours2_start"] = str_replace(":", "", pg_fetch_result($sel, $i, "officehours2_start"));
                            $arr_data[$weekday]["officehours2_end"] = str_replace(":", "", pg_fetch_result($sel, $i, "officehours2_end"));
                            $arr_data[$weekday]["officehours4_start"] = str_replace(":", "", pg_fetch_result($sel, $i, "officehours4_start"));
                            $arr_data[$weekday]["officehours4_end"] = str_replace(":", "", pg_fetch_result($sel, $i, "officehours4_end"));
                        }
                    }
                }
                $arr_hist[$h_idx]["data"] = $arr_data;
                $h_idx++;
            }

        }
        $this->empcond_officehours_hist = $arr_hist;
        return $arr_empcond_officehours;
    }

    /**
        * 個人別勤務時間帯取得
        *　※get_arr_empcond_officehours個人別勤務時間帯取得を呼んだあとに使用する
        * @param string $weekday 曜日 0:日 6:土 7:祝日
        * @param string $date 対象日、履歴対応用 20130220 省略時は履歴から検索しない
        * @return array 時間帯の配列 所定時刻、休憩時刻を設定、曜日別の時刻が未設定の時は基本時間
     *
     */
    function get_empcond_officehours($weekday, $date="") {

        $ret_data = array();
        //履歴から検索
        if ($date != "") {
            $num = count($this->empcond_officehours_hist);
            for ($i=0; $i<$num; $i++) {
                //変更日確認、指定日以降で近い日から履歴として呼出元へ返す。ただし時刻設定済みの場合
                $wk_histdate = str_replace("-", "", substr($this->empcond_officehours_hist[$i]["histdate"], 0, 10));
                if ($wk_histdate >= $date) {
                    //曜日別確認
                    $arr_data = $this->empcond_officehours_hist[$i]["data"];
                    if ($arr_data[$weekday]["officehours2_start"] != "") {
                        $ret_data = $arr_data[$weekday];
                    }
                    else {
                        //基本
                        if ($arr_data[8]["officehours2_start"] != "") {
                            $ret_data = $arr_data[8];
                        }
                    }
                    break;
                }
            }
        }
         //履歴にない場合、最新から取得
        if (count($ret_data) == 0) {

            //曜日別時間
            if ($this->arr_empcond_officehours[$weekday]["officehours2_start"] != "") {
                $ret_data = $this->arr_empcond_officehours[$weekday];
            }
            else {
                //基本時間
                if ($this->arr_empcond_officehours[8]["officehours2_start"] != "") {
                    $ret_data = $this->arr_empcond_officehours[8];
                }
            }
        }

        return $ret_data;
    }

    /*************************************************************************/
    // 日勤／夜勤時間の算出、委員会WG出席時間を除外 20140407
    // @param	$officehours_id		勤務日区分（１：通常勤務日／休日、２：勤務日１、３：勤務日２
    // @param	$pattern_id			出勤グループＩＤ
    // @param	$atdptn_ptn_id		出勤パターンＩＤ
    // @param	$duty_start_time	開始時刻
    // @param	$duty_end_time		終了時刻
    // @param	$data_standard		基準施設情報（グループ指定）
    // @param	$officehours_array	出勤表の勤務時間帯情報
    // @param	$meeting_time		会議・研修時間（未使用）
    // @param	$job_id				職員の職種ID、申し送り時間確認用
    // @param	$arr_no_subtract	申し送り時間を差し引かない職種・勤務パターン 1_1_1形式
    // @param	$out_time			外出時刻
    // @param	$ret_time			復帰時刻
    // @param	$meeting_start_time	会議・研修開始時刻
    // @param	$meeting_end_time	会議・研修終了時刻
    // @param	$hol_hour_start_time	時間有休開始時刻 20120213
    // @param	$hol_hour_end_time		時間有休終了時刻 20120213
    // @param	$date		            日付 20120309
    // @param	$assist_start_time		応援開始時刻 20140407
    // @param	$assist_end_time		応援終了時刻 20140407
    // @param	$arr_project_time		委員会WG時間データ 20140407
    //
    // @return	$data			日勤／夜勤時間
    /*************************************************************************/
    function get_duty_night_times2(
        $officehours_id,
        $pattern_id,
        $atdptn_ptn_id,
        $duty_start_time,
        $duty_end_time,
        $data_standard,
        $officehours_array,
        $meeting_time,
        $job_id="",
        $arr_no_subtract=array(),
        $out_time="",
        $ret_time="",
        $meeting_start_time="",
        $meeting_end_time="",
        $hol_hour_start_time,	//時間有休追加 20120213
        $hol_hour_end_time,		//時間有休追加 20120213
        $date,
        $assist_start_time,
        $assist_end_time,
        $arr_project_time
        ) {
        ///-----------------------------------------------------------------------------
        //初期値設定
        ///-----------------------------------------------------------------------------
        $prov_start_hhmm = $data_standard["prov_start_hhmm"];	//夜勤時間帯（開始時刻）
        $prov_end_hhmm = $data_standard["prov_end_hhmm"];		//夜勤時間帯（終了時刻）
        if ($data_standard["prov_start_hhmm"] == "") { $prov_start_hhmm = "0000"; }
        if ($data_standard["prov_end_hhmm"] == "") { $prov_end_hhmm = "0000"; }

        ///******************************************************************************
        //出勤グループごとの勤務時間帯を取得
        ///******************************************************************************
        ///-----------------------------------------------------------------------------
        //出勤表の勤務時間帯情報より対象勤務時間帯を取得
        ///-----------------------------------------------------------------------------
        //休日は通常勤務日と同様の扱い
        if ($officehours_id == 2 || $officehours_id == 3) {
            $wk_officehours_id = $officehours_id;
        } else {
            $wk_officehours_id = 1;
        }
        if ($pattern_id >= 1 && $atdptn_ptn_id >= 1) {
            $i = $pattern_id - 1;
            $wk_start_hhmm = "start_hhmm_" . $atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
            $wk_end_hhmm = "end_hhmm_" . $atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
            $officehours_start_hhmm = $officehours_array[$i][$wk_start_hhmm];
            $officehours_end_hhmm = $officehours_array[$i][$wk_end_hhmm];
            $ret_officehours_start_hhmm = $officehours_array[$i][$wk_start_hhmm]; //呼出元に返す所定時間 、除外リストで使用 20140417
            $ret_officehours_end_hhmm = $officehours_array[$i][$wk_end_hhmm];
        }
        //個人別所定時間を優先する場合 20120309
        if ($this->arr_empcond_officehours_ptn[$pattern_id][$atdptn_ptn_id] == 1) {
            $weekday_flg = $this->arr_weekday_flg[$date];
            $arr_empcond_officehours = $this->get_empcond_officehours($weekday_flg,$date); //個人別勤務時間帯履歴対応 20130220
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $officehours_start_hhmm = $arr_empcond_officehours["officehours2_start"];
                $officehours_end_hhmm = $arr_empcond_officehours["officehours2_end"];
                $ret_officehours_start_hhmm = $arr_empcond_officehours["officehours2_start"]; //呼出元に返す所定時間 20140417
                $ret_officehours_end_hhmm = $arr_empcond_officehours["officehours2_end"];
            }
        }

        if ($officehours_start_hhmm == "") { $officehours_start_hhmm = "0000"; }	//未設定時のデフォルト開始時刻
        if ($officehours_end_hhmm == "") { $officehours_end_hhmm = "2400"; }		//未設定時のデフォルト終了時刻
        ///-----------------------------------------------------------------------------
        //「当日／翌日」の勤務時間帯を算出
        ///-----------------------------------------------------------------------------
        if ($duty_start_time <= $duty_end_time) {
            $duty_start_time_to = $duty_start_time;
            $duty_end_time_to = $duty_end_time;
            $duty_start_time_next = "0000";
            $duty_end_time_next = "0000";
        } else {
            $duty_start_time_to = $duty_start_time;
            //所定終了時刻が日またがりか確認 20120319
            if ($officehours_start_hhmm > $officehours_end_hhmm) {
                $duty_end_time_to = "2400";
            }
            //日またがりでなければ退勤時刻ではなく所定終了時刻を使用
            else {
                $duty_end_time_to = $officehours_end_hhmm;
            }
            $duty_start_time_next = "0000";
            $duty_end_time_next = $duty_end_time;
        }
        ///-----------------------------------------------------------------------------
        //「当日」の勤務時間帯より対象となる時間算出
        ///-----------------------------------------------------------------------------
        $time_array = $this->get_pertinence_times(
                $duty_start_time_to,
                $duty_end_time_to,
                $officehours_start_hhmm,
                $officehours_end_hhmm);
        $start_time_to = $time_array["start_time"];	//勤務開始時間（当日）
        $end_time_to = $time_array["end_time"];		//勤務終了時間（当日）
        ///-----------------------------------------------------------------------------
        //「翌日」の勤務時間帯より対象となる時間算出
        ///-----------------------------------------------------------------------------
        if ($duty_start_time > $duty_end_time) {
            $time_array = $this->get_pertinence_times(
                    $duty_start_time_next,
                    $duty_end_time_next,
                    $officehours_start_hhmm,
                    $officehours_end_hhmm);
            $start_time_next = $time_array["start_time"];	//勤務開始時間（翌日）
            $end_time_next = $time_array["end_time"];		//勤務終了時間（翌日）
        } else {
            $start_time_next = "0000";
            $end_time_next = "0000";
        }

        //計算のための時間帯を設定
        if ($duty_start_time > $duty_end_time) {
            $wk_duty_start_time = $start_time_to;
            //所定終了時刻が日またがりか確認 20120319
            if ($officehours_start_hhmm > $officehours_end_hhmm) {
                $wk_duty_end_time = $end_time_next;
            }
            //日またがりでなければ退勤時刻ではなく所定終了時刻を使用
            else {
                $wk_duty_end_time = $officehours_end_hhmm;
            }
        } else {
            $wk_duty_start_time = $start_time_to;
            $wk_duty_end_time = $end_time_to;
        }

        $chk_ptn_id = $job_id."_".$pattern_id."_".$atdptn_ptn_id;

        //申し送りフラグ、職種、勤務パターンによりしない設定
        $send_flg = !in_array($chk_ptn_id, $arr_no_subtract);
        //時間算出、当日・翌日、日勤・夜勤
        $wk_data = $this->get_day_night_times2(
                $wk_duty_start_time,
                $wk_duty_end_time,
                $data_standard,
                $out_time,
                $ret_time,
                $send_flg,
                $meeting_start_time,
                $meeting_end_time,
                "0",
                $hol_hour_start_time,
                $hol_hour_end_time,
                $date,
                $assist_start_time,
                $assist_end_time,
                $arr_project_time
                );
        $duty_time_to = $wk_data["duty_time_to"];
        $duty_time_next = $wk_data["duty_time_next"];
        $night_time_to = $wk_data["night_time_to"];
        $night_time_next = $wk_data["night_time_next"];

        ///-----------------------------------------------------------------------------
        //日勤／夜勤時間を設定
        ///-----------------------------------------------------------------------------
        $data = array();
        $data["duty_time_to"] = 0.0;		//日勤時間（当日）
        $data["duty_time_next"] = 0.0;		//日勤時間（翌日）
        $data["night_time_to"] = 0.0;		//夜勤時間（当日）
        $data["night_time_next"] = 0.0;		//夜勤時間（翌日）
        if ($duty_time_to > 0) {	$data["duty_time_to"] = $duty_time_to; }		//日勤時間（当日）
        if ($duty_time_next> 0) {	$data["duty_time_next"] = $duty_time_next; }	//日勤時間（翌日）
        if ($night_time_to > 0) {	$data["night_time_to"] = $night_time_to; }		//夜勤時間（当日）
        if ($night_time_next > 0) { $data["night_time_next"] = $night_time_next; }	//夜勤時間（翌日）
        $data["officehours_start_hhmm"] = $ret_officehours_start_hhmm; //呼出元に返す所定時間 20140417
        $data["officehours_end_hhmm"] = $ret_officehours_end_hhmm;
        ///-----------------------------------------------------------------------------
        //デバック用
        ///-----------------------------------------------------------------------------
        $data["prov_start_hhmm"] = $prov_start_hhmm;
        $data["prov_end_hhmm"] = $prov_end_hhmm;
        $data["wk_start_hhmm"] = $wk_start_hhmm;
        $data["wk_end_hhmm"] = $wk_end_hhmm;
        $data["start_time_to"] = $start_time_to;
        $data["end_time_to"] = $end_time_to;
        $data["start_time_next"] = $start_time_next;
        $data["end_time_next"] = $end_time_next;

        return $data;
    }

    /**
     * 当日／翌日、日勤／夜勤の時間を算出
     *
     * @param mixed $duty_start_time 出勤時刻
     * @param mixed $duty_end_time 退勤時刻
     * @param mixed $data_standard 施設基準
     * @param mixed $out_time 外出時刻
     * @param mixed $ret_time 復帰時刻
     * @param mixed $send_flg 申し送りフラグ
     * @param mixed $meeting_start_time 会議・研修開始時刻
     * @param mixed $meeting_end_time 会議・研修終了時刻
     * @param mixed $hol_hour_flag 時間有休フラグ 0:通常 1:時間有休
     * @param	$hol_hour_start_time	時間有休開始時刻 20120213
     * @param	$hol_hour_end_time		時間有休終了時刻 20120213
     * @param	$date		            日付 20120309
     * @param	$assist_start_time		応援開始時刻 20140407
     * @param	$assist_end_time		応援終了時刻 20140407
     * @param	$arr_project_time		委員会WG時間データ 20140407
     *
     * @return mixed This is the return value description
     *
     */
    function get_day_night_times2(
        $duty_start_time,
        $duty_end_time,
        $data_standard,
        $out_time,
        $ret_time,
        $send_flg,
        $meeting_start_time,
        $meeting_end_time,
        $hol_hour_flag,
        $hol_hour_start_time,
        $hol_hour_end_time,
        $date,
        $assist_start_time,
        $assist_end_time,
        $arr_project_time
        ) {
        $duty_time_to = 0.0;				//日勤時間（当日）
        $duty_time_next = 0.0;				//日勤時間（翌日）
        $night_time_to = 0.0;				//夜勤時間（当日）
        $night_time_next = 0.0;				//夜勤時間（翌日）

        $prov_start_hhmm = $data_standard["prov_start_hhmm"];	//夜勤時間帯（開始時刻）
        $prov_end_hhmm = $data_standard["prov_end_hhmm"];		//夜勤時間帯（終了時刻）
        if ($data_standard["prov_start_hhmm"] == "") { $prov_start_hhmm = "0000"; }
        if ($data_standard["prov_end_hhmm"] == "") { $prov_end_hhmm = "0000"; }

        //配列方式時間計算用
        $today = date("Ymd");
        $nextday = date("Ymd", strtotime("+1 days"));
        $twoday = date("Ymd", strtotime("+2 days"));

        //勤務時間の配列
        $start_duty_date_time = $today.$duty_start_time;
        if ($duty_start_time <= $duty_end_time) {
            $end_duty_date_time = $today.$duty_end_time;
        } else {
            $end_duty_date_time = $nextday.$duty_end_time;
        }
        //夜勤時間帯が未設定の場合、全てを日勤とする
        if (($prov_start_hhmm == "0000") && ($prov_end_hhmm == "0000")) {
            $wk_prov_today_start_hhmm = $today."0000";
            $wk_prov_today_end_hhmm = $nextday."0000";
            $wk_prov_nextday_start_hhmm = $nextday."0000";
            $wk_prov_nextday_end_hhmm = $twoday."0000";
        } else {
            if ((int)$prov_start_hhmm <= (int)$prov_end_hhmm) {
                $wk_prov_today_start_hhmm = $today.$prov_start_hhmm;
                $wk_prov_today_end_hhmm = $today.$prov_end_hhmm;
                $wk_prov_nextday_start_hhmm = $nextday.$prov_start_hhmm;
                $wk_prov_nextday_end_hhmm = $nextday.$prov_end_hhmm;
            } else {
                $wk_prov_today_start_hhmm = $today.$prov_end_hhmm;
                $wk_prov_today_end_hhmm = $today.$prov_start_hhmm;
                $wk_prov_nextday_start_hhmm = $nextday.$prov_end_hhmm;
                $wk_prov_nextday_end_hhmm = $nextday.$prov_start_hhmm;
            }
        }
        $proj_cnt = count($arr_project_time);
        //外出、会議研修、半日応援、委員会、時間有休が未入力の場合
        if ($out_time == "" && $ret_time == "" &&
                $meeting_start_time  == "" && $meeting_end_time == "" &&
                $hol_hour_start_time  == "" && $hol_hour_end_time == "" &&
                $assist_start_time  == "" && $assist_end_time == "" &&
                $proj_cnt == 0) {
            //退勤日時、申し送り確認用
            if ($duty_start_time > $duty_end_time) {
                $wk_duty_end_date_time = $nextday.$duty_end_time;
            } else {
                $wk_duty_end_date_time = $today.$duty_end_time;
            }
            //時刻から時間計算
            //当日日勤
            $duty_time_to = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $wk_prov_today_start_hhmm, $wk_prov_today_end_hhmm);
            //翌日日勤
            $duty_time_next = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $wk_prov_nextday_start_hhmm, $wk_prov_nextday_end_hhmm);
            $today_night_start_hhmm1 = "";
            $today_night_end_hhmm1 = "";
            $today_night_start_hhmm2 = "";
            $today_night_end_hhmm2 = "";
            $nextday_night_start_hhmm1 = "";
            $nextday_night_end_hhmm1 = "";
            $nextday_night_start_hhmm2 = "";
            $nextday_night_end_hhmm2 = "";
            if (($prov_start_hhmm == "0000") && ($prov_end_hhmm == "0000")) {
                $night_time_to = 0;
                $night_time_next = 0;
            } else {
                if ((int)$prov_start_hhmm <= (int)$prov_end_hhmm) {
                    $today_night_start_hhmm1 = $today.$prov_start_hhmm;
                    $today_night_end_hhmm1 = $today.$prov_end_hhmm;
                    if ($duty_start_time > $duty_end_time) {
                        $nextday_night_start_hhmm1 = $nextday.$prov_start_hhmm;
                        $nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
                    }
                } else {
                    $today_night_start_hhmm1 = $today."0000";
                    $today_night_end_hhmm1 = $today.$prov_end_hhmm;
                    $today_night_start_hhmm2 = $today.$prov_start_hhmm;
                    $today_night_end_hhmm2 = $nextday."0000";

                    if ($duty_start_time > $duty_end_time) {
                        $nextday_night_start_hhmm1 = $nextday."0000";
                        $nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
                        $nextday_night_start_hhmm2 = $nextday.$prov_start_hhmm;
                        $nextday_night_end_hhmm2 = $twoday."0000";
                    }
                }
                //当日夜勤
                $night_time_to = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $today_night_start_hhmm1, $today_night_end_hhmm1);
                if ($today_night_start_hhmm2 != "" && $today_night_end_hhmm2 != "") {
                    $night_time_to += $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $today_night_start_hhmm2, $today_night_end_hhmm2);
                }
                //翌日夜勤
                $night_time_next = $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $nextday_night_start_hhmm1, $nextday_night_end_hhmm1);
                if ($nextday_night_start_hhmm2 != "" && $nextday_night_end_hhmm2 != "") {
                    $night_time_next += $this->get_intersect_times($start_duty_date_time, $end_duty_date_time, $nextday_night_start_hhmm2, $nextday_night_end_hhmm2);
                }
            }

            //上の処理のコメント化
            //申し送り時間を減算
            if ($send_flg) {
                //申し送り時間を日勤、夜勤時間帯別に取得する
                //日勤夜勤時間帯情報
                $time_info = array();
                $time_info["today_start_hhmm"] = $wk_prov_today_start_hhmm;
                $time_info["today_end_hhmm"] = $wk_prov_today_end_hhmm;
                $time_info["nextday_start_hhmm"] = $wk_prov_nextday_start_hhmm;
                $time_info["nextday_end_hhmm"] = $wk_prov_nextday_end_hhmm;
                $time_info["today_night_start_hhmm1"] = $today_night_start_hhmm1;
                $time_info["today_night_end_hhmm1"] = $today_night_end_hhmm1;
                $time_info["today_night_start_hhmm2"] = $today_night_start_hhmm2;
                $time_info["today_night_end_hhmm2"] = $today_night_end_hhmm2;
                $time_info["nextday_night_start_hhmm1"] = $nextday_night_start_hhmm1;
                $time_info["nextday_night_end_hhmm1"] = $nextday_night_end_hhmm1;
                $time_info["nextday_night_start_hhmm2"] = $nextday_night_start_hhmm2;
                $time_info["nextday_night_end_hhmm2"] = $nextday_night_end_hhmm2;

                $arr_day_night_send_times = $this->get_day_night_send_times($start_duty_date_time, $end_duty_date_time, $time_info, $data_standard, "", "", "", "", $hol_hour_flag);
                //申し送り時間を減算
                $duty_time_to -= $arr_day_night_send_times["duty_time_to"];
                $duty_time_next -= $arr_day_night_send_times["duty_time_next"];
                $night_time_to -= $arr_day_night_send_times["night_time_to"];
                $night_time_next -= $arr_day_night_send_times["night_time_next"];

            }
            $duty_time_to = $duty_time_to / 60;
            $duty_time_next = $duty_time_next / 60;
            $night_time_to = $night_time_to / 60;
            $night_time_next = $night_time_next / 60;

        } else {
            //外出、会議・研修等がある場合
            $arr_minute_duty_time = $this->get_minute_time_array($start_duty_date_time, $end_duty_date_time);
            //配列方式

            $arr_ex_data = array();
            $e_idx = 0;
            //1:タイムカード（会議）
            if ($meeting_start_time != "" && $meeting_end_time != "") {
                $arr_ex_data[$e_idx]["kind"] = "1";
                $arr_ex_data[$e_idx]["start_time"] = $meeting_start_time;
                $arr_ex_data[$e_idx]["end_time"] = $meeting_end_time;
            	$e_idx++;
            }
            //2:時間帯（半日応援）
            if ($assist_start_time != "" && $assist_end_time != "") {
                $arr_ex_data[$e_idx]["kind"] = "2";
                $arr_ex_data[$e_idx]["start_time"] = $assist_start_time;
                $arr_ex_data[$e_idx]["end_time"] = $assist_end_time;
            	$e_idx++;
            }
            //3:外出
            if ($out_time != "" && $ret_time != "") {
                $arr_ex_data[$e_idx]["kind"] = "3";
                $arr_ex_data[$e_idx]["start_time"] = $out_time;
                $arr_ex_data[$e_idx]["end_time"] = $ret_time;
               	$e_idx++;
            }
            //4:時間有休
            if ($hol_hour_start_time != "" && $hol_hour_end_time != "") {
                $arr_ex_data[$e_idx]["kind"] = "4";
                $arr_ex_data[$e_idx]["start_time"] = $hol_hour_start_time;
                $arr_ex_data[$e_idx]["end_time"] = $hol_hour_end_time;
            	$e_idx++;
            }
            //5:委員会WG
            $proj_cnt = count($arr_project_time);
            for ($p_idx = 0; $p_idx < $proj_cnt; $p_idx++) {
                $arr_ex_data[$e_idx]["kind"] = "5";
                $arr_ex_data[$e_idx]["start_time"] = $arr_project_time[$p_idx]["start_time"];
                $arr_ex_data[$e_idx]["end_time"] = $arr_project_time[$p_idx]["end_time"];
                $arr_ex_data[$e_idx]["next_day_flag"] = $arr_project_time[$p_idx]["next_day_flag"];
                $e_idx++;
            }
            //除外処理
            $ex_flg = false;
            $arr_ex_time = array();
            $e_max = count($arr_ex_data);
            for ($e_idx=0; $e_idx < $e_max; $e_idx++) {
                if ($arr_ex_data[$e_idx]["next_day_flag"] == "1") {
                    $start_date_time = $nextday.$arr_ex_data[$e_idx]["start_time"];
                }
                else {
                    $start_date_time = $today.$arr_ex_data[$e_idx]["start_time"];
                }
                //2400対応
                if ($arr_ex_data[$e_idx]["end_time"] == "2400") {
                    $end_date_time = $nextday."0000";
                }
                elseif ($arr_ex_data[$e_idx]["start_time"] > $arr_ex_data[$e_idx]["end_time"] || $arr_ex_data[$e_idx]["next_day_flag"] == "1") {
                    $end_date_time = $nextday.$arr_ex_data[$e_idx]["end_time"];
                } else {
                    $end_date_time = $today.$arr_ex_data[$e_idx]["end_time"];
                }
                $arr_ex_data[$e_idx]["start_date_time"] = $start_date_time;
                $arr_ex_data[$e_idx]["end_date_time"] = $end_date_time;
                $arr_minute_time = $this->get_minute_time_array($start_date_time, $end_date_time);
                $arr_ex_time = array_merge($arr_ex_time, $arr_minute_time);
                $ex_flg = true;
            }
            if ($ex_flg) {
                $arr_minute_duty_time = array_diff($arr_minute_duty_time, $arr_ex_time);
            }

            //退勤日時、申し送り確認用
            if ($duty_start_time > $duty_end_time) {
                $wk_duty_end_date_time = $nextday.$duty_end_time;
            } else {
                $wk_duty_end_date_time = $today.$duty_end_time;
            }
            //当日日勤の配列
            $arr_minute_today_time = $this->get_minute_time_array($wk_prov_today_start_hhmm, $wk_prov_today_end_hhmm);
            $duty_time_to = count(array_intersect($arr_minute_duty_time, $arr_minute_today_time));			//日勤時間（当日）
            //翌日日勤の配列
            $arr_minute_nextday_time = array();
            if ($duty_start_time > $duty_end_time) {
                $arr_minute_nextday_time = $this->get_minute_time_array($wk_prov_nextday_start_hhmm, $wk_prov_nextday_end_hhmm);
                $duty_time_next = count(array_intersect($arr_minute_duty_time, $arr_minute_nextday_time));				//日勤時間（翌日）
            }
            //夜勤
            $arr_minute_today_night_time = array();
            $arr_minute_nextday_night_time = array();
            $today_night_start_hhmm1 = "";
            $today_night_end_hhmm1 = "";
            $today_night_start_hhmm2 = "";
            $today_night_end_hhmm2 = "";
            $nextday_night_start_hhmm1 = "";
            $nextday_night_end_hhmm1 = "";
            $nextday_night_start_hhmm2 = "";
            $nextday_night_end_hhmm2 = "";
            if (($prov_start_hhmm == "0000") && ($prov_end_hhmm == "0000")) {
            } else {
                if ((int)$prov_start_hhmm <= (int)$prov_end_hhmm) {
                    $today_night_start_hhmm1 = $today.$prov_start_hhmm;
                    $today_night_end_hhmm1 = $today.$prov_end_hhmm;
                    $arr_minute_today_night_time = $this->get_minute_time_array($today_night_start_hhmm1, $today_night_end_hhmm1);
                    if ($duty_start_time > $duty_end_time) {
                        $nextday_night_start_hhmm1 = $nextday.$prov_start_hhmm;
                        $nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
                        $arr_minute_nextday_night_time = $this->get_minute_time_array($nextday_night_start_hhmm1, $nextday_night_end_hhmm1);
                    }
                } else {
                    $today_night_start_hhmm1 = $today."0000";
                    $today_night_end_hhmm1 = $today.$prov_end_hhmm;
                    $today_night_start_hhmm2 = $today.$prov_start_hhmm;
                    $today_night_end_hhmm2 = $nextday."0000";
                    $arr_minute_today_night_time1 = $this->get_minute_time_array($today_night_start_hhmm1, $today_night_end_hhmm1);


                    $arr_minute_today_night_time2 = $this->get_minute_time_array($today_night_start_hhmm2, $today_night_end_hhmm2);


                    $arr_minute_today_night_time = array_merge($arr_minute_today_night_time1, $arr_minute_today_night_time2);

                    if ($duty_start_time > $duty_end_time) {
                        $nextday_night_start_hhmm1 = $nextday."0000";
                        $nextday_night_end_hhmm1 = $nextday.$prov_end_hhmm;
                        $nextday_night_start_hhmm2 = $nextday.$prov_start_hhmm;
                        $nextday_night_end_hhmm2 = $twoday."0000";
                        $arr_minute_nextday_night_time1 = $this->get_minute_time_array($nextday_night_start_hhmm1, $nextday_night_end_hhmm1);
                        $arr_minute_nextday_night_time2 = $this->get_minute_time_array($nextday_night_start_hhmm2, $nextday_night_end_hhmm2);
                        $arr_minute_nextday_night_time = array_merge($arr_minute_nextday_night_time1, $arr_minute_nextday_night_time2);
                    }
                }
                $night_time_to = count(array_intersect($arr_minute_duty_time, $arr_minute_today_night_time));				//夜勤時間（当日）
                if ($duty_start_time > $duty_end_time) {
                    $night_time_next = count(array_intersect($arr_minute_duty_time, $arr_minute_nextday_night_time));				//夜勤時間（翌日）
                }
            }
            //申し送り時間を減算
            if ($send_flg) {
                //申し送り時間を日勤、夜勤時間帯別に取得する
                //日勤夜勤時間帯情報
                $time_info = array();
                $time_info["today_start_hhmm"] = $wk_prov_today_start_hhmm;
                $time_info["today_end_hhmm"] = $wk_prov_today_end_hhmm;
                $time_info["nextday_start_hhmm"] = $wk_prov_nextday_start_hhmm;
                $time_info["nextday_end_hhmm"] = $wk_prov_nextday_end_hhmm;
                $time_info["today_night_start_hhmm1"] = $today_night_start_hhmm1;
                $time_info["today_night_end_hhmm1"] = $today_night_end_hhmm1;
                $time_info["today_night_start_hhmm2"] = $today_night_start_hhmm2;
                $time_info["today_night_end_hhmm2"] = $today_night_end_hhmm2;
                $time_info["nextday_night_start_hhmm1"] = $nextday_night_start_hhmm1;
                $time_info["nextday_night_end_hhmm1"] = $nextday_night_end_hhmm1;
                $time_info["nextday_night_start_hhmm2"] = $nextday_night_start_hhmm2;
                $time_info["nextday_night_end_hhmm2"] = $nextday_night_end_hhmm2;

                $arr_day_night_send_times = $this->get_day_night_send_times2($start_duty_date_time, $end_duty_date_time, $time_info, $data_standard, $hol_hour_flag, $arr_ex_data);
                //申し送り時間を減算
                $duty_time_to -= $arr_day_night_send_times["duty_time_to"];
                $duty_time_next -= $arr_day_night_send_times["duty_time_next"];
                $night_time_to -= $arr_day_night_send_times["night_time_to"];
                $night_time_next -= $arr_day_night_send_times["night_time_next"];
            }
            $duty_time_to = $duty_time_to / 60;
            $duty_time_next = $duty_time_next / 60;
            $night_time_to = $night_time_to / 60;
            $night_time_next = $night_time_next / 60;
        }

        ///-----------------------------------------------------------------------------
        //日勤／夜勤時間を設定
        ///-----------------------------------------------------------------------------
        $data = array();
        $data["duty_time_to"] = 0.0;		//日勤時間（当日）
        $data["duty_time_next"] = 0.0;		//日勤時間（翌日）
        $data["night_time_to"] = 0.0;		//夜勤時間（当日）
        $data["night_time_next"] = 0.0;		//夜勤時間（翌日）
        if ($duty_time_to > 0) {	$data["duty_time_to"] = $duty_time_to; }		//日勤時間（当日）
        if ($duty_time_next> 0) {	$data["duty_time_next"] = $duty_time_next; }	//日勤時間（翌日）
        if ($night_time_to > 0) {	$data["night_time_to"] = $night_time_to; }		//夜勤時間（当日）
        if ($night_time_next > 0) { $data["night_time_next"] = $night_time_next; }	//夜勤時間（翌日）
        return $data;
    }
    /**
     * 申し送り時間を日勤、夜勤時間帯別に取得する、委員会対応 20140409
     *
     * @param mixed $start_duty_date_time 出勤時刻
     * @param mixed $end_duty_date_time 退勤時刻
     * @param mixed $time_info 日勤夜勤時間帯情報
     * @param mixed $data_standard 基準施設情報（グループ指定）
     * @param mixed $hol_hour_flag 時間有休フラグ
     * @param mixed $arr_ex_data 除外時間データ配列
     *
     * @return array 申し送り時間の配列
     *
     */
    function get_day_night_send_times2($start_duty_date_time, $end_duty_date_time, $time_info, $data_standard, $hol_hour_flag, $arr_ex_data){
        
        $arr_ret = array();
        //時間帯との重なりを求めて返す
        $arr_ret["duty_time_to"] = $this->get_send_time2($start_duty_date_time, $end_duty_date_time, $time_info["today_start_hhmm"], $time_info["today_end_hhmm"], $data_standard, $hol_hour_flag, $arr_ex_data);
        $arr_ret["duty_time_next"] = $this->get_send_time2($start_duty_date_time, $end_duty_date_time, $time_info["nextday_start_hhmm"], $time_info["nextday_end_hhmm"], $data_standard, $hol_hour_flag, $arr_ex_data);
        
        $night_time_to = 0;
        if ($time_info["today_night_start_hhmm1"] != "") {
            $night_time_to += $this->get_send_time2($start_duty_date_time, $end_duty_date_time, $time_info["today_night_start_hhmm1"], $time_info["today_night_end_hhmm1"], $data_standard, $hol_hour_flag, $arr_ex_data);
        }
        if ($time_info["today_night_start_hhmm2"] != "") {
            $night_time_to += $this->get_send_time2($start_duty_date_time, $end_duty_date_time, $time_info["today_night_start_hhmm2"], $time_info["today_night_end_hhmm2"], $data_standard, $hol_hour_flag, $arr_ex_data);
        }
        $arr_ret["night_time_to"] = $night_time_to;
        
        $night_time_next = 0;
        if ($time_info["nextday_night_start_hhmm1"] != "") {
            $night_time_next += $this->get_send_time2($start_duty_date_time, $end_duty_date_time, $time_info["nextday_night_start_hhmm1"], $time_info["nextday_night_end_hhmm1"], $data_standard, $hol_hour_flag, $arr_ex_data);
        }
        if ($time_info["nextday_night_start_hhmm2"] != "") {
            $night_time_next += $this->get_send_time2($start_duty_date_time, $end_duty_date_time, $time_info["nextday_night_start_hhmm2"], $time_info["nextday_night_end_hhmm2"], $data_standard, $hol_hour_flag, $arr_ex_data);
        }
        $arr_ret["night_time_next"] = $night_time_next;
        
        return $arr_ret;
    }
    /**
     * 申し送り時間取得(日勤夜勤帯対応)、委員会対応 20140409
     *
     * @param mixed $start1 所定開始時刻
     * @param mixed $end1 所定終了時刻
     * @param mixed $start2 時間帯開始時刻
     * @param mixed $end2 時間帯終了時刻
     * @param mixed $data_standard 基準施設情報（グループ指定）
     * @param mixed $hol_hour_flag 時間有休フラグ 時間有休対応 20120213
     * @param mixed $arr_ex_data 除外時間データ配列
     *
     * @return mixed 申し送り時間（分）
     *
     */
    function get_send_time2($start1, $end1, $start2, $end2, $data_standard, $hol_hour_flag, $arr_ex_data) {
        
        //所定時間と申し送りの重なりを確認後、日勤時間、夜勤時間の重なりを取得する
        
        //時間にコロン(:)がある場合消す
        $s1 = str_replace(":", "", $start1);
        $e1 = str_replace(":", "", $end1);
        $s2 = str_replace(":", "", $start2);
        $e2 = str_replace(":", "", $end2);
        
        //申し送りの時刻
        $wk_start_time = substr($s1, 8, 4);
        $wk_end_time = substr($e1, 8, 4);
        list($wk_start_date_time, $wk_end_date_time) = $this->get_send_start_end_time($wk_start_time, $wk_end_time, $data_standard, $hol_hour_flag);
        if ($wk_start_date_time == "") {
            return 0;
        }
        
        //入力チェック
        //開始終了時刻が逆の場合、0とする
        if ($wk_start_date_time >= $wk_end_date_time || $s2 >= $e2) {
            return 0;
        }
        //範囲外
        //      s1----e1
        //s2--e2      s2--e2
        if ($e2 <= $wk_start_date_time || $s2 >= $wk_end_date_time) {
            return 0;
        }
        
        //     s1----e1  s1----e1
        //    s2--e2      s2--e2
        $wk_s = max($wk_start_date_time, $s2);
        $wk_e = min($wk_end_date_time, $e2);
        
        //申し送り時間（日勤夜勤時間と重複する）を取得
        $minutes = date_utils::get_diff_minute($wk_e, $wk_s);
        
        //除外時間、重複確認
        $chk_flg = false;
        $arr_ex_minute = array();
        for ($i=0; $i<count($arr_ex_data); $i++) {
            $wk_time = $this->get_intersect_times($wk_s, $wk_e, $arr_ex_data[$i]["start_date_time"], $arr_ex_data[$i]["end_date_time"]);
            if ($wk_time > 0) {
                //範囲外
                //      s1----e1
                //s2--e2      s2--e2
                if ($wk_e <= $arr_ex_data[$i]["start_date_time"] || $wk_s >= $arr_ex_data[$i]["end_date_time"]) {
                    ;
                }
                else {
                    //重複
                    $wk_ex_s = max($arr_ex_data[$i]["start_date_time"], $wk_s);
                    $wk_ex_e = min($arr_ex_data[$i]["end_date_time"], $wk_e);
                    $arr_minute_time = $this->get_minute_time_array($wk_ex_s, $wk_ex_e);
                    $arr_ex_minute = array_merge($arr_ex_minute, $arr_minute_time);
                    $chk_flg = true;
                }
            }
            
        }
        //重複がある場合、その時間を減算する
        if ($chk_flg) {
            $arr_send_minute = $this->get_minute_time_array($wk_s, $wk_e);
            $diff_time = count(array_intersect($arr_send_minute, $arr_ex_minute));
            $minutes -= $diff_time;
        }
        
        return $minutes;
    }
}
?>

