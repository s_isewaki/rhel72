<?
ini_set("max_execution_time", 0);

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}

// 正常にアップロードされた場合、登録処理を実行
if ($uploaded) {
	$result = "t";

	// 文字コードの設定
	switch ($encoding) {
	case "1":
		$file_encoding = "SJIS";
		break;
	case "2":
		$file_encoding = "EUC-JP";
		break;
	case "3":
		$file_encoding = "UTF-8";
		break;
	default:
		exit;
	}

	// EUC-JPで扱えない文字列は「・」に変換するよう設定
	mb_substitute_character("・");

	// 文字変換テーブルの取得
	$character_table = get_character_table($file_encoding);

	// ファイルの読み込み
	$lines = file($_FILES["csvfile"]["tmp_name"]);

	// トランザクションの開始
	$con = connect2db($fname);
	pg_query($con, "begin transaction");

	// 1行分ずつinsert
	foreach ($lines as $line) {

		// 異体字を変換
		$line = convert_encoding($line, $character_table);

		$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

		// EOFを削除
		$line = str_replace(chr(0x1A), "", $line);

		// 空行は無視
		if ($line == "") continue;

		$values = split(",", $line);
		if (count($values) != 8) {
			$result = "f";
			pg_query($con, "rollback");
			pg_close($con);
			break;
		}

		// ID文字種チェック
		if (preg_match("/[^0-9a-zA-Z]/", $values[0]) > 0) {
			$result = "f";
			pg_query($con, "rollback");
			pg_close($con);
			break;
		}

		// ID重複チェック
		$sql = "select count(*) from ptifmst";
		$cond = "where ptif_id = '{$values[0]}'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_fetch_result($sel, 0, 0) > 0) {
			$result = "f";
			pg_query($con, "rollback");
			pg_close($con);
			break;
		}

		$values[3] = mb_convert_kana($values[3], "HVc");
		$values[4] = mb_convert_kana($values[4], "HVc");

		array_push($values, get_keyword($values[3]));
		array_push($values, "1");
		$sql = "insert into ptifmst (ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_birth, ptif_sex, ptif_buis, ptif_keywd, ptif_in_out) values (";
		$ins = insert_into_table($con, $sql, $values, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションのコミット
	if ($result == "t") {
		pg_query($con, "commit");
		pg_close($con);
	}
} else {
	$result = "f";
}

// 画面遷移
echo("<script type=\"text/javascript\">location.href = 'patient_bulk_register.php?session=$session&result=$result&encoding=$encoding';</script>");

function get_character_table($file_encoding) {
	switch ($file_encoding) {
	case "SJIS":
		$table_file = "character_table_sjis.txt";
		break;
	case "EUC-JP":
		$table_file = "character_table_euc.txt";
		break;
	case "UTF-8":
		$table_file = "character_table_utf8.txt";
		break;
	}

	if (!file_exists($table_file)) return array();

	$lines = file($table_file);

	$table = array();
	for ($i = 0, $j = count($lines); $i < $j; $i++) {
		$tmp_columns = split(',', trim($lines[$i]));
		if (count($tmp_columns) == 2) {
			$table[] = $tmp_columns;
		}
	}
	return $table;
}

function convert_encoding($line, $character_table) {
	foreach ($character_table as $pair) {
		$line = str_replace($pair[0], $pair[1], $line);
	}
	return $line;
}
?>
