<?
ob_start();
require_once("about_comedix.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("Cmx.php");
require_once("aclg_set.php");
ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$delete_flg = ($select_box == "DUST") ? "t" : "f";

$arr_condition = array("category"      => $wkfw_type,
                       "workflow"      => $wkfw_id,
                       "apply_title"   => $apply_title,
                       "apply_emp_nm"  => $apply_emp_nm,
                       "date_y1"       => $apply_yyyy_from,
                       "date_m1"       => $apply_mm_from,
                       "date_d1"       => $apply_dd_from,
                       "date_y2"       => $apply_yyyy_to,
                       "date_m2"       => $apply_mm_to,
                       "date_d2"       => $apply_dd_to,
                       "apply_stat"    => $apply_stat,
                       "class"         => $class,
                       "attribute"     => $attribute,
                       "dept"          => $dept,
                       "room"          => $room,
                       "apply_content" => $apply_content);

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$apply_list = get_apply_list_for_csv($con, $delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition, $mode, $apply_ids, $fname, $session);

// 全件出力の場合、申請書データから申請書IDリストを作成
if ($mode == "2") {
	$apply_ids = get_apply_ids_for_csv($apply_list);
}

// 添付ファイル情報を取得
$apply_files = get_apply_files_for_csv($con, $apply_ids, $fname, $session);

// 承認者情報を取得
$approves = get_approves_for_csv($con, $apply_ids, $fname, $session);

// データベース接続を閉じる
pg_close($con);

$csv_cols = collect_csv_cols($apply_list, $approves);
$csv_body = build_csv_body($apply_list, $approves, $apply_files, $csv_cols);
$csv = mb_convert_encoding(implode(",", $csv_cols) . "\r\n" . $csv_body, "Shift_JIS", "EUC-JP");

$file_name = "workflow.csv";
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);

function collect_csv_cols($apply_list, $approves) {
	$conf = new Cmx_SystemConfig();
	$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";

	$cols = array("申請番号", "申請書名", $title_label, "申請者", "申請日", "申請状況", "所属");

	$content_cols = array();
	foreach ($apply_list as $apply) {
		if ($apply["wkfw_content_type"] == "1" || strpos($apply["apply_content"], "<?xml") === false) {
			$content_cols[] = "テキスト入力";
		} else {
			$content_cols = array_merge($content_cols, collect_template_tags($apply["apply_content"]));
		}
		$content_cols = array_unique($content_cols);
	}
	$cols = array_merge($cols, $content_cols);

	array_push($cols, "添付ファイル", "承認者コメント");

	$approve_order_cols = array();
	foreach ($approves as $tmp_approves) {
		foreach ($tmp_approves as $tmp_approve) {
			$tmp_no = $tmp_approve["apv_order"];
			if ($tmp_approve["apv_sub_order"] != "") {
				$tmp_no .= "-" . $tmp_approve["apv_sub_order"];
			}
			array_push($approve_order_cols, $tmp_no);
			$approve_order_cols = array_unique($approve_order_cols);
		}
	}
	natsort($approve_order_cols);

	$approve_cols = array();
	foreach ($approve_order_cols as $tmp_no) {
		array_push($approve_cols, "承認者{$tmp_no}役職", "承認者{$tmp_no}氏名", "承認者{$tmp_no}承認状況");
	}
	$cols = array_merge($cols, $approve_cols);

	return $cols;
}

function collect_template_tags($apply_content) {
	$apply_content = preg_replace("/[\x01-\x08\x0b\x0c\x0e-\x1f\x7f]+/", "", $apply_content);
	$apply_content = str_replace("\0", "", $apply_content);
	if (!$dom = domxml_open_mem($apply_content)) {
		return array("テンプレート");
	}

	$root_nodes = $dom->get_elements_by_tagname('apply');
	if (count($root_nodes) == 0) {
		return array("テンプレート");
	}
	$root_node = $root_nodes[0];

	$tags = array();
	$child_nodes = $root_node->child_nodes();
	foreach ($child_nodes as $child_node) {
		if ($child_node->node_type() == XML_ELEMENT_NODE) {
			$disp_attribute = $child_node->get_attribute_node("disp");
			if ($disp_attribute) {
				$tags[] = mb_convert_encoding($disp_attribute->value(), "EUC-JP", "UTF-8");
			} else {
				$tags[] = mb_convert_encoding($child_node->tagname(), "EUC-JP", "UTF-8");
			}
		}
	}
	return $tags;
}

function build_csv_body($apply_list, $approves, $apply_files, $csv_cols) {
	$csv = "";

	$cols_count = count($csv_cols);

	foreach ($apply_list as $tmp_apply) {
		$cols = array_fill(0, $cols_count - 1, "");

		$cols[0] = format_col(format_apply_no_for_csv($tmp_apply["short_wkfw_name"], $tmp_apply["apply_date"], $tmp_apply["apply_no"]));
		$cols[1] = format_col($tmp_apply["wkfw_title"]);
		$cols[2] = format_col($tmp_apply["apply_title"]);
		$cols[3] = format_col(format_emp_name_for_csv($tmp_apply["emp_lt_nm"], $tmp_apply["emp_ft_nm"]));
		$cols[4] = format_col(format_date_for_csv($tmp_apply["apply_date"]));
		$cols[5] = format_col(format_apply_status_for_csv($tmp_apply["apply_stat"], $tmp_apply["approve_label"], $approves[$tmp_apply["apply_id"]]));
		$cols[6] = format_col(format_emp_dept_for_csv($tmp_apply["class_nm"], $tmp_apply["atrb_nm"], $tmp_apply["dept_nm"], $tmp_apply["room_nm"]));

		$tmp_apply_contents = collect_apply_contents($tmp_apply["wkfw_content_type"], $tmp_apply["apply_content"]);
		foreach ($tmp_apply_contents as $key => $val) {
			$i = get_content_index($csv_cols, $key);
			if ($i !== false) {
				$cols[$i] = $val;
			}
		}

		$i = get_last_index($csv_cols, "添付ファイル");
		if ($i !== false) {
			$cols[$i] = format_col(format_apply_file_for_csv($apply_files[$tmp_apply["apply_id"]]));
		}

		$i = get_last_index($csv_cols, "承認者コメント");
		if ($i !== false) {
			$cols[$i] = format_col(format_comment_for_csv($approves[$tmp_apply["apply_id"]]));
		}

		foreach ($approves[$tmp_apply["apply_id"]] as $tmp_approve) {
			$tmp_no = $tmp_approve["apv_order"];
			if ($tmp_approve["apv_sub_order"] != "") {
				$tmp_no .= "-" . $tmp_approve["apv_sub_order"];
			}

			$i = get_last_index($csv_cols, "承認者{$tmp_no}役職");
			if ($i !== false) {
				$cols[$i] = format_col($tmp_approve["st_nm"]);
			}

			$i = get_last_index($csv_cols, "承認者{$tmp_no}氏名");
			if ($i !== false) {
				$cols[$i] = format_col(format_emp_name_for_csv($tmp_approve["emp_lt_nm"], $tmp_approve["emp_ft_nm"]));
			}

			$i = get_last_index($csv_cols, "承認者{$tmp_no}承認状況");
			if ($i !== false) {
				$cols[$i] = format_col(format_approve_status_for_csv($tmp_approve["apv_stat"], $tmp_approve["approve_label"]));
			}
		}

		$csv .= implode(",", $cols) . "\r\n";
	}

	return $csv;
}

function collect_apply_contents($wkfw_content_type, $apply_content) {
	$apply_content = preg_replace("/[\x01-\x08\x0b\x0c\x0e-\x1f\x7f]+/", "", $apply_content);
	$apply_content = str_replace("\0", "", $apply_content);

	if ($wkfw_content_type == "1" || strpos($apply_content, "<?xml") === false) {
		return array("テキスト入力" => format_col($apply_content));
	}

	if (!$dom = domxml_open_mem($apply_content)) {
		return array();
	}
	$root_nodes = $dom->get_elements_by_tagname('apply');
	if (count($root_nodes) == 0) {
		return array();
	}
	$root_node = $root_nodes[0];

	$contents = array();
	$child_nodes = $root_node->child_nodes();
	foreach ($child_nodes as $child_node) {
		if ($child_node->node_type() != XML_ELEMENT_NODE) {
			continue;
		}

		$disp_attribute = $child_node->get_attribute_node("disp");
		if ($disp_attribute) {
			$tag = mb_convert_encoding($disp_attribute->value(), "EUC-JP", "UTF-8");
		} else {
			$tag = mb_convert_encoding($child_node->tagname(), "EUC-JP", "UTF-8");
		}

		$grandchild_nodes = $child_node->child_nodes();
		if (count($grandchild_nodes) > 0) {
			$contents[$tag] = format_col(mb_convert_encoding($grandchild_nodes[0]->node_value(), "EUC-JP", "UTF-8"));
		}
	}

	return $contents;
}

function format_col($value) {
	$buf = str_replace("\r", "", $value);
	$buf = str_replace("\n", "", $buf);
	if (strpos($buf, ",") !== false)  {
		$buf = '"' . $buf . '"';
	}
	return $buf;
}

function get_content_index($csv_cols, $key) {
	static $indice;
	if (!is_array($indice)) {
		$indice = array();
	}
	if (isset($indice[$key])) {
		return $indice[$key];
	}
	for ($i = 7, $j = count($csv_cols); $i < $j; $i++) {
		if ($csv_cols[$i] == $key) {
			$indice[$key] = $i;
			return $i;
		}
	}
	return false;
}

function get_last_index($csv_cols, $key) {
	static $indice;
	if (!is_array($indice)) {
		$indice = array();
	}
	if (isset($indice[$key])) {
		return $indice[$key];
	}
	for ($i = count($csv_cols) -1; $i >= 0; $i--) {
		if ($csv_cols[$i] == $key) {
			$indice[$key] = $i;
			return $i;
		}
	}
	return false;
}

// 申請書一覧を取得
function get_apply_list_for_csv($con, $delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition, $mode, $apply_ids, $fname, $session) {
	$category        = $arr_condition["category"];
	$workflow        = $arr_condition["workflow"];
	$apply_title     = $arr_condition["apply_title"];
	$apply_emp_nm    = $arr_condition["apply_emp_nm"];
	$apply_yyyy_from = $arr_condition["date_y1"];
	$apply_mm_from   = $arr_condition["date_m1"];
	$apply_dd_from   = $arr_condition["date_d1"];
	$apply_yyyy_to   = $arr_condition["date_y2"];
	$apply_mm_to     = $arr_condition["date_m2"];
	$apply_dd_to     = $arr_condition["date_d2"];
	$apply_stat      = $arr_condition["apply_stat"];
	$class           = $arr_condition["class"];
	$attribute       = $arr_condition["attribute"];
	$dept            = $arr_condition["dept"];
	$room            = $arr_condition["room"];
	$apply_content   = $arr_condition["apply_content"];

	$sql  = "select ";
	$sql .= "a.*, ";
	$sql .= "b.short_wkfw_name, ";
	$sql .= "b.wkfw_title, ";
	$sql .= "b.approve_label, ";
	$sql .= "c.emp_lt_nm, ";
	$sql .= "c.emp_ft_nm, ";
	$sql .= "d.class_nm, ";
	$sql .= "e.atrb_nm, ";
	$sql .= "f.dept_nm, ";
	$sql .= "g.room_nm ";
	$sql .= "from apply a ";
	$sql .= "left join wkfwmst b on a.wkfw_id = b.wkfw_id ";
	$sql .= "left join empmst c on a.emp_id = c.emp_id ";
	$sql .= "left join classmst d on a.emp_class = d.class_id ";
	$sql .= "left join atrbmst e on a.emp_attribute = e.atrb_id ";
	$sql .= "left join deptmst f on a.emp_dept = f.dept_id ";
	$sql .= "left join classroom g on a.emp_room = g.room_id ";

	// 申請書指定の場合
	if ($mode == "1") {
		$cond = "where a.apply_id in (" . join(",", $apply_ids) . ") ";

	// 全件出力の場合
	} else {
		$cond = "where (not a.draft_flg) and a.delete_flg = '$delete_flg' ";

		if ($selected_cate != "") {
			$cond .= "and b.wkfw_type = $selected_cate ";
		}

		if ($selected_folder != "") {
			$cond .= "and b.wkfw_folder_id = $selected_folder ";
		}

		if ($selected_wkfw_id != "") {
			$cond .= "and b.wkfw_id = $selected_wkfw_id ";
		}

		// カテゴリ
		if ($category != "" && $category != "-") {
			$cond .= "and b.wkfw_type = $category ";
		}

		// 申請書名
		if ($workflow != "" && $workflow != "-") {
			$cond .= "and b.wkfw_id = $workflow ";
		}

		// 表題
		if ($apply_title != "") {
			$apply_title = p($apply_title);
			$cond .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if ($apply_emp_nm != "") {
			$apply_emp_nm = p($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if ($apply_stat != "" && $apply_stat != "-") {
			$cond .= "and a.apply_stat = '$apply_stat' ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$cond .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$cond .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 部署
		if ($class != "") {
			$cond .= "and a.emp_class = $class ";
		}
		if ($attribute != "") {
			$cond .= "and a.emp_attribute = $attribute ";
		}
		if ($dept != "") {
			$cond .= "and a.emp_dept = $dept ";
		}
		if ($room != "") {
			$cond .= "and a.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = explode(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "a.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}
	}

	$cond .= "order by a.apply_date desc, a.apply_no desc ";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	return pg_fetch_all($sel);
}

function get_apply_ids_for_csv($apply_list) {
	$apply_ids = array();
	foreach ($apply_list as $tmp_apply) {
		$apply_ids[] = $tmp_apply["apply_id"];
	}
	return $apply_ids;
}

function get_apply_files_for_csv($con, $apply_ids, $fname, $session) {
	if (count($apply_ids) == 0) {
		return array();
	}

	$sql  = "select apply_id, applyfile_name from applyfile";
	$cond_in = "where apply_id in(";

	$arr_key = 0;
	$apply_files = array();

	// 申請書が1万件を越えるとエラーになるため、5000回ずつに分けてSQLを実行
	while (1) {
		$arr_slice = array();
		$arr_slice = array_slice($apply_ids, $arr_key, 5000);

		$cond_ids = implode(",", $arr_slice);
		$cond_ids .= ") order by apply_id, applyfile_no";
		$sel = select_from_table($con, $sql, $cond_in.$cond_ids, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while ($row = pg_fetch_array($sel)) {
			$apply_files[$row["apply_id"]][] = $row["applyfile_name"];
		}

		// 結果が5000件であれば、要素をずらして再度 SELECT句を実行
		if (5000 == count($arr_slice)) {
			$arr_key = $arr_key + 5000;
		}
		else {
			break;
		}
	}
	return $apply_files;
}

function get_approves_for_csv($con, $apply_ids, $fname, $session) {
	if (count($apply_ids) == 0) {
		return array();
	}

	$sql  = "select a.*, b.st_nm, c.emp_lt_nm, c.emp_ft_nm from applyapv a left join stmst b on b.st_id = a.emp_st left join empmst c on c.emp_id = a.emp_id";
	$cond_in = "where a.apply_id in (";

	$arr_key = 0;
	$approves = array();

	// 申請書が1万件を越えるとエラーになるため、5000回ずつに分けてSQLを実行
	while (1) {
		$arr_slice = array();
		$arr_slice = array_slice($apply_ids, $arr_key, 5000);

		$cond_ids = implode(",", $arr_slice);
		$cond_ids .= ") and (not a.delete_flg) order by a.apv_order, a.apv_sub_order";
		$sel = select_from_table($con, $sql, $cond_in.$cond_ids, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while ($row = pg_fetch_array($sel, NULL, PGSQL_ASSOC)) {
			$approves[$row["apply_id"]][] = $row;
		}

		// 結果が5000件であれば、要素をずらして再度 SELECT句を実行
		if (5000 == count($arr_slice)) {
			$arr_key = $arr_key + 5000;
		}
		else {
			break;
		}
	}
	return $approves;
}

function format_apply_no_for_csv($short_wkfw_name, $apply_date, $apply_no) {
	$year          = substr($apply_date, 0, 4);
	$month_and_day = substr($apply_date, 4, 4);
	if ($month_and_day >= "0101" && $month_and_day <= "0331") {
		$year = $year - 1;
	}
	return $short_wkfw_name . "-" . $year . sprintf("%04d", $apply_no);
}

function format_emp_name_for_csv($lt_nm, $ft_nm) {
	return $lt_nm . " " . $ft_nm;
}

function format_date_for_csv($ymd) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $ymd);
}

function format_apply_status_for_csv($apply_stat, $approve_label, $approves) {
	$status_prefix = ($approve_label != "2") ? "承認" : "確認";

	switch ($apply_stat) {
	case "0":
		foreach ($approves as $tmp_approve) {
			if ($tmp_approve["apv_stat"] != 0) {
				return "一部" . $status_prefix;
			}
		}
		return "未" . $status_prefix;

	case "1":
		return $status_prefix . "確定";

	case "2":
		return "否認";

	case "3":
		return "差戻し";
	}
}

function format_emp_dept_for_csv($class_nm, $atrb_nm, $dept_nm, $room_nm) {
	if ($class_nm == "" || $atrb_nm == "" || $dept_nm == "") {
		return "";
	}

	$emp_dept = "$class_nm > $atrb_nm > $dept_nm";
	if ($room_nm != "") {
		$emp_dept .= " > $room_nm";
	}

	return $emp_dept;
}

function format_apply_file_for_csv($apply_files) {
	return join("、", $apply_files);
}

function format_comment_for_csv($approves) {
	$comment = "";
	foreach ($approves as $tmp_approve) {
		if ($tmp_approve["apv_comment"] == "") {
			continue;
		}
		$tmp_emp_name = format_emp_name_for_csv($tmp_approve["emp_lt_nm"], $tmp_approve["emp_ft_nm"]);
		$comment .= "【{$tmp_emp_name}さんのコメント】{$tmp_approve["apv_comment"]}";
	}
	return $comment;
}

function format_approve_status_for_csv($apv_stat, $approve_label) {
	$status_prefix = ($approve_label != "2") ? "承認" : "確認";

	switch ($apv_stat) {
	case "0":
		return "未" . $status_prefix;

	case "1":
		return $status_prefix;

	case "2":
		return "否認";

	case "3":
		return "差戻し";
	}
}
