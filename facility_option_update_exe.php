<?

require("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 登録値の編集
if ($default_cate_id == "") {
    $default_cate_id = null;
}

// データベースに接続
$con = connect2db($fname);

// 初期画面設定を更新
$sql = "update option set";
$set = array("facility_default", "calendar_start3", "default_fclcate");
$setvalue = array($default_val, $calendar_start, $default_cate_id);
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
switch ($default_val) {
    case "1":  // 日
        $url = "facility_menu.php?session=$session&date=$date";
        break;
    case "2":  // 週（表）
        $url = "facility_week.php?session=$session&date=$date";
        break;
    case "4":  // 週（チャート）
        $url = "facility_week_chart.php?session=$session&date=$date&cate_id=$default_cate_id";
        break;
    case "3":  // 月
        $url = "facility_month.php?session=$session&date=$date";
        break;
}
$f_url = urlencode($url);
echo("<script type=\"text/javascript\">parent.location.href = 'main_menu.php?session=$session&f_url=$f_url';</script>");
