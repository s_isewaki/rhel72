<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 入院情報</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("conf/sql.inf");
require_once("show_in_search.ini");
require_once("inpatient_common.php");

$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// チェック情報を取得
$sql = "select * from bedcheck";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 46; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}

// 患者情報を取得
$sql = "select ptif_sex, ptif_birth from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$sex = get_sex_string(pg_fetch_result($sel, 0, "ptif_sex"));
$age = get_age(pg_fetch_result($sel, 0, "ptif_birth"));

// 入院情報の取得
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $SQL111, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "inpt_lt_kj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "inpt_ft_kj_nm");
$enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$ptrm = pg_fetch_result($sel,0,"ptrm_room_no");
if ($back != "t") {
	$in_dt = pg_fetch_result($sel, 0, "inpt_in_dt");
	$in_yr = substr($in_dt, 0, 4);
	$in_mon = substr($in_dt, 4, 2);
	$in_day = substr($in_dt, 6, 2);
	$in_tm = pg_fetch_result($sel, 0, "inpt_in_tm");
	$in_hr = substr($in_tm, 0, 2);
	$in_min = substr($in_tm, 2, 2);

	$arrival = pg_fetch_result($sel, 0, "inpt_arrival");
	$arv_f_h = substr($arrival, 0, 2);
	$arv_f_m = substr($arrival, 2, 2);
	$arv_t_h = substr($arrival, 4, 2);
	$arv_t_m = substr($arrival, 6, 2);

	$sect = pg_fetch_result($sel, 0, "inpt_sect_id");
	$doc = pg_fetch_result($sel, 0, "dr_id");
	$nurse = pg_fetch_result($sel, 0, "nurse_id");
	$disease = pg_fetch_result($sel, 0, "inpt_disease");

	$patho_from = pg_fetch_result($sel, 0, "inpt_patho_from");
	$patho_from_year = substr($patho_from, 0, 4);
	$patho_from_month = substr($patho_from, 4, 2);
	$patho_from_day = substr($patho_from, 6, 2);
	$patho_to = pg_fetch_result($sel, 0, "inpt_patho_to");
	$patho_to_year = substr($patho_to, 0, 4);
	$patho_to_month = substr($patho_to, 4, 2);
	$patho_to_day = substr($patho_to, 6, 2);

	$ope_dt_flg = pg_result($sel, 0, "inpt_ope_dt_flg");
	if ($ope_dt_flg == "t") {
		$ope_dt = pg_result($sel, 0, "inpt_ope_dt");
		$ope_yr = substr($ope_dt, 0, 4);
		$ope_mon = substr($ope_dt, 4, 2);
		$ope_day = substr($ope_dt, 6, 2);
		$opedtundec = "f";
	} else if ($ope_dt_flg == "f") {
		$ope_ym = pg_result($sel, 0, "inpt_ope_ym");
		$ope_u_yr = substr($ope_ym, 0, 4);
		$ope_u_mon = substr($ope_ym, 4, 2);
		$ope_u_td = pg_result($sel, 0, "inpt_ope_td");
		$opedtundec = "t";
	}
	$short_stay = pg_fetch_result($sel, 0, "inpt_short_stay");
	$purpose_cd = pg_fetch_result($sel, 0, "inpt_purpose_cd");
	$purpose_detail = pg_fetch_result($sel, 0, "inpt_purpose_content");
	$sample_blood = pg_fetch_result($sel, 0, "inpt_sample_blood");
	$outpt_test = pg_fetch_result($sel, 0, "inpt_outpt_test");
	$diagnosis = pg_fetch_result($sel, 0, "inpt_diagnosis");
	$emergency = pg_fetch_result($sel, 0, "inpt_emergency");
	$period = pg_fetch_result($sel, 0, "inpt_in_plan_period");
	$way = pg_fetch_result($sel, 0, "inpt_in_way");
	$nursing = pg_fetch_result($sel, 0, "inpt_nursing");
	$staple_fd = pg_fetch_result($sel, 0, "inpt_staple_fd");
	$fd_type = pg_fetch_result($sel, 0, "inpt_fd_type");
	$fd_dtl = pg_fetch_result($sel, 0, "inpt_fd_dtl");
	$wish_rm_cd = pg_fetch_result($sel, 0, "inpt_wish_rm_cd");
	$fd_start = pg_fetch_result($sel, 0, "inpt_fd_start");
	$insurance = pg_fetch_result($sel, 0, "inpt_insurance");
	$insu_rate_cd = pg_fetch_result($sel, 0, "inpt_insu_rate_cd");
	$reduced = pg_fetch_result($sel, 0, "inpt_reduced");
	$impaired = pg_fetch_result($sel, 0, "inpt_impaired");
	$exemption = pg_fetch_result($sel, 0, "inpt_exemption");
	$insured = pg_fetch_result($sel, 0, "inpt_insured");
	$privacy_flg = pg_fetch_result($sel, 0, "inpt_privacy_flg");
	$privacy_text = pg_fetch_result($sel, 0, "inpt_privacy_text");
	$pre_div = pg_fetch_result($sel, 0, "inpt_pre_div");
	$intro_inst_cd = pg_fetch_result($sel, 0, "inpt_intro_inst_cd");
	$intro_sect_cd = pg_fetch_result($sel, 0, "inpt_intro_sect_cd");
	$intro_doctor_no = pg_fetch_result($sel, 0, "inpt_intro_doctor_no");
	$hotline = pg_fetch_result($sel, 0, "inpt_hotline");
	$observe = pg_fetch_result($sel, 0, "inpt_observe");
	$meet = pg_fetch_result($sel, 0, "inpt_meet_flg");
	$dcb_cls = pg_fetch_result($sel, 0, "inpt_dcb_cls");

	$dcb_bas = pg_fetch_result($sel, 0, "inpt_dcb_bas");
	$dcb_bas_year = substr($dcb_bas, 0, 4);
	$dcb_bas_month = substr($dcb_bas, 4, 2);
	$dcb_bas_day = substr($dcb_bas, 6, 2);

	$dcb_exp = pg_fetch_result($sel, 0, "inpt_dcb_exp");
	$dcb_exp_year = substr($dcb_exp, 0, 4);
	$dcb_exp_month = substr($dcb_exp, 4, 2);
	$dcb_exp_day = substr($dcb_exp, 6, 2);

	$rhb_cls = pg_fetch_result($sel, 0, "inpt_rhb_cls");

	$rhb_bas = pg_fetch_result($sel, 0, "inpt_rhb_bas");
	$rhb_bas_year = substr($rhb_bas, 0, 4);
	$rhb_bas_month = substr($rhb_bas, 4, 2);
	$rhb_bas_day = substr($rhb_bas, 6, 2);

	$rhb_exp = pg_fetch_result($sel, 0, "inpt_rhb_exp");
	$rhb_exp_year = substr($rhb_exp, 0, 4);
	$rhb_exp_month = substr($rhb_exp, 4, 2);
	$rhb_exp_day = substr($rhb_exp, 6, 2);

	$care_no = pg_fetch_result($sel, 0, "inpt_care_no");
	$care_grd_cd = pg_fetch_result($sel, 0, "inpt_care_grd_cd");

	$care_apv = pg_fetch_result($sel, 0, "inpt_care_apv");
	$care_apv_year = substr($care_apv, 0, 4);
	$care_apv_month = substr($care_apv, 4, 2);
	$care_apv_day = substr($care_apv, 6, 2);

	$care_from = pg_fetch_result($sel, 0, "inpt_care_from");
	$care_from_year = substr($care_from, 0, 4);
	$care_from_month = substr($care_from, 4, 2);
	$care_from_day = substr($care_from, 6, 2);
	$care_to = pg_fetch_result($sel, 0, "inpt_care_to");
	$care_to_year = substr($care_to, 0, 4);
	$care_to_month = substr($care_to, 4, 2);
	$care_to_day = substr($care_to, 6, 2);

	$dis_grd_cd = pg_fetch_result($sel, 0, "inpt_dis_grd_cd");
	$dis_type_cd = pg_fetch_result($sel, 0, "inpt_dis_type_cd");
	$spec_name = pg_fetch_result($sel, 0, "inpt_spec_name");

	$spec_from = pg_fetch_result($sel, 0, "inpt_spec_from");
	$spec_from_year = substr($spec_from, 0, 4);
	$spec_from_month = substr($spec_from, 4, 2);
	$spec_from_day = substr($spec_from, 6, 2);
	$spec_to = pg_fetch_result($sel, 0, "inpt_spec_to");
	$spec_to_year = substr($spec_to, 0, 4);
	$spec_to_month = substr($spec_to, 4, 2);
	$spec_to_day = substr($spec_to, 6, 2);

	$nrs_obsv = pg_fetch_result($sel, 0, "inpt_nrs_obsv");
	$free = pg_fetch_result($sel, 0, "inpt_free");
	$special = pg_fetch_result($sel, 0, "inpt_special");
}

// 棟名の取得
$cond = "where bldg_cd = '$bldg_cd'";
$sel_bldg = select_from_table($con,$SQL92,$cond,$fname);		//bldgmstから取得
if($sel_bldg == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$bldg_nm = pg_fetch_result($sel_bldg,0,"bldg_name");

// 病棟名の取得
$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd'";
$sel_wd = select_from_table($con,$SQL67,$cond,$fname);		//wardmstから取得
if($sel_wd == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$ward_nm = pg_fetch_result($sel_wd,0,"ward_name");

// 病室名の取得
$cond = "where bldg_cd='$bldg_cd' and ward_cd='$ward_cd' and ptrm_room_no = '$ptrm'";
$sel_rm = select_from_table($con,$SQL94,$cond,$fname);		//ptrmmstから取得
if($sel_rm == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$ptrm_nm = pg_fetch_result($sel_rm,0,"ptrm_name");
$bed_chg = pg_fetch_result($sel_rm, 0, "ptrm_bed_chg");
if ($bed_chg > 0) {
	$ptrm_nm .= "（差額" . number_format($bed_chg) . "円）";
}
$ptrm_nm .= "&nbsp;<input type='button' value='設備詳細' onclick=\"window.open('room_equipment_detail.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd&rm_no=$ptrm', 'newwin', 'width=640,height=480,scrollbars=yes');\">";

$bed = pg_fetch_result($sel,0,"inpt_bed_no");

$arr_sectdr = array();
$arr_sectnurse = array();

// 診療科一覧の取得
$sql = "select * from sectmst";
$cond = "where enti_id = '$enti_id' and sect_del_flg = 'f' order by sect_id";
$sel_sect = select_from_table($con, $sql, $cond, $fname);
if ($sel_sect == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$arr_sect = array();

// 全診療科をループ
while ($row = pg_fetch_array($sel_sect)) {
	$sect_id = $row["sect_id"];
	$sect_nm = $row["sect_nm"];
	array_push($arr_sect, array("sect_id" => $sect_id, "sect_nm" => $sect_nm));

	// 主治医一覧の取得
	$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id' and dr_del_flg = 'f' order by dr_id";
	$sel_dr = select_from_table($con, $SQL90, $cond, $fname);
	if ($sel_dr == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_dr = array();

	// 全主治医をループ
	while ($row = pg_fetch_array($sel_dr)) {
		$dr_id = $row["dr_id"];
		$dr_nm = $row["dr_nm"];
		array_push($arr_dr, array("dr_id" => $dr_id, "dr_nm" => $dr_nm));
	}

	array_push($arr_sectdr, array("sect_id" => $sect_id, "drs" => $arr_dr));

	// 看護師一覧の取得
	$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id' and nurse_del_flg = 'f' order by nurse_id";
	$sel_nurse = select_from_table($con, $SQL107, $cond, $fname);
	if ($sel_nurse == 0) {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_nurse = array();

	// 全看護師をループ
	while ($row = pg_fetch_array($sel_nurse)) {
		$nurse_id = $row["nurse_id"];
		$nurse_nm = $row["nurse_nm"];
		array_push($arr_nurse, array("nurse_id" => $nurse_id, "nurse_nm" => $nurse_nm));
	}

	array_push($arr_sectnurse, array("enti_id" => $enti_id, "sect_id" => $sect_id, "nurses" => $arr_nurse));
}

// 入院目的マスタを取得
list($purpose_rireki, $purpose_master) = get_rireki_index_and_master($con, "A306", $fname);

// 希望病室マスタを取得
list($wish_rm_rireki, $wish_rm_master) = get_rireki_index_and_master($con, "A305", $fname);

// 医療保険負担割合マスタを取得
list($insu_rate_rireki, $insu_rate_master) = get_rireki_index_and_master($con, "A301", $fname);

// 回復期リハ算定上限日数を取得
$decubation_max_days = get_decubation_max_days($con, $fname);

// リハビリ算定期限日数を取得
$rehabilitation_limit_days = get_rehabilitation_limit_days($con, $fname);

// 要介護度マスタを取得
list($care_grd_rireki, $care_grd_master) = get_rireki_index_and_master($con, "A302", $fname);

// 身体障害者手帳等級マスタを取得
list($dis_grd_rireki, $dis_grd_master) = get_rireki_index_and_master($con, "A303", $fname);

// 身体障害者障害種別マスタを取得
list($dis_type_rireki, $dis_type_master) = get_rireki_index_and_master($con, "A304", $fname);

$url_pt_nm = urlencode($pt_nm);

// 医療機関マスタを取得
$sql = "select a.mst_cd, a.mst_name, b.rireki_index from institemmst a left join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd";
$cond = "order by a.mst_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intro_insts = array();
while ($row = pg_fetch_array($sel)) {
	$intro_insts[$row["mst_cd"]] = array(
		"name"   => $row["mst_name"],
		"rireki" => $row["rireki_index"],
		"sects"  => array()
	);
}

// 診療科マスタを取得（最新の履歴のみ）
$sql = "select a.* from institemrireki a inner join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd and b.rireki_index = a.rireki_index";
$cond = "where a.disp_flg order by a.disp_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$intro_insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["name"] = $row["item_name"];
	for ($i = 1; $i <= 10; $i++) {
		if ($row["doctor_name$i"] == "" || $row["doctor_hide$i"] == "t") {
			continue;
		}
		$intro_insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["doctors"][$i] = $row["doctor_name$i"];
	}
}

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
	$jobs[$row["job_id"]] = array("name" => $row["job_nm"], "employees" => array());
}

// 職種一覧に職員情報を追加
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_job from empmst";
$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') order by emp_kn_lt_nm, emp_kn_ft_nm";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$jobs[$row["emp_job"]]["employees"][] = array("id" => $row["emp_id"], "name" => "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}");
}

// 対象職員情報を配列に格納
$arr_target = array();
if ($back != "t") {
	$sql = "select inptop.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, jobmst.job_nm from inptop inner join empmst on empmst.emp_id = inptop.emp_id inner join jobmst on jobmst.job_id = empmst.emp_job";
	$cond = "where inptop.ptif_id = '$pt_id' order by inptop.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$arr_target[] = array("id" => $row["emp_id"], "name" => "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}（{$row["job_nm"]}）");
	}
} else if ($target_id_list1 != "") {
	$arr_target_id = split(",", $target_id_list1);
	for ($i = 0, $j = count($arr_target_id); $i < $j; $i++) {
		$tmp_emp_id = $arr_target_id[$i];
		$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, jobmst.job_nm from empmst inner join jobmst on jobmst.job_id = empmst.emp_job";
		$cond = "where empmst.emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr_target[] = array("id" => $tmp_emp_id, "name" => pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm") . "（" . pg_fetch_result($sel, 0, "job_nm") . "）");
	}
}

require_once("emplist_caller_javascript.php");
insert_javascript();
?>
<script language="javascript">
<?
$script = "m_target_list['1'] = new Array(";
$is_first = true;
foreach ($arr_target as $row) {
	if ($is_first) {
		$is_first = false;
	} else {
		$script .= ",";
	}
	$script .= "new user_info('{$row["id"]}','{$row["name"]}')";
}
$script .= ");\n";
echo($script);
?>

function initPage() {
	document.inpt.sect.value = '<? echo($sect); ?>';
	sectOnChange('<? echo($doc); ?>', '<? echo($nurse); ?>');
	sectOnChange2('<? echo($search_doc); ?>');
	opedtundecOnClick();

	switch ('<? echo($intro_inst_cd); ?>') {
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		if (document.inpt.intro_inst_name) {
			document.inpt.intro_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		}
		document.inpt.intro_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}
	setIntroSectOptions('<? echo($intro_sect_cd); ?>', '<? echo($intro_doctor_no); ?>');

<? if ($display22 == "t") { ?>
	update_target_html('1');
	jobOnChange();
<? } ?>
}

function sectOnChange(doc, nurse) {

	var sect_id = document.inpt.sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.doc);

	// 主治医セレクトボックスのオプションを作成
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.inpt.doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.doc.options.length == 0) {
		addOption(document.inpt.doc, '0', '（未登録）', doc);
	} else {
		addOption(document.inpt.doc, '0', '', doc);
	}

	if (!document.inpt.nurse) {
		return;
	}

	// 担当看護師セレクトボックスのオプションを全削除
	deleteAllOptions(document.inpt.nurse);

	// 担当看護師セレクトボックスのオプションを作成
<? foreach ($arr_sectnurse as $sectnurse) { ?>
	if (sect_id == '<? echo $sectnurse["sect_id"]; ?>') {
	<? foreach($sectnurse["nurses"] as $nurseitem) { ?>
		addOption(document.inpt.nurse, '<? echo $nurseitem["nurse_id"]; ?>', '<? echo $nurseitem["nurse_nm"]; ?>', nurse);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.inpt.nurse.options.length == 0) {
		addOption(document.inpt.nurse, '0', '（未登録）', nurse);
	} else {
		addOption(document.inpt.nurse, '0', '', nurse);
	}

}

function sectOnChange2(doc) {
	var sect_id = document.search.search_sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.search.search_doc);

	// 主治医セレクトボックスのオプションを作成
	addOption(document.search.search_doc, '0', 'すべて', doc);
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.search.search_doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}

}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';

}

function opedtundecOnClick() {
	if (!document.inpt.opedtundec) {
		return;
	}
	var opedtDisabled = (document.inpt.opedtundec.checked);
	document.inpt.ope_yr.disabled = opedtDisabled;
	document.inpt.ope_mon.disabled = opedtDisabled;
	document.inpt.ope_day.disabled = opedtDisabled;
	document.inpt.ope_u_yr.disabled = !opedtDisabled;
	document.inpt.ope_u_mon.disabled = !opedtDisabled;
	document.inpt.ope_u_td.disabled = !opedtDisabled;
}

function setDcbExp() {
	if (!document.inpt.dcb_bas_year) {return;}

	var dcb_cls = document.inpt.dcb_cls.value;
	if (dcb_cls == '') {return;}

	var dcb_bas_year = document.inpt.dcb_bas_year.value;
	if (dcb_bas_year == '-') {return;}
	dcb_bas_year = parseInt(dcb_bas_year, 10);

	var dcb_bas_month = document.inpt.dcb_bas_month.value;
	if (dcb_bas_month == '-') {return;}
	dcb_bas_month = parseInt(dcb_bas_month, 10);

	var dcb_bas_day = document.inpt.dcb_bas_day.value;
	if (dcb_bas_day == '-') {return;}
	dcb_bas_day = parseInt(dcb_bas_day, 10);

	var max_days;
	switch (dcb_cls) {
<? for ($i = 1; $i <= 6; $i++) { ?>
	case '<? echo($i); ?>':
		max_days = <? echo($decubation_max_days[$i]); ?>;
		break;
<? } ?>
	}

	var dcb_exp_date = computeDate(dcb_bas_year, dcb_bas_month, dcb_bas_day, max_days - 1);

	var dcb_exp_year = dcb_exp_date.getFullYear().toString();

	var dcb_exp_month = (dcb_exp_date.getMonth() + 1).toString();
	if (dcb_exp_month.length == 1) {dcb_exp_month = '0'.concat(dcb_exp_month);}

	var dcb_exp_day = dcb_exp_date.getDate().toString();
	if (dcb_exp_day.length == 1) {dcb_exp_day = '0'.concat(dcb_exp_day);}

	document.inpt.dcb_exp_year.value = dcb_exp_year;
	document.inpt.dcb_exp_month.value = dcb_exp_month;
	document.inpt.dcb_exp_day.value = dcb_exp_day;
}

function setRhbExp() {
	if (!document.inpt.rhb_bas_year) {return;}

	var rhb_cls = document.inpt.rhb_cls.value;
	if (rhb_cls == '') {return;}

	var rhb_bas_year = document.inpt.rhb_bas_year.value;
	if (rhb_bas_year == '-') {return;}
	rhb_bas_year = parseInt(rhb_bas_year, 10);

	var rhb_bas_month = document.inpt.rhb_bas_month.value;
	if (rhb_bas_month == '-') {return;}
	rhb_bas_month = parseInt(rhb_bas_month, 10);

	var rhb_bas_day = document.inpt.rhb_bas_day.value;
	if (rhb_bas_day == '-') {return;}
	rhb_bas_day = parseInt(rhb_bas_day, 10);

	var max_days;
	switch (rhb_cls) {
<? for ($i = 1; $i <= 5; $i++) { ?>
	case '<? echo($i); ?>':
		max_days = <? echo($rehabilitation_limit_days[$i]); ?>;
		break;
<? } ?>
	}

	var rhb_exp_date = computeDate(rhb_bas_year, rhb_bas_month, rhb_bas_day, max_days - 1);

	var rhb_exp_year = rhb_exp_date.getFullYear().toString();

	var rhb_exp_month = (rhb_exp_date.getMonth() + 1).toString();
	if (rhb_exp_month.length == 1) {rhb_exp_month = '0'.concat(rhb_exp_month);}

	var rhb_exp_day = rhb_exp_date.getDate().toString();
	if (rhb_exp_day.length == 1) {rhb_exp_day = '0'.concat(rhb_exp_day);}

	document.inpt.rhb_exp_year.value = rhb_exp_year;
	document.inpt.rhb_exp_month.value = rhb_exp_month;
	document.inpt.rhb_exp_day.value = rhb_exp_day;
}

function computeDate(year, month, day, addDays) {
	var dt = new Date(year, month - 1, day);
	var baseSec = dt.getTime();
	var addSec = addDays * 86400000;
	var targetSec = baseSec + addSec;
	dt.setTime(targetSec);
	return dt;
}

function openInstSearchWindow() {
	window.open('bed_inst_search.php?session=<? echo($session); ?>', 'instsrch', 'width=640,height=480,scrollbars=yes');
}

function clearInst() {
	document.inpt.intro_inst_cd.value = '';
	document.inpt.intro_inst_name.value = '';
	document.inpt.intro_sect_rireki.value = '';
	if (document.inpt.intro_sect_cd) {
		clearOptions(document.inpt.intro_sect_cd);
	}
	if (document.inpt.intro_doctor_no) {
		clearOptions(document.inpt.intro_doctor_no);
	}
}

function selectInst(cd) {
	switch (cd) {
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		document.inpt.intro_inst_cd.value = '<? echo($tmp_inst_cd); ?>';
		document.inpt.intro_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		document.inpt.intro_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}
	setIntroSectOptions();
}

function setIntroSectOptions(defaultSectCd, defaultDoctorNo) {
	if (!document.inpt.intro_sect_cd) {
		return;
	}

	clearOptions(document.inpt.intro_sect_cd);

	var instCd = document.inpt.intro_inst_cd.value;
	if (!instCd) {
		return;
	}

	switch (instCd) {
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
		addOption(document.inpt.intro_sect_cd, '<? echo($tmp_sect_cd); ?>', '<? echo($tmp_sect["name"]); ?>', defaultSectCd);
<? } ?>
		break;
<? } ?>
	}

	setIntroDoctorOptions(defaultDoctorNo);
}

function setIntroDoctorOptions(defaultDoctorNo) {
	if (!document.inpt.intro_doctor_no) {
		return;
	}

	clearOptions(document.inpt.intro_doctor_no);

	var sectCd = document.inpt.intro_sect_cd.value;
	if (!sectCd) {
		return;
	}

	var instCd = document.inpt.intro_inst_cd.value;
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
	if (instCd == '<? echo($tmp_inst_cd); ?>' && sectCd == '<? echo($tmp_sect_cd); ?>') {
		<? foreach ($tmp_sect["doctors"] as $tmp_doctor_no => $tmp_doctor_name) { ?>
			addOption(document.inpt.intro_doctor_no, '<? echo($tmp_doctor_no); ?>', '<? echo($tmp_doctor_name); ?>', defaultDoctorNo);
		<? } ?>
	}
	<? } ?>
<? } ?>
}

function clearOptions(selectbox) {
	for (var i = selectbox.length - 1; i > 0; i--) {
		selectbox.options[i] = null;
	}
}

var childwin = null;
function openEmployeeList() {
	var childWidth = 720;
	var url = 'emplist_popup.php?session=<? echo($session); ?>&emp_id=<? echo($src_emp_id); ?>&item_id=1&mode=20';
	childwin = window.open(url, 'emplistpopup', 'left=' + (screen.width - childWidth) + ',top=0,width=' + childWidth + ',height=600,scrollbars=yes,resizable=yes');
	childwin.focus();
}

function clear_target(emp_id, emp_name) {
	if (!confirm('担当者を全削除します。よろしいですか？')) {
		return;
	}

	var is_exist_flg = false;
	for (var i = 0, j = m_target_list['1'].length; i < j; i++) {
		if (m_target_list['1'][i].emp_id == emp_id) {
			is_exist_flg = true;
			break;
		}
	}
	m_target_list['1'] = new Array();
	if (is_exist_flg) {
		m_target_list['1'] = array_add(m_target_list['1'], new user_info(emp_id, emp_name));
	}
	update_target_html('1');
}

function jobOnChange() {
	deleteAllOptions(document.inpt.emplist);

	var job_id = document.inpt.job.value;
<? foreach ($jobs as $tmp_job_id => $tmp_job) { ?>
	if (job_id == '<? echo($tmp_job_id); ?>') {
<? foreach ($tmp_job["employees"] as $tmp_employee) { ?>
		addOption(document.inpt.emplist, '<? echo($tmp_employee["id"]); ?>', '<? echo($tmp_employee["name"]); ?>');
<? } ?>
	}
<? } ?>
}

function addEmp() {
	var job = document.inpt.job.options[document.inpt.job.selectedIndex].text;

	var emp_id_str = '';
	var emp_name_str = '';
	for (var i = 0, j = document.inpt.emplist.options.length; i < j; i++) {
		if (!document.inpt.emplist.options[i].selected) continue;
		if (emp_id_str != '') {
			emp_id_str += ', ';
			emp_name_str += ', ';
		}
		emp_id_str += document.inpt.emplist.options[i].value;
		emp_name_str += document.inpt.emplist.options[i].text + '（' + job + '）';
	}
	if (emp_id_str != '') add_target_list('1', emp_id_str, emp_name_str);
}

function selectAllEmp() {
	for (var i = 0, j = document.inpt.emplist.options.length; i < j; i++) {
		document.inpt.emplist.options[i].selected = true;
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list2 {border-collapse:collapse;}
.list2 td {border:#0e9c55 solid 1px;}

.list3 {border-collapse:collapse;}
.list3 td {border:#b8860b solid 1px;}

.list4 {border-collapse:collapse;}
.list4 td {border:#c71585 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院患者検索</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? write_search_form($pt_nm, $arr_sect, $search_sect, $session); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- タブ2 -->
<? if ($path == "A") {  // 入院患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入院情報</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "B") {  // 退院予定患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入院情報</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_out_reserve.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定情報</font></a>
</td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="inpt" action="inpatient_update_confirm.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="15%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="18%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="15%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="18%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
<td width="8%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td width="9%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sex); ?></font></td>
<td width="8%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td width="9%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($age); ?>歳</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td width="28%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$bldg_nm$ward_nm"); ?></font></td>
<td width="15%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病室</font></td>
<td width="33%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($ptrm_nm); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベット番号</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($bed); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="in_yr">
<? show_select_years_span(min($in_yr, 1960), date("Y") + 1, $in_yr, false); ?>
</select>/<select name="in_mon">
<? show_select_months($in_mon); ?>
</select>/<select name="in_day">
<? show_select_days($in_day); ?>
</select>&nbsp;<select name="in_hr">
<? show_select_hrs_0_23($in_hr); ?>
</select>：<select name="in_min">
<option value="00"<? if ($in_min == "00") {echo " selected";} ?>>00
<option value="30"<? if ($in_min == "30") {echo " selected";} ?>>30
</select>
</font></td>
</tr>
<? if ($display19 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">到着時間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="arv_f_h"><? show_hour_options_0_23($arv_f_h, true); ?></select>：<select name="arv_f_m">
<option value="">
<?
for ($i = 0; $i <= 50; $i += 10) {
	$tmp_val = sprintf("%02d", $i);
	echo("<option value=\"$tmp_val\"");
	if ($arv_f_m == $tmp_val) {echo(" selected");}
	echo(">$tmp_val");
}
?>
</select> 〜 <select name="arv_t_h"><? show_hour_options_0_23($arv_t_h, true); ?></select>：<select name="arv_t_m">
<option value="">
<?
for ($i = 0; $i <= 50; $i += 10) {
	$tmp_val = sprintf("%02d", $i);
	echo("<option value=\"$tmp_val\"");
	if ($arv_t_m == $tmp_val) {echo(" selected");}
	echo(">$tmp_val");
}
?>
</select>
</font></td>
</tr>
<? } ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td>
<select name="sect" onChange="sectOnChange();">
<?
foreach ($arr_sect as $sect_item) {
	$temp_sect_id = $sect_item["sect_id"];
	$temp_sect_nm = $sect_item["sect_nm"];
	echo "<option value='$temp_sect_id'>$temp_sect_nm";
}
?>
</select>
</td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主治医</font></td>
<td>
<select name="doc">
</select>
</td>
</tr>
<? if ($display1 == "t" || $display2 == "t") { ?>
<tr height="22">
<? if ($display1 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当看護師</font></td>
<td<? if ($display2 != "t") {echo(' colspan="3"');} ?>><select name="nurse"></select></td>
<? } ?>
<? if ($display2 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診断名(主病名)</font></td>
<td<? if ($display1 != "t") {echo(' colspan="3"');} ?>><input type="text" name="disease" size="35" value="<? echo($disease); ?>"></td>
<? } ?>
</tr>
<? } ?>
<? if ($display22 == "t") { ?>
<tr>
<td bgcolor="#f6f9ff" align="right">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者</font><br>
<input type="button" value="職員名簿" style="width:5.5em;" onclick="openEmployeeList();"><br>
<input type="button" value="クリア" style="width:5.5em;" onclick="clear_target('<? echo($src_emp_id); ?>', '<? echo($src_emp_nm); ?>');">
</td>
<td colspan="3">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" style="border:#5279a5 solid 1px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="target_disp_area1"></span></font></td>
<td><input type="button" value="&lt;&nbsp;追加" style="margin-left:1em;width=4.5em;" onclick="addEmp();"></td>
<td width="10"></td>
<td>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="1" class="non_in_list">
<tr>
<td height="45%" align="center" valign="bottom">
<select name="job" onchange="jobOnChange();">
<?
foreach ($jobs as $tmp_job_id => $tmp_job) {
	echo("<option value=\"$tmp_job_id\"");
	if ($job == $tmp_job_id) {echo(" selected");}
	echo(">{$tmp_job["name"]}</option>\n");
}
?>
</select>
</td>
</tr>
<tr>
<td><select name="emplist" size="10" multiple style="width:150px;"></select></td>
</tr>
<tr>
<td height="45%" valign="top"><input type="button" value="全て選択" onclick="selectAllEmp();"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<? } ?>
<? if ($display21 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発症年月日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="patho_from_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $patho_from_year, false); ?>
</select>/<select name="patho_from_month">
<? show_select_months($patho_from_month, true); ?>
</select>/<select name="patho_from_day">
<? show_select_days($patho_from_day, true); ?>
</select> 〜
<select name="patho_to_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $patho_to_year, false); ?>
</select>/<select name="patho_to_month">
<? show_select_months($patho_to_month, true); ?>
</select>/<select name="patho_to_day">
<? show_select_days($patho_to_day, true); ?>
</select>
</font></td>
</tr>
<? } ?>
<? if ($display3 == "t" || $display23 == "t" || $display4 == "t" || $display20 == "t") { ?>
<tr height="22">
<? if ($display3 == "t" || $display23 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院目的</font></td>
<td<? if ($display4 != "t" && $display20 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($display3 == "t") { ?>
<select name="purpose_cd">
<option value=""<? if ($purpose_cd == "") {echo " selected";} ?>>
<? foreach ($purpose_master as $tmp_purpose_cd => $tmp_purpose_name) { ?>
<option value="<? echo($tmp_purpose_cd); ?>"<? if ($tmp_purpose_cd == $purpose_cd) {echo " selected";} ?>><? echo($tmp_purpose_name); ?>
<? } ?>
</select>
<? } ?>
<? if ($display23 == "t") { ?>
<nobr><input type="checkbox" name="short_stay" value="t"<? if ($short_stay == "t") {echo(" checked");} ?>>短期入所</nobr>
<? } ?>
<? if ($display3 == "t") { ?>
<? if ($display23 == "t" || $display4 == "t" || $display20 == "t") {echo("<br>");} ?>
その他：<input name="purpose_detail" value="<? echo($purpose_detail); ?>" type="text" size="20">
<? } ?>
</font></td>
<? } ?>
<? if ($display4 == "t" || $display20 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院前検査</font></td>
<td<? if ($display3 != "t" && $display23 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($display4 == "t") { ?>
採血：<select name="sample_blood">
<option value=""<? if ($sample_blood == "") {echo " selected";} ?>>
<option value="t"<? if ($sample_blood == "t") {echo " selected";} ?>>済
<option value="f"<? if ($sample_blood == "f") {echo " selected";} ?>>未済
</select>
<? } ?>
<? if ($display20 == "t") { ?>
<nobr><input type="checkbox" name="outpt_test" value="t"<? if ($outpt_test == "t") {echo(" checked");} ?>>外来検査</nobr>
<? } ?>
<? if ($display4 == "t") { ?>
<? if ($display20 == "t" || $display3 == "t" || $display23 == "t") {echo("<br>");} ?>
その他：<input type="text" size="20" name="diagnosis" value="<? echo($diagnosis); ?>">
<? } ?>
</font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display5 == "t" || $display6 == "t") { ?>
<tr height="22">
<? if ($display5 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">緊急度</font></td>
<td<? if ($display6 != "t") {echo(' colspan="3"');} ?>><select name="emergency">
<option value=""<? if ($emergency == "") {echo " selected";} ?>>
<option value="1"<? if ($emergency == "1") {echo " selected";} ?>>即時入院
<option value="2"<? if ($emergency == "2") {echo " selected";} ?>>至急
<option value="3"<? if ($emergency == "3") {echo " selected";} ?>>なるべく早く
<option value="4"<? if ($emergency == "4") {echo " selected";} ?>>予約順
<option value="5"<? if ($emergency == "5") {echo " selected";} ?>>時期指定
<option value="6"<? if ($emergency == "6") {echo " selected";} ?>>入院日時決定済み
<option value="7"<? if ($emergency == "7") {echo " selected";} ?>>その他
</select></td>
<? } ?>
<? if ($display6 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定期間</font></td>
<td<? if ($display5 != "t") {echo(' colspan="3"');} ?>><select name="period">
<option value=""<? if ($period == "") {echo " selected";} ?>>
<option value="A"<? if ($period == "A") {echo " selected";} ?>>日帰り
<option value="B"<? if ($period == "B") {echo " selected";} ?>>1日
<option value="C"<? if ($period == "C") {echo " selected";} ?>>2日
<option value="D"<? if ($period == "D") {echo " selected";} ?>>3日
<option value="1"<? if ($period == "1") {echo " selected";} ?>>短期
<option value="2"<? if ($period == "2") {echo " selected";} ?>>7日以内
<option value="3"<? if ($period == "3") {echo " selected";} ?>>14日以内
<option value="E"<? if ($period == "E") {echo " selected";} ?>>19日以内
<option value="4"<? if ($period == "4") {echo " selected";} ?>>20日以上
<option value="5"<? if ($period == "5") {echo " selected";} ?>>その他
</select></td>
<? } ?>
</tr>
<? } ?>
<? if ($display7 == "t" || $display8 == "t") { ?>
<tr height="22">
<? if ($display7 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院方法</font></td>
<td<? if ($display8 != "t") {echo(' colspan="3"');} ?>><select name="way">
<option value=""<? if ($way == "") {echo " selected";} ?>>
<option value="1"<? if ($way == "1") {echo " selected";} ?>>独歩
<option value="2"<? if ($way == "2") {echo " selected";} ?>>車椅子
<option value="3"<? if ($way == "3") {echo " selected";} ?>>ストレッチャー
<option value="4"<? if ($way == "4") {echo " selected";} ?>>抱っこ
</select></td>
<? } ?>
<? if ($display8 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護度</font></td>
<td<? if ($display7 != "t") {echo(' colspan="3"');} ?>><select name="nursing">
<option value=""<? if ($nursing == "") {echo " selected";} ?>>
<option value="1"<? if ($nursing == "1") {echo " selected";} ?>>常時観察を必要とする
<option value="2"<? if ($nursing == "2") {echo " selected";} ?>>継続的に観察を必要とする
<option value="3"<? if ($nursing == "3") {echo " selected";} ?>>継続的観察は特に必要としない
</select></td>
<? } ?>
</tr>
<? } ?>
<? if ($display9 == "t" || $display10 == "t") { ?>
<tr height="22">
<? if ($display9 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主食</font></td>
<td<? if ($display10 != "t") {echo(' colspan="3"');} ?>><select name="staple_fd">
<option value=""<? if ($staple_fd == "") {echo " selected";} ?>>
<option value="1"<? if ($staple_fd == "1") {echo " selected";} ?>>米
<option value="2"<? if ($staple_fd == "2") {echo " selected";} ?>>軟
<option value="3"<? if ($staple_fd == "3") {echo " selected";} ?>>全粥
<option value="4"<? if ($staple_fd == "4") {echo " selected";} ?>>7分粥
<option value="5"<? if ($staple_fd == "5") {echo " selected";} ?>>5分粥
<option value="6"<? if ($staple_fd == "6") {echo " selected";} ?>>3分粥
<option value="7"<? if ($staple_fd == "7") {echo " selected";} ?>>絶食
</select></td>
<? } ?>
<? if ($display10 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事内容</font></td>
<td<? if ($display9 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="fd_type">
<option value=""<? if ($fd_type == "") {echo " selected";} ?>>
<option value="1"<? if ($fd_type == "1") {echo " selected";} ?>>常食
<option value="2"<? if ($fd_type == "2") {echo " selected";} ?>>特別食
</select><? if ($display9 == "t") {echo("<br>");} else {echo("&nbsp;");} ?>
詳細：<input type="text" name="fd_dtl" value="<? echo($fd_dtl); ?>" size="20">
</font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display11 == "t" || $display12 == "t") { ?>
<tr height="22">
<? if ($display11 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">希望病室</font></td>
<td<? if ($display12 != "t") {echo(' colspan="3"');} ?>>
<select name="wish_rm_cd">
<option value=""<? if ($wish_rm_cd == "") {echo " selected";} ?>>
<? foreach ($wish_rm_master as $tmp_wish_rm_cd => $tmp_wish_rm_name) { ?>
<option value="<? echo($tmp_wish_rm_cd); ?>"<? if ($tmp_wish_rm_cd == $wish_rm_cd) {echo " selected";} ?>><? echo($tmp_wish_rm_name); ?>
<? } ?>
</select>
</td>
<? } ?>
<? if ($display12 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事開始帯</font></td>
<td<? if ($display11 != "t") {echo(' colspan="3"');} ?>>
<select name="fd_start">
<option value=""<? if ($fd_start == "") {echo " selected";} ?>>
<option value="1"<? if ($fd_start == "1") {echo " selected";} ?>>朝
<option value="2"<? if ($fd_start == "2") {echo " selected";} ?>>昼
<option value="3"<? if ($fd_start == "3") {echo " selected";} ?>>夕
</select>
</td>
<? } ?>
</tr>
<? } ?>
<? if ($display13 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険</font></td>
<td colspan="3">
<select name="insurance">
<option value=""<? if ($insurance == "") {echo " selected";} ?>>
<option value="1"<? if ($insurance == "1") {echo " selected";} ?>>一般
<option value="9"<? if ($insurance == "9") {echo " selected";} ?>>後期高齢
<option value="2"<? if ($insurance == "2") {echo " selected";} ?>>老人
<option value="3"<? if ($insurance == "3") {echo " selected";} ?>>労災
<option value="4"<? if ($insurance == "4") {echo " selected";} ?>>自賠責
<option value="5"<? if ($insurance == "5") {echo " selected";} ?>>介護
<option value="6"<? if ($insurance == "6") {echo " selected";} ?>>生保
<option value="7"<? if ($insurance == "7") {echo " selected";} ?>>自費
<option value="8"<? if ($insurance == "8") {echo " selected";} ?>>公傷
</select>
</td>
</tr>
<? } ?>
<? if ($display24 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療保険負担割合</font></td>
<td colspan="3">
<select name="insu_rate_cd">
<option value=""<? if ($insu_rate_cd == "") {echo " selected";} ?>>
<? foreach ($insu_rate_master as $tmp_insu_rate_cd => $tmp_insu_rate_name) { ?>
<option value="<? echo($tmp_insu_rate_cd); ?>"<? if ($tmp_insu_rate_cd == $insu_rate_cd) {echo " selected";} ?>><? echo($tmp_insu_rate_name); ?>
<? } ?>
</select>
</td>
</tr>
<? } ?>
<? if ($display25 == "t" || $display26 == "t") { ?>
<tr height="22">
<? if ($display25 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">減額認定有り</font></td>
<td<? if ($display26 != "t") {echo(' colspan="3"');} ?>><input type="checkbox" name="reduced" value="t"<? if ($reduced == "t") {echo(" checked");} ?>></td>
<? } ?>
<? if ($display26 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マル障有り</font></td>
<td<? if ($display25 != "t") {echo(' colspan="3"');} ?>><input type="checkbox" name="impaired" value="t"<? if ($impaired == "t") {echo(" checked");} ?>></td>
<? } ?>
</tr>
<? } ?>
<? if ($display46 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">差額室料免除（減免）</font></td>
<td colspan="3"><input type="checkbox" name="exemption" value="t"<? if ($exemption == "t") {echo(" checked");} ?>></td>
</tr>
<? } ?>
<? if ($display27 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険者名称</font></td>
<td colspan="3"><input type="text" name="insured" value="<? echo($insured); ?>" size="30" maxlength="30" style="ime-mode:active;"></td>
</tr>
<? } ?>
<? if ($display18 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">個人情報に関する要望</font></td>
<td colspan="3">
<select name="privacy_flg" style="vertical-align:middle;">
<option value=""<? if ($privacy_flg == "") {echo " selected";} ?>>
<option value="1"<? if ($privacy_flg == "1") {echo " selected";} ?>>要望有り
<option value="2"<? if ($privacy_flg == "2") {echo " selected";} ?>>要望無し
</select>
<textarea cols="40" rows="4" name="privacy_text" style="vertical-align:middle;"><? echo($privacy_text); ?></textarea></td>
</tr>
<? } ?>
<? if ($display28 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前入院先区分</font></td>
<td colspan="3"><select name="pre_div">
<option value="">
<option value="1"<? if ($pre_div == "1") {echo(" selected");} ?>>自宅
<option value="2"<? if ($pre_div == "2") {echo(" selected");} ?>>医療機関他
</select></td>
</tr>
<? } ?>
<? if ($display14 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手術予定日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="ope_yr"><? show_update_years($ope_yr, 1, true); ?></select>/<select name="ope_mon"><? show_select_months($ope_mon, true); ?></select>/<select name="ope_day"><? show_select_days($ope_day ,true); ?></select><br>
<select name="ope_u_yr"><? show_update_years($ope_u_yr, 1, true); ?></select>/<select name="ope_u_mon"><? show_select_months($ope_u_mon, true); ?></select>/<select name="ope_u_td"><option value="-"<? if ($ope_u_td == "-") {echo " selected";} ?>><option value="1"<? if ($ope_u_td == "1") {echo " selected";} ?>>上旬<option value="2"<? if ($ope_u_td == "2") {echo " selected";} ?>>中旬<option value="3"<? if ($ope_u_td == "3") {echo " selected";} ?>>下旬<option value="9"<? if ($ope_u_td == "9") {echo " selected";} ?>>その他</select>&nbsp;手術日未定：<input type="checkbox" name="opedtundec" value="t"<? if ($opedtundec == "t") {echo(" checked");} ?> onclick="opedtundecOnClick();"></font></td>
</tr>
<? } ?>
<? if ($display15 == "t" || $display16 == "t") { ?>
<tr height="22">
<? if ($display15 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">観察</font></td>
<td<? if ($display16 != "t") {echo(' colspan="3"');} ?>>
<select name="observe">
<option value=""<? if ($observe == "") {echo(" selected");} ?>>
<option value="1"<? if ($observe == "1") {echo(" selected");} ?>>Ａ
<option value="2"<? if ($observe == "2") {echo(" selected");} ?>>Ｂ
<option value="3"<? if ($observe == "3") {echo(" selected");} ?>>Ｃ
<option value="4"<? if ($observe == "4") {echo(" selected");} ?>>Ｄ
<option value="5"<? if ($observe == "5") {echo(" selected");} ?>>Ｅ
</select>
</td>
<? } ?>
<? if ($display16 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">面会</font></td>
<td<? if ($display15 != "t") {echo(' colspan="3"');} ?>>
<select name="meet">
<option value=""<? if ($meet === "") {echo(" selected");} ?>>
<option value="0"<? if ($meet === "0") {echo(" selected");} ?>>可能
<option value="1"<? if ($meet === "1") {echo(" selected");} ?>>家族のみ
<option value="2"<? if ($meet === "2") {echo(" selected");} ?>>面会謝絶
</select>
</td>
<? } ?>
</tr>
<? } ?>
<? if ($display45 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護度分類</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
看護観察の程度：<select name="nrs_obsv">
<option value="">
<option value="A"<? if ($nrs_obsv == "A") {echo(" selected");} ?>>A
<option value="B"<? if ($nrs_obsv == "B") {echo(" selected");} ?>>B
<option value="C"<? if ($nrs_obsv == "C") {echo(" selected");} ?>>C
</select>
<span style="margin-left:10px;">生活の自由度：<select name="free">
<option value="">
<option value="1"<? if ($free == "1") {echo(" selected");} ?>>��
<option value="2"<? if ($free == "2") {echo(" selected");} ?>>��
<option value="3"<? if ($free == "3") {echo(" selected");} ?>>��
<option value="4"<? if ($free == "4") {echo(" selected");} ?>>��
</select></span>
</font></td>
</tr>
<? } ?>
<? if ($display17 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特記事項</font></td>
<td colspan="3"><textarea name="special" cols="40" rows="8" wrap="virtual"><? echo($special); ?></textarea></td>
</tr>
<? } ?>
</table>

<? if ($display29 == "t" || $display30 == "t" || $display31 == "t" || $display32 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜紹介情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list2">
<? if ($display29 == "t" || $display30 == "t") { ?>
<tr height="22">
<? if ($display29 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">紹介元医療機関</font></td>
<td<? if ($display30 != "t") {echo(' colspan="3"');} ?>>
<input type="text" name="intro_inst_name" value="<? echo($intro_inst_name); ?>" size="25" disabled> <input type="button" value="検索" onclick="openInstSearchWindow();"><? if ($display30 == "t") {echo("<br>");} else {echo(" ");} ?><input type="button" value="クリア" onclick="clearInst();">
</td>
<? } ?>
<? if ($display30 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td<? if ($display29 != "t") {echo(' colspan="3"');} ?>>
<select name="intro_sect_cd" onchange="setIntroDoctorOptions();">
<option value="">　　　　　</option>
</select>
</td>
<? } ?>
</tr>
<? } ?>
<? if ($display31 == "t" || $display32 == "t") { ?>
<tr height="22">
<? if ($display31 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前医</font></td>
<td<? if ($display32 != "t") {echo(' colspan="3"');} ?>>
<select name="intro_doctor_no">
<option value="">　　　　　</option>
</select>
</td>
<? } ?>
<? if ($display32 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ホットライン</font></td>
<td<? if ($display31 != "t") {echo(' colspan="3"');} ?>><input type="checkbox" name="hotline" value="t"<? if ($hotline == "t") {echo(" checked");} ?>></td>
<? } ?>
</tr>
<? } ?>
</table>
<? } ?>

<? if ($display33 == "t" || $display34 == "t" || $display35 == "t" || $display36 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜リハビリ情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list3">
<? if ($display33 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定期限分類</font></td>
<td colspan="3">
<select name="dcb_cls" onchange="setDcbExp();">
<option value="">
<option value="1"<? if ($dcb_cls == "1") {echo(" selected");} ?>>脳血管疾患等
<option value="2"<? if ($dcb_cls == "2") {echo(" selected");} ?>>高次脳機能障害等
<option value="3"<? if ($dcb_cls == "3") {echo(" selected");} ?>>大腿骨等
<option value="4"<? if ($dcb_cls == "4") {echo(" selected");} ?>>外科手術又は肺炎等
<option value="5"<? if ($dcb_cls == "5") {echo(" selected");} ?>>大腿骨の神経等
<option value="6"<? if ($dcb_cls == "6") {echo(" selected");} ?>>股関節、膝関節の置換
</select>
</td>
</tr>
<? } ?>
<? if ($display34 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定基準日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="dcb_bas_year" onchange="setDcbExp();">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $dcb_bas_year, false); ?>
</select>/<select name="dcb_bas_month" onchange="setDcbExp();">
<? show_select_months($dcb_bas_month, true); ?>
</select>/<select name="dcb_bas_day" onchange="setDcbExp();">
<? show_select_days($dcb_bas_day ,true); ?>
</select>
</font></td>
</tr>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定期限日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="dcb_exp_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 6, $dcb_exp_year, false); ?>
</select>/<select name="dcb_exp_month">
<? show_select_months($dcb_exp_month, true); ?>
</select>/<select name="dcb_exp_day">
<? show_select_days($dcb_exp_day ,true); ?>
</select>
</font></td>
</tr>
<? } ?>
<? if ($display35 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定期限分類</font></td>
<td colspan="3">
<select name="rhb_cls" onchange="setRhbExp();">
<option value="">
<option value="1"<? if ($rhb_cls == "1") {echo(" selected");} ?>>心大血管疾患リハビリテーション
<option value="2"<? if ($rhb_cls == "2") {echo(" selected");} ?>>脳血管疾患等リハビリテーション
<option value="3"<? if ($rhb_cls == "3") {echo(" selected");} ?>>運動器リハビリテーション
<option value="4"<? if ($rhb_cls == "4") {echo(" selected");} ?>>呼吸器リハビリテーション
</select>
</td>
</tr>
<? } ?>
<? if ($display36 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定基準日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="rhb_bas_year" onchange="setRhbExp();">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $rhb_bas_year, false); ?>
</select>/<select name="rhb_bas_month" onchange="setRhbExp();">
<? show_select_months($rhb_bas_month, true); ?>
</select>/<select name="rhb_bas_day" onchange="setRhbExp();">
<? show_select_days($rhb_bas_day ,true); ?>
</select>
</font></td>
</tr>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定期限日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="rhb_exp_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 6, $rhb_exp_year, false); ?>
</select>/<select name="rhb_exp_month">
<? show_select_months($rhb_exp_month, true); ?>
</select>/<select name="rhb_exp_day">
<? show_select_days($rhb_exp_day ,true); ?>
</select>
</font></td>
</tr>
<? } ?>
</table>
<? } ?>

<? if ($display37 == "t" || $display38 == "t" || $display39 == "t" || $display40 == "t" || $display41 == "t" || $display42 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜社会資源情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list3">
<? if ($display37 == "t" || $display38 == "t") { ?>
<tr height="22">
<? if ($display37 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">介護保険被保険者番号</font></td>
<td<? if ($display38 != "t") {echo(' colspan="3"');} ?>>
<input type="text" name="care_no" value="<? echo($care_no); ?>" size="15" maxlength="15">
</td>
<? } ?>
<? if ($display38 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">要介護度</font></td>
<td<? if ($display37 != "t") {echo(' colspan="3"');} ?>>
<select name="care_grd_cd">
<option value=""<? if ($care_grd_cd == "") {echo " selected";} ?>>
<? foreach ($care_grd_master as $tmp_care_grd_cd => $tmp_care_grd_name) { ?>
<option value="<? echo($tmp_care_grd_cd); ?>"<? if ($tmp_care_grd_cd == $care_grd_cd) {echo " selected";} ?>><? echo($tmp_care_grd_name); ?>
<? } ?>
</select>
</td>
<? } ?>
</tr>
<? } ?>
<? if ($display39 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">要介護度認定年月日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="care_apv_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $care_apv_year, false); ?>
</select>/<select name="care_apv_month">
<? show_select_months($care_apv_month, true); ?>
</select>/<select name="care_apv_day">
<? show_select_days($care_apv_day ,true); ?>
</select>
</font></td>
</tr>
<? } ?>
<? if ($display40 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">要介護度の有効期間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="care_from_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $care_from_year, false); ?>
</select>/<select name="care_from_month">
<? show_select_months($care_from_month, true); ?>
</select>/<select name="care_from_day">
<? show_select_days($care_from_day, true); ?>
</select> 〜
<select name="care_to_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 5, $care_to_year, false); ?>
</select>/<select name="care_to_month">
<? show_select_months($care_to_month, true); ?>
</select>/<select name="care_to_day">
<? show_select_days($care_to_day, true); ?>
</select>
</font></td>
</tr>
<? } ?>
<? if ($display41 == "t" || $display42 == "t") { ?>
<tr height="22">
<? if ($display41 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">身体障害者手帳等級</font></td>
<td<? if ($display42 != "t") {echo(' colspan="3"');} ?>>
<select name="dis_grd_cd">
<option value=""<? if ($dis_grd_cd == "") {echo " selected";} ?>>
<? foreach ($dis_grd_master as $tmp_dis_grd_cd => $tmp_dis_grd_name) { ?>
<option value="<? echo($tmp_dis_grd_cd); ?>"<? if ($tmp_dis_grd_cd == $dis_grd_cd) {echo " selected";} ?>><? echo($tmp_dis_grd_name); ?>
<? } ?>
</select>
</td>
<? } ?>
<? if ($display42 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">障害種別</font></td>
<td<? if ($display41 != "t") {echo(' colspan="3"');} ?>>
<select name="dis_type_cd">
<option value=""<? if ($dis_type_cd == "") {echo " selected";} ?>>
<? foreach ($dis_type_master as $tmp_dis_type_cd => $tmp_dis_type_name) { ?>
<option value="<? echo($tmp_dis_type_cd); ?>"<? if ($tmp_dis_type_cd == $dis_type_cd) {echo " selected";} ?>><? echo(mb_strimwidth($tmp_dis_type_name, 0, 24, "...")); ?>
<? } ?>
</select>
</td>
<? } ?>
</tr>
<? } ?>
</table>
<? } ?>

<? if ($display43 == "t" || $display44 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜特定疾患情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list4">
<? if ($display43 == "t") { ?>
<tr height="22">
<td bgcolor="#ffe4e1" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特定疾患の疾患名</font></td>
<td colspan="3"><input type="text" name="spec_name" value="<? echo($spec_name); ?>" size="60" maxlength="200" style="ime-mode:active;"></td>
</tr>
<? } ?>
<? if ($display44 == "t") { ?>
<tr height="22">
<td bgcolor="#ffe4e1" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特定疾患の有効期間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="spec_from_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $spec_from_year, false); ?>
</select>/<select name="spec_from_month">
<? show_select_months($spec_from_month, true); ?>
</select>/<select name="spec_from_day">
<? show_select_days($spec_from_day, true); ?>
</select> 〜
<select name="spec_to_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 5, $spec_to_year, false); ?>
</select>/<select name="spec_to_month">
<? show_select_months($spec_to_month, true); ?>
</select>/<select name="spec_to_day">
<? show_select_days($spec_to_day, true); ?>
</select>
</font></td>
</tr>
<? } ?>
</table>
<? } ?>

<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="変更"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="purpose_rireki" value="<? echo($purpose_rireki); ?>">
<input type="hidden" name="wish_rm_rireki" value="<? echo($wish_rm_rireki); ?>">
<input type="hidden" name="insu_rate_rireki" value="<? echo($insu_rate_rireki); ?>">
<input type="hidden" name="care_grd_rireki" value="<? echo($care_grd_rireki); ?>">
<input type="hidden" name="dis_grd_rireki" value="<? echo($dis_grd_rireki); ?>">
<input type="hidden" name="dis_type_rireki" value="<? echo($dis_type_rireki); ?>">
<input type="hidden" name="intro_inst_cd" value="<? echo($intro_inst_cd); ?>">
<input type="hidden" name="intro_sect_rireki" value="<? echo($intro_sect_rireki); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="pt_nm" value="<? echo($pt_nm); ?>">
<input type="hidden" name="search_sect" value="<? echo($search_sect); ?>">
<input type="hidden" name="search_doc" value="<? echo($search_doc); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function get_sex_string($sex) {
	switch ($sex) {
	case "1":
		return "男";
	case "2":
		return "女";
	default:
		return "不明";
	}
}
?>
