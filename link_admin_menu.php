<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix リンクライブラリ | カテゴリー表示設定</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");

require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 61, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// リンクライブラリ管理権限の取得
$link_admin_auth = check_authority($session, 61, $fname);

define("DEFAULT_ROW_COUNT", 4);

// カテゴリー一覧配列を初期化
$categories = array();

// 初期表示時
if ($mode == "") {

	// カテゴリー一覧を取得
	$con = connect2db($fname);
	// アクセスログ
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);



	$sql = "select * from linkcate";
	$cond = "order by linkcate_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	pg_close($con);

	// 取得した情報を配列に格納
	$categories = pg_fetch_all($sel);

	// 最低表示行数に満たない場合は行を追加
	while (count($categories) < DEFAULT_ROW_COUNT) {
		$categories[] = add_row("", "", "t", "", "t", "", "t", "");
	}

// 画面再表示時
} else {
	for ($i = 0, $j = count($linkcate_nm); $i < $j; $i++) {
		if ($mode == "DELETE_ROW" && $i == $del_id) {
			continue;
		}

		$categories[] = add_row(
			$linkcate_id[$i],
			$linkcate_nm[$i],
			$show_flg1[$i],
			$order1[$i],
			$show_flg2[$i],
			$order2[$i],
			$show_flg3[$i],
			$order3[$i]
		);
	}

	// 「行を追加」ボタン押下時
	if ($mode == "ADD_ROW") {
		$categories[] = add_row("", "", "t", "", "t", "", "t", "");
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function addRow() {
	clearDisabled();
	document.mainform.action = 'link_admin_menu.php';
	document.mainform.mode.value = 'ADD_ROW';
	document.mainform.submit();
}

function deleteRow(del_id) {
	clearDisabled();
	document.mainform.action = 'link_admin_menu.php';
	document.mainform.mode.value = 'DELETE_ROW';
	document.mainform.del_id.value = del_id;
	document.mainform.submit();
}

function updateCategories() {
	clearDisabled();
	document.mainform.action = 'link_admin_category_update_exe.php';
	return true;
}

function setOrderDisabled(row_id, page_id, disabled) {
	document.mainform.elements['order'.concat(page_id).concat('[]')][row_id].disabled = disabled;
}

function clearDisabled() {
	var inputs = document.getElementsByTagName('input');
	for (var i = 0, j = inputs.length; i < j; i++) {
		if (inputs[i].disabled) {
			inputs[i].disabled = false;
			inputs[i].value = '';
		}
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="link_menu.php?session=<? echo($session); ?>"><img src="img/icon/b40.gif" width="32" height="32" border="0" alt="リンクライブラリ"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="link_menu.php?session=<? echo($session); ?>"><b>リンクライブラリ</b></a> &gt; <a href="link_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="link_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="140" align="center" bgcolor="#5279a5"><a href="link_admin_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>カテゴリー表示設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="link_admin_link_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンク名表示設定</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" method="post" onsubmit="return updateCategories();">
<table width="740" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※編集内容は更新ボタンを押すまで確定されません</font></td>
<td align="right"><input type="button" value="行を追加" onclick="addRow();"></td>
</tr>
</table>
<table width="740" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" align="center">
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリー名</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインページ</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページ</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクライブラリ</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
</tr>
<tr height="22" bgcolor="#f6f9ff" align="center">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">順番</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">順番</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">順番</font></td>
</tr>
<? write_rows($categories); ?>
</table>
<table width="740" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="del_id" value="">
</form>
</td>
</tr>
</table>
</body>
</html>
<?
function add_row($linkcate_id, $linkcate_nm, $show_flg1, $order1, $show_flg2, $order2, $show_flg3, $order3) {
	return array(
		"linkcate_id" => $linkcate_id,
		"linkcate_nm" => $linkcate_nm,
		"show_flg1" => $show_flg1,
		"order1" => $order1,
		"show_flg2" => $show_flg2,
		"order2" => $order2,
		"show_flg3" => $show_flg3,
		"order3" => $order3
	);
}

function write_rows($categories) {
	foreach ($categories as $i => $category) {
		echo("<tr height=\"22\">\n");
		echo("<td><input type=\"text\" name=\"linkcate_nm[]\" value=\"{$category["linkcate_nm"]}\" size=\"35\" style=\"ime-mode:active;\"><input type=\"hidden\" name=\"linkcate_id[]\" value=\"{$category["linkcate_id"]}\"></td>");
		for ($page_id = 1; $page_id <= 3; $page_id++) {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
			echo("<input type=\"radio\" name=\"show_flg{$page_id}[{$i}]\" value=\"t\"");
			if ($category["show_flg{$page_id}"] == "t") {
				echo(" checked");
			}
			echo(" onclick=\"setOrderDisabled({$i}, {$page_id}, false);\">表示\n");
			echo("<input type=\"radio\" name=\"show_flg{$page_id}[{$i}]\" value=\"f\"");
			if ($category["show_flg{$page_id}"] == "f") {
				echo(" checked");
			}
			echo(" onclick=\"setOrderDisabled({$i}, {$page_id}, true);\">非表示\n");
			echo("</font></td>");
			echo("<td align=\"center\"><input type=\"text\" name=\"order{$page_id}[]\" value=\"{$category["order{$page_id}"]}\" size=\"3\" style=\"ime-mode:inactive;\"");
			if ($category["show_flg{$page_id}"] == "f") {
				echo(" disabled");
			}
			echo("></td>");
		}
		echo("<td align=\"center\"><input type=\"button\" value=\"削除\" onclick=\"deleteRow({$i});\"></td>");
		echo("</tr>\n");
	}
}
?>
