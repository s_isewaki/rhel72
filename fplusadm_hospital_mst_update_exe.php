<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require_once("about_comedix.php");
require_once("about_validation.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,79,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

//選択されていない場合はNULL
if($class_id==""){
	$class_id = NULL;
}
if($attribute_id==""){
	$attribute_id = NULL;
}
if($dept_id==""){
	$dept_id = NULL;
}
if($room_id==""){
	$room_id = NULL;
}

//マスタ情報を更新する
$sql = "update fplus_hospital_master set";
$set = array("hospital_name", "hospital_directer", "class_id", "attribute_id", "dept_id", "room_id", "update_time", "del_flg");
$setvalue = array("$hospital_name", "$hospital_directer", $class_id, $attribute_id, $dept_id, $room_id, date("Y-m-d H:i:s"), "f");
$cond = "where hospital_id = '$hospital_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'fplusadm_hospital_mst.php?session=$session&hospital_id=$hospital_id';</script>");

?>