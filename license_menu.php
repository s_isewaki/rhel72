<?
require_once("class/Cmx/Core2/C2FW.php");
require_once("Cmx.php");
require_once("aclg_set.php");

require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");
require_once("show_select_values.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// データベースへ接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);


//**************************************************************************************************
// セッションチェック
//**************************************************************************************************
if (!$GEN_LICENSE) {
	// セッションのチェック
	$session = qualify_session($session, $fname);
	if ($session == "0") {
	    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	    exit;
	}

	// ライセンス管理権限のチェック
	$checkauth = check_authority($session, 38, $fname);
	if ($checkauth == "0") {
	    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	    exit;
	}
	// アクセスログ
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);
}
else {
	$profile_type = "1";
}


//**************************************************************************************************
// 更新ボタン押下
//**************************************************************************************************
$errmsg = "";
if ($_REQUEST["try_update"]) {
	$c2app = c2GetC2App();
	$errmsg = $c2app->updateLicenseCounts($_REQUEST);
	// 成功時。画面遷移
	if ($errmsg=="ok") {
		header("Location: license_menu.php?session=".$session);
		die;
	}
}


//**************************************************************************************************
// 生成
//**************************************************************************************************
$gen_license_key = "";
if ($_REQUEST["try_generate"]) {
	require("admin_password.php");
	if ($_REQUEST["admin_pwd"] != ADMIN_PASSWORD) {
		$errmsg = '管理者パスワードが違います。';
	} else {
		$c2app = c2GetC2App();
		$obj = $c2app->genLicenseKey($_REQUEST);
		$gen_license_key = $obj["gen_license_key"];
		$errmsg = $obj["errmsg"];
	}
}








//**************************************************************************************************
// 画面表示値
//**************************************************************************************************
// 登録値の取得
$sql = "select * from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$arr_func = array();

$today_year = date("Y");
$today_month = date("m");
if ($_POST["try_generate"]) {
	$today_year = $_POST["today_year"];
	$today_month = $_POST["today_month"];
}

$licenseRow = $c2app->getLicenseRow();
$dbLicenseRow = $c2app->getLicenseRow();
if ($GEN_LICENSE) {
	$dbLicenseRow = array();
}

$licenseRow["expire_year"] = substr($licenseRow["lcs_expire_date"], 0, 4);
$licenseRow["expire_month"] = substr($licenseRow["lcs_expire_date"], 4, 2);
$licenseRow["expire_day"] = substr($licenseRow["lcs_expire_date"], 6, 2);


if ($GEN_LICENSE || $_POST["try_update"]) {
	$licenseRow = $_POST;
}



$lcs_max_user = $licenseRow["lcs_max_user"]; // 登録可能ユーザ数
$db_lcs_max_user = $dbLicenseRow["lcs_max_user"]; // 登録可能ユーザ数(DB値)

$erasedAppIds = $c2app->getErasedAppIds();
// 関係ないアプリのエントリ
// Core2が導入されるまでは、以下固定で
$erasedAppIds = array(
	           "ccusr2",  "ccusr3",  "ccusr4",  "ccusr5",
	"ccusr6",  "ccusr7",  "ccusr8",  "ccusr9",  "ccusr10",
	"ccusr11", "ccusr12", "ccusr13", "ccusr14", "ccusr15",

	"ccadm1",  "ccadm2",  "ccadm3",  "ccadm4",  "ccadm5",
	"ccadm6",  "ccadm7",  "ccadm8",  "ccadm9",  "ccadm10",
	"ccadm11", "ccadm12", "ccadm13", "ccadm14", "ccadm15",

	"omusr1", "omusr2", "omusr3", "omusr4", "omusr5",
	"omadm1", "omadm2", "omadm3", "omadm4", "omadm5"
);


$applications = $c2app->getApplications();

$purchasable = array(); // 購入可能（現在ライセンス無し）
$purchased = array(); // 購入済み（現在ライセンス有り）
$licenseIdToAppId = $c2app->getLicenseIdToAppId();
$purchasable_count = 0;
$purchased_count = 0;
foreach ($licenseIdToAppId as $lcs_func_no => $appid) {
    if (in_array($appid, $erasedAppIds, true)) continue; // 利用対象外。スルー
    $info = $c2app->getApplicationInfo($appid);
    if (!$info) continue; // 未登録カスタムアプリなどはapplicationsからエントリが削除済。スルー
    $jp = $info["jp"];
    $cmb_id = $info["lcs_max"];
    if (c2bool($dbLicenseRow["lcs_func".$lcs_func_no])) {
		$col = $purchased_count%3;
		$purchased_count++;
        //$purchased[$col] .= '<div><nobr><input type="hidden" name="lcs_func'.$lcs_func_no.'" value="1" />'.$jp.'</nobr></div>';
        $purchased[$col] .= '<div><nobr>'.$jp.'</nobr></div>';
    } else {
		$col = $purchasable_count%3;
		$purchasable_count++;
        $purchasable[$col] .=
        '<div><nobr><label>'.
        '<input type="checkbox" name="lcs_func'.$lcs_func_no.'" value="1" onclick="changeDisp()"'.
        ' cmb_id="'.$cmb_id.'"'.(c2Bool($licenseRow["lcs_func".$lcs_func_no])?" checked":"").'>'.
        $jp."</label></nobr></div>";
    }
}

if (!count($purchased))   $purchased[0]= "-";
if (!count($purchasable)) $purchasable[0]= "-";




function write_user_options($posted, $def, $kouho, $lcs_max_user, $unit="人") {

	global $GEN_LICENSE;
    foreach ($kouho as $num) {
		if ($num=="" && $GEN_LICENSE) continue;
		$disp = ($num=="" ? "" : $num.$unit);
		if ($num=="FULL") $disp = "無制限";

		// 原則的に現状、下方修正できないようになっている。
		if (!$GEN_LICENSE) {
			if ($posted=="FULL" && $num!="FULL") continue; // 現在FULLなら、FULL以外は無視する
			else if ($num!="FULL") { // 現在FULLで無い場合は、ループが数値の場合に、
		        if (intval($def) > intval($num)) continue; // その数値がデフォルトより小さいなら無視。
			}
		}
		$selected = ($posted==$num ? " selected": "");
        echo '<option value="'.$num.'"'.$selected.'>'.$disp.'</option>';
    }
}





?>
<? if ($GEN_LICENSE) { ?>
<!DOCTYPE html>
<html lang="ja"><head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1,requiresActiveX=true" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html;charset=EUC-JP" />
<title>CoMedixライセンス管理機能｜ライセンス証書番号生成</title>
<link rel="stylesheet" type="text/css" href="../css/c2/main.css">
<style type="text/css">
	#main_table { border-collapse:collapse; margin-top:5px; width:100% }
	#main_table th { text-align:right; font-weight:normal; padding:2px; vertical-align:middle; width:240px }
	#main_table td { text-align:left; padding:2px; vertical-align:middle }
	select { padding:2px }
	#main_table  .table_purchase td { border:0; padding:0 10px 0 0; line-height:20px; vertical-align:top }
</style>
<script type="text/javascript" src="../js/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../js/c2/common.js"></script>
</head>

<body style="margin:0; padding:0" onload="pageLoaded()">

<div style="width:800px; padding:5px">

<div style="height:20px; line-height:20px; text-align:left; padding:3px 6px; color:#ffffff; background-color:#5279a5">
	<b>ライセンス証書番号生成</b>
</div>




<? } else { ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ライセンス管理 | ライセンス管理</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
	body, table { font-size:13px }
	.list {border-collapse:collapse;}
	.list th, .list td {border:#5279a5 solid 1px;}
	#main_table { width:800px }
	#main_table th { height:22px; width:230px; text-align:right; font-weight:normal; background-color:#f6f9ff }
	#main_table td { height:22px; text-align:left }
	#main_table  .table_purchase td { border:0; padding:0 10px 0 0; line-height:20px; vertical-align:top }
</style>
<script type="text/javascript" src="js/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/c2/common.js"></script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="pageLoaded();">
<div>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#f6f9ff">
	<td width="32" height="32" class="spacing"><a href="license_menu.php?session=<? echo($session); ?>"
		><img src="img/icon/b29.gif" width="32" height="32" border="0" alt="ライセンス管理"></a></td>
	<td width="100%" style="font-size:16px">&nbsp;<a href="license_menu.php?session=<? echo($session); ?>"><b>ライセンス管理</b></a></td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="22">
	<td width="120" align="center" bgcolor="#5279a5"><a href="license_menu.php?session=<? echo($session); ?>"
		style="color:#ffffff"><b>ライセンス登録</b></a></td>
	<td width="5">&nbsp;</td>
	<td width="120" align="center" bgcolor="#bdd1e7"><a href="license_versionup.php?session=<? echo($session); ?>">バージョンアップ</a></td>
	<td>&nbsp;</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<? } ?>


<script type="text/javascript">
	function setExpireDisabled() {
		var disabled = document.mainform.lcs_expire_flg[0].checked;
		document.mainform.expire_year.disabled = disabled;
		document.mainform.expire_month.disabled = disabled;
		document.mainform.expire_day.disabled = disabled;
	}
	function changeDisp() {
	    $("input").each(function() {
	        if (this.type.toLowerCase()!="checkbox") return;
	        var cmb_id = this.getAttribute("cmb_id");
	        if (!cmb_id) return;
	        var tr = ee("tr_"+cmb_id);
	        if (tr) {
		        var is_hidable = tr.getAttribute("is_hidable");
		        if (is_hidable) {
					ee("tr_"+cmb_id).style.display = (this.checked ? "" : "none");
			        if (ee(cmb_id)) ee(cmb_id).disabled = !this.checked;
			    }
	        }
	    });
	}
	function pageLoaded() {
		setExpireDisabled();
		changeDisp();
		<? if ($errmsg) { ?>
		alert('<?=$errmsg?>');
		<? } ?>
	}


</script>

<form name="mainform" method="post">


<? // 更新年月(hidden) ================================================================================================ ?>
<? if (!$GEN_LICENSE) { ?>
	<input type="hidden" name="today_year" value="<?=$today_year?>" />
	<input type="hidden" name="today_month" value="<?=$today_month?>" />
<? } ?>


<table width="800" border="0" cellspacing="0" cellpadding="2" class="list" id="main_table">




<? // 更新年月 ================================================================================================ ?>
<? if ($GEN_LICENSE) { ?>
<tr>
	<th>更新年月</th>
	<td>
		<select name="today_year"><? show_select_years_future(2, $today_year); ?></select>
		年
		<select name="today_month"><? show_select_months($today_month); ?></select>
		月
	</td>
</tr>
<? } ?>



<? // 更新年月 ================================================================================================ ?>
<? if ($GEN_LICENSE) { ?>
<tr>
	<th>医療機関コード／契約番号</th>
	<td><input class="text" type="text" name="prf_org_cd" size="14" maxlength="10" style="ime-mode:disabled;"
		autocomplete="off" value="<?=$licenseRow["prf_org_cd"]?>" /></td>
</tr>
<? } ?>






<? // 登録可能ユーザ数 ================================================================================================ ?>
<tr>
	<th>登録可能ユーザ数</th>
	<td>
		<select name="lcs_max_user">
		<?
			$kouho = array("5", "10", "50");
			for ($i = 100; $i <= 5000; $i += 100) $kouho[] = "$i";
			$kouho[] = "FULL";
			write_user_options($lcs_max_user, $db_lcs_max_user, $kouho, $db_lcs_max_user);
		?>
		</select>
	</td>
</tr>



<? // 基本機能利用可能ユーザ数 ================================================================================================ ?>
<tr>
	<th>基本機能利用可能ユーザ数</th>
	<td>
		<select name="lcs_basic_user">
		<?
			$kouho = array("", "0", "5", "10", "50");
			for ($i = 100; $i <= 5000; $i += 100) {
			    $kouho[] = "$i";
			}
			$kouho[] = "FULL";
			write_user_options($licenseRow["lcs_basic_user"], $dbLicenseRow["lcs_basic_user"], $kouho, $db_lcs_max_user);
		?>
		</select>
	</td>
</tr>



<? // イントラネット ================================================================================================ ?>
<tr id="tr_lcs_intra_user" <?=(in_array("intra", $erasedAppIds)?'style="display:none"':"")?>>
	<th><?=$c2app->getLcsLabel("lcs_intra_user")?>利用可能ユーザ数</th>
	<td><select name="lcs_intra_user" id="lcs_intra_user"<?=(in_array("intra", $erasedAppIds)?' disabled':"")?>>
	<?
	    $kouho = array("", "0", "50", "100", "200", "FULL");
	    write_user_options($licenseRow["lcs_intra_user"], $dbLicenseRow["lcs_intra_user"], $kouho, $db_lcs_max_user);
	?>
	</select></td>
</tr>




<? // 看護支援 ======================================================================================================= ?>
<tr id="tr_lcs_nursing_user" <?=(in_array("amb", $erasedAppIds)?'style="display:none"':"")?>>
	<th><?=$c2app->getLcsLabel("lcs_nursing_user")?>利用可能ユーザ数</th>
	<td><select name="lcs_nursing_user" id="lcs_nursing_user"<?=(in_array("amb", $erasedAppIds)?' disabled':"")?>>
	<?
	    $kouho = array("", "0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000");
		$kouho[] = "FULL";
	    write_user_options($licenseRow["lcs_nursing_user"], $dbLicenseRow["lcs_nursing_user"], $kouho, $db_lcs_max_user);
	?>
	</select></td>
</tr>



<? // 病床管理 ======================================================================================================= ?>
<tr id="tr_lcs_bed_count" <?=(in_array("ward", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_bed_count")?></th>
<td><select name="lcs_bed_count" id="lcs_bed_count"<?=(in_array("ward", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("", "0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "FULL");
    write_user_options($licenseRow["lcs_bed_count"], $dbLicenseRow["lcs_bed_count"], $kouho, $db_lcs_max_user, "床");
?>
</select></td>
</tr>



<? // ファントルくん ======================================================================================================= ?>
<tr id="tr_lcs_fantol_user" <?=(in_array("inci", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_fantol_user")?>利用可能ユーザ数</th>
<td><select name="lcs_fantol_user" id="lcs_fantol_user"<?=(in_array("inci", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("", "0", "5", "10", "50");
    for ($i = 100; $i <= 5000; $i += 100) $kouho[] = "$i";
    $kouho[] = "FULL";
    write_user_options($licenseRow["lcs_fantol_user"], $dbLicenseRow["lcs_fantol_user"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // ファントルくん＋ ======================================================================================================= ?>
<tr id="tr_lcs_fplus_user" <?=(in_array("fplus", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_fplus_user")?>利用可能ユーザ数</th>
<td><select name="lcs_fplus_user" id="lcs_fplus_user"<?=(in_array("fplus", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("", "0", "5", "10", "50", "100", "200", "300", "400", "500", "FULL");
    write_user_options($licenseRow["lcs_fplus_user"], $dbLicenseRow["lcs_fplus_user"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // バリテス ======================================================================================================= ?>
<tr id="tr_lcs_baritess_user" <?=(in_array("manabu", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_baritess_user")?>利用可能ユーザ数</th>
<td><select name="lcs_baritess_user" id="lcs_baritess_user"<?=(in_array("manabu", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("");
    for ($i = 0; $i <= 3000; $i += 100) $kouho[] = "$i";
    $kouho[] = "FULL";
    write_user_options($licenseRow["lcs_baritess_user"], $dbLicenseRow["lcs_baritess_user"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // メドレポート ======================================================================================================= ?>
<tr id="tr_lcs_report_user" <?=(in_array("med", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_report_user")?>利用可能ユーザ数</th>
<td><select name="lcs_report_user" id="lcs_report_user"<?=(in_array("med", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("", "0", "5", "10", "50", "100", "200", "300", "400", "FULL");
    write_user_options($licenseRow["lcs_report_user"], $dbLicenseRow["lcs_report_user"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // 勤務シフト ======================================================================================================= ?>
<tr id="tr_lcs_shift_emps" <?=(in_array("shift", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_shift_emps")?>対象者職員数</th>
<td><select name="lcs_shift_emps" id="lcs_shift_emps"<?=(in_array("shift", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("");
    for ($i = 0; $i <= 900; $i += 10) $kouho[] = "$i";
    for ($i = 1000; $i <= 3000; $i += 100) $kouho[] = "$i";
	$kouho[] = "FULL";
    write_user_options($licenseRow["lcs_shift_emps"], $dbLicenseRow["lcs_shift_emps"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // 人事管理 ======================================================================================================= ?>
<tr id="tr_lcs_jinji_emps" <?=(in_array("jinji", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_jinji_emps")?>対象者職員数</th>
<td><select name="lcs_jinji_emps" id="lcs_jinji_emps"<?=(in_array("jinji", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("");
    for ($i = 0; $i <= 900; $i += 30) $kouho[] = "$i";
    for ($i = 1000; $i <= 3000; $i += 100) $kouho[] = "$i";
	$kouho[] = "FULL";
    write_user_options($licenseRow["lcs_jinji_emps"], $dbLicenseRow["lcs_jinji_emps"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // クリニカルラダー ======================================================================================================= ?>
<tr id="tr_lcs_ladder_user" <?=(in_array("ladder", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_ladder_user")?>利用可能ユーザ数</th>
<td><select name="lcs_ladder_user" id="lcs_ladder_user"<?=(in_array("ladder", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("", "0");
    for ($i = 200; $i <= 1000; $i += 100) $kouho[] = "$i";
    $kouho[] = "FULL";
    write_user_options($licenseRow["lcs_ladder_user"], $dbLicenseRow["lcs_ladder_user"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // キャリア開発ラダー ======================================================================================================= ?>
<tr id="tr_lcs_career_user" <?=(in_array("career", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_career_user")?>利用可能ユーザ数</th>
<td><select name="lcs_career_user" id="lcs_career_user"<?=(in_array("career", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("", "0");
    for ($i = 200; $i <= 1000; $i += 100) $kouho[] = "$i";
    $kouho[] = "FULL";
    write_user_options($licenseRow["lcs_career_user"], $dbLicenseRow["lcs_career_user"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>



<? // 日報・月報 ======================================================================================================= ?>
<tr id="tr_lcs_jnl_user" <?=(in_array("jnl", $erasedAppIds)?'style="display:none"':"")?>>
<th><?=$c2app->getLcsLabel("lcs_jnl_user")?>利用可能ユーザ数</th>
<td><select name="lcs_jnl_user" id="lcs_jnl_user"<?=(in_array("jnl", $erasedAppIds)?' disabled':"")?>>
<?
    $kouho = array("", "0", "5", "10", "50");
    for ($i = 100; $i <= 3000; $i += 100) $kouho[] = "$i";
    $kouho[] = "FULL";
    write_user_options($licenseRow["lcs_jnl_user"], $dbLicenseRow["lcs_jnl_user"], $kouho, $db_lcs_max_user);
?>
</select></td>
</tr>




<? // 未来アプリ ======================================================================================================= ?>
<? for ($idx=1; $idx<=15; $idx++) { ?>
    <? if ($applications["ccusr".$idx]) { ?>
        <tr id="tr_lcs_ccusr<?=$idx?>_user" <?=(in_array("ccusr".$idx, $erasedAppIds)?'style="display:none"':"")?> is_hidable="1">
        <th><?=$c2app->getLcsLabel("lcs_ccusr".$idx."_user")?>利用可能ユーザ数</th>
        <td><select name="lcs_ccusr<?=$idx?>_user" id="lcs_ccusr<?=$idx?>_user"<?=(in_array("ccusr".$idx, $erasedAppIds)?' disabled':"")?>>
        <?
            $kouho = array("");
            for ($nn = 100; $nn <= 3000; $nn += 100) $kouho[] = $nn;
            $kouho[] = "FULL";
            write_user_options($licenseRow["lcs_ccusr".$idx."_user"], $dbLicenseRow["lcs_ccusr".$idx."_user"], $kouho, $db_lcs_max_user);
        ?>
        </select></td>
        </tr>
    <? } ?>
    <? if ($applications["ccadm".$idx]) { ?>
        <tr id="tr_lcs_ccadm<?=$idx?>_user" <?=(in_array("ccadm".$idx, $erasedAppIds)?'style="display:none"':"")?> is_hidable="1">
        <th><?=$c2app->getLcsLabel("lcs_ccadm".$idx."_user")?>利用可能ユーザ数</th>
        <td><select name="lcs_ccadm<?=$idx?>_user" id="lcs_ccadm<?=$idx?>_user"<?=(in_array("ccadm".$idx, $erasedAppIds)?' disabled':"")?>>
        <?
            $kouho = array("");
            for ($nn = 100; $nn <= 3000; $nn += 100) $kouho[] = $nn;
            $kouho[] = "FULL";
            write_user_options($licenseRow["lcs_ccadm".$idx."_user"], $dbLicenseRow["lcs_ccadm".$idx."_user"], $kouho, $db_lcs_max_user);
        ?>
        </select></td>
        </tr>
    <? } ?>
<? } ?>



<? // 特製アプリ ======================================================================================================= ?>
<? for ($idx=1; $idx<=5; $idx++) { ?>
    <? if ($applications["omusr".$idx]) { ?>
        <tr id="tr_lcs_omusr<?=$idx?>_user" <?=(in_array("omusr".$idx, $erasedAppIds)?'style="display:none"':"")?> is_hidable="1">
        <th><?=$c2app->getLcsLabel("lcs_omusr".$idx."_user")?>利用可能ユーザ数</th>
        <td><select name="lcs_omusr<?=$idx?>_user" id="lcs_omusr<?=$idx?>_user"<?=(in_array("omusr".$idx, $erasedAppIds)?' disabled':"")?>>
        <?
            $kouho = array("");
            for ($nn = 100; $nn <= 3000; $nn += 100) $kouho[] = $nn;
            $kouho[] = "FULL";
            write_user_options($licenseRow["lcs_omusr".$idx."_user"], $dbLicenseRow["lcs_omusr".$idx."_user"], $kouho, $db_lcs_max_user);
        ?>
        </select></td>
        </tr>
    <? } ?>
    <? if ($applications["omadm".$idx]) { ?>
        <tr id="tr_lcs_omadm<?=$idx?>_user" <?=(in_array("omadm".$idx, $erasedAppIds)?'style="display:none"':"")?> is_hidable="1">
        <th><?=$c2app->getLcsLabel("lcs_omadm".$idx."_user")?>利用可能ユーザ数</th>
        <td><select name="lcs_omadm<?=$idx?>_user" id="lcs_omadm<?=$idx?>_user"<?=(in_array("omadm".$idx, $erasedAppIds)?' disabled':"")?>>
        <?
            $kouho = array("");
            for ($nn = 100; $nn <= 3000; $nn += 100) $kouho[] = $nn;
            $kouho[] = "FULL";
            write_user_options($licenseRow["lcs_omadm".$idx."_user"], $dbLicenseRow["lcs_omadm".$idx."_user"], $kouho, $db_lcs_max_user);
        ?>
        </select></td>
        </tr>
    <? } ?>
<? } ?>



<? // 購入する業務機能 ======================================================================================================= ?>
<tr>
	<th style="vertical-align:top; padding-top:3px">購入する業務機能</th>
    <td>
    	<table cellspacing="0" cellpadding="0" class="table_purchase"><tr>
    		<td><?=$purchasable[0]?></td><td><?=$purchasable[1]?></td><td><?=$purchasable[2]?></td>
    	</tr></table>
    </td>
</tr>



<? // 購入済み業務機能 ======================================================================================================= ?>
<? if (!$GEN_LICENSE) { ?>
<tr>
	<th style="vertical-align:top; padding-top:3px">購入済み業務機能</th>
    <td>
    	<table cellspacing="0" cellpadding="0" class="table_purchase"><tr>
    		<td><?=$purchased[0]?></td><td><?=$purchased[1]?></td><td><?=$purchased[2]?></td>
    	</tr></table>
    </td>
</tr>
<? } ?>



<? // 利用期限 ======================================================================================================= ?>
<tr>
	<th>利用期限</th>
	<td>
		<label><input type="radio" name="lcs_expire_flg" value="f"<? if ($licenseRow["lcs_expire_flg"] != "t") {echo(" checked");} ?>
			onclick="setExpireDisabled();">なし</label>
		<label><input type="radio" name="lcs_expire_flg" value="t"<? if ($licenseRow["lcs_expire_flg"] == "t") {echo(" checked");} ?>
			onclick="setExpireDisabled();">あり</label>

		<select name="expire_year"><? show_update_years($licenseRow["expire_year"], 10, true) ?></select>
		/
		<select name="expire_month"><? show_select_months($licenseRow["expire_month"], true) ?></select>
		/
		<select name="expire_day"><? show_select_days($licenseRow["expire_day"], true) ?></select>
	</td>
</tr>



<? // ライセンスキー番号（ユーザ入力） ============================================================================================== ?>
<? if (!$GEN_LICENSE) { ?>
<tr>
	<th>ライセンスキー番号</th>
	<td><input class="text" type="password" name="license_key" size="26" maxlength="10" style="ime-mode:disabled;" autocomplete="off" value="" /></td>
</tr>
<? } ?>



<? // 管理者パスワード ======================================================================================================= ?>
<? if ($GEN_LICENSE) { ?>
<tr>
	<th>管理者パスワード</th>
	<td>
		<input class="text" type="password" name="admin_pwd" size="20" style="ime-mode:disabled;" autocomplete="off"
		value="<?=($licenseRow["remain_pwd"]?hh($licenseRow["admin_pwd"]):"")?>" />
		<label><input type="checkbox" name="remain_pwd" value="1"<?=($licenseRow["remain_pwd"]?" checked":"")?>>生成のたびにクリアしない</label>
	</td>
</tr>
<? } ?>





</table>





<? // 更新ボタン ======================================================================================================= ?>
<? if (!$GEN_LICENSE) { ?>
<div style="padding-top:20px; text-align:right">
	<input type="hidden" name="try_update" value="1" />
	<input type="submit" value="更新">
</div>
<? } ?>




<? // ライセンス証書番号生成ボタン ============================================================================================ ?>
<? if ($GEN_LICENSE) { ?>
<div style="padding-top:20px; text-align:right">
	<input type="hidden" name="try_generate" value="1" />
	<input type="submit" value="ライセンス証書番号生成">
</div>
<? } ?>



<? // ライセンス証書番号 ======================================================================================================= ?>
<? if ($GEN_LICENSE) { ?>
<? if ($gen_license_key != "") { ?>
<div style="padding-top:20px">
	<div style="height:20px; line-height:20px; text-align:left; padding:3px 6px; color:#000000; background-color:#bdd1e7; font-family:Verdana">
		<b>ライセンス証書番号：<? echo($gen_license_key); ?></b>
	</div>
</div>
<? } ?>
<? } ?>



	<?=$LEGACY_CONTENT_HTML ?>




	<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</div>
</body>
</html>