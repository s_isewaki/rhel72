<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜対象者検索</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_attendance_pattern.ini");
require_once("show_select_values.ini");
require_once("show_timecard_common.ini");
require_once("work_admin_timecard_common.php");
require_once("atdbk_menu_common.ini");
require_once("get_values.ini");
require_once("timecard_tantou_class.php");
require_once("atdbk_close_class.php");


// ページ名func
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// リンク出力用変数の設定
if ($page == "") {
	$page = 0;
}
$limit = 10000;
$url_srch_name = urlencode($srch_name);

// DBコネクション作成
$con = connect2db($fname);

$emp_id = get_emp_id($con, $session, $fname);
// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}

// 締め日を取得
// ************ oose update start *****************
//$sql = "select closing from timecard";
$sql = "select closing, closing_parttime, closing_month_flg from timecard";
// ************ oose update end *****************
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
//未設定時は末日とする 20110720
if ($closing == "") {
	$closing = "6";
}
// ************ oose add start *****************
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
// ************ oose add end *****************
$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}

// 締め日が登録済みの場合
if ($closing != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}

	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		if ($day <= $closing_day) {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}

			$end_year = $year;
			$end_month = $month;
		} else {
			$start_year = $year;
			$start_month = $month;

			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	// 対象年月度を変数にセット
	$c_yyyymm = substr($start_date, 0, 6);

	$calc_month = $start_month; //上で計算した月

	//画面指定期間がある場合はそちらを使用
	if ($select_start_yr != "") {
		$start_year = $select_start_yr;
		$start_month = $select_start_mon;
		$start_day = $select_start_day;
		$end_year = $select_end_yr;
		$end_month = $select_end_mon;
		$end_day = $select_end_day;
	} else {
		if (!$calced) {
			$day = date("j");
			$start_day = $closing_day + 1;
			$end_day = $closing_day;


			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $calc_month; //上で計算した月

				$end_year = $year;
				$end_month = $calc_month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $calc_month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $calc_month;
			}
		}
	}

	// タイムカード設定情報を取得
	$sql = "select ovtmscr_tm from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
	}
	if ($ovtmscr_tm == "") {$ovtmscr_tm = 0;}

}

$arr_class_name = get_class_name_array($con, $fname);

//$mmode = "usr"; //menumode usr:権限ありの場合ユーザ側で対象者検索を使用可 "",mng:管理側
$timecard_tantou_class = new timecard_tantou_class($con, $fname);
if ($mmode == "usr") {
	$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
}
else {
	$timecard_tantou_branches = array();
}
// 組織情報を取得し、配列に格納
list($arr_cls, $arr_clsatrb, $arr_atrbdept) = $timecard_tantou_class->get_clsatrb_info($timecard_tantou_branches, $mmode);

// 該当する職員を検索
$sel_emp = false;
if ($srch_flg == "1") {
	$sql = "select count(*) from empmst";

	//検索条件SQLを取得 work_admin_timecard_common.php
    $cond = get_cond_work_admin($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id, $sus_flg);

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_cnt = pg_fetch_result($sel, 0, 0);

	$sql = "select emp_id, emp_personal_id, emp_lt_nm, emp_ft_nm, emp_class, emp_attribute, emp_dept from empmst";
	$offset = $page * $limit;
	$cond .= " order by emp_personal_id offset $offset limit $limit";
	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}
// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);
// ライセンス情報の取得
$sql = "select * from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 14; $i++) {
	$var_name = "func$i";
	$$var_name = pg_fetch_result($sel, 0, "lcs_func$i");
}

$sql = "select * from duty_shift_group ";
$cond = " order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num = pg_num_rows($sel);
$arr_shift_group = array();
for ($i = 0; $i < $num; $i++) {
	$wk_shift_group_id = pg_fetch_result($sel, $i, "group_id");
	$arr_shift_group["$wk_shift_group_id"] = pg_fetch_result($sel, $i, "group_name");
}


//20150707
$arr_closing = get_csv_closing_info($con, $fname);
//print_r($arr_closing);
//$arr_closing["02"];
$atdbk_close_class = new atdbk_close_class($con, $fname);

$arr_closing_info = array();
$i=0;
$arr_closing_info[$i]["closing"] = $closing;
$arr_closing_info[$i]["start"] = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
$arr_closing_info[$i]["end"] = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
$arr_closing_info[$i]["start_y"] = $start_year;
$arr_closing_info[$i]["start_m"] = sprintf("%02d", $start_month);
$arr_closing_info[$i]["start_d"] = sprintf("%02d", $start_day);
$arr_closing_info[$i]["end_y"] = $end_year;
$arr_closing_info[$i]["end_m"] = sprintf("%02d", $end_month);
$arr_closing_info[$i]["end_d"] = sprintf("%02d", $end_day);
for ($i=1; $i<=20; $i++) {
	$wk_id = sprintf("%02d", $i);
	if ($arr_closing["$wk_id"] != "") {
		$arr_closing_info[$i]["closing"] = $arr_closing["$wk_id"];
		$arr_term = $atdbk_close_class->get_term($c_yyyymm, $arr_closing["$wk_id"], $closing_month_flg);
		$arr_closing_info[$i]["start"] = $arr_term["start"];
		$arr_closing_info[$i]["end"] = $arr_term["end"];
		$arr_closing_info[$i]["start_y"] = substr($arr_term["start"], 0, 4);
		$arr_closing_info[$i]["start_m"] = substr($arr_term["start"], 4, 2);
		$arr_closing_info[$i]["start_d"] = substr($arr_term["start"], 6, 2);
		$arr_closing_info[$i]["end_y"] = substr($arr_term["end"], 0, 4);
		$arr_closing_info[$i]["end_m"] = substr($arr_term["end"], 4, 2);
		$arr_closing_info[$i]["end_d"] = substr($arr_term["end"], 6, 2);
	}
}
//print_r($arr_closing_info);

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function initPage() {
	classOnChange('<? echo($atrb); ?>', '<? echo($dept); ?>');
}

function classOnChange(atrb_id, dept_id) {
	if (!document.mainform) {
		return;
	}

	var class_id = document.mainform.cls.value;

	deleteAllOptions(document.mainform.atrb);

	addOption(document.mainform.atrb, '-', '----------', atrb_id);

<? foreach ($arr_clsatrb as $tmp_clsatrb) { ?>
	if (class_id == '<? echo $tmp_clsatrb["class_id"]; ?>') {
	<? foreach($tmp_clsatrb["atrbs"] as $tmp_atrb) { ?>
		addOption(document.mainform.atrb, '<? echo $tmp_atrb["id"]; ?>', '<? echo $tmp_atrb["name"]; ?>', atrb_id);
	<? } ?>
	}
<? } ?>

	atrbOnChange(dept_id);
}

function atrbOnChange(dept_id) {
	var atrb_id = document.mainform.atrb.value;

	deleteAllOptions(document.mainform.dept);

	addOption(document.mainform.dept, '-', '----------', dept_id);

<? foreach ($arr_atrbdept as $tmp_atrbdept) { ?>
	if (atrb_id == '<? echo $tmp_atrbdept["atrb_id"]; ?>') {
	<? foreach($tmp_atrbdept["depts"] as $tmp_dept) { ?>
		addOption(document.mainform.dept, '<? echo $tmp_dept["id"]; ?>', '<? echo $tmp_dept["name"]; ?>', dept_id);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
	}
}

function setYmDisplay() {
	var display;
	switch (document.mainform._action.value) {
	case 'CALC':
		display = 'none';
		break;
	default:
		display = '';
	}
	document.mainform._yyyymm.style.display = display;
}

function doAction() {
	var emp_ids = new Array();
	if (document.mainform.elements['emp_id[]'] != null) {
// && document.mainform._action.value != 'CSV2'
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			if (document.mainform.elements['emp_id[]'].checked) {
				emp_ids.push(document.mainform.elements['emp_id[]'].value);
			}
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.mainform.elements['emp_id[]'][i].checked) {
					emp_ids.push(document.mainform.elements['emp_id[]'][i].value);
				}
			}
		}
	}
    if (emp_ids.length == 0) {
	    alert('対象者が選択されていません。');
	    return;
    }

	switch (document.mainform._action.value) {
	case 'CALC':
		document.mainform.action = 'work_admin_total.php';
		document.mainform.submit();
		break;
	case 'CSV1':
		document.csv.emp_id_list.value = emp_ids.join(',');
		document.csv.action = 'work_admin_csv1.php';
		document.csv.yyyymm.value = document.mainform._yyyymm.value;
		document.csv.target = 'download';
		document.csv.submit();
		break;
	case 'CSV2':
		document.csv.emp_id_list.value = emp_ids.join(',');
		var top_emp_id = emp_ids[0];
		//document.csv.emp_id_list.value = '1';
		document.csv.yyyymm.value = document.mainform._yyyymm.value;

		var layout_id = document.mainform.csv_layout_id.value;
		if (layout_id == "-") {
			layout_id = '01';
		}
		if (document.mainform.shift_group_id) {
			var shift_group_id = document.mainform.shift_group_id.value;
		}

		wx = 1100;
		wy = 750;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_layout.php';
		url += '?session=<?=$session?>';
		url += '&layout_id='+layout_id;
		url += '&shift_group_id='+shift_group_id;
		url += '&yyyymm='+document.mainform._yyyymm.value;
		url += '&mmode=<? echo($mmode); ?>';
		url += '&top_emp_id='+top_emp_id;
		childwin_print_select = window.open(url, 'printSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

		break;
	case 'PRINT':
		wx = 470;
		wy = 270;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_pxdoc_select.php';
		url += '?session=<?=$session?>';
		url += '&layout_id='+layout_id;
		url += '&shift_group_id='+shift_group_id;
		url += '&yyyymm='+document.mainform._yyyymm.value;

		childwin_print_select = window.open(url, 'printSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

		break;
	case 'CSV_KAI':
		document.csv.emp_id_list.value = emp_ids.join(',');
		document.csv.action = 'work_admin_csv_kai.php';
		document.csv.yyyymm.value = document.mainform._yyyymm.value;
		document.csv.target = 'download';
		document.csv.submit();
		break;
	}
}

var childwin_print_select = null;
function shift_list(emp_id) {

		wx = 480;
		wy = 320;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_shift_list.php';
		url += '?session=<?=$session?>';
		url += '&emp_id=' + emp_id;
		childwin_print_select = window.open(url, 'printSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

//チェックボックスONOFF
function checkAll() {

	stat = (document.mainform.checkall.value == '全てOFF') ? false : true;
	document.mainform.checkall.value = (document.mainform.checkall.value == '全てOFF') ? '全てON' : '全てOFF';
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			document.mainform.elements['emp_id[]'].checked = stat;
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				document.mainform.elements['emp_id[]'][i].checked = stat;
			}
		}
	}
}
//ダウンロード
function downloadCSV() {

	if (document.mainform.csv_layout_id.value == '-') {
		alert('月集計CSVレイアウトを選択してください');
		return;
	}

	document.csv.emp_id_list.value = '';
	document.csv.srch_id.value = document.mainform.srch_id.value;
	document.csv.srch_name.value = document.mainform.srch_name.value;
	document.csv.cls.value = document.mainform.cls.value;
	document.csv.atrb.value = document.mainform.atrb.value;
	document.csv.dept.value = document.mainform.dept.value;
	document.csv.duty_form_jokin.value = (document.mainform.duty_form_jokin.checked) ? "checked" : "";
	document.csv.duty_form_hijokin.value = (document.mainform.duty_form_hijokin.checked) ? "checked" : "";
	document.csv.group_id.value = document.mainform.group_id.value;
	if (document.mainform.shift_group_id) {
		document.csv.shift_group_id.value = document.mainform.shift_group_id.value;
	}
	document.csv.csv_layout_id.value = document.mainform.csv_layout_id.value;
	document.csv.yyyymm.value = document.mainform._yyyymm2.value;
	//期間指定
	document.csv.select_start_yr.value = document.mainform.select_start_yr.value;
	document.csv.select_start_mon.value = document.mainform.select_start_mon.value;
	document.csv.select_start_day.value = document.mainform.select_start_day.value;
	document.csv.select_end_yr.value = document.mainform.select_end_yr.value;
	document.csv.select_end_mon.value = document.mainform.select_end_mon.value;
	document.csv.select_end_day.value = document.mainform.select_end_day.value;
	document.csv.sus_flg.value = (document.mainform.sus_flg.checked) ? "checked" : "";

	document.csv.action = 'work_admin_csv_download.php';
	document.csv.target = 'download';
	document.csv.submit();

}
//レイアウト調整画面
function openLayout() {

	document.csv.emp_id_list.value = '';
	if (document.mainform._yyyymm) {
		document.csv.yyyymm.value = document.mainform._yyyymm.value;
	}

	var layout_id = document.mainform.csv_layout_id.value;

	wx = 1100;
	wy = 750;
	dx = screen.width / 2 - (wx / 2);
	dy = screen.top;
	base = screen.height / 2 - (wy / 2);
	var url = 'work_admin_csv_layout.php';
	url += '?session=<?=$session?>';
	url += '&layout_id='+layout_id;
	url += '&from_flg=1';
	url += '&mmode=<? echo($mmode); ?>';
	childwin_print_select = window.open(url, 'printSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

var sy = <?=$start_year?>;
var sm = <?=intval($start_month);?>;
var ey = <?=$end_year?>;
var em = <?=intval($end_month);?>;

//月度変更時、期間も変更
function chg_ym(ym) {

	default_y = <?=$start_year?>;
	default_m = <?=$calc_month?>;

	yyyy=ym.substring(0, 4);
	mm=eval(ym.substring(4, 6));

	//初期年月との月の差
	diff = (yyyy*12+mm) - (default_y*12+default_m);

	//開始年月を計算し設定
//	sy = <?=$start_year?>;
//	sm = <?=intval($start_month);?>;
	sd = 1;

	dates = new Date();

	dates.setYear(sy - 1900);
	dates.setMonth(sm - 1);
	dates.setDate(sd);

	dates.setMonth(dates.getMonth()+diff);

	yyyy1 = dates.getYear() + 1900;
	yyyy = (yyyy1 < 1900) ? yyyy1 + 1900 : yyyy1;
	mm = dates.getMonth()+1;
	mm = (mm < 10) ? '0'+mm : mm;

	document.mainform.select_start_yr.value = yyyy;
	document.mainform.select_start_mon.value = mm;

	//終了年月を計算し設定
//	ey = <?=$end_year?>;
//	em = <?=intval($end_month);?>;
	ed = 1;

	datee = new Date();

	datee.setYear(ey - 1900);
	datee.setMonth(em - 1);
	datee.setDate(ed);

	datee.setMonth(datee.getMonth()+diff);

	yyyy1 = datee.getYear()+1900;
	yyyy = (yyyy1 < 1900) ? yyyy1 + 1900 : yyyy1;
	mm = datee.getMonth()+1;
	mm = (mm < 10) ? '0'+mm : mm;
	document.mainform.select_end_yr.value = yyyy;
	document.mainform.select_end_mon.value = mm;

	set_last_day();
}
//印刷（pxdoc）
function print_pxdoc() {
	var emp_ids = new Array();
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			if (document.mainform.elements['emp_id[]'].checked) {
				emp_ids.push(document.mainform.elements['emp_id[]'].value);
			}
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.mainform.elements['emp_id[]'][i].checked) {
					emp_ids.push(document.mainform.elements['emp_id[]'][i].value);
				}
			}
		}
	}
	if (emp_ids.length == 0) {
		alert('対象者が選択されていません。');
		return;
	}

	document.csv.emp_id_list.value = emp_ids.join(',');
	document.csv.action = 'work_admin_pxdoc.php';
	document.csv.yyyymm.value = document.mainform._yyyymm.value;
	document.csv.target = 'download';
	document.csv.submit();

}
//終了日月末設定
function set_last_day() {
	day = document.mainform.select_end_day.value;
	day = parseInt(day,10);
	if (day >= 28) {
		year = document.mainform.select_end_yr.value;
		month = document.mainform.select_end_mon.value;
		last_day = days_in_month(year, month);
		if (last_day != day) {
			day = last_day;
			dd = (day < 10) ? '0'+day : day;
			document.mainform.select_end_day.value = dd;
		}
	}

}
//月末取得
function days_in_month(year, month) {
	//月別日数配列
	lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
	//閏年
	wk_year = parseInt(year,10);
	wk_mon = parseInt(month,10);
	if ((wk_mon == 2) &&
		((wk_year % 4) == 0 && ((wk_year % 100) != 0 || (wk_year % 400)))) {
		wk_mon = 13;
	}

	days = lastDay[wk_mon-1];
	return days;
}

//PDF印刷調整画面
function pdf_select(btn_name) {
	var emp_ids = new Array();
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			if (document.mainform.elements['emp_id[]'].checked) {
				emp_ids.push(document.mainform.elements['emp_id[]'].value);
			}
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.mainform.elements['emp_id[]'][i].checked) {
					emp_ids.push(document.mainform.elements['emp_id[]'][i].value);
				}
			}
		}
	}
	var top_emp_id = '';
	if (emp_ids.length > 0) {
		top_emp_id = emp_ids[0];
	}

		document.csv.pdf_mode.value = btn_name	// 20130129

		wx = 470;
		wy = 270;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_pdf_select.php';
		url += '?session=<?=$session?>';
		url += '&yyyymm='+document.mainform._yyyymm2.value;
		url += '&top_emp_id='+top_emp_id;

		childwin_print_select = window.open(url, 'pdfprintSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

//打刻、残業時間、残業未承認画面
function check_workhours(menuName){
	var emp_ids = new Array();
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			if (document.mainform.elements['emp_id[]'].checked) {
				emp_ids.push(document.mainform.elements['emp_id[]'].value);
			}
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.mainform.elements['emp_id[]'][i].checked) {
					emp_ids.push(document.mainform.elements['emp_id[]'][i].value);
				}
			}
		}
	}
	if (emp_ids.length == 0) {
		alert('対象者が選択されていません。');
		return;
	}

	wx = 1100;
	wy = 800;
	dx = screen.width / 2 - (wx / 2);
	dy = screen.top;
	base = screen.height / 2 - (wy / 2);
	window.open('', 'printSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	document.csv.emp_id_list.value = emp_ids.join(',');
	document.csv.menuname.value = menuName;
	
	document.csv.srch_id.value = document.mainform.srch_id.value;
	document.csv.srch_name.value = document.mainform.srch_name.value;
	document.csv.group_id.value = document.mainform.group_id.value;
	if (document.mainform.shift_group_id) {
		document.csv.shift_group_id.value = document.mainform.shift_group_id.value;
	}

	document.csv.action = 'work_admin_timecard_check.php';
	document.csv.target = 'printSelectPopup';
	document.csv.submit();
}

//印刷（pdf）
<?
//条件に合う職員をプログラム内で検索するため、CSVダウンロードと同様の処理、引数渡しとする。
?>
function print_pdf() {

	var emp_ids = new Array();
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			if (document.mainform.elements['emp_id[]'].checked) {
				emp_ids.push(document.mainform.elements['emp_id[]'].value);
			}
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.mainform.elements['emp_id[]'][i].checked) {
					emp_ids.push(document.mainform.elements['emp_id[]'][i].value);
				}
			}
		}
	}
	if (emp_ids.length == 0) {
		alert('対象者が選択されていません。');
		return;
	}

	window.open("",
		"pdf", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=1000,height=721");

	document.csv.emp_id_list.value = emp_ids.join(',');

	document.csv.srch_id.value = document.mainform.srch_id.value;
	document.csv.srch_name.value = document.mainform.srch_name.value;
	document.csv.cls.value = document.mainform.cls.value;
	document.csv.atrb.value = document.mainform.atrb.value;
	document.csv.dept.value = document.mainform.dept.value;
	document.csv.group_id.value = document.mainform.group_id.value;
	if (document.mainform.shift_group_id) {
		document.csv.shift_group_id.value = document.mainform.shift_group_id.value;
	}
	document.csv.csv_layout_id.value = document.mainform.csv_layout_id.value;
	document.csv.duty_form_jokin.value = (document.mainform.duty_form_jokin.checked) ? "checked" : "";

	document.csv.duty_form_hijokin.value = (document.mainform.duty_form_hijokin.checked) ? "checked" : "";
	document.csv.action = (document.csv.pdf_mode.value == "pdf_shift") ? 'atdbk_timecard_shift_pdf.php' : 'work_admin_pdf.php';

	document.csv.target = 'pdf';
	document.csv.submit();



}

var c_yyyymm = '<? echo "$c_yyyymm";?>';
var arr_closing_info = new Array();
<?
for ($i=0; $i<=20; $i++) {
	if ($arr_closing_info[$i]["closing"] != "") {
		echo "arr_closing_info[$i] = new Array()\n";
		echo "arr_closing_info[$i]['closing'] = '{$arr_closing_info[$i]['closing']}';\n";
		echo "arr_closing_info[$i]['start_y'] = '{$arr_closing_info[$i]['start_y']}';\n";
		echo "arr_closing_info[$i]['start_m'] = '{$arr_closing_info[$i]['start_m']}';\n";
		echo "arr_closing_info[$i]['start_d'] = '{$arr_closing_info[$i]['start_d']}';\n";
		echo "arr_closing_info[$i]['end_y'] = '{$arr_closing_info[$i]['end_y']}';\n";
		echo "arr_closing_info[$i]['end_m'] = '{$arr_closing_info[$i]['end_m']}';\n";
		echo "arr_closing_info[$i]['end_d'] = '{$arr_closing_info[$i]['end_d']}';\n";
	}
}
?>
function layout_id_chg(val) {
	//alert(val);
	var idx;
	if (val == '-') {
		idx = 0;
	}
	else {
		idx = parseInt(val, 10);
		if (!arr_closing_info[idx]) {
			idx = 0;
		}
	}
	
	document.mainform.select_start_yr.value = arr_closing_info[idx]['start_y'];
	document.mainform.select_start_mon.value = arr_closing_info[idx]['start_m'];
	document.mainform.select_start_day.value = arr_closing_info[idx]['start_d'];
	document.mainform.select_end_yr.value = arr_closing_info[idx]['end_y'];
	document.mainform.select_end_mon.value = arr_closing_info[idx]['end_m'];
	document.mainform.select_end_day.value = arr_closing_info[idx]['end_d'];
	document.mainform._yyyymm2.value = c_yyyymm;
	sy = arr_closing_info[idx]['start_y'];
	sm = parseInt(arr_closing_info[idx]['start_m'], 10);
	ey = arr_closing_info[idx]['end_y'];
	em = parseInt(arr_closing_info[idx]['end_m'], 10);
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.block3 {border-collapse:collapse;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($mmode == "usr") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($checkauth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1&mmode=mng"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<?
}
elseif ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
if ($mmode != "usr") {
	show_work_admin_menuitem($session, $fname, "");
}
else {
	$arr_option["mmode"] = $mmode;
	show_atdbk_menuitem($session, $fname, $arr_option);
}
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
//サブメニュー表示
$arr_option["mmode"] = $mmode;
show_work_admin_submenu($session,$fname,$arr_option);
// ************ oose update start *****************
//	if ($closing == "") {  // 締め日未登録の場合
	if (($closing == "") || ($closing_parttime == "")) {  // 締め日未登録の場合
// ************ oose update end *****************
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。出退勤マスターメンテナンスの締め日画面で登録してください。</font></td>
</tr>
</table>
<? } else { ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="work_admin_menu.php" method="post">
<table width="1050" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
		<td colspan="2"><input type="text" name="srch_id" value="<? echo($srch_id); ?>" size="15" maxlength="12" style="ime-mode:inactive;"></td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名</font></td>
		<td colspan="2"><input type="text" name="srch_name" value="<? echo($srch_name); ?>" size="25" maxlength="50" style="ime-mode:active;"></td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">属性</font></td>
		<td nowrap valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="cls" onchange="classOnChange();">
				<option value="-">----------
<?
foreach ($arr_cls as $tmp_cls) {
	$tmp_id = $tmp_cls["id"];
	$tmp_name = $tmp_cls["name"];
	echo("<option value=\"$tmp_id\"");
	if ($tmp_id == $cls) {
		echo(" selected");
	}
	echo(">$tmp_name");
}
?>
				</select><? echo($arr_class_name[0]); ?><br>
			<select name="atrb" onchange="atrbOnChange();">
			</select><? echo($arr_class_name[1]); ?><br>
			<select name="dept">
			</select><? echo($arr_class_name[2]); ?>
</font>
</td>
<td nowrap valign="top">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($mmode != "usr") { ?>
&nbsp;出勤グループ
<select name="group_id">
<option value="-">----------</option>
<?
foreach ($group_names as $tmp_group_id => $group_name) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $group_id) {
		echo(" selected");
	}
	echo(">$group_name\n");
}
?>
</select>
<br>
	<?
//シフトグループ
	if ($func9 == "t") {
		echo("&nbsp;シフトグループ");
?>
<select name="shift_group_id">
<option value="-">----------</option>
<?
		foreach ($arr_shift_group as $tmp_group_id => $shift_group_name) {
			echo("<option value=\"$tmp_group_id\"");
			if ($tmp_group_id == $shift_group_id) {
				echo(" selected");
			}
			echo(">$shift_group_name\n");
		}
?>
</select>
<?
//<br>

	}

?>
<? }
//$mmode == "usr"
else {
?>
<input type="hidden" name="group_id" value="-">
<input type="hidden" name="shift_group_id" value="-">
<? } ?>

<table border="0" cellspacing="0" cellpadding="1" class="block3">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
月集計CSVレイアウト</font>
<select name="csv_layout_id" id="csv_layout_id" onChange="layout_id_chg(this.value);">
<option value="-">----------</option>
	<?
	$arr_layout = get_layout_name($con, $fname, "");
for ($i=1; $i<=20; $i++) {
	$wk_id = sprintf("%02d", $i);
	echo("<option value=\"$wk_id\"");
	if ($csv_layout_id == $wk_id) {
		echo(" selected");
	}
		echo(">レイアウト{$wk_id} {$arr_layout[$wk_id]["name"]}");
		echo("</option>");
	}
?>
</select>
</td>
<td>

</font>
	<?
	echo("<select name=\"_yyyymm2\" onChange=\"chg_ym(this.value);\">\n");
	for ($y = date("Y"); $y >= 2004; $y--) {
		for ($m = 12; $m >= 1; $m--) {
			$mm = sprintf("%02d", $m);
			echo("<option value=\"$y$mm\"");
			if ("$y$mm" == $c_yyyymm) {
				echo(" selected");
			}
			echo(">{$y}年{$m}月度\n");
		}
	}
	echo("</select>");
	?>
<input type="button" value="ダウンロード" onclick="downloadCSV();">
<input type="button" value="CSVレイアウト設定" onclick="openLayout();">
</td>
</tr>
<tr>
<td>
</td>
<td>
				<select name="select_start_yr">
				<? show_select_years(10, $start_year, false, true); ?>
				</select>/<select name="select_start_mon">
				<? show_select_months($start_month, false); ?>
				</select>/<select name="select_start_day">
				<? show_select_days($start_day, false); ?>
				</select> 〜 <select name="select_end_yr" onchange="set_last_day();">
				<? show_select_years(10, $end_year, false, true); ?>
				</select>/<select name="select_end_mon" onchange="set_last_day();">
				<? show_select_months($end_month, false); ?>
				</select>/<select name="select_end_day">
				<? show_select_days($end_day, false); ?>
				</select>
</td>
</tr>
</table>

</td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">雇用・勤務形態</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="checkbox" name="duty_form_jokin" value="checked" <?=$duty_form_jokin?>>常勤</label>　
			<label><input type="checkbox" name="duty_form_hijokin" value="checked" <?=$duty_form_hijokin?>>非常勤</label>
			</font>
		</td>
		<td align="left" width="700">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="checkbox" name="sus_flg" value="checked" <?=$sus_flg?>>利用停止者も含める</label>
			</font>
		</td>
	</tr>
</table>
<table width="1050" border="0" cellspacing="0" cellpadding="2">
<tr>
<td width="100"></td>
<td align="left"><input type="submit" value="検索"></td>
<td width="200"></td>
<td>
	<input type="button" name="check_timecard" value="打刻チェック" onclick="check_workhours(this.name);">&nbsp;
	<input type="button" name="check_overtime" value="残業時間チェック" onclick="check_workhours(this.name);">&nbsp;
	<input type="button" name="check_unapproved" value="残業未承認チェック" onclick="check_workhours(this.name);">&nbsp;
	<input type="button" name="pdf_A4" value="タイムカードA4横印刷（PDF）" onclick="pdf_select(this.name);">&nbsp;
	<input type="button" name="pdf_shift" value="出勤簿印刷（PDF）" onclick="pdf_select(this.name);">
</td>
</tr>
</table>
<? show_navi($emp_cnt, $limit, $page); ?>
<? $arr_emp_list = show_emp_list($sel_emp, $c_yyyymm); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="1">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="check_all" value="1">
<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
</form>
</td>
</tr>
</table>
<form name="csv" method="post" target="download">
<!--<form name="csv" method="get" target="_blank">-->
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id_list" value="">
<input type="hidden" name="yyyymm" value="<? echo($c_yyyymm); ?>">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="csv_layout_id" value="<? echo($layout_id); ?>">
<input type="hidden" name="srch_id" value="">
<input type="hidden" name="srch_name" value="">
<input type="hidden" name="cls" value="">
<input type="hidden" name="atrb" value="">
<input type="hidden" name="dept" value="">
<input type="hidden" name="duty_form_jokin" value="">
<input type="hidden" name="duty_form_hijokin" value="">
<input type="hidden" name="shift_group_id" value="">
<input type="hidden" name="select_start_yr" value="">
<input type="hidden" name="select_start_mon" value="">
<input type="hidden" name="select_start_day" value="">
<input type="hidden" name="select_end_yr" value="">
<input type="hidden" name="select_end_mon" value="">
<input type="hidden" name="select_end_day" value="">
<input type="hidden" name="sortkey1" value="">
<input type="hidden" name="sortkey2" value="">
<input type="hidden" name="sortkey3" value="">
<input type="hidden" name="sortkey4" value="">
<input type="hidden" name="sortkey5" value="">
<input type="hidden" name="default_start_date" value="">
<input type="hidden" name="default_end_date" value="">
<input type="hidden" name="pdf_total_print_flg" value="">
<input type="hidden" name="pdf_count_print_flg" value="">
<input type="hidden" name="pdf_mode" value="">
<input type="hidden" name="menuname" value="">
<input type="hidden" name="sus_flg" value="">
<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
<input type="hidden" name="tmcd_group_id" value="">

</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<? } ?>
</body>
<? pg_close($con); ?>
</html>
<?
// ページ切り替えリンクを表示
function show_navi($num, $limit, $page) {
	echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"><br>\n");

//todo
    global $session, $url_srch_name, $cls, $atrb, $dept, $srch_id;

	$total_page = ceil($num/$limit);
	$check_next = $num % $limit;
	$check_last = $total_page - 1;

	if ($total_page >= 2) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
  		echo("<tr>\n");
		echo("<td align=\"center\">\n");
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("ページ");
		echo("&nbsp;");
		echo("</font>");

		if ($page > 0) {
			$back = $page - 1;
//todo
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$back&srch_flg=1&srch_id=$srch_id\">←</a></font>");
		}

		for ($i = 0; $i < $total_page; $i++) {
			$j = $i + 1;
			if ($page != $i) {
                echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> <a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$i&srch_flg=1&srch_id=$srch_id\">$j</a> </font>");
			} else {
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("［{$j}］");
				echo("</font>");
			}
		}

		if ($check_last != $page) {
			$next = $page + 1;
//todo
            echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$next&srch_flg=1&srch_id=$srch_id\">→</a></font>");
		}

		echo("</td>\n");
  		echo("</tr>\n");
		echo("</table>\n");
	}
}

// 職員一覧を表示
function show_emp_list($sel, $c_yyyymm) {
	if (!$sel) {
		return;
	}

    global $session, $url_srch_name, $cls, $atrb, $dept, $page, $emp_id, $check_all, $group_id, $shift_group_id, $csv_layout_id, $duty_form_jokin, $duty_form_hijokin, $srch_id, $sus_flg, $mmode;
	$arr_emp_list = array();
	
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職員ID</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職員氏名</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">出勤予定修正</font></td>\n");
	if ($mmode != "usr") {
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">タイムカード修正</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">タイムカードA4横</font></td>\n");
	}
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">出勤簿</font></td>\n"); //出勤簿の列追加 20130129
// 	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務表印刷</font></td>\n"); //20130129
    // opt以下のphpを表示するかどうか
    $opt_display_flag = file_exists("opt/flag");
    // ----- メディアテック Start -----
    if ( phpversion() >= "5.1"  && $opt_display_flag && $mmode != "usr"){
        require_once './kintai/common/master_util.php';
        if(MasterUtil::get_kintai_ptn() != 0){
            echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">出勤簿(超勤区分)</font></td>\n");
            echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">超勤区分内訳</font></td>\n");
        }
    }
    //echo("<td width=\"40%\">\n");
    echo("<td>\n");
    // ----- メディアテック End -----
	echo("<select name=\"_action\" onchange=\"setYmDisplay();\">\n");
	echo("<option value=\"CALC\">勤務時間集計\n");
	echo("<option value=\"CSV1\">打刻CSV出力\n");
	echo("<option value=\"CSV2\">月集計CSV出力\n");
//	echo("<option value=\"PRINT\">印刷(pxdoc)\n");
//	echo("<option value=\"CSV_KAI\">実績CSV出力(快決)\n");
	echo("</select>");
	echo("<select name=\"_yyyymm\" style=\"display:none;\">\n");
	for ($y = date("Y"); $y >= 2004; $y--) {
		for ($m = 12; $m >= 1; $m--) {
			$mm = sprintf("%02d", $m);
			echo("<option value=\"$y$mm\"");
			if ("$y$mm" == $c_yyyymm) {
				echo(" selected");
			}
			echo(">{$y}年{$m}月度\n");
		}
	}
	echo("</select>");
	echo("<input type=\"button\" value=\"実行\" onclick=\"doAction();\">\n");
	echo("<input type=\"button\" id=\"checkall\" name=\"checkall\" value=\"全てOFF\" onclick=\"checkAll();\">\n");
	echo("</td>\n");
	echo("</tr>\n");

	while ($row = pg_fetch_array($sel)) {
		$tmp_id = $row["emp_id"];
		$tmp_personal_id = $row["emp_personal_id"];
		$tmp_name = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
		
		// 打刻チェック 20130605 emp_class, emp_attribute, emp_dept
		$tmp_class = $row["emp_class"];
		$tmp_attribute = $row["emp_attribute"];
		$tmp_dept = $row["emp_dept"];
		$buf = $tmp_id . "," . $tmp_personal_id . "," . $tmp_name . "," . $tmp_class . "," . $tmp_attribute . "," . $tmp_dept;
		$arr_emp_list[$tmp_id] = $buf;
		
		echo("<tr height=\"22\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_personal_id</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_name</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"work_admin_atdbk.php?session=$session&emp_id=$tmp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg&mmode=$mmode\">出勤予定修正</a></font></td>\n");
		if ($mmode != "usr") {
	        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"work_admin_timecard.php?session=$session&emp_id=$tmp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg\">タイムカード修正</a></font></td>\n");
	        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"work_admin_timecard_a4.php?session=$session&emp_id=$tmp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg\">タイムカードA4横</a></font></td>\n");
        }
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"work_admin_timecard_shift.php?session=$session&emp_id=$tmp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg&shift_from_flg=t&mmode=$mmode\">出勤簿</a></font></td>\n");
        if ( phpversion() >= "5.1" && $opt_display_flag && $mmode != "usr"){
            // ----- メディアテック Start -----
            require_once './kintai/common/master_util.php';
            if(MasterUtil::get_kintai_ptn() != 0){
                echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"kintai_ovtmlist_adm.php?session=$session&emp_id=$tmp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg\">出勤簿(超勤区分)</a></font></td>\n");
                echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"kintai_ovtmlist_detail_adm.php?session=$session&emp_id=$tmp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg\">超勤区分内訳</a></font></td>\n");
            }
            // ----- メディアテック End -----
        }
        // 		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"shift_list('$tmp_id');\">勤務表印刷</a></font></td>\n");
		echo("<td><input type=\"checkbox\" name=\"emp_id[]\" value=\"$tmp_id\"");
		if (in_array($tmp_id, $emp_id) || $check_all == "1") {
			echo(" checked");
		}
		echo("></td>\n");
		echo("</tr>\n");
	}

	echo("</table>\n");
	return $arr_emp_list;
}

//レイアウト名称取得
function get_layout_name($con, $fname, $layout_id) {

	$arr_layout = array();

	$sql = "select * from timecard_csv_layout_mst ";
	if ($layout_id == "") {
		$cond = " order by layout_id";
	} else {
		$cond = "where layout_id = '$layout_id'";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);

	for ($i=0; $i<$num; $i++) {
		$id = pg_result($sel,$i,"layout_id");
		$arr_layout["$id"]["name"] = pg_result($sel,$i,"layout_name");
	}
	return $arr_layout;
}

function get_tantou_branches($con, $fname, $emp_id) {
	$timecard_tantou_branches = array();
	$sql = "select * from timecard_tantou_branches ";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rows = pg_fetch_all($sel);
	foreach ($rows as $row) {
	    if ($row["class_id"]) $timecard_tantou_branches[]= array(
	        "class_id"=>$row["class_id"], "atrb_id"=>$row["atrb_id"], "dept_id"=>$row["dept_id"], "room_id"=>$row["room_id"]
	    );
	}
	return $timecard_tantou_branches;
}

function get_csv_closing_info($con, $fname) {
	$arr_closing = array();
	$sql = "select distinct csv_layout_id, closing from empcond ";
	$cond = "where closing != '' and  exists (select * from authmst where authmst.emp_id = empcond.emp_id and authmst.emp_del_flg = 'f') group by csv_layout_id, closing order by csv_layout_id, closing";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rows = pg_fetch_all($sel);
	foreach ($rows as $row) {
		$arr_closing[$row["csv_layout_id"]] = $row["closing"];
	}
	return $arr_closing;
}

?>
