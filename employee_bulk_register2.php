<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 一括登録2</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("employee_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 文字コードのデフォルトはShift_JIS
if ($encoding == "") {$encoding = "1";}

// データベースに接続
$con = connect2db($fname);

// サイトIDを取得
$sql = "select site_id from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");

// 組織情報を取得
$arr_class_name = get_class_name_array($con, $fname);
$class_cnt = $arr_class_name[4];


// グループ一覧を配列に格納
$sql = "select group_id, group_nm from authgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
	$groups[$row["group_id"]] = $row["group_nm"];
}
// system_config
$conf = new Cmx_SystemConfig();

//権限グループの初期設定を取得する
$group_id = $conf->get("default_set.group_id");


// 表示グループの一覧を配列に格納する
$sql = "select group_id, group_nm from dispgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$disp_groups = array();
while ($row = pg_fetch_array($sel)) {
	$disp_groups[$row["group_id"]] = $row["group_nm"];
}

//表示グループの初期設定を取得する
$disp_group_id = $conf->get("default_set.disp_group_id");


// メニューグループの一覧を配列に格納する
$sql = "select group_id, group_nm from menugroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu_groups = array();
while ($row = pg_fetch_array($sel)) {
	$menu_groups[$row["group_id"]] = $row["group_nm"];
}

//表示グループの初期設定を取得する
$menu_group_id = $conf->get("default_set.menu_group_id");



?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	return true;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? show_menu2(); ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="employee_bulk_insert2.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>


<tr height="22">
	<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></td>
	<td colspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="auth_group_id">
<option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select></font>
	</td>
</tr>


<tr height="22">
	<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></td>
	<td colspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="disp_group_id">
<option value="">　　　　　</option>
<?
foreach ($disp_groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $disp_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select></font>
	</td>
</tr>


<tr height="22">
	<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></td>
	<td colspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="menu_group_id">
<option value="">　　　　　</option>
<?
foreach ($menu_groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $menu_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select></font>
	</td>
</tr>

</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? if ($result != "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<? } ?>
<? if ($result == "f") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">登録処理が実行できませんでした。</font></td>
<? } else if ($result == "t") {?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。</font></td>
<? } ?>
<? if ($result != "") { ?>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<dl style="margin-top:0;">
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>注意</b></font></dt>
<dd>
<ul style="margin:0;">
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">データは職員DBに直接登録されます。職員登録用DBには登録されません。</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「崎」「高」などの異体字は「・」で置換されます。一般的な字形をお使いください。</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員1人あたり数秒かかります。</font></li>
</ul>
</dd>
</dl>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></dt>
<dd><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記項目をカンマで区切り、レコードごとに改行します。生年月日・入職日<? if ($class_cnt == "4") {echo("・{$arr_class_name[3]}");} ?>以外は必須です。</font></dd>
<dd>
<ul>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">苗字（漢字）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（漢字）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">苗字（かな）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（かな）</font></li>
<? if ($site_id == "") { ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインID……使用可能な文字：英数小文字・アンダーバー・ハイフン（いずれも半角。アルファベットが含まれない場合、ウェブメールの利用不可）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード……使用可能な文字：英数大文字小文字・アンダーバー・ハイフン（いずれも半角）</font></li>
<? } else { ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインID……使用可能な文字：英数大文字小文字・アンダーバー・ハイフン（いずれも半角）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード……同上</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メールID……使用可能な文字：英数小文字・アンダーバー・ハイフン（いずれも半角）</font></li>
<? } ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別……数字1桁（1：男性、2：女性）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日……YYYYMMDD形式</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入職日……同上</font></li>

<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?></font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?></font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?></font></li>
<? if ($class_cnt == "4") { ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?></font></li>
<? } ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></li>
</ul>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">例</font></dt>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">0000000001,山田,太郎,やまだ,たろう,yamada,yama01,<? if ($site_id != "") {echo("t_yamada,");} ?>1,19550505,19880808,○○<? echo($arr_class_name[0]); ?>,○○<? echo($arr_class_name[1]); ?>,○○<? echo($arr_class_name[2]); ?>,<? if ($class_cnt == "4") {echo("○○{$arr_class_name[3]},");} ?>なし,医師</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">0000000002,鈴木,花子,すずき,はなこ,suzuki,suzu02,<? if ($site_id != "") {echo("h_suzuki,");} ?>2,,,××<? echo($arr_class_name[0]); ?>,××<? echo($arr_class_name[1]); ?>,××<? echo($arr_class_name[2]); ?>,<? if ($class_cnt == "4") {echo(",");} ?>看護師長,看護師</font></dd>
</dl>
</dd>
</dl>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
