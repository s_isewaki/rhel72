<?
require("about_session.php");
require("about_authority.php");
require("cl_workflow_lib_list.ini");
require("referer_common.ini");
require("cl_workflow_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}
$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | テンプレート一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteWorkflow() {
	if (document.wkfw.elements['del_lib[]'] == undefined) {
		alert('削除可能なデータが存在しません。');
		return;
	}

	var checked = false;

	if (document.wkfw.elements['del_lib[]'] != undefined) {
		if (document.wkfw.elements['del_lib[]'].length == undefined) {
			if (document.wkfw.elements['del_lib[]'].checked) {
				checked = true;
			}
		} else {
			for (var i = 0, j = document.wkfw.elements['del_lib[]'].length; i < j; i++) {
				if (document.wkfw.elements['del_lib[]'][i].checked) {
					checked = true;
					break;
				}
			}
		}
	}

	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.wkfw.submit();
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#A0D25A solid 1px;}
.list td {border:#A0D25A solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cl_workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cl_workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cl_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="ladder_menu.php?session=<? echo($session); ?>"><b><? echo($cl_title); ?></b></a> &gt; <a href="cl_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="ladder_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
show_wkfw_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right"><input type="button" value="削除" onclick="deleteWorkflow();"></td>
</tr>
</table>

<form name="wkfw" action="cl_workflow_lib_delete.php" method="post">
<? show_workflow_lib_list($con,$session,$fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
