<?/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID

*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("summary_common.ini"); ?>
<? require("label_by_profile_type.ini"); ?>
<? require_once("get_menu_label.ini"); ?>
<?

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


// 入院履歴情報を取得
$sql = "select 
	i.inpt_in_dt, 
	i.inpt_in_tm, 
	i.inpt_out_dt, 
	i.inpt_out_tm, 
	i.inpt_out_res_dt, 
	i.inpt_out_res_tm, 
	i.inpt_disease, 
	(select s.sect_nm from sectmst s where s.enti_id = i.inpt_enti_id and s.sect_id = i.inpt_sect_id and s.sect_del_flg = 'f') as sect_nm, 
	(select d.dr_nm from drmst d where d.enti_id = i.inpt_enti_id and d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id and d.dr_del_flg = 'f') as dr_nm 
from 
	inpthist i 
where 
	i.ptif_id = '$pt_id' 
union 
select 
	i.inpt_in_dt, 
	i.inpt_in_tm, 
	i.inpt_out_dt, 
	i.inpt_out_tm, 
	i.inpt_out_res_dt, 
	i.inpt_out_res_tm, 
	i.inpt_disease, 
	(select s.sect_nm from sectmst s where s.enti_id = i.inpt_enti_id and s.sect_id = i.inpt_sect_id and s.sect_del_flg = 'f') as sect_nm, 
	(select d.dr_nm from drmst d where d.enti_id = i.inpt_enti_id and d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id and d.dr_del_flg = 'f') as dr_nm 
from 
	inptmst i 
where 
	i.ptif_id = '$pt_id' 
and 
	i.inpt_res_flg = 'f'";
$cond = "order by inpt_in_dt desc, inpt_in_tm desc";

$sel_inpthist = select_from_table($con, $sql, $cond, $fname);
if ($sel_inpthist == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}


//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み日付取得
// Name      :  format_date
// Arguments :  dt - 日付（「YYYYMMDD」形式）
// Return    :  日付（「YYYY/MM/DD」形式）
//------------------------------------------------------------------------------
function format_date($dt) {
	$pattern = '/^(\d{4})(\d{2})(\d{2})$/';
	$replacement = '$1/$2/$3';
	return preg_replace($pattern, $replacement, $dt);
}

//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み時刻取得
// Name      :  format_time
// Arguments :  tm - 時刻（「HHMM」形式）
// Return    :  時刻（「HH:MM」形式）
//------------------------------------------------------------------------------
function format_time($tm) {
	$pattern = '/^(\d{2})(\d{2})$/';
	$replacement = '$1:$2';
	return preg_replace($pattern, $replacement, $tm);
}

//==========================================================================================
//HTML出力
//==========================================================================================
//==============================
//スクリプト/スタイルシート/BODY宣言 出力
//==============================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜入院情報選択</title>

<script type="text/javascript">
	function dateSelected(date_y, date_m, date_d){
		if (!window.opener) return;
		if (!window.opener.callbackNyuinRireki) return;
		window.opener.callbackNyuinRireki(date_y, date_m, date_d);
		self.close();
	}
</script>

</head>
<body>


<div style="padding:4px">

<? //ヘッダー 出力 ?>
<? summary_common_show_dialog_header("入院情報選択"); ?>

</div>

<div style="padding:4px">

<table class="listp_table" cellspacing="1">
<tr>
<th width="140">入院日時</th>
<th width="140">退院日時</th>
<th width="140">退院予定日時</th>
<th width="140">診療科</th>
<th width="140">プロブレム</th>
<th>担当医</th>
</tr>

<?
while ($row = pg_fetch_array($sel_inpthist)) {
	$in_dt = format_date($row["inpt_in_dt"]);
	$in_tm = format_time($row["inpt_in_tm"]);
	$in_dttm = "$in_dt $in_tm";
	$out_dt = format_date($row["inpt_out_dt"]);
	$out_tm = format_time($row["inpt_out_tm"]);
	$out_dttm = "$out_dt $out_tm";
	$out_res_dt = format_date($row["inpt_out_res_dt"]);
	$out_res_tm = format_time($row["inpt_out_res_tm"]);
	$out_res_dttm = "$out_res_dt $out_res_tm";
	$sect_nm = $row["sect_nm"];
	$disease = $row["inpt_disease"];
	$dr_nm = $row["dr_nm"];

	$date_y = (int)substr($row["inpt_in_dt"], 0, 4);
	$date_m = (int)substr($row["inpt_in_dt"], 4, 2);
	$date_d = (int)substr($row["inpt_in_dt"], 6, 4);
?>
<tr>
<td><a href="javascript:void(0);" onClick="dateSelected('<?=$date_y?>', '<?=$date_m?>', '<?=$date_d?>');"><? echo $in_dttm; ?></a></td>
<td><? echo $out_dttm; ?></td>
<td><? echo $out_res_dttm; ?></td>
<td><? echo $sect_nm; ?></td>
<td><? echo $disease; ?></td>
<td><? echo $dr_nm; ?></td>
</tr>
<?
}
?>
</table>

</div>

</body>
</html>
<? pg_close($con); ?>
