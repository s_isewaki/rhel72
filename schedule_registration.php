<?php
require_once("show_select_values.ini");
require_once("about_comedix.php");
require_once("show_schedule_common.ini");
require_once("show_class_name.ini");
require_once("holiday.php");
require_once("facility_common.ini");
require_once("show_facility.ini");
require_once("mygroup_common.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 初期表示時の設定
if ($back != "t") {
    $year = date("Y", $date);
    $month = date("m", $date);
    $day = date("d", $date);

    if ($time == "") {
        $hr = "09";
        $min = "00";
    }
    else {
        $hr = substr($time, 0, 2);
        $min = substr($time, 3, 2);
    }
    
    $conf = new Cmx_SystemConfig();
    $span_default = $conf->get('schedule.span_default'); 

    if ($schd_dur_hrs == "") {

        // 時間指定の初期値を考慮して表示する終了時刻を計算 20141030TN
        if ($span_default != "") {
            $span_default_h = intval($span_default / 60);
            $span_default_m = intval($span_default % 60);
            $schd_dur_min = $min + $span_default_m;
            if ($min == 30 && ($span_default == 30 || $span_default == 90 || $span_default == 150)) {  // 分が60に達することによる時の繰り上げ処理
                $schd_dur_hrs = sprintf("%02d", intval($hr) + $span_default_h + 1);
            } 
            else {
                $schd_dur_hrs = sprintf("%02d", intval($hr) + $span_default_h);
            }
        }
        else {
            $schd_dur_hrs = sprintf("%02d", intval($hr) + 1);
            $schd_dur_min = $min;
        }
        
        if ($schd_dur_hrs >= "24") {
            $schd_dur_hrs = "24";
            $schd_dur_min = "00";
        }
    }

    if ($schd_imprt == "") {
        $schd_imprt = "2";
    }

    if ($repeat_type == "") {
        $repeat_type = "2";
    }
    if ($rpt_end_year == "") {
        $rpt_end_year = $year;
    }
    if ($rpt_end_month == "") {
        $rpt_end_month = $month;
    }
    if ($rpt_end_day == "") {
        $rpt_end_day = $day;
    }
}
else {
    $year = $schd_start_yrs;
    $month = $schd_start_mth;
    $day = $schd_start_day;
    $hr = $schd_start_hrs;
    $min = $schd_start_min;
}

$date = mktime(0, 0, 0, $month, $day, $year);

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_reservable_categories($con, $emp_id, $fname);

// オプション設定情報を取得
$sql = "select calendar_start1, schd_pub_limit from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");
if ($back != "t") {
    $pub_limit = pg_fetch_result($sel, 0, "schd_pub_limit");
}

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

if ($schd_emp_id == "") {
    $schd_emp_id = $emp_id;
}

// 初期表示時は当該職員を対象者に
if ($back != "t") {
    $target_id_list = $schd_emp_id;
}

// 対象職員情報を配列に格納
$arr_target = array();
if ($target_id_list != "") {
    $arr_target_id = split(",", $target_id_list);
    for ($i = 0; $i < count($arr_target_id); $i++) {
        $tmp_emp_id = $arr_target_id[$i];
        $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        }
        $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
        if ($tmp_emp_id == $schd_emp_id) {
            $schd_emp_name = $tmp_emp_name;
        }
        array_push($arr_target, array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
    }
}

if ($calendar == "") {
    $calendar = "off";
}
if ($repeat == "") {
    $repeat = "off";
}

$employees = array();
global $employees;

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $emp_id);

// 公開範囲用のグループ一覧を作成
$pub_limit_group = array();
$sql = "select m.group_id, m.group_nm from comgroupmst m";
$cond = "where m.public_flg or exists (select * from comgroup g where g.group_id = m.group_id and g.emp_id = '$emp_id') order by m.group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $pub_limit_group["c" . $row["group_id"]] = $row["group_nm"];
}

if ($back != "t" && $pub_limit != "") {
    switch (substr($pub_limit, 0, 1)) {
        case "d":
            $pub_limit_dept = substr($pub_limit, 1);
            $pub_limit = "d";
            break;
        case "c":
            if (isset($pub_limit_group[$pub_limit])) {
                $pub_limit_group_id = $pub_limit;
                $pub_limit = "g";
            }
            break;
    }
}

// アラーム機能
$conf = new Cmx_SystemConfig();
$schedule_alarm = $conf->get('schedule.alarm');
$schedule_alarm_start = $conf->get('schedule.alarm.start');
$schedule_alarm_end = $conf->get('schedule.alarm.end');

$salarm_start = '';
if ($schedule_alarm_start) {
    $salarm_start_h = intval($schedule_alarm_start / 60);
    $salarm_start_m = $schedule_alarm_start % 60;
    if ($salarm_start_h) {
        $salarm_start .= $salarm_start_h . '時間';
    }
    if ($salarm_start_m) {
        $salarm_start .= $salarm_start_m . '分';
    }
    $salarm_start .= '前';
}

$salarm_end = '';
if ($schedule_alarm_end) {
    $salarm_end_h = intval($schedule_alarm_end / 60);
    $salarm_end_m = $schedule_alarm_end % 60;
    if ($salarm_end_h) {
        $salarm_end .= $salarm_end_h . '時間';
    }
    if ($salarm_end_m) {
        $salarm_end .= $salarm_end_m . '分';
    }
    $salarm_end .= '前';
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 予定の追加</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<?
require_once("schedule_registration_javascript.php");
insert_javascript();
?>
<script language="javascript">
function initPage() {
    setDayOptions('<? echo($day); ?>');

<?
if ($datelist != "") {
    $tmp_arr_date = split(",", $datelist);
    foreach ($tmp_arr_date as $tmp_date) {
        echo("\tdateOnClick('$tmp_date')\n");
    }
}
?>
    for (var i = 0, j = document.mainform.cate_id.options.length; i < j; i++) {
        if (document.mainform.cate_id.options[i].value == '<? echo($cate_id); ?>') {
            document.mainform.cate_id.options[i].selected = true;
        }
    }
    cateOnChange('<? echo($facility_id); ?>');

    setRepeatDisplay('<? echo($repeat); ?>');
    setTimeDisabled();
    setCalendarDisplay('<? echo($calendar); ?>');

    //登録対象者を設定する。
    update_target_html();
    setMailDisabled();

    groupOnChange();
}

function setDayOptions(selectedDay) {
    var dayPulldown = document.mainform.schd_start_day;
    if (!selectedDay) {
        selectedDay = dayPulldown.value;
    }
    deleteAllOptions(dayPulldown);

    var year = parseInt(document.mainform.schd_start_yrs.value, 10);
    var month = parseInt(document.mainform.schd_start_mth.value, 10);
    var daysInMonth = new Date(year, month, 0).getDate();

    for (var d = 1; d <= daysInMonth; d++) {
        var tmpDayValue = d.toString();
        if (tmpDayValue.length == 1) {
            tmpDayValue = '0'.concat(tmpDayValue);
        }

        var tmpDayText = d.toString();

        var tmpDate = new Date(year, month - 1, d);
        var tmpWeekDay = tmpDate.getDay();
        switch (tmpWeekDay) {
        case 0:
            tmpDayText = tmpDayText.concat('（日）');
            break;
        case 1:
            tmpDayText = tmpDayText.concat('（月）');
            break;
        case 2:
            tmpDayText = tmpDayText.concat('（火）');
            break;
        case 3:
            tmpDayText = tmpDayText.concat('（水）');
            break;
        case 4:
            tmpDayText = tmpDayText.concat('（木）');
            break;
        case 5:
            tmpDayText = tmpDayText.concat('（金）');
            break;
        case 6:
            tmpDayText = tmpDayText.concat('（土）');
            break;
        }

        addOption(dayPulldown, tmpDayValue, tmpDayText);
    }

    while (parseInt(selectedDay, 10) > daysInMonth) {
        selectedDay = (parseInt(selectedDay, 10) - 1).toString();
    }
    dayPulldown.value = selectedDay;
}

function cateOnChange(facility_id) {

    // 施設・設備セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.facility_id);

    // 施設・設備セレクトボックスのオプションを作成
    var category_id = document.mainform.cate_id.value;
<?
foreach ($categories as $tmp_category_id => $tmp_category) {
    echo("\tif (category_id == '$tmp_category_id') {\n");
    foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
        echo("\t\taddOption(document.mainform.facility_id, '$tmp_facility_id', '{$tmp_facility["name"]}', facility_id);\n");
    }
    echo("\t}\n");
}
?>

    // オプションが1件も作成されなかった場合
    var opt_cnt = document.mainform.facility_id.options.length;
    if (opt_cnt == 0) {
        addOption(document.mainform.facility_id, '0', '----------', facility_id);
    }
}


function setTargetList() {
    document.mainform.target_id_list.value = '';
    for (var i = 0, j = document.mainform.target.options.length; i < j; i++) {
        if (i > 0) {
            document.mainform.target_id_list.value += ',';
        }
        document.mainform.target_id_list.value += document.mainform.target.options[i].value;
    }
    setMailDisabled();
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function getSelectedValue(sel) {
    return sel.options[sel.selectedIndex].value;
}

function setRepeatDisplay(repeat) {
    if (repeat == 'on') {
        document.getElementById('repeat_off_row').style.display = 'none';
        document.getElementById('repeat_on_row').style.display = '';
        document.mainform.repeat.value = 'on';
    } else {
        document.getElementById('repeat_on_row').style.display = 'none';
        document.getElementById('repeat_off_row').style.display = '';
        document.mainform.repeat.value = 'off';
    }
}

function setTimeDisabled() {
    var disabled = document.mainform.timeless.checked;
    document.mainform.schd_start_hrs.disabled = disabled;
    document.mainform.schd_start_min.disabled = disabled;
    document.mainform.schd_dur_hrs.disabled = disabled;
    document.mainform.schd_dur_min.disabled = disabled;
    document.mainform.time_option.disabled = disabled;
}

function setCalendarDisplay(calendar) {
    if (calendar == 'on') {
        document.getElementById('date_row').style.display = 'none';
        document.getElementById('calendar_row').style.display = '';
        document.mainform.calendar.value = 'on';
        document.getElementById('repeat_off_row').style.display = 'none';
        document.getElementById('repeat_on_row').style.display = 'none';
    } else {
        document.getElementById('date_row').style.display = '';
        document.getElementById('calendar_row').style.display = 'none';
        document.mainform.calendar.value = 'off';
        setRepeatDisplay(document.mainform.repeat.value);
    }
}

function setTime() {
    var span_minutes = document.mainform.time_option.value;
    if (span_minutes == '') {
        return;
    }

    var start_hour = parseInt(document.mainform.schd_start_hrs.value, 10);
    var start_minute = parseInt(document.mainform.schd_start_min.value, 10);
    var start_timestamp = start_hour * 60 + start_minute;

    var end_timestamp = start_timestamp + parseInt(span_minutes, 10);
    while (end_timestamp > 24 * 60) {
        end_timestamp -= 30;
    }

    var end_hour = Math.floor(end_timestamp / 60);
    var end_minute = end_timestamp - end_hour * 60;

    document.mainform.schd_dur_hrs.value = lpad(end_hour);
    document.mainform.schd_dur_min.value = lpad(end_minute);
}

function lpad(num) {
    var str = num.toString();
    if (str.length == 1) {
        str = '0'.concat(str);
    }
    return str;
}

function dateOnClick(ymd) {
    var add;
    var datelist = document.mainform.datelist.value;
    if (datelist.length == 0) {
        datelist += ymd;
        add = true;
    } else if (datelist.indexOf(ymd) == -1) {
        datelist += ',' + ymd;
        add = true;
    } else {
        if (datelist.length == 8) {
            datelist = '';
        } else {
            datelist = datelist.replace(',' + ymd, '');
        }
        add = false;
    }
    document.mainform.datelist.value = datelist;

    var cell = document.getElementById('id' + ymd);
    if (add) {
        cell.style.backgroundColor = '#ccffcc';
    } else {
        cell.style.backgroundColor = '';
    }
}

function setMailDisabled() {
    var disabled = (
        document.mainform.target_id_list.value == '<? echo($emp_id); ?>' ||
        document.mainform.target_id_list.value == ''
    );
    document.mainform.sendmail.disabled = disabled;
}

function checkEndMinute() {
    if (document.mainform.schd_dur_hrs.value == '24' &&
        document.mainform.schd_dur_min.value != '00') {
        document.mainform.schd_dur_min.value = '00';
        alert('終了時刻が24時の場合には00分しか選べません。');
    }
}

var childwin = null;
function openEmployeeList() {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './schedule_address_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$emp_id?>';
    url += '&mode=1';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
    $script = "m_target_list['ID'] = new Array(";
    $is_first = true;
    foreach($arr_target as $row)
    {
        if($is_first)
        {
            $is_first = false;
        }
        else
        {
            $script .= ",";
        }
        $emp_id = $row["id"];
        $emp_name = $row["name"];
        $script .= "new user_info('$emp_id','$emp_name')";
    }
    $script .= ");\n";
    print $script;
?>

function groupOnChange() {
    var group_id = getSelectedValue(document.mainform.mygroup);

    // 職員一覧セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.emplist);

    // 職員一覧セレクトボックスのオプションを作成
<? foreach ($employees as $tmp_group_id => $arr) { ?>
    if (group_id == '<? echo $tmp_group_id; ?>') {
    <? foreach($arr as $tmp_emp) { ?>
        addOption(document.mainform.emplist, '<? echo $tmp_emp["emp_id"]; ?>', '<? echo $tmp_emp["name"]; ?>');
    <? } ?>
    }
<? } ?>
}

function addEmp() {
    var emp_id_str = "";
    var emp_name_str = "";
    var first_flg = true;
    for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
        if (document.mainform.emplist.options[i].selected) {
            var emp_id = document.mainform.emplist.options[i].value;
            var emp_name = document.mainform.emplist.options[i].text;
            if (first_flg == true) {
                first_flg = false;
            } else {
                emp_id_str += ", ";
                emp_name_str += ", ";
            }
            emp_id_str += emp_id;
            emp_name_str += emp_name;
        }
    }
    if (emp_id_str != "") {
        set_wm_counter(emp_id_str);
        add_target_list(emp_id_str, emp_name_str);
    }
}

function registerSchedule() {
<? if (is_array($log)) { ?>
    if (!confirm('新たな予定が追加されますが、よろしいですか？')) {
        return;
    }
<? } ?>
    closeEmployeeList();
    document.mainform.submit();
}
</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<form name="mainform" action="schedule_registration_checker.php?session=<? echo($session); ?>&date=<? echo($date); ?>&flg=true" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>予定の追加</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();closeEmployeeList();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellpadding="2" cellspacing="0">
<tr height="22">
<td align="right"><input type="button" value="登録" onclick="registerSchedule();"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input type="text" name="schd_title" value="<? eh($schd_title); ?>" maxlength="100" size="50" style="ime-mode: active;"></td>
<td bgcolor="#f6f9ff" width="110" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラインマーカー</font></td>
<td>
<select name="marker">
<option value="0" style="background-color:white;color:black;"<? if ($marker == 0) {echo(" selected");} ?>>なし
<option value="1" style="background-color:red;color:white;"<? if ($marker == 1) {echo(" selected");} ?>>赤
<option value="2" style="background-color:aqua;color:blue;"<? if ($marker == 2) {echo(" selected");} ?>>青
<option value="3" style="background-color:yellow;color:blue;"<? if ($marker == 3) {echo(" selected");} ?>>黄
<option value="4" style="background-color:lime;color:blue;"<? if ($marker == 4) {echo(" selected");} ?>>緑
<option value="5" style="background-color:fuchsia;color:white;"<? if ($marker == 5) {echo(" selected");} ?>>ピンク
</select>
</td>
</tr>
<tr>
<td bgcolor="#f6f9ff" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先</font></td>
<td colspan="3"><select name="place_id"><option value=""></option><? show_schedule_place_selected($con, $place_id, $fname); ?><option value="0">その他</option></select>&nbsp;<input type="text" name="schd_plc" value="<? echo($schd_plc); ?>" maxlength="100" size="35" style="ime-mode: active;"></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定の公開</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schd_imprt" value="1"<? if ($schd_imprt == "1") {echo(" checked");} ?>>私用 (自分以外に予定は公開されません)
<br>
<input type="radio" name="schd_imprt" value="2"<? if ($schd_imprt == "2") {echo(" checked");} ?>>公開 （<?
echo("<input type=\"radio\" name=\"pub_limit\" value=\"\"");
if ($pub_limit == "") echo(" checked");
echo(" onclick=\"this.form.schd_imprt[1].checked = true;\">全て\n");

echo("<input type=\"radio\" name=\"pub_limit\" value=\"d\"");
if ($pub_limit == "d") echo(" checked");
echo(" onclick=\"this.form.schd_imprt[1].checked = true;\">所属 ");
echo("<select name=\"pub_limit_dept\" onchange=\"this.form.schd_imprt[1].checked = true; this.form.pub_limit[1].checked = true;\">\n");
    echo("<option value=\"1\"");
    if ($pub_limit_dept == "1") echo(" selected");
    echo(">" . $arr_class_name["class_nm"] . "\n");

    echo("<option value=\"2\"");
    if ($pub_limit_dept == "2") echo(" selected");
    echo(">" . $arr_class_name["atrb_nm"] . "\n");

    echo("<option value=\"3\"");
    if ($pub_limit_dept == "3") echo(" selected");
    echo(">" . $arr_class_name["dept_nm"] . "\n");

    if ($arr_class_name["class_cnt"] == 4) {
        echo("<option value=\"4\"");
        if ($pub_limit_dept == "4") echo(" selected");
        echo(">" . $arr_class_name["room_nm"] . "\n");
    }
echo("</select>\n");

if (count($pub_limit_group) > 0) {
    echo("<input type=\"radio\" name=\"pub_limit\" value=\"g\"");
    if ($pub_limit == "g") echo(" checked");
    echo(" onclick=\"this.form.schd_imprt[1].checked = true;\">");
    echo("<select name=\"pub_limit_group_id\" onchange=\"this.form.schd_imprt[1].checked = true; this.form.pub_limit[2].checked = true;\">\n");
    foreach ($pub_limit_group as $tmp_pub_limit_group_id => $tmp_pub_limit_group_nm) {
        echo("<option value=\"$tmp_pub_limit_group_id\"");
        if ($pub_limit_group_id == $tmp_pub_limit_group_id) echo(" selected");
        echo(">" . $tmp_pub_limit_group_nm . "\n");
    }
    echo("</select>\n");
}
?>）
<br>
<input type="radio" name="schd_imprt" value="3"<? if ($schd_imprt == "3") {echo(" checked");} ?>>自分以外には「予定あり」とのみ表示
</font></td>
</tr>
<tr id="date_row" style="display:none;">
<td bgcolor="#f6f9ff" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td style="border-right-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="schd_start_yrs" onchange="setDayOptions();"><? show_select_years_span(min($year, 2000), date("Y") + 3, $year); ?></select>/<select name="schd_start_mth" onchange="setDayOptions();"><? show_select_months($month); ?></select>/<select name="schd_start_day"></select></font></td>
<td colspan="2" align="right" style="border-left-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setCalendarDisplay('on');">カレンダー指定</a></font></td>
</tr>
<tr id="calendar_row" style="display:none;">
<td colspan="4">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="inner">
<tr>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 0); ?></td>
<td width="5"></td>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 1); ?></td>
<td width="5"></td>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 2); ?></td>
</tr>
<tr>
<td colspan="5" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setCalendarDisplay('off');">日付指定</a></font></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#f6f9ff" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="schd_start_hrs" onchange="setTime();"><? show_hour_options_0_23($hr, false); ?></select>：<select name="schd_start_min" onchange="setTime();"><? show_min_options_5($min, false); ?></select>〜<select name="schd_dur_hrs" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $schd_dur_hrs, false); ?></select>：<select name="schd_dur_min" onchange="checkEndMinute();"><? show_min_options_5($schd_dur_min, false); ?></select>
<img src="img/spacer.gif" alt="" width="15" height="1">
時間指定：<select name="time_option" onchange="setTime();">
<option value="">
<option value="30"<? if ($time_option == "30" || $span_default == "30") {echo(" selected");} ?>>30分
<option value="60"<? if ($time_option == "60" || $span_default == "60") {echo(" selected");} ?>>1時間
<option value="90"<? if ($time_option == "90" || $span_default == "90") {echo(" selected");} ?>>1時間30分
<option value="120"<? if ($time_option == "120" || $span_default == "120") {echo(" selected");} ?>>2時間
<option value="150"<? if ($time_option == "150" || $span_default == "150") {echo(" selected");} ?>>2時間30分
<option value="180"<? if ($time_option == "180" || $span_default == "180") {echo(" selected");} ?>>3時間
</select><br>
<input type="checkbox" name="timeless" value="t"<? if ($timeless == "t") {echo(" checked");} ?> onclick="setTimeDisabled();">指定しない
</font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_schd_type_radio($con, $schd_type, $fname); ?></font></td>
</tr>
<tr id="repeat_off_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img src="img/icon/plus.gif" alt="" width="20" height="20" style="margin-right:3px;vertical-align:middle;" onclick="setRepeatDisplay('on');">繰り返し</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="gray">（指定しない）</font></td>
</tr>
<tr id="repeat_on_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img src="img/icon/minus.gif" alt="" width="20" height="20" style="margin-right:3px;vertical-align:middle;" onclick="setRepeatDisplay('off');">繰り返し</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="repeat_type" value="2"<? if ($repeat_type == "2") {echo(" checked");} ?>><select name="rpt2_1" onchange="this.form.repeat_type[0].click();">
<option value="1"<? if ($rpt2_1 == "1") {echo(" selected");} ?>>毎
<option value="2"<? if ($rpt2_1 == "2") {echo(" selected");} ?>>隔
</select><select name="rpt2_2" onchange="this.form.repeat_type[0].click();">
<option value="1"<? if ($rpt2_2 == "1") {echo(" selected");} ?>>日
<option value="2"<? if ($rpt2_2 == "2") {echo(" selected");} ?>>週
<option value="3"<? if ($rpt2_2 == "3") {echo(" selected");} ?>>月（つき）
<option value="4"<? if ($rpt2_2 == "4") {echo(" selected");} ?>>月、水、金
<option value="5"<? if ($rpt2_2 == "5") {echo(" selected");} ?>>火、木
<option value="6"<? if ($rpt2_2 == "6") {echo(" selected");} ?>>月〜金
<option value="7"<? if ($rpt2_2 == "7") {echo(" selected");} ?>>土、日
</select><br>
<input type="radio" name="repeat_type" value="3"<? if ($repeat_type == "3") {echo(" checked");} ?>><select name="rpt3_1" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_1 == "1") {echo(" selected");} ?>>毎月
<option value="2"<? if ($rpt3_1 == "2") {echo(" selected");} ?>>毎隔月
<option value="3"<? if ($rpt3_1 == "3") {echo(" selected");} ?>>毎3ヶ月ごと
</select><select name="rpt3_2" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_2 == "1") {echo(" selected");} ?>>第1
<option value="2"<? if ($rpt3_2 == "2") {echo(" selected");} ?>>第2
<option value="3"<? if ($rpt3_2 == "3") {echo(" selected");} ?>>第3
<option value="4"<? if ($rpt3_2 == "4") {echo(" selected");} ?>>第4
<option value="5"<? if ($rpt3_2 == "5") {echo(" selected");} ?>>最終
</select><select name="rpt3_3" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_3 == "1") {echo(" selected");} ?>>日
<option value="2"<? if ($rpt3_3 == "2") {echo(" selected");} ?>>月
<option value="3"<? if ($rpt3_3 == "3") {echo(" selected");} ?>>火
<option value="4"<? if ($rpt3_3 == "4") {echo(" selected");} ?>>水
<option value="5"<? if ($rpt3_3 == "5") {echo(" selected");} ?>>木
<option value="6"<? if ($rpt3_3 == "6") {echo(" selected");} ?>>金
<option value="7"<? if ($rpt3_3 == "7") {echo(" selected");} ?>>土
</select><br>
　終了日：<select name="rpt_end_year">
<? show_select_years_span(min($rpt_end_year, 2000), date("Y") + 3, $rpt_end_year); ?>
</select>/<select name="rpt_end_month">
<? show_select_months($rpt_end_month); ?>
</select>/<select name="rpt_end_day">
<? show_select_days($rpt_end_day); ?>
</select>
</font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font>
<td colspan="3"><textarea name="schd_detail" rows="5" cols="40" style="ime-mode: active;"><? eh($schd_detail); ?></textarea><input type="hidden" name="f_evt_rtype" value="none"></td>
</tr>
<tr height="22" >
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録対象者</font><br>
<input type="button" value="職員名簿" style="margin-left:2em;width:5.5em;" onclick="openEmployeeList();"><br>
<input type="button" value="クリア" style="margin-left:2em;width:5.5em;" onclick="clear_target('<?=$schd_emp_id?>','<?=$schd_emp_name?>');"><br></td>
<td colspan="3">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area"></span>
</font>

</td>
<td>
<input type="button" value="&lt;&nbsp;追加" style="margin-left:1em;width:4.5em;" onclick="addEmp();">
</td>
<td width="10">
</td>
<td>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="1" class="non_in_list">
<tr>
<td height="45%" align="center" valign="bottom">

<select name="mygroup" onchange="groupOnChange();">
<?
 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
?>
<option value="<?=$tmp_mygroup_id?>"
<?
        if ($mygroup == $tmp_mygroup_id) {
            echo(" selected");
        }
?>
><?=$tmp_mygroup?></option>
<?
}
?>
</selelct>
</td></tr>
<tr><td>
<select name="emplist" size="10" multiple style="width:150px;">
</select>

</td>
</tr>
<tr>
<td height="45%" valign="top"><input type="button" value="全て選択" onclick="selectAllEmp();"></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録通知メール</font>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="sendmail" value="t"<? if ($sendmail == "t") {echo(" checked");} ?>>送信する<font color="gray">（自分以外の登録対象者にウェブメールを送信します）</font></font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備予約</font></td>
<td colspan="3"><select name="cate_id" onchange="cateOnChange();"><? show_category_options($categories, $cate_id); ?></select><select name="facility_id"></select>&nbsp;&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用人数：<input type="text" name="users" value="<? echo($users); ?>" size="4" maxlength="4" style="ime-mode:inactive;">名</font></td>
</tr>

<?php if ($schedule_alarm) {?>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アラーム</font></td>
<td colspan="3" class="j12">
<?php if ($schedule_alarm === "1") {?>
<input type="number" name="alarm_h" size="4" value="<?php eh($alarm_h);?>" class="numeric" />時間 <input type="number" name="alarm_m" size="4" value="<?php eh($alarm_m);?>" class="numeric" />分前に通知する<br />
<?php
if ($salarm_start or $salarm_end) {
    print "（設定可能範囲: {$salarm_start} 〜 {$salarm_end}）";
}
?>
<?php } else if ($schedule_alarm === "2") {?>
<label><input type="checkbox" name="alarm_m" value="1" <?php if ($alarm_m) { echo 'checked'; }?>/>通知する</label>
<?php }?>
</td>
</tr>
<?php }?>

</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" align="right"><input type="button" value="登録" onclick="registerSchedule();"></td>
</tr>
</table>
<input type="hidden" name="schd_emp_id" value="<? echo($schd_emp_id); ?>">
<input type="hidden" name="calendar" value="">
<input type="hidden" name="repeat" value="">
<input type="hidden" name="datelist" value="">
<input type="hidden" id="target_id_list"   name="target_id_list" value="">
<input type="hidden" id="target_name_list" name="target_name_list" value="">
</form>
<?
if (is_array($log)) {
    show_log($log);
}
?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 設備カテゴリオプションを出力
function show_category_options($categories, $cate_id) {
    $len = 0;
    foreach ($categories as $tmp_category) {
        $tmp_len = mb_strwidth($tmp_category["name"], "EUC-JP");
        if ($tmp_len > $len) {
            $len = $tmp_len;
        }
    }
    if ($len == 0) {$len = 10;}

    echo("<option value=\"0\">" . str_repeat("-", $len) . "\n");
    foreach ($categories as $tmp_category_id => $tmp_category) {
        echo("<option value=\"$tmp_category_id\">{$tmp_category["name"]}\n");
    }
}

// ログを表示
function show_log($log) {

    echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" style=\"margin-top:5px;\">\n");
    echo("<tr>\n");
    echo("<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>施設・設備予約結果</b></font></td>\n");
    echo("</tr>\n");
    echo("<tr bgcolor=\"#f6f9ff\">\n");
    echo("<td height=\"22\" width=\"30\"></td>\n");
    echo("<td width=\"120\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日付</font></td>\n");
    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">結果</font></td>\n");
    echo("</tr>\n");

    foreach ($log as $row) {
        list($date, $msg) = split(",", $row);

        echo("<tr>\n");
        echo("<td height=\"22\" width=\"30\"></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$date</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$msg</font></td>\n");
        echo("</tr>\n");
    }

    echo("</table>\n");

}

function show_target_options($arr_target) {
    foreach ($arr_target as $row) {
        echo("<option value=\"{$row["id"]}\">{$row["name"]}\n");
    }
}

function show_target_list($arr_target) {
    for ($i = 0; $i < count($arr_target); $i++) {
        if ($i > 0) {
            echo(",");
        }
        echo($arr_target[$i]["id"]);
    }
}

// 1か月分のカレンダーを出力
function show_calendar($year, $month, $start_wd, $offset) {

    // 処理対象年月を求める
    for ($i = 0; $i < $offset; $i++) {
        if ($month == "12") {
            $month = "01";
            $year++;
        } else {
            $month = substr("0" . ($month + 1), -2);
        }
    }

    // 日付配列を取得
    $arr_month = get_arr_month("$year$month", $start_wd);

    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    echo("<tr>\n");
    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$year/$month</font></td>\n");
    echo("</tr>\n");
    echo("<tr>\n");
    echo("<td>\n");
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
    echo("<tr>\n");
    if ($start_wd == 0) {
        echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
    }
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">火</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">水</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">木</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">金</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#defafa\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">土</font></td>\n");
    if ($start_wd == 1) {
        echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
    }
    echo("</tr>\n");

    foreach ($arr_month as $arr_week) {
        echo("<tr>\n");

        foreach ($arr_week as $tmp_ymd) {
            $tmp_y = substr($tmp_ymd, 0, 4);
            $tmp_m = substr($tmp_ymd, 4, 2);
            if ($tmp_m != $month) {
                echo("<td>&nbsp;</td>\n");
            } else {
                if (ktHolidayName(to_timestamp($tmp_ymd)) != "") {
                    $bgcolor = "#fadede";
                } else {
                    $bgcolor = "#f6f9ff";
                }
                $tmp_d = intval(substr($tmp_ymd, 6, 2));
                echo("<td id=\"id$tmp_ymd\" align=\"center\" bgcolor=\"$bgcolor\" style=\"cursor:pointer;\" onclick=\"dateOnClick('$tmp_ymd');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"id{$tmp_ymd}_a\" href=\"javascript:void(0);\">$tmp_d</a></font></td>\n");
            }
        }

        echo("</tr>");
    }

    echo("</table>\n");
    echo("</td>\n");
    echo("</tr>\n");
    echo("</table>\n");
}

// 指定年月分の日付配列を返す
function get_arr_month($ym, $start_wd) {

    // 当月1日〜末日の配列を作成
    $year = substr($ym, 0, 4);
    $month = substr($ym, 4, 2);
    $arr_date = array();
    for ($i = 1; $i <= 31; $i++) {
        $day = substr("0" . $i, -2);
        if (checkdate($month, $day, $year)) {
            array_push($arr_date, "$year$month$day");
        } else {
            break;
        }
    }

    // 空白セル分の日付を配列の先頭に追加
    $first_day = mktime(0, 0, 0, $month, "01", $year);
    $empty = date("w", $first_day) - $start_wd;
    if ($empty < 0) {
        $empty += 7;
    }
    for ($i = 1; $i <= $empty; $i++) {
        array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
    }

    // 空白セル分の日付を配列の末尾に追加
    $day = substr($arr_date[count($arr_date) - 1], -2);
    $end_day = mktime(0, 0, 0, $month, $day, $year);
    $empty = 7 - (count($arr_date) % 7);
    if ($empty == 7) {
        $empty = 0;
    }
    for ($i = 1; $i <= $empty; $i++) {
        array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
    }

    // 週単位に分割した配列を返す
    return array_chunk($arr_date, 7);
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
    $y = substr($yyyymmdd, 0, 4);
    $m = substr($yyyymmdd, 4, 2);
    $d = substr($yyyymmdd, 6, 2);
    return mktime(0, 0, 0, $m, $d, $y);
}

function get_emp_str($mygroup_id){
  global $employees;
  $ids = $employees[$mygroup_id];
  $names = array();
  $emp_ids = array();
  foreach($ids as $id){
    $name = $id['name'];
    $emp_id = $id['emp_id'];
    array_push($names, $name);
    array_push($emp_ids, $emp_id);
  }
  $rtn  = "'";
  $rtn .= join(", ", $names);
  $rtn .= "', '";
  $rtn .= join(", ", $emp_ids);
  $rtn .= "'";
  return $rtn;
}

function get_emp_id_str($mygroup_id){
  global $employees;
  $ids = $employees[$mygroup_id];
  $emp_ids = array();
  foreach($ids as $id){
    $emp_id = $id['emp_id'];
    array_push($emp_ids, $emp_id);
  }
  $rtn = join(", ", $emp_ids);
  return $rtn;
}

function get_emp_name_str($mygroup_id){
  global $employees;
  $ids = $employees[$mygroup_id];
  $names = array();
  foreach($ids as $id){
    $name = $id['name'];
    array_push($names, $name);
  }
  $rtn = join(", ", $names);
  return $rtn;
}
