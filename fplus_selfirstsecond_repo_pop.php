<?
require_once("about_comedix.php");
require_once("fplus_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// インシデント権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//検索ボタン押下時に１が入る
$serch = @$_REQUEST["search_mode"];

// 指定された日付
if($arisedate == ""){
	$arisedate = @$_REQUEST["arise_date"];
}

$hospital_id = @$_REQUEST["hospital_id"];

//検索ボタン押下後
if($serch == "1"){
	$sql= " SELECT ".
		" 	frep.report_no ".
		" 	, frep.first_report_id as report_id ".
		" 	, to_char(frep.occurrences_date,'YYYY/MM/DD') as occurrences_date ".
		" 	, to_char(frep.report_date,'YYYY/MM/DD') as report_date ".
		" 	, frep.hospital_id ".
		" 	, hosp.hospital_name ".
		" 	, frep.patient_name ".
		" 	, (CASE frep.patient_sex ".
		" 		WHEN '1' THEN '男性' ".
		" 		ELSE '女性' ".
		" 	  END) as sex ".
		" 	, frep.patient_age ".
		" 	, (CASE frep.patient_class ".
		" 		WHEN '1' THEN '入院' ".
		" 		ELSE '外来' ".
		" 	  END) as hos_vis ".
		" 	, hos_dep.item_name ".
		" 	, frep.main_disease ".
		" 	, frep.title ".
		" 	, (CASE frep.outline ".
		" 		WHEN '1' THEN '薬剤' ".
		" 		WHEN '2' THEN '輸血' ".
		" 		WHEN '3' THEN '治療・処置' ".
		" 		WHEN '4' THEN '医療機器等' ".
		" 		WHEN '5' THEN 'ドレーンチューブ' ".
		" 		WHEN '6' THEN '検査' ".
		" 		ELSE '療養上の世話（転倒・転落等）' ".
		" 	  END) as outline ".
		" 	, '1' as report_kbn ".
		" 	, '第一報' AS report_kbn_nm ".
		" FROM ".
		" 	fplus_first_report frep ".
		" 	, fplusapply app ".
		" 	,(SELECT ".
		" 		item_cd ".
		" 		, item_name ".
		" 	FROM ".
		" 		tmplitem ".
		" 	WHERE ".
		" 		mst_cd = 'A402' ".
		" 		and disp_flg = 't') hos_dep ".
		" 	, fplus_hospital_master hosp ".
		" WHERE ".
		" 	frep.occurrences_date = '$arisedate' ".
		" 	AND frep.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND frep.clinical_departments = hos_dep.item_cd ".
		" 	AND frep.hospital_id = hosp.hospital_id ".
		" 	AND frep.hospital_id = '$hospital_id' ".
		" 	AND hosp.del_flg = 'f' ".
		" UNION ".
		" SELECT ".
		" 	srep.report_no ".
		" 	, srep.second_report_id as report_id ".
		" 	, to_char(srep.occurrences_date,'YYYY/MM/DD') as occurrences_date ".
		" 	, to_char(srep.report_date,'YYYY/MM/DD') as report_date ".
		" 	, srep.hospital_id ".
		" 	, hosp.hospital_name ".
		" 	, srep.patient_name ".
		" 	, (CASE srep.patient_sex ".
		" 		WHEN '1' THEN '男性' ".
		" 		ELSE '女性' ".
		" 	  END) as sex ".
		" 	, srep.patient_age ".
		" 	, (CASE srep.patient_class ".
		" 		WHEN '1' THEN '入院' ".
		" 		ELSE '外来' ".
		" 	  END) as hos_vis ".
		" 	, hos_dep.item_name ".
		" 	, srep.main_disease ".
		" 	, srep.title ".
		" 	, (CASE srep.outline ".
		" 		WHEN '1' THEN '薬剤' ".
		" 		WHEN '2' THEN '輸血' ".
		" 		WHEN '3' THEN '治療・処置' ".
		" 		WHEN '4' THEN '医療機器等' ".
		" 		WHEN '5' THEN 'ドレーンチューブ' ".
		" 		WHEN '6' THEN '検査' ".
		" 		ELSE '療養上の世話（転倒・転落等）' ".
		" 	  END) as outline ".
		" 	, '2' AS report_kbn ".
		" 	, '第二報' AS report_nm ".
		" FROM ".
		" 	fplus_second_report srep ".
		" 	, fplusapply app ".
		" 	,(SELECT ".
		" 		item_cd ".
		" 		, item_name ".
		" 	FROM ".
		" 		tmplitem ".
		" 	WHERE ".
		" 		mst_cd = 'A402' ".
		" 		and disp_flg = 't') hos_dep ".
		" 	, fplus_hospital_master hosp ".
		" WHERE ".
		" 	srep.occurrences_date = '$arisedate' ".
		" 	AND srep.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND srep.clinical_departments = hos_dep.item_cd ".
		" 	AND srep.hospital_id = hosp.hospital_id ".
		" 	AND srep.hospital_id = '$hospital_id' ".
		" 	AND hosp.del_flg = 'f' ".
		" ORDER BY ".
		" 	occurrences_date desc ".
		" 	, report_date desc ";

	$sel = @select_from_table($con, $sql, "", $fname);
	$result = pg_fetch_all($sel);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix 検索 | 報告書</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
</style>
<script type="text/javascript">

<? //検索結果を返す ?>
function setData(rowidx) {
	opener.document.apply.elements['data_occurrence_date'].value = document.getElementById("hdn_arise_date_" + rowidx).value;
	opener.document.apply.elements['data_hospital_id'].value = document.getElementById("hdn_hospital_" + rowidx).value;
	opener.document.apply.elements['hdn_link_id'].value = document.getElementById("hdn_report_id_" + rowidx).value;
	opener.document.apply.elements['hdn_link_report_kbn'].value = document.getElementById("hdn_report_kbn_" + rowidx).value;

	self.close();
}

//今日ボタン押下処理
function getDateLabel() {

	currentDateObject = new Date();

	wYear = currentDateObject.getYear();

	currentFullYear = (wYear < 2000) ? wYear + 1900 : wYear;
	currentMonth = to2String(currentDateObject.getMonth() + 1);
	currentDate = to2String(currentDateObject.getDate());

	dateString = currentFullYear + "/" + currentMonth + "/" + currentDate;

	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = dateString;
}

function to2String(value) {
	label = "" + value;
	if (label.length < 2) {
		label = "0" + label;
	}
	return label;
}

//カレンダーからの戻り処理を行います。
function call_back_fplus_calendar(caller,ymd)
{
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = ymd;
}

function dateClear() {
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = "";
}

function clickselbutton() {
	if(document.getElementById("arise_date").value == "") {
		alert("検索条件を入力してください。");
		return false;
	}else{
		document.sel.action = 'fplus_selfirstsecond_repo_pop.php';
		document.sel.search_mode.value = "1";
		document.sel.target = "_self";
		document.sel.submit();
		return false;
	}
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
<style type="text/css">
form {margin:0;}
/*table.list {border-collapse:collapse;}*/
table.list td {border:solid #5279a5 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_fplus_header_for_sub_window("報告書検索");
?>
</table>
<form name="sel" action="#" method="post">
	<input type="hidden" name="session" id="session" value="<? echo($session); ?>">
	<input type="hidden" name="search_mode" id="search_mode" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td  bgcolor="#F5FFE5">
<? //検索条件入力フォーム　ここから-------------------- ?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="j12"><span>発生年月日</span></font>
			<font class="j12">
					<input type="text" name="arise_date" id="arise_date" size="15" readonly="readonly" value="<?=$arisedate?>" />
					<input type="button" value="今日" onclick="getDateLabel();" />
					<img id="Img4" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer; left: 0px;" alt="カレンダー" onclick="window.open('fplus_calendar.php?session=<? echo($session); ?>&caller=arise_date', 'epinet_arise_calendar', 'width=640,height=480,scrollbars=yes');"/>
					<input type="button" value="クリア" onclick="dateClear();" />
					<input id="Button2" type="button" value="検索" style="width:100px;" onclick="clickselbutton();" />
			</font>			
		</td>
	</tr>
</table>
<? //検索条件入力フォーム　ここまで-------------------- ?>
<? //検索結果一覧表示　ここから-------------- ?>
<? //show_calendars($caller,$year, $month); ?>
<table border="0" cellpadding="0" cellspacing="0" class="list1" style="background-color:#fff;" width="985px">
	<input type="hidden" id="hospital_id" name="hospital_id" value="<?=$hospital_id?>"/>
	<tr  style="background-color:#ffdfff;">
		<td rowspan="2" align="center" style="width:60px;"><font class="j12"><span>報告��</span></font></td>
		<td rowspan="2" align="center" style="width:70px;"><font class="j12"><span>報告書区分</span></font></td>
		<td rowspan="2" align="center" style="width:70px;"><font class="j12"><span>発生年月日</span></font></td>
		<td rowspan="2" align="center" style="width:70px;"><font class="j12"><span>報告年月日</span></font></td>
		<td rowspan="2" align="center" style="width:110px;"><font class="j12"><span>発生病院</span></font></td>
		<td rowspan="2" align="center" style="width:95px;"><font class="j12"><span>表題</span></font></td>
		<td rowspan="2" align="center" style="width:60px;"><font class="j12"><span>概要</span></font></td>
		<td colspan="6" align="center" style="width:390px;"><font class="j12"><span>対象患者</span></font></td>
	</tr>
	<tr  style="background-color:#ffdfff;">
		<td align="center" style="width:80px;"><font class="j12"><span>氏名</span></font></td>
		<td align="center" style="width:40px;"><font class="j12"><span>性別</span></font></td>
		<td align="center" style="width:40px;"><font class="j12"><span>年齢</span></font></td>
		<td align="center" style="width:50px;"><font class="j12"><span>入外別</span></font></td>
		<td align="center" style="width:90px;"><font class="j12"><span>診療科</span></font></td>
		<td align="center" style="width:90px;"><font class="j12"><span>主たる病名</span></font></td>
	</tr>
	<? if($serch=="1"){ ?>
		<? if($result[0]["report_no"]==""){ ?>
		<? //検索結果が0件 ?>
			<tr>
				<td colspan="13" align="center"><font class="j12">データが存在しません</font></td>
			</tr>
		<? }else{ ?>
		<? //検索結果が1件以上 ?>
			<? $count=0 ?>
			<? foreach($result as $idx => $row){ ?>
			<tr>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:60px;"><font class="j12"><?=$row["report_no"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:70px;"><font class="j12"><?=$row["report_kbn_nm"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:70px;"><font class="j12"><?=$row["occurrences_date"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left" style="width:70px;"><font class="j12"><?=$row["report_date"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left" style="width:110px;"><font class="j12"><?=$row["hospital_name"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left" style="width:95px;"><font class="j12"><?=$row["title"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left" style="width:60px;"><font class="j12"><?=$row["outline"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:80px;"><font class="j12"><?=$row["patient_name"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:40px;"><font class="j12"><?=$row["sex"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:40px;"><font class="j12"><?=$row["patient_age"]."歳"?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:50px;"><font class="j12"><?=$row["hos_vis"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center" style="width:90px;"><font class="j12"><?=$row["item_name"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left" style="width:90px;"><font class="j12"><?=$row["main_disease"]?></font></td>

				<? //hiddenにデータ格納 ?>
				<input type="hidden" id="hdn_arise_date_<?=$count?>" value="<?=$row["occurrences_date"]?>" />
				<input type="hidden" id="hdn_hospital_<?=$count?>" value="<?=$row["hospital_id"]?>" />
				<input type="hidden" id="hdn_report_no_<?=$count?>" value="<?=$row["report_no"]?>" />
				<input type="hidden" id="hdn_report_id_<?=$count?>" value="<?=$row["report_id"]?>" />
				<input type="hidden" id="hdn_report_kbn_<?=$count?>" value="<?=$row["report_kbn"]?>" />
			</tr>
			<? $count++; ?>
			<?}?>
		<? } ?>
	<? } ?>
</table>
<? //検索結果一覧表示　ここまで-------------- ?>
</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
