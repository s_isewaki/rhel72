<?php
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');

/**
 * タイムカードデータ取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $session セッション
 * @param mixed $emp_id 職員ID
 * @param mixed $yyyymm 対象年月
 * @param mixed $atdbk_common_class 共通クラス
 * @param mixed $start_date 開始日
 * @param mixed $end_date 終了日
 * @param mixed $atdbk_workflow_common_class 共通クラス
 * @param mixed $timecard_bean タイムカード設定
 * @param mixed $meeting_display_flag 会議・研修表示フラグ
 * @param mixed $arr_legal_hol_cnt 公休数
 * @param mixed $arr_legal_hol_cnt_part 公休数非常勤用
 * @param mixed $arr_holwk カレンダーマスタ上で休日出勤と判定する日
 * @param mixed $legal_hol_cnt_flg 公休数フラグ、true:下の対象期間の公休数を使用
 * @param mixed $legal_hol_cnt_kikan 対象期間の公休数
 * @param mixed $obj_hol_hour 時間有休クラス
 * @param mixed $wherefrom "7","10":勤務シフト作成実績入力から
 * @param mixed $start_date_part 非常勤者の開始日
 * @param mixed $end_date_part 非常勤者の終了日
 * @param mixed $output_flg 出力種別フラグ "":html 1:pdf
 * @param mixed $calendar_name_class カレンダー名、所定労働時間履歴用
 * @param mixed $shift_flg true:タイムカード入力の出勤簿 20130129
 * @return mixed タイムカード用情報、arr_info[]を返す。arr_info[日付idx]["name"],arr_info[日付idx][項目idx](pdf用の列順)、
 *　　arr_info["その他の情報name"]も設定。
 *
 */
function get_atdbk_info($con, $fname, $session, $emp_id, $yyyymm, $atdbk_common_class, $start_date, $end_date, $atdbk_workflow_common_class, $timecard_bean, $meeting_display_flag, $arr_legal_hol_cnt, $arr_legal_hol_cnt_part, $arr_holwk, $legal_hol_cnt_flg, $legal_hol_cnt_kikan, $obj_hol_hour, $wherefrom="", $start_date_part="", $end_date_part="", $output_flg, $calendar_name_class, $shift_flg=FALSE, $last_week_flg=FALSE, $forty_last_week="", $ovtm_class)
{
	//勤務グループ指定 20150820
	$target_tmcd_group_id = $_REQUEST["tmcd_group_id"];
	if ($target_tmcd_group_id == "all") {
		$target_tmcd_group_id = "";
	}
	
	$arr_info = array();
	//欠勤データ 20150113
	if ($ovtm_class->list_absence_disp_flg == "t") {
		$arr_absence_data = get_kintai_late_absence_data($con, $fname, $emp_id, $start_date, $end_date);
	}
	$absence_time_total = 0;
	$absence_day_total = 0;
	//有休申請データ 20150113
	$arr_paid_hol_list = $obj_hol_hour->get_paid_hol_list($emp_id, $start_date, $end_date);
	$arr_paid_hol_data = array();
	foreach ($arr_paid_hol_list as $idx => $wk_data) {
		$arr_paid_hol_data[ $wk_data["date"] ] = $wk_data;
	}
	
	$arr_info["year"] = substr($yyyymm, 0, 4);
	$arr_info["month"] = intval(substr($yyyymm, 4, 2));

	// 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理、給与支給区分・祝日計算方法を取得
	$sql = "select duty_form, no_overtime, wage, hol_minus, specified_time, am_time, pm_time, tmcd_group_id from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$duty_form =  pg_fetch_result($sel, 0, "duty_form");
		$no_overtime = pg_fetch_result($sel, 0, "no_overtime");
		$wage = pg_fetch_result($sel, 0, "wage");
		$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
		$paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
		$am_time = pg_fetch_result($sel, 0, "am_time");
		$pm_time = pg_fetch_result($sel, 0, "pm_time");
		$tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
	} else {
		$duty_form = "";
		$no_overtime = "";
		$wage = "";
		$hol_minus = "";
		$paid_specified_time = "";
		$am_time = "";
		$pm_time = "";
		$tmcd_group_id = "";
	}

	$arr_info["duty_form"] = $duty_form;
	$arr_info["no_overtime"] = $no_overtime;
	$arr_info["wage"] = $wage;
	$arr_info["hol_minus"] = $hol_minus;
	$arr_info["tmcd_group_id"] = $tmcd_group_id;

	if ($duty_form == 2) {
		//非常勤
		$wk = $timecard_bean->closing_parttime;
		//非常勤が未設定時は常勤を使用する 20091214
		if ($wk == "") {
			$wk = $timecard_bean->closing;
		}
	} else {
		//常勤
		$wk = $timecard_bean->closing;
	}
	$closing = $wk;
	$arr_info["closing"] = $closing;

	$closing_month_flg = $timecard_bean->closing_month_flg;

	$arr_info["closing_month_flg"] = $closing_month_flg;

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}

	//勤務シフト実績入力からで非常勤の場合
	if (($wherefrom == "7" || $wherefrom == "10") && $duty_form == "2") {
		$wk_start_date = $start_date_part;
		$wk_end_date = $end_date_part;
	}
	//勤務シフト実績入力以外、常勤
	else {
		$wk_start_date = $start_date;
		$wk_end_date = $end_date;
	}

	$arr_info["start_date"] = $wk_start_date;
	$arr_info["end_date"] = $wk_end_date;
	$start_month = intval(substr($wk_start_date, 4, 2));
	$start_day = intval(substr($wk_start_date, 6, 2));
	$end_month = intval(substr($wk_end_date, 4, 2));
	$end_day = intval(substr($wk_end_date, 6, 2));
	//一括修正申請情報
	$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($emp_id, $wk_start_date, $wk_end_date);

	// 氏名を取得
	$sql = "select emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
	// 職員氏名の後ろに勤務条件を追加 2008/09/18
	$str_empcond = get_employee_empcond2($con, $fname, $emp_id);
	if ($target_tmcd_group_id == "") { //20150820
		$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $wk_start_date, $wk_end_date);
	}
	else {
		$arr_group_names = get_timecard_group_names($con, $fname);
		$str_shiftname = $arr_group_names[$target_tmcd_group_id];
	}
	//所属表示
	$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);

	$arr_info["emp_id"] = $emp_id;
	$arr_info["emp_name"] = $emp_name;
	$arr_info["emp_personal_id"] = $emp_personal_id;
	$arr_info["str_empcond"] = $str_empcond;
	$arr_info["str_shiftname"] = $str_shiftname;
	$arr_info["str_orgname"] = $str_orgname;


	// 変則労働適用情報を配列で取得
	$arr_irrg_info = get_irrg_info_array($con, $emp_id, $fname);
	// 対象年月の締め状況を取得
	//月度対応 20100415
	$tmp_yyyymm = $year.sprintf("%02d", $month);
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

	// 締め済みの場合、締めデータから表示対象期間を取得
	if ($atdbk_closed) {
		$sql = "select min(date), max(date) from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$wk_start_date = pg_fetch_result($sel, 0, 0);
		$wk_end_date = pg_fetch_result($sel, 0, 1);

		$start_year = intval(substr($wk_start_date, 0, 4));
		$start_month = intval(substr($wk_start_date, 4, 2));
		$start_day = intval(substr($wk_start_date, 6, 2));
		$end_year = intval(substr($wk_end_date, 0, 4));
		$end_month = intval(substr($wk_end_date, 4, 2));
		$end_day = intval(substr($wk_end_date, 6, 2));
	}

	// 端数処理情報を取得
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = pg_fetch_result($sel, 0, "fraction$i");
			$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = pg_fetch_result($sel, 0, "others$i");
		}
		$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
		$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
		$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
		//時間給者の休憩時間追加 20100929
		$rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
		$rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
		$rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
		$rest7 = intval(pg_fetch_result($sel, 0, "rest7"));

		$night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
		$delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
		$early_leave_time_flg = pg_fetch_result($sel, 0, "early_leave_time_flg");
		//$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
		$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
		$timecard_title = pg_fetch_result($sel, 0, "timecard_title");
		$timecard_title2 = pg_fetch_result($sel, 0, "timecard_title2");	//出勤簿タイトル 20130129
		$timecard_appr_disp_flg = pg_fetch_result($sel, 0, "timecard_appr_disp_flg");
	} else {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = "";
			$$var_name_min = "";
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = "";
		}
		$rest1 = 0;
		$rest2 = 0;
		$rest3 = 0;
		//時間給者の休憩時間追加 20100929
		$rest4 = 0;
		$rest5 = 0;
		$rest6 = 0;
		$rest7 = 0;

		$night_workday_count = "";
		$delay_overtime_flg = "1";
		$early_leave_time_flg = "2";
		$modify_flg = "t";
		$timecard_appr_disp_flg = "f";
	}
	//週40時間超過分は計算しないを固定とする 20100525
	$overtime_40_flg = "2";

	//集計処理の設定を「集計しない」の固定とする 20100910
	$others1 = "1";	//勤務時間内に外出した場合
	$others2 = "1";	//勤務時間外に外出した場合
	$others3 = "1";	//時間給者の休憩の集計
	$others4 = "1";	//遅刻・早退に休憩が含まれた場合
	$others5 = "2";	//早出残業時間の集計

	if ($timecard_title == "") {
		$timecard_title = "タイムカード";
	}
	$arr_info["timecard_title"] = $timecard_title;

	if ($timecard_title2 == "") {
		$timecard_title2 = "出勤簿";
	}
	$arr_info["timecard_title2"] = $timecard_title2;

	//タイトル、決裁欄追加後の開始位置調整
	if ($timecard_appr_disp_flg == "t") {
		$line_offset = 270;
	} else {
		$line_offset = 50;
	}

	//休日出勤と判定する日設定取得 20091222
	$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $emp_id);

	//公休と判定する日設定 20100810
	$arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");

	// 集計結果格納用配列を初期化
	$sums = array_fill(0, 46, 0);

    //所定労働時間履歴を使用 20121127
    $arr_timehist = $calendar_name_class->get_calendarname_time($wk_start_date);
    $day1_time = $arr_timehist["day1_time"];
    //勤務条件にない場合
    if ($paid_specified_time == "") {
        $paid_specified_time = $day1_time;
        $am_time = $arr_timehist["am_time"];
        $pm_time = $arr_timehist["pm_time"];
        $empcond_spec_time_flg = false;
    }
    else {
        $empcond_spec_time_flg = true;
    }
    // 勤務日・休日の名称を取得
	$sql = "select day1_time ,day2, day3, day4, day5, am_time, pm_time from calendarname";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$day2 = pg_fetch_result($sel, 0, "day2");
		$day3 = pg_fetch_result($sel, 0, "day3");
		$day4 = pg_fetch_result($sel, 0, "day4");
		$day5 = pg_fetch_result($sel, 0, "day5");
        /*
		//$day1_time = pg_fetch_result($sel, 0, "day1_time"); //20121127
		//半日有給対応
		if ($paid_specified_time == "") {
			$paid_specified_time = $day1_time;
		}
		if ($am_time == "") {
			$am_time = pg_fetch_result($sel, 0, "am_time");
		}
		if ($pm_time == "") {
			$pm_time = pg_fetch_result($sel, 0, "pm_time");
		}
	}
	else{
		$day1_time = "";
        */
    }
	if ($day2 == "") {
		$day2 = "法定休日";
	}
	if ($day3 == "") {
		$day3 = "所定休日";
	}
	if ($day4 == "") {
		$day4 = "勤務日1";
	}
	if ($day5 == "") {
		$day5 = "勤務日2";
	}

	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);


	// 「退勤後復帰」ボタン表示フラグを取得
	$sql = "select ret_btn_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

	// 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
	$tmp_date = $wk_start_date;
	$wd = date("w", to_timestamp($tmp_date));
	if ($arr_irrg_info["irrg_type"] == "1") {
		while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	} else {
		//変則労働時間・週以外の場合、日曜から
		while ($wd != 0) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	}
	//データ検索用開始日
	$wk_srch_start_date = $tmp_date;

	//グループ毎のテーブルから勤務時間を取得する
	$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $tmp_date, $wk_end_date);

    //個人別所定時間を優先するための対応 20120309
    $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $wk_end_date);
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

    $holiday_count = 0;

    //翌月初日までデータを取得 20130308
    $wk_end_date_nextone = next_date($wk_end_date);
    // 処理日付の勤務日種別を取得
	$sql = "select date, type from calendar";
    $cond = "where date >= '$wk_srch_start_date' and date <= '$wk_end_date_nextone'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
	//
	// 残業理由一覧を取得
	$sql = "select reason_id, reason, over_no_apply_flag from ovtmrsn";
	$cond = "where del_flg = 'f' order by reason_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$arr_over_no_apply_flag = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_reason_id = $row["reason_id"];
		$tmp_reason = $row["reason"];
		$tmp_over_no_apply_flag = $row["over_no_apply_flag"];

		$arr_over_no_apply_flag[$tmp_reason_id] = $tmp_over_no_apply_flag;

	}


	//予定情報は、時間給制の場合に必要。20110421 必要な項目が出てきた場合は条件を変更する。
	if ($wage == "4") {
		// 処理日付の勤務予定情報を取得
		$sql =	"SELECT ".
			"atdbk.date, ".
			"atdbk.pattern, ".
			"atdbk.reason, ".
			"atdbk.night_duty, ".
			"atdbk.allow_id, ".
			"atdbk.prov_start_time, ".
			"atdbk.prov_end_time, ".
			"atdbk.tmcd_group_id, ".
			"atdptn.workday_count ".
			"FROM ".
			"atdbk ".
			"LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
		$cond = "where emp_id = '$emp_id' and date >= '$wk_start_date' and date <= '$wk_end_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$date = $row["date"];
			$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
			$arr_atdbk[$date]["pattern"] = $row["pattern"];
			$arr_atdbk[$date]["reason"] = $row["reason"];
			$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
			$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
			$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
			$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
			$arr_atdbk[$date]["workday_count"] = $row["workday_count"];
		}
	}
	// 処理日付の勤務実績を取得
    $sql =	"SELECT ".
        "atdbkrslt.*, ".
        "atdptn.workday_count, ".
        "atdptn.base_time, ".
        "atdptn.over_24hour_flag, ".
        "atdptn.out_time_nosubtract_flag, ".
        "atdptn.after_night_duty_flag, ".
        "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
        "atdptn.no_count_flag, ".
        "tmmdapply.apply_id     AS tmmd_apply_id, ".
		"tmmdapply.apply_status AS tmmd_apply_status, ".
		"ovtmapply.apply_id     AS ovtm_apply_id, ".
		"ovtmapply.apply_status AS ovtm_apply_status, ".
		"ovtmapply.reason AS other_reason, ".
		"ovtmapply.reason_id AS ovtm_reason_id, ".
		"ovtmrsn.reason AS ovtm_reason, ".
		"rtnapply.apply_id      AS rtn_apply_id, ".
		"rtnapply.apply_status  AS rtn_apply_status ".
		"FROM ".
		"atdbkrslt  ".
		"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
		"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
		"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
		"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ".
		"LEFT JOIN ovtmrsn on ovtmapply.reason_id = ovtmrsn.reason_id";
    $cond = "where atdbkrslt.emp_id = '$emp_id' and atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$wk_end_date_nextone'"; //20130308
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
        //まとめて設定
        $arr_atdbkrslt[$date] = $row;
/*
        $arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
		$arr_atdbkrslt[$date]["reason"] = $row["reason"];
		$arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
		$arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
		$arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
		$arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$arr_atdbkrslt[$date][$start_var] = $row[$start_var];
			$arr_atdbkrslt[$date][$end_var] = $row[$end_var];
		}
		$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
		$arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
		$arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
		$arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
		$arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
		$arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
		$arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
		$arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
		$arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
		$arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
		$arr_atdbkrslt[$date]["meeting_start_time"] = $row["meeting_start_time"];
		$arr_atdbkrslt[$date]["meeting_end_time"] = $row["meeting_end_time"];
		$arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
		//残業時刻追加 20100114
		$arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
		$arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
		$arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
		//残業時刻2追加 20110621
		$arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
		$arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
		$arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
		//法定内残業
		$arr_atdbkrslt[$date]["legal_in_over_time"] = $row["legal_in_over_time"];
		//早出残業 20100601
		$arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];
		//休憩時刻追加 20100921
		$arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
		$arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
		$arr_atdbkrslt[$date]["ovtm_reason"] = $row["ovtm_reason"];
		$arr_atdbkrslt[$date]["other_reason"] = $row["other_reason"];
		$arr_atdbkrslt[$date]["ovtm_reason_id"] = $row["ovtm_reason_id"];
		//外出時間を差し引かないフラグ 20110727
		$arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
		//明け追加 20110819
		$arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $arr_atdbkrslt[$date]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $arr_atdbkrslt[$date]["no_count_flag"] = $row["no_count_flag"];
        //残業３−５、休憩１−５追加
        for ($i = 3; $i <= 5; $i++) {
            $start_var = "over_start_time$i";
            $end_var = "over_end_time$i";
            $arr_atdbkrslt[$date][$start_var] = $row[$start_var];
            $arr_atdbkrslt[$date][$end_var] = $row[$end_var];
            
            $start_flag_var = "over_start_next_day_flag$i";
            $end_flag_var = "over_end_next_day_flag$i";
            $arr_atdbkrslt[$date][$start_flag_var] = $row[$start_flag_var];
            $arr_atdbkrslt[$date][$end_flag_var] = $row[$end_flag_var];
        }
        for ($i = 1; $i <= 5; $i++) {
            $start_var = "rest_start_time$i";
            $end_var = "rest_end_time$i";
            $arr_atdbkrslt[$date][$start_var] = $row[$start_var];
            $arr_atdbkrslt[$date][$end_var] = $row[$end_var];
            
            $start_flag_var = "rest_start_next_day_flag$i";
            $end_flag_var = "rest_end_next_day_flag$i";
            $arr_atdbkrslt[$date][$start_flag_var] = $row[$start_flag_var];
            $arr_atdbkrslt[$date][$end_flag_var] = $row[$end_flag_var];
        }
        */
    }
	//時間有休追加 20111207
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//時間有休データをまとめて取得
		$arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($emp_id, $wk_start_date, $wk_end_date);

	}
	//法定内勤務、法定内残業、法定外残業
	$hoteinai = 0;
	$hoteinai_zan = 0;
	$hoteigai = 0;

	//遅刻時間計
	$delay_time = 0;
	//早退時間計
	$early_leave_time = 0;
	//合計、外出、休憩、退復、日数、病棟外
	$out_time_total = 0;
	$rest_time_total = 0;
	$over_rest_time_total = 0; //残業休憩 20141209
	$return_time_total = 0;
	$meeting_time_total = 0;

	//支給換算時間
	$paid_time_total = 0;
	//時間有休当月計
	$paid_time_hour_month = 0;

	//普通残業
	$normal_over_total = 0;
	//深夜
	$late_night_total = 0;
	//休日残業
	$hol_over_total = 0;
	//休日深夜
	$hol_late_night_total = 0;
	//６０時間超計算用休日残業
	$hol_over_sixty_data_total = 0;
	//６０時間超計算用休日深夜
	$hol_late_night_sixty_data_total = 0;

	$sixty_value = 3600;
	//60時間超えた普通残業
	$sixty_normal_over_total = 0;
	//60時間超えた深夜残業
	$sixty_late_night_total = 0;
	//法定休暇の休日残業時間（60時間超）20141024
	$sixty_legal_holiday_total = 0;
	
	//45時間超計算用休日残業 20150714
	$hol_over_fortyfive_data_total = 0;
	//45時間超計算用休日深夜
	$hol_late_night_fortyfive_data_total = 0;

	$fortyfive_value = 2700;
	//45時間超えた普通残業
	$fortyfive_normal_over_total = 0;
	//45時間超えた深夜残業
	$fortyfive_late_night_total = 0;
	//法定休暇の休日残業時間（45時間超）20141024
	$fortyfive_legal_holiday_total = 0;
	
	//週40時間超残業
	$forty_last_day = ($timecard_bean->over_disp_forty_week == 0) ? "土" : "日";
	$week_forty_value = 2400;
	$week_forty_overtime_total = 0;	
	
	//当直日配列
	$arr_night_duty_date = array();
	//勤務実績回数
	//グループ
	$arr_group_id = array();
	//パターン
	$arr_group_ptn_cnt = array();
	//手当て
	$arr_allow_cnt = array();
    //呼出回数
    $return_count_total = 0;

	$day_idx = 0;

	//休日出勤テーブル 2008/10/23
	$arr_hol_work_date = array();
	while ($tmp_date <= $wk_end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));
		$work_time = 0;
		$wk_return_time = 0;
						
		//休日残業
		$hol_over_time = 0;
		//残業＝法定内残＋普通残業＋深夜＋休日残業＋休日深夜
		//普通残業
		$normal_over = 0;
		//深夜
		$late_night = 0;
		//休日残業
		$hol_over = 0;
		//休日深夜
		$hol_late_night = 0;
		//週40時間
		$week_forty_overtime = "";
		//６０時間超計算用休日残業
		$hol_over_sixty_data = 0;
		//６０時間超計算用休日深夜
		$hol_late_night_sixty_data = 0;

		//45時間超計算用休日残業
		$hol_over_fortyfive_data = 0;
		//45時間超計算用休日深夜
		$hol_late_night_fortyfive_data = 0;

		$start_time_info = array();
		$end_time_info = array(); //退勤時刻情報 20100910

		$time3 = 0; //外出時間（所定時間内） 20100910
		$time4 = 0; //外出時間（所定時間外） 20100910
		$tmp_rest2 = 0; //休憩時間 20100910
		$time3_rest = 0; //休憩時間（所定時間内）20100910
		$time4_rest = 0; //休憩時間（所定時間外）20100910
		//$arr_effective_time_wk = array(); //休憩を引く前の実働時間 20100915 20101217
		$arr_hol_time = array(); // 休日勤務情報 20101224

		$time_rest = "";

		$holiday_name = get_holiday_name($tmp_date);
		if ($holiday_name != "") {
			$holiday_count++;
		}

		// 処理日付の勤務日種別を取得
		$type = $arr_type[$tmp_date];

		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);

		// 処理日付の勤務予定情報を取得
		$prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
		$prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
		$prov_reason = $arr_atdbk[$tmp_date]["reason"];
		$prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
		$prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

		$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
		$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
		$prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
		$prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
		$prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];

		// 処理日付の勤務実績を取得
		$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
		$reason = $arr_atdbkrslt[$tmp_date]["reason"];
		$night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
		$allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
		$allow_count = $arr_atdbkrslt[$tmp_date]["allow_count"];
		$allow_id2 = $arr_atdbkrslt[$tmp_date]["allow_id2"];
		$allow_count2 = $arr_atdbkrslt[$tmp_date]["allow_count2"];
		$allow_id3 = $arr_atdbkrslt[$tmp_date]["allow_id3"];
		$allow_count3 = $arr_atdbkrslt[$tmp_date]["allow_count3"];
		$start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
		$end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
		$out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
		$ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
			$$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

			if ($$start_var != "") {
				$$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
			}
			if ($$end_var != "") {
				$$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
			}
		}

		$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
		$meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

		$workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

		$previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
        $next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

		$tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
		$tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
		$ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
		$ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
		$rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
		$rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];

		$meeting_start_time = $arr_atdbkrslt[$tmp_date]["meeting_start_time"];
		$meeting_end_time = $arr_atdbkrslt[$tmp_date]["meeting_end_time"];
		$base_time = $arr_atdbkrslt[$tmp_date]["base_time"];
		//hhmmをhh:mmに変換している
		if ($start_time != "") {
			$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
		}
		if ($end_time != "") {
			$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
		}
		//表示用に退避 20100802
		$disp_start_time = $start_time;
		$disp_end_time = $end_time;

		if ($out_time != "") {
			$out_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time);
		}
		if ($ret_time != "") {
			$ret_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
		}
		if ($meeting_start_time != "") {
			$meeting_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_start_time);
		}
		if ($meeting_end_time != "") {
			$meeting_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_end_time);
		}
		if ($previous_day_flag == "") {
			$previous_day_flag = 0;
		}
		if ($next_day_flag == "") {
			$next_day_flag = 0;
		}
		//表示用翌日フラグ
		$show_next_day_flag = $next_day_flag;
		//残業時刻追加 20100114
		$over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
		$over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
		$over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
		$over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
		if ($over_start_time != "") {
			$over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
		}
		if ($over_end_time != "") {
			$over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
		}
		//残業時刻2追加 20110616
		$over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
		$over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
		$over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
		$over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
		if ($over_start_time2 != "") {
			$over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
		}
		if ($over_end_time2 != "") {
			$over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
		}
		$over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
		//法定内残業
		$legal_in_over_time = $arr_atdbkrslt[$tmp_date]["legal_in_over_time"];
		if ($legal_in_over_time != "") {
			$legal_in_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $legal_in_over_time);
		}
		//早出残業 20100601
		$early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
		if ($early_over_time != "") {
			$early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
		}
		//休憩時刻追加 20100921
		$rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
		$rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
		if ($rest_start_time != "") {
			$rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
		}
		if ($rest_end_time != "") {
			$rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
		}
        //否認の場合は残業理由を未設定にする 20120723
        if ($ovtm_apply_status == "2") {
            $ovtm_reason = "";
            $other_reason = "";
            $ovtm_reason_id = "";
        }
        else {
            //残業理由
            $ovtm_reason = $arr_atdbkrslt[$tmp_date]["ovtm_reason"];
            $other_reason = $arr_atdbkrslt[$tmp_date]["other_reason"];
            $ovtm_reason_id = $arr_atdbkrslt[$tmp_date]["ovtm_reason_id"];
        }
        //外出時間を差し引かないフラグ 20110727
		$out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];
		//明け追加 20110819
		$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $no_count_flag = $arr_atdbkrslt[$tmp_date]["no_count_flag"];

		// 残業表示用に退勤時刻を退避
		$show_end_time = $end_time;
		$show_next_day_flag = $next_day_flag;	//残業時刻日またがり対応 20100114
		// 表示用出勤時刻 20100802
		$show_start_time = $start_time;

		$work_workday_count = "";

		//勤務グループ指定 20150820
		if ($target_tmcd_group_id != "" &&
			$target_tmcd_group_id != $tmcd_group_id) {
			$pattern = "";
			$reason = "";
			$start_time = "";
			$end_time = "";
			$show_start_time = "";
			$show_end_time = "";
			$disp_start_time = "";
			$disp_end_time = "";
			$previous_day_flag = "";
			$next_day_flag = "";
			$show_next_day_flag = "";
			$over_start_time = "";
			$over_end_time = "";
			$over_start_time2 = "";
			$over_end_time2 = "";
			$over_start_time3 = "";
			$over_end_time3 = "";
			$over_start_time4 = "";
			$over_end_time4 = "";
			$over_start_time5 = "";
			$over_end_time5 = "";
			$allow_id = "";
			$allow_id2 = "";
			$allow_id3 = "";
			$out_time = "";
			$ret_time = "";
			$o_start_time1 = "";
			$o_end_time1 = "";
			$meeting_start_time = "";
			$meeting_end_time = "";
			$after_night_duty_flag = "";
            $ovtm_reason = "";
            $other_reason = "";
		}
		//当直の有効判定
		$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

		if ($tmp_date >= $wk_start_date) {
			//当直日配列設定
			if ($night_duty == "1") {
				$arr_night_duty_date[] = array("date" => $tmp_date);
			}
			//勤務実績回数
			if ($tmcd_group_id != "") {
				$arr_group_id[$tmcd_group_id] = 1;
				if ($pattern != "") {
					if ($arr_group_ptn_cnt[$tmcd_group_id][$pattern] == "") {
						$arr_group_ptn_cnt[$tmcd_group_id][$pattern] = 1;
					} else {
						$arr_group_ptn_cnt[$tmcd_group_id][$pattern]++;
					}
				}
			}

			//事由に関わらず休暇で残業時刻がありの場合 20100916
			if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
				$over_start_time != "" && $over_end_time != "") {
				$legal_hol_over_flg = true;
				//残業時刻を設定
				$start_time = $over_start_time;
				$end_time = $over_end_time;

				//法定外残業の基準
				$wk_base_time = 0;
			} else {
				$legal_hol_over_flg = false;
			}

			// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
			if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {

				//日またがり対応 20101027
				if ($start_time > $end_time &&
						$previous_day_flag == "0") {
					$next_day_flag = "1";
				}

				//休暇時は計算しない 20090806
				if ($pattern != "10") {
					$work_workday_count = $workday_count;
				}
				if($night_duty_flag){
					//曜日に対応したworkday_countを取得する
					$work_workday_count = $work_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}
				$sums[1] = $sums[1] + $work_workday_count;
				//休日出勤のカウント方法変更 20091222
                if (check_timecard_holwk(
                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {
					$sums[2] += 1;
				}
			}

			// 事由ベースの日数計算
			$reason_day_count = $atdbk_common_class->get_reason_day_count($reason);

			$paid_time = 0;
			if ($reason_day_count > 0) {
				$am_hol_days = 0;
				$pm_hol_days = 0;
				$paid_hol_days = 0;
				//半日有休を区別して集計 20110204
				switch ($reason) {
					case "2": //午前有休
					case "38": //午前年休
					case "67": //午前特別
					case "50": //午前夏休
					case "69": //午前正休
						$sums[38] += $reason_day_count;
						$am_hol_days = 0.5;
						break;
					case "3": //午後有休
					case "39": //午後年休
					case "68": //午後特別
					case "51": //午後夏休
					case "70": //午後正休
						$sums[39] += $reason_day_count;
						$pm_hol_days = 0.5;
						break;
					case "44":  // 半有半公
					case "55":  // 半夏半有
					case "57":  // 半有半欠
					case "58":  // 半特半有
					case "62":  // 半正半有
						$sums[34] += $reason_day_count;
						$paid_hol_days = 0.5;
						break;
					default: //上記以外
                        if ($pattern == "10") { //20140106 パターンが休暇の場合
                            $sums[34] += $reason_day_count;
                            $paid_hol_days = 1.0;
                        }
				}
                //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 start
                $wk_weekday = $arr_weekday_flg[$tmp_date];
                $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date);
                if ($arr_empcond_officehours["part_specified_time"] != "") {
                    $paid_specified_time = $arr_empcond_officehours["part_specified_time"];
                    $am_time = "";
                    $pm_time = "";
                    $empcond_spec_time_flg = true;
                }
                //else { //20130521 勤務条件の所定時間を2番目に優先とする
                //    $empcond_spec_time_flg = false;
                //}
                //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 end
                //所定時間履歴を使用 20121127
                //勤務条件にない場合
                if ($empcond_spec_time_flg == false) {
                    $arr_timehist = $calendar_name_class->get_calendarname_time($tmp_date);
                    $paid_specified_time = $arr_timehist["day1_time"];
                    $am_time = $arr_timehist["am_time"];
                    $pm_time = $arr_timehist["pm_time"];
                }
                //半日有給
				$min = 0;
				//半日午前所定時間がある場合は計算し加算
				if ($am_time != "") {
					$hour2 = (int)substr($am_time, 0, 2);
					$min2 = $hour2 * 60 + (int)substr($am_time, 2, 3);
					//日数分を加算する(0.5単位のため、1回分の計算として2倍する）
					$min += $min2 * $am_hol_days * 2;
				}
				else {
					//ない場合は、半日以外の所定に加算
					$paid_hol_days += $am_hol_days;
				}
				//半日午後所定時間がある場合は計算し加算
				if ($pm_time != "") {
					$hour2 = (int)substr($pm_time, 0, 2);
					$min2 = $hour2 * 60 + (int)substr($pm_time, 2, 3);
					//日数分を加算する(0.5単位のため、1回分の計算として2倍する）
					$min += $min2 * $pm_hol_days * 2;
				}
				else {
					//ない場合は、半日以外の所定に加算
					$paid_hol_days += $pm_hol_days;
				}

				//半日以外の換算日数
				if ($paid_specified_time != "") {
					//所定労働時間を分単位にする
					$hour2 = (int)substr($paid_specified_time, 0, 2);
					$min2 = $hour2 * 60 + (int)substr($paid_specified_time, 2, 3);
					//日数分を加算する
					$min += $min2 * $paid_hol_days;
				}
				$paid_time = $min;
				$paid_time_total += $min;

			}

			//時間有休追加 20111207
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                $paid_time_hour = 0;
				$paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
				//勤務グループ指定 20150820
				if ($target_tmcd_group_id != "" &&
					$target_tmcd_group_id != $tmcd_group_id) {
					$paid_hol_min = "";
				}
                //空白以外の時、設定と計算 20120802
                if ($paid_hol_min != "") {
                    $paid_time += $paid_hol_min; //有休時間数に表示されない不具合対応 20130612 半日有休がある場合の不具合対応 20140225
                    $paid_time_hour = $paid_hol_min; //半有休等の場合に遅刻早退が計算されない不具合対応 20130530 
                    $paid_time_hour_month += $paid_hol_min;
                    $paid_time_total += $paid_hol_min;
                }
            }
			//事由により、休暇項目別日数のカウントアップ
			switch ($reason) {
				case "14":  // 代替出勤
					$sums[3] += 1;
					break;
				case "15":  // 振替出勤
					$sums[4] += 1;
					break;
				case "65":  // 午前振出
				case "66":  // 午後振出
					$sums[4] += 0.5;
					break;
				case "4":  // 代替休暇
					$sums[5] += 1;
					break;
				case "18":  // 半前代替休
				case "19":  // 半後代替休
					$sums[5] += 0.5;
					break;
				case "17":  // 振替休暇
					$sums[6] += 1;
					break;
				case "20":  // 半前振替休
				case "21":  // 半後振替休
					$sums[6] += 0.5;
					break;
				case "1":  // 有休休暇
				case "37": // 年休
                    if ($pattern == "10") { //20140106 パターンが休暇の場合
                        $sums[7] += 1;
                    }
					break;
				case "2":  // 午前有休
				case "3":  // 午後有休
				case "38": // 午前年休
				case "39": // 午後年休
					$sums[7] += 0.5;
					break;
				case "57":  // 半有半欠
					$sums[7] += 0.5;
					$sums[8] += 0.5;
					break;
				case "6":  // 一般欠勤
				case "7":  // 病傷欠勤
					$sums[8] += 1;
					break;
				case "48": // 午前欠勤
				case "49": // 午後欠勤
					$sums[8] += 0.5;
					break;
				case "8":  // その他休
					$sums[9] += 1;
					break;
				case "52":  // 午前その他休
				case "53":  // 午後その他休
					$sums[9] += 0.5;
					break;
				case "24":  // 公休
				//case "22":  // 法定休暇
				//case "23":  // 所定休暇
				case "45":  // 希望(公休)
				case "46":  // 待機(公休)
				case "47":  // 管理当直前(公休)
					$sums[16] += 1;
					break;
				case "35":  // 午前公休
				case "36":  // 午後公休
					$sums[16] += 0.5;
					break;
				case "5":   // 特別休暇
					$sums[17] += 1;
					break;
				case "67":  // 午前特別
				case "68":  // 午後特別
					$sums[17] += 0.5;
					break;
				case "58":  // 半特半有
					$sums[17] += 0.5;
					$sums[7] += 0.5;
					break;
				case "59":  // 半特半公
					$sums[17] += 0.5;
					$sums[16] += 0.5;
					break;
				case "60":  // 半特半欠
					$sums[17] += 0.5;
					$sums[8] += 0.5;
					break;
				case "25":  // 産休
					$sums[18] += 1;
					break;
				case "26":  // 育児休業
					$sums[19] += 1;
					break;
				case "27":  // 介護休業
					$sums[20] += 1;
					break;
				case "28":  // 傷病休職
					$sums[21] += 1;
					break;
				case "29":  // 学業休職
					$sums[22] += 1;
					break;
				case "30":  // 忌引
					$sums[23] += 1;
					break;
				case "31":  // 夏休
					$sums[24] += 1;
					break;
				case "50":  // 午前夏休
				case "51":  // 午後夏休
					$sums[24] += 0.5;
					break;
				case "54":  // 半夏半公
					$sums[24] += 0.5;
					$sums[16] += 0.5;
					break;
				case "55":  // 半夏半有
					$sums[24] += 0.5;
					$sums[7] += 0.5;
					break;
				case "56":  // 半夏半欠
					$sums[24] += 0.5;
					$sums[8] += 0.5;
					break;
				case "72":  // 半夏半特
					$sums[24] += 0.5;
					$sums[17] += 0.5;
					break;
				case "61":  // 年末年始
					$sums[33] += 1;
					break;
				case "69":  // 午前正休
				case "70":  // 午後正休
					$sums[33] += 0.5;
					break;
				case "62":  // 半正半有
					$sums[33] += 0.5;
					$sums[7] += 0.5;
					break;
				case "63":  // 半正半公
					$sums[33] += 0.5;
					$sums[16] += 0.5;
					break;
				case "64":  // 半正半欠
					$sums[33] += 0.5;
					$sums[8] += 0.5;
					break;
				case "32":   // 結婚休
					$sums[25] += 1;
					break;
				case "40":   // リフレッシュ休暇
					$sums[26] += 1;
					break;
				case "42":   // 午前リフレッシュ休暇
				case "43":   // 午後リフレッシュ休暇
					$sums[26] += 0.5;
					break;
				case "41":  // 初盆休暇
					$sums[27] += 1;
					break;
				case "44":  // 半有半公
					$sums[7] += 0.5;
					$sums[16] += 0.5;
					break;
			}
			// 休日となる設定と事由があう場合に公休に計算する 20100810
			// 法定休日
			if ($arr_type[$tmp_date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
				if ($reason == "22") {
					$sums[16] += 1;
				}
			}
			// 集計行に法定休日追加 20110121
			if ($reason == "22") {
				$sums[36] += 1;
			}
			// 所定休日
			if ($arr_type[$tmp_date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
				if ($reason == "23") {
					$sums[16] += 1;
				}
			}
			// 集計行に所定休日追加 20110121
			if ($reason == "23") {
				$sums[37] += 1;
			}
			// 年末年始
			if ($arr_type[$tmp_date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
				if ($reason == "61") {
					$sums[16] += 1;
				}
				if ($reason == "62" || $reason == "63" || $reason == "64") {
					$sums[16] += 0.5;
				}
			}
		}

		// 各時間帯の開始時刻・終了時刻を変数に格納
		//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
		$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
		$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
		$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
		$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
		$end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
		$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
		$end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

        //個人別所定時間を優先する場合 20120309
        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
            $wk_weekday = $arr_weekday_flg[$tmp_date];
            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130220
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $start2 = $arr_empcond_officehours["officehours2_start"];
                $end2 = $arr_empcond_officehours["officehours2_end"];
                $start4 = $arr_empcond_officehours["officehours4_start"];
                $end4 = $arr_empcond_officehours["officehours4_end"];
            }
        }

        // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
		if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
			$start2 = $prov_start_time;
			$end2 = $prov_end_time;
		}

		$over_calc_flg = false;	//残業時刻 20100525
		//残業時刻追加 20100114
		//残業開始が予定終了時刻より前の場合に設定
		if ($over_start_time != "") {
			//残業開始日時
			if ($over_start_next_day_flag == "1") {
				$over_start_date = next_date($tmp_date);
			} else {
				$over_start_date = $tmp_date;
			}

			$over_start_date_time = $over_start_date.$over_start_time;

			$over_calc_flg = true;
			//残業終了時刻 20100705
			$over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
			$over_end_date_time = $over_end_date.$over_end_time;
		}
		// 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
		$effective_time = 0;

		//一括修正のステータスがある場合は設定
		$tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
		if ($tmmd_apply_status == "" && $tmp_status != "") {
			$tmmd_apply_status = $tmp_status;
			$tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
		}
		//tmmd_apply_statusからlink_type取得
		$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
		$modify_apply_id  = $tmmd_apply_id;

		// 残業情報を取得
		//ovtm_apply_statusからlink_type取得
        /*未使用
		$night_duty_time_array  = array();
		$duty_work_time         = 0;
		if ($night_duty_flag){

			//日数換算の数値ｘ通常勤務日の労働時間の時間
			$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
		}
        */
		$overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
		$overtime_apply_id  = $ovtm_apply_id;
        
		//rtn_apply_statusからlink_type取得
		$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
		$return_apply_id  = $rtn_apply_id;

		// 所定労働・休憩開始終了日時の取得
        // 所定開始時刻が前日の場合 20121120
        if ($atdptn_previous_day_flag == "1") {
			$tmp_prev_date = last_date($tmp_date);
			$start2_date_time = $tmp_prev_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_prev_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
		} else {
			$start2_date_time = $tmp_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_end_date = next_date($tmp_date);
			} else {
				$tmp_end_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
		}

		//休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
		if ($rest_start_time != "" && $rest_end_time != "") {
			$start4 = $rest_start_time;
			$end4 = $rest_end_time;
		}
		//休憩時刻の日またがり確認 20141104
        $wk_start2 = ($start2 != "") ? $start2 : $start_time; //所定時刻なしの休憩時間計算不具合対応 20141128
        if ($previous_day_flag == "1" || $atdptn_previous_day_flag == "1") { //前日フラグがある場合 20150302
			$tmp_last_date = last_date($tmp_date);
			$start4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_last_date, $wk_start2, $start4);
			$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_last_date, $wk_start2, $end4);
		}
		else {
			$start4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $wk_start2, $start4);
			$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $wk_start2, $end4);
		}
        //if ($arr_absence_data[$tmp_date]["apply_date"] != "") {
			$arr_absence_data[$tmp_date]["start4_date_time"] = $start4_date_time;
			$arr_absence_data[$tmp_date]["end4_date_time"] = $end4_date_time;
		//}

		//外出復帰日時の取得
		$out_date_time = $tmp_date.$out_time;
		$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

		//時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持 20101217
		if ($tmp_date >= $wk_start_date) {
			$work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
		}

		// 処理当日の所定労働時間を求める
		if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
			$specified_time = 0;
		} else {
			//時間計算変更 20101217
			$specified_time = date_utils::get_diff_minute($end2_date_time, $start2_date_time);
			if ($start4 != "" && $end4 != "") {
				$wk_rest_time = date_utils::get_diff_minute($end4_date_time, $start4_date_time);
				$specified_time -= $wk_rest_time;
			}
		}

		//出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 start
		if ($over_start_time != "" && $over_end_time != "") {
			if ($start_time == "") {
				$start_time = $start2;
				$previous_day_flag = $atdptn_previous_day_flag; //時間帯設定の前日フラグ
			}
			if ($end_time == "") {
				$end_time = $end2;
				//日またがり対応
				if ($start_time > $end_time &&
						$previous_day_flag == "0") {
					$next_day_flag = "1";
				}
			}
		}
		//出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 end
		// 出退勤とも打刻済みの場合
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {

			// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
			$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
			$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
			if (empty($start2_date_time) || empty($end2_date_time)) {
				$start2_date_time = $start_date_time;
				$end2_date_time    = $end_date_time;
			}

			$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);
			//未申請を表示しない設定追加 20100809
			//休暇残業の場合の残業時間計算 20101228 //明けを追加 20110819
			if (($pattern == "10" || $after_night_duty_flag == "1") && $over_start_time != "" && $over_end_time != "") {
				$diff_min_end_time = date_utils::get_diff_minute($over_end_date_time ,$over_start_date_time);
			}
			else {
				if ($end_date_time > $end2_date_time) { //20101118
					$diff_min_end_time = date_utils::get_diff_minute($end_date_time ,$end2_date_time);
				} else {
					$diff_min_end_time = "";
				}
			}
			// 残業管理をする場合、（しない場合には退勤時刻を変えずに稼動時間の計算をする）
			if ($no_overtime != "t") {
				// 残業申請中の場合は定時退勤時刻を退勤時刻として計算
				// 残業未承認か確認
				if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
					if (date_utils::to_timestamp_from_ymdhi($start2_date_time) > date_utils::to_timestamp_from_ymdhi($start_date_time)) { //所定より出勤が早い場合には所定にする 20100914 時刻に:があるため遅刻時間の表示されない不具合 20101005
						$start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
					}
					if ($end_date_time > $end2_date_time) {
						$end_date_time = $end2_date_time;
					}
				}
			}

			//時間計算変更 20101217
			$wk_work_time = date_utils::get_diff_minute($end_date_time, $start_date_time);

			//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
			if ($wk_work_time > 0){
				//休日で残業がある場合 20101028
				if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
					//深夜残業の開始終了時間
					$start5 = "2200";
					$end5 = "0500";
				}
				//時間計算変更 20101217
				$late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報

				// 確定出勤時刻／遅刻回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
					$start_time_info = array();
				} else {
					//時間計算変更 20101217
					$start_time_info = $timecard_common_class->get_start_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction1, $fraction1_min, $fraction3);
					$fixed_start_time = $start_time_info["fixed_time"];
				}

				// 確定退勤時刻／早退回数を取得
				if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);

					$end_time_info = array();
				} else {
					//残業時刻が入っている場合は、端数処理しない 20100806
					$wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
                    if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time && $end_date_time >= $end2_date_time) { //時間が所定終了よりあとか確認20111128 //早退の場合を除く条件追加 20140902
						$fixed_end_time = $wk_over_end_date_time;
					} else {
					//時間計算変更 20101217
						$end_time_info = $timecard_common_class->get_end_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction2, $fraction2_min, $fraction3);
						$fixed_end_time = $end_time_info["fixed_time"];
					}
					//※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
					//残業申請画面を表示しないは除く 20100811
					if ($timecard_bean->over_time_apply_type != "0") {
						if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
							//退勤時刻が所定よりあとの場合設定 20101224
							if ($fixed_end_time > $end2_date_time) {
								$fixed_end_time = $end2_date_time;
							}
						}
					}
				}
				//時間有休がある場合は、遅刻早退を計算しない 20111207  //半有休等の場合に遅刻早退が計算されない不具合対応 20130530 
                if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
					//時間有休と相殺する場合
                    if ($ovtm_class->sousai_sinai_flg != "t") {
                        //遅刻
                        $tikoku_time = 0;
                        $start_time_info["diff_minutes"] = 0;
                        $start_time_info["diff_count"] = 0;
                        //早退
                        $sotai_time = 0;
                        $end_time_info["diff_minutes"] = 0;
                        $end_time_info["diff_count"] = 0;
                    }
				}
				if ($tmp_date >= $wk_start_date) {
					//遅刻
					$sums[10] += $start_time_info["diff_count"];
					$delay_time += $start_time_info["diff_minutes"];
					//早退
					$sums[11] += $end_time_info["diff_count"];
					$early_leave_time += $end_time_info["diff_minutes"];
				}

				$tmp_start_time = $fixed_start_time;
				$tmp_end_time = $fixed_end_time;

				//休日勤務時間（分）と休日出勤を取得 2008/10/23
				if ($tmp_date >= $wk_start_date) {
					//休暇時は計算しない 20090806
					//休日出勤の判定方法変更 20100721
					if (check_timecard_holwk(
								$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {
						//時間計算変更 20101217
						//所定開始時刻と残業開始時刻が一致している場合
						if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {
							$wk_end_date_time = $over_end_date_time;
						} else {
							$wk_end_date_time = $tmp_end_time;
						}
						$arr_hol_time = $timecard_common_class->get_holiday_work2($arr_type, $tmp_date, $start_date_time, $previous_day_flag, $wk_end_date_time, $next_day_flag, $reason, $work_times_info, $out_time, $ret_time);


						// 休日出勤した日付を配列へ設定
						for ($i=4; $i<7; $i++) {
							if ($arr_hol_time[$i] != "") {
								$arr_hol_work_date[$arr_hol_time[$i]] = 1;
							}
						}
					}
				}

				// 普外時間を算出（分単位）
				// 外出時間を差し引かないフラグ対応 20110727
				if ($out_time_nosubtract_flag != "1") {
					//時間計算変更 20101217
					if ($out_time != "" && $ret_time != "") { //外出 20101109
						$time3 = date_utils::get_diff_minute($ret_date_time, $out_date_time);
					}
				}


				// 実働時間を算出（分単位）
				//時間計算変更 20101217
				$effective_time = date_utils::get_diff_minute($fixed_end_time, $fixed_start_time);
				//予定終了時刻から残業開始時刻の間を除外する 20120517
                //if ($over_start_time != "") { //20130826 他画面と合わせるためコメント化
				//	$wk_end_date_time = $end2_date_time;
                //    //残業開始時刻があとの場合
                //    if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
                //        //時間を取得し減算
                //        $wk_jogai = date_utils::get_diff_minute($over_start_date_time, $wk_end_date_time);
                //        $effective_time -= $wk_jogai;
                //    }
                //}
                $tmp_start_time = str_replace(":", "", $start_time);
				$time3_rest = 0;
				$time4_rest = 0;
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					if ($effective_time > 1200) {  // 20時間を超える場合 20100929
						$effective_time -= $rest7;
						$time3_rest = 0;
						$time4_rest = $rest7;
					} else if ($effective_time > 720) {  // 12時間を超える場合
						$effective_time -= $rest6;
						$time3_rest = 0;
						$time4_rest = $rest6;
					} else if ($effective_time > 540) {  // 9時間を超える場合
						$effective_time -= $rest3;
						$time3_rest = 0;
						$time4_rest = $rest3;
					} else if ($effective_time > 480) {  // 8時間を超える場合
						$effective_time -= $rest2;
						$time3_rest = $rest2;
						$time4_rest = 0;
					} else if ($effective_time > 360) {  // 6時間を超える場合
						$effective_time -= $rest1;
						$time3_rest = $rest1;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time < "1200") {  // 4時間超え午前開始の場合
						$effective_time -= $rest4;
						$time3_rest = $rest4;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time >= "1200") {  // 4時間超え午後開始の場合
						$effective_time -= $rest5;
						$time3_rest = $rest5;
						$time4_rest = 0;
					}
					//休日残業で残業申請中は休憩を表示しない 20100916
					if ($legal_hol_over_flg /*&& $overtime_approve_flg == "1"*/) {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//休日で残業がない場合,出勤退勤がある場合 20100917
					if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//開始した日が休日の場合に休日勤務から休憩を減算 2008/10/23 //休日残業から休憩を減算しない 20111216
					//if (($previous_day_flag == true && $arr_hol_time[0] > 0) ||
					//		($previous_day_flag == false && $arr_hol_time[1] > 0)) {
					//	$sums[31] = $sums[31] - $time3_rest - $time4_rest;
					//}
				} else {
					//休憩時間 20101217
					if ($start4 != "" && $end4 != "") {
                        //所定労働時間との重なりを計算する 20140820
                        if ($start2_date_time != "" && $end2_date_time != "") {
                            $tmp_rest2 = $timecard_common_class->get_intersect_times($start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time);
                        }
                        else {
						    $tmp_rest2 = date_utils::get_diff_minute($end4_date_time, $start4_date_time);
                        }
					}
				}

				// 稼働時間を算出（分単位）
				// 半年休の時間をプラスしない 20100330
				$work_time = $effective_time - $time3 - $time4;

				//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
				if ($pattern == "10" && !$legal_hol_over_flg) {
					$work_time = 0;
				}

				// 退勤後復帰回数・時間を算出
				$return_count = 0;
				$return_time = 0;
				$return_late_time = 0; //20100715
				//申請状態 "0":申請不要 "3":承認済
				if (($return_link_type == "0" || $return_link_type == "3")) {
					for ($i = 1; $i <= 10; $i++) {
						$start_var = "o_start_time$i";
						if ($$start_var == "") {
							break;
						}
						$return_count++;

						$end_var = "o_end_time$i";
						if ($$end_var == "") {
							break;
						}

						//端数処理 20090709
						if ($fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							//開始日時
							$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
							$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_start_time = str_replace(":", "", $$start_var);
						}
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//終了日時
							$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

							$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_end_time = str_replace(":", "", $$end_var);
						}
						//時間計算変更 20101217
						$wk_ret_start = $tmp_date.$tmp_ret_start_time;
						$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
						$wk_ret_time = date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
						$return_time += $wk_ret_time;

						// 月の深夜勤務時間に退勤後復帰の深夜分を追加
						//時間計算変更 20101217
						$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");
						$return_late_time += $wk_late_night;
                        //当日が休日の場合、休日に合計。
                        if (($pattern != "10" && check_timecard_holwk(
                                        $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) ||
                                $reason == "16" || //休日出勤
                                ($pattern == "10" && (($reason != "71"
                                            && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f")))  //事由が普通残業の場合を除く、公休を休日残業としない 20140512
                                ) {
							//当日の残業
							$today_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
							//$sums[31] += $today_over;
                            //echo "1 $tmp_date sums[31]".$sums[31];
                            //$hol_over_time += $today_over;
						}
						//翌日の残業
						$next_date = next_date($tmp_date);
                        /*
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //勤務パターンと事由変更 20130308
							$nextday_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "2");
							$sums[31] += $nextday_over;
                            //echo "2 $tmp_date sums[31]".$sums[31];
                            $hol_over_time += $nextday_over;
						}
                        */
						//残業時間データ
                        $arr_overtime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $wk_ret_start, $wk_ret_end, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class); //20130308
						//普通残業
						$normal_over += $arr_overtime_data["normal_over"];
						//深夜
						$late_night += $arr_overtime_data["late_night"];
						//休日残業
						$hol_over += $arr_overtime_data["hol_over"];
						//休日深夜
						$hol_late_night += $arr_overtime_data["hol_late_night"];
						//６０時間超計算用休日残業
						$hol_over_sixty_data += $arr_overtime_data["hol_over_sixty_data"];
						//６０時間超計算用休日深夜
						$hol_late_night_sixty_data += $arr_overtime_data["hol_late_night_sixty_data"];
						//45時間超計算用休日残業
						$hol_over_fortyfive_data += $arr_overtime_data["hol_over_sixty_data"];
						//45時間超計算用休日深夜
						$hol_late_night_fortyfive_data += $arr_overtime_data["hol_late_night_sixty_data"];
					}//呼出分
				}
				$wk_return_time = $return_time;

				if ($tmp_date < $wk_start_date) {
					$tmp_date = next_date($tmp_date);
					continue;
				}
				// 深夜勤務時間を算出（分単位）
				$time2 = 0;
				//計算方法変更 20110126
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0") {
					//残業時刻がない場合で、所定終了、退勤を比較後残業計算
					if ($over_start_time == "" || $over_end_time == "") {
						//if ($end2_date_time < $fixed_end_time) { //退勤時刻を使用しない 20120406
						//	$over_start_date_time = $end2_date_time;
						//	$over_end_date_time = $fixed_end_time;
						//} else {
							$over_start_date_time = ""; //20101109
							$over_end_date_time = "";
						//}
					}
					//残業時間データ
					$next_date = next_date($tmp_date); //20140303 残業時間集計不具合対応、翌日が休暇の場合
                    //休憩除外 20140702
                    $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                    $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                    if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                        $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                        $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;
                    }
                    else {
                        $rest_start_date_time1 = "";
                        $rest_end_date_time1 = "";
                    }
                    $arr_overtime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time, $over_end_date_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class, $rest_start_date_time1, $rest_end_date_time1, $end2_date_time); //20130308
					//普通残業
					$normal_over += $arr_overtime_data["normal_over"];
					//深夜
					$late_night += $arr_overtime_data["late_night"];
					//休日残業
					$hol_over += $arr_overtime_data["hol_over"];
					//休日深夜
					$hol_late_night += $arr_overtime_data["hol_late_night"];
					//６０時間超計算用休日残業
					$hol_over_sixty_data += $arr_overtime_data["hol_over_sixty_data"];
					//６０時間超計算用休日深夜
					$hol_late_night_sixty_data += $arr_overtime_data["hol_late_night_sixty_data"];
					//45時間超計算用休日残業
					$hol_over_fortyfive_data += $arr_overtime_data["hol_over_sixty_data"];
					//45時間超計算用休日深夜
					$hol_late_night_fortyfive_data += $arr_overtime_data["hol_late_night_sixty_data"];
                    
                    //残業時間深夜内休憩
                    $overtime_rest_night = 0;
                    /*
                    if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                        //深夜内の休憩
                        $overtime_rest_night = 
                              $arr_resttime_data["late_night"]
                            + $arr_resttime_data["hol_late_night"];
                    }
                    */                    
					//深夜残業
                    $time2 = $arr_overtime_data["late_night"] + $arr_overtime_data["hol_late_night"];
                    /*
					$time2 = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");
                    //休憩除外 20140708
                    if ($time2 > $overtime_rest_night) {
                        $time2 -= $overtime_rest_night;
                    }
                    */
					//休日残業
					//当日
                    if (($pattern != "10" && check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) ||
							$reason == "16" || //休日出勤
                            ($pattern == "10" && $over_start_time != "" && $over_end_time != "" &&
                                $overtime_approve_flg != "1" && (($reason != "71"
                                        && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //休暇で残業時刻がある場合、公休を休日残業としない 20140512
						) {
                        $today_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "1", $timecard_bean);
                        //休憩除外
                        if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                            $today_over_rest = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time1,$rest_end_date_time1, "1");
                            $today_over -= $today_over_rest;
                        }
						$hol_over_time += $today_over;
					}
					//翌日フラグ
					if ($next_day_flag == "1" || $over_end_next_day_flag == "1") {
						$next_date = next_date($tmp_date);
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //勤務パターンと事由変更 20130308
							$nextday_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "2");
                            //休憩除外
                            if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                                $nextday_over_rest = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time1,$rest_end_date_time1, "2");
                                $nextday_over -= $nextday_over_rest;
                            }
                            //$sums[31] += $nextday_over;
							$hol_over_time += $nextday_over;
                            
                            //echo "$tmp_date nextday_over=$nextday_over <br>\n";
						}
					}
                    //残業３−５を追加、２−５の繰り返し処理とする
					//残業２
                    for ($idx=2; $idx<=5; $idx++) {
                    $s_t = "over_start_time".$idx;
                    $e_t = "over_end_time".$idx;
                    $s_f = "over_start_next_day_flag".$idx;
                    $e_f = "over_end_next_day_flag".$idx;
                    $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                    $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                    $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                    $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                    
                    if ($over_start_time_value != "" && $over_end_time_value != "") {
						//開始日時
                        $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
						//終了日時
                        $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;
                        $rs_t = "rest_start_time".$idx;
                        $re_t = "rest_end_time".$idx;
                        $rs_f = "rest_start_next_day_flag".$idx;
                        $re_f = "rest_end_next_day_flag".$idx;
                        $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                        $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                        $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                        $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];

                        //残業時間深夜内休憩
                        $overtime_rest_night = 0;
                            
                            if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                            }
                            else {
                                $rest_start_date_time_value = "";
                                $rest_end_date_time_value = "";
                            }

                            //echo "$tmp_date over_start_date_time_value=$over_start_date_time_value  over_end_date_time_value=$over_end_date_time_value rest_start_date_time_value=$rest_start_date_time_value rest_end_date_time_value=$rest_end_date_time_value <br>\n";
                            //残業時間データ
                            $arr_overtime_data2 = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time_value, $over_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class, $rest_start_date_time_value, $rest_end_date_time_value, $end2_date_time); //20141106 公休以外から翌日に続く残業の集計不具合対応

						//普通残業
                        $normal_over += $arr_overtime_data2["normal_over"];
						//深夜
                        $late_night += $arr_overtime_data2["late_night"];
						//休日残業
                        $hol_over += $arr_overtime_data2["hol_over"];
						//休日深夜
                        $hol_late_night += $arr_overtime_data2["hol_late_night"];
						//６０時間超計算用休日残業
						$hol_over_sixty_data += $arr_overtime_data2["hol_over_sixty_data"];
						//６０時間超計算用休日深夜
						$hol_late_night_sixty_data += $arr_overtime_data2["hol_late_night_sixty_data"];
						//45時間超計算用休日残業
						$hol_over_fortyfive_data += $arr_overtime_data2["hol_over_sixty_data"];
						//45時間超計算用休日深夜
						$hol_late_night_fortyfive_data += $arr_overtime_data2["hol_late_night_sixty_data"];
                        
						//深夜時間帯の時間取得
                        $over_time2_late_night_old = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time_value, $over_end_date_time_value, "");
                        $over_time2_late_night = $arr_overtime_data2["late_night"] + $arr_overtime_data2["hol_late_night"];
//                            echo "$tmp_date over_time2_late_night_old=$over_time2_late_night_old  over_time2_late_night=$over_time2_late_night <br>\n";
                            $time2 += $over_time2_late_night;
						//休日残
						//当日
                            if (($pattern != "10" && check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) ||
								$reason == "16" || //休日出勤
                                ($pattern == "10" &&
                                        $overtime_approve_flg != "1" && (($reason != "71"
                                                && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //休暇で残業時刻がある場合
							) {
                            $today_over2 = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time_value, $over_end_date_time_value, "1");
							//$sums[31] += $today_over2;
                                //休憩除外
                                if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                    $today_over2_rest = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time_value,$rest_end_date_time_value, "1");
                                    $today_over2 -= $today_over2_rest;
                                }
                                $hol_over_time += $today_over2;
						}
						//翌日フラグ
                        if ($next_day_flag == "1" || $over_end_next_day_flag_value == "1") {
							$next_date = next_date($tmp_date);
                                if (check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //勤務パターンと事由変更 20130308
                                $nextday_over2 = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time_value, $over_end_date_time_value, "2");
								//$sums[31] += $nextday_over2;
                                    //休憩除外
                                    if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                        $nextday_over2_rest = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time_value,$rest_end_date_time_value, "2");
                                        $nextday_over2 -= $nextday_over2_rest;
                                    }
                                    $hol_over_time += $nextday_over2;
							}
						}
					}
                    }
				}
				$time2 += $return_late_time; //20100715
                //休憩処理 20130821
                //所定開始時刻と残業開始時刻が一致している場合のフラグ設定
                if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {
                    $overtime_only_flg = true;
                }
                else {
                    $overtime_only_flg = false;
                }
                //日毎の集計$hol_over_time から休憩を減算し、月毎の集計へ加算$sums[31]
                if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合
                    ;
                }
                elseif ($start4 != "" && $end4 != "") {
                    if ($legal_hol_over_flg || $overtime_only_flg) {
                        $next_date = next_date($tmp_date);
                        $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $start4_date_time, $end4_date_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"]);
                        if ($normal_over >= $arr_resttime_data["normal_over"]) {
                            $normal_over -= $arr_resttime_data["normal_over"];
                        }
                        if ($late_night >= $arr_resttime_data["late_night"]) {
                            $late_night -= $arr_resttime_data["late_night"];
                        }
                        //if ($hol_over_time >= $arr_resttime_data["hol_over"]) {
                        //    $hol_over_time -= $arr_resttime_data["hol_over"];
                        //}
                        if ($hol_over >= $arr_resttime_data["hol_over"]) {
                            $hol_over -= $arr_resttime_data["hol_over"];
                        }
                        if ($hol_late_night >= $arr_resttime_data["hol_late_night"]) {
                            $hol_late_night -= $arr_resttime_data["hol_late_night"];
                        }
                    }
                    
                }
                //$sums[31] += $hol_over_time;
                $sums[31] += $hol_over;
                
				// 深夜残業、休日深夜残業を分ける 20130129
				if ($shift_flg){
					//$sums[15] = $sums[15] + $time2 - $hol_late_night;
                    $sums[15] = $sums[15] + $late_night; //20130308
                    $sums[40] += $hol_late_night;
				}else{
					//$sums[15] += $time2;
                    $sums[15] += $late_night + $hol_late_night;
                }

				// 表示値の編集
				$time1 = minute_to_hmm($time1);
				//$time2 = minute_to_hmm($time2);
				// 普外−＞外出に変更 20090603
				$time_outtime = minute_to_hmm($time3);
				$out_time_total += $time3;
				if ($time4 > 0) {
					$time_outtime .= "(".minute_to_hmm($time4).")";
				}
				// 残外−＞休憩に変更 20090603
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
                    $time_rest = minute_to_hmm($time3_rest);
					$rest_time_total += $time3_rest;
					if ($time4_rest > 0) {
						$time_rest .= "(".minute_to_hmm($time4_rest).")";
						$rest_time_total += $time4_rest;
					}
				} else {
					// 休憩時間
					$time_rest = minute_to_hmm($tmp_rest2);
					$rest_time_total += $tmp_rest2;
				}

				$return_time_total += $return_time;
				$return_time = minute_to_hmm($return_time);
            } else {
				// 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
				// 月の開始日前は処理しない
				if ($tmp_date < $wk_start_date) {
					$tmp_date = next_date($tmp_date);
					continue;
				}
			}
		} else {
			$time1 = "";
			$time2 = "";
			$time3 = "";
			$time4 = "";
			$time_outtime = "";
			$time_rest = "";
			$return_count = "";
			$return_time = "";

			// 事由が「代替休暇」「振替休暇」の場合、
			// 所定労働時間を稼働時間にプラス
			// 「有給休暇」を除く $reason == "1" || 2008/10/14
			if ($reason == "4" || $reason == "17") {
				if ($tmp_date >= $wk_start_date) {
					$sums[13] += $specified_time;
				}
			}
			// 退勤後復帰回数を算出
			$return_count = 0;
			// 退勤後復帰時間を算出 20100413
			$return_time = 0;
            //申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない //勤務時間を計算しないフラグ確認 20130225
            if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
				for ($i = 1; $i <= 10; $i++) {
					$start_var = "o_start_time$i";
					if ($$start_var == "") {
						break;
					}
					$return_count++;

					$end_var = "o_end_time$i";
					if ($$end_var == "") {
						break;
					}
					//端数処理
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						//開始日時
						$tmp_start_date_time = $tmp_date.$$start_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
						$tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_start_time = str_replace(":", "", $$start_var);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						//終了日時
						$tmp_end_date_time = $tmp_date.$$end_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

						$tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_end_time = str_replace(":", "", $$end_var);
					}
					//時間計算変更 20101217
					$wk_ret_start = $tmp_date.$tmp_ret_start_time;
					$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
					$wk_ret_time = date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
					$return_time += $wk_ret_time;

				}
			}
			$wk_return_time = $return_time;
			$return_time_total += $return_time;
			//分を時分に変換
			$return_time = minute_to_hmm($return_time);

			if ($tmp_date < $wk_start_date) {
				$tmp_date = next_date($tmp_date);
				continue;
			}
		}

		// 残業時間の処理を変更 20090629
		$wk_hoteigai = 0;
		$wk_hoteinai_zan = 0;
		$wk_hoteinai = 0;

		//日数換算
		$total_workday_count = "";
		if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
		{
			//休暇時は計算しない 20090806
			if ($pattern != "10") {
				if($workday_count != "")
				{
					$total_workday_count = $workday_count;
				}
			}

			if($night_duty_flag)
			{
				//曜日に対応したworkday_countを取得する
				$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

			}
		}
		else if ($workday_count == 0){
			//日数換算に0が指定された場合のみ0を入れる
			$total_workday_count = $workday_count;
		}
		//法定外残業の基準時間
		if ($base_time == "") {
			$wk_base_time = 8 * 60;
		} else {
			$base_hour = intval(substr($base_time, 0, 2));
			$base_min = intval(substr($base_time, 2, 2));
			$wk_base_time = $base_hour * 60 + $base_min;
		}
		//公休か事由なし休暇で残業時刻がありの場合 20100802
		if ($legal_hol_over_flg) {
			$wk_base_time = 0;
		}
		//残業時間 20100209 変更 20100910
		$wk_zangyo = 0;
		$kinmu_time = 0;
		$over_rest_time_day = 0; //残業休憩日毎 20141209
		//出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
        //勤務時間を計算しないフラグが"1"以外 20130225
		if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
			//残業承認時に計算、申請画面を表示しない場合は無条件に計算
			if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
					$timecard_bean->over_time_apply_type == "0"
				) {
				
                //残業開始、終了時刻がある場合
				if ($over_start_time != "" && $over_end_time != "") {
					$wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
//休憩除外 20140702
                    $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                    $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                    if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                        $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $wk_rest_time1 = date_utils::get_time_difference($rest_end_date1.$rest_end_time1,$rest_start_date1.$rest_start_time1);
                        $wk_zangyo -= $wk_rest_time1;
                        //残業休憩日毎 20141209
                        $over_rest_time_day += $wk_rest_time1;
                    }
                    
				//残業時刻未設定は計算しない 20110825
				//} else {
				//	//残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
				//	if ($end2_date_time < $fixed_end_time) {
				//		$wk_zangyo = date_utils::get_time_difference($fixed_end_time,$end2_date_time);
				//	}
				}
                //早出残業
				if ($early_over_time != "") {
					$wk_early_over_time = hmm_to_minute($early_over_time);
					$wk_zangyo += $wk_early_over_time;
				}
                //残業３−５を追加、２−５の繰り返し処理とする
                //残業２
                for ($idx=2; $idx<=5; $idx++) {
                    $s_t = "over_start_time".$idx;
                    $e_t = "over_end_time".$idx;
                    $s_f = "over_start_next_day_flag".$idx;
                    $e_f = "over_end_next_day_flag".$idx;
                    $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                    $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                    $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                    $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                    if ($over_start_time_value != "" && $over_end_time_value != "") {
                        //開始日時
                        $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
						//終了日時
                        $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;
                        $wk_over_time2 = date_utils::get_time_difference($over_end_date_time_value, $over_start_date_time_value);
                        //休憩除外 20140702
                        $rs_t = "rest_start_time".$idx;
                        $re_t = "rest_end_time".$idx;
                        $rs_f = "rest_start_next_day_flag".$idx;
                        $re_f = "rest_end_next_day_flag".$idx;

                        $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                        $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                        $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                        $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];
                        if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                            
                            $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                            $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                            $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                            $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                            $wk_rest_over_time2 = date_utils::get_time_difference($rest_end_date_time_value, $rest_start_date_time_value);
                            $wk_over_time2 -= $wk_rest_over_time2;
	                        //残業休憩日毎 20141209
	                        $over_rest_time_day += $wk_rest_over_time2;
                        }
                        //echo "ov2 $tmp_date over_start_time_value=$over_start_time_value wk_over_time2=$wk_over_time2<br>";
                        $wk_zangyo += $wk_over_time2;
                        //休憩時間確認
                        //if ($start4 != "" && $end4 != "") {
                        //	$wk_kyukei = $timecard_common_class->get_intersect_times($over_start_date_time2, $over_end_date_time2, $start4_date_time, $end4_date_time);
                        //	$wk_zangyo -= $wk_kyukei;
                        
                        //}
                    }
                }
                
			}
            //echo "$tmp_date kinmu=$kinmu_time shotei_time=$shotei_time zangyo=$wk_zangyo<br>";
            //呼出
			$wk_zangyo += $wk_return_time;

            //減算しない 20130822
			//if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
			//	//休日残業時は、休憩減算 20100916
			//	if ($legal_hol_over_flg == true) {
			//		if ($wk_zangyo >= ($time3_rest + $time4_rest)) {
			//			$wk_zangyo -= ($time3_rest + $time4_rest);
			//		}
			//	}
			//}
			//休日残業で休憩時刻がある場合 20100921
			if ($legal_hol_over_flg == true && $tmp_rest2 > 0) {
				$wk_zangyo -= $tmp_rest2;
			}
			//所定開始時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
			if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
				$overtime_only_flg = true;
				//遅刻
				$tikoku_time = 0;
				$start_time_info["diff_minutes"] = 0;
				//早退
				$sotai_time = 0;
				$end_time_info["diff_minutes"] = 0;
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100922
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0"
					) {
					$wk_zangyo -= $tmp_rest2;
				} else {
					$tmp_rest2 = 0;
					$time_rest = "";
				}
			}
			else {
				$overtime_only_flg = false;
                $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                //時間有休と相殺しない場合で時間有休ある場合 20140807
                if ($ovtm_class->sousai_sinai_flg == "t" && $paid_hol_min != "") {
                    //遅刻
                    $tikoku_time = 0;
                    //早退
                    $sotai_time = 0;
                }
                else {
                    //遅刻
                    $tikoku_time = $start_time_info["diff_minutes"];
                    //早退
                    $sotai_time = $end_time_info["diff_minutes"];
                }
			}
			// 遅刻時間を残業時間から減算する場合
			if ($timecard_bean->delay_overtime_flg == "1") {
				if ($wk_zangyo >= $tikoku_time) { //20100913
					$wk_zangyo -= $tikoku_time;
					$tikoku_time2 = 0;
				} else { // 残業時間より多い遅刻時間対応 20100925
					$tikoku_time2 = $tikoku_time - $wk_zangyo;
					$wk_zangyo = 0;
				}
			}

			//勤務時間＝所定労働時間−時間有休（ある場合減算 20111207）−休憩時間−外出時間−早退−遅刻＋残業時間
			//　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
			//　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
			//所定
			if ($pattern == "10" || //休暇は所定を0とする 20100916
					$overtime_only_flg == true ||	//残業のみ計算する 20100917
					$after_night_duty_flag == "1"  //明けの場合 20110819
				) {
				$shotei_time = 0;
			} elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
				$shotei_time = date_utils::get_diff_minute($fixed_end_time, $fixed_start_time); // 20101217
			} else {
				$shotei_time = date_utils::get_diff_minute($end2_date_time, $start2_date_time); // 20101217
			}
			//時間有休対応 20111207 時間数確認 20120802
            $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
            if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_hol_min != "") {
                //時間有休と相殺しない場合
                if ($ovtm_class->sousai_sinai_flg == "t") {
                    //遅刻早退が時間有休より多い場合、その分は勤務時間から減算されるようにする
                    if ($start_time_info["diff_minutes"] != "") {
                        if ($start_time_info["diff_minutes"] >= $paid_hol_min) {
                            $shotei_time -= $start_time_info["diff_minutes"];
                        }
                        else {
                            $shotei_time -= $paid_hol_min;
                        }
                    }
                    if ($end_time_info["diff_minutes"] != "") {
                        if ($end_time_info["diff_minutes"] >= $paid_hol_min) {
                            $shotei_time -= $end_time_info["diff_minutes"];
                        }
                        else {
                            $shotei_time -= $paid_hol_min;
                        }
                    }
                }
                else {   
                    $shotei_time -= $paid_hol_min;
                }
			}
            //休憩 $tmp_rest2 $time3_rest $time4_rest
			//外出 $time3 $time4
			$kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
            // 遅刻時間を残業時間から減算しない場合
			if ($timecard_bean->delay_overtime_flg == "2") {
				$kinmu_time -= $tikoku_time;
			}
			// 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
			else {
				if ($overtime_approve_flg == "1") {
					$kinmu_time -= $tikoku_time;
				} else { //遅刻時間不具合対応 20101005
					// 残業時間より多い遅刻時間対応 20100925
					$kinmu_time -= $tikoku_time2;
				}
			}
            //echo "<br>$tmp_date kinmu_time=$kinmu_time ";
            // 早退時間を残業時間から減算しない場合
			if ($timecard_bean->early_leave_time_flg == "2") {
				$kinmu_time -= $sotai_time;
			}
			//時間給者 20100915
			if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
				//以下の条件を使用しない 20130822
                //休日残業時は、休憩を減算しない 20100916
                //if ($legal_hol_over_flg != true) {
					$kinmu_time -= ($time3_rest + $time4_rest);
				//}
			} else {
				if ($overtime_only_flg != true && $legal_hol_over_flg != true) {	//残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
					//月給制日給制、実働時間内の休憩時間
					$kinmu_time -= $tmp_rest2;
				}
			}
            
			//法定内、法定外残業の計算
			if ($wk_zangyo > 0) {
				//法定内入力有無確認
				if ($legal_in_over_time != "") {
					$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
					if ($wk_hoteinai_zan <= $wk_zangyo) {
						$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
					} else {
						$wk_hoteigai = 0;
					}

				} else {
					//法定外残業の基準時間を超える分を法定外残業とする
					if ($kinmu_time > $wk_base_time) {
						$wk_hoteigai = $kinmu_time - $wk_base_time;
					}
					//
					if ($wk_zangyo - $wk_hoteigai > 0) {
						$wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
					} else {
						//マイナスになる場合
						$wk_hoteinai_zan = 0;
						$wk_hoteigai = $wk_zangyo;
					}
				}
                //echo "ov2 $tmp_date wk_hoteigai=$wk_hoteigai tmp_rest2=$tmp_rest2 <br>";
                //普通残業、法定内残業が深夜残業、休日残業と重複しないよう再計算
				$night_or_hol_time = $late_night + $hol_over + $hol_late_night;
				if ($night_or_hol_time > $wk_hoteigai) {
					//深夜休日が法定外より多い場合、法定内残業から減算する
					$wk_time = $night_or_hol_time - $wk_hoteigai;
					if ($wk_time > 0) {
						if ($wk_hoteinai_zan > $wk_time) {
							$wk_hoteinai_zan -= $wk_time;
						} else {
                            //法定内分を法定外へ加算する場合はコメントをはずす 20130312
                            //$wk_hoteigai += $wk_hoteinai_zan;
							$wk_hoteinai_zan = 0;
						}
					}
				}
				//遅刻早退で法定内残業がある場合、法定外残業とする 20141024 start
				if ($ovtm_class->delay_overtime_inout_flg == "t") {
					//時間有休、外出を追加 20141203
					if ($wk_hoteinai_zan > 0 &&
						(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"]) > 0 ||
						 $paid_hol_min > 0 ||
						 $time3 > 0)) {
						$wk_furikae_kouho = max(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"] + $time3), $paid_hol_min);
						$wk_furikae = min($wk_hoteinai_zan, $wk_furikae_kouho);
						$wk_hoteinai_zan -= $wk_furikae;
						$wk_hoteigai += $wk_furikae;
					}
				}
				//遅刻早退で法定内残業がある場合、法定外残業とする 20141024 end
                if ($shift_flg){
                    //普通残業 20130308 引けるか確認
                    if ($normal_over >= $wk_hoteinai_zan) {
                        $normal_over -= $wk_hoteinai_zan;
                    }
                }
            }

            //合計
			//法定内
			$wk_hoteinai = $kinmu_time - $wk_zangyo;
			$hoteinai += $wk_hoteinai;
			//法定内残業
			$hoteinai_zan += $wk_hoteinai_zan;
            //法定外残
			$hoteigai += $wk_hoteigai;
			//勤務時間
			$sums[13] += $kinmu_time;
			//残業時間
			$sums[14] += $wk_zangyo;
			//残業休憩 20141209
			$over_rest_time_total += $over_rest_time_day;

			//普通残業
			$normal_over_total += $normal_over;
			//深夜
			$late_night_total += $late_night;
			//休日残業
			$hol_over_total += $hol_over;
			//休日深夜
			$hol_late_night_total += $hol_late_night;
			//６０時間超計算用休日残業
			$hol_over_sixty_data_total += $hol_over_sixty_data;
			//６０時間超計算用休日深夜
			$hol_late_night_sixty_data_total += $hol_late_night_sixty_data;
			//45時間超計算用休日残業
			$hol_over_fortyfive_data_total += $hol_over_fortyfive_data;
			//45時間超計算用休日深夜
			$hol_late_night_fortyfive_data_total += $hol_late_night_fortyfive_data;
			
			//週40時間超えた残業 20130425
			$tmp_arr_week_forty += $kinmu_time - $wk_zangyo + $wk_hoteinai_zan;
			
			//45時間超の計算 $fortyfive_value(2700) 20150714
			$fortyfive_over = $normal_over_total + $late_night_total;
			// 法定休暇以外の休日残業とする休暇は45時間超の残業時間の計算に含める
			// 変数に"_fortyfive_data"をつけたものが、日またがり対応
			if ($timecard_bean->sixty_overtime_nonlegal_flg == "t"){
				$fortyfive_over +=  $hol_over_fortyfive_data_total +  $hol_late_night_fortyfive_data_total;
				$hol_fortyfive = $hol_over_fortyfive_data;
				$hol_fortyfive_night = $hol_late_night_fortyfive_data;
			}else{
				$hol_fortyfive = 0;
				$hol_fortyfive_night = 0;
			}
			
			if ($fortyfive_over > $fortyfive_value && $sixty_over <= $sixty_value){
				if (($fortyfive_over - $fortyfive_value) > ($normal_over + $late_night + $hol_fortyfive + $hol_fortyfive_night)){
					if ($fortyfive_over <= $sixty_value) {

						$fortyfive_normal_over_total += $normal_over + $hol_fortyfive;	//45時間超えた普通残業
						$fortyfive_late_night_total += $late_night + $hol_fortyfive_night;		//45時間超えた深夜残業
					}
					//60を超えないように加算
					else {
						$wk_nokori = $sixty_value - ($fortyfive_over - $normal_over - $late_night - $hol_fortyfive - $hol_fortyfive_night);
						if ($wk_nokori > $normal_over + $hol_fortyfive) {
							$fortyfive_normal_over_total += $normal_over + $hol_fortyfive;	//45時間超えた普通残業
							$wk_nokori -= ($normal_over + $hol_fortyfive);
						}
						else {
							$fortyfive_normal_over_total += $wk_nokori;
							$wk_nokori = 0;
						}
						if ($wk_nokori > 0) {
							if ($wk_nokori > $late_night + $hol_fortyfive_night) {
								$fortyfive_late_night_total += $late_night + $hol_fortyfive_night;		//45時間超えた深夜残業
							}
							else {
								$fortyfive_late_night_total += $wk_nokori;
							}

						}
					}
				}else{
					//残業時間の一部が45時間を超える場合には、分割して計上する（深夜残業優先計上）
					$surplus = $fortyfive_over - $fortyfive_value;
					if ($surplus > ($late_night + $hol_fortyfive_night)){
						$fortyfive_normal_over_total += $surplus - $late_night - $hol_fortyfive_night;
						$fortyfive_late_night_total += $late_night + $hol_fortyfive_night;
					}else{
						$fortyfive_late_night_total += $surplus;
					}
				}
			}
			
			//60時間超の計算 $sixty_value(3600) 20130405
			$sixty_over = $normal_over_total + $late_night_total;
			// 法定休暇以外の休日残業とする休暇は６０時間超の残業時間の計算に含める 20140919
			// 変数に"_sixty_data"をつけたものが、日またがり対応 20141028
			if ($timecard_bean->sixty_overtime_nonlegal_flg == "t"){
				$sixty_over +=  $hol_over_sixty_data_total +  $hol_late_night_sixty_data_total;
				// - $sixty_legal_holiday_total;
				$hol_sixty = $hol_over_sixty_data;
				$hol_sixty_night = $hol_late_night_sixty_data;
			}else{
				$hol_sixty = 0;
				$hol_sixty_night = 0;
				//$sixty_legal_holiday_total += $hol_over + $hol_late_night;
			}
			
			if ($sixty_over > $sixty_value){
				if (($sixty_over - $sixty_value) > ($normal_over + $late_night + $hol_sixty + $hol_sixty_night)){

					$sixty_normal_over_total += $normal_over + $hol_sixty;	//60時間超えた普通残業
					$sixty_late_night_total += $late_night + $hol_sixty_night;		//60時間超えた深夜残業
				}else{
					//残業時間の一部が60時間を超える場合には、分割して計上する（深夜残業優先計上）
					$surplus = $sixty_over - $sixty_value;
					if ($surplus > ($late_night + $hol_sixty_night)){
						$sixty_normal_over_total += $surplus - $late_night - $hol_sixty_night;
						$sixty_late_night_total += $late_night + $hol_sixty_night;
					}else{
						$sixty_late_night_total += $surplus;
					}
				}
			}


		}
		
		//週40時間超えた残業 20130425
		if (!$last_week_flg){
			$tmp_week = get_weekday(to_timestamp($tmp_date));
			if ($forty_last_day == $tmp_week){
				
				if ($forty_last_week != ""){
					$tmp_arr_week_forty += $forty_last_week;	//前月のデータを加算する
					$forty_last_week = "";
				}
				
				$forty_surplus = 0;
				if ($tmp_arr_week_forty > $week_forty_value){
					$forty_surplus = $tmp_arr_week_forty - $week_forty_value;	//週40時間越えた分を計上
				}
				//キー:日付＋曜日=>結果代入
				$week_forty_overtime = ($forty_surplus == 0) ? "0:00" : minute_to_hmm($forty_surplus);
				$week_forty_overtime_total += $forty_surplus; //週40時間合計
				$tmp_arr_week_forty = 0;
			}
		}
		
		//管理画面より残業時間をチェック 20130614
		$fixed_start    = str_replace(":", "", $start2_date_time);
		$fixed_end      = str_replace(":", "", $end2_date_time);
		$over_error_flg = "";
		if (($fixed_start != "") && ($fixed_end != "")){
			$arr_overtime = array();
			//残業時間1
			$chk_over_start = $timecard_common_class->get_timecard_end_date_time($tmp_date, $over_start_time, $over_start_next_day_flag);
			$chk_over_end   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $over_end_time, $over_end_next_day_flag);
			$arr_overtime[] = $chk_over_start;
			$arr_overtime[] = $chk_over_end;
		
			//残業時間2
			$chk_over_start2 = $timecard_common_class->get_timecard_end_date_time($tmp_date, $over_start_time2, $over_start_next_day_flag2);
			$chk_over_end2   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $over_end_time2, $over_end_next_day_flag2);
			$arr_overtime[] = $chk_over_start2;
			$arr_overtime[] = $chk_over_end2;
		
			foreach ($arr_overtime as $overtime){
				if (($fixed_start < $overtime) && ($overtime < $fixed_end)){
					$over_error_flg = "1";
					break;
				}
			}
			//残業時刻が退勤時刻より後の場合エラーとする 20150703
			if($over_error_flg != "1" && $disp_end_time != "") { //打刻がない場合、残業時刻チェックと合わせるため表示用に退避した項目(DBの内容)を使用
				$arr_overtime = array();
				for ($i=1; $i <= 5; $i++){
					$wk_idx = ($i == 1) ? "" : $i;
					$over_start_time_nm = "over_start_time".$wk_idx;
					$over_end_time_nm = "over_end_time".$wk_idx;
					$over_start_next_day_flag_nm = "over_start_next_day_flag".$wk_idx;
					$over_end_next_day_flag_nm = "over_end_next_day_flag".$wk_idx;

					if ($arr_atdbkrslt[$tmp_date][$over_start_time_nm] != "") {
						$chk_over_start = $timecard_common_class->get_timecard_end_date_time($tmp_date, $arr_atdbkrslt[$tmp_date][$over_start_time_nm], $arr_atdbkrslt[$tmp_date][$over_start_next_day_flag_nm]);
						$chk_over_end   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $arr_atdbkrslt[$tmp_date][$over_end_time_nm], $arr_atdbkrslt[$tmp_date][$over_end_next_day_flag_nm]);
						if ($chk_over_start != "") {
							$arr_overtime[] = $chk_over_start;
						}
						if ($chk_over_end != "") {
							$arr_overtime[] = $chk_over_end;
						}
					}
				}
				foreach ($arr_overtime as $overtime){
					if ($end_date_time < $overtime){ //退勤時刻
						$over_error_flg = "1";
						break;
					}
				}

			}
			
			if($over_error_flg != "1") {
				//呼出
				$chk_o_start = substr($fixed_start,8,4);
				$chk_o_end = substr($fixed_end,8,4);
				
				for ($i=1; $i <= 10; $i++){
					$buf_start 	= $arr_atdbkrslt[$tmp_date]["o_start_time".$i];
					$buf_end    = $arr_atdbkrslt[$tmp_date]["o_end_time".$i];
						
					if (($chk_o_start < $buf_start) && ($buf_start < $chk_o_end)){
						$over_error_flg = "1";
						break;
					}
					if (($chk_o_start < $buf_end) && ($buf_end < $chk_o_end)){
						$over_error_flg = "1";
						break;
					}
				}
			}
		}
		
		//グループ・パターン・事由の日本語名取得
		$group_name   = $atdbk_common_class->get_group_name($tmcd_group_id);
		$pattern_name = $atdbk_common_class->get_pattern_name($tmcd_group_id, $pattern);
		$reason_name  = $atdbk_common_class->get_reason_name($reason);
		$allow_value = "";
		$allow_value2 = "";
		$allow_value3 = "";
		if ($allow_id != ""){
			foreach($arr_allowance as $allowance){
				if($allow_id == $allowance["allow_id"]){
					$allow_value = $allowance["allow_contents"];
					$arr_allow_cnt[$allow_id]["allow_contents"] = $allow_value;
					break;
				}
			}
			if ($allow_count == "" || $allow_count == " " || $allow_count == "1") {
				$wk_allow_count = 1;
			} else {
				$wk_allow_count = intval($allow_count);
			}
			if ($arr_allow_cnt[$allow_id]["allow_cnt"] == "") {
				$arr_allow_cnt[$allow_id]["allow_cnt"] = $wk_allow_count;
			} else {
				$arr_allow_cnt[$allow_id]["allow_cnt"] += $wk_allow_count;
			}
		}
		if ($allow_id2 != ""){
			foreach($arr_allowance as $allowance){
				if($allow_id2 == $allowance["allow_id"]){
					$allow_value2 = $allowance["allow_contents"];
					$arr_allow_cnt[$allow_id2]["allow_contents"] = $allow_value2;
					break;
				}
			}
			if ($allow_count2 == "" || $allow_count2 == " " || $allow_count2 == "1") {
				$wk_allow_count2 = 1;
			} else {
				$wk_allow_count2 = intval($allow_count2);
			}
			if ($arr_allow_cnt[$allow_id2]["allow_cnt"] == "") {
				$arr_allow_cnt[$allow_id2]["allow_cnt"] = $wk_allow_count2;
			} else {
				$arr_allow_cnt[$allow_id2]["allow_cnt"] += $wk_allow_count2;
			}
		}
		if ($allow_id3 != ""){
			foreach($arr_allowance as $allowance){
				if($allow_id3 == $allowance["allow_id"]){
					$allow_value3 = $allowance["allow_contents"];
					$arr_allow_cnt[$allow_id3]["allow_contents"] = $allow_value3;
					break;
				}
			}
			if ($allow_count3 == "" || $allow_count3 == " " || $allow_count3 == "1") {
				$wk_allow_count3 = 1;
			} else {
				$wk_allow_count3 = intval($allow_count3);
			}
			if ($arr_allow_cnt[$allow_id3]["allow_cnt"] == "") {
				$arr_allow_cnt[$allow_id3]["allow_cnt"] = $wk_allow_count3;
			} else {
				$arr_allow_cnt[$allow_id3]["allow_cnt"] += $wk_allow_count3;
			}
		}

		$tmp_night_duty = "";
		if($night_duty == "1")
		{
			$tmp_night_duty = "有";
		}
		else if($night_duty == "2")
		{
			$tmp_night_duty = "無";
		}

		// 前日になる場合があるフラグ
		$previous_day_value = "";
		if ($previous_day_flag == 1){
			$previous_day_value = "前";
		}

		// 翌
		$next_day = "";
		if ($show_next_day_flag == 1){
			$next_day = "翌";
		}

		//日の属性
		$arr_info[$day_idx]["type"] = $type;
		$arr_info[$day_idx]["tmcd_group_id"] = $tmcd_group_id;
		$arr_info[$day_idx]["hidden_workday_count"] = $workday_count; //チェック用、表示用とは別とする

		$arr_info[$day_idx]["modify_link_type"] = $modify_link_type;
		$arr_info[$day_idx]["modify_apply_id"] = $modify_apply_id;

		$arr_info[$day_idx]["overtime_link_type"] = $overtime_link_type;
		$arr_info[$day_idx]["overtime_apply_id"] = $overtime_apply_id;
		$arr_info[$day_idx]["diff_min_end_time"] = $diff_min_end_time;

		$arr_info[$day_idx]["return_link_type"] = $return_link_type;
		$arr_info[$day_idx]["return_apply_id"] = $return_apply_id;

		$arr_info[$day_idx]["o_start_time1"] = $o_start_time1;

		$arr_info[$day_idx]["after_night_duty_flag"] = $after_night_duty_flag;

		// 日付
		$arr_info[$day_idx][0] = $tmp_month."月".$tmp_day."日";
		$arr_info[$day_idx]["date_str"] = $tmp_month."月".$tmp_day."日";
		$arr_info[$day_idx]["date"] = $tmp_date;
		// 曜日
		$weekday = get_weekday(to_timestamp($tmp_date));
		$arr_info[$day_idx][1] = $weekday;
		$arr_info[$day_idx]["weekday"] = $weekday;
		//勤務実績
		$arr_info[$day_idx][2] = $pattern_name;
		$arr_info[$day_idx]["pattern_name"] = $pattern_name;
		$arr_info[$day_idx]["pattern"] = $pattern;
		//事由
		$arr_info[$day_idx][3] = $reason_name;
		$arr_info[$day_idx]["reason_name"] = $reason_name;
		$arr_info[$day_idx]["reason"] = $reason;
		//前
		$arr_info[$day_idx][4] = $previous_day_value;
		$arr_info[$day_idx]["previous_day"] = $previous_day_value;
		//出勤
		$arr_info[$day_idx][5] = hhmm_to_hmm($show_start_time);
		$arr_info[$day_idx]["start_time"] = hhmm_to_hmm($show_start_time);
		//翌
		$arr_info[$day_idx][6] = $next_day;
		$arr_info[$day_idx]["next_day"] = $next_day;
		//退勤
		$arr_info[$day_idx][7] = hhmm_to_hmm($show_end_time);
		$arr_info[$day_idx]["end_time"] = hhmm_to_hmm($show_end_time);
		//遅刻
		$wk_delay_time = minute_to_hmm($start_time_info["diff_minutes"]);
		$arr_info[$day_idx][8] = $wk_delay_time;
		$arr_info[$day_idx]["delay_time"] = $wk_delay_time;

		//早退
		$wk_early_leave_time = minute_to_hmm($end_time_info["diff_minutes"]);
		$arr_info[$day_idx][9] = $wk_early_leave_time;
		$arr_info[$day_idx]["early_leave_time"] = $wk_early_leave_time;
		//外出
		$arr_info[$day_idx][10] = $time_outtime;
		$arr_info[$day_idx]["time_outtime"] = $time_outtime;
		//休憩、残業休憩追加 20141209
		$wk_time_rest = hmm_to_minute($time_rest) + $over_rest_time_day;
		$time_rest = minute_to_hmm($wk_time_rest);
		$arr_info[$day_idx][11] = $time_rest;
		$arr_info[$day_idx]["time_rest"] = $time_rest;
		//勤務
		$arr_info[$day_idx][12] = minute_to_hmm($kinmu_time);
		$arr_info[$day_idx]["kinmu_time"] = minute_to_hmm($kinmu_time);

		// タイムカード入力出勤簿に休日深夜残業と残業集計を追加 20130129
		if ($shift_flg)
		{
			$item_no = array(1=>1, 2=>3, 3=>8, 4=>2);
			//休日深夜残業
			$arr_info[$day_idx][17] = minute_to_hmm($hol_late_night);
			
			//週40時間残業
			$arr_info[$day_idx][18] = $week_forty_overtime;
			$arr_info[$day_idx]["week_forty"] = $week_forty_overtime;
			
			//残業計＝法定内残業＋普通残＋深夜残業＋休日残業＋休日深夜残業
            //$arr_info[$day_idx][19] = minute_to_hmm($wk_hoteinai_zan + $normal_over + $late_night + $hol_over_time + $hol_late_night);
            //$sums[41] += $wk_hoteinai_zan + $normal_over + $late_night + $hol_over_time + $hol_late_night;
            $arr_info[$day_idx][19] = minute_to_hmm($wk_hoteinai_zan + $normal_over + $late_night + $hol_over + $hol_late_night);
            $sums[41] += $wk_hoteinai_zan + $normal_over + $late_night + $hol_over + $hol_late_night;
        }else{
			$item_no = array(1=>0, 2=>0, 3=>0, 4=>0);
		}

		//有給
		$arr_info[$day_idx][13+$item_no[3]] = minute_to_hmm($paid_time);
		$arr_info[$day_idx]["paid_hol_time"] = minute_to_hmm($paid_time);
        //申請状態
		$arr_info[$day_idx]["paid_hol_stat"] = $arr_paid_hol_data[$tmp_date]["apply_stat"];
		//法内残
		$arr_info[$day_idx][14-$item_no[1]] = minute_to_hmm($wk_hoteinai_zan);
		$arr_info[$day_idx]["hoteinai_zan"] = minute_to_hmm($wk_hoteinai_zan);
		//法外残
        if ($shift_flg) { // 20130308
            $arr_info[$day_idx][15-$item_no[1]] = minute_to_hmm($normal_over);
            $arr_info[$day_idx]["hoteigai"] = minute_to_hmm($normal_over);
        }
        else {
            $arr_info[$day_idx][15-$item_no[1]] = minute_to_hmm($wk_hoteigai);
            $arr_info[$day_idx]["hoteigai"] = minute_to_hmm($wk_hoteigai);
        }
        //深夜残
        if ($shift_flg) { // 20130308
            $arr_info[$day_idx][16-$item_no[1]] = minute_to_hmm($late_night);
            $arr_info[$day_idx]["night_over_time"] = minute_to_hmm($late_night);
        }
        else {
            //time2 -> $late_night+$hol_late_night 20130827
            $arr_info[$day_idx][16-$item_no[1]] = minute_to_hmm($late_night+$hol_late_night);
            $arr_info[$day_idx]["night_over_time"] = minute_to_hmm($late_night+$hol_late_night);
        }
		//休日残
		//$arr_info[$day_idx][17-$item_no[1]] = minute_to_hmm($hol_over_time);
		//$arr_info[$day_idx]["hol_over_time"] = minute_to_hmm($hol_over_time);
        $arr_info[$day_idx][17-$item_no[1]] = minute_to_hmm($hol_over);
        $arr_info[$day_idx]["hol_over_time"] = minute_to_hmm($hol_over);
        //echo "day_idx=$day_idx hol_over=$hol_over";
        //呼出
        $wk_return_time = minute_to_hmm($wk_return_time);
        //呼出回数追加 20121109
        if ($return_count != "") {
            $wk_return_time .= " ".$return_count;
            $return_count_total += $return_count;
        }
        $arr_info[$day_idx][18+$item_no[4]] = $wk_return_time;
        $arr_info[$day_idx]["return_time"] = $wk_return_time;
		//日数
		// 出勤・退勤時刻がある場合のみ表示する 20090702 //明けを追加 20110819
		if (!(($start_time != "" && $end_time != "") || $after_night_duty_flag == "1")) {
			$total_workday_count = "";
		}
		$arr_info[$day_idx][19+$item_no[2]] = $total_workday_count;
		$arr_info[$day_idx]["workday_count"] = $total_workday_count;
		//残業理由
		//その他理由がある場合はそれを設定
		if ($other_reason != "") {
			$ovtm_reason = mb_substr($other_reason, 0, 8);
		}
		//残業申請不要の場合で、残業申請不要理由区分がない場合は表示しないよう空白にする
		else if ($overtime_link_type == "6" && $arr_over_no_apply_flag[$ovtm_reason_id] != "t") {
			$ovtm_reason = "";
		}
		$arr_info[$day_idx][20+$item_no[2]] = $ovtm_reason;
		$arr_info[$day_idx]["ovtm_reason"] = $ovtm_reason;
		//手当
		$wk_allow_value = $allow_value;
		if ($allow_value2 != "") {
			$wk_allow_value .= "<br>".$allow_value2;
		}
		if ($allow_value3 != "") {
			$wk_allow_value .= "<br>".$allow_value3;
		}
		$arr_info[$day_idx][21+$item_no[2]] = $wk_allow_value;
		$arr_info[$day_idx]["allow"] = $wk_allow_value;
		//会議・研修
		$meeting_time_value = "";
		$tmp_meeting_time = 0;
		if ($meeting_display_flag){
			//会議研修
			$meeting_time_hh = "";
			$meeting_time_mm = "";
			// 開始終了時刻から時間を計算する 20091008
			if ($meeting_start_time != "" && $meeting_end_time != "") {
				$tmp_meeting_start_date_time = $tmp_date.$meeting_start_time;
				if ($meeting_start_time <= $meeting_end_time) {
					$tmp_meeting_end_date_time = $tmp_date.$meeting_end_time;
				} else {
					// 日またがりの場合
					$tmp_meeting_end_date_time = next_date($tmp_date).$meeting_end_time;
				}
				$tmp_meeting_time = date_utils::get_time_difference($tmp_meeting_end_date_time, $tmp_meeting_start_date_time);
				$tmp_meeting_hhmm = $timecard_common_class->minute_to_h_hh($tmp_meeting_time);
				$meeting_time_value = $tmp_meeting_hhmm;
			} elseif ($meeting_time != null){
				$meeting_time_hh = (int)substr($meeting_time, 0, 2);
				$meeting_time_mm = (int)substr($meeting_time, 2, 4);
				$meeting_time_value = $meeting_time_hh;

				$tmp_meeting_time = $meeting_time_hh*60;

				if ($meeting_time_mm == 15){
					$meeting_time_value = $meeting_time_value.".25";
					$tmp_meeting_time += 15;
				} else if ($meeting_time_mm == 30){
					$meeting_time_value = $meeting_time_value.".5";
					$tmp_meeting_time += 30;
				} else if ($meeting_time_mm == 45){
					$meeting_time_value = $meeting_time_value.".75";
					$tmp_meeting_time += 45;
				}

			}
			$meeting_time_total += $tmp_meeting_time;
		}

		$arr_info[$day_idx][22+$item_no[2]] = $meeting_time_value;
		$arr_info[$day_idx]["meeting_time"] = $meeting_time_value;
		
		$arr_info[$day_idx]["over_error_flg"] = $over_error_flg;	//残業エラーフラグ 20130614
		
        $arr_info[$day_idx]["over_start_time"] = $over_start_time;
        $arr_info[$day_idx]["over_end_time"] = $over_end_time;
        
        
		$tmp_date = next_date($tmp_date);
		$day_idx++;
	} // end of while

	if ($ovtm_class->list_absence_disp_flg == "t") {
	$day_idx2 = 0;
	$tmp_date = $wk_start_date;
	while ($tmp_date <= $wk_end_date_nextone) {
        //欠勤 20150113
        $wk_time = "";
        $rest_day_idx = $day_idx2;
        $force_write = false;
        if ($arr_absence_data[$tmp_date]["apply_date"] != "") {
        	if ($arr_absence_data[$tmp_date]["start_time"] != "") {
	        	//$wk_time = date_utils::get_diff_minute($arr_absence_data[$tmp_date]["end_date"].$arr_absence_data[$tmp_date]["end_time"], $arr_absence_data[$tmp_date]["start_date"].$arr_absence_data[$tmp_date]["start_time"]);
				$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
				$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
	        	$tmp_rest_date = $tmp_date;
	        	//開始が前日のデータ、翌日分か確認
	        	$tmp_next_date = next_date($tmp_date);
	        	if ($arr_atdbkrslt[$tmp_next_date]["atdptn_previous_day_flag"] == "1") {
	        		$tmp_rest_date = $tmp_next_date;
			        $rest_day_idx = $day_idx2 + 1;
                    $force_write = true;
                }
	        	else
	        	if ($pattern == "10" || $after_night_duty_flag == "1") {
	        		$tmp_rest_date = last_date($tmp_date);
			        $rest_day_idx = $day_idx2 - 1;
                    $force_write = true;
                }
                else {
                    $force_write = false;
                }
				$wk_time = $timecard_common_class->get_diff_times(
					$arr_absence_data[$tmp_date]["start_date"].$arr_absence_data[$tmp_date]["start_time"],
					$arr_absence_data[$tmp_date]["end_date"].$arr_absence_data[$tmp_date]["end_time"],
					$arr_absence_data[$tmp_rest_date]["start4_date_time"],
					$arr_absence_data[$tmp_rest_date]["end4_date_time"]
					);
				//if ($rest_day_idx >= 0) {
				if ($tmp_rest_date >= $wk_start_date && $tmp_rest_date <= $wk_end_date) {
	        		$absence_time_total += $wk_time;
	        	}
	        	$wk_time = minute_to_hmm($wk_time);
        	}
        	elseif ($arr_absence_data[$tmp_date]["holiday_count"] != "") {
        		$wk_time = $arr_absence_data[$tmp_date]["holiday_count"];
	        	$absence_day_total += $wk_time;
        	}
			if ($tmp_rest_date >= $wk_start_date && $tmp_rest_date <= $wk_end_date) {
	        $arr_info[$rest_day_idx]["absence_stat"] = $arr_absence_data[$tmp_date]["apply_stat"];
	        }
        }
		if ($tmp_rest_date >= $wk_start_date && $tmp_rest_date <= $wk_end_date) {
        //前後の日に強制的に更新
        if ($force_write) {
            $arr_info[$rest_day_idx][26] = $wk_time;
        }
        else {
            //未設定の場合、更新
            if ($arr_info[$rest_day_idx][26] == "") {
                $arr_info[$rest_day_idx][26] = $wk_time;
            }
        }
        }
		$tmp_date = next_date($tmp_date);
		$day_idx2++;
	} // end of while
	}
/*
	//翌月欠勤データ
	if ($ovtm_class->list_absence_disp_flg == "t" &&
		$arr_absence_data[$tmp_date]["apply_date"] != "" &&
		$arr_absence_data[$tmp_date]["start_time"] != "") {
		$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
		$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];

    	$tmp_rest_date = $tmp_date;
    	if ($pattern == "10" || $after_night_duty_flag == "1") {
    		$tmp_rest_date = last_date($tmp_date);
	        $rest_day_idx = $day_idx - 1;
			$wk_time = $timecard_common_class->get_diff_times(
				$arr_absence_data[$tmp_date]["start_date"].$arr_absence_data[$tmp_date]["start_time"],
				$arr_absence_data[$tmp_date]["end_date"].$arr_absence_data[$tmp_date]["end_time"],
				$arr_absence_data[$tmp_rest_date]["start4_date_time"],
				$arr_absence_data[$tmp_rest_date]["end4_date_time"]
				);
	    	$absence_time_total += $wk_time;
	    	$wk_time = minute_to_hmm($wk_time);

	        $arr_info[$rest_day_idx]["absence_stat"] = $arr_absence_data[$tmp_date]["apply_stat"];
	        $arr_info[$rest_day_idx][26] = $wk_time;
    	}
	}
*/
	//日数
	$arr_info["day_count"] = $day_idx;
	//欠勤
	$arr_info["absence_day_total"] = $absence_day_total;
	$arr_info["absence_time_total"] = ($absence_time_total == 0) ? "0:00" : minute_to_hmm($absence_time_total);
	//遅刻早退時間計
	$arr_info["early_leave_time_total"] = ($early_leave_time == 0) ? "0:00" : minute_to_hmm($early_leave_time);
	$arr_info["delay_time_total"] = ($delay_time == 0) ? "0:00" : minute_to_hmm($delay_time);
	$arr_info["out_time_total"] = ($out_time_total == 0) ? "0:00" : minute_to_hmm($out_time_total);
	//休憩に残業休憩も加算 20141209
	$arr_info["rest_time_total"] = ($rest_time_total+$over_rest_time_total == 0) ? "0:00" : minute_to_hmm($rest_time_total+$over_rest_time_total);

	$arr_info["paid_time_total"] = ($paid_time_total == 0) ? "0:00" : minute_to_hmm($paid_time_total);

	$arr_info["return_time_total"] = ($return_time_total == 0) ? "0:00" : minute_to_hmm($return_time_total);
    //呼出回数
    if ($return_count_total != 0) {
        $arr_info["return_time_total"] .= " ".$return_count_total;
    }
	$arr_info["meeting_time_total"] = $timecard_common_class->minute_to_h_hh($meeting_time_total);

	// 早退時間を残業時間から減算する設定の場合、法定内残業、法定外残業の順に減算する
	if ($early_leave_time_flg == "1") {

		list($hoteinai_zan, $hoteigai) = $atdbk_common_class->get_overtime_early_leave($hoteinai_zan, $hoteigai, $early_leave_time);
		//残業時間 20100713
		$sums[14] = $hoteinai_zan + $hoteigai;
	}
	// 要勤務日数を取得 20091222
	$wk_year = substr($yyyymm, 0, 4);
	$wk_mon = substr($yyyymm, 4, 2);
	//当月日数
	if ($closing_month_flg == "2") { //締め日の月度が終了日の月の場合
		$wk_year2 = substr($wk_start_date, 0, 4);
		$wk_mon2 = substr($wk_start_date, 4, 2);
	} else {
		$wk_year2 = $wk_year;
		$wk_mon2 = $wk_mon;
	}
	$days_in_month = days_in_month($wk_year2, $wk_mon2);
	//対象年月の公休数を取得 20100615
	if ($legal_hol_cnt_flg) {
		$legal_hol_cnt = $legal_hol_cnt_kikan;
	}else {
		$legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $wk_year, $wk_mon, $closing, $closing_month_flg, $arr_info["tmcd_group_id"], $duty_form, $arr_legal_hol_cnt, $arr_legal_hol_cnt_part, $arr_holwk);
	}
	$sums[0] = $days_in_month - $legal_hol_cnt;
	if ($sums[1] == "") {
		$sums[1] = "0";
	}

	// 法定内 = 勤務時間 - 残業時間 20100713
	$sums[28] = $sums[13] - $sums[14];
	// 法定内残業
	$sums[29] = $hoteinai_zan;
	// 法定外残業
    if ($shift_flg) { // 20130308
        $sums[30] = $normal_over_total;
    }
    else {
        $sums[30] = $hoteigai;
    }

	// 変則労働期間が月の場合
	if ($arr_irrg_info["irrg_type"] == "2") {

		// 所定労働時間を基準時間とする
		$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
		if ($hol_minus == "t") {
			$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
		}
		$sums[12] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

		// 稼働時間−基準時間を残業時間とする
		$sums[14] = $sums[13] - $sums[12];
	}
	else {
		// 基準時間の計算を変更 2008/10/21
		// 要勤務日数 × 所定労働時間
		$sums[12] = $sums[0] * date_utils::hi_to_minute($day1_time);
	}

	// 月集計値の稼働時間・残業時間・深夜勤務を端数処理
	for ($i = 13; $i <= 15; $i++) {
		if ($sums[$i] < 0) {
			$sums[$i] = 0;
		}
		$sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
	}

	//合計を表示するように変更 20130507
// 	// 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
// 	if ($no_overtime == "t") {
// 		$sums[14] = 0;
// 		$sums[28] = 0;
// 		$sums[29] = 0;
// 		$sums[30] = 0;
// 		$sums[40] = 0;	//20130129 休日深夜残業
// 	}
	// 月集計値を表示用に編集
	//$sums[0] .= "日";
	//for ($i = 2; $i <= 9; $i++) {
	//	$sums[$i] .= "日";
	//}
	//for ($i = 10; $i <= 11; $i++) {
	//	$sums[$i] .= "回";
	//}
	for ($i = 12; $i <= 15; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	//for ($i = 16; $i <= 27; $i++) {
	//	$sums[$i] .= "日";
	//}
	for ($i = 28; $i <= 31; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	//20130129 休日深夜残業,残業集計
	$sums[40] = ($sums[40] == 0) ? "0:00" : minute_to_hmm($sums[40]);
	$sums[41] = ($sums[41] == 0) ? "0:00" : minute_to_hmm($sums[41]);

	//60時間超えた普通残業と深夜残業 20130405
	$sums[42] = ($sixty_normal_over_total == 0) ? "0:00" : minute_to_hmm($sixty_normal_over_total);
	$sums[43] = ($sixty_late_night_total  == 0) ? "0:00" : minute_to_hmm($sixty_late_night_total);
	
	//週40時間超えた残業 20130425
	if ($last_week_flg) {
		$sums[44] = $tmp_arr_week_forty;		//前月のデータ
	}else{
		$sums[44] = ($week_forty_overtime_total == 0) ? "0:00" : minute_to_hmm($week_forty_overtime_total);
	}

	//45時間超えた普通残業と深夜残業 20150714
	$sums[45] = ($fortyfive_normal_over_total == 0) ? "0:00" : minute_to_hmm($fortyfive_normal_over_total);
	$sums[46] = ($fortyfive_late_night_total  == 0) ? "0:00" : minute_to_hmm($fortyfive_late_night_total);
	
	//年末年始追加 20090908
	//$sums[33] .= "日";
	//支給換算日数追加 20090908
	//$sums[34] = $paid_day_count;
	//年休残追加 20100625
	$sums[35] = $timecard_common_class->get_nenkyu_zan($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $wk_start_date, $wk_end_date);//."日";
	//時間有休合計を日数換算し年休残から減算 20111207
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//１日分の所定労働時間(分)
		$specified_time_per_day = $obj_hol_hour->get_specified_time($emp_id, $day1_time);
        $wk_output_flg = ($obj_hol_hour->paid_hol_hour_unit_time == "1") ? "3" : $output_flg;
        //時間有休がある場合の年休残文字列取得 20120604
        $ret_str = $obj_hol_hour->get_nenkyu_zan_str($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $start_date, $end_date, $specified_time_per_day, $sums[35], $duty_form, $wk_output_flg, $timecard_common_class);
        $sums[35] = $ret_str;
        //当月の時間有給取得数を日数換算し有休へ加算
		//$wk_day = $paid_time_hour_month / $specified_time_per_day;
		//$wk_day = $obj_hol_hour->edit_nenkyu_zan($wk_day);
		//$sums[7] += $wk_day;
        if ($paid_time_hour_month > 0) {
            if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                $wk_hour = minute_to_hmm($paid_time_hour_month);
            }
            else {
                $wk_hour = $paid_time_hour_month / 60;
                $wk_hour = round($wk_hour * 100) / 100;
            }
            $sums[7] .= "(".$wk_hour.")";
        }
    }

	//法定追加 20110121
	//$sums[36] .= "日";
	//所定追加 20110121
	//$sums[37] .= "日";


	//return $sums;
	$arr_info["sums"] = $sums;
	//当直集計
	$arr_info["arr_night_duty_date"] = $arr_night_duty_date;
	//勤務実績回数
	if (count($arr_group_id) > 1) {
		ksort($arr_group_id, SORT_NUMERIC);
	}
	$arr_group_pattern = array();
	$i = 0;
	foreach ($arr_group_id as $group_id => $dummy) {

		$arr_group_pattern[$i]["group_id"] = $group_id;
		$arr_group_pattern[$i]["group_name"] = $atdbk_common_class->get_group_name($group_id);

		$arr_pattern = $arr_group_ptn_cnt[$group_id];
		ksort($arr_pattern, SORT_NUMERIC);

		$work_arr_pattern = array();
		foreach ($arr_pattern	as $pattern => $cnt) {
			$atdptn_nm = $atdbk_common_class->get_pattern_name($group_id, $pattern);
			$work_arr_pattern[] = array("atdptn_nm" => $atdptn_nm, "pattern_cnt" => $cnt);
		}
		$arr_group_pattern[$i]["arr_pattern"] = $work_arr_pattern;
		$i++;
	}
	$arr_info["arr_group_pattern"] = $arr_group_pattern;
	//手当て
	ksort($arr_allow_cnt, SORT_NUMERIC);
	
	$arr_info["arr_allow"] = $arr_allow_cnt;


	return $arr_info;

}



/**
 * 期間指定で公休数取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $start_date 開始日
 * @param mixed $end_date 終了日
 * @param mixed $arr_holwk カレンダーマスタ上で休日出勤と判定する日,show_timecard_common.ini::get_timecard_holwk_day()参照
 * @return mixed 公休数
 *
 */
function get_legal_hol_cnt_kikan($con, $fname, $start_date, $end_date, $arr_holwk) {

	$sql = "select date, type ";
	$sql .= " from calendar ";
	$sql .= " where date > '$start_date' and date <= '$end_date' and type >= '4' ";
	$cond = " order by date";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	for ($j=4; $j<=7; $j++) {
		$arr_data["$j"] = 0;

	}
	$num = pg_num_rows($sel);
	for ($i=0; $i<$num; $i++) {
		$type = pg_fetch_result($sel, $i, "type");
		$arr_data["$type"]++;
	}

	//件数を計算で求めるため、フラグを1,0に変換
	$wk_legal_flg = ($arr_holwk[0]["legal_holiday"] == "t") ? 1 : 0;
	$wk_national_flg = ($arr_holwk[0]["national_holiday"] == "t") ? 1 : 0;
	$wk_newyear_flg = ($arr_holwk[0]["newyear_holiday"] == "t") ? 1 : 0;
	$wk_prescribed_flg = ($arr_holwk[0]["prescribed_holiday"] == "t") ? 1 : 0;

	$cnt = $arr_data["4"] * $wk_legal_flg +
		$arr_data["5"] * $wk_prescribed_flg +
		$arr_data["6"] * $wk_national_flg +
		$arr_data["7"] * $wk_newyear_flg;

	return $cnt;
}

/*************************************************************************/
// シフト集計表示（表形式）
//  show_timecard_common.ini::show_shift_summary2を改造
// @param $con DBコネクション
// @param $fname 画面名
// @param $atdbk_closed 締めフラグ true:締めデータあり false:なし
// @param $emp_id 職員ID
// @param $start_date 開始日
// @param $end_date 終了日
// @param $arr_total_id 集計項目出力設定
// @param $atdbk_common_class 共通クラス
/*************************************************************************/
function show_shift_summary_from_info($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date, $arr_total_id, $arr_info, $atdbk_common_class)
{
	require_once("holiday.php");
	// system_config
	$conf = new Cmx_SystemConfig();
	//当直・待機
	$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
	if ($duty_or_oncall_flg == "") {
		$duty_or_oncall_flg = "1";
	}
	$duty_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";

	$arr_pattern    = array();
	$arr_night_duty = array();
	$arr_allow      = array();
	$arr_group_pattern = array();
	$work_arr_pattern = array();
//カレンダーから日の属性を取得 20150213
	$arr_type = get_calendar_type($con, $fname, $start_date, $end_date);

	$table_name = "atdbkrslt";

	$today = date("Ymd");
	//////////////////////////////////////////
	// 勤務シフト集計
	//////////////////////////////////////////
	$arr_group_pattern = $arr_info["arr_group_pattern"];

	//////////////////////////////////////////
	// 当直集計
	//////////////////////////////////////////
	$arr_night_duty_date = $arr_info["arr_night_duty_date"];

	$holiday_cnt = 0;
	$satday_cnt = 0;
	$weekday_cnt = 0;

	foreach($arr_night_duty_date as $night_duty_date)
	{
		$date = $night_duty_date["date"];
		$timestamp = to_timestamp($date);

		// 日付から休日かを求める
		//$holiday = ktHolidayName($timestamp);
		// 日付から曜日を求める
		$wd = get_weekday($timestamp);
//カレンダーの日の属性が祝日か確認 20150213
		if($arr_type[$date] == "6" || $wd == "日") //$holiday != "" から変更
		{
			$holiday_cnt++;
		}
		else if($wd == "土")
		{
			$satday_cnt++;
		}
		else
		{
			$weekday_cnt++;
		}
	}
	$arr_night_duty["holiday_cnt"] = $holiday_cnt;
	$arr_night_duty["satday_cnt"]  = $satday_cnt;
	$arr_night_duty["weekday_cnt"] = $weekday_cnt;


	//////////////////////////////////////////
	// 手当集計
	//////////////////////////////////////////
	$arr_allow = $arr_info["arr_allow"];

	//所属グループ
	$sql = "select a.group_id,b.pattern_id from duty_shift_staff a inner join duty_shift_group b on b.group_id = a.group_id ";
	$cond = "where a.emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	if ($num > 0) {
		$belong_group_id = pg_result($sel,0,"pattern_id");
	} else {
		$belong_group_id = $arr_info["tmcd_group_id"]; //勤務条件のグループ
	}
	// 名称と回数のテーブル
	$arr_summary = array();
	$i = 0;
	// 勤務シフト
	if (in_array("10_3", $arr_total_id)) {
		foreach($arr_group_pattern as $group){

			//所属グループ
			$group_id    = $group["group_id"];
			if ($group_id == $belong_group_id) {
				$group_name  = $group["group_name"];
				$arr_pattern = $group["arr_pattern"];
				foreach($arr_pattern as $pattern)
				{
					$arr_summary[$i]["name"] = $pattern["atdptn_nm"];
					$arr_summary[$i]["cnt"] = $pattern["pattern_cnt"];
					$i++;
				}
				break;
			}
		}
	}
	// 当直
	if (in_array("10_1", $arr_total_id)) {
		$arr_summary[$i]["name"] = "{$duty_str}平日";
		$arr_summary[$i]["cnt"] = $weekday_cnt;
		$i++;
		$arr_summary[$i]["name"] = "{$duty_str}土曜日";
		$arr_summary[$i]["cnt"] = $satday_cnt;
		$i++;
		$arr_summary[$i]["name"] = "{$duty_str}日曜・休日";
		$arr_summary[$i]["cnt"] = $holiday_cnt;
		$i++;
	}
	// 手当て
	if (in_array("10_2", $arr_total_id)) {
		foreach($arr_allow as $allow)
		{
			$arr_summary[$i]["name"] = $allow["allow_contents"];
			$arr_summary[$i]["cnt"] = $allow["allow_cnt"];
			$i++;
		}
	}

	if (count($arr_summary) > 0) {
		$group_name = $atdbk_common_class->get_group_name($belong_group_id);
		echo("<tr>\n");
		echo("<td align=\"left\" colspan=\"14\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>勤務実績回数：{$group_name}</b></font></td>\n");
		echo("</tr>\n");
		echo("<tr>\n");
		// 繰返し
		for ($i=0; $i<count($arr_summary); $i++) {
			//改行
			if ($i % 7 == 0) {
				echo("</tr>\n");
				echo("<tr>");
			}
			echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($arr_summary[$i]["name"]) . "</font></td>\n");
			echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $arr_summary[$i]["cnt"] . "</font></td>\n");
		}
		echo("</tr>\n");
	}

	//応援追加分
	if (in_array("10_3", $arr_total_id)) {
		foreach($arr_group_pattern as $group){
			//所属グループ以外
			$group_id    = $group["group_id"];

			if ($group_id == $belong_group_id) {
				continue;
			}

			$group_name  = $group["group_name"];
			$arr_pattern = $group["arr_pattern"];
			$arr_summary = array();
			$i = 0;
			foreach($arr_pattern as $pattern)
			{
				$arr_summary[$i]["name"] = $pattern["atdptn_nm"];
				$arr_summary[$i]["cnt"] = $pattern["pattern_cnt"];
				$i++;
			}
			echo("<tr>\n");
			echo("<td align=\"left\" colspan=\"14\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
			echo("<b>$group_name</b></font>");
			echo("</td>\n");
			echo("</tr>\n");
			echo("<tr>\n");

			$wk_width = 10 + 10 * $i;

			// 繰返し
			for ($i=0; $i<count($arr_summary); $i++) {
				//改行
				if ($i % 7 == 0) {
					echo("</tr>\n");
					echo("<tr>");
				}
				echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($arr_summary[$i]["name"]) . "</font></td>\n");
				echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $arr_summary[$i]["cnt"] . "</font></td>\n");
			}
			echo("</tr>\n");
		}
	}
}


/**
 * 出勤簿、PDF用見出しを返す 20130129
 *
 * @return 見出しの配列
 *
 */
function get_shift_title() {

	$arr_title = array("日付", "曜日", "勤務実績", "事由", "前", "出勤", "翌", "退勤",
				"遅刻", "早退", "外出", "休憩", "勤務", "法内", "普残", "深残",
				"休残", "休深", "40超", "残計", "呼出", "有休","日数", "残業理由", "手当", "会議研修", "欠勤");
	return $arr_title;
}

/**
 * 出勤簿、PDF用テキスト位置を返す 20130129
 *
 * @return テキスト位置の配列（C:center, L:left, R:right）
 *
 */
function get_shift_align() {

	$arr_align = array("C", "C", "L", "L", "C", "R", "C", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "C", "L", "L", "R");
	return $arr_align;
}

/**
 * A4横、PDF用見出しを返す
 *
 * @return 見出しの配列
 *
 */
function get_a4_title() {

	$arr_title = array("日付", "曜日", "勤務実績", "事由", "前", "出勤", "翌", "退勤", "遅刻", "早退", "外出", "休憩", "勤務", "有休", "法定内", "法定外", "深夜残", "休日残", "呼出", "日数", "残業理由", "手当", "会議研修");
	return $arr_title;
}

/**
 * A4横、PDF用テキスト位置を返す
 *
 * @return テキスト位置の配列（C:center, L:left, R:right）
 *
 */
function get_a4_align() {

	$arr_align = array("C", "C", "L", "L", "C", "R", "C", "R", "R", "R", "R", "R", "R",   "R", "R", "R", "R", "R", "R", "C", "L", "L", "R");
	return $arr_align;
}

//休日、深夜別残業時間取得
//翌日パターン、翌日事由追加 20130308
//$rest_start_date_time 休憩開始時刻
//$rest_end_date_time 休憩終了時刻
//$end2_date_time 所定終了日時、翌日へ連続している残業かの判断用
//※各残業時間からは休憩時間が引かれたあとの数値
//返す配列に休憩時間項目を追加
// 法定休暇以外の休日残業とする休暇は６０時間超の残業時間の計算に含める対応 20141028
// $arr_data["hol_over_sixty_data"]:６０時間超計算用休日残業
// $arr_data["hol_over_late_night_sixty_data"]:６０時間超計算用休日深夜
function get_overtime_data($work_times_info, $date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $start_date_time, $end_date_time, $next_pattern, $next_reason, $timecard_bean, $ovtm_class, $rest_start_date_time="", $rest_end_date_time="", $end2_date_time="") {

    //echo "start_date_time=$start_date_time rest_start_date_time=$rest_start_date_time rest_end_date_time=$rest_end_date_time <br>\n";    
    $arr_data = array();
	//普通残業
	$normal_over = 0;
	//深夜
	$late_night = 0;
	//休日残業
	$hol_over = 0;
	//休日深夜
	$hol_late_night = 0;
    //休憩
    //普通残業
    $normal_over_rest = 0;
    //深夜
    $late_night_rest = 0;
    //休日残業
    $hol_over_rest = 0;
    //休日深夜
    $hol_late_night_rest = 0;
    //６０時間超計算用休日残業
	$hol_over_sixty_data = 0;
    //６０時間超計算用休日深夜
	$hol_late_night_sixty_data = 0;
    
	if ($start_date_time != "" && $end_date_time != "") {
		// 開始日時の日、終了日時の日
		$start_date = substr($start_date_time, 0, 8);
		$end_date = substr($end_date_time, 0, 8);
		//前日翌日
		$last_date = last_date($date);
		$next_date = next_date($date);

		//前日
		if ($start_date == $last_date) {
			//深夜
			$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $start_date_time, $end_date_time, "0");
			//深夜以外
			$wk_day_time = $timecard_common_class->get_day_time($work_times_info, $start_date_time, $end_date_time, "0");
			//休日
            if (check_timecard_holwk(
                        $arr_timecard_holwk_day, $arr_type[$last_date], $pattern, $reason, "1", $timecard_bean) == true
				) {

				//深夜
				$hol_late_night += $wk_late_night;
				//深夜以外
				$hol_over += $wk_day_time;
			} else {
				//休日以外
				//深夜
				$late_night += $wk_late_night;
				//深夜以外
				$normal_over += $wk_day_time;
			}

		}

        $today_hol_flg = check_timecard_holwk($arr_timecard_holwk_day, $arr_type[$date], $pattern, $reason, "", $timecard_bean);
		//当日
		if ($start_date == $date) {
			//深夜
			$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $start_date_time, $end_date_time, "1");
            //休憩除外
            $wk_late_night_rest = 0;
            if ($rest_start_date_time != "" && $rest_end_date_time != "") {
                $wk_late_night_rest = $timecard_common_class->get_late_night($work_times_info, $rest_start_date_time, $rest_end_date_time, "1");
                $wk_late_night -= $wk_late_night_rest;
            }
			//深夜以外
			$wk_day_time = $timecard_common_class->get_day_time($work_times_info, $start_date_time, $end_date_time, "1");
            //休憩除外
            $wk_day_time_rest = 0;
            if ($rest_start_date_time != "" && $rest_end_date_time != "") {
                $wk_day_time_rest = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time, $rest_end_date_time, "1");
                $wk_day_time -= $wk_day_time_rest;
            }
            //休日
            if (($pattern != "10" && $today_hol_flg == true && $reason != "71") ||
                    $reason == "16" || //休日出勤
                    ($pattern == "10" && (($reason != "71"
                                && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //事由が普通残業の場合を除く
				) {

				//深夜
				$hol_late_night += $wk_late_night;
				//深夜以外
				$hol_over += $wk_day_time;
                //深夜休憩
                $hol_late_night_rest += $wk_late_night_rest;
                //深夜以外休憩
                $hol_over_rest += $wk_day_time_rest;
                if ($reason != "22") {
				    //６０時間超計算用休日残業
					$hol_over_sixty_data += $wk_day_time;
				    //６０時間超計算用休日深夜
					$hol_late_night_sixty_data += $wk_late_night;
                }
            } else {
				//休日以外
				//深夜
				$late_night += $wk_late_night;
				//深夜以外
				$normal_over += $wk_day_time;
                //深夜休憩
                $late_night_rest += $wk_late_night_rest;
                //深夜以外休憩
                $normal_over_rest += $wk_day_time_rest;
            }
		}

		//翌日
		if ($start_date == $next_date || $end_date == $next_date) {

			//深夜
			$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $start_date_time, $end_date_time, "2");
            $wk_late_night_rest = 0;
            if ($rest_start_date_time != "" && $rest_end_date_time != "") {
                $wk_late_night_rest = $timecard_common_class->get_late_night($work_times_info, $rest_start_date_time, $rest_end_date_time, "2");
                $wk_late_night -= $wk_late_night_rest;
            }
            //深夜以外
			$wk_day_time = $timecard_common_class->get_day_time($work_times_info, $start_date_time, $end_date_time, "2");
            //休憩除外
            $wk_day_time_rest = 0;
            if ($rest_start_date_time != "" && $rest_end_date_time != "") {
                $wk_day_time_rest = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time, $rest_end_date_time, "2");
                $wk_day_time -= $wk_day_time_rest;
            }
            
            //公休以外の日から連続した、残業は休日残業としない
            $renzoku_flg = false;
            $wk_end2_date_time = str_replace(":", "", $end2_date_time);
            $wk_start_date_time = str_replace(":", "", $start_date_time);
            if ($end2_date_time != "" && $wk_end2_date_time == $wk_start_date_time) {
                $renzoku_flg = true;
            }
            //echo "start_date=$start_date ovtm_flg={$ovtm_class->no_nextday_hol_ovtm_flg} $end2_date_time $start_date_time";
            if ($ovtm_class->no_nextday_hol_ovtm_flg == "t" && $renzoku_flg) {
                if ($today_hol_flg == true) {
                    $flg = true;
                }
                else {
                    $flg = false;
                }
            }
            else {
                //休日
                $flg = check_timecard_holwk(
                        $arr_timecard_holwk_day, $arr_type[$next_date], $next_pattern, $next_reason, "1", $timecard_bean);
            }
            if ($flg == true) { //20130308

				//深夜
				$hol_late_night += $wk_late_night;
				//深夜以外
				$hol_over += $wk_day_time;
                //深夜休憩
                $hol_late_night_rest += $wk_late_night_rest;
                //深夜以外休憩
                $hol_over_rest += $wk_day_time_rest;
                if ($next_reason != "22") {
				    //６０時間超計算用休日残業
					$hol_over_sixty_data += $wk_day_time;
				    //６０時間超計算用休日深夜
					$hol_late_night_sixty_data += $wk_late_night;
                }
            } else {
				//休日以外
				//深夜
				$late_night += $wk_late_night;
				//深夜以外
				$normal_over += $wk_day_time;
                //深夜休憩
                $late_night_rest += $wk_late_night_rest;
                //深夜以外休憩
                $normal_over_rest += $wk_day_time_rest;
            }
		}
	}
	$arr_data["normal_over"] = $normal_over; //普通残業
	$arr_data["late_night"] = $late_night; //深夜
	$arr_data["hol_over"] = $hol_over; //休日残業
	$arr_data["hol_late_night"] = $hol_late_night; //休日深夜
    //休憩
    $arr_data["normal_over_rest"] = $normal_over_rest; //普通残業
    $arr_data["late_night_rest"] = $late_night_rest; //深夜
    $arr_data["hol_over_rest"] = $hol_over_rest; //休日残業
    $arr_data["hol_late_night_rest"] = $hol_late_night_rest; //休日深夜

	$arr_data["hol_over_sixty_data"] = $hol_over_sixty_data; //６０時間超計算用休日残業
	$arr_data["hol_late_night_sixty_data"] = $hol_late_night_sixty_data; //６０時間超計算用休日深夜
    //echo "start_date=$start_date <br>\n";    
    //print_r($arr_data);
    
	return $arr_data;
}
function get_weekforty_authority($con, $fname, $emp_id, $timecard_bean){
	// 週40時間超残業対応 20130425

	$week_forty_flg = false;

	$sql = "select duty_form from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";

	switch($duty_form){
		case 1:
			if (($timecard_bean->over_forty_persons_full == "t") && ($timecard_bean->over_disp_forty_flg == "t")){
				$week_forty_flg = true;
			}
			break;
		case 2:
			if (($timecard_bean->over_forty_persons_part == "t") && ($timecard_bean->over_disp_forty_flg == "t")){
				$week_forty_flg = true;
			}
			break;
		case 3:
			if (($timecard_bean->over_forty_persons_short == "t") && ($timecard_bean->over_disp_forty_flg == "t")){
				$week_forty_flg = true;
			}
			break;
	}

	return $week_forty_flg;
}

function get_kintai_late_absence_data($con, $fname, $emp_id, $start_date, $end_date) {
	// 総合届け使用フラグ
	$general_flag = file_exists("opt/general_flag");
	$start_date_nm = ($general_flag) ? "start_date1" : "start_date";
	$end_date_nm = ($general_flag) ? "end_date1" : "end_date";
    $arr_data = array();
	//休暇申請の欠勤データ
    $sql = "SELECT ";
    $sql .= " a.*, ";
    $sql .= " b.holiday_count ";
    $sql .= "FROM kintai_hol a ";
    $sql .= "LEFT JOIN atdbk_reason_mst b ON b.reason_id = a.reason ";
    $sql .= "WHERE ";
    $sql .= " a.emp_id = '".$emp_id."' AND ";
    $sql .= " (a.apply_stat = '0' OR ";
    $sql .= "  a.apply_stat = '1') AND ";
    $sql .= " (a.kind_flag = '6') AND ";
    $sql .= " ((a.$start_date_nm >= '$start_date' AND a.$start_date_nm <= '$end_date') OR ";
    $sql .= "  (a.$end_date_nm   >= '$start_date' AND a.$end_date_nm   <= '$end_date')) ";
    $sql .= "ORDER BY ";
    $sql .= " a.$start_date_nm  ";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $wk_start_date = $row["$start_date_nm"];

        $wk_end_date = $row["$end_date_nm"];
        $apply_stat = $row["apply_stat"];

        $apply_date = $row["apply_date"];

        $wk_reason = $row["reason"];

        $tmp_date = $wk_start_date;

        while ($tmp_date <= $wk_end_date) {
            if ($tmp_date >= $start_date && $tmp_date <= $end_date) {
                $arr_data[$tmp_date]["apply_stat"] = $apply_stat;
                $arr_data[$tmp_date]["apply_date"] = $apply_date;
                $arr_data[$tmp_date]["reason"] = $wk_reason;
                $arr_data[$tmp_date]["holiday_count"] = $row["holiday_count"];
            }
            $timestamp = date_utils::to_timestamp_from_ymd($tmp_date);
            $tmp_date = date("Ymd", strtotime("+1 day", $timestamp));
        }
	
	}

	$wk_end_date = next_date($end_date);
    //遅参早退の欠勤振替
    $sql = "select a.* from kintai_late a ";
    $cond ="where a.emp_id = '$emp_id' and ((a.start_date >= '$start_date' and a.start_date <= '$wk_end_date' ) or (a.end_date >= '$start_date' and a.end_date <= '$wk_end_date' )) and a.transfer = 2 order by a.start_date;";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
    	if ($row["start_date"] >= $start_date && $row["start_date"] <= $end_date) {
            $arr_data[$row["start_date"]] = $row;
        }
        else {
            $arr_data[$row["end_date"]] = $row;
        }
    }
	return $arr_data;
}

function get_calendar_type($con, $fname, $start_date, $end_date) {
	$arr_type = array();
    // 指定期間の勤務日種別を取得
	$sql = "select date, type from calendar";
    $cond = "where date >= '$start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
	return $arr_type;
}

?>