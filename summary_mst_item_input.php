<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("show_select_values.ini");
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("label_by_profile_type.ini");
require("summary_tmplitemrireki.ini");
// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 59, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

//画面情報
$page_title = "管理項目作成";
$submit_name = "登録";
if($mode == "update")
{
	$page_title = "管理項目更新";
	$submit_name = "更新";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ポストバック時の処理
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($is_postback == "true")
{
	//==============================
	//入力チェック
	//==============================
	$err_msg = "";

	//項目コード:必須チェック
	if($err_msg == "")
	{
		if($item_cd == "")
		{
			$err_msg = "項目コードを入力してください。";
		}
	}
	//項目コード:文字種、文字長チェック
	if($err_msg == "")
	{
		if (!preg_match("/^[a-zA-Z0-9]+$/", $item_cd) || (strlen($item_cd) > 4))
		{
			$err_msg = "項目コードは半角英数４桁以内で入力してください。";
		}
	}
	//項目コード:重複チェック
	if($err_msg == "")
	{
		$sql  = "select count(*) as cnt from tmplitem";
		$cond = "where mst_cd = '$mst_cd' and item_cd = '$item_cd' and item_cd != '$target_item_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_fetch_result($sel, 0, "cnt");
		if ($cnt > 0) {
			$err_msg = "項目コード「".$item_cd."」は既に使用されております。";
		}
	}
	//項目名称:必須チェック
	if($err_msg == "")
	{
		if($item_name == "")
		{
			$err_msg = "項目コードを入力してください。";
		}
	}

	//項目名称:長さチェック
	if (strlen($item_name) > 40)
	{
		$err_msg = "項目名称を全角20文字以内で入力してください。";
	}

	//項目名称:重複チェック
	if($err_msg == "")
	{
		$sql  = "select count(*) as cnt from tmplitem";
		$cond = "where mst_cd = '$mst_cd' and item_name = '".pg_escape_string($item_name)."' and item_cd != '$target_item_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_fetch_result($sel, 0, "cnt");
		if ($cnt > 0) {
			$err_msg = "項目名称「".$item_name."」は既に使用されております。";
		}
	}
	//表示順:数値チェック
	if($err_msg == "")
	{
//		if (!is_numeric($disp_order) || !is_int($disp_order+0))
		if (!preg_match("/^[0-9]*$/", $disp_order) )
		{
			$err_msg ="表示順は数値を入力してください。";
		}
	}
/*
	//連携コード１:文字種チェック
	if($err_msg == "")
	{
		if (!preg_match("/^[a-zA-Z0-9]*$/", $link_cd1) )
		{
			$err_msg ="連携コード１は半角英数で入力してください。";
		}
	}
	//連携コード２:文字種チェック
	if($err_msg == "")
	{
		if (!preg_match("/^[a-zA-Z0-9]*$/", $link_cd2) )
		{
			$err_msg ="連携コード２は半角英数で入力してください。";
		}
	}
	//連携コード３:文字種チェック
	if($err_msg == "")
	{
		if (!preg_match("/^[a-zA-Z0-9]*$/", $link_cd3) )
		{
			$err_msg ="連携コード３は半角英数で入力してください。";
		}
	}
*/
	if($err_msg != "")
	{
		//==============================
		//エラー表示
		//==============================
		echo("<script language='javascript'>");
		echo("alert('$err_msg');");
		echo("</script>");
	}
	else
	{
		//==============================
		//登録処理
		//==============================
		if($mode == "insert")
		{
			// トランザクションの開始
			pg_query($con, "begin");

			$sql = "insert into tmplitem (mst_cd, item_cd, item_name, disp_flg, disp_order, sort1, sort2, link_cd1, link_cd2, link_cd3, create_time, create_emp_id, update_time, update_emp_id) values (";
			$content = array(
				$mst_cd,
				$item_cd,
				pg_escape_string($item_name),
				$disp_flg,
				intval($disp_order),
				pg_escape_string($sort1),
				pg_escape_string($sort2),
				$link_cd1,
				$link_cd2,
				$link_cd3,
				date("YmdHis"),
				$emp_id,
				date("YmdHis"),
				$emp_id);
			$in = insert_into_table($con,$sql,$content,$fname);
			if($in == 0){
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			//履歴更新
			entry_tmplitemrireki($con,$fname,$mst_cd);

			// トランザクションのコミット
			pg_query($con, "commit");

		}
		//==============================
		//更新処理
		//==============================
		if($mode == "update")
		{
			// トランザクションの開始
			pg_query($con, "begin");

			$sql = "update tmplitem set";
			$set = array("item_cd", "item_name", "disp_flg", "disp_order", "sort1", "sort2", "link_cd1", "link_cd2", "link_cd3", "update_time", "update_emp_id");
			$setvalue = array(
				$item_cd,
				pg_escape_string($item_name),
				$disp_flg,
				intval($disp_order),
				pg_escape_string($sort1),
				pg_escape_string($sort2),
				$link_cd1,
				$link_cd2,
				$link_cd3,
				date("YmdHis"),
				$emp_id);
			$cond = "where mst_cd = '$mst_cd' and item_cd = '$target_item_cd'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			//履歴更新
			entry_tmplitemrireki($con,$fname,$mst_cd);

			// トランザクションのコミット
			pg_query($con, "commit");

		}
		//==============================
		//画面終了
		//==============================
		echo("<script language='javascript'>");
		echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
		echo("window.close();");
		echo("</script>");
		exit;
	}
}
//初回アクセス時
else
{
	$target_item_cd = $item_cd;

	if($mode == "update")
	{
		//現在値の取得／変数展開
		$sql = "select * from tmplitem";
		$cond = "where mst_cd = '$mst_cd' and item_cd = '$target_item_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$item_info = pg_fetch_array($sel);
		foreach($item_info as $key => $val)
		{
			$$key = $val;
		}
	}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HTML出力
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<title><?=$med_report_title?> | <?=$page_title?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<form name="mainform" action="summary_mst_item_input.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="<?=$mode?>">
<input type="hidden" name="mst_cd" value="<?=$mst_cd?>">
<input type="hidden" name="target_item_cd" value="<?=$target_item_cd?>">
<input type="hidden" name="is_postback" value="true">
<table width="720" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="720" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目コード</font></td>
<td style="border-right:none;"><input type="text" name="item_cd" value="<?=$item_cd?>" maxlength="4" size="6" style="ime-mode: inactive;"></td>
</td>
<td style="border-left:none;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※先頭を数字にして登録してください。（例：0001、1001）<br>
先頭が英字のコードをシステムで使用します。（例：A001、Z001）</font></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目名称</font></td>
<td colspan="2"><input type="text" name="item_name" value="<?=$item_name?>" maxlength="40" size="46" style="ime-mode: active;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示区分</font></td>
<td colspan="2">
<select name="disp_flg">
<option value="t" <?if($disp_flg != "f"){?>selected<?}?> >表示</option>
<option value="f" <?if($disp_flg == "f"){?>selected<?}?> >非表示</option>
</select>
</td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ソート区分１</font></td>
<td colspan="2"><input type="text" name="sort1" value="<?=$sort1?>" maxlength="10" size="46" style="ime-mode: active;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ソート区分２</font></td>
<td colspan="2"><input type="text" name="sort2" value="<?=$sort2?>" maxlength="10" size="46" style="ime-mode: active;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
<td colspan="2"><input type="text" name="disp_order" value="<?=$disp_order?>" maxlength="4" size="4" style="ime-mode: inactive;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携コード１</font></td>
<td colspan="2"><input type="text" name="link_cd1" value="<?=$link_cd1?>" maxlength="10" size="20" style="ime-mode: inactive;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携コード２</font></td>
<td colspan="2"><input type="text" name="link_cd2" value="<?=$link_cd2?>" maxlength="10" size="20" style="ime-mode: inactive;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携コード３</font></td>
<td colspan="2"><input type="text" name="link_cd3" value="<?=$link_cd3?>" maxlength="10" size="20" style="ime-mode: inactive;"></td>
</tr>

<?
if($mode == "update")
{
?>
<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日付</font></td>
<td colspan="2">
<?=substr($update_time,0,4)."/".substr($update_time,4,2)."/".substr($update_time,6,2)?>
<input type="hidden" name="update_time" value="<?=$update_time?>">
</td>
</tr>
<?
}
?>

</table>
<table width="720" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" align="right"><input type="submit" value="<?=$submit_name?>"></td>
</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
