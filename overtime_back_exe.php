<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("atdbk_workflow_common_class.php");
require_once("ovtm_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//出勤時刻の時分チェック
if (($start_hour != "--" && $start_min == "--") || ($start_hour == "--" && $start_min != "--")) {
    echo("<script type=\"text/javascript\">alert('出勤時刻の時と分の一方のみは登録できません。');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
//退勤時刻の時分チェック
if (($end_hour != "--" && $end_min == "--") || ($end_hour == "--" && $end_min != "--")) {
    echo("<script type=\"text/javascript\">alert('退勤時刻の時と分の一方のみは登録できません。');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
//残業時刻1の未入力チェック 20131125
if ($over_start_hour == "--" || $over_start_min == "--" || $over_end_hour == "--" || $over_end_min == "--") {
    echo("<script type=\"text/javascript\">alert('残業時刻1は必ず入力してください。');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
if (($over_start_hour == "--" && $over_start_min != "--") || ($over_start_hour != "--" && $over_start_min == "--") || ($over_end_hour != "--" && $over_end_min == "--") || ($over_end_hour == "--" && $over_end_min != "--")) {
	echo("<script type=\"text/javascript\">alert('残業時刻1の時と分の一方のみは登録できません。');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}
//残業時刻2の終了側未入力チェック 20131125
if (($over_start_hour2 != "--" && $over_start_min2 != "--") && ($over_end_hour2 == "--" || $over_end_min2 == "--")) {
    echo("<script type=\"text/javascript\">alert('残業時刻2の終了時刻を入力してください。');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
if (($over_start_hour2 == "--" || $over_start_min2 == "--") && ($over_end_hour2 != "--" && $over_end_min2 != "--")) {
    echo("<script type=\"text/javascript\">alert('残業時刻2の開始時刻を入力してください。');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
if (($over_start_hour2 == "--" && $over_start_min2 != "--") || ($over_start_hour2 != "--" && $over_start_min2 == "--") || ($over_end_hour2 != "--" && $over_end_min2 == "--") || ($over_end_hour2 == "--" && $over_end_min2 != "--")) {
	echo("<script type=\"text/javascript\">alert('残業時刻2の時と分の一方のみは登録できません。');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}
// 入力チェック
if ($reason_id == "other") {
	if ($reason == "") {
		echo("<script type=\"text/javascript\">alert('理由が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($reason) > 200) {
		echo("<script type=\"text/javascript\">alert('理由が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$reason_id = null;
} else {
	$reason = "";
}

// データベースに接続
$con = connect2db($fname);

$obj = new atdbk_workflow_common_class($con, $fname);

$ovtm_class = new ovtm_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 申請情報を取得
$sql = "select emp_id, target_date, ovtmcfm from ovtmapply";
$cond = "where apply_id = $apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$date = pg_fetch_result($sel, 0, "target_date");
$ovtmcfm = pg_fetch_result($sel, 0, "ovtmcfm");

//空・未入力の場合、翌日フラグを初期化(0にする)
if ($next_day_flag == null || $next_day_flag == ""){
	$next_day_flag = 0;
}

if (empty($previous_day_flag)){
	$previous_day_flag = 0;
}
//キーボート入力の場合、前ゼロ対応
if ($ovtm_class->keyboard_input_flg == "t") { 
    
    if ($start_hour != "" && ($start_hour >= "0" && $start_hour <= "9")) {
        $start_hour = sprintf("%02d", $start_hour);
    }
    if ($start_min != "" && ($start_min >= "0" && $start_min <= "9")) {
        $start_min = sprintf("%02d", $start_min);
    }
    if ($end_hour != "" && ($end_hour >= "0" && $end_hour <= "9")) {
        $end_hour = sprintf("%02d", $end_hour);
    }
    if ($end_min != "" && ($end_min >= "0" && $end_min <= "9")) {
        $end_min = sprintf("%02d", $end_min);
    }
    
    for ($i=1; $i<=5; $i++) {
        $idx = ($i == 1) ? "" : $i;
        $s_h = "over_start_hour".$idx;
        $s_m = "over_start_min".$idx;
        $e_h = "over_end_hour".$idx;
        $e_m = "over_end_min".$idx;
        
        if ($$s_h != "" && ($$s_h >= "0" && $$s_h <= "9")) {
            $$s_h = sprintf("%02d", $$s_h);
        }
        if ($$s_m != "" && ($$s_m >= "0" && $$s_m <= "9")) {
            $$s_m = sprintf("%02d", $$s_m);
        }
        if ($$e_h != "" && ($$e_h >= "0" && $$e_h <= "9")) {
            $$e_h = sprintf("%02d", $$e_h);
        }
        if ($$e_m != "" && ($$e_m >= "0" && $$e_m <= "9")) {
            $$e_m = sprintf("%02d", $$e_m);
        }
        $rs_h = "rest_start_hour".$i;
        $rs_m = "rest_start_min".$i;
        $re_h = "rest_end_hour".$i;
        $re_m = "rest_end_min".$i;
        if ($$rs_h != "" && ($$rs_h >= "0" && $$rs_h <= "9")) {
            $$rs_h = sprintf("%02d", $$rs_h);
        }
        if ($$rs_m != "" && ($$rs_m >= "0" && $$rs_m <= "9")) {
            $$rs_m = sprintf("%02d", $$rs_m);
        }
        if ($$re_h != "" && ($$re_h >= "0" && $$re_h <= "9")) {
            $$re_h = sprintf("%02d", $$re_h);
        }
        if ($$re_m != "" && ($$re_m >= "0" && $$re_m <= "9")) {
            $$re_m = sprintf("%02d", $$re_m);
        }
    }
}

$over_start_time = ($over_start_hour == "--") ? "" : "$over_start_hour$over_start_min";
$over_end_time = ($over_end_hour == "--") ? "" : "$over_end_hour$over_end_min";
$over_start_time2 = ($over_start_hour2 == "--") ? "" : "$over_start_hour2$over_start_min2";
$over_end_time2 = ($over_end_hour2 == "--") ? "" : "$over_end_hour2$over_end_min2";
$maxline = 5; //20141205

for ($i=3; $i<=$maxline; $i++) {
    $s_t = "over_start_time".$i;
    $s_h = "over_start_hour".$i;
    $s_m = "over_start_min".$i;
    $s_f = "over_start_next_day_flag".$i;
    $$s_t = ($$s_h == "--") ? "" : $$s_h.$$s_m;
    if ($$s_t == "" || $$s_t == null || $$s_f == "" || $$s_f == null) {
        $$s_f = 0;
    }
    $e_t = "over_end_time".$i;
    $e_h = "over_end_hour".$i;
    $e_m = "over_end_min".$i;
    $e_f = "over_end_next_day_flag".$i;
    $$e_t = ($$e_h == "--") ? "" : $$e_h.$$e_m;
    if ($$e_t == "" || $$e_t == null || $$e_f == "" || $$e_f == null) {
        $$e_f = 0;
    }
}    

//残業開始1翌日フラグを初期化(0にする)
if ($over_start_time == "" || $over_start_time == null || $over_start_next_day_flag == "" || $over_start_next_day_flag == null){
	$over_start_next_day_flag = 0;
}
//残業終了1翌日フラグを初期化(0にする)
if ($over_end_time == "" || $over_end_time == null || $over_end_next_day_flag == "" || $over_end_next_day_flag == null){
	$over_end_next_day_flag = 0;
}
//残業開始2翌日フラグを初期化(0にする)
if ($over_start_time2 == "" || $over_start_time2 == null || $over_start_next_day_flag2 == "" || $over_start_next_day_flag2 == null){
	$over_start_next_day_flag2 = 0;
}
//残業終了2翌日フラグを初期化(0にする)
if ($over_end_time2 == "" || $over_end_time2 == null || $over_end_next_day_flag2 == "" || $over_end_next_day_flag2 == null){
	$over_end_next_day_flag2 = 0;
}
for ($i=1; $i<=$maxline; $i++) {
    $s_t = "rest_start_time".$i;
    $s_h = "rest_start_hour".$i;
    $s_m = "rest_start_min".$i;
    $s_f = "rest_start_next_day_flag".$i;
    $$s_t = ($$s_h == "--") ? "" : $$s_h.$$s_m;
    if ($$s_t == "" || $$s_t == null || $$s_f == "" || $$s_f == null) {
        $$s_f = 0;
    }
    $e_t = "rest_end_time".$i;
    $e_h = "rest_end_hour".$i;
    $e_m = "rest_end_min".$i;
    $e_f = "rest_end_next_day_flag".$i;
    $$e_t = ($$e_h == "--") ? "" : $$e_h.$$e_m;
    if ($$e_t == "" || $$e_t == null || $$e_f == "" || $$e_f == null) {
        $$e_f = 0;
    }
}    
//追加分の変更確認
$chg_flag = false;
for ($i=3; $i<=$maxline; $i++) {
    $s_t = "over_start_time".$i;
    $s_f = "over_start_next_day_flag".$i;
    $ps_t = "pre_over_start_time".$i;
    $ps_f = "pre_over_start_next_day_flag".$i;
    $e_t = "over_end_time".$i;
    $e_f = "over_end_next_day_flag".$i;
    $pe_t = "pre_over_end_time".$i;
    $pe_f = "pre_over_end_next_day_flag".$i;
    if ($$s_t != $$ps_t || $$s_f != $$ps_f || $$e_t != $$pe_t || $$e_f != $$pe_f) {
        $chg_flag = true;
        break;
    }
}
if (!$chg_flag) {
    for ($i=1; $i<=$maxline; $i++) {
        $s_t = "rest_start_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $ps_t = "pre_rest_start_time".$i;
        $ps_f = "pre_rest_start_next_day_flag".$i;
        $e_t = "rest_end_time".$i;
        $e_f = "rest_end_next_day_flag".$i;
        $pe_t = "pre_rest_end_time".$i;
        $pe_f = "pre_rest_end_next_day_flag".$i;
        if ($$s_t != $$ps_t || $$s_f != $$ps_f || $$e_t != $$pe_t || $$e_f != $$pe_f) {
            $chg_flag = true;
            break;
        }
    }
}

// 出勤時刻、退勤時刻、残業時刻が変わる場合
$start_time = ($start_hour == "--") ? "" : "$start_hour$start_min";
$end_time = ($end_hour == "--") ? "" : "$end_hour$end_min";
if ($start_time != $pre_start_time || $previous_day_flag != $pre_previous_day_flag ||
        $end_time != $pre_end_time || $next_day_flag != $pre_next_day_flag || 
        $over_start_time != $pre_over_start_time || $over_end_time != $pre_over_end_time ||
        $over_start_next_day_flag != $pre_over_start_next_day_flag ||
        $over_end_next_day_flag != $pre_over_end_next_day_flag || 
        $over_start_time2 != $pre_over_start_time2 || $over_end_time2 != $pre_over_end_time2 ||
        $over_start_next_day_flag2 != $pre_over_start_next_day_flag2 ||
        $over_end_next_day_flag2 != $pre_over_end_next_day_flag2 || $chg_flag) {
	
	// 勤務時間修正申請が出されている場合
	$sql = "select apply_id from tmmdapply";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' and not delete_flg and re_apply_id is null";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$tmmd_apply_id = pg_fetch_result($sel, 0, "apply_id");

		// 勤務時間修正申請の退勤時間とステータスを更新
		$sql = "update tmmdapply set";
		$set = array("a_end_time", "apply_status", "a_next_day_flag", "a_start_time", "a_previous_day_flag", "a_over_start_time", "a_over_end_time", "a_over_start_next_day_flag", "a_over_end_next_day_flag", "a_over_start_time2", "a_over_end_time2", "a_over_start_next_day_flag2", "a_over_end_next_day_flag2");
		$setvalue = array($end_time, 0, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
        //追加
        for ($i=3; $i<=$maxline; $i++) {
            $s_t = "a_over_start_time".$i;
            $e_t = "a_over_end_time".$i;
            $s_f = "a_over_start_next_day_flag".$i;
            $e_f = "a_over_end_next_day_flag".$i;
            $s_tv = "over_start_time".$i;
            $e_tv = "over_end_time".$i;
            $s_fv = "over_start_next_day_flag".$i;
            $e_fv = "over_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, $$s_tv);
            array_push($setvalue, $$e_tv);
            array_push($setvalue, $$s_fv);
            array_push($setvalue, $$e_fv);
        }    
        for ($i=1; $i<=$maxline; $i++) {
            $s_t = "a_rest_start_time".$i;
            $e_t = "a_rest_end_time".$i;
            $s_f = "a_rest_start_next_day_flag".$i;
            $e_f = "a_rest_end_next_day_flag".$i;
            $s_tv = "rest_start_time".$i;
            $e_tv = "rest_end_time".$i;
            $s_fv = "rest_start_next_day_flag".$i;
            $e_fv = "rest_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, $$s_tv);
            array_push($setvalue, $$e_tv);
            array_push($setvalue, $$s_fv);
            array_push($setvalue, $$e_fv);
        }    
        $cond = "where apply_id = '$tmmd_apply_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 勤務時間修正申請承認情報の承認日とステータスを更新
		$sql = "update tmmdaprv set";
		$set = array("aprv_date", "aprv_status", "aprv_comment");
		$setvalue = array(null, 0, "");
		$cond = "where apply_id = '$tmmd_apply_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

	//残業申請不要データを削除する
	$obj->delete_no_apply_ovtm_data($emp_id, $date);
	// 申請IDを採番
	$sql = "select max(apply_id) from ovtmapply";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$new_apply_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

	// 申請情報を登録
$sql = "insert into ovtmapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, comment, ovtmcfm, delete_flg, end_time, next_day_flag, start_time, previous_day_flag, over_start_time, over_end_time, over_start_next_day_flag, over_end_next_day_flag, reason_detail, over_start_time2, over_end_time2, over_start_next_day_flag2, over_end_next_day_flag2";
//追加 20140623
for ($i=3; $i<=$maxline; $i++) {
    $sql .= ", over_start_time".$i;
    $sql .= ", over_end_time".$i;
    $sql .= ", over_start_next_day_flag".$i;
    $sql .= ", over_end_next_day_flag".$i;
}
for ($i=1; $i<=$maxline; $i++) {
    $sql .= ", rest_start_time".$i;
    $sql .= ", rest_end_time".$i;
    $sql .= ", rest_start_next_day_flag".$i;
    $sql .= ", rest_end_next_day_flag".$i;
}
//追加 20150105
for ($i=2; $i<=$maxline; $i++) {
    $sql .= ", reason_id".$i;
    $sql .= ", reason".$i;
}
$sql .= ") values (";
$content = array($new_apply_id, $emp_id, $date, $reason_id, pg_escape_string($reason), date("YmdHis"), '0', pg_escape_string($comment), $ovtmcfm, 'f', $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, pg_escape_string($reason_detail), $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
    //追加
    for ($i=3; $i<=$maxline; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
        array_push($content, $$s_t);
        array_push($content, $$e_t);
        array_push($content, $$s_f);
        array_push($content, $$e_f);
    }    
    for ($i=1; $i<=$maxline; $i++) {
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        array_push($content, $$s_t);
        array_push($content, $$e_t);
        array_push($content, $$s_f);
        array_push($content, $$e_f);
    }    
    //追加 20150105
    for ($i=2; $i<=$maxline; $i++) {
        $reason_id_nm = "reason_id".$i;
        $reason_nm = "reason".$i;
        $s_t = "over_start_time".$i;
        if ($$s_t == "") {
			$$reason_id_nm = null;
			$$reason_nm = null;
        }
        else {
			if ($$reason_id_nm == "other") {
				$$reason_id_nm = null;
			}
		}
        array_push($content, $$reason_id_nm);
        array_push($content, pg_escape_string($$reason_nm));
    }
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
		
		// 承認者情報を登録
		$sql  = "insert into ovtmaprv ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "aprv_no, ";
		$sql .= "aprv_emp_id, ";
		$sql .= "null, ";
		$sql .= "'0', ";
		$sql .= "'f', ";
		$sql .= "'t', ";
		$sql .= "aprv_sub_no, ";
		$sql .= "multi_apv_flg, ";
		$sql .= "next_notice_div, ";
		$sql .= "'f', ";
		$sql .= "emp_st, ";
		$sql .= "'' ";
		$sql .= "from ovtmaprv ";
		$sql .= "where apply_id = $apply_id";
		
		$ins = insert_into_table($con, $sql, "", $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 承認者候補登録
		$sql  = "insert into ovtmaprvemp ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "aprv_no, ";
		$sql .= "person_no, ";
		$sql .= "emp_id, ";
		$sql .= "'f' ";
		$sql .= "from ovtmaprvemp ";
		$sql .= "where apply_id = $apply_id";
		
		$ins = insert_into_table($con, $sql, "", $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 非同期・同期受信登録
		$sql  = "insert into ovtm_async_recv ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "send_aprv_no, ";
		$sql .= "send_aprv_sub_no, ";
		$sql .= "recv_aprv_no, ";
		$sql .= "recv_aprv_sub_no, ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "'f' ";
		$sql .= "from ovtm_async_recv ";
		$sql .= "where apply_id = $apply_id";
		
		$ins = insert_into_table($con, $sql, "", $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	
	// 元の申請書に再申請ＩＤを更新
	$sql = "update ovtmapply set";
	$set = array("re_apply_id");
	$setvalue = array($new_apply_id);
	$cond = "where apply_id = $apply_id";	

	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

// 勤務実績を更新
$sql = "update atdbkrslt set";
$set = array("end_time", "next_day_flag", "start_time", "previous_day_flag", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
$setvalue = array($end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
//追加
for ($i=3; $i<=$maxline; $i++) {
    $s_t = "over_start_time".$i;
    $e_t = "over_end_time".$i;
    $s_f = "over_start_next_day_flag".$i;
    $e_f = "over_end_next_day_flag".$i;
    array_push($set, $s_t);
    array_push($set, $e_t);
    array_push($set, $s_f);
    array_push($set, $e_f);
    array_push($setvalue, $$s_t);
    array_push($setvalue, $$e_t);
    array_push($setvalue, $$s_f);
    array_push($setvalue, $$e_f);
}    
for ($i=1; $i<=$maxline; $i++) {
    $s_t = "rest_start_time".$i;
    $e_t = "rest_end_time".$i;
    $s_f = "rest_start_next_day_flag".$i;
    $e_f = "rest_end_next_day_flag".$i;
    array_push($set, $s_t);
    array_push($set, $e_t);
    array_push($set, $s_f);
    array_push($set, $e_f);
    array_push($setvalue, $$s_t);
    array_push($setvalue, $$e_t);
    array_push($setvalue, $$s_f);
    array_push($setvalue, $$e_f);
}    
$cond = "where emp_id = '$emp_id' and date = '$date'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュして自画面を閉じる
if($pnt_url != "") {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
	echo("<script language=\"javascript\">window.close();</script>\n");
}
?>
