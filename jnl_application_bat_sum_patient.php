<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病院別月間報告書患者数総括(一括表示)</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");
require_once("jnl_application_common.ini");


$fname = $PHP_SELF;
// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);
$con = connect2db($fname);
// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);
?>
<script type="text/javascript">
<!--
function changeOptionType(e) {
    type = document.getElementById('hidden_type');

    if (e.value==type.value) {
        return;
    }
    else {
        plant = document.getElementById('plant');
        plant.innerHTML = '';
        if (e.value == 1) {
            hospital = document.getElementById('hospital');
            for (l=0; l < hospital.options.length; l++) {
                plant.options[l] = new Option(hospital.options[l].text, hospital.options[l].value);
            }
        }
        else if (e.value == 2) {
            healthFacilities = document.getElementById('health_facilities');
            for (l=0; l<healthFacilities.options.length; l++) {
                plant.options[l] = new Option(healthFacilities.options[l].text, healthFacilities.options[l].value);
            }
        }

        type.value = e.value;
        return changeOptionPlant(plant);
    }
}

function changeOptionPlant(e) {
    trHospital = document.getElementById('tr_hospital');
    trHealthFacilities = document.getElementById('tr_health_facilities');
    if (e.value=='') {
        trHospital.style.display = 'none';
        trHealthFacilities.style.display = 'none';
    }
    else {
        type = document.getElementById('hidden_type');
        if (type.value==1) {
            trHospital.style.display = '';
            trHealthFacilities.style.display = 'none';
        }
        else if (type.value==2) {
            trHospital.style.display = 'none';
            trHealthFacilities.style.display = '';
        }
    }
}

function clickLink(cNum, scNum) {
    wkfwForm = document.getElementById("wkfw");
    wkfwForm.action = 'jnl_application_indicator_menu.php';
    wkfwForm.method = 'get';

    plant = document.getElementById('plant');

    category = document.getElementById('category');
    category.value = cNum;

    subCategory = document.getElementById('sub_category');
    subCategory.value = scNum;

    type = document.getElementById('type');
    type.value = 'plant';

    wkfwForm.submit();
    return;
}

function selectYear(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfw = document.getElementById('wkfw');
    wkfw.action = 'jnl_application_indicator_menu.php';
    wkfw.method = 'get';

    type = document.getElementById('type');
    category = document.getElementById('category');
    plant = document.getElementById('plant');
    subCategory = document.getElementById('sub_category');
    displayYear = document.getElementById('display_year');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant.value!=null) {
            hiddenType = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value = 'plant';
        }
        else {
            category.value = '0';
        }
        subCategory.parentNode.removeChild(subCategory);
    }
    else if (category==null) {
        alert('check');
        alert(category.value);
        alert(subCategory.value);
        alert(no.value);
    }
    else if (subCategory==null) {
        alert('check_a');
        alert(category.value);
    }

    displayYear.value = e.value;
    wkfw.submit();
    //return;
}
//-->
</script>
<script type="text/javascript" src="js/jnl/jnl_indicator_ajax.js"></script>
<script type="text/javascript" src="js/jnl/jnl_indicator_menu_list.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>

<style type="text/css">
table#indicator {
    margin-left: auto;
    margin-right: auto;
}
table#indicator td {
    background-color: #ffffdd;
    padding: 20px;
    text-align: center;
}
</style>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#f6f9ff">
                        <td width="32" height="32" class="spacing">
                            <a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="決裁・印影登録"></a>
                        </td>
                        <td>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_indicator_menu.php?session=<? echo($session); ?>"><b>管理指標</b></a></font>
                        </td>
<? if ($workflow_auth == "1") { ?>
                        <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
<? show_applycation_menuitem($session,$fname,""); ?>
                    </tr>
                </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form id="wkfw" name="wkfw" action="jnl_application_imprint.php" method="post" enctype="multipart/form-data">


<table width="100%" border="0" cellspacing="0" cellpadding="0">



	<tr>
		<td><h3 style="background-color: #ffdddd;">病院別月間報告書患者数総括(一括表示)</h3></td>
	</tr>


</table>

<table  border="0" cellspacing="0" cellpadding="0">

<tr>
	<td valign="top">

		<table border='0' cellspacing='0' cellpadding='1' class='list'>
			<tr>
				<td bgcolor='C8FFDD'><b>病院集計</b></td>
			</tr>
			<tr>
				<td>
					<table border='0'>

<?php
//ディレクトリ
$comedix_dire =substr($fname,0,( strlen($fname) - strlen(basename(__FILE__)) ));
$dirName = "./jnl_apply/batch/071_all/";
// 運用ディレクトリ名を取得
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
$lib_url = "http://" . $_SERVER['HTTP_HOST'] .$comedix_dire."jnl_apply/batch/071_all/" ;
//ディレクトの存在チェック
if (is_dir($dirName)) 
{
	
	//ディレクトリハンドル取得
	if ($dir = opendir($dirName)) 
	{
		
		//ファイル読み込み、表示
		while (($file = readdir($dir)) !== false) 
		{
			if(substr($file,(strlen($file) - 4),4) == ".xls")
			{
				//ディレクトリ名をはずして、ファイル名だけ取得する
				$tmp_arry[]=$file;
			}
		}
		closedir($dir);
	}
} 


sort($tmp_arry);
for($cnt = 0;$cnt < count($tmp_arry);$cnt++)
{
	$file_path ="";
	$file_path = $lib_url.$tmp_arry[$cnt];
	$file_path = mb_convert_encoding($file_path,mb_internal_encoding(),'utf-8');
	
	echo("<tr>");
	echo("<td>");	
	echo("<h3><a href=\"$file_path\">".mb_convert_encoding($tmp_arry[$cnt],mb_internal_encoding(),'utf-8')."</a></h3>");	
	echo("</td>");	
	echo("</tr>");
	
}
?>
					</table>	
				</td>
			</tr>
		</table>
	</td>
	</td>
	
	<td valign="top" align="left">　</td>

<?
$tmp_arry = null;
//ディレクトリ
$dirName = "./jnl_apply/batch/071_facility/";
// 運用ディレクトリ名を取得
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
//ディレクトの存在チェック
if (is_dir($dirName)) 
{
	
	//ディレクトリハンドル取得
	if ($dir = opendir($dirName)) 
	{
		
		//ファイル読み込み、表示
		while (($file = readdir($dir)) !== false) 
		{
			if(substr($file,0,1) == "0")
			{
				//ディレクトリ名をはずして、ファイル名だけ取得する
				$tmp_arry[]=$file;
			}
		}
		closedir($dir);
	}
} 

sort($tmp_arry);
$tmp_arry_div1 = null;
$tmp_arry_div2 = null;
$tmp_arry_div3 = null;

$sql = "select jnl_facility_id from jnl_facility_master";
$cond = "where jnl_facility_type='1' order by jnl_facility_id";
$fac_name = select_from_table($con, $sql, $cond, $fname);
if ($fac_name == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$div_cnt = 1;
while ($row = pg_fetch_array($fac_name)) 
{
	
	if($div_cnt%3==0)
	{
		for($cnt = 0;$cnt < count($tmp_arry);$cnt++)
		{
			$set_id = intval(substr($tmp_arry[$cnt],0,3));
			if($row["jnl_facility_id"] == $set_id)
			{
				$tmp_arry_div1[]=$tmp_arry[$cnt];
			}
		}
	}
	elseif($div_cnt%3==1)
	{
		for($cnt = 0;$cnt < count($tmp_arry);$cnt++)
		{
			$set_id = intval(substr($tmp_arry[$cnt],0,3));
			if($row["jnl_facility_id"] == $set_id)
			{
				$tmp_arry_div3[]=$tmp_arry[$cnt];
			}
		}
	}
	elseif($div_cnt%3==2)
	{
		for($cnt = 0;$cnt < count($tmp_arry);$cnt++)
		{
			$set_id = intval(substr($tmp_arry[$cnt],0,3));
			if($row["jnl_facility_id"] == $set_id)
			{
				$tmp_arry_div2[]=$tmp_arry[$cnt];
			}
		}
		
	}
	$div_cnt++;
}

sort($tmp_arry_div1);
sort($tmp_arry_div2);
sort($tmp_arry_div3);
?>

	<td valign="top" align="left">	
		<table border='0' cellspacing='0' cellpadding='1' class='list'>

<?php
displayDataSetFunc($tmp_arry_div3,$con,$fname,$comedix_dire);
?>
		</table>	
	</td>

	<td valign="top" align="left">　</td>


	<td valign="top" align="left">	
		<table border='0' cellspacing='0' cellpadding='1' class='list'>

<?php
displayDataSetFunc($tmp_arry_div2,$con,$fname,$comedix_dire);
?>
		</table>	
	</td>


	<td valign="top" align="left">　</td>

	<td valign="top" align="left">	
		<table border='0' cellspacing='0' cellpadding='1' class='list'>

<?php
displayDataSetFunc($tmp_arry_div1,$con,$fname,$comedix_dire);
?>

		</table>	
	</td>

</tr>
</table>

    <!--</div>-->
    <input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>

<?
function displayDataSetFunc($tmp_arry,$con,$fname,$comedix_dire)
{
	
	$lib_url = "http://" . $_SERVER['HTTP_HOST'] .$comedix_dire."jnl_apply/batch/071_facility/" ;
	
	$entcnt = 0;
	$sflg = 0;
	$rowspancnt=0;
	
	
	for($cnt = 0;$cnt < count($tmp_arry);$cnt++)
	{
		$file_path ="";
		$file_path = $lib_url.$tmp_arry[$cnt];
		$file_path = mb_convert_encoding($file_path,mb_internal_encoding(),'utf-8');
		
		$now_name = substr($tmp_arry[$cnt],0,3);
		
		if($now_name == $back_name)
		{
			;
		}
		else
		{
			$back_name = substr($tmp_arry[$cnt],0,3);
			$cond_id = intval(substr($tmp_arry[$cnt],0,3));
			
			$sql = "select jnl_facility_name from jnl_facility_master";
			$cond = "where jnl_facility_id = '$cond_id'";
			$fac_name = select_from_table($con, $sql, $cond, $fname);
			if ($fac_name == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$disp_facility = pg_fetch_result($fac_name, 0, "jnl_facility_name");	
			
			if($cnt > 0)
			{
				echo("</table");
			}
			echo("<table border='0' cellspacing='0' cellpadding='1' class='list'><tr><td bgcolor='E9C8FF'><b>$disp_facility</b></td></tr>");
		}
		
		echo("<tr>");
		
		echo("<td>");	
		echo("<h3><a href=\"$file_path\">".mb_convert_encoding($tmp_arry[$cnt],mb_internal_encoding(),'utf-8')."</a></h3>");	
		echo("</td>");	
		
		echo("</tr>");
		
	}
	if($entcnt < 1)
	{
		echo("</tr>");
	}
	
}



?>

