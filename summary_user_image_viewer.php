<?
ob_start();// 余計な標準出力を抑止
$fname = $_SERVER["PHP_SELF"];
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("summary_common.ini");
require_once("sot_util.php");

define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);

$initerror = true;
$errmsg = "";
for (;;){
	$session = qualify_session($session,$fname);//セッションのチェック
	if(!$session) break;
	$summary_check = check_authority($session,57,$fname);//権限チェック
	if(!$summary_check) break;
	$con = connect2db($fname);//DB接続(ほんとは不要)
	if(!$con) break;
	$initerror = false;
	break;
}
if ($initerror) $errmsg = "この画面の状態は無効となりました。再度開きなおしてください。";
ob_end_clean();

$basedir = "summary/user_image";
if (!$errmsg){
	if (!is_dir("summary")) mkdir("summary", 0755);
	if (!is_dir($basedir)) mkdir($basedir, 0755);
	if (!is_dir($basedir)) $errmsg = "画像格納用のディレクトリが作成できません。(<?=$basedir?>)";
}

//==============================================================================
// 画像削除
//==============================================================================
if (!$errmsg && trim(@$_REQUEST["delete_file_name"])!=""){
	if (is_file($basedir."/".trim(@$_REQUEST["delete_file_name"]))) unlink($basedir."/".trim(@$_REQUEST["delete_file_name"]));
  header("Location: summary_user_image_viewer.php?session=".$session);
  die;
}


//==============================================================================
// 画像登録
//==============================================================================
if (!$errmsg && @$_FILES["user_file"]){
	for (;;){
		$fname = $_FILES["user_file"]["name"];
		if ($fname=="") break;
    $dotpos = strrpos($fname, ".");
    if ((int)$dotpos < 1) break;
		$ext = strtolower(substr($fname, $dotpos+1, 3));
    if ($ext != "gif" && $ext != "jpg" && $ext != "png") {
			$errmsg = "ファイルの拡張子が不正です。";
			break;
    }
    switch ($_FILES["user_file"]["error"]) {
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
    	$errmsg = 'ファイルサイズが大きすぎます。';
    	break 2;
    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
    	$errmsg = 'アップロードに失敗しました。再度実行してください。';
    	break 2;
    }

		if (is_file($basedir."/".$fname)) unlink($basedir."/".$fname);
    $ret = copy($_FILES["user_file"]["tmp_name"], $basedir."/".$fname);
    if ($ret == false) {
    	$errmsg = 'ファイルの保存に失敗しました。再度実行してください。';
    	break;
    }
    break;
  }
  header("Location: summary_user_image_viewer.php?session=".$session);
  die;
}

//==============================================================================
// 画像一覧取得
//==============================================================================
$filelist = array();
if (!$errmsg){
	if($h = opendir($basedir)) {
		while($fname = readdir($h)) {
			if($fname == "." || $fname == "..") continue;
			if(!is_file($basedir."/".$fname)) continue;
			$dotpos = strrpos($fname, ".");
			if ((int)$dotpos < 1) continue;
			$ext = strtolower(substr($fname, $dotpos+1, 3));
			if ($ext != "gif" && $ext != "jpg" && $ext != "png") continue;
			$filelist[] = array("path"=>$basedir."/".$fname, "name"=>$fname, "info"=>getimagesize($basedir."/".$fname));
		}
		closedir($h);
	}
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css" />
<title>CoMedix 関連画像</title>
<script type="text/javascript">
<? if ($errmsg){ ?>alert("<?=$errmsg?>");<? } ?>

	function deleteImage(fname){
		if (!confirm("画像ファイル\n「"+fname+"」\nを削除します。よろしいですか？")) return;
		document.del.delete_file_name.value = fname;
		document.del.submit();
	}
</script>
</head>
<body>
<div style="padding:4px">



<!-- ヘッダエリア -->
<?= summary_common_show_dialog_header("関連画像"); ?>


<!-- 画像アップロードエリア -->
<form name="search" action="summary_user_image_viewer.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="session" value="<?= $session ?>">
	<div class="dialog_searchfield" style="padding:3px;">
		追加ファイル<input type="file" size="64" name="user_file"/>
		<input type="submit" value="アップロード"/>
	</div>
</form>

<!-- 画像削除フォーム -->
<form name="del" action="summary_user_image_viewer.php" method="post">
	<input type="hidden" name="session" value="<?= $session ?>">
	<input type="hidden" name="delete_file_name" id="delete_file_name" value=""/>
</form>




<!-- 画像一覧エリア -->
<table class="list_table" style="margin-top:8px;"><tr><td>
<div style="height:400px; overflow-x:hidden; overflow-y:scroll; background-color:#f4f4f4;">
	<table style="width:100%;">
	<? foreach($filelist as $idx => $row){ ?>
	<?  list($w, $h, $type, $attr) = $row["info"]; ?>
	<?  $tag_w = ($w > 150 && $w >= $h) ? 'width="150"'  : ""; ?>
	<?  $tag_h = ($h > 150 && $h >  $w) ? 'height="150"' : ""; ?>
		<tr style="border-bottom:3px solid #f4f4f4;">
			<td style="width:150px; padding:3px; background-color:#eee; text-align:center"><img src="<?=$row["path"]?>" <?=@$tag_w?> <?=@$tag_h?> /></td>
			<td style="padding:3px; vertical-align:top">
				<div><a href="<?=$row["path"]?>" target="_blank"><?=$row["path"]?></a></div>


				<div style="color:#888"><?=$w?> x <?=$h?> ピクセル</div>
			</td>
			<td style="text-align:right; padding:3px; vertical-align:top">
				<input type="button" onclick="deleteImage('<?=$row["name"]?>');" value="削除" />
			</td>
		</tr>
	<? } ?>
	</table>
</div>
</td></tr></table>
<div style="color:#995; margin-top:4px">※アップロード可能な拡張子：「.gif」「.jpg」「.png」</div>
<div style="color:#995">※同名ファイルが存在した場合、上書き保存されます。</div>
<div style="color:#995">※全角文字を含むファイル名は、正しく登録/表示されない場合があります。</div>
</div>
</body>
</html>
