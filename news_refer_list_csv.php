<?
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("news_common.ini");
//ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 処理モードの設定
define("LIST_REFERRED", 1);  // 確認済みリストを表示
define("LIST_UNREFERRED", 2);  // 未確認リストを表示
define("LIST_ALL", 4);  // 対象者リストを表示
define("LIST_UNREAD", 5);  // 未読リストを表示
define("LIST_READ", 6);  // 既読リストを表示
define("LIST_READ_UNREFERRED", 7);  // 既読・未確認リストを表示

$csv_header = "職員ID,職員氏名,所属,役職,職種";
if ($mode == LIST_UNREFERRED) {
	$csv_header .= ",督促メール送信日時";
}
if ($mode == LIST_ALL) {
	$csv_header .= ",確認";
}
if ($mode == LIST_ALL || $mode == LIST_REFERRED) {
	$csv_header .= ",確認日時,コメント";
}
$csv_header = mb_convert_encoding($csv_header, "Shift_JIS", "EUC-JP");

// データベースに接続
$con = connect2db($fname);

$csv_body = get_csv_body($con, $news_id, $mode, $fname, $sort_id);

// データベース接続を閉じる
pg_close($con);
ob_end_clean();

$file_name = "news.csv";
$csv = $csv_header . "\r\n" . $csv_body;
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);

function get_csv_body($con, $news_id, $mode, $fname, $sort_id) {
	$buf = "";

	// お知らせ情報の取得
	$sql = "select news.news_add_category, news.job_id as job_id1, news.st_id_cnt as st_id_cnt1, newscate.standard_cate_id, newscate.job_id as job_id2, newscate.st_id_cnt as st_id_cnt2, newscate.newscate_id, newscate.newscate_name, news.record_flg, news.read_control from news inner join newscate on news.news_add_category = newscate.newscate_id";
	$cond = "where news_id = $news_id";
	$sel_news = select_from_table($con, $sql, $cond, $fname);
	if ($sel_news == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$news_category = pg_fetch_result($sel_news, 0, "news_add_category");
	$standard_cate_id = pg_fetch_result($sel_news, 0, "standard_cate_id");
	$newscate_id = pg_fetch_result($sel_news, 0, "newscate_id");
	$record_flg = pg_fetch_result($sel_news, 0, "record_flg");
	$read_control = pg_fetch_result($sel_news, 0, "read_control");

	// 標準カテゴリの場合
	if ($news_category <= "3") {
		$st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt1");
		$job_id = pg_fetch_result($sel_news, 0, "job_id1");

	// 追加カテゴリの場合
	} else {
		$st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt2");
		$job_id = pg_fetch_result($sel_news, 0, "job_id2");
	}

	// 役職指定数がある場合条件追加
	$st_cond = "";
	if ($st_id_cnt > 0) {
		if ($news_category <= "3") {
			$newsst_info = get_newsst_info($con, $fname, $news_id);
		} else {
			$newsst_info = get_newscatest_info($con, $fname, $newscate_id);
		}
		$st_ids = join(",", array_keys($newsst_info));
		$st_cond .= " and (empmst.emp_st in ($st_ids) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_st in ($st_ids)))";
	}

    // ソート順
    $cond_sort = "";
    switch ($sort_id) {
    case "1":
        $cond_sort = " order by emp_personal_id ";
        break;
    case "2":
        $cond_sort = " order by emp_personal_id desc ";
        break;
    case "3":
        $cond_sort = " order by emp_kn_lt_nm, emp_kn_ft_nm ";
        break;
    case "4":
        $cond_sort = " order by emp_kn_lt_nm desc, emp_kn_ft_nm desc ";
        break;
    case "5":
        $cond_sort = " order by update_time, emp_kn_lt_nm, emp_kn_ft_nm ";
        break;
    case "6":
        $cond_sort = " order by update_time desc, emp_kn_lt_nm, emp_kn_ft_nm ";
        break;
    case "7":
       $cond_sort = " order by classmst.order_no, atrbmst.order_no, deptmst.order_no, classroom.order_no, emp_kn_lt_nm, emp_kn_ft_nm ";
       break;
    case "8":
       $cond_sort = " order by classmst.order_no desc, atrbmst.order_no desc, deptmst.order_no desc, classroom.order_no desc, emp_kn_lt_nm, emp_kn_ft_nm ";
       break;
    case "9":
        $cond_sort = " order by stmst.order_no, emp_kn_lt_nm, emp_kn_ft_nm ";
        break;
    case "10":
        $cond_sort = " order by stmst.order_no desc, emp_kn_lt_nm, emp_kn_ft_nm ";
        break;
    case "11":
        $cond_sort = " order by jobmst.order_no, emp_kn_lt_nm, emp_kn_ft_nm ";
        break;
    case "12":
        $cond_sort = " order by jobmst.order_no desc , emp_kn_lt_nm, emp_kn_ft_nm ";
        break;
    default:
        $cond_sort = " order by emp_kn_lt_nm, emp_kn_ft_nm ";
    }

	// 回覧板
	if ($news_category == 10000) {
		switch ($mode) {

		case LIST_ALL:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, newscomment.update_time, newscomment.comment, news.record_flg, news.read_control, newsref.read_flg, newsref.ref_time from newscomment inner join empmst on newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st left join newsref on newsref.news_id = $news_id and newsref.emp_id = empmst.emp_id left join news on news.news_id = $news_id";
			$cond = "where newscomment.news_id = $news_id";
			break;

		case LIST_UNREFERRED:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm from newscomment inner join empmst on newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st";
			$cond = "where newscomment.news_id = $news_id and not exists (select * from newsref where newsref.news_id = $news_id and newsref.emp_id = newscomment.emp_id and newsref.ref_time is not null)";
			break;

		case LIST_REFERRED:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, jobmst.job_nm, stmst.st_nm, newscomment.update_time, newscomment.comment, news.record_flg from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st left join news on news.news_id = $news_id";
			$cond = "where newsref.news_id = $news_id and newsref.ref_time is not null";
			break;

		case LIST_UNREAD:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm from newscomment inner join empmst on newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st";
			$cond = "where newscomment.news_id = $news_id and not exists (select * from newsref where newsref.news_id = $news_id and newsref.emp_id = newscomment.emp_id and newsref.read_flg='t')";
			break;

		case LIST_READ:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, newscomment.update_time, newscomment.comment from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st";
			$cond = "where newsref.news_id = $news_id and newsref.read_flg='t'";
			break;

		case LIST_READ_UNREFERRED:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, newscomment.update_time, newscomment.comment from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st";
			$cond = "where newsref.news_id = $news_id and newsref.read_flg='t' and newsref.ref_time is null";
			break;
		}

	// お知らせ
	} else {
		switch ($mode) {

		case LIST_ALL:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, (case when newscomment.update_time is null then newsref.ref_time else newscomment.update_time end) as update_time, newscomment.comment, news.record_flg, news.read_control, newsref.read_flg, newsref.ref_time from empmst left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id left join newsref on newsref.news_id = $news_id and newsref.emp_id = empmst.emp_id left join news on news.news_id = $news_id";
			$cond = get_base_sql_for_all($news_id, $news_category, $standard_cate_id, $newscate_id, $job_id);
			break;

		case LIST_UNREFERRED:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm from empmst left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st";
			$cond = get_base_sql_for_all($news_id, $news_category, $standard_cate_id, $newscate_id, $job_id);
			$cond .= ($cond == "") ? "where" : " and";
			$cond .= " not exists (select * from newsref where newsref.news_id = $news_id and newsref.emp_id = empmst.emp_id and newsref.ref_time is not null)";
			break;

		case LIST_REFERRED:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, (case when newscomment.update_time is null then newsref.ref_time else newscomment.update_time end) as update_time, newscomment.comment, news.record_flg from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id left join news on news.news_id = $news_id";
			$cond = "where newsref.news_id = $news_id and newsref.ref_time is not null";
			break;

		case LIST_UNREAD:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, 0 as update_time from empmst left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st";
			$cond = get_base_sql_for_all($news_id, $news_category, $standard_cate_id, $newscate_id, $job_id);
			$cond .= ($cond == "") ? "where" : " and";
			$cond .= " not exists (select * from newsref where newsref.news_id = $news_id and newsref.emp_id = empmst.emp_id and newsref.read_flg='t')";
			break;

		case LIST_READ:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, (case when newscomment.update_time is null then newsref.ref_time else newscomment.update_time end) as update_time, newscomment.comment from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id";
			$cond = "where newsref.news_id = $news_id and newsref.read_flg='t'";
			break;

		case LIST_READ_UNREFERRED:
			$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, jobmst.job_nm, stmst.st_nm, (case when newscomment.update_time is null then newsref.ref_time else newscomment.update_time end) as update_time, newscomment.comment from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join jobmst on jobmst.job_id = empmst.emp_job left join stmst on stmst.st_id = empmst.emp_st left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id";
			$cond = "where newsref.news_id = $news_id and newsref.read_flg='t' and newsref.ref_time is null";
			break;
		}

		$cond .= ($cond == "") ? "where" : " and";
		$cond .= " exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id  and (authmst.emp_newsuser_flg = 't' or authmst.emp_news_flg = 't'))";

		// 役職条件追加
		if ($st_cond != "") {
			$cond .= $st_cond;
		}
	}

	$cond .= " $cond_sort";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel)) {
		$buf .= format_csv_value($row["emp_personal_id"]);
		$buf .= "," . format_csv_value("{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}");

		$tmp_position = "{$row["class_nm"]}＞{$row["atrb_nm"]}＞{$row["dept_nm"]}";
		if ($row["room_nm"] != "") {
			$tmp_position .= "＞{$row["room_nm"]}";
		}
		$buf .= "," . format_csv_value($tmp_position);

		$buf .= "," . format_csv_value($row["st_nm"]);

		$buf .= "," . format_csv_value($row["job_nm"]);

		if ($mode == LIST_UNREFERRED) {

			// 督促メール送信日時
			$sql = "select send_time from newsmail";
			$cond = "where news_id = $news_id and emp_id = '{$row["emp_id"]}' order by send_time";
			$sel_mail = select_from_table($con, $sql, $cond, $fname);
			if ($sel_mail == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$mail_send_time = "";
			while ($row = pg_fetch_array($sel_mail)) {
				if ($mail_send_time != "") {
					$mail_send_time .= ",";
				}
				$mail_send_time .= preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5", $row["send_time"]);
			}

			$buf .= "," . format_csv_value($mail_send_time);
		}

		if ($mode == LIST_ALL) {
			if ($row["record_flg"] == "f") {

				if ($row["read_control"] == "t" && $row["read_flg"] != "t") {
					$tmp_ref_status = "未読";
				} else if ($row["read_flg"] == "t") {
					$tmp_ref_status = "既読";
				} else {

					$tmp_ref_status = "";

				}

			} else {
				if ($row["ref_time"] == "") {

					if ($row["read_control"] == "t" && $row["read_flg"] != "t") {
						$tmp_ref_status = "未読";
					} else if ($row["read_flg"] == "t") {

						$tmp_ref_status = "未確認";

					} else {
						$tmp_ref_status = "";
					}

				} else {
					$tmp_ref_status = "確認済み";
				}
			}

			$buf .= "," . format_csv_value($tmp_ref_status);
		}

		if ($row["record_flg"] == "t" and ($mode == LIST_ALL || $mode == LIST_REFERRED)) {
			$tmp_update_time = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5", $row["update_time"]);
			$buf .= "," . format_csv_value($tmp_update_time);
			$buf .= "," . format_csv_value($row["comment"]);
		}

		$buf .= "\r\n";
	}

	return $buf;
}

function get_base_sql_for_all($news_id, $news_category, $standard_cate_id, $newscate_id, $job_id) {

	// 標準カテゴリの場合
	if ($news_category <= "3") {
		$tmp_news_category = $news_category;

	// 追加カテゴリの場合
	} else {
		$tmp_news_category = $standard_cate_id;
	}

	switch ($tmp_news_category) {

	case "2":  // 部署

		// 標準カテゴリの場合
		if ($news_category <= "3") {
			$cond = "where exists (select * from newsdept where newsdept.news_id = $news_id and ((newsdept.class_id = empmst.emp_class) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = newsdept.class_id))) and ((newsdept.atrb_id = empmst.emp_attribute) or (newsdept.atrb_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = newsdept.atrb_id))) and ((newsdept.dept_id = empmst.emp_dept) or (newsdept.dept_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = newsdept.dept_id))))";

		// 追加カテゴリの場合
		} else {
			$cond = "where exists (select * from newscatedept where newscatedept.newscate_id = $newscate_id and ((newscatedept.class_id = empmst.emp_class) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = newscatedept.class_id))) and ((newscatedept.atrb_id = empmst.emp_attribute) or (newscatedept.atrb_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = newscatedept.atrb_id))) and ((newscatedept.dept_id = empmst.emp_dept) or (newscatedept.dept_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = newscatedept.dept_id))))";
		}
		break;

	case "3":  // 職種

		// 職種が1件の場合
		if ($job_id != "") {
			$cond = "where emp_job = $job_id";
		} else {

			// 標準カテゴリの場合
			if ($news_category <= "3") {
				$cond = "where exists (select * from newsjob where newsjob.news_id = $news_id and empmst.emp_job = newsjob.job_id)";
			} else {
				$cond = "where exists (select * from newscatejob where newscatejob.newscate_id = $newscate_id and empmst.emp_job = newscatejob.job_id)";
			}
		}
		break;
	}

	return $cond;
}

function format_csv_value($value) {
	$buf = str_replace("\r", "", $value);
	$buf = str_replace("\n", "", $buf);
	if (strpos($buf, ",") !== false)  {
		$buf = '"' . $buf . '"';
	}
	return mb_convert_encoding($buf, "Shift_JIS", "EUC-JP");
}
