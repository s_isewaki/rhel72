<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

$con = connect2db($fname);
pg_query($con, "begin transaction");

// 対象送信先情報を初期化
$sql = "delete from adbkgrpmem";
$cond = "where adbkgrp_id = $group_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 対象送信先情報を登録
if ($addresses_ids != "") {
	$order_no = 1;
	$addresses = explode(",", $addresses_ids);
	foreach ($addresses as $tmp_address) {
		list($tmp_id, $tmp_type) = explode(":", $tmp_address);

		if ($tmp_type == "1" || $tmp_type == "2") {
			$tmp_emp_id = $tmp_id;
			$tmp_address_id = null;
		} else {
			$tmp_emp_id = null;
			$tmp_address_id = $tmp_id;
		}

		$sql = "insert into adbkgrpmem (adbkgrp_id, order_no, type, emp_id, address_id) values (";
		$content = array($group_id, $order_no, $tmp_type, $tmp_emp_id, $tmp_address_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
		$order_no++;
	}
}

pg_query($con, "commit");
pg_close($con);

// グループ作成画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'address_group.php?session=$session&group_id=$group_id'</script>");
