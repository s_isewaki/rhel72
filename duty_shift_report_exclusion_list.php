<?
///*****************************************************************************
// 勤務シフト作成 | 勤務時間除外リスト「ＥＸＣＥＬ」
///*****************************************************************************

ini_set("max_execution_time", 0);
ob_start();
require_once("about_comedix.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_report_common_class.php");
require_once("duty_shift_menu_excel_5_workshop.php");

//debug

//ini_set( "log_errors", "On" );
//ini_set( "error_log", "/var/www/html/comedix/phpApl.log" );

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_report = new duty_shift_report_common_class($con, $fname);
///-----------------------------------------------------------------------------
// Excelオブジェクト生成、列名配列
///-----------------------------------------------------------------------------
$excelObj = new ExcelWorkShop();

///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];
///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
if ($duty_mm == "") { $duty_mm = $now_mm; }
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
///-----------------------------------------------------------------------------
//終了年月
///-----------------------------------------------------------------------------
if ($night_start_day == "") {
    $night_start_day = "1";
}
$end_yyyy = (int)$duty_yyyy;
$end_mm = (int)$duty_mm;
$end_dd = (int)$day_cnt;
if ($recomputation_flg == "2") {
    $wk_day = (int)$night_start_day + 27;
    $tmp_time = mktime(0, 0, 0, $duty_mm, $wk_day, $duty_yyyy);
    $tmp_date = date("Ymd", $tmp_time);
    $end_yyyy = (int)substr($tmp_date,0,4);
    $end_mm = (int)substr($tmp_date,4,2);
    $end_dd = (int)substr($tmp_date,6,2);
    $day_cnt = 28;
}
//日またがりの時間を加算するため、１日前を設定
if ((int)$night_start_day == 1) {
    $wk_start_yyyy = (int)$duty_yyyy;
    $wk_start_mm = (int)$duty_mm - 1;
    if ((int)$wk_start_mm < 1) {
        $wk_start_yyyy--;
        $wk_start_mm = 12;
    }
    $wk_start_dd = $obj->days_in_month($wk_start_yyyy, $wk_start_mm);
} else {
    $wk_start_yyyy = (int)$duty_yyyy;
    $wk_start_mm = (int)$duty_mm;
    $wk_start_dd = (int)$night_start_day - 1;
}
///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$start_date = sprintf("%04d%02d%02d",$wk_start_yyyy, $wk_start_mm, $wk_start_dd);
$end_date = sprintf("%04d%02d%02d",$end_yyyy, $end_mm, $end_dd);
$end_date = $obj->next_date($end_date);
$calendar_array = $obj->get_calendar_array($start_date, $end_date);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
for ($k=1; $k<=$day_cnt; $k++) {
    $tmp_date = mktime(0, 0, 0, $duty_mm, $k, $duty_yyyy);
    $week_array[$k]["name"] = $obj->get_weekday($tmp_date);
}
//前月末分
$week_array[0]["name"] = $obj->get_weekday($obj->to_timestamp($start_date));
///-----------------------------------------------------------------------------
//施設基準情報の取得
///-----------------------------------------------------------------------------
$standard_array = $obj->get_duty_shift_institution_history_array($standard_id, $duty_yyyy, $duty_mm);

//勤務パターン情報
$arr_atdptn_info = get_atdptn_info($con, $fname);

///-----------------------------------------------------------------------------
// 様式９用　看護職員情報取得
///-----------------------------------------------------------------------------
$group_id = 0 ; // 全病棟
$data = $obj_report->get_report_array(
        $standard_id,
        $duty_yyyy,
        $duty_mm,
        $night_start_day,
        $end_yyyy,
        $end_mm,
        $end_dd,
        $day_cnt,
        $standard_array[0],
        $group_id,
        $plan_result_flag);

$data_array = $data["data_array"];
$data_sub_array = $data["data_sub_array"];

$nurse_cnt = $data["nurse_cnt"];			//看護師数
$sub_nurse_cnt = $data["sub_nurse_cnt"];		//准看護師数
$assistance_nurse_cnt = $data["assistance_nurse_cnt"];	//看護補助者数

$nurse_sumtime = $data["nurse_sumtime"];			//看護師総勤務時間
$sub_nurse_sumtime = $data["sub_nurse_sumtime"];		//准看護師総勤務時間
$assistance_nurse_sumtime = $data["assistance_nurse_sumtime"];	//看護補助者総勤務時間

$wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);


///-----------------------------------------------------------------------------
/// START
///-----------------------------------------------------------------------------
$filename = "勤務時間除外リスト_{$standard_id}_{$wk_duty_yyyymm}.xls";
$excelObj->SetSheetName(mb_convert_encoding(mb_convert_encoding("勤務時間除外リスト", "sjis-win", "eucJP-win"), "UTF-8", "sjis-win"));

$arr_kind = array(
        "1" => "会議・研修・病棟外",
        "2" => "時間帯応援",
        "3" => "外出",
        "4" => "時間有休",
        "5" => "委員会",
        "6" => "遅刻",
        "7" => "早退",
        "99" => "その他");

$arr_title = array(
        "病棟",        //0
        "職員ID",        
        "氏名",        
        "職種",        
        "日付",        
        "曜日",        //5
        "開始時刻",        
        "終了時刻",        
        "除外区分",        
        "委員会WG名"   //9     
        );
//幅
$defWidth = 15.22;
$arr_width = array(
        24, //0
        10,
        16,
        16,
        11,
        5,  //5
        9,
        9,
        16,
        24
        );
$arr_align = array(
        "LEFT", //0
        "LEFT",
        "LEFT",
        "LEFT",
        "RIGHT",
        "CENTER",  //5
        "RIGHT",
        "RIGHT",
        "LEFT",
        "LEFT"
        );

$colmax = count($arr_title);
///-----------------------------------------------------------------------------
// Excel列名
///-----------------------------------------------------------------------------
$excelColumnName = array();
for ($i=0; $i<$colmax; $i++) {
    $excelColumnName[] = PHPExcel_Cell::stringFromColumnIndex($i);
}

showHeader($err_msg,
        $duty_yyyy,
        $duty_mm,
        $excelColumnName,
        $excelObj);


$excelTitleColPos = 0;

$row = 4;
for ($i=0; $i<$colmax; $i++) {
    $excelObj->SetArea($excelColumnName[$excelTitleColPos].$row);
    $excelObj->SetValueJP($arr_title[$i]);
    $excelObj->SetColDim($excelColumnName[$excelTitleColPos],$arr_width[$i]);
    $excelTitleColPos ++;
}
$row++;

//看護職員
out_exclusion_data($data_array, $row);
//看護補助者
out_exclusion_data($data_sub_array, $row);

//罫線
$excelObj->SetArea("A4:". $excelColumnName[$excelTitleColPos-1].($row-1) );
$excelObj->SetBorder( "THIN" , "000000" , 'all' );
$excelObj->SetPosV('CENTER');

///-----------------------------------------------------------------------------
// データEXCEL出力
///-----------------------------------------------------------------------------
ob_clean();

$ua = $_SERVER['HTTP_USER_AGENT'];
$filename = mb_convert_encoding($filename, 'UTF-8', mb_internal_encoding());

if(!preg_match("/safari/i",$ua)){
    header('Content-Type: application/octet-stream');
}
else {
    header('Content-Type: application/vnd.ms-excel');
}

if (preg_match("/MSIE/i", $ua)) {
    if (strlen(rawurlencode($filename)) > 21 * 3 * 3) {
        $filename = mb_convert_encoding($filename, "SJIS-win", "UTF-8");
        $filename = str_replace('#', '%23', $filename);
    }
    else {
        $filename = rawurlencode($filename);
    }
}
elseif (preg_match("/Trident/i", $ua)) {// IE11
    $filename = rawurlencode($filename);
}
elseif (preg_match("/chrome/i", $ua)) {// Google Chromeには『Safari』という字が含まれる
}
elseif (preg_match("/safari/i", $ua)) {// Safariでファイル名を指定しない場合
    $filename = "";
}
header('Content-Disposition: attachment; filename="'.$filename.'"');
header('Cache-Control: max-age=0');
ob_end_flush();
$excelObj->OutPut();

pg_close($con);

/**
 * 見出し部出力
 *
 * @param mixed $err_msg エラーメッセージ
 * @param mixed $group_name シフトグループ名
 * @param mixed $start_yyyy 開始年
 * @param mixed $start_mm 開始月
 * @param mixed $start_dd 開始日
 * @param mixed $end_yyyy 終了年
 * @param mixed $end_mm 終了月
 * @param mixed $end_dd 終了日
 * @param mixed $print_title 見出し
 * @param mixed $excelColumnName Excel列名
 * @param mixed $excelObj Excelオブジェクト
 * @return なし
 *
 */
function showHeader($err_msg,
    $yyyy,
    $mm,
    $excelColumnName,
    $excelObj){
    
    //デフォルト幅でA4縦のサイズになる件数
    $wk_colspan = 9;
    
    // 行の高さ
    $excelObj->SetRowDim('1' , 16.5);
    $excelObj->SetRowDim('2' , 13.5);
    $excelObj->SetRowDim('3' , 13.5);
    $excelObj->SetRowDim('4' , 16.5);
    if ($err_msg != "") {
        
        $excelObj->SetArea('A3');
        $excelObj->SetPosH('CENTER'); // ←中央→
        $excelObj->SetPosV('CENTER'); // ↑↓中央
        $excelObj->SetCharColor('FF0000'); // 赤文字
        $excelObj->SetValueJP($err_msg);
        
        return;
    }
    
    $wk_title = "様式９　病棟外勤務時間　除外リスト　　　";
    
    $excelObj->SetArea('A1'); // セル位置
    //タイトルの表示
    $excelObj->SetValueJP($wk_title);
    $excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),11);
    
    $excelObj->SetPosH('LEFT'); // ←左→
    $excelObj->SetPosV('CENTER'); // ↑↓中央
    
    $wk_yyyymm = $yyyy."年";
    $wk_yyyymm .= intval($mm)."月分";
    $excelObj->SetArea('E1'); // セル位置
    //期間の表示
    $excelObj->SetValueJP($wk_yyyymm);
    $excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),11);
    $excelObj->SetPosH('LEFT'); // ←左→
    $excelObj->SetPosV('CENTER'); // ↑↓中央
    
    return ;
    
}


/**
 * out_exclusion_data 除外リスト出力
 *
 * @param mixed $data_array 看護職員データor看護補助者データ 
 * @param mixed $row 行
 * @return mixed なし
 *
 */
function out_exclusion_data($data_array, &$row) {

    global $obj;
    global $excelObj;
    global $excelColumnName;
    global $calendar_array;
    global $week_array;
    global $arr_kind;
    global $arr_title;
    global $arr_width;
    global $arr_align;
    global $colmax;
    global $arr_atdptn_info;
    global $start_date;
    global $end_date;
    
    for ($i=0; $i<count($data_array); $i++) {
        
        //$k 1:前月末、2:1日、31:30日、32:31日 カレンダー0:前月末 1:1日
        for ($c_idx=0; $c_idx<count($calendar_array)-1; $c_idx++) {
            $k = $c_idx+1;
            //日付
            $wk_date = $calendar_array[$c_idx]["date"];
            $prev_date = $obj->last_date($wk_date);
            $next_date = $obj->next_date($wk_date);
            $wk_date_fmt = substr($wk_date, 0, 4)."/".intval(substr($wk_date, 4, 2),10)."/".intval(substr($wk_date, 6, 2),10);
            $wk_prev_date_fmt = substr($prev_date, 0, 4)."/".intval(substr($prev_date, 4, 2),10)."/".intval(substr($prev_date, 6, 2),10);
            $wk_next_date_fmt = substr($next_date, 0, 4)."/".intval(substr($next_date, 4, 2),10)."/".intval(substr($next_date, 6, 2),10);
            
            $arr_ex_data = array();
            $e_idx = 0;
            //1:タイムカード（会議）
            if ($data_array[$i]["meeting_start_time_$k"] != "" && $data_array[$i]["meeting_end_time_$k"] != "") {
                $arr_ex_data[$e_idx]["kind"] = "1";
                $arr_ex_data[$e_idx]["start_time"] = $data_array[$i]["meeting_start_time_$k"];
                $arr_ex_data[$e_idx]["end_time"] = $data_array[$i]["meeting_end_time_$k"];
                $e_idx++;
            }
            //2:時間帯（半日応援）
            if ($data_array[$i]["assist_start_time_$k"] != "" && $data_array[$i]["assist_end_time_$k"] != "") {
                $arr_ex_data[$e_idx]["kind"] = "2";
                $arr_ex_data[$e_idx]["start_time"] = $data_array[$i]["assist_start_time_$k"];
                $arr_ex_data[$e_idx]["end_time"] = $data_array[$i]["assist_end_time_$k"];
                $e_idx++;
            }
            //3:外出
            if ($data_array[$i]["out_time_$k"] != "" && $data_array[$i]["ret_time_$k"] != "") {
                $arr_ex_data[$e_idx]["kind"] = "3";
                $arr_ex_data[$e_idx]["start_time"] = $data_array[$i]["out_time_$k"];
                $arr_ex_data[$e_idx]["end_time"] = $data_array[$i]["ret_time_$k"];
                $e_idx++;
            }
            //4:時間有休
            if ($data_array[$i]["hol_hour_start_time_$k"] != "" && $data_array[$i]["hol_hour_end_time_$k"] != "") {
                $arr_ex_data[$e_idx]["kind"] = "4";
                $arr_ex_data[$e_idx]["start_time"] = $data_array[$i]["hol_hour_start_time_$k"];
                $arr_ex_data[$e_idx]["end_time"] = $data_array[$i]["hol_hour_end_time_$k"];
                $e_idx++;
            }
            //5:委員会WG
            if (count($data_array[$i]["project_data"] ) > 0) {
                $arr_project_data = $data_array[$i]["project_data"];
                $arr_project_time = $arr_project_data[$wk_date];
                //翌日委員会対応
                $next_dat_cnt = count($arr_project_data[$next_date]);
                if ($next_dat_cnt > 0) {
                    $prev_idx = $k - 1;
                    for ($p_idx=0; $p_idx<$next_dat_cnt; $p_idx++) {
                        //所定終了時刻前のデータを追加設定
                        $wk_o_end_time = $data_array[$i]["officehours_end_hhmm_$k"];
                        if ($wk_o_end_time == "") {
                            $wk_o_end_time = $data_array[$i]["end_time_$k"];
                        }
                        if ($arr_project_data[$next_date][$p_idx]["start_time"] <= $wk_o_end_time &&
                                $data_array[$i]["next_day_flag_$k"] == "1") {
                            $arr_project_data[$next_date][$p_idx]["next_day_flag"] = "1";
                            $arr_project_time[] = $arr_project_data[$next_date][$p_idx];
                        }
                    }
                    
                }
                $proj_cnt = count($arr_project_time);
                for ($p_idx = 0; $p_idx < $proj_cnt; $p_idx++) {
                    //所定時間内か確認
                    //開始前、終了後以外、重なりを確認
                    $wk_o_start_time = $data_array[$i]["officehours_start_hhmm_$k"];
                    $wk_o_end_time = $data_array[$i]["officehours_end_hhmm_$k"];
                    if ($wk_o_start_time == "") {
                        $wk_o_start_time = $data_array[$i]["start_time_$k"];
                    }
                    if ($wk_o_end_time == "") {
                        $wk_o_end_time = $data_array[$i]["end_time_$k"];
                    }
                    if ($wk_o_start_time != "" && $wk_o_end_time != "") { //設定がある場合に確認
                        //日またがりは終了側を2400とする。委員会は2400で制限。
                        if ($wk_o_start_time > $wk_o_end_time) {
                            $wk_o_end_time = "2400";
                        }
                        //所定より前、後の場合は出力しない 翌日データ以外
                        if ($arr_project_time[$p_idx]["next_day_flag"] != 1) {
                            if ($arr_project_time[$p_idx]["end_time"] <= $wk_o_start_time ||
                                    $arr_project_time[$p_idx]["start_time"] >= $wk_o_end_time) {
                                continue;
                            }
                        }
                    }
                    //所定設定なし、出勤退勤もなしの場合
                    else {
                        continue;
                    }
                    $arr_ex_data[$e_idx]["kind"] = "5";
                    $arr_ex_data[$e_idx]["start_time"] = $arr_project_time[$p_idx]["start_time"];
                    $arr_ex_data[$e_idx]["end_time"] = $arr_project_time[$p_idx]["end_time"];
                    $arr_ex_data[$e_idx]["pjt_public_flag"] = $arr_project_time[$p_idx]["pjt_public_flag"];
                    $arr_ex_data[$e_idx]["next_day_flag"] = $arr_project_time[$p_idx]["next_day_flag"];
                    //非公開対応する場合
                    //if ($arr_project_time[$p_idx]["pjt_public_flag"] == "t") {
                        $arr_ex_data[$e_idx]["pjt_name"] = $arr_project_time[$p_idx]["pjt_name"];
                    //}
                    //else {
                    //    $arr_ex_data[$e_idx]["pjt_name"] = "";
                    //    $arr_ex_data[$e_idx]["kind"] = "99"; //その他に設定
                    //}
                    $e_idx++;
                }
            }
            //6:遅刻 7:早退 
            if ($data_array[$i]["atdptn_ptn_id_$k"] != "10" &&
                    $data_array[$i]["officehours_start_hhmm_$k"] != "" &&
                    $data_array[$i]["officehours_end_hhmm_$k"] != "" &&
                    $data_array[$i]["start_time_$k"] != "" &&
                    $data_array[$i]["end_time_$k"] != ""
                ) {
                //日またがり対応
                $wk_start_date = ($data_array[$i]["previous_day_flag_$k"] != "1") ? $wk_date : $prev_date;
                $wk_start_date_time = $wk_start_date.$data_array[$i]["start_time_$k"];
                $wk_group_id = $data_array[$i]["pattern_id_$k"];
                $wk_atdptn_ptn_id = $data_array[$i]["atdptn_ptn_id_$k"];
                $wk_previous_day_flag = $arr_atdptn_info[$wk_group_id][$wk_atdptn_ptn_id]["previous_day_flag"];
                $wk_over_24hour_flag = $arr_atdptn_info[$wk_group_id][$wk_atdptn_ptn_id]["over_24hour_flag_flag"];
                $wk_o_start_date = ($wk_previous_day_flag != "1")  ? $wk_date : $prev_date;
                $office_start_date_time = $wk_o_start_date.$data_array[$i]["officehours_start_hhmm_$k"];
                //遅刻、出勤時刻＞所定開始、前月末を除く
                if ($wk_start_date_time > $office_start_date_time && $wk_start_date > $start_date) {
                    $arr_ex_data[$e_idx]["kind"] = "6";
                    $arr_ex_data[$e_idx]["start_time"] = $data_array[$i]["officehours_start_hhmm_$k"];
                    $arr_ex_data[$e_idx]["end_time"] = $data_array[$i]["start_time_$k"];
                    $arr_ex_data[$e_idx]["previous_day_flag"] = $wk_previous_day_flag;
                    $e_idx++;
                }
                $wk_end_date = ($data_array[$i]["next_day_flag_$k"] != "1") ? $wk_date : $next_date;
                $wk_end_date_time = $wk_end_date.$data_array[$i]["end_time_$k"];
                //所定終了日時、前日フラグ、24時間超で判断
                $next_day_flag = "";

                if ($wk_previous_day_flag == 1) {
                    if (($data_array[$i]["officehours_start_hhmm_$k"] > $data_array[$i]["officehours_end_hhmm_$k"]) || $wk_over_24hour_flag == 1) {
                        $wk_o_end_date = $wk_date;
                    }
                    else {
                        $wk_o_end_date = $prev_date;
                    }
                }
                else {
                    if (($data_array[$i]["officehours_start_hhmm_$k"] > $data_array[$i]["officehours_end_hhmm_$k"]) || $wk_over_24hour_flag == 1) {
                        $wk_o_end_date = $next_date;
                        $next_day_flag = "1";
                    }
                    else {
                        $wk_o_end_date = $wk_date;
                    }
                }
                $office_end_date_time = $wk_o_end_date.$data_array[$i]["officehours_end_hhmm_$k"];
                //早退、退勤時刻＜所定終了
                if ($wk_end_date_time < $office_end_date_time) {
                    $arr_ex_data[$e_idx]["kind"] = "7";
                    $arr_ex_data[$e_idx]["start_time"] = $data_array[$i]["end_time_$k"];
                    $arr_ex_data[$e_idx]["end_time"] = $data_array[$i]["officehours_end_hhmm_$k"];
                    $arr_ex_data[$e_idx]["next_day_flag"] = $next_day_flag;
                    $e_idx++;
                }
            }
            if ($e_idx > 0) {
                //開始時刻順
                $time_key = array();
                foreach( $arr_ex_data as $key => $wk_ex_data ) {
                    if ($wk_ex_data["next_day_flag"] == "1") {
                        $time_key[] = "1".$wk_ex_data["start_time"];
                    }
                    else {
                        $time_key[] = "0".$wk_ex_data["start_time"];
                    }
                }
                array_multisort( $time_key, SORT_ASC, $arr_ex_data );
                
                $d_max = $e_idx;
                for ($d_idx = 0; $d_idx<$d_max; $d_idx++) {
                    //前月末を除く
                    if ($wk_date == $start_date && $arr_ex_data[$d_idx]["next_day_flag"] != "1") {
                        continue;
                    }
                    //翌月初めを除く
                    if ($next_date == $end_date && $arr_ex_data[$d_idx]["next_day_flag"] == "1") {
                        continue;
                    }
                    $arr_dat_str = array();
                    $arr_dat_str[] = $data_array[$i]["main_group_name"];
                    $arr_dat_str[] = $data_array[$i]["emp_personal_id"];
                    $arr_dat_str[] = $data_array[$i]["staff_name"];
                    $arr_dat_str[] = $data_array[$i]["job_name"];
                    if ($arr_ex_data[$d_idx]["next_day_flag"] == "1") {
                        $wk_d_str = $wk_next_date_fmt;
                        $wk_w_str = $week_array[$c_idx+1]["name"];
                    }
                    elseif ($arr_ex_data[$d_idx]["previous_day_flag"] == "1") {
                        $wk_d_str = $wk_prev_date_fmt;
                        $wk_w_str = $week_array[$c_idx-1]["name"];
                    }
                    else {
                        $wk_d_str = $wk_date_fmt;
                        $wk_w_str = $week_array[$c_idx]["name"];
                    }
                    $arr_dat_str[] = $wk_d_str;
                    $arr_dat_str[] = $wk_w_str;
                    $arr_dat_str[] = $arr_ex_data[$d_idx]["start_time"];
                    $arr_dat_str[] = $arr_ex_data[$d_idx]["end_time"];
                    $arr_dat_str[] = $arr_kind[ $arr_ex_data[$d_idx]["kind"] ];
                    $arr_dat_str[] = ($arr_ex_data[$d_idx]["kind"] == "5") ? $arr_ex_data[$d_idx]["pjt_name"] :"";
                    
                    $excelTitleColPos = 0;
                    for ($j=0; $j<$colmax; $j++) {
                        $excelObj->SetArea($excelColumnName[$excelTitleColPos].$row);
                        
                        if ($j == 6 || $j == 7) { //時刻 h:mm形式
                            $wk_str = intval(substr($arr_dat_str[$j], 0, 2), 10).":".substr($arr_dat_str[$j], 2, 2);
                        }
                        else {
                            $wk_str = $arr_dat_str[$j];
                        }
                        //横位置
                        $wk_align = $arr_align[$j];
                        
                        $excelObj->SetValueJP($wk_str);
                        $excelObj->SetColDim($excelColumnName[$excelTitleColPos], $arr_width[$j]);
                        $excelObj->SetPosH($wk_align);
                        $excelTitleColPos ++;
                    }
                    $row++;
                }
                
            }
            
        }
        
    }
    
}


/**
 * get_atdptn_info 勤務パターン情報、前日フラグ、24時間超
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @return mixed データ配列、info[グループID][勤務パターンID]["previous_day_flag" or "over_24hour_flag"]
 *
 */
function get_atdptn_info($con, $fname) {

    $sql = "select group_id, atdptn_id, previous_day_flag, over_24hour_flag from atdptn ";
    $cond = " where previous_day_flag = 1 or over_24hour_flag = 1 ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    
    $arr_info = array();
    while ($row = pg_fetch_array($sel)) {
        
        if ($row["previous_day_flag"] == "1") {
            $arr_info[ $row["group_id"] ][ $row["atdptn_id"] ]["previous_day_flag"] =	$row["previous_day_flag"];
        }
        if ($row["over_24hour_flag"] == "1") {
            $arr_info[ $row["group_id"] ][ $row["atdptn_id"] ]["over_24hour_flag"] =	$row["over_24hour_flag"];
        }        
    }
    return $arr_info;
}
// program end.
