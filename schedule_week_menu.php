<?php
$fname = $_SERVER["PHP_SELF"];
require_once("Cmx.php");
require_once("aclg_set.php");

require_once("about_comedix.php");
require_once("time_check.ini");
require_once("project_check.ini");
require_once("show_date_navigation_week.ini");
require_once("holiday.php");
require_once("show_schedule_common.ini");
require_once("show_schedule_chart.ini");
require_once("label_by_profile_type.ini");
require_once("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
$this_week = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
if ($date == "") {
    $date = $this_week;
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「前週」「翌週」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$last_week = get_last_week($date);
$next_week = get_next_week($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// アクセスログ
$optsql = "select schedule1_default from option";
$optcond = "where emp_id = '{$emp_id}'";
$optsel = select_from_table($con, $optsql, $optcond, $fname);
if ($optsel == 0) {
    pg_close($con);
    js_error_exit();
}
$schedule1_default = pg_fetch_result($optsel, 0, "schedule1_default");

if ($schedule1_default === '2') {
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
}
// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
    time_update($con, $emp_id, $timegd, $fname);
}
else if ($timegd == "") {
    $timegd = time_check($con, $emp_id, $fname);
}

// 委員会・WGチェックボックスの処理
if ($pjtgd == "on" || $pjtgd == "off") {
    project_update($con, $emp_id, $pjtgd, $fname);
}
else if ($pjtgd == "") {
    $pjtgd = project_check($con, $emp_id, $fname);
}

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $emp_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);

// 時間指定のないスケジュールの情報を配列で取得
$timeless_schedules = get_timeless_schedules($con, $emp_id, $pjtgd, $timegd, $dates, $fname);

// 時間指定のあるスケジュール情報を配列で取得
$normal_schedules = get_normal_schedules($con, $emp_id, $pjtgd, $timegd, $dates, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 週</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function initPage() {
    var divs = document.getElementsByTagName('div');
    for (var i = 0, j = divs.length; i < j; i++) {
        switch (divs[i].className) {
        case 'personal':
            var cell = new YAHOO.util.DD(divs[i].id, 'cells');

            cell.startPos = YAHOO.util.Dom.getXY(divs[i]);
            cell.bgColor = divs[i].style.backgroundColor;

            cell.startDrag = function (x, y) {
                var el = this.getDragEl();
                el.style.backgroundColor = '#fefe83';
                el.style.zIndex = 999;
            }

            cell.onDragDrop = function (e, id) {
                this.fixed = true;

                var schd_id = this.getDragEl().id.split('_')[4];
                var cell_info = id.split('_');
                var url = 'schedule_move.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>&schd_id='.concat(schd_id).concat('&ymd=').concat(cell_info[1]).concat('&start_time=').concat(cell_info[2]);
                var callback = {
                    'success' : function (r) {
                        if (r.responseText == 'done') {
                            location.href = 'schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>';
                        } else {
                            alert(r.responseText);
                            r.argument.fixed = false;
                            r.argument.endDrag();
                        }
                    },
                    'failure' : function (r) {
                        r.argument.fixed = false;
                        r.argument.endDrag();
                    },
                    'argument' : this
                }
                YAHOO.util.Connect.asyncRequest('GET', url, callback);
            }

            cell.endDrag = function (e) {
                if (this.fixed) {
                    return;
                }
                var el = this.getDragEl();
                YAHOO.util.Dom.setXY(el, this.startPos);
                el.style.backgroundColor = this.bgColor;
                el.style.zIndex = 0;
            }
            break;

        case 'project':
        case 'timeguide':
        case 'none':
            new YAHOO.util.DDTarget(divs[i].id, 'cells');
            break;

        }
    }
}

function showGuide() {
    var timegd = (document.timegd.timeguide.checked) ? 'on' : 'off';
    location.href = 'schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>&timegd=' + timegd;
}

function showProject() {
    var pjtgd = (document.timegd.pjtguide.checked) ? 'on' : 'off';
    location.href = 'schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>&pjtgd=' + pjtgd;
}

function openPrint() {
    window.open('schedule_print_week_menu.php?&session=<?php eh($session); ?>&date=<?php eh($date); ?>', 'newwin', 'width=800,height=600,scrollbars=yes');
}

function changeDate(dt) {
    location.href = 'schedule_week_menu.php?session=<?php eh($session); ?>&date=' + dt;
}

function popupScheduleDetail(title, place, date, time, type, detail, e) {
    popupDetailBlue(
        new Array(
            'タイトル', title,
            '行先', place,
            '日付', date,
            '時刻', time,
            '種別', type,
            '詳細', detail
        ), 400, 80, e
    );
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
    popupDetailBlue(
        new Array(
            '施設', fcl_name,
            '日付', evt_date,
            '時刻', evt_time,
            '行事名', evt_name,
            '詳細', evt_content,
            '連絡先', evt_contact
        ), 400, 80, e
    );
}

function highlightCells(class_name) {
    changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
    changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
    var cells = document.getElementsByTagName('td');
    for (var i = 0, j = cells.length; i < j; i++) {
        if (cells[i].className != class_name) {
            continue;
        }
        cells[i].style.backgroundColor = color;
    }
}
function downloadCSV(){
    document.csv.action = 'schedule_csv.php';
    document.csv.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="//initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#5279a5"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>週</b></font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール検索</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー連携</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onclick="openPrint();"></td>
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($last_month); ?>">&lt;&lt;前月</a>
<a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($last_week); ?>">&lt;前週</a>
<a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($this_week); ?>">今週</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);">
<? show_date_options_w($date, $start_date); ?>
</select>
<a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($next_week); ?>">翌週&gt;</a>
<a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($next_month); ?>">翌月&gt;&gt;</a>
</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事/所内行事
echo ($_label_by_profile["EVENT"][$profile_type]); ?></font><input type="checkbox" name="timeguide" onclick="showGuide();"<? if ($timegd == "on") {echo(" checked");} ?>>&nbsp;<a href="javascript:void(0);" onclick="window.open('schedule_project_setting.php?session=<?php eh($session); ?>', 'newwin', 'width=640, height=700, scrollbars=yes');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></a><input type="checkbox" name="pjtguide" onclick="showProject();"<? if ($pjtgd == "on") {echo(" checked");} ?>></td>
</tr>
</table>
<?
$start_ymd = date("Ymd", $start_date);
$end_ymd = date("Ymd", strtotime("+6 days", $start_date));
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_ymd, $end_ymd);
$holiday_memo_flg = false;
foreach($arr_calendar_memo as $tmp_date => $tmp_memo) {
    if ($tmp_memo != "" && strlen($tmp_date) == 8) {
        $y = substr($tmp_date, 0, 4);
        $m = substr($tmp_date, 4, 2);
        $d = substr($tmp_date, 6, 2);
        $holiday_name = ktHolidayName(mktime(0, 0, 0, $m, $d, $y));
        if ($holiday_name != "") {
            $holiday_memo_flg = true;
            break;
        }
    }
}
if ($holiday_memo_flg == true) {
    $tmp_height = "58";
} else {
    $tmp_height = "42";
}

echo("<div style=\"height:{$tmp_height}px; overflow: auto;border:#CCCCCC solid 1px;\">\n");echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

// 日付行を表示
show_dates($con, $dates, $emp_id, $normal_schedules, $session, $fname, $arr_calendar_memo, $tmp_height);

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $session, false);

// 時間指定あり行を表示
show_normal_schedules($timeless_schedules, $normal_schedules, $session, false);

echo("</table>\n");
echo("</div>\n");

echo("<div style=\"height:520px; overflow: scroll;border:#CCCCCC solid 1px;\">\n");
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $session, false);

// 時間指定あり行を表示
show_normal_schedules($timeless_schedules, $normal_schedules, $session, false);

echo("</table>\n");
echo("</div>\n");
?>
</form>
</td>
</tr>
</table>
<!-- CSV出力用form -->
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo($session); ?>">
    <input type="hidden" name="start_date" value="<?php echo $start_ymd; ?>">
    <input type="hidden" name="end_date" value="<?php echo $end_ymd; ?>">
    <input type="hidden" name="pjt_f" value="<?php echo $pjtgd; ?>">
    <input type="hidden" name="time_f" value="<?php echo $timegd; ?>">
</form>

</body>
<? pg_close($con); ?>
</html>
<?
// 表示対象週のスタート日のタイムスタンプを取得
function get_start_date_timestamp($con, $date, $emp_id, $fname) {
    $sql = "select calendar_start1 from option";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

    $start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

    return get_start_day($date, $start_wd);
}

// 表示対象週の7日分の日付を配列で取得
function get_dates_in_week($start_date) {
    $ret = array();
    for ($i = 0; $i < 7; $i++) {
        $ret[] = date("Ymd", strtotime("+$i days", $start_date));
    }
    return $ret;
}

// 日付行を表示
function show_dates($con, $dates, $emp_id, $normal_schedules, $session, $fname, $arr_calendar_memo, $height) {
    $today = date("Ymd");

    echo("<tr height=\"$height\">\n");
    echo("<td>&nbsp;</td>\n");
    foreach ($dates as $date) {
        $colspan = get_overlap_schedule_count($normal_schedules[$date]);
        $timestamp = to_timestamp($date);
        $month = date("m", $timestamp);
        $day = date("d", $timestamp);
        $weekday_info = get_weekday_info($timestamp);
        $holiday = ktHolidayName($timestamp);
        if ($date == $today) {
            $bgcolor = "#ccffcc";
        } elseif ($holiday != "") {
            $bgcolor = "#fadede";
        } else if ($arr_calendar_memo["{$date}_type"] == "5") {
            $bgcolor = "#defafa";
        } else {
            $bgcolor = $weekday_info["color"];
        }
        $schd_type_name = get_schd_name($con, $emp_id, $emp_id, $date, $fname);

        echo("<td width=\"14%\" align=\"center\" valign=\"top\" colspan=\"$colspan\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b><a href=\"schedule_menu.php?session=$session&date=$timestamp\">{$month}/{$day}（{$weekday_info["label"]}）</a></b><br>");
        // カレンダーのメモがある場合は設定する
        if ($arr_calendar_memo["$date"] != "") {
            if ($holiday == "") {
                $holiday = $arr_calendar_memo["$date"];
            } else {
                $holiday .= "<br>".$arr_calendar_memo["$date"];
            }
        }
        if ($holiday != "") {
            echo("<font color=\"red\" class=\"j12\">$holiday</font>&nbsp;");
        }
        if ($schd_type_name != "") {
            echo("<font class=\"j12\">$schd_type_name</font><br>");
        }
        echo("</font></td>\n");
    }
    echo("</tr>\n");
}

// タイムスタンプから曜日情報を取得
function get_weekday_info($date) {
    $wd = date("w", $date);
    switch ($wd) {
    case "0":
        return array("label" => "日", "color" => "#fadede");
        break;
    case "1":
        return array("label" => "月", "color" => "#fefcdf");
        break;
    case "2":
        return array("label" => "火", "color" => "#fefcdf");
        break;
    case "3":
        return array("label" => "水", "color" => "#fefcdf");
        break;
    case "4":
        return array("label" => "木", "color" => "#fefcdf");
        break;
    case "5":
        return array("label" => "金", "color" => "#fefcdf");
        break;
    case "6":
        return array("label" => "土", "color" => "#defafa");
        break;
    }
}
