<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix 職員登録 | 復活</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

if ($personal_id != "") {

	// データベースに接続
	$con = connect2db($fname);

	// トランザクションを開始
	pg_query($con, "begin");

	// 削除済みチェック
	$sql = "select emp_del_flg from authmst";
	$cond = "where emp_id = (select emp_id from empmst where emp_personal_id = '$personal_id')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された職員IDは使われていません。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
	if (pg_fetch_result($sel, 0, "emp_del_flg") != "t") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された職員は削除されていません。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}

	// 職員ID・氏名・ログインID・パスワードを取得
	$sql = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, login.emp_login_id, login.emp_login_pass from empmst inner join login on empmst.emp_id = login.emp_id";
	$cond = "where empmst.emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_lt_nm = pg_fetch_result($sel, 0, "emp_lt_nm");
	$emp_ft_nm = pg_fetch_result($sel, 0, "emp_ft_nm");
	$login_id = pg_fetch_result($sel, 0, "emp_login_id");
	$login_pass = pg_fetch_result($sel, 0, "emp_login_pass");

	// ログインID重複チェック
	$sql = "select count(*) from login";
	$cond = "where emp_login_id = '$login_id' and exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_fetch_result($sel, 0, 0) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された職員のログインIDが、他の職員に使用されているため、\\n復活できません。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	// 削除フラグを戻す
	$sql = "update authmst set";
	$set = array("emp_del_flg");
	$setvalue = array('f');
	$cond = "where emp_id = '$emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// XOOPSアカウントの作成（本当は管理者かどうか判別すべき）
//XX 	require_once("community/mainfile.php");
	require_once("webmail/config/config.php");

//XX	$con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX	mysql_select_db(XOOPS_DB_NAME, $con_mysql);
//XX
//XX	$sql = "insert into " . XOOPS_DB_PREFIX . "_users (name, uname, email, user_regdate, user_viewemail, pass, rank, level, timezone_offset, notify_method, user_mailok) values ('$emp_lt_nm $emp_ft_nm', '$login_id', '$login_id@$domain', " . time() . ", 1, '" . md5($login_pass) . "', 0, 1, '9.0', 2, 0);";
//XX	if (!mysql_query($sql, $con_mysql)) {
//XX		pg_query($con, "rollback");
//XX		pg_close($con);
//XX		mysql_close($con_mysql);
//XX		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX		exit;
//XX	}

//XX	$uid = mysql_insert_id($con_mysql);
//XX	$sql = "insert into " . XOOPS_DB_PREFIX . "_groups_users_link (groupid, uid) values (2, $uid)";
//XX	if (!mysql_query($sql, $con_mysql)) {
//XX		pg_query($con, "rollback");
//XX		pg_close($con);
//XX		mysql_close($con_mysql);
//XX		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX		exit;
//XX	}

	// コミット処理
	pg_query($con, "commit");
	pg_close($con);
//XX	mysql_close($con_mysql);

	// メールアカウントの追加（ログインIDが数字のみでない場合）
	if (preg_match("/\D/", $login_id) > 0) {
		$dir = getcwd();
		if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
			exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $login_id $login_pass");
		} else {
			exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $login_id $login_pass $imapServerAddress $dir");
		}
	}

	$completed = true;
} else {
	$completed = false;
}
?>
</head>
<body>
<form action="employee_restore.php" method="post">
<p>職員ID：<input type="text" name="personal_id" value="<? echo(h($personal_id)); ?>"> <input type="submit" value="復活"></p>
</form>
<?
if ($completed) {
	echo("<p>復活処理が完了しました。</p>\n");
}
?>
</body>
</html>
