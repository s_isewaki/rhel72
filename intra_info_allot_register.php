<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 施設名を取得
$sql = "select name from allotfcl";
$cond = "where allotfcl_id = $fcl_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$fcl_name = pg_fetch_result($sel, 0, "name");

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 社会福祉法人の場合、「・外来」を削除
if ($profile_type == PROFILE_TYPE_WELFARE) {
	$menu2_4 = mbereg_replace("・外来", "", $menu2_4);
}
?>
<title><? echo($menu2_4); ?> | 登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form action="intra_info_allot_insert.php" method="post" enctype="multipart/form-data">
<table width="600" border="0" cellspacing="0" cellpadding="3" class="list">
<tr height="22">
<td width="140" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">施設</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? echo($fcl_name); ?></font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">ファイル</font></td>
<td><input type="file" name="file" size="50"></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><select name="year"><? show_years_future(2); ?></select>/<select name="month"><? show_select_months(date("m")); ?></select>/<select name="day"><? show_select_days(date("d")); ?></select></font><br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※同じ日付のファイルが既にある場合、今回のファイルで上書きされます。</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="3">
<tr>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="fcl_id" value="<? echo($fcl_id); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
