<?
/*

画面パラメータについて
	$session
		セッションＩＤ
	$pt_id
		患者ＩＤ
	$problem_id
		プロブレムＩＤ
*/
?><meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("./get_values.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//権限チェック
//==============================
$dept = check_authority($session,57,$fname);
if($dept == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ＤＢのコネクション作成(トランザクション開始)
//==============================
$con = connect2db($fname);
pg_query($con, "begin transaction");

//==============================
//プロブレム使用中チェック
//==============================
$tbl = "select count(*) from summary ";
$cond = "where (ptif_id = '$pt_id') and (problem_id = '$problem_id')";
$sel = select_from_table($con,$tbl,$cond,$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num=pg_result($sel,0,"count");
if($num > 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"このプロブレムは使用されているため削除できません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

//==============================
//ＤＢから削除
//==============================
$sql = "delete from summary_problem_list ";
$cond = "where (ptif_id = '$pt_id') and (problem_id = '$problem_id')";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//==============================
//履歴テーブルからも削除
//==============================
$sql = "delete from summary_problem_list_rireki ";
$cond = "where (ptif_id = '$pt_id') and (problem_id = '$problem_id')";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//==============================
// ＤＢコネクション切断(トランザクション終了)
//==============================
pg_query($con, "commit");
pg_close($con);

//==============================
// 処理完了JavaScript出力
//==============================

//呼び出し元画面をリロード
echo("<script language=\"javascript\">window.opener.location.reload();</script>\n");

//自画面をクローズ
echo("<script language=\"javascript\">window.close();</script>\n");
exit;


?>