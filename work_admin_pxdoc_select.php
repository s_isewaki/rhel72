<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix �ж�ɽ | ���ϴ��֤�Ĵ��</title>

<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_timecard_common.ini");
require_once("show_select_values.ini");

$fname = $PHP_SELF;
// �ǡ����١�������³
$con = connect2db($fname);

$sql = "select closing, closing_parttime, closing_month_flg, sortkey1, sortkey2, sortkey3, sortkey4, sortkey5 from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	
	$closing = pg_fetch_result($sel, 0, "closing");
	$closing_month_flg = pg_fetch_result($sel, 0, "closing_month_flg");
	$sortkey1 = pg_fetch_result($sel, 0, "sortkey1");
	$sortkey2 = pg_fetch_result($sel, 0, "sortkey2");
	$sortkey3 = pg_fetch_result($sel, 0, "sortkey3");
	$sortkey4 = pg_fetch_result($sel, 0, "sortkey4");
	$sortkey5 = pg_fetch_result($sel, 0, "sortkey5");
} else {
	$closing = "";
	$closing_month_flg = "1";
	$sortkey1 = "";
	$sortkey2 = "";
	$sortkey3 = "";
	$sortkey4 = "";
	$sortkey5 = "";
}

$year = substr($yyyymm, 0, 4);
$month = intval(substr($yyyymm, 4, 2));

$last_month_y = $year;
$last_month_m = $month - 1;
if ($last_month_m == 0) {
	$last_month_y--;
	$last_month_m = 12;
}
$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

$next_month_y = $year;
$next_month_m = $month + 1;
if ($next_month_m == 13) {
	$next_month_y++;
	$next_month_m = 1;
}
$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

// ���������������򻻽�
$calced = false;
switch ($closing) {
	case "1":  // 1��
		$closing_day = 1;
		break;
	case "2":  // 5��
		$closing_day = 5;
		break;
	case "3":  // 10��
		$closing_day = 10;
		break;
	case "4":  // 15��
		$closing_day = 15;
		break;
	case "5":  // 20��
		$closing_day = 20;
		break;
	case "6":  // ����
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;
	
	//��������
	if ($closing_month_flg != "2") {
		$start_year = $year;
		$start_month = $month;
		
		$end_year = $year;
		$end_month = $month + 1;
		if ($end_month == 13) {
			$end_year++;
			$end_month = 1;
		}
	}
	//���������
	else {
		$start_year = $year;
		$start_month = $month - 1;
		if ($start_month == 0) {
			$start_year--;
			$start_month = 12;
		}
		
		$end_year = $year;
		$end_month = $month;
	}
	
}
$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
// �ȿ����ؾ������
$arr_org_level = array ();
$level_cnt = 0;
$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname ";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_org_level[0] = pg_result($sel, 0, "class_nm");
$arr_org_level[1] = pg_result($sel, 0, "atrb_nm");
$arr_org_level[2] = pg_result($sel, 0, "dept_nm");
$arr_org_level[3] = pg_result($sel, 0, "room_nm");

?>
<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
//����
function print_pxdoc() {
	if(window.opener && !parent.opener.closed && window.opener.print_pxdoc){
		window.opener.document.csv.select_start_yr.value = document.mainform.start_year.value;
		window.opener.document.csv.select_start_mon.value = document.mainform.start_month.value;
		window.opener.document.csv.select_start_day.value = document.mainform.start_day.value;
		window.opener.document.csv.select_end_yr.value = document.mainform.end_year.value;
		window.opener.document.csv.select_end_mon.value = document.mainform.end_month.value;
		window.opener.document.csv.select_end_day.value = document.mainform.end_day.value;
		window.opener.document.csv.sortkey1.value = document.mainform.sortkey1.value;
		window.opener.document.csv.sortkey2.value = document.mainform.sortkey2.value;
		window.opener.document.csv.sortkey3.value = document.mainform.sortkey3.value;
		window.opener.document.csv.sortkey4.value = document.mainform.sortkey4.value;
		window.opener.document.csv.sortkey5.value = document.mainform.sortkey5.value;
		window.opener.print_pxdoc();
		window.close();
	}
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b>���ϴ��֤�Ĵ��</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">

	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
	<td align="left">
	<table width="400" border="0" cellspacing="0" cellpadding="2">
	<tr height="32">
		<td align="center" colspan="5">
			<select name="start_year">
			<? show_select_years(10, $start_year, false, true); ?>
			</select>/<select name="start_month">
			<? show_select_months($start_month, false); ?>
			</select>/<select name="start_day">
			<? show_select_days($start_day, false); ?>
			</select> �� <select name="end_year">
			<? show_select_years(10, $end_year, false, true); ?>
			</select>/<select name="end_month">
			<? show_select_months($end_month, false); ?>
			</select>/<select name="end_day">
			<? show_select_days($end_day, false); ?>
			</select>&nbsp;
		</td>
	</tr>
	<tr height="32">
<?
//�������̾
$arr_sortitem = array("", "����ID", "��̾", "$arr_org_level[0]", "$arr_org_level[1]", "$arr_org_level[2]", "����");

for ($key_idx=1; $key_idx<=5; $key_idx++) {
	
	echo("<td>");

	echo("<font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\">�����ȥ���{$key_idx}</font><br>");
	
	$varname = "sortkey".$key_idx;
	echo("<select name=\"$varname\" id=\"$varname\">");
	$sortkey_val = $$varname;
	for ($i=0; $i<count($arr_sortitem); $i++) {
		//����
		if ($sortkey_val == $i) {
			$selected = " selected";
		} else {
			$selected = "";
		}
		echo("<option value=\"$i\" $selected>");
		
		echo($arr_sortitem[$i]);
		echo("</option>");
	}
	echo("</select><br>");
	echo("</td>\n");
	
}

?>
		
	</tr>
	<tr height="32">
		<td align="right" colspan="5">
			<input type="button" value="�¹�" onclick="print_pxdoc();">&nbsp;

		</td>
	</tr>
	</table>

	</td>
	</tr>
	</table>

	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>

</body>
<? pg_close($con); ?>
</html>

