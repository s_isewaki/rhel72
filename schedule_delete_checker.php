<?
require("about_session.php");
require("about_authority.php");
require("schedule_repeat_common.php");
require("schedule_common.ini");
require("webmail/config/config.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id) from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);

// 削除通知メールの宛先を設定、登録者IDも取得
$table_name = ($timeless != "t") ? "schdmst" : "schdmst2";
$sql = "select get_mail_login_id(emp_id) as mail_login_id, schd_reg_id from $table_name";
$cond = "where schd_id = '$schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
// 削除済みの場合
if (pg_num_rows($sel) == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('この予約は既に削除されています。');</script>");
	if (isset($ret_url)) {
		echo("<script type=\"text/javascript\">history.back();</script>");
	} else {
		echo("<script type=\"text/javascript\">opener.location.reload();self.close();</script>");
	}
	exit;
}
$mailto = pg_fetch_result($sel, 0, "mail_login_id");
$schd_reg_id = pg_fetch_result($sel, 0, "schd_reg_id");

// 行先一覧を配列に格納
$places = get_places($con, $fname);

// 登録者の場合は他職員分も処理する
$sync_emp_flg = ($emp_id == $schd_reg_id) ? true : false ;
// 削除対象スケジュール一覧を取得
$del_schedules = get_repeated_schedules($con, $schd_id, $timeless, $rptopt, $fname, $sync_emp_flg);

// 削除を実行
foreach ($del_schedules as $tmp_schedule) {
	$tmp_table_name = ($tmp_schedule["timeless"] == "f") ? "schdmst" : "schdmst2";
	$tmp_schd_id = $tmp_schedule["id"];

	$sql = "delete from $tmp_table_name";
	$cond = "where schd_id = '$tmp_schd_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

$schedules_for_mail = array_filter($del_schedules, "is_approved_schedule");
usort($schedules_for_mail, "sort_by_emp_id_datetime");

$schedule_count = count($schedules_for_mail);
if ($schedule_count > 0 && $emp_id == $schd_reg_id) { // 登録者の場合送付、登録者以外は自分の予約のみのため不要
	$subject = "[CoMedix] スケジュール削除のお知らせ";
	$additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";
	$message = "下記の承認済みスケジュールが削除されました。\n\n";
	$message .= "登録者　：{$emp_nm}\n";

	// 職員IDをキーとして処理
	$old_emp_id = "";

	foreach ($schedules_for_mail as $tmp_schedule) {

		$tmp_emp_id = $tmp_schedule["emp_id"];
		// 初回のみキー設定
		if ($old_emp_id == "") {
			$old_emp_id = $tmp_emp_id;
		}

		// 職員が変わった場合
		if ($old_emp_id != $tmp_emp_id) {
			// メールアドレス取得
			$to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);
			// メール送信
			mb_send_mail($to_addr, $subject, $message, $additional_headers);
			// キー設定
			$old_emp_id = $tmp_emp_id;
			// 初期化
			$message = "下記の承認済みスケジュールが削除されました。\n\n";
			$message .= "登録者　：{$emp_nm}\n";
		}

		$tmp_place_name = ($tmp_schedule["place_id"] == "0") ? "その他" : $places[$tmp_schedule["place_id"]];
		if ($tmp_place_name != "") {
			$tmp_place_name .= " {$tmp_schedule["place_detail"]}";
		} else {
			$tmp_place_name = "{$tmp_schedule["place_detail"]}";
		}
		$mail_schd_detail = str_replace("\n", "\n　　　　　", $tmp_schedule["detail"]);
		$message .= "-----------------------------------------------------\n";
		$message .= "タイトル：{$tmp_schedule["title"]}\n";
		$message .= "行先　　：{$tmp_place_name}\n";
		$message .= "日時　　：{$tmp_schedule["date"]} {$tmp_schedule["time"]}\n";
		$message .= "詳細　　：{$mail_schd_detail}\n";
	}

	// メールアドレス取得
	$to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);

	mb_send_mail($to_addr, $subject, $message, $additional_headers);
}

// データベース接続を閉じる
pg_close($con);

// 画面遷移
if (isset($ret_url)) {
	echo("<script type=\"text/javascript\">location.href = '$ret_url';</script>");
} else {
	echo("<script type=\"text/javascript\">opener.location.reload();self.close();</script>");
}

function is_approved_schedule($schd) {
	return ($schd["approve_status"] == "3");
}

function sort_by_datetime($schd1, $schd2) {
	if ($schd1["date"] != $schd2["date"]) {
		return strcmp($schd1["date"], $schd2["date"]);
	}
	if ($schd1["timeless"] != $schd2["timeless"]) {
		return strcmp($schd1["timeless"], $schd2["timeless"]) * -1;
	}
	if ($schd1["time"] != $schd2["time"]) {
		return strcmp($schd1["time"], $schd2["time"]);
	}
	return strcmp($schd1["id"], $schd2["id"]);
}

function sort_by_emp_id_datetime($schd1, $schd2) {
	if ($schd1["emp_id"] != $schd2["emp_id"]) {
		return strcmp($schd1["emp_id"], $schd2["emp_id"]);
	}
	if ($schd1["date"] != $schd2["date"]) {
		return strcmp($schd1["date"], $schd2["date"]);
	}
	if ($schd1["timeless"] != $schd2["timeless"]) {
		return strcmp($schd1["timeless"], $schd2["timeless"]) * -1;
	}
	if ($schd1["time"] != $schd2["time"]) {
		return strcmp($schd1["time"], $schd2["time"]);
	}
	return strcmp($schd1["id"], $schd2["id"]);
}

function get_mail_addr($con, $fname, $emp_id, $domain) {
	$sql = "select get_mail_login_id(emp_id) as mail_login_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$mail_addr = pg_fetch_result($sel, 0, "mail_login_id");
	$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
	$to_addr = mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$mail_addr@$domain>";
	return $to_addr;
}
