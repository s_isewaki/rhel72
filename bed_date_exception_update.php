<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 選択されている棟の患者をいったん除外対象から外す
$sql = "update inptmst set";
$set = array("inpt_except_flg");
$setvalue = array("f");
$cond = ($bldg_cd != "0") ? "where bldg_cd = '$bldg_cd'" : "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// チェックされた患者を除外対象とする
foreach ($pt_id as $id) {
	$sql = "update inptmst set";
	$set = array("inpt_except_flg");
	$setvalue = array("t");
	$cond = "where ptif_id = '$id'";
	$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($up == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 計算除外患者指定画面を再表示
echo "<script type=\"text/javascript\">location.href = 'bed_date_exception.php?session=$session&bldg_cd=$bldg_cd&insurance=$insurance&in_days=$in_days&sort=$sort';</script>";
?>
