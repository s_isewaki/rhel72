<?php
//=========================================================
// シフトグループ登録
//
// ※元々、自動スタッフ条件設定「登録」だったため、
//   duty_shift_auto_update_exe.phpというファイル名になっています。
//=========================================================
require_once("about_comedix.php");

//---------------------------------------------------------
ob_start();
require_once("duty_shift_common_class.php");
//ページ名
$fname = $_SERVER['PHP_SELF'];

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
$adminauth = check_authority($session, 70, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///利用するCLASS
$obj = new duty_shift_common_class($con, $fname);

//---------------------------------------------------------
// 勤務シフトグループの権限のチェック
$group_id = p($_REQUEST["group_id"]);
$emp_id = p($_REQUEST["emp_id"]);
$sql = "SELECT * FROM duty_shift_group g
		WHERE group_id='{$group_id}'
		AND (person_charge_id='{$emp_id}'
		OR caretaker_id='{$emp_id}'
		OR caretaker2_id='{$emp_id}'
		OR caretaker3_id='{$emp_id}'
		OR caretaker4_id='{$emp_id}'
		OR caretaker5_id='{$emp_id}'
		OR caretaker6_id='{$emp_id}'
		OR caretaker7_id='{$emp_id}'
		OR caretaker8_id='{$emp_id}'
		OR caretaker9_id='{$emp_id}'
		OR caretaker10_id='{$emp_id}')";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel === 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) <= 0) {
	js_alert_exit("シフトグループを更新する権限がありません。");
}

//---------------------------------------------------------
// 入力項目のチェック
//-- 勤務希望締切日
if (preg_match("/[^0-9]/", $_REQUEST["close_day"]) > 0) {
	js_alert_exit("「勤務希望締切日」に数値以外が設定されています。");
}
//-- 連続勤務日上限
if (preg_match("/[^0-9]/", $_REQUEST["duty_upper_limit_day"]) > 0) {
	js_alert_exit("「連続勤務日上限」に数値以外が設定されています。");
}
//-- シフト作成期間
if ($_REQUEST["start_month_flg1"] === "1" and $_REQUEST["month_flg1"] === "1") {
	if ($_REQUEST["start_day1"] !== "0" and $_REQUEST["end_day1"] !== "0" and $_REQUEST["start_day1"] > $_REQUEST["end_day1"]) {
		js_alert_exit("終了日を開始日以降にしてください。");
	}
}
if ($_REQUEST["start_month_flg1"] === "0" && $_REQUEST["month_flg1"] === "2") {
	js_alert_exit("終了日を開始日の翌月同日以前にしてください。");
}
if (($_REQUEST["start_month_flg1"] === "0" and $_REQUEST["month_flg1"] === "1") or ($_REQUEST["start_month_flg1"] === "1" && $_REQUEST["month_flg1"] === "2")){
	if ($_REQUEST["start_day1"] !== "0" and $_REQUEST["end_day1"] !== "0" and $_REQUEST["start_day1"] < $_REQUEST["end_day1"]) {
		js_alert_exit("終了日を開始日の翌月同日以前にしてください。");
	}
}
//-- 組み合わせ指定
$seq_today = $_REQUEST["seq_today"];
$seq_next = $_REQUEST["seq_next"];
for($i=0; $i<count($seq_today); $i++) {
	if (empty($seq_today[$i]) or empty($seq_next[$i])) {
		js_alert_exit("「組み合わせ指定」で未設定の項目が存在します。");
	}
	if ($seq_today[$i] === $seq_next[$i]) {
		js_alert_exit("「組み合わせ指定」の「当日」「翌日」に同じシフトは指定できません。");
	}
}
//-- 組み合わせ禁止
$ng_today = $_REQUEST["ng_today"];
$ng_next = $_REQUEST["ng_next"];
for($i=0; $i<count($ng_today); $i++) {
	if (empty($ng_today[$i]) or empty($ng_next[$i])) {
		js_alert_exit("「組み合わせ禁止」で未設定の項目が存在します。");
	}

	// 組み合わせ指定と禁止の重複チェック
	for($j=0; $j<count($seq_today); $j++) {
		if ($seq_today[$j] === $ng_today[$i] and $seq_next[$j] === $ng_next[$i]) {
			js_alert_exit("「組み合わせ指定」と「組み合わせ禁止」に同じ組み合わせが存在します。");
		}
	}
}
//-- 連続勤務シフトの上限
$limit_id = $_REQUEST["limit_id"];
$limit_day = $_REQUEST["limit_day"];
for($i=0; $i<count($limit_id); $i++) {
	if (empty($limit_id[$i]) or empty($limit_day[$i])) {
		js_alert_exit("「連続勤務シフトの上限」で未設定の項目が存在します。");
	}
	if (preg_match("/[^0-9]/", $limit_day[$i]) > 0) {
		js_alert_exit("「連続勤務シフトの上限」の上限に数値以外が設定されています。");
	}
	for($j=0; $j<count($limit_id); $j++) {
		if ($i != $j and $limit_id[$i] == $limit_id[$j]) {
			js_alert_exit("「連続勤務シフトの上限」が重複設定されています。");
		}
	}
}
//-- 勤務シフトの間隔
$interval_id = $_REQUEST["interval_id"];
$interval_day = $_REQUEST["interval_day"];
for($i=0; $i<count($interval_id); $i++) {
	if (empty($interval_id[$i]) or empty($interval_day[$i])) {
		js_alert_exit("「勤務シフトの間隔」で未設定の項目が存在します。");
	}
	if (preg_match("/[^0-9]/", $interval_day[$i]) > 0) {
		js_alert_exit("「勤務シフトの間隔」の間隔日数に数値以外が設定されています。");
	}
	for($j=0; $j<count($interval_id); $j++) {
		if ($i != $j and $interval_id[$i] == $interval_id[$j]) {
			js_alert_exit("「勤務シフトの間隔」が重複設定されています。");
		}
	}
}
//-- 性別固定シフト
$sex_pattern = $_REQUEST["sex_pattern"];
for($i=0; $i<count($sex_pattern); $i++) {
	if (empty($sex_pattern[$i]) or empty($sex_pattern[$i])) {
		js_alert_exit("「性別固定シフト」で未設定の項目が存在します。");
	}
	for($j=0; $j<count($sex_pattern); $j++) {
		if ($i != $j and $sex_pattern[$i] == $sex_pattern[$j]) {
			js_alert_exit("「性別固定シフト」が重複設定されています。");
		}
	}
}
//-- 4週8休の開始日
if ($_REQUEST["four_week_chk_flg"] == "t") {
	if (empty($_REQUEST["chk_start_m"]) OR empty($_REQUEST["chk_start_d"]) OR empty($_REQUEST["chk_start_y"])) {
		js_alert_exit("4週8休のチェックをする場合は開始日を指定してください。");
	}
	if (!checkdate($_REQUEST["chk_start_m"], $_REQUEST["chk_start_d"], $_REQUEST["chk_start_y"])) {
		js_alert_exit("4週8休のチェックの開始日が正しくありません。");
	}
	$chk_start_date = sprintf("%04d%02d%02d", $_REQUEST["chk_start_y"], $_REQUEST["chk_start_m"], $_REQUEST["chk_start_d"]);
}
else {
	$chk_start_date = "";
	$_REQUEST["hol_pattern"] = array();
}
//-- 解析対象とする期間
if (preg_match("/[^0-9]/", $_REQUEST["auto_check_months"]) > 0) {
	js_alert_exit("「解析対象とする期間」に数値以外が設定されています。");
}
//-- 勤務シフト表タイトル
if (empty($_REQUEST["print_title"])) {
	js_alert_exit("勤務シフト表タイトルを入力してください。");
}
//作成者
if ($_REQUEST["createEmpDispFlg"] == "t" && (empty($_REQUEST["createEmpStat"]) || empty($_REQUEST["createEmpName"]))) {
	js_alert_exit("作成者を表示する場合は役職と氏名を入力してください。");
}

//---------------------------------------------------------
// トランザクションを開始
pg_query($con, "begin transaction");

//--- duty_shift_group display
$sql_grp = "SELECT display FROM duty_shift_group WHERE group_id='{$group_id}'";
$sel_grp = select_from_table($con, $sql_grp, "", $fname);
if ($sel_grp == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$grp = pg_fetch_assoc($sel_grp);
$display = unserialize($grp['display']);
$display['count_row_position'] = $_REQUEST["count_row_position"];
$display['result_row_position'] = $_REQUEST["result_row_position"];

//--- duty_shift_group
$sql = "UPDATE duty_shift_group SET";
$set = array(
		"close_day",
		"duty_upper_limit_day",
		"start_month_flg1",
		"month_flg1",
		"start_day1",
		"end_day1",
		"reason_setting_flg",
		"four_week_chk_flg",
		"chk_start_date",
		"need_less_chk_flg",
		"need_more_chk_flg",
		"auto_check_months",
		"auto_shift_filler",
		"print_title",
		"team_disp_flg",
		"job_disp_flg",
		"st_disp_flg",
		"paid_hol_disp_flg",
		"es_disp_flg",
        "display",
        "seven_work_chk_flg",
        "part_week_chk_flg",
        "unit_disp_flg",
        "transfer_holiday_days_flg",
		"approval_column_print_flg",
		"unit_disp_level"
);
$setvalue = array(
		p($_REQUEST["close_day"]),
		p($_REQUEST["duty_upper_limit_day"]),
		p($_REQUEST["start_month_flg1"]),
		p($_REQUEST["month_flg1"]),
		p($_REQUEST["start_day1"]),
		p($_REQUEST["end_day1"]),
		p($_REQUEST["reason_setting_flg"]=="f" ? "f" : "t"),
		p($_REQUEST["four_week_chk_flg"]=="t" ? "t" : "f"),
		p($chk_start_date),
		p($_REQUEST["need_less_chk_flg"]=="t" ? "t" : "f"),
		p($_REQUEST["need_more_chk_flg"]=="t" ? "t" : "f"),
		p($_REQUEST["auto_check_months"]),
		p($_REQUEST["auto_shift_filler"]),
		p($_REQUEST["print_title"]),
		p($_REQUEST["team_disp_flg"]),
		p($_REQUEST["job_disp_flg"]),
		p($_REQUEST["st_disp_flg"]),
		p($_REQUEST["paid_hol_disp_flg"]),
		p($_REQUEST["es_disp_flg"]),
		serialize($display),
		p($_REQUEST["seven_work_chk_flg"]=="t" ? "t" : "f"),
		p($_REQUEST["part_week_chk_flg"]=="t" ? "t" : "f"),
		p($_REQUEST["unit_disp_flg"]=="t" ? "t" : "f"),
		p($_REQUEST["transfer_holiday_days_flg"]=="t" ? "t" : "f"),
		p($_REQUEST["approval_column_print_flg"]=="t" ? "t" : "f"),
		p($_REQUEST["unit_disp_level"]=="4" ? "4" : "3")				// デフォルトを３階層とする
		);
$cond = "WHERE group_id='{$group_id}'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//--- duty_shift_group_seq_pattern
$sql = "DELETE FROM duty_shift_group_seq_pattern WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_group_seq_pattern (group_id,no,today_atdptn_ptn_id,nextday_atdptn_ptn_id,today_reason,nextday_reason) VALUES (";
for($i=0; $i<count($seq_today); $i++) {
	$content = array(
		$group_id,
		$i+1,
		p((int)substr($seq_today[$i], 0, 2)),
		p((int)substr($seq_next[$i], 0, 2)),
		p((int)substr($seq_today[$i], 2, 2)),
		p((int)substr($seq_next[$i], 2, 2))
		);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//--- duty_shift_group_ng_pattern
$sql = "DELETE FROM duty_shift_group_ng_pattern WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_group_ng_pattern (group_id,no,today_atdptn_ptn_id,nextday_atdptn_ptn_id,today_reason,nextday_reason) VALUES (";
for($i=0; $i<count($ng_today); $i++) {
	$content = array(
		$group_id,
		$i+1,
		p((int)substr($ng_today[$i], 0, 2)),
		p((int)substr($ng_next[$i], 0, 2)),
		p((int)substr($ng_today[$i], 2, 2)),
		p((int)substr($ng_next[$i], 2, 2))
		);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//--- duty_shift_group_upper_limit
$sql = "DELETE FROM duty_shift_group_upper_limit WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_group_upper_limit (group_id,no,atdptn_ptn_id,upper_limit_day,reason) VALUES (";
for($i=0; $i<count($limit_id); $i++) {
	$content = array(
		$group_id,
		$i+1,
		p((int)substr($limit_id[$i], 0, 2)),
		p($limit_day[$i]),
		p((int)substr($limit_id[$i], 2, 2)),
		);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//--- duty_shift_group_interval
$sql = "DELETE FROM duty_shift_group_interval WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_group_interval (group_id,no,atdptn_ptn_id,interval_day,reason) VALUES (";
for($i=0; $i<count($interval_id); $i++) {
	$content = array(
		$group_id,
		$i+1,
		p((int)substr($interval_id[$i], 0, 2)),
		p($interval_day[$i]),
		p((int)substr($interval_id[$i], 2, 2)),
		);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//--- duty_shift_group_sex_pattern
$sql = "DELETE FROM duty_shift_group_sex_pattern WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_group_sex_pattern (group_id,no,ptn_id) VALUES (";
for($i=0; $i<count($_REQUEST["sex_pattern"]); $i++) {
	$content = array(
		$group_id,
		$i+1,
		p($_REQUEST["sex_pattern"][$i])
		);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//--- duty_shift_group_hol_pattern
$sql = "DELETE FROM duty_shift_group_hol_pattern WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_group_hol_pattern (group_id,no,ptn_id) VALUES (";
for($i=0; $i<count($_REQUEST["hol_pattern"]); $i++) {
	$content = array(
			$group_id,
			$i+1,
			p($_REQUEST["hol_pattern"][$i])
			);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//--- duty_shift_group_st_name
$sql = "DELETE FROM duty_shift_group_st_name WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_group_st_name (group_id,no,st_name) VALUES (";
for($i=0; $i<count($_REQUEST["st_name"]); $i++) {
	$content = array(
			$group_id,
			$i+1,
			p($_REQUEST["st_name"][$i])
			);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 休暇自動割当設定
//--- duty_shift_vacation_limit_day
$sql = "DELETE FROM duty_shift_vacation_limit_day WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "INSERT INTO duty_shift_vacation_limit_day (group_id,holiday_1,holiday_2,holiday_3,holiday_4,holiday_5,holiday_6,holiday_7,holiday_8,force_hol_set) VALUES(";
for($i=1; $i<9; $i++){ // 月〜金・土・日・祝
	$name="holiday_".$i;
	$d[$i]=p($_REQUEST[$name]);
	if($d[$i]==""){$d[$i]=0;} // 入力が無い欄は0にする
}
$c=p($_REQUEST["force_hol_set"]); // 「土日、祝日に１日は必ず休暇（公休）を割り当てる」チェック
$content = array( $group_id ,$d[1], $d[2], $d[3], $d[4], $d[5], $d[6], $d[7], $d[8], $c );
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//作成者情報登録
$obj->conf_set($group_id, "createEmpDispFlg",$_REQUEST["createEmpDispFlg"]);
$obj->conf_set($group_id, "createEmpStat", $_REQUEST["createEmpStat"]);
$obj->conf_set($group_id, "createEmpName", $_REQUEST["createEmpName"]);

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);
ob_end_clean();
?>
<script type="text/javascript">location.href='duty_shift_auto.php?session=<?=$session?>&group_id=<?=$group_id?>'</script>