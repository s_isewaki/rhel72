<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix アドレス帳 | 更新</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("show_prefectures.ini"); ?>
<?
require("show_select_values.ini");
require("get_values.ini");
require("label_by_profile_type.ini");

$fname=$PHP_SELF;

$session=qualify_session($session,$fname);
if($session=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アドレス帳権限を取得
$adbk=check_authority($session,9,$fname);
if($adbk=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// アドレス帳管理権限を取得
$adbk_admin_auth=check_authority($session,64,$fname);
?>
<?
$con=connect2db($fname);
if($con=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$url_name = urlencode($search_name);
$url_submit = urlencode($search_submit);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$login_emp_id = pg_fetch_result($sel, 0, "emp_id");

$contact_title = "詳細";
$contact_title_flg = "f";
$in_emp_id = $emp_id; // パラメータのIDを退避
// 本人連絡先の場合はログインユーザのIDを設定
if ($emp_id == "") {
	$emp_id = $login_emp_id;
	$contact_title = "本人連絡先";
	$contact_title_flg = "t";
}

// 職員情報を取得
$infos = get_empmst($con, $emp_id, $fname);

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function copyToShared() {
	document.address.action = './address_insert.php?session=<? echo($session); ?>&shared_flg=t&admin_flg=<? echo($admin_flg); ?>';
	document.address.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="address_menu.php?session=<? echo($session); ?>"><img src="img/icon/b13.gif" width="32" height="32" border="0" alt="アドレス帳"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_menu.php?session=<? echo($session); ?>"><b>アドレス帳</b></a>

<? if ($admin_flg == "t") { ?>
 &gt; <a href="address_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
<? } else { ?>
	</font></td>
	<? if ($adbk_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<?
	}
}
?>
</tr>
</table>
<form name="address" action="./address_contact_update_exe.php?session=<? echo($session); ?>&address_id=<? echo($address_id); ?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
// 管理画面
 if ($admin_flg == "t") { ?>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./address_admin_menu.php?session=<? echo($session); ?>&search_shared_flg=<? echo($search_shared_flg); ?>&search_name=<? echo($url_name); ?>&search_submit=<? echo($url_submit); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス一覧</font></a></td>

<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="address_contact_update.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&search_shared_flg=<? echo($search_shared_flg); ?>&search_name=<? echo($url_name); ?>&search_submit=<? echo($url_submit); ?>&page=<? echo($page); ?>&admin_flg=<? echo($admin_flg); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>詳細</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="">&nbsp;</td>
<? } else {
// ユーザ画面
 ?>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./address_menu.php?session=<? echo($session); ?>&search_shared_flg=<? echo($search_shared_flg); ?>&search_name=<? echo($url_name); ?>&search_submit=<? echo($url_submit); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス一覧</font></a></td>
<?
// 詳細画面
if ($contact_title_flg == "f") {
?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="address_contact_update.php?session=<? echo($session); ?>&emp_id=<? echo($in_emp_id); ?>&search_shared_flg=<? echo($search_shared_flg); ?>&search_name=<? echo($url_name); ?>&search_submit=<? echo($url_submit); ?>&page=<? echo($page); ?>&admin_flg=<? echo($admin_flg); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><?=$contact_title?></b></font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規作成</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<?
// 本人連絡先画面
if ($contact_title_flg == "t") {
?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="address_contact_update.php?session=<? echo($session); ?>&emp_id=<? echo($in_emp_id); ?>&search_shared_flg=<? echo($search_shared_flg); ?>&search_name=<? echo($url_name); ?>&search_submit=<? echo($url_submit); ?>&page=<? echo($page); ?>&admin_flg=<? echo($admin_flg); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><?=$contact_title?></b></font></a></td>
<? } ?>
<?
// 一覧から遷移した詳細画面の場合
 if ($in_emp_id != "") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7">
<?
echo ("<a href=\"address_contact_update.php?session=$session&emp_id=&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page&admin_flg=$admin_flg\">");
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本人連絡先</font></a></td>
<? } ?>

<? if ($admin_flg != "t") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ作成</font></a></td>
<? } ?>

<td width="">&nbsp;</td>
<? // 本人連絡先 ?>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録先</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名簿</font></td>
</tr>
<tr>
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infos[1]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（漢字）</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infos[7]); ?>&nbsp;<? echo($infos[8]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（ひらがな）</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infos[9]); ?>&nbsp;<? echo($infos[10]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">郵便番号</font></td>
<td><input name="post1" type="text" size="5" maxlength="3" value="<? echo($infos[11]); ?>" style="ime-mode:inactive;">-<input name="post2" type="text" size="5" maxlength="4" value="<? echo($infos[12]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">都道府県</font></td>
<td><select name="province"><? show_select_provinces($infos[13], true); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所1</font></td>
<td><input name="address1" type="text" size="50" maxlength="50" value="<? echo($infos[14]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所2</font></td>
<td><input name="address2" type="text" size="50" maxlength="50" value="<? echo($infos[15]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">電話番号</font></td>
<td><input name="tel1" type="text" size="5" maxlength="6" value="<? echo($infos[17]); ?>" style="ime-mode:inactive;">-<input name="tel2" type="text" size="5" maxlength="6" value="<? echo($infos[18]); ?>" style="ime-mode:inactive;">-<input name="tel3" type="text" size="5" maxlength="6" value="<? echo($infos[19]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">E-Mail</font></td>
<td><input name="email2" type="text" size="40" maxlength="100" value="<? echo($infos[31]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">携帯端末・外線</font></td>
<td><input name="mobile1" type="text" size="5" maxlength="6" value="<? echo($infos[20]); ?>" style="ime-mode:inactive;">-<input name="mobile2" type="text" size="5" maxlength="6" value="<? echo($infos[21]); ?>" style="ime-mode:inactive;">-<input name="mobile3" type="text" size="5" maxlength="6" value="<? echo($infos[22]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">携帯Mail</font></td>
<td><input name="m_mail" type="text" size="40" maxlength="100" value="<? echo($infos[24]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線番号</font></td>
<td><input name="extension" type="text" size="10" maxlength="10" value="<? echo($infos[16]); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内PHS/所内PHS
echo $_label_by_profile["PHS"][$profile_type];
?></font></td>
<td><input name="phs" type="text" size="10" maxlength="6" value="<? echo($infos[32]); ?>" style="ime-mode:inactive;"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2" height="22" align="right">
<? // 一覧からの遷移の場合は戻るボタン表示
if ($contact_title_flg == "f") { 
?>
<input type="button" value="戻る" onclick="history.back();">&nbsp;
<? } ?>
<? // 管理者、本人連絡先でなければ更新不可
if ($admin_flg == "t" || $emp_id == $login_emp_id) { ?>
<input type="submit" value="更新">
<? } ?>
</td>
</tr>
</table>
<input type="hidden" name="search_shared_flg" value="<? echo($search_shared_flg); ?>">
<input type="hidden" name="search_name" value="<? echo($search_name); ?>">
<input type="hidden" name="search_submit" value="<? echo($search_submit); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="admin_flg" value="<? echo($admin_flg); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="contact_title_flg" value="<? echo($contact_title_flg); ?>">
</form>
</td>
</tr>
</table>
</body>
</html>