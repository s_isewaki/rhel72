<?
$enti = 1;
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="disease_register.php">
<input type="hidden" name="back" value="t">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="disease" value="<? echo($disease); ?>">
<input type="hidden" name="icd_cd" value="<? echo($icd_cd); ?>">
<input type="hidden" name="sect" value="<? echo($sect); ?>">
<input type="hidden" name="result" value="<? echo($result); ?>">
<input type="hidden" name="sy" value="<? echo($sy); ?>">
<input type="hidden" name="sm" value="<? echo($sm); ?>">
<input type="hidden" name="sd" value="<? echo($sd); ?>">
<input type="hidden" name="ey" value="<? echo($ey); ?>">
<input type="hidden" name="em" value="<? echo($em); ?>">
<input type="hidden" name="ed" value="<? echo($ed); ?>">
<input type="hidden" name="insurance" value="<? echo($insurance); ?>">
<input type="hidden" name="enti" value="<? echo($enti); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="r_emp" value="<? echo($r_emp); ?>">
<input type="hidden" name="r_date" value="<? echo($r_date); ?>">
<input type="hidden" name="r_time" value="<? echo($r_time); ?>">
</form>
<?

//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("get_values.ini");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$disreg = check_authority($session, 36, $fname);
if ($ptreg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$dishist = check_authority($session, 34, $fname);

//form
//正規表現チェック

if($pt_id ==""){
	echo("<script language=\"javascript\">alert(\"不正なアクセスです\");</script>\n");
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

if(strlen($disease) > 200){
	echo("<script language=\"javascript\">alert(\"病名が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if(strlen($icd_cd) > 10){
	echo("<script language=\"javascript\">alert(\"ICD10が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if($sect == "0"){
	echo("<script language=\"javascript\">alert(\"診療科を選択してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if (!checkdate($sm, $sd, $sy)) {
	echo("<script language=\"javascript\">alert(\"開始日付が不正です\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if (!checkdate($em, $ed, $ey)) {
	echo("<script language=\"javascript\">alert(\"転帰日付が不正です\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if(strlen($insurance) > 40){
	echo("<script language=\"javascript\">alert(\"保険が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if($enti == "0"){
	echo("<script language=\"javascript\">alert(\"事業所を選択してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//----------Transaction begin----------

$con = connect2db($fname);

pg_exec($con,"begin transaction");

// 登録値の編集
$strt_dt = $sy . $sm . $sd;  // 開始日
$end_dt = $ey . $em . $ed;  // 転帰日

// 診断名を更新
$sql = "update dishist set";
$set = array("dishist_disease", "dishist_icd_cd", "sect_id", "dishist_strt_dt", "dishist_end_dt", "dishist_result", "dishist_insurance");
$setvalue = array($disease, $icd_cd, $sect, $strt_dt, $end_dt, $result, $insurance);
$cond = "where ptif_id = '$pt_id' and dishist_emp_id = '$r_emp' and dishist_reg_dt = '$r_date' and dishist_reg_tm = '$r_time'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

pg_exec($con, "commit");
pg_close($con);

/*
$key1 = urlencode($key1);
$key2 = urlencode($key2);
if ($dishist == 1) {
	echo("<script language='javascript'>location.href = 'disease_detail.php?session=$session&pt_id=$pt_id&key1=$key1&key2=$key2&page=$page';</script>");
} else {
	echo("<script language='javascript'>location.href = 'disease_register.php?session=$session&pt_id=$pt_id&key1=$key1&key2=$key2&page=$page';</script>");
}
*/

echo("<script language='javascript'>opener.location.reload();self.close();</script>");
?>
</body>
