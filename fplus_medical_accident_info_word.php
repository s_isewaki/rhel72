<?php

require_once ( './libs/phpword/PHPWord.php' );
require_once ( './libs/phpword/PHPWord/IOFactory.php' );
require_once ( './libs/phpword/PHPWord/Writer/Word2007.php' );

// オブジェクト
$PHPWord = new PHPWord();


//====================================================================
// ヘッダ設定
//====================================================================
header("Content-Disposition: attachment; filename=fplus_medical_accident_info_".Date("Ymd_Hi").".docx;");
header("Content-Transfer-Encoding: binary");

// 日本語はUTF-8へ変換する。(mb_convert_encoding関数を使用)
// 変換しない場合は文書ファイルが壊れて生成される。

// デフォルトのフォント
$PHPWord->setDefaultFontName(mb_convert_encoding('ＭＳ 明朝','UTF-8','EUC-JP'));
$PHPWord->setDefaultFontSize(10.5);

// プロパティ
$properties = $PHPWord->getProperties();
$properties->setTitle(mb_convert_encoding('医療事故情報','UTF-8','EUC-JP'));
$properties->setCreator(mb_convert_encoding('Medi-System','UTF-8','EUC-JP')); 
$properties->setSubject(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setCompany(mb_convert_encoding('（株）メディシステムソリューション','UTF-8','EUC-JP'));
$properties->setCategory(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setCreated(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setModified(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setKeywords(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setLastModifiedBy(mb_convert_encoding('Medi-System','UTF-8','EUC-JP'));

// セクションとスタイル、単位に注意(1/144インチ)
$section = $PHPWord->createSection();
$sectionStyle = $section->getSettings();
$sectionStyle->setLandscape();
$sectionStyle->setPortrait();
$sectionStyle->setMarginLeft(1700);
$sectionStyle->setMarginRight(1700);
$sectionStyle->setMarginTop(1700);
$sectionStyle->setMarginBottom(1700);


$PHPWord->addParagraphStyle('pStyle', array('spacing'=>100));
$PHPWord->addFontStyle('BoldText', array('bold'=>true));
$PHPWord->addFontStyle('ColoredText', array('color'=>'FF8080'));
$PHPWord->addLinkStyle('NLink', array('color'=>'0000FF', 'underline'=>PHPWord_Style_Font::UNDERLINE_SINGLE));


//タイトル
$PHPWord->addFontStyle('title_pStyle', array('size'=>14,'spacing'=>100));
$PHPWord->addParagraphStyle('title_align', array('align'=>'center'));
$section->addText(mb_convert_encoding(mb_convert_encoding("医療事故情報（案）","sjis-win","eucJP-win"),"UTF-8","sjis-win"), 'title_pStyle','title_align');

//表題
$section->addText(mb_convert_encoding('「'.$hdn_title.'」','UTF-8','EUC-JP'), 'contents_font', 'title_align');
$section->addTextBreak(1);
// 本文
$PHPWord->addParagraphStyle('contents_align', array('align'=>'left'));
$section->addText(mb_convert_encoding('1 事故概要','UTF-8','EUC-JP'), 'contents_font', 'contents_align');
$section->addText(mb_convert_encoding('　【患者背景】','UTF-8','EUC-JP'), 'contents_font', 'contents_align');
// 入力データを改行で分割
$ary_patient_background = preg_split("/\n/", $data_patient_background);
for($i=0;$i<count($ary_patient_background);$i++){
	$section->addText(mb_convert_encoding('　'.$ary_patient_background[$i],'UTF-8','EUC-JP'), 'contents_font', 'contents_align');
}
$section->addTextBreak(1);
$section->addText(mb_convert_encoding('　【事実経過】','UTF-8','EUC-JP'), 'contents_font', 'contents_align');
// 入力データを改行で分割
$ary_fact_progress = preg_split("/\n/", $data_fact_progress);
for($i=0;$i<count($ary_fact_progress);$i++){
	$section->addText(mb_convert_encoding('　'.$ary_fact_progress[$i],'UTF-8','EUC-JP'), 'contents_font', 'contents_align');
}
$section->addTextBreak(1);
$section->addText(mb_convert_encoding('2 事故発生後の対応','UTF-8','EUC-JP'), 'contents_font', 'contents_align');
// 入力データを改行で分割
$ary_accident_cope = preg_split("/\n/", $data_accident_cope);
for($i=0;$i<count($ary_accident_cope);$i++){
	$section->addText(mb_convert_encoding('　'.$ary_accident_cope[$i],'UTF-8','EUC-JP'), 'contents_font', 'contents_align');
}
$section->addTextBreak(1);
$section->addText(mb_convert_encoding('3 患者・家族への対応','UTF-8','EUC-JP'), 'contents_font', 'contents_align');
// 入力データを改行で分割
$ary_patient_family_cope = preg_split("/\n/", $data_patient_family_cope);
for($i=0;$i<count($ary_patient_family_cope);$i++){
	$section->addText(mb_convert_encoding('　'.$ary_patient_family_cope[$i],'UTF-8','EUC-JP'), 'contents_font', 'contents_align');
}
$section->addTextBreak(1);
$section->addText(mb_convert_encoding('4 事故発生要因','UTF-8','EUC-JP'), 'contents_font', 'contents_align');
// 入力データを改行で分割
$ary_occurrences_factor = preg_split("/\n/", $data_occurrences_factor);
for($i=0;$i<count($ary_occurrences_factor);$i++){
	$section->addText(mb_convert_encoding('　'.$ary_occurrences_factor[$i],'UTF-8','EUC-JP'), 'contents_font', 'contents_align');
}
$section->addTextBreak(1);
$section->addText(mb_convert_encoding('5 再発防止の改善策','UTF-8','EUC-JP'), 'contents_font', 'contents_align');
// 入力データを改行で分割
$ary_remedy = preg_split("/\n/", $data_remedy);
for($i=0;$i<count($ary_remedy);$i++){
	$section->addText(mb_convert_encoding('　'.$ary_remedy[$i],'UTF-8','EUC-JP'), 'contents_font', 'contents_align');
}

// 出力
$WORD_OUT =  PHPWord_IOFactory::createWriter($PHPWord,'WORD2007');
//$WORD_OUT->save('./fplus/workflow/tmp/test.docx');
$WORD_OUT->save('php://output');

exit;
