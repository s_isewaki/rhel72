<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	js_login_exit();
}

if (strlen($note) > 100) {
	js_alert_exit("備考は全角50文字までで入力してください。");
}

$target_array = explode("|", $target);

$con = connect2db($fname);
pg_query($con, "begin");

switch ($target_array[0]) {

// 日付分
case "d":
	$date = $target_array[1];

	$sql = "delete from bed_outnote";
	$cond = q("where type = 'd' and mode = '%s' and date = '%s'", $mode, $date);
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}

	if ($note != "") {
		$sql = "insert into bed_outnote (type, mode, date, note) values (";
		$content = array("d", $mode, $date, $note);
		$ins = insert_into_table($con, $sql, q($content), $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	}

	break;

// 患者分
case "p":
	$ptif_id = $target_array[1];

	// 予定の場合
	if ($mode == "r") {
		$sql = "delete from bed_outnote";
		$cond = q("where type = 'p' and mode = 'r' and ptif_id = '%s'", $ptif_id);
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}

		if ($note != "") {
			$sql = "insert into bed_outnote (type, mode, ptif_id, note) values (";
			$content = array("p", "r", $ptif_id, $note);
			$ins = insert_into_table($con, $sql, q($content), $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				js_error_exit();
			}
		}

	// 実績の場合
	} else {
		$in_dttm = $target_array[2];

		$sql = "delete from bed_outnote";
		$cond = q("where type = 'p' and mode = 'o' and ptif_id = '%s' and in_dttm = '%s'", $ptif_id, $in_dttm);
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}

		if ($note != "") {
			$sql = "insert into bed_outnote (type, mode, ptif_id, in_dttm, note) values (";
			$content = array("p", "o", $ptif_id, $in_dttm, $note);
			$ins = insert_into_table($con, $sql, q($content), $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				js_error_exit();
			}
		}
	}

	break;
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
opener.location.reload(true);
self.close();
</script>
