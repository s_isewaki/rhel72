<?php

require_once("about_comedix.php");

// パラメータ
$emp_id = $_GET["emp_id"];
$public_str = $_REQUEST["str"];

// パラメータチェック
if (preg_match("/\A[0-9]{12}\z/", $emp_id) !== 1) {
    header('Status: 403 Forbidden');
    print("403 Forbidden");
    exit;
}
if (preg_match("/\A[A-Za-z0-9]{64}\z/", $public_str) !== 1) {
    header('Status: 403 Forbidden');
    print("403 Forbidden");
    exit;
}

// DB接続
$fname = $_SERVER['PHP_SELF'];
$con = connect2db($fname);

// カレンダー連携設定
$sql = "SELECT * FROM schdical WHERE emp_id='{$emp_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel === 0) {
    pg_close($con);
    header('Status: 403 Forbidden');
    print("403 Forbidden");
    exit;
}
if (pg_num_rows($sel) < 1) {
    pg_close($con);
    header('Status: 403 Forbidden');
    print("403 Forbidden");
    exit;
}

$ical = pg_fetch_assoc($sel);

if ($ical["active"] === 'f') {
    pg_close($con);
    header('Status: 403 Forbidden');
    print("403 Forbidden");
    exit;
}
if ($ical["public_str"] !== $public_str) {
    pg_close($con);
    header('Status: 403 Forbidden');
    print("403 Forbidden");
    exit;
}

// Date
$before_date = date('Y-m-d', strtotime("-{$ical["before_days"]} day"));
$after_date = date('Y-m-d', strtotime("+{$ical["after_days"]} day"));

// Header
header("Content-Type: text/calendar; charset=utf-8");
ob_start();
print("BEGIN:VCALENDAR\n");
print("VERSION:2.0\n");
print("X-WR-CALNAME:CoMedixスケジュール\n");
print("PRODID:-//hacksw/handcal//NONSGML v1.0//EN\n\n");

//==========================================================
// スケジュール
//==========================================================
$sql = "
SELECT
    schd_id,
    schd_title,
    schd_type,
    schd_start_time_v,
    schd_dur_v,
    schd_start_date,
    schd_imprt,
    schd_status,
    schd_detail,
    reg_time,
    schd_plc,
    t.type_name,
    sp.place_name,
    schdmst_id
FROM (
    SELECT
        schd_id,
        schd_title,
        schd_type,
        schd_start_time_v,
        schd_dur_v,
        schd_start_date,
        schd_imprt,
        schd_status,
        schd_detail,
        reg_time,
        schd_plc,
        schd_place_id,
        1 as schdmst_id
    FROM schdmst
    WHERE emp_id='{$emp_id}'
      AND schd_start_date >= '$before_date'
      AND schd_start_date <= '$after_date'

    UNION ALL

    SELECT
        schd_id,
        schd_title,
        schd_type,
        null AS schd_start_time_v,
        null AS schd_dur_v,
        schd_start_date,
        schd_imprt,
        schd_status,
        schd_detail,
        reg_time,
        schd_plc,
        schd_place_id,
        2 as schdmst_id
    FROM schdmst2 s
    WHERE emp_id='{$emp_id}'
      AND schd_start_date >= '$before_date'
      AND schd_start_date <= '$after_date'

) s
LEFT JOIN schdtype t ON t.type_id = s.schd_type
LEFT JOIN scheduleplace sp ON sp.place_id = s.schd_place_id
"; //"
$sel = select_from_table($con, $sql, "", $fname);
if ($sel === 0) {
    pg_close($con);
    print("END:VCALENDAR\n");
    exit;
}

//一覧を表示
while ($row = pg_fetch_assoc($sel)) {

    // タイプ
    if ($ical["type"] === '1' and $row["schd_imprt"] !== '2') {
        continue;
    }
    elseif ($ical["type"] === '2' and $row["schd_imprt"] === '1') {
        continue;
    }
    elseif ($ical["type"] === '2' and $row["schd_imprt"] === '3') {
        $row["schd_title"] = "予定あり";
    }

    print("BEGIN:VEVENT\n");

    // DTSTART - DTEND
    $date = preg_replace("/^(\d{4})\-(\d\d)\-(\d\d)/", "$1$2$3", $row["schd_start_date"]);
    if (empty($row["schd_start_time_v"]) and empty($row["schd_dur_v"])) {
        print("DTSTART;VALUE=DATE:{$date}\n");
    }
    else {
        print("DTSTART;TZID=Asia/Tokyo:{$date}T{$row["schd_start_time_v"]}00\n");
        print("DTEND;TZID=Asia/Tokyo:{$date}T{$row["schd_dur_v"]}00\n");
    }

    // DTSTAMP
    $dtstamp = preg_replace("/^(\d{8})(\d{6})\d*/", "$1T$2", $row["reg_time"]);
    print("DTSTAMP:{$dtstamp}\n");

    // SUMMARY
    $title = "";
    if ($ical["type_in_title"] == "t" and $row["schd_type"] != 1 and $ical["type"] != 2) {
        $title .= "[{$row["type_name"]}]";
    }
    $title .= $row["schd_title"];
    if ($row["schd_status"] === '2') {
        $title .= " [未承認]";
    }
    print("SUMMARY:{$title}\n");

    // DESCRIPTION
    if (!empty($row["schd_detail"])) {
        $schd_detail = preg_replace("/\r*\n/ms", '\n', $row["schd_detail"]);
        print("DESCRIPTION:{$schd_detail}\n");
    }

    // LOCATION
    if (!empty($row["place_name"]) or !empty($row["schd_plc"])) {
        $location = !empty($row["place_name"]) ? "[{$row["place_name"]}]" : "";
        $location .=!empty($row["schd_plc"]) ? $row["schd_plc"] : "";
        print("LOCATION:{$location}\n");
    }

    // CATEGORY
    if (!empty($row["schd_type"]) and $row["schd_type"] !== '1') {
        print("CATEGORIES:{$row["type_name"]}\n");
    }

    // VALARM

    print("END:VEVENT\n\n");
}

//==========================================================
// 院内行事
//==========================================================
if ($ical["timeguide"] === "t") {
    $sql = "SELECT t.*,f.name as fcl_name FROM timegd t LEFT JOIN timegdfcl f USING (fcl_id) WHERE event_date >= '$before_date' AND event_date <= '$after_date'";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel === 0) {
        pg_close($con);
        print("END:VCALENDAR\n");
        exit;
    }

    while ($row = pg_fetch_assoc($sel)) {

        print("BEGIN:VEVENT\n");

        // DTSTART - DTEND
        $date = preg_replace("/^(\d{4})\-(\d\d)\-(\d\d)/", "$1$2$3", $row["event_date"]);
        if ($row["time_flg"] === 'f') {
            print("DTSTART;VALUE=DATE:{$date}\n");
        }
        else {
            $tstart = preg_replace("/^(\d{2}):(\d\d):(\d\d)/", "$1$2$3", $row["event_time"]);
            $tend = preg_replace("/^(\d{2}):(\d\d):(\d\d)/", "$1$2$3", $row["event_dur"]);
            print("DTSTART;TZID=Asia/Tokyo:{$date}T{$tstart}\n");
            print("DTEND;TZID=Asia/Tokyo:{$date}T{$tend}\n");
        }

        // SUMMARY
        print("SUMMARY:[行事]{$row["event_name"]}\n");

        // DESCRIPTION
        if (!empty($row["event_content"])) {
            $tmp_event_content = preg_replace("/\r\n/", '\n', $row["event_content"]);
            print("DESCRIPTION:{$tmp_event_content}\n");
        }

        // LOCATION
        if (!empty($row["fcl_name"]) or !empty($row["fcl_detail"])) {
            $fcl = !empty($row["fcl_name"]) ? "[{$row["fcl_name"]}]" : "";
            $fcl .=!empty($row["fcl_detail"]) ? $row["fcl_detail"] : "";
            print("LOCATION:{$fcl}\n");
        }

        print("END:VEVENT\n\n");
    }
}

//==========================================================
// 委員会
//==========================================================
if ($ical["project"] === "t") {
    $sql = "
SELECT
  pjt_schd_id,
  pjt_schd_title,
  pjt_schd_plc,
  pjt_schd_type,
  pjt_schd_start_date,
  pjt_schd_detail,
  pjt_schd_title_abbrev,
  reg_time,
  pjt_schd_start_time_v,
  pjt_schd_dur_v,
  p.pjt_name_abbrev,
  t.type_name,
  sp.place_name,
  schdmst_id
FROM (
    SELECT
        pjt_id,
        pjt_schd_id,
        pjt_schd_title,
        pjt_schd_plc,
        pjt_schd_type,
        pjt_schd_start_date,
        pjt_schd_detail,
        pjt_schd_title_abbrev,
        reg_time,
        pjt_schd_start_time_v,
        pjt_schd_dur_v,
        pjt_schd_place_id,
        1 as schdmst_id
    FROM proschd ps
    WHERE ps.pjt_id IN (select pjt_id from schdproject where emp_id='{$emp_id}' and ownother='1')
      AND ps.pjt_schd_start_date >= '$before_date'
      AND ps.pjt_schd_start_date <= '$after_date'

    UNION ALL

    SELECT
        pjt_id,
        pjt_schd_id,
        pjt_schd_title,
        pjt_schd_plc,
        pjt_schd_type,
        pjt_schd_start_date,
        pjt_schd_detail,
        null AS pjt_schd_title_abbrev,
        reg_time,
        null AS pjt_schd_start_time_v,
        null AS pjt_schd_dur_v,
        pjt_schd_place_id,
        2 as schdmst_id
    FROM proschd2 ps
    WHERE ps.pjt_id IN (select pjt_id from schdproject where emp_id='{$emp_id}' and ownother='1')
      AND ps.pjt_schd_start_date >= '$before_date'
      AND ps.pjt_schd_start_date <= '$after_date'
) ps
JOIN project p USING (pjt_id)
LEFT JOIN proschdtype t ON t.type_id = ps.pjt_schd_type
LEFT JOIN scheduleplace sp ON sp.place_id = ps.pjt_schd_place_id
WHERE p.pjt_delete_flag = 'f'
"; //"
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel === 0) {
        pg_close($con);
        print("END:VCALENDAR\n");
        exit;
    }

    //一覧を表示
    while ($row = pg_fetch_assoc($sel)) {

        print("BEGIN:VEVENT\n");

        // DTSTART - DTEND
        $date = preg_replace("/^(\d{4})\-(\d\d)\-(\d\d)/", "$1$2$3", $row["pjt_schd_start_date"]);
        if (empty($row["pjt_schd_start_time_v"]) and empty($row["pjt_schd_dur_v"])) {
            print("DTSTART;VALUE=DATE:{$date}\n");
        }
        else {
            print("DTSTART;TZID=Asia/Tokyo:{$date}T{$row["pjt_schd_start_time_v"]}00\n");
            print("DTEND;TZID=Asia/Tokyo:{$date}T{$row["pjt_schd_dur_v"]}00\n");
        }

        // DTSTAMP
        $dtstamp = preg_replace("/^(\d{8})(\d{6})\d*/", "$1T$2", $row["reg_time"]);
        print("DTSTAMP:{$dtstamp}\n");

        // SUMMARY
        $title = "";
        if (!empty($row["pjt_name_abbrev"])) {
            $title .= "【{$row["pjt_name_abbrev"]}】";
        }
        $title .= $row["pjt_schd_title"];
        print("SUMMARY:{$title}\n");

        // DESCRIPTION
        if (!empty($row["pjt_schd_detail"])) {
            $tmp_pjt_schd_detail = preg_replace("/\r\n/", '\n', $row["pjt_schd_detail"]);
            print("DESCRIPTION:{$tmp_pjt_schd_detail}\n");
        }

        // LOCATION
        if (!empty($row["place_name"]) or !empty($row["pjt_schd_plc"])) {
            $location = !empty($row["place_name"]) ? "[{$row["place_name"]}]" : "";
            $location .=!empty($row["pjt_schd_plc"]) ? $row["pjt_schd_plc"] : "";
            print("LOCATION:{$location}\n");
        }

        // CATEGORY
        if (!empty($row["pjt_schd_type"]) and $row["pjt_schd_type"] != 1) {
            print("CATEGORIES:{$row["type_name"]}\n");
        }

        // VALARM

        print("END:VEVENT\n\n");
    }
}

pg_close($con);
print("END:VCALENDAR\n");

$output = ob_get_contents();
ob_end_clean();
print(mb_convert_encoding($output, "UTF-8", "EUC-JP"));
