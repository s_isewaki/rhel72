<?php

ob_start();
/*
?>
<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;">
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜管理指標</title>
 -->
<?
*/
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");

require_once("jnl_application_common.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);

/*
?>
<!--


<script type="text/javascript">

</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="決裁・管理指標"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_indicator_menu.php?session=<? echo($session); ?>"><b>管理指標</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="wkfw" action="jnl_application_imprint.php" method="post" enctype="multipart/form-data">
 -->
<?php
*/
require_once('jnl_indicator_data.php');
require_once('jnl_indicator_display.php');
$indicatorData    = new IndicatorData(array(
    'fname' => $fname,
    'con'   => $con
));
$indicatorDisplay = new IndicatorDisplay(array(
    'fname'         => $fname,
    'con'           => $con,
    'session'       => $session,
    'display_year'  => $display_year,
    'display_month' => $display_month,
    'display_day'   => $display_day
));


//20110419
$display_year  = $indicatorDisplay->getDisplayYear();
$display_year = !empty($fiscal_year)? $fiscal_year : $display_year;
//20110419

$display_month = $indicatorDisplay->getDisplayMonth();
$display_day   = $indicatorDisplay->getDisplayDay($display_month, $display_year);

$noArr         = $indicatorData->getNoForNo($no);
$fileNameType  = $noArr['file_name'];
switch ($fileNameType) {
    case 'name_y_m_d':
        $fileName      = sprintf('%s_%04d_%02d_%02d.xls', $noArr['name'], $display_year, $display_month, $display_day);
        break;

    case 'name_y_m':
        $fileName      = sprintf('%s_%04d_%02d.xls', $noArr['name'], $display_year, $display_month);
        break;

	case 'name_y':
		//$fileName      = sprintf('%s_%04d.xls', $noArr['name'], $indicatorDisplay->getCurrentYear());
		$fileName      = sprintf('%s_%04d.xls', $noArr['name'], $display_year);
}


if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}
$fileName = mb_convert_encoding($fileName, $encoding, mb_internal_encoding());
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');

$plantArr = $indicatorData->getHospitalList();
$dataList = $indicatorData->getDisplayDataList(
    $no,
    $indicatorDisplay->getDateToLastYearStartMonth(),
    $indicatorDisplay->getDateToCurrentYearLastMonth(),
    array('year' => $display_year, 'month' => $display_month, 'day' => $display_day, 'facility_id' => $plant)
);
$download_data = $indicatorDisplay->outputExcelData(
    $no,
    $dataList,
    $plantArr,
    $noArr,
    array('year' => $display_year, 'month' => $display_month, 'day' => $display_day)
);
echo mb_convert_encoding(
    nl2br($download_data),
    'sjis',
    mb_internal_encoding()
);
//echo $indicatorData->getSql();
//var_dump($dataList);
ob_end_flush();
/*
<!--
<input type="hidden" name="session" value="<? echo($session); ?>">

</form>
</td>
</tr>
</table>

</body>
 -->
 */
?>
<? pg_close($con); ?>
<?
/*
<!--
</html>
 -->
 */
?>