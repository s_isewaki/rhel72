<?/*

画面パラメータ
$session
	セッションID。必須。
$pt_id
	患者ID。必須。
$target_month
	対象月。YYYYMM形式。例)"200606"
	省略時は今月。
$month_hani
	月範囲。例)"1"
	省略時は１ヶ月。
$sort
	ソート条件。以下のキーワードのいずれかを指定。
	キーワード
		cre_date
		problem
		diag_div
		kisaisha
		shokushu
		priority
		desc_*   …*のキーワードに対して逆ソート
	省略時は作成年月日の逆ソート。
	副ソート条件は診療記録ID(固定)。
$is_user_only
	記載者表示条件
		true…利用者が記載者のデータのみ表示
		false…全記載者のデータを表示
	省略時はfalse


*/

//ソート条件定数
define("SORT_CRE_DATE", "cre_date");
define("SORT_PROBLEM", "problem");
define("SORT_DIAG_DIV", "diag_div");
define("SORT_KISAISHA", "kisaisha");
define("SORT_SHOKUSHU", "shokushu");
define("SORT_PRIORITY", "priority");


/**
 * YYYYMM形式の日付に指定値を加算します。
 *
 * 注意：この関数は±12ヶ月以上は対応していません。
 * @param $yyyymm 加算前のYYYYMM値
 * @param $add 加算値
 * @return 加算後のYYYYMM値
 */
function add_yyyymm($yyyymm,$add)
{
	$year = substr($yyyymm, 0, 4);
	$month = substr($yyyymm, 4, 2);
	$year1  = $year;
	$month1 = $month + $add;
	if($month1 > 12)
	{
		$year1  = $year1 +1;
		$month1 = $month1 -12;
	}
	if($month1 < 1)
	{
		$year1  = $year1 -1;
		$month1 = $month1 +12;
	}
	$year1  = "0000".$year1;
	$month1 = "00".$month1;
	$year1  = substr($year1,strlen($year1)-4,4);
	$month1 = substr($month1,strlen($month1)-2,2);
	$yyyymm1 = $year1.$month1;
	return $yyyymm1;
}

/**
 * 氏、名を氏名フォーマットに従い氏名に変換します。
 * get_values.iniのget_emp_kanji_name()と同様の変換ルールで変換します。
 * @param $lt_nm 氏
 * @param $ft_nm 名
 * @return 氏名
 */
function format_kanji_name($lt_nm,$ft_nm)
{
	$name = "$lt_nm"." "."$ft_nm";
	return $name;
}

//テンプレート名取得
function get_arr_tmplname($con, $fname) {

	$tbl = "select * from tmplmst";
	$cond = "";
	$sel = select_from_table($con,$tbl,$cond,$fname);
	if($sel == 0){
		pg_close($con);
		exit;
	}

	$arr_tmplname = array();
	$num = pg_numrows($sel);
	for ($i=0; $i<$num; $i++) {
		$id = pg_result($sel,$i,"tmpl_id");
		$arr_tmplname[$id] = pg_result($sel,$i,"tmpl_name");
	}

	return $arr_tmplname;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require("./conf/sql.inf");
require_once("about_comedix.php");
require("./get_values.ini");
require("label_by_profile_type.ini");

//========================================
//前処理
//========================================

//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
echo("<script type='text/javascript' src='./js/showpage.js'></script>");
echo("<script language='javascript'>showLoginPage(window);</script>");
exit;
}

// 権限チェック
$hiyari = check_authority($session, 57, $fname);
if ($hiyari == "0") {
	showLoginPage();
	exit;
}

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//利用者ID取得
$emp_id = get_emp_id($con, $session, $fname);

//========================================
//省略パラメータのデフォルト設定
//========================================
if(@$target_month == "") {
	$date_wk = mktime(0,0,0,date('m'), date('d'), date('Y'));
	$year_wk = date('Y', $date_wk);
	$month_wk = date('m', $date_wk);
	$target_month = $year_wk.$month_wk;
}
if(@$month_hani == "") $month_hani = "1";
if(@$sort == "") $sort = "desc_cre_date";
if(@$is_user_only == "") $is_user_only = "false";

//========================================
//作成日の範囲決定
//========================================

//開始
$from_date = add_yyyymm($target_month,1-$month_hani);
$from_date .="00";

//終了
$to_date = $target_month."99";

//========================================
//前へ／次への移動日を決定
//========================================
$next_target_month = add_yyyymm($target_month,$month_hani);
$prev_target_month = add_yyyymm($target_month,-$month_hani);

//========================================
//ソート条件の解析／ソートSQL作成
//========================================

//ソート解析
$sort_item = $sort;
$desc_value = "";
if( substr($sort,0,5) == "desc_" )
{
	$sort_item = substr($sort,5);
	$desc_value = "desc";
}

//ソートSQL
$sql_sort_order = "";
switch($sort_item)
{
	case SORT_CRE_DATE:
		$sql_sort_order = "summary.cre_date $desc_value";
		break;
	case SORT_PROBLEM:
		$sql_sort_order = "summary.problem $desc_value";
		break;
	case SORT_DIAG_DIV:
		$sql_sort_order = "summary.diag_div $desc_value";
		break;
	case SORT_KISAISHA:
		$sql_sort_order = "empmst.emp_lt_nm $desc_value , empmst.emp_ft_nm $desc_value";
		break;
	case SORT_SHOKUSHU:
		$sql_sort_order = "jobmst.job_nm $desc_value";
		break;
	case SORT_PRIORITY:
		$sql_sort_order = "summary.priority $desc_value";
		break;
	default:
		//何もしない。
		break;
}
if($sql_sort_order != "")
{
	$sql_sort_order .= ",";
}
$sql_sort_order .= "summary.summary_id";

//ソートリンク用、リンク先ソート値。
$sort_link_cre_date = ($sort == SORT_CRE_DATE) ? "desc_".SORT_CRE_DATE : SORT_CRE_DATE;
$sort_link_problem  = ($sort == SORT_PROBLEM ) ? "desc_".SORT_PROBLEM  : SORT_PROBLEM ;
$sort_link_diag_div = ($sort == SORT_DIAG_DIV) ? "desc_".SORT_DIAG_DIV : SORT_DIAG_DIV;
$sort_link_kisaisha = ($sort == SORT_KISAISHA) ? "desc_".SORT_KISAISHA : SORT_KISAISHA;
$sort_link_shokushu = ($sort == SORT_SHOKUSHU) ? "desc_".SORT_SHOKUSHU : SORT_SHOKUSHU;
$sort_link_priority = ($sort == SORT_PRIORITY) ? "desc_".SORT_PRIORITY : SORT_PRIORITY;

//========================================
//本人全員判定／絞込みSQL作成
//========================================

$sql_add_where = "";
if($is_user_only == "true")
{
	$sql_add_where = "and summary.emp_id = '$emp_id'";
}

//========================================
//表示対象データの検索
//========================================

$sql = "select summary.*,empmst.emp_lt_nm,empmst.emp_ft_nm,empmst.emp_job,jobmst.job_nm from summary ";
$sql .= "left join empmst on empmst.emp_id = summary.emp_id ";
$sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";
$cond = "where summary.ptif_id = '$pt_id' and summary.cre_date >= '$from_date' and summary.cre_date <= '$to_date' ";
$cond .= $sql_add_where;
$cond .= " order by $sql_sort_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num = pg_numrows($sel);

//echo($cond);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//テンプレート名取得
$arr_tmplname = get_arr_tmplname($con, $fname);

//========================================
//HTML出力
//========================================
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜履歴一覧</title>
<script type="text/javascript">
	var m_target_month = "<?=$target_month?>";
	var m_month_hani = "<?=$month_hani?>";
	var m_sort = "<?=$sort?>";
	var m_is_user_only = "<?=$is_user_only?>";

	function postback() {
		var url='summary_rireki_list.php?session=<?=$session?>&pt_id=<?=$pt_id?>';
		if(m_target_month != "") url = url + '&target_month=' + m_target_month;
		if(m_month_hani != "") url = url + '&month_hani=' + m_month_hani;
		if(m_sort != "") url = url + '&sort=' + m_sort;
		if(m_is_user_only != "") url = url + '&is_user_only=' + m_is_user_only;
		location.href=url;
	}

	// この子画面の現在の高さを求める
	function getCurrentHeight(){
		if (!window.parent) return 0;
		var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
		h = Math.max(h, document.body.scrollHeight);
		h = Math.max(h, 300);
		return (h*1+50);
	}
	// この子画面の最初の高さを親画面(summary_rireki.php)に通知しておく。onloadで呼ばれる。
	// 一旦サイズを小さくしたのち、正しい値を再通知するようにしないといけない。
	function setDefaultHeight(){
		if (!window.parent) return;
		window.parent.setDefaultHeight("hidari", 300);
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.setDefaultHeight("hidari",h);
	}
	// この子画面がリサイズされたら、高さを再測定して親画面(summary_rireki.php)に通知する。
	// 「サマリ内容ポップアップ」が開くときも呼ばれる。onloadでも呼ばれる。
	// 親画面はこの子画面のIFRAMEサイズを変更する。
	// 一旦大きくしたIFRAMEのサイズは通常小さくできない。
	// よって、getCurrentHeightでサイズ測定する前に、画面を一旦元に戻さないといけない。
	function resizeIframeHeight(){
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.resizeIframeHeight("hidari", h);
	}
	// この子画面の高さを、最初の高さに戻す。「サマリ内容ポップアップ」が閉じるとき呼ばれる。
	// 親画面(summary_rireki.php)は、この子画面IFRAMEの高さを最初の値に戻す。
	function resetIframeHeight(){
		if (!window.parent) return;
		window.parent.resetIframeHeight("hidari");
	}
</script>
</head>
<body style="background-color:#f4f4f4" onload="setDefaultHeight();resizeIframeHeight();">

<?
//========================================
//一覧／カレンダー切り替えボタン／本人全員切り替え
//========================================
?>
<div class="search_button_div" style="border-bottom:1px solid #fdfdfd">
<table style="width:100%">
	<tr>
		<td style="text-align:left">
			<input type="button" onclick="location.href='summary_rireki_list.php?session=<?=$session?>&pt_id=<?=$pt_id?>';" value="一覧"/>
			<input type="button" onclick="location.href='summary_rireki_calender.php?session=<?=$session?>&pt_id=<?=$pt_id?>';" value="カレンダー"/>
		</td>
		<td style="text-align:right">
			<input type="button" onclick="m_is_user_only='true';postback();" value="本人"/>
			<input type="button" onclick="m_is_user_only='false';postback();" value="全員"/>
		</td>
	</tr>
</table>
</div>

<?
//========================================
//月切り替えリンク
//========================================
?>
<? $ary_h[$month_hani] = 'background-color:#f8fdb7; border:1px solid #c5d506'; ?>
<div style="background-color:#F4F4F4; padding-top:5px; padding-bottom:5px; border-top:1px solid #d8d8d8; text-align:right">
<table style="width:100%">
	<tr>
		<td width='20%' style="text-align:left; padding:2px">
			<a href="javascript:m_target_month=<?=$prev_target_month?>;postback();">＜前ヶ月</a>
		</td>
		<td width='15%' style="text-align:center; padding:2px;<?=@$ary_h["1"]?>">
			<a href="javascript:m_month_hani=1;postback();">１ヶ月</a>
		</td>
		<td width='15%' style="text-align:center; padding:2px;<?=@$ary_h["2"]?>">
			<a href="javascript:m_month_hani=2;postback();">２ヶ月</a>
		</td>
		<td width='15%' style="text-align:center; padding:2px;<?=@$ary_h["3"]?>">
			<a href="javascript:m_month_hani=3;postback();">３ヶ月</a>
		</td>
		<td width='15%' style="text-align:center; padding:2px;<?=@$ary_h["6"]?>">
			<a href="javascript:m_month_hani=6;postback();">６ヶ月</a>
		</td>
		<td width='20%' style="text-align:right; padding:2px;">
			<a href="javascript:m_target_month=<?=$next_target_month?>;postback();">次ヶ月＞</a>
		</td>
	</tr>
</table>
</div>

<?
//========================================
//一覧
//========================================
?>
<table class="listp_table" cellspacing="1">
	<tr>
		<th style="text-align:center; padding:2px"><a href="javascript:m_sort='<?=$sort_link_cre_date?>';postback();"
			>作成年月日</a></th>
		<th style="text-align:center; padding:2px"><a href="javascript:m_sort='<?=$sort_link_problem?>';postback();"
			>プロブレム</a></th>
		<th style="text-align:center; padding:2px"><a href="javascript:m_sort='<?=$sort_link_diag_div?>';postback();"
			><?=$_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type]?></a></th>
		<th style="text-align:center; padding:2px"><a href="javascript:m_sort='<?=$sort_link_kisaisha?>';postback();"
			>記載者</a></th>
		<th style="text-align:center; padding:2px"><a href="javascript:m_sort='<?=$sort_link_shokushu?>';postback();"
			>職種</a></th>
		<th style="text-align:center; padding:2px"><a href="javascript:m_sort='<?=$sort_link_priority?>';postback();"
			><img src="img/repo-jyuuyou.gif" alt="記事重要度"></a></th>
	</tr>

	<?
	for($i=0;$i<$num;$i++){
		$summary_id = pg_result($sel,$i,"summary_id");
		$diag_div = pg_result($sel,$i,"diag_div");
		$problem = pg_result($sel,$i,"problem");
		$smry_cmt = pg_result($sel,$i,"smry_cmt");
		$sum_emp_id = pg_result($sel,$i,"emp_id");
		$cre_date = pg_result($sel,$i,"cre_date");
		$disp_date = substr($cre_date, 0, 4)."/".substr($cre_date, 4, 2)."/".substr($cre_date, 6, 2);
		$kisaisha_id = pg_result($sel,$i,"emp_id");
		$lt_nm = pg_result($sel,$i,"emp_lt_nm");
		$ft_nm = pg_result($sel,$i,"emp_ft_nm");
		$kisaisha = format_kanji_name($lt_nm,$ft_nm);
		$shokushu_id = pg_result($sel,$i,"emp_job");
		$shokushu = pg_result($sel,$i,"job_nm");
		$priority = pg_result($sel,$i,"priority");

		$problem_id = pg_result($sel,$i,"problem_id");
		$tmpl_id = pg_result($sel,$i,"tmpl_id");

		//プロブレムが未選択でテンプレート使用の場合は、テンプレート名にする
		if ($problem_id == 0 && $tmpl_id != "") {
			$problem = $arr_tmplname[$tmpl_id];
		}

		//HTML用文字変換
		$problem_html = h($problem);
		$diag_div_html = h($diag_div);
		$kisaisha_html = h($kisaisha);
		$shokushu_html = h($shokushu);

		$problem_html_disp = $problem_html;
		if(mb_strlen($problem_html_disp, "EUC-JP")>6) {
			$problem_html_disp = mb_substr($problem_html_disp,0,6, "EUC-JP");
		}
		$diag_div_html_disp = $diag_div_html;
		if(mb_strlen($diag_div_html_disp, "EUC-JP")>6) {
			$diag_div_html_disp = mb_substr($diag_div_html_disp,0,6, "EUC-JP");
		}
		$shokushu_html_disp = $shokushu_html;
		if(mb_strlen($shokushu_html_disp, "EUC-JP")>4) {
			$shokushu_html_disp = mb_substr($shokushu_html_disp,0,4, "EUC-JP");
		}
	?>
	<tr>
		<td align='center'>
			<a href='summary_rireki_naiyo.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&date=<?=$cre_date?>' style="text-decoration:none">			<?=$disp_date?>
			</a>
		</td>
		<td align='center'>
			<a href='summary_rireki_naiyo.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&problem=<?=urlencode($problem)?>&from_date=<?=$from_date?>&to_date=<?=$to_date?>' style="text-decoration:none">
			<?=$problem_html_disp?>
			</a>
		</td>
		<td align='center'>
			<a href='summary_rireki_naiyo.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&diag_div=<?=urlencode($diag_div)?>&from_date=<?=$from_date?>&to_date=<?=$to_date?>' style="text-decoration:none">
			<?=$diag_div_html_disp?>
			</a>
		</td>
		<td align='center'>
			<a href='summary_rireki_naiyo.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&kisaisha_id=<?=$kisaisha_id?>&from_date=<?=$from_date?>&to_date=<?=$to_date?>' style="text-decoration:none">
			<?=$kisaisha_html?>
			</a>
		</td>
		<td align='center'>
			<a href='summary_rireki_naiyo.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&shokushu_id=<?=$shokushu_id?>&from_date=<?=$from_date?>&to_date=<?=$to_date?>' style="text-decoration:none">
			<?=$shokushu_html_disp?>
			</a>
		</td>
		<td align='center'>
			<a href='summary_rireki_naiyo.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&priority=<?=$priority?>&from_date=<?=$from_date?>&to_date=<?=$to_date?>' style="text-decoration:none">
			<?=$priority?>
			</a>
		</td>
	</tr>
	<?
	}
	?>
</table>

</body>
</html>
<? pg_close($con); ?>
