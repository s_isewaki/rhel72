<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("./conf/sql.inf");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 経営支援権限チェック
$auth = check_authority($session,25,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 表示設定値の更新
$sql = "update option set";
$set = array("biz_bed_rate", "biz_bed_avg_rate", "biz_bed_actv_rate", "biz_bed_avg_days", "biz_bed_sml_days");
$setvalue = array($rate, $avg_rate, $active_rate, $avg_days, $simulation);
$cond = "where emp_id = '$emp_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 表示設定画面を再表示
echo("<script language='javascript'>location.href = 'biz_option.php?session=$session';</script>");
?>
