<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("cl_title_name.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;


//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);


if($mode == "REGIST")
{
   // CAS結果通知登録処理
   $obj->delete_cl_result_notice($apply_id);
   // 2011/12/2 Yamagawa upd(s)
   //$result_notice_contents = $obj->create_result_notice_contents_xml($_POST, "3");
   $result_notice_contents = $obj->create_result_notice_contents_xml($_POST, "3", "1");
   // 2011/12/2 Yamagawa upd(e)

   $arr = array("apply_id" => $apply_id, "result_notice_contents" => $result_notice_contents);
   $obj->regist_cl_result_notice($arr);

   $arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
   $apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
   $apply_content = $arr_apply_wkfwmst[0]["apply_content"];

   // 申請状況が否認または差戻し以外
   if(!($apply_stat == "2" && $apply_stat == "3"))
   {
	   // キャリア履歴登録処理
	   $obj->delete_cl_career_history($apply_id);

       $disp_flg = "f";
       if($apply_stat == "1")
       {
           $disp_flg = "t";
       }

       $levelup_apply_date = $obj->get_levelup_apply_date($apply_content);

	   $arr = array(
	                 "apply_id" => $apply_id,
	                 "emp_id" => $levelup_apply_emp_id,
	                 "apply_level" => "3",
	                 "apply_date" => $levelup_apply_date,
	                 "recognize_level" => $_POST["level"],
	                 "recognize_level_div" => $_POST["level_div"],
	                 "recognize_date" => date("Ymd"),
	                 "recognize_flg" => ($_POST["radio_level_recognize"] == "1") ? "t" : "f",
	                 "entry_date" => "",
	                 "career_apply_flg" => "f",
	                 "disp_flg" => $disp_flg,
	                 // 2011/12/2 Yamagawa add(s)
	                 "nurse_type" => "1"
	                 // 2011/12/2 Yamagawa add(e)
	                );

       $obj->regist_cl_career_history($arr);
   }
   echo("<script language=\"javascript\">self.close();</script>\n");
}

// 申請情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$apply_content = $arr_apply_wkfwmst[0]["apply_content"];
$levelup_apply_emp_id = $obj->get_levelup_apply_emp_id($apply_content);

// 志願書情報取得
$levelup_book_id = $obj->get_levelup_application_book_id($apply_content);
$arr_levelup_apply_wkfwmst = $obj->get_apply_wkfwmst($levelup_book_id);
$emp_full_nm = $arr_levelup_apply_wkfwmst[0]["emp_lt_nm"] . " " . $arr_levelup_apply_wkfwmst[0]["emp_ft_nm"];
$class = $arr_levelup_apply_wkfwmst[0]["apply_class_nm"] . " > " . $arr_levelup_apply_wkfwmst[0]["apply_atrb_nm"] . " > " . $arr_levelup_apply_wkfwmst[0]["apply_dept_nm"];
if ($arr_levelup_apply_wkfwmst[0]["apply_room_nm"] != "") {
	$class .= $arr_levelup_apply_wkfwmst[0]["apply_room_nm"];
}

// ＣＡＳ結果通知取得
$arr_cl_result_notice = $obj->get_cl_result_notice($apply_id);
$result_notice_contents = $arr_cl_result_notice[0]["result_notice_contents"];
if($result_notice_contents != "")
{
	$utf_content = mb_convert_encoding($result_notice_contents, "UTF-8", "EUC-JP");
	$utf_content = str_replace("EUC-JP", "UTF-8", $utf_content);
	$doc = domxml_open_mem($utf_content);
	$root = $doc->get_elements_by_tagname('apply');
	$node_array = $root[0]->child_nodes();
}


$cl_title = cl_title_name();
//====================================
// 初期処理
//====================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cl_title?>｜<?=$cl_title?>結果通知</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">

function init_page()
{
<?
foreach ($node_array as $child)
{
    if ($child->node_type() == XML_ELEMENT_NODE)
    {
        $type = $child->get_attribute("type");
        $itemvalue = mb_convert_encoding($child->get_content(), "EUC-JP", "UTF-8");
        if($type == "checkbox")
        {
           if($itemvalue == "t")
           {
               echo("document.getElementById('$child->tagname').checked = true;\n");
           }
        }
        else if($type == "radio")
        {
			echo("for(i=0; i<document.apply.elements['$child->tagname'].length; i++) {\n");
			echo("	if(document.apply.elements['$child->tagname'][i].value == '$itemvalue') {\n");
			echo("    document.apply.elements['$child->tagname'][i].checked = true;\n");
			echo("	}\n");
			echo("}\n");
        }
        else
        {
           echo("document.getElementById('$child->tagname').value = \"".$itemvalue."\";\n");
        }
    }
}
?>

click_radio_level_recognize();

}

function click_radio_level_recognize()
{

    if(document.apply.radio_level_recognize[0].checked)
    {
        document.getElementById('level').disabled = false;
        document.getElementById('level_div').disabled = false;
    }
    else
    {
        document.getElementById('level').disabled = true;
        document.getElementById('level_div').disabled = true;
    }
}

function regist()
{
    document.apply.action="cl_result_notice_register3.php?session=<?=$session?>&mode=REGIST";
    document.apply.target = "";
    document.apply.submit();
}

function print()
{
    var h = '600';
    var w = '600';

    var win_width = (screen.width - w) / 2 + 100;
    var win_height = (screen.height - h) / 2;

    childwin_print = window.open('', 'printFormChild', 'left='+win_width+',top='+win_height+',width='+w+',height='+h+',scrollbars=yes,resizable=yes');
    document.apply.action = "cl_result_notice_print3.php";
    document.apply.target = "printFormChild";
    document.apply.submit();
	childwin.focus();
}

function print_pdf()
{
	window.open("","pdf", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=525,height=750");
	document.apply.action="cl_result_notice_register_pdf.php";
	document.apply.target = "pdf";
	document.apply.submit();
}

</script>
<?
// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
write_yui_calendar_script2(1);
?>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="init_page();initcal();">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="apply_id" value="<?=$apply_id?>">
<input type="hidden" name="levelup_apply_emp_id" value="<?=$levelup_apply_emp_id?>">
<input type="hidden" name="upd" value="<?=$upd?>">
<input type="hidden" name="emp_full_nm" value="<?=$emp_full_nm?>">
<input type="hidden" name="class" value="<?=$class?>">
<input type="hidden" name="charange_level" value="3">
<input type="hidden" name="nurse_type" value="1">
<center>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$cl_title?>結果通知</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="3" alt=""><br>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
<?
if($upd == "t")
{
?>
<input type="button" id="update_btn" value="登録" onclick="regist();">
<?
}
?>
<input type="button" id="update_btn" value="印刷" onclick="print();">
<input type="button" id="pdf_btn" value="ＰＤＦ印刷" onclick="print_pdf();">
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>チャレンジレベルＩＩＩ<b></font></td>
</tr>
<tr>
<td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者&nbsp;：&nbsp;<?=htmlspecialchars($emp_full_nm);?></font></td>
<td width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属&nbsp;：&nbsp;<?=htmlspecialchars($class);?></font></td>
</tr>
<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
あなたのキャリアアップチャレンジは、看護の質向上に貢献するものであり、専門職としての姿<br>
そのものであります。心より経緯を評すとともに結果について報告させていただきます。
</font>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>1.結果</b></font></td>
</tr>
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">達成している項目&nbsp;：&nbsp;優れた能力でさらに磨いて頂きたい能力(チェック)</font></td>
</tr>

<tr>
<td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td>
<td width="300">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_achieved_no2" name="chk_achieved_no2" value="t">No.2&nbsp;&nbsp;
<input type="checkbox" id="chk_achieved_no4" name="chk_achieved_no4" value="t">No.4
</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">教育能力/自己学習能力</font></td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_achieved_no5" name="chk_achieved_no5" value="t">No.5&nbsp;&nbsp;
<input type="checkbox" id="chk_achieved_no7" name="chk_achieved_no7" value="t">No.7&nbsp;&nbsp;
<input type="checkbox" id="chk_achieved_no8" name="chk_achieved_no8" value="t">No.8&nbsp;&nbsp;
<input type="checkbox" id="chk_achieved_no9" name="chk_achieved_no9" value="t">No.9
</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リーダーシップ能力</font></td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_achieved_no11" name="chk_achieved_no11" value="t">No.11&nbsp;&nbsp;
</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経験年数/経験領域</font></td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_achieved_no12" name="chk_achieved_no12" value="t">No.12&nbsp;&nbsp;
</font>
</td>
</tr>

<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">達成してない項目&nbsp;：&nbsp;次のステップアップの課題として磨いて頂きたい能力(チェック)</font></td>
</tr>

<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_unachieved_no2" name="chk_unachieved_no2" value="t">No.2&nbsp;&nbsp;
<input type="checkbox" id="chk_unachieved_no4" name="chk_unachieved_no4" value="t">No.4
</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">教育能力/自己学習能力</font></td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_unachieved_no5" name="chk_unachieved_no5" value="t">No.5&nbsp;&nbsp;
<input type="checkbox" id="chk_unachieved_no7" name="chk_unachieved_no7" value="t">No.7&nbsp;&nbsp;
<input type="checkbox" id="chk_unachieved_no8" name="chk_unachieved_no8" value="t">No.8&nbsp;&nbsp;
<input type="checkbox" id="chk_unachieved_no9" name="chk_unachieved_no9" value="t">No.9
</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リーダーシップ能力</font></td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_unachieved_no11" name="chk_unachieved_no11" value="t">No.11&nbsp;&nbsp;
</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経験年数/経験領域</font></td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:
<input type="checkbox" id="chk_unachieved_no12" name="chk_unachieved_no12" value="t">No.12&nbsp;&nbsp;
</font>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>2.認定レベル/コメント(該当のところにチェック)</b></font></td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="radio_level_recognize" value="1" onclick="click_radio_level_recognize();" checked>レベル認定可
<input type="radio" name="radio_level_recognize" value="2" onclick="click_radio_level_recognize();">レベル認定不可
</font>
</td>
</tr>

<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">レベル
<select id="level" name="level" style="width:40pt">
<option value="2">ＩＩ
<option value="3" selected>ＩＩＩ
</select>
</font>
&nbsp;&nbsp;
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">区分
<select id="level_div" name="level_div" style="width:40pt">
<option value="A">A
<option value="B">B
<option value="C">C
<option value="D">D
<option value="E">E
<option value="F">F
<option value="G">G
</select>
</font>
</td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="chk_2_1" name="chk_2_1" value="t">半年が経過した後、目標達成していない項目のみ再チャレンジする。
</font>
</td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="chk_2_2" name="chk_2_2" value="t">1年が経過した後、全ての項目を再チャレンジする。
</font>
</td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="text" id="chk_2_3_text" name="chk_2_3_text" value="" size="100">
</font>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>3.認定結果についての疑問のある場合</b></font></td>
</tr>

<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
疑問の生じた評価項目とその理由を文書にし、評価用紙一式と共に看護管理部(認定委員会)<br>
へ提出して下さい。
</font>
</td>
</tr>

<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出期限：
<select name="date_y1" id="date_y1"><? show_select_years_for_cas(5, $date_y1, true)?></select>年
<select name="date_m1" id="date_m1"><? show_select_months($date_m1 ,true); ?></select>月
<select name="date_d1" id="date_d1"><? show_select_days($date_d1 ,true); ?></select>日
</font>
<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td>
</tr>
</table>



<center>
</td>
</tr>
</table>
</body>
</form>
</html>
<?
pg_close($con);
?>
