<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("kango_common.ini");


//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_KANRI, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	if($postback_mode == "save")
	{
		//==============================
		//送信データ解析
		//==============================
		$param_key_list = array('common_condition', 'form_1a_point', 'form_1a_sign', 'form_1b_point', 'form_1b_sign', 'form_2a_point', 'form_2a_sign', 'form_2b_point', 'form_2b_sign', 'form_3a_point', 'form_3a_sign', 'form_3b_point', 'form_3b_sign');

		$data_array = null;
		for($pt_class_level = 5;$pt_class_level >= 2;$pt_class_level--)
		{
			$data_array1 = null;
			$data_array1['pt_class_level'] = $pt_class_level;
			foreach($param_key_list as $param_key)
			{
				$param_name  = "lv{$pt_class_level}_{$param_key}";
				$param_value = $$param_name;
				$data_array1[$param_key] = $param_value;
			}

			if($data_array1['form_1a_point'] == "" || $data_array1['form_1a_sign'] == "")
			{
				$data_array1['form_1a_point'] = "";
				$data_array1['form_1a_sign'] = "";
			}
			if($data_array1['form_1b_point'] == "" || $data_array1['form_1b_sign'] == "")
			{
				$data_array1['form_1b_point'] = "";
				$data_array1['form_1b_sign'] = "";
			}
			if($data_array1['form_2a_point'] == "" || $data_array1['form_2a_sign'] == "")
			{
				$data_array1['form_2a_point'] = "";
				$data_array1['form_2a_sign'] = "";
			}
			if($data_array1['form_2b_point'] == "" || $data_array1['form_2b_sign'] == "")
			{
				$data_array1['form_2b_point'] = "";
				$data_array1['form_2b_sign'] = "";
			}
			if($data_array1['form_3a_point'] == "" || $data_array1['form_3a_sign'] == "")
			{
				$data_array1['form_3a_point'] = "";
				$data_array1['form_3a_sign'] = "";
			}
			if($data_array1['form_3b_point'] == "" || $data_array1['form_3b_sign'] == "")
			{
				$data_array1['form_3b_point'] = "";
				$data_array1['form_3b_sign'] = "";
			}

			$data_array[] = $data_array1;
		}

		//==============================
		//トランザクション開始
		//==============================
		pg_query($con,"begin transaction");

		//==============================
		//DB更新
		//==============================
		set_pt_class_level_setting($con, $fname, $bldg_cd, $ward_cd, $data_array);

		//==============================
		//更新後の設定情報を取得
		//==============================
		$pt_class_level_setting_after_update = get_pt_class_level_setting($con, $fname, $bldg_cd, $ward_cd);

		//==============================
		//コミット
		//==============================
		pg_query($con, "commit");
	}
}


//==============================
//初期アクセス時の処理
//==============================
else
{
	//==============================
	//初期表示する病棟を特定
	//==============================
	$top_ward = get_top_ward($con,$fname);
	$bldg_cd = $top_ward['bldg_cd'];
	$ward_cd = $top_ward['ward_cd'];
}



//==============================
//病棟運用設定を取得
//==============================
$pt_class_level_setting = get_pt_class_level_setting($con, $fname, $bldg_cd, $ward_cd);


//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "患者分類ルール";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

//画面起動時の処理
function initPage()
{
	//病棟選択初期化
	init_ward_select();

	//共通条件を更新
	common_condition_disp(5);
	common_condition_disp(4);
	common_condition_disp(3);
	common_condition_disp(2);
}

//病棟変更時の処理
function ward_changed(bldg_cd,ward_cd)
{
	document.mainform.postback_mode.value = "ward_changed";
	document.mainform.submit();
}

//更新ボタン押下時の処理
function save()
{
	//入力チェック
	var ab_list = new Array("a","b");
	for(var l=5;l>=2;l--)
	{
		for(var f=1;f<=3;f++)
		{
			for(var ab=0;ab<=1;ab++)
			{
				var target = "lv" + l + "_form_" + f + ab_list[ab] + "_";
				var point = document.getElementById(target + "point").value;
				var sign = document.getElementById(target + "sign").value;

				if(point != "")
				{
					//数値チェック
					if(!point.match(/^[0-9].*$/))
					{
						alert("点数は数値で入力してください。");
						return;
					}
					//符号チェック
					if(sign == "")
					{
						alert("点数に対する扱い(以上、以下等)を選択してください。");
						return;
					}
				}

			}
		}
	}

	document.mainform.postback_mode.value = "save";
	document.mainform.submit();
}

//指定レベルに対する共通条件の表示を更新します。
function common_condition_disp(level)
{
	var c_cond = document.getElementById("lv" + level + "_common_condition");
	var f1cond = document.getElementById("lv" + level + "_form1_condition");
	var f2cond = document.getElementById("lv" + level + "_form2_condition");
	var f3cond = document.getElementById("lv" + level + "_form3_condition");

	var t = c_cond.options[c_cond.selectedIndex].text;
	f1cond.innerHTML = t;
	f2cond.innerHTML = t;
	f3cond.innerHTML = t;
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_kango_header($session,$fname,true)
?>
<!-- ヘッダー END -->

<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">



<!-- 病棟選択・更新ボタン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<?
show_ward_select($con,$fname,$bldg_cd,$ward_cd);
?>
</td>
<td align="right">
<input type="button" value="更新" onclick="save()">
</td>
</tr>
</table>
<!-- 病棟選択・更新ボタン END -->



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>







<!-- レベル設定一覧 START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">

<tr>
<td bgcolor='#bdd1e7' align="center" rowspan="2"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>分類</font></td>
<td bgcolor='#bdd1e7' align="center" rowspan="2"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>共通条件</font></td>
<td bgcolor='#bdd1e7' align="center" colspan="3"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票1</font></td>
<td bgcolor='#bdd1e7' align="center" rowspan="2"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>&nbsp;</font></td>
<td bgcolor='#bdd1e7' align="center" colspan="3"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票2</font></td>
<td bgcolor='#bdd1e7' align="center" rowspan="2"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>&nbsp;</font></td>
<td bgcolor='#bdd1e7' align="center" colspan="3"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票3</font></td>
</tr>

<tr>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>A</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>&nbsp;</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>B</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>A</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>&nbsp;</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>B</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>A</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>&nbsp;</font></td>
<td bgcolor='#bdd1e7' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>B</font></td>
</tr>



<?
$sign_list = null;
$sign_list[1] = '以上';
$sign_list[2] = '以下';
$sign_list[3] = 'のみ';
$sign_list[4] = '以外';

foreach($pt_class_level_setting as $pt_class_level_setting1)
{
	$pt_class_level = $pt_class_level_setting1['pt_class_level'];
	$common_condition = $pt_class_level_setting1['common_condition'];
	$form_1a_point = $pt_class_level_setting1['form_1a_point'];
	$form_1a_sign = $pt_class_level_setting1['form_1a_sign'];
	$form_1b_point = $pt_class_level_setting1['form_1b_point'];
	$form_1b_sign = $pt_class_level_setting1['form_1b_sign'];
	$form_2a_point = $pt_class_level_setting1['form_2a_point'];
	$form_2a_sign = $pt_class_level_setting1['form_2a_sign'];
	$form_2b_point = $pt_class_level_setting1['form_2b_point'];
	$form_2b_sign = $pt_class_level_setting1['form_2b_sign'];
	$form_3a_point = $pt_class_level_setting1['form_3a_point'];
	$form_3a_sign = $pt_class_level_setting1['form_3a_sign'];
	$form_3b_point = $pt_class_level_setting1['form_3b_point'];
	$form_3b_sign = $pt_class_level_setting1['form_3b_sign'];

	?>
	<tr>
	<td align="center" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>レベル<?=$pt_class_level?></font></td>
	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<select id="lv<?=$pt_class_level?>_common_condition" name="lv<?=$pt_class_level?>_common_condition" onchange="common_condition_disp(<?=$pt_class_level?>);">
		<option value="0" <?if($common_condition == 0){echo("selected");}?>>又は</option>
		<option value="1" <?if($common_condition == 1){echo("selected");}?>>かつ</option>
		</select>
		</font>
		</td>

	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="lv<?=$pt_class_level?>_form_1a_point" name="lv<?=$pt_class_level?>_form_1a_point" value="<?=$form_1a_point?>" maxlength="2" style="width:25px">点
		<select id="lv<?=$pt_class_level?>_form_1a_sign" name="lv<?=$pt_class_level?>_form_1a_sign">
		<option value=""></option>
		<?
		foreach($sign_list as $sign_value => $sign_text)
		{
		?>
		<option value="<?=$sign_value?>" <?if($form_1a_sign == $sign_value){echo("selected");}?>><?=$sign_text?></option>
		<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<span id="lv<?=$pt_class_level?>_form1_condition">又は</span>
		</font>
		</td>
	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="lv<?=$pt_class_level?>_form_1b_point" name="lv<?=$pt_class_level?>_form_1b_point" value="<?=$form_1b_point?>" maxlength="2" style="width:25px">点
		<select id="lv<?=$pt_class_level?>_form_1b_sign" name="lv<?=$pt_class_level?>_form_1b_sign">
		<option value=""></option>
		<?
		foreach($sign_list as $sign_value => $sign_text)
		{
		?>
		<option value="<?=$sign_value?>" <?if($form_1b_sign == $sign_value){echo("selected");}?>><?=$sign_text?></option>
		<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>かつ</font></td>

	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="lv<?=$pt_class_level?>_form_2a_point" name="lv<?=$pt_class_level?>_form_2a_point" value="<?=$form_2a_point?>" maxlength="2" style="width:25px">点
		<select id="lv<?=$pt_class_level?>_form_2a_sign" name="lv<?=$pt_class_level?>_form_2a_sign">
		<option value=""></option>
		<?
		foreach($sign_list as $sign_value => $sign_text)
		{
		?>
		<option value="<?=$sign_value?>" <?if($form_2a_sign == $sign_value){echo("selected");}?>><?=$sign_text?></option>
		<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<span id="lv<?=$pt_class_level?>_form2_condition">又は</span>
		</font>
		</td>
	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="lv<?=$pt_class_level?>_form_2b_point" name="lv<?=$pt_class_level?>_form_2b_point" value="<?=$form_2b_point?>" maxlength="2" style="width:25px">点
		<select id="lv<?=$pt_class_level?>_form_2b_sign" name="lv<?=$pt_class_level?>_form_2b_sign">
		<option value=""></option>
		<?
		foreach($sign_list as $sign_value => $sign_text)
		{
		?>
		<option value="<?=$sign_value?>" <?if($form_2b_sign == $sign_value){echo("selected");}?>><?=$sign_text?></option>
		<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>かつ</font></td>

	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="lv<?=$pt_class_level?>_form_3a_point" name="lv<?=$pt_class_level?>_form_3a_point" value="<?=$form_3a_point?>" maxlength="2" style="width:25px">点
		<select id="lv<?=$pt_class_level?>_form_3a_sign" name="lv<?=$pt_class_level?>_form_3a_sign">
		<option value=""></option>
		<?
		foreach($sign_list as $sign_value => $sign_text)
		{
		?>
		<option value="<?=$sign_value?>" <?if($form_3a_sign == $sign_value){echo("selected");}?>><?=$sign_text?></option>
		<?
		}
		?>
		</select>
		</font>
		</td>
	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<span id="lv<?=$pt_class_level?>_form3_condition">又は</span>
		</font>
		</td>
	<td align="center" nowrap>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="lv<?=$pt_class_level?>_form_3b_point" name="lv<?=$pt_class_level?>_form_3b_point" value="<?=$form_3b_point?>" maxlength="2" style="width:25px">点
		<select id="lv<?=$pt_class_level?>_form_3b_sign" name="lv<?=$pt_class_level?>_form_3b_sign">
		<option value=""></option>
		<?
		foreach($sign_list as $sign_value => $sign_text)
		{
		?>
		<option value="<?=$sign_value?>" <?if($form_3b_sign == $sign_value){echo("selected");}?>><?=$sign_text?></option>
		<?
		}
		?>
		</select>
		</font>
		</td>
	</tr>
	<?
}
?>


<tr>
<td align="center" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>レベル1</font></td>
<td colspan="12"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>上記以外</font></td>
</tr>

</table>
<!-- レベル設定一覧 END -->










</form>
<!-- 全体 END -->

</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
?>
