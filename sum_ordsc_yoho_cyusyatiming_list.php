<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}


$mode = @$_REQUEST["mode"];
$code = @$_REQUEST["code"];
$is_disabled = @$_REQUEST["is_disabled"];
$caption = @$_REQUEST["caption"];


//==========================================================================
// 更新
//==========================================================================
if (@$_REQUEST["mode"] == "update"){
	$sql  = " update sum_med_yoho_cyusyatiming_mst set";
	$sql .= " caption = '" . pg_escape_string($caption)."'";
	$sql .= ",is_disabled = " . (int)$is_disabled;
	$sql .= " where code = ".(int)$code;
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	header("Location: sum_ordsc_yoho_cyusyatiming_list.php?session=".$session);
	die;
}

//==========================================================================
// 削除
//==========================================================================
if (@$_REQUEST["mode"] == "delete"){
	$sql =
		" delete from sum_med_yoho_cyusyatiming_mst".
		" where code = ".(int)$code;
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	header("Location: sum_ordsc_yoho_cyusyatiming_list.php?session=".$session);
	die;
}

//==========================================================================
// 表示順変更
//==========================================================================
if (@$_REQUEST["mode"] == "up" || @$_REQUEST["mode"] == "down"){
	$sql =
		" select sort_order from sum_med_yoho_cyusyatiming_mst".
		" where code = ".(int)$code;
	$sel = select_from_table($con, $sql, "", $fname);
	$cur_sort_order = pg_fetch_result($sel,0,0);

	$sql = " select * from sum_med_yoho_cyusyatiming_mst";
	if ($_REQUEST["mode"] == "up") $sql .= " where sort_order < ".$cur_sort_order ." order by sort_order desc limit 1";
	if ($_REQUEST["mode"] == "down") $sql .= " where sort_order > ".$cur_sort_order ." order by sort_order limit 1";
	$sel = select_from_table($con, $sql, "", $fname);
	$swap_row = pg_fetch_array($sel);
	if ($swap_row["sort_order"]!=""){

		$sql  = " update sum_med_yoho_cyusyatiming_mst set";
		$sql .= " sort_order = " . $swap_row["sort_order"];
		$sql .= " where code = ".(int)$code;
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		$sql  = " update sum_med_yoho_cyusyatiming_mst set";
		$sql .= " sort_order = " . $cur_sort_order;
		$sql .= " where code = ".(int)$swap_row["code"];
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		header("Location: sum_ordsc_yoho_cyusyatiming_list.php?session=".$session);
		die;
	}
}



//==========================================================================
// 以下、通常処理
//==========================================================================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';


// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | 処方・注射オーダ</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td { border:#5279a5 solid 1px; text-align:center; white-space:nowrap }
.list1 th {  border:#5279a5 solid 1px; text-align:center; font-weight:normal; white-space:nowrap; background-color:#e3ecf4 }
.subtitle { font-weight:bold; margin-top:10px }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session,$fname,"KANRI_ORDSC"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<?//==========================================================================?>
<?// 処方・注射メニュー                                                       ?>
<?//==========================================================================?>
<?= summary_common_show_ordsc_right_menu($session, "注射タイミング")?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?//==========================================================================?>
<?// サブメニュー                                                             ?>
<?//==========================================================================?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
	<tr height="22">
		<td width="100" align="center" bgcolor="#958f28">
			<a href="sum_ordsc_yoho_cyusyatiming_list.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧/編集</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_yoho_cyusyatiming_regist.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>新規登録</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td></td>
	</tr>
</table>


<?//==========================================================================?>
<?// 一覧                                                                     ?>
<?//==========================================================================?>


<form action="sum_ordsc_yoho_cyusyatiming_list.php" method="get" name="frm">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="regist">
	<input type="hidden" name="code" value="">
	<input type="hidden" name="is_disabled" value="">
	<input type="hidden" name="caption" value="">
</form>

<script type="text/javascript">
	function registData(mode, code){
		document.frm.mode.value = mode;
		document.frm.code.value = code;
		document.frm.caption.value = document.getElementById("txt_"+code).value;
		document.frm.is_disabled.value =  document.getElementById("chk_"+code).checked ? "0" : "1";
		if (document.frm.caption.value==""){
			alert("注射タイミングを指定してください。");
			return;
		}
		document.frm.submit();
	}
</script>


<table border="0" cellspacing="0" cellpadding="2" class="list1" style="margin-top:10px">
	<? $sel = select_from_table($con, "select * from sum_med_yoho_cyusyatiming_mst order by sort_order", "", $fname); ?>
	<? while ($row = pg_fetch_array($sel)){ ?>
	<tr>
		<th><?=$font?><?=$row["code"]?></font></th>
		<td><?=$font?>
			<input type="text" style="width:200px" name="txt_<?=$row["code"]?>" id="txt_<?=$row["code"]?>" value="<?=h($row["caption"])?>">
			<input type="button" value="▲" onclick="registData('up','<?=$row["code"]?>')" />
			<input type="button" value="▼" onclick="registData('down','<?=$row["code"]?>')" />
			<label style="padding-left:10px"><input type="checkbox" value="1" id="chk_<?=$row["code"]?>" name="chk_<?=$row["code"]?>" <?=$row["is_disabled"]?"":"checked"?> />表示</label>
			<input type="button" value="更新" onclick="registData('update','<?=$row["code"]?>')" />
			<input style="margin-left:10px" type="button" value="削除" onclick="if(!confirm('削除してよろしいですか？')) return; registData('delete','<?=$row["code"]?>')" />
		</font></td>
	</tr>
	<? } ?>
</table>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
