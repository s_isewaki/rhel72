<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("show_select_values.ini");
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("label_by_profile_type.ini");


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 59, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

//画面情報
$page_title = "追加病名登録";
$submit_name = "登録";
if($mode == "update")
{
	$page_title = "追加病名更新";
	$submit_name = "更新";
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ポストバック時の処理
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($is_postback == "true")
{
	//==============================
	//検索情報の収集
	//==============================
	$index_info = null;
	if($index_count == 0)
	{
		$index_info[0]['index_str'] = "";
		$index_info[0]['synonym_flg'] = "";
	}
	else
	{
		for($i=0;$i<$index_count;$i++)
		{
			$p1 = "index_str_$i";
			$p2 = "synonym_flg_$i";
			$index_info[$i]['index_str'] = $$p1;
			$index_info[$i]['synonym_flg'] = $$p2;
		}
	}

	//==============================
	//保存
	//==============================
	if($postback_mode == "save")
	{
		//==============================
		//入力チェック
		//==============================
		$err_msg = "";

		//病名:必須チェック
		if(trim($byoumei) == "")
		{
			$err_msg = "病名を入力してください。";
		}


		if($err_msg != "")
		{
			//==============================
			//エラー表示
			//==============================
			echo("<script language='javascript'>");
			echo("alert('$err_msg');");
			echo("</script>");
		}
		else
		{
			//==============================
			//登録処理
			//==============================
			if($mode == "insert")
			{
				// トランザクションの開始
				pg_query($con, "begin");

				//org_idの採番
				$sql = "select case when max(org_id) isnull then 1 else max(org_id)+1 end as new_org_id from org_byoumei";
				$sel = select_from_table($con, $sql, "", $fname);
				if ($sel == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$org_id = pg_fetch_result($sel,0,'new_org_id');

				//検索系文字変換
				$byoumei_kana_2 = mb_convert_kana($byoumei_kana,"rnC");//r:全角英字→半角,n:全角数字→半角,C:全角ひらがな→全角カタカナ
				$byoumei_kana_2 = strtoupper($byoumei_kana_2);//小文字→大文字

				//org_byoumeiテーブルへの登録
				$sql = "insert into org_byoumei (org_id, byoumei, byoumei_kana, icd10, byoumei_code, rece_code, byoumei_id) values (";
				$content = array(
					pg_escape_string($org_id),
					pg_escape_string($byoumei),
					pg_escape_string($byoumei_kana_2),
					pg_escape_string($icd10),
					pg_escape_string($byoumei_code),
					pg_escape_string($rece_code),
					pg_escape_string($byoumei_id)
					);
				$in = insert_into_table($con,$sql,$content,$fname);
				if($in == 0){
					pg_query($con,"rollback");
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}

				//org_byoumei_indexテーブルへの登録
				foreach($index_info as $i => $index_info1)
				{
					$index_str   = $index_info1['index_str'];
					if(trim($index_str) == "")
					{
						continue;
					}
					$synonym_flg = $index_info1['synonym_flg'];
					if($synonym_flg != "t")
					{
						$synonym_flg = "f";
					}

					//検索系文字変換
					$index_str_2 = mb_convert_kana($index_str,"rnC");//r:全角英字→半角,n:全角数字→半角,C:全角ひらがな→全角カタカナ
					$index_str_2 = strtoupper($index_str_2);//小文字→大文字

					$sql = "insert into org_byoumei_index (org_id, disp_index, synonym_flg, index_str) values (";
					$content = array(
						pg_escape_string($org_id),
						pg_escape_string($i),
						pg_escape_string($synonym_flg),
						pg_escape_string($index_str_2)
						);
					$in = insert_into_table($con,$sql,$content,$fname);
					if($in == 0){
						pg_query($con,"rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}

				// トランザクションのコミット
				pg_query($con, "commit");
			}
			//==============================
			//更新処理
			//==============================
			if($mode == "update")
			{
				// トランザクションの開始
				pg_query($con, "begin");

				//検索系文字変換
				$byoumei_kana_2 = mb_convert_kana($byoumei_kana,"rnC");//r:全角英字→半角,n:全角数字→半角,C:全角ひらがな→全角カタカナ
				$byoumei_kana_2 = strtoupper($byoumei_kana_2);//小文字→大文字

				//org_byoumeiテーブル更新
				$sql = "update org_byoumei set";
				$set = array("byoumei", "byoumei_kana", "icd10", "byoumei_code", "rece_code", "byoumei_id");
				$setvalue = array(
					pg_escape_string($byoumei),
					pg_escape_string($byoumei_kana),
					pg_escape_string($icd10),
					pg_escape_string($byoumei_code),
					pg_escape_string($rece_code),
					pg_escape_string($byoumei_id)
					);
				$cond = "where org_id = '$org_id'";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con,"rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				//org_byoumei_indexテーブル更新(delete)
				$sql = "delete from org_byoumei_index where org_id = '$org_id'";
				$del = delete_from_table($con, $sql, "", $fname);
				if ($del == 0)
				{
					pg_query($con,"rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				//org_byoumei_indexテーブル更新(insert)
				foreach($index_info as $i => $index_info1)
				{
					$index_str   = $index_info1['index_str'];
					if(trim($index_str) == "")
					{
						continue;
					}
					$synonym_flg = $index_info1['synonym_flg'];
					if($synonym_flg != "t")
					{
						$synonym_flg = "f";
					}

					//検索系文字変換
					$index_str_2 = mb_convert_kana($index_str,"rnC");//r:全角英字→半角,n:全角数字→半角,C:全角ひらがな→全角カタカナ
					$index_str_2 = strtoupper($index_str_2);//小文字→大文字

					$sql = "insert into org_byoumei_index (org_id, disp_index, synonym_flg, index_str) values (";
					$content = array(
						pg_escape_string($org_id),
						pg_escape_string($i),
						pg_escape_string($synonym_flg),
						pg_escape_string($index_str_2)
						);
					$in = insert_into_table($con,$sql,$content,$fname);
					if($in == 0){
						pg_query($con,"rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}

				// トランザクションのコミット
				pg_query($con, "commit");

			}
			//==============================
			//画面終了
			//==============================
			echo("<script language='javascript'>");
			echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
			echo("window.close();");
			echo("</script>");
			exit;
		}
	}

	//==============================
	//検索文字列追加
	//==============================
	if($postback_mode == "add_index")
	{
		$index_info_tmp = null;
		foreach($index_info as $i => $index_info1)
		{
			$index_info_tmp[] = $index_info1;
			if($i == $target_index)
			{
				$t = null;
				$t['index_str'] = "";
				$t['synonym_flg'] = "";
				$index_info_tmp[] = $t;
			}
		}
		$index_info = $index_info_tmp;
	}

	//==============================
	//検索文字列削除
	//==============================
	if($postback_mode == "delete_index")
	{
		$index_info_tmp = null;
		foreach($index_info as $i => $index_info1)
		{
			if($i != $target_index)
			{
				$index_info_tmp[] = $index_info1;
			}
		}
		$index_info = $index_info_tmp;
	}
}




//==============================
//初期表示時の画面データ
//==============================
if($is_postback != "true")
{
	//登録画面、初期表示
	if($mode != "update")
	{
		$byoumei = "";
		$byoumei_kana = "";
		$icd10 = "";
		$byoumei_code = "";
		$rece_code = "";
		$byoumei_id = "";
		$index_info = null;
		$index_info[0]['index_str'] = "";
		$index_info[0]['synonym_flg'] = "";
	}
	//更新画面、初期表示
	else
	{
		$sql = "select * from org_byoumei where org_id = $org_id";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$byoumei = pg_fetch_result($sel,0,'byoumei');
		$byoumei_kana = pg_fetch_result($sel,0,'byoumei_kana');
		$icd10 = pg_fetch_result($sel,0,'icd10');
		$byoumei_code = pg_fetch_result($sel,0,'byoumei_code');
		$rece_code = pg_fetch_result($sel,0,'rece_code');
		$byoumei_id = pg_fetch_result($sel,0,'byoumei_id');

		$sql = "select index_str,synonym_flg from org_byoumei_index where org_id = $org_id order by disp_index";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$index_info = pg_fetch_all($sel);
		if($index_info == "")
		{
			$index_info = null;
			$index_info[0]['index_str'] = "";
			$index_info[0]['synonym_flg'] = "";
		}
	}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HTML出力
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<title><?=$med_report_title?> | <?=$page_title?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function save()
{
	document.mainform.postback_mode.value = "save";
	document.mainform.submit();
}

function add_index(index)
{
	document.mainform.postback_mode.value = "add_index";
	document.mainform.target_index.value = index;
	document.mainform.submit();
}

function delete_index(index)
{
	document.mainform.postback_mode.value = "delete_index";
	document.mainform.target_index.value = index;
	document.mainform.submit();
}



</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#5279a5 solid 1px;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="mainform" action="summary_org_byoumei_input.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="<?=$mode?>">
<input type="hidden" name="org_id" value="<?=$org_id?>">

<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="target_index" value="">


<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></td>
<td><input type="text" name="byoumei" value="<?=$byoumei?>" maxlength="60" style="width:450px;ime-mode:active;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名カナ</font></td>
<td><input type="text" name="byoumei_kana" value="<?=$byoumei_kana?>" maxlength="100" style="width:450px;ime-mode:active;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICD10</font></td>
<td><input type="text" name="icd10" value="<?=$icd10?>" maxlength="5" style="width:50px;ime-mode:inactive;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変換用コード</font></td>
<td><input type="text" name="byoumei_code" value="<?=$byoumei_code?>" maxlength="4" style="width:40px;ime-mode:inactive;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">傷病名コード</font></td>
<td><input type="text" name="rece_code" value="<?=$rece_code?>" maxlength="33" style="width:250px;ime-mode:inactive;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理番号</font></td>
<td><input type="text" name="byoumei_id" value="<?=$byoumei_id?>" maxlength="8" style="width:80px;ime-mode:inactive;"></td>
</tr>

</table>



<img src="img/spacer.gif" alt="" width="1" height="5"><br>







<input type="hidden" name="index_count" value="<?=count($index_info)?>">
<table width="550" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索用文字列</font></td>
<td bgcolor="#f6f9ff" width="50"  align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">同義語</font></td>
<td bgcolor="#f6f9ff" width="110" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
</tr>

<?
foreach($index_info as $i => $index_info1)
{
	$index_str = $index_info1['index_str'];
	$synonym_flg = $index_info1['synonym_flg'];
?>
<tr height="22">
<td align="center"><input type="text" name="index_str_<?=$i?>" value="<?=$index_str?>" maxlength="100" style="width:100%;ime-mode:active;"></td>
<td align="center"><input type="checkbox" name="synonym_flg_<?=$i?>" value="t" <?if($synonym_flg == "t"){?>checked<?}?> ></td>
<td align="left" nowrap>



	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="non_in_list">
	<tr>
	<td width="50%" align="center">
<?
if($i == count($index_info) - 1)
{
?>
		<input type="button" value="追加" style="width:40px;" onclick="add_index(<?=$i?>);">
<?
}
?>
	</td>
	<td width="50%" align="center">
<?
if(count($index_info) != 1)
{
?>
		<input type="button" value="削除" style="width:40px;" onclick="delete_index(<?=$i?>);">
<?
}
?>
	</td>
	</tr>
	</table>


</td>
</tr>
<?
}
?>

</table>



<img src="img/spacer.gif" alt="" width="1" height="5"><br>









<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
※病名カナ、及び検索用文字列は、保存時に全角カタカナ(英数字は半角大文字)に変換されます。
</font>
</td>
<td height="22" align="right"><input type="button" value="<?=$submit_name?>" onclick="save();"></td>
</tr>
</table>

</form>
</body>
</html>
<? pg_close($con); ?>
