<?php
/*
 * GETでsession, 開始日(Ymd), 終了日（Ymd）
 * カテゴリID、施設・設備ID
 * を渡している。
 * 
 */
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require("facility_common.ini");
/*
 * fclArrayクラス
 * @auther murakami
 * @version $Revision: 1.00 $
 */
class fclArray {
    // 各要素が入る多次元連想配列
    var $list = array();
    // SQL文（前半）が入る
    var $sql;
    // SQL文（後半）が入る
    var $cond;
/*
 * SQL文から配列を作る
 * 
 * CSV出力用の配列を作成する。
 * $typeは、配列の最後に入れる文字列の変数
 * タイトル、日、開始時間、終了時間、設備カテゴリ、設備名、タイプ
 */
    function makeArray($con, $fname) {
        $sel = select_from_table($con, $this->sql, $this->cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // 配列に格納
        while ($row = pg_fetch_array($sel)){
            $this->list[] = array(
                "fclcate_name"      => $row["fclcate_name"],
                "facility_name"     => $row["facility_name"],
                "date"              => $row["date"],
                "start_time"        => $row["start_time"],
                "end_time"          => $row["end_time"],
                "title"             => $row["title"],
                "content"           => $row["content"],
				"users"             => $row["users"],
                "emp_nm"            => $row["emp_lt_nm"].$row["emp_ft_nm"]
            );
        }
    }
}


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session === "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth === "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
}

// データベースに接続
$con = connect2db($fname);
// 職員IDを取得する。
$sql = "SELECT empmst.emp_id FROM empmst";
$cond = " JOIN session ON empmst.emp_id = session.emp_id WHERE session.session_id = '$session'";

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 職員ID取得ここまで
// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

$cateids = array();
$cateids_tmp = array();

foreach ($categories as $tmp) {
    foreach ($tmp['facilities'] as $tmp2) {
        $cateids_tmp['category_id'] = $tmp2['category_id'];
        $cateids_tmp['facility_id'] = $tmp2['facility_id'];
        array_push($cateids, $cateids_tmp);
    }
}

$list = split("-",$_GET["range"]);

$where = "WHERE r.reservation_del_flg = false AND f.fclcate_id = c.fclcate_id AND(";
foreach ($cateids as $tmp) {
    if((($tmp['category_id'] === $list[0] && $list[1] === null) || ($tmp['category_id'] === $list[0] && $tmp['facility_id'] === $list[1])) || $list[0] === "all"){
        $tmp['category_id']=pg_escape_string($tmp['category_id']);
        $tmp['facility_id']=pg_escape_string($tmp['facility_id']);
        $where .= "(r.fclcate_id = {$tmp['category_id']} AND r.facility_id = {$tmp['facility_id']})";
        $where .= " OR ";
    }
}
$where .= "1 = 0)";
// 各列の名前
$fields=array(
    "fclcate_name",
    "facility_name",
    "date",
    "start_time",
    "end_time",
    "title",
    "content",
    "users",
    "emp_nm"
);

// 表示範囲の指定
$start_date=pg_escape_string($start_date);
$end_date=pg_escape_string($end_date);
//$between = " AND (r.date >= ".$start_date." AND r.date <=".$end_date;
if($start_date === $end_date){
    $between = "AND r.date = '".$start_date."'";
}
else{
    $between = " AND r.date BETWEEN '".$start_date."' AND '".$end_date."'";
}
// CSVに出力する要素を格納する
$csv_items = array();

// 設備予約の配列を取得
$fcl = new fclArray();
$fcl->sql = get_reservation( $emp_id, $where, $between );
var_dump($fcl->sql);
$fcl->makeArray($con, $fname);
$csv_items=array_merge_recursive($fcl->list,$csv_items);

// 実際に出力する文字列
$csv = "カテゴリ名,施設・設備名,日付,開始時間,終了時間,タイトル,内容,利用人数,登録者\r\n";

$new_fields = array();
$i=0;
foreach($csv_items as $key => $csv_item){
    foreach ($fields as $item) {
        $value = str_replace('"', '""', $csv_items[$key][$item]);
/*
        if( $item==="start_time" || $item==="start_time" ){
            $value = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $value);
        }
*/
        if (preg_match('/[,"\s]/', $value)) {
            $value = '"' . $value . '"';
        }
        $new_fields[$key][$item] = $value;
    }
    $csv .= implode(',', $new_fields[$key]) . "\n";
}
$csv = mb_convert_encoding($csv, "Shift_JIS", "EUC-JP");

// データベース接続を閉じる
pg_close($con);
// CSVを出力
$file_name = "reservation_list.csv";

ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

/*
 *  設備予約取得するSQL文を作るだけ
 */
function get_reservation( $emp_id, $where, $between ){
// 時間指定あり予約
$sql = <<<__SQL_END__
SELECT
    (c).fclcate_name,
    (f).facility_name,
    r.date,
    r.start_time,
    r.end_time,
    r.title,
    r.content,
    r.users,
    e.emp_lt_nm,
    e.emp_ft_nm
FROM reservation r
    LEFT JOIN fclcate c ON c.fclcate_id = r.fclcate_id
    LEFT JOIN facility f ON f.facility_id = r.facility_id
    LEFT JOIN empmst e ON e.emp_id = r.emp_id
$where
$between
__SQL_END__;

// 時間指定無し予約

$sql2 = <<<__SQL_END__
 UNION
SELECT
    (c).fclcate_name,
    (f).facility_name,
    r.date,
    null as start_time,
    null as end_time,
    r.title,
    r.content,
    r.users,
    e.emp_lt_nm,
    e.emp_ft_nm
FROM reservation2 r
    LEFT JOIN fclcate c ON c.fclcate_id = r.fclcate_id
    LEFT JOIN facility f ON f.facility_id = r.facility_id
    LEFT JOIN empmst e ON e.emp_id = r.emp_id
$where
$between
ORDER BY fclcate_name, facility_name, date, start_time, end_time
__SQL_END__;
return $sql.$sql2;
}
