<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// ≪特定１病院向け≫    栄養管理画面・スクリーニングシート画面
//
// 以下のような箇所で、特定病院に特化した造りとなっています。
// ◆表示項目が幾分独特
// ◆検査項目が、完全に特定病院に特化したIDを扱っている
// ◆食事開始変更箋テンプレート、体重集計表テンプレートなど、特定テンプレートに依存
// ◆Excel出力があるが、PHP5専用
// ◆IE6での試験は行っていない。
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■




// 基本的にrequire_onceは、about_comedix.phpのみ。
// ただし、呼ばれ方によって、以下が別途インクルードされる。本ファイルをrequire_onceで検索のこと。
//-----------------------------
// ◆ 一括引用の際は、XMLを扱うためにsot_util.phpを利用
// ◆ 定型文ﾃﾝﾌﾟﾚｰﾄから呼ばれた場合に開くダイアログは、メドレポートの一般ダイアログを開きたいため、
//    get_values.ini、summary_common.ini、get_menu_label.iniを利用
// ◆ Excelダウンロードの際は、./libs/phpexcel/Classes/PHPExcel.php を利用。
//-----------------------------
ob_start();
require_once("about_comedix.php");
ob_clean();






//**********************************************************************************************************************
// PHPその１）データ構造定義
//**********************************************************************************************************************




//==========================================================
// フィールド定義
//
// field       : DBフィールド名とシンクロ。
//               ★★★DBにフィールド追加する際、新規フィールド名は、他のフィールド名の「部分名」にならないようにお願いいたします。
//               フォーミュラ文字列の中のフィールド名を置換して値をセットしたりするためです。エクセル出力などでもエラーとなってしまうかもしれません。
//               （その為に、「ppNN_」などと強制的に番号を振っています。）
// is_header   : HTML上、ヘッダ列とするものに1。飛ばして指定することは不可。
// is_readonly : HTML上、リードオンリー灰色にするなら1。
// xlsout      : エクセル出力列とするなら1。SOAP定型文テンプレートからの引用の際も、現状、xlsout=1のものがコピー対象となる。
// width       : HTMLテーブルへの設定。列の最低幅と考えればよい。
// validation  : exec_saveの際に実行させるJavaScript構文。この式がエラー文字列を返せばエラーダイアログを表示し、保存を不可とする。
// colparent   : 簡単にいうと、「何個目のcolspanであるか」を示している。
//               いまのところ、colspanを指定したら、colparent番号を指定する必要がある。
//               この値は定義内でユニークな値とすること。
//               colspanで集約された列の合計幅を計算している箇所があり、その際に利用している。
// jp、title   : この用途の説明は割愛。title定義がなければjpが参照される。
//==========================================================
$FIELD_INFO = array(
    array("field"=>"is_exist_data"  ,"jp"=>"削除",             "is_header"=>1, "width"=>"30px"),
    array("field"=>"inpt_in_dt"     ,"jp"=>"入院日",           "is_header"=>1, "width"=>"50px"),
    array("field"=>"ptrm_name"      ,"jp"=>"病室",             "is_header"=>1, "xlsout"=>1, "width"=>"50px",   "title"=>"対象月月末時点の病室"),
    //array("field"=>"ptif_id"        ,"jp"=>"患者ID",           "is_header"=>1),
    array("field"=>"dr_nm"          ,"jp"=>"主治医",           "is_header"=>1, "xlsout"=>1,                    "title"=>"対象月月末時点の主治医"),
    array("field"=>"ptif_name"      ,"jp"=>"患者氏名",         "is_header"=>1, "xlsout"=>1),
    array("field"=>"yyyymm"         ,"jp"=>"年月",             "is_header"=>1, "width"=>"60px"),
    array("field"=>"ptrm_room_no"   ,"jp"=>"部屋番号",         "xlsout"=>1, "is_readonly"=>1,                  "title"=>"対象月月末時点の部屋番号"),
    array("field"=>"pp27_kasan"     ,"jp"=>"加算",             "xlsout"=>1, "width"=>"30px", "validation"=>"validIsKasan('[値]')"),
    array("field"=>"mokuhyou1"      ,"jp"=>"栄養管理上の目標", "xlsout"=>1, "width"=>"50px", "colspan"=>'3', "colparent"=>1),
    array("field"=>"mokuhyou2"      ,"jp"=>"",                 "xlsout"=>1, "width"=>"50px"),
    array("field"=>"mokuhyou3"      ,"jp"=>"",                 "xlsout"=>1, "width"=>"50px"),
    array("field"=>"sikkan"         ,"jp"=>"疾患",             "xlsout"=>1),
    array("field"=>"eiyou_hokyuho"  ,"jp"=>"栄養補給法",       "xlsout"=>1),
    array("field"=>"syusyoku"       ,"jp"=>"主食",             "xlsout"=>1),
    array("field"=>"fukusyoku"      ,"jp"=>"副食",             "xlsout"=>1),
    array("field"=>"tsuikahin"      ,"jp"=>"追加品",           "xlsout"=>1),
    array("field"=>"keikan_eiyouzai","jp"=>"経管栄養剤",       "xlsout"=>1),
    array("field"=>"gender"         ,"jp"=>"性別",             "xlsout"=>1, "is_readonly"=>1),
    array("field"=>"age"            ,"jp"=>"年齢",             "xlsout"=>1, "is_readonly"=>1),
    array("field"=>"shincyou"       ,"jp"=>"身長",             "xlsout"=>1),
    array("field"=>"taiju"          ,"jp"=>"体重",             "xlsout"=>1),
    array("field"=>"tai_zengetu"    ,"jp"=>"体重(前月)",       "xlsout"=>1),
    array("field"=>"tai_genritu"    ,"jp"=>"体重減少率",       "xlsout"=>1, "is_readonly"=>1),
    array("field"=>"pp01_bmi"       ,"jp"=>"BMI",              "xlsout"=>1, "is_readonly"=>1,                  "title"=>"BMI"),
    array("field"=>"pp02_ibw"       ,"jp"=>"IBW",              "xlsout"=>1, "is_readonly"=>1,                  "title"=>"目標体重"),
    array("field"=>"pp03_ibmi"      ,"jp"=>"IBMI",             "xlsout"=>1,                                    "title"=>"目標BMI"),
    array("field"=>"pp04_ree"       ,"jp"=>"REE",              "xlsout"=>1, "is_readonly"=>1,                  "title"=>"安静時代謝"),
    array("field"=>"pp05_action"    ,"jp"=>"a",                "xlsout"=>1,                   "width"=>"30px", "title"=>"活動係数"),
    array("field"=>"pp06_stress"    ,"jp"=>"s",                "xlsout"=>1,                   "width"=>"30px", "title"=>"ストレス係数"),
    array("field"=>"pp07_h_energy"  ,"jp"=>"E",                "xlsout"=>1, "is_readonly"=>1, "width"=>"30px", "title"=>"必要エネルギー"),
    array("field"=>"pp25_h_protein_f","jp"=>"p",               "xlsout"=>1,                   "width"=>"30px", "title"=>"必要たんぱく質係数"),
    array("field"=>"pp26_h_h2o_f"   ,"jp"=>"h",                "xlsout"=>1,                   "width"=>"30px", "title"=>"必要水分係数"),
    array("field"=>"pp08_h_protein" ,"jp"=>"P",                "xlsout"=>1, "is_readonly"=>1, "width"=>"30px", "title"=>"必要たんぱく質"),
    array("field"=>"pp09_h_h2o"     ,"jp"=>"H2O",              "xlsout"=>1, "is_readonly"=>1,                  "title"=>"必要水分"),
    array("field"=>"pp10_t_energy"  ,"jp"=>"E",                "xlsout"=>1,                   "width"=>"30px", "title"=>"提供エネルギー"),
    array("field"=>"pp11_t_protein" ,"jp"=>"P",                "xlsout"=>1,                   "width"=>"30px", "title"=>"提供たんぱく質"),
    array("field"=>"pp12_t_enbun"   ,"jp"=>"塩分",             "xlsout"=>1,                   "width"=>"30px", "title"=>"提供塩分"),
    array("field"=>"pp13_t_h2o"     ,"jp"=>"H2O",              "xlsout"=>1,                                    "title"=>"提供水分"),
    array("field"=>"pp14_tp"        ,"jp"=>"TP",               "xlsout"=>1,                   "width"=>"30px", "title"=>"[検査データ]総蛋白"),
    array("field"=>"pp15_alb"       ,"jp"=>"Alb",              "xlsout"=>1,                                    "title"=>"[検査データ]アルブミン"),
    array("field"=>"pp16_alb_score" ,"jp"=>"Alb-s",            "xlsout"=>1, "is_readonly"=>1,                  "title"=>"[検査データ]アルブミンスコア"),
    array("field"=>"pp17_hb"        ,"jp"=>"Hb",               "xlsout"=>1,                   "width"=>"30px", "title"=>"[検査データ]ヘモグロビン"),
    array("field"=>"pp18_wbc"       ,"jp"=>"WBC",              "xlsout"=>1,                                    "title"=>"[検査データ]白血球数"),
    array("field"=>"pp19_lymph"     ,"jp"=>"LYMPH",            "xlsout"=>1,                                    "title"=>"[検査データ]リンパ球数"),
    array("field"=>"pp20_tlc"       ,"jp"=>"TLC",              "xlsout"=>1, "is_readonly"=>1,                  "title"=>"TLC"),
    array("field"=>"pp21_tlc_score" ,"jp"=>"TLC-s",            "xlsout"=>1, "is_readonly"=>1,                  "title"=>"TLCスコア"),
    array("field"=>"pp22_tcho"      ,"jp"=>"T-cho",            "xlsout"=>1,                                    "title"=>"[検査データ]総コレステロール"),
    array("field"=>"pp23_tcho_score","jp"=>"T-cho-s",          "xlsout"=>1, "is_readonly"=>1,                  "title"=>"[検査データ]総コレステロールスコア"),
    array("field"=>"pp24_conut"     ,"jp"=>"CONUT",            "xlsout"=>1, "is_readonly"=>1,                  "title"=>"CONUT値"),
    array("field"=>"sonota"         ,"jp"=>"その他",           "xlsout"=>1),
    array("field"=>"conference_ymd" ,"jp"=>"カンファレンス日", "xlsout"=>1, "validation"=>"validIsSlashYmd('[値]')"),
    array("field"=>"kentou_naiyou"  ,"jp"=>"検討内容",         "xlsout"=>1),
    array("field"=>"kettei_naiyou"  ,"jp"=>"決定内容",         "xlsout"=>1),
    array("field"=>"update_emp_name","jp"=>"更新者",           "xlsout"=>1, "is_readonly"=>1)
);
//==========================================================
// 計算式フォーミュラ定義
// この定義順に、フォーミュラ実行されることに留意。
// フォーミュラは、行をまたいで実行できない。縦軸集計はできない。あくまで単一行内の項目同士でのみ、計算式を実行する用に設計されている。
//
// フォーミュラ実行後は、そののち"format"定義でフォーマットされるが、
// 行内で計算を実行している間はフォーマットしない。つまり
// ● フォーミュラ計算1  ⇒  結果１  ⇒  フォーミュラ計算２に利用  ⇒  結果２ ⇒  ...  ⇒ 結果１フォーマット(結果１の値確定)  ⇒ 格納と画面表示
// × フォーミュラ計算1  ⇒  結果１  ⇒ 結果１フォーマット(結果１の値確定)  ⇒  フォーミュラ計算２に利用  ⇒  結果２ ⇒  ...
// これは、極力エクセルとの計算差異をなくすものである。
// なので、画面値を安直に電卓で計算しても、微妙に計算が合わないように見える場合もある、ということに注意。
// テストをするなら、実際のエクセルとのフォーミュラ比較で行うべし。
//
//
// target      : 列ID
// base        : 計算時に必要な列IDの配列（「formula」に含まれている列IDを取り出したものと等しい）
// format      : HTML上で表示される場合・DB保存する場合のフォーマット(アスタリクスを置換して、evalをかける）
// xls_format  : 出力Excelセルに指定する書式設定
// formula     : HTML上での計算式。そのままevalをかける。
// xls_formula : 出力Excelセルに指定する計算式。列IDはExcel上のセルIDに置換される
//==========================================================
$formulaInfo = array(
    array("target"=>"tai_genritu"  , "base"=>array("taiju","tai_zengetu"), "format"=>"round(値,1)", "xls_format"=>"0.0",
        "formula"=>'(tai_zengetu ? 100 * (tai_zengetu - taiju) / tai_zengetu : EMPTY)',
        "xls_formula"=>"100 * (tai_zengetu - taiju) / tai_zengetu"
    ),
    array("target"=>"pp01_bmi"  , "base"=>array("taiju","shincyou"), "format"=>"round(値,1)", "xls_format"=>"0.0",
        "formula"=>"taiju / (shincyou * 0.01) / (shincyou * 0.01)",
        "xls_formula"=>"taiju / (shincyou * 0.01) / (shincyou * 0.01)"
    ),
    array("target"=>"pp02_ibw" , "base"=>array("shincyou","pp03_ibmi"), "format"=>"round(値,1)", "xls_format"=>"0.0",
        "formula"=>"shincyou * 0.01 * shincyou * 0.01 * pp03_ibmi",
        "xls_formula"=>"shincyou * 0.01 * shincyou * 0.01 * pp03_ibmi"
    ),
    array("target"=>"pp04_ree"  , "base"=>array("gender","pp02_ibw","shincyou","age"), "format"=>"round(値)", "xls_format"=>"0_ ",
        "formula"=>
            "(gender=='男' ?".
            " 66.47 + (13.75 * pp02_ibw) + (5 * shincyou) - (6.75 * age) :".
            " 655.1 + (9.56 * pp02_ibw) + (1.85 * shincyou) - (4.65 * age) )",
        "xls_formula"=>
            'IF(gender="男" ,'.
            " 66.47 + (13.75 * pp02_ibw) + (5 * shincyou) - (6.75 * age) ,".
            " 655.1 + (9.56 * pp02_ibw) + (1.85 * shincyou) - (4.65 * age) )"),
    array("target"=>"pp07_h_energy"   , "base"=>array("pp04_ree","pp05_action", "pp06_stress"), "format"=>"round(値)", "xls_format"=>"0_ ",
        "formula"=>"pp04_ree * pp05_action * pp06_stress",
        "xls_formula"=>"pp04_ree * pp05_action * pp06_stress"
    ),
    array("target"=>"pp08_h_protein"   , "base"=>array("pp02_ibw","pp25_h_protein_f"), "format"=>"round(値)", "xls_format"=>"0_ ",
        "formula"=>"pp02_ibw * pp25_h_protein_f",
        "xls_formula"=>"pp02_ibw * pp25_h_protein_f"
    ),
    array("target"=>"pp09_h_h2o" , "base"=>array("taiju","pp26_h_h2o_f"), "format"=>"round(値)", "xls_format"=>"0_ ",
        "formula"=>"taiju * pp26_h_h2o_f",
        "xls_formula"=>"taiju * pp26_h_h2o_f"
    ),
    array("target"=>"pp16_alb_score" , "base"=>array("pp15_alb"),
        "formula"=>"(pp15_alb >= 3.5 ? 0 : (pp15_alb >= 3 ? 2 : (pp15_alb >= 2.5 ? 4 : 6)))",
        "xls_formula"=>"IF(pp15_alb >= 3.5 , 0 , IF(pp15_alb >= 3 , 2 , IF(pp15_alb >= 2.5 , 4 , 6)))"
    ),
    array("target"=>"pp20_tlc" , "base"=>array("pp18_wbc", "pp19_lymph"),
        "formula"=>"Math.round(pp18_wbc * pp19_lymph * 100) / 10000",
        "xls_formula"=>"pp18_wbc * pp19_lymph / 100"
    ),
    array("target"=>"pp21_tlc_score" , "base"=>array("pp20_tlc"),
        "formula"=>"(pp20_tlc >= 1600 ? 0 : (pp20_tlc >= 1200 ? 1 : (pp20_tlc >= 800 ? 2 : 3)))",
        "xls_formula"=>"IF(pp20_tlc >= 1600 , 0 , IF(pp20_tlc >= 1200 , 1 , IF(pp20_tlc >= 800 , 2 , 3)))"
    ),
    array("target"=>"pp23_tcho_score" , "base"=>array("pp22_tcho"),
        "formula"=>"(pp22_tcho >= 180 ? 0 : (pp22_tcho >= 140 ? 1 : (pp22_tcho >= 100 ? 2 : 3)))",
        "xls_formula"=>"iF(pp22_tcho >= 180 , 0 , IF(pp22_tcho >= 140 , 1 , IF(pp22_tcho >= 100 , 2 , 3)))"
    ),
    array("target"=>"pp24_conut" , "base"=>array("pp16_alb_score", "pp21_tlc_score", "pp23_tcho_score"),
        "formula"=>"int(pp16_alb_score) + int(pp21_tlc_score) + int(pp23_tcho_score)",
        "xls_formula"=>"pp16_alb_score + pp21_tlc_score + pp23_tcho_score"
    )
);
//==========================================================
// 条件付き書式定義
// この格納順に、書式判定が行われ、該当すればcolorが適用されて処理を抜ける、というイメージ。
//
// target  : 列ID
// formula : 条件式、「値」文字が実際の値に置換されて判定
// color   : true時につける文字色
// ※色のほかに、true時は自動的に文字はboldになる
//==========================================================
$styleInfo = array(
    array("target"=>"pp01_bmi",  "joken"=>"値 < 18.5",  "color"=>"blue"), // 青。#0000ffと書いてもよい
    array("target"=>"pp01_bmi",  "joken"=>"値 > 25",    "color"=>"red"), // 赤。#ff0000形式で書いてもよい
    array("target"=>"pp14_tp",   "joken"=>"値 <= 6",    "color"=>"red"), // 赤
    array("target"=>"pp15_alb",  "joken"=>"値 <= 3.5",  "color"=>"red"), // 赤
    array("target"=>"pp17_hb",   "joken"=>"値 <= 10",   "color"=>"red"), // 赤
    array("target"=>"pp20_tlc",  "joken"=>"値 <= 1600", "color"=>"red"), // 赤
    array("target"=>"pp22_tcho", "joken"=>"値 <= 180",  "color"=>"red"),  // 赤
    array("target"=>"pp24_conut","joken"=>"値 <= 1",    "color"=>"black"),  // 黒
    array("target"=>"pp24_conut","joken"=>"値 <= 4",    "color"=>"blue"),   // 青
    array("target"=>"pp24_conut","joken"=>"値 <= 8",    "color"=>"orange"), // オレンジ
    array("target"=>"pp24_conut","joken"=>"値 <= 12",   "color"=>"red")     // 赤
);




//**********************************************************************************************************************
// PHPその２）汎用関数
//**********************************************************************************************************************

//==========================================================
// eucから、utf-8へ変換
//==========================================================
function utf8($euc) {
    return mb_convert_encoding(mb_convert_encoding($euc, 'sjis-win', 'eucjp'), "UTF-8", 'sjis-win');
}
//==========================================================
// エクセル用の列アルファベットを取得 ゼロオリジン
//==========================================================
function xlsAlpha($idx) {
    $ary = array(
        "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
        "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ"
    );
    return $ary[$idx];
}
//==========================================================
// 年齢の計算
//==========================================================
function get_age($birth_ymd, $current_ymd) {
  if (!$current_ymd) $current_ymd = date("Ymd");
  $month = substr($birth_ymd, 4, 4);
  $age = substr($current_ymd, 0, 4) - substr($birth_ymd, 0, 4);
  if (substr($current_ymd, 4, 4) < $month) $age = $age - 1;
  return $age;
}
//==========================================================
// SQLのセレクト文を実行
//==========================================================
function sql_select($sql){
    global $fname;
    global $con;
    $sel = select_from_table($con, $sql, "", $fname);
    if($sel) return $sel;
    echo "データベースへの問い合わせエラーが発生しました。";
    die;
    return null;
}
//==========================================================
// 配列／連想配列をJSON形式に変換する
//==========================================================
function to_json($var, $is_recursive=0) {
    if (gettype($var) == 'array') {
        // 連想配列の場合
        if (is_array($var) && count($var) && (array_keys($var) !== range(0, sizeof($var) - 1))) {
            $properties = array();
            foreach ($var as $k => $v) $properties[] = $k .":".to_json($v, 1);
            return '{' . join(',', $properties) . '}';
        }
        // 通常の配列の場合
        $properties = array();
        foreach ($var as $v) $properties[] = to_json($v, 1);

        return '[' . join(',', $properties) . ']';
    }
    // それ以外なら、文字列として処理
    if (!$is_recursive && $var=="") return "{}";
    return '"' . js($var) . '"';
}
//==========================================================
// JavaScript用にダブルクォート、改行文字をエスケープする
//==========================================================
function js($s) {
    return mb_ereg_replace('"', '\"', mb_ereg_replace("\r", '', mb_ereg_replace("\n", "\\n", mb_ereg_replace("\\\\", '\\\\', $s))));
}
//==========================================================
// (decode_request()関数からの呼び出し専用)
// 引数がユニコード数値参照文字「&#nnnnn;」形式なら文字に戻す。
// 通常文字でもPHP4でIBM拡張文字（はしご高など）に対処すべく、いったんshift_jisに変換してから文字を取得する
//==========================================================
function unicode_entity_decode($str) {
    $ary = explode(";", $str);
    $out = array();
    foreach($ary as $s) {
        if (!$s) continue;
        if ($s=="&#9") { $out[]= "\t"; continue; }
        $ss = mb_convert_encoding($s.";", "eucJP-win", "HTML-ENTITIES"); // ユニコード数値参照文字から、PHP4でいちばんまともに利用できるEUC文字セットへ
        if ($ss=="\t") { $out[] = $s.";"; continue; } // もしeucに変換できなければ、数値参照文字のままにしておく。
        // 変換が成功しても、文字コードが誤変換されているかもしれない。気づいたのはIBM拡張文字系。これだけは暫定対処する。
        $ss = mb_convert_encoding(mb_convert_encoding($ss, 'sjis-win', 'eucjp'), "eucJP-win", 'sjis-win');
        if ($ss=="\t") $out[] = $s.";"; // もしこれで変換できなければ、数値参照文字のままにしておく。（この処理には恐らく引っかからない）
        else $out[] = $ss; // 文字を追加
    }
    return implode("", $out);
}
//==========================================================
// 保存時の$_POST値をデコードする。
// $_POST値は、AJAXにつき、まともにEUCを送信できないため、JavaScript側で、ユニコード数値参照文字「&#nnnnn;」として送信される。
//==========================================================
function decode_request() {
    $out = array();
    $def = mb_substitute_character(); // たぶん初期値はハテナマーク
    mb_substitute_character(0x9); // 適当にタブ文字にでも置換しておく。何か明示的に、わかる文字であれば何でもよい。unicode_entity_decode関数で判定に利用。
    $post_names = array_keys($_POST);
    foreach ($post_names as $key) {
        $val = $_POST[$key];
        if (is_array($val)) {
            foreach ($val as $idx=>$v) $val[$idx] = unicode_entity_decode($v);
            $out[$key] = $val;
        } else {
            $out[$key] = unicode_entity_decode($val);
        }
        unset($_POST[$key]);
    }
    mb_substitute_character($def); // 初期値に戻す
    return $out;
}

function svchk($str) {
    if (!strlen($str)) return false;
    if ($str=="undefined") return false;
    return true;
}

//**********************************************************************************************************************
// PHPその３）セッションチェック、権限チェック、DB接続
//**********************************************************************************************************************
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
    if ($_REQUEST["ajax_mode"]) {
        echo "SESSION_EXPIRED"; // AJAXの場合は、JavaScriptを返してもしょうがないので、エラートークンを返す。JavaScript側へ判定を任せる。
    } else {
      echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
      echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    }
  exit;
}

//ログイン職員ＩＤ
$sel = sql_select("select emp_id from session where session_id = '$session'");
if($sel == 0){
    if ($_REQUEST["ajax_mode"]) {
        echo "SESSION_EXPIRED"; // AJAXの場合は、JavaScriptを返してもしょうがないので、エラートークンを返す。JavaScript側へ判定を任せる。
    } else {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    }
    exit;
}
$emp_id = pg_result($sel,0,"emp_id");


$sel = sql_select("select emp_lt_nm || ' ' || emp_ft_nm as emp_name from empmst where emp_id = '".pg_escape_string($emp_id)."'");
$emp_name = pg_result($sel,0,"emp_name");





$append_ref = $_REQUEST["append_ref"];
$ajax_mode = $_REQUEST["ajax_mode"];
$view_mode = $_REQUEST["view_mode"];
$list_mode = $_REQUEST["list_mode"];

// 引用の際はテンプレートを開く必要がある。template_xml_classを使いたいのでsot_utilを利用する。
if ($append_ref) {
    require_once("sot_util.php");
}









//**********************************************************************************************************************
// PHPその４）一括引用処理（関数）
//**********************************************************************************************************************
function get_master_ref($ptif_id, $inpt_in_dt, $yyyymm, $tmpl_meal_start_ids, $tmpl_weight_check_ids, $out, &$append_ref_count) {
    global $con;
    global $fname;
    $_out = $out;
    // 最大年月日は指定年月末日。計画書なら当月末日。
    $max_ymd = ($yyyymm=="000000" ? date("Ym") : $yyyymm) . "99";

    // 最小年月日は入院日
    $min_ymd = $inpt_in_dt;
    // 検査の最小年月日は入院日より過去かつ6ヶ月前の初日。計画書なら自動的に入院日。
    $kensa_min_ymd = $yyyymm-6; // 対象月より6ヶ月前
    if ($kensa_min_ymd % 100 > 6 || $kensa_min_ymd % 100==0) $kensa_min_ymd = $yyyymm - 100 + 6; // 6ヶ月引いて6月以上は無い。「ﾏｲﾅｽ1年ﾌﾟﾗｽ6ヶ月」となる。
    $kensa_min_ymd .= "01";
    if ($inpt_in_dt > $kensa_min_ymd || $yyyymm=="000000") $kensa_min_ymd = $inpt_in_dt;

    $zengetu_yyyymm = (int)$yyyymm - (int)(substr($yyyymm, 4)=="01" ? (100-11) : 1); // $yyyymmの前月 $yyyymmがゼロのときは変な値になるが無視してよい
    $taiju_max_ymd = $zengetu_yyyymm."99";
    $taiju_min_ymd = (int)$min_ymd - (int)(substr($min_ymd, 4,2)=="01" ? (10000-1100) : 100); // $min_ymdの前月初日


    //================================================================
    // 【引用その０-１】
    // 次の退院日を取得する前に、現在入院中かを今一度確認する。
    // まれに、「同日複数入院＋一部レコード退院」などというデータが存在する。
    // つまり、同日入院日が存在し、ひとつでも入院中であれば、退院レコードを見ない。
    // この「はしょった」判定は、こういった事例においては妥当となる。
    //================================================================
    $sql =
    " select ptif_id from inptmst".
    " where ptif_id = '".pg_escape_string($ptif_id)."'".
    " and inpt_in_dt = '".pg_escape_string($inpt_in_dt)."'";
    $sel = sql_select($sql);
    if (trim(@pg_result($sel,0,"ptif_id"))=="") {
        //================================================================
        // 【引用その０-２】
        // 退院日を取得。月末以内に退院済なら、退院日が最大日付とする
        //================================================================
        $sql =
        " select inpt_out_dt from inpthist".
        " where ptif_id = '".pg_escape_string($ptif_id)."'".
        " and inpt_in_dt = '".pg_escape_string($inpt_in_dt)."'";
        $sel = sql_select($sql);
        $inpt_out_dt = trim(@pg_result($sel,0,"inpt_out_dt"));
        if ($inpt_out_dt && $inpt_out_dt < $max_ymd) $max_ymd = $inpt_out_dt;
    }

    //================================================================
    // 【引用その１】
    // 食事箋は、「入院日以降〜対象月の月末まで」で最新のもの。
    // 食事箋は同名で２つもある。１つは無効なのだろうが、断定的かつ永続的判断はできない。両方有り得ることにする。
    //================================================================
    $sql =
    " select xml.xml_file_name from sum_xml xml".
    " inner join summary sum on (".
    "     sum.ptif_id = '".pg_escape_string($ptif_id)."'".
    "     and sum.tmpl_id in ('".implode("','", $tmpl_meal_start_ids)."')".
    "     and sum.summary_seq = xml.summary_seq".
    "     and sum.summary_id = xml.summary_id".
    "     and sum.cre_date >= '".pg_escape_string($inpt_in_dt)."'".
    "     and sum.cre_date <= '".pg_escape_string($max_ymd)."'".
    " )".
    " where (xml.delete_flg is null or xml.delete_flg = 0)".
    " and xml.ptif_id = '".pg_escape_string($ptif_id)."'".
    " order by xml.summary_seq desc, xml.apply_id desc limit 1";
    $sel = sql_select($sql);
    $xml_file_name = @pg_fetch_result($sel, 0, "xml_file_name");
    $xml = new template_xml_class(); // sot_util参照
    $data = $xml->get_xml_object_array_from_db($con, $fname, $xml_file_name);
    $data = @$data->nodes["template"]->nodes["WorkSheet3"];
    if ($data) {
        $_out["sikkan"] = @$data->nodes["sikkan"]->nodes["mstt_sikkanmei_name"]->value; // 主病名（＝疾患名）⇒ 疾患へ
        $eiyou_hokyuho = "";
        if (@$data->nodes["riyu"]->nodes["mstt_keikou_nomi"]->value=="はい") $eiyou_hokyuho="経口"; // 経口のみcheck はい or いいえ ⇒栄養補給法へ
        if (@$data->nodes["riyu"]->nodes["mstt_keikan_nomi"]->value=="はい") $eiyou_hokyuho="経管"; // 経管のみcheck はい or いいえ ⇒栄養補給法へ
        if (@$data->nodes["riyu"]->nodes["mstt_keikou_heiyou"]->value=="はい") $eiyou_hokyuho="併用"; // 経口経管check はいorいいえ ⇒栄養補給法へ
        if (@$data->nodes["riyu"]->nodes["mstt_riyu"]->value=="絶食入院") $eiyou_hokyuho="ＮＧ"; // 絶食check ⇒栄養補給法へ
        $_out["pp27_kasan"] = "";
        if (@$data->nodes["sikkan"]->nodes["mstt_wafarin_kasan"]->value == "加算") $_out["pp27_kasan"] = "加"; // ワーファリン加算＝加算なら「加」
        if (@$data->nodes["sikkan"]->nodes["mstt_warfarin_kasan"]->value == "加算") $_out["pp27_kasan"] = "加"; // ワーファリン加算＝加算なら「加」
        $_out["eiyou_hokyuho"] = $eiyou_hokyuho;
        $_out["syusyoku"] = @$data->nodes["syusyoku"]->nodes["mstt_syusyoku_1"]->value; //主食の１コ目
        $_out["fukusyoku"] = @$data->nodes["fukusyoku"]->nodes["mstt_fukusyoku_1"]->value; // 副食の１コ目
        $_out["keikan_eiyouzai"] = @$data->nodes["keikan"]->nodes["mstt_keikan_name_1"]->value; // 経管栄養の１コ目
    }

    //================================================================
    // 【引用その２】
    // 検査取得は「入院日あるいは6ヶ月前ついたちから対象月の月末まで」でサーチ。過去月・最新月に関わらず。
    // 該当検査を何も行っていなければ、過去を遡って取得する
    //================================================================
    $prev_uketuke_date = "";
    $prev_uketuke_no = "";
    for (;;) {
        $where = "";
        if ($prev_uketuke_date) {
            $where =
            " and (".
            "     hdr_uketuke_date < '".$prev_uketuke_date."'".
            "     or (hdr_uketuke_date = '".$prev_uketuke_date."' and hdr_uketuke_no < '".$prev_uketuke_no."')".
            " )";
        }
        $sql =
        " select hdr_uketuke_date, hdr_uketuke_no from sum_kensakekka".
        " where ptif_id = '".pg_escape_string($ptif_id)."'".
        " and hdr_uketuke_date between '".pg_escape_string($kensa_min_ymd)."' and '".pg_escape_string($max_ymd)."'".
        " ".$where.
        " order by hdr_uketuke_date desc, hdr_uketuke_no desc limit 1";
        $sel = sql_select($sql);
        $uketuke_date = @pg_fetch_result($sel, 0, "hdr_uketuke_date");
        $uketuke_no = @pg_fetch_result($sel, 0, "hdr_uketuke_no");
        if (!$uketuke_date) break;
        $prev_uketuke_date = $uketuke_date;
        $prev_uketuke_no = $uketuke_no;

        $sql =
        " select itm_item_no, itm_kensa_kekka from sum_kensakekka".
        " where ptif_id = '".pg_escape_string($ptif_id)."'".
        " and hdr_uketuke_date = '".pg_escape_string($uketuke_date)."'".
        " and hdr_uketuke_no = '".pg_escape_string($uketuke_no)."'";
        $sel = sql_select($sql);
        $rows = @pg_fetch_all($sel);
        $tmp_out = array();
        if ($rows) {
            foreach ($rows as $row) {
                $item_no = trim($row["itm_item_no"]);
                if ($item_no==="0001") $tmp_out["pp14_tp"]    = trim($row["itm_kensa_kekka"]); // 総蛋白
                if ($item_no==="0058") $tmp_out["pp15_alb"]   = trim($row["itm_kensa_kekka"]); // アルブミン
                if ($item_no==="0403") $tmp_out["pp17_hb"]    = trim($row["itm_kensa_kekka"]); // ヘモグロビン：血色素量
                if ($item_no==="0402") $tmp_out["pp18_wbc"]   = trim($row["itm_kensa_kekka"]); // 白血球数
                if ($item_no==="3209") $tmp_out["pp19_lymph"] = trim($row["itm_kensa_kekka"]); // リンパ球数パーセント
                if ($item_no==="0028") $tmp_out["pp22_tcho"]  = trim($row["itm_kensa_kekka"]); // 総コレステロール
            }
        }
        // AlbとHbの検査結果があれば終了
        if ($tmp_out["pp15_alb"]!="" && $tmp_out["pp17_hb"]!=""){
            foreach ($tmp_out as $field => $v) $_out[$field] = $v;
            break;
        }
    }

    //================================================================
    // 【引用その３】
    // 目標とその他。
    // 前月スクリーニングシートを作成済ならそこから取得、なければ計画書から取得
    // 画面編集中の値を上書きしない。
    //================================================================
    $sql =
    " select yyyymm, mokuhyou1, mokuhyou2, mokuhyou3, shincyou, taiju, sonota".
    ",pp03_ibmi, pp05_action, pp06_stress, pp25_h_protein_f, pp26_h_h2o_f, pp10_t_energy, pp11_t_protein, pp12_t_enbun, pp13_t_h2o".
    " from sum_eiyou_screening".
    " where ptif_id = '".pg_escape_string($ptif_id)."'".
    " and inpt_in_dt = '".pg_escape_string($inpt_in_dt)."'".
    " and (yyyymm = '000000' or yyyymm = '".$zengetu_yyyymm."') order by yyyymm desc limit 1";
    $sel = sql_select($sql);
    if (!strlen($_out["mokuhyou1"])) $_out["mokuhyou1"] = trim(@pg_result($sel,0,"mokuhyou1"));
    if (!strlen($_out["mokuhyou2"])) $_out["mokuhyou2"] = trim(@pg_result($sel,0,"mokuhyou2"));
    if (!strlen($_out["mokuhyou3"])) $_out["mokuhyou3"] = trim(@pg_result($sel,0,"mokuhyou3"));
    if (!strlen($_out["sonota"])) $_out["sonota"] = trim(@pg_result($sel,0,"sonota"));

    //================================================================
    // 【引用その４】
    // 身長、体重(前月)、提供塩分、提供水分、提供エネルギー、提供たんぱく質は前月スクリーニングシートを作成済ならそこから取得
    // 画面編集中の値を上書きしない。
    //================================================================
    if (trim(@pg_result($sel,0,"yyyymm"))==$zengetu_yyyymm) {
        if (!strlen($_out["tai_zengetu"]))  $_out["tai_zengetu"] = trim(@pg_result($sel,0,"taiju"));
        if (!strlen($_out["shincyou"]))     $_out["shincyou"] = trim(@pg_result($sel,0,"shincyou"));
        if (!strlen($_out["pp10_t_energy"])) $_out["pp10_t_energy"] = trim(@pg_result($sel,0,"pp10_t_energy"));
        if (!strlen($_out["pp11_t_protein"]))   $_out["pp11_t_protein"] = trim(@pg_result($sel,0,"pp11_t_protein"));
        if (!strlen($_out["pp12_t_enbun"])) $_out["pp12_t_enbun"] = trim(@pg_result($sel,0,"pp12_t_enbun"));
        if (!strlen($_out["pp13_t_h2o"]))   $_out["pp13_t_h2o"] = trim(@pg_result($sel,0,"pp13_t_h2o"));
    }

    //================================================================
    // 【引用その５】
    // 前月スクリーニングシートを作成済ならそこから取得
    // なければ固定値をセット
    // 画面編集中の値を上書きしない。
    //================================================================
    if (trim(@pg_result($sel,0,"yyyymm"))==$zengetu_yyyymm) {
        if (!$_out["pp03_ibmi"])        $_out["pp03_ibmi"] = trim(@pg_result($sel,0,"pp03_ibmi"));
        if (!$_out["pp05_action"])      $_out["pp05_action"] = trim(@pg_result($sel,0,"pp05_action"));
        if (!$_out["pp06_stress"])      $_out["pp06_stress"] = trim(@pg_result($sel,0,"pp06_stress"));
        if (!$_out["pp25_h_protein_f"]) $_out["pp25_h_protein_f"] = trim(@pg_result($sel,0,"pp25_h_protein_f"));
        if (!$_out["pp26_h_h2o_f"])     $_out["pp26_h_h2o_f"] = trim(@pg_result($sel,0,"pp26_h_h2o_f"));
    }
    if (!$_out["pp03_ibmi"])        $_out["pp03_ibmi"]        = "22";
    if (!$_out["pp05_action"])      $_out["pp05_action"]      = "1";
    if (!$_out["pp06_stress"])      $_out["pp06_stress"]      = "1";
    if (!$_out["pp25_h_protein_f"]) $_out["pp25_h_protein_f"] = "1";
    if (!$_out["pp26_h_h2o_f"])     $_out["pp26_h_h2o_f"]     = "30";

    //================================================================
    // 【引用その６】
    // 身長は、全回値がなければマスタ値を取得。未登録はもちろん、0登録も無視
    // 画面編集中の値を上書きしない。
    //================================================================
    if (!$_out["shincyou"]) {
        $sel = sql_select("select * from ptsubif where ptif_id = '".pg_escape_string($ptif_id)."'");
        if (pg_num_rows($sel) > 0) {
            list($height1, $height2) = explode(".", pg_fetch_result($sel, 0, "ptsubif_height"));
            if ($height1) {
                if ($height1!="" && $height2 == "") $height2 = "0";
                $_out["shincyou"] = $height1 . ($height1!="" ? ".":"") .$height2;
            }
        }
    }

    //================================================================
    // 【引用その７】
    // 体重は、「入院日から対象月の月末まで」でサーチ。（過去月・最新月に関わらず）
    // 体重集計表も、将来複数の可能性はある。これもあるだけ取得する。
    // 画面編集中の値を上書きしない。
    //================================================================
    if (!strlen($_out["taiju"])) {
        $sql =
        " select xml.xml_file_name from sum_xml xml".
        " inner join summary sum on (".
        "     sum.ptif_id = '".pg_escape_string($ptif_id)."'".
        "     and sum.tmpl_id in ('".implode("','", $tmpl_weight_check_ids)."')".
        "     and sum.summary_seq = xml.summary_seq".
        "     and sum.summary_id = xml.summary_id".
        "     and sum.cre_date >= '".pg_escape_string($min_ymd)."'".
        "     and sum.cre_date <= '".pg_escape_string($max_ymd)."'".
        " )".
        " where (xml.delete_flg is null or xml.delete_flg = 0)".
        " and xml.ptif_id = '".pg_escape_string($ptif_id)."'".
        " order by xml.summary_seq desc, xml.apply_id desc limit 1";
        $sel = sql_select($sql);
        $xml_file_name = @pg_fetch_result($sel, 0, "xml_file_name");
        $xml = new template_xml_class(); // sot_util参照
        $data = $xml->get_xml_object_array_from_db($con, $fname, $xml_file_name);
        $data = @$data->nodes["template"]->nodes["WeightCheck"];
        $taiju1 = @$data->nodes["wtck_weight"]->value;
        $taiju2 = @$data->nodes["wtck_weight_float"]->value;
        if ($taiju1!="" && $taiju2 == "") $taiju2 = "0";
        $_out["taiju"] = $taiju1 . ($taiju1!="" ? ".":"") .$taiju2;
    }

    if (!strlen($_out["tai_zengetu"])) {
        $sql =
        " select xml.xml_file_name from sum_xml xml".
        " inner join summary sum on (".
        "     sum.ptif_id = '".pg_escape_string($ptif_id)."'".
        "     and sum.tmpl_id in ('".implode("','", $tmpl_weight_check_ids)."')".
        "     and sum.summary_seq = xml.summary_seq".
        "     and sum.summary_id = xml.summary_id".
        "     and sum.cre_date >= '".pg_escape_string($taiju_min_ymd)."'".
        "     and sum.cre_date <= '".pg_escape_string($taiju_max_ymd)."'".
        " )".
        " where (xml.delete_flg is null or xml.delete_flg = 0)".
        " and xml.ptif_id = '".pg_escape_string($ptif_id)."'".
        " order by xml.summary_seq desc, xml.apply_id desc limit 1";
        $sel = sql_select($sql);
        $xml_file_name = @pg_fetch_result($sel, 0, "xml_file_name");
        $xml = new template_xml_class(); // sot_util参照
        $data = $xml->get_xml_object_array_from_db($con, $fname, $xml_file_name);
        $data = @$data->nodes["template"]->nodes["WeightCheck"];
        $taiju1 = @$data->nodes["wtck_weight"]->value;
        $taiju2 = @$data->nodes["wtck_weight_float"]->value;
        if ($taiju1!="" && $taiju2 == "") $taiju2 = "0";
        $_out["tai_zengetu"] = $taiju1 . ($taiju1!="" ? ".":"") .$taiju2;
    }

    foreach ($_out as $k=>$v) {
        if ($_out[$k]==$out[$k]) continue;
        $append_ref_count++;
    }

    return $_out;
}











//**********************************************************************************************************************
// PHPその５）一括保存（AJAX）  このif文の終了箇所でdieする。
//**********************************************************************************************************************
if ($ajax_mode=="exec_save" && $view_mode) {
    //================================================================
    // 【保存その１】
    // POST値を$values変数に、レコード行単位で格納しなおす
    //================================================================
    $values = array();
    $ins_or_upd = "";
    $req = decode_request(); // ★★★ POST値を取得
    foreach ($req as $k => $v) {
        if ($k=="delete_keys") continue;
        list ($inpt_in_dt, $yyyymm, $ptif_id, $field) = explode("@",$k);
        $values[$inpt_in_dt."@".$yyyymm."@".$ptif_id][$field] = $v;
    }
    //================================================================
    // 【保存その２】
    // 削除行があれば削除する
    //================================================================

    pg_query("begin");
    $is_error = 0;
    do {
        $delete_keys = explode(",", $req["delete_keys"]);
        foreach ($delete_keys as $k) {
            list ($inpt_in_dt, $yyyymm, $ptif_id) = explode("@",$k);
            $sql =
            " delete from sum_eiyou_screening".
            " where inpt_in_dt = '".pg_escape_string($inpt_in_dt)."' and ptif_id = '".pg_escape_string($ptif_id)."'".
            " and yyyymm = '".$yyyymm."'";
            if (!update_set_table($con, $sql, array(), null, "", $fname)) { $is_error = 1; echo "データの削除に失敗しました。\n".$sql."\n".pg_last_error($con); break; }
        }
        //================================================================
        // 【保存その３】
        // $values値を保存する。削除された行は飛ばす。
        //================================================================
        foreach ($values as $k => $params) {
            if (in_array($k, $delete_keys)) continue;
            //--------------------
            // パラメータ準備
            //--------------------
            $where = "";
            list ($inpt_in_dt, $yyyymm, $ptif_id) = explode("@",$k);
            $where = " where inpt_in_dt = '".$inpt_in_dt."' and ptif_id = '". $ptif_id."' and yyyymm = '".$yyyymm."'";
            $ins_keys = array("inpt_in_dt", "yyyymm", "ptif_id", "update_emp_id", "update_emp_name", "update_timestamp");
            $ins_vals = array(
                "'".pg_escape_string($inpt_in_dt)."'",
                "'".pg_escape_string($yyyymm)."'",
                "'".pg_escape_string($ptif_id)."'",
                "'".pg_escape_string($emp_id)."'",
                "'".pg_escape_string($emp_name)."'",
                "current_timestamp"
            );
            $upd_params = array(
                "update_emp_id = '".pg_escape_string($emp_id)."'",
                "update_emp_name = '".pg_escape_string($emp_name)."'",
                "update_timestamp = current_timestamp"
            );
            //--------------------
            // ＤＢへ存在確認
            // あればUPDATE、なければINSERT
            //--------------------
            $sel = sql_select("select count(*) as cnt from sum_eiyou_screening" .$where);
            if (@pg_result($sel, 0, "cnt")) {
                foreach ($params as $field => $v) {
                    $upd_params[]= $field." = '".pg_escape_string($v)."'";
                }
                $sql = "update sum_eiyou_screening set " . implode(", ", $upd_params).$where;
                if (!update_set_table($con, $sql, array(), null, "", $fname)) { $is_error = 2; echo "データの更新に失敗しました。\n".$sql."\n".pg_last_error($con); break; };
            } else {
                foreach ($params as $field => $v) {
                    $ins_keys[]= $field;
                    $ins_vals[]= "'".pg_escape_string($v)."'";
                }
                $sql = "insert into sum_eiyou_screening (".implode(", ",$ins_keys).") values (".implode(", ", $ins_vals).")";
                if (!update_set_table($con, $sql, array(), null, "", $fname)) { $is_error = 3; echo "データの登録に失敗しました。\n".$sql."\n".pg_last_error($con);  break; };
            }
            if ($is_error) break;
        }
    } while (0);
    if ($is_error) {
        pg_query($con, "rollback");
    }
    else {
        pg_query($con, "commit");
        echo "ok";
    }
    die;
}









//**********************************************************************************************************************
// PHPその６）一覧表示（AJAX）
//
// 一覧表示は、以下、いくつかの呼び出しが統合／兼用されていることに留意。
// （１）栄養管理画面での一覧表示のため
//         ⇒このif文の終了時にdie終了。
// （２）栄養管理画面での、エクセル出力時
//         ⇒このif文の途中でdie終了。「Excel」「download」などで検索。
// （３）SOAP定型文テンプレートから「栄養管理」ボタンを押した場合。（全然違う画面からの呼び出しなので特に注意）
//         ⇒このif文の途中でdie終了。「定型文」「SOAP」「get_from_soap_teikeibun_template」などで検索。
//**********************************************************************************************************************
if ($ajax_mode=="exec_load" && $view_mode) {

    $ward_or_ptif = ($view_mode=="ward_mode" ? "ward" : "ptif");

    //--------------------------------------------
    // 【一覧表示その１】
    // 通常の一覧または患者別の一覧を取得する。
    // 通常の一覧の場合は、「計画表一覧」と「スクリーニング一覧」の２種あることに注意
    //--------------------------------------------


    //--------------------------------------------
    // 通常一覧の場合
    //
    // 画面行の一意キーは、「患者ID」である。
    // ソート順は病室＋ベット番号である。
    //
    // 取得対象は
    // １）指定年月に入院している患者で、指定病棟病室に存在した「形跡のある」患者
    // ２）または指定年月に入院しているが、どの病棟にも存在しないのに、栄養管理情報データのある患者（マスタ不備対策）
    //
    // 以下を利用
    //
    // 同月内における「複数入退院」「転棟転床」を、以下を利用しつつ取得する。
    // inptmst 入院中情報
    // inpthist 退院済み情報
    // inptmove 転棟情報のSQL(転棟前の入棟日は空)
    //
    // 【複数入退院】
    // マレに、対象者に同月内での複数入退院があったとしても、複数行出さない。
    // 指定月に入院があるかどうかのみで判定する。
    // ★★★入院中情報inptmstの入院日以降か、退院済情報inpthistの入退院範囲内であるものが対象。
    //
    // 計画書は入院単位で作成されるのだが、
    // 月途中で退院と再入院が有った場合、後者の入院をひっかけるため、
    // 前者の計画書は表示されないことになるが、
    // 前者の計画書は、患者別一覧にて確認することができる。
    //
    // 【転棟転床】
    // 病棟病室ベッド番号は、転棟を考慮するため、inptmstを見ない。
    // 同月にベッドだけ移動しても、複数行表示させないことに注意。
    // 病棟病室ベッド番号は、指定された月の末日最終時点の場所ということで判断する。
    // ★★★転棟情報inptmoveで判定
    //--------------------------------------------
    if ($ward_or_ptif=="ward") {
        $yyyymm = $_REQUEST["date_y"].sprintf("%02d", $_REQUEST["date_m"]); // 画面指定された入院年月
        $def_yyyymm = ($list_mode=="screening" ? $yyyymm : "000000"); // 栄養管理テーブル上の年月。スクリーニング一覧では入院年月。計画書なら"000000"
        $dd = cal_days_in_month(CAL_GREGORIAN, (int)$_REQUEST["date_m"], (int)$_REQUEST["date_y"]);
        $lastday = "'".$yyyymm.sprintf("%02d",$dd)."'";
        $firstday = "'".$yyyymm."01'";
        $zengetu_yyyymm = ($list_mode=="screening" ? (int)$def_yyyymm - (int)(substr($def_yyyymm, 4)=="01" ? (100-11) : 1) : "000000"); // $def_yyyymmの前月

        $sel_ptrm_room_no_list = array();
        $ary = explode(",", $_REQUEST["sel_ptrm_room_no_list"]);
        foreach ($ary as $a) if (strlen($a)) $sel_ptrm_room_no_list[] = $a;
        $sel_ptrm_room_no_list[]= -1;


        //--------------------------------------------
        // 【通常一覧その１】
        // すべての入院情報を、「入院日＋患者ID」キーで収集
        //--------------------------------------------
        $asql = array(
        " select inpt.*, ptrm.ptrm_name, ptm.ptif_birth, dr.dr_nm",
        ",case ptm.ptif_sex when '1' then '男' when '2' then '女' else '' end as gender",
        ",ses.yyyymm, ses.mokuhyou1, ses.mokuhyou2, ses.mokuhyou3, ses.sikkan, ses.eiyou_hokyuho, ses.syusyoku, ses.fukusyoku, ses.keikan_eiyouzai",
        ",ses.shincyou, ses.taiju, ses.pp01_bmi, ses.pp02_ibw, ses.pp03_ibmi, ses.pp04_ree, ses.pp05_action, ses.pp06_stress, ses.pp07_h_energy",
        ",ses.pp08_h_protein, ses.pp09_h_h2o, ses.pp10_t_energy, ses.pp11_t_protein, ses.pp12_t_enbun, ses.pp13_t_h2o, ses.pp14_tp, ses.pp15_alb",
        ",ses.pp16_alb_score, ses.pp17_hb, ses.pp18_wbc, ses.pp19_lymph, ses.pp20_tlc, ses.pp21_tlc_score, ses.pp22_tcho, ses.pp23_tcho_score, ses.pp24_conut",
        ",ses.sonota, ses.conference_ymd, ses.kentou_naiyou, ses.kettei_naiyou, ses.update_timestamp, ses.update_emp_id, ses.update_emp_name",
        ",ses.pp25_h_protein_f, ses.pp26_h_h2o_f, ses.pp27_kasan, ses.tsuikahin",
        ",ses.ptif_id as ses_ptif_id",
        ",ses_before.taiju as tai_zengetu",
        " from (",
        "     select xx.ptif_id, xx.inpt_in_dt, xx.ptif_name, xx.inpt_out_dt",
        //■ 転床がなければhist/mstの値を返す。転床があれば、直近未来の転床情報の、FROM値を返すようにする。
        "    ,case when mv.ptif_id is null then xx.bldg_cd else mv.from_bldg_cd end as bldg_cd",
        "    ,case when mv.ptif_id is null then xx.ward_cd else mv.from_ward_cd end as ward_cd",
        "    ,case when mv.ptif_id is null then xx.ptrm_room_no else mv.from_ptrm_room_no end as ptrm_room_no",
        "    ,case when mv.ptif_id is null then xx.inpt_bed_no else mv.from_bed_no end as inpt_bed_no",
        "    ,case when mv.ptif_id is null then xx.inpt_enti_id else mv.from_enti_id end as inpt_enti_id",
        "    ,case when mv.ptif_id is null then xx.inpt_sect_id else mv.from_sect_id end as inpt_sect_id",
        "    ,case when mv.ptif_id is null then xx.dr_id else mv.from_dr_id end as dr_id",
        //■ 入院レコード総合。「退院済み」と「入院中」は月末23:59時点では重複しないので、患者ごとに0or1件となる。
        "     from (",
        //■ 月末時点の「入院中レコード」。入院中テーブル自体が、患者ごとに0or1件
        "         select ptif_id, inpt_in_dt, inpt_in_tm, '99999999' as inpt_out_dt, '9999' as inpt_out_tm".
        "         ,bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no, inpt_enti_id, inpt_sect_id, dr_id",
        "         ,inpt_lt_kj_nm || ' ' || inpt_ft_kj_nm as ptif_name",
        "         from inptmst",
        "         where (inpt_in_dt <= ".$lastday." and ptif_id is not null)",
        "",
        "         union",
        "",
        //■ 対象月にまたがる「退院済みレコード」の最終エントリのうち、対象月に「入院レコード」が無い患者の詳細。前処理と重複しない。患者ごとに0or1件
        "         select h1.ptif_id, h1.inpt_in_dt, h1.inpt_in_tm, h1.inpt_out_dt, h1.inpt_out_tm".
        "         ,h1.bldg_cd, h1.ward_cd, h1.ptrm_room_no, h1.inpt_bed_no, h1.inpt_enti_id, h1.inpt_sect_id, h1.dr_id",
        "         ,h1.inpt_lt_kj_nm || ' ' || h1.inpt_ft_kj_nm as ptif_name",
        "         from inpthist h1",
        "         inner join (",
        //■ 対象月にまたがる「退院済みレコード」の最終エントリ。患者ごとに0or1件
        "             select ptif_id, max(inpt_in_dttm) as last_inpt_in_dttm from (",
        //■ 対象月にまたがる「退院済みレコード」患者ごとに0〜複数件
        "                 select ptif_id, inpt_in_dt || inpt_in_tm as inpt_in_dttm from inpthist",
        "                 where (inpt_in_dt <= ".$lastday." and inpt_out_dt >= ".$firstday.")",
        "             ) d",
        "             group by ptif_id",
        "         ) h2 on (h2.ptif_id = h1.ptif_id and h2.last_inpt_in_dttm = h1.inpt_in_dt || h1.inpt_in_tm)",
        //■ 対象月より過去から開始している「入院中レコード」。入院中テーブル自体が、患者ごとに0or1件
        "         left outer join inptmst im on (",
        "             im.ptif_id = h1.ptif_id and im.inpt_in_dt <= ".$lastday." and im.ptif_id is not null",
        "         )",
        "         where im.ptif_id is null",
        "     ) xx",
        //■ 転床情報ジョイン／ 翌月以降の転床情報のうち、直近未来のものを、所定入院に紐付けて、入院との「1対1」を作成する。
        "     left outer join (",
        // ■ 転床情報詳細／翌月以降の転床情報のうち、直近未来のもの。患者ごとに0or1件
        "         select mv1.ptif_id, mv1.move_dt, mv1.move_tm, mv1.from_bldg_cd, mv1.from_ward_cd, mv1.from_ptrm_room_no, mv1.from_bed_no",
        "         ,mv1.from_enti_id, mv1.from_sect_id, mv1.from_dr_id".
        "         from inptmove mv1",
        "         inner join (",
        // ■ 転床情報キー／翌月以降の転床情報のうち、直近未来のもの。患者ごとに0or1件
        "             select ptif_id, min(move_dttm) as next_move_dttm from (",
        "                 select ptif_id, move_dt || move_tm as move_dttm from inptmove where move_dt > ".$lastday." and move_cfm_flg",
        "             ) d",
        "             group by ptif_id",
        "         ) mv2 on (mv2.ptif_id = mv1.ptif_id and mv2.next_move_dttm = mv1.move_dt || mv1.move_tm)",
        "     ) mv on (",
        "         mv.ptif_id = xx.ptif_id",
        "         and (xx.inpt_in_dt < mv.move_dt or (xx.inpt_in_dt=mv.move_dt and xx.inpt_in_tm <= mv.move_tm))",
        "         and (mv.move_dt < xx.inpt_out_dt or (mv.move_dt=xx.inpt_out_dt and mv.move_tm < xx.inpt_out_tm))",
        "     )",
        " ) inpt",
        " inner join ptifmst ptm on (ptm.ptif_id = inpt.ptif_id)",
        " left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)",
        " left outer join drmst dr on (dr.enti_id = inpt.inpt_enti_id and dr.sect_id = inpt.inpt_sect_id and dr.dr_id = inpt.dr_id)",
        " left outer join sum_eiyou_screening ses on (",
        "     ses.ptif_id = inpt.ptif_id and ses.inpt_in_dt = inpt.inpt_in_dt and ses.yyyymm = '".$def_yyyymm."'",
        " )",
        " left outer join sum_eiyou_screening ses_before on (",
        "     ses_before.ptif_id = inpt.ptif_id and ses_before.inpt_in_dt = inpt.inpt_in_dt and ses_before.yyyymm = '".$zengetu_yyyymm."'",
        " )",
        " where 1 = 1"
        );
        if ($_REQUEST["sel_bldg_cd"]) $asql[] = " and inpt.bldg_cd = ".$_REQUEST["sel_bldg_cd"];
        if ($_REQUEST["sel_ward_cd"]) {
            $asql[] = " and inpt.ward_cd = ".$_REQUEST["sel_ward_cd"];
            $asql[] = " and inpt.ptrm_room_no in (".implode(",",$sel_ptrm_room_no_list).")";
        }
        $asql[] = " order by inpt.bldg_cd, inpt.ward_cd, ptrm.ptrm_name, inpt.inpt_bed_no";
        $sel = sql_select(implode("\n",$asql));
        $raw_rows = @pg_fetch_all($sel);

        if (!$raw_rows) {
            echo "該当患者は存在しません。";
            die;
        }
        // データを集めつつ、初期値をセット
        $sort_rows = array();
        foreach ($raw_rows as $row) {
            $row["inpt_out_dt"] = trim($row["inpt_out_dt"]); // char型だけに空白８個になってしまう
            $row["inpt_in_dt"] = trim($row["inpt_in_dt"]); // char型だけに空白８個になってしまう
            if ($row["ses_ptif_id"]=="") {
                $row["yyyymm"] = $def_yyyymm;
            } else {
                $row["is_exist_data"] = "1";
            }
            $sort_rows[$row["ptif_id"]] = $row;
        }
    }


    // 患者別一覧の場合
    if ($ward_or_ptif=="ptif") {
        //--------------------------------------------
        // ★★★ 患者別一覧では、取得したいのは以下の３種類のデータのマージです。
        // １）データ有無に関わらず、画面で指定した年月範囲に対し、月ごとに行を準備
        // ２）プラス、データ有無に関わらず、計画書の行を準備
        // ３）ただし、最大範囲年月は、入院月〜退院月とする。
        // ４）ただし、最大範囲年月範囲内だが入院月〜退院月に収まらないデータが登録済なら、それは表示する
        //     （例えばデータ登録後に、別画面で入退院情報を変更された場合など）
        //
        //  ※計画書というものは、運用上では「入院日の年月にひとつ」作成するのだが、システム的には、それはあまりどうでもよい。
        //    本システム上、「とある入院日があって、それに紐ついた年月"000000"のデータ」が計画書データのあり方である。
        //--------------------------------------------

        //--------------------------------------------
        // 【患者別一覧その１】
        // 画面引数値精査
        //--------------------------------------------
        $inpt_in_dt = $_REQUEST["inpt_in_dt"]; // 患者の入院日
        $yyyymm_to = (int)($_REQUEST["date_y_to"].sprintf("%02d", $_REQUEST["date_m_to"])); // 画面入力した年月。intにしておく。
        $kikan_ym = $_REQUEST["kikan_ym"]; // 過去何ヶ月間かの値。ちなみに計算便宜上、１年は「100」で渡ってきます。過去すべては「all」です。
        $yyyymm_fr = $yyyymm_to; // 遡った期間の初期値は、画面入力年月。
        if ($kikan_ym=="all") $yyyymm_fr = max(substr($inpt_in_dt,0,6), 197001); // allなら、入院日と1970年の大きい方を、一番過去の年月とする。
        else if ($kikan_ym!="") { // all以外なら
            $yyyymm_fr = $yyyymm_fr - $kikan_ym; // 年月を差し引いた年月を、一番過去の年月とする。
            if ($yyyymm_fr % 100 > 12) $yyyymm_fr = $yyyymm_fr - 100 + 12; // 現状この行には到達しないが、念の為。
        }
        $yyyymm_fr = (int)$yyyymm_fr; // intにしておく。

        //--------------------------------------------
        // 【患者別一覧その２】
        // プルダウン用現在過去の入退院情報
        //--------------------------------------------
        $ptif_name = "";
        $inpt_out_dt = "";
        $sql =
        " select inpt_in_dt, inpt_out_dt, inpt_lt_kj_nm || ' ' || inpt_ft_kj_nm as ptif_name".
        " from inpthist where ptif_id = '".$_REQUEST["ptif_id"]."' order by inpt_in_dt";
        $inpt_io_list = array();
        $sel = sql_select($sql);
        while ($row = pg_fetch_array($sel)){
            $in_dt_disp = substr($row["inpt_in_dt"], 0, 4)."/".substr($row["inpt_in_dt"], 4,2)."/".substr($row["inpt_in_dt"],6);
            $out_dt_disp = substr($row["inpt_out_dt"], 0, 4)."/".substr($row["inpt_out_dt"], 4,2)."/".substr($row["inpt_out_dt"],6);
            if (!$row["inpt_out_dt"]) $out_dt_disp = "";
            $inpt_io_list[] = trim($row["inpt_in_dt"])."-".$in_dt_disp." 〜 ".$out_dt_disp;
            if ($_REQUEST["inpt_in_dt"] == trim($row["inpt_in_dt"])) {
                $ptif_name = $row["ptif_name"];
                $inpt_out_dt = $row["inpt_out_dt"];
            }
        }
        $sql = "select inpt_in_dt, inpt_lt_kj_nm || ' ' || inpt_ft_kj_nm as ptif_name from inptmst where ptif_id = '".$_REQUEST["ptif_id"]."'";
        $sel = sql_select($sql);
        while ($row = pg_fetch_array($sel)){
            $in_dt_disp = substr($row["inpt_in_dt"], 0, 4)."/".substr($row["inpt_in_dt"], 4,2)."/".substr($row["inpt_in_dt"],6);
            $inpt_io_list[] = trim($row["inpt_in_dt"]."-".$in_dt_disp. "〜");
            if ($_REQUEST["inpt_in_dt"] == trim($row["inpt_in_dt"])) {
                $ptif_name = $row["ptif_name"];
                $inpt_out_dt = $row["inpt_out_dt"];
            }
        }
        //--------------------------------------------
        // 【患者別一覧その３】
        // 患者の誕生日と性別
        //--------------------------------------------
        $sql =
        " select ptif_birth, case ptif_sex when '1' then '男' when '2' then '女' else '' end as gender from ptifmst".
        " where ptif_id = '".$_REQUEST["ptif_id"]."'";
        $sel = sql_select($sql);
        $gender = trim(@pg_result($sel,0,"gender"));
        $ptif_birth = trim(@pg_result($sel,0,"ptif_birth"));

        //--------------------------------------------
        // 【患者別一覧その４】
        // 栄養管理情報を収集してマージ
        // yyyymmがゼロの行は、計画書の行であることを示す。
        //--------------------------------------------
        $sort_rows = array();
        $sql =
        " select ses.*, 1 as is_exist_data".
        " from sum_eiyou_screening ses".
        " where 1 = 1".
        " and (ses.yyyymm='000000' or (ses.yyyymm >= '".$yyyymm_fr."' and ses.yyyymm <= '".$yyyymm_to."'))".
        " and ses.ptif_id = '".$_REQUEST["ptif_id"]."'".
        " and ses.inpt_in_dt = '".$_REQUEST["inpt_in_dt"]."'";
        $sel = sql_select($sql);
        while ($row = pg_fetch_array($sel)){
            $sort_rows[$row["yyyymm"]] = $row; // 配列に格納
        }

        //--------------------------------------------
        // 【患者別一覧その５】
        // 過去から対象月までの各月の月末時点の主治医と病室を取得
        //--------------------------------------------
        $ptrm_dr_list = array();
        $sql =
        " select im.inpt_in_dt, im.ptrm_room_no, ptrm.ptrm_name, dr.dr_nm from inptmst im".
        " left outer join ptrmmst ptrm on (ptrm.bldg_cd = im.bldg_cd and ptrm.ward_cd = im.ward_cd and ptrm.ptrm_room_no = im.ptrm_room_no)".
        " left outer join drmst dr on (dr.enti_id = im.inpt_enti_id and dr.sect_id = im.inpt_sect_id and dr.dr_id = im.dr_id)".
        " where im.ptif_id = '".$_REQUEST["ptif_id"]."'";
        $sel = sql_select($sql);
        while ($row = pg_fetch_array($sel)) {
            $ptrm_dr_list[$row["inpt_in_dt"]] = array("ptrm_name"=>$row["ptrm_name"], "dr_nm"=>$row["dr_nm"], "ptrm_room_no"=>$row["ptrm_room_no"]);
        }
        $sql =
        " select ih.inpt_in_dt, ih.ptrm_room_no, ptrm.ptrm_name, dr.dr_nm from inpthist ih".
        " left outer join ptrmmst ptrm on (ptrm.bldg_cd = ih.bldg_cd and ptrm.ward_cd = ih.ward_cd and ptrm.ptrm_room_no = ih.ptrm_room_no)".
        " left outer join drmst dr on (dr.enti_id = ih.inpt_enti_id and dr.sect_id = ih.inpt_sect_id and dr.dr_id = ih.dr_id)".
        " where ih.ptif_id = '".$_REQUEST["ptif_id"]."'";
        $sel = sql_select($sql);
        while ($row = pg_fetch_array($sel)) {
            $ptrm_dr_list[$row["inpt_in_dt"]] = array("ptrm_name"=>$row["ptrm_name"], "dr_nm"=>$row["dr_nm"], "ptrm_room_no"=>$row["ptrm_room_no"]);
        }
        $sql =
        " select im.move_dt, im.to_ptrm_room_no, ptrm.ptrm_name, dr.dr_nm from inptmove im".
        " left outer join ptrmmst ptrm on (ptrm.bldg_cd = im.to_bldg_cd and ptrm.ward_cd = im.to_ward_cd and ptrm.ptrm_room_no = im.to_ptrm_room_no)".
        " left outer join drmst dr on (dr.enti_id = im.to_enti_id and dr.sect_id = im.to_sect_id and dr.dr_id = im.to_dr_id)".
        " where im.ptif_id = '".$_REQUEST["ptif_id"]."'";
        $sel = sql_select($sql);
        while ($row = pg_fetch_array($sel)) {
            $ptrm_dr_list[$row["move_dt"]] = array("ptrm_name"=>$row["ptrm_name"], "dr_nm"=>$row["dr_nm"], "ptrm_room_no"=>$row["ptrm_room_no"]);
        }
        ksort($ptrm_dr_list);

        //--------------------------------------------
        // 【患者別一覧その６】
        // 対象期間の、未登録である歯抜け月を埋める。データがなければ追加。
        //--------------------------------------------
        //$fields = array("ptif_id", "ptif_name", "inpt_in_dt", "inpt_out_dt", "gender", "ptif_birth");
        $ym_fr = "000000"; // スタート年月
        $ym_to = $yyyymm_to; // エンド年月
        $ym_nyuin = substr($inpt_in_dt, 0, 6); // 入院年月
        $ym_taiin = ($inpt_out_dt ? substr($inpt_out_dt, 0, 6) : "999999"); // 退院年月
        $ymd_taiin = ($inpt_out_dt ? $inpt_out_dt : "99999999"); // 退院年月日
        $ym = $ym_fr;
        $tai_zengetu = "";
        for (;;) {
            if ($ym > $ym_to) break; // ループ年月がエンド年月を超えた。終了。
            if (($ym_nyuin <= $ym && $ym <= $ym_taiin) || $ym=="000000") { // ループ年月が入院から退院までの間である場合
                $sort_rows[$ym]["yyyymm"] = $ym; // 年月はループ年月をセット
                $sort_rows[$ym]["ptif_id"] = $_REQUEST["ptif_id"];
                $sort_rows[$ym]["ptif_name"] = $ptif_name;
                $sort_rows[$ym]["inpt_in_dt"] = $_REQUEST["inpt_in_dt"];
                $sort_rows[$ym]["inpt_out_dt"] = $inpt_out_dt;
                $sort_rows[$ym]["gender"] = $gender;
                $sort_rows[$ym]["ptif_birth"] = $ptif_birth;
            }
            if ($ym=="000000") { $ym = $yyyymm_fr; continue; }
            if ($ym_nyuin <= $ym && $ym <= $ym_taiin) { // ループ年月が入院から退院までの間である場合
                foreach ($ptrm_dr_list as $in_ymd => $tmp_row) {
                    if ($in_ymd < $inpt_in_dt || $ymd_taiin < $in_ymd) continue; // この入退院の期間でなければスルー
                    $last_ymd = date(substr($ym,0,4)."-".substr($ym,4)."-t");
                    if ($in_ymd < $last_ymd) continue;
                    $sort_rows[$ym]["ptrm_name"] = $tmp_row["ptrm_name"];
                    $sort_rows[$ym]["dr_nm"] = $tmp_row["dr_nm"];
                    $sort_rows[$ym]["ptrm_room_no"] = $tmp_row["ptrm_room_no"];
                    break;
                }
            }
            $sort_rows[$ym]["tai_zengetu"] = $tai_zengetu;
            $tai_zengetu = $sort_rows[$ym]["taiju"];
            $ym = $ym + 1; // ループ年月をインクリメント
            if ($ym % 100 > 12) $ym = $ym + 100 - 12; // 12月を越えたら翌年1月とする。
        }
        krsort($sort_rows); // ソート実行。年月降順とする。
        //--------------------------------------------
        // 定型文テンプレートでの表示でなければ、計画書は末尾から先頭に持ってくる。
        //--------------------------------------------
        if (!$_REQUEST["get_from_soap_teikeibun_template"]) {
            $last_row = array_pop($sort_rows);
            array_unshift($sort_rows, $last_row);
        }
    }



    //--------------------------------------------
    // 【一覧表示その２】
    // $sort_rows一覧から、配列のキーを変えて、$rowsにデータを移す。
    // この配列のキー（trkey）はブラウザ側でも多く参照されるものである。
    //--------------------------------------------
    $rows = array();
    foreach ($sort_rows as $row) {
        $trkey = '"k'.$row["inpt_in_dt"]."@".$row["yyyymm"]."@".$row["ptif_id"].'"';
        $rows[$trkey] = $row;
    }

    //--------------------------------------------
    // 【一覧表示その３】
    // 患者年齢を計算し、「age」として配列フィールドを追加
    // スクリーニングの場合は、yyyymmのついたち時点の年齢を。
    // 計画書の場合は、入院日時点の年齢を計算する。
    //--------------------------------------------
    foreach($rows as $trkey => $row) {
        if ($rows[$trkey]["ptif_birth"]) {
            if ($row["yyyymm"]=="000000") {
                $ymd = $row["inpt_in_dt"];
            } else {
                $yy = substr($row["yyyymm"],0,4);
                $mm = substr($row["yyyymm"],4);
                $ymd = $row["yyyymm"]. date("t", mktime(0, 0, 0, $mm, 1, $yy));
            }
            $rows[$trkey]["age"] = get_age($rows[$trkey]["ptif_birth"], $ymd);
        }
    }

    //--------------------------------------------
    // 【一覧表示その４】
    // もし「一括引用」を押してリクエストしたなら、「$append_ref」フラグが"1"になっているはずである。
    // この場合、テンプレートやマスタデータから、引用値を取得する。
    //--------------------------------------------
    $append_ref_output = array();
    $append_ref_count = 0;
    if ($append_ref) {
        // 食事箋のテンプレートIDを取得しておく
        $TEMPLATE_FILE = "tmpl_MealStart.php";
        $sel = sql_select("select tmpl_id from tmplmst where tmpl_file = '".$TEMPLATE_FILE."'");
        $rows2 = @pg_fetch_all($sel);
        $tmpl_meal_start_ids = array();
        if ($rows2) {
            foreach ($rows2 as $row) $tmpl_meal_start_ids[] = $row["tmpl_id"];
        }
        // 体重集計表のテンプレートIDを取得しておく。
        $TEMPLATE_FILE = "tmpl_WeightCheck.php";
        $sel = sql_select("select tmpl_id from tmplmst where tmpl_file = '".$TEMPLATE_FILE."'");
        $rows2 = @pg_fetch_all($sel);
        $tmpl_weight_check_ids = array();
        if ($rows2) {
            foreach ($rows2 as $row) $tmpl_weight_check_ids[] = $row["tmpl_id"];
        }
        //================================================================
        // 引用値が空欄であるセルや、対象外のセルについては、画面上の「未保存データ」があればセットする
        //================================================================
        $req = decode_request(); // ★★★ POST値を取得
        foreach ($req as $k => $v) {
            if (!strlen($v)) continue;
            list ($_inpt_in_dt, $_yyyymm, $_ptif_id, $_field) = explode("@",$k);
            $_trkey = '"k'.$_inpt_in_dt."@".$_yyyymm."@".$_ptif_id.'"';
            if (!strlen($append_ref_output[$_trkey][$_field])) $append_ref_output[$_trkey][$_field] = $v;
        }
        // 表示期間年月あるいは職員をループし、月ごとに引用内容を取得してゆく。
        foreach($rows as $trkey => $row) {
            $append_ref_output[$trkey] = get_master_ref(
                $row["ptif_id"], $row["inpt_in_dt"], $row["yyyymm"], $tmpl_meal_start_ids,
                $tmpl_weight_check_ids, $append_ref_output[$trkey], $append_ref_count
            );
        }
    }

    //--------------------------------------------
    // 【一覧表示その５】
    // このリクエストでは不要な列を除去しつつ、列定義を取得する。
    //--------------------------------------------
    $field_info = array();
    for ($idx=0; $idx<count($FIELD_INFO); $idx++) {
        $field = $FIELD_INFO[$idx]["field"];
        if ($field=="inpt_in_dt") {
            if ($ward_or_ptif!="ward") continue; // 入院日の列は、通常一覧のみ必要。
            if ($list_mode=="screening") continue; // 入院日の列は、スクリーニング一覧の場合は不要。
        }
        if ($field=="ptif_id" || $field=="ptif_name" || $field=="ptrm_name") {
            if ($ward_or_ptif!="ward") continue; // 患者IDか患者名の列は、通常一覧のみ必要。
        }
        if ($field=="yyyymm") {
            if ($ward_or_ptif!="ptif") continue; // 年月の列は、患者別一覧では不要
        }
        $field_info[] = $FIELD_INFO[$idx];
    }


    //--------------------------------------------
    // 【一覧表示その６】
    // 定型文テンプレートから「栄養管理」ボタンを押した場合。
    // 以下の画面HTMLを出力して終了する。
    // この小さめのダイアログ画面は、ダイアログ画面上の年月リンクを押しても再度呼ばれる。
    //--------------------------------------------
    if ($_REQUEST["get_from_soap_teikeibun_template"]) {
        require_once("./get_values.ini");
        require_once("summary_common.ini");
        require_once("get_menu_label.ini");

        echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        echo '<html xmlns="http://www.w3.org/1999/xhtml">';
        echo '<head>';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">';
        echo '<link rel="stylesheet" type="text/css" href="css/main_summary.css">';
        echo '<title>CoMedix '.get_report_menu_label($con, $fname).'｜栄養管理</title>';
        echo '<script type="text/javascript">';
        // 年月リンククリック処理。自分URLを呼ぶ。
        echo '  function loadContent(yyyymm) {';
        echo ('      location.href = "summary_eiyou_kanri.php?session='.$session.
                    '&ptif_id='.$_REQUEST["ptif_id"].
                    '&inpt_in_dt='.$_REQUEST['inpt_in_dt'].
                    '&callback_func='.$_REQUEST["callback_func"].
                    '&get_from_soap_teikeibun_template=1'.
                    '&date_y_to='.date("Y").
                    '&date_m_to='.date("m").
                    '&kikan_ym=all'.
                    '&ajax_mode=exec_load'.
                    '&view_mode=ptif_mode'.
                    '&yyyymm="+yyyymm;' // クリック時の関数の引数の年月
        );
        echo '  }';
        // コピーボタンクリック処理。呼び出し元に指定されたコールバック関数名を、関数として呼ぶ。
        echo '  function copyContent() {';
        echo '      window.opener.'.$_REQUEST["callback_func"].'(document.getElementById("div_content").innerHTML);';
        echo '  }';
        echo '</script>';
        echo '</head>';
        echo '<body>';
        echo '<div style="padding:4px">';
        summary_common_show_dialog_header("栄養管理");
        echo '<div id="main_content" style="padding-top:10px; height:540px">';

        if (!$rows || !count($rows)) {
            echo "栄養管理情報はありません。";
        } else {
            $yyyymm = $_REQUEST["yyyymm"];
            echo '<table cellspacing="0" cellpadding="0" style="height:100%"><tr><td style="vertical-align:top; background-color:#dddddd; height:100%">';
            echo '<div style="overflow-y:scroll; height:100%">';

            echo '<table cellspacing="0" cellpadding="0" class="listp_table">';
            echo '<tr><th style="padding:2px; width:70px"">年月</th></tr>';
            $ridx = 0;
            $first_row = 0;
            foreach ($rows as $row){
                if (!$row["is_exist_data"]) continue;
                $ridx++;

                if ($yyyymm==$row["yyyymm"] || (!$yyyymm && !$target_row)) $target_row = $row;

                $trcls = ($row["yyyymm"]=="000000" ? "keikakusyo" : "screening");
                echo '<tr id="k'.$row["inpt_in_dt"]."@".$row["yyyymm"]."@".$row["ptif_id"].'" class="'.$trcls.'">';
                echo '<td style="padding:2px 6px"><a href="javascript:void(0)" onclick="loadContent(\''.$row["yyyymm"].'\')">';
                if ($row["yyyymm"]=="000000") echo '(計画書)';
                else echo substr($row["yyyymm"],0,4) ."/" . substr($row["yyyymm"],4);
                echo '</a></td>';
                echo '</tr>';
            }
            echo "</table>";
            echo '</div></td><td style="padding-left:10px; vertical-align:top">';
            $divs = '';
            $dive = '<br>';
            if ($target_row) {
                echo '<div style="padding-bottom:4px">';
                echo '<button onclick="copyContent()">コピー</button>';
                echo'</div>';
                echo '<div style="width:350px; height:506px; overflow-y:scroll; border:1px solid #9bc8ec; padding:4px">';
                echo '<div id="div_content">';
                if ($target_row["yyyymm"]=="000000") echo '【栄養管理計画書】<br>';
                else echo "【栄養スクリーニング（".substr($target_row["yyyymm"],0,4) ."/" . substr($target_row["yyyymm"],4)."）】<br>";

                $html = array();
                $html[]= $divs.h("[栄養管理上の目標]").$dive;
                if (svchk($target_row["mokuhyou1"]) || svchk($target_row["mokuhyou1"]) || svchk($target_row["mokuhyou1"])) {
                    $html[]= $divs.h("＃１、".$target_row["mokuhyou1"]).$dive;
                    $html[]= $divs.h("＃２、".$target_row["mokuhyou2"]).$dive;
                    $html[]= $divs.h("＃３、".$target_row["mokuhyou3"]).$dive;
                }
                $html[]= $divs;
                if (svchk($target_row["sikkan"])) $html[]= h("疾患：".$target_row["sikkan"]."　　");
                if ($target_row["pp27_kasan"]) $html[]= "加算";
                $html[]= $dive;
                if (svchk($target_row["eiyou_hokyuho"])) $html[]= $divs.h("栄養補給法：".$target_row["eiyou_hokyuho"]).$dive;

                $ary = array();
                if (svchk($target_row["syusyoku"])) $ary[1][]= h("主食：".$target_row["syusyoku"]);
                if (svchk($target_row["fukusyoku"])) $ary[1][]= h("副食：".$target_row["fukusyoku"]);
                if (svchk($target_row["tsuikahin"])) $ary[1][]= h("追加品：".$target_row["tsuikahin"]);
                if (count($ary[1])) $html[]= $divs.implode("　　", $ary[1]).$dive;

                if (svchk($target_row["keikan_eiyouzai"])) $html[]= $divs.h("経管栄養剤：".$target_row["keikan_eiyouzai"]).$dive;

                if (svchk($target_row["shincyou"])) $ary[2][]= h("身長".$target_row["shincyou"]."cm");
                if (svchk($target_row["taiju"])) $ary[2][]= h("体重".$target_row["taiju"]."kg");
                if (svchk($target_row["pp01_bmi"])) $ary[2][]= h("BMI".$target_row["pp01_bmi"]."kg/m")."<sup>2</sup>";
                if (count($ary[2])) $html[]= $divs.implode("　", $ary[2]).$dive;

                if (svchk($target_row["pp02_ibw"])) $ary[3][]= h("目標体重".$target_row["pp02_ibw"]."kg");
                if (svchk($target_row["pp03_ibmi"])) $ary[3][]= h("（目標BMI）".$target_row["pp03_ibmi"]."kg/m")."<sup>2</sup>";
                if (svchk($target_row["pp05_action"])) $ary[3][]= h("活動係数：".$target_row["pp05_action"]);
                if (svchk($target_row["pp05_action"])) $ary[3][]= h("ストレス係数：".$target_row["pp06_stress"]);
                if (count($ary[3])) $html[]= $divs.implode("　　", $ary[3]).$dive;

                if (svchk($target_row["pp07_h_energy"])) $ary[4][]= h("必要エネルギー".$target_row["pp07_h_energy"]."Kcal");
                if (svchk($target_row["pp08_h_protein"])) $ary[4][]= h("たんぱく質".$target_row["pp08_h_protein"]."g");
                if (svchk($target_row["pp09_h_h2o"])) $ary[4][]= h("水分".$target_row["pp09_h_h2o"]."ml");
                if (count($ary[4])) $html[]= $divs.implode("　　", $ary[4]).$dive;

                if (svchk($target_row["pp10_t_energy"])) $ary[5][]= h("提供エネルギー".$target_row["pp10_t_energy"]."Kcal");
                if (svchk($target_row["pp11_t_protein"])) $ary[5][]= h("たんぱく質".$target_row["pp11_t_protein"]."g");
                if (svchk($target_row["pp13_t_h2o"])) $ary[5][]= h("水分".$target_row["pp13_t_h2o"]."ml");
                if (svchk($target_row["pp12_t_enbun"])) $ary[5][]= h("塩分".$target_row["pp12_t_enbun"]."g未満");
                if (count($ary[5])) $html[]= $divs.implode("　　", $ary[5]).$dive;

                if (svchk($target_row["pp14_tp"])) $ary[6][]= h("TP".$target_row["pp14_tp"]."g/dl");
                if (svchk($target_row["pp15_alb"])) $ary[6][]= h("Alb".$target_row["pp15_alb"]."g/dl");
                if (svchk($target_row["pp17_hb"])) $ary[6][]= h("Hb".$target_row["pp17_hb"]."g/dl");
                if (count($ary[6])) $html[]= $divs.implode("　　", $ary[6]).$dive;

                if (svchk($target_row["pp24_conut"])) $html[]= $divs.h("CONUT".$target_row["pp24_conut"]."点").$dive;
                if (svchk($target_row["sonota"])) $html[]= $divs.h("その他：".$target_row["sonota"]).$dive;
                if (svchk($target_row["conference_ymd"])) $html[]= $divs.h("カンファレンス実施日：".$target_row["conference_ymd"]).$dive;
                if (svchk($target_row["kentou_naiyou"])) $html[]= $divs.h("検討内容：".$target_row["kentou_naiyou"]).$dive;
                if (svchk($target_row["kettei_naiyou"])) $html[]= $divs.h("決定内容：".$target_row["kettei_naiyou"]).$dive;

                echo implode("", $html);
                echo "</div></div>";
            }
            echo "</td></tr></table>";
        }
        echo '</div>';
        echo '</div>';
        echo '</body>';
        echo '</html>';
        die;
    }




    //--------------------------------------------
    // 【一覧表示その７】
    // 「Excel」ボタンを押した場合。エクセル出力を行う。PHP5.1.6のみ。
    //  ※PHP5.1.6以下の場合、そもそもExcelボタンが押せないよう、画面側で制御されている。HTML側を参照のこと。「phpversion」で検索。
    //--------------------------------------------
    if ($_REQUEST["download_excel"]) {
        require_once './libs/phpexcel/Classes/PHPExcel.php';

        //--------------------------------------------------
        // 【Excelその１】
        // 出力準備
        //--------------------------------------------------
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $xxds = $excel->getDefaultStyle();
        $xxft = $xxds->getFont();
        $xxft->setName(utf8('ＭＳ Ｐゴシック')); // 標準フォント設定
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle(utf8(substr($yyyymm,0,4)."年".substr($yyyymm,4)."月"));

        // フォーミュラ定義を、列IDをキーにして格納しなおしておく。
        $formulaTargets = array();
        $xlsABC = array();
        foreach($formulaInfo as $formula) {
            $formulaTargets[$formula["target"]] = $formula;
        }

        //--------------------------------------------------
        // 【Excelその２】
        // １行目作成
        //--------------------------------------------------
        $cidx = -1;
        $xlsABC = array();
        foreach ($field_info as $idx => $finfo) {
            if (!$finfo["xlsout"]) continue;
            $cidx++;
            $xlsABC[$finfo["field"]] = xlsAlpha($cidx);
            $sheet->setCellValueByColumnAndRow($cidx, 1, utf8($finfo["jp"]));
            $sty = $sheet->getStyleByColumnAndRow($cidx, 1);
            $field = $finfo["field"];
            $formulaObj = $formulaTargets[$field];
            if ($formulaObj["formula"]) {
                $fill = $sty->getFill();
                $fill->setFillType("solid");
                $scolor = $fill->getStartColor();
                $scolor->setRGB("FFFF00");
            }
            // 縦エリア
            $area = $sheet->getStyle(xlsAlpha($cidx)."2:".xlsAlpha($cidx).(count($rows)+1));

            // 縦列フォーマット
            if ($formulaObj["xls_format"]) {
                $xxnf = $area->getNumberFormat();
                $xxnf->setFormatCode(utf8($formulaObj["xls_format"]));
            }
        }
        //--------------------------------------------------
        // 【Excelその３】
        // データ行作成
        //--------------------------------------------------
        $ridx = 1;
        foreach ($rows as $row){
            $ridx++;
            $cidx = -1;
            foreach ($field_info as $idx => $finfo) {
                if (!$finfo["xlsout"]) continue;
                $field = $finfo["field"];
                $cidx++;

                // フォーミュラの有無
                $str = "";
                if ($formulaTargets[$field]["formula"]) {
                    $formulaObj = $formulaTargets[$field];
                    $formula = $formulaObj["xls_formula"];
                    if (!$formula) $formula  = $formulaObj["formula"];
                    foreach ($formulaObj["base"] as $ff) $formula = str_replace($ff, $xlsABC[$ff].$ridx, $formula);
                    $sheet->setCellValueByColumnAndRow($cidx, $ridx, "=".utf8($formula));
                } else {
                    $sheet->setCellValueByColumnAndRow($cidx, $ridx, utf8(trim($row[$field])));
                    if (strpos($row[$field], "\n")!==FALSE) {
                        $sty = $sheet->getStyle(xlsAlpha($cidx).$ridx);
                        $alignment = $sty->getAlignment();
                        $alignment->setWrapText(true);
                    }
                }
            }
        }

        //--------------------------------------------------
        // 【Excelその４】
        // 仕上げ
        //--------------------------------------------------

        // 全体に罫線を引く
        $area = $sheet->getStyle("A1:" . xlsAlpha($cidx).$ridx);
        $xxbo = $area->getBorders();
        $xxab = $xxbo->getAllBorders();
        $xxab->setBorderStyle(thin);

        // 全体に縦中央（しかし、利いてない）
        $alignment = $area->getAlignment();
        $alignment->setVertical("center");

        // Excel出力
        ob_clean();
        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="unit_history.xls"');
        $writer->save("php://output");
        die;
    }



    // ここから栄養管理画面一覧のHTML出力。
    // 「一覧スクロール＋固定行ヘッダ＋固定列ヘッダ」を実現するために今回、
    // 以下の４つのテーブルタグをレイヤー的に重ねあわせて実現している。
    // ・メインデータテーブル
    // ・左側行ヘッダ
    // ・上部列ヘッダ
    // ・左上コーナーヘッダ
    //
    // 各テーブルの幅や高さが極力ズレないように、かつ、位置計算が簡単な都合上、
    // 重なっているヘッダ部分も、同じようにHTML描画している。
    //
    // 例えば、固定ヘッダが一切不要なら、
    // ヘッダテーブルがなくとも、メインデータテーブルさえあれば「ほぼ」よい、ということである。
    //
    // ちなみに、特に左上コーナー部分は、上記４つのテーブルタグが全て重なっている場所であり、
    // 重なっている箇所は、ほぼ同じ４つのHTMLを吐いている、ということである。


    //======================================================================
    // 【一覧HTML出力その１】
    // テーブルタグとは別に、
    // 以下データを、JavaScript-JSONで排出。JSON値をhiddenに入れてしまう。
    // ------------------------------------------------------------------
    // ◆ cur_data
    //    ⇒ 基本、変更差異確認用で参照。
    // ◆ field_info
    //    ⇒ 列定義である。列定義はformula定義のように、別途固定で出力してもよいのだが、まあエントリが動的に微妙に変化するのでここで出力。
    // ◆ append_ref_output
    //    ⇒ 一括引用データ。「ajax処理明け ⇒ HTMLが描画された後 ⇒ さらにその穴埋め材料」  として、JavaScriptで動的に参照するもの。
    // ◆ row_count
    //    ⇒ データ行数
    //======================================================================
    $js_rows = array();
    foreach ($rows as $row){
        $key = '"k'.$row["inpt_in_dt"]."@".$row["yyyymm"]."@".$row["ptif_id"].'"';
        $js_rows[$key] = $row;
    }
    echo '<input type="hidden" id="append_ref_count" value="'.$append_ref_count.'" />';
    echo '<input type="hidden" id="append_ref_output" value="'.h(to_json($append_ref_output)).'" />';
    echo '<input type="hidden" id="cur_data" value="'.h(to_json($js_rows)).'" />';
    echo '<input type="hidden" id="field_info" value="'.h(to_json($field_info)).'" />';
    echo '<input type="hidden" id="row_count" value="'.count($rows).'" />';
    echo '<input type="hidden" id="inpt_io_list" value="'.implode(",", $inpt_io_list).'" />';

    //======================================================================
    // 【一覧HTML出力その２】
    // メインデータテーブル
    //======================================================================
    echo '<div style="z-index:100; position:absolute; overflow:scroll; left:0; top:0; z-index:20" id="scroll_div" onscroll="mainScroll()">';

    echo '<table cellspacing="0" cellpadding="0" class="list_table main_table" style="padding:1px 0 0 1px; z-index:5; position:relative"';
    echo ' onmousedown="mainTableMouseDown(this, event)" id="main_table_content">';
    echo '<tbody>';
    echo '<tr id="main_table_contents_tr">';
    foreach ($field_info as $idx => $finfo) {
        if (!$finfo["jp"]) continue;
        echo '<th '.($finfo["colspan"]?' colspan="'.$finfo["colspan"].'" id="colparent'.$finfo["colparent"].'"':'').'><div';
        if ($finfo["width"]) echo ' style="width:'.$finfo["width"].'"';
        echo '>';
        if ($finfo["field"]=="age" && $list_mode=="keikakusyo") {
            echo "入院時年齢";
        } else {
            echo h($finfo["jp"]);
        }
        echo '</th>';
    }
    echo '</tr>';
    $ridx = 0;
    foreach ($rows as $row){
        $ridx++;

        $trcls = ($row["yyyymm"]=="000000" ? "keikakusyo" : "screening");
        echo '<tr id="k'.$row["inpt_in_dt"]."@".$row["yyyymm"]."@".$row["ptif_id"].'" row_index="'.$ridx.'" class="'.$trcls.'">';
        $colspanflag = 0;
        $colparentidx = "";
        foreach ($field_info as $idx => $finfo) {
            $field = $finfo["field"];
            $colparent = "";
            if ($ridx==1) {
                if ($colspanflag==0) {
                    $colspanflag = $finfo["colspan"];
                    $colparentidx = $finfo["colparent"];
                }
                if ($colspanflag>0) $colparent = " colparent".$colparentidx;
                $colspanflag--;
            }
            echo '<td';
            if ($ridx==1 && $finfo["width"]) echo ' style="width:'.$finfo["width"].'"';
            $cls = array();
            if ($finfo["is_readonly"]) $cls[]= "readonly";
            if (count($cls) || $colparent) echo ' class="'.implode(" ", $cls).$colparent.'"';
            echo '>';
            if ($field=="yyyymm") {
                if ($row[$field]=="000000") {
                    echo "(計画書)";
                } else {
                    echo substr($row[$field],0,4) ."/" . substr($row[$field],4);
                }
            }
            else if ($field=="inpt_in_dt") {
                echo substr($row[$field],0,4)."/".substr($row[$field],4,2)."/".substr($row[$field],6);
            } else {
                echo str_replace("\n", "<br>", h($row[$field]));
            }
             echo '</td>';
        }
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';



    //======================================================================
    // 【一覧HTML出力その３】
    // セル入力のための、入力テキストボックス<TEXTAREA>、入力ボックス赤枠の<DIV>、テキストサイズ確認用の<SPAN>、の３つを排出
    // このシステムは、テキストボックスはこの<TEXTAREA>ひとつでやりくりしている。
    // 実は、AJAXで、毎々再描画する必要もないのだが。まあ面倒なので毎々再作成する。
    //======================================================================
    echo '<span id="cell_span" style="position:absolute; left:0; top:0; color:#ffffff; z-index:0"></span>';

    echo '<div id="cell_div" style="position:absolute; border:1px solid #ff0000; left:0; top:0; z-index:200"';
    echo ' onmouseup="" onmousedown="gIsCellMouseDown=1;" onclick="activateCell(\'edit2\');"></div>';

    echo '<textarea id="cell_tbox" onfocus="textFocus()" onblur="textBlur()" onkeydown="textKeyDown(event)"';
    echo ' onkeyup="textKeyUp(event)"';
    echo ' style="border:0; padding:1px 0 0 0; position:absolute; width:10px; height:10px;';
    echo ' left:0; top:0; z-index:300; resize:none; overflow:hidden"></textarea>';


    // ここで、メインデータの閉じタグ
    echo '</div>';



    //======================================================================
    // 【一覧HTML出力その４】
    // 左側の行ヘッダ
    //======================================================================
    echo '<div style="position:absolute; left:0; top:0; z-index:400; overflow:hidden" id="div_main_table_header_left">';
    echo '<table cellspacing="0" cellpadding="0" class="list_table main_table" style="padding:1px 0 0 0; position:absolute" id="main_table_header_left">';
    echo '<tbody>';
    echo '<tr>';
    foreach ($field_info as $idx => $finfo) {
        if (!$finfo["is_header"]) break;
        echo '<th><div';
        if($finfo["width"]) echo ' style="width:'.$finfo["width"].'"';
        echo '>'.h($finfo["jp"]).'</div></th>';
    }
    echo '</tr>';

    $ridx=0;
    foreach ($rows as $row){
        $ridx++;
        $trcls = ($row["yyyymm"]=="000000" ? "keikakusyo" : "screening");

        $trkey = $row["inpt_in_dt"]."@".$row["yyyymm"]."@".$row["ptif_id"];
        echo '<tr id="ltr_k'.$trkey.'" class="'.$trcls.'">';
        foreach ($field_info as $idx => $finfo) {
            $field = $finfo["field"];
            if (!$finfo["is_header"]) break;
            if ($field=="is_exist_data") {
                if ($row[$field]) {
                    echo '<td><a href="javascript:void(0)" onclick="deleteRow(\''.$trkey.'\')">削除</a></td>';
                } else {
                    echo '<td></td>';
                }
            }
            else if ($field=="yyyymm") {
                if ($row[$field]=="000000") {
                    echo "<td>(計画書)</td>";
                } else {
                    echo "<td>".substr($row[$field],0,4) ."/" . substr($row[$field],4)."</td>";
                }
            }
            else if ($field=="inpt_in_dt") {
                echo "<td>".substr($row[$field],0,4)."/".substr($row[$field],4,2)."/".substr($row[$field],6)."</td>";
            }
            else if ($field=="ptif_name") {
                echo '<td><a href="javascript:void(0)"';
                echo ' onclick="if (!isChangeViewOK()) return false; changeView(\'ptif_mode\', \''.$row["ptif_id"].'\', \''.$row["inpt_in_dt"].'\', \'\', this.innerHTML)">';
                echo h($row["ptif_name"]).'</a></td>';
            } else {
                echo '<td>'.h($row[$field]).'</td>';
            }
        }
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
    echo '</div>';




    //======================================================================
    // 【一覧HTML出力その５】
    // 上部の列ヘッダ
    //======================================================================
    echo '<div style="position:absolute; left:0; top:0px; z-index:500; overflow:hidden" id="div_main_table_header_top">';
    echo '<table cellspacing="0" cellpadding="0" class="list_table main_table" style="padding:0 0 0 1px; position:absolute" id="main_table_header_top">';
    echo '<tbody>';
    echo '<tr id="main_table_header_top_tr">';

    foreach ($field_info as $idx => $finfo) {
        if (!$finfo["jp"]) continue;
        echo '<th';
        if ($finfo["title"]) echo ' title="'.h($finfo["title"]).'"';
        if ($finfo["colspan"]) echo ' colspan="'.$finfo["colspan"].'"';
        echo '><div>';
        if ($finfo["field"]=="age" && $list_mode=="keikakusyo") {
            echo "入院時年齢";
        } else {
            echo h($finfo["jp"]);
        }
        echo '</div></th>';
    }
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';
    echo '</div>';


    //======================================================================
    // 【一覧HTML出力その６】
    // 左上、コーナーヘッダ
    //======================================================================
    echo '<div style="position:absolute; left:0; top:0; z-index:600" id="main_table_header_topleft">';
    echo '<table cellspacing="0" cellpadding="0" class="list_table main_table" style="padding:0 0 0 0;">';
    echo '<tbody>';
    echo '<tr id="main_table_header_topleft_tr">';

    foreach ($field_info as $idx => $finfo) {
        if (!$finfo["is_header"]) break;
        if (!$finfo["jp"]) continue;
        echo '<th';
        if ($finfo["colspan"]) echo ' colspan="'.$finfo["colspan"].'"';
        if ($finfo["title"]) echo ' title="'.h($finfo["title"]).'"';
        echo '><div>'.h($finfo["jp"]).'</div></th>';
    }
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';
    echo '</div>';


    die; // 終了。このあとは、AJAX呼び出し側 changeView() 関数へ続く。
}




// 以降、初期表示時のみ通過。




//**********************************************************************************************************************
// PHPその７）初期表示準備２  病棟などが指定されていなければデフォルトを設定する。初期表示時のみここを通過する。
//**********************************************************************************************************************
$sql =
" select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
" inner join empmst emp on (".
"   emp.emp_class = swer.class_id".
"   and emp.emp_attribute = swer.atrb_id".
"   and emp.emp_dept = swer.dept_id".
"   and emp.emp_id = '".pg_escape_string($emp_id)."'".
" )";
$emp_ward = sql_select($sql);
$sel_bldg_cd = (int)@pg_fetch_result($emp_ward, 0, "bldg_cd");
$sel_ward_cd = (int)@pg_fetch_result($emp_ward, 0, "ward_cd");












//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// HTMLコンテンツ開始
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?><!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css" media="all">
  a.closebtn { background:url(img/icon/close.gif) no-repeat; display:block; width:24px; height:24px; }
  a.closebtn:hover { background:url(img/icon/closeo.gif) no-repeat; }
  .list_table th { text-align:center }
  .main_table { width:auto; }
  .main_table th, #main_table tr, #main_table td { position:relative; }
  .main_table th { white-space:nowrap; padding-left:4px; padding-right:4px }
  .main_table td { white-space:nowrap; font-size:13px; line-height:15px; }
  .main_table tr { height:20px }
  #main_table_header_left td { background-color:#f5f5f5; }
  td.readonly { background-color:#eeeeee; }
  td.valchanged { background-color:#ccffff; }
  textarea { background-color:#ffffcc; }
  textarea.valchanged { background-color:#99eeff; }
  textarea.readonly { background-color:#eeeeee; }
  .white { color:#ffffff; }
  #main_table_header_left  tr.do_delete td { background-color:#aaaaaa }
  #sel_ptrm_room_no_container div { float:left; }
  textarea, span#cell_span { font-size:13px; line-height:15px; }
  .center { text-align:center }
</style>
<script type='text/javascript' src='js/showpage.js'></script>
<script type="text/javascript" src="js/jquery/jquery-1.10.1.min.js"></script>
<title>CoMedix | 栄養管理</title>
<script type="text/javascript">
<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// JavaScript：グローバル変数
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
var EMPTY = "";
var gAjaxSeq = 0;
var gRowCount = 0;
var gColFirst = 0;
var gColLast = 0;
var gScrollDiv = 0;
var gBtnFormula = 0;
var gBtnStyle = 0;
var gCurFormula = "";
var gCurStyle = "";
var gTBox = 0;
var gDiv = 0;
var $gDiv = 0;
var gSpan = 0;
var gTD = 0;
var gTR = 0;
var gTBody = 0;
var gTDIdx = 0;
var gTRIdx = 0;
var gTRKey = "";
var gEMode = "";
var gMainTableContent = 0;
var gDivMainTableHeaderLeft = 0;
var gMainTableHeaderTop = 0;
var gDivMainTableHeaderTop = 0;
var gMainTableHeaderTopLeft = 0;
var gMainTableHeaderTopCells = [];
var gMainTableHeaderTopLeftCells = [];
var gMainTableContentCells = [];
var gColspanGroups = {};
var gHeaderWidth = 0;
var gHeaderHeight = 0;
var gCtrl = 0;
var gViewMode = "<?=$view_mode?>";
var gInptInDt = "";
var gPtifId = "";
var gPtifName = "";
var gCurData = {};
var gNewData = {};
var gCngData = {};
var gBaseUrl = "summary_eiyou_kanri.php?session=<?=$_REQUEST["session"]?>";
var gFieldInfo = {};
var gIsCellMouseDown = 0;
var gDiffCount = 0;
var gDivDiffCount = 0;
var gDeleteKeys = 0;
//var gAppendRef = 0;
var gFieldIndexes = {};
var gFormulaInfo = <?=to_json($formulaInfo)?>;
var gFormulaBases = 0;
var gFormulaTargets = {};
var gFormulaTargetsJp = {};
var gStyleInfo = <?=to_json($styleInfo)?>;
var gWardSelections = {};
var gTeamSelections = {};
var gRoomSelections = {};
var gPtrmCodeList = [];
var gPtrmAllCodeList = [];
var gMemory = {
    sel_bldg_cd:"<?=$sel_bldg_cd?>", sel_ward_cd:"<?=$sel_ward_cd?>", sel_team_id:"",
    date_y:"<?=date("Y")?>", date_m:"<?=date("m")?>",
    date_y_to:"<?=date("Y")?>", date_m_to:"<?=date("m")?>", kikan_ym:"all"
};

<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// JavaScript：汎用関数
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
var uEnc = function(s) { return encodeURIComponent(s); }
var int = function(s) { if (!s) return 0; var num = parseInt(s,10); if (!num || isNaN(num)) return 0; return num; };
var round = function(n, d) { if(!d) return Math.round(n); var r=Math.pow(10,d); return (Math.round(n*r)/r).toFixed(d); }
var str = function(s) { if (!s && s!=0) return ""; return ""+s; }
var trim = function(s) { if (s==undefined) return ""; s+=""; return s.replace(/^[\s　]+|[\s　]+$/g, ''); }
var ee = function(id) { return document.getElementById(id); }
var stopEvent = function(evt) { if(!evt) evt=window.event; try{evt.stopPropagation();}catch(ex){} try{evt.preventDefault();}catch(ex){} try{evt.cancelBubble=true;}catch(ex){} return false; };
var isOneOf = function(str, ary) { for (var idx=0; idx<ary.length; idx++) if (ary[idx]==str) return true; return false; }
var dummyEvent = function() { return true; };
var getComboValue = function(id) { var obj=ee(id); if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].value; }
var setComboValue = function(id,v) { var obj=ee(id); for(var idx=0; idx<obj.options.length; idx++) if (obj.options[idx].value==v) { obj.selectedIndex=idx; return; } obj.selectedIndex = 0; }
var htmlEscape = function(str) { return trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;");}
var htmlUnEscape = function(str) { return trim(str).replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&nbsp;/g," ").replace(/&amp/g,"&");}
var isUruudosi = function(y) { if (y%400==0) return true; if ( (y%4==0) && (y%100!=0) ) return true; return false; }
var getMonthLastDay = function(y,m) { var ad=[0,31,28,31,30,31,30,31,31,30,31,30,31]; if (isUruudosi(y)) ad[2]=29; return ad[m]; }


//―――――――――――――――――――――――――――――――――――――――
// 指定yyyy/mm/ddが日付でなければエラーメッセージを返す
//―――――――――――――――――――――――――――――――――――――――
var validIsSlashYmd = function(ymd) {
    var a = ymd.split("/");
    if (a.length!=3) return "日付形式が不正です。「yyyy/mm/dd」形式で指定してください。";
    var yy = int(a[0]); if (a[0].length > 4 || yy<2000 || yy>2100) return "日付の「年」が不正または範囲外です。"+yy;
    var mm = int(a[1]); if (a[1].length > 2 || mm<1 || mm>12) return "日付の「月」が不正です。";
    var dd = int(a[2]); if (a[2].length > 2 || dd<1 || dd>31) return "日付の「日」が不正です。";
    if ((mm==2 || mm==4 || mm==6 || mm==9 || mm==11) && dd > 30) return "日付の「日」が不正です。";
    if (mm==2 && dd > 29) return "日付の「日」が不正です。";
    if (mm==2 && dd > 28 && !isUruudosi(yy)) return "日付の「日」が不正です。";
    return "";
}
var validIsKasan = function(s) {
    if (s=="" || s=="加") return "";
    return "加算には「加」のみ指定できます。";
}
//―――――――――――――――――――――――――――――――――――――――
// 保存時に利用。送信文字列をすべて数値参照文字（&#nnnnn;）の形式にする。
//―――――――――――――――――――――――――――――――――――――――
function encodeToUncodeEntity(ss) {
    ss = ""+ss;
    var surrogate_1st = 0;
    var out = [];
    for (var i = 0; i < ss.length; ++i) {
        var utf16_code = ss.charCodeAt(i);
        if (surrogate_1st != 0) {
            if (utf16_code >= 0xDC00 && utf16_code <= 0xDFFF) {
                var surrogate_2nd = utf16_code;
                var unicode_code = (surrogate_1st - 0xD800) * (1 << 10) + (1 << 16) + (surrogate_2nd - 0xDC00);
                out.push("&#"+unicode_code.toString(10).toUpperCase() + ";");
            } else {
                // Malformed surrogate pair ignored.
            }
            surrogate_1st = 0;
        } else if (utf16_code >= 0xD800 && utf16_code <= 0xDBFF) {
            surrogate_1st = utf16_code;
        } else {
            out.push("&#"+utf16_code.toString(10).toUpperCase() + ";");
        }
    }
    return out.join("");
}
//―――――――――――――――――――――――――――――――――――――――
// 削除リンククリックイベント。削除エントリへ追加する。再度呼ばれたら削除エントリから消す。
//―――――――――――――――――――――――――――――――――――――――
function deleteRow(trkey) {
    if (gDeleteKeys && gDeleteKeys[trkey]) {
        delete gDeleteKeys[trkey];
        ee("ltr_k"+trkey).className = "";
    } else {
        if (!gDeleteKeys) gDeleteKeys = {};
        gDeleteKeys[trkey] = 1;
        ee("ltr_k"+trkey).className = "do_delete";
    }
    var existCnt = 0;
    if (gDeleteKeys) for(var idx in gDeleteKeys) existCnt++;
    if (!existCnt) gDeleteKeys = 0;
    ee("deleteCount").innerHTML = (existCnt ? "削除："+existCnt+"行" : "");
}
//―――――――――――――――――――――――――――――――――――――――
// 一括引用ボタンクリック
// セーブデータごと一旦サーバに送信し、引用データとマージして再表示する
//―――――――――――――――――――――――――――――――――――――――
function changeViewWithRef() {
    var mmsg = [
        "サーバデータからの引用を行います。以下の列に対し、セル単位で引用します。",
        "■疾患、栄養補給法、主食、副食、経管栄養剤、加算",
        "　　⇒【食事箋ﾃﾝﾌﾟﾚｰﾄ】入院日〜対象月月末での最新",
        "　　　◎再読込",
        "■体重",
        "　　⇒【体重集計表ﾃﾝﾌﾟﾚｰﾄ】入院日〜対象月月末での最新",
        "■体重前月",
        "　　⇒【前月栄養ｽｸﾘｰﾆﾝｸﾞ】",
        "　　　なければ【体重集計表ﾃﾝﾌﾟﾚｰﾄ】入院日〜対象前月月末での最新",
        "■目標、その他",
        "　　⇒【前月栄養ｽｸﾘｰﾆﾝｸﾞ】なければ【栄養管理計画書】",
        "■身長",
        "　　⇒【前月栄養ｽｸﾘｰﾆﾝｸﾞ】なければ【基礎データ】",
        "■提供エネルギー、提供たんぱく質、提供塩分、提供水分",
        "　　⇒【前月栄養ｽｸﾘｰﾆﾝｸﾞ】",
        "■TP、Alb、Hb、WBC、LYMPH、T-cho",
        "　　⇒【検査結果】入院後６ヶ月前月初〜対象月月末の最新",
        "　　　（Alb/Hb検査データが無い場合は過去を遡って取得）",
        "　　※TP：0001／Alb：0058／Hb：0403",
        "　　※WBC：0402／LYMPH：3209／T-cho：0028",
        "　　　◎再読込",
        "■IBMI、a、s、p、h",
        "　　⇒【前月栄養ｽｸﾘｰﾆﾝｸﾞ】",
        "　　　なければ【固定値】（IBMI=22／a=1／s=1／p=1／h=30）",
        "",
        "※「◎再読込」列は、編集中セル値へも上書き引用します。",
        "  それ以外は画面セルに値があれば引用は行いません。",
        "※ 「＠再読込」であってもデータがサーバ側に無ければ引用されません。",
        "   （∴手修正の値はクリアされません）",
        "※ データの保存は別途「保存」ボタンを押して実施してください。"
    ];
    if(!confirm(mmsg.join("\n"))) return false;
    var scTop = gScrollDiv.scrollTop;
    var scLeft = gScrollDiv.scrollLeft;
    changeView(gViewMode, gPtifId, gInptInDt, 1, "", 1);
    gScrollDiv.scrollTop = scTop;
    gScrollDiv.scrollLeft = scLeft;
}
//―――――――――――――――――――――――――――――――――――――――
// 保存ボタンクリックイベント。AJAX越しに保存を行う。成功時はchangeView()を呼ぶ ⇒ リロードされる
//―――――――――――――――――――――――――――――――――――――――
function saveData() {
    var postData = {};
    var isExistRegData = 0;
    // 行ごとに差異箇所の確認を行う。trkeyは行キー。
    for (var trkey in gCngData) {
        var isExist = 0;
        var cngData = gCngData[trkey]; // 新データあり
        var newData = gNewData[trkey]; // 新データ行（画面入力データ）
        var curData = gCurData[trkey]; // 現データ行（保存前DBデータ）
        // 新データ行を、フィールドごとに確認
        for (var field in cngData) {
            if (!cngData[field]) continue;
            var sNew = newData[field];
            if (sNew==" " || typeof sNew==="undefined") sNew = ""; // 新データが空白１コは空文字にする
            var sCur = curData[field];
            if (sCur==" " || typeof sCur==="undefined") sCur = ""; // 現データが空白１コは空文字にする
            if (sNew==sCur) continue; // 差異なしならスルー
            var fieldIdx = gFieldIndexes[field]; // フィールド定義取得
            var finfo = gFieldInfo[fieldIdx]; // フィールド定義取得
            if (!finfo.is_readonly && !finfo.is_header) isExist = 1; // 差異があって、かつ、読み取り専用でなければ、この行に差異ありと判定。
        }
        if (isExist!=1) continue; // 行に差異がない。あるいは差異があってもリードオンリーだった。この場合スルー
        var trkey2 = trkey.substring(1); // 行キーの先頭「k」の文字を捨て去る
        // 再度、新データをフィールドごとに確認。この行は差異があることが判明している。
        for (var field in cngData) {
            if (!cngData[field]) continue;
            var sNew = newData[field];
            if (sNew==" " || typeof sNew==="undefined") sNew = ""; // 新データが空白１コは空文字にする（さっきと同じ）
            var sCur = curData[field];
            if (sCur==" " || typeof sCur==="undefined") sCur = ""; // 現データが空白１コは空文字にする（さっきと同じ）
            if (sNew==sCur) continue; // 差異なしならスルー（さっきと同じ）
            var fieldIdx = gFieldIndexes[field]; // フィールド定義取得（さっきと同じ）
            var finfo = gFieldInfo[fieldIdx]; // フィールド定義取得（さっきと同じ）
            if (finfo.is_header) {
                continue; // ヘッダ項目は無視する
            }
            if (finfo.validation) { // バリデーション定義がある場合
                // バリデーションを実施する。エラーがあれば登録せずに終了。
                var errmsg = "エラーが発生しました。";
                try {eval("errmsg = "+finfo.validation.replace('[値]', sNew.replace(/\'/g, "\\'"))); } catch(ex){ alert("エラーが発生しました。"+ex); return; }
                if (errmsg) {
                    var tr = ee(trkey);
                    var rowIndex = tr.getAttribute("row_index");
                    alert(errmsg + "（"+rowIndex+"行目／"+finfo.jp+"）");
                    return;
                }
            }
            // バリデーション対象外か、エラーがなかったら、POSTデータに追加する。数値参照文字に変換して追加する。
            postData[trkey2+"@"+field] = encodeToUncodeEntity(sNew);
            // 登録データありフラグを1に。
            isExistRegData = 1;
        }
    }
    // 削除対象があるかを確認。あればPOStに追加する。
    var delete_keys = [];
    for(var key in gDeleteKeys) delete_keys.push(key);
    if (delete_keys.length) {
        postData["delete_keys"] = encodeToUncodeEntity(delete_keys.join(","));
        // 削除データがあった。登録データありフラグを1に。
        isExistRegData = 1;
    }
    if (!isExistRegData) return alert("変更データはありません。"); // 登録データなし。alertを出して終了。
    if (!confirm("保存します。よろしいですか？")) return;
    gAjaxSeq++; // ブラウザキャッシュ利用しないための保護連番。
    // AJAXで登録POSTする
    var param = "&yyyymm="+encodeURIComponent(getComboValue("date_y") + getComboValue("date_m"))+"&seq="+gAjaxSeq;
    if (gViewMode=="ptif_mode") param = "&ptif_id="+encodeURIComponent(gPtifId);
    $.ajax({ url:gBaseUrl+"&ajax_mode=exec_save&view_mode="+gViewMode+param, data:postData, async:false, type:"post", success:function(msg) {
        if (msg=="SESSION_EXPIRED") { return ajaxSessionError(); } // 実はセッション切れ。終了。
        if (msg!="ok") return alert("エラー\n"+msg); // 引数が悪いか、サーバロジック的に問題。つまりシステムエラー。エラーダイアログを出す。
        gDiffCount = 0; // changeView()を呼んでも確認ダイアログが出ないように、変更数をリセット
        gDeleteKeys = 0; // changeView()を呼んでも確認ダイアログが出ないように、削除数をリセット
        gDivDiffCount.innerHTML = ""; // ついでに変更数表示をクリア
        ee("deleteCount").innerHTML = ""; // ついでに削除予定数表示をクリア
        var scTop = gScrollDiv.scrollTop;
        var scLeft = gScrollDiv.scrollLeft;
        changeView(gViewMode, gPtifId, gInptInDt, 1); // 画面を再描画
        gScrollDiv.scrollTop = scTop;
        gScrollDiv.scrollLeft = scLeft;
    }});
}
//―――――――――――――――――――――――――――――――――――――――
// タイムアウトエラー・セッションエラーを表示
//―――――――――――――――――――――――――――――――――――――――
function ajaxSessionError() {
    alert("長時間利用されませんでしたのでタイムアウトしました。\n再度ログインしてください。");
    showLoginPage(window);
}
//―――――――――――――――――――――――――――――――――――――――
// Excelボタン押下処理。エクセルを出力する。
//―――――――――――――――――――――――――――――――――――――――
function downloadExcel() {
    <? if (phpversion() < '5.1.6') { ?>
    alert("Excel出力を行います。\n\nこの機能は、このバージョンでは利用できません。");
    return;
    <? } ?>
    var param = "";
    if (gViewMode=="ward_mode") {
        param =
        "&sel_bldg_cd="+getComboValue("sel_bldg_cd")+
        "&sel_ward_cd="+getComboValue("sel_ward_cd")+
        "&sel_ptrm_room_no_list="+gPtrmCodeList.join(",")+
        "&date_y="+getComboValue("date_y")+
        "&date_m="+getComboValue("date_m")+
        "&list_mode="+(ee("list_mode_screening").checked ? "screening" : "keikakusyo");
    } else {
        param =
        "&ptif_id="+uEnc(gPtifId)+
        "&inpt_in_dt="+uEnc(gInptInDt)+
        "&date_y_to="+getComboValue("date_y_to")+
        "&date_m_to="+getComboValue("date_m_to")+
        "&kikan_ym="+getComboValue("kikan_ym");
    }
    gAjaxSeq++; // ブラウザキャッシュ利用しないための保護連番。
    window.open(gBaseUrl+"&ajax_mode=exec_load&download_excel=1&view_mode="+gViewMode+param+"&seq="+gAjaxSeq);
}
//―――――――――――――――――――――――――――――――――――――――
// 変更箇所ありか、削除予定ありか、の場合に、ビューチェンジしようとしたら警告を出す
//―――――――――――――――――――――――――――――――――――――――
function isChangeViewOK(isClose) {
    if (!gDiffCount && !gDeleteKeys) return true;
    if (isClose) return confirm("変更箇所があります。\n保存せずに、画面を閉じてよろしいですか？");
    var ret = confirm("変更箇所があります。\n保存せずに、表示を切替えてよろしいですか？");
    if (ret) return true;
    return false;
}
//―――――――――――――――――――――――――――――――――――――――
// 検索条件に従って画面リロード
// vm(viewMode)は、"ward_mode" または "ptif_mode" いずれか。
// ptif_modeなら、ptif_idとinpt_in_dtは必須。
// 患者氏名をクリックした場合は、ptif_nameが渡ってくる。（ward_modeからptif_modeに変わるような呼ばれ方をする場合である）
// histModeに値を指定すれば履歴に残さない。カラなら履歴に残す。
//―――――――――――――――――――――――――――――――――――――――
var changeViewHistory = [];
var histPos = 0;
function histView(isForward) {
    var inc = (isForward ? -1 : 1);
    histPos += inc;
    var obj = changeViewHistory[changeViewHistory.length - histPos - 1];
    changeView(obj.vm, obj.ptif_id, obj.inpt_in_dt, 1, obj.ptif_name);
    btnMoveSetDisplay();
}
// 履歴を追加。最大50までとする。
function addViewHist(vm, ptif_id, inpt_in_dt, ptif_name) {
    if (histPos > 0) {
        changeViewHistory.splice(changeViewHistory.length - histPos, histPos); // 戻ったぶんの末尾要素を削除
        histPos = 0;
    }
    if (changeViewHistory.length > 50) changeViewHistory.shift(); // 最初の要素を削除
    changeViewHistory.push({vm:vm, ptif_id:ptif_id, inpt_in_dt:inpt_in_dt, ptif_name:ptif_name}); // 追加
    btnMoveSetDisplay();
}
function btnMoveSetDisplay() {
    ee("btnmove_back_on").style.display = (changeViewHistory.length - histPos > 1 ? "" : "none");
    ee("btnmove_forward_on").style.display = (histPos > 0 ? "" : "none");
    ee("btnmove_back_off").style.display = (ee("btnmove_back_on").style.display=="none" ? "" : "none");
    ee("btnmove_forward_off").style.display = (ee("btnmove_forward_on").style.display=="none" ? "" : "none");
}
function changeView(vm, ptif_id, inpt_in_dt, histMode, ptif_name, isAppendRef) {
    if (!histMode) addViewHist(vm, ptif_id, inpt_in_dt, ptif_name);
    // 患者別一覧への変更なら検索条件エリアの表示内容を多少整える
    if (vm=="ptif_mode") {
        gPtifId = ptif_id;
        gInptInDt = inpt_in_dt;
        ee("ptif_mode_id").innerHTML = ptif_id;
        if (ptif_name) ee("ptif_mode_name").innerHTML = ptif_name;
        var trkey = "k"+gInptInDt+"@"+getComboValue("date_y")+getComboValue("date_m")+"@"+gPtifId;
        var data = gCurData[trkey];
    }
    // 各検索条件エリアやボタンの表示設定
    ee("div_ward_mode").style.display = (vm=="ward_mode" ? "" : "none");
    ee("div_ptif_mode").style.display = (vm=="ptif_mode" ? "" : "none");
    ee("span_ward_mode1").style.display = (vm=="ward_mode" ? "" : "none");
    ee("span_ward_mode2").style.display = (vm=="ward_mode" ? "" : "none");
    // 最上部ぱんくず部の表示内容変更
    var pankuzu = "";
    if (vm=="ward_mode") pankuzu += '栄養管理';
    else {
        pankuzu +=
            '<a href="javascript:void(0)" onclick="if (!isChangeViewOK()) return false; changeView(\'ward_mode\')">'+
            '栄養管理</a>&nbsp;&gt;&nbsp;患者別一覧';
    }
    ee("view_mode_title").innerHTML = pankuzu;
    // 通常一覧から患者別一覧へ変更する場合は、年月コンボ値を引継ぐ
    if (gViewMode=="ward_mode" && vm=="ptif_mode") {
        setComboValue("date_y_to", getComboValue("date_y"));
        setComboValue("date_m_to", getComboValue("date_m"));
        gMemory["date_y_to"] = getComboValue("date_y_to");
        gMemory["date_m_to"] = getComboValue("date_m_to");
    }
    ee("spread_sheet").innerHTML = "処理に時間がかかる場合があります。しばらくお待ちください。………";
    // URPパラメータ作成
    var param = "";
    if (vm=="ward_mode") {
        param =
        "&sel_bldg_cd="+getComboValue("sel_bldg_cd")+
        "&sel_ward_cd="+getComboValue("sel_ward_cd")+
        "&sel_ptrm_room_no_list="+gPtrmCodeList.join(",")+
        "&date_y="+getComboValue("date_y")+
        "&date_m="+getComboValue("date_m")+
        "&list_mode="+(ee("list_mode_screening").checked ? "screening" : "keikakusyo");
    } else {
        param =
        "&ptif_id="+uEnc(gPtifId)+
        "&inpt_in_dt="+uEnc(gInptInDt)+
        "&date_y_to="+getComboValue("date_y_to")+
        "&date_m_to="+getComboValue("date_m_to")+
        "&kikan_ym="+getComboValue("kikan_ym");
    }
    if (isAppendRef) param += "&append_ref=1";
    gAjaxSeq++; // ブラウザキャッシュ利用しないための保護連番。

    var postData = "";
    // 値の入っている全マスを取得。編集中かどうかは不問
    if (isAppendRef) {
        postData = {};
        // 行ごとに差異箇所の確認を行う。trkeyは行キー。
        for (var trkey in gCurData) {
            var cngData = gCngData[trkey]; // 新データあり
            var curData = gCurData[trkey]; // 現データ行（保存前DBデータ）
            var newData = gNewData[trkey]; // 新データ行（画面入力データ）
            // 新データ行を、フィールドごとに確認
            for (var field in curData) {
                var sCur = curData[field];
                var sNew = newData[field];
                var sVal = "";
                if (cngData[field]) {
                    if (!sNew || sNew==" " || typeof sNew==="undefined") sNew = ""; // 編集中（青色）。
                    sVal = sNew;
                }
                else {
                    if (!sCur || sCur==" " || typeof sCur==="undefined") continue; // 未編集だが旧データが空欄。スルー
                    sVal = sCur;
                }
                var fieldIdx = gFieldIndexes[field]; // フィールド定義取得
                var finfo = gFieldInfo[fieldIdx]; // フィールド定義取得
                if (!finfo) continue;
                if (finfo.is_readonly) continue; // 値があっても読み取り専用。スルー

                var trkey2 = trkey.substring(1); // 行キーの先頭「k」の文字を捨て去る
                // 再度、新データをフィールドごとに確認。この行は差異があることが判明している。
                if (finfo.validation) { // バリデーション定義がある場合
                    // バリデーションを実施する。エラーがあれば登録せずに終了。
                    var errmsg = "エラーが発生しました。";
                    try {eval("errmsg = "+finfo.validation.replace('[値]', sVal.replace(/\'/g, "\\'"))); } catch(ex){ alert("エラーが発生しました。"+ex); return; }
                    if (errmsg) {
                        var tr = ee(trkey);
                        var rowIndex = tr.getAttribute("row_index");
                        alert(errmsg + "（"+rowIndex+"行目／"+finfo.jp+"）");
                        return;
                    }
                }
                // バリデーション対象外か、エラーがなかったら、POSTデータに追加する。数値参照文字に変換して追加する。
                postData[trkey2+"@"+field] = encodeToUncodeEntity(sVal);
            }
        }
    }

    if (isAppendRef) param += "&yyyymm="+encodeURIComponent(getComboValue("date_y") + getComboValue("date_m"));

    // グローバル変数を次のビュー表示内容を問い合わせる前に初期化
    gViewMode = vm;
    gTD = 0;
    gTRIdx= 0;
    gTRKey = "";
    gRowCount = 0;
    gCurData = {};
    gNewData = {};
    gCngData = {};
    gAppendRefOutput = [];
    gAppendRefCount = 0;
    gFieldInfo = {};


    $.ajax({ url:gBaseUrl+"&ajax_mode=exec_load&view_mode="+vm+param+"&seq="+gAjaxSeq, async:false, data:postData, type:"post", success:function(msg) {
        if (msg=="SESSION_EXPIRED") { return ajaxSessionError(); } // 実はセッション切れ。終了。
        // グローバル状態を初期化
        gDiffCount = 0;
        gDeleteKeys = 0;
        ee("deleteCount").innerHTML = "";
        ee("spread_sheet").innerHTML = msg; // ここでごそっとHTML描画
        if (!ee("row_count")) return; // 行数が無い。つまり入院患者が存在しない。終了。
        eval("gCurData="+ee("cur_data").value); // 現データをJavaScript変数に。
        eval("gAppendRefOutput="+ee("append_ref_output").value); // 引用データをJavaScript変数に。
        eval("gAppendRefCount="+ee("append_ref_count").value); // 引用データをJavaScript変数に。
        eval("gFieldInfo="+ee("field_info").value); // フィールド定義をJavaScript変数に。
        for (var trkey in gCurData) {
            gNewData[trkey] = {};
            gCngData[trkey] = {};
        }

        // 入退院プルダウン
        var ary = ee("inpt_io_list").value.split(",");
        var cmb = ee("inpt_in_dt");
        cmb.options.length = ary.length;
        for (var idx=0; idx<ary.length; idx++) {
            var param = ary[idx].split("-");
            cmb.options[idx] = new Option(param[1], param[0]);
            if (param[0] == gInptInDt) cmb.selectedIndex = idx;
        }

        // フィールド定義から、固定列でない列番号を判別取得
        gColFirst = 0;
        for (var idx=0; idx<gFieldInfo.length; idx++) {
            if (!gFieldInfo[idx].is_header) break;
            gColFirst++;
        }
        // フィールド定義から、列IDがキーとなるような定義を作成しておく。アクセシビリティ向上用
        for (var idx=0; idx<gFieldInfo.length; idx++) {
            gFieldIndexes[gFieldInfo[idx].field] = idx;
        }
        // フォーミュラ定義も、アクセシビリティ向上用にいくつかパターンを作成しておく。
        gFormulaBases = {}; // フォーミュラ計算元となる列IDを集めたもの。
        gFormulaTargets = {}; // フォーミュラ計算先となる列IDをキーにしたもの。
        gFormulaTargetsJp = {};  // フォーミュラ計算先となる列IDをキーにしたもの。格納される値は日本語。説明表示用。
        for (var idx=0; idx<gFormulaInfo.length; idx++) {
            var bases = gFormulaInfo[idx].base;
            var formula = gFormulaInfo[idx].formula;
            gFormulaTargets[gFormulaInfo[idx].target] = formula;

            for (var idx2=0; idx2<bases.length; idx2++) {
                gFormulaBases[bases[idx2]] = 1;
                var fidx = gFieldIndexes[bases[idx2]];
                var jp = gFieldInfo[fidx].jp;
                if (gFieldInfo[fidx].title) jp = gFieldInfo[fidx].title;
                formula = formula.split(bases[idx2]).join(jp);
            }
            gFormulaTargetsJp[gFormulaInfo[idx].target] = formula;
        }
        // 一括引用の場合。引用データが有る場合は、セルに引用データを穴埋めしてゆく。
        var refCount = 0;
        if (isAppendRef) {
            for (var trkey in gAppendRefOutput) {
                var tr = ee(trkey);
                var appendedFlg = 0;
                for (var field in gAppendRefOutput[trkey]) {
                    var v = str(gAppendRefOutput[trkey][field]);
                    if (str(gCurData[trkey][field])!=v) {
                        appendedFlg = 1;
                        gNewData[trkey][field] = v;
                        gCngData[trkey][field] = 1;
                        gDiffCount++;
                        var fidx = gFieldIndexes[field];
                        td = getTD(fidx, tr);
                        td.innerHTML = v.replace(/\n/g, "<br>");
                        $(td).addClass("valchanged"); // valchangedは、セルを青くするcss class。
                    }
                }
                if (appendedFlg) refCount++;
            }
        }
        // 変更箇所表示をクリア。しかし一括引用の結果があれば件数表示。
        gDivDiffCount.innerHTML = "";
        if (gDiffCount) gDivDiffCount.innerHTML = "変更箇所："+gDiffCount;

        gBtnFormula.disabled = true; // 「計算式」ボタン非アクティブに
        gBtnStyle.disabled = true; // 「条件付き書式」ボタン非アクティブに
        gColLast = gFieldInfo.length - 1; // 一覧の一番右列の列番号。ヘッダ列含む。
        gRowCount = ee("row_count").value*1; // データ行数

        // コンテンツ内オブジェクトのハンドラを再作成しておく
        gMainTableContent = ee("main_table_content"); // メインデータテーブル
        gDivMainTableHeaderLeft = ee("div_main_table_header_left"); // 左側、行ヘッダテーブルのラッパーDIV
        gMainTableHeaderLeft = ee("main_table_header_left"); // 左側、行ヘッダテーブル
        gDivMainTableHeaderTop = ee("div_main_table_header_top"); // 上部、列ヘッダテーブルのラッパーDIV
        gMainTableHeaderTop = ee("main_table_header_top"); // 上部、列ヘッダテーブル
        gMainTableHeaderTopLeft = ee("main_table_header_topleft"); // 左上、コーナーテーブル
        gScrollDiv = ee("scroll_div"); // スクロール領域。メインデータテーブルのラッパーDIV

        gScrollDiv.style.width = gSpreadSheet.offsetWidth + "px"; // スクロール領域サイズを、最大範囲にする
        gScrollDiv.style.height = gSpreadSheet.offsetHeight + "px"; // スクロール領域サイズを、最大範囲にする


        // colspanが指定されている列が、どのセル列を含有しているかを管理するもの。
        // キーは上部列ヘッダテーブルのTHのID。colspan列指定されているもの。
        // 値はメインデータテーブルの最初の行のうち、colspanされた<TD>ノードの配列。
        gColspanGroups = {};

        // セル幅、セル高調整のためのアクセッサを準備
        gMainTableHeaderTopLeftCells = []; // 左上、コーナーテーブルの<TH>内の<DIV>一覧
        gMainTableHeaderTopCells = []; // 上部、列ヘッダテーブルの<TH>内の<DIV>一覧
        gMainTableContentCells = []; // メインデータテーブルの最初のヘッダ行、<TH>の一覧
        var tr = ee("main_table_contents_tr");
        if (!tr) return;
        for (var idx=0; idx<tr.childNodes.length; idx++) {
            var node = tr.childNodes[idx];
            if (node.nodeType==1) {
                gMainTableContentCells.push(node);
                if (node.id.length >= 10 && node.id.substring(0,9)=="colparent") {
                    gColspanGroups[node.id] = [];
                    $("#main_table_content td."+node.id).each(function(){
                        gColspanGroups[node.id].push(this);
                    });
                }
            }
        }
        var tr = ee("main_table_header_top_tr");
        for (var idx=0; idx<tr.childNodes.length; idx++) {
            var node = tr.childNodes[idx];
            if (node.nodeType==1) {
                if (node.childNodes[0].tagName=="DIV") gMainTableHeaderTopCells.push(node.childNodes[0]);
                else if (node.childNodes[1].tagName=="DIV") gMainTableHeaderTopCells.push(node.childNodes[1]);
            }
        }
        var tr = ee("main_table_header_topleft_tr");
        for (var idx=0; idx<tr.childNodes.length; idx++) {
            var node = tr.childNodes[idx];
            if (node.nodeType==1) {
                if (node.childNodes[0].tagName=="DIV") gMainTableHeaderTopLeftCells.push(node.childNodes[0]);
                else if (node.childNodes[1].tagName=="DIV") gMainTableHeaderTopLeftCells.push(node.childNodes[1]);
            }
        }


        // エディットボックス。scroll_divノード上に存在する。
        // scroll_divの中のz-index的には、
        // ・赤枠divは一番上。常に見えているような状態
        // ・つぎにメインデータテーブル
        // ・そのつぎにエディットボックス(TEXTAREA)。編集開始時点で、メインデータテーブルの手前にz-index変更される。
        // ・最後に文字サイズ確認用spanタグ。
        //
        // もし画面に赤枠が見えているなら、そのときは確実にtextareaにフォーカスが当たっている状態である。
        // ただし通常状態（移動中状態）のときは、z-indexが低いため、見えていない状態である。
        // 実際に編集を開始するとき、メインデータテーブル上にz-indexが変わる。
        // spanは隠しなので、常に一番下。見えることはない。
        //
        // なお、IEの場合、z-indexが低くても、キャレットの明滅がどうしても見えてしまう。
        // かたや、IEはテキストを全選択した状態であれば、キャレットを消すことができる。
        // しかしながら、もし、テキストがカラのとき、テキスト全選択という行為自体が不可能なので、
        // キャレットを消したいがためだけに、もしテキストがカラなら、わざわざ半角スペースをひとつ、textareaにセットするようにしている。
        gDiv = ee("cell_div"); // エディットボックス赤枠
        $gDiv = $(gDiv);// エディットボックス赤枠
        gTBox = ee("cell_tbox");// エディットボックス（textarea）。
        gSpan = ee("cell_span");// 文字サイズ確認用SPAN

        // 条件つき書式を、列IDがキーのアクセッサを用意しておく。
        gStyleTargets = {};
        for (var idx=0; idx<gStyleInfo.length; idx++) {
            var tmp = str(gStyleTargets[gStyleInfo[idx].target]);
            if (tmp) tmp += "\n";
            gStyleTargets[gStyleInfo[idx].target] = tmp + gStyleInfo[idx].joken.replace("値", "[値]")+ " → " + gStyleInfo[idx].color;
        }

        // フォーミュラを全行に対し実行する
        for (var trkey in gCurData) {
            var tr = ee(trkey);
            execFormula("", tr, trkey);
            for (var idx=0; idx<gFieldInfo.length; idx++) { // フォーミュラ構造体を適用順にループ
                var field = gFieldInfo[idx].field; // targetは列ID。この列IDのセルを、これから計算する。
                applyStyle(field, "", tr); // フォーミュラしたこのセルに、条件つき書式を実行
            }
        }

        documentResized(); // ウィンドウのリサイズイベントをコールし、各種枠のサイズ再調整を行う
        gScrollDiv.scrollLeft = 0; // スクロール領域を左上にリセット
        gScrollDiv.scrollTop = 0; // スクロール領域を左上にリセット
        adjustHeader(); // 列ヘッダセルの幅、行ヘッダセルの高さをシンクロさせる
        var len = gMainTableHeaderTopLeftCells.length;
        gHeaderWidth = gMainTableContentCells[len].offsetLeft; // 左側ヘッダ列の幅を取得。これはメインデータ先頭列の左位置と等しい。
        gHeaderHeight = gMainTableContentCells[0].offsetHeight; // 上部ヘッダ行の高さを取得
        gMainTableHeaderLeft.style.top = 0;
        gMainTableHeaderTop.style.left = 0;

        // 引用した場合は結果をダイアログ表示
        if (isAppendRef) {
            if (gAppendRefCount) alert(gAppendRefCount+"箇所のマスタデータからの引用を行いました。");
            else alert("マスタデータの引用箇所はありませんでした。");

        }
    }});
    return true;
}
//―――――――――――――――――――――――――――――――――――――――
// 引数<TD>または<TR>ノードの、ひとつ前のタグを返す。
//―――――――――――――――――――――――――――――――――――――――
function getPrevNode(node){
    for(;;) {
        node = node.previousSibling;
        if (node && node.nodeType==1) return node; // 通常タグが取得できた。OK終了。
        if (node.nodeType!=3) break; // タグとタグの隙間ノード（テキストノード）でなければエラーブレーク。
    }
    return null;
}
//―――――――――――――――――――――――――――――――――――――――
// 引数<TD>または<TR>ノードの、ひとつ次のタグを返す。
//―――――――――――――――――――――――――――――――――――――――
function getNextNode(node){
    for(;;) {
        node = node.nextSibling;
        if (node==null) return null;
        if (node.nodeType==1) return node; // 通常タグが取得できた。OK終了。
        if (node.nodeType!=3) break; // タグとタグの隙間ノード（テキストノード）でなければエラーブレーク。
    }
    return null;
}
//―――――――――――――――――――――――――――――――――――――――
// 指定されたtrの中の、指定されたインデックスのTDノードを返す。（ゼロオリジン）
//―――――――――――――――――――――――――――――――――――――――
function getTD(tdIdx, tr) {
    var cnt = -1;
    for (var idx=0; idx<tr.childNodes.length; idx++) {
        var node = tr.childNodes[idx];
        if (node.nodeType==1) { cnt++; if (tdIdx==cnt) return node; }
    }
    return null;
}
//―――――――――――――――――――――――――――――――――――――――
// 指定されたインデックスのTRタグを返す（ゼロオリジンだがヘッダ行があるので１以上となる）
//―――――――――――――――――――――――――――――――――――――――
function getTR(trIdx){
    var cnt = -1;
    for (var idx=0; idx<gTBody.childNodes.length; idx++) {
        var node = gTBody.childNodes[idx];
        if (node.nodeType==1) { cnt++; if (trIdx==cnt) return node; }
    }
    return null;
}
//―――――――――――――――――――――――――――――――――――――――
// 編集セルをアクティベート
//―――――――――――――――――――――――――――――――――――――――
function activateCell(emode) {
    gEMode=(emode ? emode : ""); // たいていカラにセットされ、移動モードになる。赤枠クリック時のみ、詳細編集モード「edit2」が渡ってくる。
    gTBox.value = htmlUnEscape(gTD.innerHTML).replace(/<br>/g, "\n"); // セルの中身をテキストエリアに
    if (gTBox.value=="" && gEMode=="") gTBox.value = " "; // 移動モードなのにテキストボックスがカラなら、半角空白セット
    if (gTBox.value==" " && gEMode!="") gTBox.value = ""; // 移動モードでない（テキストボックスが見える）のに、半角が有る場合はクリアする
    resizeDivAndText(); // 赤編集ボックス位置とサイズ調整
    gDiv.style.display = ""; // 赤枠を表示（不要かも）
    gTBox.focus(); // テキストボックスへフォーカス
    gTBox.select(); // テキストボックス文字を全選択

    // 画面上、「計算式」「条件付き書式」コンテンツの準備
    var field = gFieldInfo[gTDIdx].field; // 列定義取得
    var jp = gFieldInfo[gTDIdx].jp; // 列名
    var title = jp; // タイトルも列名
    if (gFieldInfo[gTDIdx].title) title = gFieldInfo[gTDIdx].title; // 列タイトル定義があれば、タイトルはそちらを
    gCurFormula = str(gFormulaTargetsJp[field]);
    if (gCurFormula) gCurFormula = "【計算式】　"+title+"　（"+jp+"）\n------------------------------\n[値] = "+gCurFormula+"\n\n（※編集不可の列を除き、いずれかの列に値を設定すると計算を開始します）";
    gCurStyle = str(gStyleTargets[field]);
    if (gCurStyle) gCurStyle = "【条件つき書式】　"+title+"　（"+jp+"）\n------------------------------\n"+gCurStyle;
    gBtnFormula.disabled = (gCurFormula ? false : true);
    gBtnStyle.disabled = (gCurStyle ? false : true);
}
//―――――――――――――――――――――――――――――――――――――――
// 編集セルの位置サイズ調整と、画面オートスクロール
//―――――――――――――――――――――――――――――――――――――――
function resizeDivAndText() {
    // テキストボックスの位置とサイズの調整
    resizeText();
    // 見えている赤枠は、微妙にアニメーション移動させる
    $gDiv.stop(true, true).animate({left:gTD.offsetLeft-1, top:gTR.offsetTop-1, width:gTD.clientWidth, height:gTD.clientHeight}, 80);
    // スクロール領域をスクロール
    var sL = gScrollDiv.scrollLeft;
    var sT = gScrollDiv.scrollTop;
    var obj = {};
    var diffLR = (gTD.offsetLeft + gTD.offsetWidth+32) - gScrollDiv.scrollLeft - gScrollDiv.offsetWidth; // 右へスクロール
    if (diffLR>0) obj.scrollLeft = sL+diffLR;
    else {
        diffLR = gTD.offsetLeft - gScrollDiv.scrollLeft - gHeaderWidth - 20; // 左へスクロール
        if (diffLR<0) obj.scrollLeft = sL+diffLR;
    }
    var diffTB = gTD.offsetTop - gScrollDiv.scrollTop - gHeaderHeight - 10; // 上へスクロール
    if (diffTB<0) obj.scrollTop = sT+diffTB;
    else {
        diffTB = (gTD.offsetTop + gTD.offsetHeight+32) - gScrollDiv.scrollTop - gScrollDiv.offsetHeight; // 下へスクロール
        if (diffTB>0) obj.scrollTop = sT+diffTB;
    }
    if (obj.scrollLeft || obj.scrollTop) {
        $(gScrollDiv).stop(true, true).animate(obj, 80);
    }
}

//―――――――――――――――――――――――――――――――――――――――
// 編集セルのうち、textareaの位置サイズ調整
//―――――――――――――――――――――――――――――――――――――――
function resizeText() {
    var finfo = gFieldInfo[gTDIdx]; // 列定義取得
    gTBox.readOnly = (finfo.is_readonly ? true : false); // 列のリードオンリー設定を確認
    var nbsp = "";
    if (gTBox.value.substring(gTBox.value.length-1)=="\n") nbsp = "<br>&nbsp;"; // テキストボックスの末尾が改行なら、空白文字をつける準備
    gSpan.innerHTML = htmlEscape(gTBox.value).split("\n").join("<br>") + nbsp; // サイズ調整spanに、テキストボックスの文字をセット。改行で終わるならプラス空白文字。
    var w = Math.max(gSpan.offsetWidth + (gEMode==""?0:15), gTD.clientWidth-1); // 即座にspanのサイズを取得
    var h = Math.max(gSpan.offsetHeight, gTD.clientHeight-1); // 即座にspanのサイズを取得
    gTBox.style.width = (w) + "px"; // spanサイズをテキストボックスに
    gTBox.style.height = (h) + "px"; // spanサイズをテキストボックスに
    gTBox.style.left = (1+gTD.offsetLeft) + "px"; // テキストボックスの位置は、TDの位置と同じ位置に
    gTBox.style.top = (gTR.offsetTop) + "px"; // テキストボックスの位置は、TDの位置と同じ位置に
    gTBox.className = gTD.className; // テキストボックスのクラス名は、TDのクラス名に。青設定にする「valchanged」というclass名がセットされるかも。
    if (gEMode=="") gTBox.style.zIndex = 0; // 移動モードなら、textareaは背面に移動
    else gTBox.style.zIndex = 300; // 移動モードでなければ、textareaを前面に
}
//―――――――――――――――――――――――――――――――――――――――
// オンロード処理
//―――――――――――――――――――――――――――――――――――――――
function prepareContents() {
    gBtnFormula = ee("formula_preview");
    gBtnStyle = ee("style_preview");
    gSpreadSheet = ee("spread_sheet");
    gDivDiffCount = ee("diffCount");
    setComboValue("sel_bldg_cd", gMemory.sel_bldg_cd);
    bldgChanged();
    setComboValue("sel_ward_cd", gMemory.sel_ward_cd);
    wardChanged();
    changeView("ward_mode");
    teamChanged();

    // 常にテキストボックスの値の変化を監視。
    // 結果、テキストのリサイズや、gEModeを簡易編集モードに変化させる。
    // 監視速度は50ミリ秒毎。
    setInterval(function() {
        if (!gTD) return;
        var tval = gTBox.value;
        if (tval==" ") tval = "";
        var tdval = htmlUnEscape(gTD.innerHTML.replace(/<br>/g, "\n"));
        if (tdval==" ") tdval = "";
        if (tdval != tval) {
            if(gEMode=="" && !gCtrl) gEMode = "edit1";
            resizeText();
        }
    }, 50);
}
//―――――――――――――――――――――――――――――――――――――――
// スクロール領域のスクロールイベント
//―――――――――――――――――――――――――――――――――――――――
function mainScroll() {
    if (!gTBox) return;
    gMainTableHeaderLeft.style.top = (gScrollDiv.scrollTop*-1) + "px"; // 左側ヘッダの上下位置をシンクロ
    gMainTableHeaderTop.style.left = (gScrollDiv.scrollLeft*-1) + "px"; // 上部ヘッダの左右位置をシンクロ
}
//―――――――――――――――――――――――――――――――――――――――
// 画面リサイズイベント
//―――――――――――――――――――――――――――――――――――――――
function documentResized() {
    gSpreadSheet.style.height=(document.documentElement.clientHeight - gSpreadSheet.offsetTop)+'px'; // スプレッドシート領域（＝スクロール領域）を最大化
    gScrollDiv.style.width = gSpreadSheet.offsetWidth + "px"; // スクロール領域をスプレッドシート領域と同じにする
    gScrollDiv.style.height = gSpreadSheet.offsetHeight + "px"; // スクロール領域をスプレッドシート領域と同じにする
    // 左側ヘッダの高さ、上部ヘッダの幅をスクロール領域にあわせる。ただし、スクロールバーのサイズ（16pxとする）を引く
    gDivMainTableHeaderLeft.style.height = (gSpreadSheet.offsetHeight-16) + "px";
    gDivMainTableHeaderLeft.style.width = (gDivMainTableHeaderLeft.childNodes[0].offsetWidth) + "px";
    gDivMainTableHeaderTop.style.width = (gSpreadSheet.offsetWidth-16) + "px";
    gDivMainTableHeaderTop.style.height = (gDivMainTableHeaderTop.childNodes[0].offsetHeight) + "px";
}
//―――――――――――――――――――――――――――――――――――――――
// メインデータテーブルのクリックイベント
//―――――――――――――――――――――――――――――――――――――――
function mainTableMouseDown(tbl, evt) {
    evt = (evt ? evt :window.event);
    var td = evt.srcElement||evt.target; // クリックされたセルを取得
    if (td.nodeName!="TD") return;
    gTD = td; // 現在セルにする
    gTDIdx = gTD.cellIndex; // 現在セルのインデックス、ゼロオリジンだがヘッダ列があるので、ゼロにはならないだろう
    gTR = gTD.parentNode; // 行TR
    gTRIdx = gTR.sectionRowIndex; // 行TRの番号、ゼロオリジンだがヘッダが１行あるので1以上。
    gTRKey = gTR.id; // 行TRのID。先頭は"k"一文字あるが、その後は入院日、対象年月、患者IDがアットマーク区切りとなっているもの。
    gTBody = gTR.parentNode; // TRの親。TBODYのこと。
    activateCell(); // 赤い編集ボックスを活性化
    stopEvent(evt);
}
//―――――――――――――――――――――――――――――――――――――――
// テキストエリアのフォーカスイベント
//―――――――――――――――――――――――――――――――――――――――
function textFocus() {
    gDiv.style.display = ""; // 赤枠を見せる
    var finfo = gFieldInfo[gTDIdx];
    gTBox.readOnly = (finfo.is_readonly ? true : false); // リードオンリー設定をテキストボックスに反映
}
//―――――――――――――――――――――――――――――――――――――――
// テキストエリアのフォーカスアウトイベント
//―――――――――――――――――――――――――――――――――――――――
function textBlur() {
    if (!gIsCellMouseDown) gDiv.style.display = "none"; // マウスダウンでフォーカスアウトしたのでなければ、赤枠を消す
    gIsCellMouseDown = 0; // マウスダウンしたかどうかのフラグをクリアする
}
//―――――――――――――――――――――――――――――――――――――――
// テキストエリア キーダウンイベント。この画面GUIのノースブリッジ的な重要箇所。
//
// ◆gEMode
// Excelの挙動を簡易エミュレートするにあたり、エディットモードを３種存在し、
// このエディットモードを忠実な軸とする考え方をとっている。
// ３つのいずれのモードにおいても、textareaにはフォーカスがある状態であり、赤枠が見えている状態である。
// �� gEMode == "" （移動モード）の場合
//    ・十字キーやTab/Enter/PageUp/PageDown/Home/Endキーで、縦横無尽に動く状態。
//    ・textareaはメインデータテーブルの背後に隠れていて、見えない
//    ・裏ではtextarea文字列を全選択している状態なので、セルが見えなくても、ダイレクト入力や、copy、deleteなどがそのまま効く。
// �� gEMode == "edit1" （簡易編集モード）の場合
//    ・文字キーを押して、文字の直接入力が開始されたときの状態
//    ・textareaはメインデータテーブルの前面に表示され、編集されていることが視覚的にわかる状態
//    ・ただし、十字キーやTab/Enter/PageUp/PageDown/Home/End を押すと、移動モードの命令とみなされ、移動モードに戻る。
// �� gEMode == "edit2" （詳細編集モード）の場合
//    ・F2キーを押すか、マウスクリックで文字間編集を行おうとしたときの状態
//    ・textareaはメインデータテーブルの前面に表示され、編集されていることが視覚的にわかる状態
//    ・十字キー/Home/End を押すと、セル移動でなく、セル内のキャレット移動となる状態
//    ・Tab/Entern、およびEscキーで解除される。/PageUp/PageDowは押しても無効である。
//―――――――――――――――――――――――――――――――――――――――
function textKeyDown(evt) {
    var wevt = window.event;
    var key = (wevt ? wevt.keyCode || wevt.which : evt.keyCode);
    evt = (evt ? evt :window.event);
    if (evt) {
        var ctrl  = typeof evt.modifiers == 'undefined' ? evt.ctrlKey : evt.modifiers & Event.CONTROL_MASK;
        var shift = typeof evt.modifiers == 'undefined' ? evt.shiftKey : evt.modifiers & Event.SHIFT_MASK;
    } else {
        var ctrl = wevt.ctrlKey;
        var shift = wevt.shiftKey;
    }
    // 簡易編集または詳細編集モード中に、Escが押された。移動モードになる。
    if (key==27 && gEMode!="") { // 27[Esc]
        activateCell(); // 引数なしは移動モードになる
        return stopEvent(evt);
    }
    var ret = (key==9 ? stopEvent : dummyEvent); // 9[Tab]
    // F2が押された。詳細編集モードへ。
    if (key==113) { // 113[F2]
        gEMode="edit2";
    }
    // Shift無しでEnterが押された。移動モードに戻りつつ、[↓]キーを押したこととする。（Shift+Enterはセルへの改行挿入）
    if (key==13 && !shift) { // 13[Enter]
        if (gEMode!="") { key = 40; gEMode=""; }  ret = stopEvent; // 40[↓]
    }
    // Tabキーが押された。移動モードに戻る。
    if (key==9) { // [tab]
        if (gEMode!="") { gEMode=""; ret = stopEvent; }
    }
    // 移動モードで[Delete]が押された。セルをクリアして終了
    if (key==46 && gEMode=="") {
        var finfo = gFieldInfo[gTDIdx];
        if (!finfo.is_readonly) {
            gTBox.value = "";
            textKeyUp(evt);
        }
        return stopEvent(evt);
    }
    // 詳細モードでなければ移動系を実行。
    if (gEMode=="" || gEMode=="edit1") {
        // Endが押された。最右列へ。
        if (key==35 && gEMode=="") {// 35[End]
            var nn=getNextNode(gTD);
            gTD = getTD(gColLast, gTR);
            gTDIdx = gColLast;
            activateCell(); // 引数なしは移動モードになる
            return stopEvent(evt);
        }
        // PageUp / PageDownが押された。上下10行移動
        if (key==33 || key==34) {// 33[PageUp] or 34[PageDown]
            var idx=Math.max(1, gTRIdx-10);
            if (key==34) idx = Math.min(gRowCount, gTRIdx*1+10);
            var nn=getTR(idx);
            if (!nn) return ret(evt);
            gTR = nn;
            gTRIdx = idx;
            gTRKey = gTR.id;
            gTD = getTD(gTDIdx, gTR);
            activateCell(); // 引数なしは移動モードになる
            return stopEvent(evt);
        }
        // [←]か、[Shift]+[Tab]か、[Home]を押された。左移動
        if (key==37 || (key==9 && shift) || (key==36 && gEMode=="")) { // 37[←] 9[Tab] 36[Home]
            if (gTDIdx<=gColFirst) return ret(evt); // これ以上左へ移動できない。終わり。
            if ((key==37 && ctrl) || key==36) { // [Ctrl]+[←]または[Home]なら、最左列へ
                gTD = getTD(gColFirst, gTR);
                gTDIdx = gColFirst;
            } else if (key==37 && shift) { // [Shift]+[←]なら、５列左へ
                var nidx = Math.max(gColFirst, gTDIdx-5);
                gTD = getTD(nidx, gTR);
                gTDIdx = nidx;
            } else { // それ以外。ひとつ左へ
                var nn=getPrevNode(gTD);
                gTD=nn;
                gTDIdx--;
            }
            activateCell(); // 引数なしは移動モードになる
            return stopEvent(evt);
        }
        // [↑]を押された。ひとつ上へ
        if (key==38) { // 38[↑]
            if (gTRIdx<=1) return ret(evt);
            var nn=getPrevNode(gTR);
            gTR=nn;
            gTRIdx--;
            gTRKey = gTR.id;
            gTD=getTD(gTDIdx, gTR);
            activateCell(); // 引数なしは移動モードになる
            return stopEvent(evt);
        }
        // [→]または、[Shift]なし[Tab]を押された。右移動
        if (key==39 || (key==9 && !shift)) { // 39[→] 9[Tab]
            var nn=getNextNode(gTD);
            if (key==39 && ctrl) { // [Ctrl]+[→]。最右列へ
                gTD = getTD(gColLast, gTR);
                gTDIdx = gColLast;
            } else if (key==39 && shift) { // [Shift]+[→]。５列右へ
                var nidx = Math.min(gColLast, gTDIdx+5);
                gTD = getTD(nidx, gTR);
                gTDIdx = nidx;
            } else { // それ以外。ひとつ右へ
                if (!nn) return ret(evt);
                gTD=nn;
                gTDIdx++;
            }
            activateCell(); // 引数なしは移動モードになる
            return stopEvent(evt);
        }
        // [↓]または[Shift]なし[Enter]を押された。ひとつ下へ
        if (key==40 || (key==13 && !shift)) {// 40[↓] 13[Enter]
            var nn=getNextNode(gTR);
            if (!nn) return ret(evt);
            gTR=nn;
            gTRIdx++;
            gTRKey = gTR.id;
            gTD=getTD(gTDIdx, gTR);
            activateCell(); // 引数なしは移動モードになる
            return stopEvent(evt);
        }
    }
    // コピー/カット/貼り付け
    // 移動モードのときにコピーを実行すると、空白文字１個をコピーすることになる。
    // そのまま貼り付ければ、空白文字１個を貼り付けたことになる。
    // このことによって、「空白コピー」「空白ペースト」というものが実現可能となった。
    gCtrl = ""; // コントロールと同時に押した有効キー
    if (ctrl && key==86) { gCtrl = "v"; } // ctrl + 86[v] 貼り付け
    if (ctrl && key==67) { gCtrl = "c"; } // ctrl + 67[c] コピー
    if (ctrl && key==88) { gCtrl = "x"; } // ctrl + 88[x] カット

    // 意味のあるキーコード一覧。これらのキーを押された場合、いま移動モード中なら、edit1簡易編集モードになる。
    // ちなみに
    // ・半角全角や変換キーなどは含まれない（キーが取れるか試してない）。フォーカスはテキストエリアにあるので問題ない。
    // ・移動モードのとき、変換キーを押すと変換が開始されるが、
    //   ここでのタイミングではなく、「setInterval()」処理の箇所で引っかかって簡易編集モードに遷移するようになっている。
    var el = [
        8,48,49,50,51,52,53,54,55,56,57,58,59,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,
        81,82,83,84,85,86,87,88,89,90,96,97,98,99,100,101,102,103,104,105,106,107,109,110,111,113,160,173,188,190,191,219,220,221
    ];
    var bb=0;
    for (var idx=0; idx<el.length; idx++) if (el[idx]==key) bb = 1;
    if (key==13 && shift) bb = 1; // [Shift]+[Enter]でも、移動モードから簡易編集モードに移行させる。
    if (gCtrl) bb = 0; // 貼り付け/コピー/カット系は、モード変更を行わせない。

    if (bb) {
        if (gEMode=="") gEMode = "edit1"; // 移動モードなら、簡易編集モードへ
        if (gTBox.value==" ") gTBox.value = ""; // 簡易編集モードになって可視化するので、ダミー空白文字は除去
    }
    // [F2]を押すと、詳細編集モードに入るだけでなく、キャレットを文字の末尾へ移動するように。
    if (key==113) { // 113[F2]
        gTBox.focus();
        if (gTBox.createTextRange) {
            var range = gTBox.createTextRange();
            range.move('character', gTBox.value.length);
            range.select();
        } else if (gTBox.setSelectionRange) {
            gTBox.setSelectionRange(gTBox.value.length, gTBox.value.length);
        }
    }
}
//―――――――――――――――――――――――――――――――――――――――
// テキストエリア キーアップイベント。この画面GUIのサウスブリッジ的な箇所。
//―――――――――――――――――――――――――――――――――――――――
function textKeyUp(evt) {
    var wevt = window.event;
    var key = (wevt ? wevt.keyCode || wevt.which : evt.keyCode);

    // 現在の<TD>の値と、textareaの値取得。半角空白があれば除去
    // TDの値は、gNewDataの値と同じである。
    // 本来はgNewDataの値との比較を行うのが厳密である。
    // が、gNewDataがgCurDataと同じ値のとき、gNewDataが存在しないので、
    // gNewDataと比較するのは面倒。よってTDの値との比較で済ます。
    var tdval = gTD.innerHTML.replace(/<br>/g, "\n");
    if (tdval==" ") tdval = "";
    var tval = gTBox.value;
    if (tval==" ") tval="";

    // 直前の値との間に変更が無い。おとなしく終了。
    if ((tdval == tval) && gEMode=="") return;

    // 以降、なにがしか変更があった場合。
    // まずTDタグの値を変更。
    gTD.innerHTML = htmlEscape(tval).replace(/\n/g, "<br>");

    // ＤＢ現データの値を取得
    var v = "";
    var finfo = gFieldInfo[gTDIdx];
    if (gCurData[gTRKey]) v = str(gCurData[gTRKey][finfo.field]);
    if (v==" ") v = "";

    // リードオンリー項目でなければ新データを編集する。
    // リードオンリー項目やフォーミュラ項目は、別途、execFormula()などで新データが作成されるであろう。
    // 真のリードオンリー項目（患者名や性別年齢）は、DBには保存されることは無いであろう。
    if (!finfo.is_readonly) {
        // ＤＢ現データとの間に差異あり。
        if (v!=tval) {
            // 新データを作成する
            if (!gCngData[gTRKey][finfo.field]) {
                gCngData[gTRKey][finfo.field] = 1;
                gDiffCount++;
                gDivDiffCount.innerHTML = "変更箇所："+gDiffCount;
            }
            gNewData[gTRKey][finfo.field] = tval;
            $(gTD).addClass("valchanged"); // セルを青くする
        }
        // ＤＢ現データとの間に差異なし
        else {
            // 新データがあれば除去する
            if (gCngData[gTRKey][finfo.field]) {
                gCngData[gTRKey][finfo.field] = "";
                //delete gCngData[gTRKey][finfo.field];
                gDiffCount--;
                gDivDiffCount.innerHTML = (gDiffCount ? "変更箇所："+gDiffCount : "");
            }
            delete gNewData[gTRKey][finfo.field];
//            gNewData[gTRKey][finfo.field] = "";
            $(gTD).removeClass("valchanged"); // セルの青色を解除
        }
        // フォーミュラ実行
        execFormula(finfo.field, gTR, gTRKey);
        // 編集したこのセルに、条件付書式を実行
        applyStyle(finfo.field, gTD);
    }

    adjustHeader(); // セル高、セル幅をシンクロ

    // カットペースト系はセルリセットが必要。カットペーストカットを繰り返す場合があるため。しかし連続ペーストはちょっとおかしくなる。
    if (gCtrl && gEMode=="") activateCell();
    else resizeDivAndText(); // 赤い編集ボックスのサイズ位置調整

    // [F2]の場合、キャレットを末尾へ
    if (key==113) {
        gTBox.focus();
        if (gTBox.createTextRange) {
            var range = gTBox.createTextRange();
            range.move('character', gTBox.value.length);
            range.select();
        } else if (gTBox.setSelectionRange) {
            gTBox.setSelectionRange(gTBox.value.length, gTBox.value.length);
        }
    }
}


//―――――――――――――――――――――――――――――――――――――――
// 指定した行に対し、フォーミュラを実行
// field：テキスト変更した列ID
// tr: テキスト変更した行
// trkey: テキスト変更した行trのID
//―――――――――――――――――――――――――――――――――――――――
function execFormula(field, tr, trkey) {
    if (field && !gFormulaBases[field]) return; // テキスト変更した列IDは、フォーミュラ計算元には含まれていないものだった。終了。
    var stat = ""; // デバッグステータス保持用
    try {
        for (var baseField in gFormulaBases) eval("var "+baseField+' = ""'); // 計算元セルを、evalを使ってすべてレキシカル変数にしておく。値はカラ文字で。
        for (var idx=0; idx<gFormulaInfo.length; idx++) { // フォーミュラ構造体を適用順にループ
            var target = gFormulaInfo[idx].target; // targetは列ID。この列IDのセルを、これから計算する。
            var format = str(gFormulaInfo[idx].format);
            stat = "Formula Error A";            var isNNExist = 0; // 計算元のセル群中、計算を実行すべき値があれば1となる。
            stat = "Formula Error B";            var bases = gFormulaInfo[idx].base; // 計算元セルIDの配列。
            stat = "Formula Error C";            for (var idx2=0; idx2<bases.length; idx2++) { // 計算元セルIDのループ
            stat = "Formula Error C1";               var fieldIdx = gFieldIndexes[bases[idx2]]; // この計算元セルの定義内位置番号
            stat = "Formula Error C2";               var finfo = gFieldInfo[fieldIdx]; // この計算元セルのフィールド定義を取得
            stat = "Formula Error D1";               var nn = str(gCurData[trkey][bases[idx2]]); // ★★★計算元値
            stat = "Formula Error D2";               if (gCngData[trkey][bases[idx2]]) nn = str(gNewData[trkey][bases[idx2]]); // ★★★計算元値
            stat = "Formula Error E ";               nn = nn.replace(/\n/g, "").replace(/\"/g, ""); // 計算値は通常数値。改行は除去する。ダブルクォートも除去。
            stat = "Formula Error F";                eval(bases[idx2]+'= "'+nn+'";'); // レキシカル変数に値を文字列代入する。
            // 計算を実際に行うかどうかのフラグを決める。
            // 計算元値がすべてカラ文字なら、計算結果をカラ文字にしたいため。
            // よって、ひとつでも有効な入力があれば、計算を開始するようにしたい。
            // ただし、もし計算元値に値があっても、それがリードオンリー（年齢性別など）なら、計算をする判定にはならない。
            // なぜなら、年齢や性別は、常に値があって当たり前だからである。定数のようなものである。
            // しかし、リードオンリーであっても、それがフォーミュラ結果値（pp20_tlc）なら、計算は行うべきである。
            // これに値がセットされているということは、それ以前に計算元になった入力値があるはずである。まとめると、
            // ◆この計算元値に、なんらかの文字が入力されていること
            // ◆この計算元列は、リードオンリーでは無いか、あるいは別のフォーミュラ結果であること
            stat = "Formula Error F2";               if (nn!="" && (!finfo.is_readonly || gFormulaTargets[bases[idx2]])) isNNExist = 1;
            stat = "Formula Error F3";           }
            stat = "Formula Error G1";           eval("var out = "+gFormulaInfo[idx].formula+";"); // ★★★どちらにせよフォーミュラ実行。eval利用。
            stat = "Formula Error G2";           if (typeof out === "undefined") out = "";
            stat = "Formula Error G3";           var out2 = str(isNNExist ? out : ""); // もし実行不要なら、結果はカラ文字とする。
            stat = "Formula Error H1";           var tOut = "";
            stat = "Formula Error H2";           var tFormat = gFormulaInfo[idx].format; // この結果列のフォーマット指定。roundなど。
            stat = "Formula Error H3";           var tFormatValue = (tFormat ? tFormat.replace('値', out) : out); // フォーマット式を作成
            stat = "Formula Error H4";           if (tFormatValue!="" && isNNExist && out2!="") eval("tOut = "+tFormatValue+";"); // フォーマット実行。eval利用
            stat = "Formula Error I1";           if (gCurData[trkey][target]===tOut) { // 結果がDB現データと等しいなら
            stat = "Formula Error I2";               gCngData[trkey][target] = ""; // データ変更ありフラグをオフに
            stat = "Formula Error I3";               gNewData[trkey][target] = ""; // 新データを消す
            stat = "Formula Error I4";           } else { // もしDB現データと異なる結果が出たら
            stat = "Formula Error I5";               gCngData[trkey][target] = 1; // データ変更ありフラグをオンに
            stat = "Formula Error J1";               gNewData[trkey][target] = out2; // 新データに格納する。ここではまだround()などを実行していないことに注意。
            stat = "Formula Error J2";           }
            stat = "Formula Error K";            var formatFormula = str(format ? format.replace('値', out) : out); // round等のフォーマットを行う
            stat = "Formula Error J";            var td = getTD(gFieldIndexes[target], tr); // メインデータテーブル内のTDセルを取得
            stat = "Formula Error L";            td.innerHTML = tOut;// ｾﾙにﾌｫｰﾏｯﾄ後の値をｾｯﾄ。
            stat = "Formula Error M";            applyStyle(target, td); // フォーミュラしたこのセルに、条件つき書式を実行
        }
        // 上記までで、この行に対しフォーミュラを実行したわけだが、
        // 新データ（gNewData）には、不適切な小数表現などが含まれたままである。
        // この行の新データのフォーミュラ結果値それぞれを、フォーマット指定があればフォーマットして格納しなおす。
        for (var idx=0; idx<gFormulaInfo.length; idx++) {
            var target = gFormulaInfo[idx].target; // とあるフォーミュラ結果列の列ID
            var format = gFormulaInfo[idx].format; // この結果列のフォーマット指定。roundなど。
            if (!format) continue; // このフォーミュラ結果には、フォーマット指定が特に指定されていない。スルー。
            if (!gNewData[trkey]) continue; // 新データが無い。スルー
            if (str(gNewData[trkey][target]).length==0) {
                //delete gCngData[trkey][target]; // 変更ありフラグも落とす
                gCngData[trkey][target] = ""; // 変更ありフラグも落とす
                continue; // 新データが無い。スルー
            }
            var formatValue = format.replace('値', gNewData[trkey][target]); // フォーマット式を作成
            eval("gNewData[trkey][target] = "+formatValue+";"); // フォーマット実行。eval利用。結果を新データに格納しなおす
            if (gCurData[trkey][target]==gNewData[trkey][target]) {
                //delete gNewData[trkey][target]; // もしフォーマット結果が現データと同じなら、新データを抹消。
                //delete gCngData[trkey][target]; // 変更ありフラグも落とす
                gNewData[trkey][target] = ""; // もしフォーマット結果が現データと同じなら、新データを抹消。
                gCngData[trkey][target] = ""; // 変更ありフラグも落とす
            }
        }
    } catch(ex) {
        alert(stat);
        throw ex;
    }
}

//―――――――――――――――――――――――――――――――――――――――
// セルTDへ条件付き書式を実行する。セルTDの見た目（innerHTML）の値に対して行う。
//―――――――――――――――――――――――――――――――――――――――
function applyStyle(field, td, tr) {
    if (!gStyleTargets[field]) return; // この列には条件付き書式が定義されていない。終わり。
    for (var idx=0; idx<gStyleInfo.length; idx++) { // 条件付き書式定義を適用順にループ
        var info = gStyleInfo[idx]; // とある条件付き書式定義。
        if (field!=info.target) continue; // もしこの書式定義が、対象の列IDのものでなければスルー
        if (!td) td = getTD(gFieldIndexes[field], tr);
        var joken = info.joken.replace('値', '"'+td.innerHTML.replace(/\"/,'\"')+'"'); // 条件の「値」を、実際のinnerHTMLに置換して式を作成
        var bool = 0;
        try { eval("bool = ("+joken+");");} catch(ex){ throw ex; }; // evalで式を評価する。結果はboolに格納される。
        td.style.color = (bool ? info.color : ""); // もし結果boolがtrue判定されるなら、TDタグに色つけする。そうでなければ色を外す。
        td.style.fontWeight = (bool ? "bold" : ""); // もし結果boolがtrue判定されるなら、TDタグにBOLD属性をつける。そうでなければBOLDを外す。
        if (bool) break; // もし結果boolがtrueとなったのなら、後続条件は実行しない。ここで終了。
    }
}
//―――――――――――――――――――――――――――――――――――――――
// メインデータテーブルとヘッダテーブルのセルサイズをシンクロさせる
//―――――――――――――――――――――――――――――――――――――――
function adjustHeader() {
    // これからセル幅/セル高をいじるが、いじっている最中に、IEはスクロール位置がゼロに戻ってしまうので、
    // いったん現在のスクロール位置を保持
    var scl = gScrollDiv.scrollLeft;
    var sct = gScrollDiv.scrollTop;
    // colspan部を整える。
    // colspan配下のtdの幅を計算し、親TRに与える。
    // そうしないと、いくらtdでwidthを指定しても、セルの中身がカラだったりすると、
    // 指定幅を裏切って、ブラウザが幅調整を勝手に行ってしまうため。
    for (var thID in gColspanGroups) {
        var ttlWidth = 0;
        // colspan属性のついた<TH>は、いったん幅をクリア（autoにする）（現状、「栄養管理上の目標」ヘッダのみ）
        ee(thID).childNodes[0].style.width = "auto"; // THタグ
        // 配下のTDタグの幅を足してゆく（「栄養管理上の目標」なら、３列分。）
        for (var td in gColspanGroups[thID]) {
            ttlWidth += Math.max(parseInt(gColspanGroups[thID][td].style.width), gColspanGroups[thID][td].offsetWidth);
        }
        // 改めて、THタグに幅をセット
        ee(thID).childNodes[0].style.width = ttlWidth + "px";
    }
    // 上部ヘッダのセル幅を、すべてメインデータテーブルのセル幅にあわせる。
    // コンテンツ幅のほうが小さい場合、セル幅が合わせられないので、その可能性があるなら$FIELD_INFOでwidthを定義してやること。
    for (var idx=0; idx<gMainTableContentCells.length; idx++) {
        gMainTableHeaderTopCells[idx].style.width = (gMainTableContentCells[idx].offsetWidth-8) + "px";
    }
    // 左上コーナー部ヘッダのセル幅を、すべて上部ヘッダテーブルのセル幅にあわせる。
    for (var idx=0; idx<gMainTableHeaderTopLeftCells.length; idx++) {
        gMainTableHeaderTopLeftCells[idx].style.width = gMainTableHeaderTopCells[idx].style.width;
    }

    // 左部ヘッダのセル高を、すべてメインデータテーブルにあわせる。
    for (var trkey in gCurData) {
        var ltr = ee("ltr_"+trkey);
        var mtr = ee(trkey);
        ltr.style.height = mtr.offsetHeight + "px";
    }
    // スクロール位置を戻す
    gScrollDiv.scrollLeft = scl;
    gScrollDiv.scrollTop = sct;
}
//―――――――――――――――――――――――――――――――――――――――
// 年月プルダウン変更イベント
//―――――――――――――――――――――――――――――――――――――――
function changeYM(cmb, viewMode) {
    if (!isChangeViewOK()) { setComboValue(cmb.id, gMemory[cmb.id]); return; }
    gMemory[cmb.id] = getComboValue(cmb.id);
    var date_y = (viewMode=="ward_mode" ? "date_y" : "date_y_to");
    var date_m = (viewMode=="ward_mode" ? "date_m" : "date_m_to");

    // 現在の選択年より３年前までを、延々さらに選択できるようにする。
    var optionsY = ee(date_y).options;
    var lastYY = optionsY[optionsY.length-1].value;
    var curYY = int(getComboValue(date_y));
    for (var yy=curYY-1; yy>=curYY-3; yy--) {
        if (yy<lastYY) optionsY[optionsY.length] = new Option(yy, yy);
    }

    // 年月を本日年月と比較する。
    if (getComboValue(date_y)>"<?=date("Y")?>" || (getComboValue(date_y)=="<?=date("Y")?>" && getComboValue(date_m) > "<?=date("m")?>")) {
        alert("未来の年月は指定できません。");
        setComboValue(date_y, "<?=date("Y")?>");
        setComboValue(date_m, "<?=date("m")?>");
    }
    // （このままでも間違いではないのだが）、もし「栄養管理計画書」モードで開いていたのなら、
    // 紛らわしいのでいったん「栄養スクリーニングシート」モードに戻す。
    ee("list_mode_screening").checked = true;
    // リロード
    changeView(viewMode, gPtifId, gInptInDt);
}
//―――――――――――――――――――――――――――――――――――――――
// 事業所プルダウン変更イベント
//―――――――――――――――――――――――――――――――――――――――
function bldgChanged(doChangeView) {
    if (!isChangeViewOK()) { setComboValue("sel_bldg_cd", gMemory.sel_bldg_cd); return; }
    ee("sel_ptrm_room_no_container").innerHTML = "";
    var cmbTeam = ee("sel_team_id");
    cmbTeam.options.length = 1;
    var bldg_cd = getComboValue("sel_bldg_cd");
    var cmbWard = ee("sel_ward_cd");
    // 病棟プルダウン中身を再作成。そうでなければ病棟プルダウンをクリア
    cmbWard.options.length = 0;
    var idx = 0;
    for(var i in gWardSelections){
        if (gWardSelections[i].bldg_cd != bldg_cd) continue;
        cmbWard.options.length = cmbWard.options.length + 1;
        cmbWard.options[idx].text  = gWardSelections[i].name;
        cmbWard.options[idx].value = gWardSelections[i].code;
        idx++;
    }
    gMemory.sel_ward_cd = getComboValue("sel_ward_cd");
    if (doChangeView) changeView("ward_mode"); // doChangeViewなら（つまりオンロード直後でなければ）リロードする。
}
//―――――――――――――――――――――――――――――――――――――――
// 病棟プルダウン変更イベント
//―――――――――――――――――――――――――――――――――――――――
function wardChanged(doChangeView) {
    if (!isChangeViewOK()) { setComboValue("sel_ward_cd", gMemory.sel_ward_cd); return; }
    ee("sel_ptrm_room_no_container").innerHTML = "";
    var bldg_cd = getComboValue("sel_bldg_cd");
    var ward_cd = getComboValue("sel_ward_cd");
    var cmbTeam = ee("sel_team_id");
    // 病棟を指定している場合は、チームプルダウンを再作成。そうでなければチームプルダウンはクリア
    cmbTeam.options.length = 1;
    if (ward_cd) {
        var idx = 1;
        for(var i in gTeamSelections){
            cmbTeam.options.length = cmbTeam.options.length + 1;
            cmbTeam.options[idx].text  = gTeamSelections[i].team_nm;
            cmbTeam.options[idx].value = gTeamSelections[i].team_id;
            idx++;
        }
    }
    gMemory.sel_ward_cd = getComboValue("sel_ward_cd");
    // 病室チェックボックスをチェック状態で作成
    prepareRoomCheckbox();
    if (doChangeView) changeView("ward_mode"); // doChangeViewなら（つまりオンロード直後でなければ）リロードする。
}
//―――――――――――――――――――――――――――――――――――――――
// チームプルダウン変更イベント
//―――――――――――――――――――――――――――――――――――――――
function teamChanged(doChangeView) {
    if (!isChangeViewOK()) { setComboValue("sel_team_id", gMemory.sel_team_id); return; }
    gMemory.sel_team_id = getComboValue("sel_team_id");
    // 病室チェックボックス再作成
    ee("sel_ptrm_room_no_container").innerHTML = "";
    prepareRoomCheckbox();
    if (doChangeView) changeView("ward_mode"); // doChangeViewなら（つまりオンロード直後でなければ）リロードする。
}
//―――――――――――――――――――――――――――――――――――――――
// 病室チェックボックスをクリアして描画しなおす
//―――――――――――――――――――――――――――――――――――――――
function prepareRoomCheckbox(isAllOff) {
    var bldg_cd = getComboValue("sel_bldg_cd");
    var ward_cd = getComboValue("sel_ward_cd");
    var team_id = getComboValue("sel_team_id");
    var html = [];
    gPtrmAllCodeList = [];
    gPtrmCodeList = [];
    var isCheckExist = 0;
    for(var i in gRoomSelections){
      if (gRoomSelections[i].bldg_cd != bldg_cd) continue;
      if (gRoomSelections[i].ward_cd != ward_cd) continue;
      var room = gRoomSelections[i].code;
      var joint_code = bldg_cd+"_"+ward_cd+"_"+room+"_"+team_id;
      if (team_id && !team_relations[joint_code]) continue;
      gPtrmAllCodeList.push(room);
      gPtrmCodeList.push(room);
      var checked = (isAllOff ? "" : " checked");
      html.push('<div><label><input type="checkbox" id="chkRoom'+room+'" onclick="roomChecked()" '+checked+' />'+gRoomSelections[i].name+'</label></div>');
      isCheckExist = 1;
    }
    if (isCheckExist) {
        html.push('<div style="padding-left:4px"><a href="javascript:void(0)" onclick="prepareRoomCheckbox(1); roomChecked();">全解除</a></div>');
        html.push('<div style="padding-left:4px"><a href="javascript:void(0)" onclick="prepareRoomCheckbox(); roomChecked();">全選択</a></div>');
    }
    html.push('<div style="clear:both"></div>');
    ee("sel_ptrm_room_no_container").innerHTML = html.join("");
}
//―――――――――――――――――――――――――――――――――――――――
// 病室チェックボックスのクリックイベント
//―――――――――――――――――――――――――――――――――――――――
function roomChecked() {
    if (!isChangeViewOK()) return false;
    gPtrmCodeList = [];
    for (var idx=0; idx<gPtrmAllCodeList.length; idx++) {
        if (!ee("chkRoom"+gPtrmAllCodeList[idx]).checked) continue;
        gPtrmCodeList.push(gPtrmAllCodeList[idx]);
    }
    changeView("ward_mode");  // リロード
}

<?
//**********************************************************************************************************************
// PHPその８）事業所、病棟、病室、チーム、チームリレーションデータを全部、JavaScript変数として保持しておく
//**********************************************************************************************************************
// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel = sql_select("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) {
  $bldg_data_options[] = '<option value="'.$row["bldg_cd"].'">'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel = sql_select("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "gWardSelections = [". implode(",", $ary) . "];\n";

// 病室選択肢 JavaScript変数として出力する
$sel = sql_select("select bldg_cd, ward_cd, ptrm_room_no, ptrm_name from ptrmmst where ptrm_del_flg = 'f' order by ptrm_name, ptrm_room_no");
$mst = (array)pg_fetch_all($sel);
$ary = array();
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "gRoomSelections = [". implode(",", $ary) . "];\n";

// チーム選択肢 JavaScript変数として出力する
$sel = sql_select("select team_id, team_nm from teammst where team_del_flg = 'f' order by team_id");
$mst = (array)pg_fetch_all($sel);
$ary = array();
foreach($mst as $key => $row) $ary[] = '{"team_id":"'.$row["team_id"].'","team_nm":"'.$row["team_nm"].'"}';
echo "gTeamSelections = [". implode(",", $ary) . "];\n";

// チームリレーション選択肢 JavaScript変数として出力する
$sel = sql_select("select bldg_cd, ward_cd, ptrm_room_no, team_id from sot_ward_team_relation");
$mst = (array)pg_fetch_all($sel);
$ary = array();
echo "team_relations = {};\n";
foreach($mst as $key => $row) echo 'team_relations["'.$row["bldg_cd"].'_'.$row["ward_cd"].'_'.$row["ptrm_room_no"].'_'.$row["team_id"].'"]=1;'."\n";

?>
</script>
</head>
<?
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//
// BODY
//
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<body onload="prepareContents()"<? if (@$_REQUEST["print_preview"]) {?>style="padding:1px"<? } ?> onresize="documentResized()">




<?//==================================================================================================================?>
<?// タイトルバー                                                                                                     ?>
<?//==================================================================================================================?>
<table width="100%" style="font-size:16px; font-weight:bold;" id="header_menu_table">
  <tr class="bg-top-line">
    <td style="width:32px; height:32px; text-align:left; padding-left:4px"><img src="img/icon/b57.gif?v=2" alt="メドレポート"/></td>
    <td style="padding-left:8px;">メドレポート&nbsp;&gt;&nbsp;<span id="view_mode_title"></span></td>
    <td style="padding-right:10px; width:80px">
        <a href="javascript:void(0)" onclick="histView()" id="btnmove_back_on" style="display:none">戻る</a>
        <span id="btnmove_back_off" style="color:#999999">戻る</span>
        <a href="javascript:void(0)" onclick="histView(1)" id="btnmove_forward_on" style="display:none">進む</a>
        <span id="btnmove_forward_off" style="color:#999999">進む</span>
    </td>
    <td style="padding-right:4px; width:24px">
        <a href="javascript:void(0)" onclick="if(isChangeViewOK(1))window.close()" class="closebtn"></a>
    </td>
  </tr>
</table>




<?//==================================================================================================================?>
<?// 検索条件部分（通常一覧用）                                                                                       ?>
<?//==================================================================================================================?>
<div id="div_ward_mode">
  <table class="list_table" style="margin-bottom:8px; margin-top:4px" cellspacing="1">
    <tr>
      <th style="background-color:#b3daf9" width="70"><nobr>事業所(棟)</nobr></th>
      <td width="60"><select onchange="return bldgChanged(1);" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
      <th style="background-color:#b3daf9" width="36"><nobr>病棟</nobr></th>
      <td width="60"><select onchange="return wardChanged(1);" id="sel_ward_cd"></select></td>
      <th style="background-color:#b3daf9" width="36"><nobr>チーム</nobr></th>
      <td width="60"><select onchange="return teamChanged(1);" id="sel_team_id"><option value="">　　　　</option></select></td>
      <th style="background-color:#b3daf9" width="36" rowspan="2"><nobr>病室</nobr></th>
      <td  rowspan="2" id="sel_ptrm_room_no_container" style="vertical-align:top"></td>
    </tr>
    <tr>
      <th style="background-color:#b3daf9" width="40"><nobr>年月</nobr></th>
      <td width="180" colspan="5"><nobr>
            <select id="date_y" onchange="return changeYM(this, 'ward_mode')">
            <? for ($i=date("Y"); $i>=date("Y")-14; $i--) { ?>
            <option value="<?=$i?>"<?=($i==date("Y")?" selected":"")?>><?=$i?></option>
            <? } ?>
            </select>&nbsp;年
            <select id="date_m" onchange="return changeYM(this, 'ward_mode')">
            <? for ($i=1; $i<=12; $i++) { ?>
            <option value="<?=sprintf("%02d",$i)?>"<?=($i==date("m")?" selected":"")?>><?=$i?></option>
            <? } ?>
            </select>&nbsp;月入院中</nobr>
      </td>
      </tr>
    </table>
</div>




<?//==================================================================================================================?>
<?// 検索条件部分（患者別一覧用）                                                                                     ?>
<?//==================================================================================================================?>
<div id="div_ptif_mode" style="display:none">
  <table class="list_table" style="margin-bottom:8px; margin-top:4px" cellspacing="1">
    <tr>
      <th bgcolor="#fefcdf" width="46">患者ID</th>
      <td width="80" id="ptif_mode_id" class="center"></td>
      <th bgcolor="#fefcdf" width="46">患者名</th>
      <td width="80" id="ptif_mode_name" style="white-space:nowrap" class="center"></td>
      <th bgcolor="#fefcdf" width="46">入院日</th>
      <td width="80" id="edit_inpt_in_dt" style="white-space:nowrap" class="center">
      <select name="inpt_in_dt" id="inpt_in_dt" onchange="changeView(gViewMode, gPtifId, this.value);"></select>
      </td>
      <th bgcolor="#fefcdf" width="46">期間</th>
      <td>
        <select id="date_y_to" onchange="return changeYM(this, 'ptif_mode')">
            <? for ($i=date("Y"); $i>=date("Y")-14; $i--) { ?>
            <option value="<?=$i?>"><?=$i?></option>
            <? } ?>
            </select>&nbsp;年
        <select id="date_m_to" onchange="return changeYM(this, 'ptif_mode')">
            <? for ($i=1; $i<=12; $i++) { ?>
            <option value="<?=sprintf("%02d",$i)?>"><?=$i?></option>
            <? } ?>
            </select>&nbsp;月
        <select id="kikan_ym" onchange="return changeYM(this, 'ptif_mode')">
            <option value="all" selected>過去すべて</option>
            <option value="3">過去3ヶ月間</option>
            <option value="5">過去5ヶ月間</option>
            <option value="100">過去1年間</option>
        </select>
      </td>
    </tr>
  </table>
</div>




<?//==================================================================================================================?>
<?// コマンド領域                                                                                                     ?>
<?//==================================================================================================================?>
<table cellspacing="0" cellpadding="0" style="width:100%"><tr>
<td style="padding-bottom:3px"><nobr>
    <span id="span_ward_mode1">
        <label><input type="radio" name="list_mode" value="keikakusyo" onclick="if (!isChangeViewOK()) return false; changeView('ward_mode')">栄養管理計画書</label>
        <span style="padding-left:10px">
            <label><input type="radio" name="list_mode" value="screening" id="list_mode_screening" onclick="if (!isChangeViewOK()) return false; changeView('ward_mode')" checked>栄養スクリーニングシート</label>
        </span>
    </span></nobr>
</td>
<td style="text-align:right; padding-bottom:3px"><nobr>
    <span style="padding-right:10px; font-weight:bold; color:#00aaee" id="deleteCount"></span>
    <span style="padding-right:10px; font-weight:bold; color:#00aaee" id="diffCount"></span>
    <button type="button" onclick="alert('【移動操作】\n------------------------------\n[↑]　　　　　上へ\n[↓]　　　　　下へ\n[←]　　　　　左へ\n[→]　　　　　右へ\n[Enter]　　　　下へ\n[Tab]　　　　　右へ\n[Shift]＋[Tab]　　左へ\n[Shift]＋[←]　　５列左へ\n[Shift]＋[→]　　５列右へ\n[Ctrl]＋[←]　　先頭列へ\n[Ctrl]＋[→]　　最終列へ\n[Home]　　　　先頭列へ\n[End]　　　　　　最終列へ\n[PageUp]　　　　１０行上へ\n[PageDown]　　　１０行下へ\n\n【入力操作】\n------------------------------\n[F2]　　セル内移動モードへ\n[Esc]　　セル内選択を解除\n[Shift]＋[Enter]　　　改行を挿入\n[Ctrl]＋[c]　　コピー\n[Ctrl]＋[v]　　貼り付け\n[Ctrl]＋[x]　　カット')">操作</button><!--
    --><button type="button" id="formula_preview" disabled onclick="alert(gCurFormula)">計算式</button><!--
    --><button type="button" id="style_preview" disabled onclick="alert(gCurStyle)">条件付き書式</button>
    <span id="span_ward_mode2">
        <span style="padding-left:30px"><button type="button" onclick="downloadExcel();" title="保存されたデータをExcel出力します。編集中データは含まれません。">Excel</button></span>
    </span>
    <span style="padding-left:10px"><button type="button" onclick="changeViewWithRef();" value="">一括引用</button></span>
    <span style="padding-left:10px"><button type="button" onclick="saveData();">　保存　</button></span></nobr>
</td>
</tr></table>



<?//==================================================================================================================?>
<?// スプレッドシート領域                                                                                             ?>
<?//==================================================================================================================?>
<div style="overflow:hidden; position:relative; height:300px; background-color:#dddddd" id="spread_sheet"></div>



</body>
<? pg_close($con); ?>
</html>


