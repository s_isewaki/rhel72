<?
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// ログインユーザのマイグループ情報を全削除
$sql = "delete from mygroup";
$cond = "where owner_id = '$emp_id' and mygroup_id = $mygroup_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 選択されたマイグループ情報を登録
if ($mygroup_list != "") {
	$order_no = 1;
	$arr_mygroup = split(",", $mygroup_list);
	foreach ($arr_mygroup as $member_id) {
		$sql = "insert into mygroup (owner_id, member_id, order_no, mygroup_id) values (";
		$content = array($emp_id, $member_id, $order_no, $mygroup_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$order_no++;

	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// マイグループ画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'option_mygroup.php?session=$session&mygroup_id=$mygroup_id&cls=$cls&atrb=$atrb&dept=$dept'</script>");
?>
