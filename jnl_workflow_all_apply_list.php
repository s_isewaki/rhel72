<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー | 申請参照一覧</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("show_jnl_workflow_all_apply_list.ini");
require_once("referer_common.ini");
require_once("jnl_workflow_common.ini");
require_once("jnl_application_workflow_select_box.ini");
require_once("jnl_application_workflow_common_class.php");
require_once("yui_calendar_util.ini");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 81, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new jnl_application_workflow_common_class($con, $fname);


// トランザクション開始
pg_query($con, "begin");
switch($mode)
{
	case "DELETE":
		foreach($apply_ids as $id)
		{
			$obj->update_delflg_all_apply($id, "t");
		}
		break;
	case "DELETE_CANCEL":
		foreach($apply_ids as $id)
		{
			$obj->update_delflg_all_apply($id, "f");
		}
		break;
	case "DRAG":
		if($select_box == "ALL")
		{
			$obj->update_delflg_all_apply($del_apply_id, "t");
		}
		else if($select_box == "DUST")
		{
			$obj->update_delflg_all_apply($del_apply_id, "f");
		}
		break;
}
// トランザクションをコミット
pg_query($con, "commit");

$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
	$tmp_wkfw_type = $row_cate["wkfw_type"];
	$tmp_wkfw_nm = $row_cate["wkfw_nm"];

	$arr_wkfwmst_tmp = array();
	$sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
	while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
		$tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
		$wkfw_title = $row_wkfwmst["wkfw_title"];

		$arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
	}
	$arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}


// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>

<?
// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
write_yui_calendar_script2(2);
?>

<script type="text/javascript">

//ロード時の処理
function init_action()
{
	//ドラッグアンドドロップを初期設定
	constructDragDrop();
	cateOnChange();
}


function search_apply()
{
    document.wkfw.action = "<?=$fname?>";
    document.wkfw.submit();
}

function cateOnChange() {

	var obj_cate = document.wkfw.search_category;
	var obj_wkfw = document.wkfw.search_workflow;

	var cate_id = getSelectedValue(obj_cate);
	// 申請書セレクトボックスのオプションを全削除
	deleteAllOptions(obj_wkfw);

	// 申請書セレクトボックスのオプションを作成
	addOption(obj_wkfw, '-', 'すべて');

<? foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) { ?>
	if (cate_id == '<? echo $tmp_cate_id; ?>') {
	<? foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm)
	   {
           $tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
           $tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
           $tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);
    ?>
		addOption(obj_wkfw, '<?=$tmp_wkfw_id?>', '<?=$tmp_wkfw_nm?>',  '<?=$search_workflow?>');
	<? } ?>
	}
<? } ?>
}



function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}


function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_action();initcal();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="日報・月報"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_wkfw_menuitem($session, $fname, "");
?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
</table>
<form name="wkfw" action="#" method="post">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="20%"></td>
			<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
			<td width="80%">

				<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="1" cellpadding="2" class="block">
								<tr>
									<td width="10%" bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
									<td width="50%" ><select name="search_category" onchange="cateOnChange();"><? show_cate_options($arr_wkfwcatemst, $search_category); ?></select></td>
									<td width="10%" bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font>
									<td width="30%" ><select name="search_workflow"></select></td>
								</tr>
								<tr>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
									<td><input type="text" size="40" maxlength="40" name="search_apply_title" value="<?=htmlspecialchars($search_apply_title)?>"></td>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
									<td><input type="text" size="30" maxlength="30" name="search_apply_emp_nm" value="<?=htmlspecialchars($search_apply_emp_nm)?>"></td>
								</tr>

								<tr>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
									<td>

										<table border="0" cellspacing="0" cellpadding="0" class="block_in">
											<tr>
												<td>
													<select id="date_y1" name="date_y1"><? show_select_years(10, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select>
												</td>
												<td>
													<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
													<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
														<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
													</font>
												</td>
												<td>
													&nbsp;〜<select id="date_y2" name="date_y2"><? show_select_years(10, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select>
												</td>
												<td>
													<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
													<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
													<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
														</font>
												</td>
											</tr>
										</table>

									</td>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請状況</font></td>
									<td>
										<select name="search_apply_stat"><? show_apply_stat_options($search_apply_stat); ?></select>
									</td>
								</tr>

								<tr>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
									<td colspan="3">
										<?show_post_select_box($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room);	?>
									</td>
								</tr>

								<tr>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文字列検索</font></td>
									<td colspan="2" style="border-right:none;"><input type="text" size="50" name="search_apply_content" value="<?=htmlspecialchars($search_apply_content)?>"></td>
									<td align="right" style="border-left:none;"><input type="button" value="検索" onclick="search_apply();"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td colspan="3"><img src="img/spacer.gif" width="1" height="3" alt=""></td>
		</tr>

<?
$arr_condition = array(
						"category" => $search_category,
						"workflow" => $search_workflow,
						"apply_title" => $search_apply_title,
                        "apply_emp_nm" => $search_apply_emp_nm,
						"date_y1" => $date_y1,
						"date_m1" => $date_m1,
						"date_d1" => $date_d1,
						"date_y2" => $date_y2,
						"date_m2" => $date_m2,
						"date_d2" => $date_d2,
                        "apply_stat" => $search_apply_stat,
                        "class" => $search_emp_class,
                        "attribute" => $search_emp_attribute,
                        "dept" => $search_emp_dept,
                        "room" => $search_emp_room,
						"apply_content" => $search_apply_content
					   );

if($select_box == "")
{
	$select_box = "ALL";
}

show_all_apply_list($con, $session, $fname, $select_box, $page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition);
?>
	</table>
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="select_box" id="select_box" value="<? echo($select_box); ?>">
	<input type="hidden" name="selected_cate" id="selected_cate" value="<? echo($selected_cate); ?>">
	<input type="hidden" name="selected_folder" id="selected_folder" value="<? echo($selected_folder); ?>">
	<input type="hidden" name="selected_wkfw_id" id="selected_wkfw_id" value="<? echo($selected_wkfw_id); ?>">
</form>
</td>
</tr>
</table>

<form id="csvform" name="csvform" action="jnl_workflow_all_apply_csv.php" method="post" target="download">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="select_box" value="<? echo($select_box); ?>">
	<input type="hidden" name="selected_cate" value="<? echo($selected_cate); ?>">
	<input type="hidden" name="selected_folder" value="<? echo($selected_folder); ?>">
	<input type="hidden" name="selected_wkfw_id" value="<? echo($selected_wkfw_id); ?>">
	<? foreach ($arr_condition as $k => $v) { ?>
	<input type="hidden" name="<? echo($k); ?>" value="<? echo($v); ?>">
	<? } ?>
	<input type="hidden" name="mode" value="">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
<?
// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from jnl_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from jnl_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// カテゴリオプションを出力
function show_cate_options($arr_cate, $cate) {
	echo("<option value=\"-\">すべて");
	foreach ($arr_cate as $tmp_cate_id => $arr) {
		$tmp_cate_name = $arr["name"];
		echo("<option value=\"$tmp_cate_id\"");
		if($cate != "" && $cate != '-') {
			if ($cate == $tmp_cate_id) {
				echo(" selected");
			}
		}
		echo(">$tmp_cate_name\n");
	}
}

// 申請状況オプションを出力
function show_apply_stat_options($stat) {

	$arr_apply_stat_nm = array("未承認", "承認確定", "否認", "差戻し");
	$arr_apply_stat_id = array("0", "1", "2", "3");

	echo("<option value=\"-\">すべて");
	for($i=0;$i<count($arr_apply_stat_nm);$i++) {

		echo("<option value=\"$arr_apply_stat_id[$i]\"");
		if($stat == $arr_apply_stat_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_apply_stat_nm[$i]\n");
	}
}
pg_close($con);
?>