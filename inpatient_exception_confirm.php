<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
$date_array = array();
for ($i = 1; $i <= 5; $i++) {
	$tmp_from_year_var = "from_year$i";
	$tmp_from_month_var = "from_month$i";
	$tmp_from_day_var = "from_day$i";
	$tmp_to_year_var = "to_year$i";
	$tmp_to_month_var = "to_month$i";
	$tmp_to_day_var = "to_day$i";

	if ($$tmp_from_year_var != "-" && $$tmp_from_month_var != "-" && $$tmp_from_day_var != "-") {
		if (!checkdate($$tmp_from_month_var, $$tmp_from_day_var, $$tmp_from_year_var)) {
			echo("<script type=\"text/javascript\">alert('除外開始日{$i}が不正です。');</script>");
			echo("<script type=\"text/javascript\">history.back()</script>");
			exit;
		}
		$tmp_from_date = "${$tmp_from_year_var}${$tmp_from_month_var}${$tmp_from_day_var}";
	} else if ($$tmp_from_year_var != "-" || $$tmp_from_month_var != "-" || $$tmp_from_day_var != "-") {
		echo("<script type=\"text/javascript\">alert('除外開始日{$i}が不正です。');</script>");
		echo("<script type=\"text/javascript\">history.back()</script>");
		exit;
	} else {
		$tmp_from_date = "";
	}
	if ($$tmp_to_year_var != "-" && $$tmp_to_month_var != "-" && $$tmp_to_day_var != "-") {
		if (!checkdate($$tmp_to_month_var, $$tmp_to_day_var, $$tmp_to_year_var)) {
			echo("<script type=\"text/javascript\">alert('除外終了日{$i}が不正です。');</script>");
			echo("<script type=\"text/javascript\">history.back()</script>");
			exit;
		}
		$tmp_to_date = "${$tmp_to_year_var}${$tmp_to_month_var}${$tmp_to_day_var}";
	} else if ($$tmp_to_year_var != "-" || $$tmp_to_month_var != "-" || $$tmp_to_day_var != "-") {
		echo("<script type=\"text/javascript\">alert('除外終了日{$i}が不正です。');</script>");
		echo("<script type=\"text/javascript\">history.back()</script>");
		exit;
	} else {
		$tmp_to_date = "";
	}
	if ($tmp_from_date != "" && $tmp_to_date != "") {
		if ($tmp_from_date > $tmp_to_date) {
			echo("<script type=\"text/javascript\">alert('除外終了日{$i}が不正です。');</script>");
			echo("<script type=\"text/javascript\">history.back()</script>");
			exit;
		}
		$date_array[] = array("key" => $i, "from" => $tmp_from_date, "to" => $tmp_to_date);
	} else if (($tmp_from_date != "" && $tmp_to_date == "") || ($tmp_from_date == "" && $tmp_to_date != "")) {
		echo("<script type=\"text/javascript\">alert('除外開始日と除外終了日は両方入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back()</script>");
		exit;
	}
}

// スタート日の昇順で並べ替え
usort($date_array, 'sort_date_array');

// 日付の重なりをチェック
for ($c = 1, $i = count($date_array); $c < $i; $c++) {
	for ($p = 0, $j = $c; $p < $j; $p++) {
		if ($date_array[$c]["from"] <= $date_array[$p]["to"]
		 && $date_array[$c]["to"]   >= $date_array[$p]["from"]) {
			echo("<script type=\"text/javascript\">alert('期間が重なっています。');</script>");
			echo("<script type=\"text/javascript\">history.back()</script>");
			exit;
		}
	}
}

// 登録値の編集
if ($except_flg != "t") {$except_flg = "f";}

if ($reason_id1 == "") {$reason_id1 = null;}
if ($reason_id2 == "") {$reason_id2 = null;}
if ($reason_id3 == "") {$reason_id3 = null;}
if ($reason_id4 == "") {$reason_id4 = null;}
if ($reason_id5 == "") {$reason_id5 = null;}
$reason_ids = array(
	"1" => $reason_id1,
	"2" => $reason_id2,
	"3" => $reason_id3,
	"4" => $reason_id4,
	"5" => $reason_id5
);

// データベースに接続
$con = connect2db($fname);

// 除外患者指定フラグを更新

//// 入院患者／退院予定患者
if ($path == "4" || $path == "5" || $path == "A" || $path == "B") {
	$sql = "update inptmst set";
	$cond = "where ptif_id = '$pt_id'";

//// 退院患者
} else {
	$sql = "update inpthist set";
	$cond = "where ptif_id = '$pt_id' and inpt_in_dt = '$in_dt' and inpt_in_tm = '$in_tm'";
}
$set = array("inpt_except_flg", "inpt_except_from_date1", "inpt_except_to_date1", "inpt_except_from_date2", "inpt_except_to_date2", "inpt_except_from_date3", "inpt_except_to_date3", "inpt_except_from_date4", "inpt_except_to_date4", "inpt_except_from_date5", "inpt_except_to_date5", "inpt_except_reason_id1", "inpt_except_reason_id2", "inpt_except_reason_id3", "inpt_except_reason_id4", "inpt_except_reason_id5");
$setvalue = array($except_flg, $date_array[0]["from"], $date_array[0]["to"], $date_array[1]["from"], $date_array[1]["to"], $date_array[2]["from"], $date_array[2]["to"], $date_array[3]["from"], $date_array[3]["to"], $date_array[4]["from"], $date_array[4]["to"], $reason_ids[$date_array[0]["key"]], $reason_ids[$date_array[1]["key"]], $reason_ids[$date_array[2]["key"]], $reason_ids[$date_array[3]["key"]], $reason_ids[$date_array[4]["key"]]);
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// チェックリスト画面を再表示

//// 入退院登録＞入院患者／退院予定患者
echo("<script type=\"text/javascript\">location.href = '");
if ($path == "4" || $path == "5") {
	echo("inpatient_exception.php?session=$session&pt_id=$pt_id&path=$path");

//// 入院患者検索＞入院患者／退院予定患者
} else if ($path == "A" || $path == "B") {
	$url_pt_nm = urlencode($pt_nm);
	echo("bed_menu_inpatient_exception.php?session=$session&pt_id=$pt_id&path=$path&pt_nm=$url_pt_nm&search_sect=$search_sect&search_doc=$search_doc");

//// 入退院登録＞退院患者
} else {
	echo "inpatient_exception.php?session=$session&pt_id=$pt_id&path=$path&in_dt=$in_dt&in_tm=$in_tm";
}
echo("';</script>");

function sort_date_array($date1, $date2) {
	if ($date1["from"] != $date2["from"]) {
		return strcmp($date1["from"], $date2["from"]);
	}
	return strcmp($date1["to"], $date2["to"]);
}
?>
