<?php
// 使用ライブラリ：pChart 1.27d
// http://pchart.sourceforge.net/
// このプログラムは UTF-8 で書かれています。

set_include_path(get_include_path() . PATH_SEPARATOR . "./PEAR");
include("pChart/pData.class");
include("pChart/pChart.class");
require("PEAR/Cache/Lite.php");
require_once("about_comedix.php");
require_once("cl_common_log_class.inc");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

//==============================
//初期処理
//==============================

//画面名
$fname = $_SERVER["PHP_SELF"];

//セッションのチェック
$session = qualify_session($_GET["session"],$fname);
if($session == "0"){
    js_login_exit();
}

// フォント
define("DRAW_FONT", "/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf");

//==============================
//パラメータ
//==============================
$type   = !empty($_GET["type"])  ? $_GET["type"]  : 1;
$cache  = !empty($_GET["cache"]) ? $_GET["cache"] : 0;
$hanrei = !empty($_COOKIE["_cl_graph_hanrei"]) ? $_COOKIE["_cl_graph_hanrei"] : 1;
$h_size = !empty($_COOKIE["_cl_graph_h_size"]) ? $_COOKIE["_cl_graph_h_size"] : 9;
$z_size = !empty($_COOKIE["_cl_graph_z_size"]) ? $_COOKIE["_cl_graph_z_size"] : 9;

//==============================
//専用セッションを使用する。
//==============================
session_name("cl_level_analysis_sid");
session_start();

//==============================
//集計結果情報の取得
//==============================

//集計結果データ
$stats  = $_SESSION['stats_result_info']['report_stats_data'];
$counts = $stats["stats_data_list"]["cells"];
$v_sums = $stats["stats_data_list"]["vertical_sums"];
$h_sums = $stats["stats_data_list"]["horizontal_sums"];

//==============================
// キャッシュ
//==============================
$cache_id = session_id()  . $type;
$cachelite = new Cache_Lite(array(
    'cacheDir' => '/tmp/',
    'lifeTime' => 3600,
    'automaticSerialization' => true,
    'automaticCleaningFactor' => 100,
));
// キャッシュのクリア
if (!$cache) {
    $cachelite->clean('cl_level_analysis_chart','ingroup');
}

//==============================
//データ
//==============================
$cache_data = $cachelite->get($cache_id, 'cl_level_analysis_chart');
$data  = $cache_data["data"];
$v_sum = $cache_data["v_sum"];
$v_max = $cache_data["v_max"];

// キャッシュがなければデータ生成
if ($cache_data === false) {
    $data = new pData;

    // 横軸ラベル
    $label = array();
    foreach ($stats["horizontal_options"] as $h) {
        $label[] = mb_convert_encoding(mb_convert_encoding($h['easy_name'],"sjis-win","eucJP-win"),"UTF-8","sjis-win");
    }

    //データ
    $v_index = -1;
    $v_sum = 0;
    $v_max = 0;
    $pie = array();
    $pielabel = array();
    foreach ($stats["vertical_options"] as $v) {
        $v_index++;
        // 縦軸ラベル
        $vlabel = mb_convert_encoding(mb_convert_encoding($v['easy_name'],"sjis-win","eucJP-win"),"UTF-8","sjis-win");

        // 棒グラフ、折れ線グラフ
        if ($type != 2) {
            $data->SetSerieName($vlabel, "Serie".$v_index);

            $h_index = -1;
            $point = array();
            foreach ($stats["horizontal_options"] as $h) {
                $h_index++;
                $point[] = $counts[$v_index][$h_index];

                if ($v_max < $counts[$v_index][$h_index]) {
                    $v_max = $counts[$v_index][$h_index];
                }
                if ($v_sum < $h_sums[$h_index]) {
                    $v_sum = $h_sums[$h_index];
                }
            }
            $data->AddPoint($point, "Serie".$v_index);
        }

        // 円グラフ
        else if ($v_sums[$v_index] > 0) {
            $pie[] = $v_sums[$v_index];
            $pielabel[] = $vlabel."({$v_sums[$v_index]})";
        }
    }
    if ($type != 2) {
        $data->AddPoint($label, "Label");
        $data->SetAbsciseLabelSerie("Label");
    }
    else {
        $data->AddPoint($pie, "Pie");
        $data->AddPoint($pielabel, "Label");
        $data->SetAbsciseLabelSerie("Label");
    }

    $data->AddAllSeries();
    $cachelite->save(array("data" => $data, "v_sum" => $v_sum, "v_max" => $v_max), $cache_id, 'cl_level_analysis_chart');
}

//==============================
//描画
//==============================
$graph = new pChart(1030,550);

// 凡例の色
$graph->setColorPalette( 0,  0,  0,255); // #0000ff
$graph->setColorPalette( 1,  0,255,  0); // #00ff00
$graph->setColorPalette( 2,255,  0,  0); // #ff0000
$graph->setColorPalette( 3,  0,255,255); // #00ffff
$graph->setColorPalette( 4,255,255,  0); // #ffff00
$graph->setColorPalette( 5,128,255,255); // #80ffff
$graph->setColorPalette( 6,255,128,255); // #ff80ff
$graph->setColorPalette( 7,255,255,128); // #ffff80
$graph->setColorPalette( 8,128,  0,  0); // #800000
$graph->setColorPalette( 9,  0,128,  0); // #008000
$graph->setColorPalette(10,  0,  0,128); // #000080
$graph->setColorPalette(11,255,128,  0); // #ff8000
$graph->setColorPalette(12,128,255,  0); // #80ff00
$graph->setColorPalette(13,128,  0,255); // #8000ff
$graph->setColorPalette(14,255,  0,128); // #ff0080
$graph->setColorPalette(15,  0,255,128); // #00ff80
$graph->setColorPalette(16,  0,128,255); // #0080ff
$graph->setColorPalette(17,255,128,128); // #ff8080
$graph->setColorPalette(18,128,255,128); // #80ff80
$graph->setColorPalette(19,128,128,255); // #8080ff
$graph->setColorPalette(20,  0,128,128); // #008080
$graph->setColorPalette(21,128,  0,128); // #800080
$graph->setColorPalette(22,128,128,  0); // #808000
$graph->setColorPalette(23,128,128,128); // #808080
$graph->setColorPalette(24, 64,  0,  0); // #400000
$graph->setColorPalette(25,  0, 64,  0); // #004000
$graph->setColorPalette(26,  0,  0, 64); // #000040
$graph->setColorPalette(27,192,  0,  0); // #c00000
$graph->setColorPalette(28,  0,192,  0); // #00c000
$graph->setColorPalette(29,  0,  0,192); // #0000c0
$graph->setColorPalette(30,  0,255, 64); // #00ff40
$graph->setColorPalette(31,255, 64,  0); // #ff4000
$graph->setColorPalette(32, 64,  0,255); // #4000ff

// 凡例を表示する
if ($hanrei == 1) {
    $garea_x = 850;
}
// 凡例を表示しない
else {
    $garea_x = 1000;
}

switch ($type) {
    // 積み上げ棒グラフ
    case 1:
        $graph->setGraphArea(50,10,$garea_x,450);
        $graph->drawFilledRectangle(0,0,1030,550,245,255,229,false,100);// #F5FFE5
        $graph->drawGraphArea(245,255,229,false);// #F5FFE5

        $graph->setFontProperties(DRAW_FONT,$z_size);
        $graph->setFixedScale(0,$v_sum,$v_sum);
        $graph->drawScale($data->GetData(),$data->GetDataDescription(),SCALE_ADDALLSTART0,0,0,0,true,0,0,true);

        $graph->drawGrid(1,true,220,220,220,50);

        $graph->drawStackedBarGraph($data->GetData(), $data->GetDataDescription());

        if ($hanrei == 1) {
            $graph->setFontProperties(DRAW_FONT,$h_size);
            $data->removeSerie("Label");
            $graph->drawLegend(860,10,$data->GetDataDescription(),245,255,229,-1,-1,-1,0,0,0,false);
        }
        break;

    // 折れ線グラフ
    case 3:
        $graph->setGraphArea(50,10,$garea_x,450);
        $graph->drawFilledRectangle(0,0,1030,550,245,255,229,false,100);// #F5FFE5
        $graph->drawGraphArea(245,255,229,false);// #F5FFE5

        $graph->setFontProperties(DRAW_FONT,$z_size);
        $graph->setFixedScale(0,$v_max,$v_max);
        $graph->drawScale($data->GetData(),$data->GetDataDescription(),SCALE_START0,0,0,0,true,0,0,false);

        $graph->drawGrid(1,true,220,220,220,50);

        $graph->drawLineGraph($data->GetData(), $data->GetDataDescription());

        if ($hanrei == 1) {
            $graph->setFontProperties(DRAW_FONT,$h_size);
            $data->removeSerie("Label");
            $graph->drawLegend(860,10,$data->GetDataDescription(),245,255,229,-1,-1,-1,0,0,0,false);
        }
        break;

    // 円グラフ
    case 2:
        $graph->setGraphArea(0,0,1030,550);
        $graph->drawFilledRectangle(0,0,1030,550,245,255,229,false,100);// #F5FFE5
        $graph->drawGraphArea(245,255,229,false);// #F5FFE5

        $graph->setFontProperties(DRAW_FONT,$h_size);
        $graph->drawFlatPieGraph($data->GetData(), $data->GetDataDescription(),515,275,200,PIE_PERCENTAGE_LABEL,0,1);
        break;
}

$graph->Stroke();
