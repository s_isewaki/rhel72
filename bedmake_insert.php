<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//------------------------------------------------------------------------------
// 共通処理
//------------------------------------------------------------------------------

// ページ名
$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力値チェック
//// 依頼内容が未選択
if ($sheet_chg_flg == "" && $mattress_st_flg == "" && $bed_st_flg == "") {
	echo("<script language='javascript'>alert('依頼内容が選択されていません');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

//------------------------------------------------------------------------------
// DB処理
//------------------------------------------------------------------------------

// DB登録値の編集
if ($sheet_chg_flg == "") {
	$sheet_chg_flg = "f";
}
if ($mattress_st_flg == "") {
	$mattress_st_flg = "f";
}
if ($bed_st_flg == "") {
	$bed_st_flg = "f";
}
$order_dt = date("Ymd");
$order_tm = date("Hi");

// トランザクションの開始
$con = connect2db($fname);
pg_query($con, "begin transaction");

// ベットメイクIDの採番
$sql = "select max(bdmk_id) from bedmake";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$tmp_bdmk_id = pg_fetch_result($sel, 0, 0);
if ($tmp_bdmk_id == "") {
	$bdmk_id = 1;
} else {
	$bdmk_id = $tmp_bdmk_id + 1;
}

// ベットメイク情報の作成
$sql = "insert into bedmake (bdmk_id, bldg_cd, ward_cd, room_no, bed_no, sheet_chg_flg, mattress_st_flg, bed_st_flg, order_dt, order_tm) values (";
$content = array($bdmk_id, $bldg_cd, $ward_cd, $room_no, $bed_no, $sheet_chg_flg, $mattress_st_flg, $bed_st_flg, $order_dt, $order_tm);
$in = insert_into_table($con, $sql, $content, $fname);
if ($in == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");
pg_close($con);

//------------------------------------------------------------------------------
// 画面遷移
//------------------------------------------------------------------------------
if ($wherefrom == "1") {
	echo("<script language='javascript'>location.href = 'bedmake_menu.php?session=$session&bldg_wd=$bldg_wd&ptrm=$room_no&bed=$bed_no';</script>");
} else {
	echo("<script language='javascript'>location.href = 'bedmake_list.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd&room_no=$room_no&bed_no=$bed_no';</script>");
}
?>
