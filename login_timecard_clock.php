<?
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Expires: Thu, 01 Jan 1970 00:00:00 GMT");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix</title>
<?
require("about_postgres.php");
//require("show_extension_from_login.ini");
//require("show_allot_from_login.ini");
//require("show_welfare_from_login.ini");
//require("show_qa_from_login.ini");
//require("show_stat_from_login.ini");
//require("menu_news_from_login.ini");
//require("show_reservation_from_login.ini");
//require("show_library_from_login.ini");
//require("show_ext_search_from_login.ini");
//require("show_link_from_login.ini");
//require("show_copyright.ini");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// 環境設定値を取得
$sql = "select * from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$interval = pg_fetch_result($sel, 0, "interval");
$login_page_reflesh = ($interval == "t") ? pg_fetch_result($sel, 0, "login_page_reflesh") * 1000 : 0;  // 更新間隔はミリ秒単位に変換
$login_color = pg_fetch_result($sel, 0, "login_color");

$onload = "";
if ($interval == "t" && $login_page_reflesh > 0) {
	$onload .= "setReload();";
}

if ($login_color == "1") {
	$block_colors["border"] = "#5279a5";
	$block_colors["header"] = "#bdd1e7";
	$block_colors["column"] = "#f6f9ff";
} else {
	$block_colors["border"] = "#0e9c55";
	$block_colors["header"] = "#ceefc2";
	$block_colors["column"] = "#f7fdf2";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
function rld() {
	location.reload();
}
function setReload() {
	setTimeout('rld()', <? echo($login_page_reflesh); ?>);
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
img.close {
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
}
.block {border-collapse:collapse; border:<? echo($block_colors["border"]); ?> solid 1px;}
.block .header td {background-color:<? echo($block_colors["header"]); ?>}
.block .column td {background-color:<? echo($block_colors["column"]); ?>}
.block td, .block td .block td {border:<? echo($block_colors["border"]); ?> solid 1px;}
.block td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5"<? if ($onload != "") {echo(" onload=\"$onload\"");} ?>>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
if (strpos($_SERVER["HTTP_USER_AGENT"], "Safari") !== FALSE)
{
	$fontsize = "160px";
}
else
{
	$fontsize = "200px";
}
?>
<td align="center" id="time" style="font-size:<?=$fontsize?>;"><? echo(date("H")); ?><span style="margin:auto 24px">:</span><? echo(date("i")); ?></td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
