<?
$forPDF = false ;
$klst ;
if ($_REQUEST["direct_template_pdf_print_requested"])
{
  $forPDF = true ;
  $klst = new Klst() ;
  ob_start() ;
}

ob_start();
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");
ob_end_clean();


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}

//====================================================================
//印刷設定画面表示
//====================================================================
if (@$_REQUEST['print_setting'] == 'y') {
    dispPrintSetting($session);
    exit;
}

//ログイン職員ＩＤ
$emp_id = get_emp_id($con, $session, $fname);

//=================================================
// 対象日付がない場合は本日を設定
//=================================================
if(@$apply_date_default == "on") list($date_y, $date_m, $date_d) = split("/", date("Y/m/d", strtotime("today")));
$target_yyyymmdd = $date_y. ((int)@$date_m <10 ? "0".(int)@$date_m : $date_m) . ((int)@$date_d <10 ? "0".(int)@$date_d : $date_d);

//=================================================
// 病棟などが指定されていなければデフォルトを設定する
//=================================================
if (@$apply_date_default && @$sel_bldg_cd=="" && @$sel_ward_cd==""){
	$sql =
		" select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
		" inner join empmst emp on (".
		"   emp.emp_class = swer.class_id".
		"   and emp.emp_attribute = swer.atrb_id".
		"   and emp.emp_dept = swer.dept_id".
		"   and emp.emp_id = '".pg_escape_string($emp_id)."'".
		" )";
	$emp_ward = @$c_sot_util->select_from_table($sql);
	$_REQUEST["sel_bldg_cd"] = @pg_fetch_result($emp_ward, 0, "bldg_cd");
	$_REQUEST["sel_ward_cd"] = @pg_fetch_result($emp_ward, 0, "ward_cd");
	$sel_bldg_cd= $_REQUEST["sel_bldg_cd"];
	$sel_ward_cd= $_REQUEST["sel_ward_cd"];
}
$sel_team_id = (int)@$_REQUEST["sel_team_id"];

$THIS_WORKSHEET_ID = "1"; // [sotmst][sot_id]の固定値

ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | 体重集計表</title>
<? write_yui_calendar_use_file_read_0_12_2();// 外部ファイルを読み込む ?>
<? write_yui_calendar_script2(1);// カレンダー作成、関数出力 ?>
<script type="text/javascript">
function popupPrintPreview(){
//	window.open(
//		"sot_worksheet_weight.php?session=<?=$session?>&date_y=<?=@$date_y?>&date_m=<?=@$date_m?>&date_d=<?=@$date_d?>&sel_bldg_cd=<?=@$sel_bldg_cd?>&sel_ward_cd=<?=@$sel_ward_cd?>&sel_ptrm_room_no=<?=@$sel_ptrm_room_no?>&sel_team_id=<?=@$sel_team_id?>&print_preview=y",
//		"worksheet_keikan_eiyou_print",
//		"directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1024,height=700"
//	);

  document.frm_pdf_print.submit() ;

}

// 印刷設定画面を開く
function popupPrintSetting(){
  var url = "sot_worksheet_weight.php?session=<? echo $session; ?>&date_y=<? echo @$date_y; ?>&date_m=<? echo @$date_m; ?>&date_d=<? echo @$date_d; ?>&sel_bldg_cd=<? echo @$sel_bldg_cd; ?>&sel_ward_cd=<? echo @$sel_ward_cd; ?>&sel_ptrm_room_no=<? echo @$sel_ptrm_room_no; ?>&sel_team_id=<? echo @$sel_team_id; ?>&print_setting=y";
  window.open(url, "", "directories=no,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no,width=460,height=150");
}

</script>
<style type="text/css" media="all">
	body { margin:0; color:#000; background-color:#fff; }
	.list {border-collapse:collapse; }
	.list td {border:#5279a5 solid 1px; padding:1px;}
	table.block_in {border-collapse:collapse;}
	table.block_in td {border:#5279a5 solid 0px;}
	img { border: 0; }
	.date_header { background-color:#ffa; }
	.c_red { color:#f05 }
	.apply_header { border: 1px solid #ccc; }
<? if (@$_REQUEST["print_preview"]) {?>
	.print_preview_hidden { display:none; }
<? } ?>
</style>
<? if (@$_REQUEST["print_preview"]) {?>
<style type="text/css" media="print">
	html { font-size:14px; }
	.print_color_black { color:#000; }
	.print_target_table th { font-size:14px }
	.print_target_table td { font-size:14px }
	.print_hidden { display:none; }
	.list td {border:#000 solid 1px;}
	.date_header { border: 1px solid #777; }
	.apply_header { border: 1px solid #777; }
</style>
<? } ?>
</head>
<body onload="<? if (@$_REQUEST["print_preview"]) {?>print();close();<?}else{ ?>initcal();<?}?>"<? if (@$_REQUEST["print_preview"]) {?>style="padding:1px"<? } ?>>

<div class="print_preview_hidden">

<? $param = "&date_y=".@$date_y."&date_m=".@$date_m."&date_d=".@$date_d."&sel_bldg_cd=".@$sel_bldg_cd."&sel_ward_cd=".@$sel_ward_cd."&sel_ptrm_room_no=".@$sel_ptrm_room_no."&sel_team_id=".$sel_team_id; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "体重集計表", $param); ?>

<? summary_common_show_main_tab_menu($con, $fname, "看護支援", 1); ?>


<script type="text/javascript">
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
	combobox[1] = document.getElementById("sel_bldg_cd");
	combobox[2] = document.getElementById("sel_ward_cd");
	combobox[3] = document.getElementById("sel_ptrm_room_no");
	if (node < 3) combobox[3].options.length = 1;
	if (node < 2) combobox[2].options.length = 1;

	<? // 初期化用 自分コンボを再選択?>
	var cmb = combobox[node];
	if (node > 0 && cmb.value != selval) {
		for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
	}

	var idx = 1;
	if (node == 1){
		for(var i in ward_selections){
			if (ward_selections[i].bldg_cd != selval) continue;
			combobox[2].options.length = combobox[2].options.length + 1;
			combobox[2].options[idx].text  = ward_selections[i].name;
			combobox[2].options[idx].value = ward_selections[i].code;
			idx++;
		}
	}
	if (node == 2){
		for(var i in room_selections){
			if (room_selections[i].bldg_cd != combobox[1].value) continue;
			if (room_selections[i].ward_cd != selval) continue;
			combobox[3].options.length = combobox[3].options.length + 1;
			combobox[3].options[idx].text  = room_selections[i].name;
			combobox[3].options[idx].value = room_selections[i].code;
			idx++;
		}
	}
}

function searchByDay(day_incriment){
	var yy = document.search.date_y;
	var mm = document.search.date_m;
	var dd = document.search.date_d;
	var y = (parseInt(yy.value,10) ? yy.value : yy.options[1].value);
	var m = (parseInt(mm.value,10) ? mm.value : 1)*1;
	var d = (parseInt(dd.value,10) ? dd.value : 1)*1;
	var dt = new Date(y, m-1, d);
	dt.setTime(dt.getTime() + day_incriment * 86400000);

	var idx_y = yy.options[1].value - dt.getFullYear() + 1;
	if (idx_y < 1) return;
	yy.selectedIndex = idx_y;
	mm.selectedIndex = dt.getMonth()+1;
	dd.selectedIndex = dt.getDate();
	document.search.submit();
}

function searchByToday(){
	var dt = new Date();
	document.search.date_y.selectedIndex = document.search.date_y.options[1].value - dt.getFullYear() + 1;
	document.search.date_m.selectedIndex = dt.getMonth()+1;
	document.search.date_d.selectedIndex = dt.getDate();
	document.search.submit();
}

</script>
<?
$EMPTY_OPTION = '<option value="">　　　　</option>';

// チーム一覧
$team_options = array();
$sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel)){
	$team_options[] =
	$selected = ($row["team_id"] == $sel_team_id ? " selected" : "");
	$team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}

// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) {
	$selected = ($row["bldg_cd"] == @$_REQUEST["sel_bldg_cd"] ? " selected" : "");
	$bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$mst = $c_sot_util->getPatientsRoomInfo();
$ary = array();
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";
?>

<form name="search" action="sot_worksheet_weight.php?session=<?=$session?>" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="excelout" value="">
<table class="list_table" style="margin-top:4px; margin-bottom:8px" cellspacing="1">
	<tr>
		<th>事業所(棟)</th>
		<td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
		<th>病棟</th>
		<td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
		<th>病室</th>
		<td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
		<script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
		<script type="text/javascript">sel_changed(2, "<?=@$_REQUEST["sel_ward_cd"]?>");</script>
		<script type="text/javascript">sel_changed(3, "<?=@$_REQUEST["sel_ptrm_room_no"]?>");</script>
		<th bgcolor="#fefcdf">チーム</th>
		<td>
			<select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
		</td>
	</tr>
	<tr>
		<th>日付</th>
		<td colspan="7">
			<div style="float:left">
				<div style="float:left">
					<select id="date_y1" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>&nbsp;/
					<select id="date_m1" name="date_m"><? show_select_months($date_m, true); ?></select>&nbsp;/
					<select id="date_d1" name="date_d"><? show_select_days($date_d, true); ?></select>
				</div>
				<div style="float:left">
					<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
					<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
				</div>
				<div style="float:left; padding-left:4px">
					<input type="button" onclick="searchByDay('-7');" value="≪" title="１週間前"/><!--
				--><input type="button" onclick="searchByDay('-1');" value="<" title="１日前"/><!--
				--><input type="button" onclick="searchByDay('1');" value=">" title="１日後"/><!--
				--><input type="button" onclick="searchByDay('7');" value="≫" title="１週間後"/>
				</div>
				<div style="float:left;">
					<input type="button" onclick="searchByToday();" value="本日"/>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="float:right">
				<input type="button" onclick="document.search.excelout.value=''; document.search.submit();" value="検索"/>
				<input type="button" onclick="document.search.excelout.value=''; popupPrintSetting();" value="印刷"/>
				<input type="button" onclick="document.search.excelout.value='1'; document.search.submit();" value="Excel出力"/>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>


</form>

  <form id="frm_pdf_print" name="frm_pdf_print" action="sot_worksheet_weight.php" method="post" target="_blank">
  <input type="hidden" name="session" value="<?=$session?>" />
  <input type="hidden" name="date_y" value="<?=@$date_y?>" />
  <input type="hidden" name="date_m" value="<?=@$date_m?>" />
  <input type="hidden" name="date_d" value="<?=@$date_d?>" />
  <input type="hidden" name="sel_bldg_cd" value="<?=@$sel_bldg_cd?>" />
  <input type="hidden" name="sel_ward_cd" value="<?=@$sel_ward_cd?>" />
  <input type="hidden" name="sel_ptrm_room_no" value="<?=@$sel_ptrm_room_no?>" />
  <input type="hidden" name="sel_team_id" value="<?=@$sel_team_id?>" />
  <input type="hidden" name="print_preview" value="y" />
  <input type="hidden" name="page_break" id="page_break" value="n" />
  <input type="hidden" name="direct_template_pdf_print_requested" value="1" />
  </form>

<? summary_common_show_worksheet_tab_menu("体重集計表", $param); ?>


</div><!-- end of [class="print_preview_hidden"] -->


<?
//====================================
// スケジュール中の患者と病室を取得
// ・患者ごとに、どの期間を取得すべきかを求める
//   ・指定日に入院しているなら、入院開始予定日と終了予定日の間のスケジュールをとる
//   ・指定日に入院していないなら、直近の退院日からのスケジュールをとる。
//     ・直近の退院日もない場合は、システム稼動日からが求める期間となる。
//====================================
/*
$sel = select_from_table($con, "select emp_id, emp_lt_nm, emp_ft_nm from empmst", "", $fname);
$empmst = array();
while ($row = pg_fetch_array($sel)){
	$empmst[$row["emp_id"]] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
}
*/
$target_week = "";
if (checkdate((int)@$date_m, (int)@$date_d, (int)@$date_y)) {
	$target_ymd_time = mktime(0, 0, 0, (int)$date_m, (int)$date_d, (int)$date_y);
	$weeks_en = array("sun","mon","tue","wed","thu","fri","sat");
	$target_week = $weeks_en[date("w", $target_ymd_time)];
}

$where = "";

if ((int)@$_REQUEST["sel_bldg_cd"] || $sel_team_id==0) { // チームがなければ必須指定
	$where .= " and inpt.bldg_cd = " .      (int)@$_REQUEST["sel_bldg_cd"];
}
if ((int)@$_REQUEST["sel_ward_cd"] || $sel_team_id==0) { // チームがなければ必須指定
	$where .= " and inpt.ward_cd = " .      (int)@$_REQUEST["sel_ward_cd"];
}

if (@$_REQUEST["sel_ptrm_room_no"]!="") $where .= " and inpt.ptrm_room_no = " . (int)$_REQUEST["sel_ptrm_room_no"];

$team_inner_join = "";
if ($sel_team_id) {
	$team_inner_join =
		" inner join sot_ward_team_relation t on (".
		"   t.bldg_cd = inpt.bldg_cd and t.ward_cd = inpt.ward_cd and t.ptrm_room_no = inpt.ptrm_room_no".
		"   and team_id = " . $sel_team_id.
		" )";
}


// 対象日の入院中患者情報
$sql_nyuin_kanja_list =
  " select".
  " inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
	",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, bldg.bldg_name, wd.ward_name, ptrm.ptrm_name".
	" from (".
			" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
			",max(inpt.inpt_in_dt) as inpt_in_dt".
			",min(inpt.inpt_out_dt) as inpt_out_dt".
			" from (".
					" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id, inpt.inpt_in_dt, '' as inpt_out_dt".
					" from inptmst inpt".
					$team_inner_join.
					" where inpt.inpt_in_dt <= '" . $target_yyyymmdd . "'"." " . $where.
					" union".
					" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id, inpt.inpt_in_dt, inpt.inpt_out_dt".
					" from inpthist inpt".
					$team_inner_join.
					" where inpt.inpt_in_dt <= '" . $target_yyyymmdd . "'".
					" and inpt.inpt_out_dt >= '" . $target_yyyymmdd . "'".
					" " . $where.
			" ) inpt".
			" group by inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
	" ) inpt".
	" left join ptifmst pt on (pt.ptif_id = inpt.ptif_id)".
	" left join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
	" left join wdmst wd on (wd.bldg_cd = inpt.bldg_cd and wd.ward_cd = inpt.ward_cd)".
	" left join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
	" group by".
	" inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_sect_id".
	",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, bldg.bldg_name, wd.ward_name, ptrm.ptrm_name";


// 対象日が停止期間に含まれる食事停止箋から最新のスケジュールのものを抽出
// 患者IDごとに１レコードが取得される
$sql_latest=
  " select" .
  "   sss2.schedule_seq     as schedule_seq_2" .
  "  ,sss2.ptif_id          as ptif_id_2" .
  ",d.ptrm_name".
  " from" .
  "   sot_summary_schedule sss2" .
  "   inner join" .
  "   (" .
  "   select" .
  "     sss3.ptif_id" .
  "    ,max(sss3.schedule_seq) as schedule_seq_max" .
  "   from" .
  "     sot_summary_schedule sss3" .
"     inner join summary smry on (smry.tmpl_id = sss3.tmpl_id and smry.ptif_id = sss3.ptif_id and smry.summary_id = sss3.summary_id and smry.summary_seq = sss3.summary_seq)" .
  "   where" .
  "         sss3.delete_flg = false" .
  "     and sss3.start_ymd <= " . $target_yyyymmdd .
  "     and sss3.end_ymd   >= " . $target_yyyymmdd .
  "     and sss3.temporary_update_flg = 0" .
  "     and sss3.tmpl_chapter_code = 'wtck'" .
  "     and sss3.tmpl_file_name = 'tmpl_WeightCheck.php'" .
  "   group by" .
  "     sss3.ptif_id" .
  "   ) sss4 on (sss4.ptif_id = sss2.ptif_id and sss4.schedule_seq_max = sss2.schedule_seq)" .
  "   inner join sum_xml smxl on (smxl.xml_file_name = sss2.xml_file_name)" .
  " inner join (".$sql_nyuin_kanja_list.") d on (d.ptif_id = sss2.ptif_id)".
  " where" .
  "       sss2.delete_flg = false" .
  "   and sss2.start_ymd <= " . $target_yyyymmdd .
  "   and sss2.end_ymd   >= " . $target_yyyymmdd .
  "   and sss2.temporary_update_flg = 0" .
  "   and sss2.tmpl_chapter_code = 'wtck'" .
  "   and sss2.tmpl_file_name = 'tmpl_WeightCheck.php'" .
  "   order by d.bldg_cd, d.ward_cd, d.ptrm_room_no, d.inpt_bed_no, ptif_id_2, schedule_seq_2";

if (!@$_REQUEST["sel_ward_cd"] && !$sel_team_id) {
	$latest_seq = array();
} else {
	$sel = $c_sot_util->select_from_table($sql_latest);
	$latest_seq = (array)pg_fetch_all($sel);
}

// スケジュールと受付実施情報を照らしあわせた情報
// およびひとつひとつの指示を照らし合わせた情報
$sql_schedule =
  " select".
  " sss.schedule_seq".
  ",sss.ptif_id".
  ",sssv.int1".
  ",sssv.int2".
  ",sssv.int5".
  ",sssv.int6".
  ",sssv.int7".
  ",sssv.int8".
  ",pt.ptsubif_weight".
  ",im.inpt_in_dt".
  ",d.ptif_id".
  ",d.bldg_cd".
  ",d.ward_cd".
  ",d.inpt_sect_id".
  ",d.ptif_lt_kaj_nm".
  ",d.ptif_ft_kaj_nm".
  ",d.bldg_name".
  ",d.ward_name".
  ",d.ptrm_name".
  ",d.ptrm_room_no".
  ",d.inpt_bed_no".
  " from sot_summary_schedule sss".
  " inner join (".$sql_nyuin_kanja_list.") d on (d.ptif_id = sss.ptif_id)".
  " inner join sot_summary_schedule_various sssv on (".
  "   sssv.schedule_seq = sss.schedule_seq".
  "   and sssv.string1 = '体重測定'".
  " )".
  "inner join inptmst im on (im.ptif_id = sss.ptif_id)".
  "left join ptsubif pt on (pt.ptif_id = sss.ptif_id)".
  " order by d.bldg_cd, d.ward_cd, d.ptrm_room_no, d.inpt_bed_no, d.ptif_id, sss.schedule_seq";

if (!@$_REQUEST["sel_ward_cd"] && !$sel_team_id) {
	$data = array();
} else {
	$sel = $c_sot_util->select_from_table($sql_schedule);
	$data = (array)pg_fetch_all($sel);
}

//ユーザー毎の最新データを取得
$schedule = array();
foreach($data as $key => $row){
	foreach($latest_seq  as $key2 => $value){
		if($row["schedule_seq"] == $value["schedule_seq_2"] && $row["ptif_id"] == $value["ptif_id_2"] ){
		    $schedule[] = $row;
		}
	}
}
//データを表示用に並べ替え
foreach($schedule as $key => $row)
{
	$ymd1 = $row["int1"] ? $c_sot_util->slash_yyyymmdd($row["int1"]) : '-';
	$val1 = $row["int5"] ? $row["int5"].".".intval($row["int6"]).' kg' : '-';

	$ymd2 = $row["int2"] ? $c_sot_util->slash_yyyymmdd($row["int2"]) : '-';
    $val2 = $row["int7"] ? $row["int7"].".".intval($row["int8"]).' kg' : '-';

	$ymd3 = $row["inpt_in_dt"] ? $c_sot_util->slash_yyyymmdd($row["inpt_in_dt"]) : '-';
    $val3 = $row["ptsubif_weight"] ? number_format($row["ptsubif_weight"], 1).' kg' : '-';
	$diff1 = ($row["int5"] && $row["int7"]) ? number_format(round(($row["int5"].".".intval($row["int6"])) - ($row["int7"].".".intval($row["int8"])), 1), 1).' kg' : '-';
	$diff2 = ($row["int5"] && $row["ptsubif_weight"]) ? number_format(round(($row["int5"].".".intval($row["int6"])) - $row["ptsubif_weight"], 1), 1).' kg' : '-';
	$diff3 = ($row["int7"] && $row["ptsubif_weight"]) ? number_format(round(($row["int7"].".".intval($row["int8"])) - $row["ptsubif_weight"], 1), 1).' kg' : '-';

	$schedule_detail[] = array($row["ptrm_name"], $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"], $ymd1, $val1, $diff1, $diff2, $ymd2, $val2, $diff3, $ymd3, $val3);
}
?>

<?
if (@$_REQUEST["excelout"]){
	ob_end_clean();
	xlsOut($schedule_detail);
	die;
}
ob_end_flush();
?>

<? if (!@$_REQUEST["sel_ward_cd"] && !$sel_team_id) { ?>
	<div style="padding:10px; color:#aaa; text-align:center">病棟またはチームを指定してください。</div>
<? } ?>

<? if ($schedule[0]){  ?>
	<table class="listp_table print_target_table" <? if (@$_REQUEST["print_preview"]) {?>style="width:600px"<? } ?> cellspacing="1">
	<tr>
		<th style="white-space:nowrap;">病室</th>
		<th style="white-space:nowrap;">患者氏名</th>
		<th style="white-space:nowrap;">最新測定日</th>
		<th style="white-space:nowrap;">測定値</th>
		<th style="white-space:nowrap;">前回差</th>
		<th style="white-space:nowrap;">初回差</th>
		<th style="white-space:nowrap;">前回測定日</th>
		<th style="white-space:nowrap;">測定値</th>
		<th style="white-space:nowrap;">初回差</th>
		<th style="white-space:nowrap;">初回入院測定日</th>
		<th style="white-space:nowrap;">測定値</th>
	</tr>

<? //体重表表示
	foreach($schedule_detail as $keys => $rows)
    { ?>
      <tr>
  <?    foreach($rows as $key => $row)
        { ?>
          <td style="vertical-align:top"><? echo h($rows[$key]); ?></td>
  <?    } ?>
     </tr>
<?  } ?>
    </table>
<? } ?>


</body>
<? pg_close($con); ?>
</html>







<? //-------------------------------------------------------------------------?>
<? // エクセル出力                                                            ?>
<? //-------------------------------------------------------------------------?>
<? function xlsOut($schedule) { ?>
<?   global $c_sot_util; ?>
<?   global$schedule_detail; ?>
<?   header("content-type: application/vnd.ms-excel"); ?>
<?   header("Pragma: public"); ?>
<?   header("Expires: 0"); ?>
<?   header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); ?>
<?   header("Content-Disposition: attachment; filename=fplus_aggregate_".Date("Ymd_Hi").".xls;"); ?>
<?   header('Content-Type: application/vnd.ms-excel'); ?>
<?   header("Content-Transfer-Encoding: binary"); ?>
<?   $fontfamily = mb_convert_encoding("ＭＳ Ｐゴシック", "SJIS"); ?>

<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=shift_jis">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel=File-List href="xls.files/filelist.xml">
<link rel=Edit-Time-Data href="xls.files/editdata.mso">
<link rel=OLE-Object-Data href="xls.files/oledata.mso">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Version>11.9999</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.98in .79in .98in .79in;
	mso-header-margin:.51in;
	mso-footer-margin:.51in;}
tr
	{mso-height-source:auto;
	mso-ruby-visibility:none;}
col
	{mso-width-source:auto;
	mso-ruby-visibility:none;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	border:none;
	mso-protection:locked visible;
	mso-style-name:標準;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	border:.5pt solid windowtext;
	background:silver;
	mso-pattern:auto none;}
.xl25
	{mso-style-parent:style0;
	border:.5pt solid windowtext;}
ruby
	{ruby-align:left;}
rt
	{color:windowtext;
	font-size:6.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	mso-char-type:katakana;
	display:none;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Sheet1</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:Print>
      <x:ValidPrinterInfo/>
      <x:PaperSizeIndex>9</x:PaperSizeIndex>
      <x:HorizontalResolution>300</x:HorizontalResolution>
      <x:VerticalResolution>300</x:VerticalResolution>
     </x:Print>
     <x:Selected/>
     <x:DoNotDisplayGridlines/>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet2</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet3</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>4725</x:WindowHeight>
  <x:WindowWidth>8475</x:WindowWidth>
  <x:WindowTopX>480</x:WindowTopX>
  <x:WindowTopY>30</x:WindowTopY>
  <x:AcceptLabelsInFormulas/>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>

<body link=blue vlink=purple>

<table x:str border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse;table-layout:fixed;'>
  <tr>
    <td class=xl24><ruby><?=mb_convert_encoding("病室","SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("患者氏名"  ,"SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("最新測定日","SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("測定値"    ,"SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("前回差"    ,"SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("初回差"    ,"SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("前回測定日","SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("測定値"    ,"SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("初回差"    ,"SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("初回入院測定日","SJIS")?></ruby></td>
    <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("測定値"    ,"SJIS")?></ruby></td>
  </tr>
<?
foreach ($schedule as $key => $rows) { ?>
  <tr>
  	<td class=xl25 style='vertical-align:top;border-top:none'><? echo mb_convert_encoding(h($rows[0]),"SJIS"); ?></td>
  	<td class=xl25 style='vertical-align:top;border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[1]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[2]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[3]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[4]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[5]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[6]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[7]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[8]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[9]),"SJIS"); ?></td>
  	<td class=xl25 style='border-top:none;border-left:none'><? echo mb_convert_encoding(h($rows[10]),"SJIS"); ?></td>
  </tr>
<? } ?>
</table>

</body>

</html>
<? } ?>

<? //-------------------------------------------------------------------- ?>
<? // PDF印刷用クラス定義                                                 ?>
<? //-------------------------------------------------------------------- ?>
<?
class Klst
{
  var $TITLE = "体重集計表" ;
  var $dtStr ;
}

?>
<? //-------------------------------------------------------------------- ?>
<? // PDF印刷スクリプトを出力する                                         ?>
<? //-------------------------------------------------------------------- ?>
<?
if ($forPDF)
{
  ob_end_clean() ;

  require_once('fpdf153/mbfpdf.php') ;

  //=================================================
  // PDF生成ライブラリの拡張クラス定義
  // ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //=================================================
  class CustomMBFPDF extends MBFPDF
  {
    // A4縦：297 x 210
    var $A4H = 297 ;
    var $A4W = 210 ;

    var $MOST_LEFT  =  10 ;
    var $MOST_RIGHT = 200 ;
    var $MOST_TOP   =   7 ;
    var $MOST_BTM   = 285 ;
    var $CELL_H     = 5   ;
    var $LINE_W_1   = 0.2 ;
    var $LINE_W_2   = 0.5 ;

    var $HD_CELLS = array(
      array(15, "病室"      ),
      array(21, "患者氏名"  ),
      array(18, "最新測定日"),
      array(16, "測定値"    ),
      array(16, "前回差"    ),
      array(16, "初回差"    ),
      array(18, "前回測定日"),
      array(16, "測定値"    ),
      array(16, "初回差"    ),
      array(23, "初回入院測定日"),
      array(16, "測定値"    )
      ) ;

    var $klTitle ;
    var $dtStr1 ;
    var $dtStr2 ;
    var $currY ;

    var $PRINT_JS = "(print\\('true'\\);)" ; // 印刷ダイアログボックスを表示するためにPDF文書内に埋め込むJavaScript
    var $printJs ;
    var $cin ; // 現在までに生成されたのCustomMBFPDFインスタンス数

    // コンストラクタ(PHP5可)
    function CustomMBFPDF($klTitle, $dtStr)
    {
      $this->MBFPDF("P", "mm", "A4") ; // PHPでは継承元クラスのコンストラクタは明示的に呼び出す必要がある

      $this->klTitle = $klTitle ;
      $this->dtStr1  = date("y/m/d h:i") ;
      $this->dtStr2  = $dtStr ;

      $this->eiyosX  = $this->MOST_LEFT + $this->HD_CELLS[0][0] + $this->HD_CELLS[1][0] ;
    }
    //ヘッダー
  	function Header(){
      $this->currY = $this->getY() ;
      $this->writeTitleHeader();
      $this->writeClmnsHeader();
  	}

    // タイトルヘッダーを出力する
    function writeTitleHeader()
    {
      $this->setY($this->currY) ;
      $this->SetFont(GOTHIC, "",  8.0) ;
      $this->Cell(0, 3.0, $this->dtStr1,  "", 1, "R") ;
      $this->SetFont(GOTHIC, "", 16.0) ;
      $this->Cell(0, 5.0, $this->klTitle, "", 1, "C") ;
      $this->SetFont(GOTHIC, "",  9.5) ;
      $this->Cell(0, 6.0, $this->dtStr2,  "", 1, "L") ;
      $this->currY = $this->GetY() - 0.5 ;
    }

    // 項目ヘッダーを出力する
    function writeClmnsHeader()
    {
      $this->setY($this->currY) ;
      $this->SetFont(GOTHIC, "", 8.5) ;
      $this->SetLineWidth($this->LINE_W_1) ;
      $this->SetFillColor(210);
      foreach($this->HD_CELLS as $key => $cell)
      {
        $lineEnd = ($key < count($this->HD_CELLS) - 1) ? 0 : 1 ;
        $this->Cell($cell[0], $this->CELL_H, $cell[1], 1, $lineEnd, "L", 1) ;
      }
      $this->currY = $this->GetY() ;
      $sX = $this->MOST_LEFT ;
    }
    //フッター
    function Footer()
    {
      $this->SetFont(GOTHIC, '' , 8) ;
      $this->SetY(-10);
      $this->Cell(0, 3, "- " . $this->PageNo() . " -", '', 0, 'C') ;
    }

  // 印刷ダイアログボックスを表示するためにオーバーライドします
    function Output($showPrintDialog)
    {
      $this->printJs = ($showPrintDialog) ? $this->PRINT_JS : "" ;
      parent::Output() ;
    }
    // 印刷ダイアログボックスを表示するためにオーバーライドします
    function _putresources()
    {
      parent::_putresources() ;
      if (strlen($this->printJs) < 1) return ;
      $this->_newobj() ;
      $this->cin = $this->n ;
      $this->_out("<<\n/Names [(EmbeddedJS) " . ($this->cin + 1) . " 0 R]\n>>\nendobj\n") ;
      $this->_newobj() ;
      $this->_out("<<\n/S /JavaScript\n/JS " . $this->printJs . "\n>>\nendobj\n") ;
    }
    function _putcatalog()
    {
      parent::_putcatalog() ;
      if (strlen($this->printJs) < 1) return ;
      $this->_out("/Names <</JavaScript " . $this->cin . " 0 R>>") ;
    }

    //｢EUC-JP → SJIS｣変換のためのオーバーライド
    //MultiCell()もWrite()もこのCell()で表示しているので、オーバーライドはCell()だけでOK
    function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '')
    {
      $enc = mb_internal_encoding();
      if($enc != "sjis-win"){
        $txt = mb_convert_encoding($txt, "sjis-win", $enc) ;
        parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link) ;
      }
    }
  }
  //=================================================
  // ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
  // PDF生成ライブラリの拡張クラス定義
  //=================================================

  $klTitle = $klst->TITLE ;
  $dtStr   = $date_y . "年" . (int)$date_m . "月" . (int)$date_d . "日" ;

  $pdf = new CustomMBFPDF($klTitle, $dtStr) ;
  $pdf->SetAutoPageBreak(true, 15) ;
  $pdf->AddMBFont(GOTHIC, 'SJIS') ;
  $pdf->SetMargins($pdf->MOST_LEFT, $pdf->MOST_TOP) ;
  $pdf->Open() ;

  $pdf->AddPage() ;

  $ptif_num = count($schedule_detail);    // 患者数
  $ptif_count = 0;    // 患者ループループカウンタ
  foreach ($schedule_detail as $idxp => $ptif) // 患者ループ 開始
  {
     $pdf->Cell(15, 5, $ptif[0] , 1, 0, "L") ;
  	 $pdf->Cell(21, 5, $ptif[1] , 1, 0, "L") ;
  	 $pdf->Cell(18, 5, $ptif[2] , 1, 0, "L") ;
  	 $pdf->Cell(16, 5, $ptif[3] , 1, 0, "L") ;
  	 $pdf->Cell(16, 5, $ptif[4] , 1, 0, "L") ;
  	 $pdf->Cell(16, 5, $ptif[5] , 1, 0, "L") ;
  	 $pdf->Cell(18, 5, $ptif[6] , 1, 0, "L") ;
  	 $pdf->Cell(16, 5, $ptif[7] , 1, 0, "L") ;
  	 $pdf->Cell(16, 5, $ptif[8] , 1, 0, "L") ;
  	 $pdf->Cell(23, 5, $ptif[9] , 1, 0, "L") ;
     $pdf->Cell(16, 5, $ptif[10], 1, 1, "L") ;
     $ptif_count++;
     if ($_REQUEST['page_break'] == 'y' && $ptif_count < $ptif_num) {
         // 患者毎に改ページ
         $pdf->AddPage();
     }
  }

  $pdf->Output(true) ;
  die ;
}
else
{
  ob_end_flush() ;
}

/**
 * 印刷設定表示
 * 
 * @param string $session セッション
 * 
 * @return void
 */
function dispPrintSetting($session)
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 体重集計表印刷</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css" />
<script type="text/javascript">
function startPrint(){
  if (!confirm("印刷PDF作成処理を開始します。")) {
    return;
  }
  var opener_page_break = window.opener.document.getElementById("page_break");
  if (document.getElementById("page_break").checked) {
    opener_page_break.value = "y";
  } else {
    opener_page_break.value = "n";
  }
  window.opener.popupPrintPreview();
  window.close();
}
</script>
</head>
<body>
<center>
<div style="width:450px; margin-top:4px;">

<!-- タイトルヘッダ -->
<script type="text/javascript" src="js/swapimg.js" charset="euc-jp"></script>
<div>
  <table class="dialog_titletable">
    <tr>
      <th>体重集計表印刷</th>
      <td><a href="javascript:window.close()"><img src="img/icon/close.gif" alt="閉じる"
        onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');" /></a></td>
    </tr>
  </table>
</div>

<!-- 印刷条件エリア -->
<form name="setting" method="get">
<input type="hidden" name="session" value="<? echo $session; ?>">
<div class="dialog_searchfield" style="height:105px;">
  <table style="width:100%">
    <tr>
      <td style="white-space:nowrap">
        <div style="margin-top:25px; margin-left:25px;"><label><input type="checkbox" name="page_break" id="page_break" />患者ごとに改ページする</label></div>
      </td>
    </tr>
  </table>
  <div style="margin-top:25px; margin-right:10px; text-align:right;"><input type="button" onclick="startPrint();" value="印刷開始"/></div>
</div>
</form>
</div>
</center>
</body>
</html>
<?
}
?>
