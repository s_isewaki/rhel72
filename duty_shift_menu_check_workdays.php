
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 勤務チェック一覧</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

require_once("yui_calendar_util.ini");

require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];
///-----------------------------------------------------------------------------
//各変数値の初期値設定
///-----------------------------------------------------------------------------
if ($date_y1 == "") { $date_y1 = $duty_yyyy; };
if ($date_m1 == "") { $date_m1 = $duty_mm - 1; };
if ((int)$date_m1 < 1) {
	$date_y1--;
	$date_m1 = 12;
}

$msg_list = array();
$days_count = $_REQUEST["workdays_count"];
for ($i=0; $i<$days_count; $i++){
	 $msg_list[$i]["emp_name"] = $_REQUEST["workdays_emp_name" . $i];
	 $msg_list[$i]["msg"] = $_REQUEST["workdays_msg" . $i];
	 $msg_list[$i]["date"] = $_REQUEST["workdays_date" . $i];
}

$register_flg = $_REQUEST["register_flg"]; //登録フラグ
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	// 「ＯＫ」開始年月を設定する
	function OkBtn() {
		window.opener.mainform.draft_flg.value = "";
		window.opener.mainform.action = "duty_shift_menu_update_exe.php";
		window.opener.mainform.target = "";
		window.opener.mainform.submit();
		window.close();
	}
	// 「キャンセル」
	function CancelBtn() {
		window.close();
	}

	window.onload = function(){
		if (document.mainform.days_count.value == 0){
			OkBtn();
		}
	}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
table.employlist { margin: auto; border-collapse:separate; border-spacing:1px; background-color:#9bc8ec; empty-cells:show; }
table.employlist tr { background-color:#fff }
table.employlist th { padding:5px; font-weight:normal; text-align:left; background:url(css/img/bg-b-gura.gif) top repeat-x; }
table.employlist td { padding:5px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>勤務チェック一覧</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
	<input type="hidden" name="session" value="<?=$session?>">
	<input type="hidden" name="days_count" value="<?=$workdays_count?>">
	</form>
	<table width="100%" border="2" class="employlist">
		<tr>
			<th width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></th>
			<th width="35%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></th>
			<th width="45%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></th>
		</tr>
		<?php
			foreach ($msg_list as $item){
				$rows = count($item["date"]);
				foreach ($item["date"] as $key=> $date){
					$tmp_date = $item["date"][$key];
					$tmp_msg = $item["msg"][$key];
					echo("<tr height=\"22\">\n");
					if ($key == 0){
						echo("<td width=\"20%\" rowspan=\"$rows\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$item['emp_name']}</font></td>\n");
					}
					echo("<td width=\"35%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_date</font></td>\n");
					echo("<td width=\"45%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_msg</font></td>\n");
					echo("</tr>\n");
				}
			}		
		?>
		</td>
		</tr>
		</table>
	
		<?php 
			if($register_flg == "1"){ 
				if ($workdays_count > 0) {
		?> 
				<div>
					<p align="right" ><font size="3" color="#ff0000"><b>登録処理を続行しますか？ </font></b></p>
				</div>
				<?php } ?>
		<?php }else{ ?>
			<br>
		<?php } ?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「ＯＫ」「キャンセル」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="100%" align="right">
		<?php if($register_flg == "1"){ ?> 
			<input type="button" value="はい " onclick="OkBtn();">
			<input type="button" value="いいえ" onclick="CancelBtn();">
		<?php }else{ ?>
			<input type="button" value=" OK " onclick="CancelBtn();">
		<?php } ?>
		</td>
		</tr>
	</table>
</center>
</body>
<?php pg_close($con); ?>
</html>

