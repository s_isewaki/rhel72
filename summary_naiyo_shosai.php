<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<title>CoMedix メドレポート｜サマリー内容</title>
<? require("./conf/sql.inf"); ?>
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<?

//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
echo("<script type='text/javascript' src='./js/showpage.js'></script>");
echo("<script language='javascript'>showLoginPage(window);</script>");
exit;
}

// 権限チェック
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//DBコネクション
$con = connect2db($fname);

require("get_values.ini");

//診療録を取得
$sel = select_from_table($con,"select * from summary where (ptif_id = '$pt_id') and (summary_id = '$sum_id')","",$fname);		//診療録を取得
if($sel==0){
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
?>

<script type="text/javascript">
	function resizeIframe(){
		var iframe_name = "<?=@$iframe?>";
		if (!window.parent) return;
		if (iframe_name){
			var iframe = window.parent.document.getElementById(iframe_name);
			if (!iframe) return;
			iframe.style.height = "300px";
			var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
			h = Math.max(h, document.body.scrollHeight) + (10*1);
			iframe.style.height = h + "px";
		}
		// ひとつ上のリサイズ関数を呼ぶ。(summary_rireki_naiyo.php)
		if (window.parent.resizeIframeHeight){
			window.parent.resizeIframeHeight();
		}
	}
</script>
</head>
<body bgcolor="#ffffff" onload="resizeIframe();">
<?
	if(@$mode == 'template'){
		$enc_str = urlencode($summary_div);
		echo("<script language=\"javascript\">location.href=\"./summary_tmpl_read.php?session=$session&pt_id=$pt_id&emp_id=$emp_id&summary_id=$sum_id&xml_file=$xml_file&tmpl_id=$tmpl_id&cre_date=$year$month$day&title=".@$title."&sum_problem=".@$problem."&sansho_flg=true&iframe=$iframe&enc_diag=$enc_str&auto_parent_resize=1&is_template_readonly=yes\";</script>\n");
	}else{
		require_once("summary_tmpl_util.ini");
//		$summary_naiyo1 = get_summary_naiyo_html($con,$fname,$sum_id,$pt_id);
    // @@@ 表示用HTMLデータはsummaryテーブルではなくsum_xmlテーブルから取得するように変更 (2011/02/24) @@@
    $summary_naiyo1 = get_summary_naiyo_html_from_sum_xml2($con,$fname,$sum_id,$pt_id,$summary_seq2);
//		echo(str_replace(" ", "&nbsp;", $summary_naiyo1));
		echo $summary_naiyo1;
	}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="sum_id" value="<? echo($sum_id); ?>">
</body>
<? pg_close($con); ?>
</html>
