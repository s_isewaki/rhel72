<?
//<!-- ************************************************************************ -->
//<!-- 勤務シフト作成　様式９画面 看護職員表 共通部品ＣＬＡＳＳ -->
//<!-- ************************************************************************ -->

require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_report_common_class.php");

class duty_shift_report1_common_class
{

    var $file_name; // 呼び出し元ファイル名
    var $_db_con;   // DBコネクション
    var $obj_rpt;   // 勤務シフト作成 様式9 共通クラス

    /*************************************************************************/
    // コンストラクタ
    // @param object $con DBコネクション
    // @param string $fname 画面名
    /*************************************************************************/
    function duty_shift_report1_common_class($con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
        // クラス new
        $this->obj = new duty_shift_common_class($con, $fname);
        $this->obj_rpt = new duty_shift_report_common_class($con, $fname);
    }

    /**
     * EXCEL OR関数を返す
     * @param type $cell
     * @param type $array
     * @return type
     */
    function excel_or($cell, $array) {
        $or = array();
        foreach ($array as $item) {
            $or[] = sprintf('%s="%s"', $cell, $item);
        }
        return 'OR(' . implode(',', $or) . ')';
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（画面）
//-------------------------------------------------------------------------------------------------------------------------
    /*************************************************************************/
    //
    // 看護職員表（見出し）
    //
    /*************************************************************************/
    function showNurseTitle_1(
                            $btn_show_flg,              //ボタン表示フラグ（１：表示）
                            $recomputation_flg,         //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                            $date_y1,                   //カレンダー用年
                            $date_m1,                   //カレンダー用月
                            $date_d1,                   //カレンダー用日
                            $day_cnt,                   //日数
                            $group_id,
                            $create_yyyy,           //作成年
                            $create_mm,             //作成月
                            $create_dd,             //作成日
                            $plan_result_flag,      //予定実績フラグ 20120209
                            $duty_yyyy,
                            $duty_mm
    ) {
        //-------------------------------------------------------------------------------
        // 初期値設定
        //-------------------------------------------------------------------------------
        $data = "";
        //全病棟の場合、新様式の場合、列を追加
        $title_col_cnt = 12;
        $wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
        if ($group_id == "0") {
            $title_col_cnt++;
        }
        if ($wk_duty_yyyymm >= "201204") {
            $title_col_cnt++;
        }
        $wk_colspan = $day_cnt + $title_col_cnt;

        $wk_recomputation = "";
        if ($recomputation_flg == "2") {
            $wk_recomputation = "４週計算　開始日" . $date_y1 . "年" . $date_m1 . "月" . $date_d1 . "日";
        }
        //-------------------------------------------------------------------------------
        // 見出し
        //-------------------------------------------------------------------------------
        $data .= "<tr>\n";
        $data .= "<td colspan=\"" . $wk_colspan . "\" style=\"border-style : solid solid solid solid; border-width : 1px 1px 1px 1px; border-color : #5279a5 #ffffff #5279a5 #ffffff;\">\n";
        $data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";

        $data .= "<tr>\n";
        if ($group_id != "0") {
            $group_name = $this->obj->get_group_name($group_id);
        } else {
            $group_name = "";
        }
        // タイトルに予定実績の文言追加 20120209
        switch ($plan_result_flag) {
            case "1":
                $wk_plan_result_str = "・実績";
                break;
            case "2":
                $wk_plan_result_str = "・予定";
                break;
            case "3":
                $wk_plan_result_str = "・予定と実績";
                break;
        }
        $data .= "<td nowrap align=\"left\" colspan=\"". $wk_colspan . "\"><b><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">《看護職員表{$wk_plan_result_str}》" . $wk_recomputation . "　　". $group_name . "　　" . $date_y1 . "年" . $date_m1 . "月" ."</font></b>";
        if ($btn_show_flg == "1") {
            //「ＥＸＣＥＬ」ボタン
            $data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp;&nbsp;";
            $data .= "<input type=\"button\" value=\"ＥＸＣＥＬ\" onclick=\"makeExcel();\">";
            $data .= "</font>";
        }
        $data .= "</td>\n";
        $data .= "</tr>\n";

        $data .= "</table>\n";
        $data .= "</td>\n";
        $data .= "<td nowrap align=\"right\" colspan=\"3\" style=\"border-style : solid solid solid solid; border-width : 1px 1px 1px 1px; border-color : #5279a5 #ffffff #5279a5 #ffffff;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成年月日\n";
        $data .= $create_yyyy . "年";
        $data .= $create_mm . "月";
        $data .= $create_dd . "日";
        $data .= "</font></td>\n";
        $data .= "</tr>\n";

        return $data;
    }
    /*************************************************************************/
    //
    // 看護職員表（表　タイトル）
    //
    /*************************************************************************/
    function showNurseData_1(
        $excel_flg,                 //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
        $week_array,                //曜日情報
        $calendar_array,            //カレンダー情報
        $day_cnt,                   //日数
        $group_id,
        $duty_yyyy,
        $duty_mm
    ) {
        $data = "";
        if ($excel_flg == "1") {
            $wk_font =  "<font size=\"1\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
        } else {
            $wk_font =  "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
        }
        //-------------------------------------------------------------------------------
        // 看護職員表（タイトル）
        //-------------------------------------------------------------------------------
        $data .= "<tr>\n";
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\">" . $wk_font . "種別</font></td>\n";
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\">" . $wk_font . "番号</font></td>\n";
        if ($group_id == "0") {
            $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\">" . $wk_font . "病棟</font></td>\n";
        }
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\">" . $wk_font . "氏名</font></td>\n";
        $wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
        $wk_colspan = ($wk_duty_yyyymm >= "201204") ? 4 : 3;
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\" colspan=\"{$wk_colspan}\">" . $wk_font . "雇用・勤務形態</font></td>\n";
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\" colspan=\"3\">" . $wk_font . "夜勤の有無</font></td>\n";
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\" width=\"50px\">" . $wk_font . "夜勤<br>従事者数<br>への計上</font></td>\n";
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\">&nbsp;</td>\n";
        $data .= "<td align=\"left\"   bgcolor=\"#fefcdf\" rowspan=\"1\" colspan=\"$day_cnt\">" . $wk_font . "日付別の勤務時間数</font></td>\n";
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\" width=\"30px\">" . $wk_font . "月勤務時間数<br>（延べ時間数）</font></td>\n";
        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"3\" width=\"40px\">" . $wk_font . "（再掲）夜勤専従者<br>及び月16時間以下<br>の者の夜勤時間数";
        //平成２４年度改訂対応 20120406
        $wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
        if ($wk_duty_yyyymm >= "201204") {
            $data .= "<br>（短時間正職員の場合は<br>12時間より短いもの）";
        }
        $data .= "</font></td>\n";

        $data .= "<td align=\"center\" bgcolor=\"#fefcdf\" rowspan=\"2\" colspan=\"2\" width=\"40px\">" . $wk_font . "職種別<br>月勤務時間数</font></td>\n";
        $data .= "</tr>\n";

        $data .= "<tr>\n";
        for($i=1; $i<=$day_cnt; $i++) {
            $wk_dd = substr($calendar_array[($i - 1)]["date"],6,2);
            $data .= "<td bgcolor=\"#fefcdf\">" . $wk_font . $wk_dd . "日</font></td>\n";
        }
        $data .= "</tr>\n";

        $data .= "<tr>\n";
        for($i=1;$i<=$day_cnt;$i++) {
            $data .= "<td bgcolor=\"#fefcdf\">" . $wk_font . $week_array[$i]["name"] . "曜</font></td>\n";
        }
        $data .= "<td bgcolor=\"#fefcdf\" width=\"20px\">" . $wk_font . "看護師</font></td>\n";
        $data .= "<td bgcolor=\"#fefcdf\" width=\"20px\">" . $wk_font . "准看護師</font></td>\n";
        $data .= "</tr>\n";

        $wk_array = array();
        $wk_array["data"] = $data;
        return $wk_array;
    }
    /*************************************************************************/
    //
    // 看護職員表（表　データ）
    //
    /*************************************************************************/
    function showNurseData_2(
        $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
        $data_array,                //看護職員情報
        $day_cnt,                   //日数
        $standard_array,            //施設基準
        $group_id,
        $duty_yyyy,
        $duty_mm
    ) {
        // 看護師
        $nurse1_name = $this->obj_rpt->get_nurse_job_name(1);
        // 准看護師
        $nurse2 = $this->obj_rpt->get_nurse_job_id(2);
        $nurse2_name = $this->obj_rpt->get_nurse_job_name(2);

        if ($excel_flg == "1") {
            $wk_font =  "<font size=\"1\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
        } else {
            $wk_font =  "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
        }
        //職員一覧開始行
         //平成２４年度改訂対応 20120406
        $wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
        if ($wk_duty_yyyymm >= "201204") {
            $even_base_row = 8; //偶数行
            $odd_base_row = 9; //奇数行
        }
        else {
            $even_base_row = 6; //偶数行
            $odd_base_row = 7; //奇数行
        }
        //月所定労働時間
        $wk_labor_month = $day_cnt * $standard_array[0]["labor_cnt"] / 7;
        //-------------------------------------------------------------------------------
        // 看護職員表（データ）
        //-------------------------------------------------------------------------------
        $wk_array = array();
        for($i=0; $i<count($data_array); $i++) {
            $data = "";
            $no = $i + 1;
            //-------------------------------------------------------------------------------
            // 看護職員表（データ 日勤時間数）
            //-------------------------------------------------------------------------------
            $data .= "<tr>\n";
            $data .= "<td align=\"left\"  rowspan=\"2\">" . $wk_font . $data_array[$i]["job_name"] . "</font></td>\n";
            $data .= "<td align=\"right\" rowspan=\"2\">" . $wk_font . $no . "</font></td>\n";
            if ($group_id == "0") {
                $data .= "<td align=\"right\" rowspan=\"2\">" . $wk_font . $data_array[$i]["main_group_name"] ."</font></td>\n";
            }
            $data .= "<td align=\"left\"  rowspan=\"2\">" . $wk_font . $data_array[$i]["staff_name"] . "</font></td>\n";
            $data .= "<td align=\"left\">" . $wk_font . "常勤</font></td>\n";
            if ($wk_duty_yyyymm >= "201204") {
                $data .= "<td align=\"left\">" . $wk_font . "短時間</font></td>\n"; //追加 20120411
            }
            $data .= "<td align=\"left\">" . $wk_font . "非常勤</font></td>\n";
            $data .= "<td align=\"left\">" . $wk_font . "他部署兼務</font></td>\n";
            $data .= "<td align=\"left\" bgcolor=\"#DFFFDC\">" . $wk_font . "有</font></td>\n";
            $data .= "<td align=\"left\" bgcolor=\"#DFFFDC\">" . $wk_font . "無</font></td>\n";
            $data .= "<td align=\"left\">" . $wk_font . "夜専</font></td>\n";
            $data .= "<td align=\"left\" bgcolor=\"#DFFFDC\">" . $wk_font . "夜勤従事者</font></td>\n";
            $data .= "<td align=\"left\">" . $wk_font . "日勤時間数</font></td>\n";

            //勤務時間（日勤）
            $wk_times_1 = 0.0;
            $wk_times_2 = 0.0;
            for ($k=1; $k<=$day_cnt; $k++) {
                $wk = $data_array[$i]["duty_time_$k"];
                if ($data_array[$i]["job_name"] == "准看護師" or in_array($data_array[$i]["job_id"], $nurse2)) {
                    $wk_times_2 += $wk;
                } else {
                    $wk_times_1 += $wk;
                }
                if ($wk <= 0) {
                    $wk = "　";
                } else {
                    $wk = sprintf("%01.2f",$wk);
                    $wk = $this->obj->rtrim_zero($wk);
                }
                //勤務時間（日勤）
                $data .= "<td align=\"right\">" . $wk_font . $wk . "<font></td> \n";
            }
            //勤務時間（日勤）
            $wk_times_day = $wk_times_1 + $wk_times_2;

            //月勤務時間数（延べ時間数）
            //（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数
            //職種別月勤務時間数（看護師　※保健師、助産師などを含む）
            //職種別月勤務時間数（准看護師）
//          $wk1 = sprintf("%01.2f",($wk_times_1 + $wk_times_2));
//          $wk1 = $this->obj->rtrim_zero($wk1);
            $wk2 = "　";
//          $wk3 = sprintf("%01.2f",$wk_times_1);
//          $wk3 = $this->obj->rtrim_zero($wk3);
//          $wk4 = sprintf("%01.2f",$wk_times_2);
//          $wk4 = $this->obj->rtrim_zero($wk4);

            $wk_line = $even_base_row + ($i * 2);
            $wk_line1 = $odd_base_row + ($i * 2);
            //列の位置からカラムを求める
            //"A" + 0 -> "A", "A" + 1 => "B"
            $wk_pos = 3; //"A" + 3 > "D"
            if ($group_id == "0") {
                $wk_pos++;
            }
                //$wk_pos = 4; //"A" + 4 > "E"
                $wk_jokin_col = chr(ord("A") + $wk_pos); //常勤列
                $wk_pos++;
                $wk_tanjikan_col = chr(ord("A") + $wk_pos); //短時間列
                $wk_hijokin_base_col = ($wk_duty_yyyymm >= "201204") ? $wk_tanjikan_col : $wk_jokin_col;
                if ($wk_duty_yyyymm >= "201204") {
                    $wk_pos++;
                }
                $wk_hijokin_col = chr(ord("A") + $wk_pos); //非常勤列
                $wk_pos++;
                $wk_tabusho_col = chr(ord("A") + $wk_pos); //他部署兼務列
                $wk_pos++;
                $wk_night_col = chr(ord("A") + $wk_pos); //夜勤有り列
                $wk_pos++;
                $wk_no_night_col = chr(ord("A") + $wk_pos); //夜勤無し列
                $wk_pos++;
                $wk_night_sen_col = chr(ord("A") + $wk_pos); //夜勤専従列
                $wk_pos += 3;
                $wk_start_col = chr(ord("A") + $wk_pos); //"N";日付欄開始
                $title_col_cnt = $wk_pos; //氏名等の列数
                //$wk_end_col = "A".chr(ord("C") + $day_cnt - 16);
            $wk_end_col = "A".chr(ord("A") + $title_col_cnt + $day_cnt - 26 - 1); //列数-26("Z")日付欄終了("A"+0=>"A"のため１調整)

            $wk_col1 = "A".chr(ord("A") + $title_col_cnt + $day_cnt - 26); //月勤務時間数の列
            $wk1 = "=SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line})";

            $wk3 = "=IF(". $this->excel_or("A{$wk_line}", $nurse1_name) .",SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line}),0)";
            $wk4 = "=IF(". $this->excel_or("A{$wk_line}", $nurse2_name) .",SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line}),0)";

            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk1 . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk2 . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk3 . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk4 . "</font></td>\n";
            $data .= "</tr>\n";
            //-------------------------------------------------------------------------------
            // 看護職員表（データ 夜勤時間数）
            //-------------------------------------------------------------------------------
            $data .= "<tr>\n";

            //勤務時間（夜勤の月計）
            $wk_times_1 = 0.0;
            $wk_times_2 = 0.0;
            for($k=1; $k<=$day_cnt; $k++) {
                $wk = $data_array[$i]["night_time_$k"];
                if ($data_array[$i]["job_name"] == "准看護師" or in_array($data_array[$i]["job_id"], $nurse2)) {
                    $wk_times_2 += $wk;
                } else {
                    $wk_times_1 += $wk;
                }
            }
            //常勤／非常勤／他部署兼務
            //夜勤：有り／夜勤：無し／夜専
            $wk1 = "　";
            $wk2 = "　";
            $wk3 = "　";
            $wk4 = "　";
            $wk5 = "　";
            $wk6 = "　";
            $wk7 = "　"; //短時間追加 20120411
            if ($data_array[$i]["duty_form"] == "1") { $wk1 = "1"; }
            if ($data_array[$i]["duty_form"] == "3") { $wk7 = "1"; }
            if ($data_array[$i]["duty_form"] == "2") { $wk2 = "1"; }
            if ($data_array[$i]["other_post_flg"] == "1") { $wk3 = "1"; }
            //夜専の表示を変更 20120220
            //16時間超を夜勤有りとする 短時間の場合は12時間以上とする 20120411
            if ($data_array[$i]["duty_form"] == "3") {
                //12以上
                if (($wk_times_1 + $wk_times_2) >= 12) {
                    $wk4 = "1";
                } else {
                    $wk5 = "1";
                }
            }
            else {
                //16より大きい
                if (($wk_times_1 + $wk_times_2) > 16) {
                    $wk4 = "1";
                } else {
                    $wk5 = "1";
                }
            }
            //夜専
            if ($data_array[$i]["night_duty_flg"] == "2") {
                $wk6 = "1";
            }

            $data .= "<td align=\"right\">" . $wk_font . $wk1 . "</font></td>\n";
            //新様式
            if ($wk_duty_yyyymm >= "201204") {
                $data .= "<td align=\"right\">" . $wk_font . $wk7 . "</font></td>\n"; //短時間
                //計算式設定
                $wk4_str = "=IF(AND({$wk_jokin_col}{$wk_line1}=0,{$wk_tanjikan_col}{$wk_line1}=0,{$wk_hijokin_col}{$wk_line1}=0,{$wk_tabusho_col}{$wk_line1}=0), 0, IF(OR(AND({$wk_tanjikan_col}{$wk_line1}=1,{$wk_col1}{$wk_line1}>=12),AND({$wk_tanjikan_col}{$wk_line1}<>1,{$wk_col1}{$wk_line1}>16)),1,0))";
                $wk5_str = "=IF(AND({$wk_jokin_col}{$wk_line1}=0,{$wk_tanjikan_col}{$wk_line1}=0,{$wk_hijokin_col}{$wk_line1}=0,{$wk_tabusho_col}{$wk_line1}=0), 0, IF({$wk_night_col}{$wk_line1}<>1, 1, 0))";
                //専従、計算式
                $wk_sen_str = "=IF(OR({$wk_night_sen_col}{$wk_line1}=1, {$wk_no_night_col}{$wk_line1}=1), 0, IF(AND({$wk_jokin_col}{$wk_line1}=1,{$wk_tanjikan_col}{$wk_line1}=0,{$wk_hijokin_col}{$wk_line1}=0,{$wk_tabusho_col}{$wk_line1}=0), 1, MIN(1,(ROUND(({$wk_col1}{$wk_line}+{$wk_col1}{$wk_line1})/({$day_cnt}*{$standard_array[0]["labor_cnt"]}/7),2)))))";
            }
            else {
                //夜勤有り、=IF(AS39>16,1,0)
                $wk4_str = "=IF({$wk_col1}{$wk_line1}>16,1,0)";
                //夜勤無し、=IF(AS39<=16,1,0)
                $wk5_str = "=IF({$wk_col1}{$wk_line1}<=16,1,0)";
                //専従、=IF(OR(K39=1,J39=1),0,IF(AND(F39=1,G39=0,H39=0),1,MIN(1,(AS38+AS39)/($M32*$M31/7))))
                $wk_sen_str = "=IF(OR({$wk_night_sen_col}{$wk_line1}=1, {$wk_no_night_col}{$wk_line1}=1),0,IF(AND({$wk_jokin_col}{$wk_line1}=1,{$wk_hijokin_col}{$wk_line1}=0,{$wk_tabusho_col}{$wk_line1}=0),1,MIN(1,(ROUND(({$wk_col1}{$wk_line}+{$wk_col1}{$wk_line1})/({$day_cnt}*{$standard_array[0]["labor_cnt"]}/7),2)))))";
            }
            $data .= "<td align=\"right\">" . $wk_font . $wk2 . "</font></td>\n";
            $data .= "<td align=\"right\">" . $wk_font . $wk3 . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk4_str . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk5_str . "</font></td>\n";
            $data .= "<td align=\"right\">" . $wk_font . $wk6 . "</font></td>\n";

            //夜勤従事者数
            $wk = "　";
            //16時間超の夜勤有り、夜勤専従以外の場合 20120229
            if ($wk4 == "1" && $wk6 != "1") {
                //"other_post_flg"他部署兼務（１：有り）
                //"duty_form"勤務形態（1:常勤、2:非常勤 3:短時間勤務）
                if (($data_array[$i]["other_post_flg"] == "1") ||
                        ($data_array[$i]["duty_form"] == "2") ||
                        ($data_array[$i]["duty_form"] == "3") ||
                        ($data_array[$i]["duty_form"] == "")) {
                    //時間数から率を計算、小数点第3位以下四捨五入へ変更 20120315
                    $wk = round((($wk_times_day + $wk_times_1 + $wk_times_2) / $wk_labor_month) * 100) / 100;
                    if ($wk > 1) {
                        $wk = "1";
                    }
                } else {
                    //夜勤従事者数に1を計上
                    $wk = "1";
                }
            }
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk_sen_str . "</font></td>\n";

            $data .= "<td align=\"left\">" . $wk_font . "夜勤時間数</font></td>\n";

            //勤務時間（夜勤）
            for($k=1; $k<=$day_cnt; $k++) {
                $wk = $data_array[$i]["night_time_$k"];
                if ($wk <= 0) {
                    $wk = "　";
                } else {
                    $wk = sprintf("%01.2f",$wk);
                    $wk = $this->obj->rtrim_zero($wk);
                }
                //勤務時間（夜勤）
                $data .= "<td align=\"right\">" . $wk_font . $wk . "<font></td>\n";
            }

            //月勤務時間数（延べ時間数）
            //（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数
            //職種別月勤務時間数（看護師）
            //職種別月勤務時間数（准看護師）
            //エクセルの計算式を設定
            $wk_line = $odd_base_row + ($i * 2);
            $wk_line_job_nm = $wk_line - 1; // 職種位置

            $wk1 = "=SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line})";
            //夜勤なし、夜勤専従を確認する
            //$wk2 = "=IF(OR(SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line})<={$wk_night_chk_hour},{$wk_night_sen_col}{$wk_line}=1),SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line}),0)";
            $wk2 = "=IF(OR({$wk_night_sen_col}{$wk_line}=1,{$wk_no_night_col}{$wk_line}=1),{$wk_col1}{$wk_line},0)";

            $wk3 = "=IF(". $this->excel_or("A{$wk_line_job_nm}", $nurse1_name) .",SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line}),0)";

            $wk4 = "=IF(". $this->excel_or("A{$wk_line_job_nm}", $nurse2_name) .",SUM({$wk_start_col}{$wk_line}:{$wk_end_col}{$wk_line}),0)";

            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk1 . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk2 . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk3 . "</font></td>\n";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk4 . "</font></td>\n";
            $data .= "</tr>\n";

            //データ設定
            $wk_array[$i]["data"] = $data;
        }
        $wk_array["sum_night_times"] = $sum_night_times;
        return $wk_array;
    }
    /*************************************************************************/
    //
    // 看護職員表（表　合計）
    //
    /*************************************************************************/
    function showNurseData_3(
        $excel_flg,                 //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
        $data_array,                //看護職員情報
        $day_cnt,                   //日数
        $sum_night_times,
        $group_id,
        $hosp_patient_cnt,      //１日平均入院患者数
        $report_kbn,                //届出区分
        $duty_yyyy,
        $duty_mm
    ) {
        $wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
        $data = "";
        if ($excel_flg == "1") {
            $wk_font =  "<font size=\"1\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
        } else {
            $wk_font =  "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
        }
        //-------------------------------------------------------------------------------
        // 看護職員表（合計　日勤時間数）
        //-------------------------------------------------------------------------------
        $data .= "<tr>\n";
        $data .= "<td bgcolor=\"#DFFFDC\" rowspan=\"2\"></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\" rowspan=\"2\">" . $wk_font . "計</font></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\" rowspan=\"2\">" . $wk_font . "　</font></td>\n";
        if ($group_id == "0") {
            $data .= "<td bgcolor=\"#DFFFDC\" rowspan=\"2\">" . $wk_font . "　</font></td>\n";
        }
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "常勤</font></td>\n";
        if ($wk_duty_yyyymm >= "201204") {
            $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "短時間</font></td>\n";
        }
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "非常勤</font></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "他部署兼務</font></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "有</font></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "無</font></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "夜専</font></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "夜勤従事者</font></td>\n";
        $data .= "<td bgcolor=\"#DFFFDC\">" . $wk_font . "日勤時間数</font></td>\n";

        //カラム位置のアルファベット
        $wk_pos = 3; //"A" + 3 > "D"
        if ($group_id == "0") {
            $wk_pos++;
        }
        $wk_col_3 = chr(ord("A") + $wk_pos); //常勤欄
        if ($wk_duty_yyyymm >= "201204") {
            $wk_pos++;
        }
        $wk_pos += 6;
        $wk_night_senju_col = chr(ord("A") + $wk_pos); //夜勤専従列
        $wk_pos += 2;
        $title_col_cnt = $wk_pos; //氏名等の列数
        $wk_col_1 = chr(ord("A") + $wk_pos); //"N";日付欄開始
        $wk_pos += 16;
        $wk_col_2 = chr(ord("A") + $wk_pos - 26); // "A?":16日の位置の右側
        $wk_base_day = 26 - $title_col_cnt + 1; //26("Z"),"A"0始まりのため位置調整

        //開始行
        //平成２４年度改訂対応 20120406
        if ($wk_duty_yyyymm >= "201204") {
            $even_base_row = 8; //偶数行
            $odd_base_row = 9; //奇数行
        }
        else {
            $even_base_row = 6; //偶数行
            $odd_base_row = 7; //奇数行
        }

        //勤務時間（日勤）
        for($k=1; $k<=$day_cnt; $k++) {
            if ($k < $wk_base_day) {
                $wk_col = chr(ord($wk_col_1) + $k - 1);
            } else {
                $wk_col = "A".chr(ord("A") + $k - $wk_base_day);
            }
            //偶数行を集計するエクセルの式を設定
            //例、=SUMPRODUCT((MOD(ROW(E6:E74),2)=0)*E6:E74)
            $wk_cols = "{$wk_col}{$even_base_row}:{$wk_col}".(($even_base_row-2)+count($data_array)*2);
//          $wk = "=SUM(IF(MOD(ROW({$wk_cols}),2)=0,{$wk_cols},0))";
            $wk = "=SUMPRODUCT((MOD(ROW({$wk_cols}),2)=0)*{$wk_cols})";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk . "</font></td>\n";
        }
        //月勤務時間数（延べ時間数）
        //（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数
        //職種別月勤務時間数（看護師　※保健師、助産師などを含む）
        //職種別月勤務時間数（准看護師）
        for($j=0; $j<4; $j++) {
            if ($j == 1) {  //夜勤の列のため、日勤の行は設定不要
                $wk = "　";
            } else {
                //偶数行を集計するエクセルの式を設定
                $wk_col = "A".chr(ord("A") + $title_col_cnt + $day_cnt - 26 + $j);
                $wk_cols = "{$wk_col}{$even_base_row}:{$wk_col}".(($even_base_row-2)+count($data_array)*2);
                $wk = "=SUMPRODUCT((MOD(ROW({$wk_cols}),2)=0)*{$wk_cols})";
            }
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk . "</font></td>\n";
        }
        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        // 看護職員表（合計　夜勤時間数）
        //-------------------------------------------------------------------------------
        $data .= "<tr>\n";

        //常勤／非常勤／他部署兼務
        //夜勤：有り／夜勤：無し／夜専
        $wk_cnt = ($wk_duty_yyyymm >= "201204") ? 8 : 7; //常勤等、集計列の数
        for($j=0; $j<$wk_cnt; $j++) {
            //奇数行を集計するエクセルの式を設定
            //例、=SUM(IF(MOD(ROW(E7:E75),2)=1,E7:E75,0))
            $wk_col = chr(ord($wk_col_3)+$j);
            $wk_cols = "{$wk_col}{$odd_base_row}:{$wk_col}".(($odd_base_row-2)+count($data_array)*2);
            //$wk = "=SUM(IF(MOD(ROW({$wk_cols}),2)=1,{$wk_cols},0))";
            //夜勤従事者数の計、小数点以下2位で切捨て
            if ($j == $wk_cnt - 1) {
                $wk = "=ROUNDDOWN(SUM(IF(MOD(ROW({$wk_cols}),2)=1,{$wk_cols},0)), 1)";
            }
            else {
                $wk = "=SUM(IF(MOD(ROW({$wk_cols}),2)=1,{$wk_cols},0))";
            }
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk . "</font></td>\n";
        }
        //夜勤従事者
        $data .= "<td align=\"left\" bgcolor=\"DFFFDC\">" . $wk_font . "夜勤時間数</font></td>\n";

        //勤務時間（夜勤）
        for($k=1; $k<=$day_cnt; $k++) {
            if ($k < $wk_base_day) {
                $wk_col = chr(ord($wk_col_1) + $k - 1);
            } else {
                $wk_col = "A".chr(ord("A") + $k - $wk_base_day);
            }
            //奇数行を集計するエクセルの式を設定
            //例、=SUM(IF(MOD(ROW(E7:E75),2)=1,E7:E75,0))
            $wk_cols = "{$wk_col}{$odd_base_row}:{$wk_col}".(($odd_base_row-2)+count($data_array)*2);
            $wk = "=SUMPRODUCT((MOD(ROW({$wk_cols}),2)=1)*{$wk_cols})";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk . "</font></td>\n";
        }
        //月勤務時間数（延べ時間数）
        //（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数
        //職種別月勤務時間数（看護師）
        //職種別月勤務時間数（准看護師）
        for($j=0; $j<4; $j++) {
            //奇数行を集計するエクセルの式を設定
            //例、=SUM(IF(MOD(ROW(E7:E75),2)=1,E7:E75,0))
            $wk_col = "A".chr(ord("A") + $title_col_cnt + $day_cnt - 26 + $j);
            $wk_cols = "{$wk_col}{$odd_base_row}:{$wk_col}".(($odd_base_row-2)+count($data_array)*2);
            $wk = "=SUMPRODUCT((MOD(ROW({$wk_cols}),2)=1)*{$wk_cols})";
            $data .= "<td align=\"right\" bgcolor=\"#DFFFDC\">" . $wk_font . $wk . "</font></td>\n";
        }
        $data .= "</tr>\n";
        //病棟平均夜勤時間数 追加 20111206
        if ($group_id != "0") {
            $wk_colspan = $day_cnt + $title_col_cnt;

            //夜勤従事者数等追加 20120126
            $wk_row1 = ($even_base_row+count($data_array)*2); //日勤時間数計の行
            $wk_row2 = $wk_row1 + 1; //夜勤時間数計の行
            $wk_col1 = "A".chr(ord("A") + $title_col_cnt + $day_cnt - 26 + 0); //月勤務時間数の列
            $wk_col2 = "A".chr(ord("A") + $title_col_cnt + $day_cnt - 26 + 1); //（再掲）〜夜勤時間数の列
            $wk_col3 = "A".chr(ord("A") + $title_col_cnt + $day_cnt - 26 + 2); //（看護師の列
            //$wk_night_senju_col = "K";

            $wk_row_c = $wk_row1 + 11; //月延べ勤務時間数計[C]の行
            $wk_row_d = $wk_row1 + 5; //月延べ夜勤時間数計[D]の行
            $wk_row_e = $wk_row1 + 6; //夜勤専従、１６時間以下夜勤[E]の行
            $wk_row_a = $wk_row1 + 12; //１日平均入院患者数[A]の行
            $wk_row_kbn = $wk_row1 + 13; //届出区分比率の行
            $wk_row_day = $wk_row1 + 14; //今月の稼働日数の行

            $data .= "</table>\n";
            $data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";

            for ($i=0; $i<12; $i++) {

                $wk_title = "";
                $wk = "";
                switch($i) {
                    case 0:
                        $wk_title = "＊以下は病棟単位の計算結果です";
                        $wk = "";
                        break;
                    case 1:
                        $wk_title = "月平均夜勤時間数[（D-E）/B]";
                        $wk = "=IF({$wk_night_senju_col}{$wk_row2}>0,(ROUNDDOWN(({$wk_col1}{$wk_row2}-{$wk_col2}{$wk_row2})/{$wk_night_senju_col}{$wk_row2},2)),0)";
                        break;
                    case 2:
                        $wk_title = "月延べ夜勤時間数計[D]";
                        $wk = "={$wk_col1}{$wk_row2}";
                        break;
                    case 3:
                        $wk_title = "夜勤専従、１６時間以下夜勤時間数[E]";
                        $wk = "={$wk_col2}{$wk_row2}";
                        break;
                    case 4:
                        $wk_title = "月延べ夜勤時間数[D-E]";
                        $wk = "={$wk_col3}{$wk_row_d}-{$wk_col3}{$wk_row_e}";
                        break;
                    case 5:
                        $wk_title = "夜勤従事者数[B]";
                        $wk = "={$wk_night_senju_col}{$wk_row2}";
                        break;
                    case 6:
                        //地域包括ケア 20140325
                        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
                            $wk_kbn_title = $report_kbn - 900;
                            $wk = "=ROUNDUP((({$wk_col3}{$wk_row_a}/{$wk_kbn_title}*3)),0)";
                        }
                        else {
                            $wk_kbn_title = "届出区分";
                            $wk = "=ROUNDUP((({$wk_col3}{$wk_row_a}/{$wk_col3}{$wk_row_kbn}*3)),0)";
                        }
                        $wk_title = "１日看護配置数（A/{$wk_kbn_title}）×3";
                        break;
                    case 7:
                        $wk_title = "月平均１日看護配置数[C/（日数×8）]";
                        $wk = "=ROUNDDOWN({$wk_col3}{$wk_row_c}/({$wk_col3}{$wk_row_day}*8),1)";
                        break;
                    case 8:
                        $wk_title = "月延べ勤務時間数計[C]";
                        $wk = "={$wk_col1}{$wk_row1}+{$wk_col1}{$wk_row2}";
                        break;
                    case 9:
                        $wk_title = "１日平均入院患者数[A]";
                        $wk = "$hosp_patient_cnt";
                        break;
                    case 10:
                        $wk_title = "届出区分";
                        $wk = ($report_kbn > 900) ? $report_kbn - 900 : $report_kbn;
                        break;
                    case 11:
                        $wk_title = "今月の稼働日数";
                        $wk = "$day_cnt";
                        break;
                }
                $data .= "<tr>\n";
                $data .= "<td colspan=\"" . $wk_colspan . "\">\n";
                $data .= "</td>\n";
                //コメント表示
                if ($i == 0) {
                    $wk_style = "";
                    $wk_align = "left";
                }
                else {
                    $wk_style = "style=\"border-style: solid; border-width: thin;\"";
                    $wk_align = "right";
                }
                if ($excel_flg == "1") {
                    $wk_font =  "<font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
                }
                $data .= "<td nowrap align=\"{$wk_align}\" colspan=\"2\" {$wk_style}>{$wk_font}{$wk_title}\n";
                $data .= "</font></td>\n";
                $data .= "<td nowrap align=\"{$wk_align}\" colspan=\"2\" {$wk_style}>{$wk_font}{$wk}\n";
                $data .= "</font></td>\n";
                $data .= "</tr>\n";

            }
        }

        $wk_array = array();
        $wk_array["data"] = $data;
        return $wk_array;
    }
    /*************************************************************************/
    //
    // 看護職員表（療養病棟入院基本料における夜勤体制の要件を満たすことの確認）
    //
    /*************************************************************************/
    function showNurseData_4(
                            $excel_flg,                 //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                            $day_cnt,                   //日数
                            $sum_night_b,               //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                            $sum_night_c,               //月延べ勤務時間数の計〔Ｃ〕
                            $sum_night_d,               //月延べ夜勤時間数の計〔Ｄ〕
                            $sum_night_e,               //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
                            $sum_night_f,               //1日看護配置数〔(A ／届出区分の数)×３〕
                            $sum_night_g                //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
    ) {
        //-------------------------------------------------------------------------------
        // 初期値設定
        //-------------------------------------------------------------------------------
        $data = "";
        if ($excel_flg == "1") {
            $colspan_1 = 2;
            $colspan_2 = 1;
            $colspan_3 = 4;
            $colspan_4 = 1;
        } else {
            $colspan_1 = 1;
            $colspan_2 = 1;
            $colspan_3 = 1;
            $colspan_4 = 1;
        }
        $colspan_top = $colspan_1 + $colspan_2 + $colspan_3 + $colspan_4;

        //-------------------------------------------------------------------------------
        //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
        //月延べ勤務時間数の計〔Ｃ〕
        //-------------------------------------------------------------------------------
        $wk1 = sprintf("%01.1f",$sum_night_b);
        $wk1 = $this->obj->rtrim_zero($wk1);
        $wk2 = sprintf("%01.2f",$sum_night_c);
        $wk2 = $this->obj->rtrim_zero($wk2);
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_1 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">夜勤従事者数(夜勤ありの職員数)〔Ｂ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\">" . $wk1 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk1 . "\" readonly></td>\n";
        }
        $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月延べ勤務時間数の計〔Ｃ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">" . $wk2 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk2 . "\" readonly></td>\n";
        }
        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        //月延べ夜勤時間数〔Ｄ−Ｅ〕
        //月延べ夜勤時間数の計〔Ｄ〕
        //-------------------------------------------------------------------------------
        $wk1 = sprintf("%01.2f",($sum_night_d - $sum_night_e));
        $wk1 = $this->obj->rtrim_zero($wk1);
        $wk2 = sprintf("%01.2f",$sum_night_d);
        $wk2 = $this->obj->rtrim_zero($wk2);
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_1 . "\" rowspan=\"2\" bgcolor=\"#f6f9ff\">月延べ夜勤時間数〔Ｄ−Ｅ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"2\">" . $wk1 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_2 . "\" rowspan=\"2\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk1 . "\" readonly></td>\n";
        }
        $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月延べ夜勤時間数の計〔Ｄ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">" . $wk2 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk2 . "\" readonly></td>\n";
        }
        $data .= "</tr>\n";

        //夜勤専従者及び月１６時間以下の者の夜勤時間数〔Ｅ〕
        $wk = sprintf("%01.2f",$sum_night_e);
        $wk = $this->obj->rtrim_zero($wk);
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">夜勤専従者及び月１６時間以下の者の夜勤時間数〔Ｅ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">" . $wk . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk . "\" readonly></td>\n";
        }
        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        //１日看護配置数〔(Ａ／届出区分の数)×３〕
        //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
        //-------------------------------------------------------------------------------
        $wk1 = sprintf("%01.1f",$sum_night_f);
        $wk1 = $this->obj->rtrim_zero($wk1);
        $wk2 = sprintf("%01.1f",$sum_night_g);
        $wk2 = $this->obj->rtrim_zero($wk2);
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_1 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">１日看護配置数〔(Ａ／届出区分の数)×３〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\">" . $wk1 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk1 . "\" readonly></td>\n";
        }
        $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月平均１日あたり看護配置数〔Ｃ／(日数×８）〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">" . $wk2 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk2 . "\" readonly></td>\n";
        }
        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        //データ設定
        //-------------------------------------------------------------------------------
        $wk_array = array();
        $wk_array["data"] = $data;

        return $wk_array;
    }

    //平成２４年新様式
    function showNurseData_4_new(
        $excel_flg,                 //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
        $day_cnt,                   //日数
        $sum_night_b,               //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
        $sum_night_c,               //月延べ勤務時間数の計〔Ｃ〕
        $sum_night_d,               //月延べ夜勤時間数の計〔Ｄ〕
        $sum_night_e,               //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
        $sum_night_f,               //1日看護配置数〔(A ／届出区分の数)×３〕
        $sum_night_g,               //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
        $sum_night_h,             //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
        $sum_night_i,              //１日看護夜間配置数　〔A ／１２〕
        $report_kbn
        ) {
        //-------------------------------------------------------------------------------
        // 初期値設定
        //-------------------------------------------------------------------------------
        $data = "";
        if ($excel_flg == "1") {
            $colspan_1 = 1;
            $colspan_2 = 3;
            $colspan_3 = 1;
            $colspan_4 = 2;
            $colspan_5 = 1;
        } else {
            $colspan_1 = 1;
            $colspan_2 = 1;
            $colspan_3 = 1;
            $colspan_4 = 1;
            $colspan_5 = 1;
        }
        $colspan_top = $colspan_1 + $colspan_2 + $colspan_3 + $colspan_4 + $colspan_5;

        //-------------------------------------------------------------------------------
        //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
        //月延べ勤務時間数の計〔Ｃ〕
        //-------------------------------------------------------------------------------
        $wk1 = sprintf("%01.1f",$sum_night_b);
        $wk1 = $this->obj->rtrim_zero($wk1);
        $wk2 = sprintf("%01.2f",$sum_night_c);
        $wk2 = $this->obj->rtrim_zero($wk2);
        $data .= "<tr>\n";
        $data .= "<td align=\"center\" colspan=\"" . $colspan_1 . "\" rowspan=\"3\" bgcolor=\"#f6f9ff\">看護職員の勤務実績(勤務時間数)</td>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">夜勤従事者数(夜勤ありの職員数)〔Ｂ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">" . $wk1 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk1 . "\" readonly></td>\n";
        }
        $data .= "<td align=\"right\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月延べ勤務時間数の計〔Ｃ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_5 . "\" rowspan=\"1\">" . $wk2 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_5 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk2 . "\" readonly></td>\n";
        }
        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        //月延べ夜勤時間数〔Ｄ−Ｅ〕
        //月延べ夜勤時間数の計〔Ｄ〕
        //-------------------------------------------------------------------------------
        $wk1 = sprintf("%01.2f",($sum_night_d - $sum_night_e));
        $wk1 = $this->obj->rtrim_zero($wk1);
        $wk2 = sprintf("%01.2f",$sum_night_d);
        $wk2 = $this->obj->rtrim_zero($wk2);
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"2\" bgcolor=\"#f6f9ff\">月延べ夜勤時間数〔Ｄ−Ｅ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"2\">" . $wk1 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_3 . "\" rowspan=\"2\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk1 . "\" readonly></td>\n";
        }
        $data .= "<td align=\"right\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月延べ夜勤時間数の計〔Ｄ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_5 . "\" rowspan=\"1\">" . $wk2 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_5 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk2 . "\" readonly></td>\n";
        }
        $data .= "</tr>\n";

        //夜勤専従者及び月１６時間以下の者の夜勤時間数〔Ｅ〕
        $wk = sprintf("%01.2f",$sum_night_e);
        $wk = $this->obj->rtrim_zero($wk);
        $data .= "<tr>\n";
        $data .= "<td nowrap align=\"right\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">夜勤専従者及び月１６時間以下の者の夜勤時間数〔Ｅ〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_5 . "\" rowspan=\"1\">" . $wk . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_5 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk . "\" readonly></td>\n";
        }

        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        //１日看護配置数〔(Ａ／届出区分の数)×３〕
        //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
        //-------------------------------------------------------------------------------
        $wk1 = sprintf("%01.1f",$sum_night_f);
        $wk1 = $this->obj->rtrim_zero($wk1);
        $wk2 = sprintf("%01.1f",$sum_night_g);
        $wk2 = $this->obj->rtrim_zero($wk2);
        $data .= "<tr>\n";
        $data .= "<td align=\"center\" colspan=\"" . $colspan_1 . "\" rowspan=\"2\" bgcolor=\"#f6f9ff\">１日当たり看護配置数</td>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月平均１日あたり看護配置数〔Ｃ／(日数×８）〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">" . $wk2 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk2 . "\" readonly></td>\n";
        }
        $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px thin; border-color : #ffffff #ffffff #ffffff #000000;\">（実績値）</td>\n";
        $data .= "<td align=\"left\" colspan=\"" . $colspan_5 . "\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px 0px; border-color : #ffffff #ffffff #ffffff #ffffff;\"></td>\n";
        $data .= "</tr>\n";
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">１日看護配置数〔(Ａ／届出区分の数)×３〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">" . $wk1 . "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk1 . "\" readonly></td>\n";
        }

        $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px thin; border-color : #ffffff #ffffff #ffffff #000000;\">（基準値）</td>\n";
        $data .= "</tr>\n";

        //一般病棟の場合（療養病棟以外、区分=20,25）
        if ($report_kbn != "20" && $report_kbn != "25") {
            //
            $wk1 = sprintf("%01.1f",$sum_night_h);
            $wk1 = $this->obj->rtrim_zero($wk1);
            $wk2 = sprintf("%01.1f",$sum_night_i);
            $wk2 = $this->obj->rtrim_zero($wk2);
            $data .= "<tr>\n";
            $data .= "<td align=\"center\" colspan=\"" . $colspan_1 . "\" rowspan=\"2\" bgcolor=\"#f6f9ff\">＜看護職員夜間看護配置加算＞<br>１日当たり夜間看護配置数</td>\n";
            $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月平均１日当たり夜間看護配置数〔Ｄ／(日数×１６）〕</td>\n";
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">" . $wk1 . "</td>\n";
            } else {
                $data .= "<td align=\"left\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">";
                $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk1 . "\" readonly></td>\n";
            }
            $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px thin; border-color : #ffffff #ffffff #ffffff #000000;\">（実績値）</td>\n";
            $data .= "</tr>\n";
            $data .= "<tr>\n";
            $data .= "<td align=\"right\" colspan=\"" . $colspan_2 . "\" rowspan=\"1\" bgcolor=\"#f6f9ff\">１日看護夜間配置数　〔Ａ／１２〕</td>\n";
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">" . $wk2 . "</td>\n";
            } else {
                $data .= "<td align=\"left\" colspan=\"" . $colspan_3 . "\" rowspan=\"1\">";
                $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"\" value=\"" . $wk2 . "\" readonly></td>\n";
            }

            $data .= "<td align=\"left\" colspan=\"" . $colspan_4 . "\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px thin; border-color : #ffffff #ffffff #ffffff #000000;\">（基準値）</td>\n";
            $data .= "</tr>\n";
        }
        //-------------------------------------------------------------------------------
        //データ設定
        //-------------------------------------------------------------------------------
        $wk_array = array();
        $wk_array["data"] = $data;

        return $wk_array;
    }
    //地域包括ケア 20140326
    function showCommunityCareData(
        $excel_flg,                 //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
        $day_cnt,                   //日数
        $sum_night_b,               //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
        $sum_night_c,               //月延べ勤務時間数の計〔Ｃ〕
        $sum_night_d,               //月延べ夜勤時間数の計〔Ｄ〕
        $sum_night_e,               //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
        $sum_night_f,               //1日看護配置数〔(A ／届出区分の数)×３〕
        $sum_night_g,               //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
        $sum_night_h,             //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
        $sum_night_i,              //１日看護夜間配置数　〔A ／１２〕
        $report_kbn
        ) {
        //-------------------------------------------------------------------------------
        // 初期値設定
        //-------------------------------------------------------------------------------
        $data = "";
        if ($excel_flg == "1") {
            $colspan_1 = 1;
            $colspan_2 = 3;
            $colspan_3 = 1;
            $colspan_4 = 2;
            $colspan_5 = 1;
        } else {
            $colspan_1 = 1;
            $colspan_2 = 1;
            $colspan_3 = 1;
            $colspan_4 = 1;
            $colspan_5 = 1;
        }
        $colspan_top = $colspan_1 + $colspan_2 + $colspan_3 + $colspan_4 + $colspan_5;

        $data .= "<tr>\n";
        $data .= "<td colspan=\"" . $colspan_top . "\" style=\"border-style : solid solid solid solid; border-width : 1px 1px 1px 1px; border-color : #5279a5 #ffffff #5279a5 #ffffff;\">\n";
        $data .= "</td>\n";
        $data .= "</tr>\n";
        
        $data .= "<tr>\n";
        $data .= "<td colspan=\"" . $colspan_top . "\" style=\"border-style : solid solid solid solid; border-width : 1px 1px 1px 1px; border-color : #5279a5 #ffffff #5279a5 #ffffff;\">\n";
        $data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";
        $data .= "<tr><td nowrap align=\"left\" colspan=\"" . $colspan_top . "\">";
        $data .= "〔看護職員配置加算を届け出る場合の看護職員数の算出方法〕";
        $data .= "</td></tr>\n";
        $data .= "</table>\n";
        $data .= "</td>\n";
        $data .= "</tr>\n";
        
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"4\" rowspan=\"1\" bgcolor=\"#f6f9ff\">１日看護配置数〔Ｆ〕　　〔(Ａ/13)×３〕</td>\n";
        $wk1 = sprintf("%01.1f",$sum_night_f);
        $wk1 = $this->obj->rtrim_zero($wk1);
        $data .= "<td align=\"right\" colspan=\"1\" rowspan=\"1\">" . $wk1 . "</td>\n";
        $data .= "<td align=\"left\" colspan=\"3\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px thin; border-color : #ffffff #ffffff #ffffff #000000;\">（基準値）</td>\n";
        $data .= "</tr>\n";

        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"4\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月平均１日当たり看護配置数〔Ｇ〕　　〔看護職員のみのＣ/(日数×８(時間)〕</td>\n";
        //$wk1 = sprintf("%01.1f",($sum_night_c/($day_cnt * 8)));
        $wk1 = $sum_night_c/($day_cnt * 8);
        $wk1 = floor($wk1 * 10) / 10;
        $wk1 = $this->obj->rtrim_zero($wk1);
        $data .= "<td align=\"right\" colspan=\"1\" rowspan=\"1\">" . $wk1 . "</td>\n";
        $data .= "<td align=\"left\" colspan=\"3\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px thin; border-color : #ffffff #ffffff #ffffff #000000;\">（実績値）</td>\n";
        $data .= "</tr>\n";
        
        $data .= "<tr>\n";
        $data .= "<td align=\"right\" colspan=\"4\" rowspan=\"1\" bgcolor=\"#f6f9ff\">月平均１日当たり当該入院料の施設基準の最小必要人数以上の看護職員配置数看護職員数<br>{〔看護職員のみのＣ〕 − （〔Ｆ〕×日数×８（時間））} /（日数×８（時間））</td>\n";
        $wk1 = ($sum_night_c - ($sum_night_f * $day_cnt * 8))/($day_cnt * 8);
        if ($wk1 < 0) {
            $wk1 = 0;
        }
        else {
            $wk1 = floor($wk1 * 10) / 10;
            //$wk1 = sprintf("%01.1f", $wk1);
            $wk1 = $this->obj->rtrim_zero($wk1);
        }
        $data .= "<td align=\"right\" colspan=\"1\" rowspan=\"1\">" . $wk1 . "</td>\n";
        $data .= "<td align=\"left\" colspan=\"3\" rowspan=\"1\" style=\"border-style : solid solid solid solid; border-width : 0px 0px 0px thin; border-color : #ffffff #ffffff #ffffff #000000;\"></td>\n";
        $data .= "</tr>\n";
        
        //-------------------------------------------------------------------------------
        //データ設定
        //-------------------------------------------------------------------------------
        $wk_array = array();
        $wk_array["data"] = $data;
        
        return $wk_array;
    }
    
}
