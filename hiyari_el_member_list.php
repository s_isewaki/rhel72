<?


require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_member_list.ini");
require_once("hiyari_common.ini");


//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
if (check_authority($session, 47, $fname) == "0")
{
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//デフォルト値
//==============================

// イニシャルのデフォルトは「あ行」とする
if ($in_id == "")
{
	$in_id = 1;
}

//職種のデフォルトはログインユーザーの職種とする。
if($job_id == "")
{
	//ログインユーザーID取得
	$select_id = "select emp_id from login where emp_id in (select emp_id from session where session_id = '$session')";
	$result_select = pg_exec($con, $select_id);
	if($result_select == false)
	{
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_result($result_select, 0, "emp_id");	$select_empmst = "select emp_job from empmst where emp_id = '$emp_id'";
	
	//ログインユーザーの職種ID取得
	$result_j = pg_exec($con,$select_empmst);
	if($result_j == false)
	{
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$job_id = pg_result($result_j,0,"emp_job");
}

//==============================
//職種一覧の検索
//==============================
$select_name = "select job_id, job_nm from jobmst where job_del_flg = 'f' order by order_no";
$result_name = pg_exec($con,$select_name);
if($result_name == false)
{
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
//職種一覧の件数
$count = pg_numrows($result_name);









//画面名
$PAGE_TITLE = "登録者指定";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>

<script language="javascript">
<!--
function getSelected()
{
	var select = document.member.select.value;
	var mem = document.member.member.value;
	var url = "./hiyari_el_member_list.php?session=<? echo($session); ?>&job_id="+select+"&member="+mem;
	location.href= url;
}

function checkIn(in_member, in_name, route)
{
	opener.document.search.emp_name.value = in_name;
	opener.document.search.hid_emp_name.value = in_name;
	opener.document.search.hid_emp_id.value = in_member;
	self.close();
}
//-->
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>


<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>

<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5" align="center">




<form name="member">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#F5FFE5">
<td height="22"><?
echo("<select name=\"select\" onChange=\"getSelected();\">\n");
echo("<option value=\"0\" \n");
if($select == 0)
{
	echo("selected");
}

echo(">すべて</option>\n");
for($i=0; $i<$count; $i++)
{
	$j_id = pg_result($result_name, $i, job_id);
	$j_nm = pg_result($result_name, $i, job_nm);
	echo("<option value=\"$j_id\"");
	if($job_id == $j_id)
	{
		echo(" selected");
	}
	echo(">\n");
	echo($j_nm);
	echo("</option>\n");
}
echo("</select>\n");
?>
</td>
</tr>
<tr bgcolor="#F5FFE5">
<td height="22"><? show_library_member_list($job_id,$in_id,$member,$session,$num); ?></td>
</tr>
<tr bgcolor="#F5FFE5">
<td height="22"><? show_member_list($job_id,$in_id,$member,$session,$num); ?></td>
</tr>
</table>
<input type="hidden" name="member" value="<? echo($member); ?>">
</form>




		</td>
		<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/r_3.gif" width="10" height="10"></td>
		<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td><img src="img/r_4.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- 本体 END -->



</td>
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
