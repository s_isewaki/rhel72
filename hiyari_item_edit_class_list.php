<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//======================================
// 組織階層情報取得
//======================================
$arr_class_name = get_class_name_array($con, $fname);

$classes_nm = $arr_class_name["class_nm"]."＞".$arr_class_name["atrb_nm"]."＞".$arr_class_name["dept_nm"];
if($arr_class_name["class_cnt"] == 4) {
    $classes_nm .= "＞".$arr_class_name["room_nm"];
}

//======================================
// 部門情報取得
//======================================
$sql = "select a.class_id, a.class_nm from classmst a";
$cond = "where a.class_del_flg = 'f' order by a.order_no asc";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$classes = array();
while ($row_class = pg_fetch_array($sel_class)) {
    $tmp_class_id = $row_class["class_id"];

    $classes[$tmp_class_id][0][0][0] = array("name" => $row_class["class_nm"]);

    $sql = "select a.atrb_id, a.atrb_nm from atrbmst a";
    $cond = "where a.class_id = $tmp_class_id and a.atrb_del_flg = 'f' order by a.order_no asc";
    $sel_atrb = select_from_table($con, $sql, $cond, $fname);
    if ($sel_atrb == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row_atrb = pg_fetch_array($sel_atrb)) {
        $tmp_atrb_id = $row_atrb["atrb_id"];

        $classes[$tmp_class_id][$tmp_atrb_id][0][0] = array("name" => $row_atrb["atrb_nm"]);

        $sql = "select a.dept_id, a.dept_nm from deptmst a";
        $cond = "where a.atrb_id = $tmp_atrb_id and a.dept_del_flg = 'f' order by a.order_no asc";
        $sel_dept = select_from_table($con, $sql, $cond, $fname);
        if ($sel_dept == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row_dept = pg_fetch_array($sel_dept)) {
            $tmp_dept_id = $row_dept["dept_id"];

            $classes[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id][0] = array("name" => $row_dept["dept_nm"]);

            if ($arr_class_name["class_cnt"] == 4) {

                $sql = "select a.room_id, a.room_nm from classroom a";
                $cond = "where a.dept_id = $tmp_dept_id and a.room_del_flg = 'f' order by a.order_no asc";
                $sel_room = select_from_table($con, $sql, $cond, $fname);
                if ($sel_room == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                while ($row_room = pg_fetch_array($sel_room)) {
                    $tmp_room_id = $row_room["room_id"];

                    $classes[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id][$tmp_room_id] = array("name" => $row_room["room_nm"]);
                }
            }
        }
    }
}


//==============================
//項目名の取得
//==============================
$easy_item_name = get_easy_item_name($con,$fname,$grp_code,$easy_item_code);

//==============================
//部署別設定済み情報の取得
//==============================
$arr_no_disp_override = get_item_element_no_disp_override($con, $fname, $grp_code, $easy_item_code, $arr_class_name["class_cnt"]);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>

<script type="text/javascript">

function initPage() {

<?
foreach($arr_no_disp_override as $override)
{
    $disp_area = "disp_area_";
    if($override["class_id"] != "")
    {
        $disp_area .= $override["class_id"];
    }

    if($override["attribute_id"] != "")
    {
        $disp_area .= "-";
        $disp_area .= $override["attribute_id"];
    }

    if($override["dept_id"] != "")
    {
        $disp_area .= "-";
        $disp_area .= $override["dept_id"];
    }

    if($override["room_id"] != "")
    {
        $disp_area .= "-";
        $disp_area .= $override["room_id"];
    }

?>
if(document.getElementById('<?=$disp_area?>'))
{
    document.getElementById('<?=$disp_area?>').innerHTML = '部署別設定あり';
}

<?
}
?>

}



function changeDisplay(img) {
    img.className = (img.className == 'close') ? 'open' : 'close';
    var row_id = img.id.replace('img', 'tr');
    setChildDisplay(row_id);
}

function is_parent_closed(id)//例）id=1-1-1-1
{

    var ids = id.split("-");
    if(ids.length == 1)
    {
        return false;
    }
    else
    {
        var target_id = "";
        for(var i=0; i<ids.length - 1; i++)
        {
            if(target_id != ""){target_id = target_id + "-";}
            target_id = target_id + ids[i];
            if( is_closed(target_id) )
            {
                return true;
            }
        }
    }
}
function is_closed(id)
{
    var a =  document.getElementById('tr'.concat(id)).style.display == 'none';
    var b =  document.getElementById('img'.concat(id)).className == 'close';
    return a || b;
}




function setChildDisplay(parent_row_id) {
    var img = document.getElementById(parent_row_id.replace('tr', 'img'));
    if (!img) {
        return;
    }

    var rows = document.getElementById('classlist').rows;
    for (var i = 0, j = rows.length; i < j; i++) {

        var class_names = rows[i].className.split(' ');
        if (img.className == 'open') {
            if (class_names[0] == parent_row_id) {
                if(!is_parent_closed(parent_row_id.replace('tr', ''))) {
                rows[i].style.display = '';
                setChildDisplay(rows[i].id);
            }
            }
        } else {
            for (var k = 0, l = class_names.length; k < l; k++) {
                if (class_names[k] == parent_row_id) {
                    rows[i].style.display = 'none';
                    break;
                }
            }
        }
    }
}

function call_item_edit_non_disp(element_id)
{

    var images = document.getElementsByTagName('img');
    var form = document.getElementById('mainform');
    for (var i = 0, j = images.length; i < j; i++) {
        if (images[i].className == 'open') {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'opened_ids[]';
            input.value = images[i].id.replace('img', '');
            form.appendChild(input);
        }
    }


    var url = "hiyari_item_edit_non_disp.php?session=<?=$session?>&grp_code=<?=$grp_code?>&easy_item_code=<?=$easy_item_code?>&element_id=" + element_id;
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

    window.open(url, 'newwin',option);

}


function add_updated_class(element_id) {

    disp_area = "disp_area_" + element_id;
    disp_area_content = "部署別設定あり";
    document.getElementById(disp_area).innerHTML = disp_area_content;
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#35B341 solid 1px;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

img.close {
    background-image:url("img/icon/plus.gif");
    vertical-align:middle;
}
img.open {
    background-image:url("img/icon/minus.gif");
    vertical-align:middle;
}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<form id="mainform" name="mainform" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="grp_code" value="<?=$grp_code?>">
<input type="hidden" name="easy_item_code" value="<?=$easy_item_code?>">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?
    $PAGE_TITLE = "項目管理";
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<? // ヘッダータブ
show_item_edit_header_tab($session, $fname, $grp_code, $easy_item_code)
?>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>

<table width="1000" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b><?=h($easy_item_name)?></b></font>
</td>
</tr>
</table>

<table id="classlist" width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="26" bgcolor="#DFFFDC">
<td width="400"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属(<?echo($classes_nm);?>)</font></td>
<td width="600"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示設定</font></td>
</tr>

<? // １階層目(部門)
foreach ($classes as $tmp_class_id => $tmp_atrbs){
?>
<tr bgcolor="#FFFFFF" height="26" id="tr<? echo($tmp_class_id); ?>">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img id="img<? echo($tmp_class_id); ?>" src="img/spacer.gif" alt="" width="20" height="20" class="close" style="margin-right:3px;" onclick="changeDisplay(this)"><? echo($tmp_atrbs[0][0][0]["name"]); ?></font></td>
<td>
<table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
<tr>
<td><input type="button" value="設定" onclick="call_item_edit_non_disp('<? echo($tmp_class_id); ?>')"></td>


<td width="100">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo($tmp_class_id); ?>"></span></font>
</td>

</tr>


</table>
</td>
</tr>
<?
if (count($tmp_atrbs) == 1)
{
?>
<tr  bgcolor="#FFFFFF" class="tr<? echo($tmp_class_id); ?>" height="26" style="display:none;">
<td colspan="5" style="padding-left:50px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（未登録）</font></td>
</tr>
<?
}
?>

<?  // ２階層目(課)
foreach ($tmp_atrbs as $tmp_atrb_id => $tmp_depts)
{
if ($tmp_atrb_id == "0") {continue;}
?>
<tr  bgcolor="#FFFFFF" class="tr<? echo($tmp_class_id); ?>" id="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>" height="26" style="display:none;">
<td style="padding-left:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img id="img<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>" src="img/spacer.gif" alt="" width="20" height="20" class="close" style="margin-right:3px;" onclick="changeDisplay(this)"><? echo($tmp_depts[0][0]["name"]); ?></font></td>
<td>
<table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
<tr>
<td><input type="button" value="設定" onclick="call_item_edit_non_disp('<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>');"></td>

<td width="100">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>"></span></font>
</td>


</tr>
</table>
</td>
</tr>

<?
if (count($tmp_depts) == 1)
{?>
<tr bgcolor="#FFFFFF" class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>" height="26" style="display:none;">
<td colspan="5" style="padding-left:80px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（未登録）</font></td>
</tr>
<?
}
?>

<? // ３階層目(科)
foreach ($tmp_depts as $tmp_dept_id => $tmp_room)
{
if ($tmp_dept_id == "0") {continue;}
?>
<tr bgcolor="#FFFFFF" class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>" id="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>" height="26" style="display:none;">
<td style="padding-left:80px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($arr_class_name["class_cnt"] == 4) { ?><img id="img<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>" src="img/spacer.gif" alt="" width="20" height="20" class="close" style="margin-right:3px;" onclick="changeDisplay(this)"><?}?><? echo($tmp_room[0]["name"]); ?></font></td>
<td>
<table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
<tr>
<td><input type="button" value="設定" onclick="call_item_edit_non_disp('<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>');"></td>

<td width="100">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>"></span></font>
</td>

</tr>
</table>
</td>
</tr>
<?
if ($arr_class_name["class_cnt"] == 4)
{
?>
<?
if (count($tmp_room) == 1)
{
?>
<tr bgcolor="#FFFFFF" class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?> tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>" height="26" style="display:none;">
<td colspan="5" style="padding-left:110px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（未登録）</font></td>
</tr>
<?
}
?>
<? // ４階層目(室)
foreach ($tmp_room as $tmp_room_id => $tmp_data)
{
if ($tmp_room_id == "0") {continue;}
?>
<tr bgcolor="#FFFFFF" class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?> tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>" id="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>" height="26" style="display:none;">
<td style="padding-left:110px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_data["name"]); ?></font></td>
<td>

<table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
<tr>
<td><input type="button" value="設定" onclick="call_item_edit_non_disp('<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>');"></td>

<td width="100">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>"></span></font>
</td>

</tr>
</table>
</td>
</tr>
<?
}
?>

<?
}
?>

<?
}
?>
<?
}
?>

<?
}
?>
</table>


</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
</table>

</form>
</body>
</html>

<?
/**
 * 項目名を取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return integer 新しい選択項目コード。
 */
function get_easy_item_name($con,$fname,$grp_code,$easy_item_code)
{
    $sql="";
    $sql.=" select easy_item_name from inci_easyinput_item_mst";
    $sql.=" where grp_code = $grp_code and easy_item_code = $easy_item_code";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_result($sel,0,"easy_item_name");
}

/**
 * 項目名を取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return array 部署別設定済み情報
 */
function get_item_element_no_disp_override($con, $fname, $grp_code, $easy_item_code, $classes_cnt)
{

    $sql   = "select i.class_id, i.attribute_id, i.dept_id, i.room_id from inci_easyinput_item_element_no_disp_override i left join classmst c on i.class_id = c.class_id left join atrbmst a on i.attribute_id = a.atrb_id left join deptmst d on i.dept_id = d.dept_id left join classroom r on i.room_id = r.room_id";
    $cond  = "where i.grp_code = $grp_code and i.easy_item_code = $easy_item_code ";

    if($classes_cnt == 3)
    {
        $cond .= "and i.room_id is null ";
    }

    $cond .= "order by c.order_no, a.order_no, d.order_no, r.order_no ";

    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $arr = array();
    while($row = pg_fetch_array($sel))
    {
        $arr[] = array("class_id" => $row["class_id"], "attribute_id" => $row["attribute_id"], "dept_id" => $row["dept_id"], "room_id" => $row["room_id"]);
    }
    return $arr;
}

pg_close($con);
?>