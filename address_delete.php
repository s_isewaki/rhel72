<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("./about_postgres.php");

$fname=$PHP_SELF;

$con=connect2db($fname);
if($con==0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

if($trashbox==""){
	echo("<script language='javascript'>alert(\"チェックボックスをオンにしてください。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$in="where address_id in (";
$count=count($trashbox);
for($i=0;$i<$count;$i++){
	if(!$trashbox[$i]==""){
		$in.="'$trashbox[$i]'";
		if($i<($count-1)){
			$in.=",";
		}
	}
}
$in.=")";
$delete_address="delete from address $in";
$result_delete=pg_exec($con,$delete_address);
pg_close($con);
if($result_delete==true){
	if ($admin_flg == "t") {
		$next_php = "address_admin_menu.php";
	} else {
		$next_php = "address_menu.php";
	}
	
	$url_name = urlencode($search_name);
	$url_submit = urlencode($search_submit);
	echo("<script language='javascript'>location.href=\"./$next_php?session=$session&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page&search_item_flg=$search_item_flg\";</script>");
	exit;
}else{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
?>