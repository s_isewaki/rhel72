<?
require_once("holiday.php");
class date_utils
{

	/**
	 * 日曜・祝日か判定を行う
	 * @param date yyyymmddの日付型文字列
	 */
	function is_holiday($date){

		$timestamp = date_utils::to_timestamp_from_ymd($date);

		// 日付から休日かを求める
		$holiday = ktHolidayName($timestamp);

		// 日付から曜日を求める
		$wd = date_utils::get_weekday_name($date);

		//祝日か日曜か
		return ($holiday != "" || $wd == "日");
	}

	/**
	 * 日曜・祝日か判定を行う
	 * @param date yyyymmddの日付型文字列
	 */
	function is_Saturday($date){
		//土日曜か
		return (date_utils::get_weekday_name($date) == "土");
	}

	/**
	 * 曜日を取得する
	 * @param date yyyymmddの日付型文字列
	 */
	function get_weekday_name($date){

		$weekday_name = "";
		$timestamp = date_utils::to_timestamp_from_ymd($date);
		$weekday_id = date("w", $timestamp);

		switch ($weekday_id) {
			case 0:
				$weekday_name = "日";
				break;
			case 1:
				$weekday_name = "月";
				break;
			case 2:
				$weekday_name = "火";
				break;
			case 3:
				$weekday_name = "水";
				break;
			case 4:
				$weekday_name = "木";
				break;
			case 5:
				$weekday_name = "金";
				break;
			case 6:
				$weekday_name = "土";
				break;
		}

		return $weekday_name;
	}

	/**
	 * yyyyMMdd日付をタイムスタンプに変換する。時刻は0:0:0
	 * @param date 年月日
	 */
	function to_timestamp_from_ymd($date) {
		return date_utils::to_timestamp_from_hhmm("0000", $date);
	}

	/**
	 * 年月日時分をタイムスタンプに変換
	 * @param date 年月日(yyyymmddhhmm または yyyymmddhh:mm)指定がない場合は本日
	 *                                                ＤＢに保持する時間のフォーマットがhhmmとhh:mmの２パターンあったため
	 */
	function to_timestamp_from_ymdhi($date_time) {
		$result_timestamp = null;

		//ymdHi以外は処理しない
		if (date_utils::is_ymdhi($date_time)){
			$date   = substr($date_time, 0, 8);
			$time   = substr($date_time, 8);
			$result_timestamp = date_utils::to_timestamp_from_hhmm($time, $date);
		}

		return $result_timestamp;
	}

	/**
	 * hhmm日付をタイムスタンプに変換
	 * @param time 時分 hh:mm または hhmm
	 * @param date 年月日(yyyymmdd)指定がない場合は本日
	 */
	function to_timestamp_from_hhmm($time, $date) {
		$result_timestamp = null;

		//時間にコロン(:)がある場合消す
		$time = str_replace(":", "", $time);
		if (strlen($date) == 8 && strlen($time) == 4){
			$year   = substr($date, 0, 4);
			$month  = substr($date, 4, 2);
			$day    = substr($date, 6, 2);
			$hour   = substr($time, 0, 2);
			$minute = substr($time, 2, 2);
			$second = 0;

			//引数の日付が正常な日付のときのみ日付を指定して返す
			if (checkdate($month, $day, $year)){
				$result_timestamp = mktime($hour, $minute, $second, $month, $day, $year);
			}
			else{
				$result_timestamp = mktime($hour, $minute);
			}
		}

		return $result_timestamp;
	}

	/**
	 * 日付の比較を行う第一引数が大きい場合差を＋の数値で返す（分単位）<br>
	 * 　　　　　　　　第一引数が小さい場合差を−の数値で返す（分単位）<br>
	 * 　　　　　　　　第一引数・第二引数が同じ場合０<br>
	 * 　　　　　　　　第一引数・第二引数に異常時も０<br>
	 * @param date1 yyyymmddhhmm 年月日
	 * @param date2 yyyymmddhhmm 年月日
	 */
	function get_diff_minute($date_time1, $date_time2) {
		$result_diff = 0;
		if (date_utils::is_ymdhi($date_time1) && date_utils::is_ymdhi($date_time2)){
			$result_diff = date_utils::ymdhi_to_minute($date_time1) - date_utils::ymdhi_to_minute($date_time2);
		}
		return $result_diff;
	}

	/**
	 * 時間表記hhmm(時刻ではない)を分に変換
	 * @param $time hhmmかhh:mm(時刻ではない) ※「:」で区切られている場合は2桁以外にも対応 20130228
	 */
	function hi_to_minute($time){

		$reslt_minute = 0;

		//時間にコロン(:)がない場合
        if (strpos($time, ":") === false) {
            
            if (strlen($time) == 4){
                $hour   = substr($time, 0, 2);
                $minute = substr($time, 2, 2);
                $reslt_minute = $minute + ($hour * 60);
            }
        }
        //時間にコロン(:)がある場合、分割後に計算
        else {
            $time_hm = explode(":", $time);
            $reslt_minute = $time_hm[0] * 60 + $time_hm[1];            
        }
		return $reslt_minute;
	}

	/**
	 * yyyymmddhhmmを分に変換。余りは切り捨て
	 * @param $date_time
	 */
	function ymdhi_to_minute($date_time){
		$work_timestamp = date_utils::to_timestamp_from_ymdhi($date_time);
		return date_utils::timestamp_to_minute($work_timestamp);
	}

	/**
	 * タイムスタンプを分に変換。余りは切り捨て
	 * @param timestamp 
	 */
	function timestamp_to_minute($timestamp) {

		$minute = null;

		if (is_numeric($timestamp)){
			$minute = floor ($timestamp / 60);
			$hour   = floor ($minute / 60);
		}

		return $minute;
	}

	/**
	 * 分をタイムスタンプに変換
	 * (秒にしてるだけ)
	 * @param $minute
	 */
	function minute_to_timestamp($minute) {

		$timestamp = null;

		if (is_numeric($minute)){
			$timestamp = $minute * 60;
		}

		return $timestamp;
	}

	/**
	 * 繰上げ・繰り下げ処理
	 * @param $base_date_time   基準の日時これを軸に繰り上げ・繰り下げを行う yyyymmddhhmm または、yyyymmddhh:mm
	 * @param $date_time        対象の日時　yyyymmddhhmm または、yyyymmddhh:mm
	 * @param $moving_minutes   繰り上げ・下げ時間（分単位）
	 * @param $moving_type      繰り下げ:3 繰り上げ:2 
	 *
	 */
	function move_time($base_date_time, $date_time, $moving_minutes, $moving_type) {

		$result_date_time = $date_time;
		$move_up_flag     = ($moving_type == "3");
		$move_down_flag   = ($moving_type == "2");

		//引数が異常な場合、繰り上げ・下げを行わない
		if (empty($base_date_time) || empty($date_time) || empty($moving_minutes) || $move_up_flag == $move_down_flag){
			return $result_date_time;
		}

		//yyyymmddhhmm → timestamp → 分に変換
		$base_timestamp     = date_utils::to_timestamp_from_ymdhi($base_date_time);
		$taget_timestamp    = date_utils::to_timestamp_from_ymdhi($date_time);
		$base_minutes       = date_utils::timestamp_to_minute($base_timestamp);
		$taget_minutes      = date_utils::timestamp_to_minute($taget_timestamp);

		// 基準時刻と実績時刻が同じなら入力値をそのまま返す
		// 繰り下げ・繰り上げ指定
		if ($base_minutes < $taget_minutes) {

			$work_minutes = $base_minutes;
			while (true) {
				$work_minutes += $moving_minutes;
				if ($work_minutes >= $taget_minutes && $move_down_flag) {
					break;
				}
				if ($work_minutes > $taget_minutes) {
					break;
				}
			}

			//繰り上げ
			if ($move_up_flag){
				$work_minutes -= $moving_minutes;
			}

		}
		else if ($base_minutes > $taget_minutes) {
			$work_minutes = $base_minutes;
			while (true) {
				$work_minutes -= $moving_minutes;
				if ($work_minutes <= $taget_minutes && $move_up_flag) {
					break;
				}
				if ($work_minutes < $taget_minutes) {
					break;
				}
			}

			//繰り下げ
			if ($move_down_flag){
				$work_minutes += $moving_minutes;
			}
		}

		//分 → timestamp →yyyymmddhhmm に変換
		if ($base_minutes != $taget_minutes) {
			$result_timestamp   = date_utils::minute_to_timestamp($work_minutes);
			$result_date_time   = date('YmdHi', $result_timestamp);
		}

		return $result_date_time;
	}

	/**
	 * yyyymmddhhmm に対して日数の追加を行う
	 * 引き算をしたいときはマイナス値を渡すと
	 * @param add_day 追加する日数
	 */
	function add_day_ymdhi($date_time, $add_day) {

		$work_timestamp = date_utils::to_timestamp_from_ymdhi($date_time);
		return date("YmdHi", strtotime($add_day." day", $work_timestamp));

	}

	/**
	 * ymdhi に対して分の追加を行う
	 * 引き算をしたいときはマイナス値を渡すと
	 * @param timestamp 
	 */
	function add_minute_ymdhi($date_time, $minutes) {

		$work_timestamp = date_utils::to_timestamp_from_ymdhi($date_time);
		return date("YmdHi", strtotime($minutes." minute", $work_timestamp));

	}

	/**
	 * 第一引数の時間と第二引数の時間の差を分単位で返す
	 * @param $date_time1 yyyymmddhhmm　または yyyymmddhh:mm
	 * @param $date_time2 yyyymmddhhmm　または yyyymmddhh:mm
	 * @return 時差(分)
	 */
	function get_time_difference($date_time1, $date_time2){

		$result_minute = 0;

		if (date_utils::is_ymdhi($date_time1) && date_utils::is_ymdhi($date_time2)){
			//分に変換
			$minute1 = date_utils::ymdhi_to_minute($date_time1);
			$minute2 = date_utils::ymdhi_to_minute($date_time2);

			if ($minute1 > $minute2){
				$result_minute = $minute1 - $minute2;
			}
			else{
				$result_minute = $minute2 - $minute1;
			}
		}

		return $result_minute;
	}

	/**
	 * ymdhiから時刻部分を取得する
	 * 引数が異常な場合はそのまま返す
	 * @param format		date関数で使えるフォーマット
	 * @return フォーマットされた日時
	 */
	function get_format_ymdhi($date_time, $format) {
		$work_timestamp = date_utils::to_timestamp_from_ymdhi($date_time);
		return date($format, $work_timestamp);
	}


	/**
	 * 引数がただしいymdhiか判定する
	 * @param ymdhi
	 * @retrun 正常;true 異常:false
	 */
	function is_ymdhi($ymdhi){
		$result = false;

		//コロンがある場合消す
		$ymdhi = str_replace(":", "", $ymdhi);

		//長さは１２(yyyyMMddhhmm)
		if (strlen($ymdhi) == 12){
			$year   = substr($ymdhi, 0, 4);
			$month  = substr($ymdhi, 4, 2);
			$day    = substr($ymdhi, 6, 2);
			$hour   = substr($ymdhi, 8, 2);
			$minute = substr($ymdhi, 10, 2);

			//時間が０以上２４未満
			$time_flag = false;
			if ($hour < 24 && $hour > -1){
				//分が０以上６０未満
				if ($minute < 60 && $minute > -1){
					$time_flag = true;
				}
			}

			$result = $time_flag && checkdate($month, $day, $year);
		}

		return $result;
	}

}
?>