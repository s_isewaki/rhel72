<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜家族付き添い詳細</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 患者名を取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 付き添い情報を取得
$sql = "select * from inptfamily";
$cond = "where ptif_id = '$pt_id' and from_date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$from_date = pg_fetch_result($sel, 0, "from_date");
$to_date = pg_fetch_result($sel, 0, "to_date");
$head_count = pg_fetch_result($sel, 0, "head_count");
$comment = pg_fetch_result($sel, 0, "comment");

// 外出日時のデフォルト値を設定
$from_yr = substr($from_date, 0, 4);
$from_mon = substr($from_date, 4, 2);
$from_day = substr($from_date, 6, 2);
$to_yr = substr($to_date, 0, 4);
$to_mon = substr($to_date, 4, 2);
$to_day = substr($to_date, 6, 2);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function updateDetail() {
	document.mainform.action = 'inpatient_family_update_exe.php';
	document.mainform.submit();
}

function deleteDetail() {
	if (!confirm('削除します。よろしいですか？')) {
		return;
	}

	document.mainform.action = 'inpatient_family_delete.php';
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>家族付き添い詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者ID</font></td>
<td width="180"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($pt_id); ?></font></td>
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者氏名</font></td>
<td width="160"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">開始日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><select name="from_yr"><? show_update_years($from_yr, 1); ?></select>/<select name="from_mon"><? show_select_months($from_mon); ?></select>/<select name="from_day"><? show_select_days($from_day); ?></select></font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">終了日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><select name="to_yr"><? show_select_years_future(2, $to_yr); ?></select>/<select name="to_mon"><? show_select_months($to_mon); ?></select>/<select name="to_day"><? show_select_days($to_day); ?></select></font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">人数</font></td>
<td colspan="3"><select name="head_count">
<option value="1"<? if ($head_count == "1") {echo(" selected");} ?>>1人
<option value="2"<? if ($head_count == "2") {echo(" selected");} ?>>2人
</select></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">コメント</font></td>
<td colspan="3"><textarea name="comment" cols="40" rows="5" style="ime-mode:active;"><? echo($comment); ?></textarea></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="4">
<tr>
<td align="right">
<input type="button" value="更新" onclick="updateDetail();">
<input type="button" value="削除" onclick="deleteDetail();">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
