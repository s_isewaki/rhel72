<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 77, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($linkage_flg == "t" && $linkage_url == "") {
	echo("<script type=\"text/javascript\">alert('患者検索URLを入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 登録値の編集
if ($linkage_flg != "t") {
	$linkage_url = null;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// データ連携URLをDELETE〜INSERT
$sql = "delete from ptadm";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == "0") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "insert into ptadm (linkage_url) values (";
$content = array($linkage_url);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == "0") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// データ連携設定画面を再表示
echo("<script type=\"text/javascript\">location.href = 'patient_admin_menu.php?session=$session';</script>");
?>
