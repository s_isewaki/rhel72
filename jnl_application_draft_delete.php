<?

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("jnl_application_workflow_common_class.php");

$fname=$PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 80, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}



// データベースに接続
$con = connect2db($fname);

$obj = new jnl_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");


// 申請論理削除
$obj->update_delflg_all_apply($apply_id, "t");


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 添付ファイルの移動
foreach (glob("jnl_apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

foreach (glob("jnl_apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href='jnl_application_menu.php?session=$session';</script>");

?>