<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($name == "") {
	echo("<script type=\"text/javascript\">alert('管理項目名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($name) > 80) {
	echo("<script type=\"text/javascript\">alert('管理項目名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if ($total_flg == "f") {
	$total_start_month = null;
	$total_start_day = null;
}

// データベースへ接続
$con = connect2db($fname);

// 管理項目IDを採番
$sql = "select max(statitem_id) from statitem";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 管理項目名を登録
$sql = "insert into statitem (statitem_id, name, total_flg, total_start_month, total_start_day) values (";
$content = array($id, $name, $total_flg, $total_start_month, $total_start_day);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 管理項目一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'stat_master_item_list.php?session=$session'</script>");
?>
