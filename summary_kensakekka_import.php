<?
ob_start();
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
//require_once("./about_error.php");
ob_end_clean();
$kensakekka_dir = $argv[1] ; // 検査結果データファイル格納ディレクトリ、バッチ処理でないときは $kensakekka_dir は null
if ($kensakekka_dir)
{
  //====================================================================
  // 検査結果datデータ自動収集
  //====================================================================
  echo("Kensakekka dat Auto Import [START]\n");
  $fname        = $argv[0] ; // プログラム名
  $dat_save_dir = $argv[2] ; // 処理用ディレクトリ
  $ext          = $argv[3] ; // 拡張子
  // ディレクトリの存在を確認
  if ( !is_dir($kensakekka_dir) ) {
    echo "kensakekka_dir is not found: $kensakekka_dir\n" ;
    exit;
  }
  if ( !is_dir($dat_save_dir) ) {
    echo "dat_save_dir is not found: $dat_save_dir\n" ;
    exit;
  }
  $con = connect2db($fname);
  $upd = pg_exec($con, "truncate table sum_kensakekka_import_log");
  // データ取り込みループ
  $files = glob("$kensakekka_dir/*.$ext");
  foreach( $files as $file ) {
    echo basename($file). "\n";
    // ファイルの移動
    $move_file = "$dat_save_dir/".date('Ymd_His_').basename($file) ;
    rename($file, $move_file);
    echo "  file move: $kensakekka_dir --> $dat_save_dir\n";
    // データ取り込み実行
    $res = kensakekkaImport($con, "sjis-win", $move_file) ;
    if ( $res ) {
      echo "  dat import: OK\n";
    } else {
      echo "  dat import: NG\n";
    }
  }
  echo("Kensakekka dat Auto Import [END]\n");
  exit;
}

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$fname = $_SERVER["PHP_SELF"];
$con = @connect2db($fname);
if ($_REQUEST["give_me"] != "current_status"){
  if(!$session || !$fplusadm_auth || !$con){
    echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    exit;
  }
}
?>
<?
//****************************************************************************
// 画面描画
//****************************************************************************
if ((@$_REQUEST["mode"]!="import") && (@$_REQUEST["give_me"]!="current_status"))
{
  $font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
  // 診療科権限を取得
  $entity_admin_auth = check_authority($session, 28, $fname);
  // 組織タイプを取得
  $profile_type = get_profile_type($con, $fname);
  // 診療区分/記録区分
  $summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
  // メドレポート/ＰＯレポート(Problem Oriented)
  $med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
  // 診療科/担当部門
  $section_title = $_label_by_profile["SECTION"][$profile_type];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><? echo($med_report_title); ?>管理 | 検査結果</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list        { border-collapse:collapse; }
.list td     { border:#5279a5 solid 1px; text-align:center }
.code_tbl    { border-collapse:collapse;border:#1c251c solid 1px;background-color:#c9ffc9;font-size:10px;color:#1c251c; }
.code_tbl td { border:black solid 1px;padding:0px 2px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_KENSAKEKKA"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>

<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>

<td valign="top">
<?//****************************************************************************?>
<?// 検査結果メニュー                                                           ?>
<?//****************************************************************************?>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#5279a5"><a href="./summary_kensakekka_import.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>インポート</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="./summary_kensakekka_sansyo.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検査結果参照</font></a></td>
<td align="right"><!--input type="button" value="削除" onclick="deleteStatus();"--></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>

<? //**************************************************************************** ?>
<? // インポート状況のリアルタイムチェック                                        ?>
<? //**************************************************************************** ?>
<script type="text/javascript">
  var ajaxObj = null;
  var currentMessage = "処理を開始しています";

  function startImport(){
    document.frm.btn_import.disabled = "disabled";
    document.getElementById('result_div').style.display='none';
    document.getElementById("status_div").style.display = '';
    document.frm.submit();
//    inquiryImportCheck();
//    dotCounter(5);
  }

  function inquiryImportCheck(){
    if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
    else {
      try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
      catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
    }
    ajaxObj.onreadystatechange = returnImportCheck;
    ajaxObj.open("POST", "summary_kensakekka_import.php", true);
    ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajaxObj.send("session=<?=$session?>&give_me=current_status");
  }

  function returnImportCheck(){
    if (ajaxObj == null) return;
    if (ajaxObj.readyState != 4) return;
    if (typeof(ajaxObj.status) != "number") return;
    if (ajaxObj.status != 200) return;
    if (!ajaxObj.responseText) return;
    try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
    catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
    if (!ret) return;
    currentMessage = ret.msg;
    document.getElementById("current_status_div").style.display = '';
    ajaxObj = null;
    setTimeout("inquiryImportCheck()", 3000);
  }

  function dotCounter(cnt){
    var dots = "";
    for(var i = 0; i < cnt; i++) dots += "...";
    cnt++;
    if (cnt > 10) cnt = 1;
    setTimeout("dotCounter("+cnt+")", 600);
    document.getElementById("current_status_div").innerHTML = currentMessage + dots;
  }

</script>

<form action="summary_kensakekka_import.php" method="post" name="frm" style="margin-top:4px" enctype="multipart/form-data" target="import_frame">
 <input type="hidden" name="session" value="<?=$session ?>">
 <input type="hidden" name="mode" value="import">

 <table width="100%" border="0" cellspacing="0" cellpadding="3" class="list">
  <tr>
   <td width="25%">固定長ファイル</td>
   <td style="text-align:left;">
    <input type="file" name="upfile" size="100" style="height:21px;">
   </td>
  </tr>
  <tr>
   <td>ファイルの文字コード</td>
   <td style="text-align:left;">
    <label><input type="radio" name="file_enc" value="sjis-win" checked />Shift_JIS (Windows/Mac標準)</label>
    <label><input type="radio" name="file_enc" value="eucJP-win"        />EUC-JP</label>
<!--    <label><input type="radio" name="file_enc" value="UTF-8"            />UTF-8</label> -->
   </td>
  </tr>

 </table>
 <div style="margin:4px;text-align:right;">
  <input type="button" name="btn_import" value=" 実行 " onclick="startImport();" />
 </div>
</form>


<?=$font?>
  <div id="status_div" style="margin-top:20px; display:none; padding:8px;">完了まで、このまましばらくお待ちください。</div>
  <div id="current_status_div" style="margin:4px; background-color:#fcffbf; padding:8px; display:none"></div>
</font>


<? //**************************************************************************** ?>
<? // 完了通知                                                                    ?>
<? //**************************************************************************** ?>
<?
  $errmsg = array();
  $counts = array();
  if ($_REQUEST["mode"]=="finished_import"){
    $sel = select_from_table($con, "select kubun, message from sum_kensakekka_import_log order by seq", "", $fname);
    while($row = pg_fetch_array($sel)) {
      $kubun = $row["kubun"];
      if ($kubun != "err_msg") $counts[$kubun] = $row["message"];
      else $errmsg[] = $row["message"];
    }
  }
?>

<?=$font?>
<div id="result_div">

  <? if ($_REQUEST["mode"]=="finished_import"){ ?>
  <div style="margin:4px; margin-top:20px; background-color:#fcffbf; padding:8px"><b>処理が完了しました。</b></div>
  <? } ?>

  <? if (@$counts["row_count"]){ ?>
  <div style="margin:4px; margin-top:10px">--- <?=max(0, ((int)$counts["row_count"]))?>行の処理を行いました。</div>
  <? } ?>

  <? if (@$counts["reg_count"]) { ?>
  <div style="margin:4px; margin-top:10px">--- <?=$counts["reg_count"]?>件の検査結果が新規登録されました。</div>
  <? } ?>

  <? if (@$counts["upd_count"]) { ?>
  <div style="margin:4px; margin-top:10px">--- <?=$counts["upd_count"]?>件の検査結果の更新が行われました。</div>
  <? } ?>

  <? if (@$counts["del_count"]) { ?>
  <div style="margin:4px; margin-top:10px">--- <?=$counts["del_count"]?>件の検査結果の削除が行われました。</div>
  <? } ?>

  <? $errlen = (int)@$counts["err_count"]; ?>
  <? if ($errlen) { ?>
  <div style="margin:4px; background-color:#fdccee;">
    <div style="padding:4px; background-color:#8b0563; color:#fff"><b><?=$errlen==999999 ? "100件を超えるエラーが発生しました。" : $errlen."件のエラーが発生しました。"?></b></div>
    <div style="margin:4px; padding:8px">
    <? foreach($errmsg as $msg) echo $msg."<br/>\n"; ?>
    </div>
  </div>
  <? } ?>

</font>
</div>

<div style="margin:10px;"></div>
<span style="background-color:#e3ecf4;"><b>インポート用検査結果固定長レイアウト</b></span>
<div style="margin:9px;"></div>
<table>
 <tr>
  <td style="vertical-align:top">
    <table class="code_tbl" border="1">
     <tr><td rowspan="6"><b>レコード区分</b></td>
         <td>1 ヘッダー</td></tr>
     <tr><td>2 患者</td></tr>
     <tr><td>3 検体</td></tr>
     <tr><td>4 コメント1</td></tr>
     <tr><td>5 コメント2</td></tr>
     <tr><td>6 項目</td></tr>
    </table>
  </td>
  <td style="vertical-align:top">
    <table class="code_tbl" border="1">
     <tr><td rowspan="2"><b>更新区分</b></td>
         <td>1 検体置換</td></tr>
     <tr><td>9 検体削除</td></tr>
    </table>
  </td>
  <td style="vertical-align:top">
    <table class="code_tbl" border="1">
     <tr><td rowspan="3"><b>報告区分</b></td>
         <td>0 途中</td></tr>
     <tr><td>1 最終</td></tr>
     <tr><td>　不明</td></tr>
    </table>
  </td>
  <td style="vertical-align:top">
    <table class="code_tbl" border="1">
     <tr><td rowspan="3"><b>性別</b></td>
         <td>M 男</td></tr>
     <tr><td>F 女</td></tr>
     <tr><td>　不明</td></tr>
    </table>
  </td>
  <td style="vertical-align:top">
    <table class="code_tbl" border="1">
     <tr><td rowspan="3"><b>入院外来</b></td>
         <td>1 外来</td></tr>
     <tr><td>2 入院</td></tr>
     <tr><td>　なし</td></tr>
    </table>
  </td>
 </tr>
</table>
<div style="margin:6px;"></div>
<img src="img/kensakekka_record_layout.png" alt="レコードレイアウト" width="1103" height="1602">

</td>
</tr>
</table>
<iframe name="import_frame" style="display:none"></iframe>
</body>
</html>
<?
  pg_close($con);
  die ;
}
?>

<?
//****************************************************************************
// インポート開始
//****************************************************************************
if ($_REQUEST["mode"]=="import")
{
  $fileEnc = $_REQUEST["file_enc"] ;
  // データ取り込み実行
  $res = kensakekkaImport($con, $fileEnc, null);
  //==============================
  // 画面遷移用のHTMLを出力して終了。
  //==============================
?>
<html>
  <head>
    <script type="text/javascript">
      function loaded() { window.parent.location.href = "summary_kensakekka_import.php?session=<?=$session?>&mode=finished_import"; }
    </script>
  </head>
  <body onload="loaded();"></body>
</html>
<?
  die;
}
?>
<?
////////////////////////////////////////////////////////////////////////////////////////////////////

// 指定された検体共通番号に対応した共通レコード部配列を作成する(すでに作成済みのときは何もしない)
function makeCommArray($kentai_no, &$commArrays, $dataSizeAll)
{
  $commArray = @$commArrays[$kentai_no] ;
  if (!$commArray)
  {
    $commArray = array() ;
    foreach ($dataSizeAll as $idx => $dataSize)
    {
      if ($idx < 6) // 項目レコードは除外する
      {
        foreach ($dataSize as $key => $size)
        {
          $commArray[$key] = "" ;
        }
      }
    }
    $commArrays[$kentai_no] = $commArray ;
  }
}

// 共通レコード取得処理 (検体共通番号をキーとする共通レコード部配列に格納する(以下同様))
function getCommRecord($fp, $record_kbn_no, $kentai_no, $dataSize, &$commArrays, $readEnc, $fileEnc)
{
  // 最後に可変長データを持つレコードは、全体のレコード長を算出しておく(改行コードより前までのサイズ)
  $recSize = 0 ;
  if ($record_kbn_no >= 4)
  {
    $curpt = ftell($fp) ; // 現在のファイルポインタ位置を保存する
    while (($chr = fgetc($fp)) !== false)
    {
      $ccd = ord($chr) ;
      if (($ccd == 13) || ($ccd == 10)) break ;  // "\r"=13, "\n"=10
      $recSize++ ;
    }
    fseek($fp, $curpt, SEEK_SET) ; // ファイルポインタ位置を元に戻す
  }
  // データを取得して、共通レコード部配列に格納する
  $incSize = 0 ;
  foreach ($dataSize as $key => $size)
  {
    if ($size == 0) $size = $recSize - $incSize ; // 可変長データのとき
    $data = fread($fp, $size) ;
    $data = mb_convert_encoding($data, $readEnc, $fileEnc) ;
    $commArrays[$kentai_no][$key] = $data ; 
    $incSize += $size ;
  }
}

// 項目レコード部取得
function getItemRecord($fp, $kentai_no, $dataSize, &$commArrays, &$dataArrays, $readEnc, $fileEnc)
{
  // 最後に可変長データを持つレコードなので、全体のレコード長を算出しておく(改行コードより前までのサイズ)
  $recSize = 0 ;
  $curpt = ftell($fp) ; // 現在のファイルポインタ位置を保存する
  while (($chr = fgetc($fp)) !== false)
  {
    $ccd = ord($chr) ;
    if (($ccd == 13) || ($ccd == 10)) break ;  // "\r"=13, "\n"=10
    $recSize++ ;
  }
  fseek($fp, $curpt, SEEK_SET) ; // ファイルポインタ位置を元に戻す

  // データを取得して、項目レコード部配列に格納する
  $itemArray = array() ;
  $incSize = 0 ;
  foreach ($dataSize as $key => $size)
  {
    if ($size == 0) $size = $recSize - $incSize ; // 可変長データのとき
    $data = fread($fp, $size) ;
    $data = mb_convert_encoding($data, $readEnc, $fileEnc) ;
    $itemArray[$key] = $data ;
    $incSize += $size ;
  }

  // 項目レコード存在フラグを立てる
  $commArrays[$kentai_no]["itm_flg"] = true ;
  // 全データ格納配列に追加する
  $dataArrays[] = $commArrays[$kentai_no] + $itemArray ;
}


//****************************************************************************
// インポート進捗の問い合わせ
//****************************************************************************
if ($_REQUEST["give_me"] == "current_status"){
  $sel = select_from_table($con, "select kensakekka_upload_message from summary_option", "", $fname);
  echo '{"msg":"'.pg_fetch_result($sel,0,0).'"}';
  die;
}
?>
<?
function kensakekkaImport($con, $fileEnc, $targetFile)
{
  // データサイズ配列
  $dataSizeAll = array(
      // 項目レコード存在フラグ
      0=>array(
        "itm_flg"=>false // 存在するときはtrue
        ),
      // ヘッダレコード
      1=>array(
        "hdr_record_kbn"  =>1, // レコード区分
        "hdr_kentai_no"   =>5, // 検体共通番号
        "hdr_update_kbn"  =>1, // 更新区分
        "hdr_center_cd"   =>2, // センターコード
        "hdr_kentai_syu"  =>1, // 検体種別
        "hdr_uketuke_date"=>8, // 受付日
        "hdr_uketuke_no"  =>6, // 受付番号
        "hdr_houkoku_date"=>8, // 報告日
        "hdr_houkoku_kbn" =>1, // 報告区分
        "hdr_uketuke_cnt" =>3, // 受付項目数
        "hdr_houkoku_cnt" =>3  // 報告項目数
        ),
      // 患者レコード
      2=>array(
        "ptr_record_kbn" => 1, // レコード区分
        "ptr_kentai_no"  => 5, // 検体共通番号
        "ptr_name_kana"  =>20, // カナ氏名
        "ptr_name_kanji" =>24, // 漢字氏名
        "ptr_karte_no"   =>10, // カルテ番号
        "ptr_birth_era"  => 1, // 生年月日−年号
        "ptr_birth_yy"   => 2, // 生年月日−年
        "ptr_birth_mm"   => 2, // 生年月日−月
        "ptr_birth_dd"   => 2, // 生年月日−日
        "ptr_age_yyy"    => 3, // 年齢−才
        "ptr_age_mm"     => 2, // 年齢−月
        "ptr_sex"        => 1, // 性別
        "ptr_blood_type" => 3, // 血液型
        "ptr_ninpu_info" => 6, // 妊婦非妊婦
        "ptr_ninsin_week"=> 2, // 妊娠週
        "ptr_hoken_syu"  =>12, // 保険種別
        "ptr_kazoku_kbn" => 4  // 本人家族区分
        ),
      // 検体レコード
      3=>array(
        "knr_record_kbn"  => 1, // レコード区分
        "knr_kentai_no"   => 5, // 検体共通番号
        "knr_sinryou_sect"=>12, // 診療科
        "knr_nyugai_kbn"  => 1, // 入院外来
        "knr_byoutou_1"   =>10, // 病棟1
        "knr_byoutou_2"   => 6, // 病棟2
        "knr_tantou_doc"  =>10, // 担当医
        "knr_jutu_zengo"  => 1, // 術前後
        "knr_saiketu_date"=> 8, // 採血−採血日
        "knr_saiketu_time"=> 4, // 採血−採血時間
        "knr_nyou_ryou"   => 4, // 尿量
        "knr_zairyou_1"   =>14, // 材料−材料1
        "knr_zairyou_2"   =>14, // 材料−材料2
        "knr_zairyou_3"   =>14, // 材料−材料3
        "knr_zairyou_4"   =>14, // 材料−材料4
        "knr_others"      =>10, // その他
        "knr_free"        =>40, // フリー
        "knr_tantou_cd"   => 3, // 担当者コード
        "knr_hospital_cd" => 5, // 病院コード
        "knr_hoken_pt_1"  => 4, // 保険点数−保険点数1
        "knr_hoken_pt_2"  => 4, // 保険点数−保険点数2
        "knr_hoken_pt_3"  => 4, // 保険点数−保険点数3
        "knr_hoken_pt_4"  => 4, // 保険点数−保険点数4
        "knr_hoken_pt_5"  => 4, // 保険点数−保険点数5
        "knr_hoken_pt_kei"=> 4  // 保険点数−保険点数合計
        ), 
      // 検体コメントレコード1
      4=>array(
        "cmt1_record_kbn"=> 1, // レコード区分
        "cmt1_kentai_no" => 5, // 検体共通番号
        "cmt1_irai_cmt_1"=>18, // 依頼コメント1
        "cmt1_irai_cmt_2"=> 0  // 依頼コメント2 (0は可変長データ)
        ),                                                 
      // 検体コメントレコード2
      5=>array(
        "cmt2_record_kbn"  => 1, // レコード区分
        "cmt2_kentai_no"   => 5, // 検体共通番号
        "cmt2_kentai_cmt_1"=>18, // 検体コメント1
        "cmt2_kentai_cmt_2"=> 0  // 検体コメント2 (0は可変長データ)
        ),                                                 
      // 項目レコード
      6=>array(
        "itm_record_kbn"      => 1, // レコード区分
        "itm_kentai_no"       => 5, // 検体共通番号
        "itm_item_no"         => 5, // 項目番号
        "itm_item_sno"        => 3, // 枝番
        "itm_sort_no"         => 7, // 並び順−親
        "itm_sort_sno"        => 6, // 並び順−子
        "itm_kekka_kbn"       => 1, // 結果区分
        "itm_ijouti_kbn"      => 1, // 異常値区分
        "itm_kensa_kekka"     =>40, // 検査結果
        "itm_kekka_jouge_kbn" => 1, // 結果上下区分
        "itm_item_name"       =>24, // 項目名称
        "itm_item_sname"      =>20, // 枝番名称
        "itm_seijou_low_m"    => 8, // 男性−正常範囲低
        "itm_seijou_high_m"   => 8, // 男性−正常範囲高
        "itm_seijou_low_f"    => 8, // 女性−正常範囲低
        "itm_seijou_high_f"   => 8, // 女性−正常範囲高
        "itm_seijou_jouge_kbn"=> 1, // 正常上下区分
        "itm_tani"            =>10, // 単位
        "itm_ryouritu_kbn"    => 2, // 料率区分
        "itm_hoken_pt"        => 4, // 保険点数
        "itm_kekka_cmt_1"     =>12, // 結果コメント1
        "itm_kekka_cmt_2"     => 0  // 結果コメント2 (0は可変長データ)
        )          
      ) ;
  
  $dataArrays = array() ; // インポートする全データが格納される配列
  $commArrays = array() ; // 検体共通番号と対応した共通レコード部配列の配列
  $errmsg     = array() ;
  
  $readEnc = "eucJP-win" ; // "eucJP-win", "EUC-JP", "sjis-win", "UTF-8"
  
  for (;;) // forループ:START
  {
    $upfile_name ;
    if ($targetFile)
    {
      $upfile_name = $targetFile ;
    }
    else
    {
      $upd = pg_exec($con, "truncate table sum_kensakekka_import_log");
      if (!@$_FILES['upfile']['name']){
        $errmsg[] = "ファイルを指定してください。";
        break;
      }
      if (@$_FILES['upfile']['error']){
        $errmsg[] = "アップロードエラーが発生しました。権限またはファイルサイズ容量がサーバ設定されていないか可能性があります。システム管理者にお問い合わせください。";
        break;
      }
      if (!@$_FILES['upfile']['size']){
        $errmsg[] = "ファイルサイズが確認できません。正しいファイルを指定してください。";
        break;
      }
      $upfile_name = $_FILES["upfile"]["tmp_name"];
    }

    if (($fp = fopen($upfile_name, "rb")) == FALSE){
//      $errmsg[] = "ファイルの読込みに失敗しました。";
      $errmsg[] = "ファイルの読込みに失敗しました。(".$upfile_name.")";
      break;
    }
    
    //    $file_conts = file_get_contents($upfile_name) ;
    //    if ($fileEnc != $readEnc) $file_conts = mb_convert_encoding($file_conts, $readEnc, $fileEnc) ;
    //    $fp = fopen("php://memory", "r+") ;
    //    fwrite($fp, $file_conts) ;
    //    rewind($fp) ;
    
    // 検査結果データファイル全体をバイナリ読み込みする
    $rowcnt = 0 ;
    while (!feof($fp)) // whileループ:START
    {
      // レコード区分を取得する
      $record_kbn = fread($fp, 1) ;
      if ($record_kbn === false) { $errmsg[] = "ファイルの読み込みに失敗しました。" ; break ; }
      $record_kbn_no = (int)$record_kbn ; // 数値に変換できないときは0
      if (($record_kbn_no < 1) || (6 < $record_kbn_no)) continue ; // 正しいレコード区分が取得できなかったとき(文字や改行コードなど)
      $rowcnt++;
      $upd = pg_exec($con, "update summary_option set kensakekka_upload_message = '".$rowcnt."行目を処理中 '");
      // 検体共通番号を取得する
      $kentai_no  = fread($fp, 5) ;
      fseek($fp, -(1+5), SEEK_CUR) ; // ファイルポインタを現在位置から(1+5)戻す(レコード区分と検体共通番号を取得したため)
      // このレコード区分に対応するデータサイズ配列を取得する
      $dataSize = $dataSizeAll[$record_kbn_no] ;
      // この検体共通番号に対応した共通レコード部配列を作成する(すでに作成済みのときは何もしない)
      makeCommArray($kentai_no, $commArrays, $dataSizeAll) ;
      // データ取得処理を実行する
      if ($record_kbn_no < 6) getCommRecord($fp, $record_kbn_no, $kentai_no, $dataSize, $commArrays,              $readEnc, $fileEnc) ;
      else                    getItemRecord($fp,                 $kentai_no, $dataSize, $commArrays, $dataArrays, $readEnc, $fileEnc) ;
    } // whileループ:END
    
    if (count($errmsg) > 0) break ;
    
    // 項目レコードがない検体共通番号のデータも全データ格納配列に追加する(項目レコード取得処理の中で追加処理を行っているため)
    foreach ($commArrays as $kentai_no => $commArray)
    {
      if (!$commArray["itm_flg"]) $dataArrays[] = $commArray ;
    }
    
    // 取得したデータでテーブルを更新する
    $shoris = array("reg"=>"新規登録", "upd"=>"更新", "del"=>"削除") ;
    $regcnt = 0 ;
    $updcnt = 0 ;
    $delcnt = 0 ;
    foreach ($dataArrays as $idx => $rec)  // foreachループ:START // この一つ一つの$recが1レコードに対応する
    {
      $shori = "" ;
      $sql   = "" ;
      
      $pt_id        = trim($rec["ptr_karte_no"    ]) ; // カルテ番号(=患者ID)
      $uketuke_date =      $rec["hdr_uketuke_date"]  ; // 受付日
      $uketuke_no   =      $rec["hdr_uketuke_no"  ]  ; // 受付番号
      $item_no      =     @$rec["itm_item_no"     ]  ; // 項目番号
      $item_sno     =     @$rec["itm_item_sno"    ]  ; // 枝番
      
      // カルテ番号(=患者ID)の頭0は削除する
      $pt_id_no = @(int)$pt_id ;
      $pt_id_str = ($pt_id_no) ? (string)$pt_id_no : $pt_id ;

      // 項目レコードが存在するときは、項目番号と枝番も条件に加える
      $item_no_cond  = ($item_no ) ? " and itm_item_no  = '" . pg_escape_string($item_no)  . "'" : "" ; 
      $item_sno_cond = ($item_sno) ? " and itm_item_sno = '" . pg_escape_string($item_sno) . "'" : "" ; 
      $cnds = 
        " where" .
        "       hdr_uketuke_date = '" . pg_escape_string($uketuke_date) . "'" . 
        "   and hdr_uketuke_no   = '" . pg_escape_string($uketuke_no)   . "'" . 
        $item_no_cond .
        $item_sno_cond ;
      
      // 削除のとき
      if ($rec["hdr_update_kbn"] == "9") // 更新区分
      {
        $shori = "del" ;
        $sql =
          " update" .
          "   sum_kensakekka" .
          " set" .
          "   hdr_update_kbn = '9'" .
          "  ,update_timestamp = CURRENT_TIMESTAMP" .
          $cnds ;
      }
      else
      {
        $shori = "reg" ;
        $cols  = "" ;
        $vals  = "" ;
        foreach ($rec as $col => $val)
        {
          if ($col == "itm_flg") continue ; // 項目レコード存在フラグはとばす
          $cols .= ($col . ",") ;
          $vals .= ("'" . pg_escape_string($val) . "',") ;
        }  
        $cols .= "ptif_id" ;
        $vals .= ("'" . pg_escape_string($pt_id_str) . "'") ;
        $sql = "insert into sum_kensakekka (" . $cols . ") values (" . $vals . ")" ;
      }
      
      // @@@ テーブルに対する処理を実行する @@@
      $upd = @pg_exec($con, $sql) ;
      $rws = @pg_affected_rows($upd) ;
      
      $hasErr  = false ;
      $lastErr = "" ;
      // 処理に失敗したとき
      if (!$upd)
      {
        $hasErr = true ;
        $lastErr = pg_last_error($con) ;
        // 新規登録時の主キー重複で失敗したとき
        if (($shori == "reg") && (strpos($lastErr, "duplicate") > -1))
        {
          // 更新処理を行う
          $shori = "upd" ;
          $sets  = " " ;
          foreach ($rec as $col => $val)
          {
            if ($col == "itm_flg") continue ; // 項目レコード存在フラグはとばす
            $sets .= ($col . " = '" . pg_escape_string($val) . "',") ;
          }  
          $sets .= "ptif_id = '" . pg_escape_string($pt_id_str) . "'," ;
          
          $sets .= "update_timestamp = CURRENT_TIMESTAMP" ;
          $sql =
            " update" .
            "   sum_kensakekka" .
            " set" .
            $sets .
            $cnds ;
          $upd = @pg_exec($con, $sql) ;
          $rws = @pg_affected_rows($upd) ;
          if (!$upd) { $lastErr = pg_last_error($con) ;    }
          else       { $hasErr = false ; $updcnt += $rws ; }
        }
        // 最終的にエラーとなったとき
        if ($hasErr) $errmsg[] = "データの".$shoris[$shori]."に失敗しました。（[受付日]".$uketuke_date."[受付番号]".$uketuke_no."[項目番号]".$item_no."[枝番]".$item_sno."）".$lastErr ;
      }
      // 処理が成功したとき
      else
      {
        if ($shori == "del") $delcnt += $rws ;
        if ($shori == "reg") $regcnt += $rws ;
      }
    } // foreachループ:END
    
    break;
  } // forループ:END
  
  //==============================
  // 完了。ログを格納して終了。
  //==============================
  $upd = pg_exec($con, "update summary_option set kensakekka_upload_message = 'しばらくお待ちください。'");
  $seq = 0;
  if ($rowcnt){
    $seq++;
    $upd = pg_exec($con, "insert into sum_kensakekka_import_log (seq, kubun, message) values (".$seq.",'row_count','".$rowcnt."');");
  }
  if ($regcnt){
    $seq++;
    $upd = pg_exec($con, "insert into sum_kensakekka_import_log (seq, kubun, message) values (".$seq.",'reg_count','".$regcnt."');");
  }
  if ($updcnt){
    $seq++;
    $upd = pg_exec($con, "insert into sum_kensakekka_import_log (seq, kubun, message) values (".$seq.",'upd_count','".$updcnt."');");
  }
  if ($delcnt){
    $seq++;
    $upd = pg_exec($con, "insert into sum_kensakekka_import_log (seq, kubun, message) values (".$seq.",'del_count','".$delcnt."');");
  }
  $errcnt = count($errmsg);
  if ($errcnt > 100) $errcnt = 999999;
  if ($errcnt){
    $seq++;
    $upd = pg_exec($con, "insert into sum_kensakekka_import_log (seq, kubun, message) values (".$seq.",'err_count','".$errcnt."');");
    $displen = min($errcnt, 100);
    for ($i = 0; $i < $displen; $i++){
      $seq++;
      $upd = pg_exec($con, "insert into sum_kensakekka_import_log (seq, kubun, message) values (".$seq.",'err_msg','・ ".pg_escape_string($errmsg[$i])."');");
    }
    if ($errcnt > 100){
      $seq++;
      $upd = pg_exec($con, "insert into sum_kensakekka_import_log (seq, kubun, message) values (".$seq.",'err_msg','……………（以下省略）');");
    }
  }

  if ($errcnt > 0) return false ;
  return true ;
}
?>








