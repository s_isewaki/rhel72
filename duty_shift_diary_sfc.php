<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜管理日誌（SFC形式）</title>

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_sfc_class.php");
require_once("date_utils.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_sfc = new duty_shift_diary_sfc_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
	$err_flg_1 = "";

	// 期間
	if ($start_yr == "") {
		$start_yr = date('Y');
	}
	if ($start_mon == "") {
		$start_mon = date('m');
	}

	$arr_title = array(
		"職員ID"//,				//0
		//"氏名"					//1
	);

	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	///-----------------------------------------------------------------------------
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
//	$data_emp = $obj->get_empmst_array("");
	$data_emp = array();	//以下の処理で未使用のため
	$data_wktmgrp = $obj->get_wktmgrp_array();

//連携用病棟コードがあるシフトグループ数
$sfc_code_cnt = 0;
	
///-----------------------------------------------------------------------------
// 勤務シフトグループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
if (count($group_array) <= 0) {
	$err_flg_1 = "1";
} else {

	//連携用病棟コードがあるシフトグループ数
	for ($i=0; $i<count($group_array); $i++) {
		if ($group_array[$i]["sfc_group_code"] != "") {
			$sfc_code_cnt++;
		}
	}		

	///-----------------------------------------------------------------------------
	//勤務シフトグループ情報ＤＢより情報取得
	///-----------------------------------------------------------------------------
	if ($group_id == ""){
		$group_id = $group_array[0]["group_id"];
	}
}

if ($action == "表示") {
	
    $wk_group_array = $obj_sfc->get_group_info($group_id);
    $arr_emp_info = $obj_sfc->get_emp_info_for_sfc($group_id, $start_yr, $start_mon, $wk_group_array);

    $arr_data = $obj_sfc->get_sfc_csv_data($group_id, $start_yr, $start_mon, $arr_emp_info, $wk_group_array, "1");

}
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function downloadCSV() {

	document.csv.action = 'duty_shift_diary_sfc_csv.php';
	document.csv.group_id.value = document.mainform.group_id.value;
	document.csv.duty_yyyy.value = document.mainform.start_yr.value;
	document.csv.duty_mm.value = document.mainform.start_mon.value;
	document.csv.submit();
}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		if ($group_id == "all") {
			$wk_group_id = "";
		} else {
			$wk_group_id = $group_id;
		}
		$arr_option = "&group_id=" . $wk_group_id;
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
<?
if ($err_flg_1 == "1") {
	echo("勤務シフトグループ情報が未設定です。管理画面で登録してください。");
} elseif ($sfc_code_cnt == 0) {
	echo("連携用病棟コードが設定されたシフトグループがありません。");
} else {
?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<table width="960" border="0" cellspacing="0" cellpadding="2" class="">
		<tr>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_bld_manage.php?session=<?=$session?>">病棟管理日誌</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary.php?session=<?=$session?>">勤務データ出力</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_allotment.php?session=<?=$session?>">勤務分担表</a></font></td>
		<td align="left" width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>管理日誌（SFC形式）</b></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_list.php?session=<?=$session?>">在院者一覧</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_duty.php?session=<?=$session?>">当直者一覧</a></font></td>
		<td align="left" width=""></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務シフトグループ名 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
		<!-- 勤務シフトグループ名 -->
		<td width="340" align="left" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ（病棟）名</font>
		<select name="group_id">
		<?
		for($i=0;$i<count($group_array);$i++) {
			//連携病棟コード
			if ($group_array[$i]["sfc_group_code"] == "") {
				continue;
			}
			$wk_id= $group_array[$i]["group_id"];
			$wk_name = $group_array[$i]["group_name"];
			echo("<option value=\"$wk_id\"");
			if ($group_id == $wk_id) {
				echo(" selected");
			}
			echo(">$wk_name\n");
		}
		?>
		</select>&nbsp;</td>
		<td width="200" align="left" nowrap>
		<?
		// 期間
		?>
		<select id='start_yr' name='start_yr'><? 
		show_select_years(15, $start_yr, false);
		?></select>年
		<select id='start_mon' name='start_mon'><? 
		show_select_months($start_mon, false);
		?></select>月
		<!-- 表示ボタン -->
		<input type="submit" name="action" value="表示"></td>
		<td align="right" width="100">
		<input type="button" value="CSV出力" onclick="downloadCSV()">
		</td>
		<td></td>
		</tr>
		</table>
<?
// 表示処理
if ($action == "表示") {
?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="1080" border="0" cellspacing="0" cellpadding="2" class="list">
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（見出し） -->
			<!-- ------------------------------------------------------------------------ -->
			<tr height="22" bgcolor="#f6f9ff">
			<?
		for ($i=0; $i<count($arr_title); $i++) {
			echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
			echo($arr_title[$i]);
			echo("</font></td> \n");
		}
		for ($i=0; $i<31; $i++) {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
			echo($i+1);
			echo("</font></td> \n");
		}
			?>

				</font></td>
			</tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（データ） -->
			<!-- ------------------------------------------------------------------------ -->
			<?
		$cnt=0;
        for ($i=0; $i<count($arr_emp_info); $i++) {
			echo("<tr height=\"22\"> \n");
			echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
            $wk_emp_id = $arr_emp_info[$i]["emp_personal_id"];
            //職員ID
			echo($wk_emp_id);
			echo("</font></td> \n");
			for ($j=1; $j<=31; $j++) {
				echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
				$wk_sfc_code = $arr_data[$i][$j];
				$wk_sfc_code = ($wk_sfc_code == "") ? "0000" : $wk_sfc_code;
				echo($wk_sfc_code);
				echo("</font></td> \n");
			}
			echo("</tr>\n");
		}
			?>
		</table>
<? } ?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">

	</form>
	<? } ?>
	<form name="csv" method="get" target="download">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
	<input type="hidden" name="duty_yyyy" value="<? echo($start_yr); ?>">
	<input type="hidden" name="duty_mm" value="<? echo($start_mon); ?>">
	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>


</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
