<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("aclg_set.php");

$fname=$PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 通らないはずだが念のためチェック
if ($update_action != "t" && $approve == "") {
	echo("<script language='javascript'>alert('承認ステータスが不明のため処理を中断します。');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$obj = new application_workflow_common_class($con, $fname);

//勤怠申請機能を使用するかファイルで確認 
$kintai_flag = file_exists("kintai/common/mdb.php");
// 総合届け使用フラグ
$general_flag = file_exists("opt/general_flag");
// トランザクションを開始
pg_query($con, "begin");
// ----- メディアテック Start  -----
if (phpversion() >= '5.1' && $kintai_flag) {
    $template_type = $_POST["kintai_template_type"];
    if (!empty($template_type)) {
        require_once 'kintai/common/mdb_wrapper.php';
        require_once 'kintai/common/mdb.php';
        MDBWrapper::init();
        $mdb = new MDB();
        $mdb->beginTransaction(); // トランザクション開始
    }
}
// ----- メディアテック End -----

$apply = get_apply_by_apply_id($con, $apply_id, $fname);

// テンプレートの場合XML形式のテキスト$contentを作成
$wkfw_content_type = $apply["wkfw_content_type"];
if ($wkfw_content_type == "2") {
	$savexmlfilename = "workflow/tmp/{$session}_x.php";

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($apply["wkfw_id"], $apply["wkfw_history_no"]);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);

	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	fclose($fp);

	include($savexmlfilename);
}

// ----- メディアテック Start -----
if (phpversion() >= '5.1' && $kintai_flag) {
    // テンプレートタイプを取得
    $template_type = $_POST["kintai_template_type"];
    $class = null;
    switch ($template_type) {
        case "ovtm": // 時間外勤務命令の場合
            if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                $class = WORKFLOW_OVTM_CLASS;
            }
            break;
        case "hol": // 休暇申請の場合
            //総合届け
            if ($general_flag) {
                if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                    $class = WORKFLOW_HOL_CLASS;
                }
            }
            //休暇申請
            else {
                if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                    $class = WORKFLOW_HOL2_CLASS;
                }
            }
            break;
        case "travel": // 旅行命令
            if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                $class = WORKFLOW_TRVL_CLASS;
            }
            break;
        case "late": // 遅刻早退の場合
        	if ($_POST["late_early_type"] === "new") {
	            if (module_check(WORKFLOW_LATE2_MODULE, WORKFLOW_LATE2_CLASS)) {
	                $class = WORKFLOW_LATE2_CLASS;
	            }
        	}else{
	            if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
	                $class = WORKFLOW_LATE_CLASS;
	            }
        	}
            break;
    }
}
// ----- メディアテック End -----

// 申請内容更新ボタンが押された場合、内容のみを更新し、画面を再表示する
if ($update_action == "t") {
    //	$obj->update_apply($apply_id, $content, $apply["apply_title"]); //DBのタイトル
    $obj->update_apply($apply_id, $content, $_POST["apply_title"]); //画面からのタイトル 20131017
    // ----- メディアテック Start -----
    if (!empty($class)) {
        $module = new $class;
        $module->update_apply($apply_id);
        // 申請日を更新
        require_once 'kintai/common/workflow_common_util.php';
        WorkflowCommonUtil::apply_date_update($con, $apply_id);
	    // MDBトランザクションをコミット
	    $mdb->endTransaction();
    }
    // ----- メディアテック End -----
    pg_query($con, "commit");
	pg_close($con);
	echo("<script type='text/javascript'>location.href = '$location_href';</script>");
	exit;
}

// 申請結果通知削除・登録
$obj->delete_applynotice($apply_id);
if($notice_emp_id != "")
{
    $arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
    for($i=0; $i<count($arr_notice_emp_id); $i++)
    {
        $obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
    }
}

//休暇申請で確定する階層設定 "1":1階層目 "","2":最終承認の階層 20141010
if ($template_type == "hol") {
    $apply_hol_cfm = $conf->get('apply.hol.cfm');
}
if ($apply_hol_cfm == "") {
	$apply_hol_cfm = "2";
}
// 承認処理
$approval_just_fixed = $obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, "DETAIL");
// ----- メディアテック Start -----
if (phpversion() >= '5.1' && $kintai_flag) {
    if (!empty($class)) {
        $module = new $class;
        
        switch($approve) {
            case "1": // 承認の場合
                // 2013/09/28 休暇申請は承認確定のときのみ行う
                // 承認条件にテンプレートが休暇申請以外のときも追加、総合届けはapprove()の中で確認しているので条件追加されても問題はない
                if ($template_type != "hol" ||  //テンプレートが休暇申請以外のとき
                    ($apply_hol_cfm == "2" && $template_type == "hol" && $approval_just_fixed) || //休暇申請で最終承認確認が必要の場合、承認確定時
                    ($apply_hol_cfm == "1" && $template_type == "hol")  //休暇申請で1階層目でよい場合、確定の確認をしない
                    ) {
                    $module->approve($apply_id, $con);
                }
                break;
            case "2": // 否認の場合
                $module->cancel_apply($apply_id);
                break;
            case "3": // 差戻しの場合
                $module->update_status($apply_id, $approve);
                break;
        }
    }
}
//// ----- メディアテック End   -----

// 承認依頼受信通知メールを送るかどうか判断、送る場合は宛先を配列に追加
$to_addresses = array();
if (must_send_mail_to_next_approvers($con, $obj, $apply_id, $wkfw_appr, $apv_order, $approve, $next_notice_div, $fname)) {

	// 次の階層で未承認の承認者（全員のはず）のメールアドレスを取得
	$next_apv_order = $apv_order + 1;
	$sql = "select e.emp_email2 from empmst e";
	$cond = "where exists (select * from applyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id and a.apv_order = $next_apv_order and a.apv_stat = '0')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		if ($row["emp_email2"] != "") {
			$to_addresses[] = $row["emp_email2"];
		}
	}
}

// 承認依頼受信通知メール送信に必要な情報を取得
if (count($to_addresses) > 0) {
	$apply_title = $apply["apply_title"];
	$apply_emp_id = $apply["emp_id"];

	$emp_detail = $obj->get_empmst_detail($apply_emp_id);
	$emp_nm = format_emp_nm($emp_detail[0]);
	$emp_mail_header = format_emp_mail_header($emp_detail[0]);
	$emp_mail = format_emp_mail($emp_detail[0]);
	$emp_pos = format_emp_pos($emp_detail[0]);

	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
	$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
}

// 承認確定時なら、承認確定通知メールの送信に必要なデータを取得
$must_send_aprv_mail = ($approval_just_fixed && get_wkfw_aprv_mail_flg($con, $apply["wkfw_id"], $fname) == "t");
if ($must_send_aprv_mail) {
	$apply_emp_detail = $obj->get_empmst_detail($apply["emp_id"]);
	$apply_emp_mail = $apply_emp_detail[0]["emp_email2"];
	if ($apply_emp_mail == "") {
		$must_send_aprv_mail = false;
	} else {
		$aprv_emp_id = get_emp_id($con, $session, $fname);
		$aprv_emp_detail = $obj->get_empmst_detail($aprv_emp_id);
		$aprv_emp_mail_header = format_emp_mail_header($aprv_emp_detail[0]);
		$aprv_emp_mail = format_emp_mail($aprv_emp_detail[0]);

		$apply_time = format_apply_time($apply["apply_date"]);
		$apply_title = $apply["apply_title"];

		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
		$wkfw_title = $arr_apply_wkfwmst[0]["wkfw_title"];
		$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
	}
}

// 文書登録処理
$files = register_library($con, $approve, $obj, $apply_id, $fname);

// ----- メディアテック Start -----
// MDBトランザクションをコミット
if (phpversion() >= '5.1' && $kintai_flag) {
    if (!empty($template_type)) {
    	$mdb->endTransaction();
	}
}
// ----- メディアテック End -----
// トランザクションをコミット
pg_query($con, "commit");

// 承認依頼受信通知メール送信
if (count($to_addresses) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] {$approve_label}依頼のお知らせ";
	$mail_content = format_mail_content($con, $wkfw_content_type, $content, $fname);
	$mail_separator = str_repeat("-", 60) . "\n";
	$additional_headers = "From: $emp_mail_header";
	$additional_parameter = "-f$emp_mail";

	foreach ($to_addresses as $to_address) {
		$mail_body = "以下の{$approve_label}依頼がありました。\n\n";
		$mail_body .= "申請者：{$emp_nm}\n";
		$mail_body .= "所属：{$emp_pos}\n";
		$mail_body .= "{$title_label}：{$apply_title}\n";
		if ($mail_content != "") {
			$mail_body .= $mail_separator;
			$mail_body .= "{$mail_content}\n";
		}

		mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
	}
}

// 承認確定通知メール送信
if ($must_send_aprv_mail) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] {$approve_label}確定のお知らせ";

	$mail_body = "以下の申請が{$approve_label}確定されました。\n\n";
	$mail_body .= "申請書名：{$wkfw_title}\n";
	$mail_body .= "申請日時：{$apply_time}\n";
	$mail_body .= "{$title_label}：{$apply_title}\n";
	if ($apv_comment != "") {
		$mail_separator = str_repeat("-", 60) . "\n";
		$mail_body .= $mail_separator;
		$mail_body .= "{$apv_comment}\n";
	}

	$additional_headers = "From: $aprv_emp_mail_header";
	$additional_parameter = "-f$aprv_emp_mail";

	mb_send_mail($apply_emp_mail, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
}

// データベース接続を切断
pg_close($con);

// ファイルコピー
foreach ($files as $tmp_file) {
	copy_apply_file_to_library($tmp_file);
}

// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");

function format_apply_time($datetime) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5", $datetime);
}

// ----- メディアテック Start -----
// モジュール・クラスの存在チェック
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}
// ----- メディアテック End -----
