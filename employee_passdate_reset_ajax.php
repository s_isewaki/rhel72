<?php

/**
 * 管理者によるパスワード変更日のリセット
 *
 */
ob_start();
require_once("Cmx.php");
require_once("about_comedix.php");

//ページ名
$fname = $_SERVER['PHP_SELF'];

//セッションのチェック
$session = qualify_session($_COOKIE["CMX_AUTH_SESSION"], $fname);
if ($session === "0") {
    header('HTTP/1.1 403 Forbidden');
    echo '0';
    exit;
}

//DBコネクション
$con = connect2db($fname);
if ($con === "0") {
    header('HTTP/1.1 403 Forbidden');
    echo '0';
    exit;
}

$emp_id = $_POST['emp'];

// パスワードリセットSQL
$sql = "UPDATE login SET pass_change_date='' WHERE emp_id='{$emp_id}'";
$upd = update_set_table($con, $sql, array(), array(), "", $fname);
if ($upd == 0) {
	pg_close($con);
    header('HTTP/1.1 403 Forbidden');
    echo '0';
    exit;
}

ob_end_clean();