#!/bin/sh
#=====================================================================
# atdbk_duty_shift_copy.sh
# [atdbk] copy to [atdbk_sche]
#=====================================================================

# CoMedix Directory
comedix_dir='/var/www/html/comedix'

#---------------------------------------------------------------------
# PHP Program CALL
php -q -c /etc/php.ini -f $comedix_dir/atdbk_duty_shift_copy.php $1
#---------------------------------------------------------------------

