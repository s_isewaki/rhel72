<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix リンクライブラリ</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");

require_once("about_comedix.php");
require_once("link_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 60, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// リンクライブラリ管理権限の取得
$link_admin_auth = check_authority($session, 61, $fname);

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 表示対象カテゴリー一覧を取得
$sql = "select * from linkcate";
$cond = "where show_flg3 = true and exists (select * from link where link.linkcate_id = linkcate.linkcate_id and show_flg3 = true) order by order3";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$categories = (pg_num_rows($sel) > 0) ? pg_fetch_all($sel) : array();
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="link_menu.php?session=<? eh($session); ?>"><img src="img/icon/b40.gif" width="32" height="32" border="0" alt="リンクライブラリ"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="link_menu.php?session=<? eh($session); ?>"><b>リンクライブラリ</b></a></font></td>
<?
if ($link_admin_auth == "1") {
?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="link_admin_menu.php?session=<? eh($session); ?>"><b>管理画面へ</b></a></font></td>
	<?
}
?>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<?
if (count($categories) == 0) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクは登録されていません。</font></td>
</tr>
</table>
	<?
} else {
	write_categories($categories, 3, $con, $fname);
}
pg_close($con);
?>
</td>
</tr>
</table>
</body>
</html>