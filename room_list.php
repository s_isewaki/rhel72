<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>マスターメンテナンス | 病室一覧</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("show_select_values.ini"); ?>
<? require("show_room_list.ini"); ?>
<? require("get_values.ini"); ?>
<?
// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$auth_check = check_authority($session,21,$fname);
if ($auth_check == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// DBコネクション作成
$con = connect2db($fname);
?>
<script type="text/javascript">
function deleteRoom() {
	if (document.room.elements['del_room[]'] == undefined) {
		alert('削除可能な病室が存在しません');
		return;
	}

	if (document.room.elements['del_room[]'].length == undefined) {
		if (!document.room.elements['del_room[]'].checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.room.elements['del_room[]'].length; i < j; i++) {
			if (document.room.elements['del_room[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	}

	if (confirm('削除してよろしいですか？')) {
		document.room.submit();
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="building_list.php?session=<? echo($session); ?>"><b>病棟</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<form name="room" action="room_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<? if ($ward_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<? } ?>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="building_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">棟一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="building_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">棟登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="ward_list.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="room_list.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&enti_id=<? echo($enti_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病室一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="room_register.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病室登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="ward_div_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟区分</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="room_equipment_register.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteRoom();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>棟：<? echo(show_bldg_nm($con,$bldg_cd,$fname)); ?>&nbsp;病棟：<? echo(get_ward_nm($con,$bldg_cd,$ward_cd,$fname)); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="30" height="22"></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病室名</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最大病床数</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">現在病床数</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベット差額</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備設定</font></td>
</tr>
<? show_room_list($con, $session, $bldg_cd, $ward_cd, $fname); ?>
</table>
</td>
<!-- right -->
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
