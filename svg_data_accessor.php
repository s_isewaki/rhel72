<?
//require("about_session.php");
//require("about_authority.php");
require("about_postgres.php");
//require("get_values.ini");
require("conf/sql.inf");

$fname = $PHP_SELF;
$con   = connect2db($fname);

$oid = '';
if ($_POST['oid'] && $_POST['oid'] != '') {
    $oid = $_POST['oid'];
}

$status ='';
if ($_POST['status'] && $_POST['status'] != '') {
    $status = $_POST['status'];
}

$pt_id ='';
if ($_POST['pt_id'] && $_POST['pt_id'] != '') {
    $pt_id = $_POST['pt_id'];
}

$emp_id ='';
if ($_POST['emp_id'] && $_POST['emp_id'] != '') {
    $emp_id = $_POST['emp_id'];
}

$svg_data ='';
if ($_REQUEST['svg_data'] && $_REQUEST['svg_data'] != '') {
    $svg_data  = '<?xml version="1.0" encoding="UTF-8"?>\n';
    $svg_data .= mb_convert_encoding(rawurldecode($_REQUEST['svg_data']), 'EUC-JP', 'UTF-8');
}

$tpl_flg = '';
if ($_POST['tpl_flg'] && $_POST['tpl_flg'] != '') {
    $tpl_flg = $_POST['tpl_flg'];
}

$data_name = date('Y/m/d H:i:s');
$img_name  = date('YmdHis') . '.png';

$img_data = '';
if ($_POST['img_data'] && $_POST['img_data'] != '') {
    $img_data  = $_POST['img_data'];
}

// セーブデータ呼び出し
if ($status == 'select') {
    $sql  = "select svg_data from genogram";
    $cond = "where oid='" . $oid . "' and tpl_flg = '" . $tpl_flg . "' and delete_flg = '0'";
    $sel  = select_from_table($con, $sql, $cond, $fname);
    $res  = pg_fetch_assoc($sel);

    echo ($res['svg_data']);
}
// データ新規登録
elseif ($status == 'I') {

	// 患者ID重複の有無
	$ptid_flg = dataChk($con, $fname, $pt_id);
	if ($ptid_flg == 1) {	// 患者IDがすでに存在していたら更新に変更する。
		dataRenew($con, $fname, $svg_data, $data_name, $img_name, $img_data, $pt_id);
	}
	else {
    	$sql     = "insert into genogram (ptif_id, emp_id, svg_data, tpl_flg, delete_flg, data_name, img_name, img_data) values (";

    	$content = array($pt_id, $emp_id, $svg_data, '2', '0', $data_name, $img_name, $img_data);
    	$ins     = insert_into_table($con, $sql, $content, $fname);
    	if ($ins == 0) {
        	pg_query($con, "rollback");
        	pg_close($con);
        	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        	exit;
    	}
	}
}
// データ更新
elseif ($status == 'U') {
	// 患者ID重複の有無
	$ptid_flg = dataChk($con, $fname, $pt_id);
	if ($ptid_flg == 2) {	// 患者IDが存在していなければ新規登録に変更する。
		dataNew($con, $fname, $svg_data, $data_name, $img_name, $img_data, $pt_id, $emp_id);
	}
	else {
    	$sql      = 'update genogram set';
    	$set      = array('svg_data', 'data_name', 'img_name', 'img_data');
    	$setvalue = array($svg_data, $data_name, $img_name, $img_data);
    	$cond     = "where ptif_id = '" . $pt_id . "'";
    	$upd      = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    	if ($upd == 0) {
        	pg_close($con);
        	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        	exit;
    	}
	}
}
// データ削除
elseif ($status == 'D') {

	// 患者ID重複の有無
	$ptid_flg = dataChk($con, $fname, $pt_id);
	if ($ptid_flg == 1) {	// 患者IDが存在していたら削除
    	$sql  = 'delete from genogram';
//    $cond = "where ptif_id = '" . $pt_id . "' and emp_id = '" . $emp_id . "'";
    	$cond = "where ptif_id = '" . $pt_id . "'";	// 20120306　キーを患者IDのみに変更
    	$upd  = delete_from_table($con, $sql, $cond, $fname);
    	if ($del == 0) {
        	pg_query($con, "rollback");
        	pg_close($con);
        	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        	exit;
    	}
	}
	else {
	}
}

// リンク用セーブデータ情報呼出
if ($status == 'sdlset') {
    $sql      = "select oid, data_name from genogram";
//    $cond     = "where ptif_id='" . $pt_id . "' and emp_id='" . $emp_id . "' and tpl_flg = '2' and delete_flg = '0'";	// 20120306　キーを患者IDのみに変更
    $cond     = "where ptif_id='" . $pt_id . "' and tpl_flg = '2' and delete_flg = '0'";
    $sel      = select_from_table($con, $sql, $cond, $fname);
    $sd_array = pg_fetch_all($sel);
    $sdcnt    = count($sd_array);

    echo ($sd_array[0]['oid'] . ',' . $sd_array[0]['data_name']);
}

// 20120306 患者IDが重複していないかチェック
function dataChk($con, $fname, $pt_id){
    $sql  = "select ptif_id from genogram";
    $cond = "where ptif_id = '" . $pt_id . "'";
    $sel  = select_from_table($con, $sql, $cond, $fname);

	$flg = 2;	// チェックフラグ(2:データなし、1:データあり)
	if($sel != 0 && pg_fetch_row($sel) != 0){
		$flg = 1;
	}
	return $flg;
}

// 20120306 新規として入ってきたが患者ID既存の場合の更新用
function dataRenew($con, $fname, $svg_data, $data_name, $img_name, $img_data, $pt_id){
    $sql      = 'update genogram set';
    $set      = array('svg_data', 'data_name', 'img_name', 'img_data');
    $setvalue = array($svg_data, $data_name, $img_name, $img_data);
    $cond     = "where ptif_id = '" . $pt_id . "'";
    $upd      = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 20120306 更新として入ってきたがすでに削除されていた場合の新規用
function dataNew($con, $fname, $svg_data, $data_name, $img_name, $img_data, $pt_id, $emp_id){
    $sql     = "insert into genogram (ptif_id, emp_id, svg_data, tpl_flg, delete_flg, data_name, img_name, img_data) values (";

    $content = array($pt_id, $emp_id, $svg_data, '2', '0', $data_name, $img_name, $img_data);
    $ins     = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
pg_close($con);
?>
