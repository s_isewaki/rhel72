<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;


//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);


//====================================
// 初期処理
//====================================
// ログインユーザの職員IDを取得

$arr_empmst = $obj->get_empmst($session);
$emp_id = $arr_empmst[0]["emp_id"];

$arr_preceptor_eval = $obj->get_preceptor_eval_list($emp_id);

$cl_title = cl_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜プリセプティ評価選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">


function set_preceptee(apply_id, apv_order, apv_sub_order, emp_full_nm, apv_date, total_score)
{
    if(chk_dpl_preceptee(apply_id, apv_order, apv_sub_order))
    {
        preceptee_names = opener.document.getElementById('preceptee_names');
        preceptee_apv_dates = opener.document.getElementById('preceptee_apv_dates');
        preceptee_scores = opener.document.getElementById('preceptee_scores');

        preceptee_apply_ids = opener.document.getElementById('preceptee_apply_ids');
        preceptee_apv_orders = opener.document.getElementById('preceptee_apv_orders');
        preceptee_apv_sub_orders = opener.document.getElementById('preceptee_apv_sub_orders');

        preceptee_names_val     = preceptee_names.value;
        preceptee_apv_dates_val = preceptee_apv_dates.value;
        preceptee_scores_val    = preceptee_scores.value;

        preceptee_apply_ids_val      = preceptee_apply_ids.value;
        preceptee_apv_orders_val     = preceptee_apv_orders.value;
        preceptee_apv_sub_orders_val = preceptee_apv_sub_orders.value;

        join = "";
        if(preceptee_names_val != "")
        {
            join = ",";
        }

        preceptee_names.value = preceptee_names_val + join + emp_full_nm;
        preceptee_apv_dates.value = preceptee_apv_dates_val + join + apv_date;
        preceptee_scores.value = preceptee_scores_val + join + total_score;

        preceptee_apply_ids.value = preceptee_apply_ids_val + join + apply_id;
        preceptee_apv_orders.value = preceptee_apv_orders_val + join + apv_order;
        preceptee_apv_sub_orders.value = preceptee_apv_sub_orders_val + join + apv_sub_order;

        opener.create_preceptee_table();
    }
}

function chk_dpl_preceptee(apply_id, apv_order, apv_sub_order)
{

    preceptee_apply_ids = opener.document.getElementById('preceptee_apply_ids').value;
    preceptee_apv_orders = opener.document.getElementById('preceptee_apv_orders').value;
    preceptee_apv_sub_orders = opener.document.getElementById('preceptee_apv_sub_orders').value;

    if(preceptee_apply_ids != "")
    {
        arr_preceptee_apply_ids       = preceptee_apply_ids.split(",");
        arr_preceptee_apv_orders      = preceptee_apv_orders.split(",");
        arr_preceptee_apv_sub_orders  = preceptee_apv_sub_orders.split(",");

        for(i=0; i<arr_preceptee_apply_ids.length; i++)
        {
            if(apply_id == arr_preceptee_apply_ids[i] && apv_order == arr_preceptee_apv_orders[i] && apv_sub_order == arr_preceptee_apv_sub_orders[i])
            {
                return false;
            }
        }
    }

    return true;
}

</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

</style>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>プリセプティ評価選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?
if(count($arr_preceptor_eval) == 0)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">70点以上のプリセプティ評価がございません。</font>
</td>
</tr>
</table>
<?
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<tr bgcolor="#E5F6CD" align="center">
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">選択</font></td>
<td width="120"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プリセプティ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">総合点</font></td>
</tr>

<?
foreach($arr_preceptor_eval as $preceptor_eval)
{
    $apply_id      = $preceptor_eval["apply_id"];
    $apv_order     = $preceptor_eval["apv_order"];
    $apv_sub_order = $preceptor_eval["apv_sub_order"];
    $apply_date    = preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $preceptor_eval["apply_date"]);
    $apply_no      = $preceptor_eval["apply_no"];

    $emp_full_nm   = $preceptor_eval["emp_lt_nm"]." ".$preceptor_eval["emp_ft_nm"];;
    $apv_date      = preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $preceptor_eval["apv_date"]);
    $total_score   = $preceptor_eval["total_score"]; 

	// 申請番号
    $year = substr($apply_date, 0, 4);
    $md   = substr($apply_date, 4, 4);
    if($md >= "0101" and $md <= "0331")
    {
        $year = $year - 1;
    }
    $apply_no = "c217-".$year."".sprintf("%04d", $apply_no);


?>
<tr>
<td align="center"><input type="button" value="選択" onclick="set_preceptee('<?=$apply_id?>', '<?=$apv_order?>', '<?=$apv_sub_order?>', '<?=$emp_full_nm?>', '<?=$apv_date?>', '<?=$total_score?>')"></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_no?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($emp_full_nm)?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apv_date?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$total_score?></font></td>
</tr>

<?
}
?>

</table>
<?
}
?>

<center>
</body>
</form>
</html>


<?


pg_close($con);
?>