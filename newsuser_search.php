<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("news_common.ini");
require_once("get_values.ini");
require_once("show_newsuser_search.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 46, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// お知らせ管理権限の取得
$news_admin_auth = check_authority($session, 24, $fname);

// ページ番号の設定
if ($page == "") {
    $page = 1;
}

// データベースに接続
$con = connect2db($fname);

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// カテゴリ一覧を取得 2:全館〜回覧板
//$category_list = get_category_list_from_db($con, $fname, 4, $emp_id);
$category_list_db = get_category_list_from_db($con, $fname, 4, $emp_id);
$category_list = array();
foreach ($category_list_db as $category_id => $category_name) {
    $category_list[$category_id] = $category_name["newscate_name"];
}

// 部門一覧を取得
$class_list = get_class_list($con, $fname);
$atrb_list = get_atrb_list($con, $fname);
$dept_list = get_dept_list($con, $fname);
$room_list = get_room_list($con, $fname);

// 通達区分一覧を取得
$notice_list = get_notice_list($con, $fname);
$notice2_list = get_notice2_list($con, $fname);

// 登録者名を取得
$emp_name = ($srch_emp_id != "") ? get_emp_kanji_name($con, $srch_emp_id, $fname) : "";

// 発信者名を取得
$src_emp_nm = ($src_emp_id != "") ? get_emp_kanji_name($con, $src_emp_id, $fname) : "";

// 一般ユーザ登録許可フラグを取得
$everyone_flg = get_everyone_flg($con, $fname);

// お知らせ返信機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_henshin = $conf->get('setting_henshin');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if ($setting_henshin != "t") {
    $setting_henshin = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ | 検索</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setSrcAtrbOptions('<? echo($srch_src_atrb_id); ?>', '<? echo($srch_src_dept_id); ?>', '<? echo($srch_src_room_id); ?>');
}

function setSrcAtrbOptions(atrb_id, dept_id, room_id) {
	var atrb_elm = document.mainform.srch_src_atrb_id;
	deleteAllOptions(atrb_elm);
	addOption(atrb_elm, '0', '　　　', atrb_id);

	var class_id = document.mainform.srch_src_class_id.value;
	switch (class_id) {
<?
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
	echo("\tcase '$tmp_class_id':\n");
	foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
		echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setSrcDeptOptions(dept_id, room_id);
}

function setSrcDeptOptions(dept_id, room_id) {
	var dept_elm = document.mainform.srch_src_dept_id;
	deleteAllOptions(dept_elm);
	addOption(dept_elm, '0', '　　　', dept_id);

	var atrb_id = document.mainform.srch_src_atrb_id.value;
	switch (atrb_id) {
<?
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
	echo("\tcase '$tmp_atrb_id':\n");
	foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
		echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setSrcRoomOptions(room_id);
}

function setSrcRoomOptions(room_id) {
<? if ($arr_class_name['class_cnt'] != 4) { ?>
	return;
<? } ?>

	var room_elm = document.mainform.srch_src_room_id;
	deleteAllOptions(room_elm);
	addOption(room_elm, '0', '　　　', room_id);

	var dept_id = document.mainform.srch_src_dept_id.value;
	switch (dept_id) {
<?
foreach ($room_list as $tmp_dept_id => $rooms_in_dept) {
	echo("\tcase '$tmp_dept_id':\n");
	foreach ($rooms_in_dept as $tmp_room_id => $tmp_room_nm) {
		echo("\t\taddOption(room_elm, '$tmp_room_id', '$tmp_room_nm', room_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}
}

function selectEmployee() {
	window.open('news_employee_select.php?session=<? echo($session); ?>&class_id='.concat(document.mainform.srch_src_class_id.value), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	if (selected == value) {
		box.options[box.length - 1].selected = true;
	}
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<? echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板</b></a></font></td>
<? if ($news_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>&news=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<? if ($everyone_flg == "t") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="newsuser_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>検索</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="newsuser_search.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="25%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input type="text" name="srch_news_title" value="<? echo($srch_news_title); ?>" size="50" style="ime-mode:active;"></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td><input type="text" name="srch_news" value="<? echo($srch_news); ?>" size="50" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></td>
<td><select name="srch_news_category"><?php show_options($category_list, $srch_news_category, true); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
<td><select name="srch_notice_id"><?php show_options($notice_list, $srch_notice_id, true); ?></select></td>
</tr>
<?php if (!empty($notice2_list)) {?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分</font></td>
<td colspan="3"><select name="srch_notice2_id"><?php show_options($notice2_list, $srch_notice2_id, true); ?></select></td>
</tr>
<? }?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発信部署</font></td>
<td><select name="srch_src_class_id" onchange="setSrcAtrbOptions();"><? show_options($class_list, $srch_src_class_id, true); ?></select><select name="srch_src_atrb_id" onchange="setSrcDeptOptions();"></select><select name="srch_src_dept_id" onchange="setSrcRoomOptions();"></select><? if ($arr_class_name['class_cnt'] == 4) { ?><select name="srch_src_room_id"></select><? } ?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="selectEmployee();">発信者</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="text" name="src_emp_nm" size="30" value="<? echo($src_emp_nm); ?>" disabled>
<a href="javascript:void(0);" onclick="document.mainform.src_emp_nm.value = ''; document.mainform.src_emp_id.value = ''; document.mainform.hid_src_emp_nm.value = '';">クリア</a>
<input type="hidden" name="src_emp_id" value="<? echo($src_emp_id); ?>">
<input type="hidden" name="hid_src_emp_nm" value="<? echo($src_emp_nm); ?>">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="srch_news_date_from1"><? show_select_years(10, $srch_news_date_from1, true); ?></select>/<select name="srch_news_date_from2"><? show_select_months($srch_news_date_from2, true); ?></select>/<select name="srch_news_date_from3"><? show_select_days($srch_news_date_from3, true); ?></select> 〜 <select name="srch_news_date_to1"><? show_select_years(10, $srch_news_date_to1, true); ?></select>/<select name="srch_news_date_to2"><? show_select_months($srch_news_date_to2, true); ?></select>/<select name="srch_news_date_to3"><? show_select_days($srch_news_date_to3, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載日（ログイン画面）</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="srch_news_date_login_from1"><? show_select_years(10, $srch_news_date_login_from1, true); ?></select>/<select name="srch_news_date_login_from2"><? show_select_months($srch_news_date_login_from2, true); ?></select>/<select name="srch_news_date_login_from3"><? show_select_days($srch_news_date_login_from3, true); ?></select> 〜 <select name="srch_news_date_login_to1"><? show_select_years(10, $srch_news_date_login_to1, true); ?></select>/<select name="srch_news_date_login_to2"><? show_select_months($srch_news_date_login_to2, true); ?></select>/<select name="srch_news_date_login_to3"><? show_select_days($srch_news_date_login_to3, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('news_member_list.php?session=<? echo($session); ?>&from_page_id=2', 'newwin', 'width=640,height=480,scrollbars=yes')">登録者</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="text" name="emp_name" size="30" value="<? echo($emp_name); ?>" disabled>
<a href="javascript:void(0);" onclick="document.mainform.emp_name.value = ''; document.mainform.srch_emp_id.value = '';">クリア</a>
<input type="hidden" name="srch_emp_id" value="<? echo($srch_emp_id); ?>">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="srch_news_date1"><? show_select_years(10, $srch_news_date1, true); ?></select>/<select name="srch_news_date2"><? show_select_months($srch_news_date2, true); ?></select>/<select name="srch_news_date3"><? show_select_days($srch_news_date3, true); ?></select></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="検索"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="t">
</form>
<? if ($srch_flg == "t") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr height="22" bgcolor="#f6f9ff">
            <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></td>
            <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
            <?if (!empty($notice2_list)) {?>
                <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ<br>区分</font></td>
            <? }?>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
            <td align="center" width="175"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間<br />（掲載期間：ログイン画面）</font></td>
            <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧</font></td>
            <?php if ($setting_henshin == 't') {?>
                <td align="center" width="55">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">返信一覧</font>
                </td>
            <? }?>
        </tr>
        <?show_news_search_result_list($con, $page, $class_called, $session, $fname, $notice2_list, $setting_henshin);?>
    </table>
<? show_news_search_page_list($con, $page, $session, $fname); ?>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
