<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($_FILES["file"]["name"] == "") {
	echo("<script type=\"text/javascript\">alert('ファイルが選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
switch ($_FILES["file"]["error"]) {
case 1:  // UPLOAD_ERR_INI_SIZE
case 2:  // UPLOAD_ERR_FORM_SIZE
	echo("<script type=\"text/javascript\">alert('ファイルサイズが大きすぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
case 3:  // UPLOAD_ERR_PARTIAL:
case 4:  // UPLOAD_ERR_NO_FILE:
	echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (!checkdate($month, $day, $year)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 同じ日付の文書があるかどうかチェック
$date = "$year$month$day";
$sql = "select count(*) from allot";
$cond = "where allotfcl_id = $fcl_id and date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$allot_exists = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, 0) : false;

// 同じ日付の文書が存在した場合
if ($allot_exists) {

	// 文書情報を更新
	$sql = "update allot set";
	$set = array("update_time", "emp_id");
	$setvalue = array(date("YmdHis"), $emp_id);
	$cond = "where allotfcl_id = $fcl_id and date = '$date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
} else {

	// 文書情報を登録
	$sql = "insert into allot (allotfcl_id, date, update_time, emp_id) values (";
	$content = array($fcl_id, $date, date("YmdHis"), $emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 既存ファイルの削除
exec("rm -f " . getcwd() . "/intra/allot/{$fcl_id}_{$date}.*");

// ファイルを保存
copy($_FILES["file"]["tmp_name"], "intra/allot/{$fcl_id}_{$date}" . strrchr($_FILES["file"]["name"], "."));

// 自画面を閉じ、親画面をリフレッシュ
echo("<script type=\"text/javascript\">self.close(); opener.location.href = 'intra_info_allot.php?session=$session';</script>");
?>
