<?php
// settings_common.php
// 設定情報の取得を行う共通関数

/**
 * get_settings_value
 * settings.phpに設定された変数の内容を返す。ない場合は、デフォルトを返す。
 * @param mixed $varname 変数名
 * @param mixed $default デフォルト、未設定は""となります。
 * @return mixed This is the return value description
 *
 */
function get_settings_value($varname, $default="") {
    if (file_exists("opt/settings.php")){
        require("opt/settings.php");
    }
    
    if ($$varname != "") {
        return $$varname;
    }
    else {
        return $default;
    }
}

?>