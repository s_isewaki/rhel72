<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("atdbk_workflow_common_class.php");
require_once("ovtm_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");


$obj = new atdbk_workflow_common_class($con, $fname);
$obj->approve_application($apply_id, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, "TMMD");

$ovtm_class = new ovtm_class($con, $fname);

// 申請情報を取得
$sql = "select * from tmmdapply";
$cond = "where apply_id = $apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_status = pg_fetch_result($sel, 0, "apply_status");


// 否認の場合
if ($apply_status == "2") {


	$apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
	$target_date = pg_fetch_result($sel, 0, "target_date");

	//一日分の修正の場合
	if (strlen($target_date) == 8) {
		$b_tmcd_group_id = pg_fetch_result($sel, 0, "b_tmcd_group_id");
		$b_pattern = pg_fetch_result($sel, 0, "b_pattern");
		$b_reason = pg_fetch_result($sel, 0, "b_reason");
		$b_night_duty = pg_fetch_result($sel, 0, "b_night_duty");
		$b_allow_id = pg_fetch_result($sel, 0, "b_allow_id");
		$b_meeting_time = pg_fetch_result($sel, 0, "b_meeting_time");
		$b_previous_day_flag = pg_fetch_result($sel, 0, "b_previous_day_flag");
		$b_next_day_flag = pg_fetch_result($sel, 0, "b_next_day_flag");
		
		$b_start_time = pg_fetch_result($sel, 0, "b_start_time");
		$b_out_time = pg_fetch_result($sel, 0, "b_out_time");
		$b_ret_time = pg_fetch_result($sel, 0, "b_ret_time");
		$b_end_time = pg_fetch_result($sel, 0, "b_end_time");
		$a_end_time = pg_fetch_result($sel, 0, "a_end_time");
        $b_rest_start_time = pg_fetch_result($sel, 0, "b_rest_start_time");
        $b_rest_end_time = pg_fetch_result($sel, 0, "b_rest_end_time");
        for ($i = 1; $i <= 10; $i++) {
			$b_start_time_ver = "b_o_start_time$i";
			$b_end_time_ver = "b_o_end_time$i";
			$$b_start_time_ver = pg_fetch_result($sel, 0, "$b_start_time_ver");
			$$b_end_time_ver = pg_fetch_result($sel, 0, "$b_end_time_ver");
			$a_start_time_ver = "a_o_start_time$i";
			$$a_start_time_ver = pg_fetch_result($sel, 0, "$a_start_time_ver");
		}
		$b_allow_count = pg_fetch_result($sel, 0, "b_allow_count");
		$b_over_start_time = pg_fetch_result($sel, 0, "b_over_start_time");
		$b_over_end_time = pg_fetch_result($sel, 0, "b_over_end_time");
		$b_over_start_next_day_flag = pg_fetch_result($sel, 0, "b_over_start_next_day_flag");
		$b_over_end_next_day_flag = pg_fetch_result($sel, 0, "b_over_end_next_day_flag");
		$b_over_start_time2 = pg_fetch_result($sel, 0, "b_over_start_time2");
		$b_over_end_time2 = pg_fetch_result($sel, 0, "b_over_end_time2");
		$b_over_start_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_start_next_day_flag2");
		$b_over_end_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_end_next_day_flag2");
        for ($i = 3; $i <= 5; $i++) {
            $b_start_time_ver = "b_over_start_time$i";
            $b_end_time_ver = "b_over_end_time$i";
            $$b_start_time_ver = pg_fetch_result($sel, 0, "$b_start_time_ver");
            $$b_end_time_ver = pg_fetch_result($sel, 0, "$b_end_time_ver");

            $b_start_flag_ver = "b_over_start_next_day_flag$i";
            $b_end_flag_ver = "b_over_end_next_day_flag$i";
            $$b_start_flag_ver = pg_fetch_result($sel, 0, "$b_start_flag_ver");
            $$b_end_flag_ver = pg_fetch_result($sel, 0, "$b_end_flag_ver");
        }
        for ($i = 1; $i <= 5; $i++) {
            $b_start_time_ver = "b_rest_start_time$i";
            $b_end_time_ver = "b_rest_end_time$i";
            $$b_start_time_ver = pg_fetch_result($sel, 0, "$b_start_time_ver");
            $$b_end_time_ver = pg_fetch_result($sel, 0, "$b_end_time_ver");
            
            $b_start_flag_ver = "b_rest_start_next_day_flag$i";
            $b_end_flag_ver = "b_rest_end_next_day_flag$i";
            $$b_start_flag_ver = pg_fetch_result($sel, 0, "$b_start_flag_ver");
            $$b_end_flag_ver = pg_fetch_result($sel, 0, "$b_end_flag_ver");
        }
        
		// 勤務実績を申請前の状態に戻す
		$sql = "update atdbkrslt set";
		$set = array("tmcd_group_id", "pattern", "reason", "night_duty", "allow_id", "start_time", "out_time", "ret_time", "end_time", "o_start_time1", "o_end_time1", "o_start_time2", "o_end_time2", "o_start_time3", "o_end_time3", "o_start_time4", "o_end_time4", "o_start_time5", "o_end_time5", "o_start_time6", "o_end_time6", "o_start_time7", "o_end_time7", "o_start_time8", "o_end_time8", "o_start_time9", "o_end_time9", "o_start_time10", "o_end_time10", "meeting_time", "previous_day_flag", "next_day_flag", "allow_count", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
		$setvalue = array($b_tmcd_group_id, $b_pattern, $b_reason, $b_night_duty, $b_allow_id, $b_start_time, $b_out_time, $b_ret_time, $b_end_time, $b_o_start_time1, $b_o_end_time1, $b_o_start_time2, $b_o_end_time2, $b_o_start_time3, $b_o_end_time3, $b_o_start_time4, $b_o_end_time4, $b_o_start_time5, $b_o_end_time5, $b_o_start_time6, $b_o_end_time6, $b_o_start_time7, $b_o_end_time7, $b_o_start_time8, $b_o_end_time8, $b_o_start_time9, $b_o_end_time9, $b_o_start_time10, $b_o_end_time10, $b_meeting_time, $b_previous_day_flag, $b_next_day_flag, $b_allow_count, $b_over_start_time, $b_over_end_time, $b_over_start_next_day_flag, $b_over_end_next_day_flag, $b_over_start_time2, $b_over_end_time2, $b_over_start_next_day_flag2, $b_over_end_next_day_flag2);
        for ($i=3; $i<=5; $i++) {
            $s_t = "over_start_time".$i;
            $e_t = "over_end_time".$i;
            $s_f = "over_start_next_day_flag".$i;
            $e_f = "over_end_next_day_flag".$i;
            $s_tv = "b_over_start_time".$i;
            $e_tv = "b_over_end_time".$i;
            $s_fv = "b_over_start_next_day_flag".$i;
            $e_fv = "b_over_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, $$s_tv);
            array_push($setvalue, $$e_tv);
            array_push($setvalue, $$s_fv);
            array_push($setvalue, $$e_fv);
        }    
        for ($i=1; $i<=5; $i++) {
            $s_t = "rest_start_time".$i;
            $e_t = "rest_end_time".$i;
            $s_f = "rest_start_next_day_flag".$i;
            $e_f = "rest_end_next_day_flag".$i;
            $s_tv = "b_rest_start_time".$i;
            $e_tv = "b_rest_end_time".$i;
            $s_fv = "b_rest_start_next_day_flag".$i;
            $e_fv = "b_rest_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, $$s_tv);
            array_push($setvalue, $$e_tv);
            array_push($setvalue, $$s_fv);
            array_push($setvalue, $$e_fv);
        }
        if ($ovtm_class->rest_disp_flg == "t") {
            array_push($set, "rest_start_time");
            array_push($set, "rest_end_time");
            array_push($setvalue, $b_rest_start_time);
            array_push($setvalue, $b_rest_end_time);
        }
        $cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 退勤時刻が変わる場合は同日の残業申請を削除
		if ($b_end_time != $a_end_time) {
			
			$sql = "select apply_id from ovtmapply ";
			$cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			while($row = pg_fetch_array($sel))
			{
				$ovtm_apply_id = $row["apply_id"];
				
				// 残業申請承認論理削除
				$sql = "update ovtmaprv set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $ovtm_apply_id";
				
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				// 残業申請論理削除
				$sql = "update ovtmapply set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $ovtm_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				// 承認候補を論理削除
				$sql = "update ovtmaprvemp set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $ovtm_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				// 同期・非同期情報を論理削除
				$sql = "update ovtm_async_recv set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $ovtm_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
		
		// 退勤後復帰時刻が変わる場合は同日の退勤後復帰申請を削除
		if ($b_o_start_time1 != $a_o_start_time1 || $b_o_start_time2 != $a_o_start_time2 || $b_o_start_time3 != $a_o_start_time3 || $b_o_start_time4 != $a_o_start_time4 || $b_o_start_time5 != $a_o_start_time5 || $b_o_start_time6 != $a_o_start_time6 || $b_o_start_time7 != $a_o_start_time7 || $b_o_start_time8 != $a_o_start_time8 || $b_o_start_time9 != $a_o_start_time9 || $b_o_start_time10 != $a_o_start_time10) {
			
			$sql = "select apply_id from rtnapply ";
			$cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			while($row = pg_fetch_array($sel))
			{
				$rtn_apply_id = $row["apply_id"];
				
				// 退勤後復帰申請承認論理削除
				$sql = "update rtnaprv set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $rtn_apply_id";
				
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				// 退勤後復帰申請論理削除
				$sql = "update rtnapply set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $rtn_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				// 承認候補を論理削除
				$sql = "update rtnaprvemp set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $rtn_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				// 同期・非同期情報を論理削除
				$sql = "update rtn_async_recv set";
				$set = array("delete_flg");
				$setvalue = array("t");
				$cond = "where apply_id = $rtn_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
	}
	//一括修正の場合
	else {
		// 一括修正情報を取得
		$sql = "select * from tmmdapplyall";
		$cond = "where apply_id = $apply_id order by target_date";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);
		
		for ($i=0; $i<$num; $i++) {
			$target_date = pg_fetch_result($sel, $i, "target_date");
			
			$b_tmcd_group_id = pg_fetch_result($sel, $i, "b_tmcd_group_id");
			$b_pattern = pg_fetch_result($sel, $i, "b_pattern");
			$b_reason = pg_fetch_result($sel, $i, "b_reason");
			$b_night_duty = pg_fetch_result($sel, $i, "b_night_duty");
			$b_allow_id = pg_fetch_result($sel, $i, "b_allow_id");
			
			$b_previous_day_flag = pg_fetch_result($sel, $i, "b_previous_day_flag");
			$b_next_day_flag = pg_fetch_result($sel, $i, "b_next_day_flag");
			
			$b_start_time = pg_fetch_result($sel, $i, "b_start_time");
			$b_out_time = pg_fetch_result($sel, $i, "b_out_time");
			$b_ret_time = pg_fetch_result($sel, $i, "b_ret_time");
			$b_end_time = pg_fetch_result($sel, $i, "b_end_time");
			$a_end_time = pg_fetch_result($sel, $i, "a_end_time");
			$b_o_start_time1 = pg_fetch_result($sel, $i, "b_o_start_time1");
			$b_o_end_time1 = pg_fetch_result($sel, $i, "b_o_end_time1");
			$a_o_start_time1 = pg_fetch_result($sel, $i, "a_o_start_time1");
			
			$b_allow_count = pg_fetch_result($sel, $i, "b_allow_count");
			$b_over_start_time = pg_fetch_result($sel, $i, "b_over_start_time");
			$b_over_end_time = pg_fetch_result($sel, $i, "b_over_end_time");
			$b_over_start_next_day_flag = pg_fetch_result($sel, $i, "b_over_start_next_day_flag");
			$b_over_end_next_day_flag = pg_fetch_result($sel, $i, "b_over_end_next_day_flag");
			$b_over_start_time2 = pg_fetch_result($sel, $i, "b_over_start_time2");
			$b_over_end_time2 = pg_fetch_result($sel, $i, "b_over_end_time2");
			$b_over_start_next_day_flag2 = pg_fetch_result($sel, $i, "b_over_start_next_day_flag2");
			$b_over_end_next_day_flag2 = pg_fetch_result($sel, $i, "b_over_end_next_day_flag2");
			
			// 勤務実績を申請前の状態に戻す
			$sql = "update atdbkrslt set";
			$set = array("tmcd_group_id", "pattern", "reason", "night_duty", "allow_id", "start_time", "out_time", "ret_time", "end_time", "o_start_time1", "o_end_time1", "previous_day_flag", "next_day_flag", "allow_count", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
			$setvalue = array($b_tmcd_group_id, $b_pattern, $b_reason, $b_night_duty, $b_allow_id, $b_start_time, $b_out_time, $b_ret_time, $b_end_time, $b_o_start_time1, $b_o_end_time1, $b_previous_day_flag, $b_next_day_flag, $b_allow_count, $b_over_start_time, $b_over_end_time, $b_over_start_next_day_flag, $b_over_end_next_day_flag, $b_over_start_time2, $b_over_end_time2, $b_over_start_next_day_flag2, $b_over_end_next_day_flag2);
			$cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			// 退勤時刻が変わる場合は同日の残業申請を削除
			if ($b_end_time != $a_end_time) {
				
				$sql = "select apply_id from ovtmapply ";
				$cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
				$sel2 = select_from_table($con, $sql, $cond, $fname);
				if ($sel2 == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				while($row = pg_fetch_array($sel2))
				{
					$ovtm_apply_id = $row["apply_id"];
					
					// 残業申請承認論理削除
					$sql = "update ovtmaprv set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $ovtm_apply_id";
					
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					
					// 残業申請論理削除
					$sql = "update ovtmapply set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $ovtm_apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					
					// 承認候補を論理削除
					$sql = "update ovtmaprvemp set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $ovtm_apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					
					// 同期・非同期情報を論理削除
					$sql = "update ovtm_async_recv set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $ovtm_apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
			
			// 退勤後復帰時刻が変わる場合は同日の退勤後復帰申請を削除
			if ($b_o_start_time1 != $a_o_start_time1) {
				
				$sql = "select apply_id from rtnapply ";
				$cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
				$sel3 = select_from_table($con, $sql, $cond, $fname);
				if ($sel3 == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				while($row = pg_fetch_array($sel3))
				{
					$rtn_apply_id = $row["apply_id"];
					
					// 退勤後復帰申請承認論理削除
					$sql = "update rtnaprv set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $rtn_apply_id";
					
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					
					// 退勤後復帰申請論理削除
					$sql = "update rtnapply set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $rtn_apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					
					// 承認候補を論理削除
					$sql = "update rtnaprvemp set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $rtn_apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					
					// 同期・非同期情報を論理削除
					$sql = "update rtn_async_recv set";
					$set = array("delete_flg");
					$setvalue = array("t");
					$cond = "where apply_id = $rtn_apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
		}
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
if($pnt_url != "") {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");	
	echo("<script language=\"javascript\">window.close();</script>\n");
}
?>
