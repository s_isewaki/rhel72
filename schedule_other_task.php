<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>スケジュール | 公開タスク</title>
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// スケジュール権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// タスク権限のチェック
$checkauth = check_authority($session, 30, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// デフォルト日付をシステム日付とする
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$year = date("Y", $date);
$month = date("m", $date);
$day = date("d", $date);

// データベースに接続
$con = connect2db($fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteTask(){
	if (confirm("削除してよろしいですか？")) {
		document.task.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>公開タスク</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_month_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($year); ?>&s_date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_list_upcoming.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>公開タスク</b></font></a></td>
<td width="43"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo(get_emp_kanji_name($con, $emp_id, $fname)); ?></b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? show_task_navigation($emp_id, $date, $session); ?>
<? show_task_list($con, $emp_id, $date, $session, $fname); ?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// タスク一覧のナビゲーション部を表示
function show_task_navigation($emp_id, $date, $session) {

	// 前週・前日・翌日・翌週のタイムスタンプを求める
	$last_week = strtotime("-7 days", $date);
	$last_day = strtotime("-1 day", $date);
	$next_day = strtotime("+1 day", $date);
	$next_week = strtotime("+7 days", $date);
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($last_week); ?>">&lt;&lt;前週</a>&nbsp;&nbsp;</font><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($last_day); ?>">&lt;前日</a>&nbsp;&nbsp;</font><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($next_day); ?>">翌日&gt;</a>&nbsp;&nbsp;</font><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($next_week); ?>">翌週&gt;&gt;</a></font></td>
	<td width="40" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">高&nbsp;</font></td>
	<td width="20" bgcolor="#ccff33">&nbsp;</td>
	<td width="20" bgcolor="#ccff66">&nbsp;</td>
	<td width="20" bgcolor="#ccff99">&nbsp;</td>
	<td width="20" bgcolor="#ccffcc">&nbsp;</td>
	<td width="20" bgcolor="#ccffff">&nbsp;</td>
	<td width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;低</font></td>
	<td width="100" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅延作業&nbsp;</font>
	</td>
	<td width="20" bgcolor="#ff0066">&nbsp;</td>
	</tr>
	</table>
<?
}

// タスク一覧を表示
function show_task_list($con, $emp_id, $date, $session, $fname) {

	// 処理対象日付を配列に格納
	$dates = array();
	for ($i = 0; $i < 30; $i++) {
		$dates[$i] = date("Ymd", strtotime("+$i day", $date));
	}
	$today = date("Ymd");

	echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n");

	// 個人タスク一覧のヘッダ行を出力
	show_task_list_header($dates, $today);

	// 個人タスク情報を取得
	$sql = "select work_id, work_name, work_status, work_priority, to_char(work_start_date, 'YYYYMMDD') as work_start_date, to_char(work_expire_date, 'YYYYMMDD') as work_expire_date from work";
	$cond = "where emp_id = '$emp_id' and work_public_flag = 't' and work_status != '1' and work_start_date <= '{$dates[0]}' order by work_priority desc, work_expire_date";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>\n");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>\n");
		exit;
	}

	// 個人タスク一覧を出力
	while ($row = pg_fetch_array($sel)) {
		$tmp_work_id = $row["work_id"];
		$tmp_name = $row["work_name"];
		$tmp_status = $row["work_status"];
		$tmp_priority = $row["work_priority"];
		$tmp_start_date = $row["work_start_date"];
		$tmp_expire_date = $row["work_expire_date"];

		switch ($tmp_status) {
		case "0":
			$tmp_status = "未着手";
			break;
		case "2":
			$tmp_status = "着手";
			break;
		case "3":
			$tmp_status = "25%";
			break;
		case "4":
			$tmp_status = "50%";
			break;
		case "5":
			$tmp_status = "75%";
			break;
		}

		switch ($tmp_priority) {
		case "1":
			$tmp_bgcolor = "#ccffff";
			break;
		case "2":
			$tmp_bgcolor = "#ccffcc";
			break;
		case "3":
			$tmp_bgcolor = "#ccff99";
			break;
		case "4":
			$tmp_bgcolor = "#ccff66";
			break;
		case "5":
			$tmp_bgcolor = "#ccff33";
			break;
		}

		echo("<tr height=\"22\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('to_do_detail.php?session=$session&wk_id=$tmp_work_id&emp_id=$emp_id&checkauth=30n', 'newwin', 'width=640,height=480,scrollbars=yes');\">$tmp_name</a></font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_status</font></td>\n");

		foreach ($dates as $tmp_date) {
			if ($tmp_date < $tmp_start_date) {
				echo("<td>&nbsp;</td>\n");
			} else if ($tmp_date <= $tmp_expire_date) {
				echo("<td bgcolor=\"$tmp_bgcolor\">&nbsp;</td>\n");
			} else if ($today > $tmp_expire_date && $tmp_date <= $today) {
				echo("<td bgcolor=\"ff0066\">&nbsp;</td>\n");
			} else {
				echo("<td>&nbsp;</td>\n");
			}
		}

		echo("</tr>\n");
	}

	echo("</table>\n");
}

// ヘッダ行を出力
function show_task_list_header($dates, $today) {
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作業名</font></td>\n");
	echo("<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">進捗度</font></td>\n");

	foreach ($dates as $date) {
		$month = intval(substr($date, 4, 2));
		$day = intval(substr($date, 6, 2));

		if ($month != $pre_month) {
			$date_str = "$month/$day";
			$pre_month = $month;
		} else {
			$date_str = $day;
		}

		if ($date == $today) {
			$date_str = "<font color=\"red\"><b>$date_str</b></font>";
		}

		echo("<td width=\"17\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j10\">$date_str</font></td>\n");
	}
	echo("</tr>\n");
}
?>
