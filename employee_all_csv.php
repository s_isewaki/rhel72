<?

ob_start();

require_once("about_session.php");
require_once("about_authority.php");
require_once("show_attendance_pattern.ini");

$fname = $PHP_SELF;

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// ���¤Υ����å�
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}


// ���ѡ���̳����
$list_duty_form = array(
    "1" => "���",
    "2" => "����",
    "3" => "û����������"
);

// �ǡ����١�������³
$con = connect2db($fname);

// ���롼��̾�����
$group_names = get_timecard_group_names($con, $fname);

// �ѥ���ɤ�ޥ������뤫
$sql = "select hide_passwd from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$hide_passwd = pg_fetch_result($sel, 0, "hide_passwd");
// ����������CSV�����Ǽ���
$csv = get_emp_list_csv($con, $hide_passwd);

// �ǡ����١�����³���Ĥ���
pg_close($con);

// CSV�����
$file_name = "list.csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// �ؿ�
//------------------------------------------------------------------------------
// �����CSV�����Ǽ���
function get_emp_list_csv($con, $hide_passwd)
{

    // �ȿ���������
    require_once("show_class_name.ini");
    $arr_class_name = get_class_name_array($con, $fname);
    $class_cnt = $arr_class_name["class_cnt"];

    // �ȿ������פ����
    require_once("label_by_profile_type.ini");
    $profile_type = get_profile_type($con, $fname);

    // ��Ϳ�ٵ��ʬ
    $list_wage = array(
        "1" => "�����",
        //    "2" => "��������",
        "3" => "������",
        "4" => "���ֵ���",
        "5" => "ǯ����",
    );

    $sql = <<<__SQL_END__
select empmst.*, empcond.*, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, stmst.st_nm, jobmst.job_nm, authmst.group_id, authgroup.group_nm, dispgroup.group_nm as disp_group_nm, menugroup.group_nm as menu_group_nm, login.emp_login_id, login.emp_login_pass, login.emp_login_mail, wktmgrp.group_id , wktmgrp.group_name, atdptn.atdptn_id, atdptn.atdptn_nm, emp_del_flg, emp_relation.*
from empmst
inner join classmst on empmst.emp_class = classmst.class_id
inner join atrbmst on empmst.emp_attribute = atrbmst.atrb_id
inner join deptmst on empmst.emp_dept = deptmst.dept_id
inner join stmst on empmst.emp_st = stmst.st_id
inner join jobmst on empmst.emp_job = jobmst.job_id
left join classroom on empmst.emp_room = classroom.room_id
inner join authmst USING (emp_id)
left outer join authgroup on authmst.group_id = authgroup.group_id
inner join login USING (emp_id)
left outer join empcond USING (emp_id)
left outer join wktmgrp on empcond.tmcd_group_id = wktmgrp.group_id
left outer join atdptn on empcond.atdptn_id = atdptn.atdptn_id
and wktmgrp.group_id = atdptn.group_id
inner join option USING (emp_id)
left outer join	dispgroup on option.disp_group_id = dispgroup.group_id
left outer join menugroup on option.menu_group_id = menugroup.group_id
LEFT JOIN emp_relation USING (emp_id)
__SQL_END__;

    $cond = "order by emp_personal_id desc";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // �ܥǥ��Ԥ�����
    while ($row = pg_fetch_array($sel)) {
        $buf .= $row["group_nm"] . ",";
        $buf .= $row["disp_group_nm"] . ",";
        $buf .= $row["menu_group_nm"] . ",";
        $buf .= $row["emp_personal_id"] . ",";
        $buf .= $row["emp_login_id"] . ",";

        if ($hide_passwd === "f") {
            $buf .= $row["emp_login_pass"] . ",";
        }
        else {
            $buf .= "****,";
        }
        $buf .= $row["emp_lt_nm"] . ",";
        $buf .= $row["emp_ft_nm"] . ",";
        $buf .= $row["emp_kn_lt_nm"] . ",";
        $buf .= $row["emp_kn_ft_nm"] . ",";
        $buf .= $row["emp_login_mail"] . ",";
        $buf .= $row["emp_sex"] . ",";
        $buf .= $row["emp_birth"] . ",";
        $buf .= $row["emp_join"] . ",";
        $buf .= $row["emp_retire"] . ",";
        $buf .= $row["class_nm"] . ",";
        $buf .= $row["atrb_nm"] . ",";
        $buf .= $row["dept_nm"] . ",";
        if ($class_cnt == 4) {
            $buf .= $row["room_nm"] . ",";
        }
        $buf .= $row["st_nm"] . ",";
        $buf .= $row["job_nm"] . ",";
        $buf .= $row["emp_del_flg"] . ",";
        if (trim($row["emp_zip1"]) !== "" || trim($row["emp_zip2"]) !== "") {
            $buf .= trim($row["emp_zip1"]) . "-" . trim($row["emp_zip2"]) . ",";
        }
        else {
            $buf .= ",";
        }
        $buf .= prv2_change($row["emp_prv"]) . ",";
        $buf .= $row["emp_addr1"] . ",";
        $buf .= $row["emp_addr2"] . ",";
        if ($row["emp_tel1"]) {
            $buf .= $row["emp_tel1"] . "-" . $row["emp_tel2"] . "-" . $row["emp_tel3"] . ",";
        }
        else {
            $buf .= ",";
        }
        $buf .= $row["emp_email2"] . ",";

        if ($row["emp_mobile1"]) {
            $buf .= $row["emp_mobile1"] . "-";
            $buf .= $row["emp_mobile2"] . "-";
            $buf .= $row["emp_mobile3"] . ",";
        }
        else {
            $buf .= ",";
        }

        $buf .= $row["emp_m_email"] . ",";

        $emp_ext = str_replace(',', '.', $row["emp_ext"]);
        $buf .= $emp_ext . ",";
        $buf .= $row["emp_phs"] . ",";

        $buf .= $list_wage[$row["wage"]] . ",";
        $buf .= $row["group_name"] . ",";
        if ($row["duty_form"] == 1) {
            $buf .= "���";
        }
        else if ($row["duty_form"] == 2) {
            $buf .= "����";
        }
        if ($row["duty_form"] == 3) {
            $buf .= "û����������";
        }
        else {
            $buf .=" ";
        }

        // Ϣ���Բĥե饰
        if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
            $buf .= "," . $row["emp_name_deadlink_flg"];
            $buf .= "," . $row["emp_class_deadlink_flg"];
            $buf .= "," . $row["emp_job_deadlink_flg"];
            $buf .= "," . $row["emp_st_deadlink_flg"];
        }

        $buf .= "\r\n";
    }

    // Shift_JIS���Ѵ�
    return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}

function get_paid_hol_tbl_id_name($id)
{
    $arr_zenkaku = array("��", "��", "��", "��");
    $str = "";
    if ($id == "") {
        $str = "�ʤ�";
    }
    elseif ($id == 1) {
        $str = "���";
    }
    elseif ($id >= 2 && $id <= 5) {
        $wk_pos = 5 - $id;
        $str = "��" . $arr_zenkaku[$wk_pos] . "��";
    }
    return $str;
}

// ��ƻ�ܸ������ɤ���ƻ�ܸ�̾���Ѵ�
function prv2_change($prefecture_cd)
{
    $prefecture = array(
        '0' => '�̳�ƻ', '1' => '�Ŀ���', '2' => '��긩', '3' => '�ܾ븩', '4' => '���ĸ�',
        '5' => '������', '6' => 'ʡ�縩', '7' => '��븩', '8' => '���ڸ�', '9' => '���ϸ�',
        '10' => '��̸�', '11' => '���ո�', '12' => '�����', '13' => '�����', '14' => '���㸩',
        '15' => '�ٻ���', '16' => '���', '17' => 'ʡ�温', '18' => '������', '19' => 'Ĺ�',
        '20' => '���츩', '21' => '�Ų���', '22' => '���θ�', '23' => '���Ÿ�', '24' => '���츩',
        '25' => '������', '26' => '�����', '27' => 'ʼ�˸�', '28' => '���ɸ�', '29' => '�²λ���',
        '30' => 'Ļ�踩', '31' => '�纬��', '32' => '������', '33' => '���縩', '34' => '������',
        '35' => '���縩', '36' => '���', '37' => '��ɲ��', '38' => '���θ�', '39' => 'ʡ����',
        '40' => '���츩', '41' => 'Ĺ�긩', '42' => '���ܸ�', '43' => '��ʬ��', '44' => '�ܺ긩',
        '45' => '�����縩', '46' => '���츩'
    );

    return $prefecture[$prefecture_cd];
}
