<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="application_apply_detail.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?php
foreach ($filename as $tmp_filename) {
    echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
    echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
    $varname = "regist_emp_id$i";
    echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
    $varname = "precond_apply_id$i";
    echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
    $varname = "precond_wkfw_id$i";
    echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}


?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<? echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

</form>


<?

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("Cmx.php");
require_once("aclg_set.php");

$fname=$PHP_SELF;

$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if (mb_strlen($apply_title) > 80) {
    echo("<script type=\"text/javascript\">alert('{$title_label}が長すぎます。');</script>");
//  echo("<script type=\"text/javascript\">document.items.submit();</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$obj = new application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");


// 再申請チェック
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
if($apply_stat != "3") {
    echo("<script type=\"text/javascript\">alert(\"申請状況が変更されたため、再申請できません。\");</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// 新規申請(apply_id)採番
$sql = "select max(apply_id) from apply";
$cond = "";
$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
$max = pg_result($apply_max_sel,0,"max");
if($max == ""){
    $new_apply_id = "1";
}else{
    $new_apply_id = $max + 1;
}

// 新規申請番号(apply_no)採番
$date = date("Ymd");
$year = substr($date, 0, 4);
$md   = substr($date, 4, 4);

if($md >= "0101" and $md <= "0331")
{
    $year = $year - 1;
}
$max_cnt = $obj->get_apply_cnt_per_year($year);
$apply_no = $max_cnt + 1;


//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
    $ext = ".php";
    $savexmlfilename = "workflow/tmp/{$session}_x{$ext}";

    $arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
    $wkfw_content = $arr_wkfw_template_history["wkfw_content"];

    $wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
    $fp = fopen($savexmlfilename, "w");
    if (!$fp) {
        echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、申請してください。$savexmlfilename');</script>");
        echo("<script language='javascript'>history.back();</script>");
    }
    if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
        fclose($fp);
        echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、申請してください。');</script>");
        echo("<script language='javascript'>history.back();</script>");
    }
    fclose($fp);

    include( $savexmlfilename );

}

// 'エスケープ
$apply_title   = pg_escape_string($apply_title);
$apply_content = pg_escape_string($content);

// 申請登録
$obj->regist_re_apply($new_apply_id, $apply_id, $apply_content, $apply_title, "DETAIL");

//----- メディアテック Start -----
if (phpversion() >= '5.1') {
    $template_type = $_POST["kintai_template_type"];
    if (!empty($template_type)) {
        // MDB初期化
        require_once 'kintai/common/mdb_wrapper.php';
        require_once 'kintai/common/mdb.php';
        MDBWrapper::init();
        $mdb = new MDB();
        $mdb->beginTransaction();

        $class = null;
        switch ($template_type) {
            case "ovtm": // 時間外勤務命令の場合
                if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                    $class = WORKFLOW_OVTM_CLASS;
                }
                break;
            case "hol": // 休暇申請の場合
                // 総合届け使用フラグ
                $general_flag = file_exists("opt/general_flag");
                if ($general_flag) {
                    if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                        $class = WORKFLOW_HOL_CLASS;
                    }
                }
                //休暇申請
                else {
                    if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                        $class = WORKFLOW_HOL2_CLASS;
                    }
                }
                break;
            case "travel": // 旅行命令
                if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                    $class = WORKFLOW_TRVL_CLASS;
                }
                break;
            case "late": // 遅刻早退の場合
            	if ($_POST["late_early_type"] === "new") {
            		if (module_check(WORKFLOW_LATE2_MODULE, WORKFLOW_LATE2_CLASS)) {
            			$class = WORKFLOW_LATE2_CLASS;
            		}
            	}else{
	                if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
	                    $class = WORKFLOW_LATE_CLASS;
	                }
            	}
                break;
        }
        if (!empty($class)) {
            $module = new $class;
            $module->regist_re_apply($new_apply_id, $apply_id);
        }
    }
}
// モジュール・クラスの存在チェック
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}
//----- メディアテック End -----

// 承認登録
$obj->regist_re_applyapv($new_apply_id, $apply_id);


// 承認者候補登録
$obj->regist_re_applyapvemp($new_apply_id, $apply_id);


// 添付ファイル登録
$no = 1;
foreach ($filename as $tmp_filename)
{
    $obj->regist_applyfile($new_apply_id, $no, $tmp_filename);
    $no++;
}

// 非同期・同期受信登録
$obj->regist_re_applyasyncrecv($new_apply_id, $apply_id);


// 申請結果通知登録
if($notice_emp_id != "")
{
    $arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
    for($i=0; $i<count($arr_notice_emp_id); $i++)
    {
        $obj->regist_applynotice($new_apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
    }
}

// 前提とする申請書(申請用)登録
for($i=0; $i<$precond_num; $i++)
{
    $order = $i + 1;
    $varname = "precond_wkfw_id$order";
    $precond_wkfw_id = $$varname;

    $varname = "precond_apply_id$order";
    $precond_apply_id = $$varname;

    $obj->regist_applyprecond($new_apply_id, $precond_wkfw_id, $order, $precond_apply_id);
}

// 元の申請書に再申請ＩＤを更新
$obj->update_re_apply_id($apply_id, $new_apply_id);

// 合議者更新 2012.8.16
// 合議者は、事前登録の合議者を更新しない
$council_emp_id_list=array();
$council_del_emp_id_list=array();

$council_emp_id_list=explode(",",$council_emp_id);		// 更新後の合議者のリスト
$council_del_emp_id_list=explode(",",$council_del_emp_id);	// 更新前の合議者のリスト。この登録リストの合議者に削除フラグ設定して新しいnew_apply_idでINSERTする


$comment_list=array();
foreach($council_del_emp_id_list as $num=>$emp_id){
	if( $emp_id == "" ){
		continue;
	}
	$comment_list[$emp_id] = $obj->get_council_comment($emp_id,$apply_id); // 一旦削除フラグ…の前に当該のコメントを取り出す
}
$obj->update_delflg_applycouncil($apply_id, "t");	// 削除フラグ立てる

foreach($council_emp_id_list as $num=>$emp_id){
	if( $emp_id == "" ){
		continue;
	}
	$comment=$comment_list[$emp_id];
	if( $comment == "" ){
		$comment=NULL;
	}
	$obj->regist_apply_council_comment($new_apply_id, $emp_id , $comment);	// INSERT
}


// メール送信準備
$wkfw_send_mail_flg = get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
$to_addresses = array();
if ($wkfw_send_mail_flg == "t") {
    $apply = get_apply_by_apply_id($con, $new_apply_id, $fname);

    $sql = "select e.emp_email2, a.apv_order from applyapv a inner join empmst e on e.emp_id = a.emp_id";
    $cond = "where a.apply_id = $new_apply_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        if (must_send_mail($wkfw_send_mail_flg, $row["emp_email2"], $apply["wkfw_appr"], $row["apv_order"])) {
            $to_addresses[] = $row["emp_email2"];
        }
    }

    if (count($to_addresses) > 0) {
        $emp_detail = $obj->get_empmst_detail($apply["emp_id"]);
        $emp_nm = format_emp_nm($emp_detail[0]);
        $emp_mail_header = format_emp_mail_header($emp_detail[0]);
        $emp_mail = format_emp_mail($emp_detail[0]);
        $emp_pos = format_emp_pos($emp_detail[0]);
    }
}

//----- メディアテック Start   -----
if (phpversion() >= '5.1') {
    if (!empty($template_type)) {
        // 申請日を更新
        require_once 'kintai/common/workflow_common_util.php';
        WorkflowCommonUtil::apply_date_update($con, $new_apply_id);
        $mdb->endTransaction();
    }
}
//----- メディアテック End  -----
// トランザクションをコミット
pg_query($con, "commit");

// メール送信
if (count($to_addresses) > 0) {
    $approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";

    mb_internal_encoding("EUC-JP");
    mb_language("Japanese");

    $mail_subject = "[CoMedix] {$approve_label}依頼のお知らせ";
    $mail_content = format_mail_content($con, $wkfw_content_type, $content, $fname);
    $mail_separator = str_repeat("-", 60) . "\n";
    $additional_headers = "From: $emp_mail_header";
    $additional_parameter = "-f$emp_mail";

    foreach ($to_addresses as $to_address) {
        $mail_body = "以下の{$approve_label}依頼がありました。\n\n";
        $mail_body .= "申請者：{$emp_nm}\n";
        $mail_body .= "所属：{$emp_pos}\n";
        $mail_body .= "{$title_label}：{$apply_title}\n";
        if ($mail_content != "") {
            $mail_body .= $mail_separator;
            $mail_body .= "{$mail_content}\n";
        }

        mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
    }
}

// データベース接続を切断
pg_close($con);

if (!is_dir("apply")) {
    mkdir("apply", 0755);
}

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $tmp_filename = $filename[$i];
    $tmp_fileno = $i + 1;
    $ext = strrchr($tmp_filename, ".");

    $tmp_filename = "apply/tmp/{$session}_{$tmp_file_id}{$ext}";
    copy($tmp_filename, "apply/{$new_apply_id}_{$tmp_fileno}{$ext}");

}
foreach (glob("apply/tmp/{$session}_*.*") as $tmpfile) {
    unlink($tmpfile);
}





// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">close();</script>\n");

?>
</body>
