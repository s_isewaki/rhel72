<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("news_common.ini");
require_once("referer_common.ini");
require_once("mygroup_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 日付の設定
$news_end = get_default_end_date($con, $fname);
if ($deadline_date1 == "") {$deadline_date1 = date("Y", $news_end);}
if ($deadline_date2 == "") {$deadline_date2 = date("m", $news_end);}
if ($deadline_date3 == "") {$deadline_date3 = date("d", $news_end);}
if ($deadline_hour == "") {$deadline_hour = 0;}
if ($deadline_min == "") {$deadline_min = 0;}
if ($news_date1 == "") {$news_date1 = date("Y");}
if ($news_date2 == "") {$news_date2 = date("m");}
if ($news_date3 == "") {$news_date3 = date("d");}
if ($news_begin1 == "") {$news_begin1 = date("Y");}
if ($news_begin2 == "") {$news_begin2 = date("m");}
if ($news_begin3 == "") {$news_begin3 = date("d");}
if ($news_end1 == "") {$news_end1 = date("Y", $news_end);}
if ($news_end2 == "") {$news_end2 = date("m", $news_end);}
if ($news_end3 == "") {$news_end3 = date("d", $news_end);}

if ($news_login1 == "") {$news_login1 = date("Y");}
if ($news_login2 == "") {$news_login2 = date("m");}
if ($news_login3 == "") {$news_login3 = date("d");}
$news_login_end = get_default_end_date($con, $fname);
if ($news_login_end1 == "") {$news_login_end1 = date("Y", $news_login_end);}
if ($news_login_end2 == "") {$news_login_end2 = date("m", $news_login_end);}
if ($news_login_end3 == "") {$news_login_end3 = date("d", $news_login_end);}

// デフォルト値の設定
if ($back == "") {
    $show_login_flg = "t";
    $record_flg = get_default_record_flg($con, $fname);
    if ($record_flg == "f") {
        $comment_flg = "f";
    }
    else {
        $comment_flg = "t";
    }
    $public_flg = "f";
    $henshin_flg = "f";
}

if ($qa_cnt == "") {
    $qa_cnt = 1;
}

// 遷移元の取得
$referer = get_referer($con, $session, "news", $fname);

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// カテゴリ一覧を取得
//$category_list = get_category_list_from_db($con, $fname);
$category_list_db = get_category_list_from_db($con, $fname);
$category_list = array();
foreach ($category_list_db as $category_id => $category_name) {
    $category_list[$category_id] = $category_name["newscate_name"];
}

// 部署情報を配列で取得
$class_list = get_class_list($con, $fname);
$atrb_list = get_atrb_list($con, $fname);
$dept_list = get_dept_list($con, $fname);
$room_list = get_room_list($con, $fname);

// 職種一覧を取得
$job_list = get_job_list($con, $fname);

// 通達区分一覧を取得
$notice_list = get_notice_list($con, $fname);
$notice2_list = get_notice2_list($con, $fname);

// 役職一覧を取得
$st_list = get_st_list($con, $fname);

// ログインユーザの情報を取得
$sql = "select emp_class, emp_attribute, emp_dept, emp_room, emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$src_class_id = pg_fetch_result($sel, 0, "emp_class");
$src_atrb_id = pg_fetch_result($sel, 0, "emp_attribute");
$src_dept_id = pg_fetch_result($sel, 0, "emp_dept");
$src_room_id = pg_fetch_result($sel, 0, "emp_room");
$src_emp_id = pg_fetch_result($sel, 0, "emp_id");
$src_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

// お知らせ返信機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_henshin = $conf->get('setting_henshin');

// お知らせ集計機能設定を取得
$setting_aggregate = $conf->get('setting_aggregate');

// 職員一覧を表示する対象カテゴリ設定を取得（部署）
$setting_emplist_class = get_setting_emplist_class($con, $fname);

// 職員一覧を表示する対象カテゴリ（職種）
$setting_emplist_job = get_setting_emplist_job($con, $fname);

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if($setting_henshin != "t"){
    $setting_henshin = "f";
}
if($setting_aggregate != "t"){
    $setting_aggregate = "f";
}


// 初期表示時は当該職員を対象者に
if ($back != "t") {
    $target_id_list1 = $src_emp_id;
}

// 対象職員情報を配列に格納
$arr_target = array();
if ($target_id_list1 != "") {
    $arr_target_id = explode(",", $target_id_list1);
    for ($i = 0; $i < count($arr_target_id); $i++) {
        $tmp_emp_id = $arr_target_id[$i];
        $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        }
        $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
        array_push($arr_target, array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
    }
}

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $src_emp_id, $src_emp_id);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | お知らせ登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<?php
require("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
function initPage() {
<?php
    if (!is_array($class_id)) {
        echo("\tsetClassOptions(0);\n");
    } else {
        for ($i = 0, $j = count($class_id); $i < $j; $i++) {
            echo("\tsetClassOptions($i, '$class_id[$i]', '$atrb_id[$i]', '$dept_id[$i]');\n");
        }
    }
    if (!is_array($job_id)) {
        echo("\tsetJobOptions(0);\n");
    } else {
        for ($i = 0, $j = count($job_id); $i < $j; $i++) {
            echo("\tsetJobOptions($i, '$job_id[$i]');\n");
        }
    }
    if (!is_array($st_id)) {
        echo("\tsetStOptions(0);\n");
    } else {
        for ($i = 0, $j = count($st_id); $i < $j; $i++) {
            echo("\tsetStOptions($i, '$st_id[$i]');\n");
        }
    }
?>
    showSubCategory();
<?php if ($back == "") { ?>
    setShowLoginFlg();
<?php } ?>
    setSrcAtrbOptions('<?php echo($src_atrb_id); ?>', '<?php echo($src_dept_id); ?>', '<?php echo($src_room_id); ?>');
    //登録対象者を設定する。
    update_target_html('1');
    groupOnChange();
}

function setClassOptions(row_id, class_id, atrb_id, dept_id) {
    var table = document.getElementById('depts');
    if (row_id > table.rows.length - 1) {
        addDeptRow();
    }

    var class_elm;
    if (document.mainform.elements['class_id[]'].options) {
        class_elm = document.mainform.elements['class_id[]'];
    } else {
        class_elm = document.mainform.elements['class_id[]'][row_id];
    }

    deleteAllOptions(class_elm);
<?php
foreach ($class_list as $tmp_class_id => $tmp_class_nm) {
    echo("\taddOption(class_elm, '$tmp_class_id', '$tmp_class_nm', class_id);\n");
}
?>
    setAtrbOptions(row_id, atrb_id, dept_id);
}

function setAtrbOptions(row_id, atrb_id, dept_id) {
    var atrb_elm;
    if (document.mainform.elements['atrb_id[]'].options) {
        atrb_elm = document.mainform.elements['atrb_id[]'];
    } else {
        atrb_elm = document.mainform.elements['atrb_id[]'][row_id];
    }

    deleteAllOptions(atrb_elm);
    addOption(atrb_elm, '0', 'すべて', atrb_id);

    var class_id;
    if (document.mainform.elements['class_id[]'].options) {
        class_id = document.mainform.elements['class_id[]'].value;
    } else {
        class_id = document.mainform.elements['class_id[]'][row_id].value;
    }

    switch (class_id) {
<?php
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
    echo("\tcase '$tmp_class_id':\n");
    foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
        echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
    }
    echo("\t\tbreak;\n");
}
?>
    }

    setDeptOptions(row_id, dept_id);
}

function setDeptOptions(row_id, dept_id) {
    var dept_elm;
    if (document.mainform.elements['dept_id[]'].options) {
        dept_elm = document.mainform.elements['dept_id[]'];
    } else {
        dept_elm = document.mainform.elements['dept_id[]'][row_id];
    }

    deleteAllOptions(dept_elm);
    addOption(dept_elm, '0', 'すべて', dept_id);

    var atrb_id;
    if (document.mainform.elements['atrb_id[]'].options) {
        atrb_id = document.mainform.elements['atrb_id[]'].value;
    } else {
        atrb_id = document.mainform.elements['atrb_id[]'][row_id].value;
    }

    switch (atrb_id) {
<?php
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
    echo("\tcase '$tmp_atrb_id':\n");
    foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
        echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
    }
    echo("\t\tbreak;\n");
}
?>
    }
}
var std_cate_id = new Array();
function showSubCategory() {
    document.getElementById('sub_category1').style.display = 'none';
    document.getElementById('sub_category2').style.display = 'none';
    document.getElementById('sub_category3').style.display = 'none';

    switch (document.mainform.news_category.value) {
    case '1':  // 全館
        document.getElementById('target_emplist').style.display = 'none';
        document.getElementById('sub_category1').style.display = '';
        break;
    case '2':  // 部門
        document.getElementById('target_emplist').style.display = '<?php echo ($setting_emplist_class == 't') ? '' : 'none';?>';
        document.getElementById('sub_category2').style.display = '';
        break;
    case '3':  // 職種
        document.getElementById('target_emplist').style.display = '<?php echo ($setting_emplist_job == 't') ? '' : 'none';?>';
        document.getElementById('sub_category3').style.display = '';
        break;
    default :
        document.getElementById('target_emplist').style.display = 'none';
        break;
    }
    if (document.mainform.news_category.value == 10000) {
        document.getElementById('target_emplist').style.display = 'none';
        document.getElementById('st_row').style.display = 'none';
        document.getElementById('login_flg_row').style.display = 'none';
        document.getElementById('target_row').style.display = '';
    } else {
        document.getElementById('st_row').style.display = '';
        document.getElementById('login_flg_row').style.display = '';
        document.getElementById('target_row').style.display = 'none';

        document.mainform.comment_flg.value = 'f';
    }
    if (document.mainform.record_flg.value == 't') {
        document.mainform.comment_flg.value = 't';
    } else {
        document.mainform.comment_flg.value = 'f';
    }
    if (document.mainform.news_category.value <= 3) {
        document.getElementById('st_row').style.display = '';
    } else {
        document.getElementById('st_row').style.display = 'none';
    }
}

function addDeptRow() {
    var addbtn = document.getElementById('addbtn');
    addbtn.parentNode.removeChild(addbtn);

    var table = document.getElementById('depts');
    var row_id = table.rows.length;
    var row = table.insertRow(row_id);
    var cell = row.insertCell(0);
    cell.innerHTML = '<select name="class_id[]" onchange="setAtrbOptions(this.parentNode.parentNode.rowIndex);"><\/select><br><select name="atrb_id[]" onchange="setDeptOptions(this.parentNode.parentNode.rowIndex);"><\/select><br><select name="dept_id[]"><\/select><br><input type="button" value="削除" onclick="deleteDeptRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;"><input id="addbtn" type="button" value="追加" onclick="addDeptRow();" style="margin-left:2px;">';

    var class_id = document.mainform.elements['class_id[]'][row_id - 1].value;
    var atrb_id = document.mainform.elements['atrb_id[]'][row_id - 1].value;
    var dept_id = document.mainform.elements['dept_id[]'][row_id - 1].value;
    setClassOptions(row_id, class_id, atrb_id, dept_id);
}

function deleteDeptRow(row_id) {
    var table = document.getElementById('depts');
    table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

    if(!document.getElementById('addbtn')) {
        table.rows[table.rows.length - 1].cells[0].innerHTML += '<input id="addbtn" type="button" value="追加" onclick="addDeptRow();" style="margin-left:2px;">';
    }
}

function setSrcAtrbOptions(atrb_id, dept_id, room_id) {
    var atrb_elm = document.mainform.src_atrb_id;
    deleteAllOptions(atrb_elm);
    addOption(atrb_elm, '0', '　　　', atrb_id);

    var class_id = document.mainform.src_class_id.value;
    switch (class_id) {
<?php
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
    echo("\tcase '$tmp_class_id':\n");
    foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
        echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
    }
    echo("\t\tbreak;\n");
}
?>
    }

    setSrcDeptOptions(dept_id, room_id);
}

function setSrcDeptOptions(dept_id, room_id) {
    var dept_elm = document.mainform.src_dept_id;
    deleteAllOptions(dept_elm);
    addOption(dept_elm, '0', '　　　', dept_id);

    var atrb_id = document.mainform.src_atrb_id.value;
    switch (atrb_id) {
<?php
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
    echo("\tcase '$tmp_atrb_id':\n");
    foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
        echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
    }
    echo("\t\tbreak;\n");
}
?>
    }

    setSrcRoomOptions(room_id);
}

function setSrcRoomOptions(room_id) {
<?php if ($arr_class_name['class_cnt'] != 4) { ?>
    return;
<?php } ?>

    var room_elm = document.mainform.src_room_id;
    deleteAllOptions(room_elm);
    addOption(room_elm, '0', '　　　', room_id);

    var dept_id = document.mainform.src_dept_id.value;
    switch (dept_id) {
<?php
foreach ($room_list as $tmp_dept_id => $rooms_in_dept) {
    echo("\tcase '$tmp_dept_id':\n");
    foreach ($rooms_in_dept as $tmp_room_id => $tmp_room_nm) {
        echo("\t\taddOption(room_elm, '$tmp_room_id', '$tmp_room_nm', room_id);\n");
    }
    echo("\t\tbreak;\n");
}
?>
    }
}

function setJobOptions(row_id, job_id) {
    var table = document.getElementById('jobs');
    if (row_id > table.rows.length - 1) {
        addJobRow();
    }

    var job_elm;
    if (document.mainform.elements['job_id[]'].options) {
        job_elm = document.mainform.elements['job_id[]'];
    } else {
        job_elm = document.mainform.elements['job_id[]'][row_id];
    }

    deleteAllOptions(job_elm);
<?php
foreach ($job_list as $tmp_job_id => $tmp_job_nm) {
    echo("\taddOption(job_elm, '$tmp_job_id', '$tmp_job_nm', job_id);\n");
}
?>
}

function addJobRow() {
    var addbtn = document.getElementById('addjobbtn');
    addbtn.parentNode.removeChild(addbtn);

    var table = document.getElementById('jobs');
    var row_id = table.rows.length;
    var row = table.insertRow(row_id);
    var cell = row.insertCell(0);
    cell.innerHTML = '<select name="job_id[]"><\/select><input type="button" value="削除" onclick="deleteJobRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;"><input id="addjobbtn" type="button" value="追加" onclick="addJobRow();" style="margin-left:2px;">';

    var job_id = document.mainform.elements['job_id[]'][row_id - 1].value;
    setJobOptions(row_id, job_id);
}

function deleteJobRow(row_id) {
    var table = document.getElementById('jobs');
    table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

    if(!document.getElementById('addjobbtn')) {
        table.rows[table.rows.length - 1].cells[0].innerHTML += '<input id="addjobbtn" type="button" value="追加" onclick="addJobRow();" style="margin-left:2px;">';
    }
}

function attachFile() {
    window.open('news_attach.php?session=<?php echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
    if (e == undefined) {
        e = window.event;
    }

    var btn_id;
    if (e.target) {
        btn_id = e.target.getAttribute('id');
    } else {
        btn_id = e.srcElement.id;
    }
    var id = btn_id.replace('btn_', '');

    var p = document.getElementById('p_' + id);
    document.getElementById('attach').removeChild(p);
}

function selectEmployee() {
    window.open('news_employee_select.php?session=<?php echo($session); ?>&class_id='.concat(document.mainform.src_class_id.value), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    box.options[box.length] = opt;
    if (selected == value) {
        box.options[box.length - 1].selected = true;
    }
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function setStOptions(row_id, st_id) {
    var table = document.getElementById('sts');
    if (row_id > table.rows.length - 1) {
        addStRow();
    }

    var st_elm;
    if (document.mainform.elements['st_id[]'].options) {
        st_elm = document.mainform.elements['st_id[]'];
    } else {
        st_elm = document.mainform.elements['st_id[]'][row_id];
    }

    deleteAllOptions(st_elm);
<?php
    echo("\taddOption(st_elm, '', '　', st_id);\n");
foreach ($st_list as $tmp_st_id => $tmp_st_nm) {
    echo("\taddOption(st_elm, '$tmp_st_id', '$tmp_st_nm', st_id);\n");
}
?>
}

function addStRow() {
    var addbtn = document.getElementById('addstbtn');
    addbtn.parentNode.removeChild(addbtn);

    var table = document.getElementById('sts');
    var row_id = table.rows.length;
    var row = table.insertRow(row_id);
    var cell = row.insertCell(0);
    cell.innerHTML = '<select name="st_id[]"><\/select><input type="button" value="削除" onclick="deleteStRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;"><input id="addstbtn" type="button" value="追加" onclick="addStRow();" style="margin-left:2px;">';

    var st_id = document.mainform.elements['st_id[]'][row_id - 1].value;
    setStOptions(row_id, st_id);
}

function deleteStRow(row_id) {
    var table = document.getElementById('sts');
    table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

    if(!document.getElementById('addstbtn')) {
        table.rows[table.rows.length - 1].cells[0].innerHTML += '<input id="addstbtn" type="button" value="追加" onclick="addStRow();" style="margin-left:2px;">';
    }
}

function setShowLoginFlg() {
    if (document.mainform.news_category.value == '1') {
        document.mainform.show_login_flg.checked = true;
        document.getElementById('news_login').style.display = '';
    } else {
        document.mainform.show_login_flg.checked = false;
        document.getElementById('news_login').style.display = 'none';
    }
}

function changeNum(no) {
    document.mainform.action = "news_register.php#qa"+no;
    submitForm();
}

function changeItem(no,obj) {
    var idx = obj.selectedIndex;
   
    if(idx==0){
        document.getElementsByName('select_num_'+no)[0].disabled= false;
        document.getElementsByName('multi_flg_'+no)[0].disabled= false;
        document.getElementsByName('multi_flg_'+no)[1].disabled= false;
    }else if(idx==1){        
        document.mainform.action = "news_register.php#qa"+no;
        submitForm();
       
        
    }
}



function checkRecordFlg(flg) {
    disp = (flg) ? "" : "none";
    document.getElementById('enquet_set').style.display = disp;
    if (flg == true) {
        document.mainform.elements['comment_flg'][0].checked = true;
    } else {
        document.mainform.elements['comment_flg'][1].checked = true;
    }
}

function checkCommentFlg(flg) {
    if (flg) {
        if (document.mainform.elements['record_flg'][1].checked == true) {
            alert('コメントを許可するには、確認者を記録する設定が必要です');
            document.mainform.elements['comment_flg'][1].checked = true;
        }
    }
}

function addQaRow() {
    var addqabtn = document.getElementById('addqabtn');
    addqabtn.parentNode.removeChild(addqabtn);

    var table = document.getElementById('qas');
    var row_id = table.rows.length;
    var new_id = parseInt(document.mainform.qa_cnt.value) + 1;
    document.mainform.qa_cnt.value = new_id;
    var row = table.insertRow(row_id);
    var cell = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var select_num_menu = '回答形式&nbsp;<select name="select_changeItem_'+new_id+'" onChange="changeItem('+new_id+',this);"><option value="0">選択式</option><option value="1">自由記述式</option></select>&nbsp;&nbsp;選択肢数&nbsp;<select name="select_num_'+new_id+'" onChange="changeNum('+new_id+');"><option value="0"><\/option>';
    for (var i=2; i<=10; i++) {
        select_num_menu += '<option value="'+i+'">'+i+'<\/option>\n';
    }
    select_num_menu += '<\/select>&nbsp;';
    cell.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">質問'+new_id+'<\/font>&nbsp;<input type="text" name="question_'+new_id+'" value="" size="70" maxlength="100" style="ime-mode:active;">&nbsp;<br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'+select_num_menu+'複数選択&nbsp;<input type="radio" name="multi_flg_'+new_id+'" value="f" checked>不可&nbsp;<input type="radio" name="multi_flg_'+new_id+'" value="t">可<\/font>';
    cell2.innerHTML = '<input type="button" value="削除" onclick="deleteQaRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;"><input id="addqabtn" type="button" value="追加" onclick="addQaRow();" style="margin-left:2px;">';
    cell2.style.verticalAlign  = 'top';

}

function deleteQaRow(row_id) {
    var table = document.getElementById('qas');
    table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

    if(!document.getElementById('addqabtn')) {
        table.rows[table.rows.length - 1].cells[1].innerHTML += '<input id="addqabtn" type="button" value="追加" onclick="addQaRow();" style="margin-left:2px;">';
    }
}
function setShowCommentFlg() {
    if (document.mainform.elements['record_flg'][0].checked == true) {
        document.mainform.elements['comment_flg'][0].checked = true;
    } else {
        document.mainform.elements['comment_flg'][1].checked = true;
    }
}

var childwin = null;
function openEmployeeList(item_id) {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$src_emp_id?>';
    url += '&mode=17';
    url += '&item_id='+item_id;
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?php
    for ($i = 1; $i <= 1; $i++) {
    $script = "m_target_list['$i'] = new Array(";
    $is_first = true;
    foreach ($arr_target as $row) {
        if ($is_first) {
            $is_first = false;
        }
        else {
            $script .= ",";
        }
        $tmp_emp_id = $row["id"];
        $tmp_emp_name = $row["name"];
        $script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
    }
    $script .= ");\n";
    print $script;
}
?>

// クリア
function clear_target(emp_id,emp_name) {
    if(confirm("回覧対象者を削除します。よろしいですか？"))
    {
        var is_exist_flg = false;
        for(var i=0;i<m_target_list['1'].length;i++)
        {
            if(emp_id == m_target_list['1'][i].emp_id)
            {
                is_exist_flg = true;
                break;
            }
        }
        m_target_list['1'] = new Array();
        if (is_exist_flg == true) {
            m_target_list['1'] = array_add(m_target_list['1'],new user_info(emp_id,emp_name));
        }
        update_target_html('1');
    }
}

function selectAllEmp() {
    for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
        document.mainform.emplist.options[i].selected = true;
    }
}

function getSelectedValue(sel) {
    return sel.options[sel.selectedIndex].value;
}

function groupOnChange() {
    var group_id = getSelectedValue(document.mainform.mygroup);

    // 職員一覧セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.emplist);

    // 職員一覧セレクトボックスのオプションを作成
<?php foreach ($employees as $tmp_group_id => $arr) { ?>
    if (group_id == '<?php echo $tmp_group_id; ?>') {
    <?php foreach($arr as $tmp_emp) { ?>
        addOption(document.mainform.emplist, '<?php echo $tmp_emp["emp_id"]; ?>', '<?php echo $tmp_emp["name"]; ?>');
    <?php } ?>
    }
<?php } ?>
}

function addEmp() {
    var emp_id_str = "";
    var emp_name_str = "";
    var first_flg = true;
    for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
        if (document.mainform.emplist.options[i].selected) {
            var emp_id = document.mainform.emplist.options[i].value;
            var emp_name = document.mainform.emplist.options[i].text;
            if (first_flg == true) {
                first_flg = false;
            } else {
                emp_id_str += ", ";
                emp_name_str += ", ";
            }
            emp_id_str += emp_id;
            emp_name_str += emp_name;
        }
    }
    if (emp_id_str != "") {
        set_wm_counter(emp_id_str);
        add_target_list('1', emp_id_str, emp_name_str);
    }
}

function set_wm_counter(ids)
{

    var url = 'emplist_address_counter.php';
    var params = $H({'session':'<?=$session?>','emp_ids':ids}).toQueryString();
    var myAjax = new Ajax.Request(
        url,
        {
            method: 'post',
            postBody: params
        });
}
</script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
if(!tinyMCE.isOpera)
{
    tinyMCE.init({
        //mode : "textareas",
        mode : "exact",
        elements : "content_text",
        theme : "advanced",
        plugins : "preview,table,emotions,fullscreen,layer,paste",
        //language : "ja_euc-jp",
        language : "ja",
        width : "100%",
        height : "300",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
        theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
        theme_advanced_buttons3 : "tablecontrols,|,visualaid,|,code,pasteword",
        content_css : "tinymce/tinymce_content.css",
        theme_advanced_statusbar_location : "none",
        force_br_newlines : true,
        forced_root_block : '',
        force_p_newlines : false
    });
}

<?php write_js_set_end_date(); ?>
<?php write_login_js_set_end_date(); ?>
function change(login_flg){
    var chk = login_flg.checked;
    var display = '';
    if (!chk) {
        display = 'none';
    }
    document.getElementById('news_login').style.display = display;
}

function emplist_popup() {
    var target = 'ATMARK';
    var dx = screen.availWidth - 10
    var dy = screen.top;
    var base = 0;
    var wx = 720;
    var wy = 600;
    window.open("", target, 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    // formを生成
    var frmList = document.createElement("form");
    var category = document.mainform.news_category.value;
    var session = document.mainform.session.value;

    frmList.name = 'frmList';
    frmList.action = 'news_employee_list.php';
    frmList.target = target;
    frmList.method = 'post';
    switch(category) {
        case "2":
            var arr_class_id = document.mainform.elements['class_id[]'];
            if (typeof arr_class_id.value == 'string' && arr_class_id.value != '') {
                //make_hidden('class_id',0,arr_class_id.selectedIndex,frmList);
                make_hidden('class_id',0,arr_class_id.value,frmList);
            } else {
                for(var i = 0; i < arr_class_id.length; i++) {
                    make_hidden('class_id',i,arr_class_id[i].value,frmList);
                }
            }
            var arr_atrb_id = document.mainform.elements['atrb_id[]'];
            if (typeof arr_atrb_id.value == 'string' && arr_atrb_id.value != '') {
                make_hidden('atrb_id',0,arr_atrb_id.value,frmList);
            } else {
                for(var i = 0; i < arr_atrb_id.length; i++) {
                    make_hidden('atrb_id',i,arr_atrb_id[i].value,frmList);
                }
            }
            var arr_dept_id = document.mainform.elements['dept_id[]'];
            if (typeof arr_dept_id.value == 'string' && arr_dept_id.value != '') {
                make_hidden('dept_id',0,arr_dept_id.value,frmList);
            } else {
                for(var i = 0; i < arr_dept_id.length; i++) {
                    make_hidden('dept_id',i,arr_dept_id[i].value,frmList);
                }
            }

            var arr_st_id = document.mainform.elements['st_id[]'];
            if (typeof arr_st_id.value == 'string' && arr_st_id.value != '') {
                make_hidden('st_id',0,arr_st_id.value,frmList);
            } else {
                for(var i = 0; i < arr_st_id.length; i++) {
                    make_hidden('st_id',i,arr_st_id[i].value,frmList);
                }
            }
            break;
        case "3":
            var arr_job_id = document.mainform.elements['job_id[]'];
            if (typeof arr_job_id.value == 'string' && arr_job_id.value != '') {
                make_hidden('job_id',0,arr_job_id.value,frmList);
            } else {
                for(var i = 0; i < arr_job_id.length; i++) {
                    make_hidden('job_id',i,arr_job_id[i].value,frmList);
                }
            }

            var arr_st_id = document.mainform.elements['st_id[]'];
            if (typeof arr_st_id.value == 'string' && arr_st_id.value != '') {
                make_hidden('st_id',0,arr_st_id.value,frmList);
            } else {
                for(var i = 0; i < arr_st_id.length; i++) {
                    make_hidden('st_id',i,arr_st_id[i].value,frmList);
                }
            }
            break;
        default :
            break;
    }

    make_hidden('category',0,category,frmList);
    make_hidden('session',0,session,frmList);

    // formをbodyに追加して、サブミットする。その後、formを削除
    var body = document.getElementsByTagName("body")[0];
    body.appendChild(frmList);
    submitForm(frmList);
    body.removeChild(frmList);
}

function make_hidden(name,i,val,frm){
    var q = document.createElement('input');
    q.type = 'hidden';
    q.name = name+'['+i+']';
    q.value = val;
    frm.appendChild(q);
}

var skipUnloadConfirm = false;

window.onbeforeunload = function (e) {
    if (skipUnloadConfirm) {
        skipUnloadConfirm = false;
        return;
    }

    var content;
    if (!tinyMCE.isOpera) {
        content = tinyMCE.get('news').getContent();
    } else {
        content = document.mainform.news.value;
    }
    if (content.length == 0) return;

    // ブラウザによってはこのメッセージは使われない
    var message = '入力内容は保存されません。';
    if (!e) e = window.event;
    if (e) e.returnValue = message;
    return message;
}

function submitForm(form) {
    if (!form) form = document.mainform;
    skipUnloadConfirm = true;
    form.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
p.attach {margin:0;}
table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<?php if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<?php echo($session); ?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>
<?php } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<?php echo($session); ?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<?php echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<?php echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<?php } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="90" height="22" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="news_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>お知らせ登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="5">&nbsp;</td>
<?php //オプション設定で集計機能を許可するにチェックを入れることで、タブに集計表が表示される。?>
<?php if($setting_aggregate == 't'){?>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_aggregate.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font></a></td>
<?php } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10">

<form name="mainform" action="news_insert.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="24%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td width="44%"><input type="text" name="news_title" value="<?php eh($news_title); ?>" size="50" maxlength="100" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" width="20%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラインマーカー</font></td>
<td width="12%">
<select name="marker">
<option value="0" style="background-color:white;color:black;"<?php if ($marker == 0) {echo(" selected");} ?>>なし
<option value="1" style="background-color:red;color:white;"<?php if ($marker == 1) {echo(" selected");} ?>>赤
<option value="2" style="background-color:aqua;color:blue;"<?php if ($marker == 2) {echo(" selected");} ?>>青
<option value="3" style="background-color:yellow;color:blue;"<?php if ($marker == 3) {echo(" selected");} ?>>黄
<option value="4" style="background-color:lime;color:blue;"<?php if ($marker == 4) {echo(" selected");} ?>>緑
<option value="5" style="background-color:fuchsia;color:white;"<?php if ($marker == 5) {echo(" selected");} ?>>ピンク
</select>
</td>
</tr>
<tr height="28">
<td valign="top" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ<br /><a href="javascript:void(0);" id="target_emplist" style="display:none;" onclick="emplist_popup();">対象者</a></font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="1">
<tr valign="top">
<td style="padding-right:4px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="news_category" onchange="showSubCategory();setShowLoginFlg();setShowCommentFlg();"><?php show_options($category_list, $news_category); ?></select></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="sub_category1" style="display:none;"></div>
<div id="sub_category2" style="display:none;">
<table id="depts" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <select name="class_id[]" onchange="setAtrbOptions(0);"></select><br>
    <select name="atrb_id[]" onchange="setDeptOptions(0);"></select><br>
    <select name="dept_id[]"></select><br>
    <input type="button" value="削除" disabled style="margin-left:2px;"><input id="addbtn" type="button" value="追加" onclick="addDeptRow();" style="margin-left:2px;">
</td>
</tr>
</table>
</div>
<div id="sub_category3" style="display:none;">
<table id="jobs" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><select name="job_id[]"><?php show_options($job_list, $job_id); ?></select><input type="button" value="削除" disabled style="margin-left:2px;"><input id="addjobbtn" type="button" value="追加" onclick="addJobRow();" style="margin-left:2px;"></td>
</tr>
</table></div>
</font></td>
</tr>
</table>
</td>
</tr>
<tr height="22" id="st_row">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象役職</font></td>
<td colspan="3">
<table id="sts" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<select name="st_id[]"></select><input type="button" value="削除" disabled style="margin-left:2px;"><input id="addstbtn" type="button" value="追加" onclick="addStRow();" style="margin-left:2px;"></td>
</tr>
</table>
</td>
</tr>
<tr height="22" id="login_flg_row">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインページ</font></td>
<td colspan="3"><input type="checkbox" name="show_login_flg" value="t"<?php if ($show_login_flg == "t") {echo(" checked");} ?> onclick="change(this)"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示する</font></td>
</tr>
<tr height="22" id="target_row">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回覧対象者</font><br>
<input type="button" value="職員名簿" style="margin-left:2em;width:5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" value="クリア" style="margin-left:2em;width:5.5em;" onclick="clear_target('<?=$src_emp_id?>','<?=$src_emp_nm?>');"><br></td>
<td colspan="3">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area1"></span>
</font>

</td>
<td>
<input type="button" value="&lt;&nbsp;追加" style="margin-left:1em;width:4.5em;" onclick="addEmp();">
</td>
<td width="10">
</td>
<td>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="1" class="non_in_list">
<tr>
<td height="45%" align="center" valign="bottom">

<select name="mygroup" onchange="groupOnChange();">
<?php
 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
?>
<option value="<?=$tmp_mygroup_id?>"
<?php
        if ($mygroup == $tmp_mygroup_id) {
            echo(" selected");
        }
?>
><?=$tmp_mygroup?></option>
<?php
}
?>
</selelct>
</td></tr>
<tr><td>
<select name="emplist" size="10" multiple style="width:150px;">
</select>

</td>
</tr>
<tr>
<td height="45%" valign="top"><input type="button" value="全て選択" onclick="selectAllEmp();"></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
<td colspan="3"><select name="notice_id"><?php show_options($notice_list, '', true); ?></select></td>
</tr>
<?php if (!empty($notice2_list)) {?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分</font></td>
<td colspan="3"><select name="notice2_id"><?php show_options($notice2_list, '', true); ?></select></td>
</tr>
<?php }?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発信部署</font></td>
<td colspan="3"><select name="src_class_id" onchange="setSrcAtrbOptions();"><?php show_options($class_list, $src_class_id, true); ?></select><select name="src_atrb_id" onchange="setSrcDeptOptions();"></select><select name="src_dept_id" onchange="setSrcRoomOptions();"></select><?php if ($arr_class_name['class_cnt'] == 4) { ?><select name="src_room_id"></select><?php } ?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="selectEmployee();">発信者</a></font></td>
<td colspan="3">
<input type="text" name="src_emp_nm" value="<?php echo($src_emp_nm); ?>" size="30" disabled>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="document.mainform.src_emp_nm.value = ''; document.mainform.src_emp_id.value = ''; document.mainform.hid_src_emp_nm.value = '';">クリア</a></font>
<input type="hidden" name="src_emp_id" value="<?php echo($src_emp_id); ?>">
<input type="hidden" name="hid_src_emp_nm" value="<?php echo($src_emp_nm); ?>">
</td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td colspan="3" style="height: 320px;">
<textarea id="content_text" name="news" cols="40" rows="12" style="ime-mode:active;"><?php echo(h($news)); ?></textarea>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="attach">
<?php
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $tmp_filename = $filename[$i];
    $ext = strrchr($tmp_filename, ".");

    echo("<p id=\"p_{$tmp_file_id}\" class=\"attach\">\n");
    echo("<a href=\"news/tmp/{$session}_{$tmp_file_id}{$ext}?t=".date('YmdHis')."\" target=\"_blank\">{$tmp_filename}</a>\n");
    echo("<input type=\"button\" id=\"btn_{$tmp_file_id}\" name=\"btn_{$tmp_file_id}\" value=\"削除\" onclick=\"detachFile(event);\">\n");
    echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
    echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
    echo("</p>\n");
}
?>
</div>
<input type="button" value="追加" onclick="attachFile();">
</font></td>
</tr>
<tr height="22">
    <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>確認者を記録する</nobr></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="record_flg" value="t"<?php if ($record_flg == "t") {echo(" checked");} ?> onClick="checkRecordFlg(true);">する&nbsp;<input type="radio" name="record_flg" value="f"<?php if ($record_flg == "f") {echo(" checked");} ?> onClick="checkRecordFlg(false);">しない</font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コメント</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="comment_flg" value="t"<?php if ($comment_flg == "t") {echo(" checked");} ?> onClick="checkCommentFlg(true);">許可する&nbsp;<input type="radio" name="comment_flg" value="f"<?php if ($comment_flg == "f") {echo(" checked");} ?> onClick="checkCommentFlg(false);">許可しない</font></td>
</tr>

<?php
//オプション機能で返信機能を許可するにチェックを入れることで返信ボタンの項目が表示される。
if($setting_henshin == 't')
{
?>
    <tr height="22">
        <td align="right" bgcolor="#f6f9ff">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">返信ボタン</font>
        </td>
        <td colspan="3">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="henshin_flg" value="t"<?php if ($henshin_flg == "t") {echo(" checked");} ?>>表示する&nbsp;<input type="radio" name="henshin_flg" value="f"<?php if ($henshin_flg == "f") {echo(" checked");} ?>>表示しない</font>
        </td>
    </tr>
<?php
}
?>

<tr height="22" id="enquet_set" style="display:<?php
$disp_enquet_set = ($record_flg != "f") ? "" : "none";
echo($disp_enquet_set);
?>">
<td align="right" valign="top" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アンケート</font></td>
<td colspan="3">
<?php
// 質問用テーブル
// qa_cntは追加ボタンクリック毎にカウントしID付加用として使用
?>
<table id="qas" width="100%" border="0" cellspacing="0" cellpadding="2">
<?php
// データ設定、配列は1番目から使用
$question = array();
$select_num = array();
$multi_flg = array();
$answer = array();
$answer_comment_flg = array();
if ($back == "") {
    for ($i = 1; $i <= 10; $i++) {
        $answer[1][$i] = "";
    }
    $multi_flg[1] = "f";
}
$dst_idx = 1;
for ($src_idx=1; $src_idx<=$qa_cnt; $src_idx++) {
    $v_num_item = "select_changeItem_".$src_idx;
    
    if($$v_num_item=='0'){
        $v_num_src = "select_num_".$src_idx;
        if ($$v_num_src == "") {
            continue;
        }
        $select_num[$dst_idx] = $$v_num_src;
        $v_question_src = "question_".$src_idx;
        $question[$dst_idx] = $$v_question_src;
        
        $v_mult_src = "multi_flg_".$src_idx;
        $multi_flg[$dst_idx] = $$v_mult_src;
        if ($multi_flg[$dst_idx] == "") {
            $multi_flg[$dst_idx] = "f";
        }

        for ($i = 1; $i <= $$v_num_src; $i++) {
            $varname = "answer".$src_idx."_".$i;
            $answer[$dst_idx][$i] = $$varname;
            $v_com_src = "answer_comment_flg" . $src_idx . "_" . $i;
            $answer_comment_flg[$dst_idx][$i] = $$v_com_src;
        }
        $dst_idx++;
    }else if($$v_num_item=='1'){
        $v_question_src = "question_".$src_idx;
        $question[$dst_idx] = $$v_question_src;
        $multi_flg[$dst_idx] = "f";
        $dst_idx++;
    }
    
}
$qa_cnt = $dst_idx - 1;
if ($qa_cnt == 0) {
    $qa_cnt = 1;
}
// 質問数分表示
for ($qa_idx=1; $qa_idx<=$qa_cnt; $qa_idx++) {

?>
<tr>
<td>
<a name="qa<?=$qa_idx?>"></a>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">質問<?=$qa_idx?>&nbsp;<input type="text" name="question_<?=$qa_idx?>" value="<?=$question[$qa_idx]?>" size="70" maxlength="100" style="ime-mode:active;">
<br>
回答形式&nbsp;
<select name="select_changeItem_<?=$qa_idx?>" onchange="changeItem(<?=$qa_idx?>,this);">

<?php $select_num_tmp = "select_changeItem_".$qa_idx ?>
<option value="0" <?php if($$select_num_tmp ==0)  echo(" selected"); ?>>選択式</option>
<option value="1" <?php if($$select_num_tmp ==1) echo(" selected"); ?>>自由記載式</option>
</select>
&nbsp;選択肢数<select  name="select_num_<?=$qa_idx?>" onChange="changeNum(<?=$qa_idx?>);" <?php if($$select_num_tmp == 1) echo(" disabled");?>><option value="0"></option>
<?php
for ($i = 2; $i <= 10; $i++) {
    echo("<option value=\"$i\"");
    if ($i == $select_num[$qa_idx]) {
        echo(" selected");
    }
    echo(">$i</option>\n");
}
?>
</select>&nbsp;複数選択&nbsp;<input type="radio" name="multi_flg_<?=$qa_idx?>" value="f"<?php if ($multi_flg[$qa_idx] == "f") {echo (" checked");}  if($$select_num_tmp == 1) echo(" disabled");?>>不可&nbsp;<input type="radio" name="multi_flg_<?=$qa_idx?>" value="t"<?php if ($multi_flg[$qa_idx] == "t") {echo (" checked");} if($$select_num_tmp == 1) echo(" disabled"); ?>>可
</font>
<br>
<?php
// 表示
if ($select_num[$qa_idx] > 0) {
?>
<table width="258" border="0" cellspacing="0" cellpadding="2" class="block2">
<tr>
<td width="40" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項番</font></td>
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入力欄</font></td>
</tr>
<?php
    //項番の数
    for ($i = 1; $i <= $select_num[$qa_idx]; $i++) {
        echo("<tr>\n");
        echo("<td height=\"22\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$i</font></td>\n");
        echo("<td><input type=\"text\" name=\"answer{$qa_idx}_{$i}\" value=\"{$answer[$qa_idx][$i]}\" size=\"40\" style=\"ime-mode:active;\">\n");
        echo("<td><input type=\"checkbox\" name=\"answer_comment_flg{$qa_idx}_{$i}\" value=\"t\"");
        if ($answer_comment_flg[$qa_idx][$i] == 't') {
            echo " checked";
        }
        echo(">\n");
        echo("</td>\n");
        echo("</tr>\n");
                
    }
?>
</table>
<?php
}
// end of if ($select_num[$qa_idx] > 0)
?>
<?php
// 表示
$select_num_tmp = "select_changeItem_".$qa_idx;
if ($select_num[$qa_idx] == 0 &&  $$select_num_tmp==1) {
    echo("<textarea rows='5' cols='50' readonly>回答欄</textarea>");
  
}
?>
</td>
<td valign="top">
<input type="button" value="削除" onclick="deleteQaRow(this.parentNode.parentNode.rowIndex);" <?php if ($qa_idx == 1) {echo("disabled");} ?> style="margin-left:2px;"><?php if ($qa_idx == $qa_cnt) { ?><input type="button" id="addqabtn" value="追加" onclick="addQaRow();" style="margin-left:2px;">
<?php } ?>
</td>
</tr>
<?php
}
// 質問用テーブル終わり
?>
</table>

<table border="0" cellspacing="0" cellpadding="2">
<tr><td><img src="img/spacer.gif" alt="" width="1" height="2"></td></tr>
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">締切り日時&nbsp;</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="deadline_date1"><?php show_select_years_future(2, $deadline_date1); ?></select>/<select name="deadline_date2"><?php show_select_months($deadline_date2); ?></select>/<select name="deadline_date3"><?php show_select_days($deadline_date3); ?></select>&nbsp;&nbsp;<select name="deadline_hour"><?php show_select_hrs_0_23($deadline_hour); ?></select>:<select name="deadline_min"><?php show_select_min_30($deadline_min); ?></select><br></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計結果の公開&nbsp;</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="public_flg" value="f"<?php if ($public_flg == "f") {echo (" checked");} ?>>非公開&nbsp;<input type="radio" name="public_flg" value="t"<?php if ($public_flg == "t") {echo (" checked");} ?>>公開</font></td>
</tr>
</table>
<input type="hidden" name="qa_cnt" value="<?php echo($qa_cnt); ?>">

</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="news_begin1"><?php show_select_years_future(2, $news_begin1); ?></select>/<select name="news_begin2"><?php show_select_months($news_begin2); ?></select>/<select name="news_begin3"><?php show_select_days($news_begin3); ?></select> 〜 <select name="news_end1"><?php show_select_years_future(2, $news_end1); ?></select>/<select name="news_end2"><?php show_select_months($news_end2); ?></select>/<select name="news_end3"><?php show_select_days($news_end3); ?></select><?php write_term_select_box($term) ?></font></td>
</tr>
<tr height="22" id="news_login" <?php if ($show_login_flg !== "t") {echo("style=\"display: none;\"");} ?>>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間（ログイン画面）</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="news_login1"><?php show_select_years_future(2, $news_login1); ?></select>/<select name="news_login2"><?php show_select_months($news_login2); ?></select>/<select name="news_login3"><?php show_select_days($news_login3); ?></select> 〜 <select name="news_login_end1"><?php show_select_years_future(2, $news_end1); ?></select>/<select name="news_login_end2"><?php show_select_months($news_end2); ?></select>/<select name="news_login_end3"><?php show_select_days($news_end3); ?></select><?php write_login_term_select_box($login_term) ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="news_date1"><?php show_select_years_future(2, $news_date1); ?></select>/<select name="news_date2"><?php show_select_months($news_date2); ?></select>/<select name="news_date3"><?php show_select_days($news_date3); ?></select></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="登録" onclick="submitForm();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="frompage" value="2">
<input type="hidden" name="back" value="t">
<input type="hidden" name="emp_id" value="<?php echo($src_emp_id); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">

<?php if($setting_henshin != 't') { ?>
<input type="hidden" name="henshin_flg"    value="f">
<?php } ?>

</form>
</body>
</html>
<?php pg_close($con);