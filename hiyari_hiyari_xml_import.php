<?php

// セッションの開始
session_start();

// ==================================================
// ファイルの読込み
// ==================================================
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("hiyari_common.ini");
require_once("hiyari_post_select_box.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_mail.ini");
require_once("hiyari_mail_input.ini");
require_once("hiyari_wf_utils.php");

require_once("get_values.php");
require_once("hiyari_header_class.php");

require_once('class/Cmx/Model/SystemConfig.php');

define("MAX_READ_NUMBER", 2000);

// ==================================================
// 初期処理
// ==================================================

// 画面名
$fname = $PHP_SELF;

// 一時的にXMLファイルを格納するフォルダ
$tmp_dir = "./inci/xml/";

// 画面に表示する最大件数
$disp_count_max_in_page = 20;

// モード
$mode = $_POST["mode"];

// ページ番号
$page = $_POST["page"];
if (!($page >= 1 && $page != "")) {
	$page = 1;
}

// ==================================================
// セッションのチェック
// qualify_session() from about_session.php
// ==================================================
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// 権限チェック
// check_authority() from about_authority.php
// ==================================================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

// ==================================================
// DBコネクション取得
// connect2db() from about_postgres.php
// ==================================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// ==================================================
// セクション名(部門・課・科・室)取得
// get_class_name_array() from show_class_name.ini
// ==================================================
$arr_class_name = get_class_name_array($con, $fname);

// ==================================================
// 職員ID取得
// get_emp_id() from get_values.php
// ==================================================
$emp_id = get_emp_id($con, $session, $fname);

// ==================================================
// 職員情報取得
// get_empmst() from get_values.php
// ==================================================
$emp_info = get_empmst($con, $emp_id, $fname);
$registrant_class = $emp_info[2];
$registrant_attribute = $emp_info[3];
$registrant_dept = $emp_info[4];
$registrant_room = $emp_info[33];

// ==================================================
// インスタンス作成
// hiyari_report_class() from hiyari_report_class.php
// ==================================================
$rep_obj = new hiyari_report_class($con, $fname);

$under_disp_flg = false;		// 画面下部表示フラグ
$under_disp_message = "";		// 画面下部表示メッセージ
$verification_flg = false;		// 検証結果フラグ

// 同じ画面から遷移された場合
if ($mode == "import") {
	$xml_file_name = "";

	// ==================================================
	// インポート処理
	// import_xml_file() from self
	// ==================================================
	$xml_file_name = import_xml_file($_FILES, $tmp_dir);

	// インポート失敗
	if ($xml_file_name == "") {
		$under_disp_message = "XMLファイルの読込みに失敗しました。";

	// インポート成功
	} elseif ($xml_file_name != "") {
		// ==================================================
		// DTDファイルによる検証
		// verification_xml_file() from self
		// ==================================================
		$verification_flg =true;

		// 検証成功
		if ($verification_flg == true) {
			// ==================================================
			// xmlデータを連想配列に格納
			// convert_xml_data() from self
			// ==================================================
                     
			$xml_arr = convert_xml_data($tmp_dir . $xml_file_name);

			// ==================================================
			// XMLデータの必須チェック
			// indispensable_check() from self
			// ==================================================
			$indispensable_arr = indispensable_check($xml_arr);

			// 必須チェッククリア
			if (count($indispensable_arr) == 0  && $under_disp_flg==false) {
				// 初期化
				$_SESSION["HIYARI_HIYARI_XML_IMPORT"] = array();

				// 件数初期化
				$total_cnt = 0;		// 処理件数			：総件数
				$import_cnt = 0;	// インポート件数	：置換含め成功した件数
				$replace_cnt = 0;	// 置換件数			：置換のみの件数 [重複データの処理にて、"インポートする（置換）"を選択]
				$error_cnt = 0;		// 重複エラー		：はじいた件数 [重複データの処理にて、"インポートしない"を選択]

				// 画面データ取得
				$repetition_rb = $_POST["repetition_rb"];	// 重複チェック対象 [1:軽 2:重]
				$replace_rd = $_POST["replace_rd"];			// 重複データ処理方法

				// GRPCASEREPORT毎にループ
				foreach ($xml_arr["CASEREPORT"][0]["GRPCASEREPORT"] as $grpcasereport) {
					// ==================================================
					// 重複チェック
					// check_repetition() from self
					// ==================================================
					$id_arr = check_repetition($con, $fname, $emp_id, $repetition_rb, $grpcasereport, $_POST);

					// 配列から件数を取得
					if (is_array($id_arr) == true) {
						$duplication_cnt = count($id_arr);
					} else {
						$duplication_cnt = 0;
					}

					// 重複2件以上で、処理がインポートする（置換）を選択された場合
					if ($duplication_cnt >= 2 && $replace_rd == "2") {
						// 重複チェック対象 [1:軽 2:重]
						$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_TYPE"] = $repetition_rb;

						// ポストデータの保持
						$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"] = $_POST;

						// 重複したレポートデータをセッション変数に格納 [XMLデータ]
						$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_REPORT"][] = $grpcasereport;

						// 重複チェックで取得したID配列を格納 [データベース]
						$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_ID_ARRAY"][] = $id_arr;

						continue;

					} else {
						// 処理件数インクリメント
						$total_cnt++;

						// 重複有
						if ($duplication_cnt > 0) {
							$chk_num = "2";
						// 重複無
						} else if ($duplication_cnt == 0) {
							$chk_num = "1";
						}
					}

					// 重複していて、処理がインポートする（置換）を選択された場合
					if ($chk_num == "2" && $replace_rd == "2") {
						$chk_num = "3";
					}

					if ($chk_num == "1") {
						$process_division = "insert";
					} elseif ($chk_num == "3") {
						$process_division = "update";
					}

					// 重複チェックの結果により分岐
					switch ($chk_num) {
						// 重複している
						// 重複データ処理 [インポートする（置換）：replace_rd="2"]
						case "3":
							// 置換件数インクリメント
							$replace_cnt++;

							// break;しません

						// 重複していない
						case "1":
							// インポート件数インクリメント
							$import_cnt++;

							// トランザクションの開始
							pg_query($con, "begin transaction");

							// ==================================================
							// 報告書反映
							// set_report_from_xml() from self
							// ==================================================
							set_report_from_xml($con, $fname, $session, $process_division, $repetition_rb, $id_arr[0], $emp_id, $grpcasereport, $_POST);

							// 処理が正常に終了したら、トランザクションをコミット
							pg_query($con, "commit transaction");

							break;

						// 重複している
						// 重複データ処理 [インポートしない：replace_rd="1"]
						case "2":
							// 重複エラーインクリメント
							$error_cnt++;

							break;
					}

					// 重複データの場合、セッション変数にレポートを保持
					if ($chk_num == "2" || $chk_num == "3") {
						$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_RESULT"][] = $grpcasereport;
					}
				}

				// カウンタをセッション変数に保持
				$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["TOTAL_CNT"] = $total_cnt;
				$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["IMPORT_CNT"] = $import_cnt;
				$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["REPLACE_CNT"] = $replace_cnt;
				$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["ERROR_CNT"] = $error_cnt;

			// 必須チェックエラー
			} else if($under_disp_flg==TRUE){
                            $under_disp_message .= "";
                        }else{
				$under_disp_message = "シーケンス番号：";
				$flg = 0;
				foreach ($indispensable_arr as $indispensable) {
					if ($flg == 1) {
						$under_disp_message .= "、";
					}
					$under_disp_message .= $indispensable;
					$flg = 1;
				}
				$under_disp_message .= "<br>上記報告書の必須項目が未設定です。";
			}
		// 検証失敗
		} elseif ($verification_flg == false) {
			$under_disp_message = "XMLファイルの検証に失敗しました。";
		}

		// インポートしたファイルの削除
		unlink($tmp_dir . $xml_file_name);
	}
// 子画面（重複データ置換対象選択）から遷移された場合
} else if ($mode == "duplication") {
	$target_xml_index = $_POST["target_xml_index"];
	$target_db_index = $_POST["target_db_index"];

	// トランザクションの開始
	pg_query($con, "begin transaction");

	// ==================================================
	// 報告書反映
	// set_report_from_xml() from self
	// ==================================================
	set_report_from_xml($con
						, $fname
						, $session
						, "update"
						, $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_TYPE"]
						, $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_ID_ARRAY"][$target_xml_index][$target_db_index]
						, $emp_id
						, $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_REPORT"][$target_xml_index]
						, $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_POST"]
						, "1"
						);

	// 処理が正常に終了したら、トランザクションをコミット
	pg_query($con, "commit transaction");

	// 重複データの更新を行ったので、セッション変数にレポートを保持
	$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_RESULT"][] = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_REPORT"][$target_xml_index];

	// カウンタのインクリメント
	$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["TOTAL_CNT"]++;
	$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["IMPORT_CNT"]++;
	$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["REPLACE_CNT"]++;

	// 処理済データのインデックスを保持
	$_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_FINISH"][] = $target_xml_index;

	// 最後にページング処理になりかわる
	$mode = "paging";
}

// 初期表示時に専用セッション変数を初期化
if (!($mode != "")) {
	$_SESSION["HIYARI_HIYARI_XML_IMPORT"] = array();
}

// ==================================================
//表示
// ==================================================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("session_name", session_name());
$smarty->assign("session_id", session_id());
$smarty->assign("under_disp_message", $under_disp_message);

//------------------------------
//ヘッダ用データ
//------------------------------
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//------------------------------
//部署
//------------------------------
$row_num = 3;
if ($arr_class_name["class_cnt"] == 4) {
    $row_num++;
}
$smarty->assign("row_num", $row_num);
$smarty->assign("arr_class_name", $arr_class_name);
$smarty->assign("post_select_data", get_post_select_data($con, $fname));

//ログインユーザの所属先
$smarty->assign("registrant_class", $registrant_class);
$smarty->assign("registrant_attribute", $registrant_attribute);
$smarty->assign("registrant_dept", $registrant_dept);
$smarty->assign("registrant_room", $registrant_room);

//------------------------------
//報告者職種と様式
//------------------------------
$job_sel_arr = get_job_array($con, $fname);
$own_job = get_own_job($con, $emp_id, $fname);
$eis_sel_arr = $rep_obj->get_eis_job_link_info();

$registrant_jobs = array();
foreach($job_sel_arr as $job_data) {
    $job = $job_data;

    if ($own_job == $job_data["job_id"]) {
        $job['selected'] = " selected";
    }
    else{
        $job['selected'] = "";
    }    

    $registrant_jobs[] = $job;
}
$smarty->assign("registrant_jobs", $registrant_jobs);
$smarty->assign("eis_sel_arr", $eis_sel_arr);

//------------------------------
// インシレベルに対する情報を取得
// get_inci_level_infos() from hiyari_report_class.php
//------------------------------
$level_infos = $rep_obj->get_inci_level_infos();
if(count($level_infos) > 0)
{
    if ($design_mode == 1){
        $popup_detail_message = "<table>";
        for($i=0; $i<count($level_infos); $i++)
        {
            if($level_infos[$i]["use_flg"])
            {
                $easy_name = h($level_infos[$i]['easy_name'], ENT_QUOTES)."：";
                $message   = h($level_infos[$i]['message'], ENT_QUOTES);
                $popup_detail_message .= "<tr valign=\\'top\\'><td align=\\'right\\' nowrap><font size=\\'3\\' face=\\'ＭＳ Ｐゴシック, Osaka\\' class=\\'j12\\'>$easy_name</font></td>";
                $popup_detail_message .= "<td><font size=\\'3\\' face=\\'ＭＳ Ｐゴシック, Osaka\\' class=\\'j12\\'>$message</font></td></tr>";
            }
        }
        $popup_detail_message .= "</table>";
    }
    else{
        for($i=0; $i<count($level_infos); $i++)
        {
            if($level_infos[$i]["use_flg"])
            {
                $easy_name = h($level_infos[$i]['easy_name'], ENT_QUOTES)." ： ";
                $message   = h($level_infos[$i]['message'], ENT_QUOTES);
                $level_infos[$i]['detail'] = $easy_name . $message;
            }
        }
    }
}

// タイトル名取得
$sql  = "select * from inci_easyinput_item_mst";
$cond = "where grp_code = 90 and easy_item_code = 10";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
    pg_close($con);
    echo("<script type='text/javascript' src='js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$title = pg_result($sel,0,"easy_item_name");
$smarty->assign("title", $title);
$smarty->assign("popup_detail_message", $popup_detail_message);
$smarty->assign("level_infos", $level_infos);

//------------------------------
//処理内容
//------------------------------
$smarty->assign("is_verified_ok", $verification_flg == true);
$smarty->assign("num_indispensable", count($indispensable_arr));
$smarty->assign("mode", $mode);
$smarty->assign("TOTAL_CNT", $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["TOTAL_CNT"]);
$smarty->assign("IMPORT_CNT", $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["IMPORT_CNT"]);
$smarty->assign("REPLACE_CNT", $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["REPLACE_CNT"]);
$smarty->assign("ERROR_CNT", $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_COUNT"]["ERROR_CNT"]);

//------------------------------
//重複データ一覧
//------------------------------
$smarty->assign("num_duplicated", count($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_RESULT"]));

// データ数取得
$data_cnt = count($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_RESULT"]);

// ページングに必要なデータを取得
get_data_for_paging($page, $data_cnt, $disp_count_max_in_page, $s_index, $e_index, $page_max);

// 配列の初期化
$fix_arr = array();

// 固定データ配列を取得
$fix_arr = get_fix_xml_data_array();

$index = -1;

// 重複データ
$duplicated = array();
foreach ($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_CHECK_RESULT"] as $chk_result) {
    $index++;

    if (!($s_index <= $index && $index <= $e_index)) {
        continue;
    }

    $result['seq'] = $chk_result["_attr"]["SEQ"];
    $result['year'] = $chk_result["DATYEAR"][0]["_value"];
    $result['month'] = $chk_result["DATMONTH"][0]["_attr"]["CODE"];
    $result['time'] = $fix_arr["DATTIMEZONE"][(string)$chk_result["DATTIMEZONE"][0]["_attr"]["CODE"]];

    $flg = 0;
    $site = "";
    foreach ($chk_result["LSTSITE"][0]["DATSITE"] as $datsite) {
        if ($flg == 1) {
            $site .= "、";
        }
        $site .=  $fix_arr["DATSITE"][(string)$datsite["_attr"]["CODE"]];

        $flg = 1;
    }
    $result['site'] = $site;

    $result['summary'] = $fix_arr["DATSUMMARY"][(string)$chk_result["DATSUMMARY"][0]["_attr"]["CODE"]];

    $duplicated[] = $result;
}
$smarty->assign("duplicated", $duplicated);

//------------------------------
//ページング
//------------------------------
$smarty->assign("page", $page);
$smarty->assign("page_max", $page_max);
if ($page_max > 1){
    $smarty->assign("page", $page);
    $page_list = array();
    $page_stt = max(1, $page - 3);
    $page_end = min($page_stt + 6, $page_max);
    $page_stt = max(1, $page_end - 6);
    for ($i=$page_stt; $i<=$page_end; $i++){
        $page_list[] = $i;
    }
    $smarty->assign("page_list", $page_list);
}

//------------------------------
//未処理データ一覧
//------------------------------
$smarty->assign("num_repetition", count($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_REPORT"]));

// 配列の初期化
$fix_arr = array();

// 固定データ配列を取得
$fix_arr = get_fix_xml_data_array();

$index = 0;

// 未処理データ
$repetition = array();
foreach ($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_REPORT"] as $repetition_report) {
    $report['seq'] = $repetition_report["_attr"]["SEQ"];
    $report['year'] = $repetition_report["DATYEAR"][0]["_value"];
    $report['month'] = $repetition_report["DATMONTH"][0]["_attr"]["CODE"];
    $report['time'] = $fix_arr["DATTIMEZONE"][(string)$repetition_report["DATTIMEZONE"][0]["_attr"]["CODE"]];

    $flg = 0;
    $site = "";
    foreach ($repetition_report["LSTSITE"][0]["DATSITE"] as $datsite) {
        if ($flg == 1) {
            $site .= "、";
        }
        $site .=  $fix_arr["DATSITE"][(string)$datsite["_attr"]["CODE"]];

        $flg = 1;
    }
    $report['site'] = $site;

    $report['summary'] = $fix_arr["DATSUMMARY"][(string)$repetition_report["DATSUMMARY"][0]["_attr"]["CODE"]];

    $button_str = "";
    foreach ($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_FINISH"] as $finish_index) {
        if ($index == $finish_index) {
            $button_str = "disabled";
            break;
        }
    }
    $report['button_str'] = $button_str;

    $repetition[] = $report;
}
$smarty->assign("repetition", $repetition);

$smarty->assign("design_mode", $design_mode);

//------------------------------
//表示
//------------------------------
if ($design_mode == 1){
    $smarty->display("hiyari_hiyari_xml_import1.tpl");
}
else{
    $smarty->display("hiyari_hiyari_xml_import2.tpl");
}

// DB接続の切断
pg_close($con);

//======================================================================================================================================================
//内部関数
//======================================================================================================================================================
/**
 * 選択されたXMLファイルのインポート
 * 
 * @param  object $inputs 画面入力情報
 * @param  string $dir    格納フォルダ
 * @return string         インポートしたファイル名（失敗は空文字）
 */
function import_xml_file($inputs, $dir)
{
	// 返り値
	$ret = "";
	
	// 一時名称
	$tmp_name = "";
	
	// 拡張子
	$str = "";
	
	// インポートされたファイルかを調べる
	if (is_uploaded_file($inputs["xml_file"]["tmp_name"])) {
		// 一時名称取得
		$tmp_name = $inputs["xml_file"]["tmp_name"];
		$tmp_name = substr($tmp_name, strrpos($tmp_name, "/") + 1);
		
		// 拡張子取得
		$str = substr($inputs["xml_file"]["name"], strrpos($inputs["xml_file"]["name"], '.') + 1);
		
		// 拡張子判定
		if ($str == "xml") {
			// ファイルの移動を行う
			if (move_uploaded_file($inputs["xml_file"]["tmp_name"], $dir . $inputs["xml_file"]["name"])) {
				// パーミッション設定
				chmod($dir . $inputs["xml_file"]["name"], 0644);
				
				$new_name = $tmp_name . "." . $str;
				
				// 名称変更
				rename($dir . $inputs["xml_file"]["name"], $dir . $new_name);
				
				$ret = $new_name;
			}
		}
	}
	
	return $ret;
}

/**
 * 指定されたXMLファイルをDTDにより検証
 * 
 * @param  string  $file_name ファイル名称
 * @return boolean            検証結果
 */
function verification_xml_file($file_name)
{
	// インスタンス作成
	$dom = new DOMDocument;
	
	// ファイルのフルパス設定
	$path = realPath($file_name);
	
	// XMLを読み込む
	$dom->Load($path);
	
//	$doctype = $dom->DOMDocumentType;
	echo $dom->DOMDocumentType;
	
//	echo $doctype->name;
	
	// DTDによる検証を行う
	if ($dom->validate()) {
		// 検証成功
		return true;
	} else {
		// 検証失敗
		return false;
	}
}

/**
 * 指定されたXMLファイルを連想配列に変換
 * 
 * @param  string  $file_path    ファイルパス
 * @return simplexml_load_string simplexml形式のxml
 */
function convert_xml_data($file_path)
{
    global $under_disp_flg,$under_disp_message;
    $ver =  phpversion();
    if($ver<5.0){
        $under_disp_message = "PHPのバージョン5.0以上が必要です。";
        $under_disp_flg= TRUE;
        return;
    }
    // ファイルの読込み
	$xml_contents = file_get_contents($file_path);
	
	// XML文字列をオブジェクトに格納
	$xml = simplexml_load_string($xml_contents);
	
	// ==================================================
	// XMLデータを連想配列に格納
	// xml2array() from self
	// ==================================================
        
        $array_count = count($xml->GRPCASEREPORT);
        if($array_count>MAX_READ_NUMBER){
            //global $under_disp_flg,$under_disp_message;
            $under_disp_message = $array_count."件データがあります。".MAX_READ_NUMBER."件以下にしてください。";
            $under_disp_flg= TRUE;
            
            return;
        }
        
        $version = array("1.0");
        $xml_array = array();

        for($i=0;$i<$array_count;$i++){
            $grpcasereport = $xml->GRPCASEREPORT[$i];   
            $xml_ar =xml2array($grpcasereport);
             
            //$xml_ar['GRPCASEREPORT'][0]['_attr']['DIVISION'] = (string)$grpcasereport->attributes()->DIVISION;
            //$xml_ar['GRPCASEREPORT'][0]['_attr']['SEQ'] =(string)$grpcasereport->attributes()->SEQ;

            $array_grpcasereport =  (array)$grpcasereport;
            $xml_ar['GRPCASEREPORT'][0]['_attr']['DIVISION'] =$array_grpcasereport["@attributes"]["DIVISION"];
            $xml_ar['GRPCASEREPORT'][0]['_attr']['SEQ'] =$array_grpcasereport["@attributes"]["SEQ"];
         
                    
            $xml_array[]=$xml_ar["GRPCASEREPORT"][0];
              
            unset($grpcasereport);
            
        }    
        
        $convert_xml = array("CASEREPORT"=>array("0"=>array("VERSION"=>$version,"GRPCASEREPORT"=>$xml_array)));   
        unset($xml);
        return $convert_xml;
       
}

/**
 * simplexml形式のxmlデータを連想配列に変換
 * 
 * @param  simplexml_load_string($xml)  $sxml simplexml形式のxml$
 * @return array                              連想配列
 * 
 * from web [http://blog.aulta.net/2010/03/08/php-xmlを配列にする。/]
 */
function xml2array(&$sxml, $isRoot = true)
{
	if ($isRoot) {
 //           $aaa = (array)$sxml;
   //         echo $aaa;
            return array($sxml->getName() => array(xml2array($sxml, false)));
	}
	
	$r = array();
	mb_language("Japanese");
       
	foreach ($sxml->children() as $key=>$cld) {
		$a = &$r[(string)$key];
		$a = &$a[count($a)];
		$array_cld=(array)$cld;
		if (count($cld->children()) == 0) {
			// 日本語が文字化けするのでエンコードを行う
			$a['_value'] = mb_convert_encoding((string)$cld, "EUC-JP", "AUTO");
		} else {
			$a = xml2array($cld, false);
		}
		$array_cld = (array)$cld;
		foreach ($array_cld["@attributes"] as $key_at => $at) {
			$a['_attr'][(string)$key_at] = (string)$at;    
		}
	}

	return $r;
}

/**
 * XMLデータの必須チェック
 * 
 * @param  array $xml_arr xmlデータ配列
 * @return array          結果配列
 */
function indispensable_check($xml_arr)
{
	$ret_arr = array();
	
	foreach ($xml_arr["CASEREPORT"][0]["GRPCASEREPORT"] as $grpcasereport) {
		$division = $grpcasereport["_attr"]["DIVISION"];
		$seq = $grpcasereport["_attr"]["SEQ"];
		
		$flg = 0;
		
		// 発生年：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATYEAR"]) == 0) {
			$flg = 1;
		}
		
		// 発生月：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATMONTH"]) == 0) {
			$flg = 1;
		}
		
		// 発生曜日：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATDAY"]) == 0) {
			$flg = 1;
		}
		
		// 曜日区分：事故必須
		if ($division == "1" && count($grpcasereport["DATHOLIDAY"]) == 0) {
			$flg = 1;
		}
		
		// 発生時間帯：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATTIMEZONE"]) == 0) {
			$flg = 1;
		}
		
		// 医療の実施の有無：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATEXECUTION"]) == 0) {
			$flg = 1;
		}
		
		// 治療の程度：医療の実施の有無が「実施あり」の場合、事故、ヒヤリ共に必須
		if ($grpcasereport["DATEXECUTION"][0]["_attr"]["CODE"] == "01" && count($grpcasereport["DATLEVEL"]) == 0) {
			$flg = 1;
		}
		
		// 事故の程度：事故必須
		if ($division == "1" && count($grpcasereport["DATSEQUELAE"]) == 0) {
			$flg = 1;
		}
		
		// 影響度：医療の実施の有無が「実施なし」の場合、ヒヤリ必須
		if ($grpcasereport["DATEXECUTION"][0]["_attr"]["CODE"] == "02" && $division == "2" && count($grpcasereport["DATINFLUENCE"]) == 0) {
			$flg = 1;
		}
		
		// 発生場所：事故、ヒヤリ共に必須
		if (count($grpcasereport["LSTSITE"]) == 0) {
			$flg = 1;
		}
		
		// 概要：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATSUMMARY"]) == 0) {
			$flg = 1;
		}
		
		// 特に報告を求める事例：事故必須
		if ($division == "1" && count($grpcasereport["DATSPECIALLY"]) == 0) {
			$flg = 1;
		}
		
		// 関連診療科：事故必須
		if ($division == "1" && count($grpcasereport["LSTDEPARTMENT"]) == 0) {
			$flg = 1;
		}
		
		// 患者：事故、ヒヤリ共に必須
		if (count($grpcasereport["GRPPATIENT"]) == 0) {
			$flg = 1;
		}
			// 患者の数：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPPATIENT"][0]["DATPATIENT"]) == 0) {
				$flg = 1;
			}
			
			// 患者の年齢：患者の数が一人(CODE="1")の場合必須
			if ($grpcasereport["GRPPATIENT"][0]["DATPATIENT"][0]["_attr"]["CODE"] == 1) {
				$flg_years = 0;
				$flg_months = 0;
				foreach ($grpcasereport["GRPPATIENT"][0]["LSTPATIENTAGE"][0]["DATPATIENTAGE"] as $patientage) {
					if ($patientage["_attr"]["KBN"] == "years" && $patientage["_value"] != "") {
						$flg_years = 1;
					}
					if ($patientage["_attr"]["KBN"] == "months" && $patientage["_value"] != "") {
						$flg_months = 1;
					}
				}
				if ($flg_years == 0) {
					$flg = 1;
				}
				if ($flg_months == 0) {
					$flg = 1;
				}
			}
			
			// 患者の性別：患者の数が一人(CODE="1")の場合必須
			if ($grpcasereport["GRPPATIENT"][0]["DATPATIENT"][0]["_attr"]["CODE"] == 1 && count($grpcasereport["GRPPATIENT"][0]["DATPATIENTSEX"]) == 0) {
				$flg = 1;
			}
			
			// 患者区分：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPPATIENT"][0]["DATPATIENTDIV"]) == 0) {
				$flg = 1;
			}
		
		// 疾患名：事故、ヒヤリ共に必須
		if (count($grpcasereport["LSTDISEASE"]) == 0) {
			$flg = 1;
		}
			// 事故(事例)に直接関連する疾患名：事故、ヒヤリ共に必須
			$flg_disease0 = 0;
			foreach ($grpcasereport["LSTDISEASE"][0]["DATDISEASE"] as $disease) {
				if ($disease["_attr"]["KBN"] == "disease0" && $disease["_value"] != "" ) {
					$flg_disease0 = 1;
					break;
				}
			}
			if ($flg_disease0 == 0) {
				$flg = 1;
			}
		
		// 発見者：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATDISCOVERER"]) == 0) {
			$flg = 1;
		}
		
		// 当事者：事故、ヒヤリ共に必須
		if (count($grpcasereport["LSTCONCERNED"]) == 0) {
			$flg = 1;
		}
			foreach ($grpcasereport["LSTCONCERNED"][0]["GRPCONCERNED"] as $grpconcerned) {
				// 当事者職種：事故、ヒヤリ共に必須
				if (count($grpconcerned["DATCONCERNED"]) == 0) {
					$flg = 1;
				}
				
				// 当事者職種経験
				$flg_years = 0;
				$flg_months = 0;
				foreach ($grpconcerned["LSTEXPERIENCE"][0]["DATEXPERIENCE"] as $datexperience) {
					if ($datexperience["_attr"]["KBN"] == "years" && $datexperience["_value"] != "") {
						$flg_years = 1;
					}
					if ($datexperience["_attr"]["KBN"] == "months" && $datexperience["_value"] != "") {
						$flg_months = 1;
					}
				}
				// 年：事故、ヒヤリ共に必須
				if ($flg_years == 0) {
					$flg = 1;
				}
				// ヶ月：事故必須
				if ($division == "1" && $flg_months == 0) {
					$flg = 1;
				}
				
				// 当事者部署配属期間
				$flg_years = 0;
				$flg_months = 0;
				foreach ($grpconcerned["LSTLENGTH"][0]["DATLENGTH"] as $datlength) {
					if ($datlength["_attr"]["KBN"] == "years" && $datlength["_value"] != "") {
						$flg_years = 1;
					}
					if ($datlength["_attr"]["KBN"] == "months" && $datlength["_value"] != "") {
						$flg_months = 1;
					}
				}
				// 年：事故、ヒヤリ共に必須
				if ($flg_years == 0) {
					$flg = 1;
				}
				// ヶ月：事故必須
				if ($division == "1" && $flg_months == 0) {
					$flg = 1;
				}
				
				// 直前１週間の当直・夜勤回数：事故必須
				if ($division == "1" && count($grpconcerned["DATDUTY"]) == 0) {
					$flg = 1;
				}
				
				// 勤務形態：事故必須
				if ($division == "1" && count($grpconcerned["DATSHIFT"]) == 0) {
					$flg = 1;
				}
				
				// 直前１週間の勤務時間：事故必須
				if ($division == "1" && count($grpconcerned["DATHOURS"]) == 0) {
					$flg = 1;
				}
			}
		
		// 概要が「薬剤」の場合
		if ($grpcasereport["DATSUMMARY"][0]["_attr"]["CODE"] == "02") {
			// 薬剤に関する項目：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPMEDICINE"]) == 0) {
				$flg = 1;
			}
			
			// 種類：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPMEDICINE"][0]["DATTYPE"]) == 0) {
				$flg = 1;
			}
			
			// 関連医薬品(販売名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPMEDICINE"][0]["GRPMEDICALSUPPLIES"][0]["DATNAME"]) == 0) {
				$flg = 1;
			}
			
			// 関連医薬品(製造販売業者名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPMEDICINE"][0]["GRPMEDICALSUPPLIES"][0]["DATMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 発生場面：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPMEDICINE"][0]["DATSCENE"]) == 0) {
				$flg = 1;
			}
			
			// 事故(事例)の内容：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPMEDICINE"][0]["DATDETAIL"]) == 0) {
				$flg = 1;
			}
		}
		
		// 概要が「輸血」の場合
		if ($grpcasereport["DATSUMMARY"][0]["_attr"]["CODE"] == "03") {
			// 輸血に関する項目：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTRANSFUSION"]) == 0) {
				$flg = 1;
			}
			
			// 発生場面：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTRANSFUSION"][0]["DATSCENE"]) == 0) {
				$flg = 1;
			}
			
			// 事故(事例)の内容：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTRANSFUSION"][0]["DATDETAIL"]) == 0) {
				$flg = 1;
			}
		}
		
		// 概要が「治療・処置」の場合
		if ($grpcasereport["DATSUMMARY"][0]["_attr"]["CODE"] == "04") {
			// 治療・処置に関する項目：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTREATMENT"]) == 0) {
				$flg = 1;
			}
			
			// 種類：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTREATMENT"][0]["DATTYPE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(販売名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTREATMENT"][0]["GRPMATERIAL"][0]["DATNAME"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(製造販売業者名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTREATMENT"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(購入年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTREATMENT"][0]["GRPMATERIAL"][0]["DATPURCHASE"]) == 0) {
				$flg = 1;
			}
			
			// 発生場所：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTREATMENT"][0]["DATSCENE"]) == 0) {
				$flg = 1;
			}
			
			// 事故(事例)の内容：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPTREATMENT"][0]["DATDETAIL"]) == 0) {
				$flg = 1;
			}
		}
		
		// 概要が「医療機器等」の場合
		if ($grpcasereport["DATSUMMARY"][0]["_attr"]["CODE"] == "10") {
			// 医療機器等・医療材料の使用・管理に関する項目：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"]) == 0) {
				$flg = 1;
			}
			
			// 種類：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["DATTYPE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(販売名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATNAME"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(製造販売業者名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(製造年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATDATEOFMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(購入年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATPURCHASE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(直近の保守・点検年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPEQUIPMENT"][0]["DATMAINTENANCE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(販売名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPMATERIAL"][0]["DATNAME"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(製造販売業者名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(購入年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["GRPMATERIAL"][0]["DATPURCHASE"]) == 0) {
				$flg = 1;
			}
			
			// 発生場面：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["DATSCENE"]) == 0) {
				$flg = 1;
			}
			
			// 事故(事例)の内容：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPAPPARATUS"][0]["DATDETAIL"]) == 0) {
				$flg = 1;
			}
		}
		
		// 概要が「ドレーン・チューブ」の場合
		if ($grpcasereport["DATSUMMARY"][0]["_attr"]["CODE"] == "06") {
			// ドレーン・チューブ類の使用・管理に関する項目：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPDRAINTUBE"]) == 0) {
				$flg = 1;
			}
			
			// 種類：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPDRAINTUBE"][0]["DATTYPE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(販売名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPDRAINTUBE"][0]["GRPMATERIAL"][0]["DATNAME"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(製造販売業者名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPDRAINTUBE"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(購入年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPDRAINTUBE"][0]["GRPMATERIAL"][0]["DATPURCHASE"]) == 0) {
				$flg = 1;
			}
			
			// 発生場所：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPDRAINTUBE"][0]["DATSCENE"]) == 0) {
				$flg = 1;
			}
			
			// 事故(事例)の内容：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPDRAINTUBE"][0]["DATDETAIL"]) == 0) {
				$flg = 1;
			}
		}
		
		// 概要が「検査」の場合
		if ($grpcasereport["DATSUMMARY"][0]["_attr"]["CODE"] == "08") {
			// 検査に関する項目：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"]) == 0) {
				$flg = 1;
			}
			
			// 種類：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["DATTYPE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(販売名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATNAME"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(製造販売業者名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(製造年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATDATEOFMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(購入年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATPURCHASE"]) == 0) {
				$flg = 1;
			}
			
			// 医療機器等(直近の保守・点検年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPEQUIPMENT"][0]["DATMAINTENANCE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(販売名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPMATERIAL"][0]["DATNAME"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(製造販売業者名)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPMATERIAL"][0]["DATMANUFACTURE"]) == 0) {
				$flg = 1;
			}
			
			// 医療材料・諸物品等(購入年月)：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["GRPMATERIAL"][0]["DATPURCHASE"]) == 0) {
				$flg = 1;
			}
			
			// 発生場面：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["DATSCENE"]) == 0) {
				$flg = 1;
			}
			
			// 事故(事例)の内容：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPINSPECTION"][0]["DATDETAIL"]) == 0) {
				$flg = 1;
			}
		}
		
		// 概要が「療養上の世話」の場合
		if ($grpcasereport["DATSUMMARY"][0]["_attr"]["CODE"] == "09") {
			// 療養上の場面に関する項目：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPRECUPERATING"]) == 0) {
				$flg = 1;
			}
			
			// 種類：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPRECUPERATING"][0]["DATTYPE"]) == 0) {
				$flg = 1;
			}
			
			// 発生場所：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPRECUPERATING"][0]["DATSCENE"]) == 0) {
				$flg = 1;
			}
			
			// 事故(事例)の内容：事故、ヒヤリ共に必須
			if (count($grpcasereport["GRPRECUPERATING"][0]["DATDETAIL"]) == 0) {
				$flg = 1;
			}
		}
		
		// 実施した医療行為の目的：事故必須
		if ($division == "1" && count($grpcasereport["DATPURPOSE"]) == 0) {
			$flg = 1;
		}
		
		// 事故(事例)の内容：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATCONTENTTEXT"]) == 0) {
			$flg = 1;
		}
		
		// 発生要因：事故、ヒヤリ共に必須
		if (count($grpcasereport["LSTFACTOR"]) == 0) {
			$flg = 1;
		}
		
		// 事故(事例)の背景要因の概要：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATBACKGROUND"]) == 0) {
			$flg = 1;
		}
		
		// 事故調査委員会設置の有無：事故必須
		if ($division == "1" && count($grpcasereport["DATCOMMITTEE"]) == 0) {
			$flg = 1;
		}
		
		// 改善策：事故、ヒヤリ共に必須
		if (count($grpcasereport["DATIMPROVEMENTTEXT"]) == 0) {
			$flg = 1;
		}
		
		if ($flg == 1) {
			$ret_arr[] = $seq;
		}
	}
	
	return $ret_arr;
}

/**
 * 職種データの取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @return array         職種データ配列
 */
function get_job_array($con, $fname)
{
	// 職種データ取得
	$sql = "select";
		$sql .= " a.job_id";
		$sql .= ", b.job_nm";
	$sql .= " from";
		$sql .= " (";
			$sql .= " select distinct";
				$sql .= " job_id";
			$sql .= " from";
				$sql .= " inci_set_job_link";
		$sql .= ") a";
		$sql .= " inner join";
		$sql .= " jobmst b";
		$sql .= " on";
		$sql .= " a.job_id = b.job_id";
	$sql .= " order by";
		$sql .= " a.job_id";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}

/**
 * DEFデータの取得
 * 
 * @param  onject $con            接続コネクション
 * @param  string $fname          画面名
 * @param  int    $grp_code       グループコード
 * @param  int    $easy_item_code 項目コード
 * @return array                  DEFデータ配列
 */
function get_def_array($con, $fname, $grp_code, $easy_item_code)
{
	// データ取得
	$sql = "select";
		$sql .= " easy_code";
		$sql .= ", def_code";
	$sql .= " from";
		$sql .= " inci_report_materials";
	$cond = " where";
		$cond .= " grp_code = " . $grp_code;
		$cond .= " and easy_item_code = " . $easy_item_code;
	$cond .= " order by";
		$cond .= " def_code";
		$cond .= ", easy_code";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}

/**
 * SUPER ITEM IDデータの取得
 * 
 * @param  onject $con            接続コネクション
 * @param  string $fname          画面名
 * @param  string $xml_code       XMLコード
 * @return string                 SUPER ITEM ID
 */
function convert_code_to_super_item_id($con, $fname, $xml_code)
{
	// xml_codeが指定されない場合は空文字を返す
	if ($xml_code == "") {
		return "";
	}
	
	// 職種データ取得
	$sql = "select";
		$sql .= " super_item_id";
	$sql .= " from";
		$sql .= " inci_super_item_2010";
	
	$cond = " where";
		$cond .= " code = '" . $xml_code . "'";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$row = pg_fetch_row($sel);
	
	return $row[0];
}

/**
 * ITEM IDデータ配列の取得
 * 
 * @param  onject $con   接続コネクション
 * @param  string $fname 画面名
 * @param  string $super 概要
 * @param  string $div   種類［DATTYPE="kind"、DATSCENE="scene"、DATDETAIL="content"］
 * @param  string $code  XMLコード
 * @return array         item_id, sub_item_idを含む配列
 */
function get_item_id_array($con, $fname, $super, $div, $code)
{
	// xml_codeが指定されない場合は空文字を返す
	if ($code == "") {
		return "";
	}
	
	// 職種データ取得
	$sql = "select";
		$sql .= " item_id";
		$sql .= ", sub_item_id";
	$sql .= " from";
		$sql .= " inci_sub_item_2010";
	
	$cond = " where";
		$cond .= " super_item_id = " . $super;
		$cond .= " and item_code = '" . $div . "'";
		$cond .= " and code = '" . $code . "'";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_row($sel);
}

/**
 * データの重複チェック
 * 
 * @param onject $con    接続コネクション
 * @param string $fname  画面名
 * @param string $emp_id 報告者ID
 * @param string $target 重複チェック対象 [1:軽 2:重]
 * @param array  $report レポートデータ
 * @param array  $post   画面送信データ
 * @return array         レポートID、データID配列
 */
function check_repetition($con, $fname, $emp_id, $target, $report, $post)
{
	// ==================================================
	// for inci_report
	// ==================================================
	
	// 登録部署1 [registrant_class]
	$emp_class = $post["search_emp_class"];
	if ($emp_class == "") {
		$emp_class = null;
	}
	
	// 登録部署2 [registrant_attribute]
	$emp_attribute = $post["search_emp_attribute"];
	if ($emp_attribute == "") {
		$emp_attribute = null;
	}
	
	// 登録部署3 [registrant_dept]
	$emp_dept = $post["search_emp_dept"];
	if ($emp_dept == "") {
		$emp_dept = null;
	}
	
	// 登録部署4 [registrant_room]
	$emp_room = $post["search_emp_room"];
	if ($emp_room == "") {
		$emp_room = null;
	}
	
	// 有効部署数
	$sql = "select";
		$sql .= " class_cnt";
	$sql .= " from";
		$sql .= " classname";
	
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$row = pg_fetch_row($sel);
	
	$class_cnt = $row[0];
	
	// シーケンス番号 [seq]
	$seq = $report["_attr"]["SEQ"];
	
	// チェックする内容により分岐
	switch ($target) {
		// 軽
		case "1":
			$sql = "select";
				$sql .= " report_id";
				$sql .= ", eid_id";
			$sql .= " from";
				$sql .= " inci_report";
			$sql .= " where";
				if (is_null($emp_class)) {
					$sql .= " registrant_class is null";
				} else {
					$sql .= " registrant_class = " . $emp_class;
					if (is_null($emp_attribute)) {
						$sql .= " and registrant_attribute is null";
					} else {
						$sql .= " and registrant_attribute = " . $emp_attribute;
						if (is_null($emp_dept)) {
							$sql .= " and registrant_dept is null";
						} else {
							$sql .= " and registrant_dept = " . $emp_dept;
							if ($class_cnt == 4) {
								if (is_null($emp_room)) {
									$sql .= " and registrant_room is null";
								} else {
									$sql .= " and registrant_room = " . $emp_room;
								}
							}
						}
					}
				}
				$sql .= " and registrant_id = '" . $emp_id . "'";
				$sql .= " and seq = '" . $seq . "'";
				$sql .= " and del_flag <> 't'";
				$sql .= " and shitagaki_flg <> 't'";
				$sql .= " and kill_flg <> 't'";
			$sql .= " order by";
				$sql .= " report_id";
				$sql .= ", eid_id";
			
			break;
		// 重
		case "2":
			// ==================================================
			// for inci_easyinput_data
			// ==================================================
			
			// 発生年 [発生年月日の頭4桁:[100][5]]
			$year = $report["DATYEAR"][0]["_value"];
			
			// 発生月 [100][10]
			// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
			$month = (int) $report["DATMONTH"][0]["_attr"]["CODE"];
			
			// 発生曜日 [100][20]
			// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
			$day = (int) $report["DATDAY"][0]["_attr"]["CODE"];
			
			// 発生時間帯 [100][40]
			// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
			$timezone = (int) $report["DATTIMEZONE"][0]["_attr"]["CODE"];
			
			// 事故（事例）の概要 [900][10]
			// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
			$summary = convert_code_to_super_item_id($con, $fname, $report["DATSUMMARY"][0]["_attr"]["CODE"]);
			
			// 患者の数 [200][10]
			$patient = "";
			
			// ==================================================
			// DEFデータの取得
			// get_def_array() from self
			// ==================================================
			$def_arr = get_def_array($con, $fname, 200, 10);
			
			foreach ($def_arr as $def) {
				// XMLデータとデータベースのDEFコードを比較
				if ($report["GRPPATIENT"][0]["DATPATIENT"][0]["_attr"]["CODE"] == trim($def["def_code"])) {
					// 対応コードを格納
					$patient = $def["easy_code"];
					
					break;
				}
			}
			
			// 患者性別 [210][30]
			// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
			$patientsex = (int) $report["GRPPATIENT"][0]["DATPATIENTSEX"][0]["_attr"]["CODE"];
			
			// 患者年齢
			foreach ($report["GRPPATIENT"][0]["LSTPATIENTAGE"][0]["DATPATIENTAGE"] as $datpatientage) {
				switch ($datpatientage["_attr"]["KBN"]) {
					// 歳 [210][40]
					// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(3桁)で対応
					case "years":
						$patientyears = substr("000" . $datpatientage["_value"], -3);
						break;
					// ヶ月 [210][50]
					// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(2桁)で対応
					case "months":
						$patientmonths = substr("00" . $datpatientage["_value"], -2);
						break;
				}
			}
			
			// SQL文生成
			$sql = "select";
				$sql .= " report_id";
				$sql .= ", eid_id";
			$sql .= " from";
				$sql .= " (";
					$sql .= " select";
						$sql .= " a.report_id";
						$sql .= ", a.eid_id";
						$sql .= ", a.registrant_class";
						$sql .= ", a.registrant_attribute";
						$sql .= ", a.registrant_dept";
						if ($class_cnt == 4) {
							$sql .= ", a.registrant_room";
						}
						$sql .= ", a.registrant_id";
						$sql .= ", a.seq";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " substr(input_item, 1, 4)";
								$sql .= " from";
								$sql .= " inci_easyinput_data b";
							$sql .= " where";
								$sql .= " a.eid_id = b.eid_id";
								$sql .= " and b.grp_code = 100";
								$sql .= " and b.easy_item_code = 5";
						$sql .= ") as year";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data c";
							$sql .= " where";
								$sql .= " a.eid_id = c.eid_id";
								$sql .= " and c.grp_code = 100";
								$sql .= " and c.easy_item_code = 10";
						$sql .= ") as month";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data d";
							$sql .= " where";
								$sql .= " a.eid_id = d.eid_id";
								$sql .= " and d.grp_code = 100";
								$sql .= " and d.easy_item_code = 20";
						$sql .= ") as day";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data e";
							$sql .= " where";
								$sql .= " a.eid_id = e.eid_id";
								$sql .= " and e.grp_code = 100";
								$sql .= " and e.easy_item_code = 40";
						$sql .= ") as timezone";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data f";
							$sql .= " where";
								$sql .= " a.eid_id = f.eid_id";
								$sql .= " and f.grp_code = 900";
								$sql .= " and f.easy_item_code = 10";
						$sql .= ") as summary";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data g";
							$sql .= " where";
								$sql .= " a.eid_id = g.eid_id";
								$sql .= " and g.grp_code = 200";
								$sql .= " and g.easy_item_code = 10";
						$sql .= ") as patient";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data h";
							$sql .= " where";
								$sql .= " a.eid_id = h.eid_id";
								$sql .= " and h.grp_code = 210";
								$sql .= " and h.easy_item_code = 30";
						$sql .= ") as patientsex";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data i";
							$sql .= " where";
								$sql .= " a.eid_id = i.eid_id";
								$sql .= " and i.grp_code = 210";
								$sql .= " and i.easy_item_code = 40";
						$sql .= ") as patientyears";
						$sql .= ", (";
							$sql .= "select";
								$sql .= " input_item";
							$sql .= " from";
								$sql .= " inci_easyinput_data j";
							$sql .= " where";
								$sql .= " a.eid_id = j.eid_id";
								$sql .= " and j.grp_code = 210";
								$sql .= " and j.easy_item_code = 50";
						$sql .= ") as patientmonths";
					$sql .= " from";
						$sql .= " inci_report a";
					$sql .= " where";
						$sql .= " a.del_flag <> 't'";
						$sql .= " and a.shitagaki_flg <> 't'";
						$sql .= " and a.kill_flg <> 't'";
				$sql .= ") k";
			$sql .= " where";
				if (is_null($emp_class)) {
					$sql .= " registrant_class is null";
				} else {
					$sql .= " registrant_class = " . $emp_class;
					if (is_null($emp_attribute)) {
						$sql .= " and registrant_attribute is null";
					} else {
						$sql .= " and registrant_attribute = " . $emp_attribute;
						if (is_null($emp_dept)) {
							$sql .= " and registrant_dept is null";
						} else {
							$sql .= " and registrant_dept = " . $emp_dept;
							if ($class_cnt == 4) {
								if (is_null($emp_room)) {
									$sql .= " and registrant_room is null";
								} else {
									$sql .= " and registrant_room = " . $emp_room;
								}
							}
						}
					}
				}
				$sql .= " and registrant_id = '" . $emp_id . "'";
				$sql .= " and seq = '" . $seq . "'";
				$sql .= " and year = '" . $year . "'";
				$sql .= " and month = '" . $month . "'";
				$sql .= " and day = '" . $day . "'";
				$sql .= " and timezone = '" . $timezone . "'";
				$sql .= " and summary = '" . $summary . "'";
				$sql .= " and patient = '" . $patient . "'";
				$sql .= " and patientsex = '" . $patientsex . "'";
				$sql .= " and patientyears = '" . $patientyears . "'";
				$sql .= " and patientmonths = '" . $patientmonths . "'";
			$sql .= " order by";
				$sql .= " report_id";
				$sql .= ", eid_id";
			
			break;
	}
	
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}

/**
 * 報告書反映
 *
 * @param onject $con           接続コネクション
 * @param string $fname         画面名
 * @param string $session       セッションID
 * @param string $division      処理区分 [ or update]
 * @param string $target        重複チェック対象 [1:軽 2:重] ：処理区分がupdateの時のみ使用
 * @param array  $target_id_arr 重複した対象のID配列         ：処理区分がupdateの時のみ使用
 * @param string $emp_id        報告者ID
 * @param array  $report        XMLデータ [grpcasereport]
 * @param array  $post          画面送信データ
 * @param string $dupli         複数重複データ判別 [0:default、1:duplication]
 */
function set_report_from_xml($con, $fname, $session, $division, $target, $target_id_arr, $emp_id, $report, $post, $dupli=0)
{
	// 挿入の場合のみ採番を行う
	if ($division == "insert") {
		// 報告書ID
		// シーケンスを使用し採番する
		$result = pg_query($con, "select nextval('inci_report_report_id_seq')");
		$report_id = pg_fetch_result($result, 0);
	// 更新の場合は、重複チェックで取得したレポートIDを使用
	} else if ($division == "update") {
		$report_id = $target_id_arr["report_id"];
	}
	
	// 表題
	$report_title = mb_substr($report["DATCONTENTTEXT"][0]["_value"], 0, 10);
	
	// (下書き)様式の職種ID
	$job_id = $post["sel_job_id"];
	
	// 報告者ID
	$registrant_id = $emp_id;
	
	// ==================================================
	// 報告者氏名
	// get_emp_kanji_name() from get_values.php
	// ==================================================
	$registrant_name = get_emp_kanji_name($con, $emp_id, $fname);
	
	// 報告日
	$registration_date = date("Y/m/d");
	
	// (下書き)様式ID
	$eis_id = $post["sel_eis_id"];
	
	// データID
	// シーケンスを使用し採番する
	$result = pg_query($con, "select nextval('inci_report_eid_id_seq')");
	$eid_id = pg_fetch_result($result, 0);
	
	// 削除フラグ
	$del_flag = "false";
	
	// ==================================================
	// インスタンス作成
	// hiyari_report_class() from hiyari_report_class.php
	// ==================================================
	$rep_obj = new hiyari_report_class($con, $fname);
	
	// ==================================================
	// 事案番号
	// get_new_report_no() from hiyari_report_class.php
	// ==================================================
	$report_no = $rep_obj->get_new_report_no();
	
	// 下書きフラグ
	$shitagaki_flg = "false";
	
	// 報告書備考
	$report_comment = "";
	
	// SMロックフラグ
	$edit_lock_flg = "false";
	
	
	// ==================================================
	// 匿名報告フラグ
	// ==================================================
	
	// *** 匿名情報取得 ***
	$sql = "select";
		$sql .= " count(*) as cnt";
	$sql .= " from";
		$sql .= " inci_anonymous_mail_use";
	
	$cond = " where";
		$cond .= " use_flg = 't'";
		$cond .= " and default_flg = 't'";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$row = pg_fetch_row($sel);
	
	// *** 匿名フラグ設定 ***
	if ($row[0] > 0) {
		$anonymous_report_flg = "true";
	} else {
		$anonymous_report_flg = "false";
	}
	
	// 報告部署
	$search_registrant_class = $post['search_emp_class'];
	$search_registrant_attribute = $post['search_emp_attribute'];
	$search_registrant_dept = $post['search_emp_dept'];
	$search_registrant_room = $post['search_emp_room'];
	if ($search_registrant_class == "") { 
		$search_registrant_class = null;
	}
	if ($search_registrant_attribute == "") {
		$search_registrant_attribute = null;
	}
	if ($search_registrant_dept == "") {
		$search_registrant_dept = null;
	}
	if ($search_registrant_room == "") {
		$search_registrant_room = null;
	}
	
	// 有効部署数
	$sql = "select";
		$sql .= " class_cnt";
	$sql .= " from";
		$sql .= " classname";
	
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$row = pg_fetch_row($sel);
	
	$class_cnt = $row[0];
	
	// ==================================================
	// 報告者職種
	// get_own_job() from get_values.php
	// ==================================================
	$registrant_job = get_own_job($con, $emp_id, $fname);
	
	// 主治医ID
	$doctor_emp_id = null;
	
	// 完全削除フラグ
	$kill_flg = "false";
	
	// 初回送信時間
	$first_send_time = date("YmdHis");
	
	// シーケンス番号
	$seq = $report["_attr"]["SEQ"];
	
	// 処理区分が挿入
	if ($division == "insert") {
		// select句を生成
		$sql_field_arr = array(
			"report_id"
			, "report_title"
			, "job_id"
			, "registrant_id"
			, "registrant_name"
			, "registration_date"
			, "eis_id"
			, "eid_id"
			, "del_flag"
			, "report_no"
			, "shitagaki_flg"
			, "report_comment"
			, "edit_lock_flg"
			, "anonymous_report_flg"
			, "registrant_class"
			, "registrant_attribute"
			, "registrant_dept"
			, "registrant_room"
			, "registrant_job"
			, "doctor_emp_id"
			, "kill_flg"
			, "first_send_time"
			, "seq"
		);
		
		// 登録するデータを配列に格納
		$sql_val_arr = array(
			$report_id								// report_id			：報告書ID
			, pg_escape_string($report_title)		// report_title			：表題
			, $job_id								// job_id				：(下書き)様式の職種ID
			, $emp_id								// registrant_id		：報告者ID
			, pg_escape_string($registrant_name)	// registrant_name		：報告者氏名
			, $registration_date					// registration_date	：報告日
			, $eis_id								// eis_id				：(下書き)様式ID
			, $eid_id								// eid_id				：データID
			, $del_flag								// del_flag				：削除フラグ
			, $report_no							// report_no			：事案番号
			, $shitagaki_flg						// shitagaki_flg		：下書きフラグ
			, pg_escape_string($report_comment)		// report_comment		：(必ずNULL)
			, $edit_lock_flg						// edit_lock_flg		：SMロックフラグ
			, $anonymous_report_flg					// anonymous_report_flg	：匿名報告フラグ
			, $search_registrant_class				// registrant_class		：報告部署1
			, $search_registrant_attribute			// registrant_attribute	：報告部署2
			, $search_registrant_dept				// registrant_dept		：報告部署3
			, $search_registrant_room				// registrant_room		：報告部署4
			, $registrant_job						// registrant_job		：報告者職種
			, $doctor_emp_id						// doctor_emp_id		：主治医ID
			, $kill_flg								// kill_flg				：完全削除フラグ
			, $first_send_time						// first_send_time		：初回送信時間
			, $seq									// seq					：シーケンス
		);
		
		// sql文を生成
		$sql = "insert into";
			$sql .= " inci_report";
			$sql .= "(";
				$sql .= implode(", ", $sql_field_arr);
			$sql .= ") values(";
		
		// ==================================================
		// 登録SQLを実行
		// insert_into_table() from about_postgres.php
		// ==================================================
		$result = insert_into_table($con, $sql, $sql_val_arr, $fname);
		if ($result == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			showErrorPage();
			exit;
		}
		
	// 処理区分が更新
	} else if ($division == "update") {
		// select句を生成
		$sql_field_arr = array(
			"report_title"
			, "job_id"
			, "registrant_name"
			, "registration_date"
			, "eis_id"
			, "eid_id"
			, "del_flag"
			, "shitagaki_flg"
			, "report_comment"
			, "edit_lock_flg"
			, "anonymous_report_flg"
			, "registrant_job"
			, "doctor_emp_id"
			, "kill_flg"
			, "first_send_time"
		);
		
		// 登録するデータを配列に格納
		$sql_val_arr = array(
			 pg_escape_string($report_title)		// report_title			：表題
			, $job_id								// job_id				：(下書き)様式の職種ID
			, pg_escape_string($registrant_name)	// registrant_name		：報告者氏名
			, $registration_date					// registration_date	：報告日
			, $eis_id								// eis_id				：(下書き)様式ID
			, $eid_id								// eid_id				：データID
			, $del_flag								// del_flag				：削除フラグ
			, $shitagaki_flg						// shitagaki_flg		：下書きフラグ
			, pg_escape_string($report_comment)		// report_comment		：(必ずNULL)
			, $edit_lock_flg						// edit_lock_flg		：SMロックフラグ
			, $anonymous_report_flg					// anonymous_report_flg	：匿名報告フラグ
			, $registrant_job						// registrant_job		：報告者職種
			, $doctor_emp_id						// doctor_emp_id		：主治医ID
			, $kill_flg								// kill_flg				：完全削除フラグ
			, $first_send_time						// first_send_time		：初回送信時間
		);
		
		$sql = "update inci_report set";
		
		$cond = "where";
			if (is_null($search_registrant_class)) {
				$cond .= " registrant_class is null";
			} else {
				$cond .= " registrant_class = " . $search_registrant_class;
				if (is_null($search_registrant_attribute)) {
					$cond .= " and registrant_attribute is null";
				} else {
					$cond .= " and registrant_attribute = " . $search_registrant_attribute;
					if (is_null($search_registrant_dept)) {
						$cond .= " and registrant_dept is null";
					} else {
						$cond .= " and registrant_dept = " . $search_registrant_dept;
						if ($class_cnt == 4) {
							if (is_null($search_registrant_room)) {
								$cond .= " and registrant_room is null";
							} else {
								$cond .= " and registrant_room = " . $search_registrant_room;
							}
						}
					}
				}
			}
			$cond .= " and registrant_id = '" . $emp_id . "'";
			$cond .= " and seq = '" . $seq . "'";
		
		// 重複チェック [重] の場合
		if ($target == "2") {
			// 重複チェックで取得したデータIDをwhere句に加える
			$cond .= " and eid_id = " . $target_id_arr["eid_id"];
		}
		
		// 複数重複の場合
		if ($dupli == "1") {
			// 重複チェックで取得したデータIDをwhere句に加える
			$cond .= " and report_id = " . $report_id;
		}
		
		// ==================================================
		// 更新SQLを実行
		// update_set_table() from about_postgres.php
		// ==================================================
		$result = update_set_table($con, $sql, $sql_field_arr, $sql_val_arr, $cond, $fname);
		if ($result == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			showErrorPage();
			exit;
		}
	}
	
	//==================================================
	// 報告書データ反映
	// set_report_content_from_xml() from self
	//==================================================
	set_report_content_from_xml($con, $fname, $session, $report_id, $eid_id, $report, $post);
	
	$mail_id = "";
	
	// 処理区分が挿入
	if ($division == "insert") {
		//==================================================
		// レポート内容をHTML形式で取得します
		// get_report_html_for_mail_message() from hiyari_mail.ini
		//==================================================
		$message = get_report_html_for_mail_message($con, $fname, $report_id, $eis_id, "", $session, $eis_title_id);
		
		// メール表示画面用にカット
		$message = '<br><br><table width="60%" border="0" cellspacing="0" cellpadding="0"><tr><td>' . $message . '</td></tr></table>';
		
		// デフォルトの送信先を取得
		$dupli_default_to_arr = get_new_report_default_tocc_list($con, $fname, "TO", $registrant_class, $registrant_attribute, $registrant_dept, $registrant_room, "");
		$dupli_default_cc_arr = get_new_report_default_tocc_list($con, $fname, "CC", $registrant_class, $registrant_attribute, $registrant_dept, $registrant_room, "");
		
		$default_to_arr = array();
		$default_cc_arr = array();
		
		// データをためておく配列
		$temp_arr = array();
		
		$to_index = 0;
		$cc_index = 0;
		$temp_index = 0;
		
		// TO、CC両方での重複データを削除する
		// TO
		for ($i = 0; $i < count($dupli_default_to_arr); $i++) {
			$to_id = $dupli_default_to_arr[$i]["emp_id"];
			$to_name = $dupli_default_to_arr[$i]["emp_name"];
			$to_ano = $dupli_default_to_arr[$i]["is_anonymous"];
			
			$flg = 0;
			
			for ($j = 0; $j < count($temp_arr); $j++) {
				$temp_id = $temp_arr[$j]["emp_id"];
				$temp_name = $temp_arr[$j]["emp_name"];
				$temp_ano = $temp_arr[$j]["is_anonymous"];
				
				if ($to_id == $temp_id && $to_name == $temp_name && $to_ano == $temp_ano) {
					$flg = 1;
				}
			}
			
			if ($flg == 0) {
				$default_to_arr[$to_index]["emp_id"] = $to_id;
				$default_to_arr[$to_index]["emp_name"] = $to_name;
				$default_to_arr[$to_index]["is_anonymous"] = $to_ano;
				
				$temp_arr[$temp_index]["emp_id"] = $to_id;
				$temp_arr[$temp_index]["emp_name"] = $to_name;
				$temp_arr[$temp_index]["is_anonymous"] = $to_ano;
				
				$to_index++;
				$temp_index++;
			}
		}
		// CC
		for ($i = 0; $i < count($dupli_default_cc_arr); $i++) {
			$cc_id = $dupli_default_cc_arr[$i]["emp_id"];
			$cc_name = $dupli_default_cc_arr[$i]["emp_name"];
			$cc_ano = $dupli_default_cc_arr[$i]["is_anonymous"];
			
			$flg = 0;
			
			for ($j = 0; $j < count($temp_arr); $j++) {
				$temp_id = $temp_arr[$j]["emp_id"];
				$temp_name = $temp_arr[$j]["emp_name"];
				$temp_ano = $temp_arr[$j]["is_anonymous"];
				
				if ($cc_id == $temp_id && $cc_name == $temp_name && $cc_ano == $temp_ano) {
					$flg = 1;
				}
			}
			
			if ($flg == 0) {
				$default_cc_arr[$cc_index]["emp_id"] = $cc_id;
				$default_cc_arr[$cc_index]["emp_name"] = $cc_name;
				$default_cc_arr[$cc_index]["is_anonymous"] = $cc_ano;
				
				$temp_arr[$temp_index]["emp_id"] = $cc_id;
				$temp_arr[$temp_index]["emp_name"] = $cc_name;
				$temp_arr[$temp_index]["is_anonymous"] = $cc_ano;
				
				$cc_index++;
				$temp_index++;
			}
		}
		
		$to_id_arr = "";
		$to_name_arr = "";
		$to_anonymous_arr = "";
		
		$flg = 0;
		foreach ($default_to_arr as $arr) {
			if ($flg == 1) {
				$to_id_arr .= ",";
				$to_name_arr .= ",";
				$to_anonymous_arr .= ",";
			}
			$to_id_arr .= $arr["emp_id"];
			$to_name_arr .= $arr["emp_name"];
			$to_anonymous_arr .= $arr["is_anonymous"];
			
			$flg = 1;
		}
		
		$cc_id_arr = "";
		$cc_name_arr = "";
		$cc_anonymous_arr = "";
		
		$flg = 0;
		foreach ($default_cc_arr as $arr) {
			if ($flg == 1) {
				$cc_id_arr .= ",";
				$cc_name_arr .= ",";
				$cc_anonymous_arr .= ",";
			}
			$cc_id_arr .= $arr["emp_id"];
			$cc_name_arr .= $arr["emp_name"];
			$cc_anonymous_arr .= $arr["is_anonymous"];
			
			$flg = 1;
		}
		
		// $mail_post_data = $_POST;
		$mail_post_data = array();
		$mail_post_data["to_emp_id_list"] = $to_id_arr;
		$mail_post_data["cc_emp_id_list"] = $cc_id_arr;
		$mail_post_data["to_emp_name_list"] = $to_name_arr;
		$mail_post_data["cc_emp_name_list"] = $cc_name_arr;
		$mail_post_data["to_emp_anonymous_list"] = $to_anonymous_arr;
		$mail_post_data["cc_emp_anonymous_list"] = $cc_anonymous_arr;
		$mail_post_data["mail_subject"] = $report_title;
		$mail_post_data["mail_message"] = $message;
		
		if ($row[0] > 0) {
			$mail_post_data["anonymous"] = "t";
		} else {
			$mail_post_data["anonymous"] = "";
		}
		
		$mail_post_data["marker"] = 0;
		
		//==================================================
		// メール送信処理
		// send_mail_from_submit_data() from hiyari_mail_input.ini
		//==================================================
		$mail_id = send_mail_from_submit_data($session, $con, $fname, $mail_post_data, $report_id, $job_id, $eis_id);
	}
	
	// ==================================================
	// 報告書履歴反映
	// get_new_report_no() from hiyari_report_class.php
	// ==================================================
	$rep_obj->set_report_rireki($report_id, $emp_id, $mail_id);
}

/**
 * 報告書データ反映
 *
 * @param onject  $con          接続コネクション
 * @param string  $fname        画面名
 * @param string  $session      セッションID
 * @param string  $report_id    報告書ID
 * @param string  $eid_id       データID
 * @param array   $report       XMLデータ [grpcasereport]
 * @param array   $post         画面送信データ
 */
// ※この関数は新規登録及びレポート編集(下書きを含む)にてレポート内容を更新する際に使用することを前提とします。
// 更新マージを行うパターンでは$input_grpsが必須です。
// SM専用項目がありえない場合は$sessionは不要。
function 	set_report_content_from_xml($con, $fname, $session, $report_id, $eid_id, $report, $post, $input_grps="")
{
	// 配列の初期化
	$fix_arr = array();
	
	// ==================================================
	// 固定データ配列を取得
	// get_fix_xml_data_array() from self
	// ==================================================
	$fix_arr = get_fix_xml_data_array();
	
	// (下書き)様式ID
	$eis_id = $post["sel_eis_id"];
	
	// 発生年 [Level2]
	$datyear = $report["DATYEAR"][0]["_value"];
	
	// 発生月 [Level2]
	// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
	$reg_arr[100][10] = (int) $report["DATMONTH"][0]["_attr"]["CODE"];
	
	// 発生曜日 [Level2]
	// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
	$reg_arr[100][20] = (int) $report["DATDAY"][0]["_attr"]["CODE"];
	
	// 曜日区分 [Level2]
	// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
	$reg_arr[100][30] = (int) $report["DATHOLIDAY"][0]["_attr"]["CODE"];
	
	// 発生年月日
	$reg_arr[100][5] = $datyear . "/" . substr("00" . $reg_arr[100][10], -2) . "/00";
	
	// 発生時間帯 [Level2]
	// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
	$reg_arr[100][40] = (int) $report["DATTIMEZONE"][0]["_attr"]["CODE"];
	if ($report["DATTIMEZONE"][0]["_attr"]["CODE"] == "90" && $report["DATTIMEZONE"][0]["_value"] != "") {
		$reg_arr[100][50] = $report["DATTIMEZONE"][0]["_value"];
	}
	
	// 医療の実施の有無 [Level2]
	// データベースには日本語が入ります
	$reg_arr[1100][10] = $fix_arr["DATEXECUTION"][$report["DATEXECUTION"][0]["_attr"]["CODE"]];
	
	// 治療の程度 [Level2]
	// データベースには日本語が入ります
	$reg_arr[1100][20] = $fix_arr["DATLEVEL"][$report["DATLEVEL"][0]["_attr"]["CODE"]];
	
	// 事故の程度 [Level2]
	// データベースには日本語が入ります
	$reg_arr[1100][30] = $fix_arr["DATSEQUELAE"][$report["DATSEQUELAE"][0]["_attr"]["CODE"]];
	if ($report["DATSEQUELAE"][0]["_attr"]["CODE"] == "90" && $report["DATSEQUELAE"][0]["_value"] != "") {
		$reg_arr[1100][40] = $report["DATSEQUELAE"][0]["_value"];
	}
	
	// 影響度 [Level2]
	// データベースには日本語が入ります
	$reg_arr[1100][50] = $fix_arr["DATINFLUENCE"][$report["DATINFLUENCE"][0]["_attr"]["CODE"]];
	
	// ==================================================
	// DEFデータの取得
	// get_def_array() from self
	// ==================================================
	$def_arr = get_def_array($con, $fname, 110, 60);
	
	// 発生場所 [Level2:Level3]
	$reg_arr[110][60] = "";
	$reg_arr[110][70] = "";
	
	foreach ($report["LSTSITE"][0]["DATSITE"] as $datsite) {
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($datsite["_attr"]["CODE"] == trim($def["def_code"])) {
				// TSV形式用
				if ($reg_arr[110][60] != "") {
					$reg_arr[110][60] .= "\t";
				}
				// 一致したら登録用配列に選択項目コードを格納
				$reg_arr[110][60] .= $def["easy_code"];
				
				// XMLコードが"99"で備考が入力されている場合のみ、備考を格納する
				if ($datsite["_attr"]["CODE"] == "99" && $datsite["_value"] != "") {
					// TSV形式用
					if ($reg_arr[110][70] != "") {
						$reg_arr[110][70] .= "\t";
					}
					
					$reg_arr[110][70] .= $datsite["_value"];
				}
				
				// 既存getOne()の為、取得できたら発生場所の全ループを抜ける
				break 2;
			}
		}
	}
	
	// 概要 [Level2]
	// ==================================================
	// SUPER ITEM IDデータの取得
	// convert_code_to_super_item_id() from self
	// ==================================================
	$reg_arr[900][10] = convert_code_to_super_item_id($con, $fname, $report["DATSUMMARY"][0]["_attr"]["CODE"]);
	if ($report["DATSUMMARY"][0]["_attr"]["CODE"] == "99" && $report["DATSUMMARY"][0]["_value"] != "") {
		$reg_arr[900][20] = $report["DATSUMMARY"][0]["_value"];
	}
	
	// 特に報告を求める事例 [Level2]
	// データベースには日本語が入ります
	$reg_arr[1110][10] = $fix_arr["DATSPECIALLY"][$report["DATSPECIALLY"][0]["_attr"]["CODE"]];
	$reg_arr[1110][20] = $report["DATSPECIALLY"][0]["_value"];
	
	// ==================================================
	// DEFデータの取得
	// get_def_array() from self
	// ==================================================
	$def_arr = get_def_array($con, $fname, 130, 90);
	
	// 関連診療科 [Level2:Level3]
	$reg_arr[130][90] = "";
	$reg_arr[130][100] = "";
	
	foreach ($report["LSTDEPARTMENT"][0]["DATDEPARTMENT"] as $datdepartment) {
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($datdepartment["_attr"]["CODE"] == trim($def["def_code"])) {
				// TSV形式用
				if ($reg_arr[130][90] != "") {
					$reg_arr[130][90] .= "\t";
				}
				// 一致したら登録用配列に選択項目コードを格納
				$reg_arr[130][90] .= $def["easy_code"];
				
				// XMLコードが"99"で備考が入力されている場合のみ、備考を格納する
				if ($datdepartment["_attr"]["CODE"] == "99" && $datdepartment["_value"] != "") {
					// TSV形式用
					if ($reg_arr[130][100] != "") {
						$reg_arr[130][100] .= "\t";
					}
					
					$reg_arr[130][100] .= $datdepartment["_value"];
				}
				break;
			}
		}
	}
	
	// 患者情報 [Level2]
	$grppatient = $report["GRPPATIENT"][0];
	
		// ==================================================
		// DEFデータの取得
		// get_def_array() from self
		// ==================================================
		$def_arr = get_def_array($con, $fname, 200, 10);
		
		// 患者の数 [:Level3]
		$reg_arr[200][10] = "";
		$reg_arr[200][20] = "";
		
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($grppatient["DATPATIENT"][0]["_attr"]["CODE"] == trim($def["def_code"])) {
				// 対応コードを格納
				$reg_arr[200][10] = $def["easy_code"];
				
				// XMLデータが"2"：複数人の場合は、備考を格納する
				if ($grppatient["DATPATIENT"][0]["_attr"]["CODE"] == "2" && $grppatient["DATPATIENT"][0]["_value"] != "") {
					$reg_arr[200][20] = $grppatient["DATPATIENT"][0]["_value"];
				}
				
				break;
			}
		}
		
		// 患者の年齢 [:Level3:Level4]
		foreach ($grppatient["LSTPATIENTAGE"][0]["DATPATIENTAGE"] as $datpatientage) {
			switch ($datpatientage["_attr"]["KBN"]) {
				// 歳
				// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(3桁)で対応
				case "years":
					if ($datpatientage["_value"] != "") {
						$reg_arr[210][40] = substr("000" . $datpatientage["_value"], -3);
					}
					break;
				// ヶ月
				// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(2桁)で対応
				case "months":
					if ($datpatientage["_value"] != "") {
						$reg_arr[210][50] = substr("00" . $datpatientage["_value"], -2);
					}
					break;
			}
		}
		
		// 患者の性別 [:Level3]
		// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
		$reg_arr[210][30] = (int) $grppatient["DATPATIENTSEX"][0]["_attr"]["CODE"];
		
		// ==================================================
		// DEFデータの取得
		// get_def_array() from self
		// ==================================================
		$def_arr = get_def_array($con, $fname, 230, 60);
		
		// 患者区分 [:Level3]
		$reg_arr[230][60] = "";
		
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($grppatient["DATPATIENTDIV"][0]["_attr"]["CODE"] == trim($def["def_code"])) {
				// 対応コードを格納
				$reg_arr[230][60] = $def["easy_code"];
				
				break;
			}
		}
		
	// 疾患名 [Level2:Level3]
	foreach ($report["LSTDISEASE"][0]["DATDISEASE"] as $datdisease) {
		switch ($datdisease["_attr"]["KBN"]) {
			// 事故(事例)に直接関連する疾患名
			case "disease0":
				$reg_arr[250][80] = $datdisease["_value"];
				break;
			// 関連診療科する疾患名1
			case "disease1":
				$reg_arr[260][90] = $datdisease["_value"];
				break;
			// 関連診療科する疾患名2
			case "disease2":
				$reg_arr[270][100] = $datdisease["_value"];
				break;
			// 関連診療科する疾患名3
			case "disease3":
				$reg_arr[280][110] = $datdisease["_value"];
				break;
		}
	}
	
	// ==================================================
	// DEFデータの取得
	// get_def_array() from self
	// ==================================================
	$def_arr = get_def_array($con, $fname, 290, 120);
	
	// 直前の患者の状態 [Level2:Level3]
	$reg_arr[290][120] = "";
	$reg_arr[290][130] = "";
	
	foreach ($report["LSTPATIENTSTATE"][0]["DATPATIENTSTATE"] as $datpatientstate) {
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($datpatientstate["_attr"]["CODE"] == trim($def["def_code"])) {
				// TSV形式用
				if ($reg_arr[290][120] != "") {
					$reg_arr[290][120] .= "\t";
				}
				// 一致したら登録用配列に選択項目コードを格納
				$reg_arr[290][120] .= $def["easy_code"];
				
				// XMLコードが"99"で備考が入力されている場合のみ、備考を格納する
				if ($datpatientstate["_attr"]["CODE"] == "99" && $datpatientstate["_value"] != "") {
					// TSV形式用
					if ($reg_arr[290][130] != "") {
						$reg_arr[290][130] .= "\t";
					}
					
					$reg_arr[290][130] .= $datpatientstate["_value"];
				}
				
				break;
			}
		}
	}
	
	// 発見者 [Level2]
	// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
	$reg_arr[3000][10] = (int) $report["DATDISCOVERER"][0]["_attr"]["CODE"];
	if ($report["DATDISCOVERER"][0]["_attr"]["CODE"] == "99" && $report["DATDISCOVERER"][0]["_value"] != "") {
		$reg_arr[3000][20] = $report["DATDISCOVERER"][0]["_value"];
	}
	
	// 当事者 [Level2:Level3]
	$i = 0;
	foreach ($report["LSTCONCERNED"][0]["GRPCONCERNED"] as $grpconcerned) {
		// ==================================================
		// DEFデータの取得
		// get_def_array() from self
		// ==================================================
		$def_arr = get_def_array($con, $fname, 305 . $i, 30);
		
		// 当事者職種 [:Level4]
		$reg_arr[305 . $i][30] = "";
		$reg_arr[305 . $i][35] = "";
		
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($grpconcerned["DATCONCERNED"][0]["_attr"]["CODE"] == trim($def["def_code"])) {
				// 対応コードを格納
				$reg_arr[305 . $i][30] = $def["easy_code"];
				
				// XMLコードが"99"で備考が入力されている場合のみ、備考を格納する
				if ($grpconcerned["DATCONCERNED"][0]["_attr"]["CODE"] == "99" && $grpconcerned["DATCONCERNED"][0]["_value"] != "") {
					$reg_arr[305 . $i][35] = $grpconcerned["DATCONCERNED"][0]["_value"];
				}
				
				break;
			}
		}
		
		// 専門医・認定医及びその他の医療従事者の専門・認定資格 [:Level4]
		$reg_arr[320 . $i][60] = $grpconcerned["DATQUALIFICATION"][0]["_value"];
		
		// 当事者職種経験 [:Level4:Level5]
		foreach ($grpconcerned["LSTEXPERIENCE"][0]["DATEXPERIENCE"] as $datexperience) {
			switch ($datexperience["_attr"]["KBN"]) {
				// 年
				// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(3桁)で対応
				case "years":
					if ($datexperience["_value"] != "") {
						$reg_arr[325 . $i][70] = substr("000" . $datexperience["_value"], -3);
					}
					break;
				// ヶ月
				// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(2桁)で対応
				case "months":
					if ($datexperience["_value"] != "") {
						$reg_arr[325 . $i][80] = substr("00" . $datexperience["_value"], -2);
					}
					break;
			}
		}
		
		// 当事者部署配属期間 [:Level4:Level5]
		foreach ($grpconcerned["LSTLENGTH"][0]["DATLENGTH"] as $datlength) {
			switch ($datlength["_attr"]["KBN"]) {
				// 年
				// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(3桁)で対応
				case "years":
					if ($datlength["_value"] != "") {
						$reg_arr[330 . $i][90] = substr("000" . $datlength["_value"], -3);
					}
					break;
				// ヶ月
				// inci_report_materialsにdef_codeが設定されていないので、ゼロ詰め(2桁)で対応
				case "months":
					if ($datlength["_value"] != "") {
						$reg_arr[330 . $i][100] = substr("00" . $datlength["_value"], -2);
					}
					break;
			}
		}
		
		// ==================================================
		// DEFデータの取得
		// get_def_array() from self
		// ==================================================
		$def_arr = get_def_array($con, $fname, 335 . $i, 110);
		
		// 直前1週間の当直・夜勤回数 [:Level4]
		$reg_arr[335 . $i][110] = "";
		$reg_arr[335 . $i][115] = "";
		
		foreach ($def_arr as $def) {
			if ($grpconcerned["DATDUTY"][0]["_attr"]["CODE"] == trim($def["def_code"])) {
				// 対応コードを格納
				$reg_arr[335 . $i][110] = $def["easy_code"];
				
				// XMLコードが"90"で備考が入力されている場合のみ、備考を格納する
				if ($grpconcerned["DATDUTY"][0]["_attr"]["CODE"] == "90" && $grpconcerned["DATDUTY"][0]["_value"] != "") {
					$reg_arr[335 . $i][115] = $grpconcerned["DATDUTY"][0]["_value"];
				}
			}
		}
		
		// ==================================================
		// DEFデータの取得
		// get_def_array() from self
		// ==================================================
		$def_arr = get_def_array($con, $fname, 340 . $i, 120);
		
		// 勤務形態 [:Level4]
		$reg_arr[340 . $i][120] = "";
		$reg_arr[340 . $i][130] = "";
		
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($grpconcerned["DATSHIFT"][0]["_attr"]["CODE"] == trim($def["def_code"])) {
				// 対応コードを格納
				$reg_arr[340 . $i][120] = $def["easy_code"];
				
				// XMLコードが"99"で備考が入力されている場合のみ、備考を格納する
				if ($grpconcerned["DATSHIFT"][0]["_attr"]["CODE"] == "99" && $grpconcerned["DATSHIFT"][0]["_value"] != "") {
					$reg_arr[340 . $i][130] = $grpconcerned["DATSHIFT"][0]["_value"];
				}
			}
		}
		
		// 直前1週間の勤務時間 [:Level4]
		$reg_arr[345 . $i][140] = $grpconcerned["DATHOURS"][0]["_value"];
		
		$i++;
	}
	
	// ==================================================
	// DEFデータの取得
	// get_def_array() from self
	// ==================================================
	$def_arr = get_def_array($con, $fname, 3500, 150);
	
	// 当事者以外の関連職種 [Level2:Level3]
	$reg_arr[3500][150] = "";
	$reg_arr[3500][160] = "";
	
	foreach ($report["LSTRELCATEGORY"][0]["DATRELCATEGORY"] as $datrelcategory) {
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($datrelcategory["_attr"]["CODE"] == trim($def["def_code"])) {
				// TSV形式用
				if ($reg_arr[3500][150] != "") {
					$reg_arr[3500][150] .= "\t";
				}
				// 一致したら登録用配列に選択項目コードを格納
				$reg_arr[3500][150] .= $def["easy_code"];
				
				// XMLコードが"99"で備考が入力されている場合のみ、備考を格納する
				if ($datrelcategory["_attr"]["CODE"] == "99" && $datrelcategory["_value"] != "") {
					// TSV形式用
					if ($reg_arr[3500][160] != "") {
						$reg_arr[3500][160] .= "\t";
					}
					
					$reg_arr[3500][160] .= $datrelcategory["_value"];
				}
				
				break;
			}
		}
	}
	
	// 概要 [Level2] により、分岐
	switch ($report["DATSUMMARY"][0]["_attr"]["CODE"]) {
		// 薬剤に関する項目
		case "02":
			// 薬剤 [Level2]
			$grpmedicine = $report["GRPMEDICINE"][0];
				
				// 種類 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "kind", $grpmedicine["DATTYPE"][0]["_attr"]["CODE"]);
				$reg_arr[910][10] = $tmp_arr[0];
				$reg_arr[920][10] = $tmp_arr[1];
				if (substr($grpmedicine["DATTYPE"][0]["_attr"]["CODE"], -2) == "99" && $grpmedicine["DATTYPE"][0]["_value"] != "") {
					$reg_arr[930][10] = $grpmedicine["DATTYPE"][0]["_value"];
				}
				
				// 関連医薬品 [:Level3]
				$grpmedicalsupplies = $grpmedicine["GRPMEDICALSUPPLIES"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][5] = $grpmedicalsupplies["DATNAME"][0]["_value"];
					
					// 厚労省コード [:Level4]
					$reg_arr[1000][10] = $grpmedicalsupplies["DATCODE"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][15] = $grpmedicalsupplies["DATMANUFACTURE"][0]["_value"];
					
				// 発生場面 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "scene", $grpmedicine["DATSCENE"][0]["_attr"]["CODE"]);
				$reg_arr[940][10] = $tmp_arr[0];
				$reg_arr[950][10] = $tmp_arr[1];
				if (substr($grpmedicine["DATSCENE"][0]["_attr"]["CODE"], -2) == "99" && $grpmedicine["DATSCENE"][0]["_value"] != "") {
					$reg_arr[960][10] = $grpmedicine["DATSCENE"][0]["_value"];
				}
				
				// 事故(事例)の内容 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "content", $grpmedicine["DATDETAIL"][0]["_attr"]["CODE"]);
				$reg_arr[970][10] = $tmp_arr[0];
				$reg_arr[980][10] = $tmp_arr[1];
				if (substr($grpmedicine["DATDETAIL"][0]["_attr"]["CODE"], -2) == "99" && $grpmedicine["DATDETAIL"][0]["_value"] != "") {
					$reg_arr[990][10] = $grpmedicine["DATDETAIL"][0]["_value"];
				}
				
			break;
			
		// 輸血に関する項目
		case "03":
			// 輸血 [Level2]
			$grptransfusion = $report["GRPTRANSFUSION"][0];
				
				// 発生場面 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "scene", $grptransfusion["DATSCENE"][0]["_attr"]["CODE"]);
				$reg_arr[940][10] = $tmp_arr[0];
				$reg_arr[950][10] = $tmp_arr[1];
				if (substr($grptransfusion["DATSCENE"][0]["_attr"]["CODE"], -2) == "99" && $grptransfusion["DATSCENE"][0]["_value"] != "") {
					$reg_arr[960][10] = $grptransfusion["DATSCENE"][0]["_value"];
				}
				
				// 事故(事例)の内容 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "content", $grptransfusion["DATDETAIL"][0]["_attr"]["CODE"]);
				$reg_arr[970][10] = $tmp_arr[0];
				$reg_arr[980][10] = $tmp_arr[1];
				if (substr($grptransfusion["DATDETAIL"][0]["_attr"]["CODE"], -2) == "99" && $grptransfusion["DATDETAIL"][0]["_value"] != "") {
					$reg_arr[990][10] = $grptransfusion["DATDETAIL"][0]["_value"];
				}
				
			break;
			
		// 治療・処置に関する項目
		case "04":
			// 治療・処置 [Level2]
			$grptreatment = $report["GRPTREATMENT"][0];
				
				// 種類 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "kind", $grptreatment["DATTYPE"][0]["_attr"]["CODE"]);
				$reg_arr[910][10] = $tmp_arr[0];
				$reg_arr[920][10] = $tmp_arr[1];
				if (substr($grptreatment["DATTYPE"][0]["_attr"]["CODE"], -2) == "99" && $grptreatment["DATTYPE"][0]["_value"] != "") {
					$reg_arr[930][10] = $grptreatment["DATTYPE"][0]["_value"];
				}
				
				// 医療材料・諸物品等 [:Level3]
				$grpmaterial = $grptreatment["GRPMATERIAL"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][20] = $grpmaterial["DATNAME"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][25] = $grpmaterial["DATMANUFACTURE"][0]["_value"];
					
					// 購入年月 [:Level4]
					$reg_arr[1000][30] = $grpmaterial["DATPURCHASE"][0]["_value"];
				
				// 発生場面 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "scene", $grptreatment["DATSCENE"][0]["_attr"]["CODE"]);
				$reg_arr[940][10] = $tmp_arr[0];
				$reg_arr[950][10] = $tmp_arr[1];
				if (substr($grptreatment["DATSCENE"][0]["_attr"]["CODE"], -2) == "99" && $grptreatment["DATSCENE"][0]["_value"] != "") {
					$reg_arr[960][10] = $grptreatment["DATSCENE"][0]["_value"];
				}
				
				// 事故(事例)の内容 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "content", $grptreatment["DATDETAIL"][0]["_attr"]["CODE"]);
				$reg_arr[970][10] = $tmp_arr[0];
				$reg_arr[980][10] = $tmp_arr[1];
				if (substr($grptreatment["DATDETAIL"][0]["_attr"]["CODE"], -2) == "99" && $grptreatment["DATDETAIL"][0]["_value"] != "") {
					$reg_arr[990][10] = $grptreatment["DATDETAIL"][0]["_value"];
				}
				
			break;
			
		// 医療機器等・医療材料の使用・管理に関する項目
		case "10":
			// 医療機器等 [Level2]
			$grpapparatus = $report["GRPAPPARATUS"][0];
				
				// 種類 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "kind", $grpapparatus["DATTYPE"][0]["_attr"]["CODE"]);
				$reg_arr[910][10] = $tmp_arr[0];
				$reg_arr[920][10] = $tmp_arr[1];
				if (substr($grpapparatus["DATTYPE"][0]["_attr"]["CODE"], -2) == "99" && $grpapparatus["DATTYPE"][0]["_value"] != "") {
					$reg_arr[930][10] = $grpapparatus["DATTYPE"][0]["_value"];
				}
				
				// 医療機器等 [:Level3]
				$grpequipment = $grpapparatus["GRPEQUIPMENT"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][35] = $grpequipment["DATNAME"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][40] = $grpequipment["DATMANUFACTURE"][0]["_value"];
					
					// 製造年月 [:Level4]
					$reg_arr[1000][45] = $grpequipment["DATDATEOFMANUFACTURE"][0]["_value"];
					
					// 購入年月 [:Level4]
					$reg_arr[1000][50] = $grpequipment["DATPURCHASE"][0]["_value"];
					
					// 直近の保守・点検年月 [:Level4]
					$reg_arr[1000][55] = $grpequipment["DATMAINTENANCE"][0]["_value"];
				
				// 医療材料・諸物品等 [:Level3]
				$grpmaterial = $grpapparatus["GRPMATERIAL"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][20] = $grpmaterial["DATNAME"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][25] = $grpmaterial["DATMANUFACTURE"][0]["_value"];
					
					// 購入年月 [:Level4]
					$reg_arr[1000][30] = $grpmaterial["DATPURCHASE"][0]["_value"];
				
				// 発生場面 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "scene", $grpapparatus["DATSCENE"][0]["_attr"]["CODE"]);
				$reg_arr[940][10] = $tmp_arr[0];
				$reg_arr[950][10] = $tmp_arr[1];
				if (substr($grpapparatus["DATSCENE"][0]["_attr"]["CODE"], -2) == "99" && $grpapparatus["DATSCENE"][0]["_value"] != "") {
					$reg_arr[960][10] = $grpapparatus["DATSCENE"][0]["_value"];
				}
				
				// 事故(事例)の内容 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "content", $grpapparatus["DATDETAIL"][0]["_attr"]["CODE"]);
				$reg_arr[970][10] = $tmp_arr[0];
				$reg_arr[980][10] = $tmp_arr[1];
				if (substr($grpapparatus["DATDETAIL"][0]["_attr"]["CODE"], -2) == "99" && $grpapparatus["DATDETAIL"][0]["_value"] != "") {
					$reg_arr[990][10] = $grpapparatus["DATDETAIL"][0]["_value"];
				}
				
			break;
			
		// ドレーン・チューブ類の使用・管理に関する項目
		case "06":
			// ドレーン・チューブ [Level2]
			$grpdraintube = $report["GRPDRAINTUBE"][0];
				
				// 種類 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "kind", $grpdraintube["DATTYPE"][0]["_attr"]["CODE"]);
				$reg_arr[910][10] = $tmp_arr[0];
				$reg_arr[920][10] = $tmp_arr[1];
				if (substr($grpdraintube["DATTYPE"][0]["_attr"]["CODE"], -2) == "99" && $grpdraintube["DATTYPE"][0]["_value"] != "") {
					$reg_arr[930][10] = $grpdraintube["DATTYPE"][0]["_value"];
				}
				
				// 医療材料・諸物品等 [:Level3]
				$grpmaterial = $grpdraintube["GRPMATERIAL"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][20] = $grpmaterial["DATNAME"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][25] = $grpmaterial["DATMANUFACTURE"][0]["_value"];
					
					// 購入年月 [:Level4]
					$reg_arr[1000][30] = $grpmaterial["DATPURCHASE"][0]["_value"];
				
				// 発生場面 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "scene", $grpdraintube["DATSCENE"][0]["_attr"]["CODE"]);
				$reg_arr[940][10] = $tmp_arr[0];
				$reg_arr[950][10] = $tmp_arr[1];
				if (substr($grpdraintube["DATSCENE"][0]["_attr"]["CODE"], -2) == "99" && $grpdraintube["DATSCENE"][0]["_value"] != "") {
					$reg_arr[960][10] = $grpdraintube["DATSCENE"][0]["_value"];
				}
				
				// 事故(事例)の内容 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "content", $grpdraintube["DATDETAIL"][0]["_attr"]["CODE"]);
				$reg_arr[970][10] = $tmp_arr[0];
				$reg_arr[980][10] = $tmp_arr[1];
				if (substr($grpdraintube["DATDETAIL"][0]["_attr"]["CODE"], -2) == "99" && $grpdraintube["DATDETAIL"][0]["_value"] != "") {
					$reg_arr[990][10] = $grpdraintube["DATDETAIL"][0]["_value"];
				}
				
			break;
			
		// 検査に関する項目
		case "08":
			// 検査 [Level2]
			$grpinspection = $report["GRPINSPECTION"][0];
				
				// 種類 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "kind", $grpinspection["DATTYPE"][0]["_attr"]["CODE"]);
				$reg_arr[910][10] = $tmp_arr[0];
				$reg_arr[920][10] = $tmp_arr[1];
				if (substr($grpinspection["DATTYPE"][0]["_attr"]["CODE"], -2) == "99" && $grpinspection["DATTYPE"][0]["_value"] != "") {
					$reg_arr[930][10] = $grpinspection["DATTYPE"][0]["_value"];
				}
				
				// 医療機器等 [:Level3]
				$grpequipment = $grpinspection["GRPEQUIPMENT"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][35] = $grpequipment["DATNAME"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][40] = $grpequipment["DATMANUFACTURE"][0]["_value"];
					
					// 製造年月 [:Level4]
					$reg_arr[1000][45] = $grpequipment["DATDATEOFMANUFACTURE"][0]["_value"];
					
					// 購入年月 [:Level4]
					$reg_arr[1000][50] = $grpequipment["DATPURCHASE"][0]["_value"];
					
					// 直近の保守・点検年月 [:Level4]
					$reg_arr[1000][55] = $grpequipment["DATMAINTENANCE"][0]["_value"];
				
				// 医療材料・諸物品等 [:Level3]
				$grpmaterial = $grpinspection["GRPMATERIAL"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][20] = $grpmaterial["DATNAME"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][25] = $grpmaterial["DATMANUFACTURE"][0]["_value"];
					
					// 購入年月 [:Level4]
					$reg_arr[1000][30] = $grpmaterial["DATPURCHASE"][0]["_value"];
				
				// 発生場面 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "scene", $grpinspection["DATSCENE"][0]["_attr"]["CODE"]);
				$reg_arr[940][10] = $tmp_arr[0];
				$reg_arr[950][10] = $tmp_arr[1];
				if (substr($grpinspection["DATSCENE"][0]["_attr"]["CODE"], -2) == "99" && $grpinspection["DATSCENE"][0]["_value"] != "") {
					$reg_arr[960][10] = $grpinspection["DATSCENE"][0]["_value"];
				}
				
				// 事故(事例)の内容 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "content", $grpinspection["DATDETAIL"][0]["_attr"]["CODE"]);
				$reg_arr[970][10] = $tmp_arr[0];
				$reg_arr[980][10] = $tmp_arr[1];
				if (substr($grpinspection["DATDETAIL"][0]["_attr"]["CODE"], -2) == "99" && $grpinspection["DATDETAIL"][0]["_value"] != "") {
					$reg_arr[990][10] = $grpinspection["DATDETAIL"][0]["_value"];
				}
				
			break;
			
		// 療養上の場面に関する項目
		case "09":
			// 療養上の世話 [Level2]
			$grprecuperating = $report["GRPRECUPERATING"][0];
				
				// 種類 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "kind", $grprecuperating["DATTYPE"][0]["_attr"]["CODE"]);
				$reg_arr[910][10] = $tmp_arr[0];
				$reg_arr[920][10] = $tmp_arr[1];
				if (substr($grprecuperating["DATTYPE"][0]["_attr"]["CODE"], -2) == "99" && $grprecuperating["DATTYPE"][0]["_value"] != "") {
					$reg_arr[930][10] = $grprecuperating["DATTYPE"][0]["_value"];
				}
				
				// 医療材料・諸物品等 [:Level3]
				$grpmaterial = $grprecuperating["GRPMATERIAL"][0];
					
					// 販売名 [:Level4]
					$reg_arr[1000][20] = $grpmaterial["DATNAME"][0]["_value"];
					
					// 製造販売業者名 [:Level4]
					$reg_arr[1000][25] = $grpmaterial["DATMANUFACTURE"][0]["_value"];
					
					// 購入年月 [:Level4]
					$reg_arr[1000][30] = $grpmaterial["DATPURCHASE"][0]["_value"];
				
				// 発生場面 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "scene", $grprecuperating["DATSCENE"][0]["_attr"]["CODE"]);
				$reg_arr[940][10] = $tmp_arr[0];
				$reg_arr[950][10] = $tmp_arr[1];
				if (substr($grprecuperating["DATSCENE"][0]["_attr"]["CODE"], -2) == "99" && $grprecuperating["DATSCENE"][0]["_value"] != "") {
					$reg_arr[960][10] = $grprecuperating["DATSCENE"][0]["_value"];
				}
				
				// 事故(事例)の内容 [:Level3]
				// ==================================================
				// ITEM IDデータ配列の取得
				// get_item_id_array() from self
				// ==================================================
				$tmp_arr = get_item_id_array($con, $fname, $reg_arr[900][10], "content", $grprecuperating["DATDETAIL"][0]["_attr"]["CODE"]);
				$reg_arr[970][10] = $tmp_arr[0];
				$reg_arr[980][10] = $tmp_arr[1];
				if (substr($grprecuperating["DATDETAIL"][0]["_attr"]["CODE"], -2) == "99" && $grprecuperating["DATDETAIL"][0]["_value"] != "") {
					$reg_arr[990][10] = $grprecuperating["DATDETAIL"][0]["_value"];
				}
				
			break;
	}
	
	// 実施した医療行為の目的 [Level2]
	$reg_arr[500][10] = $report["DATPURPOSE"][0]["_value"];
	
	// 事故（事例）の内容 [Level2]
	$reg_arr[520][30] = $report["DATCONTENTTEXT"][0]["_value"];
	
	// ==================================================
	// DEFデータの取得
	// get_def_array() from self
	// ==================================================
	$def_arr = get_def_array($con, $fname, 600, 10);
	
	// 発生要因 [Level2:Level3]
	$reg_arr[600][10] = "";
	$reg_arr[600][20] = "";
	
	foreach ($report["LSTFACTOR"][0]["DATFACTOR"] as $datfactor) {
		foreach ($def_arr as $def) {
			// XMLデータとデータベースのDEFコードを比較
			if ($datfactor["_attr"]["CODE"] == trim($def["def_code"])) {
				// TSV形式用
				if ($reg_arr[600][10] != "") {
					$reg_arr[600][10] .= "\t";
				}
				// 一致したら登録用配列に選択項目コードを格納
				$reg_arr[600][10] .= $def["easy_code"];
				
				// XMLコードの下2桁が"99"で備考が入力されている場合のみ、備考を格納する
				if (substr($datfactor["_attr"]["CODE"], -2) == "99" && $datfactor["_value"] != "") {
					// TSV形式用
					if ($reg_arr[600][20] != "") {
						$reg_arr[600][20] .= "\t";
					}
					
					// インデックスコードが不明 [マスタにそれらしいのが細分化されているので使用する為には確認要]
					$reg_arr[600][20] .= $datfactor["_value"];
				}
				
				break;
			}
		}
	}
	
	// 事故（事例）の背景要因の概要 [Level2]
	$reg_arr[610][20] = $report["DATBACKGROUND"][0]["_value"];
	
	// 事故調査委員会設置の有無 [Level2]
	// inci_report_materialsにdef_codeが設定されていないので、型変換で対応
	$reg_arr[590][90] = (int) $report["DATCOMMITTEE"][0]["_attr"]["CODE"];
	if ($report["DATCOMMITTEE"][0]["_attr"]["CODE"] == "99" && $report["DATCOMMITTEE"][0]["_value"] != "") {
		$reg_arr[590][91] = $report["DATCOMMITTEE"][0]["_value"];
	}
	
	// 改善策 [Level2]
	$reg_arr[620][30] = $report["DATIMPROVEMENTTEXT"][0]["_value"];
	
	// 患者影響レベル [画面]
    if ($post["design_mode"] == 1){
        $reg_arr[90][10] = $post["patient_level"];      //selectの値
    }
    else{
        $reg_arr[90][10] = $post["patient_level"][0];   //radioの値
    }
	
	// select句を生成
	$sql_field_arr = array(
		"eid_id"
		, "eis_id"
		, "report_id"
		, "grp_code"
		, "easy_item_code"
		, "input_item"
	);
	
	foreach ($reg_arr as $key1 => $val1) {
		foreach ($reg_arr[$key1] as $key2 => $val2) {
			// data部を生成
			$sql_val_arr = array(
				$eid_id			// eid_id			：データID
				, $eis_id		// eis_id			：(下書き)様式の職種ID
				, $report_id	// report_id		：報告書ID
				, $key1			// grp_code			：グループコード
				, $key2			// easy_item_code	：項目コード
				, $val2			// input_item		：項目値
			);
			
			if ($val2 != "") {
				// sql文を生成
				$sql = "insert into";
					$sql .= " inci_easyinput_data";
					$sql .= "(";
						$sql .= implode(", ", $sql_field_arr);
					$sql .= ") values(";
				
				// ==================================================
				// 登録SQLを実行
				// insert_into_table() from about_postgres.php
				// ==================================================
				$result = insert_into_table($con, $sql, $sql_val_arr, $fname);
				if ($result == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					showErrorPage();
					exit;
				}
			}
		}
	}

	// 発生日を取得
	$sql  = "select max(input_item) from inci_easyinput_data";
	$cond = "where eid_id = $eid_id and grp_code = 100 and easy_item_code = 5";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	$incident_date = pg_fetch_result($sel, 0, 0);

	// 評価予定期日を取得
	$sql  = "select max(input_item) from inci_easyinput_data";
	$cond = "where eid_id = $eid_id and grp_code = 620 and easy_item_code = 33";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	$evaluation_date = pg_fetch_result($sel, 0, 0);

	// 患者影響レベルを取得
	$sql  = "select max(input_item) from inci_easyinput_data";
	$cond = "where eid_id = $eid_id and grp_code = 90 and easy_item_code = 10";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	$inci_level = pg_fetch_result($sel, 0, 0);

	// レポートの発生日、評価予定期日、患者影響レベルを更新
	$sql = "update inci_report set";
	$set = array("incident_date", "evaluation_date", "inci_level");
	$setvalue = array($incident_date, $evaluation_date, $inci_level);
	$cond = "where report_id = $report_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
}

/**
 * XMLの固定内容データを連想配列に格納
 *
 * @return array 固定内容データ配列
 */
function get_fix_xml_data_array()
{
	$ret = array();
	
	$ret = array(
		"DATMONTH" => array(
			"01" => "１月"
			, "02" => "２月"
			, "03" => "３月"
			, "04" => "４月"
			, "05" => "５月"
			, "06" => "６月"
			, "07" => "７月"
			, "08" => "８月"
			, "09" => "９月"
			, "10" => "１０月"
			, "11" => "１１月"
			, "12" => "１２月"
		)
		, "DATDAY" => array(
			"01" => "日曜日"
			, "02" => "月曜日"
			, "03" => "火曜日"
			, "04" => "水曜日"
			, "05" => "木曜日"
			, "06" => "金曜日"
			, "07" => "土曜日"
		)
		, "DATHOLIDAY" => array(
			"01" => "平日"
			, "02" => "休日・祝日"
		)
		, "DATTIMEZONE" => array(
			"01" => "0:00〜1:59"
			, "02" => "2:00〜3:59"
			, "03" => "4:00〜5:59"
			, "04" => "6:00〜7:59"
			, "05" => "8:00〜9:59"
			, "06" => "10:00〜11:59"
			, "07" => "12:00〜13:59"
			, "08" => "14:00〜15:59"
			, "09" => "16:00〜17:59"
			, "10" => "18:00〜19:59"
			, "11" => "20:00〜21:59"
			, "12" => "22:00〜23:59"
			, "90" => "不明"
		)
		, "DATEXECUTION" => array(
			"01" => "実施あり"
			, "02" => "実施なし"
		)
		, "DATLEVEL" => array(
			"01" => "濃厚な治療"
			, "02" => "軽微な治療"
			, "03" => "治療なし"
			, "90" => "不明"
		)
		, "DATSEQUELAE" => array(
			"01" => "死亡"
			, "02" => "障害残存の可能性がある(高い)"
			, "03" => "障害残存の可能性がある(低い)"
			, "04" => "障害残存の可能性なし"
			, "05" => "障害なし"
			, "90" => "不明"
		)
		, "DATINFLUENCE" => array(
			"01" => "死亡もしくは重篤な状況に至ったと考えられる"
			, "02" => "濃厚な処置・治療が必要であると考えられる"
			, "03" => "軽微な処置・治療が必要もしくは処置・治療が不要と考えられる"
		)
		, "DATSITE" => array(
			"01" => "外来診察室"
			, "02" => "外来処置室"
			, "03" => "外来待合室"
			, "04" => "救急外来"
			, "05" => "救命救急センター"
			, "06" => "病室"
			, "07" => "病棟処置室"
			, "08" => "手術室"
			, "09" => "ICU"
			, "10" => "CCU"
			, "11" => "NICU"
			, "12" => "検査室"
			, "13" => "カテーテル検査室"
			, "14" => "放射線治療室"
			, "15" => "放射線撮影室"
			, "16" => "核医学検査室"
			, "17" => "透析室"
			, "18" => "分娩室"
			, "19" => "機能訓練室"
			, "20" => "トイレ"
			, "21" => "廊下"
			, "22" => "浴室"
			, "23" => "階段"
			, "24" => "薬局(調剤所)"
			, "90" => "不明"
			, "99" => "その他"
		)
		, "DATSUMMARY" => array(
			"02" => "薬剤"
			, "03" => "輸血"
			, "04" => "治療・処置"
			, "10" => "医療機器等"
			, "06" => "ドレーン・チューブ"
			, "08" => "検査"
			, "09" => "療養上の世話"
			, "99" => "その他"
		)
		, "DATSPECIALLY" => array(
			"090105" => "汚染された薬剤・材料・生体由来材料等の使用による事故"
			, "090107" => "院内感染による死亡や障害"
			, "090201" => "患者の自殺又は自殺企図"
			, "090202" => "入院患者の失踪"
			, "090204" => "患者の熱傷"
			, "090205" => "患者の感電"
			, "090206" => "医療施設内の火災による患者の死亡や障害"
			, "090207" => "間違った保護者の許への新生児の引渡し"
			, "999999" => "本事例は選択肢には該当しない"
		)
		, "DATDEPARTMENT" => array(
			"01" => "内科"
			, "02" => "麻酔科"
			, "03" => "循環器内科"
			, "04" => "神経科"
			, "05" => "呼吸器内科"
			, "06" => "消化器科"
			, "07" => "血液内科"
			, "08" => "循環器外科"
			, "09" => "アレルギー科"
			, "10" => "リウマチ科"
			, "11" => "小児科"
			, "12" => "外科"
			, "13" => "整形外科"
			, "14" => "形成外科"
			, "15" => "美容外科"
			, "16" => "脳神経外科"
			, "17" => "呼吸器外科"
			, "18" => "心臓血管外科"
			, "19" => "小児外科"
			, "20" => "ペインクリニック"
			, "21" => "皮膚科"
			, "22" => "泌尿器科"
			, "23" => "性病科"
			, "24" => "肛門科"
			, "25" => "産婦人科"
			, "26" => "産科"
			, "27" => "婦人科"
			, "28" => "眼科"
			, "29" => "耳鼻咽喉科"
			, "30" => "心療内科"
			, "31" => "精神科"
			, "32" => "リハビリテーション科"
			, "33" => "放射線科"
			, "34" => "歯科"
			, "35" => "矯正歯科"
			, "36" => "小児歯科"
			, "37" => "歯科口腔外科"
			, "90" => "不明"
			, "99" => "その他"
		)
		, "DATPATIENT" => array(
			"1" => "１人"
			, "2" => "複数人"
		)
		, "DATPATIENTAGE" => array(
			"years" => "歳"
			, "months" => "ヶ月"
		)
		, "DATPATIENTSEX" => array(
			"01" => "男性"
			, "02" => "女性"
		)
		, "DATPATIENTDIV" => array(
			"01" => "入院"
			, "04" => "外来"
		)
		, "DATDISEASE" => array(
			"disease0" => "事故(事例)に直接関連する疾患名"
			, "disease1" => "関連する疾患名１"
			, "disease2" => "関連する疾患名２"
			, "disease3" => "関連する疾患名３"
		)
		, "DATPATIENTSTATE" => array(
			"01" => "意識障害"
			, "02" => "視覚障害"
			, "03" => "聴覚障害"
			, "04" => "構音障害"
			, "05" => "精神障害"
			, "06" => "認知症・健忘"
			, "07" => "上肢障害"
			, "08" => "下肢障害"
			, "09" => "歩行障害"
			, "10" => "床上安静"
			, "11" => "睡眠中"
			, "13" => "薬剤の影響下"
			, "14" => "麻酔中・麻酔前後"
			, "99" => "その他特記する心身状態あり"
		)
		, "DATDISCOVERER" => array(
			"01" => "当事者本人"
			, "02" => "同職種者"
			, "03" => "他職種者"
			, "04" => "患者本人"
			, "05" => "家族・付き添い"
			, "06" => "他患者"
			, "99" => "その他"
		)
		, "LSTCONCERNED" => array(
			"01" => "１人"
			, "02" => "２人"
			, "03" => "３人"
			, "04" => "４人"
			, "05" => "５人"
			, "06" => "６人"
			, "07" => "７人"
			, "08" => "８人"
			, "09" => "９人"
			, "10" => "１０人"
			, "90" => "１１人以上"
		)
		, "DATCONCERNED" => array(
			"01" => "医師"
			, "02" => "歯科医師"
			, "03" => "看護師"
			, "04" => "准看護師"
			, "05" => "薬剤師"
			, "06" => "臨床工学技士"
			, "07" => "助産師"
			, "08" => "看護助手"
			, "09" => "診療放射線技師"
			, "10" => "臨床検査技師"
			, "12" => "管理栄養士"
			, "13" => "栄養士"
			, "14" => "調理師・調理従事者"
			, "11" => "理学療法士(PT)"
			, "16" => "作業療法士(OT)"
			, "15" => "言語聴覚士(ST)"
			, "17" => "衛生検査技師"
			, "18" => "歯科衛生士"
			, "19" => "歯科技工士"
			, "99" => "その他"
		)
		, "DATEXPERIENCE" => array(
			"years" => "年"
			, "months" => "ヶ月"
		)
		, "DATLENGTH" => array(
			"years" => "年"
			, "months" => "ヶ月"
		)
		, "DATDUTY" => array(
			"00" => "０回"
			, "01" => "１回"
			, "02" => "２回"
			, "03" => "３回"
			, "04" => "４回"
			, "05" => "５回"
			, "06" => "６回"
			, "07" => "７回"
			, "90" => "不明"
		)
		, "DATSHIFT" => array(
			"02" => "２交替"
			, "03" => "３交替"
			, "04" => "４交替"
			, "08" => "交替勤務なし"
			, "99" => "その他"
		)
		, "DATRELCATEGORY" => array(
			"01" => "医師"
			, "02" => "歯科医師"
			, "03" => "看護師"
			, "04" => "准看護師"
			, "05" => "薬剤師"
			, "06" => "臨床工学技士"
			, "07" => "助産師"
			, "08" => "看護助手"
			, "09" => "診療放射線技師"
			, "10" => "臨床検査技師"
			, "12" => "管理栄養士"
			, "13" => "栄養士"
			, "14" => "調理師・調理従事者"
			, "11" => "理学療法士(PT)"
			, "16" => "作業療法士(OT)"
			, "15" => "言語聴覚士(ST)"
			, "17" => "衛生検査技師"
			, "18" => "歯科衛生士"
			, "19" => "歯科技工士"
			, "99" => "その他"
		)
		, "DATTYPE" => array(
			// 薬剤
			"02" => array(
				"020401" => "血液製剤"
				, "020402" => "麻薬"
				, "020403" => "抗腫瘍剤"
				, "020404" => "循環器用薬"
				, "020405" => "抗糖尿病薬"
				, "020406" => "抗不安剤"
				, "020407" => "睡眠導入剤"
				, "020408" => "抗凝固剤"
				, "020499" => "その他"
			)
			// 治療・処置
			, "04" => array(
				"040101" => "開頭"
				, "040102" => "開胸"
				, "040103" => "開心"
				, "040104" => "開腹"
				, "040105" => "四肢"
				, "040106" => "鏡視下手術"
				, "040199" => "その他の手術"
				, "040201" => "全身麻酔(吸入麻酔+静脈麻酔)"
				, "040202" => "局所麻酔"
				, "040203" => "吸引麻酔"
				, "040204" => "静脈麻酔"
				, "040205" => "脊椎・硬膜外麻酔"
				, "040299" => "その他の麻酔"
				, "040301" => "経膣分娩"
				, "040302" => "帝王切開"
				, "040303" => "人工妊娠中絶"
				, "040399" => "その他の分娩・人工妊娠中絶等"
				, "040401" => "血液浄化療法(血液透析含む)"
				, "040402" => "IVR(血液カテーテル治療等)"
				, "040403" => "放射線治療"
				, "040404" => "ペインクリニック"
				, "040405" => "リハビリテーション"
				, "040406" => "観血的歯科治療"
				, "040407" => "非観血的歯科治療"
				, "040408" => "内視鏡的治療"
				, "040499" => "その他の治療"
				, "040501" => "中心静脈ライン"
				, "040502" => "末梢静脈ライン"
				, "040503" => "動脈ライン"
				, "040504" => "血液浄化用カテーテル"
				, "040505" => "栄養チューブ(NG・ED)"
				, "040506" => "尿道カテーテル"
				, "040509" => "ドレーンに関する処置"
				, "040508" => "創傷処置"
				, "040599" => "その他の一般的処置"
				, "040601" => "気管挿管"
				, "040602" => "気管切開"
				, "040603" => "心臓マッサージ"
				, "040604" => "酸素療法"
				, "040605" => "血管確保"
				, "040699" => "その他の救急措置"
			)
			// 医療機器等
			, "10" => array(
				"050101" => "人工呼吸器"
				, "050102" => "酸素療法機器"
				, "050103" => "麻酔器"
				, "050104" => "人工心肺"
				, "050105" => "除細動器"
				, "050106" => "IABP"
				, "050107" => "ペースメーカー"
				, "050108" => "輸血・輸注ポンプ"
				, "050109" => "血液浄化用機器"
				, "050110" => "保育器"
				, "050111" => "内視鏡"
				, "050112" => "低圧持続吸引器"
				, "050113" => "心電図・血液モニター"
				, "050114" => "パルスオキシメーター"
				, "050115" => "高気圧酸素療法装置"
				, "050116" => "歯科用回転研削器具"
				, "050117" => "歯科用根管治療用器具"
				, "050118" => "歯科補綴物・充填物"
				, "050199" => "その他の医療機器等"
			)
			// ドレーン・チューブ
			, "06" => array(
				"060101" => "中心静脈ライン"
				, "060102" => "末梢静脈ライン"
				, "060103" => "動脈ライン"
				, "060104" => "気管チューブ"
				, "060105" => "気管カニューレ"
				, "060106" => "栄養チューブ(NG・ED)"
				, "060107" => "尿道カテーテル"
				, "060108" => "胸腔ドレーン"
				, "060109" => "腹腔ドレーン"
				, "060110" => "脳室・脳槽ドレーン"
				, "060111" => "皮下持続吸引ドレーン"
				, "060112" => "硬膜外カテーテル"
				, "060113" => "血液浄化用カテーテル・回路"
				, "060114" => "三方活栓"
				, "060199" => "その他のドレーン・チューブ類"
			)
			// 検査
			, "08" => array(
				"080101" => "採血"
				, "080102" => "採尿"
				, "080103" => "採便"
				, "080104" => "採痰"
				, "080105" => "穿刺液"
				, "080199" => "その他の検体採取"
				, "080201" => "超音波検査"
				, "080202" => "心電図検査"
				, "080203" => "トレッドミル検査"
				, "080204" => "ホルター負荷心電図"
				, "080205" => "脳波検査"
				, "080206" => "筋電図検査"
				, "080207" => "肺機能検査"
				, "080299" => "その他の生理検査"
				, "080301" => "一般撮影"
				, "080302" => "ポータブル撮影"
				, "080303" => "CT"
				, "080304" => "MRI"
				, "080305" => "血管カテーテル撮影"
				, "080306" => "上部消化管撮影"
				, "080307" => "下部消化管撮影"
				, "080399" => "その他の画像検査"
				, "080401" => "上部消化管"
				, "080402" => "下部消化管"
				, "080403" => "気管支鏡"
				, "080499" => "その他の内視鏡検査"
				, "080501" => "耳鼻科検査"
				, "080502" => "眼科検査"
				, "080503" => "歯科検査"
				, "080505" => "検体検査"
				, "080506" => "血糖測定(病棟で実施したもの)"
				, "080507" => "病理検査"
				, "080508" => "核医学検査"
				, "080599" => "その他の機能検査"
			)
			// 療養上の世話
			, "09" => array(
				"090101" => "気管内・口腔内吸引"
				, "090102" => "体位交換"
				, "090103" => "清拭"
				, "090104" => "更衣介助"
				, "090106" => "入浴介助"
				, "090107" => "排泄介助"
				, "090109" => "移動介助"
				, "090110" => "搬送・移送"
				, "090115" => "罨法"
				, "090114" => "患者観察"
				, "090112" => "患者周辺物品管理"
				, "090111" => "体温管理"
				, "090113" => "配膳"
				, "090199" => "その他の療養上の世話"
				, "090201" => "経口摂取"
				, "090202" => "経管栄養"
				, "090203" => "食事介助"
				, "090301" => "散歩中"
				, "090302" => "移動中"
				, "090303" => "外出・外泊中"
				, "090304" => "食事中"
				, "090305" => "入浴中"
				, "090306" => "着替え中"
				, "090307" => "排泄中"
				, "090308" => "就寝中"
				, "090399" => "その他の療養上の場面"
			)
		)
		, "DATSCENE" => array(
			// 薬剤
			"02" => array(
				"010201" => "手書きによる処方箋の作成"
				, "010202" => "オーダリングによる処方箋の作成"
				, "010203" => "口頭による処方指示"
				, "010204" => "手書きによる処方の変更"
				, "010205" => "オーダリングによる処方の変更"
				, "010206" => "口頭による処方の変更"
				, "010299" => "その他の処方に関する場面"
				, "020401" => "内服薬調剤"
				, "020402" => "注射薬調剤"
				, "020403" => "血液製剤調剤"
				, "020404" => "外用薬調剤"
				, "020499" => "その他の調剤に関する場面"
				, "020301" => "内服薬製剤管理"
				, "020302" => "注射薬製剤管理"
				, "020303" => "血液製剤管理"
				, "020304" => "外用薬製剤管理"
				, "020399" => "その他の製剤管理に関する場面"
				, "020101" => "与薬準備"
				, "020201" => "皮下・筋肉注射"
				, "020202" => "静脈注射"
				, "020203" => "動脈注射"
				, "020204" => "抹消静脈点滴"
				, "020205" => "中心静脈注射"
				, "020206" => "内服"
				, "020207" => "外用"
				, "020208" => "坐剤"
				, "020209" => "吸入"
				, "020210" => "点鼻・点耳・点眼"
				, "020299" => "その他の与薬に関する場面"
			)
			// 輸血
			, "03" => array(
				"010201" => "手書きによる処方箋の作成"
				, "010202" => "オーダリングによる処方箋の作成"
				, "010203" => "口頭による処方指示"
				, "010204" => "手書きによる処方の変更"
				, "010205" => "オーダリングによる処方の変更"
				, "010206" => "口頭による処方の変更"
				, "010299" => "その他の処方に関する場面"
				, "030101" => "準備"
				, "030102" => "実施"
				, "030199" => "その他の輸血検査に関する場面"
				, "030201" => "準備"
				, "030202" => "実施"
				, "030299" => "その他の放射線照射に関する場面"
				, "030301" => "製剤の交付"
				, "030399" => "その他の輸血準備に関する場面"
				, "030401" => "実施"
				, "030499" => "その他の輸血実施に関する場面"
			)
			// 治療・処置
			, "04" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040199" => "その他の管理に関する場面"
				, "040201" => "準備"
				, "040299" => "その他の準備に関する場面"
				, "040301" => "実施"
				, "040399" => "その他の治療・処置に関する場面"
			)
			// 医療機器等
			, "10" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// ドレーン・チューブ
			, "06" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// 検査
			, "08" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// 療養上の世話
			, "09" => array(
				"010301" => "手書きによる計画又は指示の作成"
				, "010302" => "オーダリングによる計画又は指示の作成"
				, "010303" => "口頭による計画又は指示"
				, "010304" => "手書きによる計画又は指示の変更"
				, "010305" => "オーダリングによる計画又は指示の変更"
				, "010306" => "口頭による計画又は指示の変更"
				, "010399" => "その他の計画又は指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "実施中"
			)
		)
		, "DATDETAIL" => array(
			// 薬剤
			"02" => array(
				"010201" => "処方忘れ"
				, "010202" => "処方遅延"
				, "010203" => "処方量間違い"
				, "010204" => "重複処方"
				, "010205" => "禁忌薬剤の処方"
				, "010206" => "対象患者処方間違い"
				, "010207" => "処方薬剤間違い"
				, "010208" => "処方単位間違い"
				, "010209" => "投与方法処方間違い"
				, "010299" => "その他の処方に関する内容"
				
				, "020212" => "調剤忘れ"
				, "020201" => "処方箋・注射箋鑑査間違い"
				, "020202" => "秤量間違い調剤"
				, "020203" => "数量間違い"
				, "020204" => "分包間違い"
				, "020205" => "規格間違い調剤"
				, "020206" => "単位間違い調剤"
				, "020207" => "薬剤取り違え調剤"
				, "020208" => "説明文書の取り違え"
				, "020209" => "交付患者間違い"
				, "020210" => "薬剤・製剤の取り違え交付"
				, "020211" => "期限切れ製剤の交付"
				, "020299" => "その他の調剤に関する内容"
				
				, "020302" => "薬袋・ボトルの記載間違い"
				, "020305" => "異物混入"
				, "020306" => "細菌汚染"
				, "020307" => "期限切れ製剤"
				, "020399" => "その他の製剤管理に関する内容"
				
				, "020114" => "過剰与薬準備"
				, "020115" => "過少与薬準備"
				, "020104" => "与薬時間・日付間違い"
				, "020105" => "重複与薬"
				, "020116" => "禁忌薬剤の与薬"
				, "020107" => "投与速度速すぎ"
				, "020108" => "投与速度遅すぎ"
				, "020109" => "患者間違い"
				, "020110" => "薬剤間違い"
				, "020111" => "単位間違い"
				, "020112" => "投与方法間違い"
				, "020113" => "無投薬"
				, "020117" => "混合間違い"
				, "020199" => "その他の与薬準備に関する内容"
				
				, "020414" => "過剰投与"
				, "020415" => "過少投与"
				, "020404" => "投与時間・日付間違い"
				, "020405" => "重複投与"
				, "020416" => "禁忌薬剤の投与"
				, "020407" => "投与速度速すぎ"
				, "020408" => "投与速度遅すぎ"
				, "020409" => "患者間違い"
				, "020410" => "薬剤間違い"
				, "020411" => "単位間違い"
				, "020412" => "投与方法間違い"
				, "020413" => "無投薬"
				, "020499" => "その他の与薬に関する内容"
			)
			// 輸血
			, "03" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010110" => "指示量間違い"
				, "010111" => "重複指示"
				, "010112" => "禁忌薬剤の指示"
				, "010113" => "対象患者指示間違い"
				, "010114" => "指示薬剤間違い"
				, "010115" => "指示単位間違い"
				, "010116" => "投与方法指示間違い"
				, "010199" => "その他の指示出しに関する内容"
				
				, "030101" => "未実施"
				, "030102" => "検体取り違え"
				, "030105" => "判定間違い"
				, "030104" => "結果記入・入力間違い"
				, "030199" => "その他の輸血検査に関する内容"
				
				, "030201" => "未実施"
				, "030202" => "過剰照射"
				, "030205" => "過少照射"
				, "030203" => "患者間違い"
				, "030204" => "製剤間違い"
				, "030299" => "その他の放射線照射に関する内容"
				
				, "020302" => "薬袋・ボトルの記載間違い"
				, "020305" => "異物混入"
				, "020306" => "細菌汚染"
				, "020307" => "期限切れ製剤"
				, "020399" => "その他の輸血管理に関する内容"
				
				, "020114" => "過剰与薬準備"
				, "020115" => "過少与薬準備"
				, "020104" => "与薬時間・日付間違い"
				, "020105" => "重複与薬"
				, "020116" => "禁忌薬剤の与薬"
				, "020107" => "投与速度速すぎ"
				, "020108" => "投与速度遅すぎ"
				, "020109" => "患者間違い"
				, "020110" => "薬剤間違い"
				, "020111" => "単位間違い"
				, "020112" => "投与方法間違い"
				, "020113" => "無投薬"
				, "020199" => "その他の輸血準備に関する内容"
				
				, "020214" => "過剰投与"
				, "020215" => "過少投与"
				, "020204" => "投与時間・日付間違い"
				, "020205" => "重複投与"
				, "020216" => "禁忌薬剤の投与"
				, "020207" => "投与速度速すぎ"
				, "020208" => "投与速度遅すぎ"
				, "020209" => "患者間違い"
				, "020210" => "薬剤間違い"
				, "020211" => "単位間違い"
				, "020212" => "投与方法間違い"
				, "020213" => "無投薬"
				, "020299" => "その他の輸血実施に関する内容"
			)
			// 治療・処置
			, "04" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010116" => "治療・処置指示間違い"
				, "010117" => "日程間違い"
				, "010118" => "時間間違い"
				, "010199" => "その他の治療・処置の指示に関する内容"
				
				, "040301" => "治療・処置の管理"
				, "040399" => "その他の治療・処置の管理に関する内容"
				
				, "040201" => "医療材料取り違え"
				, "040202" => "患者体位の誤り"
				, "040203" => "消毒・清潔操作の誤り"
				, "040299" => "その他の治療・処置の準備に関する内容"
				
				, "040101" => "患者間違い"
				, "040102" => "部位取違え"
				, "040105" => "方法(手技)の誤り"
				, "040106" => "未実施・忘れ"
				, "040107" => "中止・延期"
				, "040108" => "日程・時間の誤り"
				, "040109" => "順番の誤り"
				, "040110" => "不必要行為の実施"
				, "040114" => "誤嚥"
				, "040115" => "誤飲"
				, "040116" => "異物の体内残存"
				, "040104" => "診察・治療・処置等その他の取違え"
				, "040199" => "その他の治療・処置の実施に関する内容"
			)
			// 医療機器等
			, "10" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "使用方法指示間違い"
				, "010199" => "その他の医療機器等・医療材料の使用に関する内容"
				
				, "050301" => "保守・点検不良"
				, "050302" => "保守・点検忘れ"
				, "050303" => "使用中の点検・管理ミス"
				, "050304" => "破損"
				, "050399" => "その他の医療機器等・医療材料の管理に関する内容"
				
				, "050201" => "組み立て"
				, "050202" => "設定条件間違い"
				, "050203" => "設定忘れ"
				, "050204" => "電源入れ忘れ"
				, "050205" => "警報設定忘れ"
				, "050206" => "警報設定範囲間違い"
				, "050207" => "便宜上の警報解除後の再設定忘れ"
				, "050208" => "消毒・清潔操作の誤り"
				, "050209" => "使用前の点検・管理ミス"
				, "050210" => "破損"
				, "050299" => "その他の医療機器等・医療材料の準備に関する内容"
				
				, "050105" => "医療機器等・医療材料の不適切使用"
				, "050104" => "誤作動"
				, "050106" => "故障"
				, "050116" => "破損"
				, "050199" => "その他の医療機器等・医療材料の使用に関する内容"
			)
			// ドレーン・チューブ
			, "06" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "使用方法指示間違い"
				, "010199" => "その他のドレーン・チューブ類の使用・管理の指示に関する内容"
				
				, "060301" => "点検忘れ"
				, "060302" => "点検不良"
				, "060303" => "使用中の点検・管理ミス"
				, "060304" => "破損"
				, "060399" => "その他のドレーン・チューブ類の管理に関する内容"
				
				, "060201" => "組み立て"
				, "060202" => "設定条件間違い"
				, "060203" => "設定忘れ"
				, "060204" => "消毒・清潔操作の誤り"
				, "060205" => "使用前の点検・管理ミス"
				, "060299" => "その他のドレーン・チュ−ブ類の準備に関する内容"
				
				, "060101" => "点滴漏れ"
				, "060102" => "自己抜去"
				, "060103" => "自然抜去"
				, "060104" => "接続はずれ"
				, "060105" => "未接続"
				, "060106" => "閉塞"
				, "060107" => "切断・破損"
				, "060108" => "接続間違い"
				, "060109" => "三方活栓操作間違い"
				, "060110" => "ルートクランプエラー"
				, "060111" => "空気混入"
				, "060112" => "誤作動"
				, "060113" => "故障"
				, "060114" => "ドレーン・チューブ類の不適切使用"
				, "060199" => "その他のドレーン・チューブ類の使用に関する内容"
			)
			// 検査
			, "08" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "指示検査の間違い"
				, "010199" => "その他の検査の指示に関する内容"
				
				, "070301" => "分析機器・器具管理"
				, "070302" => "試薬管理"
				, "070303" => "データ紛失"
				, "070304" => "計算・入力・暗記"
				, "070399" => "その他の検査の管理に関する内容"
				
				, "070201" => "患者取違え"
				, "070204" => "検体取違え"
				, "070205" => "検体紛失"
				, "070210" => "検査機器・器具の準備"
				, "070206" => "検体破損"
				, "070299" => "その他の検査の準備に関する内容"
				
				, "070101" => "患者取違え"
				, "070104" => "検体取違え"
				, "070115" => "試薬の間違い"
				, "070105" => "検体紛失"
				, "070102" => "検査の手技・判定技術の間違い"
				, "070103" => "検体採取時のミス"
				, "070106" => "検体破損"
				, "070107" => "検体のコンタミネーション"
				, "070111" => "データ取違え"
				, "070114" => "結果報告"
				, "070199" => "その他の検査の実施に関する内容"
			)
			// 療養上の世話
			, "09" => array(
				"010101" => "計画忘れ又は指示出し忘れ"
				, "010102" => "計画又は指示の遅延"
				, "010111" => "計画又は指示の対象患者間違い"
				, "010119" => "計画又は指示内容間違い"
				, "010199" => "その他の療養上の世話の計画又は指示に関する内容"
				
				, "080104" => "拘束・抑制"
				, "080505" => "給食の内容の間違い"
				, "080109" => "安静指示"
				, "080110" => "禁食指示"
				, "080306" => "外出・外泊許可"
				, "080405" => "異物混入"
				, "080101" => "転倒"
				, "080102" => "転落"
				, "080103" => "衝突"
				, "080106" => "誤嚥"
				, "080107" => "誤飲"
				, "080108" => "誤配膳"
				, "080202" => "遅延"
				, "080203" => "実施忘れ"
				, "080204" => "搬送先間違い"
				, "080205" => "患者間違い"
				, "080404" => "延食忘れ"
				, "080403" => "中止の忘れ"
				, "080301" => "自己管理薬飲み忘れ・注射忘れ"
				, "080305" => "自己管理薬注入忘れ"
				, "080303" => "自己管理薬取違え摂取"
				, "080406" => "不必要行為の実施"
				, "089999" => "その他の療養上の世話の管理・準備・実施に関する内容"
			)
		)
		, "DATFACTOR" => array(
			"010101" => "確認を怠った"
			, "010102" => "観察を怠った"
			, "010103" => "報告が遅れた(怠った)"
			, "010104" => "記録などに不備があった"
			, "010105" => "連携ができていなかった"
			, "010106" => "患者への説明が不十分であった(怠った)"
			, "010107" => "判断を誤った"
			, "010201" => "知識が不足していた"
			, "010202" => "技術・手技が未熟だった"
			, "010203" => "勤務状況が繁忙だった"
			, "010204" => "通常とは異なる身体的条件下にあった"
			, "010205" => "通常とは異なる心理的条件下にあった"
			, "010299" => "その他"
			, "010301" => "コンピュータシステム"
			, "010302" => "医薬品"
			, "010303" => "医療機器"
			, "010304" => "施設・設備"
			, "010305" => "諸物品"
			, "010306" => "患者側"
			, "010399" => "その他"
			, "010401" => "教育・訓練"
			, "010402" => "仕組み"
			, "010403" => "ルールの不備"
			, "010499" => "その他"
		)
		, "DATCOMMITTEE" => array(
			"05" => "既設の医療安全に関する委員会等で対応"
			, "07" => "内部調査委員会設置(予定も含む)"
			, "08" => "外部調査委員会設置(予定も含む)"
			, "06" => "現在顕著宇宙で対応は未定"
			, "99" => "その他"
		)
	);
	
	return $ret;
}

/**
 * ページング処理に必要な情報を設定
 *
 * @param in  int $page          現在のページ番号
 * @param in  int $data_cnt      取得したデータ件数
 * @param in  int $disp_cnt      画面に表示する最大件数
 * @param out int &$ret_s_index  表示開始インデックス
 * @param out int &$ret_e_index  表示終了インデックス
 * @param out int &$ret_page_max 最大ページ数
 */
function get_data_for_paging($page, $data_cnt, $disp_cnt, &$ret_s_index, &$ret_e_index, &$ret_page_max)
{
	//表示開始インデックス
	$ret_s_index = ($page - 1) * $disp_cnt;
	
	// 表示終了インデックス
	$ret_e_index   = $ret_s_index + $disp_cnt - 1;
	
	// 最大ページ数
	$ret_page_max  = floor($data_cnt / $disp_cnt);
	if ($data_cnt % $disp_cnt > 0) {
		$ret_page_max++;
	}
}
