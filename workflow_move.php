<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("application_workflow_common_class.php");
require_once("Cmx.php");
require_once("aclg_set.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, 3, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//--------------------------------
// パラメータ
// $type
// $src_cate_id
// $src_folder_id

// $dest_cate_id
// $dest_folder_id
// $wkfw_id
/*
echo("type=".$type."<BR>");
echo("src_cate_id=".$src_cate_id."<BR>");
echo("src_folder_id=".$src_folder_id."<BR>");

echo("dest_cate_id=".$dest_cate_id."<BR>");
echo("dest_folder_id=".$dest_folder_id."<BR>");
echo("wkfw_id=".$wkfw_id."<BR>");
*/
//--------------------------------

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

$obj = new application_workflow_common_class($con, $fname);

// トランザクションの開始
pg_query($con, "begin");


switch ($type)
{
	// ドラッグ元が「すべて」フォルダ内のワークフロー(本体ワークフロー)の場合
	case "all":

		// ドラッグ先が「ゴミ箱」の場合
		if($dest_cate_id == "" && $dest_folder_id == "")
		{
			// ゴミ箱移動
			update_delflg_wkfwmst_real($con, $fname, $wkfw_id, "t");
			$arr_alias_wkfw_id = $obj->get_alias_wkfw_id($wkfw_id);
			foreach($arr_alias_wkfw_id as $alias)
			{
				$alias_wkfw_id = $alias["alias_wkfw_id"];
				update_delflg_wkfwmst_alias($con, $fname, $alias_wkfw_id, "t");
			}

			update_delflg_wkfwaliasmng_for_real($con, $fname, $wkfw_id);
		}
		// ドラッグ先が「すべて以外」フォルダの場合(エイリアスコピー処理)
		else
		{
			create_alias($obj, $wkfw_id, $dest_cate_id, $dest_folder_id);
		}

		break;

	// ドラッグ元が「ゴミ箱」アイコン内のワークフロー(本体ワークフロー)の場合
	case "dust":

		// ドラッグ先が「すべて」フォルダの場合
		if($dest_cate_id == "" && $dest_folder_id == "")
		{
			// 「すべて」フォルダ移動
			update_delflg_wkfwmst_real($con, $fname, $wkfw_id, "f");
		}

		break;

	// ドラッグ元が「すべて以外」フォルダ内のワークフロー(エイリアスワークフロー)の場合
	case "alias":

		// ドラッグ先が「すべて以外」フォルダの場合
		if($dest_cate_id != "")
		{
			update_folder($con, $fname, $wkfw_id, $dest_cate_id, $dest_folder_id);
		}
		break;

	// ドラッグ元がカテゴリの場合
	case "category":

		$obj->move_category($src_cate_id, $dest_cate_id, $dest_folder_id);
		break;

	// ドラッグ元がフォルダの場合
	case "folder":

		$obj->move_folder($src_cate_id, $src_folder_id, $dest_cate_id, $dest_folder_id, false);
		break;

	default:
		break;
}



// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面を再表示
if($type == "all" || $type == "dust")
{
	echo("<script type=\"text/javascript\">location.href = 'workflow_menu.php?session=$session&select_box=ALL&page=$page'</script>");
}
else
{
	echo("<script type=\"text/javascript\">location.href = 'workflow_menu.php?session=$session&selected_cate=$dest_cate_id&selected_folder=$dest_folder_id'</script>");
}



// エイリアスワークフローのフォルダ移動
function update_folder($con, $fname, $wkfw_id, $wkfw_type, $wkfw_folder_id)
{
	$wkfw_folder_id = ($wkfw_folder_id == "") ? null : $wkfw_folder_id;

	$sql = "update wkfwmst set";
	$set = array("wkfw_type", "wkfw_folder_id", "wkfw_del_flg");
	$setvalue = array($wkfw_type, $wkfw_folder_id, "f");
	$cond = "where wkfw_id = $wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


// 本体ワークフローの論理削除フラグ更新
function update_delflg_wkfwmst_real($con, $fname, $wkfw_id, $del_flg)
{
	$sql = "update wkfwmst_real set";
	$set = array("wkfw_del_flg");
	$setvalue = array($del_flg);
	$cond = "where wkfw_id = $wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// エイリアスワークフローの論理削除フラグ更新
function update_delflg_wkfwmst_alias($con, $fname, $wkfw_id, $del_flg)
{
	$sql = "update wkfwmst set";
	$set = array("wkfw_del_flg");
	$setvalue = array($del_flg);
	$cond = "where wkfw_id = $wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// エイリアス管理論理削除
function update_delflg_wkfwaliasmng_for_real($con, $fname, $real_wkfw_id)
{
	$sql = "update wkfwaliasmng set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where real_wkfw_id = $real_wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


// エイリアスワークフロー作成(本体ワークフローコピー処理)
function create_alias($obj, $real_wkfw_id, $wkfw_type, $wkfw_folder_id)
{
	// エイリアスワークフローＩＤ採番
	$alias_wkfw_id = $obj->get_max_wkfw_id("ALIAS");
	$alias_wkfw_id = intval($alias_wkfw_id) + 1;

	// エイリアス管理登録
	$alias_wkfw_no = $obj->get_max_alias_wkfw_no($real_wkfw_id);
	$alias_wkfw_no = intval($alias_wkfw_no) + 1;

	$obj->regist_wkfwaliasmng($real_wkfw_id, $alias_wkfw_id, $alias_wkfw_no);

	// ワークフロー情報登録
	$obj->regist_copy_wkfwmst($real_wkfw_id, $alias_wkfw_id, $wkfw_type, $wkfw_folder_id);

	// テンプレート履歴登録
	$arr_wkfwmst       = $obj->get_wkfwmst($alias_wkfw_id);;
	$wkfw_content      = $arr_wkfwmst[0]["wkfw_content"];
	$wkfw_content_type = $arr_wkfwmst[0]["wkfw_content_type"];
	// 本文形式タイプのデフォルトを「テキスト」とする
	if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

	if($wkfw_content_type == "2")
	{
		$obj->regist_wkfw_template_history($alias_wkfw_id, 1, $wkfw_content);
	}

	// ワークフロー・承認者管理情報登録
	$obj->regist_copy_wkfwapvmng($real_wkfw_id, $alias_wkfw_id);

	// 部署役職、職員、委員会・ＷＧ登録
	$obj->regist_copy_wkfwapvdtl($real_wkfw_id, $alias_wkfw_id);
	$obj->regist_copy_wkfwapvpstdtl($real_wkfw_id, $alias_wkfw_id);
	$obj->regist_copy_wkfwpjtdtl($real_wkfw_id, $alias_wkfw_id);
	$obj->regist_copy_wkfwapvsectdtl($real_wkfw_id, $alias_wkfw_id);

	// 申請者以外の結果通知登録
	$obj->regist_copy_wkfwnoticemng($real_wkfw_id, $alias_wkfw_id);
	$obj->regist_copy_wkfwnoticedtl($real_wkfw_id, $alias_wkfw_id);
	$obj->regist_copy_wkfwnoticestdtl($real_wkfw_id, $alias_wkfw_id);
	$obj->regist_copy_wkfwnoticepjtdtl($real_wkfw_id, $alias_wkfw_id);
	$obj->regist_copy_wkfwnoticesectdtl($real_wkfw_id, $alias_wkfw_id);

	// 前提となる申請書登録
	$obj->regist_copy_wkfwfprecond($real_wkfw_id, $alias_wkfw_id);

	// カテゴリ用アクセス権（科）登録
	$obj->regist_copy_wkfw_refdept($real_wkfw_id, $alias_wkfw_id);

	// カテゴリ用アクセス権（役職）登録
	$obj->regist_copy_wkfw_refst($real_wkfw_id, $alias_wkfw_id);

	// カテゴリ用アクセス権（職員）
	$obj->regist_copy_wkfw_refemp($real_wkfw_id, $alias_wkfw_id);

	// ワークファイル情報登録
	$obj->regist_copy_wkfwfile($real_wkfw_id, $alias_wkfw_id);

	// ワークファイル履歴登録
	$arr_wkfwfile = $obj->get_wkfwfile($alias_wkfw_id);

	foreach($arr_wkfwfile as $wkfwfile)
	{
		$wkfwfile_no = $wkfwfile["wkfwfile_no"];
		$wkfwfile_name = $wkfwfile["wkfwfile_name"];
		$obj->regist_wkfwfile_history($alias_wkfw_id, $wkfwfile_no, 1, $wkfwfile_name);
	}

	// ファイルコピー
	foreach($arr_wkfwfile as $wkfwfile)
	{
		$wkfwfile_no = $wkfwfile["wkfwfile_no"];
		$wkfwfile_name = $wkfwfile["wkfwfile_name"];
		$ext = strrchr($wkfwfile_name, ".");
		
		$tmp_filename = "workflow/real/{$real_wkfw_id}_{$wkfwfile_no}{$ext}";
		copy($tmp_filename, "workflow/{$alias_wkfw_id}_{$wkfwfile_no}{$ext}");
	}

	// 合議者リスト（事前登録職員）2012.5.21
 	$obj->regist_copy_wkfwcouncil($real_wkfw_id, $alias_wkfw_id);
}

?>
