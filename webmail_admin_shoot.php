<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ウェブメール | トラブルシュート</title>
<?
ini_set("max_execution_time", 0);

require_once("about_comedix.php");
require_once('Cmx.php');
require_once("webmail_quota_functions.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
$check_auth = check_authority($session, 66, $fname);
if ($check_auth == "0") {
	js_login_exit();
}

$error = "";
$message = "";

// 初期表示時
if ($mode == "") {
	$mode = "reset";

// 「実行」押下時
} else {
	if ($mail_id == "") {
		$error = "メールIDを入力してください。";
	} else {
		$con = connect2db($fname);
		$error = run($con, $mail_id, $mode, $fname);
		pg_close($con);

		if ($error == "") {
			switch ($mode) {
			case "reset":
				$message = "認証情報の再設定コマンドを実行しました。";
				break;
			case "reconstruct":
				$message = "メールボックスの復旧コマンドを実行しました。";
				break;
			case "init":
				$message = "メールボックスの初期化コマンドを実行しました。";
				break;
			}
		}
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function run() {
	if (document.mainform.mode[2].checked && !confirm('この処理は元に戻せません。\nまた、完了まで時間がかかる場合があります。\n\n本当に実行してもよろしいですか？')) {
		return;
	}
	closeEmployeeList();
	document.mainform.submit();
}

var childwin = null;

function openEmployeeList() {
	var windowWidth = 720;
	var left = screen.availWidth - 10 - windowWidth;
	var url = '<? echo(CMX_BASE_URL); ?>/emplist/emplist.php?mode=webmail_shoot';
	childwin = window.open(url, 'emplistpopup', 'left=' + left + ',top=0,width=' + windowWidth + ',height=600,scrollbars=yes,resizable=yes');
	childwin.focus();
}

function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

function setMailId(mailId) {
	document.mainform.mail_id.value = mailId;
	closeEmployeeList();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}

p.info {color:blue; font-weight:bold; font-size:90%;}
p.error {color:red; font-weight:bold; font-size:90%;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="webmail_menu.php?session=<? echo($session); ?>"><img src="img/icon/b01.gif" width="32" height="32" border="0" alt="ウェブメール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="webmail_menu.php?session=<? echo($session); ?>"><b>ウェブメール</b></a> &gt; <a href="webmail_admin_menu.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="webmail_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="150" align="center" bgcolor="#bdd1e7"><a href="webmail_admin_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インターネット接続</font></a></td>
<td width="5"></td>
<td width="150" align="center" bgcolor="#bdd1e7"><a href="webmail_admin_display.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション設定</font></a></td>
<td width="5"></td>
<td width="150" align="center" bgcolor="#5279a5"><a href="webmail_admin_shoot.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>トラブルシュート</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<form name="mainform" action="webmail_admin_shoot.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メールID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="mail_id" type="text" value="<? eh($mail_id); ?>" size="30"> <input name="" type="button" value="職員名簿からコピー" onclick="openEmployeeList();">
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">試したい内容</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="mode" type="radio" value="reset"<? if ($mode == "reset") {echo(" checked");} ?>>認証情報の再設定（ウェブメールにログインできない場合）<br>
<input name="mode" type="radio" value="reconstruct"<? if ($mode == "reconstruct") {echo(" checked");} ?>>メールボックスの復旧（破損の可能性がある場合）<br>
<input name="mode" type="radio" value="init"<? if ($mode == "init") {echo(" checked");} ?>>メールボックスの初期化（メールや設定情報を一括削除したい場合）<b>※実行注意</b>
</font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="30">
<td align="right"><input type="button" value="実行" onclick="run();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? if ($message != "") { ?>
<p class="info"><? eh($message); ?></p>
<? } ?>
<? if ($error != "") { ?>
<p class="error"><? eh($error); ?></p>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function run($con, $mail_id, $mode, $fname) {
	if (preg_match("/[a-z]/", $mail_id) === 0) {
		return "メールIDが数字のみです。";
	}

	$sql = "select use_cyrus from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}
	$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");
	if ($use_cyrus != "t") {
		return "CoMedix以外のIMAPサーバを使用されています。";
	}

	$sql = "select emp_login_pass from login";
	$cond = q("where get_mail_login_id(emp_id) = '%s'", $mail_id);
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}
	if (pg_num_rows($sel) == 0) {
		return "指定のメールIDは存在しません。";
	}
	$pass = pg_fetch_result($sel, 0, "emp_login_pass");

	switch ($mode) {
	case "reset":
		exec("echo " . escapeshellarg($pass) . " | saslpasswd2 -p " . escapeshellarg($mail_id));
		break;
	case "reconstruct":
		$mailbox = "user.$mail_id";
		exec("sudo -u cyrus /usr/lib/cyrus-imapd/reconstruct -r " . escapeshellarg($mailbox), $output);
		if (count($output) === 0) {
			return "メールボックスが存在しません。サポート窓口までお知らせください。";
		}
		break;
	case "init":
		$dir = getcwd() . "/mboxadm";
		exec("/usr/bin/perl " . escapeshellarg("$dir/delmailbox") . " " . escapeshellarg($mail_id));
		exec("/usr/bin/perl " . escapeshellarg("$dir/addmailbox") . " " . escapeshellarg($mail_id) . " " . escapeshellarg($pass), $output, $exit_status);
		if ($exit_status !== 0) {
			return "予期せぬエラーが発生しました。サポート窓口までお知らせください。";
		}
		webmail_quota_change_to_default($mail_id);
		break;
	}

	return "";
}
