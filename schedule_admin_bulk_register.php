<?php
/*
 * CoMedixスケジュール一括登録機能
 * 画面 スケジュール ＞ 管理者画面 ＞ 一括登録
 */
// *** 前処理 ***
// * ファイル読み込み *
require_once("about_comedix.php");
require_once("show_schedule_place_list.ini");
require_once("schedule_place_next.ini");
require_once("get_values.ini");
require_once("facility_common.ini");
require_once("schedule_common.ini");
require_once("Cmx/Model/SystemConfig.php");

// * ファイル名取得 *
$fname = $PHP_SELF;

// * セッションチェック *
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// * 権限チェック *
$checkauth = check_authority($session,13,$fname);
if($checkauth == "0"){
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// * DBコネクション取得 *
$con = connect2db($fname);

// * スケジュールの初期画面を取得 *
$sql = "select schedule1_default from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
switch ($schedule1_default) {
    case "1":
        $default_url = "schedule_menu.php";
        break;
    case "2":
        $default_url = "schedule_week_menu.php";
        break;
    case "3":
        $default_url = "schedule_month_menu.php";
        break;
}

// *** 画面更新時の処理 ***
if ( $is_postback === 'true' ) {
    $result = schedule_bulk_insert($con, $target_emp_id, $_FILES["csvfile"]);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix スケジュール｜一括登録</title>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <script type="text/javascript" src="./js/fontsize.js"></script>
        <script type="text/javascript">
            function checkForm(form) {
                if (form.target_emp_id.value == '') {
                     alert('登録者を選択してください。');
                    return false;
                }
                if (form.csvfile.value == '') {
                    alert('ファイルを選択してください。');
                    return false;
                } else {
                    closeEmployeeList();
                    return true;
                }
            }
            var childwin = null;
            function openEmployeeList(item_id) {
                dx = screen.width;
                dy = screen.top;
                base = 0;
                wx = 720;
                wy = 600;
                var url = './emplist_popup.php';
	            url += '?session=<?=$session?>';
	            url += '&emp_id=<?=$emp_id?>';
                url += '&mode=14';
                url += '&item_id=' + item_id;
                childwin = window.open(url, 'emplistpopup', 'left=' + (dx - wx) + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');

                childwin.focus();
            }

            function closeEmployeeList() {
                if (childwin != null && !childwin.closed) {
                    childwin.close();
                }
                childwin = null;
            }

            function add_target_list(item_id, emp_id, emp_name) {
                var emp_ids = emp_id.split(", ");
                var emp_names = emp_name.split(", ");
                if (emp_ids.length > 1) {
                    alert('登録者は１名のみ指定してください。');
                    return false;
                }

                document.mainform.target_emp_id.value = emp_id;
                document.mainform.target_emp_name.value = emp_name;

            }
        </script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
        <form name="mainform" action="schedule_admin_bulk_register.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>スケジュール</b></a> &gt; <a href="schedule_place_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_comgroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">共通グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_setting_etc.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="schedule_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                            </tr>
                        </table>
                        <table width="600" border="0" cellspacing="0" cellpadding="1">
                            <tr>
                                <td height="22" align="right">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者：</font>
                                </td>
                                <td>
                                    <input name="target_emp_name" type="text" value="" size="30" readonly />
                                    <input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width:5.5em;" onclick="openEmployeeList('1');" />
                                </td>
                            </tr>
                            <tr>
                                <td height="22" align="right">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル：</font>
                                </td>
                                <td>
                                    <input name="csvfile" type="file" value="" size="50">
                                </td>
                            </tr>
                            <tr>
                                <td height="22" colspan="2" align="right">
                                    <input type="submit" name="sbmt" value="登録" />
                                </td>
                            </tr>
                        </table>

                        <input type="hidden" name="session" value="<?=$session?>">
                        <input type="hidden" name="target_emp_id" value="">
                        <input type="hidden" name="is_postback" value="true">
                        <img src="img/spacer.gif" width="1" height="5" alt=""><br>
						
						<?php if ( !empty($result) ) { ?>
                        <table width="600" border="0" cellspacing="0" cellpadding="1">
                            <tr>
                                <td colspan="2">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>実行結果</b></font>
                                </td>
                            </tr>
                            <tr>
                                <td width="30">
                                </td>
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <?php if ( $result['failure_flg'] === true ) { ?>
                                            <p style="margin:0;"><span style="color:#ff0000;">一括登録されませんでした。</span></p>
                                            <?php echo($result['error_message']);?>
                                        <?php }else {?>
                                            <p style="margin:0;">一括登録処理が正しく実行されました。</p>
                                        <?php }?>
                                    </font>
                                </td>
                            </tr>
                        </table>
                        <?php }?>
                        <table width="600" border="0" cellspacing="0" cellpadding="1">
                            <tr>
                                <td colspan="2">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font>
                                </td>
                            </tr>
                            <tr>
                                <td width="30">
                                </td>
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。</p>
                                        <ul style="margin:0;">
                                        <li style="margin-left:40px;">1.職員ID･･･必須</li>
                                        <li style="margin-left:40px;">2.日付･･･必須、YYYYMMDD形式</li>
                                        <li style="margin-left:40px;">3.開始時刻･･･必須、HHMM形式（0000 &#xFF5E; 2355）</li>
                                        <li style="margin-left:40px;">4.終了時刻･･･必須、HHMM形式（0005 &#xFF5E; 2400）</li>
                                        <li style="margin-left:40px;">5.タイトル･･･必須、全角100文字以内</li>
                                        <li style="margin-left:40px;">6.行先･･･全角50文字以内</li>
                                        <li style="margin-left:40px;">7.内容･･･改行は「\n」で表現</li>
                                        <li style="margin-left:40px;">8.マーカー･･･空もしくは0=なし、1=赤、2＝青、3＝黄、4＝緑、5=ピンク</li>
                                        <li style="margin-left:40px;">9.タイプ･･･空=公開、1=私用、2=公開、3＝「予定あり」と表示</li>
                                        <li style="margin-left:40px;">10.種別･･･全角7文字</li>
                                        </ul>
                                        <dl style="margin:0;">
                                        <dt>例：</dt>
                                        <dd style="margin-left:20px;font-family:monospace;">000000000000,20120322,1830,2000,○○会議,○○会議室,○○について\n机はコの字</dd>
                                        <dd style="margin-left:20px;font-family:monospace;">000000000000,20120322,0900,0930,朝礼</dd>
                                        </dl>
                                    </font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
<?php
/**
 * 内部関数
 * スケジュール一括登録処理
 * 
 * 引数
 * $con      : DBコネクション
 * $emp_id   : 登録者ID
 * $csv_file : CSVファイル
 * 
 * 呼出
 * schedule_admin_bulk_register.php(本ファイル)
 */
function schedule_bulk_insert($con, $emp_id, $csv_file) {
    
    // *** 内部変数宣言 ***
    $result           = array (         // 型：array   実行結果（戻り値）
        'failure_flg'       => false ,   // → 型：boolean  失敗フラグ
        'error_message'     => '' ,      // → 型：string   エラーメッセージ
    );
    $readLine         = 1;              // 型：int     CSVの読込んでいる行
    $emp_id_list      = array();        // 型：array   emp_personal_idキーにしたemp_idの配列
    $csv_check        = false;          // 型：boolean CSVが正しく作成されているかチェックするフラグ
    $bulk_insert_list = array();        // 型：array   一括登録用配列

    // *** ファイルが正常にアップロードされたかチェック ***
    // ** 正常系の処理 **
    if ( $csv_file['error'] == 0 && $csv_file['size'] > 0) {
        // *** 職員情報取得 ***
        // ** 職員ID取得 **
        $sql  = 'select emp_id, emp_personal_id from empmst'; 
        $cond = '';
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
        
            exit;
        }
    
        // ** 職員IDを配列に格納 **
        while ($row = pg_fetch_assoc($sel)) {
            // * emp_personal_idをキーにしたemp_idの配列を作成 *
            $emp_id_list[$row['emp_personal_id']] = $row['emp_id'];
        }

        // *** CSVファイルの形式チェック ***
        // ** CSVファイルの読込 **
        if (($handle = fopen($csv_file['tmp_name'], "r")) !== false) {
            
            // ** 一行ずつ読み込み **
            while (($data = fgetcsv_reg($handle,4096,",")) !== false) {
                // * 値の妥当性チェック *
                // 職員IDの存在チェック
                if(empty($emp_id_list[$data[0]])){
                    // エラー出力 ＆ ログに格納
                    $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : 職員IDが正しくありません</span><br />";
                    $result['failure_flg'] = true ;
                    $csv_check = true;
                }

                // 日付妥当性チェック
                if ( !checkdate(substr($data[1], 4, 2), substr($data[1], 6, 2), substr($data[1], 0, 4)) ) {
                        $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : 日付の入力値が正しくありません</span><br />";
                        $result['failure_flg'] = true ;
                        $csv_check = true;
                }

                $matches = array();  // 時刻妥当性チェック用変数
                preg_match("/(\d\d)(\d\d)/", $data[2], $matches);
                if ( !( (int)$matches[1] >= 0
                            && (int)$matches[1] <= 24
                            && (int)$matches[2] >= 0
                            && (int)$matches[2] <= 55
                            && (int)$matches[2] % 5 === 0
                        )
                            || $matches[1] == ''
                            || $matches[2] == ''
                ) {
                        // エラー出力 ＆ ログに格納
                        $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : 開始時刻の入力値が正しくありません</span><br />";
                        $result['failure_flg'] = true ;
                        $csv_check = true;
                } 

                $matches = array();  // 時刻妥当性チェック用変数
                preg_match("/(\d\d)(\d\d)/", $data[3], $matches);
                if ( !( (int)$matches[1] >= 0
                            && (int)$matches[1] <= 24
                            && (int)$matches[2] >= 0
                            && (int)$matches[2] <= 55
                            && (int)$matches[2] % 5 === 0
                        )
                            || $matches[1] == ''
                            || $matches[2] == ''
                    ) {
                        // エラー出力 ＆ ログに格納
                        $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : 終了時刻の入力値が正しくありません</span><br />";
                        $result['failure_flg'] = true ;
                        $csv_check = true;
                }
				
                // タイトルの存在チェック
                if(empty($data[4])){
                    // エラー出力 ＆ ログに格納
                    $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : タイトルを入力してください</span><br />";
                    $result['failure_flg'] = true ;
                    $csv_check = true;
                }
                
                // タイトルの入力妥当性チェック
                else {
                    // タイトルはMAX200バイト
                    if (strlen($data[4]) > 200) {
                        // エラー出力 ＆ ログに格納
                        $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : タイトルが長すぎます。全角100文字以内にまとめてください</span><br />";
                        $result['failure_flg'] = true ;
                        $csv_check = true;
                    }
                }
                
                // 行先の入力妥当性チェック
                if (strlen($data[5]) > 100) {
                    // エラー出力 ＆ ログに格納
                    $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : 行先が長すぎます。全角50文字以内にまとめてください</span><br />";
                    $result['failure_flg'] = true ;
                    $csv_check = true;
                }
                
                
                // マーカーの妥当性チェック
                $data[7] = $data[7] == ''  ? 0 : $data[7];
                $data[7] = (int)$data[7];
                if ( $data[7] > 5 || 0 > $data[7] ) {
                    // エラー出力 ＆ ログに格納
                    $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : マーカーは0〜5の整数で指定してください。</span><br />";
                    $result['failure_flg'] = true ;
                    $csv_check = true;
                }
                
                // タイプの妥当性チェック
                $data[8] = $data[8] == ''  ? 2 : $data[8];
                $data[8] = (int)$data[8];
                if ( $data[8] > 3 || 1 > $data[8] ) {
                    // エラー出力 ＆ ログに格納
                    $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : タイプは1〜3の整数で指定してください。</span><br />";
                    $result['failure_flg'] = true ;
                    $csv_check = true;
                }
                // 種別の妥当性チェック
                if (strlen($data[9]) > 14) {
                    // エラー出力 ＆ ログに格納
                    $result['error_message'] .= "<span style='color:#ff0000;'>CSV file error line={$readLine} : 種別が長すぎます。全角7文字以内にまとめてください</span><br />";
                    $result['failure_flg'] = true ;
                    $csv_check = true;
                }
                
                
                // 一括登録用配列作成
                $bulk_insert_list[]  = array (
                    'emp_id'                 => $emp_id_list[$data[0]],
                    'schd_title'             => mb_convert_encoding($data[4], "EUC-JP", "auto"),
                    'schd_date'              => mb_convert_encoding($data[1], "EUC-JP", "auto"),
                    'schd_start_time'        => mb_convert_encoding( substr($data[2], 0, 2) . ':' . substr($data[2], 2, 2), "EUC-JP", "auto"),
                    'schd_end_time'          => mb_convert_encoding( substr($data[3], 0, 2) . ':' . substr($data[3], 2, 2), "EUC-JP", "auto"),
                    'schd_start_time_v'      => mb_convert_encoding($data[2], "EUC-JP", "auto"),
                    'schd_end_time_v'        => mb_convert_encoding($data[3], "EUC-JP", "auto"),
                    'schd_plc'               => mb_convert_encoding($data[5], "EUC-JP", "auto"),
                    'schd_detail'            => mb_convert_encoding($data[6], "EUC-JP", "auto"),
                    'schd_marker'            => mb_convert_encoding($data[7], "EUC-JP", "auto"),
                    'schd_schd_imprt'        => mb_convert_encoding($data[8], "EUC-JP", "auto"),
                    'schd_type_id'           => mb_convert_encoding($data[9], "EUC-JP", "auto"),
                );
                
                // 行をインクリメント
                $readLine++;
            }
        }
        // ** CSVの形式が正しければ一括登録処理を行う ***
        if ( $csv_check === false ) {
            $conf = new Cmx_SystemConfig();
            $status_f = false;
            $aprv_ctrl = $conf->get("schedule.aprv_ctrl");
            if($aprv_ctrl == "2")
            {
            // 承認依頼しない（管理画面のその他タグで指定）
                $status_f = true;
            // $tmp_status = "1";
            }
            // ** 登録処理 **
            // * トランザクションの開始 *
            pg_query($con, 'begin transaction');
            // * スケジュールデータ分ループ処理 *
            foreach ($bulk_insert_list as $schd_data) {

                // スケジュールIDを採番
                $schd_id = get_schd_id($con, $fname, 'f');

                // スケジュール情報を登録
                if($status_f === true){
                    $tmp_status = "1";
                }
                else{
                    $tmp_status = ($schd_data['emp_id'] == $emp_id) ? "1" : "2";
                }

                // 登録時刻
                $reg_time = get_millitime();

                // 24:00対応
                $schd_dur = $schd_data['schd_end_time'];
                if ($schd_dur == "24:00") {
                    $schd_dur = "23:59";
                }
                
                // 種別ID取得
                $schd_type_id = get_schd_place_id($con, $schd_data['schd_type_id']);

                $sql = "insert into schdmst (schd_id, emp_id, schd_title, schd_plc, schd_type, schd_imprt, schd_start_date, schd_start_time, schd_day_flg, schd_dur, schd_detail, schd_reg_id, schd_status, marker, repeat_flg, reg_time, schd_start_time_v, schd_dur_v, pub_limit, alarm, alarm_confirm) values (";
                $detail = implode("\n", explode("\\n", $schd_data['schd_detail']));
                $content = array(
                    $schd_id,                           // schd_id              : スケジュールID
                    $schd_data['emp_id'],               // emp_id               : 職員ID
                    q($schd_data['schd_title']),        // schd_title           : タイトル
                    $schd_data['schd_plc'],             // schd_plc             : 行先
                    $schd_type_id,                      // schd_type            : 種別（なし、外来診療・・・）
                    $schd_data['schd_schd_imprt'],      // schd_imprt           : タイプ（私用、公開、予定あり）
                    $schd_data['schd_date'],            // schd_start_date      : 登録日時（開始日時）
                    $schd_data['schd_start_time'],      // schd_start_time      : 開始時刻
                    "f",                                // schd_day_flg         : ？
                    $schd_dur,                          // schd_dur             : 終了時刻
                    q($detail),                         // schd_detail          : 詳細
                    $emp_id,                            // schd_reg_id          : 登録者ID
                    $tmp_status,                        // schd_status          : 承認ステータス
                    $schd_data['schd_marker'],          // marker               : ラインマーカー
                    'f',                                // repeat_flg           : 修正フラグ
                    $reg_time,                          // reg_time             : 登録時刻
                    $schd_data['schd_start_time_v'],    // schd_start_time_v    : 開始時刻（文字列）
                    $schd_data['schd_end_time_v'],      // schd_dur_v           : 終了時刻（文字列）
                    '',                                 // pub_limit            : 公開範囲
                    '0',                                // alarm                : アラーム
                    '0'                                 // alarm_confirm        : アラームチェック
                );

                $ins = insert_into_table($con, $sql, $content, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }            
            }

            // トランザクションをコミット
            pg_query($con, 'commit');
        }
    }
    // *** ファイルアップロードエラー処理 ***
    else {
        // ** エラーメッセージ出力 **
        $result['error_message'] = "<span style='color:#ff0000;'>ファイルが正常にアップロードされませんでした</span><br />";
    }

    // *** 戻り値を返す ***
    return $result;
}
/**
 * 内部関数
 * 種別のIDを取得
 * 
 * 呼出
 * schedule_admin_bulk_register.php(本ファイル)
 *   function schedule_bulk_insert
 */
function get_schd_place_id($con, $type_name) {
    $schd_type_id = '1';
    
    $sql = "select type_id from schdtype where type_name='{$type_name}'";
    $sel = select_from_table($con,$sql,'',$fname);
    
    if ( $sel == 0 ) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    
    while ( $row = pg_fetch_assoc($sel) ) {
        $schd_type_id = $row['type_id'];
    }
    
    return $schd_type_id;
}

/**
 * 内部関数
 * CSV一行読込処理 ※ fgetcsvのバグ回避
 * http://yossy.iimp.jp/wp/?p=56
 * 
 * 呼出
 * schedule_admin_bulk_register.php(本ファイル)
 *   function schedule_bulk_insert
 */
function fgetcsv_reg(&$handle, $length = null, $d = ',', $e = '"') {
    $d = preg_quote($d);
    $e = preg_quote($e);
    $_line = "";

    $eof = false;

    while (($eof != true) && (!feof($handle))) {
        $_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
        $itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
        if ($itemcnt % 2 == 0) $eof = true;
    }

    $_csv_line = preg_replace('/(?:\r\n|[\r\n])?$/', $d, trim($_line));
    $_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';

    preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);

    $_csv_data = $_csv_matches[1];

    for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
        $_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
        $_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
    }
    
    return empty($_line) ? false : $_csv_data;
}