<?
// 患者データ抽出CSV出力
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

if (!checkdate($_GET["from_month"], $_GET["from_day"], $_GET["from_year"])) {
	echo("<script type=\"text/javascript\">alert('入院期間（自）が不正です。');</script>");
	exit;
}
if (!checkdate($_GET["to_month"], $_GET["to_day"], $_GET["to_year"])) {
	echo("<script type=\"text/javascript\">alert('入院期間（至）が不正です。');</script>");
	exit;
}
$from_date = "{$_GET["from_year"]}{$_GET["from_month"]}{$_GET["from_day"]}";
$to_date = "{$_GET["to_year"]}{$_GET["to_month"]}{$_GET["to_day"]}";
if ($from_date > $to_date) {
	echo("<script type=\"text/javascript\">alert('期間が不正です。');</script>");
	exit;
}

$header_line = construct_header_line();
$body_lines = construct_body_lines(count(split(",", $header_line)), $from_date, $to_date, $fname);
$csv = $header_line . "\r\n" . $body_lines;
$csv = mb_convert_encoding($csv, "Shift_JIS", "EUC-JP");
$file_name = "patient_list.csv";
header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=$file_name");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
header("Content-Length: " . strlen($csv));
echo($csv);

function construct_header_line() {
	$labels = array(
		"c1" => "患者氏名",
		"c2" => "生年月日",
		"c45" => "かな氏名",
		"c46" => "年齢",
		"c3" => "性別",
		"c4" => "入院日",
		"c66" => "退院予定日",
		"c67" => "退院予定時間",
		"c5" => "退院日",
		"c47" => "病棟",
		"c53" => "病室",
		"c54" => "ベッドNo",
		"c48" => "主治医",
		"c6" => "在院日数",
		"c7" => "診断名（主病名）",
		"c49" => "発症年月日",
		"c50" => "発症から入院日までの日数",
		"c51" => "担当者",
		"c63" => "入院目的",
		"c64" => "入院目的その他",
		"c8" => "保険",
		"c9" => "医療保険負担割合",
		"c10" => "減額認定有無",
		"c11" => "マル障有無",
		"c65" => "個人情報に関する要望",
		"c12" => "看護度分類",
		"c13" => "転帰",
		"c14" => "退院理由",
		"c15" => "紹介元医療機関",
		"c16" => "診療科",
		"c17" => "前医",
		"c18" => "ホットライン",
		"c19" => "退院先区分",
		"c20" => "退院先施設名",
		"c23" => "退院先市区町村",
		"c21" => "かかりつけ医療機関",
		"c22" => "かかりつけ医主治医",
		"c24" => "回復期リハビリ算定期限分類",
		"c25" => "回復期リハビリ算定期限日",
		"c26" => "リハビリ算定期限分類",
		"c27" => "リハビリ算定期限日",
		"c28" => "介護保険被保険者番号",
		"c29" => "要介護度",
		"c30" => "要介護認定年月日",
		"c31" => "要介護度の有効期間",
		"c32" => "身体障害者手帳等級",
		"c33" => "障害種別",
		"c34" => "特定疾患の疾患名",
		"c35" => "特定疾患の有効期間",
		"c36" => "救護区分・移動手段",
		"c37" => "酸素必要",
		"c38" => "人工呼吸器",
		"c39" => "血液型",
		"c40" => "気管切開",
		"c41" => "治験患者",
		"c42" => "透析患者",
		"c43" => "医療区分",
		"c44" => "ADL区分",
		"c55" => "郵便番号",
		"c56" => "都道府県",
		"c57" => "住所1",
		"c58" => "住所2",
		"c59" => "電話番号",
		"c60" => "携帯端末",
		"c61" => "E-Mail",
		"c62" => "携帯Mail"
	);

	$header_cols = array("患者ID");
	foreach ($labels as $label_key => $label_val) {
		if (array_key_exists($label_key, $_GET)) {
			$header_cols[] = $label_val;
		}
	}

	if (isset($_GET["c52"])) {
		$header_cols[] = "転床日時";
		$header_cols[] = "転床元";
		$header_cols[] = "転床先";
	}

	return join(",", $header_cols);
}

function construct_body_lines($col_count, $from_date, $to_date, $fname) {
	$lines = "";

	$con = connect2db($fname);

	// 医療保険負担割合
	if (isset($_GET["c9"])) {
		$insu_rate_master = get_master($con, "A301", $fname);
	}

	// 入院目的
	if (isset($_GET["c63"])) {
		$inpt_purpose_master = get_master($con, "A306", $fname);
	}

	// 退院理由
	if (isset($_GET["c14"])) {
		$out_reason_master = get_master($con, "A308", $fname);
	}

	// 紹介元医療機関／退院先施設名／かかりつけ医療機関
	if (isset($_GET["c15"]) || isset($_GET["c20"]) || isset($_GET["c21"])) {
		$inst_master = get_inst_master($con, $fname);
	}

	// 診療科（紹介先）／前医／かかりつけ医主治医
	if (isset($_GET["c16"]) || isset($_GET["c17"]) || isset($_GET["c22"])) {
		$inst_item_master = get_inst_item_master($con, $fname);
	}

	// 退院先区分
	if (isset($_GET["c19"])) {
		$out_pos_master = get_master($con, "A307", $fname);
	}

	// 要介護度
	if (isset($_GET["c29"])) {
		$care_grade_master = get_master($con, "A302", $fname);
	}

	// 身体障害者手帳等級
	if (isset($_GET["c32"])) {
		$disabled_grade_master = get_master($con, "A303", $fname);
	}

	// 障害種別
	if (isset($_GET["c33"])) {
		$disabled_type_master = get_master($con, "A304", $fname);
	}

	$sel = get_patients($con, $from_date, $to_date, $fname);
	while ($row = pg_fetch_array($sel)) {
		$lines .= $row["ptif_id"];

		// 患者氏名
		if (isset($_GET["c1"])) {
			$lines .= "," . $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"];
		}

		// 生年月日
		if (isset($_GET["c2"])) {
			$lines .= "," . format_date($row["ptif_birth"]);
		}

		// かな氏名
		if (isset($_GET["c45"])) {
			$lines .= "," . $row["ptif_lt_kana_nm"] . " " . $row["ptif_ft_kana_nm"];
		}

		// 年齢
		if (isset($_GET["c46"])) {
			$lines .= "," . format_age($row["ptif_birth"]);
		}

		// 性別
		if (isset($_GET["c3"])) {
			$lines .= "," . format_sex($row["ptif_sex"]);
		}

		// 入院日
		if (isset($_GET["c4"])) {
			$lines .= "," . format_date($row["inpt_in_dt"]);
		}

		// 退院予定日
		if (isset($_GET["c66"])) {
			$lines .= "," . format_date($row["inpt_out_res_dt"]);
		}

		// 退院予定時間
		if (isset($_GET["c67"])) {
			$lines .= "," . format_time($row["inpt_out_res_tm"]);
		}

		// 退院日
		if (isset($_GET["c5"])) {
			$lines .= "," . format_date($row["inpt_out_dt"]);
		}

		// 病棟
		if (isset($_GET["c47"])) {
			$lines .= "," . $row["ward_name"];
		}

		// 病棟
		if (isset($_GET["c53"])) {
			$lines .= "," . $row["ptrm_name"];
		}

		// 病棟
		if (isset($_GET["c54"])) {
			$lines .= "," . $row["inpt_bed_no"];
		}

		// 主治医
		if (isset($_GET["c48"])) {
			$lines .= "," . $row["dr_nm"];
		}

		// 在院日数
		if (isset($_GET["c6"])) {
			$lines .= "," . format_in_days($row["inpt_in_dt"], $row["inpt_out_dt"]);
		}

		// 診断名（主病名）
		if (isset($_GET["c7"])) {
			$lines .= "," . $row["inpt_disease"];
		}

		// 発症年月日
		if (isset($_GET["c49"])) {
			$lines .= "," . format_term2($row["inpt_patho_from"], $row["inpt_patho_to"]);
		}

		// 発症から入院日までの日数
		if (isset($_GET["c50"])) {
			$lines .= "," . format_diff_days($row["inpt_patho_from"], $row["inpt_in_dt"]);
		}

		// 担当者
		if (isset($_GET["c51"])) {
			$lines .= "," . format_operators($con, $row["ptif_id"], $row["inpt_in_dt"], $row["inpt_in_tm"], $row["inpt_out_dt"], $fname);
		}

		// 入院目的
		if (isset($_GET["c63"])) {
			$lines .= "," . format_inpt_purpose($inpt_purpose_master[$row["inpt_purpose_rireki"]][$row["inpt_purpose_cd"]], ($row["inpt_short_stay"] == "t"));
		}

		// 入院目的その他
		if (isset($_GET["c64"])) {
			$lines .= "," . $row["inpt_purpose_content"];
		}

		// 保険
		if (isset($_GET["c8"])) {
			$lines .= "," . format_insurance($row["inpt_insurance"]);
		}

		// 医療保険負担割合
		if (isset($_GET["c9"])) {
			$lines .= "," . $insu_rate_master[$row["inpt_insu_rate_rireki"]][$row["inpt_insu_rate_cd"]];
		}

		// 減額認定有無
		if (isset($_GET["c10"])) {
			$lines .= "," . format_existence($row["inpt_reduced"]);
		}

		// マル障有無
		if (isset($_GET["c11"])) {
			$lines .= "," . format_existence($row["inpt_impaired"]);
		}

		// 個人情報に関する要望
		if (isset($_GET["c65"])) {
			$lines .= "," . format_privacy($row["inpt_privacy_flg"], $row["inpt_privacy_text"]);
		}

		// 看護度分類
		if (isset($_GET["c12"])) {
			$lines .= "," . format_nurse_degree($row["inpt_nrs_obsv"], $row["inpt_free"]);
		}

		// 転帰
		if (isset($_GET["c13"])) {
			$lines .= "," . format_result($row["inpt_result"]);
		}

		// 退院理由
		if (isset($_GET["c14"])) {
			$lines .= "," . $out_reason_master[$row["inpt_out_rsn_rireki"]][$row["inpt_out_rsn_cd"]];
		}

		// 紹介元医療機関
		if (isset($_GET["c15"])) {
			$lines .= "," . $inst_master[$row["inpt_intro_inst_cd"]];
		}

		// 診療科（紹介先）
		if (isset($_GET["c16"])) {
			$lines .= "," . $inst_item_master[$row["inpt_intro_inst_cd"]][$row["inpt_intro_sect_rireki"]][$row["inpt_intro_sect_cd"]]["sect_name"];
		}

		// 前医
		if (isset($_GET["c17"])) {
			$lines .= "," . $inst_item_master[$row["inpt_intro_inst_cd"]][$row["inpt_intro_sect_rireki"]][$row["inpt_intro_sect_cd"]]["doctor_name{$row["inpt_intro_doctor_no"]}"];
		}

		// ホットライン
		if (isset($_GET["c18"])) {
			$lines .= "," . format_checked($row["inpt_hotline"]);
		}

		// 退院先区分
		if (isset($_GET["c19"])) {
			$lines .= "," . $out_pos_master[$row["inpt_out_pos_rireki"]][$row["inpt_out_pos_cd"]];
		}

		// 紹介元医療機関
		if (isset($_GET["c20"])) {
			$lines .= "," . $inst_master[$row["inpt_out_inst_cd"]];
		}

		// 退院先市区町村
		if (isset($_GET["c23"])) {
			$lines .= "," . $row["inpt_out_city"];
		}

		// かかりつけ医療機関
		if (isset($_GET["c21"])) {
			$lines .= "," . $inst_master[$row["inpt_pr_inst_cd"]];
		}

		// かかりつけ医主治医
		if (isset($_GET["c22"])) {
			$lines .= "," . $inst_item_master[$row["inpt_pr_inst_cd"]][$row["inpt_pr_sect_rireki"]][$row["inpt_pr_sect_cd"]]["doctor_name{$row["inpt_pr_doctor_no"]}"];
		}

		// 回復期リハビリ算定期限分類
		if (isset($_GET["c24"])) {
			$lines .= "," . format_decubation_class($row["inpt_dcb_cls"]);
		}

		// 回復期リハビリ算定期限日
		if (isset($_GET["c25"])) {
			$lines .= "," . format_date($row["inpt_dcb_exp"]);
		}

		// リハビリ算定期限分類
		if (isset($_GET["c26"])) {
			$lines .= "," . format_rehabilitation_class($row["inpt_rhb_cls"]);
		}

		// リハビリ算定期限日
		if (isset($_GET["c27"])) {
			$lines .= "," . format_date($row["inpt_rhb_exp"]);
		}

		// 介護保険被保険者番号
		if (isset($_GET["c28"])) {
			$lines .= "," . $row["inpt_care_no"];
		}

		// 要介護度
		if (isset($_GET["c29"])) {
			$lines .= "," . mb_ereg_replace("要介護", "", $care_grade_master[$row["inpt_care_grd_rireki"]][$row["inpt_care_grd_cd"]]);
		}

		// 要介護認定年月日
		if (isset($_GET["c30"])) {
			$lines .= "," . format_date($row["inpt_care_apv"]);
		}

		// 要介護度の有効期間
		if (isset($_GET["c31"])) {
			$lines .= "," . format_term($row["inpt_care_from"], $row["inpt_care_to"]);
		}

		// 身体障害者手帳等級
		if (isset($_GET["c32"])) {
			$lines .= "," . mb_ereg_replace("級", "", $disabled_grade_master[$row["inpt_dis_grd_rireki"]][$row["inpt_dis_grd_cd"]]);
		}

		// 障害種別
		if (isset($_GET["c33"])) {
			$lines .= "," . $disabled_type_master[$row["inpt_dis_type_rireki"]][$row["inpt_dis_type_cd"]];
		}

		// 特定疾患の疾患名
		if (isset($_GET["c34"])) {
			$lines .= "," . $row["inpt_spec_name"];
		}

		// 特定疾患の有効期間
		if (isset($_GET["c35"])) {
			$lines .= "," . format_term($row["inpt_spec_from"], $row["inpt_spec_to"]);
		}

		// 救護区分・移動手段
		if (isset($_GET["c36"])) {
			$lines .= "," . format_transportation($row["ptsubif_move_flg"], $row["ptsubif_move1"], $row["ptsubif_move2"], $row["ptsubif_move3"], $row["ptsubif_move4"], $row["ptsubif_move5"]);
		}

		// 酸素必要
		if (isset($_GET["c37"])) {
			$lines .= "," . format_checked($row["ptsubif_move6"]);
		}

		// 人工呼吸器
		if (isset($_GET["c38"])) {
			$lines .= "," . format_checked($row["ptsubif_ventilator"]);
		}

		// 血液型
		if (isset($_GET["c39"])) {
			$lines .= "," . format_blood_type($row["ptsubif_blood_abo"], $row["ptsubif_blood_rh"]);
		}

		// 気管切開
		if (isset($_GET["c40"])) {
			$lines .= "," . format_checked($row["ptsubif_bronchotomy"]);
		}

		// 治験患者
		if (isset($_GET["c41"])) {
			$lines .= "," . format_checked($row["ptsubif_experiment_flg"]);
		}

		// 透析患者
		if (isset($_GET["c42"])) {
			$lines .= "," . format_checked($row["ptsubif_dialysis"]);
		}

		// 医療区分
		if (isset($_GET["c43"])) {
			$lines .= "," . $row["ptsubif_medical_div"];
		}

		// ADL区分
		if (isset($_GET["c44"])) {
			$lines .= "," . $row["ptsubif_adl_div"];
		}

		// 郵便番号
		if (isset($_GET["c55"])) {
			$lines .= "," . format_zip($row["ptif_zip1"], $row["ptif_zip2"]);
		}

		// 都道府県
		if (isset($_GET["c56"])) {
			$lines .= "," . get_province($row["ptif_prv"]);
		}

		// 住所1
		if (isset($_GET["c57"])) {
			$lines .= "," . $row["ptif_addr1"];
		}

		// 住所2
		if (isset($_GET["c58"])) {
			$lines .= "," . $row["ptif_addr2"];
		}

		// 電話番号
		if (isset($_GET["c59"])) {
			$lines .= "," . format_tel($row["ptif_tel1"], $row["ptif_tel2"], $row["ptif_tel3"]);
		}

		// 携帯端末
		if (isset($_GET["c60"])) {
			$lines .= "," . format_tel($row["ptif_mobile1"], $row["ptif_mobile2"], $row["ptif_mobile3"]);
		}

		// E-Mail
		if (isset($_GET["c61"])) {
			$lines .= "," . $row["ptif_email"];
		}

		// 携帯Mail
		if (isset($_GET["c62"])) {
			$lines .= "," . $row["ptif_m_email"];
		}

		// 転床履歴が未選択の場合
		if (!isset($_GET["c52"])) {
			$lines .= "\r\n";
			continue;
		}

		// 転床履歴
		$sql = "select m.move_dt, m.move_tm, fw.ward_name as from_ward_name, fr.ptrm_name as from_ptrm_name, m.from_bed_no, tw.ward_name as to_ward_name, tr.ptrm_name as to_ptrm_name, m.to_bed_no from inptmove m inner join wdmst fw on fw.bldg_cd = m.from_bldg_cd and fw.ward_cd = m.from_ward_cd inner join ptrmmst fr on fr.bldg_cd = m.from_bldg_cd and fr.ward_cd = m.from_ward_cd and fr.ptrm_room_no = m.from_ptrm_room_no inner join wdmst tw on tw.bldg_cd = m.to_bldg_cd and tw.ward_cd = m.to_ward_cd inner join ptrmmst tr on tr.bldg_cd = m.to_bldg_cd and tr.ward_cd = m.to_ward_cd and tr.ptrm_room_no = m.to_ptrm_room_no";
		$cond = "where m.ptif_id = '{$row["ptif_id"]}' and (m.move_dt > '{$row["inpt_in_dt"]}' or (m.move_dt = '{$row["inpt_in_dt"]}' and m.move_tm >= '{$row["inpt_in_tm"]}'))";
		if ($row["inpt_out_dt"] != "") {
			$cond .= " and (m.move_dt < '{$row["inpt_out_dt"]}' or (m.move_dt = '{$row["inpt_out_dt"]}' and m.move_tm < '{$row["inpt_out_tm"]}'))";
		}
		$cond .= " and m.move_cfm_flg and (not m.move_del_flg) order by m.ptif_id, m.move_dt, m.move_tm";
		$sel_moving = select_from_table($con, $sql, $cond, $fname);
		if ($sel_moving == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel_moving) == 0) {
			$lines .= ",,,\r\n";
			continue;
		}
		$first_line = true;
		$blanks = $col_count - 4;
		if (isset($_GET["c1"])) $blanks--;
		while ($row_moving = pg_fetch_array($sel_moving)) {
			if ($first_line) {
				$first_line = false;
			} else {
				$lines .= $row["ptif_id"];
				if (isset($_GET["c1"])) {
					$lines .= "," . $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"];
				}
				$lines .= str_repeat(",", $blanks);
			}

			$lines .= "," . format_date($row_moving["move_dt"]) . " " . format_time($row_moving["move_tm"]);
			$lines .= "," . $row_moving["from_ward_name"] . " " . $row_moving["from_ptrm_name"] . " No." . $row_moving["from_bed_no"];
			$lines .= "," . $row_moving["to_ward_name"] . " " . $row_moving["to_ptrm_name"] . " No." . $row_moving["to_bed_no"];
			$lines .= "\r\n";
		}
	}

	pg_close($con);

	return $lines;
}

function get_master($con, $mst_cd, $fname) {
	$sql = "select rireki_index, item_cd, item_name from tmplitemrireki";
	$cond = "where mst_cd = '$mst_cd' and disp_flg";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["rireki_index"]][$row["item_cd"]] = $row["item_name"];
	}
	return $master;
}

function get_inst_master($con, $fname) {
	$sql = "select mst_cd, mst_name from institemmst";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["mst_cd"]] = $row["mst_name"];
	}
	return $master;
}

function get_inst_item_master($con, $fname) {
	$sql = "select mst_cd, rireki_index, item_cd, item_name, doctor_name1, doctor_name2, doctor_name3, doctor_name4, doctor_name5, doctor_name6, doctor_name7, doctor_name8, doctor_name9, doctor_name10 from institemrireki";
	$cond = "where disp_flg";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["mst_cd"]][$row["rireki_index"]][$row["item_cd"]] = array(
			"sect_name"     => $row["item_name"],
			"doctor_name1"  => $row["doctor_name1"],
			"doctor_name2"  => $row["doctor_name2"],
			"doctor_name3"  => $row["doctor_name3"],
			"doctor_name4"  => $row["doctor_name4"],
			"doctor_name5"  => $row["doctor_name5"],
			"doctor_name6"  => $row["doctor_name6"],
			"doctor_name7"  => $row["doctor_name7"],
			"doctor_name8"  => $row["doctor_name8"],
			"doctor_name9"  => $row["doctor_name9"],
			"doctor_name10" => $row["doctor_name10"]
		);
	}
	return $master;
}

function get_patients($con, $from_date, $to_date, $fname) {
	$today = date("Ymd");

	$columns = array(
		"i.inpt_keywd",
		"i.ptif_id",
		"p.ptif_birth",
		"i.inpt_lt_kj_nm",
		"i.inpt_ft_kj_nm",
		"p.ptif_lt_kana_nm",
		"p.ptif_ft_kana_nm",
		"p.ptif_sex",
		"i.inpt_in_dt",
		"i.inpt_in_tm",
		"%inpt_out_res_dt%",
		"%inpt_out_res_tm%",
		"%inpt_out_dt%",
		"%inpt_out_tm%",
		"w.ward_name",
		"r.ptrm_name",
		"i.inpt_bed_no",
		"d.dr_nm",
		"i.inpt_disease",
		"i.inpt_patho_from",
		"i.inpt_patho_to",
		"i.inpt_purpose_rireki",
		"i.inpt_purpose_cd",
		"i.inpt_short_stay",
		"i.inpt_purpose_content",
		"i.inpt_insurance",
		"i.inpt_insu_rate_rireki",
		"i.inpt_insu_rate_cd",
		"i.inpt_reduced",
		"i.inpt_impaired",
		"i.inpt_privacy_flg",
		"i.inpt_privacy_text",
		"i.inpt_nrs_obsv",
		"i.inpt_free",
		"i.inpt_result",
		"i.inpt_out_rsn_rireki",
		"i.inpt_out_rsn_cd",
		"i.inpt_intro_inst_cd",
		"i.inpt_intro_sect_rireki",
		"i.inpt_intro_sect_cd",
		"i.inpt_intro_doctor_no",
		"i.inpt_hotline",
		"i.inpt_out_pos_rireki",
		"i.inpt_out_pos_cd",
		"i.inpt_out_inst_cd",
		"i.inpt_out_city",
		"i.inpt_pr_inst_cd",
		"i.inpt_pr_sect_rireki",
		"i.inpt_pr_sect_cd",
		"i.inpt_pr_doctor_no",
		"i.inpt_dcb_cls",
		"i.inpt_dcb_exp",
		"i.inpt_rhb_cls",
		"i.inpt_rhb_exp",
		"i.inpt_care_no",
		"i.inpt_care_grd_rireki",
		"i.inpt_care_grd_cd",
		"i.inpt_care_apv",
		"i.inpt_care_from",
		"i.inpt_care_to",
		"i.inpt_dis_grd_rireki",
		"i.inpt_dis_grd_cd",
		"i.inpt_dis_type_rireki",
		"i.inpt_dis_type_cd",
		"i.inpt_spec_name",
		"i.inpt_spec_from",
		"i.inpt_spec_to",
		"s.ptsubif_move_flg",
		"s.ptsubif_move1",
		"s.ptsubif_move2",
		"s.ptsubif_move3",
		"s.ptsubif_move4",
		"s.ptsubif_move5",
		"s.ptsubif_move6",
		"s.ptsubif_ventilator",
		"s.ptsubif_blood_abo",
		"s.ptsubif_blood_rh",
		"s.ptsubif_bronchotomy",
		"s.ptsubif_experiment_flg",
		"s.ptsubif_dialysis",
		"s.ptsubif_medical_div",
		"s.ptsubif_adl_div",
		"p.ptif_zip1",
		"p.ptif_zip2",
		"p.ptif_prv",
		"p.ptif_addr1",
		"p.ptif_addr2",
		"p.ptif_tel1",
		"p.ptif_tel2",
		"p.ptif_tel3",
		"p.ptif_mobile1",
		"p.ptif_mobile2",
		"p.ptif_mobile3",
		"p.ptif_email",
		"p.ptif_m_email"
	);
	$column_list = join(",", $columns);
	unset($columns);

	$in_patient_column_list = str_replace("%inpt_out_res_dt%", "i.inpt_out_res_dt", $column_list);
	$in_patient_column_list = str_replace("%inpt_out_res_tm%", "i.inpt_out_res_tm", $in_patient_column_list);
	$in_patient_column_list  = str_replace("%inpt_out_dt%", "varchar(8) '' as inpt_out_dt", $in_patient_column_list);
	$in_patient_column_list  = str_replace("%inpt_out_tm%", "varchar(4) '' as inpt_out_tm", $in_patient_column_list);

	$out_patient_column_list  = str_replace("%inpt_out_res_dt%", "varchar(8) '' as inpt_out_res_dt", $column_list);
	$out_patient_column_list  = str_replace("%inpt_out_res_tm%", "varchar(4) '' as inpt_out_res_tm", $out_patient_column_list);
	$out_patient_column_list = str_replace("%inpt_out_dt%", "i.inpt_out_dt", $out_patient_column_list);
	$out_patient_column_list = str_replace("%inpt_out_tm%", "i.inpt_out_tm", $out_patient_column_list);

	$join  = "inner join ptifmst p on p.ptif_id = i.ptif_id ";
	$join .= "inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd ";
	$join .= "inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no ";
	$join .= "left join ptsubif s on s.ptif_id = i.ptif_id ";
	$join .= "left join drmst d on d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id ";

	$sql  = "select $in_patient_column_list ";
	$sql .= "from inptmst i ";
	$sql .= $join;
	$sql .= "where not ($today < '$from_date') ";
	$sql .= "and not (i.inpt_in_dt > '$to_date') ";
	$sql .= "union all ";
	$sql .= "select $out_patient_column_list ";
	$sql .= "from inpthist i ";
	$sql .= $join;
	$sql .= "where not ($today < '$from_date') ";
	$sql .= "and not (i.inpt_in_dt > '$to_date') ";
	$sql .= "and not (i.inpt_out_dt < '$from_date') ";
	$sql .= "order by 1, 2, 9, 10";
	$cond = "";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	return $sel;
}

function format_date($ymd) {
	return preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $ymd);
}

function format_time($hm) {
	return preg_replace("/(\d{2})(\d{2})/", "$1:$2", $hm);
}

function format_age($birth) {
	if ($birth == "") return "";

	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = date("Y") - $birth_yr;
	if ($birth_md > date("md")) {
		$age--;
	}
	return $age;
}

function format_sex($sex_cd) {
	switch ($sex_cd) {
	case "0":
		return "不明";
	case "1":
		return "男性";
	case "2":
		return "女性";
	default:
		return "";
	}
}

function format_in_days($in_dt, $out_dt) {
	$to_dt = ($out_dt == "") ? date("Ymd") : $out_dt;

	$from_y = substr($in_dt, 0, 4);
	$from_m = substr($in_dt, 4, 2);
	$from_d = substr($in_dt, 6, 2);

	$to_y = substr($to_dt, 0, 4);
	$to_m = substr($to_dt, 4, 2);
	$to_d = substr($to_dt, 6, 2);

	require_once("PEAR/Date/Calc.php");
	return Date_Calc::dateDiff($from_d, $from_m, $from_y, $to_d, $to_m, $to_y) + 1;
}

function format_diff_days($from, $to) {
	if ($from == "" || $to == "" || $from > $to) return "";

	$from_y = substr($from, 0, 4);
	$from_m = substr($from, 4, 2);
	$from_d = substr($from, 6, 2);

	$to_y = substr($to, 0, 4);
	$to_m = substr($to, 4, 2);
	$to_d = substr($to, 6, 2);

	require_once("PEAR/Date/Calc.php");
	$diff = Date_Calc::dateDiff($from_d, $from_m, $from_y, $to_d, $to_m, $to_y);
	return "{$diff}日";
}

function format_operators($con, $ptif_id, $in_dt, $in_tm, $out_dt, $fname) {

	// 入院患者・退院予定患者の場合
	if ($out_dt == "") {
		$sql = "select e.emp_lt_nm, e.emp_ft_nm, j.job_nm from inptop i inner join empmst e on e.emp_id = i.emp_id inner join jobmst j on j.job_id = e.emp_job";
		$cond = "where i.ptif_id = '$ptif_id' order by i.order_no";

	// 退院患者の場合
	} else {
		$sql = "select e.emp_lt_nm, e.emp_ft_nm, j.job_nm from inptophist i inner join empmst e on e.emp_id = i.emp_id inner join jobmst j on j.job_id = e.emp_job";
		$cond = "where i.ptif_id = '$ptif_id' and i.inpt_in_dt = '$in_dt' and i.inpt_in_tm = '$in_tm' order by i.order_no";

	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$operators = array();
	while ($row = pg_fetch_array($sel)) {
		$operators[] = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}（{$row["job_nm"]}）";
	}
	return join("、", $operators);
}

function format_insurance($insurance_cd) {
	switch ($insurance_cd) {
	case "1":
		return "一般";
	case "9":
		return "後期高齢";
	case "2":
		return "老人";
	case "3":
		return "労災";
	case "4":
		return "自賠責";
	case "5":
		return "介護";
	case "6":
		return "生保";
	case "7":
		return "自費";
	case "8":
		return "公傷";
	default:
		return "";
	}
}

function format_existence($flg) {
	switch ($flg) {
	case "t":
		return "有";
	default:
		return "";
	}
}

function format_nurse_degree($nrs_obsv, $free) {
	$values = array();
	if ($nrs_obsv != "") {
		$values[] = $nrs_obsv;
	}
	if ($free != "") {
		switch ($free) {
		case "1":
			$values[] = "��";
			break;
		case "2":
			$values[] = "��";
			break;
		case "3":
			$values[] = "��";
			break;
		case "4":
			$values[] = "��";
			break;
		}
	}
	return join(" - ", $values);
}

function format_result($result_cd) {
	switch ($result_cd) {
	case "1":
		return "死亡";
	case "2":
		return "完治";
	case "3":
		return "不変";
	case "4":
		return "軽快";
	case "5":
		return "後遺症残";
	case "6":
		return "不明";
	case "7":
		return "悪化";
	case "8":
		return "その他";
	default:
		return "";
	}
}

function format_checked($checked) {
	switch ($checked) {
	case "t":
		return "○";
	default:
		return "";
	}
}

function format_decubation_class($dcb_cls_cd) {
	switch ($dcb_cls_cd) {
	case "1":
		return "脳血管疾患等";
	case "2":
		return "高次脳機能障害等";
	case "3":
		return "大腿骨等";
	case "4":
		return "外科手術又は肺炎等";
	case "5":
		return "大腿骨の神経等";
	default:
		return "";
	}
}

function format_rehabilitation_class($rhb_cls_cd) {
	switch ($rhb_cls_cd) {
	case "1":
		return "心大血管疾患リハビリテーション";
	case "2":
		return "脳血管疾患等リハビリテーション";
	case "3":
		return "運動器リハビリテーション";
	case "4":
		return "呼吸器リハビリテーション";
	default:
		return "";
	}
}

function format_term($from, $to) {
	if (strlen($from)) {
		$from = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $from);
	}
	if (strlen($to)) {
		$to = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $to);
	}
	if (strlen($from) || strlen($to)) {
		return trim("$from 〜 $to");
	} else {
		return "";
	}
}

function format_term2($from, $to) {
	if (strlen($from)) {
		$from = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $from);
	}
	if (strlen($to)) {
		$to = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $to);
	}
	if (strlen($from) && strlen($to)) {
		return "$from 〜 $to";
	} else if (strlen($from)) {
		return $from;
	} else if (strlen($to)) {
		return "〜 $to";
	} else {
		return "";
	}
}

function format_transportation($move_flg, $move1, $move2, $move3, $move4, $move5) {
	if ($move_flg == "t") {
		return "独歩行";
	} else if ($move1 == "t") {
		return "杖歩行";
	} else if ($move2 == "t") {
		return "介助歩行";
	} else if ($move3 == "t") {
		return "車椅子（自己駆動）";
	} else if ($move4 == "t") {
		return "車椅子（介助）";
	} else if ($move5 == "t") {
		return "担送";
	} else {
		return "";
	}
}

function format_blood_type($abo, $rh) {
	$values = array();
	if ($abo != "") {
		switch ($abo) {
		case "0":
			$values[] = "不明";
			break;
		case "1":
			$values[] = "A型";
			break;
		case "2":
			$values[] = "B型";
			break;
		case "3":
			$values[] = "AB型";
			break;
		case "4":
			$values[] = "O型";
			break;
		}
	}
	if ($rh != "") {
		switch ($rh) {
		case "0":
			$values[] = "不明";
			break;
		case "1":
			$values[] = "Rh+";
			break;
		case "2":
			$values[] = "Rh-";
			break;
		}
	}
	return join("／", $values);
}

function format_zip($zip1, $zip2) {
	if ($zip1 != "" && $zip2 != "") {
		return $zip1 . "-" . $zip2;
	}
	if ($zip1 != "") {
		return $zip1;
	}
	return $zip2;
}

function format_tel($tel1, $tel2, $tel3) {
	$fields = array();
	if ($tel1 != "") {
		$fields[] = $tel1;
	}
	if ($tel2 != "") {
		$fields[] = $tel2;
	}
	if ($tel3 != "") {
		$fields[] = $tel3;
	}
	return implode("-", $fields);
}

function format_inpt_purpose($inpt_purpose, $short_stay) {
	$purposes = array();
	if ($inpt_purpose != "") $purposes[] = $inpt_purpose;
	if ($short_stay) $purposes[] = "短期入所";
	return implode("／", $purposes);
}

function format_privacy($privacy_flg, $privacy_text) {
	$privacy = "";
	switch ($privacy_flg) {
	case "1":
		$privacy = "要望有り";
		if ($privacy_text != "") $privacy .= "（" . mb_ereg_replace("\r?\n", " ", $privacy_text) . "）";
		break;
	case "2":
		$privacy = "要望無し";
		break;
	}
	return $privacy;
}
