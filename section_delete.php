<?
// このファイルは統合されました。現在利用されていません。（entity_menu.php）
?>

<?
/*
require_once("about_session.php");
require_once("about_authority.php");
require_once("./conf/sql.inf");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_check = check_authority($session, 28, $fname);
if ($auth_check == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// チェックされた診療科をループ
if (is_array($del_sect)) {
	foreach($del_sect as $key) {
		list($tmp_enti_id, $tmp_sect_id) = split("-", $key);

		// 入院情報に使われていたら削除不可
		$sql = "select count(*) from inptmst";
		$cond = "where inpt_enti_id = $tmp_enti_id and inpt_sect_id = $tmp_sect_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$sect_nm = get_sect_nm($con, $tmp_enti_id, $tmp_sect_id, $fname);
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$sect_nm}」は入院情報で使用されているため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 入院予定情報に使われていたら削除不可
		$sql = "select count(*) from inptres";
		$cond = "where inpt_enti_id = $tmp_enti_id and inpt_sect_id = $tmp_sect_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$sect_nm = get_sect_nm($con, $tmp_enti_id, $tmp_sect_id, $fname);
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$sect_nm}」は入院予定情報で使用されているため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 入院履歴情報に使われていたら削除不可
		$sql = "select count(*) from inpthist";
		$cond = "where inpt_enti_id = $tmp_enti_id and inpt_sect_id = $tmp_sect_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$sect_nm = get_sect_nm($con, $tmp_enti_id, $tmp_sect_id, $fname);
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$sect_nm}」は入院履歴情報で使用されているため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 入院キャンセル情報に使われていたら削除不可
		$sql = "select count(*) from inptcancel";
		$cond = "where inpt_enti_id = $tmp_enti_id and inpt_sect_id = $tmp_sect_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$sect_nm = get_sect_nm($con, $tmp_enti_id, $tmp_sect_id, $fname);
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$sect_nm}」は入院キャンセル情報で使用されているため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 転床情報に使われていたら削除不可
		$sql = "select count(*) from inptmove";
		$cond = "where (from_enti_id = $tmp_enti_id and from_sect_id = $tmp_sect_id) or (to_enti_id = $tmp_enti_id and to_sect_id = $tmp_sect_id)";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$sect_nm = get_sect_nm($con, $tmp_enti_id, $tmp_sect_id, $fname);
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$sect_nm}」は転床情報で使用されているため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 診療科を論理削除
		$sql = "update sectmst set";
		$cond = "where enti_id = $tmp_enti_id and sect_id = $tmp_sect_id";
		$set = array("sect_del_flg");
		$setvalue = array("t");
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// 診療科一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'entity_menu.php?session=$session';</script>\n");

function get_sect_nm($con, $enti_id, $sect_id, $fname) {
	$sql = "select sect_nm from sectmst";
	$cond = "where enti_id = $enti_id and sect_id = $sect_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_result($sel, 0, "sect_nm");
}
*/
?>
