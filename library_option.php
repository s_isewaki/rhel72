<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>文書管理 | オプション</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("referer_common.ini");
require_once("menu_common.ini");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 32, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 文書管理管理者権限を取得
$lib_admin_auth = check_authority($session, 63, $fname);

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "library", $fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$intra_menu2 = pg_fetch_result($sel, 0, "menu2");
$intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// オプション設定情報を取得
if ($back != "t") {
    $sql = "select whatsnew_flg, whatsnew_days from liboption";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0) {
        $whatsnew_flg = pg_fetch_result($sel, 0, "whatsnew_flg");
        $whatsnew_days = pg_fetch_result($sel, 0, "whatsnew_days");
    }
}

// デフォルト値の設定
if ($whatsnew_flg == "") {$whatsnew_flg = "t";}
if ($whatsnew_days == "") {$whatsnew_days = 7;}

// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setWhatsnewDaysDisabled() {
    var whatsnew_flg = (document.mainform.whatsnew_flg[0].checked) ;
    document.mainform.whatsnew_days.disabled = !whatsnew_flg;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setWhatsnewDaysDisabled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>"><b><? echo($intra_menu2_3); ?></b></a> &gt; <a href="library_option.php?session=<? echo($session); ?>"><b>オプション設定</b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($intra_menu2)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_info.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($intra_menu2); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<? if ($newlist_flg == 't') { ?>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
<? } ?>
<td>&nbsp;</td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<? echo($session); ?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<? echo($session); ?>"><b>文書管理</b></a> &gt; <a href="library_option.php?session=<? echo($session); ?>"><b>オプション設定</b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<? if ($newlist_flg == 't') { ?>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<!--<img src="img/spacer.gif" width="1" height="2" alt=""><br>-->
<form name="mainform" action="library_option_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="left" valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>最新情報の表示（マイページ）</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="1" class="list">
<tr height="22">
<td width="220" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最近登録された文書を表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="whatsnew_flg" value="t"<? if ($whatsnew_flg == "t") {echo(" checked");} ?> onclick="setWhatsnewDaysDisabled();">する
<input type="radio" name="whatsnew_flg" value="f"<? if ($whatsnew_flg == "f") {echo(" checked");} ?> onclick="setWhatsnewDaysDisabled();">しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示期間</font></td>
<td>
<select name="whatsnew_days">
<? show_whatsnew_days_options($whatsnew_days); ?>
</select>
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_whatsnew_days_options($whatsnew_days) {
    echo("<option value=\"1\"");
    if ($whatsnew_days == "1") {echo(" selected");}
    echo(">1日間（当日分のみ）\n");

    for ($i = 2; $i <= 31; $i++) {
        echo("<option value=\"$i\"");
        if ($whatsnew_days == $i) {echo(" selected");}
        echo(">{$i}日間\n");
    }
}
?>
