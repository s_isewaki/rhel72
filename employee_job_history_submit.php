<?php
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

// 入力内容のチェック
if (empty($_POST["job_id"])) {
    echo("<script type=\"text/javascript\">alert('職種が選択されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if (!preg_match("/^\d\d\d\d\-\d\d\-\d\d \d\d:\d\d:\d\d$/", $_POST["histdate"])) {
    echo("<script type=\"text/javascript\">alert('日付が正しく入力されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// UPDATE
if (!empty($_POST["job_history_id"])) {
    $sql = "UPDATE job_history SET ";
    $cond = "WHERE job_history_id={$_POST["job_history_id"]}";
    $upd = update_set_table($con, $sql, array('emp_id', 'job_id', 'histdate'), array($_POST["emp_id"], $_POST["job_id"], $_POST["histdate"]), $cond, $fname);
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
// INSERT
else {
    $sql = "insert into job_history (emp_id, job_id, histdate) values(";
    $content = array($_POST["emp_id"], $_POST["job_id"], $_POST["histdate"]);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
location.href = 'employee_detail.php?session=<?=$session?>&emp_id=<?php eh($_POST["emp_id"]);?>';
</script>