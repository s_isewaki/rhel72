<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 日（印刷）</title>
<?
require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_menu_common.ini");
require("holiday.php");
require("show_calendar_memo.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// 前月・前日・翌日・翌月のタイムスタンプを変数に格納
$last_month = get_last_month($date);
$last_date = strtotime("-1 day", $date);
$next_date = strtotime("+1 day", $date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

$start_date = date("Ymd", $date);
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $start_date);

$holiday_name = ktHolidayName($date);

if ($holiday_name != "") {
	$holiday_bgcolor = " bgcolor=\"#fadede\"";
} else if ($arr_calendar_memo["{$start_date}_type"] == "5") {
	$holiday_bgcolor = " bgcolor=\"#defafa\"";
	$holiday_name = "&nbsp;";
} else {
	$holiday_bgcolor = "";
	$holiday_name = "&nbsp;";
}

if ($arr_calendar_memo["$start_date"] != "") {
	if ($holiday_name == "&nbsp;") {
		$holiday_name = $arr_calendar_memo["$start_date"];
	} else {
		$holiday_name .= "<br>".$arr_calendar_memo["$start_date"];
	}
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];

// 表の開始時刻
$start_hour = 6;
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
@media print {
	.noprint {visibility:hidden;}
	.reserve {top:1px;}
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<table width="800" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><span class="noprint"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲&nbsp;&nbsp;<? show_range_string($con, $fname, $range); ?></font></span><img src="img/spacer.gif" width="180" height="1" alt=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? show_date_string($date, $session, $range) ?></font></td>
<?
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	$time_str = date("Y")."/".date("m")."/".date("d")." ".date("H").":".date("i");
	echo("作成日時：$time_str\n");
	echo("</font></td>\n");
?>
</tr>
</table>
<div style="position:absolute;top:32;left:12;">
<table width="800" border="1" cellspacing="0" cellpadding="0">
<tr height="22">
<td colspan="2" width="140">&nbsp;</td>

<? 
for ($i = $start_hour; $i < 24; $i++) {

	echo '<td width="28" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">'.sprintf("%02d",$i).'</font></td>'."\n";
}
?>
<td width="128" align="center"<? echo($holiday_bgcolor); ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? echo($holiday_name); ?></font></td>
</tr>
<?
$reservations = get_reservation($con, $categories, $range, $emp_id, $date, $session, $fname);

show_border($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations);
?>
</table>
<table width="800" border="1" cellspacing="0" cellpadding="0" style="position:absolute;top:0;left:0;">
<tr height="22">
<td colspan="2" width="140">&nbsp;</td>
<? 
for ($i = $start_hour; $i < 24; $i++) {

	echo '<td width="28" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">'.sprintf("%02d",$i).'</font></td>'."\n";
}
?>
<td width="128" align="center"<? echo($holiday_bgcolor); ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? echo($holiday_name); ?></font></td>
</tr>
<? show_reservation($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations); ?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示範囲を出力
function show_range_string($con, $fname, $range) {
	if ($range == "all") {
		$range_string = "全て";
	} else if (strpos($range, "-") === false) {
		$sql = "select fclcate_name from fclcate";
		$cond = "where fclcate_id = $range";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "【" . pg_fetch_result($sel, 0, "fclcate_name") . "】";
	} else {
		list($cate_id, $facility_id) = explode("-", $range);
		$sql = "select facility_name from facility";
		$cond = "where fclcate_id = $cate_id and facility_id = $facility_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "・" . pg_fetch_result($sel, 0, "facility_name");
	}

	echo($range_string);
}

// 選択日付を出力
function show_date_string($date, $session, $range) {
	$tmp_ymd = date("Y/m/d", $date);
	$tmp_wd = "(" . get_weekday($date) . ")";
	echo("【{$tmp_ymd}{$tmp_wd}】");
}

// 予約状況の罫線を出力
function show_border($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations) {
	$cate_col = 3 + (24 - $start_hour);

	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	$ymd = date("Ymd", $date);

	// カテゴリ一覧をループ
	foreach ($categories as $tmp_category_id => $tmp_category) {
		$tmp_category_name = $tmp_category["name"];
		$tmp_bg_color = $tmp_category["bg_color"];

		// カテゴリ行を出力
		echo("<tr bgcolor=\"#$tmp_bg_color\">\n");
		echo("<td height=\"22\" colspan=\"{$cate_col}\" style=\"padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>$tmp_category_name</b></font></td>\n");
		echo("</tr>\n");

		// 施設・設備一覧をループ
		foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
			$tmp_facility_name = $tmp_facility["name"];
			$tmp_admin_flg = $tmp_facility["admin_flg"];
/*
			// 予約情報を取得
			$sql3 = "select * from reservation";
			$cond3 = "where fclcate_id = $tmp_category_id and facility_id = $tmp_facility_id and date = '$ymd' and reservation_del_flg = 'f' order by start_time";
			$sel3 = select_from_table($con, $sql3, $cond3, $fname);
			if ($sel3 == 0) {
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 施設・設備行を出力
			$rsv_count = pg_num_rows($sel3);
*/
			// 予約情報配列を参照
			$tmp_reservation = $reservations["{$tmp_facility_id}"];
			$rsv_count = count($tmp_reservation);
			if ($rsv_count <= 1) {
				$rowspan = "";
			} else {
				$rowspan = " rowspan=\"$rsv_count\"";
			}
			echo("<tr height=\"22\">\n");
			echo("<td$rowspan valign=\"top\" style=\"border-right-style:none;padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_facility_name</font></td>\n");
			echo("<td$rowspan style=\"border-left-style:none;border-right-style:none;\">&nbsp;</td>");
			if ($rsv_count == 0) {
				for ($j = $start_hour; $j <= 23; $j++) {
					echo("<td style=\"border-left:#0000cc solid 1px;border-right:none;\">&nbsp;</td>\n");
				}
				echo("<td style=\"border-left:#0000cc solid 1px;\">&nbsp;</td>\n");
				echo("</tr>\n");
			} else {

				// 予約行を出力
				for ($i = 0; $i < $rsv_count; $i++) {
					$tmp_rsv_id = $tmp_reservation[$i]["reservation_id"];
					$tmp_s_time = $tmp_reservation[$i]["start_time"];
					$tmp_e_time = $tmp_reservation[$i]["end_time"];
					$tmp_title = $tmp_reservation[$i]["title"];
					$tmp_rsv_emp_id = $tmp_reservation[$i]["emp_id"];
					$tmp_marker = $tmp_reservation[$i]["marker"];
					if ($i == 0) {
						if ($rsv_count == 1) {
							$row_style = "";
						} else {
							$row_style = "border-bottom:silver solid 1px;";
						}
					} else if ($i < $rsv_count - 1) {
						$row_style = "border-top-style:none;border-bottom:silver solid 1px;";
						echo("<tr>\n");
					} else {
						$row_style = "border-top-style:none;";
						echo("<tr>\n");
					}

					if ($tmp_rsv_emp_id == $emp_id) {
						$bgcolor = "blue";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					} else if ($tmp_admin_flg) {
						$bgcolor = "red";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					} else {
						$bgcolor = "red";
						$detail_url = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					}

					// 時刻指定なしの場合
					if ($tmp_s_time == "") {
						$timeless = "t";
						$tmp_s_time = "0600";
						$tmp_e_time = "0600";
						$detail_url .= "&timeless=t";
					} else {
						$timeless = "f";
					}

					$tmp_s_minutes = substr($tmp_s_time, 0, 2) * 60 + substr($tmp_s_time, 2, 2);
					$tmp_e_minutes = substr($tmp_e_time, 0, 2) * 60 + substr($tmp_e_time, 2, 2);
					// 表の開始時刻以降の場合
					if ($tmp_s_minutes - $start_hour * 60 >= 0) {
						$position_left = round(($tmp_s_minutes - $start_hour * 60) / 5 * 2.5);
						$color_cell_width = round(($tmp_e_minutes - $tmp_s_minutes) / 5 * 2.5);
					} else {
					// 表の開始時刻より前の場合
						$position_left = 0;
						$color_cell_width = round(($tmp_e_minutes - ($start_hour * 60)) / 5 * 2.5);
					}


					$tmp_s_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_s_time);
					$tmp_e_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_e_time);

					if ($timeless != "t") {
						$tmp_time_str = "{$tmp_s_time_str}-{$tmp_e_time_str}<br>";
					} else {
						// 縦方向の位置あわせ用に空白画像を設定
						$tmp_time_str = "<img src=\"img/spacer.gif\" alt=\"\" width=\"8\" height=\"8\">&nbsp;";
					}
					$style = get_style_by_marker_dummy($tmp_marker);

					for ($j = $start_hour; $j <= 23; $j++) {
						echo("<td style=\"border-left:#0000cc solid 1px;border-right:none;$row_style\">&nbsp;</td>\n");
					}
					echo("<td style=\"border-left:#0000cc solid 1px;$row_style\">\n");
					echo("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:0;\">\n");
					echo("<tr>\n");
					echo("<td width=\"120\" style=\"padding:0 3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"#ffffff\">{$tmp_time_str}<span$style>" . h($tmp_title) . "</span></font></td>\n");
					echo("</tr>\n");
					echo("</table>\n");
					echo("</td>\n");
					echo("</tr>\n");
				}
			}
		}
	}
}

// 予約状況を出力
function show_reservation($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations) {
	$cate_col = 3 + (24 - $start_hour);
	$rsv_col = 1 + (24 - $start_hour);
	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	$ymd = date("Ymd", $date);

	// カテゴリ一覧をループ
	foreach ($categories as $tmp_category_id => $tmp_category) {
		$tmp_category_name = $tmp_category["name"];
		$tmp_bg_color = $tmp_category["bg_color"];

		// カテゴリ行を出力
		echo("<tr bgcolor=\"#$tmp_bg_color\">\n");
		echo("<td height=\"22\" colspan=\"{$cate_col}\" style=\"padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>$tmp_category_name</b></font></td>\n");
		echo("</tr>\n");

		// 施設・設備一覧をループ
		foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
			$tmp_facility_name = $tmp_facility["name"];
			$tmp_admin_flg = $tmp_facility["admin_flg"];
/*
			// 予約情報を取得
			$sql3 = "select * from reservation";
			$cond3 = "where fclcate_id = $tmp_category_id and facility_id = $tmp_facility_id and date = '$ymd' and reservation_del_flg = 'f' order by start_time";
			$sel3 = select_from_table($con, $sql3, $cond3, $fname);
			if ($sel3 == 0) {
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 施設・設備行を出力
			$rsv_count = pg_num_rows($sel3);
*/
			// 予約情報配列を参照
			$tmp_reservation = $reservations["{$tmp_facility_id}"];
			$rsv_count = count($tmp_reservation);
			if ($rsv_count <= 1) {
				$rowspan = "";
			} else {
				$rowspan = " rowspan=\"$rsv_count\"";
			}
			echo("<tr height=\"22\">\n");
			echo("<td$rowspan valign=\"top\" style=\"border-right-style:none;padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_facility_name</font></td>\n");
			echo("<td$rowspan style=\"border-left-style:none;border-right-style:none;\">&nbsp;</td>");
			if ($rsv_count == 0) {
				echo("<td colspan=\"{$rsv_col}\" style=\"border-left:#0000cc solid 1px;\">&nbsp</td>\n");
				echo("</tr>\n");
			} else {

				// 予約行を出力
				for ($i = 0; $i < $rsv_count; $i++) {
					$tmp_rsv_id = $tmp_reservation[$i]["reservation_id"];
					$tmp_s_time = $tmp_reservation[$i]["start_time"];
					$tmp_e_time = $tmp_reservation[$i]["end_time"];
					$tmp_title = $tmp_reservation[$i]["title"];
					$tmp_rsv_emp_id = $tmp_reservation[$i]["emp_id"];
					$tmp_marker = $tmp_reservation[$i]["marker"];
					if ($i == 0) {
						if ($rsv_count == 1) {
							$row_style = "";
						} else {
							$row_style = "border-bottom:silver solid 1px;";
						}
					} else if ($i < $rsv_count - 1) {
						$row_style = "border-top-style:none;border-bottom:silver solid 1px;";
						echo("<tr>\n");
					} else {
						$row_style = "border-top-style:none;";
						echo("<tr>\n");
					}

					if ($tmp_rsv_emp_id == $emp_id) {
						$bgcolor = "blue";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					} else if ($tmp_admin_flg) {
						$bgcolor = "red";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					} else {
						$bgcolor = "red";
						$detail_url = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					}

					// 時刻指定なしの場合
					if ($tmp_s_time == "") {
						$timeless = "t";
						$tmp_s_time = "0600";
						$tmp_e_time = "0600";
						$detail_url .= "&timeless=t";
					} else {
						$timeless = "f";
					}

					$tmp_s_minutes = substr($tmp_s_time, 0, 2) * 60 + substr($tmp_s_time, 2, 2);
					$tmp_e_minutes = substr($tmp_e_time, 0, 2) * 60 + substr($tmp_e_time, 2, 2);
					// 表の開始時刻以降の場合
					if ($tmp_s_minutes - $start_hour * 60 >= 0) {
						$position_left = round(($tmp_s_minutes - $start_hour * 60) / 5 * 2.5);
						$color_cell_width = round(($tmp_e_minutes - $tmp_s_minutes) / 5 * 2.5);
					} else {
					// 表の開始時刻より前の場合
						$position_left = 0;
						$color_cell_width = round(($tmp_e_minutes - ($start_hour * 60)) / 5 * 2.5);
					}

					$tmp_s_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_s_time);
					$tmp_e_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_e_time);

					if ($timeless != "t") {
						$tmp_time_str = "{$tmp_s_time_str}-{$tmp_e_time_str}<br>";
					} else {
						$tmp_time_str = "<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
					}
					$style = get_style_by_marker($tmp_marker);

					echo("<td colspan=\"{$rsv_col}\" style=\"border-left:#0000cc solid 1px;$row_style\">\n");
					echo("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"position:relative;left:$position_left;margin:0;\" class=\"reserve\">\n");
					echo("<tr>\n");
					echo("<td bgcolor=\"$bgcolor\" width=\"$color_cell_width\" style=\"cursor:pointer;\" onclick=\"window.open('$detail_url', 'newwin', 'width=640,height=480,scrollbars=yes');\">&nbsp;</td>\n");
					echo("<td width=\"120\" bgcolor=\"#ffffff\" style=\"padding:0 3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_time_str}<span$style>" . h($tmp_title) . "</span></font></td>\n");
					echo("</tr>\n");
					echo("</table>\n");
					echo("</td>\n");
					echo("</tr>\n");
				}
			}
		}
	}
}

?>
