<?php
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $_SERVER['PHP_SELF'];

///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if ($con == "0") {
    js_login_exit();
}

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    js_login_exit();
}
//ユーザ画面用
$section_user_auth = $obj->check_authority_user($session, $fname);

// 委員会・WG連携
$pjt_link = get_project_link_to_yoshiki9($con, $fname);
if (empty($pjt_link)) {
    $pjt_link = "f";
}
$pjt_display = ($pjt_link == "t") ? "" : "none";

///-----------------------------------------------------------------------------
// 勤務パターンを取得
///-----------------------------------------------------------------------------
$sql_ptn = "
    select a.group_id, a.atdptn_id, a.atdptn_nm, b.group_name, a.allowance
    from atdptn a
    left join wktmgrp b on b.group_id = a.group_id
    order by b.group_id, a.atdptn_id
";
$sel_ptn = select_from_table($con, $sql_ptn, '', $fname);
if ($sel_ptn == 0) {
    pg_close($con);
    js_error_exit();
}
$pattern_list = array();
while ($row = pg_fetch_assoc($sel_ptn)) {
    $pattern_list[] = array(
        "id"   => $row["group_id"] . "_" . $row["atdptn_id"],
        "name" => $row["group_name"] . "＞" . $row["atdptn_nm"],
    );
}

///-----------------------------------------------------------------------------
// 職種一覧を取得
///-----------------------------------------------------------------------------
$sql_job = "
    select job_id, job_nm from jobmst
    where job_del_flg = 'f' order by order_no
";
$sel_job = select_from_table($con, $sql_job, '', $fname);
if ($sel_job == 0) {
    pg_close($con);
    js_error_exit();
}
$data_job = array();
$job_list = array();
$nurses = array();
$sub_nurses = array();
while ($row = pg_fetch_assoc($sel_job)) {
    $job_list[$row["job_id"]] = $row["job_nm"];
    $data_job[] = array(
        "id"   => $row["job_id"],
        "name" => $row["job_nm"],
    );
    if (preg_match("/\A(看護師|保健師|助産師)\z/", $row["job_nm"]) === 1) {
        $nurses[] = $row["job_id"];
    }
    else if ($row["job_nm"] === '准看護師') {
        $sub_nurses[] = $row["job_id"];
    }
}

//委員会・WGデータ取得
$arr_pjt_list = get_project_list($con, $session, $fname);


///-----------------------------------------------------------------------------
//登録時
///-----------------------------------------------------------------------------
if ($postback === "1") {
    // トランザクションを開始
    pg_query($con, "begin transaction");

    // delete from duty_shift_rpt_nurse
    $del_n1 = delete_from_table($con, "delete from duty_shift_rpt_nurse", '', $fname);
    if ($del_n1 === 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }

    // delete from duty_shift_rpt_sub_nurse
    $del_n2 = delete_from_table($con, "delete from duty_shift_rpt_sub_nurse", '', $fname);
    if ($del_n2 === 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }

    // delete from duty_shift_rpt_assist_job
    $del_n3 = delete_from_table($con, "delete from duty_shift_rpt_assist_job", '', $fname);
    if ($del_n3 === 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }

    // nurse1
    $nurse1 = array();
    foreach (explode(',', $_POST['nurse1']) as $i => $id) {
        //空白を除く
        if (empty($id)) {
            continue;
        }
        $ins = insert_into_table($con, "insert into duty_shift_rpt_nurse (no, job_id) values (", array($i + 1, $id), $fname);
        if ($ins === 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }
        if (! in_array($id, $nurses)) {
            $nurse1[] = $id;
        }
    }


    // nurse2
    $nurse2 = array();
    foreach (explode(',', $_POST['nurse2']) as $i => $id) {
        //空白を除く
        if (empty($id)) {
            continue;
        }
        $ins = insert_into_table($con, "insert into duty_shift_rpt_sub_nurse (no, job_id) values (", array($i + 1, $id), $fname);
        if ($ins === 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }
        if (! in_array($id, $sub_nurses)) {
            $nurse2[] = $id;
        }
    }

    // nurse3
    $nurse3 = explode(',', $_POST['nurse3']);
    foreach ($nurse3 as $i => $id) {
        //空白を除く
        if (empty($id)) {
            continue;
        }
        $ins = insert_into_table($con, "insert into duty_shift_rpt_assist_job (no, job_id) values (", array($i + 1, $id), $fname);
        if ($ins === 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }
    }

    // delete from duty_shift_rpt_no_subtract
    $del_ptn = delete_from_table($con, "delete from duty_shift_rpt_no_subtract", "", $fname);
    if ($del_ptn === 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }

    // job_pattern
    $job_pattern = array();
    foreach ($_POST['job_id'] as $i => $id) {
        //入力チェック
        if (empty($id) or empty($_POST['ptn_id'][$i])) {
            continue;
        }

        list($ptn_id, $ptdptn_id) = explode('_', $_POST['ptn_id'][$i]);
        $ins = insert_into_table($con, "insert into duty_shift_rpt_no_subtract (no, job_id, pattern_id, atdptn_ptn_id) values (", array($i + 1, $id, $ptn_id, $ptdptn_id), $fname);
        if ($ins === 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }

        $job_pattern[] = array(
            'job_id' => $id,
            'pattern_id' => $_POST['ptn_id'][$i],
        );
    }
    // delete from duty_shift_rpt_no_subtract_pjt
    $del_pjt = delete_from_table($con, "delete from duty_shift_rpt_no_subtract_pjt", '', $fname);
    if ($del_pjt === 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }
    // duty_shift_rpt_no_subtract_pjt
    $pjt = explode(',', $_POST['pjt']);
    foreach ($pjt as $i => $id) {
        //空白を除く
        if (empty($id)) {
            continue;
        }
        $ins = insert_into_table($con, "insert into duty_shift_rpt_no_subtract_pjt (no, pjt_id) values (", array($i + 1, $id), $fname);
        if ($ins === 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }
    }
    
    // トランザクションをコミット
    pg_query("commit");
}

///-----------------------------------------------------------------------------
//初期表示時、DBから取得
///-----------------------------------------------------------------------------
else {
    // 看護師
    $nurse1 = array();
    $sql_nurse1 = "select * from duty_shift_rpt_nurse order by no";
    $sel_nurse1 = select_from_table($con, $sql_nurse1, "", $fname);
    if ($sel_nurse1 === 0) {
        pg_close($con);
        js_error_exit();
    }
    while ($row = pg_fetch_assoc($sel_nurse1)) {
        if (in_array($row['job_id'], $nurses)) {
            continue;
        }
        $nurse1[] = $row['job_id'];
    }

    // 准看護師
    $nurse2 = array();
    $sql_nurse2 = "select * from duty_shift_rpt_sub_nurse order by no";
    $sel_nurse2 = select_from_table($con, $sql_nurse2, "", $fname);
    if ($sel_nurse2 === 0) {
        pg_close($con);
        js_error_exit();
    }
    while ($row = pg_fetch_assoc($sel_nurse2)) {
        if (in_array($row['job_id'], $sub_nurses)) {
            continue;
        }
        $nurse2[] = $row['job_id'];
    }

    // 看護補助者
    $nurse3 = array();
    $sql_nurse3 = "select * from duty_shift_rpt_assist_job order by no";
    $sel_nurse3 = select_from_table($con, $sql_nurse3, "", $fname);
    if ($sel_nurse3 === 0) {
        pg_close($con);
        js_error_exit();
    }
    while ($row = pg_fetch_assoc($sel_nurse3)) {
        $nurse3[] = $row['job_id'];
    }

    // 申し送り時間を差し引かない職種・勤務パターン
    $job_pattern = array();
    $sql_ptn = "select * from duty_shift_rpt_no_subtract order by no";
    $sel_ptn = select_from_table($con, $sql_ptn, "", $fname);
    if ($sel_ptn === 0) {
        pg_close($con);
        js_error_exit();
    }
    while ($row = pg_fetch_assoc($sel_ptn)) {
        $job_pattern[] = array(
            'job_id'     => $row['job_id'],
            'pattern_id' => $row['pattern_id'] . '_' . $row['atdptn_ptn_id'],
        );
    }
    // 委員会
    $pjt = array();
    $sql_pjt = "select * from duty_shift_rpt_no_subtract_pjt order by no";
    $sel_pjt = select_from_table($con, $sql_pjt, "", $fname);
    if ($sel_pjt === 0) {
        pg_close($con);
        js_error_exit();
    }
    while ($row = pg_fetch_assoc($sel_pjt)) {
        $pjt[] = $row['pjt_id'];
    }
}

pg_close($con);

/**
 * 職種プルダウンリスト
 * @param type $data_job
 * @param type $selected_id
 * @param type $blank
 */
function show_options($data_job, $selected_id, $blank) {
    if ($blank) {
        echo('<option value=""></option>');
    }
    foreach ($data_job as $row) {
        $selected = ($row["id"] === $selected_id) ? 'selected' : '';
        printf('<option %s value="%s">%s</option>', $selected, h($row["id"]), h($row["name"]));
    }
}

/**
 * 職種メニュー用にoptionを出力
 * @param type $data_job 職種情報
 * @param type $reg_all_flg reg:登録分 all:全て
 * @param type $tgt_ids 登録分の場合、idの配列
 */
function show_job_options($data_job, $reg_all_flg, $tgt_ids) {
    global $job_list;

    if ($reg_all_flg === "all") {
        foreach ($data_job as $row) {
            printf('<option value="%s">%s</option>', h($row["id"]), h($row["name"]));
        }
    }
    else {
        foreach ($tgt_ids as $id) {
            if (empty($id)) {
                continue;
            }
            printf('<option value="%s">%s</option>', h($id), h($job_list[$id]));
        }
    }
}
/**
 * 委員会メニュー用にoptionを出力
 * @param type $arr_pjt_list 委員会情報
 * @param type $reg_all_flg reg:登録分 all:全て
 * @param type $tgt_ids 登録分の場合、idの配列
 */
function show_pjt_options($arr_pjt_list, $reg_all_flg, $tgt_ids) {
    
    if ($reg_all_flg === "all") {
        foreach ($arr_pjt_list as $row) {
            $wk_name = ($row["pjt_parent_id"] == "") ? $row["pjt_name"] : "　".$row["pjt_name"];
            printf('<option value="%s">%s</option>', h($row["pjt_id"]), h($wk_name));
            echo("\n");
        }
    }
    else {
        $pjt_name_list = array();
        foreach ($arr_pjt_list as $row) {
            $wk_name = ($row["pjt_parent_id"] == "") ? $row["pjt_name"] : "　".$row["pjt_name"];
            $pjt_name_list[$row["pjt_id"]] = $wk_name;
        }
        foreach ($tgt_ids as $id) {
            if (empty($id)) {
                continue;
            }
            printf('<option value="%s">%s</option>', h($id), h($pjt_name_list[$id]));
        }
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix 勤務シフト作成｜管理 |届出添付書類設定</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/shift/entity_report.css">
        <script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="js/fontsize.js"></script>
        <script type="text/javascript" src="js/duty_shift/entity_report.js?<?php echo time(); ?>"></script>
        <script type="text/javascript">
            var job_list = <?= cmx_json_encode($data_job); ?>;
            var pattern_list = <?= cmx_json_encode($pattern_list); ?>;
        </script>
    </head>

    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <? show_manage_title($session, $section_user_auth); ?>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <? show_manage_tab_menuitem($session, $fname, array()); ?>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
            </tr>
        </table>
        <img src="img/spacer.gif" width="1" height="5" alt=""><br>

        <form id="mainform" action="duty_shift_entity_report.php" method="post">

            <input type="hidden" name="session" value="<?= h($session); ?>">
            <input type="hidden" name="postback" value="1">
            <input type="hidden" id="hidden_nurse1" name="nurse1" value="">
            <input type="hidden" id="hidden_nurse2" name="nurse2" value="">
            <input type="hidden" id="hidden_nurse3" name="nurse3" value="">
            <input type="hidden" id="hidden_pjt" name="pjt" value="">

            <div class="listHeader">■看護職員(看護師)とする職種</div>
            <table class="job-select" width="600">
                <tr>
                    <td>
                        <select class="select" name="nurse1" id="nurse1" size="7" multiple>
                            <?
                            foreach ($nurses as $id) {
                                if (empty($id)) {
                                    continue;
                                }
                                printf('<option value="%s" class="disabled" disabled="disabled">%s</option>', h($id), h($job_list[$id]));
                            }
                            foreach ($nurse1 as $id) {
                                if (empty($id)) {
                                    continue;
                                }
                                printf('<option value="%s">%s</option>', h($id), h($job_list[$id]));
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <button class="select" type="button" onclick="selectItem('nurse1', 'job1');">←</button>
                        <button class="select" type="button" onclick="deleteItem('nurse1');">→</button>
                    </td>
                    <td>
                        <select class="select" name="job1" id="job1" size="7" multiple>
                            <?
                            show_job_options($data_job, "all", array());
                            ?>
                        </select>
                    </td>
                </tr>
            </table>

            <div class="listHeader">■看護職員(准看護師)とする職種</div>
            <table class="job-select" width="600">
                <tr>
                    <td>
                        <select class="select" name="nurse2" id="nurse2" size="7" multiple>
                            <?
                            foreach ($sub_nurses as $id) {
                                if (empty($id)) {
                                    continue;
                                }
                                printf('<option value="%s" class="disabled" disabled="disabled">%s</option>', h($id), h($job_list[$id]));
                            }
                            foreach ($nurse2 as $id) {
                                if (empty($id)) {
                                    continue;
                                }
                                printf('<option value="%s">%s</option>', h($id), h($job_list[$id]));
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <button class="select" type="button" onclick="selectItem('nurse2', 'job2');">←</button>
                        <button class="select" type="button" onclick="deleteItem('nurse2');">→</button>
                    </td>
                    <td>
                        <select class="select" name="job2" id="job2" size="7" multiple>
                            <?
                            show_job_options($data_job, "all", array());
                            ?>
                        </select>
                    </td>
                </tr>
            </table>

            <div class="listHeader">■看護補助者とする職種</div>
            <table class="job-select" width="600">
                <tr>
                    <td>
                        <select class="select" name="nurse3" id="nurse3" size="7" multiple>
                            <?
                            foreach ($nurse3 as $id) {
                                if (empty($id)) {
                                    continue;
                                }
                                printf('<option value="%s">%s</option>', h($id), h($job_list[$id]));
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <button class="select" type="button" onclick="selectItem('nurse3', 'job3');">←</button>
                        <button class="select" type="button" onclick="deleteItem('nurse3');">→</button>
                    </td>
                    <td>
                        <select class="select" name="job3" id="job3" size="7" multiple>
                            <?
                            show_job_options($data_job, "all", array());
                            ?>
                        </select>
                    </td>
                </tr>
            </table>

            <div class="listHeader">■申し送り時間を差し引かない職種・勤務パターン</div>
            <table id="jobptns" class="job-pattern">
                <tr height="22">
                    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
                    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務パターン</font></td>
                    <td></td>
                </tr>
                <tr height="22">
                    <td>
                        <select name="job_id[]">
                            <?
                            show_options($data_job, $job_pattern[0]['job_id'], true);
                            ?>
                        </select>
                    </td>
                    <td>
                        <select name="ptn_id[]">
                            <?
                            show_options($pattern_list, $job_pattern[0]['pattern_id'], true);
                            ?>
                        </select>
                    </td>
                    <td><input id="addbtn" type="button" value="追加" onclick="addDatRow(this);" style="margin-left:2px;">
                    </td>
                </tr>
                <?
                foreach ($job_pattern as $idx => $row) {
                    if ($idx === 0) {
                        continue;
                    }
                    echo('<tr><td><select name="job_id[]">');
                    show_options($data_job, $row['job_id'], false);
                    echo('</select></td>');
                    echo('<td><select name="ptn_id[]">');
                    show_options($pattern_list, $row['pattern_id'], false);
                    echo('</select></td>');
                    echo('<td><input id="addbtn" type="button" value="追加" onclick="addDatRow(this);" style="margin-left:2px;"><input type="button" value="削除" onclick="deleteDatRow(this);" style="margin-left:2px;"></td></tr>');
                }
                ?>
            </table>

            <div class="listHeader" style="display:<? echo($pjt_display); ?>">■勤務時間から控除しない委員会・WG</div>
            <table class="job-select" width="600" style="display:<? echo($pjt_display); ?>">
                <tr>
                    <td>
                        <select class="select" name="pjt" id="pjt" size="7" multiple>
<?
show_pjt_options($arr_pjt_list, "reg", $pjt);
                            ?>
                        </select>
                    </td>
                    <td>
                        <button class="select" type="button" onclick="selectItem('pjt', 'pjt_list');">←</button>
                        <button class="select" type="button" onclick="deleteItem('pjt');">→</button>
                    </td>
                    <td>
                        <select class="select" name="pjt_list" id="pjt_list" size="7" multiple>
<?
show_pjt_options($arr_pjt_list, "all", array());
                            ?>
                        </select>
                    </td>
                </tr>
            </table>

            <button type="button" class="submit" onclick="updateSetting();">更新</button>
        </form>
    </body>
</html>

<?
// 委員会・WGデータを取得（オプションリスト用）
//show_project_list.iniのshow_project_list()を参考にしている
function get_project_list($con, $session, $fname) {
		$arr_list = array();
        // 委員会情報を取得(管理者権限)
		$sql = "select project.*, empmst.emp_lt_nm, empmst.emp_ft_nm, class_nm as struc1, atrb_nm as struc2 from project ";
		$sql .= " inner join empmst on project.pjt_response = empmst.emp_id 
				left join classmst on project.class_id = classmst.class_id 
				left join atrbmst on project.atrb_id = atrbmst.atrb_id ";
		
		$cond = "where  project.pjt_parent_id is null and project.pjt_delete_flag = 'f' ";
		$cond .= " order by coalesce(classmst.order_no, 0), coalesce(atrbmst.order_no, 0), pjt_sort_id";
		
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		

		// 0件の場合は復帰
		if (pg_num_rows($sel) == 0) {
			return $arr_list;
		}
		
		// 委員会名の昇順で並び替え
		$projects = pg_fetch_all($sel);

	foreach ($projects as $tmp_project) 
	{
        //委員会データ設定
        $arr_list[] = $tmp_project;
		$tmp_pjt_id = $tmp_project["pjt_id"];

			// WG情報を取得(管理者権限)
			$sql = "select project.*, empmst.emp_lt_nm, empmst.emp_ft_nm from project 
			inner join empmst on project.pjt_response = empmst.emp_id ";
			$cond = " where project.pjt_parent_id = $tmp_pjt_id and project.pjt_delete_flag = 'f'";
			$cond .= " order by wg_sort_id";

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// WG名の昇順で並び替え
		$wgs = (pg_num_rows($sel) == 0) ? array() : pg_fetch_all($sel);

		// WGが存在する場合
		if (pg_num_rows($sel) > 0) {
            //WGデータ設定
			foreach ($wgs as $tmp_wg) {
                $arr_list[] = $tmp_wg;
            }
        }
    }
    return $arr_list;
}

function get_project_link_to_yoshiki9($con, $fname) {

    $sql = "select value from system_config ";
    $cond = " where key = 'project.link_to_yoshiki9' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
    if (pg_num_rows($sel) > 0) {
        $ret = pg_fetch_result($sel, 0, "value");
    }
    else {
        $ret = "f";
    }
    return $ret;    

}