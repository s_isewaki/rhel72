<?
require("about_session.php");
require("get_values.ini");
require_once("about_postgres.php");
require("show_clock_in_common.ini");
require_once("atdbk_workflow_common_class.php");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}



// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");


$obj = new atdbk_workflow_common_class($con, $fname);
$obj->approve_application($apply_id, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, "RTN");


// 申請情報を取得
$sql = "select apply_status, emp_id, target_date from rtnapply";
$cond = "where apply_id = $apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$apply_status = pg_fetch_result($sel, 0, "apply_status");

// 否認の場合
if ($apply_status == "2") {


	$apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
	$target_date = pg_fetch_result($sel, 0, "target_date");

	// 勤務実績情報を取得
	$arr_result = get_atdbkrslt($con, $apply_emp_id, $target_date, $fname);

	// 申請情報を更新
	$sql = "update rtnapply set";
	$set = array();
	$setvalue = array();
	for ($i = 1; $i <= 10; $i++) {
		array_push($set, "o_start_time$i", "o_end_time$i");
		array_push($setvalue, $arr_result["o_start_time$i"], $arr_result["o_end_time$i"]);
	}
	$cond = "where apply_id = $apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 勤務実績の退勤後復帰・復帰後退勤時刻を初期化
	$sql = "update atdbkrslt set";
	$set = array();
	$setvalue = array();
	for ($i = 1; $i <= 10; $i++) {
		array_push($set, "o_start_time$i", "o_end_time$i");
		array_push($setvalue, null, null);
	}
	$cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
if($pnt_url != "") {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");	
	echo("<script language=\"javascript\">window.close();</script>\n");
}
?>
