<?/*

画面パラメータ
	$session セッションID
	$div_id  診療区分ID
	$linktmpl 紐付け対象テンプレートID(配列)

*/?><meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療区分登録権限チェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//共通SQL部品
$in=" in (";
$count=count($linktmpl);
for($i=0;$i<$count;$i++){
	if(!$linktmpl[$i]==""){
		$in.="'$linktmpl[$i]'";
		if($i<($count-1)){
			$in.=",";
		}
	}
}
$in.=")";


//全て未選択フラグ
$is_no_select = false;
if($in == " in ()")
{
	$is_no_select = true;;
}


////使用中チェック　･･･使用中テンプレートを紐付け解除することはできない。
//$tbl = "select * from divmst left join summary on summary.diag_div = divmst.diag_div";
//$cond = " where divmst.div_id = '$div_id' and summary.tmpl_id is not null";
//if(!$is_no_select)
//{
//	$cond .= " and not summary.tmpl_id $in";
//}
//$sel = select_from_table($con,$tbl,$cond,$fname);
//if($sel == 0){
//	pg_close($con);
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showErrorPage(window);</script>");
//	pg_close($con);
//	exit;
//}
//$num = pg_numrows($sel);
//if($num > 0)
//{
//	echo("<script language='javascript'>alert(\"使用中のテンプレートを割り当て解除することはできません。\");</script>");
//	echo("<script language='javascript'>history.back();</script>");
//	pg_close($con);
//	exit;
//}


// トランザクションの開始
pg_query($con, "begin");

//tmpl_div_linkテーブルより、対象の診療区分のデフォルトテンプレートIDを取得
$tbl = "select tmpl_div_link.tmpl_id from tmpl_div_link ";
$cond = " where tmpl_div_link.div_id = '$div_id' and default_flg = '1'";
$sel = select_from_table($con,$tbl,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$num = pg_numrows($sel);
$old_default_tmpl_id = "";
if($num != 0)
{
	$old_default_tmpl_id = pg_result($sel,0,"tmpl_id");
}


//新デフォルトテンプレートの決定
$new_default_tmpl_id = "";
$count=count($linktmpl);
for($i=0;$i<$count;$i++){
	if(!$linktmpl[$i]==""){
		$tmpl_id = $linktmpl[$i];//対象テンプレート

		//旧デフォルトが今回も指定されている場合はそれをデフォルトとする。
		if($tmpl_id == $old_default_tmpl_id){
			$new_default_tmpl_id = $old_default_tmpl_id;
			break;
		}
		//先頭のテンプレートをデフォルトテンプレートとする。(旧デフォルトが未指定の場合)
		if($new_default_tmpl_id == "")
		{
			$new_default_tmpl_id = $tmpl_id;
		}
	}
}


//tmpl_div_linkテーブルより、対象の診療区分の紐付けデータを全削除
$delete_status="delete from tmpl_div_link where tmpl_div_link.div_id = '$div_id' ";
$result_delete=pg_exec($con,$delete_status);
if($result_delete==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	pg_query($con, "rolllback");
	pg_close($con);
	exit;
}



//tmpl_div_linkテーブルへ、紐付け情報を登録
$count=count($linktmpl);
for($i=0;$i<$count;$i++){
	if(!$linktmpl[$i]==""){
		$tmpl_id = $linktmpl[$i];//対象テンプレート

		//デフォルト判定
		$default_flg = "f";
		if($tmpl_id == $new_default_tmpl_id)
		{
			$default_flg = "t";
		}

		$intbl = "insert into tmpl_div_link (tmpl_id,div_id,default_flg) values(";
		$content = array($tmpl_id,$div_id,$default_flg);
		$ins = insert_into_table($con,$intbl,$content,$fname);
		if ($ins == 0) {
			//echo($tmpl_id."<br>");
			//echo($div_id."<br>");
			//echo($default_flg."<br>");
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			pg_query($con, "rolllback");
			pg_close($con);
			exit;
		}

	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// テンプレート管理画面に遷移
echo("<script language=\"javascript\">location.href = 'summary_kanri_tmpl.php?session=$session';</script>");
?>
