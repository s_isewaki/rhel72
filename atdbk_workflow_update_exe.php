<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 承認者情報をDELETE〜INSERT
$sql = "delete from tmcdwkfw";
$cond = "where emp_id = '$emp_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if ($overtime_id == "") {$overtime_id = null;}
if ($modify_id == "") {$modify_id = null;}
if ($return_id == "") {$return_id = null;}
$sql = "insert into tmcdwkfw (emp_id, overtime_id, modify_id, return_id) values (";
$content = array($emp_id, $overtime_id, $modify_id, $return_id);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 承認者設定画面を再表示
echo("<script type=\"text/javascript\">location.href = 'atdbk_workflow.php?session=$session';</script>");
?>
