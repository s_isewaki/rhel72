<?
/*
 * 印影画像出力
 * パラメータ
 *  $session
 *  $emp_id
 *  $apv_flg 1:承認 2:否認 3:差戻し
 *  $imprint_flg 't':印影機能を利用する 'f','':印影機能を利用しない。既存の承認画像を表示。　※apv_flg=1の時、判定に使用
 *  $t ダミー※operaで画像再登録しても、キャッシュが利用されるらしく
 *             画像が変わらないためURLを変化させるため必要。呼出側で年月日時分秒を指定している。
 */
require("about_session.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$dirname = "cas_workflow/imprint/";

$img_file = "img/spacer.gif";
$content_type = "Content-Type: image/gif";

switch ($apv_flg) {
case 1:
	if ($imprint_flg == "t") {
		$img_file1 = "$dirname$emp_id.gif";
		$img_file2 = "$dirname$emp_id.jpg";
// 当該職員の画像が登録済みであれば出力
		if (is_file($img_file1)) {
			$img_file = $img_file1;
		} else if (is_file($img_file2)) {
			$img_file = $img_file2;
			$content_type = "Content-Type: image/jpeg";
		}
	} else {
		$img_file = "img/approved.gif";
	}

	break;
case 2:
	$img_file = "img/approve_ng.gif";

	break;
case 3:
	$img_file = "img/returned.gif";

	break;
}


if (is_file($img_file)) {
	$fp = fopen($img_file, "r");
	if ($fp) {
		header($content_type);
		while (!feof($fp)) {
			echo fgetc($fp);
		}
		fclose($fp);
	} else {
		echo("<meta http-equiv='Content-Type' content='text/html; charset=EUC-JP'>");
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
	}
	exit;
}

/// 未登録のため空白画像を出力する
$fp = fopen("img/spacer.gif", "r");
if ($fp) {
	header("Content-Type: image/gif");
	while (!feof($fp)) {
		echo fgetc($fp);
	}
	fclose($fp);
} else {
	echo("<meta http-equiv='Content-Type' content='text/html; charset=EUC-JP'>");
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
}
?>
