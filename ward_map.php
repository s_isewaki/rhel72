<?
ob_start();

require("./about_session.php");
require("./about_authority.php");
require("./about_postgres.php");

//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病床管理権限チェック
$check_auth = check_authority($session,14,$fname);
if($check_auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟マップ画像の存在確認
if (is_file("map/$bldg_wd.gif")) {
	$file_name = "map/$bldg_wd.gif";
} else if (is_file("map/$bldg_wd.jpg")) {
	$file_name = "map/$bldg_wd.jpg";
} else if (is_file("map/$bldg_wd.png")) {
	$file_name = "map/$bldg_wd.png";
} else {
	$file_name = "";
}

ob_clean();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜病棟マップ</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="680" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病棟マップ</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<?
if ($file_name != "") {
	echo("<img src=\"$file_name\">");
} else {
	echo("<table width=\"760\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
	echo("<tr>");
	echo("<td height=\"22\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">マップは登録されていません。</font></td>");
	echo("</tr>");
	echo("</table>");
}
?>
</center>
</body>
</html>
