<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | タイムカード入力</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("atdbk_menu_common.ini");

require_once './kintai/common/mdb_wrapper.php';
require_once './kintai/common/user_info.php';
require_once './kintai/common/master_util.php';
require_once './kintai/common/code_util.php';
require_once './kintai/common/date_util.php';

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//フォームデータ
$yyyymm = $_REQUEST["yyyymm"];



// 勤務管理権限の取得
$work_admin_auth = check_authority($session, 42, $fname);

//データアクセス
try {

	MDBWrapper::init(); // おまじない
	$log = new common_log_class(basename(__FILE__));
	$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

	//職員情報
	$ukey  = $session;
	$uType = "s";
	$user_info   = UserInfo::get($ukey, $uType);
	$base_info   = $user_info->get_base_info();
	$job_info    = $user_info->get_job_info();
	$class_info  = $user_info->get_class_info();
	$attr_info   = $user_info->get_attr_info();
	$dept_info   = $user_info->get_dept_info();
	$croom_info  = $user_info->get_class_room_info();
	$cond_info   = $user_info->get_emp_condition();
	
	//組織階層
	$class_cnt  = MasterUtil::get_organization_level_count();
	
	//所属
	$shozoku = $class_info->class_nm."＞".$attr_info->atrb_nm."＞".$dept_info->dept_nm;
	if($class_cnt == 4){
		if($croom_info->room_nm != ""){
			$shozoku .= "＞".$croom_info->room_nm;
		}
	}
	
	//締め日情報を取得
	$closing_info  = MasterUtil::get_closing();
	if(is_null($closing_info)){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if($cond_info->duty_form == "2"){
		if($closing_info["closing_parttime"] != ""){
			$closing = $closing_info["closing_parttime"];
		}else{
			$closing = $closing_info["closing"];
		}
	}else{
		$closing = $closing_info["closing"];
	}
	
	//期間取得（月度考慮）
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);
	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);
	$prdArr = DateUtil::get_cutoff_period($cond_info->duty_form, $year, $month);
	
	// 勤務実績を取得
	$sql  = "select ";
	$sql .= " atdbkrslt.emp_id, ";
	$sql .= " atdbkrslt.date, ";
	$sql .= " atdbkrslt.pattern, ";
	$sql .= " atdbkrslt.reason, ";
	$sql .= " atdbkrslt.tmcd_group_id, ";
	$sql .= " atdptn.atdptn_nm, ";
	$sql .= " case when atdbk_reason_mst.display_name = '' then atdbk_reason_mst.default_name else display_name end as reason_name ";
	$sql .= "FROM ";
	$sql .= " atdbkrslt ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdptn ";
	$sql .= "ON ";
	$sql .= " CAST(atdptn.atdptn_id AS varchar) = atdbkrslt.pattern AND ";
	$sql .= " atdptn.group_id  = atdbkrslt.tmcd_group_id ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdbk_reason_mst ";
	$sql .= "ON ";
	$sql .= " atdbk_reason_mst.reason_id = atdbkrslt.reason ";
	$sql .= "WHERE ";
	$sql .= " atdbkrslt.emp_id = '".$base_info->emp_id."' AND ";
    $sql .= " (atdbkrslt.date >= '".$prdArr["start"]."' AND atdbkrslt.date <= '".$prdArr["end"]."') order by atdbkrslt.date";
	$atdbk_rslt = $db->find($sql);
	
	// リストの編集
	$display_list = array();
	
	// 区分マスタを取得
	$ovtmkbn_mst = MasterUtil::get_ovtm_div_all();
	$total_time = 0;
	// 集計用連想配列初期化
	$ovtmkbn_ttl_list = array();
	$ovtmkbn_mst_list = array();
	foreach ($ovtmkbn_mst as $item) {
		$ovtmkbn_ttl_list[$item["ovtmkbn_kind"]] = 0; // 区分毎集計初期化
		$ovtmkbn_mst_list[$item["ovtmkbn_id"]] = $item["ovtmkbn_kind"]; // コード変換用
	}

	foreach ($atdbk_rslt as $rslt) {
		$display_line = array();
		$date_map = DateUtil::split_date($rslt["date"]);
		
		// 曜日
		$display_line["date"] = intval($date_map["m"])."月".intval($date_map["d"])."日";
		$display_line["cal_date"] = $rslt["date"];
		$display_line["day_of_week"] = CodeUtil::get_day_of_week($date_map["w"]);
		$display_line["atdptn_name"] = $rslt["atdptn_nm"];
		$display_line["reason_name"] = $rslt["reason_name"];
		
		// 区分カラムの初期化
		$line_kbn_columns = array(); // 表示用
		$line_kbn_total = array(); // 行の集計用
		foreach ($ovtmkbn_mst as $item) {
			$line_kbn_columns[$item["ovtmkbn_kind"]] = "";
			$line_kbn_total[$item["ovtmkbn_kind"]] = "0";
		}
		
		// ここから集計処理
		$ovtm_get_sql = "select ";
		$ovtm_get_sql.= "kintai_ovtm_seq.minute, kintai_ovtm_seq.ovtmkbn_id from kintai_ovtm_seq ";
		$ovtm_get_sql.= "join ";
		$ovtm_get_sql.= "kintai_ovtm on kintai_ovtm_seq.emp_id = kintai_ovtm.emp_id and kintai_ovtm_seq.date = kintai_ovtm.date ";
		$ovtm_get_sql.= "where ";
		$ovtm_get_sql.= "kintai_ovtm_seq.emp_id = '".$rslt["emp_id"]."' and kintai_ovtm_seq.date = '".$rslt["date"]."' and kintai_ovtm.apply_stat = '1' ";
		$ovtm_get_sql.= "order by kintai_ovtm_seq.no";
		$ovtm_times = $db->find($ovtm_get_sql);
		
		foreach ($ovtm_times as $time) {
			$minute = $time["minute"];
			$kbn_kind = $ovtmkbn_mst_list[$time["ovtmkbn_id"]]; // ovtnkbn_kindを取得
			
			if ($total_time + $minute > 3600) { // 60h over
				$under = 0;
				$over  = 0;
				if ($total_time < 3600) {
					$under = 3600 - $total_time;
					$over  = $minute - $under;
				} else {
					$over = $minute;
				}
				
				switch($kbn_kind) {
					case "kbn_125": // 125 → 150
						$line_kbn_total["kbn_125"] += $under;
						$line_kbn_total["kbn_150"] += $over;
						break;
					case "kbn_135": // 135 → 150
						$line_kbn_total["kbn_135"] += $under;
						$line_kbn_total["kbn_150"] += $over;
						break;
					case "kbn_150": // 150 → 175
						$line_kbn_total["kbn_150"] += $under;
						$line_kbn_total["kbn_175_none"] += $over;
						break;
					case "kbn_160": // 160 → 175
						$line_kbn_total["kbn_160"] += $under;
						$line_kbn_total["kbn_175_none"] += $over;
						break;
					case "kbn_135_hol": // 135休  → 150
						$line_kbn_total["kbn_135_hol"] += $under;
						$line_kbn_total["kbn_150"] += $over;
						break;
					default:
						// そのまま加算
						$line_kbn_total[$kbn_kind] += $minute;
						break;
				}
			} else {
				// そのまま加算
				$line_kbn_total[$kbn_kind] += $minute;
			}
            $total_time += $minute;
        }
		
		$line_total = 0;
		// 行の計算が終わったら区分毎合計に加算、表示用に編集
		foreach($line_kbn_total as $kind => $value) {
			// 区分合計に加算
			$ovtmkbn_ttl_list[$kind] += $value;
			$line_total += $value;
			// 表示用
			if ($value > 0) {
				$line_kbn_columns[$kind] = intval($value / 60).":".sprintf('%02d', $value % 60);
			}
		}
		
		$display_line["kbn_list"] = $line_kbn_columns;
		if ($line_total > 0) {
			$display_line["line_total"] = intval($line_total / 60).":".sprintf('%02d', $line_total % 60);
		}
		array_push($display_list, $display_line);
	}
	
} catch (BusinessException $bex) {
	$message = $bex->getMessage();
	echo("<script type='text/javascript'>alert('".$message."');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} catch (FatalException $fex) { 
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

function getBgColor($date) {
	if(is_cal_holiday($date) == true){
		return "#FADEDE";
	}else{
		return "#FFFFFF";
	}

}
function is_cal_holiday($date) {
	$sql = "select type from calendar where date = :date";
	$value = get_mdb()->one(
		$sql, array("text"), array("date" => $date));

	if (in_array($value, array("6", "7"))) {
		return true;
	}
	return false;
}
function get_mdb() {
	$log = new common_log_class(basename(__FILE__));
	return new MDB($log);
}
// データベースに接続
$con = connect2db($fname);

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--

//一括修正画面
function openTimecardAll() {
	base_left = 0;
	base = 0;
	wx = window.screen.availWidth;
	wy = window.screen.availHeight;
	url = 'atdbk_timecard_all.php?session=<?=$session?>&yyyymm=<? echo($yyyymm); ?>&view=<? echo($view); ?>&check_flg=<? echo($check_flg); ?>';
	window.open(url, 'TimecardAll', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
}

function print_xls(){

	if(document.frmKintai.emp_id.value == ""){
		alert("職員が選択されていません。");
		return false;
	}

	document.frmKintai.action = "./kintai_ovtmlist_detail_xls.php";
	document.frmKintai.method = "POST";
	document.frmKintai.submit();

}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
td.lbl {padding-right:1em;}
td.txt {padding-right:0.5em;}
td.tm {width:2.8em;text-align:right;}
td.mark {width:2.8em;text-align:center;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="frmKintai" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<?php 
if ($work_admin_auth == "1") { 
?>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?php echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<?php 
} 
?>
				</tr>
			</table>
<?php
	// メニュー表示
	show_atdbk_menuitem($session, $fname, "");
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
<?php
if ($closing == "") {  // 締め日未登録の場合 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。管理者に連絡してください。</font></td>
				</tr>
			</table>
<?php
} else { 
?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                    <?php
    // タイムカードタブ直下のメニュー表示
    timecard_menu_view($con, $fname, $session, $yyyymm, "url_ovtmlist_detail");
 ?>
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td height="22">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="kintai_ovtmlist_detail.php?session=<?php echo($session); ?>&yyyymm=<?php echo($last_yyyymm); ?>&view=<?php echo($view); ?>">&lt;前月</a>&nbsp;<?php echo("{$year}年{$month}月度"); ?>&nbsp;<a href="kintai_ovtmlist_detail.php?session=<?php echo($session); ?>&yyyymm=<?php echo($next_yyyymm); ?>&view=<?php echo($view); ?>">翌月&gt;</a></font>&nbsp;
								</td>
								<td >
									<input type="button" value="印刷" onclick="return print_xls();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td height="22" width="">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									職員ID：<?php echo $base_info->emp_personal_id; ?>&nbsp;&nbsp;
									職員氏名：<?php echo $base_info->emp_lt_nm."&nbsp;".$base_info->emp_ft_nm; ?>&nbsp;&nbsp;
									所属：<?php echo $shozoku; ?>&nbsp;&nbsp;
									（
										<?php echo $job_info->job_nm; ?>&nbsp;
										<?php echo CodeUtil::get_duty_form($cond_info->duty_form); ?>&nbsp;
										<?php echo CodeUtil::get_salary_type($cond_info->wage); ?>
									）
									</font>
								</td>
								<td align="right" width="">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<!--りすと-->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
			<table border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="center" nowrap width="56" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
					<td align="center" nowrap width="18"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜<br>日</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<?php
foreach($ovtmkbn_mst as $column) {
?>
<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $column["ovtmkbn_name"]; ?></font></td>
<?php
}
?>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計<br>(日次)</font></td>
				</tr>
<!-- ここから勤務実績＋超勤区分 -->
<?php
foreach ($display_list as $line) {
?>
			<tr class="j12" bgcolor="<?php echo getBgColor($line["cal_date"]); ?>">
				<td align="center" nowrap><?php echo $line["date"]; ?></td>
				<td align="center" nowrap><?php echo $line["day_of_week"]; ?></td>
				<td align="center" nowrap><?php echo $line["atdptn_name"]; ?></td>
				<td align="center" nowrap><?php echo $line["reason_name"]; ?></td>
<?php
// 区分を表示
$line_kbn_columns = $line["kbn_list"];
foreach ($line_kbn_columns as $kbn) {
?>
				<td align="right" nowrap><?php echo $kbn; ?></td>
<?php
}
?>
				<td align="right" nowrap><?php echo $line["line_total"]; ?></td>
			</tr>
<?php
}
?>
			<tr class="j12">
				<td colspan="4" align="right">合計(月次)</td>
<?php
foreach ($ovtmkbn_ttl_list as $item) {
	$kbn_ttl = $item > 0 ? intval($item / 60).":".sprintf('%02d', $item % 60):"";
?>
				<td align="right" nowrap><?php echo $kbn_ttl; ?></td>
<?php
}
$all_total_time = $total_time > 0 ? intval($total_time / 60).":".sprintf('%02d', $total_time % 60):"";
?>
				<td align="right" nowrap><?php echo $all_total_time; ?></td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<?php
}
?>
<input type="hidden" name="session" value="<?php echo $session; ?>">
<input type="hidden" name="emp_id"  value="<?php echo $base_info->emp_id; ?>">
<input type="hidden" name="yyyymm"  value="<?php echo $yyyymm; ?>">

</form>
</body>
</html>
