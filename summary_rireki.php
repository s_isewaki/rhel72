<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("show_patient_next.ini"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<?
require("label_by_profile_type.ini");
require_once("sum_exist_workflow_check_exec.ini");
require_once("summary_common.ini");

//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

require_once("summary_common.ini");
summary_common_write_operate_log("access", $con, $fname, $session, "", "", $pt_id);

// 検索フォームからの引継ぎ値をurlencode
$url_key1 = urlencode(@$key1);
$url_key2 = urlencode(@$key2);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <? echo ($patient_title); ?>選択</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border:#5279a5 solid 1px;}
</style>
<script type="text/javascript">
	// iframeのリサイズ
	// 左右子画面のサイズと親画面のサイズを制御する。
	// 上の階層(main_menu.php)のresizeIframe()関数は利用しない。
	var CurrentHeight = {};
	CurrentHeight.hidari = 0;
	CurrentHeight.migi = 0;
	var DefaultHeight = {};
	DefaultHeight.hidari = 0;
	DefaultHeight.migi = 0;
	var StartMargin = 0;
	// 左右子画面の初期状態の高さを設定する。
	// 左右子画面からそれぞれ呼ばれる。
	function setDefaultHeight(hidari_or_migi, h){
		DefaultHeight[hidari_or_migi] = h;
	}
	// 左右子画面の高さを設定する。
	// 左右子画面それぞれから呼ばれる。
	// 右：summary_new.php、summary_update.php
	// 左：summary_rireki_table.php、summary_rireki_naiyo.php
	// 設定した後は、どちらか高いほうにあわせて自分の高さを調整する。
	// 親画面のoverflowをhiddenに設定して、スクロールバーのちらつきをなくす。
	function checkStartHeightMargin(){
		if (!window.parent) return;
		iframe = window.parent.document.getElementById("floatingpage");
		if (!iframe) return;
//		iframe.style.overflow = "hidden";
		var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
		h = Math.max(h, document.body.scrollHeight);
		h = Math.max(h, 300);

		var iframe = document.getElementById("hidari");
		CurrentHeight.hidari = parseInt(iframe.style.height);
		var iframe = document.getElementById("migi");
		CurrentHeight.migi = parseInt(iframe.style.height);

		StartMargin = h - Math.max(CurrentHeight.hidari, CurrentHeight.migi);
	}
	function resizeIframeHeight(hidari_or_migi, h_new){
		var h_cur = CurrentHeight[hidari_or_migi];
		// 変更なしなら終了
		if (h_cur == h_new) return;

		var iframe = document.getElementById(hidari_or_migi);
		iframe.style.height = h_new + "px";

		if (!window.parent) return;
		iframe = window.parent.document.getElementById("floatingpage");
		if (!iframe) return;

		// 高くなった場合は、このドキュメントのサイズを再測定
		if (h_cur < h_new){
			var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
			h = Math.max(h, document.body.scrollHeight);
			h = Math.max(h, 300);
			iframe.style.height = (h*1+50) + "px";
			CurrentHeight[hidari_or_migi] = h_new;
			return;
		}

		// 低くなった場合は、差分を縮める
		var tonari = (hidari_or_migi == "hidari" ? CurrentHeight.migi : CurrentHeight.hidari);
		// 最初からとなりが高い場合は、何もしない
		if (tonari >= h_cur){
			return;
		}
		// となりが低い場合は、差分をそのまま引く
		if (tonari <= h_new){
			iframe.style.height = (parseInt(iframe.style.height) - (h_cur - h_new)) + "px";
			return;
		}
		// となりが高くなってしまう場合は、となりにあわせる
		iframe.style.height = (parseInt(iframe.style.height) - (tonari - h_new)) + "px";
	}
	// 左右子画面それぞれのサイズを元に戻す。
	// 左右子画面それぞれから呼ばれる。
	// 戻した後は、どちらか高いほうにあわせて自分の高さを調整する。
	function resetIframeHeight(hidari_or_migi){
		CurrentHeight[hidari_or_migi] = DefaultHeight[hidari_or_migi];
		var iframe = document.getElementById(hidari_or_migi);
		iframe.style.height = DefaultHeight[hidari_or_migi] + "px";
		if (!window.parent) return;
		iframe = window.parent.document.getElementById("floatingpage");
		if (!iframe) return;

		var bigger = Math.max(Math.max(DefaultHeight.hidari, DefaultHeight.migi), Math.max(CurrentHeight.hidari, CurrentHeight.migi));
		iframe.style.height = (StartMargin*1 + bigger*1) + "px";
	}
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="checkStartHeightMargin();">

<? $pager_param_addition = "&page=".@$page."&key1=".@$key1."&key2=".@$key2; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "レポート", "", $pt_id); ?>

<? summary_common_show_report_tab_menu($con, $fname, "レポート", $is_workflow_exist, $pt_id, $pager_param_addition); ?>

<? //--------------- ?>
<? // 左右のフレーム ?>
<? //--------------- ?>
<table width="100%" border="0" cellspacing="0" style="Z-INDEX:90; background-color:#f4f4f4">
	<tr>
		<td width="52%" style="vertical-align:top; text-align:left"><iframe style="Z-INDEX:100; border:0; margin:0;" id="head" name="head" scrolling="no" frameborder="0" width="100%" height="56" src="<?echo("./summary_kanja_joho.php");?>?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$ekey2); ?>"></iframe>
      <input type="hidden" style="width:100%;" id="itm_ord" value="">
			<iframe style="Z-INDEX:100; border:0; margin:0;" id="hidari" name="hidari" scrolling="no" frameborder="0" width="100%" height="550" src="<?echo("./summary_rireki_table.php");?>?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$ekey2); ?>"></iframe>
		</td>
    <td bgcolor="#F4F4F4" class="bg-pepo-line" style="text-align:center">&nbsp;</td>
		<td width="47%" style="vertical-align:top; text-align:right">
			<iframe style="Z-INDEX:100;" id="migi" name="migi" frameborder="0" width="100%" height="300" src="<?echo("./summary_new.php");?>?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$ekey2); ?>"></iframe>
		</td>
	</tr>
</table>


</body>
<? pg_close($con); ?>
</html>

<?
function get_age($birth) {

	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}

	return $age;

}
?>
