<?
$fname = $PHP_SELF;
ob_start();
require_once("about_comedix.php");
require_once("show_fplusadm_lib_list.ini");
require_once("fplusadm_common.ini");
require_once("summary_tmplitemrireki.ini"); // 兼用
ob_end_clean();

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$checkauth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$checkauth || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}



if($reg_flg == "reg")
{
	//登録処理
	
	
	if($only_approve == NULL)
	{
		$up_only_approve="false";
	}
	else
	{
		$up_only_approve="true";
	}
	
	if($search_approve_date_save == NULL)
	{
		$up_search_approve_date_save="false";
	}
	else
	{
		$up_search_approve_date_save="true";
	}
	
	$sql = "update fplus_etc_setting set";
	$set = array("only_approver","search_approve_date_save");
	$setvalue = array(
			$up_only_approve,$up_search_approve_date_save
			);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}



//==============================
//報告書承認者設定を取得
//==============================
$item_list = "";
$sql = "select * from fplus_etc_setting";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_list = pg_fetch_all($sel);


//兼務承認者のチェック
if($item_list[0]["only_approver"] == "t")
{
	$ck_only_approve = "checked";
}
else
{
	$ck_only_approve = "";
}

//検索日時保存のチェック
if($item_list[0]["search_approve_date_save"] == "t")
{
	$ck_search_approve_date_save = "checked";
}
else
{
	$ck_search_approve_date_save = "";
}

?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | テンプレート登録</title>

<script language="JavaScript" type="text/JavaScript">
<!--
function registReport()
{

	ret_mes = confirm("表示状態で登録しますが、よろしいですか？");

	if(ret_mes == false)
	{
		return false;
	}

	document.regReport.reg_flg.value = "reg";
	document.regReport.submit();
}
</script>

<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
				</tr>
			</table>

	<form name="regReport" action="fplusadm_etc_setting.php" method="post" style="margin-top:4px">


			<table style="width:100%; margin-top:4px">
				<tr>
					<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
					<td style="vertical-align:top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
							<tr height="22"><? show_wkfw_menuitem($session, $fname, ""); ?></tr>
						</table>

						<table width="580" border="0" cellspacing="0" cellpadding="2" class="list" style="margin-top:4px">
							<tr>
								<td valign="top">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="only_approve[]" id="only_approve[]" value="01" <?=$ck_only_approve?> ><label for="only_approve[]">報告先に兼務先所属を使用しない（兼務を受付者（承認者）としてのみ使用する）</label></font>
								</td>
							</tr>
							<tr>
								<td valign="top">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="search_approve_date_save[]" id="search_approve_date_save[]" value="01" <?=$ck_search_approve_date_save?> ><label for="search_approve_date_save[]">受付一覧の検索条件「報告日」の日付を保存する（検索ボタン押下時に保存）</label></font>
								</td>
							</tr>
							<tr bgcolor="#feeae9">
								<td align="right">
									<input type="button" value="登録" onclick="registReport();">
									<input type="hidden" name="session" value="<? echo($session); ?>">
									<input type="hidden" name="reg_flg" id="reg_flg" value="">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			
	</form>
			
		</td>
	</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
