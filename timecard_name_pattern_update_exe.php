<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
//foreach ($pattern_names as $i => $pattern_name) {
//	$pattern_id = $i + 1;
for ($i = 0; $i < $pattern_count; $i++) {
    $var = "pattern_names_" . $i;
    $pattern_name = $$var;
    $var = "atdptn_id_" . $i;
    $pattern_id = $$var;

    if ($pattern_name == "") {
        echo("<script type=\"text/javascript\">alert('出勤パターン名{$pattern_id}を入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }

    if (mb_strlen($pattern_name, 'EUC-JP') > 10) {
        echo("<script type=\"text/javascript\">alert('出勤パターン名{$pattern_id}が長すぎます。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

//勤務日数換算情報を取得
$sql = "select atdptn_id, workday_count,previous_day_possible_flag,reason,allowance,previous_day_flag,meeting_start_time,meeting_end_time,assist_time_flag,assist_start_time,assist_end_time,assist_group_id,base_time,over_24hour_flag, auto_clock_in_flag, auto_clock_out_flag, out_time_nosubtract_flag, after_night_duty_flag, duty_connection_flag, empcond_officehours_flag, no_count_flag, previous_day_threshold_hour from atdptn";
$cond = "where group_id = $group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$arr_atdptn_info = array();
while ($row = pg_fetch_array($sel)) {
    $arr_atdptn_info[] = array("atdptn_id" => $row["atdptn_id"], "workday_count" => $row["workday_count"], "row_data" => $row);
}

// 出勤パターン名をDELETE〜INSERT
$sql = "delete from atdptn";
$cond = "where group_id = $group_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
//foreach ($pattern_names as $i => $pattern_name) {
for ($i = 0; $i < $pattern_count; $i++) {
    $var = "pattern_names_" . $i;
    $pattern_name = $$var;
    $var = "atdptn_id_" . $i;
    $pattern_id = $$var;
    $var = "order_no_" . $i;
    $order_no = $$var;
    $var = "display_flag_" . $i;
    $display_flag = ($$var == "f") ? "f" : "t";

    $workday_count = "";
    foreach ($arr_atdptn_info as $atdptn_info) {
        $atdptn_id = $atdptn_info["atdptn_id"];
        //if($atdptn_id == ($i+1))
        if ($atdptn_id == $pattern_id) {
            $workday_count = $atdptn_info["workday_count"];

            $row_data = $atdptn_info["row_data"];

            $previous_day_possible_flag = $row_data["previous_day_possible_flag"];
            $reason = $row_data["reason"];
            $allowance = $row_data["allowance"];
            $previous_day_flag = $row_data["previous_day_flag"];
            $meeting_start_time = $row_data["meeting_start_time"];
            $meeting_end_time = $row_data["meeting_end_time"];
            $assist_time_flag = $row_data["assist_time_flag"];
            $assist_start_time = $row_data["assist_start_time"];
            $assist_end_time = $row_data["assist_end_time"];
            $assist_group_id = $row_data["assist_group_id"];
            $base_time = $row_data["base_time"];
            $over_24hour_flag = $row_data["over_24hour_flag"];
            $auto_clock_in_flag = $row_data["auto_clock_in_flag"];
            $auto_clock_out_flag = $row_data["auto_clock_out_flag"];
            $out_time_nosubtract_flag = $row_data["out_time_nosubtract_flag"];
            $after_night_duty_flag = $row_data["after_night_duty_flag"];
            $duty_connection_flag = $row_data["duty_connection_flag"];
            $empcond_officehours_flag = $row_data["empcond_officehours_flag"];
            $no_count_flag = $row_data["no_count_flag"];
            $previous_day_threshold_hour = $row_data["previous_day_threshold_hour"];
            break;
        }
    }
    if ($workday_count == "" || $workday_count == null) {
        $workday_count = 1;
    }
    $previous_day_possible_flag = ($previous_day_possible_flag == "") ? null : $previous_day_possible_flag;
    $reason = ($reason == "") ? null : $reason;
    $allowance = ($allowance == "") ? null : $allowance;
    $previous_day_flag = ($previous_day_flag == "") ? null : $previous_day_flag;
    $assist_time_flag = ($assist_time_flag == "") ? null : $assist_time_flag;
    $over_24hour_flag = ($over_24hour_flag == "") ? null : $over_24hour_flag;
    $auto_clock_in_flag = ($auto_clock_in_flag == "") ? null : $auto_clock_in_flag;
    $auto_clock_out_flag = ($auto_clock_out_flag == "") ? null : $auto_clock_out_flag;
    $out_time_nosubtract_flag = ($out_time_nosubtract_flag == "") ? null : $out_time_nosubtract_flag;
    $after_night_duty_flag = ($after_night_duty_flag == "") ? null : $after_night_duty_flag;
    $duty_connection_flag = ($duty_connection_flag == "") ? null : $duty_connection_flag;
    $empcond_officehours_flag = ($empcond_officehours_flag == "") ? null : $empcond_officehours_flag;
    $no_count_flag = ($no_count_flag == "") ? null : $no_count_flag;

    $sql = "insert into atdptn (atdptn_id, atdptn_nm, group_id, workday_count, previous_day_possible_flag,reason,allowance,previous_day_flag,meeting_start_time,meeting_end_time,assist_time_flag,assist_start_time,assist_end_time,assist_group_id,base_time,over_24hour_flag, atdptn_order_no, display_flag, auto_clock_in_flag, auto_clock_out_flag, out_time_nosubtract_flag, after_night_duty_flag, duty_connection_flag, empcond_officehours_flag, no_count_flag, previous_day_threshold_hour) values (";
    $content = array($pattern_id, pg_escape_string($pattern_name), $group_id, $workday_count, $previous_day_possible_flag, $reason, $allowance, $previous_day_flag, $meeting_start_time, $meeting_end_time, $assist_time_flag, $assist_start_time, $assist_end_time, $assist_group_id, $base_time, $over_24hour_flag, $order_no, $display_flag, $auto_clock_in_flag, $auto_clock_out_flag, $out_time_nosubtract_flag, $after_night_duty_flag, $duty_connection_flag, $empcond_officehours_flag, $no_count_flag, $previous_day_threshold_hour);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}


//atdptnから削除されたパターンはduty_shift_patternからも削除する
$sql = "delete from duty_shift_pattern";
$cond = "where not exists(SELECT * FROM atdptn WHERE atdptn.atdptn_id = duty_shift_pattern.atdptn_ptn_id AND atdptn.group_id = '$group_id') AND duty_shift_pattern.pattern_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

//atdptnに追加されたパターンをduty_shift_patternにも追加
$sql = "select * from atdptn";
$cond = "where atdptn_id <> 10 and not exists(SELECT * FROM duty_shift_pattern WHERE atdptn.atdptn_id = duty_shift_pattern.atdptn_ptn_id AND duty_shift_pattern.pattern_id = '$group_id') AND atdptn.group_id = '$group_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $arr_atdptn_info[] = array("atdptn_id" => $row["atdptn_id"], "workday_count" => $row["workday_count"]);

	$atdptn_nm = $row["atdptn_nm"];
	//看護日誌出力区分
	if (mb_strpos($atdptn_nm, "深夜") !== false) {
		$output_kbn = "0";
	}
	elseif (mb_strpos($atdptn_nm, "準夜") !== false) {
		$output_kbn = "2";
	}
	elseif (mb_strpos($atdptn_nm, "明け") !== false) {
		$output_kbn = "9";
	}
	elseif (mb_strpos($atdptn_nm, "夜") !== false) {
		$output_kbn = "0";
	}
	else {
		$output_kbn = "1";
	}

    $sql = "insert into duty_shift_pattern (pattern_id, atdptn_ptn_id, reason, count_kbn_gyo, count_kbn_retu, font_color_id, back_color_id, output_kbn) values (";
    $content = array($row["group_id"], $row["atdptn_id"], "", 9999, 9999, "#000000", "#ffffff", $output_kbn);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}


// 休暇事由なしのデータ reasonが「半角スペース」のデータに対して、「34」に更新する
$sql = "update duty_shift_pattern set";
$set = array("reason");
$setvalue = array("34");
$cond = "where atdptn_ptn_id = 10 AND reason = ' '";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

//初回のみ休暇+事由のデータを登録する
$arr_reason = array("34", "24", "22", "23", "1", "37", "4", "17", "5", "6", "7", "40", "41", "8"); // なし(34),公休(24),法定(22),所定(23),有給(1),年休(37),代替(4),振替(17),特別(5),欠勤(6),病欠(7),リフレッシュ休暇(40),初盆休暇(41),その他(8)
foreach ($arr_reason as $reason) {
    $sql = "select count(*) as cnt from duty_shift_pattern";
    $cond = "where pattern_id = $group_id AND atdptn_ptn_id = 10 AND reason = '$reason'";

    $sel_reason = select_from_table($con, $sql, $cond, $fname);
    if ($sel_reason == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $cnt = pg_fetch_result($sel_reason, 0, "cnt");

    // データがなければ登録
    if ($cnt == 0) {
        $sql = "insert into duty_shift_pattern (pattern_id, atdptn_ptn_id, reason, count_kbn_gyo, count_kbn_retu, font_color_id, back_color_id) values (";
        $content = array($group_id, 10, $reason, 9999, 9999, "#000000", "#ffffff");
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}


// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 出勤パターン名画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_name_pattern.php?session=$session&group_id=$group_id';</script>");
?>
