<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("label_by_profile_type.ini");
require_once("show_sinryo_top_header.ini");
require_once("summary_common.ini");
require_once("about_comedix.php");


//==============================
//ヘッダー処理
//==============================

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 診療記録使用権限のチェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
if($con == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療記録区分/記録区分
$summary_record_kubun_title = $_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];






//==============================
//ポストバック時
//==============================
if(@$is_postback == "true")
{
  if (count($delete_id) == 0) {
    echo("<script type=\"text/javascript\">alert('チェックボックスをオンにしてください。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
  }

  //==============================
  //削除処理
  //==============================

  //削除パラメータ作成
  $delete_target_list = "";
  foreach($delete_id as $fixedform_id)
  {
    if($delete_target_list != "")
    {
      $delete_target_list .= ",";
    }
    $delete_target_list .= "'".pg_escape_string($fixedform_id)."'";
  }

  // トランザクションの開始
  pg_query($con, "begin");

  //テーブル削除
  $sql = "delete from fixedform where fixedform_id in($delete_target_list)";
  $del = delete_from_table($con, $sql, "", $fname);
  if ($del == 0)
  {
    pg_query($con,"rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
  }

  // トランザクションのコミット
  pg_query($con, "commit");
}


//==============================
//一覧を取得
//==============================
$sql = "select a.fixedform_id, a.fixedform_sentence from fixedform a ";
$cond = " where (a.emp_id is null or a.emp_id = '')";
$cond .= " order by a.fixedform_id";

//SQL実行
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
  pg_close($con);
  echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
  echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
  exit;
}
$data_list = pg_fetch_all($sel);
if (!is_array($data_list)) $data_list = array();

?>
<title>CoMedix マスターメンテナンス | マスタ管理</title>
<script language="JavaScript" type="text/JavaScript">
function reload_page()
{
  location.href = "summary_fixedform.php?session=<?=$session?>";
}

function add_fixedform()
{
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=500";
  window.open('summary_fixedform_regist.php?session=<?=$session?>&kind_flg=2', 'newwin', option);
}
function update_fixedform(fixedform_id)
{
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=500";
  window.open('summary_fixedform_update.php?session=<?=$session?>&kind_flg=2&fixedform_id='+fixedform_id, 'newwin', option);
}
function delete_fixedform()
{
  if (!confirm('選択された定型文を削除します。よろしいですか？'))
  {
    return;
  }
  document.mainform.submit();
}


</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list2 {border-collapse:collapse;}
.list2 td {border:#5279a5 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_MST"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr height="22">
  <? summary_common_show_mst_tab_menu($session, '定型文（共用）'); ?>
  <td align="right" nowrap>
  <input type="button" value="追加" onclick="add_fixedform()">
  <input type="button" value="削除" onclick="delete_fixedform()">
  </td>

  </tr>
</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<!-- theme list start -->



<form name="mainform" action="summary_fixedform.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="is_postback" value="true">


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list2_in2">
<tr>
<td bgcolor="#f6f9ff" width="30">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp</font>
</td>
<td bgcolor="#f6f9ff" width="*">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font>
</td>
</tr>
<?
  foreach($data_list as $data_list1)
  {
    $fixedform_id = $data_list1['fixedform_id'];
    $fixedform_sentence = $data_list1['fixedform_sentence'];
?>
<tr>
<td bgcolor="#ffffff" align="center">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <input type="checkbox" name="delete_id[]" value="<?=$fixedform_id?>">
   </font>
</td>
<td bgcolor="#ffffff">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <a href="javascript:update_fixedform('<?=$fixedform_id?>')">
   <?=h($fixedform_sentence) ?>
   </a>
   </font>
</td>
</tr>
<?
  }
?>
</table>




</td>
</tr>
</table>
</form>
<!-- comment view end -->
</td>
</tr>
</table>




</td>
<!-- right -->
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
