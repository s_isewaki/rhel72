<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// 入力チェック
if ($group_name == "") {
	js_alert_exit("メール送信グループ名が入力されていません。");
}

$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// メール送信グループ名を登録
$sql = "insert into adbkgrp (emp_id, name) values (";
$content = array($emp_id, $group_name);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// IDを取得
$sql = "select max(id) from adbkgrp";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$group_id = pg_fetch_result($sel, 0, 0);

// トランザクションをコミット
pg_query($con, "commit");

pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = 'address_group.php?session=$session&group_id=$group_id';</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
