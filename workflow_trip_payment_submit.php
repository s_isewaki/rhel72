<?php
// 出張旅費
// 支給基準マスタ・更新
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// 入力項目の確認
if (empty($_REQUEST["payment_date"])) {
	js_alert_exit('「施行日」が設定されていません。');
}
foreach ($_REQUEST["st_id"] as $id) {
	if (!empty($_REQUEST["hotel"][$id]) and !is_numeric($_REQUEST["hotel"][$id])) {
		js_alert_exit('宿泊料(一般)に数字以外が入力されています。');
	}
	if (!empty($_REQUEST["hotel_tokyo"][$id]) and !is_numeric($_REQUEST["hotel_tokyo"][$id])) {
		js_alert_exit('宿泊料(東京23区)に数字以外が入力されています。');
	}
	if (!empty($_REQUEST["stay"][$id]) and !is_numeric($_REQUEST["stay"][$id])) {
		js_alert_exit('宿泊日当に数字以外が入力されています。');
	}
	if (!empty($_REQUEST["daytrip"][$id]) and !is_numeric($_REQUEST["daytrip"][$id])) {
		js_alert_exit('日帰日当に数字以外が入力されています。');
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

foreach ($_REQUEST["st_id"] as $id) {
	// 登録されているかチェック
	$sql = "SELECT * FROM trip_payment WHERE st_id={$id} AND type={$_REQUEST["type"]}";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		js_error_exit();
	}

	// UPDATE	
	if (pg_num_rows($sel) > 0) {
		$set = array(
				"hotel" => !empty($_REQUEST["hotel"][$id]) ? p($_REQUEST["hotel"][$id]) : 0,
				"hotel_tokyo" => !empty($_REQUEST["hotel_tokyo"][$id]) ? p($_REQUEST["hotel_tokyo"][$id]) : 0,
				"stay" => !empty($_REQUEST["stay"][$id]) ? p($_REQUEST["stay"][$id]) : 0,
				"daytrip" => !empty($_REQUEST["daytrip"][$id]) ? p($_REQUEST["daytrip"][$id]) : 0,
				"view_flg" => !empty($_REQUEST["view"][$id]) ? p($_REQUEST["view"][$id]) : 'f',
				"updated_on" => date('Y-m-d H:i:s')
				);
		$sql = "UPDATE trip_payment SET ";
		$cond = "WHERE st_id={$id} AND type={$_REQUEST["type"]}";
		$upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	}

	// INSERT
	else {
		$sql = "INSERT INTO trip_payment (st_id,hotel,hotel_tokyo,stay,daytrip,view_flg,created_on,updated_on,type) values (";
		$content = array(
				$id,
				!empty($_REQUEST["hotel"][$id]) ? p($_REQUEST["hotel"][$id]) : 0,
				!empty($_REQUEST["hotel_tokyo"][$id]) ? p($_REQUEST["hotel_tokyo"][$id]) : 0,
				!empty($_REQUEST["stay"][$id]) ? p($_REQUEST["stay"][$id]) : 0,
				!empty($_REQUEST["daytrip"][$id]) ? p($_REQUEST["daytrip"][$id]) : 0,
				!empty($_REQUEST["view"][$id]) ? p($_REQUEST["view"][$id]) : 'f',
				date('Y-m-d H:i:s'),
				date('Y-m-d H:i:s'),
				$_REQUEST["type"]
				);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	}
}

// 日付
if ($_REQUEST["type"] == 1) {
	$payment_date = 'payment1_date';
}
else {
	$payment_date = 'payment2_date';
}
$set = array($payment_date => p($_REQUEST["payment_date"]));
$sql = "UPDATE trip_option SET ";
$upd = update_set_table($con, $sql, array_keys($set), array_values($set), "", $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

?>
<script type="text/javascript">
location.href = 'workflow_trip_payment.php?session=<?=$session?>&type=<?eh($_REQUEST["type"])?>';
</script>