<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("jnl_application_common.ini");
require_once("jnl_application_workflow_select_box.ini");
require_once("jnl_application_workflow_common_class.php");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new jnl_application_workflow_common_class($con, $fname);

//一画面内の最大表示件数
$disp_max_page = 15;

if($page == "")
{
	$page = 1;
}

$arr_condition = array(
                        "session" => $session,
                        "apply_emp_nm" => $search_apply_emp_nm,
                        "apply_stat" => $search_apply_stat,
                        "class" => $search_emp_class,
                        "attribute" => $search_emp_attribute,
                        "dept" => $search_emp_dept,
                        "room" => $search_emp_room,
						"page" => $page,
						"max_page" => $disp_max_page
                       );

$applynotice_list_count = $obj->get_applynotice_list_count($arr_condition);

$arr_applynotice = $obj->get_applynotice_list($arr_condition);


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>日報・月報 | 申請結果通知</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function search_notice()
{
	document.apply.action="jnl_application_notice_list.php?session=<?=$session?>";
	document.apply.submit();
}

function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

function show_sub_window(url) {
	var h = '700';
	var w = '780';
	var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
	window.open(url, 'approvewin',option);
}

// この関数は別画面から呼び出されます。
function reload_page()
{
	document.apply.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="日報・月報"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_notice_list.php?session=<? echo($session); ?>&mode=search"><b>申請結果通知</b></a></font></td>
					<? if ($workflow_auth == "1") { ?>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
					<? } ?>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
			<img src="img/spacer.gif" alt="" width="1" height="2"><br>

			<form name="apply" action="#" method="post">
				<img src="img/spacer.gif" alt="" width="1" height="2"><br>
				<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
					<tr>
						<td>

							<table width="100%" border="0" cellspacing="1" cellpadding="2" class="list">
								<tr>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者名</font></td>
									<td><input type="text" size="40" maxlength="40" name="search_apply_emp_nm" value="<?=htmlspecialchars($search_apply_emp_nm)?>"></td>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請状況</font></td>
									<td><select name="search_apply_stat"><? show_apply_stat_options($search_apply_stat); ?></select></td>
								</tr>
								<tr>
									<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
									<td colspan="3">
										<?
										show_post_select_box($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room);
										?>

									</td>
								</tr>
							</table>

							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
								<tr align="right">
									<td>
										<input type="button" value="検索" onclick="search_notice();">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

				<img src="img/spacer.gif" alt="" width="1" height="5"><br>
				<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
					<tr bgcolor="#f6f9ff">
						<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号<br>(管理ＣＤ-連番)</font></td>
						<td width="200" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
						<td width="200" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font></td>
						<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
						<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者名</font></td>
						<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
						<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請<BR>状況</font></td>
					</tr>

<?
if($applynotice_list_count > 0)
{
	$count = "1";
	foreach($arr_applynotice as $applynotice)
	{
		$apply_id = $applynotice["apply_id"];

		// 申請番号
		$apply_date = $applynotice["apply_date"];
		$short_wkfw_name = $applynotice["short_wkfw_name"];
		$apply_no        = $applynotice["apply_no"];
		$year = substr($apply_date, 0, 4);
		$md   = substr($apply_date, 4, 4);
		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}
		$apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

		// 申請者名
		$apply_lt_nm = $applynotice["apply_lt_nm"];
		$apply_ft_nm = $applynotice["apply_ft_nm"];
		$apply_full_nm = $apply_lt_nm." ".$apply_ft_nm;

		// 申請日
		$apply_ymd = preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $apply_date);

		// 申請状況
		$apply_stat = $applynotice["apply_stat"];

		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";

		$apply_stat_nm = "";
		if($apply_stat == "1")
		{
			$apply_stat_nm = $approve_label . "確定";
		}
		else if($apply_stat == "2")
		{
			$apply_stat_nm = "否認";
		}
		else if($apply_stat == "3")
		{
			$apply_stat_nm = "差戻し";
		}

		// カテゴリ名(フォルダパス)取得
		$folder_path = $applynotice["wkfw_nm"];
		if($wkfw_folder_id != "")
		{
			// フォルダ名
			$folder_list = $obj->get_folder_path($applynotice["wkfw_folder_id"]);
			foreach($folder_list as $folder)
			{
				if($folder_path != "")
				{
					$folder_path .= " > ";
				}
				$folder_path .= $folder["name"];
			}
		}

		$confirmed_flg = $applynotice["confirmed_flg"];
		$confirmed_color = "";
		if($confirmed_flg == "f")
		{
			$confirmed_color = "bgcolor=\"#ffcccc\"";
		}

		$onclick_event = "onclick=\"show_sub_window('jnl_application_workflow_detail.php?session=$session&apply_id=$apply_id&screen=NOTICE');\"";
?>
					<tr <?=$confirmed_color?>>
						<td width="100" class="apply_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_no?></font></td>
						<td width="200" class="apply_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($folder_path)?></font></td>
						<td width="200" class="apply_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($applynotice["wkfw_title"])?></font></td>
						<td width="*" class="apply_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($applynotice["apply_title"])?></font></td>
						<td width="100" align="center" class="apply_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($apply_full_nm)?></font></td>
						<td width="100" align="center" class="apply_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_ymd?></font></td>
						<td width="60" align="center" class="apply_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_stat_nm?></font></td>
					</tr>
<?
	$count++;
	}
}
?>
				</table>
<?
//最大ページ数
if($applynotice_list_count == 0)
{
	$page_max  = 1;
}
else
{
	$page_max  = floor( ($applynotice_list_count-1) / $disp_max_page ) + 1;
}

show_page_area($page_max,$page);


?>
			</form>
		</td>
	</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</body>
</html>

<?
// 申請状況オプションを出力
function show_apply_stat_options($stat) {

	$arr_apply_stat_nm = array("承認確定","否認","差戻し");
	$arr_apply_stat_id = array("1","2","3");

	echo("<option value=\"-\">すべて");
	for($i=0;$i<count($arr_apply_stat_nm);$i++) {

		echo("<option value=\"$arr_apply_stat_id[$i]\"");
		if($stat == $arr_apply_stat_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_apply_stat_nm[$i]\n");
	}
}

function show_page_area($page_max,$page)
{
	?>
	<input type="hidden" name="page" value="">
	<script type="text/javascript">

	//ページ遷移します。
	function page_change(page)
	{
		document.apply.page.value=page;
		document.apply.submit();
	}

	</script>

	<?
	if($page_max != 1)
	{
	?>
		<table width="100%">
			<tr>
				<td>
					<table>
						<tr>
							<td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    先頭へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(1);">
		    先頭へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    前へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page-1?>);">
		    前へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		<?

		for($i=1;$i<=$page_max;$i++)
		{
			if($i == $page)
			{
		?>
		    [<?=$i?>]
		<?
			}
			else
			{
		?>
		    <a href="javascript:page_change(<?=$i?>);">[<?=$i?>]</a>
		<?
			}
		}
		?>
		    </font>
		  </td>
		  <td>
		<?


		if($page == $page_max)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    次へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page+1?>);">
		    次へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  </tr>
		  </table>
		</td>
		</tr>
		</table>
	<?
	}
	?>
	<?
}

pg_close($con);
?>
