<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $PHP_SELF;

require_once("Cmx.php");
require_once("aclg_set.php");
require("about_postgres.php");
require("schedule_repeat_common.php");

// 処理対象テーブルの設定
if ($timeless != "t") {$timeless = "f";}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

// トランザクションを開始
pg_query($con, "begin");

// 削除対象スケジュール一覧を取得
$del_schedules = get_repeated_project_schedules($con, $pjt_schd_id, $timeless, $rptopt, $fname);

// 削除を実行
foreach ($del_schedules as $tmp_schedule) {
	$tmp_table_name = ($tmp_schedule["timeless"] == "f") ? "proschd" : "proschd2";
	$tmp_pjt_schd_id = $tmp_schedule["id"];

	// スケジュール情報を削除
	$sql = "delete from $tmp_table_name";
	$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 議事録情報を削除
	$sql = "delete from proceeding";
	$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 添付ファイル情報を削除
	$sql = "delete from prcdfile";
	$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 承認情報を削除
	$sql = "delete from prcdaprv";
	$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 出席者情報を削除
	$sql = "delete from prcdatnd";
	$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 議事録添付ファイルがあれば削除
if (!is_dir("proceeding")) {
	mkdir("proceeding", 0755);
}
foreach ($del_schedules as $tmp_schedule) {
	$tmp_pjt_schd_id = $tmp_schedule["id"];
	foreach (glob("proceeding/{$tmp_pjt_schd_id}_*.*") as $tmpfile) {
		unlink($tmpfile);
	}
}

// 親画面をリフレッシュして自画面を閉じる
echo("<script type=\"text/javascript\">window.opener.location.reload();</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
?>
