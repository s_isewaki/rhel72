<?php
require_once("cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

//---------------------------------------------------
// クリオニカルラダー申請共通
//---------------------------------------------------

// テンプレートパラメータ前処理
function template_parameter_pre_proccess($post_param){

	global $log;
	// テンプレートの項目のみをパラメータに設定
	define("TEMPLATE_PREFIX","clp_");
	$arr_temp_value = array();
	$arr_field_name = array_keys($post_param);
	foreach($arr_field_name as $field_name){
		$pos = strpos($field_name,TEMPLATE_PREFIX);
		if ($pos === 0) {
			$param_name = substr($field_name,strlen(TEMPLATE_PREFIX));
			$arr_temp_value[$param_name] = $post_param[$field_name];
			$log->debug("テンプレート項目：".$param_name."　値：".$post_param[$field_name],__FILE__,__LINE__);
		}
	}
	return $arr_temp_value;
}

// JSONデータ作成
function create_json($array){
	global $log;

	$log->info(__FUNCTION__." START",__FILE__.__LINE__);

	/*
	 * JSON 開始中括弧
	 */
	$json="{";

	/*
	 * 配列要素毎処理
	 */
	foreach( $array as $key => $value ){

		/*
		 * 要素の値が、配列の場合
		 */
		if (is_array($value)) {
			/*
			 * 連想配列かどうか
			 */
			if( is_hash($value) ){

				$log->debug("連想配列です。",__FILE__.__LINE__);

				/*
				 * 「create_json()」を再帰呼び出し
				 */
				$value = create_json($value);
				$json=$json."\"$key\":[$value],";
			}
			else{
				$log->debug("連想配列ではありません。",__FILE__.__LINE__);
				/*
				 * 「create_json()」を再帰呼び出し
				 */
				$value = create_json_no_hash($value);
				$json=$json."\"$key\":[$value],";
			}
		}
		/*
		 * 要素の値が、配列でない場合
		 */
		else {
			$json=$json."\"$key\":\"".htmlspecialchars($value)."\",";
		}

	}
	/*
	 * 配列要素毎処理で末尾に付加された「,」を削除する。
	 */
	$json=rtrim($json,",");

	/*
	 * JSON 閉じ中括弧
	 */
	$json=$json."}";

	$log->info(__FUNCTION__." END",__FILE__.__LINE__);

	return $json;
}

// JSONデータ作成
function create_json_no_hash($array){
	global $log;

	$log->info(__FUNCTION__." START",__FILE__.__LINE__);

	/*
	 * JSON 開始中括弧
	 */
	$json="";

	for($i=0;$i<count($array);$i++){

		$value=$array[$i];

		/*
		 * 要素の値が、配列の場合
		 */
		if (is_array($value)) {
			/*
			 * 連想配列かどうか
			 */
			if( is_hash($value) ){

				$log->debug("連想配列です。",__FILE__.__LINE__);

				/*
				 * 「create_json()」を再帰呼び出し
				 */
				$value = create_json($value);
				$json=$json.$value.",";
			}
			else{
				$log->debug("連想配列ではありません。",__FILE__.__LINE__);
			}
		}
		/*
		 * 要素の値が、配列でない場合
		 */
		else {
			$json=$json.$value.",";
		}


	}

	/*
	 * 配列要素毎処理で末尾に付加された「,」を削除する。
	 */
	$json=rtrim($json,",");

	/*
	 * JSON 閉じ中括弧
	 */
	//$json=$json."}";

	$log->info(__FUNCTION__." END",__FILE__.__LINE__);

	return $json;
}

function is_hash(&$array) {
	reset($array);
	list($k) = each($array);
	return $k !== 0;
}

// JSONデータ作成
function create_json_old($array){

	global $log;
	$json="{";
	foreach( $array as $key => $value ){
		$json=$json."\"$key\":\"$value\",";
	}
	$json=rtrim($json,",");
	$json=$json."}";
	return $json;
}

/**
 * 年オプションHTML取得
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

$log->info(basename(__FILE__)." END");
