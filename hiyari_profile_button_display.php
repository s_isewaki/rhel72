<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ( $auth_chk == '0' ) { 
	showLoginPage();
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if ( $con == "0" ) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 更新処理
if ( $mode == 'update' ) {
	
	// トランザクションの開始
	pg_query($con, "begin transaction");
	
	// 値を設定
	set_inci_profile_button_display($_POST);
	
	// トランザクションをコミット
	pg_query($con, "commit");
	
}

//担当者ごとのボタン表示設定を取得
$flags = get_inci_profile_button_display($con, $fname);

//==============================
//HTML出力
//==============================
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix <?=$INCIDENT_TITLE?> | 個人設定</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript">
	<!--
	function update() {

		mainform.mode.value = "update";
		document.mainform.submit();

	}
	// -->
	</script>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
        #flag_list{
            width: 800px;
            background-color: white;
        }
        #flag_list, #flag_list th, #flag_list td{
            border-collapse: collapse;
            border: #35B341 solid 1px;
            font-weight: normal;
        }
        #flag_list th, #flag_list td{
            padding: 3px;
        }
        #flag_list td.flag{
            text-align: center;
        }
	</style>
	</head>
	<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
		<form name="mainform" action="hiyari_profile_button_display.php" method="post">
			
			<input type="hidden" name="session" value="<?=$session?>">
			<input type="hidden" name="mode" value="">
			<input type="hidden" name="class_flg" value="<?=$arr_inci_profile["class_flg"]?>">
			<input type="hidden" name="attribute_flg" value="<?=$arr_inci_profile["attribute_flg"]?>">
			<input type="hidden" name="dept_flg" value="<?=$arr_inci_profile["dept_flg"]?>">
			<input type="hidden" name="room_flg" value="<?=$arr_inci_profile["room_flg"]?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<!-- ヘッダー -->
						<?php show_hiyari_header($session,$fname,true); ?>
						<!-- ヘッダー -->

						<img src="img/spacer.gif" width="1" height="5" alt=""><br>

						<table width="500" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
								<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
								<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
							</tr>
							<tr>
								<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
								<td bgcolor="#F5FFE5">
									<!-- ここから -->
                                    <table id="flag_list">
                                        <thead>                                            
                                            <th bgcolor="#dfffdc">&nbsp;</th>
                                            <th bgcolor="#dfffdc">
                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    本人プロフィールからコピー
                                                </font>
                                            </th>
                                            <th bgcolor="#dfffdc">
                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    他の職員プロフィールからコピー
                                                </font>
                                            </th>
                                            <th bgcolor="#dfffdc">
                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    プロフィール不明
                                                </font>
                                            </th>
                                        </thead>
                                        <tbody>                                        
                                            <?php foreach ($flags as $auth_code => $auth){ ?>
                                                <tr>
                                                    <td>
                                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                            <?php echo $auth['auth_name']; ?>
                                                        </font>
                                                    </td>
                                                    <?php foreach ($auth['btns'] as $btn_code => $btn){ ?>
                                                        <td class="flag">
                                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                                <input type="checkbox" name="<?php echo($btn_code . '_' . $auth_code); ?>" value="t" <?php echo $btn == 't' ? 'checked':''; ?>>
                                                            </font>
                                                        </td>
                                                    <?php } ?>    
                                                </tr>
                                            <?php } ?>
                                        </tbody>                                    
                                    </table>

									<img src="img/spacer.gif" width="1" height="5" alt=""><br>

									<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tr>
									<td align="right">
									<input type="button" value="更新" onclick="update();">
									</td>
									</tr>
									</table>
									<!-- ここまで -->
								</td>
								<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
							</tr>
							
							<tr>
								<td><img src="img/r_3.gif" width="10" height="10"></td>
								<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
								<td><img src="img/r_4.gif" width="10" height="10"></td>
							</tr>
						</table>
						<img src="img/spacer.gif" width="1" height="5" alt=""><br>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

<?php
//==============================
//DBコネクション終了
//==============================
pg_close($con);

?>