function checkTime(tmmd_ovtm_flg) {

    kinmu_total_min = 0;
    rest_total_min = 0;

    var ovtm_min = 0;
    var flg = 0;
    var rest_flg = 0;
    var rest_input_flg = 0;
    //休憩時間計算
    var wk_rest_min = 0;
    var ovtm_flg = 0;

    var s_h = 'start_hour';
    var s_m = 'start_min';

    var s_hh = document.getElementById(s_h).value;
    var s_mm = document.getElementById(s_m).value;

    if (s_hh == '--') {
        s_hh = '';
    }
    if (s_mm == '--') {
        s_mm = '';
    }

    if (s_hh != '') {
        var s_hh_int = parseInt(s_hh, 10);
        if (!s_hh.match(/^[0-9]+$/)
                || s_hh_int > 23) {
            alert('出勤時刻 の「時」は半角の0から23の範囲内で入力してください。');
            return 1;
        }
    }
    if (s_mm  != '') {
        var s_mm_int = parseInt(s_mm, 10);
        if (!s_mm.match(/^[0-9]+$/)
                || s_mm_int > 59) {
            alert('出勤時刻の「分」は半角の0から59の範囲内で入力してください。');
            return 1;
        }
    }
    if ((s_hh != '' && s_mm == '') || (s_hh == '' && s_mm != '')) {
        alert('出勤時刻の時と分の一方のみは登録できません。');
        return 1;
    }
    if (tmmd_ovtm_flg == 1) {
    //休憩時間
    if (rest_disp_flg == 't') {
	    var s_h = 'rest_start_hour';
	    var s_m = 'rest_start_min';
	    var e_h = 'rest_end_hour';
	    var e_m = 'rest_end_min';

	    var s_hh = document.getElementById(s_h).value;
	    var s_mm = document.getElementById(s_m).value;
	    var e_hh = document.getElementById(e_h).value;
	    var e_mm = document.getElementById(e_m).value;

	    if (s_hh == '--') {
	        s_hh = '';
	    }
	    if (s_mm == '--') {
	        s_mm = '';
	    }
	    if (e_hh == '--') {
	        e_hh = '';
	    }
	    if (e_mm == '--') {
	        e_mm = '';
	    }
	    if (s_hh != '') {
	        var s_hh_int = parseInt(s_hh, 10);
	        if (!s_hh.match(/^[0-9]+$/)
	                || s_hh_int > 23) {
	            alert('休憩開始時刻 の「時」は半角の0から23の範囲内で入力してください。');
	            return 1;
	        }
	    }
	    if (s_mm  != '') {
	        var s_mm_int = parseInt(s_mm, 10);
	        if (!s_mm.match(/^[0-9]+$/)
	                || s_mm_int > 59) {
	            alert('休憩開始時刻の「分」は半角の0から59の範囲内で入力してください。');
	            return 1;
	        }
	    }
	    if ((s_hh != '' && s_mm == '') || (s_hh == '' && s_mm != '')) {
	        alert('休憩開始時刻の時と分の一方のみは登録できません。');
	        return 1;
	    }
    	
	    if (e_hh  != '') {
	       var e_hh_int = parseInt(e_hh, 10);
	       if (!e_hh.match(/^[0-9]+$/)
	                || e_hh_int > 23) {
	            alert('休憩終了時刻の「時」は半角の0から23の範囲内で入力してください。');
	            return 1;
	        }
	    }
	    if (e_mm  != '') {
	        var e_mm_int = parseInt(e_mm, 10);
	        if (!e_mm.match(/^[0-9]+$/)
	                || e_mm_int > 59) {
	            alert('休憩終了時刻の「分」は半角の0から59の範囲内で入力してください。');
	            return 1;
	        }
	    }
	    if ((e_hh != '' && e_mm == '') || (e_hh == '' && e_mm != '')) {
	        alert('休憩終了時刻の時と分の一方のみは登録できません。');
	        return 1;
	    }
	    if (s_hh != '') {
	        rest_input_flg = 1;
	        //日またがり
            if (e_hh_int < s_hh_int) {
                e_hh_int += 24;
            }

            var rs_min = s_hh_int  * 60 + s_mm_int;    
            var re_min = e_hh_int  * 60 + e_mm_int;    
            //休憩計算
            wk_rest_min = re_min - rs_min;
	        
	        if (wk_rest_min < 0 || wk_rest_min > 8 * 60) {
	            alert('休憩時刻の開始と終了が正しくありません。');
	            return 1;
	        }
        }
    }
    }
    var e_h = 'end_hour';
    var e_m = 'end_min';
    var e_hh = document.getElementById(e_h).value;
    var e_mm = document.getElementById(e_m).value;
    if (e_hh == '--') {
        e_hh = '';
    }
    if (e_mm == '--') {
        e_mm = '';
    }
    if (e_hh  != '') {
       var e_hh_int = parseInt(e_hh, 10);
       if (!e_hh.match(/^[0-9]+$/)
                || e_hh_int > 23) {
            alert('退勤時刻の「時」は半角の0から23の範囲内で入力してください。');
            return 1;
        }
    }
    if (e_mm  != '') {
        var e_mm_int = parseInt(e_mm, 10);
        if (!e_mm.match(/^[0-9]+$/)
                || e_mm_int > 59) {
            alert('退勤時刻の「分」は半角の0から59の範囲内で入力してください。');
            return 1;
        }
    }
    if ((e_hh != '' && e_mm == '') || (e_hh == '' && e_mm != '')) {
        alert('退勤時刻の時と分の一方のみは登録できません。');
        return 1;
    }
    if (tmmd_ovtm_flg == 1 && document.getElementById('out_hour')) {
        //外出時刻
        var s_h = 'out_hour';
        var s_m = 'out_min';
        var e_h = 'ret_hour';
        var e_m = 'ret_min';

        var s_hh = document.getElementById(s_h).value;
        var s_mm = document.getElementById(s_m).value;
        var e_hh = document.getElementById(e_h).value;
        var e_mm = document.getElementById(e_m).value;

        if (s_hh == '--') {
            s_hh = '';
        }
        if (s_mm == '--') {
            s_mm = '';
        }
        if (e_hh == '--') {
            e_hh = '';
        }
        if (e_mm == '--') {
            e_mm = '';
        }
        if (s_hh != '') {
            var s_hh_int = parseInt(s_hh, 10);
            if (!s_hh.match(/^[0-9]+$/)
                    || s_hh_int > 23) {
                alert('外出時刻 の「時」は半角の0から23の範囲内で入力してください。');
                return 1;
            }
        }
        if (s_mm  != '') {
            var s_mm_int = parseInt(s_mm, 10);
            if (!s_mm.match(/^[0-9]+$/)
                    || s_mm_int > 59) {
                alert('外出時刻の「分」は半角の0から59の範囲内で入力してください。');
                return 1;
            }
        }
        if ((s_hh != '' && s_mm == '') || (s_hh == '' && s_mm != '')) {
            alert('外出時刻の時と分の一方のみは登録できません。');
            return 1;
        }
    	
        if (e_hh  != '') {
           var e_hh_int = parseInt(e_hh, 10);
           if (!e_hh.match(/^[0-9]+$/)
                    || e_hh_int > 23) {
                alert('戻り時刻の「時」は半角の0から23の範囲内で入力してください。');
                return 1;
            }
        }
        if (e_mm  != '') {
            var e_mm_int = parseInt(e_mm, 10);
            if (!e_mm.match(/^[0-9]+$/)
                    || e_mm_int > 59) {
                alert('戻り時刻の「分」は半角の0から59の範囲内で入力してください。');
                return 1;
            }
        }
        if ((e_hh != '' && e_mm == '') || (e_hh == '' && e_mm != '')) {
            alert('戻り時刻の時と分の一方のみは登録できません。');
            return 1;
        }
	    if (s_hh != '' && e_hh != '') {
	        //日またがり
            if (e_hh_int < s_hh_int) {
                e_hh_int += 24;
            }

            var out_min = s_hh_int  * 60 + s_mm_int;    
            var ret_min = e_hh_int  * 60 + e_mm_int;    
            //計算
            wk_out_min = ret_min - out_min;
	        
	        if (wk_out_min < 0 || wk_out_min > 8 * 60) {
	            alert('外出時刻と戻り時刻が正しくありません。');
	            return 1;
	        }
        }
    }

    if (tmmd_ovtm_flg == 2) {
        var s_h = 'over_start_hour';
        var s_m = 'over_start_min';
        var e_h = 'over_end_hour';
        var e_m = 'over_end_min';
        var s_hh = document.getElementById(s_h).value;
        var s_mm = document.getElementById(s_m).value;
        var e_hh = document.getElementById(e_h).value;
        var e_mm = document.getElementById(e_m).value;
        if (s_hh == '--') {
            s_hh = '';
        }
        if (s_mm == '--') {
            s_mm = '';
        }
        if (e_hh == '--') {
            e_hh = '';
        }
        if (e_mm == '--') {
            e_mm = '';
        }
        if (s_hh == '' && s_mm == '' && e_hh == '' && e_mm == '') {
            alert('残業申請の場合には、残業時刻1は必ず入力してください。');
            return 1;
        }
    }

    for (var i=1; i<=maxline; i++) {

        var wk_line = (i == 1) ? '' : i;

	    //項目名
        var s_h = 'over_start_hour'+wk_line;
        var s_m = 'over_start_min'+wk_line;
        var s_f = 'over_start_next_day_flag'+wk_line;
        var e_h = 'over_end_hour'+wk_line;
        var e_m = 'over_end_min'+wk_line;
        var e_f = 'over_end_next_day_flag'+wk_line;
        //value
        var s_hh = document.getElementById(s_h).value;
        var s_mm = document.getElementById(s_m).value;
        var s_flag = document.getElementById(s_f).checked;
        var e_hh = document.getElementById(e_h).value;
        var e_mm = document.getElementById(e_m).value;
        var e_flag = document.getElementById(e_f).checked;
        if (s_hh == '--') {
            s_hh = '';
        }
        if (s_mm == '--') {
            s_mm = '';
        }
        if (e_hh == '--') {
            e_hh = '';
        }
        if (e_mm == '--') {
            e_mm = '';
        }

        if (s_hh  != '') {
            var s_hh_int = parseInt(s_hh, 10);
            if (!s_hh.match(/^[0-9]+$/)
                    || s_hh_int > 23) {
                alert('残業時刻' + i + 'の「開始時」は半角の0から23の範囲内で入力してください。');
                return 1;
            }
        }
        if (s_mm  != '') {
            var s_mm_int = parseInt(s_mm, 10);
            if (!s_mm.match(/^[0-9]+$/)
                    || s_mm_int > 59) {
                alert('残業時刻' + i + 'の「開始分」は半角の0から59の範囲内で入力してください。');
                return 1;
            }
        }
        if ((s_hh != '' && s_mm == '') || (s_hh == '' && s_mm != '')) {
            alert('残業時刻' + i + '「開始」の時と分の一方のみは登録できません。');
            return 1;
        }
        if (e_hh  != '') {
            var e_hh_int = parseInt(e_hh, 10);
            if (!e_hh.match(/^[0-9]+$/)
                    || e_hh_int > 23) {
                alert('残業時刻' + i + 'の「終了時」は半角の0から23の範囲内で入力してください。');
                return 1;
            }
        }
        if (e_mm  != '') {
            var e_mm_int = parseInt(e_mm, 10);
            if (!e_mm.match(/^[0-9]+$/)
                    || e_mm_int > 59) {
                alert('残業時刻' + i + 'の「終了分」は半角の0から59の範囲内で入力してください。');
                return 1;
            }
        }
        if ((e_hh != '' && e_mm == '') || (e_hh == '' && e_mm != '')) {
            alert('残業時刻' + i + '「終了」の時と分の一方のみは登録できません。');
            return 1;
        }
        if (s_hh == '' && s_mm == '' && e_hh != '' && e_mm != '') {
            alert('残業時刻' + i + 'の「開始時刻」を入力してください。');
            return 1;
        }
        if (s_hh != '' && s_mm != '' && e_hh == '' && e_mm == '') {
            alert('残業時刻' + i + 'の「終了時刻」を入力してください。');
            return 1;
        }
        var ovtms_min = 0;
        var ovtme_min = 0;
        //開始、終了の前後関係チェック
	    if (s_hh != '' && e_hh != '') {
	        //日またがり
	        wk_s_hh_int = s_hh_int;
	        wk_e_hh_int = e_hh_int;
            if (s_flag) {
                wk_s_hh_int += 24;
            }
            if (e_flag) {
                wk_e_hh_int += 24;
            }

            ovtms_min = wk_s_hh_int  * 60 + s_mm_int;    
            ovtme_min = wk_e_hh_int  * 60 + e_mm_int;    
            //計算
            wk_ovtm_min = ovtme_min - ovtms_min;
	        
	        if (wk_ovtm_min < 0) {
	            alert('残業時刻' + i + 'の「開始時刻」と「終了時刻」が正しくありません。');
	            return 1;
	        }
        }
		if (ovtm_rest_disp_flg == 't') {
		    //休憩
	        var rs_h = 'rest_start_hour'+i;
	        var rs_m = 'rest_start_min'+i;
	        var rs_f = 'rest_start_next_day_flag'+i;
	        var re_h = 'rest_end_hour'+i;
	        var re_m = 'rest_end_min'+i;
	        var re_f = 'rest_end_next_day_flag'+i;
	        //value
	        var rs_hh = document.getElementById(rs_h).value;
	        var rs_mm = document.getElementById(rs_m).value;
	        var rs_flag = document.getElementById(rs_f).checked;
	        var re_hh = document.getElementById(re_h).value;
	        var re_mm = document.getElementById(re_m).value;
	        var re_flag = document.getElementById(re_f).checked;
	        if (rs_hh == '--') {
	            rs_hh = '';
	        }
	        if (rs_mm == '--') {
	            rs_mm = '';
	        }
	        if (re_hh == '--') {
	            re_hh = '';
	        }
	        if (re_mm == '--') {
	            re_mm = '';
	        }

	        if (rs_hh  != '') {
	            var rs_hh_int = parseInt(rs_hh, 10);
	            if (!rs_hh.match(/^[0-9]+$/)
	                    || rs_hh_int > 23) {
	                alert('残業時刻' + i + 'の「休憩開始時」は半角の0から23の範囲内で入力してください。');
	                return 1;
	            }
	        }
	        if (rs_mm  != '') {
	            var rs_mm_int = parseInt(rs_mm, 10);
	            if (!rs_mm.match(/^[0-9]+$/)
	                    || rs_mm_int > 59) {
	                alert('残業時刻' + i + 'の「休憩開始分」は半角の0から59の範囲内で入力してください。');
	                return 1;
	            }
	        }
	        if ((rs_hh != '' && rs_mm == '') || (rs_hh == '' && rs_mm != '')) {
	            alert('残業時刻' + i + '「休憩開始」の時と分の一方のみは登録できません。');
	            return 1;
	        }
	        if (re_hh  != '') {
	            var re_hh_int = parseInt(re_hh, 10);
	            if (!re_hh.match(/^[0-9]+$/)
	                    || re_hh_int > 23) {
	                alert('残業時刻' + i + 'の「休憩終了時」は半角の0から23の範囲内で入力してください。');
	                return 1;
	            }
	        }
	        if (re_mm  != '') {
	            var re_mm_int = parseInt(re_mm, 10);
	            if (!re_mm.match(/^[0-9]+$/)
	                    || re_mm_int > 59) {
	                alert('残業時刻' + i + 'の「休憩終了分」は半角の0から59の範囲内で入力してください。');
	                return 1;
	            }
	        }
	        if ((re_hh != '' && re_mm == '') || (re_hh == '' && re_mm != '')) {
	            alert('残業時刻' + i + '「休憩終了」の時と分の一方のみは登録できません。');
	            return 1;
	        }
            if (rs_hh == '' && rs_mm == '' && re_hh != '' && re_mm != '') {
                alert('残業時刻' + i + 'の「休憩開始」を入力してください。');
                return 1;
            }
            if (rs_hh != '' && rs_mm != '' && re_hh == '' && re_mm == '') {
                alert('残業時刻' + i + 'の「休憩終了」を入力してください。');
                return 1;
            }
            
            //開始、終了の前後関係チェック
	        if (rs_hh != '' && re_hh != '') {
	            //日またがり
	            wk_rs_hh_int = rs_hh_int;
	            wk_re_hh_int = re_hh_int;
                if (rs_flag) {
                    wk_rs_hh_int += 24;
                }
                if (re_flag) {
                    wk_re_hh_int += 24;
                }

                var ovtmrs_min = wk_rs_hh_int  * 60 + rs_mm_int;    
                var ovtmre_min = wk_re_hh_int  * 60 + re_mm_int;    
                //計算
                wk_ovtmrest_min = ovtmre_min - ovtmrs_min;
    	        
	            if (wk_ovtmrest_min < 0) {
	                alert('残業時刻' + i + 'の「休憩開始時刻」と「休憩終了時刻」が正しくありません。');
	                return 1;
	            }
	            //休憩が範囲内か確認
	            if (ovtmrs_min < ovtms_min || ovtmrs_min > ovtme_min || ovtmre_min < ovtms_min || ovtmre_min > ovtme_min) {
	                alert('残業時刻' + i + 'の「休憩時刻」が正しくありません。');
	                return 1;
	            }
            }
	        if (s_hh == '' && s_mm == '' && rs_hh != '' && rs_mm != '') {
                alert('残業時刻' + i + 'の「休憩時刻」のみは登録できません。');
                return 1;
            }
	        if (s_hh != '' && s_mm != '' && e_hh != '' && e_mm != '') {
		    //妥当性チェック、数値
	    	    
	    	    //翌日フラグ
	            if (s_flag) {
	                s_hh_int += 24;
	            }
	            if (e_flag) {
	                e_hh_int += 24;
	            }
	            var s_min = s_hh_int  * 60 + parseInt(s_mm, 10);    
	            var e_min = e_hh_int  * 60 + parseInt(e_mm, 10);    
		        //残業計算
	            var min = e_min - s_min;

	            ovtm_min += min;
	            kinmu_total_min  += min;
	            
		        //休憩除外
	            if (rs_hh != '' && rs_mm != '' && re_hh != '' && re_mm != '') {
	    	        //翌日フラグ
	                if (rs_flag) {
	                    rs_hh_int += 24;
	                }
	                if (re_flag) {
	                    re_hh_int += 24;
	                }
	                var rs_min = rs_hh_int  * 60 + parseInt(rs_mm);    
	                var re_min = re_hh_int  * 60 + parseInt(re_mm);    
		            //休憩計算
	                var rest_min = re_min - rs_min;
	                ovtm_min -= rest_min;
	                kinmu_total_min -= rest_min;
	                rest_total_min += rest_min;

	            }
	        }
        }   
    }
    if (tmmd_ovtm_flg == 1) {
    //時間有休
    if (document.getElementById('paid_hol_start_hour')) {
    	
    	var pattern = document.getElementById('pattern').value;
    	var paid_hol_start_hour = document.getElementById('paid_hol_start_hour').value;
    	var paid_hol_start_min = document.getElementById('paid_hol_start_min').value;
    	var paid_hol_hour = document.getElementById('paid_hol_hour').value;
    	var paid_hol_min = document.getElementById('paid_hol_min').value;
		//1項目でも入力
        if (paid_hol_start_hour != "--" || paid_hol_start_min != "--" || paid_hol_hour != "--" || paid_hol_min != "--") {
	    	//勤務実績がない場合
	    	if (pattern == "--"){
	    		alert('勤務実績が登録されていません。');
        		return 1;
	    	}
            //休暇の場合はエラーとする
            if (pattern == "10") {
	    		alert('休暇の場合には時間有休を指定できません。');
        		return 1;
			}
            //全項目が入力されていない場合
            if (!(paid_hol_start_hour != "--" && paid_hol_start_min != "--" && paid_hol_hour != "--" && paid_hol_min != "--")) {
                alert('時間有休の開始時刻、時間、分を全て指定してください。');
        		return 1;
            }
		}
    }
    //呼出
    for (var i=1; i<=10; i++) {
        var s_h = 'o_start_hour'+i;
        var s_m = 'o_start_min'+i;
        var e_h = 'o_end_hour'+i;
        var e_m = 'o_end_min'+i;
        
    	if (document.getElementById(s_h)) {
	        var s_hh = document.getElementById(s_h).value;
	        var s_mm = document.getElementById(s_m).value;
	        var e_hh = document.getElementById(e_h).value;
	        var e_mm = document.getElementById(e_m).value;
	        if ((s_hh != '--' && s_mm == '--') || (s_hh == '--' && s_mm != '--')) {
	            alert('呼出勤務時刻' + i + '「開始」の時と分の一方のみは登録できません。');
	            return 1;
	        }
	        if ((e_hh != '--' && e_mm == '--') || (e_hh == '--' && e_mm != '--')) {
	            alert('呼出勤務時刻' + i + '「終了」の時と分の一方のみは登録できません。');
	            return 1;
	        }
    	}
    	else {
    		break;
    	}
    }
    
    //会議・研修・病棟外勤務
    if (document.getElementById('meeting_start_hour')) {
	    var s_h = 'meeting_start_hour';
	    var s_m = 'meeting_start_min';
	    var e_h = 'meeting_end_hour';
	    var e_m = 'meeting_end_min';

	    var s_hh = document.getElementById(s_h).value;
	    var s_mm = document.getElementById(s_m).value;
	    var e_hh = document.getElementById(e_h).value;
	    var e_mm = document.getElementById(e_m).value;

	    if (s_hh == '--') {
	        s_hh = '';
	    }
	    if (s_mm == '--') {
	        s_mm = '';
	    }
	    if (e_hh == '--') {
	        e_hh = '';
	    }
	    if (e_mm == '--') {
	        e_mm = '';
	    }
	    if ((s_hh != '' && s_mm == '') || (s_hh == '' && s_mm != '')) {
	        alert('会議・研修・病棟外勤務開始時刻の時と分の一方のみは登録できません。');
	        return 1;
	    }
	    if ((e_hh != '' && e_mm == '') || (e_hh == '' && e_mm != '')) {
	        alert('会議・研修・病棟外勤務終了時刻の時と分の一方のみは登録できません。');
	        return 1;
	    }
    }


	//理由
    if (document.mainform.reason_id) {
        if ((document.mainform.reason_id.value == 'other' &&
             document.mainform.reason.value == '')) {
            alert('修正理由が入力されていません。');
            return 1;
        }
    }
    }

	if (tmmd_ovtm_flg == 1) {
    	//残業理由1-5チェック
    	if (document.mainform.ovtm_reason_id2) {
    		if (over_time_apply_type != "0"
				&& no_overtime != "t") {
				for (var i=1; i<=5; i++) {
			        var wk_line = (i == 1) ? '' : i;
			        var s_h = 'over_start_hour'+wk_line;
			        var s_hh = document.getElementById(s_h).value;
					var reason_id_nm = 'ovtm_reason_id'+wk_line;
					var reason_nm = 'ovtm_reason'+wk_line;
			        var reason_id_val = document.getElementById(reason_id_nm).value;
			        var reason_val = document.getElementById(reason_nm).value;
					if ((s_hh != '--' && s_hh != '') && reason_id_val == '') {
			            alert('残業理由'+i+'が選択されていません。');
			            return 1;
					}
					if ((s_hh != '--' && s_hh != '') && reason_id_val == 'other' && reason_val == '') {
			            alert('残業理由'+i+'が入力されていません。');
			            return 1;
					}
				}
			}
	    }
	    else {
		    if (document.mainform.ovtm_reason_id.value == 'other') {
		    	if (((document.mainform.over_start_hour.value != '--' && document.mainform.over_start_hour.value != '') || (document.mainform.over_start_hour2.value != '--' && document.mainform.over_start_hour2.value != ''))			
		    	&& over_time_apply_type != "0"
				&& no_overtime != "t") { // 残業管理をする場合

			        if (document.mainform.ovtm_reason.value == '') {
			            alert('残業理由が入力されていません。');
			            return 1;
			        }
		        }
		    }
	    }
    }
    else {
    	//残業理由1-5チェック
    	if (document.mainform.reason_id2) {
    		if (over_time_apply_type != "0"
				&& no_overtime != "t") {
				for (var i=1; i<=5; i++) {
			        var wk_line = (i == 1) ? '' : i;
			        var s_h = 'over_start_hour'+wk_line;
			        var s_hh = document.getElementById(s_h).value;
					var reason_id_nm = 'reason_id'+wk_line;
					var reason_nm = 'reason'+wk_line;
			        var reason_id_val = document.getElementById(reason_id_nm).value;
			        var reason_val = document.getElementById(reason_nm).value;
					if ((s_hh != '--' && s_hh != '') && reason_id_val == '') {
			            alert('残業理由'+i+'が選択されていません。');
			            return 1;
					}
					if ((s_hh != '--' && s_hh != '') && reason_id_val == 'other' && reason_val == '') {
			            alert('残業理由'+i+'が入力されていません。');
			            return 1;
					}
				}
			}
    	}
    	else {
		    if (document.mainform.reason_id.value == 'other') {
		    	if (((document.mainform.over_start_hour.value != '--' && document.mainform.over_start_hour.value != '') || (document.mainform.over_start_hour2.value != '--' && document.mainform.over_start_hour2.value != ''))			
		    	&& over_time_apply_type != "0"
				&& no_overtime != "t") { // 残業管理をする場合

			        if (document.mainform.reason.value == '') {
			            alert('残業理由が入力されていません。');
			            return 1;
			        }
		        }
		    }
	    }
    }

    if (rest_check_flg  == 't') {
        
	    
        //休憩時刻入力有
        if (rest_input_flg == 1) {
            //休憩時間計算
            //var wk_rest_min = 30;
            //所定時間再計算
            var wk_office_min = office_time_min + rest_time_min - wk_rest_min;
            //集計
            kinmu_total_min += wk_office_min;
            rest_total_min += wk_rest_min;
        }
        //休憩時刻なし
        else {
            kinmu_total_min += office_time_min;
            rest_total_min += rest_time_min;
        }        
        //勤務時間＞８時間、かつ、休憩時間＜６０分
        //または
        //勤務時間＞６時間、かつ、休憩時間＜４５分
        if ((kinmu_total_min > 8 * 60 && rest_total_min < 60) ||
                (kinmu_total_min > 6 * 60 && rest_total_min < 45)) {
            rest_flg = 1;
            //休憩不足時間
            rest_fusoku_min = 0;
            if (kinmu_total_min > 8 * 60) {
                rest_fusoku_min = 60 - rest_total_min;
            }
            else if (kinmu_total_min > 6 * 60) {
                rest_fusoku_min = 45 - rest_total_min;
            }
        }
    }
    if (ovtm_limit_check_flg == 't' && ovtm_min > 0) {
        if (ovtm_approve_apply_min + ovtm_min > ovtm_limit_month_min) {
            over_limit_str = '';
            ovtm_flg = 1;
            var over_limit_min = (ovtm_approve_apply_min + ovtm_min) - ovtm_limit_month_min;
            var over_limit_hour = parseInt(over_limit_min / 60, 10);
            var over_limit_min_str = over_limit_min % 60;
            if (over_limit_hour > 0) {
                over_limit_str = over_limit_hour + '時間';
            }
            if (over_limit_min_str > 0) {
                over_limit_str = over_limit_str + over_limit_min_str + '分';
            }
        }
    }
    var apply_update_str = (apply_update_flg == '1') ? '申請' : '更新';
    if (rest_flg == 1 && ovtm_flg == 0) {
        flg = 2;
        msg = 'あと'+rest_fusoku_min+'分の休憩をとってください。このまま'+apply_update_str+'しますか？';
    } 
    else if (rest_flg == 0 && ovtm_flg == 1) {
        flg = 3;
        msg = '当月残業限度時間を'+ over_limit_str  +'超えています。このまま'+apply_update_str+'しますか？';
    } 
    else if (rest_flg == 1 && ovtm_flg == 1) {
        flg = 4;
        msg = 'あと'+rest_fusoku_min+'分の休憩をとってください。当月残業限度時間を'+ over_limit_str  +'超えています。このまま'+apply_update_str+'しますか？';
    }
    return flg;
}

//翌日フラグチェック
function ovtmNextFlgSet(over_or_rest, start_or_end, line, chkitem) {

// var base_time=''; base_time変数を呼出元で定義する
    var wk_base_time = base_time;
    if (wk_base_time == '') {
        var e_hh = document.getElementById('end_hour').value;
        var e_mm = document.getElementById('end_min').value;
        if (e_hh == '--') {
            e_hh = '';
        }
        if (e_mm == '--') {
            e_mm = '';
        }
        if (e_hh != '' && e_mm != '') {
           var e_hh_int = parseInt(e_hh, 10);
           if (e_hh_int > 0 && e_hh_int < 10) {
                e_hh = '0'+e_hh;
           }
           wk_base_time = ''+e_hh+e_mm;
        }
    } 
    var wk_line = (over_or_rest == 'over' && line == 1) ? '' : line;
    var item_hh = over_or_rest + '_' + start_or_end + '_hour' + wk_line;
    var item_mm = over_or_rest + '_' + start_or_end + '_min' + wk_line;

    var hh = document.getElementById(item_hh).value;
    var mm = document.getElementById(item_mm).value;
    if (hh == '' && mm == '') {
        document.getElementById(chkitem).checked = false;
        return false;
    }
    if ((hh != '' && !hh.match(/^[0-9]+$/)) ||
        (mm != '' && !mm.match(/^[0-9]+$/)) ) {
        document.getElementById(chkitem).checked = false;
        return false;
    }
    if (hh.length == 1) {
        hh = '0'+hh;
    }
    if (mm.length == 1) {
        mm = '0'+mm;
    }

    var stat = false;
    var hhmm = hh+mm;
    if (hhmm.length == 4) {
        if (wk_base_time != '') {
            //早出、昼の残業時刻対応、基準時刻の１２時間以内は当日とする
            var base_minute = parseInt(wk_base_time.substr(0, 2), 10) * 60 + parseInt(wk_base_time.substr(2, 2), 10);
            var input_minute = parseInt(hh, 10) * 60 + parseInt(mm, 10);
            var chktime = base_minute - input_minute;
            if (chktime <= 12 * 60) {
                stat  = false;
            }
            else {
                if (hhmm < wk_base_time) {
                    stat  = true;
               }
            }
        }
    }
    document.getElementById(chkitem).checked = stat;

    return false;
}
//所定時刻取得
function get_officetime_ajax(emp_id, tmcd_group_id, pattern, date)
{
	var url = 'atdbk_timecard_get_officetime_ajax.php';
	var params = $H({'session':_session,'emp_id':emp_id, 'tmcd_group_id':tmcd_group_id, 'pattern':pattern, 'date':date}).toQueryString();
	var myAjax = new Ajax.Request(
		url,
		{
			method: 'post',
			postBody: params,
			asynchronous: false
		});
		//同期処理後のデータ処理
		var transport = myAjax.transport;
		set_officetime_ajax_response(transport);
}

//ajax取得データの処理
function set_officetime_ajax_response(oXMLHttpRequest)
{
	if(oXMLHttpRequest.status == 200)
	{
		var response = new ajax_response_object(oXMLHttpRequest.responseText);
		if(response.ajax == "success")
		{
			office_start_time = response.office_start_time;
			office_end_time = response.office_end_time;
			rest_start_time = response.rest_start_time;
			rest_end_time = response.rest_end_time;
			office_time_min = response.office_time_min;
			rest_time_min = response.rest_time_min;
			base_time = response.base_time;
		}
		else
		{
			//処理失敗
			;
		}
	}
	else
	{
		//通信失敗
		;
	}
}

//レスポンスデータよりレスポンスオブジェクトを生成します。(コンストラクタ)
function ajax_response_object(response_text)
{
	var a1 = response_text.split("\n");
	for (var i = 0; i < a1.length; i++)
	{
		var line = a1[i];
		var sep_index = line.indexOf("=");
		if(sep_index == -1)
		{
			break;//最後の改行行と判定
		}
		var key = line.substring(0,sep_index);
		var val = line.substring(sep_index+1);

		var eval_string = "this." + key + " = '" + val.replace("'", "\'") + "';";
		eval(eval_string);
	}
}
