YAHOO.namespace('cal');
YAHOO.util.Event.onDOMReady(initial_setting);

// カレンダー初期設定
var calOptions = {
    title: "日付を選択して下さい",
    close: true,
    MDY_YEAR_POSITION:       1,
    MDY_MONTH_POSITION:      2,
    MDY_DAY_POSITION:        3,
    MY_YEAR_POSITION:        1,
    MY_MONTH_POSITION:       2,
    MY_LABEL_YEAR_POSITION:  1,
    MY_LABEL_MONTH_POSITION: 2,
    MY_LABEL_YEAR_SUFFIX:    "\u5E74",
    MY_LABEL_MONTH_SUFFIX:   "",
    MONTHS_SHORT:            ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    MONTHS_LONG:             ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    WEEKDAYS_1CHAR:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_SHORT:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_MEDIUM:         ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_LONG:           ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']
};

// 初期設定
function initial_setting() {
	if (document.getElementById("cal1Container")) {
		YAHOO.cal.cal1 = new YAHOO.widget.Calendar("cal1","cal1Container", calOptions);
		YAHOO.cal.cal1.render();
		YAHOO.cal.cal1.hide();
		YAHOO.cal.cal1.selectEvent.subscribe(handleSelect, YAHOO.cal.cal1, true);
		YAHOO.util.Event.addListener(["date_y1","date_m1","date_d1"], "change", updateCal);
		YAHOO.util.Event.addListener("cal1_icon", "click", function(){ YAHOO.cal.cal1.show(); });
	}

	if (document.getElementById("cal2Container")) {
		YAHOO.cal.cal2 = new YAHOO.widget.Calendar("cal2","cal2Container", calOptions);
		YAHOO.cal.cal2.render();
		YAHOO.cal.cal2.hide();
		YAHOO.cal.cal2.selectEvent.subscribe(handleSelect, YAHOO.cal.cal2, true);
		YAHOO.util.Event.addListener(["date_y2","date_m2","date_d2"], "change", updateCal);
		YAHOO.util.Event.addListener("cal2_icon", "click", function(){ YAHOO.cal.cal2.show(); });
	}

	// 出力CSV項目切り替え
	YAHOO.util.Event.addListener("type1", "click", function(){
		if (this.checked) {
			document.getElementById('type1check').style.display = "";
			document.getElementById('type2check').style.display = "none";
		}
		else {
			document.getElementById('type1check').style.display = "none";
			document.getElementById('type2check').style.display = "";
		}
	});
	YAHOO.util.Event.addListener("type2", "click", function(){
		if (this.checked) {
			document.getElementById('type1check').style.display = "none";
			document.getElementById('type2check').style.display = "";
		}
		else {
			document.getElementById('type1check').style.display = "";
			document.getElementById('type2check').style.display = "none";
		}
	});
	document.getElementById('type1check').style.display = "none";
	document.getElementById('type2check').style.display = "none";

	// 全部ON/OFF
	YAHOO.util.Event.addListener("allcheck", "click", function(){
		var flg = false;	
		if (document.getElementById('allcheck').innerHTML == '全部OFF') {
			flg = false;
			document.getElementById('allcheck').innerHTML = '全部ON';
		}
		else {
			flg = true;
			document.getElementById('allcheck').innerHTML = '全部OFF';
		}

		for (i=0; i<document.getElementsByName('item[]').length; i++) {
			document.getElementsByName('item[]')[i].checked = flg;
		}
		for (i=0; i<document.getElementsByName('item1[]').length; i++) {
			document.getElementsByName('item1[]')[i].checked = flg;
		}
		for (i=0; i<document.getElementsByName('item2[]').length; i++) {
			document.getElementsByName('item2[]')[i].checked = flg;
		}
	});
}

// カレンダー選択時に呼び出されるfunction
function handleSelect(type,args,obj) {
    var id = this.id.slice(-1);
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];

    var selMonth = document.getElementById("date_m" + id);
    var selDay   = document.getElementById("date_d" + id);
    var selYear  = document.getElementById("date_y" + id);

    selMonth.selectedIndex = month;
    selDay.selectedIndex = day;
    for (var y=0;y<selYear.options.length;y++) {
        if (selYear.options[y].value == year) {
            selYear.selectedIndex = y;
            break;
        }
    }
	this.hide();
}

// カレンダーのプルダウン変更時に呼び出されるfunction
function updateCal() {
    var id = this.id.slice(-1);
    var selMonth = document.getElementById("date_m" + id);
    var selDay   = document.getElementById("date_d" + id);
    var selYear  = document.getElementById("date_y" + id);

    var month = parseInt(selMonth.options[selMonth.selectedIndex].value, 10);
    var day = parseInt(selDay.options[selDay.selectedIndex].value, 10);
    var year = parseInt(selYear.options[selYear.selectedIndex].value, 10);

    if (! isNaN(month) && ! isNaN(day) && ! isNaN(year)) {
        var date = year + "/" + month + "/" + day;
        var cal = eval('YAHOO.cal.cal' + id);
        cal.select(date);
        cal.cfg.setProperty("pagedate", year + "/" + month);
        cal.render();
    }
}
