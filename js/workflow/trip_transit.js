// テーブルのDrap&Drop並べ替え
YAHOO.util.Event.addListener(window, "load", function() {
	var list = document.getElementById("msttable");
    var TR = list.getElementsByTagName('tr');
    for (var i=0; i<TR.length; i++) {
        if (/tbody/i.test(TR[i].parentNode.nodeName)) {
			new DDTableRow(TR[i]);
		}
	}
});

// レコード追加ボタン
YAHOO.util.Event.addListener("submit_add", "click", function() {
	var form = document.getElementById("mstform");
	form.action = 'workflow_trip_transit_form.php';
	form.submit();
});

// 表示順の更新ボタン
YAHOO.util.Event.addListener("submit_no", "click", function() {
	var form = document.getElementById("mstform");
	form.action = 'workflow_trip_transit_update.php';
	document.getElementById("mode").value = 'no';
	form.submit();
});

// 表示/非表示の更新ボタン
YAHOO.util.Event.addListener("submit_view", "click", function() {
	var form = document.getElementById("mstform");
	form.action = 'workflow_trip_transit_update.php';
	document.getElementById("mode").value = 'view';
	form.submit();
});

// 削除ボタン
YAHOO.util.Event.addListener("submit_del", "click", function() {
	var form = document.getElementById("mstform");
	form.action = 'workflow_trip_transit_update.php';
	document.getElementById("mode").value = 'del';
	form.submit();
});
