var childwin = null;
var emplist = new Array();
YAHOO.namespace('cal');
YAHOO.util.Event.onDOMReady(initial_setting);

// カレンダー初期設定
var calOptions = {
    title: "日付を選択して下さい",
    close: true,
    MDY_YEAR_POSITION:       1,
    MDY_MONTH_POSITION:      2,
    MDY_DAY_POSITION:        3,
    MY_YEAR_POSITION:        1,
    MY_MONTH_POSITION:       2,
    MY_LABEL_YEAR_POSITION:  1,
    MY_LABEL_MONTH_POSITION: 2,
    MY_LABEL_YEAR_SUFFIX:    "\u5E74",
    MY_LABEL_MONTH_SUFFIX:   "",
    MONTHS_SHORT:            ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    MONTHS_LONG:             ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    WEEKDAYS_1CHAR:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_SHORT:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_MEDIUM:         ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_LONG:           ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']
};

// 初期設定
function initial_setting() {
    // カレンダー
    YAHOO.cal.cal1 = new YAHOO.widget.Calendar("cal1","cal_oil2", calOptions);
    YAHOO.cal.cal1.select(document.getElementsByName('oil_price2_date')[0].value);
    YAHOO.cal.cal1.cfg.setProperty("pagedate", document.getElementsByName('oil_price2_date')[0].value);
    YAHOO.cal.cal1.render();
    YAHOO.cal.cal1.hide();

    YAHOO.cal.cal2 = new YAHOO.widget.Calendar("cal2","cal_oil3", calOptions);
    YAHOO.cal.cal2.select(document.getElementsByName('oil_price3_date')[0].value);
    YAHOO.cal.cal2.cfg.setProperty("pagedate", document.getElementsByName('oil_price3_date')[0].value);
    YAHOO.cal.cal2.render();
    YAHOO.cal.cal2.hide();

    YAHOO.cal.cal1.selectEvent.subscribe(handleSelect_cal1, YAHOO.cal.cal1, true);
    YAHOO.cal.cal2.selectEvent.subscribe(handleSelect_cal2, YAHOO.cal.cal2, true);

	// 前渡しチケットメール
	var mail = document.getElementById('preticket_mail').childNodes;
	for (i=0; i<mail.length; i++) {
		emplist[mail[i].id] = true;
	}
}

// カレンダー選択時に呼び出されるfunction
function handleSelect_cal1(type,args,obj) {
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];
    document.getElementsByName('oil_price2_date')[0].value = year + '/' + month.fillZero(2) + '/' + day.fillZero(2);
    this.hide();
}
function handleSelect_cal2(type,args,obj) {
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];
    document.getElementsByName('oil_price3_date')[0].value = year + '/' + month.fillZero(2) + '/' + day.fillZero(2);
    this.hide();
}

// "先頭を0で埋めて桁をそろえる"
// http://d.hatena.ne.jp/higeorange/20080515/1210817629
Number.prototype.fillZero = function(n) {
    return Array((n+1) - this.toString().split('').length).join('0') + this;
}

function openEmployeeList(session) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=' + session;
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

// 職員の追加
function add_target_list(item_id, emp_id,emp_name) {
	var emp = emp_id.split(", ");
	var empname = emp_name.split(", ");
	for (i=0; i<emp.length; i++) {
		if (!emplist[emp[i]]) {
			emplist[emp[i]] = true;
			document.getElementById('preticket_mail').innerHTML =
				document.getElementById('preticket_mail').innerHTML +
				'<div><input type="hidden" name="preticket_mail[]" value="' + emp[i] + '"><div style="width:150px;float:left">' +
				empname[i] + '</div><button type="button" onclick="remove_row(this);">削除</button></div><span style="clear:both;"></span>';
		}
	}
}

// 職員の削除
function remove_row(obj) {
    var row = obj.parentNode;
    document.getElementById('preticket_mail').removeChild(row);
	emplist[row.id] = false;
}
