YAHOO.namespace('cal');
YAHOO.util.Event.onDOMReady(initial_setting);

// カレンダー初期設定
var calOptions = {
    title: "日付を選択して下さい",
    close: true,
    MDY_YEAR_POSITION:       1,
    MDY_MONTH_POSITION:      2,
    MDY_DAY_POSITION:        3,
    MY_YEAR_POSITION:        1,
    MY_MONTH_POSITION:       2,
    MY_LABEL_YEAR_POSITION:  1,
    MY_LABEL_MONTH_POSITION: 2,
    MY_LABEL_YEAR_SUFFIX:    "\u5E74",
    MY_LABEL_MONTH_SUFFIX:   "",
    MONTHS_SHORT:            ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    MONTHS_LONG:             ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    WEEKDAYS_1CHAR:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_SHORT:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_MEDIUM:         ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_LONG:           ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']
};

// 初期設定
function initial_setting() {
    // カレンダー
    YAHOO.cal.cal1 = new YAHOO.widget.Calendar("cal1","calContainer", calOptions);
    YAHOO.cal.cal1.select(document.getElementById('date').value);
    YAHOO.cal.cal1.cfg.setProperty("pagedate", document.getElementById('date').value);
    YAHOO.cal.cal1.render();
    YAHOO.cal.cal1.hide();

    YAHOO.cal.cal1.selectEvent.subscribe(handleSelect_cal1, YAHOO.cal.cal1, true);
}

// カレンダー選択時に呼び出されるfunction
function handleSelect_cal1(type,args,obj) {
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];
    document.getElementById('date').value = year + '/' + month.fillZero(2) + '/' + day.fillZero(2);
    this.hide();
}

// "先頭を0で埋めて桁をそろえる"
// http://d.hatena.ne.jp/higeorange/20080515/1210817629
Number.prototype.fillZero = function(n) {
    return Array((n+1) - this.toString().split('').length).join('0') + this;
}
