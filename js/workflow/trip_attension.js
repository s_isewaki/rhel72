// レコード追加ボタン
YAHOO.util.Event.addListener("submit_add", "click", function() {
	var form = document.getElementById("mstform");
	form.action = 'workflow_trip_attension_form.php';
	form.submit();
});

// 表示/非表示の更新ボタン
YAHOO.util.Event.addListener("submit_view", "click", function() {
	var form = document.getElementById("mstform");
	form.action = 'workflow_trip_attension_update.php';
	document.getElementById("mode").value = 'view';
	form.submit();
});
