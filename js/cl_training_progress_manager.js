function research()
{
	// 2012/07/04 Yamagawa add(s)
	document.training_form.research_flg.value = 'true';
	// 2012/07/04 Yamagawa add(e)
	document.training_form.action = "#";
	document.training_form.target = "";
	document.training_form.submit();
}

function output_to_csv(type){

	switch (type){
		case 1:
			document.training_form.user_csv_flg.value = 'true';
			document.training_form.training_csv_flg.value = 'false';
			break;

		case 2:
			document.training_form.user_csv_flg.value = 'false';
			document.training_form.training_csv_flg.value = 'true';
			break;


		default:
			document.training_form.user_csv_flg.value = 'false';
			document.training_form.training_csv_flg.value = 'false';
		}

	document.training_form.action = "./cl_training_progress_manager_csv.php";
	document.training_form.submit();
}

function show_detail(emp_id,year)
{

	var moduleName = "../../cl_training_progress";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.emp_id = emp_id;
	params.year = year;
	params.mode = 'detail';
	params.p_session = document.training_form.session.value;

	//ウィンドウサイズなどのオプション
	//var h = window.screen.availHeight;
	//var w = window.screen.availWidth;
	var w = 880;
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option,'cl_training_progress',params.p_session);

}
