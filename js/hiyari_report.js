/******************************************************************************
 * file name   : hiyari_report.js
 * description : 報告画面　新旧デザイン共通のJSファイル
 ******************************************************************************/
 
//===============================================
//亀画像選択
//===============================================
function rp_action_image_select()
{
    var url = "hiyari_image_select.php?session="+session;
    show_rp_sub_window(url);
}

//===============================================
//実行アイコン
//===============================================
/**
 * 新規登録
 */
function rp_action_new()
{
    var url = "hiyari_easyinput.php?session="+session+"&gamen_mode=new";
    show_rp_sub_window(url);
}

/**
 * 返信
 */
function rp_action_return(mail_id_mode)
{
    //単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }

    //※リストID=メールIDを前提とする。
    var url = "hiyari_rp_mail_input.php?session="+session+"&mode=return&mail_id=" + list_ids[0] + "&mail_id_mode=" + mail_id_mode;
    show_rp_sub_window(url);
}

/**
 * 転送
 */
function rp_action_forward(mail_id_mode)
{
    //単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }

    //※リストID=メールIDを前提とする。
    var url = "hiyari_rp_mail_input.php?session="+session+"&mode=forward&mail_id=" + list_ids[0] + "&mail_id_mode=" + mail_id_mode;
    show_rp_sub_window(url);
}

/**
 * 削除
 */
function rp_action_delete()
{
    //複数選択
    var list_ids = get_selected_list_ids();
    if(! check_multi_select(list_ids))
    {
        return;
    }

    if(! confirm("削除してもよろしいですか？"))
    {
        return;
    }

    document.list_form.mode.value="delete";
    document.list_form.submit();
}

/**
 * 下書き編集
 */
function rp_action_shitagaki_update()
{
    //単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }

    var report_id = get_report_id_from_list_id(list_ids[0]);
    rp_action_shitagaki_update_from_report_id(report_id);
}

/**
 * レポート編集
 */
function rp_action_update(mail_id_mode, list_id_is_mail_id, filename)
{
    //単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }
    
    // 編集できるか確認
    if (!check_update_flg(list_ids[0])) {
        return;
    }
    
    var report_id = get_report_id_from_list_id(list_ids[0]);
    
    var url = "hiyari_easyinput.php?session="+session+"&gamen_mode=update&report_id=" + report_id + "&callerpage=" + filename;
    if(list_id_is_mail_id)
    {
        url = url + "&mail_id=" + list_ids[0];
        url = url + "&mail_id_mode=" + mail_id_mode;
    }else{
        url = url + "&mail_id=" + get_mail_id_from_list_id(list_ids[0]);
    }
    show_rp_sub_window(url);
}

/**
 * 既読・未読
 */
function rp_action_readflg()
{
    //複数選択
    var list_ids = get_selected_list_ids();
    if(! check_multi_select(list_ids))
    {
        return;
    }

    document.list_form.mode.value="read_change";
    document.list_form.submit();
}

/**
 * 進捗登録
 */
function rp_action_progress(mail_id_mode, list_id_is_mail_id, return_list)
{
	//単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }

    var report_id = get_report_id_from_list_id(list_ids[0]);
    var url = "hiyari_rp_progress.php?session="+session+"&report_id=" + report_id;
    if(list_id_is_mail_id)
    {
        url = url + "&mail_id=" + list_ids[0];
        url = url + "&mail_id_mode=" + mail_id_mode;
    }else{
        url = url + "&mail_id=" + get_mail_id_from_list_id(list_ids[0]);
    }
    url = url + "&return_list=" + return_list;
    show_rp_sub_window(url);
}

/**
 * 分析・再発防止（旧デザイン）、評価（新デザイン）
 */
function rp_action_analysis(mail_id_mode, list_id_is_mail_id) {
    //単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }

    var report_id = get_report_id_from_list_id(list_ids[0]);
    var send_job_id = get_send_job_id(list_ids[0]);
    var eis_no = get_eis_no(list_ids[0]);
    
    var url = "hiyari_easyinput.php?session="+session+"&gamen_mode=analysis&report_id=" + report_id;
    if (list_id_is_mail_id) {
        url = url + "&mail_id=" + list_ids[0];
        url = url + "&mail_id_mode=" + mail_id_mode;
    }
    url = url + "&job_id=" + send_job_id;
    url = url + "&eis_no=" + eis_no;
    show_rp_sub_window(url);
}

/**
 * 出来事分析 20100601
 */
function rp_action_analysis2() {
    //単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }

    var report_id = get_report_id_from_list_id(list_ids[0]);
    var url = "hiyari_analysis_regist.php?session="+session+"&report_id=" + report_id;
    var h = 400;//window.screen.availHeight;
    var w = 500;//window.screen.availWidth;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

    window.open(url, '_blank',option);
}

function rp_action_analysis_pastreport() {
    //単一選択
    var list_ids = get_selected_list_ids();
    if(! check_single_select(list_ids))
    {
        return;
    }

    var report_id = get_report_id_from_list_id(list_ids[0]);
    var url = "hiyari_analysis_regist.php?session="+session+"&report_id=" + report_id+"&from_pastreport=on";
    var h = 400;//window.screen.availHeight;
    var w = 500;//window.screen.availWidth;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

    window.open(url, '_blank',option);
}
/**
 * エクセル出力
 */
function rp_action_report_excel()
{
    var list_ids = get_selected_list_ids();
    //単一選択の場合:単一出力
    if(list_ids.length == 1)
    {
        var report_id = get_report_id_from_list_id(list_ids[0]);
        document.list_form.action = "hiyari_rp_report_excel.php"
        document.list_form.report_id.value = report_id;
        document.list_form.submit();
        
        document.list_form.action = "";
        document.list_form.target = "_self";
    }
    //未選択の場合:リスト出力
    else if(list_ids.length == 0)
    {
        var report_id_list = document.list_form.report_id_list.value;
        if(report_id_list == "")
        {
            alert("出力する報告がありません。");
        }
        else
        {
            var h = window.screen.availHeight;
            var w = window.screen.availWidth;
            var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
            window.open("", "list_excel_window", option);
            document.list_form.action = "hiyari_excel_output_item.php";
            document.list_form.target = "list_excel_window";
            document.list_form.submit();
            
            document.list_form.action = "";
            document.list_form.target = "_self";
        }
    }
    //それ以外(複数選択の場合):NG
    else
    {
        alert("選択しない、もしくは1件だけ選択してください。 (" + list_ids.length + "件選択されています。)");
    }
}

/**
 * 削除取消
 */
function rp_action_delete_cancel()
{
    //複数選択
    var list_ids = get_selected_list_ids();
    if(! check_multi_select(list_ids))
    {
        return;
    }

    document.list_form.mode.value="delete_cancel";
    document.list_form.submit();
}

/**
 * ゴミ箱を空にする
 */
function rp_action_kill()
{
    //      //複数選択
    //      var list_ids = get_selected_list_ids();
    //      if(! check_multi_select(list_ids))
    //      {
    //          return;
    //      }
    //選択して削除ではなく、表示されていないものも含めて全削除。

    if(! confirm("ゴミ箱内の全ての報告を削除します。\nゴミ箱から削除された報告は元に戻すことが出来なくなります。\nまたメール等、その報告に紐付く情報全てにアクセス出来なくなります。\nよろしいでしょうか？"))
    {
        return;
    }

    document.list_form.mode.value="kill";
    document.list_form.submit();
}

/**
 * 検索条件入力画面を表示
 */
function show_search_input_page(filename)
{
    if(j$("#search_area").css("display") == "none"){
        j$("#search_area").css("display", "");
        document.list_form.search_toggle.value = 'open';
    }
    else{
        j$("#search_area").css("display", "none");
        document.list_form.search_toggle.value = '';
    }

    if (filename == "hiyari_rpt_report_classification.php"){
        document.getElementById('auto_group_flg').checked = false;
    }
}

/**
 * 選択されているリストID配列を取得
 */
function get_selected_list_ids()
{
    var result = new Array();
    var r_i = 0;

    var list_select_objs = document.getElementsByName("list_select[]");
    for(var i = 0; i < list_select_objs.length ; i++)
    {
        list_select_obj = list_select_objs.item(i);
        if(list_select_obj.checked)
        {
            result[r_i] = list_select_obj.value;
            r_i++;
        }
    }

    return result;
}

/**
 * 単一選択チェック
 */
function check_single_select(ids)
{
    if(ids.length < 1)
    {
        alert("選択されていません。");
        return false;
    }
    if(ids.length != 1)
    {
        alert("1件だけ選択してください。 (" + ids.length + "件選択されています。)");
        return false;
    }
    return true;
}

/**
 * 複数選択チェック
 */
function check_multi_select(ids)
{
    if(ids.length < 1)
    {
        alert("選択されていません。");
        return false;
    }
    return true;
}


/**
 * リストIDからmailIDを取得
 */
function get_mail_id_from_list_id(list_id)
{
    var obj = document.getElementById("mail_id_of_list_id_"+list_id);
    return obj.value;
}

/**
 * リストIDからレポートIDを取得
 */
function get_report_id_from_list_id(list_id)
{
    var obj = document.getElementById("report_id_of_list_id_"+list_id);
    return obj.value;
}

/**
 * リストIDから職種IDを取得
 */
function get_send_job_id(list_id)
{
    var obj = document.getElementById("send_job_id_"+list_id);
    return obj.value;
}

/**
 * リストIDから様式IDを取得
 */
function get_eis_no(list_id)
{
    var obj = document.getElementById("eis_no_"+list_id);
    return obj.value;
}

/**
 * レポーティング子画面を表示
 */
function show_rp_sub_window(url)
{
    var h = window.screen.availHeight;
    var w = window.screen.availWidth;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
    window.open(url, '_blank', option);
}

/**
 * 更新可否の取得
 */
function check_update_flg(list_id) {
    // *** 変数宣言 ***
    
    // ** 編集権限が格納されているオブジェクトを取得 **
    var obj = document.getElementById("report_torey_update_flg_id_" + list_id);
    
    // ** 編集権限があるかないか **
    var update_flg ;
    
    // *** 編集権限を確認 ***
    if (obj.value == '1') {
        update_flg = true;
    }
    else {
        alert("このレポートの編集権限がありません。");
        update_flg = false;
    }
    
    return update_flg ;
}

//===============================================
//検索
//===============================================
//兼務部署の全てを表示オプション
function hiyari_search_emp_all(is_search_emp_all, class_cnt) 
{
    j$("#search_emp_class").attr("disabled", is_search_emp_all);
    j$("#search_emp_attribute").attr("disabled", is_search_emp_all);
    j$("#search_emp_dept").attr("disabled", is_search_emp_all);
    if (class_cnt == 4){
        j$("#search_emp_room").attr("disabled", is_search_emp_all);
    }
}

//検索
function search_torey(file, title_name, predetermined_eval)
{
    //入力チェック
    var st = document.getElementById('search_torey_date_in_search_area_start').value;
    var ed = document.getElementById('search_torey_date_in_search_area_end').value;
    if( (st != "" && !is_date_str(st)) || (ed != "" && !is_date_str(ed)) ){
        alert(title_name + "が正しくありません。");
        return;
    }

    if (file != "hiyari_rpt_shitagaki_db.php"){
        var st = document.getElementById('search_incident_date_start').value;
        var ed = document.getElementById('search_incident_date_end').value;
        if( (st != "" && !is_date_str(st)) || (ed != "" && !is_date_str(ed)) ){
            alert("発生日が正しくありません。");
            return;
        }
    }

    //検索実行
    if (file != "hiyari_rpt_shitagaki_db.php"){
        //患者
        document.list_form.search_patient_id.value           =   document.getElementById('search_patient_id').value;
        document.list_form.search_patient_name.value         =   document.getElementById('search_patient_name').value;
        document.list_form.search_patient_year_from.value    =   document.getElementById('search_patient_year_from').value;
        document.list_form.search_patient_year_to.value      =   document.getElementById('search_patient_year_to').value;

        //事案番号
        document.list_form.search_report_no.value            =   document.getElementById('search_report_no').value;

        // キーワード
        if (file != "hiyari_rpt_send_torey.php"){
            document.list_form.search_title.value = document.getElementById('search_title') ? document.getElementById('search_title').value : null;
            document.list_form.search_incident.value = document.getElementById('search_incident') ? document.getElementById('search_incident').value : null;
        }
        var title_and_or_div = document.getElementsByName('title_and_or_div');
        for(var i=0; title_and_or_div.length > i; i++) {
            if(title_and_or_div[i].checked == true) {
                document.list_form.title_and_or_div.value = title_and_or_div[i].value;
                break;
            }
        }
        var incident_and_or_div = document.getElementsByName('incident_and_or_div');
        for(var i=0; incident_and_or_div.length > i; i++) {
            if(incident_and_or_div[i].checked == true) {
                document.list_form.incident_and_or_div.value = incident_and_or_div[i].value;
                break;
            }
        }
    }

    if ((file == "hiyari_rpt_report_classification.php" || file == "hiyari_analysis_search.php")&& predetermined_eval == 't'){
        //評価予定日
        document.list_form.search_evaluation_date.value      =   document.getElementById('search_evaluation_date').value;
    }

    //送信日/報告日/作成日
    document.list_form.search_torey_date_in_search_area_start.value =   document.getElementById('search_torey_date_in_search_area_start').value;
    document.list_form.search_torey_date_in_search_area_end.value   =   document.getElementById('search_torey_date_in_search_area_end').value;

    if (file != "hiyari_rpt_shitagaki_db.php"){
        //発生日
        document.list_form.search_incident_date_start.value  =   document.getElementById('search_incident_date_start').value;
        document.list_form.search_incident_date_end.value    =   document.getElementById('search_incident_date_end').value;
    }

    if (file == "hiyari_rpt_report_classification.php"){
        if (document.getElementById('auto_group_flg').checked) {
            document.list_form.auto_group_flg.value    =   '1';
        } else {
            document.list_form.auto_group_flg.value    =   '0';
            document.list_form.folder_id.value    =   '';
        }
    }

    //検索時には日付検索領域は使用不可となる。
    document.list_form.search_date_mode.value = "non";

    document.list_form.page.value=1;
    document.list_form.submit();
}

//===============================================
//一覧の操作
//===============================================
/**
 * ソート
 */
function set_report_list_sort(item)
{
    var sort_item = document.list_form.sort_item.value;

    if(sort_item == item)
    {
        var sort_div = document.list_form.sort_div.value

        if(sort_div == "0")
        {
            // 降順
            document.list_form.sort_div.value = "1";
        }
        else
        {
            // 昇順
            document.list_form.sort_div.value = "0";
        }
    }
    else
    {
        // 昇順
        document.list_form.sort_div.value = "0";
    }

    document.list_form.sort_item.value = item;
    document.list_form.mode.value="sort";
    document.list_form.submit();
}

/**
 * メール内容表示（件名のクリック）
 */
function rp_action_mail_disp(mail_id, mail_id_mode)
{
    var url = "hiyari_rp_mail_disp.php?session="+session+"&mail_id_mode=" + mail_id_mode + "&mail_id=" + mail_id;
    show_rp_sub_window(url);
}

/**
 * レポート編集(レポートタイトルのクリック)
 */
function rp_action_update_from_report_id(report_id, filename, mail_id)
{
    var url = "hiyari_easyinput.php?session="+session+"&gamen_mode=update&report_id=" + report_id + "&callerpage=" + filename + "&mail_id=" + mail_id;
    show_rp_sub_window(url);
}

/**
 * 下書き編集(レポートタイトルのクリック)
 */
function rp_action_shitagaki_update_from_report_id(report_id)
{
    var url = "hiyari_easyinput.php?session="+session+"&gamen_mode=shitagaki&report_id=" + report_id;
    show_rp_sub_window(url);
}

/**
 * 受信者一覧表示（既読のクリック）
 */
function rp_action_recv_emp_list_disp(mail_id)
{
    var url = "hiyari_rp_mail_recv_emp_list.php?session="+session+"&mail_id=" + mail_id;
    show_rp_sub_window(url);
}

/**
 * 分析・再発防止の表示（評価ステータスアイコンまたは影響レベルのクリック）
 */
function rp_action_analysis_disp(report_id)
{
    var url = "hiyari_easyinput.php?session="+session+"&gamen_mode=analysis&report_id=" + report_id + "&non_update_flg=true";
    show_rp_sub_window(url);
}

/**
 * 確認コメント一覧表示（進捗確認のクリック）
 */
function rp_action_progress_comment_list(report_id,auth)
{
    var url = "hiyari_rp_progress_comment_list.php?session="+session+"&report_id=" + report_id + "&auth=" + auth;
    show_rp_sub_window(url);
}

//===============================================
//分類フォルダ（報告ファイルトレイの場合のみ）
//===============================================
/**
 * フォルダ追加
 */
function rp_action_new_classification_folder()
{
    var url = "hiyari_rp_classification_folder_input.php?session="+session+"&mode=new";
    show_rp_sub_window(url);
}

/**
 * フォルダ更新
 */
function rp_action_update_classification_folder()
{
    //単一選択
    var ids = get_selected_classification_folder_ids();
    if(! check_single_select(ids)){
        return;
    }

    var folder_id = ids[0];
    rp_action_update_classification_folder1(folder_id);
}

/**
 * フォルダ更新(フォルダ指定)
 */
function rp_action_update_classification_folder1(folder_id)
{
    var url = "hiyari_rp_classification_folder_input.php?session="+session+"&mode=update&folder_id="+folder_id;
    show_rp_sub_window(url);
}

/**
 * フォルダ削除
 */
function rp_action_delete_classification_folder()
{
    //複数選択
    var ids = get_selected_classification_folder_ids();
    if(! check_multi_select(ids)){
        return;
    }

    if(! confirm("削除してもよろしいですか？")){
        return;
    }

    document.list_form.mode.value="folder_delete";
    document.list_form.submit();
}

/**
 * フォルダ選択変更
 */
function rp_action_change_classification_folder(folder_id)
{
    document.list_form.page.value=1;
    document.list_form.folder_id.value=folder_id;
    document.list_form.submit();
}

/**
 * レポート分類変更
 */
function rp_action_change_report_classification(to_folder_id,report_id)
{
    document.list_form.mode.value="change_report_classification";
    //document.list_form.page.value=1;//ページ数を変更しない。(分類解除時にページオーバーするが、0件表示となるためOK)
    document.list_form.change_report_classification_report_id.value=report_id;
    document.list_form.change_report_classification_folder_id.value=to_folder_id;
    document.list_form.submit();
}

/**
 * 選択されているリストID配列を取得
 */
function get_selected_classification_folder_ids()
{
    var result = new Array();
    var r_i = 0;

    var list_select_objs = document.getElementsByName("folder_select[]");
    for(var i = 0; i < list_select_objs.length ; i++){
        list_select_obj = list_select_objs.item(i);
        if(list_select_obj.checked){
            result[r_i] = list_select_obj.value;
            r_i++;
        }
    }

    return result;
}

//===============================================
//報告書リンク（報告ファイルトレイの場合のみ）
//===============================================
/**
 * 報告書リンク変更
 */
function rp_action_update_same_report_link(report_id)
{
    var url = "hiyari_rp_same_report_link.php?session="+session+"&target_report_id="+report_id;
    show_rp_sub_window(url);
}

/**
 * 報告書リンク追加
 */
function rp_action_add_update_same_report_link(report_id,add_report_id)
{
    var url = "hiyari_rp_same_report_link.php?session="+session+"&target_report_id="+report_id+"&add_report_id="+add_report_id;
    show_rp_sub_window(url);
}
