/**
 * ファントルくんのe-ラーニング用JS
 * 　職員名簿部分は、hiyari_el_emplist_javascript.iniから移植
 * 　その他の部分は、hiyari_el_folder_register.php、hiyari_el_folder_update.php、hiyari_el_register.phpにあった共通部分から移植
 */
//===================================================================
//職員名簿
//===================================================================
//職員名簿ウィンド
var m_childwin = null;

//--------------------------------------------------
//登録対象者情報構造体
//--------------------------------------------------
function user_info(emp_id,emp_name)
{
	this.emp_id = emp_id;
	this.emp_name = emp_name;
}

//--------------------------------------------------
//職員名簿を開きます。
//--------------------------------------------------
function openEmployeeList(item_id, session)
{
	dx = screen.availWidth - 10;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'hiyari_emp_list.php';
	url += '?session=' + session + '&input_div=' + item_id;
	m_childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	m_childwin.focus();
}

//--------------------------------------------------
//職員名簿を閉じます。
//--------------------------------------------------
function closeEmployeeList()
{
	if (m_childwin != null && !m_childwin.closed)
	{
		m_childwin.close();
	}
	m_childwin = null;
}

//--------------------------------------------------
//登録対象者追加 引数：", "で区切られたID、名前
//--------------------------------------------------
//※この関数は職員名簿画面から呼ばれます。
function add_emp_list(emp_id, emp_name, item_id)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");

	//追加
	for(i=0;i<emp_ids.length;i++)
	{
		m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_ids[i],emp_names[i]));
	}
	//登録対象者一覧の重複除去
	single_target_list(item_id);
	//HTML反映処理
	update_target_html(item_id);
}

//--------------------------------------------------
//クリア
//--------------------------------------------------
// 
// 「クリア」の仕様は文書管理にあわせてあります。
// emp_id, emp_name にはログインユーザーが指定されます。
//
function clear_target(item_id, emp_id, emp_name)
{
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		//指定ユーザーが登録対象者に含まれるか判定します。
		var is_exist_flg = false;
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			if(emp_id == m_target_list[item_id][i].emp_id)
			{
				is_exist_flg = true;
				break;
			}
		}
		
		//登録対象者を全削除します。
		m_target_list[item_id] = new Array();
		
		//指定ユーザーが登録対象者に含まれていた場合のみ、登録対象者に残します。
		if (is_exist_flg == true)
		{
			m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
		}
		update_target_html(item_id);
	}
}

//--------------------------------------------------
//登録対象者を削除します。
//--------------------------------------------------
function delete_target(item_id, emp_id,emp_name)
{
	if(confirm("「" + emp_name + "」を登録対象者から削除します。よろしいですか？"))
	{
		delete_target_list(item_id, emp_id);
		
		//HTML反映処理
		update_target_html(item_id);
	}
}

//--------------------------------------------------
//m_target_listより登録対象者を削除します。
//--------------------------------------------------
//※ここではHTML反映は行わないため、update_target_html()を実行する必要あり。
function delete_target_list(item_id, emp_id)
{
	var new_array = new Array();
	for(var i=0;i<m_target_list[item_id].length;i++)
	{
		if(emp_id != m_target_list[item_id][i].emp_id)
		{
			new_array = array_add(new_array,m_target_list[item_id][i]);
		}
	}
	m_target_list[item_id] = new_array;
}

//--------------------------------------------------
//m_target_listの登録対象者の重複をなくします。
//--------------------------------------------------
function single_target_list(item_id)
{
	var new_target_list = new Array();
	new_target_list[item_id] = new Array();
	var emp_id_csv = "";
	for(var i = 0; i < m_target_list[item_id].length; i++)
	{
		var emp_id = m_target_list[item_id][i].emp_id;
		if(emp_id_csv.indexOf(emp_id) == -1)
		{
			//重複していない。
			emp_id_csv = emp_id_csv + "," + emp_id;
			new_target_list[item_id] = array_add(new_target_list[item_id],m_target_list[item_id][i]);
		}
	}
	m_target_list[item_id] = new_target_list[item_id];
}

//--------------------------------------------------
//m_target_listより登録対象者のHTMLを再生成します。
//--------------------------------------------------
function update_target_html(item_id)
{
	//登録対象者HTMLを更新
	_update_target_html2(item_id,document.getElementById("target_disp_area"+item_id),document.getElementById("target_id_list"+item_id),document.getElementById("target_name_list"+item_id));
}

function _update_target_html2(item_id,disp_obj,target_hidden_id,target_hidden_name)
{
	if(m_target_list[item_id].length == 0)
	{
		disp_obj.innerHTML = "";
		target_hidden_id.value = "";
		target_hidden_name.value = "";
	}
	else
	{
		var disp_obj_html = "";
		var target_hidden_id_value = "";
		var target_hidden_name_value = "";
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			var emp_id   = m_target_list[item_id][i].emp_id;
			var emp_name = m_target_list[item_id][i].emp_name;
			if(i!=0)
			{
				disp_obj_html = disp_obj_html + ",";
				target_hidden_id_value = target_hidden_id_value + ",";
				target_hidden_name_value = target_hidden_name_value + ",";
			}
			
			disp_obj_html = disp_obj_html + "<a href=\"javascript:void(0);\" onClick=\"delete_target('" + item_id + "','" + emp_id + "','" + emp_name + "')\">" + emp_name + "</a>";
			
			target_hidden_id_value = target_hidden_id_value + emp_id;
			target_hidden_name_value = target_hidden_name_value + emp_name;
		}
		disp_obj.innerHTML = disp_obj_html;
		target_hidden_id.value = target_hidden_id_value;
		target_hidden_name.value = target_hidden_name_value;
	}
}

//--------------------------------------------------
//配列追加
//--------------------------------------------------
function array_add(array_obj,add_obj)
{
	return array_obj.concat( new Array( add_obj ) );
}

//--------------------------------------------------
//m_target_list形式データをデバッグします。
//--------------------------------------------------
function debug_target_list(item_id,target_list)
{
	var str = "\n";
	if(target_list[item_id].length != 0)
	{
		for(var i=0;i<target_list[item_id].length;i++)
		{
			str = str + target_list[item_id][i].emp_id + "(" + target_list[item_id][i].emp_name + ")" + "\n";
		}
	}
	alert(str);
}

//===================================================================
//権限部分の初期制御
//===================================================================
//権限部分の初期制御
function onChangeArchive(
    init_flg,
    default_ref_class_src,
    default_ref_atrb_src,
    default_upd_class_src,
    default_upd_atrb_src
    )
{
    //参照可能部署の各リストボックスを構築します。
    setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src);

    //更新可能部署の各リストボックスを構築します。
    setUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src);

    //更新・参照権限部分の有効状態を制御します。
    setDisabled();
}

//===================================================================
// 所属リストボックスの構築
//===================================================================
//参照権限 - 部署の各リストボックスを構築します。　※この関数は子画面でも使用されます。
function setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src)
{
    //起動時の場合
    if (!init_flg)
    {
        //科のリストボックス項目を全削除
        deleteAllOptions(document.mainform.ref_dept);
    }

    //部のリストボックス項目を全削除
    deleteAllOptions(document.mainform.ref_class_src);

    //全ての部をリストボックスに項目追加します。
    addOption(document.mainform.ref_class_src, '-', '----------', default_ref_class_src);
    for (var i = 0, len = classes.length; i < len; i++) {
        addOption(document.mainform.ref_class_src, classes[i].id, classes[i].name, default_ref_class_src);
    }

    //課のリストボックスを再構築します。
    setRefAtrbSrcOptions(default_ref_atrb_src);
}

//参照権限 - 部のリストボックスを元に課のリストボックスを再構築します。
function setRefAtrbSrcOptions(default_ref_atrb_src)
{
    //課のリストボックス項目を全削除します。
    deleteAllOptions(document.mainform.ref_atrb_src);

    //選択されている部を取得
    var class_id = document.mainform.ref_class_src.value;

    //部に該当する全ての課をリストボックスに項目追加します。
    addOption(document.mainform.ref_atrb_src, '-', '----------', default_ref_atrb_src);
    if (atrbs[class_id])
    {
        for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
            addOption(document.mainform.ref_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_ref_atrb_src);
        }
    }

    //科のリストボックスを再構築します。
    setRefDeptSrcOptions();
}

//参照権限 - 課のリストボックスを元に科のリストボックスを再構築します。
function setRefDeptSrcOptions()
{
    //課のリストボックス項目を全削除します。
    deleteAllOptions(document.mainform.ref_dept_src);

    //選択されている部・課を取得
    var class_id = document.mainform.ref_class_src.value;
    var atrb_id = document.mainform.ref_atrb_src.value;

    //部・課に該当する全ての課をリストボックスに項目追加します。
    if (depts[class_id])
    {
        if (depts[class_id][atrb_id])
        {
            for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
                var dept_id = depts[class_id][atrb_id][i].id;
                var value = class_id + '-' + atrb_id + '-' + dept_id;
                addOption(document.mainform.ref_dept_src, value, depts[class_id][atrb_id][i].name);
            }
        }
        else if (atrb_id == '-')
        {
            for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
                var atrb_id = atrbs[class_id][i].id;
                if (depts[class_id][atrb_id]) {
                    for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
                        var dept_id = depts[class_id][atrb_id][j].id;
                        var value = class_id + '-' + atrb_id + '-' + dept_id;
                        addOption(document.mainform.ref_dept_src, value, depts[class_id][atrb_id][j].name);
                    }
                }
            }
        }
    }
}

//更新権限 - 部署の各リストボックスを構築します。　※この関数は子画面でも使用されます。
function setUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src)
{
    //起動時の場合
    if (!init_flg)
    {
        //科のリストボックス項目を全削除
        deleteAllOptions(document.mainform.upd_dept);
    }

    //部のリストボックス項目を全削除
    deleteAllOptions(document.mainform.upd_class_src);

    //全ての部をリストボックスに項目追加します。
    addOption(document.mainform.upd_class_src, '-', '----------', default_upd_class_src);
    for (var i = 0, len = classes.length; i < len; i++) {
        addOption(document.mainform.upd_class_src, classes[i].id, classes[i].name, default_upd_class_src);
    }

    //課のリストボックスを再構築します。
    setUpdAtrbSrcOptions(default_upd_atrb_src);
}

//更新権限 - 部のリストボックスを元に課のリストボックスを再構築します。
function setUpdAtrbSrcOptions(default_upd_atrb_src)
{
    //課のリストボックス項目を全削除します。
    deleteAllOptions(document.mainform.upd_atrb_src);

    //選択されている部を取得
    var class_id = document.mainform.upd_class_src.value;

    //部に該当する全ての課をリストボックスに項目追加します。
    addOption(document.mainform.upd_atrb_src, '-', '----------', default_upd_atrb_src);
    if (atrbs[class_id])
    {
        for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
            addOption(document.mainform.upd_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_upd_atrb_src);
        }
    }

    //科のリストボックスを再構築します。
    setUpdDeptSrcOptions();
}

//更新権限 - 課のリストボックスを元に科のリストボックスを再構築します。
function setUpdDeptSrcOptions()
{
    //課のリストボックス項目を全削除します。
    deleteAllOptions(document.mainform.upd_dept_src);

    //選択されている部・課を取得
    var class_id = document.mainform.upd_class_src.value;
    var atrb_id = document.mainform.upd_atrb_src.value;

    //部・課に該当する全ての課をリストボックスに項目追加します。
    if (depts[class_id])
    {
        if (depts[class_id][atrb_id]) {
            for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
                var dept_id = depts[class_id][atrb_id][i].id;
                var value = class_id + '-' + atrb_id + '-' + dept_id;
                addOption(document.mainform.upd_dept_src, value, depts[class_id][atrb_id][i].name);
            }
        } else if (atrb_id == '-') {
            for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
                var atrb_id = atrbs[class_id][i].id;
                if (depts[class_id][atrb_id]) {
                    for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
                        var dept_id = depts[class_id][atrb_id][j].id;
                        var value = class_id + '-' + atrb_id + '-' + dept_id;
                        addOption(document.mainform.upd_dept_src, value, depts[class_id][atrb_id][j].name);
                    }
                }
            }
        }
    }
}

//===================================================================
// リストボックスに対する一般的な処理
//===================================================================
//リストボックスに項目を追加します。　※この関数は子画面でも使用されます。
function addOption(box, value, text, selected)
{
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value)
    {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    if (!box.multiple)
    {
        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }
}

//リストボックスdest_boxにリストボックスsrc_boxの選択項目を追加します。
function addSelectedOptions(dest_box, src_box)
{
    //dest_boxの全項目とsrc_boxの選択項目を取得
    var options = new Array();
    for (var i = 0, j = dest_box.length; i < j; i++)
    {
        options[dest_box.options[i].value] = dest_box.options[i].text;
    }
    for (var i = 0, j = src_box.length; i < j; i++)
    {
        if (src_box.options[i].selected)
        {
            options[src_box.options[i].value] = src_box.options[i].text;
        }
    }

    //取得した項目をソート
    options.sort();
    
    //取得した項目でdest_boxを再作成
    deleteAllOptions(dest_box);
    for (var i in options)
    {
        addOption(dest_box, i, options[i]);
    }
}

//指定されたリストボックスを全選択状態にします。
function selectAllOptions(box)
{
    for (var i = 0, j = box.length; i < j; i++)
    {
        box.options[i].selected = true;
    }
}

//指定されたリストボックスの選択されている選択項目を全削除します。
function deleteSelectedOptions(box)
{
    for (var i = box.length - 1; i >= 0; i--)
    {
        if (box.options[i].selected)
        {
            box.options[i] = null;
        }
    }
}

//指定されたリストボックスの選択項目を全削除します。
function deleteAllOptions(box)
{
    for (var i = box.length - 1; i >= 0; i--)
    {
        box.options[i] = null;
    }
}

//===================================================================
// 権限部分の有効制御
//===================================================================
//参照・更新権限部分の制御を行います。
function setDisabled()
{
    var disabled;

    //参照可能部署役職指定有無「許可しない」は常に有効
    disabled = false;
    document.mainform.ref_dept_st_flg.disabled = disabled;

    //参照可能部署役職指定有無「許可しない」がチェックされている場合は、
    //参照可能部署指定「すべて」「指定する」を無効にする。
    disabled = !(!document.mainform.ref_dept_st_flg.disabled && !document.mainform.ref_dept_st_flg.checked);
    document.mainform.ref_dept_flg[0].disabled = disabled;
    document.mainform.ref_dept_flg[1].disabled = disabled;

    //参照可能部署指定「指定する」がチェックされていない場合は、
    //参照可能部署関連のフィールド、ボタンを無効にする。
    disabled = !(!document.mainform.ref_dept_flg[1].disabled && document.mainform.ref_dept_flg[1].checked);
    document.mainform.ref_dept_all.disabled = disabled;
    document.mainform.ref_class_src.disabled = disabled;
    document.mainform.ref_atrb_src.disabled = disabled;
    document.mainform.ref_dept_src.disabled = disabled;
    document.mainform.ref_dept.disabled = disabled;
    document.mainform.add_ref_dept.disabled = disabled;
    document.mainform.delete_ref_dept.disabled = disabled;
    document.mainform.delete_all_ref_dept.disabled = disabled;
    document.mainform.select_all_ref_dept.disabled = disabled;
    if ( $(".ref_btn").hasClass("button") || $(".ref_btn").hasClass("button_disabled") ){
      if (disabled){
        $(".ref_btn").removeClass("button");
        $(".ref_btn").addClass("button_disabled");
        $(".ref_arrow").removeClass("arrow");
        $(".ref_arrow").addClass("arrow_disabled");
      }
      else{
        $(".ref_btn").removeClass("button_disabled");
        $(".ref_btn").addClass("button");
        $(".ref_arrow").removeClass("arrow_disabled");
        $(".ref_arrow").addClass("arrow");
      }
    }
    
    //参照可能部署役職指定有無「許可しない」がチェックされている場合は、
    //参照可能役職指定「すべて」「指定する」を無効にする。
    disabled = (document.mainform.ref_dept_st_flg.disabled || document.mainform.ref_dept_st_flg.checked);
    document.mainform.ref_st_flg[0].disabled = disabled;
    document.mainform.ref_st_flg[1].disabled = disabled;

    //参照可能役職指定「指定する」がチェックされていない場合は、
    //参照可能役職のフィールドを無効にする。
    disabled = !(!document.mainform.ref_st_flg[1].disabled && document.mainform.ref_st_flg[1].checked);
    document.mainform.elements['ref_st[]'].disabled = disabled;

    //参照可能職員指定は常に有効
    disabled = false;
    document.mainform.emplist1.disabled = disabled;
    document.mainform.emplist1_clear.disabled = disabled;



    //更新可能部署役職指定有無は常に有効
    disabled = false;
    document.mainform.upd_dept_st_flg.disabled = disabled;

    //参更新可能部署役職指定有無「許可しない」がチェックされている場合は、
    //更新可能部署指定「すべて」「指定する」を無効にする。
    disabled = !(!document.mainform.upd_dept_st_flg.disabled && !document.mainform.upd_dept_st_flg.checked);
    document.mainform.upd_dept_flg[0].disabled = disabled;
    document.mainform.upd_dept_flg[1].disabled = disabled;

    //更新可能部署指定「指定する」がチェックされていない場合は、
    //更新可能部署関連のフィールド、ボタンを無効にする。
    disabled = !(!document.mainform.upd_dept_flg[1].disabled && document.mainform.upd_dept_flg[1].checked);
    document.mainform.upd_dept_all.disabled = disabled;
    document.mainform.upd_class_src.disabled = disabled;
    document.mainform.upd_atrb_src.disabled = disabled;
    document.mainform.upd_dept_src.disabled = disabled;
    document.mainform.upd_dept.disabled = disabled;
    document.mainform.add_upd_dept.disabled = disabled;
    document.mainform.delete_upd_dept.disabled = disabled;
    document.mainform.delete_all_upd_dept.disabled = disabled;
    document.mainform.select_all_upd_dept.disabled = disabled;
    if ( $(".upd_btn").hasClass("button") || $(".upd_btn").hasClass("button_disabled") ){
      if (disabled){
        $(".upd_btn").removeClass("button");
        $(".upd_btn").addClass("button_disabled");
        $(".upd_arrow").removeClass("arrow");
        $(".upd_arrow").addClass("arrow_disabled");
      }
      else{
        $(".upd_btn").removeClass("button_disabled");
        $(".upd_btn").addClass("button");
        $(".upd_arrow").removeClass("arrow_disabled");
        $(".upd_arrow").addClass("arrow");
      }
    }

    //更新可能部署役職指定有無「許可しない」がチェックされている場合は、
    //更新可能役職指定「すべて」「指定する」を無効にする。
    disabled = (document.mainform.upd_dept_st_flg.disabled || document.mainform.upd_dept_st_flg.checked);
    document.mainform.upd_st_flg[0].disabled = disabled;
    document.mainform.upd_st_flg[1].disabled = disabled;

    //更新可能役職指定「指定する」がチェックされていない場合は、
    //更新可能役職のフィールドを無効にする。
    disabled = !(!document.mainform.upd_st_flg[1].disabled && document.mainform.upd_st_flg[1].checked);
    document.mainform.elements['upd_st[]'].disabled = disabled;

    //更新可能職員指定は常に有効
    disabled = false;
    document.mainform.emplist2.disabled = disabled;
    document.mainform.emplist2_clear.disabled = disabled;
}

//===================================================================
// 権限部分の開封制御
//===================================================================
//参照・更新権限指定欄の表示有無を切り替え(旧デザイン用)
function toggle1(button)
{
    var display;
    if (button.innerHTML == '▼')
    {
        button.innerHTML = '▲';
        display = '';
    }
    else
    {
        button.innerHTML = '▼';
        display = 'none';
    }

    //配下のtrタグ３行の表示設定を更新
    for (var i = 1; i <= 3; i++)
    {
        document.getElementById(button.id.concat(i)).style.display = display;
    }
}

//参照・更新権限指定欄の表示有無を切り替え(新デザイン用)
function toggle2(button, id)
{
    var display;
    if (button.innerHTML == '▼')
    {
        button.innerHTML = '▲';
        display = '';
    }
    else
    {
        button.innerHTML = '▼';
        display = 'none';
    }

    //表示を更新
    $("#" + button.id + "_area").toggle();    
}

//===================================================================
// 子画面表示
//===================================================================
//全画面表示を行います。(div=ref:参照可能部署,upd=更新可能部署)
function showDeptAll(div, session)
{
    var emp_class = '';
    window.open('hiyari_el_select_dept_all.php?session=' + session + '&emp_class='.concat(emp_class).concat('&div=').concat(div), 'deptall', 'width=800,height=500,scrollbars=yes');
}

//保存先フォルダの選択画面を表示します。
function selectFolder(session, path)
{
    var a = document.mainform.archive.value;
    var c = document.mainform.category.value;
    var f = document.mainform.folder_id.value;
    window.open('hiyari_el_folder_select.php?session=' + session + '&a=' + a + '&c=' + c + '&f=' + f + '&path=' + path, 'newwin', 'width=640,height=700,scrollbars=yes');
}

//===================================================================
// 送信
//===================================================================
//画面内容を送信します。
function submitForm()
{
    //入力チェック(コンテンツ登録画面)
    if (document.mainform.file_count){
      for (var i = 1; i <= document.mainform.file_count.value; i++){
          if (document.mainform.elements['keywd' + i]){
              if (document.mainform.elements['keywd' + i].value == ''){
                  alert('キーワードが入力されていません。');
                  document.mainform.elements['keywd' + i].focus();
                  return;
              }
          }
      }
    }
    
    //入力チェック(コンテンツ更新画面)
    if (document.mainform.keywd){
        if (document.mainform.keywd.value == ''){
            alert('キーワードが入力されていません。');
            document.mainform.keywd.focus();
            return false;
        }
    }    
    
    //HIDDENタグを生成し、参照可能部署をセット
    var ref_dept_box = document.mainform.ref_dept;
    if (!ref_dept_box.disabled)
    {
        for (var i = 0, j = ref_dept_box.length; i < j; i++)
        {
            addHiddenElement(document.mainform, 'hid_ref_dept[]', ref_dept_box.options[i].value);
        }
    }
    //HIDDENタグを生成し、更新可能部署をセット
    var upd_dept_box = document.mainform.upd_dept;
    if (!upd_dept_box.disabled)
    {
        for (var i = 0, j = upd_dept_box.length; i < j; i++)
        {
            addHiddenElement(document.mainform, 'hid_upd_dept[]', upd_dept_box.options[i].value);
        }
    }

    //参照・更新権限の「▲▼」状態をHIDDENにセット
    document.mainform.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;
    document.mainform.upd_toggle_mode.value = document.getElementById('upd_toggle').innerHTML;

    //職員名簿画面を閉じる。
    closeEmployeeList();

    //送信
    document.mainform.submit();
}

//HIDDENオブジェクトを生成し送信フォームに追加します。
function addHiddenElement(frm, name, value)
{
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    frm.appendChild(input);
}
