function popupDetailBlue(values, box_width, label_width, e) {
	popupDetail(values, box_width, label_width, '#5279a5', '#f6f9ff', e);
}

function popupDetailPink(values, box_width, label_width, e) {
	popupDetail(values, box_width, label_width, '#c71585', '#ffe4e1', e);
}

function popupDetailYellow(values, box_width, label_width, e) {
	popupDetail(values, box_width, label_width, '#b8860b', '#fffacd', e);
}

function popupDetailGreen(values, box_width, label_width, e) {
	popupDetail(values, box_width, label_width, '#13781D', '#DFFFDC', e);
}


function popupDetail(values, box_width, label_width, strong_color, pale_color, e) {
	var div = document.getElementById('popup');
	if (!div) {
		var selects = document.getElementsByTagName('select');
		for (var i = 0, j = selects.length; i < j; i++) {
			selects[i].style.visibility = 'hidden';
		}

		var table = document.createElement('table');
		table.style.borderCollapse = 'collapse';
		table.style.width = '100%';
		for (var i = 0, j = values.length; i < j; i += 2) {
			addDetailRow(table, values[i], values[i + 1], strong_color, pale_color);
		}
		table.rows[0].cells[0].style.width = String(label_width).concat('px');

		div = document.createElement('div');
		div.id = 'popup';
		div.style.position = 'absolute';
		div.style.width = String(box_width).concat('px');
//		div.style.borderColor = '#5279a5';
		div.style.borderColor = '#35B341';
		div.style.borderStyle = 'solid';
		div.style.borderWidth = '3px';
		div.style.backgroundColor = '#ffffff';
		div.style.padding = '3px';
		div.appendChild(table);
		document.body.appendChild(div);
	}

	var bodyWidth = document.body.clientWidth;
	var bodyHeight;
	var iframe = top.document.getElementById('floatingpage');
	if (iframe) {
		var iframeOffsetTop = getOffsetTopFromBody(iframe);
		
		var scroll_top = top.document.body.scrollTop;
		var top_shift = iframeOffsetTop - scroll_top;
		bodyHeight = top.document.body.clientHeight - top_shift;

//		bodyHeight = top.document.body.clientHeight - iframeOffsetTop;

	} else {
		bodyHeight = top.document.body.clientHeight;
	}

	if (e == undefined) {
		e = window.event;
	}

	var x = e.clientX + 8;
	var y = e.clientY + 8;

	if (x + div.clientWidth + 6 > bodyWidth) {
		x -= (div.clientWidth + 16);
		if (x < 0) {x = 0;}
	}
	if (y + div.clientHeight + 6 > bodyHeight) {
		y -= (y + div.clientHeight - bodyHeight + 16);
		if (y < 0) {y = 0;}
	}

	div.style.left = x;
	div.style.top = y + document.body.scrollTop;
}

function addDetailRow(table, header, value, border_color, header_color) {
	var row = table.insertRow(table.rows.length);
	row.style.height = '22px';
	addDetailCell(row, header, border_color, header_color);
	addDetailCell(row, value, border_color, '#ffffff');
}

function addDetailCell(row, str, border_color, bg_color) {
	var cell = row.insertCell(row.cells.length);
	cell.style.borderColor = border_color;
	cell.style.borderStyle = 'solid';
	cell.style.borderWidth = '1px';
	cell.style.padding = '2px';
	cell.style.backgroundColor = bg_color;
	cell.style.color = '#000000';
	cell.style.fontFamily = '�ͣ� �Х����å�, Osaka';
//	cell.className = 'j12';
	cell.innerHTML = str;
}

function closeDetail() {
	var div = document.getElementById('popup');
	if (div) {
		document.body.removeChild(div);
	}

	var selects = document.getElementsByTagName('select');
	for (var i = 0, j = selects.length; i < j; i++) {
		selects[i].style.visibility = '';
	}
}

function getOffsetTopFromBody(element) {
	var offset = 0;
	while (element.offsetParent) {
		offset += element.offsetTop;
		element = element.offsetParent;
	}
	return offset;
}
