function search_training(){
	document.training_form.action = "#";
	document.training_form.target = '';
	document.training_form.submit();
}

function delete_training(){
	if(!confirm('選択した研修を削除します。よろしいですか？')){
		return;
	}

	document.training_form.action = "#";
	document.training_form.target = '';
	document.training_form.delete_flg.value = 'true';
	document.training_form.submit();
}

function insert_training(session){
	// 暫定対応(課題：IEだとモーダルでは画面サイズ変更ができない？)
	//var url = "cl_training_insert.php?session=" + session;
	//window.showModalDialog(url, this, 'dialogLeft:0;dialogTop:0;dialogWidth:900;dialogHeight:700;scroll:yes;resizable:no;maximize:no;minimize:no');
	var dx = screen.width;
	var dy = screen.top;
	var base = 0;
	var wx = 900;
	var wy = 700;
	window.open('', 'insert_training', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=no');
	document.training_form.action="cl_training_insert.php";
	document.training_form.target = "insert_training";
	document.training_form.submit();
}

function update_training(session,id){
	// 暫定対応(課題：IEだとモーダルでは画面サイズ変更ができない？)
	//var url = "cl_training_update.php?session=" + session + "&training_id=" + id;
	//window.showModalDialog(url, this, 'dialogLeft:0;dialogTop:0;dialogWidth:900;dialogHeight:700;scroll:yes;resizable:no;maximize:no;minimize:no');
	var dx = screen.width;
	var dy = screen.top;
	var base = 0;
	var wx = 900;
	var wy = 700;
	window.open('', 'update_training', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=no');
	document.training_form.action="cl_training_update.php";
	document.training_form.target = "update_training";
	document.training_form.training_id.value = id;
	document.training_form.submit();
}

//function open_plan_list(p_login_user,p_training_id){
function open_plan_list_org(p_training_id,p_session){

	alert('open_plan_list Called !! p_training_id:' + p_training_id + " p_session:" + p_session);

	//dx = screen.width;
	//dy = screen.top;
	//base = 0;
	//wx = 600;
	//wy = 400;

	/** 呼出（画面サイズ、画面名、モーダレスは仮設定） */
	//window.open('', 'training_plan', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	//document.training_form.training_id.value = id;
	//document.training_form.action = "./cl/common/cl_training_plan_regist_1.php";
	//document.training_form.target = "training_plan";
	//document.training_form.submit();

	//子画面のモジュール名
	var moduleName = "cl_training_plan_select_1";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	//params.gamen_mode = "training_select";
	//params.head_nurse_recommend_flg = "false";
	//params.login_user = p_login_user;
	params.training_id = p_training_id;
//	params.session = document.training_form.session.value;
//	params.session = p_session;



	//params.plan_id_csv = "";

	//ウィンドウサイズなどのオプション
	//var h = window.screen.availHeight;
	//var w = window.screen.availWidth;
	//w = 1024;
	var w = 1080;
	var h = 600;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	//commonOpen(moduleName,params,option);
	var wid = moduleName;
	commonOpen(moduleName,params,option,wid,p_session);


}

function open_plan_regist(id){

	var moduleName = "cl_training_plan_regist_1";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.training_id = id;
	params.session = document.training_form.session.value;

	//ウィンドウサイズなどのオプション
	var h = window.screen.availHeight;
	var w = window.screen.availWidth;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option);
}

function csv_print(){
	document.training_form.action = "cl_training_management_csv.php";
	document.training_form.target = 'csv_form';
	document.training_form.submit();
}