function toInt(s) { if (!s) return 0; var ret = parseInt(s, 10); if(isNaN(ret)) return 0; return parseInt(s, 10); }
function clearZero(s) { if (!s || s==="0") return ""; return s; }
function selectValue (id) { if (!elems[id] || !elems[id].options) alert(id); return elems[id].options[elems[id].options.selectedIndex].value; }
function selectText (id) { if (!elems[id] || !elems[id].options) alert(id); return elems[id].options[elems[id].options.selectedIndex].text; }
function checkValue (id) { if (!elems[id]) alert(id); return elems[id].checked ? elems[id].value : ""; }
function textValue (id) { if (!elems[id]) alert(id); return elems[id].value; }
function str(s) { if (!s && s!=0) return ""; return s; }
function tabAryValue (tabStr, idx) { var cms = ""; for (var i=0;i<idx;i++) cms+="\t"; return str((tabStr+cms).split("\t")[idx-1]); }
function wbr (s) { return s.split("").join("<wbr>"); }
function selectSelect (id, v) {
  if (!elems[id]) alert(id);
  var dropdown = elems[id];
  for (var i = 0; i < dropdown.options.length; i++){
    if (dropdown.options[i].value != v) continue;
    dropdown.options.selectedIndex = i;
    return;
  }
  dropdown.options.selectedIndex = 0;
}
function checkboxCheck (id, v) {
  var checkbox = elems[id];
  checkbox.checked = (v ? "checked" : "");
}

function call_back_calendar_select(suffix, result) {
  if (suffix==1){
    var yy = elems["yoho_touyo_start_ymd_y"];
    var mm = elems["yoho_touyo_start_ymd_m"];
    var dd = elems["yoho_touyo_start_ymd_d"];
  }
  if (suffix==2){
    var yy = elems["force_end_ymd_y"];
    var mm = elems["force_end_ymd_m"];
    var dd = elems["force_end_ymd_d"];
  }
  for (var i=0; i<yy.options.length; i++) if (yy.options[i].value==result.year) yy.options.selectedIndex = i;
  for (var i=0; i<mm.options.length; i++) if (mm.options[i].value==result.month) mm.options.selectedIndex = i;
  for (var i=0; i<dd.options.length; i++) if (dd.options[i].value==result.day)   dd.options.selectedIndex = i;
}


var EMPTY_MEDINFO = {"seq":"",
                     "status":"",
                     "med_name":"",
                     "yj_cd":"",
                     "remark":"",
                     "yoryo":"",
                     "med_unit_name":"",
                     "units_separated_tab":"",
                     "seibun_bunrui":"",
                     "zaikei_bunrui":"",
                     "yoryo_type":"",
                     "med_chozai_siji":"",
                     "med_kouhatu_fuka":""};
var EMPTY_YOHOINFO = {};
var SEIBUN_BUNRUI_CAPTIONS = {"":"","N":"内服","C":"注射","G":"外用"};
var ZAIKEI_BUNRUI_CAPTIONS = {"":"","S":"散剤","J":"錠剤","C":"ｶﾌﾟｾﾙ","E":"液剤","Z":"その他"};
var YOHO_KUBUN_CAPTIONS = {"N":"内服","G":"外用","P":"頓服","Y":"頓用","C":"注射"};
var NAIYO_TIMING_CAPTIONS = ["","起床時","眠前","朝","昼","夕","朝〜昼","昼〜夕","夕〜眠前"];
var NAIYO_MEAL_TIMING_CAPTIONS = ["","食前","食直前","食直後","食後","食中","食間"];
var CHOZAI_SIJI_CAPTIONS = {"J":"錠剤","K":"懸濁","F":"粉砕","I":"一包化", "H":"はずし"};
var YORYO_TYPE_CAPTIONS = {"1":"1日","2":"1回"};

var elems = {};
var rpInfo = {1:{"isMedEmpty":"1","isYohoEmpty":"1","medIdx":0,"yohoIdx":0,"medInfo":[EMPTY_MEDINFO],"yohoInfo":[EMPTY_YOHOINFO]}};
var tableInfo = [{"rp":"1","typeMY":"med","typeIdx":"1"}];
var medExtent = "hist";
var treeviewInfo = [];
var yoho_kubun = "N";
var isSet = "";
var listType = "tabletype";
var fukintouType = "";
var fukintou_naiyo_timing = false;
var fukintou_naiyo_meal_timing = false;
var fukintou_naiyo_hours = false;
var yoho_naiyo_per_day = 0;

var currentRP = 1;
var currentINFO = 1;
var currentTypeMy = 1;
var currentMedSeq = 0;
var currentLine = 1;
var maxRP = 1;
var treeViewListedOnce = false;
var medSearchStocker = {};
var setSearchStocker = {};
var currentTreeSearchKaisou = 1;
var currentTreeSearchParentId = 0;
var initialSearchd = false;
var oscnt_ymd_k = "";
var cyouzai_bi = "";
var maxYohoNaiyoHours = 6;  // 最大時刻指定枠数
var default_link_med = "";  // 薬品設定のデフォルト表示タブ(P:処方薬 C:採用薬)
var is_med_search_remark = false;  // 薬品検索時に「別名・備考欄」も検索するか(true:検索する false:検索しない)
var is_add_remark_rp = true;  // RPに「別名・備考」を追加するか(true:追加する false:追加しない)
var is_input_injection_content = false; // 用法内容入力(true:入力あり false:入力なし)

var ajaxObj = null;

//**************************
// 初期化
//**************************
function load_end_action(){
  if (window.parent.showLeftHistoryArea && !is_template_readonly) window.parent.showLeftHistoryArea(false);
}
function load_action() {
    for (i = 0; i < idlist.length; i++) elems[idlist[i]] = document.getElementById(idlist[i]);

    if (is_syoho_cyusya=="C"){
        elems["rdo_seibun_bunrui_c"].checked = "checked";
        elems["current_seibun_bunrui"].value = "C";
        yoho_kubun = "C";
    }

    if (is_template_readonly) {
        elems["td_template_rightarea"].style.display = "none";
        elems["td_template_rightarea"].style.display = "none";
        elems["btn_byoumei_select"].style.display = "none";
        elems["btn_problem_select"].style.display = "none";
        if(elems["btn_rp_add"]) elems["btn_rp_add"].style.display = "none";
        if(elems["btn_rp_del"]) elems["btn_rp_del"].style.display = "none";
    }

    selectSelect("yoryo_type", yoryo_type);
    changeDisplay("", "");
    // 薬品設定のデフォルト表示が設定されている場合
    if (default_link_med) {
        changeDisplay("med", document.getElementById(default_link_med));
    }

    loadOrder(schedule_seq, 0);
}

function InputCheck(){
  setToRP("","");
  if (!rpInfo) { alert("オーダを指定してください。"); return false; }

  var idxList = [];
  for (var idx in rpInfo) idxList.push(idx);
  for (var idx2 = idxList.length-1; idx2>=0; idx2--) {
    var idx = idxList[idx2];
    if (rpInfo[idx].isMedEmpty && rpInfo[idx].isYohoEmpty && !rpInfo[idx].isSavedData) {
        currentRP = idx;
        delRP();
        continue;
    }
//  if (rpInfo[idx].medInfo[i].status  != 9 && rpInfo[idx].isMedEmpty)  { alert("RP"+idx+"の薬品を指定してください。"); return false; }
//  if (rpInfo[idx].yohoInfo[i].status != 9 && rpInfo[idx].isYohoEmpty) { alert("RP"+idx+"の用法を指定してください。"); return false; }
    if (rpInfo[idx].isMedEmpty)  { alert("RP"+idx+"の薬品を指定してください。"); return false; }
    if (rpInfo[idx].isYohoEmpty) { alert("RP"+idx+"の用法を指定してください。"); return false; }
  }

  if (rpInfo[1].isMedEmpty) { alert("薬品を指定してください。"); return false; }
  if (rpInfo[1].isYohoEmpty) { alert("用法を指定してください。"); return false; }
  return true;
}

//**************************
// サーバ送信共通関数
//**************************
/*********
 * receiverに入れられたオブジェクトにsum_ordsc_ajax_for_template.phpへの要求結果が渡され、
 * 再表示のためのデータが組み立てられる。
 */
function inquiryAjax(receiver, param){
  if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
  else {
    try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
    catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
  }
  ajaxObj.onreadystatechange = receiver;
  ajaxObj.open("POST", "sum_ordsc_ajax_for_template.php", true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=EUC-JP');
  ajaxObj.send("session="+ session + param);
}

//**************************
// サーバ受信共通関数
//**************************
function checkReceiveAjax(){
  if (ajaxObj == null) return false;
  if (ajaxObj.readyState != 4) return false;
  if (typeof(ajaxObj.status) != "number") return false;
  if (ajaxObj.status != 200) return false;
  if (!ajaxObj.responseText) return false;

  response = ajaxObj.responseText;
  ajaxObj = null;
  response = response.replace(/\r/g, "\\r").replace(/\n/g, "\\n");
  //response = response.replace(/\r\n/g, "\n");//IE
  //response = response.replace(/\r\n/g, "<br/>");
  //response = response.replace(/\n/g, "");//firefox

  try{
    var ret = false;
    eval("ret = " + response);
    if (ret.error) {
      alert(ret.error);
      return false;
    }
  }
  catch(e){ alert("[response invalid]\n"+response); return false; }
  if (!ret) return false;
  return ret;
}

//**************************
// オーダサーチ
//**************************
function loadOrder(load_request_schedule_seq, load_request_set_seq){
  var param = "";
  if (load_request_schedule_seq) param += "&load_request_schedule_seq=" + load_request_schedule_seq;
  else if (load_request_set_seq) param += "&load_request_set_seq=" + load_request_set_seq;
  inquiryAjax(returnLoadOrder, param); // inquiryAjaxで取得した要求結果が、returnLoadOrderで処理される。
}

//**************************
// オーダサーチの応答
//**************************
function returnLoadOrder(){
    var ret = checkReceiveAjax();
    if (!ret) return;
    var curRP = 0;
    if (ret.order) {
        elems["byoumei"].value = ret.order.byoumei ? ret.order.byoumei : "";
        elems["byoumei_code"].value = ret.order.byoumei_code ? ret.order.byoumei_code : "";
        elems["icd10"].value = ret.order.icd10 ? ret.order.icd10 : "";
    }
    if (ret.rp) {
        if (maxRP==1 && rpInfo[maxRP].isMedEmpty) maxRP = 0;
        for (var rpidx=0; rpidx < ret.rp.length; rpidx++){
            var rp = ret.rp[rpidx];
            var curRP = parseInt(rp.rp_seq) + parseInt(maxRP);
            if (!rpInfo[curRP]) rpInfo[curRP] = {"isMedEmpty":"1","isYohoEmpty":"1","medIdx":0,"yohoIdx":0,"medInfo":[EMPTY_MEDINFO],"yohoInfo":[EMPTY_YOHOINFO]}
            if (rp.rp_line_div=="M" && parseInt(rp.med_seq_cd)) {
                delete(rpInfo[curRP].isMedEmpty);
                if (!currentMedSeq) currentMedSeq = rp.med_seq_cd;
                if(rpidx == 0) elems["schedule_seq"].value = rp.schedule_seq;

                rpInfo[curRP].medInfo[rpInfo[curRP].medIdx] = {
                    "schedule_seq" : rp.schedule_seq,
                    "seq" : rp.med_seq_cd,
                    "status" : (rp.status ? rp.status : 0),
                    "default_status" : (rp.status ? rp.status : 0),
                    "med_name" : rp.med_name,
                    "yj_cd" : (rp.yj_cd ? rp.yj_cd : ""),
                    "seibun_bunrui" : rp.seibun_bunrui,
                    "zaikei_bunrui" : rp.zaikei_bunrui,
                    "remark" : rp.remark,
                    "yoryo" : (rp.med_yoryo ==0.0) ? "" : parseFloat(rp.med_yoryo),
                    "med_unit_name" : rp.med_unit_name,
                    "units_separated_tab" : rp.units_separated_tab,
                    "yoryo_type" : rp.yoryo_type,
                    "med_chozai_siji" : rp.med_chozai_siji,
                    "med_kouhatu_fuka" : rp.med_kouhatu_fuka
                };
                medSearchStocker[rp.med_seq_cd] = {
                    "schedule_seq" : rp.schedule_seq,
                    "status" : (rp.status ? rp.status : 0),
                    "default_status" : (rp.status ? rp.status : 0),
                    "med_name" : rp.med_name,
                    "yj_cd" : (rp.yj_cd ? rp.yj_cd : ""),
                    "remark" : rp.remark,
                    "seibun_bunrui" : rp.seibun_bunrui,
                    "zaikei_bunrui" : rp.zaikei_bunrui,
                    "med_unit_name" : rp.med_unit_name,
                    "units_separated_tab": rp.units_separated_tab
                };

                rpInfo[curRP].medInfo.push({});
                if (edit_flg=="1") rpInfo[curRP].isSavedData = 1;
                else {
                    rpInfo[curRP].medInfo[rpInfo[curRP].medIdx].default_status = 0;
                    rpInfo[curRP].medInfo[rpInfo[curRP].medIdx].status = 0;
                }
                rpInfo[curRP].medIdx++;
            }
            if (rp.rp_line_div=="Y") {
                delete(rpInfo[curRP].isYohoEmpty);
                rpInfo[curRP].yohoInfo.push({});
                rpInfo[curRP].yohoInfo[rpInfo[curRP].yohoIdx] = rp;
                rpInfo[curRP].yohoInfo[rpInfo[curRP].yohoIdx].default_status = (rp.status ? rp.status : 0);
                if (edit_flg=="1") rpInfo[curRP].isSavedData = 1;
                else {
                    rpInfo[curRP].yohoInfo[rpInfo[curRP].yohoIdx].default_status = 0;
                    rpInfo[curRP].yohoInfo[rpInfo[curRP].yohoIdx].status = 0;
                }
                rpInfo[curRP].yohoIdx++;
            }
        }
    }
    if (curRP) {
        maxRP = curRP;
        setToRP("","");
    }
    if (!initialSearchd) {
        initialSearchd = true;
        medSearch();
    }
}

//**************************
// 薬品サーチ（ツリー用）
//**************************
function medSearchForTreeView(kaisou, parentId){

  if (!parentId && treeViewListedOnce) return;
  /*
  if (parentId && document.getElementById(parentId).style.display != "none"){
    document.getElementById(parentId).style.display = "none";
    document.getElementById(parentId).innerHTML = "";
    return;
  }
*/
  currentTreeSearchKaisou = kaisou;
  currentTreeSearchParentId = parentId;
  medSearch();
}

//**************************
// 薬品サーチ（一般用）
//**************************
function medSearchWithClearTree(){
  currentTreeSearchKaisou = 1;
  currentTreeSearchParentId = 0;
  medSearch();
}
//**************************
// 薬品サーチ（共通）
//**************************
function medSearch(){

    var medName = textValue("sel_med_name");
    if (listType != "treetype") {
        if (medName.length < 3) {
          if (medName.length > 0) alert("薬品名は3文字以上入力してください");
          return;
        }
    }
    var seibunBunrui = "0";
    if (elems["rdo_seibun_bunrui_n"].checked) seibunBunrui = "N";
    if (elems["rdo_seibun_bunrui_c"].checked) seibunBunrui = "C";
    if (elems["rdo_seibun_bunrui_g"].checked) seibunBunrui = "G";
    if (elems["rdo_seibun_bunrui_a"].checked) seibunBunrui = "A";


    var param = "";
    param += "&med_extent=" + medExtent;
    param += "&med_extent_hist_option=" + selectValue("med_extent_hist_option");
    param += "&pt_id="+pt_id;
    param += "&seibun_bunrui=" + encodeURIComponent(encodeURIComponent(seibunBunrui));
    param += "&med_name=" + encodeURIComponent(encodeURIComponent(medName.toKatakanaCase()));
    param += "&list_type=" + listType;
    param += is_med_search_remark ? "&is_med_search_remark=true" : "&is_med_search_remark=false";    // 薬品検索時に「別名・備考欄」も検索するか


    if (listType=="treetype") {
        param += "&yakkou_bunrui_kaisou="+currentTreeSearchKaisou;

        if (currentTreeSearchParentId){
            var ids_str = String(currentTreeSearchParentId);
            var ids = ids_str.split("_");
            if (ids[2]) param += "&yakkou_bunrui_code1=" + ids[2];
            if (ids[3]) param += "&yakkou_bunrui_code2=" + ids[3];
            if (ids[4]) param += "&yakkou_bunrui_code3=" + ids[4];

        /*  param += "&yakkou_bunrui_code1=1";
            param += "&yakkou_bunrui_code2=1";
            param += "&yakkou_bunrui_code3=1";
         */
        }
    }

    elems["div_med_tabletype_search_result"].style.display = "";
    inquiryAjax(returnMedSearch, param);
}

//**************************
// 薬品サーチ結果コールバック
//**************************
function returnMedSearch(){
  var ret = checkReceiveAjax();
  if (!ret) return;

  //==========================
  // ツリーの場合
  //==========================
  if (ret.list_type=="treetype"){

    var parentId = "div_treeview";
    var parentLabel = "";
    if (ret.yakkou_bunrui_kaisou >= 2) { parentId += "_" + ret.code1; parentLabel = ret.code1; }
    if (ret.yakkou_bunrui_kaisou >= 3) { parentId += "_" + ret.code2; parentLabel += "."+ret.code2; }
    if (ret.yakkou_bunrui_kaisou >= 4) { parentId += "_" + ret.code3; parentLabel = ""; }
    var out = [];
    var treenode = document.getElementById(parentId);
    //out.push(ret.list.length);
    //out.push(ret.yakkou_bunrui_kaisou);

    for (var idx = 0; idx < ret.list.length; idx++){
      var row = ret.list[idx];

      if (ret.yakkou_bunrui_kaisou==4) {
        out.push("<div id='"+parentId+"_"+row.seq+"_title' style='padding-left:"+((ret.yakkou_bunrui_kaisou-1)*15)+"px;'>#"+(parseInt(idx+1))+"&nbsp;");
        out.push("<a class='always_blue' href='' onclick='medListSelected(\""+row.seq+"\"); return false;'>"+row["med_name"]+"</a>");
        out.push("</div>");
      }
      else {
        var code = row["code"+ret.yakkou_bunrui_kaisou];
        if (ret.yakkou_bunrui_kaisou==3)  var label = row.yakkou_bunrui_cd;
        else var label = parentLabel+"."+code;
        out.push("<div id='"+parentId+"_"+code+"_title' style=padding-left:"+((ret.yakkou_bunrui_kaisou-1)*15)+"px;>"+label+"&nbsp;");
        if (!row.medCount){
          out.push(row["caption"] + "&nbsp;(0)");
        } else {
          var cmd = "medSearchForTreeView("+parseInt(ret.yakkou_bunrui_kaisou*1+1*1)+", \""+parentId+"_"+code+"\");";
      //    out.push("<a class='always_blue' href='' onclick='"+cmd+" return false;'>"+row["caption"]+"</a>&nbsp;("+row.medIdx+")");
          out.push("<a class='always_blue' href='' onclick='"+cmd+" return false;'>"+row["caption"]+"</a>&nbsp;("+row.medCount+")");
        }
        out.push("</div>");
      }

      out.push("<div id='"+parentId+"_"+code+"' style='display:none'></div>");

      if (ret.yakkou_bunrui_kaisou==4){
        var yj_cd = row.yj_cd ? row.yj_cd : "";
        medSearchStocker[row.seq] = {
          "schedule_seq"   : row.schedule_seq,
          "med_name":row.med_name,
          "yj_cd":row.yj_cd,
          "remark":row.remark,
          "seibun_bunrui":row.seibun_bunrui,
          "zaikei_bunrui":row.zaikei_bunrui,
          "med_unit_name" : row.med_unit_name,
          "units_separated_tab":row.units_separated_tab
        };
      }
    }
    treenode.innerHTML = out.join("\n");
    treenode.style.display = "";
    treeViewListedOnce = true;
    ajaxObj = null;
    currentTreeSearchKaisou = parseInt(currentTreeSearchKaisou)+1;
    return;
  }

  //==========================
  // ツリーでない場合
  //==========================
  var dat = [];
  dat.push('<table class="med_table" style="width:600px"><tbody>');
  for (var idx = 0; idx < ret.list.length; idx++){
    elems["med_tabletype_header_unit"].style.display = (medExtent=="hist" ? "" : "none");
    var row = ret.list[idx];
    var yj_cd = row.yj_cd ? row.yj_cd : "";
    medSearchStocker[row.seq] = {
      "schedule_seq" : row.schedule_seq,
      "status":row.status,
      "default_status":row.status,
      "med_name":row.med_name,
      "yj_cd":yj_cd,
      "remark":row.remark,
      "seibun_bunrui":row.seibun_bunrui,
      "zaikei_bunrui":row.zaikei_bunrui,
      "med_unit_name" : row.med_unit_name,
      "units_separated_tab":row.units_separated_tab
    };
    dat.push('<tr onclick="medListSelected('+row.seq+');" onmouseover="this.style.backgroundColor=\'#fefc8f\'" onmouseout="this.style.backgroundColor=\'#fff\'">');
    dat.push('<th style="width:320px">'+row.med_name+'</th>');
    if (medExtent=="hist") dat.push('<td style="width:50px">'+row.med_unit_name+'</td>');
    dat.push('<td>'+row.remark+'</td>');
    dat.push('<input type="hidden" name="yj_cd" value="'+yj_cd+'">');
    dat.push('</tr>');
  }
  dat.push('</tbody></table>');
  elems["div_med_tabletype_search_result"].style.display = "";
  elems["div_med_tabletype_search_result"].innerHTML = dat.join("\n");
  ajaxObj = null;
}

//**************************
// セットの登録
//**************************
function setRegist(){
  var setName = textValue("new_set_name");
  if (setName=="") { alert("セット名を指定してください。"); return; }
  var param = "";
  param += "&is_rinji_teiki="+is_rinji_teiki;
  param += "&is_syoho_cyusya="+is_syoho_cyusya;
  param += "&set_request=regist";
  param += "&new_set_name=" + encodeURIComponent(encodeURIComponent(setName));
  param += "&new_set_extent_option=" + selectValue("new_set_extent_option");
  param += "&pt_id="+pt_id;
  param += "&ordsc_basedata_stream=" + encodeURIComponent(encodeURIComponent(textValue("ordsc_basedata_stream")));
  param += "&ordsc_rpdata_stream=" + encodeURIComponent(encodeURIComponent(textValue("ordsc_rpdata_stream")));
  inquiryAjax(returnSetRegist, param);
}

//**************************
// セットの登録結果コールバック
//**************************
function returnSetRegist(){
  var ret = checkReceiveAjax();
  if (!ret) return;
  if (ret.result=="ok"){
    elems["new_set_name"].value = "";
    elems["current_set_name"].value = "";
    elems["sel_set_extent_option"].options.selectedIndex = elems["new_set_extent_option"].options.selectedIndex;
    elems["current_set_seq"].value = ret.set_seq;
    setSearch();
  }
}

//**************************
// セットの削除
//**************************
function setDelete(){
  if (!confirm("指定したセットを削除します。よろしいですか？")) return;
  inquiryAjax(returnSetDelete, "&set_request=delete&set_seq=" + textValue("current_set_seq"));
}

//**************************
// セットの削除結果コールバック
//**************************
function returnSetDelete(){
  var ret = checkReceiveAjax();
  if (!ret) return;
  if (ret.result=="ok"){
    elems["current_set_name"].value = "";
    elems["current_set_seq"].value  = "";
    setSearch();
  }
  setButtonEnableCheck();
}

//**************************
// セット一覧のサーチ
//**************************
function setSearch(){
  var setName = elems["sel_set_name"].value;
  var param = "";
  param += "&set_request=list";
  param += "&set_name=" + encodeURIComponent(encodeURIComponent(setName));
  param += "&set_extent_option=" + selectValue("sel_set_extent_option");
  param += "&pt_id="+pt_id;
  inquiryAjax(returnSetSearch, param);
}

//**************************
// セット一覧サーチ結果コールバック
//**************************
function returnSetSearch(){
  var ret = checkReceiveAjax();
  if (!ret) return;
  var currentSetSeq = elems["current_set_seq"].value;
  var dat = [];
  dat.push('<table class="med_table"><tbody>');
  for (var idx = 0; idx < ret.list.length; idx++){
    var row = ret.list[idx];
    setSearchStocker[row.set_seq] = row.set_name;
    dat.push('<tr onclick="setListSelected('+row.set_seq+');" onmouseover="this.style.backgroundColor=\'#fefc8f\'" onmouseout="this.style.backgroundColor=\'#fff\'">');
    dat.push('<th>'+row.set_name+'</th>');
    dat.push('</tr>');
    if (currentSetSeq==row.set_seq) elems["current_set_name"].value = row.set_name;
  }
  dat.push('</tbody></table>');
  elems["div_set_tabletype_search_result"].innerHTML = dat.join("\n");
  ajaxObj = null;
}

//**************************
// セットテーブルのクリック
//**************************
function setListSelected(set_seq){
  var setName = setSearchStocker[set_seq];
  elems["current_set_seq"].value = set_seq;
  elems["current_set_name"].value = setName;
  setButtonEnableCheck();
}

//**************************
// 薬品検索結果テーブルのクリック
//**************************
function medListSelected(seq){
    var medInfo = medSearchStocker[seq];
    elems["current_med_seq"].value = seq;
    elems["current_seibun_bunrui"].value = medInfo.seibun_bunrui;
    elems["current_zaikei_bunrui"].value = medInfo.zaikei_bunrui;
    elems["current_status"].value = (medInfo.status ? medInfo.status : "0");
    elems["current_yj_cd"].value = medInfo.yj_cd ? medInfo.yj_cd : "";
    elems["current_med_name"].value = medInfo.med_name;
    elems["current_remark"].value = medInfo.remark;
    elems["current_yoryo"].value = "";
    var units = medInfo.units_separated_tab.split(",");
    var unit = medInfo.med_unit_name;
    if (unit=="") {
        unit = (units.length == 1 ? units[0]: "");
    }
    elems["current_unit"].value = unit;
    if (elems["current_med_chozai_siji_f"]) {
        elems["current_med_chozai_siji_f"].checked = false;
    }
    if (elems["current_med_chozai_siji_h"]) {
        elems["current_med_chozai_siji_h"].checked = false;
    }
    var c = SEIBUN_BUNRUI_CAPTIONS[medInfo.seibun_bunrui];
    elems["current_seibun_bunrui_disp"].innerHTML = c ? "("+c+")" : "";
    var z = ZAIKEI_BUNRUI_CAPTIONS[medInfo.zaikei_bunrui];
    elems["current_zaikei_bunrui_disp"].innerHTML = z ? "("+z+")" : "";

    standbyCurrentUnitSelection(medInfo);

    changeDisplay("", "");
    elems["current_yoryo"].focus();
}

//**************************
// 薬品ボタンEnableチェック
//**************************
function medYohoButtonEnableCheck(){
  isEntryOk = true;
  isDeleteOk = false;
  isModifyOk = false;
  var medSeq = elems["current_med_seq"].value;
  if (medSeq && currentMedSeq) { isDeleteOk = true; isModifyOk = true; }
  if (!medSeq) isEntryOk = false;
  if (!elems["current_med_name"].value) isEntryOk = false;
  elems["btn_med_entry"].disabled = (isEntryOk ? "" : "disabled");
  elems["btn_med_delete"].disabled = (isDeleteOk ? "" : "disabled");
  elems["btn_med_modify"].disabled = (isModifyOk ? "" : "disabled");
  if (rpInfo[currentRP]) {
  elems["btn_yoho_delete"].disabled = (rpInfo[currentRP].isYohoEmpty ? "disabled" : "");
    }
}

//**************************
// セットボタンEnableチェック
//**************************
function setButtonEnableCheck(){
  isAddOk = (isSet ? true : false);
  isDelOk = (isSet ? true : false);
  var setSeq = elems["current_set_seq"].value;
  if (!setSeq) { isAddOk = false; isDelOk = false; }
  elems["btn_set_entry"].disabled = (isAddOk ? "" : "disabled");
  elems["btn_set_delete"].disabled = (isDelOk ? "" : "disabled");
}

//**************************
// RPテーブルの行クリック
//**************************
function rpListSelected(typeMY, i, j){
    if (is_template_readonly) return;
    isSet = "";
    var medInfo;
    var yohoInfo;
    var yoho;

    currentRP = i;
    currentINFO = j;
    currentTypeMy = typeMY;

    if (typeMY == 1) {  // 薬品データ
        medInfo = rpInfo[i].medInfo[j];
        rpInfo[i].medIdx = j;
        currentMedSeq = (medInfo.seq ? medInfo.seq : 0);
    } else {            // 用法データ
        yoho = rpInfo[i].yohoInfo[j];
        rpInfo[i].yohoIdx = j;
    }

    //====================
    // 薬品データクリア
    //====================
    if(typeMY == 2){
        elems["current_med_seq"].value = "";
        elems["current_seibun_bunrui"].value = "";
        elems["current_zaikei_bunrui"].value = "";
        elems["current_status"].value = "";
        elems["current_yj_cd"].value = "";
        elems["current_med_name"].value = "";
        elems["current_remark"].value = "";
        elems["current_yoryo"].value = "";
        elems["current_unit"].value = "";
        selectSelect("yoryo_type", yoryo_type);
        if (elems["current_med_chozai_siji_f"]) {
            elems["current_med_chozai_siji_f"].checked = false;
        }
        if (elems["current_med_chozai_siji_h"]) {
            elems["current_med_chozai_siji_h"].checked = false;
        }
        if (elems["current_med_kouhatu_fuka"]) {
            elems["current_med_kouhatu_fuka"].checked = false;
        }
    }
    //====================
    // 薬品データを右側へ
    //====================
    if(typeMY == 1){
        elems["current_med_seq"].value = currentMedSeq;
        elems["current_seibun_bunrui"].value = medInfo.seibun_bunrui;
        elems["current_zaikei_bunrui"].value = medInfo.zaikei_bunrui;
        elems["current_status"].value = (medInfo.status ? medInfo.status : 0);
        elems["current_yj_cd"].value = (medInfo.yj_cd ? medInfo.yj_cd : "" );
        elems["current_med_name"].value = str(medInfo.med_name);
        elems["current_remark"].value = str(medInfo.remark);
        elems["current_yoryo"].value = str(medInfo.yoryo);
        elems["current_unit"].value = str(medInfo.med_unit_name);
        var c = SEIBUN_BUNRUI_CAPTIONS[medInfo.seibun_bunrui];
        elems["current_seibun_bunrui_disp"].innerHTML = c ? "("+c+")" : "";
        var z = ZAIKEI_BUNRUI_CAPTIONS[medInfo.zaikei_bunrui];
        elems["current_zaikei_bunrui_disp"].innerHTML = z ? "("+z+")" : "";
        selectSelect("yoryo_type", medInfo.yoryo_type);
        standbyCurrentUnitSelection(medInfo);
        if (elems["current_med_chozai_siji_f"] && elems["current_med_chozai_siji_f"].style.display == "") {
            elems["current_med_chozai_siji_f"].checked = (medInfo.med_chozai_siji.indexOf("F") > -1) ? true : false;
        }
        if (elems["current_med_chozai_siji_h"] && elems["current_med_chozai_siji_h"].style.display == "") {
            elems["current_med_chozai_siji_h"].checked = (medInfo.med_chozai_siji.indexOf("H") > -1) ? true : false;
        }
        if (elems["current_med_kouhatu_fuka"]) {
            elems["current_med_kouhatu_fuka"].checked = (medInfo.med_kouhatu_fuka == "1") ? true : false;
        }
    }
    //====================
    // 用法クリア
    //====================
    fukintouType = "";
    fukintou_naiyo_timing = false;
    fukintou_naiyo_meal_timing = false;
    fukintou_naiyo_hours = false;

    selectSelect("yoho_naiyo_per_day", "");
    selectSelect("yoho_cyusyaroute_cd", "");
    selectSelect("yoho_cyusyasiji_cd", "");
    elems["yoho_cyusyasokudo"].value = "";
    selectSelect("yoho_cyusyasokudo_unit", "");
    for (var k = 1; k <= 6; k++) elems["yoho_cyusyatiming"+k].value = "";

    for (var k = 1; k <= 8; k++) {
        checkboxCheck("yoho_naiyo_timing_"+k, "");
        elems["yoho_naiyo_timing_fukintou_"+k].value = "";
    }
    for (var k = 1; k <= 6; k++) {
        checkboxCheck("yoho_naiyo_meal_timing_"+k, "");
        elems["yoho_naiyo_meal_timing_fukintou_"+k].value = "";
    }
    for (var k = 1; k <= maxYohoNaiyoHours; k++) {
        if (!elems["yoho_naiyo_hours_"+k]) {
            continue;
        }
        elems["yoho_naiyo_hours_"+k].value = "";
        elems["yoho_naiyo_hours_fukintou_"+k].value = "";
    }
    elems["yoho_naiyo_hour_dist"].value = "";
    var suffix;
    var key;
    for (key in CHOZAI_SIJI_CAPTIONS) {
        suffix = key.toLowerCase();
        if (!elems["yoho_naiyo_chozai_siji_" + suffix]) {
            // 要素が存在していなければスキップ
            continue;
        }
        elems["yoho_naiyo_chozai_siji_" + suffix].checked = false;
    }

    selectSelect("yoho_gaiyo_touyo_kaisu_cd", "");
    selectSelect("yoho_gaiyo_siyou_houhou_cd", "");
    selectSelect("yoho_gaiyo_bui_cd", "");
    elems["yoho_gaiyo_bui_l"].checked = "";
    elems["yoho_gaiyo_bui_r"].checked = "";
    elems["yoho_gaiyo_bui_b"].checked = "";

    selectSelect("yoho_tonpu_kaisu", "");
    selectSelect("yoho_tonpu_timing1_cd", "");
    selectSelect("yoho_tonpu_timing2_cd", "");
    for (var k = 1; k <= 3; k++) elems["yoho_tonpu_timing2_text_"+k].value = "";
    tonpu1TimingChanged("");
    tonpu2TimingChanged();
    elems["yoho_tonpu_ondo"].value = "";
    selectSelect("yoho_touyo_start_ymd_y", "");
    selectSelect("yoho_touyo_start_ymd_m", "");
    selectSelect("yoho_touyo_start_ymd_d", "");
    elems["yoho_touyo_nichi_bun"].value = "";
    elems["yoho_touyo_nichi_oki"].value = "";
    checkboxCheck("yoho_naiyo_timing_ishi_shiji","");
    elems["yoho_comment"].value = "";
    if (elems["yoho_touyo_teiji_tunagi"]) { // 定時つなぎ
        elems["yoho_touyo_teiji_tunagi"].checked = false;
    }
    if (elems["yoho_content"]) {    // 用法内容
        elems["yoho_content"].value = "";
    }

    if(typeMY == 2){
        if(yoho.yoho_kubun) yoho_kubun = yoho.yoho_kubun ;

        //====================
        // 用法データを右側へ（内用薬）
        //====================
        if (yoho.yoho_kubun == "C") {
            //elems["yoho_content"].value = yoho.yoho_content ? yoho.yoho_content.replace(/<\/br>/g, "\n") : "";    // </br>→改行コード(\n)
            elems["yoho_content"].value = str(yoho.yoho_content);
        }

        if (yoho.yoho_kubun=="N" || yoho.yoho_kubun=="C"){
            selectSelect("yoho_naiyo_per_day", yoho.yoho_naiyo_per_day);
        }

        if (yoho.yoho_kubun=="C"){
            selectSelect("yoho_cyusyaroute_cd", yoho.yoho_cyusyaroute_cd);
            selectSelect("yoho_cyusyasiji_cd", yoho.yoho_cyusyasiji_cd);
            elems["yoho_cyusyasokudo"].value =str(yoho.yoho_cyusyasokudo);
            selectSelect("yoho_cyusyasokudo_unit", yoho.yoho_cyusyasokudo_unit);
            for (var i = 1; i <= 6; i++) {
                elems["yoho_cyusyatiming"+i].value = tabAryValue(yoho.yoho_cyusyatiming, i);
            }
        }

        if (yoho.yoho_kubun=="N"){  // 入院臨時処方、入院定時処方、院内外来処方箋、院外外来処方箋、退院時処方
            var fukintou_timing_open = false;   // 入力欄展開フラグ
            for (var i = 1; i <= 8; i++) {
                checkboxCheck("yoho_naiyo_timing_"+i, tabAryValue(yoho.yoho_naiyo_timing, i));
                elems["yoho_naiyo_timing_fukintou_"+i].value = tabAryValue(yoho.yoho_naiyo_timing_fukintou, i);
                if (elems["yoho_naiyo_timing_fukintou_"+i].value) {
                    fukintou_timing_open = true;
                }
            }
            document.getElementById("link_fukintou_naiyo_timing").textContent = "不均等";
            if (fukintou_timing_open) {
                setFukintouTiming(null);    // 入力欄を展開する
            }

            for (var i = 1; i <= 6; i++) {
                checkboxCheck("yoho_naiyo_meal_timing_"+i, tabAryValue(yoho.yoho_naiyo_meal_timing, i));
                elems["yoho_naiyo_meal_timing_fukintou_"+i].value = tabAryValue(yoho.yoho_naiyo_meal_timing_fukintou, i);
            }

            var fukintou_hours_open = false;    // 入力欄展開フラグ
            for (var i = 1; i <= maxYohoNaiyoHours; i++) {
                if (!elems["yoho_naiyo_hours_"+i]) {
                    continue;
                }
                elems["yoho_naiyo_hours_"+i].value = tabAryValue(yoho.yoho_naiyo_hours, i);
                elems["yoho_naiyo_hours_fukintou_"+i].value = tabAryValue(yoho.yoho_naiyo_hours_fukintou, i);
                if (elems["yoho_naiyo_hours_fukintou_"+i].value) {
                    fukintou_hours_open = true;
                }
            }
            document.getElementById("link_fukintou_naiyo_hours").textContent = "不均等";
            if (fukintou_hours_open) {
                setFukintouHours(null); // 入力欄を展開する
            }

            elems["yoho_naiyo_hour_dist"].value = clearZero(yoho.yoho_naiyo_hour_dist);
            for (key in CHOZAI_SIJI_CAPTIONS) {
                suffix = key.toLowerCase();
                if (!elems["yoho_naiyo_chozai_siji_" + suffix]) {
                    // 要素が存在していなければスキップ
                    continue;
                }
                elems["yoho_naiyo_chozai_siji_" + suffix].checked = (yoho.yoho_naiyo_chozai_siji.indexOf(key)>=0 ? "checked" : "");
            }
        }
        if (yoho.yoho_kubun=="G" || yoho.yoho_kubun=="Y"){
            selectSelect("yoho_gaiyo_touyo_kaisu_cd", yoho.yoho_gaiyo_touyo_kaisu_cd);
            selectSelect("yoho_gaiyo_siyou_houhou_cd", yoho.yoho_gaiyo_siyou_houhou_cd);
            selectSelect("yoho_gaiyo_bui_cd", yoho.yoho_gaiyo_bui_cd);
            buiChanged(yoho.yoho_gaiyo_bui_cd);
            elems["yoho_gaiyo_bui_l"].checked = (yoho.yoho_gaiyo_bui_lrb.indexOf("L")>=0 ? "checked" : "");
            elems["yoho_gaiyo_bui_r"].checked = (yoho.yoho_gaiyo_bui_lrb.indexOf("R")>=0 ? "checked" : "");
            elems["yoho_gaiyo_bui_b"].checked = (yoho.yoho_gaiyo_bui_lrb.indexOf("B")>=0 ? "checked" : "");
        }
        if (yoho.yoho_kubun=="P"){
            selectSelect("yoho_tonpu_kaisu", yoho.yoho_tonpu_kaisu);
            selectSelect("yoho_tonpu_timing1_cd", yoho.yoho_tonpu_timing1_cd);
            tonpu1TimingChanged(yoho.yoho_tonpu_timing1_cd);
            selectSelect("yoho_tonpu_timing2_cd", yoho.yoho_tonpu_timing2_cd);
            tonpu2TimingChanged();
            for (var i = 1; i <= 3; i++) {
                elems["yoho_tonpu_timing2_text_"+i].value = tabAryValue(yoho.yoho_tonpu_timing2_text, i);
            }
            elems["yoho_tonpu_ondo"].value = str(yoho.yoho_tonpu_ondo);
        }

        var ymdy, ymdm, ymdd;
        var rpInfoMaxCnt = countTotalRP();
        if (yoho.yoho_touyo_start_ymd) {    // 投与開始日
            ymdy = yoho.yoho_touyo_start_ymd.substr(0,4);
            ymdm = yoho.yoho_touyo_start_ymd.substr(4,2);
            ymdd = yoho.yoho_touyo_start_ymd.substr(6,2);
        } else if (rpInfoMaxCnt > 0 && rpInfo[1].yohoInfo[0].yoho_touyo_start_ymd) {
            ymdy = rpInfo[1].yohoInfo[0].yoho_touyo_start_ymd.substr(0,4);
            ymdm = rpInfo[1].yohoInfo[0].yoho_touyo_start_ymd.substr(4,2);
            ymdd = rpInfo[1].yohoInfo[0].yoho_touyo_start_ymd.substr(6,2);
        }

        selectSelect("yoho_touyo_start_ymd_y", toInt(ymdy));
        selectSelect("yoho_touyo_start_ymd_m", toInt(ymdm));
        selectSelect("yoho_touyo_start_ymd_d", toInt(ymdd));
        elems["yoho_touyo_nichi_bun"].value = clearZero(yoho.yoho_touyo_nichi_bun);
        elems["yoho_touyo_nichi_oki"].value = clearZero(yoho.yoho_touyo_nichi_oki);
        if (elems["yoho_touyo_teiji_tunagi"]) {     // 定時つなぎ
            checkboxCheck("yoho_touyo_teiji_tunagi", yoho.yoho_touyo_teiji_tunagi == 1 ? "true" : "");
            onClickTeijiTunagi();
        }
        //elems["yoho_comment"].value = yoho.yoho_comment ? yoho.yoho_comment.replace(/<\/br>/g, "\n") : "";    // </br>→改行コード(\n)
        elems["yoho_comment"].value = str(yoho.yoho_comment);
        checkboxCheck("yoho_naiyo_timing_ishi_shiji",yoho.yoho_naiyo_timing_ishi_shiji == 1 ? "true" : "");
    }
    changeDisplay("", "");
}

//**************************
// RP追加
//**************************
function addRP(){
  if (maxRP == 0) maxRP = 1;
  ++maxRP;
  var medInfo = {};
  var yohoInfo = {};
  for (f in EMPTY_MEDINFO) medInfo[f] = EMPTY_MEDINFO[f];
  for (f in EMPTY_YOHOINFO) yohoInfo[f] = EMPTY_YOHOINFO[f];
  rpInfo[maxRP] = {};
  rpInfo[maxRP].medIdx = 0;
  rpInfo[maxRP].yohoIdx = 0;
  rpInfo[maxRP].medInfo = [medInfo];
  rpInfo[maxRP].yohoInfo = [yohoInfo];
  rpInfo[maxRP].isMedEmpty = 1;
  rpInfo[maxRP].isYohoEmpty = 1;

  currentRP = maxRP;
  currentMedSeq = 0;
  setToRP("","");
}

//**************************
// RP削除
//**************************
function delRP(){
    if (maxRP <= 0) return;
    //if (edit_flg == 1) {
    if (rpInfo[currentRP].isSavedData) {
        for (var i = 0; i < rpInfo[currentRP].medInfo.length; ++i) {
            if (rpInfo[currentRP].medInfo[i].default_status==9) continue;
            var rps = rpInfo[currentRP].medInfo[i].status;
            rpInfo[currentRP].medInfo[i].status = (rps==9 ? rpInfo[currentRP].medInfo[i].default_status : 9);
        }
        for (var i = 0; i < rpInfo[currentRP].yohoInfo.length; ++i) {
            if (rpInfo[currentRP].yohoInfo[i].default_status==9) continue;
            var rps = rpInfo[currentRP].yohoInfo[i].status;
            rpInfo[currentRP].yohoInfo[i].status = (rps==9 ? rpInfo[currentRP].yohoInfo[i].default_status : 9);
        }
        setToRP("","");
    } else {
        currentMedSeq = 0;
        if (maxRP == 1) {
            delete rpInfo[currentRP];
            rpInfo[currentRP] = {"isMedEmpty":"1","isYohoEmpty":"1","medIdx":0,"yohoIdx":0,"medInfo":[EMPTY_MEDINFO],"yohoInfo":[EMPTY_YOHOINFO]}
        } else {
            delete rpInfo[currentRP];
            for (var i = currentRP; i < maxRP; ++i) {
                rpInfo[i] = rpInfo[i + 1];
            }
            delete rpInfo[maxRP];
            maxRP -= 1;
            currentRP--;
        }
        setToRP("","");
    }
}

//**************************
// RPテーブルへ
//**************************
function setToRP(pos, command){
    var newYohoPer1 = "";
    var newYohoPer2 = "";
    var newYohoPer = "";
    var order_status = 0;
    var yt = "";
    var err = [];

    if(elems["yoryo_type"]) newYohoPer1 = elems["yoryo_type"].value;

    if (command=="entry" || command=="modify" || command=="delete"){
        if (!currentRP) return alert("RPを指定してください。");
        if (pos=="med" && rpInfo[currentRP].medInfo[0].status=="9") return alert("削除されたRPへは適用できません。");
        if (pos=="yoho" && rpInfo[currentRP].yohoInfo[0].status=="9") return alert("削除されたRPへは適用できません。");
    }

    // 薬品データ
    var idx = 0;
    if (pos=="med" && (command=="entry" || command=="modify")){
        idx = setToRPMedInfo(command, err);
    }

    // 用法データ
    if (pos=="yoho" && (command=="entry" || command=="modify")){
        rpInfo[currentRP].yohoIdx = (currentTypeMy == 1) ? currentINFO : (rpInfo[currentRP].yohoInfo.length - 1);
        if (err.length) { alert(err.join("\n")); return; }
        setToRPYohoInfo(err);
    }

    //====================
    // RP内薬品・用法削除の場合
    //====================
    if (command == "delete") {
        if (pos == "med") {
            if (status_visible) {
                if (copy_flg != 1) {
                    var arr_cts = rpInfo[currentRP].medInfo.length;
                    if (2 < arr_cts) {
                        rpInfo[currentRP].medInfo.splice(currentINFO, 1);
                    } else {
                        rpInfo[currentRP].medInfo = [EMPTY_MEDINFO];
                        rpInfo[currentRP].isMedEmpty = 1;
                    }

                /*  rpInfo[currentRP].medInfo[currentINFO].status = 9;
                    rpInfo[currentRP].isMedEmpty = 1;
                    for (var i = 0; i < rpInfo[currentRP].medInfo.length ; i++) {
                        if (rpInfo[currentRP].medInfo[i].status < 9) {
                            delete(rpInfo[currentRP].isMedEmpty);
                            break;
                        }
                    }   */
                } else if (copy_flg == 1) {
                    var arr_cts = rpInfo[currentRP].medInfo.length;
                    if (2 < arr_cts) {
                        rpInfo[currentRP].medInfo.splice(currentINFO, 1);
                    } else {
                        rpInfo[currentRP].medInfo = [EMPTY_MEDINFO];
                        rpInfo[currentRP].isMedEmpty = 1;
                    }
                }
            }
            else {
                //rpInfo[currentRP].medInfo.splice(currentINFO,1);
                var arr_cts = rpInfo[currentRP].medInfo.length;
                if (2 < arr_cts) {
                    rpInfo[currentRP].medInfo.splice(currentINFO, 1);
                } else {
                    rpInfo[currentRP].medInfo = [EMPTY_MEDINFO];
                    rpInfo[currentRP].isMedEmpty = 1;
                }
            }
        }
        if (pos == "yoho") {
            rpInfo[currentRP].yohoInfo[currentINFO] = EMPTY_YOHOINFO;
            rpInfo[currentRP].isYohoEmpty = 1;
        }
    }

    //==========================
    // RP表の領域の、HTMLを作成
    //==========================
    // RP数を取得
    var rpInfoMaxCnt = countTotalRP();

    // HTMLを作成
    var dat = createRPTableHTML(idx, rpInfoMaxCnt);

    elems["rp_listtable"].innerHTML = dat.join('');
    var feymd_y = toInt(selectValue("force_end_ymd_y"));
    var feymd_m = toInt(selectValue("force_end_ymd_m"));
    var feymd_d = toInt(selectValue("force_end_ymd_d"));
    var force_end_ymd = ("0000"+feymd_y).slice(-4) + ("00"+feymd_m).slice(-2) + ("00"+feymd_d).slice(-2);
    //==========================
    // 保存用データ作成１：オーダ基本データ
    //==========================
    var dmpBase = [];
    dmpBase.push("summary_seq=" + summary_seq);
    dmpBase.push("set_seq=0");
    dmpBase.push("is_rinji_teiki="+ is_rinji_teiki);
    dmpBase.push("is_syoho_cyusya="+ is_syoho_cyusya);
    dmpBase.push("is_nyuin_gairai=N");
    dmpBase.push("is_innai_ingai=N");
    dmpBase.push("set_target=");
    dmpBase.push("set_name=");
    dmpBase.push("order_ymd="+order_ymd);
    dmpBase.push("byoumei=" + textValue("byoumei"));
    dmpBase.push("byoumei_code=" + textValue("byoumei_code"));
    dmpBase.push("icd10=" + textValue("icd10"));
    dmpBase.push("pt_id="+pt_id);
    dmpBase.push("enti_id=");
    dmpBase.push("sect_id=");
    dmpBase.push("force_end_ymd=" + force_end_ymd);
    dmpBase.push("is_uketuke_zumi=");
    dmpBase.push("is_kensa_zumi=");
    dmpBase.push("is_cyozai_zumi=");
    dmpBase.push("is_touyo_zumi=");
    dmpBase.push("regist_emp_id=" + login_emp_id);
    elems["ordsc_basedata_stream"].value = dmpBase.join("\t");

    //==========================
    // 保存用データ作成２：詳細データ（セパレータ"\t@\t"でフィールド区切り、セパレータ"\t*\t"で行データ区切り）
    //==========================
    var dmpRp = [];
    var rpNum = 0;
    var rpLineSeq = 0;
    for(var i = 1; i <= rpInfoMaxCnt ; i++){
        var dmpline = [];
        dmpline.push("rp_seq=" + i); // RP番号

        // 薬品
        var medInfoMaxCnt = (rpInfo[i] && rpInfo[i].medInfo && rpInfo[i].medInfo.length) ? ((rpInfo[i].medInfo.length == 1) ? 1 : (rpInfo[i].medInfo.length - 1)): 0;
        for(var j = 0; j < medInfoMaxCnt ; j++) {
            dmpline.push("rp_line_seq=" + j); // RP内の何番目の行か
            if (!rpInfo[i].medInfo[j]) continue;
            if (!rpInfo[i].medInfo[j].med_name) continue;
            var minfo = rpInfo[i].medInfo[j] ;
            dmpline.push("rp_line_div=M"); // M:薬剤行
            dmpline.push("status=" + (minfo.status ? minfo.status : "" ));
            dmpline.push("med_seq_cd=" + minfo.seq); // 薬剤コード
            dmpline.push("med_name=" + minfo.med_name);
            dmpline.push("med_yoryo=" + minfo.yoryo);
            dmpline.push("med_unit_name=" + minfo.med_unit_name);
            dmpline.push("remark=" + minfo.remark);
            dmpline.push("med_chozai_siji=" + (minfo.med_chozai_siji ? minfo.med_chozai_siji : ""));
            dmpline.push("yj_cd=" + (minfo.yj_cd ? minfo.yj_cd : ""));
            dmpline.push("yoryo_type=" + (minfo.yoryo_type ? minfo.yoryo_type : ""));
            dmpline.push("med_kouhatu_fuka=" + minfo.med_kouhatu_fuka);
            dmpRp.push(dmpline.join("\t@\t"));
        }

        // 用法
        var yohoInfoMaxCnt = (rpInfo[i] && rpInfo[i].medInfo && rpInfo[i].yohoInfo.length) ? ((rpInfo[i].yohoInfo.length == 1) ? 1 : (rpInfo[i].yohoInfo.length - 1)) : 0;
        for(var j = 0; j < yohoInfoMaxCnt ; j++) {
            dmpline.push("rp_line_seq=" + (100 + j)); // RP内の何番目の行か
            if (!rpInfo[i].yohoInfo[j]) continue;
            if (!rpInfo[i].yohoInfo[j]) continue;
            var yoho = rpInfo[i].yohoInfo[j] ;
            dmpline.push("rp_line_div=Y"); // Y:用法行

            dmpline.push("yoho_kubun="+str(yoho.yoho_kubun));
            dmpline.push("yoho_touyo_start_ymd=" + str(yoho.yoho_touyo_start_ymd));
            dmpline.push("yoho_touyo_nichi_bun=" + str(yoho.yoho_touyo_nichi_bun));
            dmpline.push("yoho_touyo_nichi_oki=" + str(yoho.yoho_touyo_nichi_oki));
            dmpline.push("yoho_touyo_teiji_tunagi=" + str(yoho.yoho_touyo_teiji_tunagi));    // 定時つなぎ
            if (yoho.yoho_kubun=="N" || yoho.yoho_kubun=="C") {
                dmpline.push("yoho_naiyo_per_day=" + str(yoho.yoho_naiyo_per_day));
            }
            if (yoho.yoho_kubun=="N" || yoho.yoho_kubun=="P") {
                dmpline.push("yoho_naiyo_timing=" + str(yoho.yoho_naiyo_timing));
                dmpline.push("yoho_naiyo_timing_fukintou=" + str(yoho.yoho_naiyo_timing_fukintou));
                dmpline.push("yoho_naiyo_meal_timing=" + str(yoho.yoho_naiyo_meal_timing));
                dmpline.push("yoho_naiyo_meal_timing_fukintou=" + str(yoho.yoho_naiyo_meal_timing_fukintou));
                dmpline.push("yoho_naiyo_hour_dist=" + str(yoho.yoho_naiyo_hour_dist));
                dmpline.push("yoho_naiyo_hours=" + str(yoho.yoho_naiyo_hours));
                dmpline.push("yoho_naiyo_hours_fukintou=" + str(yoho.yoho_naiyo_hours_fukintou));
                dmpline.push("yoho_naiyo_fukintou_type=" + str(yoho.yoho_naiyo_fukintou_type));
                dmpline.push("yoho_naiyo_chozai_siji=" + str(yoho.yoho_naiyo_chozai_siji));
            }
            if (yoho.yoho_kubun=="G" || yoho.yoho_kubun=="Y"){
                dmpline.push("yoho_gaiyo_touyo_kaisu_cd=" + str(yoho.yoho_gaiyo_touyo_kaisu_cd));
                dmpline.push("yoho_gaiyo_siyou_houhou_cd=" + str(yoho.yoho_gaiyo_siyou_houhou_cd));
                dmpline.push("yoho_gaiyo_bui_cd=" + str(yoho.yoho_gaiyo_bui_cd));
                dmpline.push("yoho_gaiyo_bui_lrb=" + str(yoho.yoho_gaiyo_bui_lrb));
            }
            if (yoho.yoho_kubun=="P"){
                dmpline.push("yoho_tonpu_kaisu=" + str(yoho.yoho_tonpu_kaisu));
                dmpline.push("yoho_tonpu_timing1_cd=" + str(yoho.yoho_tonpu_timing1_cd));
                dmpline.push("yoho_tonpu_timing2_cd=" + str(yoho.yoho_tonpu_timing2_cd));
                dmpline.push("yoho_tonpu_timing2_text=" + str(yoho.yoho_tonpu_timing2_text));
                dmpline.push("yoho_tonpu_ondo=" + str(yoho.yoho_tonpu_ondo));
            }
            if (yoho.yoho_kubun=="C") {
                dmpline.push("yoho_cyusyaroute_cd=" + (yoho.yoho_cyusyaroute_cd ? yoho.yoho_cyusyaroute_cd : 0));
                dmpline.push("yoho_cyusyasiji_cd=" + str(yoho.yoho_cyusyasiji_cd));
                dmpline.push("yoho_cyusyasokudo=" + str(yoho.yoho_cyusyasokudo));
                dmpline.push("yoho_cyusyasokudo_unit=" + str(yoho.yoho_cyusyasokudo_unit));
                dmpline.push("yoho_cyusyatiming=" + str(yoho.yoho_cyusyatiming));
                dmpline.push("yoho_content=" + str(yoho.yoho_content));
            }
            dmpline.push("status=" + (yoho.status ? yoho.status : "" ));
            minfo = rpInfo[i].medInfo[j];
            dmpline.push("seibun_bunrui=" + str(minfo.seibun_bunrui));
            dmpline.push("yoho_comment=" + str(yoho.yoho_comment));
            //var yoho_display_string  // </br>→改行コード(\n)
            //     = yoho.yoho_display_string ? yoho.yoho_display_string.replace(/<\/br>/g, "\n") : "";
            //dmpline.push("yoho_display_string=" + yoho_display_string);
            dmpline.push("yoho_display_string=" + (yoho.yoho_display_string?yoho.yoho_display_string.replace(/<\/br>/g, "\n"):""));
            dmpline.push("yoho_naiyo_timing_ishi_shiji=" + (yoho.yoho_naiyo_timing_ishi_shiji ? yoho.yoho_naiyo_timing_ishi_shiji : 0));
            dmpRp.push(dmpline.join("\t@\t"));
        }
    }

    elems["ordsc_rpdata_stream"].value = dmpRp.join("\t*\t");

    //==========================
    // 表示調整
    //==========================
    var isDisabled = "";
    for (rp in rpInfo) if (!rpInfo[rp].medInfo[0].med_name) isDisabled = "disabled";
    if(elems["btn_rp_add"]) elems["btn_rp_add"].disabled = isDisabled;
    if(elems["btn_rp_del"]) elems["btn_rp_del"].disabled = (maxRP<1 || (maxRP==1 && rpInfo[1].isMedEmpty)   ? "disabled": "");
    medYohoButtonEnableCheck();
    setButtonEnableCheck();
}

//**************************
// 薬品設定RPデータ作成
//**************************
function setToRPMedInfo(command, err) {
    var newYoryo = elems["current_yoryo"].value;
    var newUnit = elems["current_unit"].value;
    var newMedSeq = elems["current_med_seq"].value;
    var newYjcd = elems["current_yj_cd"].value;
    var newSeibunBunrui = textValue("current_seibun_bunrui");
    var newZaikeiBunrui = textValue("current_zaikei_bunrui").value;

    fukintou_naiyo_timing = false;
    fukintou_naiyo_meal_timing = false;
    fukintou_naiyo_hours = false;

    rpInfo[currentRP].medIdx = (currentTypeMy == 1) ? currentINFO : (rpInfo[currentRP].medInfo.length - 1);
    if (newSeibunBunrui != "G"){
        // 用量チェック
        if (!newYoryo) {
            err.push("用量を指定してください。");
        } else {
            // 分数は入力不可で、小数点第3位まで
            var pattern = /^(([1-9]\d{0,4})|0)(\.\d{1,3})?$/;
            if (!newYoryo.match(pattern)) {
                err.push("用量の指定が適切ではありません。");
            }
        }
    }
    if (!newUnit) err.push("単位を指定してください。");

    if (command=="entry" || (newMedSeq != currentMedSeq)){
        var isErr = false;
        for (var idx in rpInfo[currentRP].medInfo) {
            if (rpInfo[currentRP].medInfo[idx].yj_cd == newYjcd) {
                err.push("指定した薬品は、既に現在選択中のRPに含まれています。");
                isErr = true;
            }

            // 中川の郷療育センター様対応の為コメントアウト(必要になった場合はコメント解除) 2014.02.21
//            if (rpInfo[currentRP].medInfo[idx].seibun_bunrui && (rpInfo[currentRP].medInfo[idx].seibun_bunrui != newSeibunBunrui)) {
//                err.push("同一RP内に、異なる成分分類種別の薬剤を含めることはできません。");
//                isErr = true;
//            }

            if (isErr) break;
        }
    }

    if (err.length) { alert(err.join("\n")); return; }

    delete(rpInfo[currentRP].isMedEmpty);
    if (command=="entry"){
        rpInfo[currentRP].medIdx = (rpInfo[currentRP].medInfo.length == 1) ? 0 : (rpInfo[currentRP].medInfo.length - 1);
        rpInfo[currentRP].medInfo.push({});
    }

    // 用量（1日／1回）
    yt = selectValue("yoryo_type");
    var match_pattern = /１日|回/g;
    if(newYoryo.search(match_pattern) >= 0){
        var arr = newYoryo.split("回");
        newYoryo = arr[1];
    }

    // 薬剤毎の調剤指示
    var medChozaiSiji = "";
    if (elems["current_med_chozai_siji_f"] && elems["current_med_chozai_siji_f"].style.display == "") {
        medChozaiSiji += checkValue("current_med_chozai_siji_f");
    }
    if (elems["current_med_chozai_siji_h"] && elems["current_med_chozai_siji_h"].style.display == "") {
        medChozaiSiji += checkValue("current_med_chozai_siji_h");
    }

    // 後発品変更不可(院外外来処方箋のみ)
    var kouhatuFuka = "";
    if (elems["current_med_kouhatu_fuka"]) {
        if (checkValue("current_med_kouhatu_fuka")) {
            kouhatuFuka = "1";
        } else {
            kouhatuFuka = "0";
        }
    }

    currentMedSeq = newMedSeq;
    rpInfo[currentRP].medInfo[rpInfo[currentRP].medIdx] = {
        "schedule_seq" : rp.schedule_seq,
        "seq" : newMedSeq,
        "status" : ((command == "modify") ? (status_visible ? 2 : 0) : (status_visible ? 1 : 0) ),
        "yj_cd" : (elems["current_yj_cd"] ? elems["current_yj_cd"].value : ""),
        "med_name" : elems["current_med_name"].value,
        "seibun_bunrui" : newSeibunBunrui,
        "zaikei_bunrui" : newZaikeiBunrui,
        "remark" : elems["current_remark"].value,
        "yoryo" : newYoryo,
        "med_unit_name" : newUnit,
        "units_separated_tab" : medSearchStocker[newMedSeq].units_separated_tab,
        "yoryo_type" : yt,
        "med_chozai_siji" : medChozaiSiji,
        "med_kouhatu_fuka" : kouhatuFuka
    };

    return idx;
}

//**************************
// 用法データ
//**************************
function setToRPYohoInfo(err) {
    var out = [];
    var yh = {};
    var tmp = "";
    var sb = "";
    var yoho_naiyo_meal_timing = "";

    yh["yoho_kubun"] = yoho_kubun;

    //====================
    // 用法(6-1)：内用薬(1)
    //====================
    setToRPYohoNaiyo1(yh, out, tmp);

    //====================
    // 用法(6-2)：外用薬/外用頓服薬
    //====================
    if (yh["yoho_kubun"]=="G" || yh["yoho_kubun"]=="Y"){
        yh["yoho_gaiyo_touyo_kaisu_cd"] = selectValue("yoho_gaiyo_touyo_kaisu_cd");
        yh["yoho_gaiyo_siyou_houhou_cd"] = selectValue("yoho_gaiyo_siyou_houhou_cd");
        yh["yoho_gaiyo_bui_cd"] = selectValue("yoho_gaiyo_bui_cd");
        yh["yoho_gaiyo_bui_name"] = selectText("yoho_gaiyo_bui_cd");
        yh["yoho_gaiyo_bui_lrb"] = checkValue("yoho_gaiyo_bui_l") + checkValue("yoho_gaiyo_bui_r") + checkValue("yoho_gaiyo_bui_b");
        if (yh["yoho_gaiyo_touyo_kaisu_cd"]) out.push(selectText("yoho_gaiyo_touyo_kaisu_cd"));
        if (yh["yoho_gaiyo_siyou_houhou_cd"]) out.push("| " + selectText("yoho_gaiyo_siyou_houhou_cd"));
        else out.push("| ");

        var tmp1 = "";
        if (yh["yoho_gaiyo_bui_cd"]) tmp1 = yh["yoho_gaiyo_bui_name"];
        var tmp = yh["yoho_gaiyo_bui_lrb"].replace("L","/左").replace("R","/右").replace("B","/両方");

        if (tmp) tmp1 += "| " + "("+tmp.substr(1)+")";

        if (tmp1) out.push("| " + tmp1);
    }

    //====================
    // 用法(6-3)：頓服
    //====================
    if (yh["yoho_kubun"] == "P") {
        yh["yoho_tonpu_kaisu"]        = selectValue("yoho_tonpu_kaisu");
        yh["yoho_tonpu_timing1_cd"]   = selectValue("yoho_tonpu_timing1_cd");
        yh["yoho_tonpu_timing2_cd"]   = selectValue("yoho_tonpu_timing2_cd");
        yh["yoho_tonpu_timing2_text"] = textValue("yoho_tonpu_timing2_text_1") + "\t" + textValue("yoho_tonpu_timing2_text_2") + "\t" + textValue("yoho_tonpu_timing2_text_3");
        yh["yoho_tonpu_ondo"]         = textValue("yoho_tonpu_ondo");

        if (yh["yoho_tonpu_kaisu"]) out.push(yh["yoho_tonpu_kaisu"] + "回分");
        out.push("| ");
        if (yh["yoho_tonpu_timing1_cd"]) out.push(selectText("yoho_tonpu_timing1_cd"));
        if (yh["yoho_tonpu_timing2_cd"]) {
            var tmp2_text = selectText("yoho_tonpu_timing2_cd");
            if (tmp2_text) {
                for (var i = 1; i <= 3; i++){
                    var tmp2_subtext = textValue("yoho_tonpu_timing2_text_"+i);
                    tmp2_text = tmp2_text.replace("{@"+i+"}", tmp2_subtext);
                }
            }
            out.push( tmp2_text);
        }
        if (yh["yoho_tonpu_ondo"]) out.push("| " + yh["yoho_tonpu_ondo"]+"℃");
    }

    //====================
    // 用法(6-4)：共通(1)
    //====================
    var ymdy = ("0000"+selectValue("yoho_touyo_start_ymd_y")).slice(-4);
    var ymdm = ("00"+selectValue("yoho_touyo_start_ymd_m")).slice(-2);
    var ymdd = ("00"+selectValue("yoho_touyo_start_ymd_d")).slice(-2);
    yh["yoho_touyo_start_ymd"] = ymdy + ymdm + ymdd;
    if (elems["yoho_touyo_teiji_tunagi"] && checkValue("yoho_touyo_teiji_tunagi") != "") {
        // 「定時つなぎ」項目が存在し、「定時つなぎ」が選択されている場合(定時つなぎは入院臨時処方箋、入院定時処方箋、院外外来処方箋のみ)
        yh["yoho_touyo_teiji_tunagi"] = 1;
    } else {
        yh["yoho_touyo_teiji_tunagi"] = 0;
    }
    yh["yoho_touyo_nichi_bun"] = textValue("yoho_touyo_nichi_bun");
    yh["yoho_touyo_nichi_oki"] = textValue("yoho_touyo_nichi_oki");

    tmpS = "";
    var oscnt_cyouzai_date = "";
    if (toInt(ymdy) > 0 && toInt(ymdm) > 0 && toInt(ymdd) > 0){
        tmpS = toInt(ymdy)+"年" + toInt(ymdm)+"月" + toInt(ymdd)+"日";
        oscnt_cyouzai_date = ymdy+ymdm+ymdd;
        if(elems["oscnt_cyouzai_date"]){
            if(elems["oscnt_cyouzai_date"].value != ""){
                elems["oscnt_cyouzai_date"].value = parseInt(elems["oscnt_cyouzai_date"].value) < parseInt(oscnt_cyouzai_date) ? elems["oscnt_cyouzai_date"].value : oscnt_cyouzai_date;
            }
            else{
                elems["oscnt_cyouzai_date"].value = oscnt_cyouzai_date;
            }
        }
    }

    if (tmpS) {
        if (yh["yoho_kubun"] == "C" && is_input_injection_content) {
            // 注射箋用法で「内容入力あり」の場合、「〜　　日分　　日おき」は表示しない
            out.push("| " + tmpS);
        } else {
            out.push("| " + tmpS + "〜");
        }
    }

    tmp = "";
    if (yh["yoho_touyo_nichi_bun"]!="") tmp += "/" + yh["yoho_touyo_nichi_bun"] + "日分";
    if (yh["yoho_touyo_nichi_oki"]!="") tmp += "/" + yh["yoho_touyo_nichi_oki"] + "日おき";
    if (yh["yoho_touyo_teiji_tunagi"] == 1) tmp += "/" + "定時つなぎ";    // 定時つなぎ
    if (tmp) out.push(tmp.substr(1));

    //====================
    // 用法(6-5)：内用薬(2)
    //====================
    var value;
    if (yh["yoho_kubun"]=="N"){
        tmp = "";
        yh["yoho_naiyo_chozai_siji"] = "";
        for (key in CHOZAI_SIJI_CAPTIONS) {
            suffix = key.toLowerCase();
            if (!elems["yoho_naiyo_chozai_siji_" + suffix]) {
                // 要素が存在していなければスキップ
                continue;
            }
            value = checkValue("yoho_naiyo_chozai_siji_" + suffix);
            yh["yoho_naiyo_chozai_siji"] += value;
            if (value) {
                tmp += "/" + CHOZAI_SIJI_CAPTIONS[key];
            }
        }

        if (tmp) out.push("| " + tmp.substr(1));
        else out.push("| ");
    }

    //====================
    // 用法(6-6)：共通(2)
    //====================
    yh["yoho_comment"] = textValue("yoho_comment");
    yh["yoho_comment_copy"] = textValue("yoho_comment");
    if (yh["yoho_comment"]) out.push("| " + yh["yoho_comment"]);
    else out.push("| " );

    yh["yoho_display_string"] = out.join("　");
    yh["status"] = rpInfo[currentRP].isYohoEmpty ? (status_visible ? 1 : 0) : (status_visible ? 2 : 0) ;
    var yoho_gaiyo_touyo_kaisu = selectText("yoho_gaiyo_touyo_kaisu_cd");
    var yoho_gaiyo_siyou_houhou = selectText("yoho_gaiyo_siyou_houhou_cd");

    delete(rpInfo[currentRP].isYohoEmpty);
    rpInfo[currentRP].yohoInfo[0] = yh;
}

//**************************
// 用法(6-1)：内用薬(1)
//**************************
function setToRPYohoNaiyo1(yh, out, tmp) {
    if (yh["yoho_kubun"] == "C") {  // 用法内容
        yh["yoho_content"] = textValue("yoho_content"); // 用法内容
        if (yh["yoho_content"] != "") {
            out.push(yh["yoho_content"].replace(/　/g, " "));
        }
    }

    var cyusya_out = [];
    if (yh["yoho_kubun"] == "C" && is_input_injection_content) {    // 用法内容入力あり
        setToRPYohoNaiyoCyusya(yh, cyusya_out);
    }

    if (yh["yoho_kubun"] == "N" || yh["yoho_kubun"] == "C") {    // 回数
        yh["yoho_naiyo_per_day"] = selectValue("yoho_naiyo_per_day");
        yoho_naiyo_per_day = parseInt(yh["yoho_naiyo_per_day"]);
        if (yh["yoho_naiyo_per_day"] != "") {
            var per_day = "１日" + yh["yoho_naiyo_per_day"] + "回";
            if (yh["yoho_kubun"] == "C") {
                cyusya_out.push(per_day);
            } else {
                out.push(per_day);
            }
        }
    }

    if (yh["yoho_kubun"] == "C" && !is_input_injection_content) {    // 用法内容入力なし
        setToRPYohoNaiyoCyusya(yh, cyusya_out);
    }

    // 注射タイミング
    if (yh["yoho_kubun"]=="C"){
        yh["yoho_cyusyatiming"] = "";
        for (var i = 1; i <= 6; i++) {
            yh["yoho_cyusyatiming"] += "\t" + textValue("yoho_cyusyatiming"+i);
            if (textValue("yoho_cyusyatiming"+i) != "") {
                cyusya_out.push(textValue("yoho_cyusyatiming"+i));
            }
        }
        yh["yoho_cyusyatiming"] = yh["yoho_cyusyatiming"].substr(1);
    }

    var length = cyusya_out.length;
    var i;
    if (length) {
        out.push("| ");
        for (i = 0; i < length; i++) {
            out.push(cyusya_out[i]);
        }
    }

    if (yh["yoho_kubun"]=="N"){
        var yoho_naiyo_timing = "";
        yh["yoho_naiyo_timing"] = "";
        for (var i = 1; i <= 8; i++) yh["yoho_naiyo_timing"] += "\t" + checkValue("yoho_naiyo_timing_"+i);
        yh["yoho_naiyo_timing"] = yh["yoho_naiyo_timing"].substr(1);

        yh["yoho_naiyo_timing_fukintou"] = "";
        for (var i = 1; i <= 8; i++) yh["yoho_naiyo_timing_fukintou"] += "\t" + textValue("yoho_naiyo_timing_fukintou_"+i);
        yh["yoho_naiyo_timing_fukintou"] = yh["yoho_naiyo_timing_fukintou"].substr(1);

        var toyaku_check_count = 0;
        var toyaku_input_count = 0;
        var toyaku_check = false;
        var j = 0;
        var minzen = "";
        var minzen_fukintou = false;

        for (i = 1; i <= 8; i++) {
            if (!tabAryValue(yh["yoho_naiyo_timing"],i)) {
                yoho_naiyo_timing = yoho_naiyo_timing+"0";
            } else {
                j++;
                if(toInt(yh["yoho_naiyo_per_day"]) == 1){
                    tmp += NAIYO_TIMING_CAPTIONS[i];
                }
                else if(toInt(yh["yoho_naiyo_per_day"]) == 2){
                    if(j == 1) tmp += NAIYO_TIMING_CAPTIONS[i];
                    else tmp += "、" + NAIYO_TIMING_CAPTIONS[i];
                }
                else if(toInt(yh["yoho_naiyo_per_day"]) == 3){
                    match_pattern1 = /朝〜昼/g;
                    match_pattern2 = /起床時、朝〜昼/g;
                    match_pattern3 = /起床時、昼〜夕/g;
                    match_pattern4 = /眠前、朝〜昼/g;
                    match_pattern5 = /眠前、昼〜夕/g;
                    match_pattern6 = /朝/g;
                    match_pattern7 = /起床時/g;
                    match_pattern8 = /眠前/g;
                    if(tmp.search(match_pattern1) == 0) {
                        tmp += "、" + NAIYO_TIMING_CAPTIONS[i];
                    }
                    else if(tmp.search(match_pattern6) >= 0 && tmp.search(match_pattern1) < 0) {
                        tmp += NAIYO_TIMING_CAPTIONS[i];
                    }
                    else if(NAIYO_TIMING_CAPTIONS[i].search(match_pattern7) == 0 ||(tmp.search(match_pattern7) == 0 || (tmp.search(match_pattern2) > 0|| tmp.search(match_pattern3) > 0) )){
                        if(j == 1) {
                            if(!fukintou_naiyo_timing) tmp += NAIYO_TIMING_CAPTIONS[i] + "、";
                            else {
                                tmp += NAIYO_TIMING_CAPTIONS[i] + tabAryValue(yh["yoho_naiyo_timing_fukintou"],i) + "、";
                                minzen_fukintou = true;
                            }
                        }
                        else if(j == 2) {
                            if((NAIYO_TIMING_CAPTIONS[i] == "朝〜昼") || (NAIYO_TIMING_CAPTIONS[i] == "昼〜夕") || (NAIYO_TIMING_CAPTIONS[i] == "夕〜眠前")){
                                tmp += NAIYO_TIMING_CAPTIONS[i] + "/";
                            }
                            else {
                                tmp += NAIYO_TIMING_CAPTIONS[i];
                            }
                        }
                        else if(j == 3) {tmp += NAIYO_TIMING_CAPTIONS[i];}
                    }
                    else if(NAIYO_TIMING_CAPTIONS[i].search(match_pattern8) == 0 ||(tmp.search(match_pattern8) == 0 || (tmp.search(match_pattern2) > 0|| tmp.search(match_pattern3) > 0) )){
                        if(j == 1) {
                            if(!fukintou_naiyo_timing) tmp += NAIYO_TIMING_CAPTIONS[i] + "、";
                            else {
                                tmp += NAIYO_TIMING_CAPTIONS[i] + tabAryValue(yh["yoho_naiyo_timing_fukintou"],i) + "、";
                                minzen_fukintou = true;
                            }
                        }
                        else if(j == 2) {
//                          tmp += NAIYO_TIMING_CAPTIONS[i] + "/";
                            if((NAIYO_TIMING_CAPTIONS[i] == "朝〜昼") || (NAIYO_TIMING_CAPTIONS[i] == "昼〜夕") || (NAIYO_TIMING_CAPTIONS[i] == "夕〜眠前")){
                                tmp += NAIYO_TIMING_CAPTIONS[i] + "/";
                            }
                            else {
                                tmp += NAIYO_TIMING_CAPTIONS[i];
                            }
                        }
                        else if(j == 3) {tmp += NAIYO_TIMING_CAPTIONS[i];}
                    }
                    else {
                        if(j == 2) {
                            tmp += "、" + NAIYO_TIMING_CAPTIONS[i];
                        }
                        else {
                            tmp += NAIYO_TIMING_CAPTIONS[i];
                        }
                    }
                }
                else if(toInt(yh["yoho_naiyo_per_day"]) == 4){
                    match_pattern2 = /起床時、朝〜昼/g;
                    if(j == 2){
                        tmp += "、" + NAIYO_TIMING_CAPTIONS[i] ;
                    }
                    else if(tmp.search(match_pattern2) == 0){
                        tmp += "、" + NAIYO_TIMING_CAPTIONS[i];
                    } else {
                        tmp += NAIYO_TIMING_CAPTIONS[i];
                    }
                }
                else if(NAIYO_TIMING_CAPTIONS[i] == "眠前"){
                        minzen = "、" + NAIYO_TIMING_CAPTIONS[i];
                        minzen += tabAryValue(yh["yoho_naiyo_timing_fukintou"],i);
                        minzen_fukintou = true;

                }
                else if(toInt(yh["yoho_naiyo_per_day"]) == 5){
                    if(j == 1) {
                        if(!fukintou_naiyo_timing) tmp += NAIYO_TIMING_CAPTIONS[i] + "、";
                        else {
                            tmp += NAIYO_TIMING_CAPTIONS[i] + tabAryValue(yh["yoho_naiyo_timing_fukintou"],i) + "、";
                            minzen_fukintou = true;
                        }
                    }
                    else {
                        tmp += NAIYO_TIMING_CAPTIONS[i];
                    }
                }
                else {
                    if(NAIYO_TIMING_CAPTIONS[i] == "眠前"){
                        minzen = "、" + NAIYO_TIMING_CAPTIONS[i];
                    } else if(j == 2 || i > 5 || (minzen && j == 3)){
                        tmp += "、" + NAIYO_TIMING_CAPTIONS[i];
                    }
                    else {
                        tmp += NAIYO_TIMING_CAPTIONS[i];
                    }
                }

                yoho_naiyo_timing = yoho_naiyo_timing+"1";
                toyaku_check_count++;
                if(i > 2) toyaku_check = true;
                if (!fukintou_naiyo_timing) continue;
                if(minzen_fukintou != true) tmp += tabAryValue(yh["yoho_naiyo_timing_fukintou"],i);
                minzen_fukintou = false;
                if(tabAryValue(yh["yoho_naiyo_timing_fukintou"],i)) toyaku_input_count += parseInt(tabAryValue(yh["yoho_naiyo_timing_fukintou"],i));
            }
        }
        tmp = tmp.replace(/[、]*$/, "");
        /*
        yoho_naiyo_per_day = parseInt(textValue("yoho_naiyo_per_day"));
        if( toyaku_check_count > 0 && !fukintou_naiyo_timing && yoho_naiyo_per_day < 9){
            if((yoho_naiyo_per_day != toyaku_check_count)){
                alert("指定された投薬回数と投薬タイミングの数が合いません。入力しなおしてください。");
                return;
            }
        }

        if(fukintou_naiyo_timing){
            if((yoho_naiyo_per_day != toyaku_input_count)){
                alert("指定された投薬回数と投薬タイミングの数が合いません。入力しなおしてください。");
                return;
            }
        }
        */
    }

    if (yh["yoho_kubun"]=="N"){
        var yoho_naiyo_meal_timing = "";

        yh["yoho_naiyo_meal_timing"] = "";
        for (var i = 1; i <= 6; i++) yh["yoho_naiyo_meal_timing"] += "\t" + checkValue("yoho_naiyo_meal_timing_"+i);
        yh["yoho_naiyo_meal_timing"] = yh["yoho_naiyo_meal_timing"].substr(1);

        yh["yoho_naiyo_meal_timing_fukintou"] = "";
        for (var i = 1; i <= 6; i++) yh["yoho_naiyo_meal_timing_fukintou"] += "\t" + textValue("yoho_naiyo_meal_timing_fukintou_"+i);
        yh["yoho_naiyo_meal_timing_fukintou"] = yh["yoho_naiyo_meal_timing_fukintou"].substr(1);

        yh["yoho_naiyo_hour_dist"] = textValue("yoho_naiyo_hour_dist");

        for (i = 1; i <= 6; i++) {
            if (!tabAryValue(yh["yoho_naiyo_meal_timing"],i)) {
                yoho_naiyo_meal_timing = yoho_naiyo_meal_timing+"0";
            } else {
                yoho_naiyo_meal_timing = yoho_naiyo_meal_timing+"1";
                tmp += NAIYO_MEAL_TIMING_CAPTIONS[i];
                if (!fukintou_naiyo_meal_timing) continue;
                tmp += tabAryValue(yh["yoho_naiyo_meal_timing_fukintou"],i);
            }
        }

        if(minzen){
            tmp += minzen;
        }

        // 食前、食前前、食直後、食後、食中、食間で、表示されているものを抽出し、メッセージ用に文字列を作成
        var meal_timing_str = "";
        var meal_timing_visible = $("input[id^='yoho_naiyo_meal_timing_']:visible");
        for (i = 0; i < meal_timing_visible.length; i++) {
            if (i != 0) {
                meal_timing_str += "、";
            }
            meal_timing_str += $($(meal_timing_visible[i]).parent()[0]).text();
        }

        if(!toyaku_check && parseInt(yoho_naiyo_meal_timing) != 0){
            alert("食事の設定がされていません。" + meal_timing_str + "の選択をクリアしてください。");
            return;
        }
        if (tmp) out.push("| "+tmp.substr(0));
        else out.push("| ");

        if (yh["yoho_naiyo_hour_dist"]!="" && !yoho_naiyo_timing_ishi_shiji) {
            if((parseInt(yoho_naiyo_meal_timing) > 0 && toyaku_check) || parseInt(toyaku_check_count) > 0){
                alert("時間間隔が指定されています。投薬タイミングの指示を入力しないか、もしくは時間間隔の指定をしないでください。");
                return;
            }
            out.push("| " + yh["yoho_naiyo_hour_dist"]+"時間おき");
        }

        if(checkValue("yoho_naiyo_timing_ishi_shiji")){
            yh["yoho_naiyo_timing_ishi_shiji"] = 1;

            var yoho_naiyo_timing_ishi_shiji = textValue("yoho_naiyo_timing_ishi_shiji") ;
            yoho_naiyo_per_day = parseInt(textValue("yoho_naiyo_per_day"));
            if(yoho_naiyo_timing_ishi_shiji){
                out.push("| 医師の指示通り ");
            }
        } else {
            yh["yoho_naiyo_timing_ishi_shiji"] = 0;
        }

        if( !yh["yoho_naiyo_hour_dist"] && !yoho_naiyo_timing_ishi_shiji){
            out.push("| ");
        }

    }

    var meal_timing_list = ["yoho_naiyo_timing_3","yoho_naiyo_timing_4","yoho_naiyo_timing_5"];
    var meal_check = 0;
    for (i=0; i<meal_timing_list.length; i++){
        var chk = document.getElementById(meal_timing_list[i]);
        if (chk.checked) meal_check = 1;
    }

    if(toyaku_check && parseInt(yoho_naiyo_meal_timing) == 0 && meal_check == 1) {
        alert(meal_timing_str + "のいずれかを選択してください。");
        return;
    }

    if (yh["yoho_kubun"]=="N"){
        tmp = setToRPYohoNaiyoHours(yh, out);
        if(!tmp) out.push("| ");
    }
}

//**************************
// タイミング以外の注射指示内容をセット
//**************************
function setToRPYohoNaiyoCyusya(yh, out) {
    yh["yoho_cyusyaroute_cd"]    = selectValue("yoho_cyusyaroute_cd");     // ルート
    yh["yoho_cyusyasiji_cd"]     = selectValue("yoho_cyusyasiji_cd");      // 投与法
    yh["yoho_cyusyasokudo"]      = textValue("yoho_cyusyasokudo");         // 注射速度
    yh["yoho_cyusyasokudo_unit"] = selectValue("yoho_cyusyasokudo_unit");  // 注射速度単位

    if (yh["yoho_cyusyaroute_cd"] != "") {  // ルート
        out.push(selectText("yoho_cyusyaroute_cd"));
    }

    if (is_input_injection_content) {    // 用法内容入力あり
        if (yh["yoho_cyusyasokudo"] + yh["yoho_cyusyasokudo_unit"] != "") {  // 速度
            out.push(yh["yoho_cyusyasokudo"] + selectText("yoho_cyusyasokudo_unit"));
        }

        if (yh["yoho_cyusyasiji_cd"] != "") {    // 投与法
            out.push(selectText("yoho_cyusyasiji_cd"));
        }
    } else {    // 用法内容入力なし
        if (yh["yoho_cyusyasiji_cd"] != "") {  // 投与法
            out.push(selectText("yoho_cyusyasiji_cd"));
        }

        if (yh["yoho_cyusyasokudo"] + yh["yoho_cyusyasokudo_unit"] != "") {  // 速度
            out.push(yh["yoho_cyusyasokudo"] + selectText("yoho_cyusyasokudo_unit"));
        }
    }
}

//**************************
// 時刻指定RPデータ作成
//**************************
function setToRPYohoNaiyoHours(yh, out) {
    yh["yoho_naiyo_hours"] = "";
    yh["yoho_naiyo_hours_fukintou"] = "";
    for (var i = 1; i <= maxYohoNaiyoHours; i++) {
        if (!elems["yoho_naiyo_hours_" + i]) {
            continue;
        }
        yh["yoho_naiyo_hours"] += "\t" + textValue("yoho_naiyo_hours_" + i);
        yh["yoho_naiyo_hours_fukintou"] += "\t" + textValue("yoho_naiyo_hours_fukintou_"+i);
    }
    yh["yoho_naiyo_hours"] = yh["yoho_naiyo_hours"].substr(1);
    yh["yoho_naiyo_hours_fukintou"] = yh["yoho_naiyo_hours_fukintou"].substr(1);

    var tmp = "";
    for (var i = 1; i <= maxYohoNaiyoHours; i++) {
        if (!tabAryValue(yh["yoho_naiyo_hours"], i)) {
            continue;
        }
        tmp += "/" + tabAryValue(yh["yoho_naiyo_hours"], i) + "時";

        if (!fukintou_naiyo_hours) {
            continue;
        }
        tmp += tabAryValue(yh["yoho_naiyo_hours_fukintou"], i);
    }
    if (tmp) {
        out.push("| " + tmp.substr(1));
    }

    return tmp;
}

//**************************
// RP表の領域の、HTMLを作成
//**************************
function createRPTableHTML(idx, rpInfoMaxCnt) {
    var dat = [];
    dat.push('<table style="width:100%;">');

    var medBgColor = "";
    var order_status;
    var yc = "";
    var yt = "";
    var key;

    for(var i = 1; i <= rpInfoMaxCnt ; i++){
        var cellBgColor = "";
        var cell23ForeColor = "color:#aaa;";
        var cell45ForeColor = "";
        var medTopBorder = idx == 0 ? "" : " border-top:0;";
        if(is_template_readonly){
            cellBgColor = "";
            cell123ForeColor = "color:#aaa;";
            celll45ForeColor = "";
        }
        var medInfoMaxCnt = (rpInfo[i] && rpInfo[i].medInfo && rpInfo[i].medInfo.length) ? ((rpInfo[i].medInfo.length == 1) ? 1 : (rpInfo[i].medInfo.length - 1)): 0;

        for (var j = 0; j < medInfoMaxCnt ; j++) {
            if (j>0) medTopBorder = " border-top:0;";
            order_status = "";
            var minfo = rpInfo[i].medInfo[j] ? rpInfo[i].medInfo[j]: EMPTY_MEDINFO;
            medBgColor = (is_template_readonly ? "" : (i == currentRP && (j == currentINFO) && (currentTypeMy == 1) ? " background-color:#8beefc;" : "")); // 選択中の行の場合、行に背景色をつける。
            if (!is_template_readonly){
                cellBgColor = (i == currentRP) ? " background-color:#8beefc;" : "";
                cell23ForeColor = (i == currentRP) ? " color:#0073ff;" : " color:#aaa;";
                cell45ForeColor = (i == currentRP) ? " color:#0073ff;" : "";
                if(minfo.status == 9){
                    cell23ForeColor = " color:#CCCCCC;" ;
                    cell45ForeColor = " color:#CCCCCC;" ;
                    dat.push('<tr background-color="#fff\">');
                } else {
                    cell23ForeColor = " color:#aaa;" ;
                    cell45ForeColor = " color:#aaa;" ;
                    dat.push('<tr onclick="rpListSelected(1,'+i+','+j+');" onmouseover="this.style.backgroundColor=\'#fefc8f\'" onmouseout="this.style.backgroundColor=\'#fff\'">');
                }
            } else {
                dat.push('<tr>');
            }
            var sb = '';
            if (minfo.seibun_bunrui=="N") sb = "内服";
            if (minfo.seibun_bunrui=="C") sb = "注射";
            if (minfo.seibun_bunrui=="G") sb = "外用";
            if (minfo.seibun_bunrui=="Y") sb = "頓用";
            if (minfo.seibun_bunrui=="P") sb = "頓服";
            order_status = status_visible ? (minfo.status == 9 ? "削":(minfo.status == 2 ? "修": (minfo.status == 1 ? "追": ""))) : "";

            if (new_flg == 1 || copy_flg == 1) {
                order_status = '';
                minfo.status = '';
            }
            var ftplc = '#aaa';
            if (minfo.status == 9) {
                ftplc = '#CCC';
            }

            // 薬品名
            var mname = (!minfo.med_name ? "<br />" : minfo.med_name);

            // 薬剤毎の調剤指示
            var med_chozai_siji = createMedChozaiSijiStr(minfo);

            // 後発品変更不可
            var med_kouhatu_fuka = "";
            if (minfo.med_kouhatu_fuka == "1") {
                med_kouhatu_fuka = "×";
            }

            dat.push('<th style="border-bottom:0px #fff solid; width:36px;' + cellBgColor + medTopBorder + '"><font style="color:#0073ff;">' + (j<=0?i:"") + '</font></th>');
            dat.push('<th style="width:37px;'  + medBgColor + cell23ForeColor + '">' + order_status + '</th>');
            dat.push('<th style="width:26px;'  + medBgColor + cell23ForeColor + '"><font style="color:#0073ff;">薬</font></th>');
            dat.push('<th style="width:48px;'  + cell23ForeColor + '"><font color="#0073ff">' + sb + '</font></th>');
            if (is_add_remark_rp) {
                dat.push('<td style="width:212px;' + cell45ForeColor + '"><font color="' + ftplc + '">' + med_kouhatu_fuka + mname + " " + minfo.remark + " " + med_chozai_siji + '</font></td>');
            } else {    // 別名を表示しない
                dat.push('<td style="width:212px;' + cell45ForeColor + '"><font color="' + ftplc + '">' + med_kouhatu_fuka + mname + " " + med_chozai_siji + '</font></td>');
            }

            yt = minfo.yoryo_type;

            if(yt == 1) yt = "1日";
            else if(yt == 2) yt = "1回";
            else yt = "";

            dat.push('<th style="width:87px;' + cell45ForeColor + '"><font color="' + ftplc + '">'+ yt + minfo.yoryo + minfo.med_unit_name + '</font></th>');
            dat.push('<input type="hidden" name="yj_cd_' + i + '_' + j + '" id="yj_cd_' + i + '_' + j + '" value="' + (minfo.yj_cd ? minfo.yj_cd : "") + '">');
            dat.push('<input type="hidden" name="seibun_bunrui" value="' + minfo.seibun_bunrui + '">');
        }
        var yohoInfoMaxCnt = (rpInfo[i] && rpInfo[i].medInfo && rpInfo[i].yohoInfo.length) ? ((rpInfo[i].yohoInfo.length == 1) ? 1 : (rpInfo[i].yohoInfo.length - 1)) : 0;
        for (var j = 0; j < yohoInfoMaxCnt ; j++) {
            order_status = "";
            var yoho = rpInfo[i].yohoInfo[j] ? rpInfo[i].yohoInfo[j] : EMPTY_YOHOINFO;


            medBgColor = (is_template_readonly ? "" : (i == currentRP && (j == currentINFO) && (currentTypeMy == 2) ? " background-color:#8beefc;" : "")); // 選択中の行の場合、行に背景色をつける。
            if (!is_template_readonly){
                if(yoho.status == 9){
                    cell23ForeColor = " color:#CCCCCC;" ;
                    cell45ForeColor = " color:#CCCCCC;" ;
                    dat.push('<tr background-color="#fff\">');
                } else {
                    cell23ForeColor = " color:#aaa;" ;
                    cell45ForeColor = " color:#aaa;" ;
                    dat.push('<tr onclick="rpListSelected(2,'+i+','+j+');" onmouseover="this.style.backgroundColor=\'#fefc8f\'" onmouseout="this.style.backgroundColor=\'#fff\'">');
                }
            } else {
                dat.push('<tr>');
            }
            var disp = (rpInfo[i].isYohoEmpty ? "&nbsp;" : wbr(yoho.yoho_display_string).replace(/\n/g, "<br/>"));
            var yoho_kubun_disp = (rpInfo[i].isYohoEmpty ? "&nbsp;" : str(YOHO_KUBUN_CAPTIONS[yoho.yoho_kubun]));
            order_status = status_visible ? (yoho.status == 9 ? "削":(yoho.status == 2 ? "修": (yoho.status == 1 ? "追": ""))) : "";

            if (new_flg == 1 || copy_flg == 1) {
                order_status = '';
                yoho.status  = '';
            }
            if (j == (yohoInfoMaxCnt -1)) dat.push('<th style="border-top:0;' + cellBgColor + '"></th>');
            else
            dat.push('<td style="border-top:0; border-bottom:0;'+cellBgColor+'"></td>');

            if(yoho.yoho_display_string) yc = getYohoCode(yoho.yoho_display_string,yoho.yoho_kubun);

            dat.push('<th style="' + medBgColor + cell23ForeColor + '">' + order_status + '</th>');
            dat.push('<th style="' + medBgColor + cell23ForeColor + '"><font style="color:#0073ff;">用</font></th>');
            dat.push('<th style="' + cell23ForeColor + '"><font color="#0073ff">' + yoho_kubun_disp+'</font></th>');
            dat.push('<td style="' + cell45ForeColor + '" colspan="2"><font color="' + ftplc + '">' + disp +'</font></td>');
            dat.push('<input type="hidden" name="yoho_code_' + i + '" id="yoho_code_' + i + '" value="' + yc + '">');
            dat.push('<input type="hidden" name="yoho_comment_copy" id="yoho_comment_copy_' + i + '" value="' + yoho.yoho_comment_copy + '">');
        }
        dat.push('</tr>');
    }
    dat.push('</table>');

    return dat;
}

//********************************************
// 表示用薬剤毎の調剤指示文字列を生成する
//********************************************
function createMedChozaiSijiStr(minfo) {
    med_chozai_siji = "";
    for (var loop = 0; loop < minfo.med_chozai_siji.length; loop++) {
        if (med_chozai_siji) {
            med_chozai_siji += "/";
        }
        med_chozai_siji += CHOZAI_SIJI_CAPTIONS[minfo.med_chozai_siji.substr(loop, 1)];
    }
    if (med_chozai_siji) {
        med_chozai_siji = "(" + med_chozai_siji + ")";
    }

    return med_chozai_siji;
}

//********************************************
// 不均等リンクと入力ボックスの表示制御
//********************************************
function setFukintou(flag){
    if (!fukintou_naiyo_timing) {
        elems["yoho_naiyo_timing_fukintou_1"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_timing_fukintou_2"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_timing_fukintou_3"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_timing_fukintou_4"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_timing_fukintou_5"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_timing_fukintou_6"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_timing_fukintou_7"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_timing_fukintou_8"].style.display = (flag ? "" : "none");
    }

    if (!fukintou_naiyo_meal_timing) {
        elems["yoho_naiyo_meal_timing_fukintou_1"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_meal_timing_fukintou_2"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_meal_timing_fukintou_3"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_meal_timing_fukintou_4"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_meal_timing_fukintou_5"].style.display = (flag ? "" : "none");
        elems["yoho_naiyo_meal_timing_fukintou_6"].style.display = (flag ? "" : "none");
    }

    var i;
    if (!fukintou_naiyo_hours) {
        for (i = 1; i <= maxYohoNaiyoHours; i++) {
            if (!elems["yoho_naiyo_hours_fukintou_" + i]) {
                continue
            }
            elems["yoho_naiyo_hours_fukintou_" + i].style.display = (flag ? "" : "none");
        }
    }
}

//********************************************
// 不均等リンクと入力ボックスの表示制御
//********************************************
function setFukintouTiming(elem){
  fukintou_naiyo_timing = fukintou_naiyo_timing ? false : true;
  var fukintou_elem = document.getElementById("link_fukintou_naiyo_timing");
  if (fukintou_naiyo_timing) {  // 不均等
    for (var i = 1; i <= 8; i++) {
      elems["yoho_naiyo_timing_fukintou_"+i].style.display = "";
    }
    fukintou_elem.textContent = "均等";
  } else {            // 均等
    for (var i = 1; i <= 8; i++) {
      elems["yoho_naiyo_timing_fukintou_"+i].style.display = "none";
      elems["yoho_naiyo_timing_fukintou_"+i].value = "";
    }
    fukintou_elem.textContent = "不均等";
  }
}

//********************************************
// 不均等リンクと入力ボックスの表示制御
//********************************************
function setFukintouMealTiming(elem){
  fukintou_naiyo_meal_timing = fukintou_naiyo_meal_timing ? false : true;
  elems["yoho_naiyo_meal_timing_fukintou_1"].style.display = (fukintou_naiyo_meal_timing ? "" : "none");
  elems["yoho_naiyo_meal_timing_fukintou_2"].style.display = (fukintou_naiyo_meal_timing ? "" : "none");
  elems["yoho_naiyo_meal_timing_fukintou_3"].style.display = (fukintou_naiyo_meal_timing ? "" : "none");
  elems["yoho_naiyo_meal_timing_fukintou_4"].style.display = (fukintou_naiyo_meal_timing ? "" : "none");
  elems["yoho_naiyo_meal_timing_fukintou_5"].style.display = (fukintou_naiyo_meal_timing ? "" : "none");
  elems["yoho_naiyo_meal_timing_fukintou_6"].style.display = (fukintou_naiyo_meal_timing ? "" : "none");
  document.getElementById("link_fukintou_naiyo_meal_timing").textContent = (fukintou_naiyo_meal_timing ? "均等" : "不均等");
}

//********************************************
// 不均等リンクと入力ボックスの表示制御
//********************************************
function setFukintouHours(elem){
    fukintou_naiyo_hours = fukintou_naiyo_hours ? false : true;
    var fukintou_elem = document.getElementById("link_fukintou_naiyo_hours");
    if (fukintou_naiyo_hours) { // 不均等
        for (var i = 1; i <= maxYohoNaiyoHours; i++) {
            if (!elems["yoho_naiyo_hours_fukintou_"+i]) {
                continue;
            }
            elems["yoho_naiyo_hours_fukintou_"+i].style.display = "";
        }
        fukintou_elem.textContent = "均等";
    } else {                    // 均等
        for (var i = 1; i <= maxYohoNaiyoHours; i++) {
            if (!elems["yoho_naiyo_hours_fukintou_"+i]) {
                continue;
            }
            elems["yoho_naiyo_hours_fukintou_"+i].style.display = "none";
            elems["yoho_naiyo_hours_fukintou_"+i].value = "";
        }
        fukintou_elem.textContent = "不均等";
    }
}

//********************************************
// 画面各場所の表示やEnable状態をセットする
//********************************************
function changeDisplay(pos, elem)
{
    if (pos == "med") {
        if (elem && elem.id.substr(0, 13) == "div_link_med_") {
            var extent = elem.id.replace("div_link_med_", "");
            isSet = (extent=="set" ? "set" : "");
            if (extent != "set" && extent != "back") medExtent = extent;
        }
        if (elem && elem.id == "med_view"){
            listType = (listType=="tabletype" ? "treetype" : "tabletype");
        }
    }

    if (pos=="yoho"){
        fukintou_naiyo_timing = false;
        fukintou_naiyo_meal_timing = false;
        fukintou_naiyo_hours = false;

        if (elem && elem.id.substr(0, 14)=="div_link_yoho_"){
            var tmp = elem.id.replace("div_link_yoho_", "");
            if (tmp=="tonpu" ) yoho_kubun = "P";
            if (tmp=="tonyou") yoho_kubun = "Y";
            if (tmp=="naiyo") yoho_kubun = "N";
            if (tmp=="gaiyo") yoho_kubun = "G";
            if (tmp=="cyusya") yoho_kubun = "C";
        }
    }

    var curSeibunBunrui = textValue("current_seibun_bunrui");
    if (curSeibunBunrui == "") {
        curSeibunBunrui = yoho_kubun;
    }

    if (curSeibunBunrui == "C") {
        yoho_kubun = "C";
    }

    // 要素の表示/非表示を制御
    elems["div_right_set"].style.display = (isSet=="set" ? "" : "none");
    elems["div_right_med"].style.display = (isSet!="set" ? "" : "none");
    elems["div_right_yoho"].style.display = (isSet!="set" ? "" : "none");

    elems["div_link_med_hist"].style.backgroundColor = (!isSet && medExtent == "hist" ? "#ffffe6" : "#4baaf8");
    elems["div_link_med_saiyo"].style.backgroundColor = (!isSet && medExtent == "saiyo" ? "#ffffe6" : "#4baaf8");
    elems["div_link_med_allmed"].style.backgroundColor = (!isSet && medExtent == "allmed" ? "#ffffe6" : "#4baaf8");
    elems["div_link_med_set"].style.backgroundColor = (!isSet && medExtent == "set" ? "#ffffe6" : "#b9b9ff");
    elems["div_link_med_back"].style.backgroundColor = ( isSet ? "#ffffe6" : "#4baaf8");

    elems["div_med_list_treetype"].style.display = (listType == "treetype" ? "" : "none");
    elems["div_med_list_tabletype"].style.display = (listType == "tabletype" ? "" : "none");

    elems["div_link_yoho_naiyo"].style.backgroundColor = (yoho_kubun == "N" ? "#ffffe6" : "#4baaf8");
    elems["div_link_yoho_gaiyo"].style.backgroundColor = (yoho_kubun == "G" ? "#ffffe6" : "#4baaf8");
    elems["div_link_yoho_tonpu"].style.backgroundColor = (yoho_kubun == "P" ? "#ffffe6" : "#4baaf8");
    elems["div_link_yoho_tonyou"].style.backgroundColor = (yoho_kubun == "Y" ? "#ffffe6" : "#4baaf8");
    elems["div_link_yoho_cyusya"].style.backgroundColor = (yoho_kubun == "C" ? "#ffffe6" : "#4baaf8");
    elems["span_yoho_touyo"].style.display = (yoho_kubun == "C" && is_input_injection_content ? "none" : "");

    elems["div_link_yoho_naiyo"].style.display = (curSeibunBunrui == "C" || curSeibunBunrui == "Y" ? "none" : "");
    elems["div_link_yoho_gaiyo"].style.display = (curSeibunBunrui == "C" || curSeibunBunrui == "P" ? "none" : "");
    elems["div_link_yoho_tonpu"].style.display = (curSeibunBunrui == "C" || curSeibunBunrui == "Y" || curSeibunBunrui == "G" ? "none" : "");
    elems["div_link_yoho_tonyou"].style.display = (curSeibunBunrui == "C" || curSeibunBunrui == "P" || curSeibunBunrui == "N" ? "none" : "");
    elems["div_link_yoho_cyusya"].style.display = (curSeibunBunrui == "C" ? "" : "none");

    elems["div_yoho_naiyo_per_day"].style.display = (yoho_kubun == "N" || yoho_kubun == "C" ? "" : "none");
    elems["div_yoho_cyusya_setting"].style.display = (yoho_kubun == "C" ? "" : "none");
    elems["div_yoho_cyusyasiji"].style.display = (yoho_kubun == "C" ? "" : "none");
    elems["div_yoho_cyusyaroute"].style.display = (yoho_kubun == "C" ? "" : "none");
    elems["div_yoho_cyusyasokudo"].style.display = (yoho_kubun == "C" ? "" : "none");
    elems["div_yoho_cyusyatiming"].style.display = (yoho_kubun == "C" ? "" : "none");
    elems["div_yoho_content"].style.display = (yoho_kubun == "C" ? "" : "none");
    elems["div_yoho_naiyo_timing"].style.display = (yoho_kubun == "N" ? "" : "none");
    elems["div_yoho_naiyo_meal_timing"].style.display = (yoho_kubun == "N" ? "" : "none");
    elems["div_yoho_naiyo_hour_dist"].style.display = (yoho_kubun == "N" ? "" : "none");
    elems["div_yoho_naiyo_hours"].style.display = (yoho_kubun == "N" ? "" : "none");
    elems["div_yoho_gaiyo_touyo_kaisu"].style.display = ((yoho_kubun == "G" || yoho_kubun == "Y") ? "" : "none");
    elems["div_yoho_gaiyo_siyou_houhou"].style.display = ((yoho_kubun == "G" || yoho_kubun == "Y") ? "" : "none");
    elems["div_yoho_gaiyo_bui"].style.display = ((yoho_kubun == "G" || yoho_kubun == "Y") ? "" : "none");
    elems["div_yoho_tonpu_kaisu"].style.display = (yoho_kubun == "P" ? "" : "none");
    elems["div_yoho_tonpu_timing"].style.display = (yoho_kubun == "P" ? "" : "none");
    elems["div_yoho_naiyo_chozai_siji"].style.display = (yoho_kubun == "N" ? "" : "none");
    if (elems["div_yoho_touyo_teiji_tunagi"]) {
        elems["div_yoho_touyo_teiji_tunagi"].style.display = (yoho_kubun == "C" ? "none" : "");
    }

    if(pos == "") setFukintou(false);
    medYohoButtonEnableCheck();
    setButtonEnableCheck();
    setToRP("","");
    if (elem && elem.id == "med_view" && listType == "treetype") {
        medSearchForTreeView(currentTreeSearchKaisou, currentTreeSearchParentId);
    }

    if (isSet) {
        elems["new_set_name"].value = "";
        if (elem && elem.id.substr(0, 13)=="div_link_med_"){
            elems["current_set_name"].value = "";
            elems["current_set_seq"].value = "";
            setSearch();
        }
        setButtonEnableCheck();
    }
}

//**************************
// 投与期間の終了日を求める
//**************************
/*
function calcTouyoEndYmd(startYMD) {
  startYMD = startYMD.replace(/年|月|日/g,"");
  var ret = [0,0,0];
  var startY = toInt(selectValue("yoho_touyo_start_ymd_y"),10);
  var startM = toInt(selectValue("yoho_touyo_start_ymd_m"),10);
  var startD = toInt(selectValue("yoho_touyo_start_ymd_d"),10);
  var bun = toInt(textValue("yoho_touyo_nichi_bun"),10);
  var oki = toInt(textValue("yoho_touyo_nichi_oki"),10);
  if (!startY || !startM || !startD || !bun) return ret;
  if (isNaN(startY) || isNaN(startM) || isNaN(startD) || isNaN(bun)) return ret;
  var dt = new Date(startY, startM-1, startD);
  if (isNaN(dt)) return ret;
  if (dt.getFullYear() != startY && dt.getMonth() != startM-1 && dt.getDate() != startD) return ret;
  if (!oki || isNaN(oki)) oki = 0;
  var tosec = parseInt(oki+1) * (bun-1) * 1000 * 60 * 60 * 24;
  var ndt = new Date(parseInt(dt*1 + tosec*1));
  elems["yoho_touyo_end_ymd"].innerHTML = "(〜"+ndt.getFullYear()+"/"+parseInt(ndt.getMonth()+1*1)+"/"+ndt.getDate()+")";
  return [ndt.getFullYear(), (ndt.getMonth()+1*1), ndt.getDate()];
}
*/
//********************************************
// 単位ドロップダウンの表示/非表示切り替え
//********************************************
function showCurrentUnitSelection() {
  var list = elems["current_unit_selection"];
  list.style.display = list.style.display == "none" ? "" : "none";
  if (list.style.display==""){
    var curUnit = elems["current_unit"].value;
    for (var i = 0; i < list.options.length; i++){
      if (curUnit==list.options[i].value) list.options[i].selected = "selected";
    }
    list.focus();
    return;
  }
  elems["btn_current_unit_selection"].focus();
}

//********************************************
// 単位ドロップダウンの選択結果を入力ボックスに転記
//********************************************
function applyCurrentUnitSelection() {
  var list = elems["current_unit_selection"];
  var v = list.options[list.options.selectedIndex].value;
  elems["current_unit"].value = v;
  list.style.display = "none";
  setTimeout('elems["btn_current_unit_selection"].focus();', 200);
}

//********************************************
// 単位ドロップダウンの選択肢を用意する
//********************************************
function standbyCurrentUnitSelection(medInfo){
  var units = medInfo.units_separated_tab.split(",");
  var key = medInfo.seibun_bunrui + "@" + medInfo.zaikei_bunrui;
  if (unit_mst_list[key]) {
    for (var k =0; k < unit_mst_list[key].length; k++){
      var v = unit_mst_list[key][k];
      var exist = false;
      for (var u = 0; u < units.length; u++){
        if (units[u] == v) { exist = true; break; }
      }
      if (!exist) units.push(v);
    }
  }
  var list = elems["current_unit_selection"];
  list.options.length = units.length;
  var curUnit = elems["current_unit"].value;
  for (var i = 0; i < units.length; i++){
    list.options[i].text = units[i];
    list.options[i].value = units[i];
    if (curUnit==units[i]) list.options[i].selected = "selected";
  }
  elems["current_unit_selection"].style.display = "none";
}
function currentUnitSelectionKeyDown(e){
  e = e || window.event;
  if (e.keyCode==27) {
    elems["current_unit_selection"].style.display = "none";
    elems["btn_current_unit_selection"].focus();
    return false;
  }
  if (e.keyCode==13) {
    applyCurrentUnitSelection();
    return false;
  }
}

//********************************************
// 部位変更時に左右両方チェックを押せるかどうかをセット
//********************************************
function buiChanged(bui_cd){
  var okL = (bui_lrb_list[bui_cd] && bui_lrb_list[bui_cd].indexOf("L")>=0) ? true : false;
  var okR = (bui_lrb_list[bui_cd] && bui_lrb_list[bui_cd].indexOf("R")>=0) ? true : false;
  var okB = (bui_lrb_list[bui_cd] && bui_lrb_list[bui_cd].indexOf("B")>=0) ? true : false;

  elems["yoho_gaiyo_bui_l"].disabled = okL ? "" : "disabled";
  elems["yoho_gaiyo_bui_r"].disabled = okR ? "" : "disabled";
  elems["yoho_gaiyo_bui_b"].disabled = okB ? "" : "disabled";
  if (!okL) elems["yoho_gaiyo_bui_l"].checked = "";
  if (!okR) elems["yoho_gaiyo_bui_r"].checked = "";
  if (!okB) elems["yoho_gaiyo_bui_b"].checked = "";

  elems["label_yoho_gaiyo_bui_l"].style.color = okL ? "#000" : "#aaa";
  elems["label_yoho_gaiyo_bui_r"].style.color = okR ? "#000" : "#aaa";
  elems["label_yoho_gaiyo_bui_b"].style.color = okB ? "#000" : "#aaa";
}

//********************************************
// 頓服タイミング１を変更したときに、頓服タイミング２の選択肢をセット
//********************************************
function tonpu1TimingChanged(tonpu1_cd){
    var tonpu2list = tonpu2_mst_list[tonpu1_cd];
    var dropdown = elems["yoho_tonpu_timing2_cd"];
    var len = 0;
    for (var tonpu2_cd in tonpu2list) len++;

    if (len == 0) {
        // 頓服タイミング２の項目が無ければ、ドロップダウンリストを表示しない
        dropdown.options.selectedIndex = 0;
        dropdown.style.display = "none";
        return;
    }

    dropdown.options.length = len + 1;
    var len = 0;
    for (var tonpu2_cd in tonpu2list) {
        len++;
        dropdown.options[len].value = tonpu2_cd;
        dropdown.options[len].text = tonpu2list[tonpu2_cd];
    }
    dropdown.options.selectedIndex = 0;
    dropdown.style.display = (tonpu1_cd ? "" : "none");
    for (var i = 1; i <= 3; i++){
        elems["yoho_tonpu_timing2_text_"+i].style.display = "none";
        elems["span_yoho_tonpu_timing2_text_"+i].style.display = "none";
    }
}

//**************************
// 頓服タイミング２を変更したときに、頓服タイミング２の補足入力を制御
//**************************
function tonpu2TimingChanged(){
  var dropdown = elems["yoho_tonpu_timing2_cd"];
  var txt = dropdown.options[dropdown.options.selectedIndex].text;
  for (var i = 1; i <= 3; i++){
    var isActive = (txt.indexOf("{@"+i+"}") >= 0 ? true : false);
    elems["yoho_tonpu_timing2_text_"+i].style.display = (isActive ? "" : "none");
    elems["span_yoho_tonpu_timing2_text_"+i].style.display = (isActive ? "" : "none");
    elems["yoho_tonpu_timing2_text_"+i].value = "";
    elems["span_yoho_tonpu_timing2_text_"+i].innerHTML = (isActive ? "@"+i+"：" : "");
  }
}

function popupByoumeiSelect(){
  var session = document.getElementsByName("session");
  //var ipt = elems["byoumei"].value;
  var ipt = encodeURIComponent(encodeURIComponent(elems["byoumei"].value));
  window.open("summary_byoumei_select.php?session=" + session[0].value + "&search_input=" + ipt, "summary_byoumei_select_window", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700");
}
function call_back_summary_byoumei_select(id, result){
  elems["byoumei"].value = result.byoumei;
  elems["byoumei_code"].value = result.byoumei_code;
  elems["icd10"].value = result.icd10;
  setToRP('','');
}
function showAlternateProblemDiv(){
  var d = elems["problem_list_div"].style.display;
  elems["problem_list_div"].style.display = (d == "none" ? "" : "none");
}
function problemSelected(icd10, byoumei_code, problem_name){
  elems["byoumei"].value = problem_name;
  elems["byoumei_code"].value = byoumei_code;
  elems["icd10"].value = icd10;
  elems["problem_list_div"].style.display = "none";
}

function sendCooperationData(){
  sendCPRequest("C");
}
function printPDF(){
  // 引数が指定されている場合は控え印刷
  if (arguments.length == 1) {
    // 引数がない場合
    reservePrintPDF();
    return;
  }
  sendCPRequest("P");
}

// 控え印刷の場合
function reservePrintPDF(){
  sendCPRequest("R");
}

function printPDF2(){
  sendCPRequest("Y");
}
function printPDF4(){
  sendCPRequest("Y","4");
}
function sendCPRequest(reqType, reqType2)
{
  var max_rows = 0;
  formPdf = document.createElement("FORM");
  var b = document.getElementsByTagName("body");
  b[0].appendChild(formPdf);

  formPdf.name = "frm_pdf_print";
  if(reqType == "P" || reqType == "Y" || reqType == "R") formPdf.target = "_blank";
  if(reqType == "C") formPdf.target = "";
  formPdf.method = "post";
  if(reqType == "P" || reqType == "Y" || reqType == "R") formPdf.action = script_name;
  if(reqType == "C") formPdf.action = 'summary_tmpl_read.php';
  if(reqType == "C"){
    //login_emp_id
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "emp_id" );
    input.setAttribute( "value" , login_emp_id );
    formPdf.appendChild( input );

    //summary_seq
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "summary_seq" );
    input.setAttribute( "value" , summary_seq );
    formPdf.appendChild( input );

    //pt_id
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "pt_id" );
    input.setAttribute( "value" , pt_id );
    formPdf.appendChild( input );

    //xml_file
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "xml_file" );
    input.setAttribute( "value" , xml_file );
    formPdf.appendChild( input );

    //tmpl_id
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "tmpl_id" );
    input.setAttribute( "value" , tmpl_id );
    formPdf.appendChild( input );

    //summary_id
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "summary_id" );
    input.setAttribute( "value" , summary_id );
    formPdf.appendChild( input );

    //enc_diag
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "enc_diag" );
    input.setAttribute( "value" , enc_diag );
    formPdf.appendChild( input );

    //div_id
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "div_id" );
    input.setAttribute( "value" , div_id );
    formPdf.appendChild( input );

    //cre_date
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "cre_date" );
    input.setAttribute( "value" , cre_date );
    formPdf.appendChild( input );

    //seiki_emp_id
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "seiki_emp_id" );
    input.setAttribute( "value" , seiki_emp_id );
    formPdf.appendChild( input );

    //dairi_emp_id
    var input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "dairi_emp_id" );
    input.setAttribute( "value" , dairi_emp_id );
    formPdf.appendChild( input );
  }

  // session
  var input = document.createElement( "input" );
  input.setAttribute( "type" , "hidden" );
  input.setAttribute( "name" , "session" );
  input.setAttribute( "value" , session );
  formPdf.appendChild( input );

  // oscnt_schedule_seq
  var input = document.createElement( "input" );
  input.setAttribute( "type" , "hidden" );
  input.setAttribute( "name" , "oscnt_schedule_seq" );
  input.setAttribute( "value" , elems["schedule_seq"].value );
  formPdf.appendChild( input );

  // oscnt_pt_kana_nm
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_pt_kana_nm' );
  input.setAttribute( 'value' , oscnt_pt_kana_nm );
  formPdf.appendChild( input );

  // oscnt_pt_kaj_nm
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_pt_kaj_nm' );
  input.setAttribute( 'value' , elems["oscnt_pt_kaj_nm"].value );
  formPdf.appendChild( input );

  // oscnt_pt_sex
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_pt_sex' );
  if(reqType == "P" || reqType == "Y" || reqType == "R") input.setAttribute( 'value' , elems["oscnt_pt_sex"].value);
  if(reqType == "C") input.setAttribute( 'value' , ((elems["oscnt_pt_sex"].value == "男" || elems["oscnt_pt_sex"].value == "男性") ? "M" : "F"));
  formPdf.appendChild( input );

  // oscnt_pt_birth
  //var oscnt_pt_birth = getText(document.getElementById("oscnt_pt_birth"));
  var oscnt_pt_birth = convDateToNum(elems["oscnt_pt_birth"].value);
  if(reqType == "C") oscnt_pt_birth = oscnt_pt_birth.replace(/年|月|日/g,"");
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_pt_birth' );
  input.setAttribute( 'value' , oscnt_pt_birth );
  formPdf.appendChild( input );

  if(reqType == "P" || reqType == "Y" || reqType == "R") oscnt_pt_birth = elems["oscnt_pt_birth"].value.replace(/年|月/g,"\/");
  var oscnt_pt_age = getAge(oscnt_pt_birth.replace(/日/g,""));

  // oscnt_pt_age
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_pt_age' );
  input.setAttribute( 'value' , oscnt_pt_age );
  formPdf.appendChild( input );

  // oscnt_pt_id
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_pt_id' );
  input.setAttribute( 'value' , elems["oscnt_pt_id"].value );
  formPdf.appendChild( input );

  // oscnt_ward_name
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_ward_name' );
  input.setAttribute( 'value' , elems["oscnt_ward_name"].value );
  formPdf.appendChild( input );

  //oscnt_room_num
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_room_num' );
  input.setAttribute( 'value' , elems["oscnt_room_num"].value );
  formPdf.appendChild( input );

  //oscnt_department
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_department' );
  input.setAttribute( 'value' , sect_nm );
  formPdf.appendChild( input );

  //oscnt_department_code
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_department_code' );
  input.setAttribute( 'value' , getDepartmentCode(sect_nm) );
  formPdf.appendChild( input );

  //oscnt_name
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_name' );
  input.setAttribute( 'value' , elems["oscnt_name"].value );
  formPdf.appendChild( input );

  //oscnt_disease_name
  var oscnt_disease_name = "";
  if (elems["byoumei"].value) oscnt_disease_name = elems["byoumei"].value;
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'oscnt_disease_name' );
  input.setAttribute( 'value' ,oscnt_disease_name );
  formPdf.appendChild( input );

  var force_end_ymd_y = "";
  if (document.getElementById('force_end_ymd_y').value) force_end_ymd_y = document.getElementById('force_end_ymd_y').value;
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'force_end_ymd_y' );
  input.setAttribute( 'value', force_end_ymd_y );
  formPdf.appendChild( input );

  var force_end_ymd_m = "";
  if (document.getElementById('force_end_ymd_m').value) force_end_ymd_m = document.getElementById('force_end_ymd_m').value;
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'force_end_ymd_m' );
  input.setAttribute( 'value', force_end_ymd_m );
  formPdf.appendChild( input );

  var force_end_ymd_d = "";
  if (document.getElementById('force_end_ymd_d').value) force_end_ymd_d = document.getElementById('force_end_ymd_d').value;
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'force_end_ymd_d' );
  input.setAttribute( 'value', force_end_ymd_d );
  formPdf.appendChild( input );

  var reserve_flg = "";
  if (reqType == "R") reserve_flg = 1;
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  input.setAttribute( 'name' , 'reserve_flg' );
  input.setAttribute( 'value', reserve_flg );
  formPdf.appendChild( input );

  // direct_template_pdf_print_requested
  input = document.createElement( 'input' );
  input.setAttribute( 'type' , 'hidden' );
  if(reqType == "P" || reqType == "R") input.setAttribute( 'name' , 'direct_template_pdf_print_requested' );
  if(reqType == "Y" && reqType2!="4") input.setAttribute( 'name' , 'direct_template_pdf_print2_requested' );
  if(reqType == "Y" && reqType2=="4") input.setAttribute( 'name' , 'direct_template_pdf_print4_requested' );
  if(reqType == "C") input.setAttribute( 'name' , 'direct_template_create_csv_requested' );
  input.setAttribute( 'value' , 1 );
  formPdf.appendChild( input );


  if(elems["schedule_seq"]){
    // oscnt_schedule_seq
    input = document.createElement( "input" );
    input.setAttribute( "type" , "hidden" );
    input.setAttribute( "name" , "oscnt_schedule_seq" );
    input.setAttribute( "value" , elems["schedule_seq"].value );
    formPdf.appendChild( input );
  }

  //prf_addr1
  if(elems["prf_addr1"]){
    var prf_addr1 = "";
    if (elems["prf_addr1"].value) prf_addr1 = elems["prf_addr1"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'prf_addr1' );
    input.setAttribute( 'value' ,prf_addr1 );
    formPdf.appendChild( input );
  }

  //prf_addr2
  if(elems["prf_addr2"]){
    var prf_addr2 = "";
    if (elems["prf_addr2"].value) prf_addr2 = elems["prf_addr2"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'prf_addr2' );
    input.setAttribute( 'value' ,prf_addr2 );
    formPdf.appendChild( input );
  }

  //prf_name
  if(elems["prf_name"]){
    var prf_name = "";
    if (elems["prf_name"].value) prf_name = elems["prf_name"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'prf_name' );
    input.setAttribute( 'value' ,prf_name );
    formPdf.appendChild( input );
  }

  //prf_tel
  if(elems["prf_tel"]){
    var prf_tel = "";
    if (elems["prf_tel"].value) prf_tel = elems["prf_tel"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'prf_tel' );
    input.setAttribute( 'value' ,prf_tel );
    formPdf.appendChild( input );
  }

  // prf_org_cd
  if(elems["prf_org_cd"]){
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'prf_org_cd' );
    input.setAttribute( 'value' , elems["prf_org_cd"].value );
    formPdf.appendChild( input );
  }

  // oscnt_ymd_k
  if(elems["oscnt_ymd_k"]){
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_ymd_k' );
    input.setAttribute( 'value' , elems["oscnt_ymd_k"].value );
    formPdf.appendChild( input );
  }

  //oscnt_insurance_code
  if(elems["oscnt_insurance_code"]){
    var oscnt_insurance_code = "";
    if (elems["oscnt_insurance_code"].value) oscnt_insurance_code = elems["oscnt_insurance_code"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_insurance_code' );
    input.setAttribute( 'value' ,oscnt_insurance_code );
    formPdf.appendChild( input );
  }

  //insurance_id
  //oscnt_insurance_id
  if(elems["oscnt_insurance_id"]){
    var oscnt_insurance_id = "";
    if (elems["oscnt_insurance_id"].value) oscnt_insurance_id = elems["oscnt_insurance_id"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_insurance_id' );
    input.setAttribute( 'value' ,oscnt_insurance_id );
    formPdf.appendChild( input );
  }

  //oscnt_charge_rate
  if(elems["oscnt_charge_rate"]){
    var oscnt_charge_rate = "";
    if (elems["oscnt_charge_rate"].value) oscnt_charge_rate = elems["oscnt_charge_rate"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_charge_rate' );
    input.setAttribute( 'value' ,oscnt_charge_rate );
    formPdf.appendChild( input );
  }
  //oscnt_isr_person_id
  if(elems["oscnt_isr_person_id"]){
    var oscnt_isr_person_id = "";
    if (elems["oscnt_isr_person_id"].value) oscnt_isr_person_id = elems["oscnt_isr_person_id"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_isr_person_id' );
    input.setAttribute( 'value' ,oscnt_isr_person_id );
    formPdf.appendChild( input );
  }
  //oscnt_chr_person_id
  if(elems["oscnt_chr_person_id"]){
    var oscnt_chr_person_id = "";
    if (elems["oscnt_chr_person_id"].value) oscnt_chr_person_id = elems["oscnt_chr_person_id"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_chr_person_id' );
    input.setAttribute( 'value' ,oscnt_chr_person_id );
    formPdf.appendChild( input );
  }
  //oscnt_recipient_id
  if(elems["oscnt_recipient_id"]){
    var oscnt_recipient_id = "";
    if (elems["oscnt_recipient_id"].value) oscnt_recipient_id = elems["oscnt_recipient_id"].value;
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_recipient_id' );
    input.setAttribute( 'value' ,oscnt_recipient_id );
    formPdf.appendChild( input );
  }

  var rp_listtableObj = document.getElementById("rp_listtable");
  var tablesObj = rp_listtableObj.getElementsByTagName("table");

  for(var i=0; i < tablesObj.length ; i++){
    var trObjs = tablesObj.item(i).getElementsByTagName("tr");
    var k =0;
    var l =0;
    var max_rows = 0;
    var yoho_str = "";
    var med_name = "";
    var yoryo = "";

    for(var j=0; j < trObjs.length ; j++){
      var thObjs = trObjs.item(j).getElementsByTagName("th");
      var tdObjs = trObjs.item(j).getElementsByTagName("td");
      var field_name = "";
      if(thObjs.length == 5){
        getMedInfo(l,k,formPdf,thObjs,tdObjs);
        k++;
      }
      if(thObjs.length == 4){
        getYohoInfo(l, max_rows, formPdf, thObjs, tdObjs, reqType);
        k = 0;
        l++;
      }
    }
  }
  var timings_keys = ["yoho_naiyo_timing", "yoho_naiyo_hour_dist", "yoho_naiyo_per_day", "yoho_naiyo_hours"];
  var idx = -1;
  for (var rp in rpInfo) {
    idx++;
    for (var idx2=0; idx2<timings_keys.length; idx2++) {
        var k = timings_keys[idx2];
        input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('name', k+"[" + idx + "]");
        input.setAttribute('value', str(rpInfo[rp].yohoInfo[0][k]));
        formPdf.appendChild( input );
    }
  }

  if(reqType == "C"){
    // oscnt_ymd_k
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , 'oscnt_ymd_k' );
    if(nyuin_gairai == "G") input.setAttribute( 'value' , elems["oscnt_ymd_k"].value );
    if(nyuin_gairai == "N") input.setAttribute( 'value' , oscnt_ymd_k );
    formPdf.appendChild( input );

    //cyouzai_bi
    field_name = "";
    field_name = "cyouzai_bi";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , cyouzai_bi );
    formPdf.appendChild( input );
  }

  formPdf.submit();
}

//********************************************
// PDF出力時に必要な薬品情報
//********************************************
function getMedInfo(l,k,formPdf,thObjs,tdObjs){
    var field_name = "";

    // 薬品名
    var status = getText(thObjs.item(1));
    var med_name = rpInfo[l + 1].medInfo[k].med_name;
    if (is_add_remark_rp && rpInfo[l + 1].medInfo[k].remark) {
        med_name += " " + rpInfo[l + 1].medInfo[k].remark;   // 別名
    }
    var med_chozai_siji = createMedChozaiSijiStr(rpInfo[l + 1].medInfo[k]); // 薬剤毎の調剤指示
    med_name += med_chozai_siji ? " "  + med_chozai_siji : "";
    field_name = "med_name["+l+"]["+k+"]";
    if(status == "追"){med_name = "[追]:"+med_name;}
    if(status == "修"){med_name = "[修]:"+med_name;}
    if(status == "削"){med_name = "[削]:"+med_name;}
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , med_name );
    formPdf.appendChild( input );

    // 個別医薬品コード
    field_name = "yj_cd["+l+"]["+k+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , document.getElementById("yj_cd_"+(l+1)+"_"+k).value );
    formPdf.appendChild( input );

    // RPに表示されている用量
    var yoryo = getText(thObjs.item(4));
    field_name = "";
    field_name = "yoryo["+l+"]["+k+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , yoryo );
    formPdf.appendChild( input );

    // 用量
    field_name = "";
    field_name = "unit["+l+"]["+k+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , rpInfo[l + 1].medInfo[k].yoryo );
    formPdf.appendChild( input );

    // 単位表記名
    field_name = "";
    field_name = "med_unit_name["+l+"]["+k+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , getZaikeiCode(rpInfo[l + 1].medInfo[k].med_unit_name) );
    formPdf.appendChild( input );

    // 後発品変更不可
    field_name = "";
    field_name = "med_kouhatu_fuka["+l+"]["+k+"]";
    input = document.createElement('input');
    input.setAttribute('type' , 'hidden');
    input.setAttribute('name' , field_name);
    input.setAttribute('value' , rpInfo[l + 1].medInfo[k].med_kouhatu_fuka);
    formPdf.appendChild(input);
}

function getZaikeiCode(med_unit_name){
  var zc = "";
  if(med_unit_name == "錠") zc = "T";
  else if(med_unit_name == "カプセル") zc = "CP";
  else if(med_unit_name == "g") zc = "G";
  else if(med_unit_name == "mg") zc = "mg";
  else if(med_unit_name == "ml") zc = "ML";
  else if(med_unit_name == "包") zc = "ﾎｳ";
  else if(med_unit_name == "個") zc = "ｺ";
  else if(med_unit_name == "枚") zc = "ﾏｲ";
  else if(med_unit_name == "cc") zc = "cc";
  else if(med_unit_name == "バイアル") zc = "V";
  else if(med_unit_name == "アンプル") zc = "A";
  else if(med_unit_name == "ボトル") zc = "B";
  else zc = "";
  return zc;
}

function getDepartmentCode(oscnt_department)
{
  if (oscnt_department == "内科")                      return "00101";
  else if (oscnt_department == "心療内科")             return "00102";
  else if (oscnt_department == "精神科")               return "00103";
  else if (oscnt_department == "神経科")               return "00104";
  else if (oscnt_department == "呼吸器科")             return "00105";
  else if (oscnt_department == "消化器科")             return "00106";
  else if (oscnt_department == "循環器科")             return "00107";
  else if (oscnt_department == "アレルギー科")         return "00108";
  else if (oscnt_department == "リウマチ科")           return "00109";
  else if (oscnt_department == "小児科")               return "00110";
  else if (oscnt_department == "神経内科")             return "00111";
  else if (oscnt_department == "胃腸科")               return "00112";
  else if (oscnt_department == "外科")                 return "00121";
  else if (oscnt_department == "整形外科")             return "00122";
  else if (oscnt_department == "形成外科")             return "00123";
  else if (oscnt_department == "美容外科")             return "00124";
  else if (oscnt_department == "脳神経外科")           return "00125";
  else if (oscnt_department == "呼吸器外科")           return "00126";
  else if (oscnt_department == "心臓血管外科")         return "00127";
  else if (oscnt_department == "小児外科")             return "00128";
  else if (oscnt_department == "皮膚泌尿器科")         return "00129";
  else if (oscnt_department == "性病科")               return "00130";
  else if (oscnt_department == "肛門科")               return "00131";
  else if (oscnt_department == "産婦人科")             return "00132";
  else if (oscnt_department == "眼科")                 return "00133";
  else if (oscnt_department == "耳鼻咽喉科")           return "00134";
  else if (oscnt_department == "気管食道科")           return "00135";
  else if (oscnt_department == "リハビリテーション科") return "00136";
  else if (oscnt_department == "放射線科")             return "00137";
  else if (oscnt_department == "皮膚科")               return "00138";
  else if (oscnt_department == "泌尿器科")             return "00139";
  else if (oscnt_department == "産科")                 return "00140";
  else if (oscnt_department == "婦人科")               return "00141";
  else if (oscnt_department == "歯科")                 return "00151";
  else if (oscnt_department == "矯正歯科")             return "00152";
  else if (oscnt_department == "小児歯科")             return "00153";
  else if (oscnt_department == "歯科口腔外科")         return "00154";
  else if (oscnt_department == "ペインクリニック")     return "64210";
  else if (oscnt_department == "移植外科")             return "64211";
  else if (oscnt_department == "消火器外科")           return "64212";
  else if (oscnt_department == "腎臓内科")             return "64213";
  else if (oscnt_department == "糖尿病科")             return "64214";
  else if (oscnt_department == "内分泌代謝科")         return "64215";
  else if (oscnt_department == "麻酔科")               return "64216";
  else if (oscnt_department == "免疫血液内科")         return "64217";
  else return "99999";
}

//********************************************
// PDF出力時に必要な用法情報
//********************************************
function getYohoInfo(l, k, formPdf, thObjs, tdObjs, reqType) {
    var field_name = "";
    var med_name = "";
    var yoryo = "";
    var yoho = "";
    var yoho_arry;
    var match_pattern = "";
    var tmp_str = "";
    var arr;
    var med_sum_per_day = 0;
    var yoho_str = "";
    var i = 0;
    var days = 0;
    var day_sum = 0;
    var kaisu = "";
    var max_rows = 0;
    var oscnt_ymd_k_tmp = "";

    var yohoInfo = rpInfo[l + 1].yohoInfo[0];  // 対象RPの用法
    yoho_str = getText(tdObjs.item(0));
    //==============================================================================
    // PDF出力時に用法コメントで勝手に改行されてしまう現象への対応 2012/09/26
    // 原因：用法データ登録時に、全角スペースをデリミタとして使用しているのが原因。
    // 対応：PDF出力時に、用法コメントに含まれる全角スペースを半角スペースに変換
    //==============================================================================
    // (1) 一旦「|」で分割
    var tmp_array = yoho_str.split("|");

    if (yoho_kubun == "C" && is_input_injection_content && reqType != "Y") {    // 注射箋で用法入力あり、かつ指示書以外
        // 注射箋では、(1)をそのまま利用する
        yoho_arry = tmp_array;
    } else {
        // (2) 用法コメントのみ全角スペースを半角に変換(「|」で連結されたデータの最後がコメントのはず)
        tmp_array[tmp_array.length - 1] = tmp_array[tmp_array.length - 1].replace(/　/g," ");

        // (3) 再度「|」で結合
        for (var i = 0; i < tmp_array.length; i++) {
            if (i != 0) {
                tmp_str += '|';
            }
            tmp_str += tmp_array[i];
        }

        yoho_str = tmp_str.replace(/\| /g,"");
        yoho_arry = yoho_str.split("　");
    }

    yoho_str = "";

    // コメント
    field_name = "comment[" + l + "]";
    input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', field_name);
    var comment = yohoInfo.yoho_comment;
    if (comment) {
        comment = comment.replace(/^\|/g, "");
        comment = comment.replace(/(^\s+)|(\s+$)/g, "");    // 前後の空白を削除
        comment = comment.replace(/<\/br>/g, "\n");
        input.setAttribute('value', comment);
        formPdf.appendChild(input);
    }
    tmp_str = "";

    // 投与期間の「開始日」
    oscnt_ymd_k_tmp = yohoInfo.yoho_touyo_start_ymd
    if (oscnt_ymd_k_tmp && oscnt_ymd_k_tmp != "00000000") {
        field_name = "";
        field_name = "toyaku_start_ymd["+l+"]";
        input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('name', field_name);
        input.setAttribute('value', oscnt_ymd_k_tmp);
        formPdf.appendChild(input);
        if(oscnt_ymd_k_tmp != "") {
            if(oscnt_ymd_k == "") oscnt_ymd_k = oscnt_ymd_k_tmp;
            cyouzai_bi = parseInt(cyouzai_bi) < parseInt(oscnt_ymd_k_tmp) ? cyouzai_bi : oscnt_ymd_k_tmp;
        }
    }
    // [用法]のデータ作成
//    match_pattern1 = /1日|回/g;
    match_pattern2 = /日分|定時つなぎ/g;
//    match_pattern3 = /(\d+)年(\d+)月(\d+)日/g;

    var maxlen;
    if (yoho_kubun == "C") {
        maxlen = 56;
    } else {
        maxlen = 42;
    }

    for (i = 0; i < yoho_arry.length - 1; i++) {
        var status = getText(thObjs.item(1));
        if (i == 0) {
            if (status == "追"){yoho = "[追]:" + yoho;}
            if (status == "修"){yoho = "[修]:" + yoho;}
            if (status == "削"){yoho = "[削]:" + yoho;}
        }

        if(!yoho_arry[i]) continue;     // 空ならばスキップ

        if ((yoho_kubun != "C" || !is_input_injection_content) && (yoho_arry[i].search(match_pattern2) > -1)) {
            // [用量]のデータ作成
            // (「10日分/2日おき」等)
            if (yohoInfo.yoho_touyo_teiji_tunagi == 1) {
                days = '定時つなぎ';
            } else {
                days = yohoInfo.yoho_touyo_nichi_bun;
            }
            field_name = "";
            field_name = "toyaku_days["+l+"]";
            input = document.createElement( 'input' );
            input.setAttribute( 'type' , 'hidden' );
            input.setAttribute( 'name' , field_name );
            input.setAttribute( 'value' , days );
            formPdf.appendChild( input );

            oki = yohoInfo.yoho_touyo_nichi_oki;
            field_name = "";
            field_name = "oki_days["+l+"]";
            input = document.createElement( 'input' );
            input.setAttribute( 'type' , 'hidden' );
            input.setAttribute( 'name' , field_name );
            input.setAttribute( 'value' , oki );
            formPdf.appendChild( input );

            continue;
        }

        if(yoho_arry[i] == rpInfo[l + 1].yohoInfo[0].yoho_comment_copy){
            yoho_str = yoho_arry[i];
            continue;
        }

        if((getLength(yoho) + getLength(yoho_arry[i]) + 1) > maxlen){
            field_name = "";
            field_name = "yoho["+l+"]["+max_rows+"]";
            input = document.createElement( 'input' );
            input.setAttribute( 'type' , 'hidden' );
            input.setAttribute( 'name' , field_name );
            input.setAttribute( 'value' , yoho );
            formPdf.appendChild( input );

            field_name = "kaisu["+l+"]["+max_rows+"]";
            input = document.createElement( 'input' );
            input.setAttribute( 'type' , 'hidden' );
            input.setAttribute( 'name' , field_name );
            input.setAttribute( 'value' , "" );
            formPdf.appendChild( input );

            yoho = "";
            max_rows++;
            k++;
        }

        if (yoho_kubun == "C" && is_input_injection_content && reqType != "Y") {    // 注射箋で、かつ指示書以外
            if (yoho) {
                yoho = yoho + "\n";
            }

            // 前後の空白を削除して追加
            yoho = yoho + $.trim(yoho_arry[i]);
        } else {
            yoho = yoho + " " + yoho_arry[i];
        }
    }

    field_name = "yoho_code["+l+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    var yy = document.getElementById("yoho_code_"+(l+1)).value ;
    input.setAttribute( 'value' , document.getElementById("yoho_code_"+(l+1)).value );
    formPdf.appendChild( input );

    field_name = "";
    field_name = "yoho["+l+"]["+max_rows+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , yoho );
    formPdf.appendChild( input );

    // 回数
    day_sum = yohoInfo.yoho_naiyo_per_day;
    med_sum_per_day = (days && day_sum) ? parseInt(days) * parseInt(day_sum) : 0;
    kaisu = med_sum_per_day ? (med_sum_per_day + "回数") : "";

    field_name = "kaisu["+l+"]["+max_rows+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , kaisu );
    formPdf.appendChild( input );

    med_name = "";
    max_rows++;
    k++;

    // 用法設定
    field_name = "";
    j = 0;

    // 日数
    field_name = "nissu["+l+"]";
    input = document.createElement( 'input' );
    input.setAttribute( 'type' , 'hidden' );
    input.setAttribute( 'name' , field_name );
    input.setAttribute( 'value' , days );
    formPdf.appendChild( input );
}

/**
 *  日付(YYYY年MM月DD日）をyyyymmdd形式に変換する。
 */
function convDateToNum(org_date){
  var arr = org_date.split(/年|月|日/);
  var ret = "";
  return convertNum(arr[0],4) + convertNum(arr[1],2) + convertNum(arr[2],2);
}

/**
 *  数値を指定桁数の文字列に整形する。
 */
function convertNum(num, figures) {
  var str = String(num);
  while (str.length < figures) {
    str = "0"+str;
  }
  return str;
}

/**
 *  生年年月日(YYYY年MM月DD日）から、現時点での年齢を算出する。
 */
function getAge(birth)
{
  var nowdate = new Date();
  var ty = nowdate.getYear();
  ty = (ty < 2000) ? ty+1900 : ty;
  var tm = nowdate.getMonth() + 1;
  var td = nowdate.getDate();
  var arr = birth.split('/');
  var a = ty - parseInt(arr[0], 10);
  if(tm * 100 + td < parseInt(arr[1], 10) * 100 + parseInt(arr[2], 10)) a--;
  return a;
}

/**
 *  タグ名からタグ内のテキストを抽出する。
 */
function getText(tag)
{
  return (tag.innerText || tag.textContent || "") ;
}

/**
 *  指定したラジオボタンの入力内容を取得する。
 */
function getRadioInfo(name, nl)
{
  var retStr = (nl) ? null : "" ;
  var radios = document.getElementsByName(name);
  for (var i = 0 ; i < radios.length ; i++)
  {
    if (radios[i].checked) { retStr = getText(radios[i].parentNode) ; break ; }
  }
  return retStr ;
}

function getJobName(emp_id)
{
  if (window.XMLHttpRequest)
  {
    ajaxObj = new XMLHttpRequest() ;
    if (ajaxObj.overrideMimeType) { ajaxObj.overrideMimeType("text/xml") ; }
  }
  else
  {
    try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP") ; }
    catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP") ; }
  }
  if (ajaxObj == null) return ;

  var postval = "job_name_requested=1&emp_id=" + emp_id ;
  ajaxObj.open("POST", "tmpl_MealStart.php", false) ;
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval) ;

  return getJobNameHandler() ;
}

function getJobNameHandler()
{
  if (ajaxObj.readyState == 4) // 通信完了
  {
    if (ajaxObj.status == 200) // 正常終了(status:OK)
    {
      if (ajaxObj.responseText)
      {
        try
        {
          var ret ;
          eval("ret = " + ajaxObj.responseText) ;
          return ret.job_nm ;
        }
        catch (e) { }
      }
    }
  }
  return "" ;
}

var currentCyusyaTimingBox = 0;
function getClientWidth(){
  if (document.documentElement && document.documentElement.clientWidth) return document.documentElement.clientWidth;
  if (document.body.clientWidth) return document.body.clientWidth;
  if (window.innerWidth) return window.innerWidth;
  return 0;
}
function showCyusyaTimingYougoDiv(idx){
  if (currentCyusyaTimingBox) currentCyusyaTimingBox.style.backgroundColor = "#ffffe6";
  currentCyusyaTimingBox = elems["yoho_cyusyatiming"+idx];
  currentCyusyaTimingBox.style.backgroundColor = "#8beefc";
  elems["cyusya_timing_result"].value = currentCyusyaTimingBox.value;
  elems["div_cyusyatiming_yougo"].style.display = "";
  var w = getClientWidth();
  if (w) elems["div_cyusyatiming_yougo"].style.left = (w / 2 - 230) + "px";
}

//********************************************
// コメント欄への入力サポート子画面を表示
//********************************************
var yoho_comment_caret_pos; // 用法コメント欄のカーソル位置
function showYohoCommentYougoDiv(target_id) {
    elems[target_id].style.backgroundColor = "#8beefc";
    elems["yoho_comment_result"].value = "";

    // 入力サポート子画面の、薬剤名称リストを作成
    createYohoCommentMedList();

    elems["div_yoho_comment_yougo"].style.display = "";
    var w = getClientWidth();
    if (w) {
        elems["div_yoho_comment_yougo"].style.left = (w / 2 - 230) + "px";
    }
    // 用法コメント欄のカーソル位置を取得
    yoho_comment_caret_pos = getCaretPosition(elems[target_id]);
}

function btnCyusyaTimingClicked(obj){
  elems["cyusya_timing_result"].value += obj.innerHTML;
  elems["cyusya_timing_result"].focus();
}

//********************************************
// 入力サポート子画面内のボタンクリック時の処理
//********************************************
function btnSupportWindowClicked(obj, target_id) {
    elems[target_id].value += obj.innerHTML;
    elems[target_id].focus();
}

function applyCyusyaTimingString(){
  currentCyusyaTimingBox.value = elems["cyusya_timing_result"].value;
  currentCyusyaTimingBox.style.backgroundColor = "#ffffe6";
  elems["div_cyusyatiming_yougo"].style.display = "none";
}
function cancelCyusyaTimingString(){
  currentCyusyaTimingBox.style.backgroundColor = "#ffffe6";
  elems["div_cyusyatiming_yougo"].style.display = "none";
}

//********************************************
// 入力サポート子画面からの入力をコメント欄へ反映
//********************************************
function applyYohoCommentString(target_id) {
    var agent = window.navigator.userAgent.toLowerCase();
    elems[target_id].focus();
    var comment_result = elems["yoho_comment_result"].value;
    if (agent.indexOf('msie') != -1) {
        yoho_comment_caret_pos.text = elems["yoho_comment_result"].value;
        yoho_comment_caret_pos.select();
    } else {
        var comment = elems[target_id].value;
        var np = yoho_comment_caret_pos + comment_result.length;
        elems[target_id].value = comment.substr(0, yoho_comment_caret_pos) + comment_result + comment.substr(yoho_comment_caret_pos);
        elems[target_id].setSelectionRange(np, np);
    }
    cancelYohoCommentString(target_id);
}

//********************************************
// コメント欄への入力サポート子画面を閉じる
//********************************************
function cancelYohoCommentString(target_id) {
    elems[target_id].style.backgroundColor = "#ffffff";
    elems["div_yoho_comment_yougo"].style.display = "none";
}

function hovIn(obj) { obj.style.color='#f50'; }
function hovOut(obj) { obj.style.color='#00f'; }

function enterSearch(key)
{
  var smnv = document.getElementById('sel_med_name').value;
  if (smnv !='') {
    if (key == 13) {
      medSearchWithClearTree();
    }
  }
}
function isNewDisp(num)
{
  var new_flg = num;
}
function isEditDisp(num)
{
  var edit_flg = num;
}
function isCopyDisp(num)
{
  var copy_flg = num;
}

function popupSyohou(values, box_width, label_width, e) {
  popupDetail(values, box_width, label_width, '#5279a5', '#f6f9ff', e);
}

function popupDetail(values, box_width, label_width, strong_color, pale_color, e) {
  var div = document.getElementById('popup');
  if (!div) {
    var selects = document.getElementsByTagName('select');
    for (var i = 0, j = selects.length; i < j; i++) {
      selects[i].style.visibility = 'hidden';
    }

    var table = document.createElement('table');
    table.style.borderCollapse = 'collapse';
    table.style.width = '100%';
    for (var i = 0, j = values.length; i < j; i += 2) {
      addDetailRow(table, values[i], values[i + 1], strong_color, pale_color);
    }
    table.rows[0].cells[0].style.width = String(label_width).concat('px');

    div = document.createElement('div');
    div.id = 'popup';
    div.style.position = 'absolute';
    div.style.width = String(box_width).concat('px');
    div.style.borderColor = '#5279a5';
//    div.style.borderColor = '#35B341';
    div.style.borderStyle = 'solid';
    div.style.borderWidth = '3px';
    div.style.backgroundColor = '#ffffff';
    div.style.padding = '3px';
    div.appendChild(table);
    document.body.appendChild(div);
  }

  var bodyWidth = document.body.clientWidth;
  var bodyHeight;
  var iframe = top.document.getElementById('floatingpage');
  if (iframe) {
    var iframeOffsetTop = getOffsetTopFromBody(iframe);

    var scroll_top = top.document.body.scrollTop;
    var top_shift = iframeOffsetTop - scroll_top;
    bodyHeight = top.document.body.clientHeight - top_shift;

//    bodyHeight = top.document.body.clientHeight - iframeOffsetTop;

  } else {
    bodyHeight = top.document.body.clientHeight;
  }

  if (e == undefined) {
    e = window.event;
  }

  var scrollLeft = document.body.scrollLeft;
  var x = e.clientX + 8 + scrollLeft;
  var y = e.clientY + 8 + document.body.scrollTop;

  if (x - scrollLeft + div.clientWidth + 6 > bodyWidth) {
    x -= (div.clientWidth + 16);
    if (x < 0) {x = 0;}
  }

  var scrollTop = (self == top) ? document.body.scrollTop : top.document.body.scrollTop;
  if (y - scrollTop + div.clientHeight + 6 > bodyHeight) {
    y -= (y - scrollTop + div.clientHeight - bodyHeight + 16);
    if (y < 0) {y = 0;}
  }

  div.style.left = x + "px";
  div.style.top = y + "px";
}

function addDetailRow(table, header, value, border_color, header_color) {
  var row = table.insertRow(table.rows.length);
  row.style.height = '22px';
  addDetailCell(row, header, border_color, header_color);
  addDetailCell(row, value, border_color, '#ffffff');
}

function addDetailCell(row, str, border_color, bg_color) {
  var cell = row.insertCell(row.cells.length);
  cell.style.borderColor = border_color;
  cell.style.borderStyle = 'solid';
  cell.style.borderWidth = '1px';
  cell.style.padding = '2px';
  cell.style.backgroundColor = bg_color;
  cell.style.color = '#000000';
  cell.style.fontFamily = 'ＭＳ Ｐゴシック, Osaka';
//  cell.className = 'j8';
  cell.innerHTML = str;
}

function closeDetail() {
  var div = document.getElementById('popup');
  if (div) {
    document.body.removeChild(div);
  }

  var selects = document.getElementsByTagName('select');
  for (var i = 0, j = selects.length; i < j; i++) {
    selects[i].style.visibility = '';
  }
}

function getOffsetTopFromBody(element) {
  var offset = 0;
  while (element.offsetParent) {
    offset += element.offsetTop;
    element = element.offsetParent;
  }
  return offset;
}

//**************************
// 用法入力チェック
//**************************
function isValidYohoEntry(chk_chozai_siji) {
    if (yoho_kubun == 'N') {
        if (!isValidYohoEntryN(chk_chozai_siji)) {    // 用法入力チェック(内服)
            return false;
        }
    }

    // 投与期間の日数入力チェック(日分)
    if (!isValidYohoTouyoNichiBun()) {
        return false;
    }

    // 投与期間の投与間隔入力チェック(日おき)
    if (!isValidYohoTouyoNichiOki()) {
        return false;
    }

    return true;
}

//**************************
// 用法入力チェック(内服)
//**************************
function isValidYohoEntryN(chk_chozai_siji) {
    if (!elems['yoho_naiyo_per_day'].value) {
        alert('用法設定の回数を選択してください。');
        return false;
    }

    // 投薬タイミング、時間間隔、時刻指定の重複チェック用のカウンタ
    var checkHourOrTimingCount = 0;

    // 投薬タイミングチェックボックスのうち、表示されており、かつチェックが入っている要素を抽出
    var timingElemes = new Array();
    timingElemes = $("input[type='checkbox']:checked" + ":visible").filter(function() {
        return this.id.match(/yoho_naiyo_timing_\d/);
    });

    if (timingElemes.length > 0) {
        // 投薬タイミング設定あり
        checkHourOrTimingCount++;
    }

    // 時間間隔入力チェック
    if (!isValidYohoNaiyoHourDist()) {
        return false;
    }

    // 時間間隔が表示されているか
    var isHourDistVisible = $('#yoho_naiyo_hour_dist').is(':visible');
    if (isHourDistVisible && elems['yoho_naiyo_hour_dist'].value != '') {
        // 時間間隔が表示されており、値が入力されている場合のみ時間間隔設定あり
        checkHourOrTimingCount++;
    }

    // 時刻指定入力チェック
    if (!isValidYohoNaiyoHours()) {
        return false;
    }

    // 時刻指定があるか
    var i;
    for (i = 1; i <= 4; i++) {
        if (elems['yoho_naiyo_hours_' + i].value) {
            // 1つでも時刻の指定があれば、時刻指定あり
            checkHourOrTimingCount++;
            break;
        }
    }

    if (checkHourOrTimingCount == 0) {
        if (isHourDistVisible) {
            alert('投薬タイミング、時間間隔、時刻指定のいずれかを指定してください。');
        } else {
            alert('投薬タイミング又は時刻指定を選択してください。');
        }
        return false;
    }

    if (checkHourOrTimingCount > 1) {
        if (isHourDistVisible) {
            alert('投薬タイミング、時間間隔、時刻指定は、重複して設定できません。どれか１つだけ設定してください。');
        } else {
            alert('投薬タイミング、時刻指定は、重複して設定できません。どちらかを設定してください。');
        }
        return false;
    }
    // 20140709 「用法設定投与期間の開始日」チェックにおいて、以下３つのテンプレートは無チェックとする
    var _tmpsn = script_name.toLowerCase();
    if (_tmpsn != 'tmpl_ordsc_innaigairai.php' && _tmpsn != 'tmpl_ordsc_ingaigairai.php' && _tmpsn != 'tmpl_ordsc_taiinji.php') {
        if (!elems['yoho_touyo_start_ymd_y'].value
         || !elems['yoho_touyo_start_ymd_m'].value
         || !elems['yoho_touyo_start_ymd_d'].value) {
            alert('用法設定投与期間の開始日を入力してください。');
            return false;
        }
    }

    // 「定時つなぎ」項目が存在しないか、「定時つなぎ」が選択されていない場合のみ、日数をチェック
    if (!elems['yoho_touyo_teiji_tunagi'] || checkValue('yoho_touyo_teiji_tunagi') == "") {
        if (!elems['yoho_touyo_nichi_bun'].value) {
            alert('用法設定投与期間の日数を入力してください。');
            return false;
        }
    }

    if (chk_chozai_siji) {  // 調剤指示の入力チェックが必要な場合
        var checked = false;
        var suffix;
        for (var key in CHOZAI_SIJI_CAPTIONS) {
            suffix = key.toLowerCase();
            if (!elems["yoho_naiyo_chozai_siji_" + suffix]) {
                // 要素が存在していなければスキップ
                continue;
            }

            if (elems['yoho_naiyo_chozai_siji_' + suffix].checked) {
                checked = true;
                break;
            }
        }

        // 全ての調剤指示がチェック無しの場合、メッセージを表示
        if (!checked) {
            alert('用法設定の調剤指示を選択してください。');
            return false;
        }
    }

    return true;
}

//********************************************
// 時間間隔入力チェック
//********************************************
function isValidYohoNaiyoHourDist() {
    if (elems['yoho_naiyo_hour_dist'].value == '') {
        return true;
    }

    var hour = parseInt(elems['yoho_naiyo_hour_dist'].value);
    if (!elems['yoho_naiyo_hour_dist'].value.match(/^[0-9]+$/) || hour < 1
            || hour > 12) {
        alert('時間間隔の指定が適切ではありません。1〜12の整数のみ入力可能です。');
        return false;
    }
    return true;
}

//********************************************
// 時刻指定入力チェック
//********************************************
function isValidYohoNaiyoHours() {
    var hour;
    for ( var i = 1; i <= 4; i++) {
        if (elems['yoho_naiyo_hours_' + i].value != '') {
            hour = parseInt(elems['yoho_naiyo_hours_' + i].value);
            if (!elems['yoho_naiyo_hours_' + i].value.match(/^[0-9]+$/)
                    || hour > 24) {
                alert('時刻指定' + i + 'の「時間」が適切ではありません。0〜24の整数のみ入力可能です。');
                return false;
            }
        }
        if (!$('#yoho_naiyo_hours_fukintou_' + i).is(':visible')) {
            continue;
        }
        if (elems['yoho_naiyo_hours_fukintou_' + i].value != '') {
            hour = parseInt(elems['yoho_naiyo_hours_fukintou_' + i].value);
            if (!elems['yoho_naiyo_hours_fukintou_' + i].value
                    .match(/^[0-9]+$/)
                    || hour > 59) {
                alert('時刻指定' + i + 'の「分」が適切ではありません。0〜59の整数のみ入力可能です。');
                return false;
            }
        }
    }
    return true;
}

//********************************************
// 用法設定投与期間の日数入力チェック
//********************************************
function isValidYohoTouyoNichiBun() {
    if (elems['yoho_touyo_nichi_bun'].value == '') {
        return true;
    }
    var days = parseInt(elems['yoho_touyo_nichi_bun'].value);
    if (!elems['yoho_touyo_nichi_bun'].value.match(/^[0-9]+$/) || days < 1
     || days > 90) {
        alert('投与期間の日数の指定が適切ではありません。1〜90の整数のみ入力可能です。');
        return false;
    }
    return true;
}

//********************************************
// 用法設定投与期間の投与間隔入力チェック
//********************************************
function isValidYohoTouyoNichiOki() {
    if (elems['yoho_touyo_nichi_oki'].value == '') {
        return true;
    }
    if (!elems['yoho_touyo_nichi_oki'].value.match(/^[1-3]{1,1}$/)) {
        alert('投与期間の投与間隔の指定が適切ではありません。1〜3の整数のみ入力可能です。');
        return false;
    }
    return true;
}

//********************************************
// 定時つなぎチェックボックスクリック時の処理
//********************************************
function onClickTeijiTunagi() {
    // 「定時つなぎ」が選択されている場合(定時つなぎは入院臨時処方箋のみ)
    if (checkValue('yoho_touyo_teiji_tunagi') != '') {
        elems['yoho_touyo_nichi_bun'].value = '';
        elems['yoho_touyo_nichi_bun'].disabled = 'disabled';
        elems['yoho_touyo_nichi_oki'].value = '';
        elems['yoho_touyo_nichi_oki'].disabled = 'disabled';
    } else {
        elems['yoho_touyo_nichi_bun'].disabled = '';
        elems['yoho_touyo_nichi_oki'].disabled = '';
    }
}

//********************************************
// 適用ボタンクリック時の処理
//********************************************
function onYohoEntry(input_dispensing) {
    // 調剤指示の入力が必須か？
    var chk_chozai_siji = false;
    if (input_dispensing == 1) {
        chk_chozai_siji = true;
    }

    // 入力チェック
    if (!isValidYohoEntry(input_dispensing)) {
        return;
    }
    setToRP('yoho','entry');    // RPテーブルへ適用
}

//********************************************
// 投与開始日全適用ボタンクリック時の処理
//********************************************
function onTouyoStartAllEntry() {
    // 指定した投与開始日を取得
    var ymdy = ("0000"+selectValue("yoho_touyo_start_ymd_y")).slice(-4);
    var ymdm = ("00"+selectValue("yoho_touyo_start_ymd_m")).slice(-2);
    var ymdd = ("00"+selectValue("yoho_touyo_start_ymd_d")).slice(-2);

    var yoho_touyo_start_ymd = ymdy + ymdm + ymdd;
    var touyo_start_ymd_display = toInt(ymdy) + "年" + toInt(ymdm) + "月" + toInt(ymdd) + "日";
    var yoho_touyo_nichi_bun = textValue("yoho_touyo_nichi_bun");
    var touyo_nichi_bun_display = yoho_touyo_nichi_bun + "日分";
    var yoho_touyo_nichi_oki = textValue("yoho_touyo_nichi_oki");
    var touyo_nichi_oki_display = yoho_touyo_nichi_oki + "日おき";

    if (yoho_touyo_nichi_oki) { // 「日おき」に入力がある場合
        if (!confirm("RP欄で「日おき」が未設定の場合は、「日おき」の日数が適用されません。よろしいですか？")) {
            return;
        }
    }

    // RP数を取得
    var rpInfoMaxCnt = countTotalRP();

    // 全RPに対して、指定した「投与開始日」「日分」「日おき」をセットする
    var tmp;
    for (var i = 1; i <= rpInfoMaxCnt; i++) {
        tmp = rpInfo[i].yohoInfo[0]["yoho_display_string"];
        if (!tmp) {
            // 用法未設定ならばスキップ
            continue;
        }
        if ((yoho_touyo_start_ymd != '00000000')
         && (rpInfo[i].yohoInfo[0]["yoho_touyo_start_ymd"] != '00000000')) {  // 投与開始日
            rpInfo[i].yohoInfo[0]["yoho_touyo_start_ymd"] = yoho_touyo_start_ymd;
            tmp = tmp.replace(/\|\s(\d+)年(\d+)月(\d+)日/, '| ' + touyo_start_ymd_display);
        }
        if ((parseInt(yoho_touyo_nichi_bun, 10) > 0)
         && (parseInt(rpInfo[i].yohoInfo[0]["yoho_touyo_nichi_bun"], 10) > 0)) {    // 投与期間の「何日分」
            rpInfo[i].yohoInfo[0]["yoho_touyo_nichi_bun"] = yoho_touyo_nichi_bun;
            tmp = tmp.replace(/(\d+)日分/, touyo_nichi_bun_display);
        }
        if ((parseInt(yoho_touyo_nichi_oki, 10) > 0)
         && (parseInt(rpInfo[i].yohoInfo[0]["yoho_touyo_nichi_oki"], 10) > 0)) {    // 投与期間の「何日おき」
            rpInfo[i].yohoInfo[0]["yoho_touyo_nichi_oki"] = yoho_touyo_nichi_oki;
            tmp = tmp.replace(/(\d+)日おき/, touyo_nichi_oki_display);
        }

        // 適用後の用法をセットし直す
        rpInfo[i].yohoInfo[0]["yoho_display_string"] = tmp;
    }

    // RP表の領域の、HTMLを作成
    var dat = createRPTableHTML(0, rpInfoMaxCnt);
    elems["rp_listtable"].innerHTML = dat.join('');
}

//********************************************
// 入力サポート子画面の、薬剤名称リストを作成
//********************************************
function createYohoCommentMedList() {
    // 重複の無い薬剤名の文字列リストを作成
    var list = Array();
    var medInfo = null;
    for (var m in rpInfo) {
        medInfo = rpInfo[m].medInfo;
        for (var n in medInfo) {
            if (!medInfo[n].med_name) {
                continue;
            }

            if (list.indexOf(medInfo[n].med_name) != -1) {
                // 同じ薬剤名は追加しない
                continue;
            }

            list.push(medInfo[n].med_name);
        }
    }

    // 割り当て済みの子要素を削除
    while (elems['div_med_list'].firstChild) {
        elems['div_med_list'].removeChild(elems['div_med_list'].firstChild);
    }

    var div = null;
    for (var i = 0; i < list.length; i++) {
        div = document.createElement('div');
        div.style.cssText = 'margin: 2px;'
                          + 'border: 1px solid #aaa;'
                          + 'background-color: #ddd;'
                          + 'cursor: pointer;'
                          + 'color: #00f;';
        div.innerHTML = list[i];
        div.onclick = function() {
            btnSupportWindowClicked(this, 'yoho_comment_result');
        };
        div.onmouseover = function() {
            hovIn(this);
        }
        div.onmouseout = function() {
            hovOut(this);
        }
        elems['div_med_list'].appendChild(div);
    }
}

//********************************************
// 指定した要素のカーソル位置を取得する
//********************************************
function getCaretPosition(obj) {
    var agent = window.navigator.userAgent.toLowerCase();
    obj.focus();
    var pos;
    if (agent.indexOf('msie') != -1) {
        pos = document.selection.createRange();
    } else {
        pos = obj.selectionStart;
    }
    return pos;
}

//********************************************
// RP数を取得する
//********************************************
function countTotalRP() {
    var rpInfo_indi;
    var rpInfoMaxCnt = 0;
    for (rpInfo_indi in rpInfo) {
        rpInfoMaxCnt++;
    }

    return rpInfoMaxCnt;
}