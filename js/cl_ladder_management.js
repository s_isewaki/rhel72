function research()
{
	document.ladder_form.action = "#";
	document.ladder_form.target = "";
	document.ladder_form.submit();
}

function show_detail(emp_id,year)
{

	var moduleName = "cl_ladder_authorize_employe";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.emp_id = emp_id;
	params.year = year;
	params.mode = 'detail';
	params.p_session = document.ladder_form.session.value;

	//ウィンドウサイズなどのオプション
	//2012/05/15 K.Fujii upd(s)
	//var w = 880;
	var w = 1024;
	//2012/05/15 K.Fujii upd(e)
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option,'cl_ladder_authorize_employe',params.p_session);

}
function update_certificate_date()
{
	var update_count = document.ladder_form.ladder_chkbox.length;
	var update_chk_count = 0;
	var emp_id = "";
	var emp_id_str = "";
	if (update_count > 1) {
		for (i=0; i < update_count; i++){
			if( document.ladder_form.ladder_chkbox[i].checked ){
				var emp_data = document.ladder_form.ladder_chkbox[i].value.split(",");
				if (emp_data[5] == "") {
					alert("認定証が出力できません。");
					return false;
				}
				update_chk_count++;
				emp_id = document.ladder_form.ladder_chkbox[i].value;
				emp_id_str = emp_id_str + emp_data + '@';
			}
		}
	} else {
		if( document.ladder_form.ladder_chkbox.checked ){
			var emp_data = document.ladder_form.ladder_chkbox[i].value.split(",");
			if (emp_data[5] == "") {
				alert("認定証が出力できません。");
				return false;
			}
			update_chk_count++;
			emp_id = document.ladder_form.ladder_chkbox[i].value;
			emp_id_str = emp_id;
		}
	}

	if( update_chk_count == 0 ){
		alert("認定証発行できるものがありません。");
		return;
	}

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	document.ladder_form.emp_date.value = emp_id_str;
	document.ladder_form.date_update_flg.value = "update";

	research();

}
function output_to_print()
{

	var moduleName = "cl_ladder_authorize_print_pdf";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.emp_date = document.ladder_form.emp_date.value;
	params.p_session = document.ladder_form.session.value;

	//ウィンドウサイズなどのオプション
	var w = 500;
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option,'cl_ladder_authorize_print_pdf',params.p_session);

}
