Event.domReady.add(initial_setting);
YAHOO.namespace('cal');
YAHOO.namespace('trip');

// カレンダー初期設定
var calOptions = {
    title: "日付を選択して下さい",
    close: true,
    MDY_YEAR_POSITION:       1,
    MDY_MONTH_POSITION:      2,
    MDY_DAY_POSITION:        3,
    MY_YEAR_POSITION:        1,
    MY_MONTH_POSITION:       2,
    MY_LABEL_YEAR_POSITION:  1,
    MY_LABEL_MONTH_POSITION: 2,
    MY_LABEL_YEAR_SUFFIX:    "\u5E74",
    MY_LABEL_MONTH_SUFFIX:   "",
    MONTHS_SHORT:            ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    MONTHS_LONG:             ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
    WEEKDAYS_1CHAR:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_SHORT:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_MEDIUM:         ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
    WEEKDAYS_LONG:           ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']
};

// 初期設定
function initial_setting() {
    // 非表示セット
    display_set();

    // 出張届
    YAHOO.util.Event.addListener('trip1select', "change", trip1_select);
    trip1_select('trip1select');

    // 宿泊地
    YAHOO.util.Event.addListener('stay1', "click", stay_select);
    YAHOO.util.Event.addListener('stay2', "click", stay_select);
    stay_select('stay1');
    stay_select('stay2');

    // 不要
    YAHOO.util.Event.addListener('transit_flg', "click", transit_flg_select);
    YAHOO.util.Event.addListener('stay_flg', "click", stay_flg_select);
    transit_flg_select('transit_flg');
    stay_flg_select('stay_flg');

    // 金額
    YAHOO.util.Event.addListener('kikan', "change", total_fee_calc);
    YAHOO.util.Event.addListener('kikan2', "change", total_fee_calc);
    YAHOO.util.Event.addListener('stay1_days', "change", total_fee_calc);
    YAHOO.util.Event.addListener('stay2_days', "change", total_fee_calc);
    YAHOO.util.Event.addListener('stay1_days2', "change", total_fee_calc);
    YAHOO.util.Event.addListener('stay2_days2', "change", total_fee_calc);
    YAHOO.util.Event.addListener('entrance_fee', "change", total_fee_calc);

    // カレンダー
    YAHOO.cal.cal1 = new YAHOO.widget.Calendar("cal1","cal1Container", calOptions);
    YAHOO.cal.cal1.render();
    YAHOO.cal.cal1.hide();

    YAHOO.cal.cal2 = new YAHOO.widget.Calendar("cal2","cal2Container", calOptions);
    YAHOO.cal.cal2.render();
    YAHOO.cal.cal2.hide();

    YAHOO.cal.cal1.selectEvent.subscribe(handleSelect, YAHOO.cal.cal1, true);
    YAHOO.cal.cal2.selectEvent.subscribe(handleSelect, YAHOO.cal.cal2, true);

    YAHOO.util.Event.addListener(["date_y1","date_m1","date_d1","date_y2","date_m2","date_d2"], "change", updateCal);
    YAHOO.util.Event.addListener(["date_h1","date_i1","date_h2","date_i2"], "change", kikan_update);

    YAHOO.util.Event.addListener("cal1_icon", "click", function(){ YAHOO.cal.cal1.show(); });
    YAHOO.util.Event.addListener("cal2_icon", "click", function(){ YAHOO.cal.cal2.show(); });

    // 交通費内訳
    add_tt1_row(trip_transport_options);
    add_tt2_row();
    add_tt3_row();
    set_tt_data();

    // 精算金額計算
    total_fee_calc();

    storeLoadedContent();
}

// カレンダー選択時に呼び出されるfunction
function handleSelect(type,args,obj) {
    var id = this.id.slice(-1);
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];

    var selMonth = document.getElementById("date_m" + id);
    var selDay   = document.getElementById("date_d" + id);
    var selYear  = document.getElementById("date_y" + id);

    selMonth.selectedIndex = month;
    selDay.selectedIndex = day;
    for (var y=0;y<selYear.options.length;y++) {
        if (selYear.options[y].value == year) {
            selYear.selectedIndex = y;
            break;
        }
    }
    this.hide();
    kikan_update(false);
}

// カレンダーのプルダウン変更時に呼び出されるfunction
function updateCal() {
    var id = this.id.slice(-1);
    var selMonth = document.getElementById("date_m" + id);
    var selDay   = document.getElementById("date_d" + id);
    var selYear  = document.getElementById("date_y" + id);

    var month = parseInt(selMonth.options[selMonth.selectedIndex].value, 10);
    var day = parseInt(selDay.options[selDay.selectedIndex].value, 10);
    var year = parseInt(selYear.options[selYear.selectedIndex].value, 10);
    if (! isNaN(month) && ! isNaN(day) && ! isNaN(year)) {
        var date = year + "/" + month + "/" + day;
        var cal = eval('YAHOO.cal.cal' + id);
        cal.select(date);
        cal.cfg.setProperty("pagedate", year + "/" + month);
        cal.render();
    }
}

// display set
function display_set() {
    document.getElementById('tr_fuyou').style.display = 'none';
    document.getElementById('tr_stay').style.display = 'none';
    document.getElementById('tr_transit').style.display = 'none';
    document.getElementById('tr_daily_fee').style.display = 'none';
    document.getElementById('tr_hotel_fee').style.display = 'none';
    document.getElementById('tr_prepayment').style.display = 'none';
    document.getElementById('label_stay_flg').style.display = 'none';
}

// 出張届select選択処理
function trip1_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;

    if (el.value=="") {
        // 選択されていなければボタン無効
        document.getElementById('trip1table').style.display = 'none';
        switch_button(false);
    }
    else {
        var t = trip1[el.value];
        YAHOO.trip.t = t;

        // ボタン有効
        document.getElementById('trip1table').style.display = '';
        switch_button(true);

        // 出張届欄に値を設定
        document.getElementById('trip1_kubun').innerHTML = trip1_kubun[t.kubun];
        document.getElementById('trip1_kikan').innerHTML = t.date_y1+'/'+t.date_m1+'/'+t.date_d1+' 〜 '+ t.date_y2+'/'+t.date_m2+'/'+t.date_d2;
        document.getElementById('trip1_place').innerHTML = trip1_pref[t.pref] + ' ' + t.city + ' ' + t.place;
        document.getElementById('trip1_youken').innerHTML = youken_text(t);
        document.getElementById('trip1_sharei').innerHTML = trip1_sharei[t.sharei];
        document.getElementById('trip1_prepayment').innerHTML = trip1_prepayment[t.prepayment];
        document.getElementById('trip1_preticket').innerHTML = trip1_preticket[t.preticket];
        document.getElementById('trip1_youken1').value = youken1_text(t);

        var transit = "";
        if (t.transit) {
            for (i=0; i<t.transit.length; i++) {
                transit += trip1_transit[t.transit[i]] + " ";
            }
        }
        document.getElementById('trip1_transit').innerHTML = transit;

        // 非表示リセット
        display_set();
        if (typeof(id)!="string") {
            document.getElementById('transit_flg').checked = false;
            document.getElementById('stay_flg').checked = false;
        }

        // 日当、種泊料、パーセンテージをリセット
        if (typeof(id)!="string") {
            document.getElementById('daily_fee').value = 0;
            document.getElementById('hotel_fee').value = 0;
            document.getElementById('tokyo_fee').value = 0;
            document.getElementById('daily_fee2').value = 0;
            document.getElementById('hotel_fee2').value = 0;
            document.getElementById('tokyo_fee2').value = 0;
            document.getElementById('daily_fee_percentage').value = 0;
            document.getElementById('tokyo_fee_percentage').value = 0;
            document.getElementById('hotel_fee_percentage').value = 0;
            document.getElementById('daily_fee_percentage2').value = 0;
            document.getElementById('tokyo_fee_percentage2').value = 0;
            document.getElementById('hotel_fee_percentage2').value = 0;
        }

        // 出張期間
        if (typeof(id)!="string") {
            document.getElementById("date_y1").value = t.date_y1;
            document.getElementById("date_m1").value = t.date_m1;
            document.getElementById("date_d1").value = t.date_d1;
            document.getElementById("date_y2").value = t.date_y2;
            document.getElementById("date_m2").value = t.date_m2;
            document.getElementById("date_d2").value = t.date_d2;
        }
        kikan_update(true);

        // 区分による設定（謝礼が本人以外の場合）
        if (t.sharei != 2) {
            switch (t.kubun) {
                case "1": // 出張(日帰り)
                case "4": // 長期日帰り(同一出張先に連続して3週間以上)
                    // 要不要
                    document.getElementById('tr_fuyou').style.display = '';

                    // 交通費内訳
                    document.getElementById('tr_transit').style.display = '';

                    // 日当
                    document.getElementById('tr_daily_fee').style.display = '';

                    // 日当パーセンテージ
                    if (t.kubun == 4) {
                        document.getElementById('daily_fee_percentage').value = long_daytrip_percentage;
                        document.getElementById('daily_fee_percentage2').value = long_daytrip_percentage;
                    }
                    else {
                        document.getElementById('daily_fee_percentage').value = 100;
                        document.getElementById('daily_fee_percentage2').value = 100;
                    }
                    break;

                case "2": // 出張(4日間以内の宿泊)
                case "3": // 出張(5日間以上の宿泊)
                case "5": // 長期宿泊出張(同一出張先に連続して2週間以上)
                    // 要不要
                    document.getElementById('tr_fuyou').style.display = '';
                    document.getElementById('label_stay_flg').style.display = '';

                    // 宿泊地
                    document.getElementById('tr_stay').style.display = '';

                    // 交通費内訳
                    document.getElementById('tr_transit').style.display = '';

                    // 日当
                    document.getElementById('tr_daily_fee').style.display = '';

                    // 宿泊
                    document.getElementById('tr_hotel_fee').style.display = '';

                    // 日当、宿泊パーセンテージ
                    if (t.kubun == 5) {
                        document.getElementById('daily_fee_percentage').value = long_stay_percentage;
                        document.getElementById('tokyo_fee_percentage').value = long_hotel_percentage;
                        document.getElementById('hotel_fee_percentage').value = long_hotel_percentage;
                        document.getElementById('daily_fee_percentage2').value = long_stay_percentage;
                        document.getElementById('tokyo_fee_percentage2').value = long_hotel_percentage;
                        document.getElementById('hotel_fee_percentage2').value = long_hotel_percentage;
                    }
                    else {
                        document.getElementById('daily_fee_percentage').value = 100;
                        document.getElementById('tokyo_fee_percentage').value = 100;
                        document.getElementById('hotel_fee_percentage').value = 100;
                        document.getElementById('daily_fee_percentage2').value = 100;
                        document.getElementById('tokyo_fee_percentage2').value = 100;
                        document.getElementById('hotel_fee_percentage2').value = 100;
                    }
                    break;

                case "6": // 自己研修(日帰り)
                case "7": // 自己研修(4日間以内の宿泊)
                case "8": // 自己研修(5日間以上の宿泊)
                case "9": // 公用外出
                    // 要不要
                    document.getElementById('tr_fuyou').style.display = '';

                    // 交通費内訳
                    document.getElementById('tr_transit').style.display = '';
                    break;
            }
        }

        // 私有車理由
        document.getElementById("tt3_reason").value = t.reason_transit6;

        // 前渡金
        if (t.prepayment == 2) {
            document.getElementById('tr_prepayment').style.display = '';
            document.getElementById('prepayment').value = t.prepayment_value;
        } else {
            document.getElementById('tr_prepayment').style.display = 'none';
            document.getElementById('prepayment').value = 0;
        }
    }
}

// 要件テキスト
function youken_text(t) {
    var y;
    if (t.youken==1) {
        y = t.gakkai_name + '<br />';
        y += t.date_y3+'/'+t.date_m3+'/'+t.date_d3+' 〜 '+ t.date_y4+'/'+t.date_m4+'/'+t.date_d4 + '<br />';
        y += '演題発表：' + trip1_presentation[t.presentation];
        if (t.presentation==1) {
            y += ' (' + trip1_act[t.act] + ')<br />';
            y += '演題名：' + t.presentation_titie;
        }
    }
    else {
        y = t.gakkai_igai.replace(/\n/g,"<br />");
    }
    return y;
}

// 要件テキスト1行目
function youken1_text(t) {
    var y;
    if (t.youken==1) {
        return t.gakkai_name;
    }
    else if (t.gakkai_igai.match(/\n/)) {
        return t.gakkai_igai.replace(/\n.*/,"");
    }
    else {
        return t.gakkai_igai;
    }
}

// 2つの日付の差（何日間あるか）を求める
// http://www.hoge256.net/2007/08/64.html
function compareDate(y2, m2, d2, h2, i2, y1, m1, d1, h1, i1) {
    var dt1 = new Date(y1, m1 - 1, d1);
    var dt2 = new Date(y2, m2 - 1, d2);
    var hour1 = h1 + i1;
    var hour2 = h2 + i2;

    // 日帰り
    if (dt1.toString() == dt2.toString()) {
        // 5時間以内
        var hh1 = new Date(1970, 0, 1, h1, i1);
        var hh2 = new Date(1970, 0, 1, h2, i2);
        if ((hh2 - hh1) / 3600000 < 5) {
            return 0;
        }
        // 出発が11:30以降
        if (hour1 >= "1130") {
            return 0.5;
        }
        // 帰着が13:30以前
        if (hour2 <= "1330") {
            return 0.5;
        }
        return 1;
    }

    // 宿泊
    else {
        var kikan = (dt2 - dt1) / 86400000 + 1;
        // 12:00以降の出発
        if (hour1 >= "1200") {
            kikan -= 0.5;
        }
        // 12:00以前の帰着
        if (hour2 <= "1200") {
            kikan -= 0.5;
        }
        return kikan;
    }
}

// 交通費不要checkbox選択処理
function transit_flg_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.checked) {
        document.getElementById('tr_transit').style.display = 'none';
    } else if (document.getElementById('trip1select').value && YAHOO.trip.t && YAHOO.trip.t.sharei != 2) {
        document.getElementById('tr_transit').style.display = '';
    }
}

// 宿泊料不要checkbox選択処理
function stay_flg_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.checked) {
        document.getElementById('tr_hotel_fee').style.display = 'none';
    } else if (document.getElementById('trip1select').value && YAHOO.trip.t && YAHOO.trip.t.sharei != 2) {
        if (YAHOO.trip.t.kubun == 2 || YAHOO.trip.t.kubun == 3 || YAHOO.trip.t.kubun == 5) {
            document.getElementById('tr_hotel_fee').style.display = '';
        }
    }
}

// 宿泊地checkbox選択処理
function stay_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.value == 1) {
        if (el.checked) {
            document.getElementById('stay1_form').style.display = '';
        } else {
            document.getElementById('stay1_form').style.display = 'none';
            document.getElementById('stay1_days').value = 0;
        }
    }
    else {
        if (el.checked) {
            document.getElementById('stay2_form').style.display = '';
        } else {
            document.getElementById('stay2_form').style.display = 'none';
            document.getElementById('stay2_days').value = 0;
        }
    }
}

// 交通費内訳1計算
function tt1_fee_calc() {
    var tt1_fee = 0;
    for (i=0; i<document.getElementsByName('tt1_fee[]').length; i++) {
        if (document.getElementsByName('tt1_from[]')[i].value && document.getElementsByName('tt1_to[]')[i].value) {
            tt1_fee += parseInt(document.getElementsByName('tt1_fee[]')[i].value);
        }
    }
    document.getElementById("tt1_fee_total").value = tt1_fee;
}

// 交通費内訳2計算
function tt2_fee_calc() {
    var tt2_fee = 0;
    var tt2_stay = false;
    for (i=0; i<document.getElementsByName('tt2_fee[]').length; i++) {
        tt2_fee += parseInt(document.getElementsByName('tt2_fee[]')[i].value);
        if (document.getElementsByName('tt2_type[]')[i].value >= 2 && document.getElementsByName('tt2_type[]')[i].value <= 4) {
            tt2_stay = true;
        }
    }
    document.getElementById("tt2_fee_total").value = tt2_fee;

    if (tt2_stay) {
        document.getElementById("stay_flg").checked = true;
        stay_flg_select('stay_flg');
    }
}

// 交通費内訳3計算
function tt3_fee_calc() {

    // 距離合計を割り出す
    var dist = 0;
    for (i=0; i<document.getElementsByName('tt3_fee[]').length; i++) {
        if (document.getElementsByName('tt3_type[]')[i].value == 1) {
            if (document.getElementsByName('tt3_flg[]')[i].value != 1) {
                dist += parseInt(document.getElementsByName('tt3_dist[]')[i].value);
            }
        }
    }

    var tt3_fee = 0;
    for (i=0; i<document.getElementsByName('tt3_fee[]').length; i++) {
        // 距離からガソリン代を計算
        // 走行距離 / 1リットルあたりの走行距離 * 1リットルあたりの購入額 + 購入額に加算する額
        if (document.getElementsByName('tt3_type[]')[i].value == 1) {
            if (document.getElementsByName('tt3_flg[]')[i].value != 1) {
                if (dist >= 12) {
                    document.getElementsByName('tt3_fee[]')[i].value = get_oil_price(
                        document.getElementsByName('tt3_dist[]')[i].value,
                        document.getElementsByName('tt3_car[]')[i].value,
                        document.getElementsByName('tt3_date[]')[i].value
                    );
                }
                else {
                    document.getElementsByName('tt3_fee[]')[i].value = 0;
                }
            }
        } else {
            document.getElementsByName('tt3_dist[]')[i].value = 0;
        }

        tt3_fee += parseInt(document.getElementsByName('tt3_fee[]')[i].value);
    }
    document.getElementById("tt3_fee_total").value = tt3_fee;
}

// 精算金額計算
function total_fee_calc() {
    // 交通費内訳計算
    tt1_fee_calc();
    tt2_fee_calc();
    tt3_fee_calc();

    // 実働日数
    document.getElementById("daily_fee_days").value = document.getElementById("kikan").value;
    if (document.getElementById("kikan2").value > 0) {
        document.getElementById("daily_fee_days2").value = document.getElementById("kikan2").value;
    }

    // 宿泊日数
    document.getElementById("tokyo_fee_days").value = document.getElementById("stay1_days").value;
    document.getElementById("hotel_fee_days").value = document.getElementById("stay2_days").value;
    if (document.getElementById("stay1_days2").value > 0) {
        document.getElementById("tokyo_fee_days2").value = document.getElementById("stay1_days2").value;
    }
    if (document.getElementById("stay2_days2").value > 0) {
        document.getElementById("hotel_fee_days2").value = document.getElementById("stay2_days2").value;
    }

    // 日当
    document.getElementById('daily_fee_total').value = Math.floor(
        parseInt(document.getElementById('daily_fee').value) *
        parseFloat(document.getElementById('daily_fee_days').value) *
        parseInt(document.getElementById('daily_fee_percentage').value) / 100 + 0.5
    );
    if (document.getElementById("kikan2").value > 0) {
        document.getElementById('daily_fee_total2').value = Math.floor(
            parseInt(document.getElementById('daily_fee2').value) *
            parseFloat(document.getElementById('daily_fee_days2').value) *
            parseInt(document.getElementById('daily_fee_percentage2').value) / 100 + 0.5
        );
    }

    // 宿泊料
    document.getElementById('hotel_fee_total').value = Math.floor(
        parseInt(document.getElementById('hotel_fee').value) *
        parseInt(document.getElementById('hotel_fee_days').value) *
        parseInt(document.getElementById('hotel_fee_percentage').value) / 100 + 0.5
    );
    if (document.getElementById("stay2_days2").value > 0) {
        document.getElementById('hotel_fee_total2').value = Math.floor(
            parseInt(document.getElementById('hotel_fee2').value) *
            parseInt(document.getElementById('hotel_fee_days2').value) *
            parseInt(document.getElementById('hotel_fee_percentage2').value) / 100 + 0.5
        );
    }

    // 宿泊料(東京)
    document.getElementById('tokyo_fee_total').value = Math.floor(
        parseInt(document.getElementById('tokyo_fee').value) *
        parseInt(document.getElementById('tokyo_fee_days').value) *
        parseInt(document.getElementById('tokyo_fee_percentage').value) / 100 + 0.5
    );
    if (document.getElementById("stay1_days2").value > 0) {
        document.getElementById('tokyo_fee_total2').value = Math.floor(
            parseInt(document.getElementById('tokyo_fee2').value) *
            parseInt(document.getElementById('tokyo_fee_days2').value) *
            parseInt(document.getElementById('tokyo_fee_percentage2').value) / 100 + 0.5
        );
    }

    // 謝礼本人
    if (YAHOO.trip.t && YAHOO.trip.t.sharei == 2) {
        document.getElementById('tt1_fee_total').value = 0;
        document.getElementById('tt2_fee_total').value = 0;
        document.getElementById('tt3_fee_total').value = 0;
        document.getElementById('daily_fee_total').value = 0;
        document.getElementById('hotel_fee_total').value = 0;
        document.getElementById('tokyo_fee_total').value = 0;
        document.getElementById('daily_fee_total2').value = 0;
        document.getElementById('hotel_fee_total2').value = 0;
        document.getElementById('tokyo_fee_total2').value = 0;
    }

    // 交通費不要
    if (document.getElementById('transit_flg').checked) {
        document.getElementById('tt1_fee_total').value = 0;
        document.getElementById('tt2_fee_total').value = 0;
        document.getElementById('tt3_fee_total').value = 0;
    }

    // 宿泊料不要
    if (document.getElementById('stay_flg').checked) {
        document.getElementById('hotel_fee_total').value = 0;
        document.getElementById('tokyo_fee_total').value = 0;
        document.getElementById('hotel_fee_total2').value = 0;
        document.getElementById('tokyo_fee_total2').value = 0;
    }

    // 合計
    var total_fee =
        parseInt(document.getElementById('tt1_fee_total').value) +
        parseInt(document.getElementById('tt2_fee_total').value) +
        parseInt(document.getElementById('tt3_fee_total').value) +
        parseInt(document.getElementById('entrance_fee').value) +
        parseInt(document.getElementById('daily_fee_total').value) +
        parseInt(document.getElementById('hotel_fee_total').value) +
        parseInt(document.getElementById('tokyo_fee_total').value) +
        parseInt(document.getElementById('daily_fee_total2').value) +
        parseInt(document.getElementById('hotel_fee_total2').value) +
        parseInt(document.getElementById('tokyo_fee_total2').value) -
        parseInt(document.getElementById('prepayment').value);
    if (isNaN(total_fee)) {
        document.getElementById('total_fee').value = 0;
    } else {
        document.getElementById('total_fee').value = total_fee;
    }
}

// ボタンスイッチ
function switch_button(flg) {
    if (document.getElementById('apply_button'))  document.getElementById('apply_button').disabled  = !flg;
    if (document.getElementById('draft_button'))  document.getElementById('draft_button').disabled  = !flg;
    if (document.getElementById('print_button'))  document.getElementById('print_button').disabled  = !flg;
    if (document.getElementById('apply_button2')) document.getElementById('apply_button2').disabled = !flg;
    if (document.getElementById('draft_button2')) document.getElementById('draft_button2').disabled = !flg;
    if (document.getElementById('print_button2')) document.getElementById('print_button2').disabled = !flg;
}

// テーブル行の削除
function remove_table_row(obj) {
    var tr = obj.parentNode.parentNode;
    tr.parentNode.deleteRow(tr.sectionRowIndex);
}

// テーブル(tt1)行の追加
function add_tt1_row(transport, t) {
    var table = document.getElementById("transtable1");
    var y = document.getElementById("date_y1").value;
    var m = document.getElementById("date_m1").value;
    var d = document.getElementById("date_d1").value;

    var row = table.insertRow(-1);
    var cell01 = row.insertCell(0);
    var cell02 = row.insertCell(1);
    var cell03 = row.insertCell(2);
    var cell04 = row.insertCell(3);
    var cell05 = row.insertCell(4);
    var cell06 = row.insertCell(5);
    var cell07 = row.insertCell(6);
    var cell08 = row.insertCell(7);
    var cell09 = row.insertCell(8);

    cell01.innerHTML = '<input type="text" name="tt1_date[]" value="'+y+'/'+m+'/'+d+'" size="11" readonly onclick="tt1_calendar(this);" />';
    cell02.innerHTML = '<select name="tt1_transport[]">'+transport+'</select>';
    cell03.innerHTML = '<select name="tt1_oufuku[]"><option value="2">往復</option><option value="1">片道</option></select>';
    cell04.innerHTML = '<input type="text" name="tt1_from[]" value="" size="10" style="ime-mode:active;" />';
    cell05.innerHTML = '<input type="text" name="tt1_to[]" value="" size="10" style="ime-mode:active;" />';
    cell06.innerHTML = '<input type="number" name="tt1_fee[]" value="0" size="8" style="ime-mode:disabled;text-align:right;" onchange="total_fee_calc()" />円';
    cell07.innerHTML = '<input type="text" name="tt1_note[]" value="" size="56" style="ime-mode:active;" />';
    cell08.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
    cell09.innerHTML = '<button type="button" onclick="goto_transit_site(this);">路線</button>';

    if (t && t.transport) {
        cell02.firstChild.value = t.transport;
        cell03.firstChild.value = t.oufuku;
        cell04.firstChild.value = t.from;
        cell05.firstChild.value = t.to;
        cell06.firstChild.value = t.fee;
    }
}

// テーブル(tt2)行の追加
function add_tt2_row() {
    var table = document.getElementById("transtable2");
    var y = document.getElementById("date_y1").value;
    var m = document.getElementById("date_m1").value;
    var d = document.getElementById("date_d1").value;

    var row = table.insertRow(-1);
    var cell01 = row.insertCell(0);
    var cell02 = row.insertCell(1);
    var cell03 = row.insertCell(2);
    var cell04 = row.insertCell(3);
    var cell05 = row.insertCell(4);

    cell01.innerHTML = '<input type="text" name="tt2_date[]" value="'+y+'/'+m+'/'+d+'" size="11" readonly onclick="tt2_calendar(this);" />';
    cell02.innerHTML = '<select name="tt2_type[]"><option value="1">交通費のみ</option><option value="2">宿泊のみ</option><option value="3">交通費＋宿泊</option><option value="4">マンスリーアパート</option><option value="5">その他</option></select>';
    cell03.innerHTML = '<input type="number" name="tt2_fee[]" value="0" size="8" style="ime-mode:disabled;text-align:right;" onchange="total_fee_calc()" />円';
    cell04.innerHTML = '<input type="text" name="tt2_note[]" value="" size="80" style="ime-mode:active;" />';
    cell05.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
}

// テーブル(tt3)行の追加
function add_tt3_row(t) {
    var table = document.getElementById("transtable3");
    var y = document.getElementById("date_y1").value;
    var m = document.getElementById("date_m1").value;
    var d = document.getElementById("date_d1").value;

    var row = table.insertRow(-1);
    var cell01 = row.insertCell(0);
    var cell02 = row.insertCell(1);
    var cell03 = row.insertCell(2);
    var cell04 = row.insertCell(3);
    var cell05 = row.insertCell(4);
    var cell06 = row.insertCell(5);
    var cell07 = row.insertCell(6);
    var cell08 = row.insertCell(7);
    var cell09 = row.insertCell(8);
    var cell10 = row.insertCell(9);

    cell01.innerHTML = '<input type="text" name="tt3_date[]" value="'+y+'/'+m+'/'+d+'" size="11" readonly onclick="tt3_calendar(this);" />';
    cell02.innerHTML = '<select name="tt3_type[]" onchange="tt3_type_select(this,true)"><option value="1">私有車</option><option value="2">高速料金</option><option value="3">駐車料金</option></select>';
    cell03.innerHTML = '<select name="tt3_car[]" onchange="total_fee_calc()"><option value="1">普通車</option><option value="2">軽自動車</option></select>';
    cell04.innerHTML = '<input type="text" name="tt3_from[]" value="" size="10" style="ime-mode:active;" />';
    cell05.innerHTML = '<input type="text" name="tt3_via1[]" value="" size="10" style="ime-mode:active;" />';
    cell06.innerHTML = '<input type="text" name="tt3_via2[]" value="" size="10" style="ime-mode:active;" />';
    cell07.innerHTML = '<input type="text" name="tt3_to[]" value="" size="10" style="ime-mode:active;" />';
    cell08.innerHTML = '<span><input type="number" name="tt3_dist[]" value="0" size="5" style="ime-mode:disabled;text-align:right;" onchange="total_fee_calc()" />km</span>';
    cell09.innerHTML = '<input type="number" name="tt3_fee[]" value="0" size="8" readonly style="ime-mode:disabled;text-align:right;" onchange="total_fee_calc()" />円';
    cell10.innerHTML = '<input type="hidden" name="tt3_flg[]" value="0" /><button type="button" onclick="remove_table_row(this);">削除</button>';
}

// tt3_type_select
function tt3_type_select(obj,reset) {
    var i = obj.parentNode.parentNode.sectionRowIndex;

    if (reset) {
        document.getElementsByName('tt3_car[]')[i].value = "1";
        document.getElementsByName('tt3_from[]')[i].value = "";
        document.getElementsByName('tt3_via1[]')[i].value = "";
        document.getElementsByName('tt3_via2[]')[i].value = "";
        document.getElementsByName('tt3_to[]')[i].value = "";
        document.getElementsByName('tt3_dist[]')[i].value = 0;
        document.getElementsByName('tt3_fee[]')[i].value = 0;
    }

    switch (document.getElementsByName('tt3_type[]')[i].value) {
        case "1": // 私有車
            document.getElementsByName('tt3_car[]')[i].style.display = "";
            document.getElementsByName('tt3_from[]')[i].style.display = "";
            document.getElementsByName('tt3_via1[]')[i].style.display = "";
            document.getElementsByName('tt3_via2[]')[i].style.display = "";
            document.getElementsByName('tt3_to[]')[i].style.display = "";
            document.getElementsByName('tt3_dist[]')[i].parentNode.style.display = "";
            document.getElementsByName('tt3_fee[]')[i].readOnly = true;
            break;
        case "2": // 高速料金
            document.getElementsByName('tt3_car[]')[i].style.display = "";
            document.getElementsByName('tt3_from[]')[i].style.display = "";
            document.getElementsByName('tt3_via1[]')[i].style.display = "";
            document.getElementsByName('tt3_via2[]')[i].style.display = "";
            document.getElementsByName('tt3_to[]')[i].style.display = "";
            document.getElementsByName('tt3_dist[]')[i].parentNode.style.display = "none";
            document.getElementsByName('tt3_fee[]')[i].readOnly = false;
            break;
        case "3": // 駐車料金
            document.getElementsByName('tt3_car[]')[i].style.display = "none";
            document.getElementsByName('tt3_from[]')[i].style.display = "none";
            document.getElementsByName('tt3_via1[]')[i].style.display = "none";
            document.getElementsByName('tt3_via2[]')[i].style.display = "none";
            document.getElementsByName('tt3_to[]')[i].style.display = "none";
            document.getElementsByName('tt3_dist[]')[i].parentNode.style.display = "none";
            document.getElementsByName('tt3_fee[]')[i].readOnly = false;
            break;
    }
}

// カレンダー呼び出し(tt1)
function tt1_calendar(obj) {
    var id = 'tt1cal_' + obj.parentNode.parentNode.sectionRowIndex;
    var cal = new YAHOO.widget.Calendar(id, 'tt1_cal', calOptions);
    cal.select(obj.value);
    cal.cfg.setProperty("pagedate", obj.value);
    cal.render();
    cal.selectEvent.subscribe(handleSelect_tt1, cal, true);
    cal.show();
}

// カレンダー呼び出し(tt2)
function tt2_calendar(obj) {
    var id = 'tt2cal_' + obj.parentNode.parentNode.sectionRowIndex;
    var cal = new YAHOO.widget.Calendar(id, 'tt2_cal', calOptions);
    cal.select(obj.value);
    cal.cfg.setProperty("pagedate", obj.value);
    cal.render();
    cal.selectEvent.subscribe(handleSelect_tt2, cal, true);
    cal.show();
}

// カレンダー呼び出し(tt3)
function tt3_calendar(obj) {
    var id = 'tt3cal_' + obj.parentNode.parentNode.sectionRowIndex;
    var cal = new YAHOO.widget.Calendar(id, 'tt3_cal', calOptions);
    cal.select(obj.value);
    cal.cfg.setProperty("pagedate", obj.value);
    cal.render();
    cal.selectEvent.subscribe(handleSelect_tt3, cal, true);
    cal.show();
}

// カレンダー選択時に呼び出されるfunction(tt1)
function handleSelect_tt1(type,args,obj) {
    var i = this.id.slice(7);
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];
    document.getElementsByName('tt1_date[]')[i].value = year + '/' + month.fillZero(2) + '/' + day.fillZero(2);
    this.hide();
}

// カレンダー選択時に呼び出されるfunction(tt2)
function handleSelect_tt2(type,args,obj) {
    var i = this.id.slice(7);
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];
    document.getElementsByName('tt2_date[]')[i].value = year + '/' + month.fillZero(2) + '/' + day.fillZero(2);
    this.hide();
}

// カレンダー選択時に呼び出されるfunction(tt3)
function handleSelect_tt3(type,args,obj) {
    var i = this.id.slice(7);
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];
    document.getElementsByName('tt3_date[]')[i].value = year + '/' + month.fillZero(2) + '/' + day.fillZero(2);
    this.hide();
}

// 入力値チェック
function InputCheck() {
    // 出張届
    if (!document.getElementById("trip1select").value) {
        alert('出張届が選択されていません。');
        return false;
    }

    // 精算金額計算
    total_fee_calc();

    // 発着時間
    if (document.getElementById("date_y2").value == '--' ||
        document.getElementById("date_m2").value == '--' ||
        document.getElementById("date_d2").value == '--' ||
        document.getElementById("date_h2").value == '--' ||
        document.getElementById("date_i2").value == '--' ||
        document.getElementById("date_y1").value == '--' ||
        document.getElementById("date_m1").value == '--' ||
        document.getElementById("date_d1").value == '--' ||
        document.getElementById("date_h1").value == '--' ||
        document.getElementById("date_i1").value == '--' ) {
        alert('発着時間が選択されていません。');
        return false;
    }
    // 出張実働日数
    if (document.getElementById("kikan").value < 0) {
        alert('出張実働日数が正しくありません。');
        return false;
    }
    var datetime_1 = document.getElementById("date_y1").value +
                     document.getElementById("date_m1").value +
                     document.getElementById("date_d1").value +
                     document.getElementById("date_h1").value +
                     document.getElementById("date_i1").value;
    var datetime_2 = document.getElementById("date_y2").value +
                     document.getElementById("date_m2").value +
                     document.getElementById("date_d2").value +
                     document.getElementById("date_h2").value +
                     document.getElementById("date_i2").value;
    if (datetime_2 <= datetime_1) {
        alert('発着時間が正しくありません。');
        return false;
    }
    if (document.getElementById('kikan').value.match(/[^0-9.]+/)) {
        alert('出張実働日数に数字以外が入力されています。');
        return false;
    }
    if (document.getElementById('kikan2').value.match(/[^0-9.]+/)) {
        alert('出張実働日数(B)に数字以外が入力されています。');
        return false;
    }
    var check_kikan = compareDate(
        document.getElementById("date_y2").value,
        document.getElementById("date_m2").value,
        document.getElementById("date_d2").value,
        document.getElementById("date_h2").value,
        document.getElementById("date_i2").value,
        document.getElementById("date_y1").value,
        document.getElementById("date_m1").value,
        document.getElementById("date_d1").value,
        document.getElementById("date_h1").value,
        document.getElementById("date_i1").value
    );
    var total_kikan = parseFloat(document.getElementById('kikan').value) + parseFloat(document.getElementById('kikan2').value);
    if (total_kikan > check_kikan) {
        alert('出張実働日数が発着時間の日数を超えています。');
        return false;
    }
    // 宿泊地
    if (document.getElementById("stay1").checked) {
        if (document.getElementById("stay1_days").value < 0) {
            alert('東京23区内:宿泊数が正しくありません。');
            return false;
        }
        if (document.getElementById('stay1_days').value.match(/[^0-9]+/)) {
            alert('東京23区内:宿泊数に数字以外が入力されています。');
            return false;
        }
        if (document.getElementById('stay1_days2').value.match(/[^0-9]+/)) {
            alert('東京23区内:宿泊数(B)に数字以外が入力されています。');
            return false;
        }
    }
    if (document.getElementById("stay2").checked) {
        if (document.getElementById("stay2_days").value < 0) {
            alert('東京23区外:宿泊数が正しくありません。');
            return false;
        }
        if (document.getElementById('stay2_days').value.match(/[^0-9]+/)) {
            alert('東京23区外:宿泊数に数字以外が入力されています。');
            return false;
        }
        if (document.getElementById('stay2_days2').value.match(/[^0-9]+/)) {
            alert('東京23区外:宿泊数(B)に数字以外が入力されています。');
            return false;
        }
        if (document.getElementById("stay2_city").value < 1) {
            alert('東京23区外:場所が入力されていません。');
            return false;
        }
    }
    if (document.getElementById("stay1").checked || document.getElementById("stay2").checked) {
        var stay_days = 0;
        stay_days += document.getElementById("stay1").checked ? parseInt(document.getElementById('stay1_days').value) : 0;
        stay_days += document.getElementById("stay1").checked ? parseInt(document.getElementById('stay1_days2').value) : 0;
        stay_days += document.getElementById("stay2").checked ? parseInt(document.getElementById('stay2_days').value) : 0;
        stay_days += document.getElementById("stay2").checked ? parseInt(document.getElementById('stay2_days2').value) : 0;
        if (stay_days >= check_kikan) {
            alert('宿泊数合計が発着時間の日数以上になっています。');
            return false;
        }
    }
    // 参加費
    if (document.getElementById("entrance_fee").value < 0) {
        alert('参加費が正しくありません。');
        return false;
    }
    if (document.getElementById('entrance_fee').value.match(/[^0-9]+/)) {
        alert('参加費に数字以外が入力されています。');
        return false;
    }
    // 交通費1
    for (i=0; i<document.getElementsByName('tt1_fee[]').length; i++) {
        if (!document.getElementsByName('tt1_from[]')[i].value && document.getElementsByName('tt1_to[]')[i].value) {
            alert('交通費内訳:公共交通機関:出発駅が入力されていません。');
            return false;
        }
        if (document.getElementsByName('tt1_from[]')[i].value && !document.getElementsByName('tt1_to[]')[i].value) {
            alert('交通費内訳:公共交通機関:到着駅が入力されていません。');
            return false;
        }
        if (document.getElementsByName('tt1_from[]')[i].value && document.getElementsByName('tt1_to[]')[i].value) {
            if (document.getElementsByName('tt1_fee[]')[i].value < 1) {
                alert('交通費内訳:公共交通機関:金額が正しくありません。');
                return false;
            }
            if (document.getElementsByName('tt1_fee[]')[i].value.match(/[^0-9]+/)) {
                alert('交通費内訳:公共交通機関:金額に数字以外が入力されています。');
                return false;
            }
        }
    }
    // 交通費2
    for (i=0; i<document.getElementsByName('tt2_fee[]').length; i++) {
        if (document.getElementsByName('tt2_fee[]')[i].value.match(/[^0-9]+/)) {
            alert('交通費内訳:ビジネスパック:金額に数字以外が入力されています。');
            return false;
        }
    }
    // 交通費3
    for (i=0; i<document.getElementsByName('tt3_fee[]').length; i++) {
        switch (document.getElementsByName('tt3_type[]')[i].value) {
            case "1": // 私有車
                if (!document.getElementsByName('tt3_from[]')[i].value && document.getElementsByName('tt3_to[]')[i].value) {
                    alert('交通費内訳:私有車:出発地が入力されていません。');
                    return false;
                }
                if (document.getElementsByName('tt3_from[]')[i].value && !document.getElementsByName('tt3_to[]')[i].value) {
                    alert('交通費内訳:私有車:到着地が入力されていません。');
                    return false;
                }
                if (document.getElementsByName('tt3_from[]')[i].value && document.getElementsByName('tt3_to[]')[i].value) {
                    if (document.getElementsByName('tt3_dist[]')[i].value.match(/[^0-9]+/)) {
                        alert('交通費内訳:私有車:距離に数字以外が入力されています。');
                        return false;
                    }
                }
                break;
            case "2": // 高速料金
                if (!document.getElementsByName('tt3_from[]')[i].value && document.getElementsByName('tt3_to[]')[i].value) {
                    alert('交通費内訳:高速料金:出発地が入力されていません。');
                    return false;
                }
                if (document.getElementsByName('tt3_from[]')[i].value && !document.getElementsByName('tt3_to[]')[i].value) {
                    alert('交通費内訳:高速料金:到着地が入力されていません。');
                    return false;
                }
                if (document.getElementsByName('tt3_from[]')[i].value && document.getElementsByName('tt3_to[]')[i].value) {
                    if (document.getElementsByName('tt3_fee[]')[i].value < 1) {
                        alert('交通費内訳:高速料金:金額が正しくありません。');
                        return false;
                    }
                    if (document.getElementsByName('tt3_fee[]')[i].value.match(/[^0-9]+/)) {
                        alert('交通費内訳:高速料金:金額に数字以外が入力されています。');
                        return false;
                    }
                }
                break;
            case "3": // 駐車料金
                if (document.getElementsByName('tt3_fee[]')[i].value.match(/[^0-9]+/)) {
                    alert('交通費内訳:駐車料金:金額に数字以外が入力されています。');
                    return false;
                }
                break;
        }
    }

    if (!confirm('内容の確認してください。「OK」を押すと登録されます。')) {
        return false;
    }
    return true;
}

// 交通費内訳データの変換
function set_tt_data() {
    if (document.apply.tt1_data.value) {
        var tt1 = eval("("+document.apply.tt1_data.value+")");
        for (i=0; i<tt1.length; i++) {
            add_tt1_row(trip_transport_options);
            document.getElementsByName('tt1_date[]')[i].value = tt1[i].date;
            document.getElementsByName('tt1_transport[]')[i].value = tt1[i].transport;
            document.getElementsByName('tt1_oufuku[]')[i].value = tt1[i].oufuku;
            document.getElementsByName('tt1_from[]')[i].value = tt1[i].from;
            document.getElementsByName('tt1_to[]')[i].value = tt1[i].to;
            document.getElementsByName('tt1_fee[]')[i].value = tt1[i].fee;
            document.getElementsByName('tt1_note[]')[i].value = tt1[i].note;
        }
    }

    if (document.apply.tt2_data.value) {
        var tt2 = eval("("+document.apply.tt2_data.value+")");
        for (i=0; i<tt2.length; i++) {
            add_tt2_row();
            document.getElementsByName('tt2_date[]')[i].value = tt2[i].date;
            document.getElementsByName('tt2_type[]')[i].value = tt2[i].type;
            document.getElementsByName('tt2_fee[]')[i].value = tt2[i].fee;
            document.getElementsByName('tt2_note[]')[i].value = tt2[i].note;
        }
    }

    if (document.apply.tt3_data.value) {
        var tt3 = eval("("+document.apply.tt3_data.value+")");
        for (i=0; i<tt3.length; i++) {
            add_tt3_row();
            document.getElementsByName('tt3_date[]')[i].value = tt3[i].date;
            document.getElementsByName('tt3_date[]')[i].onclick = "";
            document.getElementsByName('tt3_type[]')[i].value = tt3[i].type;
            document.getElementsByName('tt3_car[]')[i].value = tt3[i].car;
            document.getElementsByName('tt3_from[]')[i].value = tt3[i].from;
            document.getElementsByName('tt3_via1[]')[i].value = tt3[i].via1;
            document.getElementsByName('tt3_via2[]')[i].value = tt3[i].via2;
            document.getElementsByName('tt3_to[]')[i].value = tt3[i].to;
            document.getElementsByName('tt3_dist[]')[i].value = tt3[i].dist;
            document.getElementsByName('tt3_dist[]')[i].readOnly = true;
            document.getElementsByName('tt3_fee[]')[i].value = tt3[i].fee;
            document.getElementsByName('tt3_fee[]')[i].style.backgroundColor = "silver";
            document.getElementsByName('tt3_flg[]')[i].value = 1;
        }
        for (i=0; i<document.getElementsByName('tt3_fee[]').length; i++) {
            tt3_type_select(document.getElementsByName('tt3_type[]')[i],false);
        }
    }
}

// Yahoo!路線を開く
function goto_transit_site(obj) {
    var i = obj.parentNode.parentNode.sectionRowIndex;
    var from = document.getElementsByName('tt1_from[]')[i].value;
    var to   = document.getElementsByName('tt1_to[]')[i].value;

    if (!from || !to) {
        return false;
    }
    var url = 'http://transit.map.yahoo.co.jp/search/result?from=' + encodeURIComponent(from)  + '&to=' + encodeURIComponent(to) ;
    window.open(url, null);
}

// 経路マスタ選択を開く
function select_transit_mst(mode,session) {
    var url = 'workflow_trip_transit_select.php?session=' + session  + '&mode=' + mode ;
    window.open(url, 'select_mst',"width=500,height=500,menubar=no,resizable=yes,scrollbars=yes");
}

// PDF印刷
function report_pdf_print() {
    var default_target = document.apply.target;
    document.apply.action = "workflow_trip_report_print.php";
    document.apply.target = '_new';
    document.apply.submit();
    document.apply.target = default_target;
}

// "先頭を0で埋めて桁をそろえる"
// http://d.hatena.ne.jp/higeorange/20080515/1210817629
Number.prototype.fillZero = function(n) {
    return Array((n+1) - this.toString().split('').length).join('0') + this;
}

// ガソリン代計算
function get_oil_price(dist,car,date) {
    if (dist == 0) {
        return 0;
    }

    var price = 0;
    var car_distance = 0;

    if (oil_price2_date > oil_price3_date) {
        if (oil_price3_date > date) {
            price = oil_price3;
        }
        else if (oil_price2_date > date) {
            price = oil_price2;
        }
        else {
            price = oil_price;
        }
    }
    else {
        if (oil_price2_date > date) {
            price = oil_price2;
        }
        else if (oil_price3_date > date) {
            price = oil_price3;
        }
        else {
            price = oil_price;
        }
    }

    if (car == 1) {
        car_distance = normal_car_distance;
    }
    else {
        car_distance = small_car_distance;
    }

    return Math.floor(
        parseInt(dist) / car_distance * Math.floor(price + price * oil_percentage / 100 + 0.5) + 0.5
    );
}

// 期間更新
function kikan_update(flg){
    if (flg && document.getElementById('application_mode').value != 'menu') {
        var display = document.getElementById('kikan2').value > 0 ? "" : "none";
        document.getElementById('span_kikan2_1').style.display = display;
        document.getElementById('span_kikan2_2').style.display = display;
        document.getElementById('span_kikan2_3').style.display = display;
        document.getElementById('span_kikan2_4').style.display = display;
        document.getElementById('span_kikan2_5').style.display = display;
        document.getElementById('span_kikan2_6').style.display = display;
        document.getElementById('kikan_after_1').innerHTML = document.getElementById('kikan2_date').value;
        return;
    }

    var params = $H({
        'session': document.getElementsByName('session')[0].value,
        'y1': document.getElementById("date_y1").value,
        'm1': document.getElementById("date_m1").value,
        'd1': document.getElementById("date_d1").value,
        'h1': document.getElementById("date_h1").value,
        'i1': document.getElementById("date_i1").value,
        'y2': document.getElementById("date_y2").value,
        'm2': document.getElementById("date_m2").value,
        'd2': document.getElementById("date_d2").value,
        'h2': document.getElementById("date_h2").value,
        'i2': document.getElementById("date_i2").value
    }).toQueryString();

    // Loading
    document.getElementById('triptable').style.opacity = 0.5;

    // ajax
    var url = 'workflow_trip_get_payment_ajax.php';
    var myAjax = new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: params,
            onComplete: set_payment
        }
    );
}

function set_payment(o) {
    document.getElementById('triptable').style.opacity = 1;
    var data = eval("("+o.responseText+")");

    // 期間
    if (data.result == "2") {
        document.getElementById("kikan").value = data.kikan1;
        document.getElementById('kikan2').value = data.kikan2;
        document.getElementById("daily_fee_days").value = data.kikan1;
        document.getElementById('daily_fee_days2').value = data.kikan2;

        document.getElementById('kikan_after_1').innerHTML = data.date;
        document.getElementById('kikan2_date').value = data.date;
    }
    else {
        document.getElementById("kikan").value = data.kikan;
        document.getElementById("daily_fee_days").value = data.kikan;
        document.getElementById('kikan2').value = 0;
        document.getElementById('daily_fee_days2').value = 0;
    }

    // 日当
    if (YAHOO.trip.t && YAHOO.trip.t.sharei != 2) {
        switch (YAHOO.trip.t.kubun) {
            case "1": // 出張(日帰り)
            case "4": // 長期日帰り(同一出張先に連続して3週間以上)
                // 日当
                document.getElementById('daily_fee').value = data.payment.daytrip;
                if (data.result == "2") {
                    document.getElementById('daily_fee').value = data.before_payment.daytrip;
                    document.getElementById('daily_fee2').value = data.payment.daytrip;
                }
                break;

            case "2": // 出張(4日間以内の宿泊)
            case "3": // 出張(5日間以上の宿泊)
            case "5": // 長期宿泊出張(同一出張先に連続して2週間以上)
                // 日当
                if (data.result == "2") {
                    document.getElementById('daily_fee').value = data.before_payment.stay;
                    document.getElementById('daily_fee2').value = data.payment.stay;
                }
                else {
                    document.getElementById('daily_fee').value = data.payment.stay;
                    document.getElementById('daily_fee2').value = 0;
                    document.getElementById('daily_fee_total2').value = 0;
                }

                // 宿泊
                if (data.result == "2") {
                    document.getElementById('hotel_fee').value = data.before_payment.hotel;
                    document.getElementById('tokyo_fee').value = data.before_payment.hotel_tokyo;
                    document.getElementById('hotel_fee2').value = data.payment.hotel;
                    document.getElementById('tokyo_fee2').value = data.payment.hotel_tokyo;
                }
                else {
                    document.getElementById('hotel_fee').value = data.payment.hotel;
                    document.getElementById('tokyo_fee').value = data.payment.hotel_tokyo;
                    document.getElementById('hotel_fee2').value = 0;
                    document.getElementById('tokyo_fee2').value = 0;
                    document.getElementById('hotel_fee_total2').value = 0;
                    document.getElementById('tokyo_fee_total2').value = 0;
                }
                break;
        }
    }

    // 2つめの期間を表示/非表示
    var display = data.result == "2" ? "" : "none";
    document.getElementById('span_kikan2_1').style.display = display;
    document.getElementById('span_kikan2_2').style.display = display;
    document.getElementById('span_kikan2_3').style.display = display;
    document.getElementById('span_kikan2_4').style.display = display;
    document.getElementById('span_kikan2_5').style.display = display;
    document.getElementById('span_kikan2_6').style.display = display;

    // 精算金額
    total_fee_calc();
}
