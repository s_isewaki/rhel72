Event.domReady.add(initial_setting);
YAHOO.namespace('cal');

// 初期設定
function initial_setting() {
    // 出張届
    if (document.getElementById("trip1select")) {
        YAHOO.util.Event.addListener('trip1copy', "click", trip1copy);
    }

    // 種別
    for (i=0; i<document.getElementsByName('kubun[]').length; i++) {
        YAHOO.util.Event.addListener(document.getElementsByName('kubun[]')[i], "click", kubun_select);
        kubun_select('kubun'+document.getElementsByName('kubun[]')[i].value);
    }

    // 要件
    YAHOO.util.Event.addListener('youken1', "click", youken_select);
    YAHOO.util.Event.addListener('youken2', "click", youken_select);
    youken_select('youken1');
    youken_select('youken2');

    // 要件→演題発表
    YAHOO.util.Event.addListener('presentation0', "click", presentation_select);
    YAHOO.util.Event.addListener('presentation1', "click", presentation_select);
    presentation_select('presentation0');
    presentation_select('presentation1');

    // 交通手段
    YAHOO.util.Event.addListener('transit1', "click", transit_select);
    YAHOO.util.Event.addListener('transit2', "click", transit_select);
    YAHOO.util.Event.addListener('transit3', "click", transit_select);
    YAHOO.util.Event.addListener('transit4', "click", transit_select);
    YAHOO.util.Event.addListener('transit5', "click", transit_select);
    YAHOO.util.Event.addListener('transit6', "click", transit_select);
    transit_select_all();

    // 謝礼
    YAHOO.util.Event.addListener('sharei1', "click", sharei_select);
    YAHOO.util.Event.addListener('sharei2', "click", sharei_select);
    YAHOO.util.Event.addListener('sharei3', "click", sharei_select);
    sharei_select('sharei1');
    sharei_select('sharei2');
    sharei_select('sharei3');

    // 前渡金
    YAHOO.util.Event.addListener('prepayment1', "click", maewatashi_select);
    YAHOO.util.Event.addListener('prepayment2', "click", maewatashi_select);
    maewatashi_select('prepayment1');
    maewatashi_select('prepayment2');

    // 前渡チケット
    YAHOO.util.Event.addListener('preticket1', "click", preticket_select);
    YAHOO.util.Event.addListener('preticket2', "click", preticket_select);
    YAHOO.util.Event.addListener('preticket21', "click", preticket2_select);
    YAHOO.util.Event.addListener('preticket22', "click", preticket2_select);
    preticket_select('preticket1');
    preticket_select('preticket2');
    preticket2_select('preticket21');
    preticket2_select('preticket22');

    // カレンダー初期設定
    var calOptions = {
        title: "日付を選択して下さい",
        close: true,
        MDY_YEAR_POSITION:       1,
        MDY_MONTH_POSITION:      2,
        MDY_DAY_POSITION:        3,
        MY_YEAR_POSITION:        1,
        MY_MONTH_POSITION:       2,
        MY_LABEL_YEAR_POSITION:  1,
        MY_LABEL_MONTH_POSITION: 2,
        MY_LABEL_YEAR_SUFFIX:    "\u5E74",
        MY_LABEL_MONTH_SUFFIX:   "",
        MONTHS_SHORT:            ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
        MONTHS_LONG:             ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
        WEEKDAYS_1CHAR:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
        WEEKDAYS_SHORT:          ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
        WEEKDAYS_MEDIUM:         ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>'],
        WEEKDAYS_LONG:           ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']
    };

    if (document.getElementById("cal1Container")) {
        YAHOO.cal.cal1 = new YAHOO.widget.Calendar("cal1","cal1Container", calOptions);
        YAHOO.cal.cal1.render();
        YAHOO.cal.cal1.hide();
        YAHOO.cal.cal1.selectEvent.subscribe(handleSelect, YAHOO.cal.cal1, true);
        YAHOO.util.Event.addListener(["date_y1","date_m1","date_d1"], "change", updateCal);
        YAHOO.util.Event.addListener("cal1_icon", "click", function(){ YAHOO.cal.cal1.show(); });
    }

    if (document.getElementById("cal2Container")) {
        YAHOO.cal.cal2 = new YAHOO.widget.Calendar("cal2","cal2Container", calOptions);
        YAHOO.cal.cal2.render();
        YAHOO.cal.cal2.hide();
        YAHOO.cal.cal2.selectEvent.subscribe(handleSelect, YAHOO.cal.cal2, true);
        YAHOO.util.Event.addListener(["date_y2","date_m2","date_d2"], "change", updateCal);
        YAHOO.util.Event.addListener("cal2_icon", "click", function(){ YAHOO.cal.cal2.show(); });
    }

    if (document.getElementById("cal3Container")) {
        YAHOO.cal.cal3 = new YAHOO.widget.Calendar("cal3","cal3Container", calOptions);
        YAHOO.cal.cal3.render();
        YAHOO.cal.cal3.hide();
        YAHOO.cal.cal3.selectEvent.subscribe(handleSelect, YAHOO.cal.cal3, true);
        YAHOO.util.Event.addListener(["date_y3","date_m3","date_d3"], "change", updateCal);
        YAHOO.util.Event.addListener("cal3_icon", "click", function(){ YAHOO.cal.cal3.show(); });
    }

    if (document.getElementById("cal4Container")) {
        YAHOO.cal.cal4 = new YAHOO.widget.Calendar("cal4","cal4Container", calOptions);
        YAHOO.cal.cal4.render();
        YAHOO.cal.cal4.hide();
        YAHOO.cal.cal4.selectEvent.subscribe(handleSelect, YAHOO.cal.cal4, true);
        YAHOO.util.Event.addListener(["date_y4","date_m4","date_d4"], "change", updateCal);
        YAHOO.util.Event.addListener("cal4_icon", "click", function(){ YAHOO.cal.cal4.show(); });
    }

    if (document.getElementById("cal5Container")) {
        YAHOO.cal.cal5 = new YAHOO.widget.Calendar("cal5","cal5Container", calOptions);
        YAHOO.cal.cal5.render();
        YAHOO.cal.cal5.hide();
        YAHOO.cal.cal5.selectEvent.subscribe(handleSelect, YAHOO.cal.cal5, true);
        YAHOO.util.Event.addListener(["date_y5","date_m5","date_d5"], "change", updateCal);
        YAHOO.util.Event.addListener("cal5_icon", "click", function(){ YAHOO.cal.cal5.show(); });
    }

    if (document.getElementById("cal6Container")) {
        YAHOO.cal.cal6 = new YAHOO.widget.Calendar("cal6","cal6Container", calOptions);
        YAHOO.cal.cal6.render();
        YAHOO.cal.cal6.hide();
        YAHOO.cal.cal6.selectEvent.subscribe(handleSelect, YAHOO.cal.cal6, true);
        YAHOO.util.Event.addListener(["date_y6","date_m6","date_d6"], "change", updateCal);
        YAHOO.util.Event.addListener("cal6_icon", "click", function(){ YAHOO.cal.cal6.show(); });
    }

    if (document.getElementById("cal7Container")) {
        YAHOO.cal.cal7 = new YAHOO.widget.Calendar("cal7","cal7Container", calOptions);
        YAHOO.cal.cal7.render();
        YAHOO.cal.cal7.hide();
        YAHOO.cal.cal7.selectEvent.subscribe(handleSelect, YAHOO.cal.cal7, true);
        YAHOO.util.Event.addListener(["date_y7","date_m7","date_d7"], "change", updateCal);
        YAHOO.util.Event.addListener("cal7_icon", "click", function(){ YAHOO.cal.cal7.show(); });
    }

    if (document.getElementById("cal8Container")) {
        YAHOO.cal.cal8 = new YAHOO.widget.Calendar("cal8","cal8Container", calOptions);
        YAHOO.cal.cal8.render();
        YAHOO.cal.cal8.hide();
        YAHOO.cal.cal8.selectEvent.subscribe(handleSelect, YAHOO.cal.cal8, true);
        YAHOO.util.Event.addListener(["date_y8","date_m8","date_d8"], "change", updateCal);
        YAHOO.util.Event.addListener("cal8_icon", "click", function(){ YAHOO.cal.cal8.show(); });
    }

    // 期間
    kikan_update(1);

    storeLoadedContent();
}

// カレンダー選択時に呼び出されるfunction
function handleSelect(type,args,obj) {
    var id = this.id.slice(-1);
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];

    var selMonth = document.getElementById("date_m" + id);
    var selDay   = document.getElementById("date_d" + id);
    var selYear  = document.getElementById("date_y" + id);

    selMonth.selectedIndex = month;
    selDay.selectedIndex = day;
    for (var y=0;y<selYear.options.length;y++) {
        if (selYear.options[y].value == year) {
            selYear.selectedIndex = y;
            break;
        }
    }

    // 日帰りの場合、期間開始の日付と終了の日付を連動
    if (id == 1 && document.getElementById("triptype").value == 1) {
        document.getElementById("date_y2").value = year;
        document.getElementById("date_m2").value = month.fillZero(2);
        document.getElementById("date_d2").value = day.fillZero(2);
    }

    kikan_update(id);
    this.hide();
}

// カレンダーのプルダウン変更時に呼び出されるfunction
function updateCal() {
    var id = this.id.slice(-1);
    var selMonth = document.getElementById("date_m" + id);
    var selDay   = document.getElementById("date_d" + id);
    var selYear  = document.getElementById("date_y" + id);

    var month = parseInt(selMonth.options[selMonth.selectedIndex].value, 10);
    var day = parseInt(selDay.options[selDay.selectedIndex].value, 10);
    var year = parseInt(selYear.options[selYear.selectedIndex].value, 10);

    // 日帰りの場合、期間開始の日付と終了の日付を連動
    if (id == 1 && document.getElementById("triptype").value == 1) {
        document.getElementById("date_y2").value = year;
        document.getElementById("date_m2").value = month.fillZero(2);
        document.getElementById("date_d2").value = day.fillZero(2);
    }

    if (! isNaN(month) && ! isNaN(day) && ! isNaN(year)) {
        var date = year + "/" + month + "/" + day;
        var cal = eval('YAHOO.cal.cal' + id);
        cal.select(date);
        cal.cfg.setProperty("pagedate", year + "/" + month);
        cal.render();
        kikan_update(id);
    }
}

// 区分radio選択処理
function kubun_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.value>=6 && el.checked) {
        document.getElementById('prepayment2_form').style.display = 'none';
        document.getElementById('prepayment1').checked = true;
    }
    else {
        document.getElementById('prepayment2_form').style.display = '';
    }
}

// 要件radio選択処理
function youken_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.value==2 && el.checked) {
        document.getElementById('form_youken1').style.display = 'none';
        document.getElementById('form_youken2').style.display = '';
    }
    else {
        document.getElementById('form_youken1').style.display = '';
        document.getElementById('form_youken2').style.display = 'none';
        if (document.getElementById("date_y3").value == '-' &&
            document.getElementById("date_m3").value == '-' &&
            document.getElementById("date_d3").value == '-' &&
            document.getElementById("date_y4").value == '-' &&
            document.getElementById("date_m4").value == '-' &&
            document.getElementById("date_d4").value == '-' ) {
            document.getElementById("date_y3").value = document.getElementById("date_y1").value;
            document.getElementById("date_m3").value = document.getElementById("date_m1").value;
            document.getElementById("date_d3").value = document.getElementById("date_d1").value;
            document.getElementById("date_y4").value = document.getElementById("date_y2").value;
            document.getElementById("date_m4").value = document.getElementById("date_m2").value;
            document.getElementById("date_d4").value = document.getElementById("date_d2").value;
        }
    }
}

// 演題発表radio選択処理
function presentation_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.value==1 && el.checked) {
        document.getElementById('check_presentation1').style.display = '';
    }
    else {
        document.getElementById('check_presentation1').style.display = 'none';
    }
}

// 交通手段checkbox選択処理
function transit_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    switch (el.value) {
        case "6":
            if (el.checked) {
                document.getElementById('attension_transit6').style.display = '';
            } else {
                document.getElementById('attension_transit6').style.display = 'none';
            }
            break;
        default:
            if (el.checked) {
                document.getElementById('attension_transit'+el.value).style.display = '';
            } else {
                document.getElementById('attension_transit'+el.value).style.display = 'none';
            }
            break;
    }
}
function transit_select_all() {
    transit_select('transit1');
    transit_select('transit2');
    transit_select('transit3');
    transit_select('transit4');
    transit_select('transit5');
    transit_select('transit6');
}

// 謝礼radio選択処理
function sharei_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.value==2 && el.checked) {
        document.getElementById('attension_sharei2').style.display = '';
        document.getElementById('attension_sharei3').style.display = 'none';
    }
    else if (el.value==3 && el.checked) {
        document.getElementById('attension_sharei2').style.display = 'none';
        document.getElementById('attension_sharei3').style.display = '';
    }
    else if (el.value==1) {
        document.getElementById('attension_sharei2').style.display = 'none';
        document.getElementById('attension_sharei3').style.display = 'none';
    }
}

// 前渡金radio選択処理
function maewatashi_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.value==2 && el.checked) {
        document.getElementById('prepayment_form').style.display = '';
        document.getElementById('prepayment_attension').style.display = '';
    }
    else {
        document.getElementById('prepayment_form').style.display = 'none';
        document.getElementById('prepayment_attension').style.display = 'none';
    }
}

// 前渡チケットradio選択処理
function preticket_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.value==2 && el.checked) {
        document.getElementById('preticket2_form').style.display = '';
        document.getElementById('preticket2_attension').style.display = '';
        preticket2_select('preticket21');
        preticket2_select('preticket22');
    }
    else {
        document.getElementById('preticket2_form').style.display = 'none';
        document.getElementById('preticket2_attension').style.display = 'none';
        document.getElementById('preticket21_form').style.display = 'none';
        document.getElementById('preticket22_form').style.display = 'none';
    }
}

// 前渡チケット種類select選択処理
function preticket2_select(id) {
    var el = (typeof(id)=="string") ? document.getElementById(id) : this;
    if (el.checked) {
        document.getElementById('preticket2'+el.value+'_form').style.display = '';
    }
    else {
        document.getElementById('preticket2'+el.value+'_form').style.display = 'none';
    }
}

// 期間更新
function kikan_update(id) {
    if (id == 1 || id == 2) {
        if (document.getElementById("kikan")) {
            var kikan_value = compareDate(
                document.getElementById("date_y2").value,
                document.getElementById("date_m2").value,
                document.getElementById("date_d2").value,
                document.getElementById("date_y1").value,
                document.getElementById("date_m1").value,
                document.getElementById("date_d1").value
            );
            if (!isNaN(kikan_value)) {
                document.getElementById("kikan").value = kikan_value;
            }
        }
    }
}

// 出張届コピー
function trip1copy() {
    var t = trip1[document.getElementById('trip1select').value];
    document.apply.pref.value = t.pref;
    document.apply.city.value = t.city;
    document.apply.place.value = t.place;
    document.apply.gakkai_name.value = t.gakkai_name;
    document.apply.gakkai_organizer.value = t.gakkai_organizer;
    document.apply.date_y3.value = t.date_y3;
    document.apply.date_m3.value = t.date_m3;
    document.apply.date_d3.value = t.date_d3;
    document.apply.date_y4.value = t.date_y4;
    document.apply.date_m4.value = t.date_m4;
    document.apply.date_d4.value = t.date_d4;
    document.apply.presentation_titie.value = t.presentation_titie;
    document.apply.gakkai_igai.value = t.gakkai_igai;

    for(i=0; i<document.apply.elements['youken[]'].length; i++) {
        if(document.apply.elements['youken[]'][i].value == t.youken) {
            document.apply.elements['youken[]'][i].checked = true;
        }
    }
    youken_select('youken'+t.youken);

    if (t.presentation != null && t.presentation != "") {
        for(i=0; i<document.apply.elements['presentation[]'].length; i++) {
            if(document.apply.elements['presentation[]'][i].value == t.presentation) {
                document.apply.elements['presentation[]'][i].checked = true;
            }
        }
        presentation_select('presentation'+t.presentation);
    }

    for(i=0; i<document.apply.elements['act[]'].length; i++) {
        if(document.apply.elements['act[]'][i].value == t.act) {
            document.apply.elements['act[]'][i].checked = true;
        }
    }

    // 交通手段
    // 一旦チェックをリセット
    for(i=0; i<document.getElementsByClassName('checkbox_transit').length; i++) {
        document.getElementsByClassName('checkbox_transit')[i].checked = false;
    }
    // チェックを入れる
    for(i=0; i<t.transit.length; i++) {
        document.getElementById('transit'+t.transit[i]).checked = true;
    }
    transit_select_all();
    document.apply.reason_transit6.value = t.reason_transit6;
}

// 2つの日付の差（何日間あるか）を求める
// http://www.hoge256.net/2007/08/64.html
function compareDate(year1, month1, day1, year2, month2, day2) {
    var dt1 = new Date(year1, month1 - 1, day1);
    var dt2 = new Date(year2, month2 - 1, day2);
    var diff = dt1 - dt2;
    var diffDay = diff / 86400000;//1日は86400000ミリ秒
    return diffDay+1;
}

// 入力値チェック
function InputCheck() {

    // 種別
    var kubun = false;
    for (i=0; i<document.getElementsByName('kubun[]').length; i++) {
        if (document.getElementsByName('kubun[]')[i].checked) {
            kubun = true;
            break;
        }
    }
    if (!kubun) {
        alert('種別が選択されていません。');
        return false;
    }

    // 期間
    if (document.getElementById("date_y1").value == '-' ||
        document.getElementById("date_m1").value == '-' ||
        document.getElementById("date_d1").value == '-' ||
        document.getElementById("date_y2").value == '-' ||
        document.getElementById("date_m2").value == '-' ||
        document.getElementById("date_d2").value == '-' ) {
        alert('期間が選択されていません。');
        return false;
    }
    switch (document.getElementById('triptype').value) {
        case '1':
            if (document.getElementById('kikan').value != 1) {
                alert('期間が日帰りになっていません。');
                return false;
            }
            break;
        case '2':
            if (document.getElementById('kikan').value < 2 || document.getElementById('kikan').value > 4) {
                alert('期間が4日以内になっていません。');
                return false;
            }
            break;
        case '3':
            if (document.getElementById('kikan').value < 5) {
                alert('期間が5日以上になっていません。');
                return false;
            }
            break;
    }

    // 出張先
    if (!document.getElementById('pref').value) {
        alert('都道府県が選択されていません。');
        return false;
    }
    if (!document.getElementById('city').value) {
        alert('市区町村が入力されていません。');
        return false;
    }
    if (!document.getElementById('place').value) {
        alert('出張場所が入力されていません。');
        return false;
    }

    // 要件
    var youken = false;
    var youken_value = 0;
    for (i=0; i<document.getElementsByName('youken[]').length; i++) {
        if (document.getElementsByName('youken[]')[i].checked) {
            youken_value = document.getElementsByName('youken[]')[i].value;
            youken = true;
            break;
        }
    }
    if (!youken) {
        alert('要件が選択されていません。');
        return false;
    }

    // 要件：学会
    if (youken_value == 1) {
        // 学会名
        if (!document.getElementById('gakkai_name').value) {
            alert('(学会・講演会等)名称が入力されていません。');
            return false;
        }
        // 学会期間
        if (document.getElementById("date_y3").value == '-' ||
            document.getElementById("date_m3").value == '-' ||
            document.getElementById("date_d3").value == '-' ||
            document.getElementById("date_y4").value == '-' ||
            document.getElementById("date_m4").value == '-' ||
            document.getElementById("date_d4").value == '-' ) {
            alert('(学会・講演会等)期間が選択されていません。');
            return false;
        }
        if (compareDate(
            document.getElementById('date_y4').value,
            document.getElementById('date_m4').value,
            document.getElementById('date_d4').value,
            document.getElementById('date_y3').value,
            document.getElementById('date_m3').value,
            document.getElementById('date_d3').value
        ) < 1) {
            alert('(学会・講演会等)期間が正しく選択されていません。');
            return false;
        }
        // 演題発表
        var presentation = false;
        var presentation_value = 0;
        for (i=0; i<document.getElementsByName('presentation[]').length; i++) {
            if (document.getElementsByName('presentation[]')[i].checked) {
                presentation_value = document.getElementsByName('presentation[]')[i].value;
                presentation = true;
                break;
            }
        }
        if (!presentation) {
            alert('演題発表が選択されていません。');
            return false;
        }
        if (presentation_value == 1) {
            // 主演・共演
            var act = false;
            for (i=0; i<document.getElementsByName('act[]').length; i++) {
                if (document.getElementsByName('act[]')[i].checked) {
                    act = true;
                    break;
                }
            }
            if (!act) {
                alert('主演・共演が選択されていません。');
                return false;
            }
            // 演題名
            if (!document.getElementById('presentation_titie').value) {
                alert('演題名が入力されていません。');
                return false;
            }
        }

    }
    // 要件：学会以外
    else if (youken_value == 2) {
        if (!document.getElementById('gakkai_igai').value) {
            alert('見学・会議等が入力されていません。');
            return false;
        }
    }

    // 交通手段
    if (document.getElementById('transit6').checked) {
        if (!document.getElementById('reason_transit6').value) {
            alert('私有車利用がやむを得ない理由が入力されていません。');
            return false;
        }
    }

    // 謝礼
    var sharei = false;
    for (i=0; i<document.getElementsByName('sharei[]').length; i++) {
        if (document.getElementsByName('sharei[]')[i].checked) {
            sharei = true;
            break;
        }
    }
    if (!sharei) {
        alert('謝礼が選択されていません。');
        return false;
    }

    // 前渡金
    var prepayment = false;
    var prepayment_value = 0;
    for (i=0; i<document.getElementsByName('prepayment[]').length; i++) {
        if (document.getElementsByName('prepayment[]')[i].checked) {
            prepayment_value = document.getElementsByName('prepayment[]')[i].value;
            prepayment = true;
            break;
        }
    }
    if (!prepayment) {
        alert('前渡金が選択されていません。');
        return false;
    }
    if (prepayment_value == 2) {
        // 申請金額
        if (!document.getElementById('prepayment_value').value) {
            alert('申請金額が入力されていません。');
            return false;
        }
        if (document.getElementById('prepayment_value').value.match(/[^0-9]+/)) {
            alert('申請金額に数字以外が入力されています。');
            return false;
        }
    }

    // 前渡チケット
    var preticket = false;
    var preticket_value = 0;
    for (i=0; i<document.getElementsByName('preticket[]').length; i++) {
        if (document.getElementsByName('preticket[]')[i].checked) {
            preticket_value = document.getElementsByName('preticket[]')[i].value;
            preticket = true;
            break;
        }
    }
    if (!preticket) {
        alert('前渡しチケットが選択されていません。');
        return false;
    }
    if (preticket_value == 2) {
        if (!document.getElementById('preticket21').checked && !document.getElementById('preticket22').checked) {
            alert('前渡しチケットの種類がチェックされていません。');
            return false;
        }
        // 新幹線
        if (document.getElementById('preticket21').checked) {
            if (!document.apply.station1_from.value && !document.apply.station1_to.value &&
                !document.apply.station2_from.value && !document.apply.station2_to.value) {
                alert('発着駅が選択されていません。');
                return false;
            }
            if (!document.apply.station1_from.value &&  document.apply.station1_to.value ||
                 document.apply.station1_from.value && !document.apply.station1_to.value) {
                alert('行きの発着駅が選択されていません。');
                return false;
            }
            if (document.apply.station1_from.value && document.apply.station1_from.value == document.apply.station1_to.value) {
                alert('行きの発着駅が同じです。');
                return false;
            }
            if (document.apply.station1_from.value && (
                document.getElementById("date_y5").value == '--' ||
                document.getElementById("date_m5").value == '--' ||
                document.getElementById("date_d5").value == '--' ||
                document.getElementById("date_h5").value == '--' ||
                document.getElementById("date_i5").value == '--' )) {
                alert('行きの到着希望時刻が選択されていません。');
                return false;
            }
            if (!document.apply.station2_from.value &&  document.apply.station2_to.value ||
                 document.apply.station2_from.value && !document.apply.station2_to.value) {
                alert('帰りの発着駅が選択されていません。');
                return false;
            }
            if (document.apply.station2_from.value && document.apply.station2_from.value == document.apply.station2_to.value) {
                alert('帰りの発着駅が同じです。');
                return false;
            }
            if (document.apply.station2_from.value && (
                document.getElementById("date_y6").value == '--' ||
                document.getElementById("date_m6").value == '--' ||
                document.getElementById("date_d6").value == '--' ||
                document.getElementById("date_h6").value == '--' ||
                document.getElementById("date_i6").value == '--' )) {
                alert('帰りの到着希望時刻が選択されていません。');
                return false;
            }
        }
        // 飛行機
        if (document.getElementById('preticket22').checked) {
            if (!document.apply.airport1_from.value && !document.apply.airport1_to.value &&
                !document.apply.airport2_from.value && !document.apply.airport2_to.value) {
                alert('発着空港が選択されていません。');
                return false;
            }
            if (!document.apply.airport1_from.value &&  document.apply.airport1_to.value ||
                 document.apply.airport1_from.value && !document.apply.airport1_to.value) {
                alert('行きの発着空港が選択されていません。');
                return false;
            }
            if (document.apply.airport1_from.value && (
                document.getElementById("date_y7").value == '--' ||
                document.getElementById("date_m7").value == '--' ||
                document.getElementById("date_d7").value == '--' ||
                document.getElementById("date_h7").value == '--' ||
                document.getElementById("date_i7").value == '--' )) {
                alert('行きの到着希望時刻が選択されていません。');
                return false;
            }
            if (document.apply.airport1_from.value && document.apply.airport1_from.value == document.apply.airport1_to.value) {
                alert('行きの発着空港が同じです。');
                return false;
            }
            if (!document.apply.airport2_from.value &&  document.apply.airport2_to.value ||
                 document.apply.airport2_from.value && !document.apply.airport2_to.value) {
                alert('帰りの発着空港が選択されていません。');
                return false;
            }
            if (document.apply.airport2_from.value && document.apply.airport2_from.value == document.apply.airport2_to.value) {
                alert('帰りの発着空港が同じです。');
                return false;
            }
            if (document.apply.airport2_from.value && (
                document.getElementById("date_y8").value == '--' ||
                document.getElementById("date_m8").value == '--' ||
                document.getElementById("date_d8").value == '--' ||
                document.getElementById("date_h8").value == '--' ||
                document.getElementById("date_i8").value == '--' )) {
                alert('帰りの到着希望時刻が選択されていません。');
                return false;
            }
        }
    }
    if (!confirm('内容の確認してください。「OK」を押すと登録されます。')) {
        return false;
    }

    return true;
}

// "先頭を0で埋めて桁をそろえる"
// http://d.hatena.ne.jp/higeorange/20080515/1210817629
Number.prototype.fillZero = function(n) {
    return Array((n+1) - this.toString().split('').length).join('0') + this;
}
