/****************************************************************
 * numberOnly()
 * 数字のみ入力許可する
 * 但し、0より大きい値
 *
 * 引　数：obj 要素オブジェクト
 * 戻り値：なし
 * 
 * 2012/06/14 追加
 ****************************************************************/
function numberOnly(obj)
{
  var value = obj.value;
  value = value.replace(/[^0-9]/g, '');

  var number = Number(value);
  if (number == 0) {
    value = '';
  } else {
    value = number.toString();
  }
  obj.value = value;
}