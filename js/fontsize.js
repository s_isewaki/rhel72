// fontsize
var vNum = navigator.appVersion.charAt(0);
var bName = navigator.appName.charAt(0);
var bVer = navigator.userAgent;

document.writeln("<STYLE TYPE='text/css'><!--");
if(navigator.appVersion.indexOf("Mac") > -1){
	if( bName == "M"){
		// MAC IE
		document.writeln(".j9{font-size:60%; line-height:120%}");
		document.writeln(".j10n{font-Size:70%; line-height:120%}");
		document.writeln(".j10{font-Size:70%; line-height:130%}");
		document.writeln(".j10w{font-Size:70%; line-height:140%}");
		document.writeln(".j12n{font-Size:80%; line-height:125%}");
		document.writeln(".j12{font-Size:80%; line-height:135%}");
		document.writeln(".j12w{font-Size:80%; line-height:145%}");
		document.writeln(".j14n{font-Size:100%; line-height:110%}");
		document.writeln(".j14{font-Size:100%; line-height:120%}");
		document.writeln(".j14w{font-Size:100%; line-height:125%}");
		document.writeln(".j18{font-Size:120%; line-height:115%}");
		document.writeln(".j9f{font-size:9px; line-height:14px}");
		document.writeln(".j10f{font-Size:10px; line-height:13px}");
		document.writeln(".j12f{font-Size:12px; line-height:15px}");		
	}
	else{
		if( vNum < 5 ){
			// MAC NETSCAPE 4.x
			document.writeln(".j9{font-size:80%; line-height:100%}");
			document.writeln(".j10n{font-Size:90%; line-height:110%}");
			document.writeln(".j10{font-Size:90%; line-height:120%}");
			document.writeln(".j10w{font-Size:90%; line-height:125%}");
			document.writeln(".j12n{font-Size:100%; line-height:135%}");
			document.writeln(".j12{font-Size:100%; line-height:145%}");
			document.writeln(".j12w{font-Size:100%; line-height:150%}");
			document.writeln(".j14n{font-Size:120%; line-height:145%}");
			document.writeln(".j14{font-Size:120%; line-height:155%}");
			document.writeln(".j14w{font-Size:120%; line-height:165%}");
			document.writeln(".j18{font-Size:150%; line-height:200%}");
			document.writeln(".j9f{font-size:9px; line-height:14px}");
			document.writeln(".j10f{font-Size:10px; line-height:13px}"); 
			document.writeln(".j12f{font-Size:12px; line-height:15px}");			
		}
		else{
			if( bVer.indexOf("Netscape6", 0) > -1){
				// MAC NETSCAPE 6.x~
				document.writeln(".j9{font-size:70%; line-height:120%}");
				document.writeln(".j10n{font-Size:70%; line-height:155%}");
				document.writeln(".j10{font-Size:70%; line-height:170%}");
				document.writeln(".j10w{font-Size:70%; line-height:180%}");
				document.writeln(".j12n{font-Size:90%; line-height:125%}");
				document.writeln(".j12{font-Size:90%; line-height:135%}");
				document.writeln(".j12w{font-Size:90%; line-height:145%}");
				document.writeln(".j14n{font-Size:100%; line-height:130%}");
				document.writeln(".j14{font-Size:100%; line-height:140%}");
				document.writeln(".j14w{font-Size:100%; line-height:145%}");
				document.writeln(".j18{font-Size:130%; line-height:120%}");
				document.writeln(".j9f{font-size:9px; line-height:14px}");
				document.writeln(".j10f{font-Size:10px; line-height:13px}"); 
				document.writeln(".j12f{font-Size:12px; line-height:15px}");		
			}
			else{
				// MAC NETSCAPE 7.x~
				document.writeln(".j9{font-size:70%; line-height:110%}");
				document.writeln(".j10n{font-Size:75%; line-height:120%}");
				document.writeln(".j10{font-Size:75%; line-height:130%}");
				document.writeln(".j10w{font-Size:75%; line-height:140%}");
				document.writeln(".j12n{font-Size:90%; line-height:130%}");
				document.writeln(".j12{font-Size:90%; line-height:140%}");
				document.writeln(".j12w{font-Size:90%; line-height:145%}");
				document.writeln(".j14n{font-Size:100%; line-height:125%}");
				document.writeln(".j14{font-Size:100%; line-height:135%}");
				document.writeln(".j14w{font-Size:100%; line-height:140%}");
				document.writeln(".j18{font-Size:130%; line-height:130%}");
			}
		}
	 }
}
else{
	if( bName == "M"){
		// WIN IE
		document.writeln(".j9{font-size:60%; line-height:120%}");
		document.writeln(".j10n{font-Size:70%; line-height:120%}");
		document.writeln(".j10{font-Size:70%; line-height:124%}");
		document.writeln(".j10w{font-Size:70%; line-height:130%}");
		document.writeln(".j12n{font-Size:80%; line-height:130%;}");
		document.writeln(".j12{font-Size:80%; line-height:140%;}");
		document.writeln(".j12w{font-Size:80%; line-height:150%;}");
		document.writeln(".j14n{font-Size:90%; line-height:130%; letter-spacing:1px}");
		document.writeln(".j14{font-Size:90%; line-height:140%; letter-spacing:1px}");
		document.writeln(".j14w{font-Size:90%; line-height:150%; letter-spacing:1px}");
		document.writeln(".j18{font-Size:105%; line-height:140%; letter-spacing:2px}");
		document.writeln(".j9f{font-size:10px; line-height:12px}");
		document.writeln(".j10f{font-Size:11px; line-height:13px}");
		document.writeln(".j12f{font-Size:12px; line-height:16px}");
		
	}
	else{
		if( vNum < 5 ){
			// WIN NETSCAPE 4.x
			document.writeln(".j9{font-size:90%; line-height:120%;}");
			document.writeln(".j10n{font-size:95%; line-height:100%;}");
			document.writeln(".j10{font-size:95%; line-height:110%;}");
			document.writeln(".j10w{font-size:95%; line-height:120%;}");
			document.writeln(".j12n{font-Size:110%; line-height:130%;}");
			document.writeln(".j12{font-Size:110%; line-height:135%;}");
			document.writeln(".j12w{font-Size:110%; line-height:145%;}");
			document.writeln(".j14n{font-Size:130%; line-height:145%;}");
			document.writeln(".j14{font-Size:130%; line-height:150%;}");
			document.writeln(".j14w{font-Size:130%; line-height:155%;}");
			document.writeln(".j18{font-Size:155%; line-height:190%}");
			document.writeln(".j9f{font-size:11px; line-height:12px}");
			document.writeln(".j10f{font-Size:11px; line-height:15px}");
			document.writeln(".j12f{font-Size:12px; line-height:16px}");			
		}
		else{
			if( bVer.indexOf("Netscape6", 0) > -1){
				// WIN NETSCAPE 6.x~
				document.writeln(".j9{font-size:60%; line-height:110%}");
				document.writeln(".j10n{font-Size:70%; line-height:120%}");
				document.writeln(".j10{font-Size:70%; line-height:130%}");
				document.writeln(".j10w{font-Size:70%; line-height:140%}");
				document.writeln(".j12n{font-Size:80%; line-height:125%}");
				document.writeln(".j12{font-Size:80%; line-height:135%}");
				document.writeln(".j12w{font-Size:80%; line-height:145%}");
				document.writeln(".j14n{font-Size:90%; line-height:150%}");
				document.writeln(".j14{font-Size:90%; line-height:160%}");
				document.writeln(".j14w{font-Size:90%; line-height:170%}");
				document.writeln(".j18{font-Size:110%; line-height:145%}");
			}
			else{
				// WIN NETSCAPE 7.x~
				document.writeln(".j9{font-size:60%; line-height:115%}");
				document.writeln(".j10n{font-Size:70%; line-height:115%}");
				document.writeln(".j10{font-Size:70%; line-height:125%}");
				document.writeln(".j10w{font-Size:70%; line-height:135%}");
				document.writeln(".j12n{font-Size:80%; line-height:125%}");
				document.writeln(".j12{font-Size:80%; line-height:135%;}");
				document.writeln(".j12w{font-Size:80%; line-height:145%}");
				document.writeln(".j14n{font-Size:90%; line-height:135%; letter-spacing:1px}");
				document.writeln(".j14{font-Size:90%; line-height:145%; letter-spacing:1px}");
				document.writeln(".j14w{font-Size:90%; line-height:150%; letter-spacing:1px}");
				document.writeln(".j18{font-Size:110%; line-height:130%; letter-spacing:2px}");
				document.writeln(".j9f{font-size:10px; line-height:12px}");
				document.writeln(".j10f{font-Size:11px; line-height:15px; letter-spacing:1px}");
				document.writeln(".j12f{font-Size:12px; line-height:16px; letter-spacing:1px}");				
			}
		}
	}
}
 document.writeln("--></STYLE>");