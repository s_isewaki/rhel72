// textareaの上下高さを入力行数にあわせて自動リサイズするスクリプト
// summary_tmpl_read.php から流用
var isWindows = (navigator.userAgent.indexOf("Win") != -1 ); // Windows系かどうか
//var isMSIE = /*@cc_on!@*/false; //IE系かどうか
function adjustTextAreaHeight(id, rowsize, letDummyShowing){
	var ta = document.getElementById(id);
	if (!ta) return;
	if (isWindows) ta.style.letterSpacing = "1px"; // Windowsの場合は、文字間隔を開ける
	if (ta.value==""){
		ta.rows = rowsize;
		ta.style.height = null;
		if(rowsize <= 1) ta.style.height = "22px";
		return;
	}
	var dmy = document.getElementById("textarea_dummy");
	if (ta.style.fontSize) dmy.style.fontSize = ta.style.fontSize;
	else ta.style.fontSize = dmy.style.fontSize;
	if (ta.style.fontFamily) dmy.style.fontFamily = ta.style.fontFamily;
	else ta.style.fontFamily = dmy.style.fontFamily;
	dmy.style.width = ta.offsetWidth+"px";
	dmy.value = "test";
	dmy.style.height = null;
	dmy.rows = rowsize;
	dmy.style.display = "";
	var h = dmy.offsetHeight;
	dmy.style.height = "10px";
	dmy.value = ta.value;

//	var margin = isMSIE ? 14 : 2;// IE系の場合は、14px程度プラスして、次行も表示させる。それ以外は下を2pxあける。
	var margin = 14;
	for(var i=0; i<30; i++){
		dmy.scrollTop = 1000;
		if (dmy.scrollTop == 0) break;
		dmy.style.height = (dmy.offsetHeight + dmy.scrollTop + margin)+"px";
	}
	if (h < parseInt(dmy.style.height,10)) h = parseInt(dmy.style.height,10);
	if (h < 22) h = 22;
	ta.style.height = h+"px";
	if (!letDummyShowing) dmy.style.display = "none";
}
