function reload_page() {
	//document.approve.action="cl_application_approve_list.php?session=<?=$session?>&mode=search";
	document.approve.mode.value="search";
	document.approve.action="cl_application_approve_list.php";
	document.approve.submit();
}

// 2012/11/29 Yamagawa add(s)
function reload_page_all_approve() {
	document.approve.mode.value="search";
	document.approve.action="cl_workflow_all_approve_list.php";
	document.approve.submit();
}
// 2012/11/29 Yamagawa add(e)

function initPage() {

	cateOnChange();

}
function approve_regist(idx) {

	var msg = "";

	if(idx == '1') {
		msg = '「承認」';
	} else if(idx == '2') {
		msg = '「否認」';
	} else if(idx == '3') {
		msg = '「差戻し」';
	}

	if (document.approve.elements['approve_chk[]'] == undefined) {
		alert(msg + 'するデータを選択してくだい。');
		return;
	}

	if (document.approve.elements['approve_chk[]'].length == undefined) {
		if (!document.approve.elements['approve_chk[]'].checked) {
			alert(msg + 'するデータを選択してくだい。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.approve.elements['approve_chk[]'].length; i < j; i++) {
			if (document.approve.elements['approve_chk[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert(msg + 'するデータを選択してくだい。');
			return;
		}
	}

	if (confirm('選択された申請書の' + msg + 'をします。よろしいですか？')) {
		//document.approve.action="cl_application_approve_list_regist.php?session=<?=$session?>&approve=" + idx;
		document.approve.approve.value=idx;
		document.approve.action="cl_application_approve_list_regist.php";
		document.approve.submit();
	}
}
function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}


function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}


function approve_search() {

	// ページ番号が存在する場合、初期化
	if (document.approve.page) {
		document.approve.page.value="";
	}
	//document.approve.action="cl_application_approve_list.php?session=<?=$session?>&mode=search";
	document.approve.mode.value="search";
	document.approve.action="cl_application_approve_list.php";
	document.approve.submit();

}

/**
 * 管理者用 承認一覧
 */
function all_approve_search() {

	// ページ番号が存在する場合、初期化
	if (document.approve.page) {
		document.approve.page.value="";
	}
	//document.approve.action="cl_application_approve_list.php?session=<?=$session?>&mode=search";

	document.approve.mode.value="search";
	document.approve.action="cl_workflow_all_approve_list.php";
	document.approve.submit();

}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}
function show_sub_window(url) {
//	var h = window.screen.availHeight;
//	var w = window.screen.availWidth;
	var h = '700';
	var w = '1024';
	var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
	window.open(url, 'approvewin',option);
}

function clearApplyDate() {
	document.approve.date_y1.selectedIndex = 0;
	document.approve.date_m1.selectedIndex = 0;
	document.approve.date_d1.selectedIndex = 0;
	document.approve.date_y2.selectedIndex = 0;
	document.approve.date_m2.selectedIndex = 0;
	document.approve.date_d2.selectedIndex = 0;
}

function setEventHandler() {
	YAHOO.ui.calendar.cal3.selectEvent.subscribe(clearApplyDate);
	YAHOO.ui.calendar.cal4.selectEvent.subscribe(clearApplyDate);
	YAHOO.util.Event.addListener(['date_m3', 'date_d3', 'date_y3', 'date_m4', 'date_d4', 'date_y4'], 'change', clearApplyDate);
}
