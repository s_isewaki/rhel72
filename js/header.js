var enablepersist="on"
var collapseprevious="no"

if (document.getElementById){
document.write('<style type="text/css">')
document.write('.switchcontent{display:none;}')
document.write('</style>')
}

function getElementbyClass(classname) {
	ccollect = new Array();
	var inc = 0;
	var alltags = document.getElementsByTagName("*");
	for (i = 0; i < alltags.length; i++) {
		if (alltags[i].className == classname) {
			ccollect[inc++] = alltags[i];
		}
	}
}

function contractcontent(omit){
var inc=0
while (ccollect[inc]){
if (ccollect[inc].id!=omit)
ccollect[inc].style.display="none"
inc++
}
}

function expandcontent(cid) {
	if (typeof ccollect != 'undefined') {
		if (collapseprevious == 'yes') {
			contractcontent(cid);
		}
		var c = document.getElementById(cid);
		var c_display = c.style.display;
		c_display = (c_display != 'block') ? 'block' : 'none';
		c.style.display = c_display;
		var down_img = document.getElementById(cid + '_d');
		if (down_img) {
			down_img.style.display = (c_display == 'block') ? 'none' : '';
		}
		var up_img = document.getElementById(cid + '_u');
		if (up_img) {
			up_img.style.display = (c_display == 'block') ? '' : 'none';
		}
	}
}

function revivecontent() {
	contractcontent('omitnothing');
	var selectedItem = getselectedItem();
	var selectedComponents = selectedItem.split('|');
	for (var i = 0, j = selectedComponents.length; i < j; i++) {
		var components = document.getElementById(selectedComponents[i]);
		if (components) {
			components.style.display = 'block';
		}
		var down_img = document.getElementById(selectedComponents[i] + '_d');
		if (down_img) {
			down_img.style.display = 'none';
		}
		var up_img = document.getElementById(selectedComponents[i] + '_u');
		if (up_img) {
			up_img.style.display = '';
		}
	}
}

function get_cookie(Name) {
var search = Name + "="
var returnvalue = "";
if (document.cookie.length > 0) {
offset = document.cookie.indexOf(search)
if (offset != -1) {
offset += search.length
end = document.cookie.indexOf(";", offset);
if (end == -1) end = document.cookie.length;
returnvalue=unescape(document.cookie.substring(offset, end))
}
}
return returnvalue;
}

function getselectedItem(){
if (get_cookie(window.location.pathname) != ""){
selectedItem=get_cookie(window.location.pathname)
return selectedItem
}
else
return ""
}

function saveswitchstate(){
var inc=0, selectedItem=""
while (ccollect[inc]){
if (ccollect[inc].style.display=="block")
selectedItem+=ccollect[inc].id+"|"
inc++
}

document.cookie=window.location.pathname+"="+selectedItem
}

function do_onload(){
getElementbyClass("switchcontent")
if (enablepersist=="on" && typeof ccollect!="undefined")
revivecontent()
}


if (window.addEventListener)
window.addEventListener("load", do_onload, false)
else if (window.attachEvent)
window.attachEvent("onload", do_onload)
else if (document.getElementById)
window.onload=do_onload

if (enablepersist=="on" && document.getElementById)
window.onunload=saveswitchstate