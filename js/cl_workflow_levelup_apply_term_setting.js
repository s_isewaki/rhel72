function regist(){

	if (!regist_validate()) {
		return;
	}

	if (!confirm('レベルアップ申請期間を更新します。よろしいですか？')) {
		return;
	}

	document.wkfw.regist_flg.value = 'true';
	document.wkfw.submit();
}

function regist_validate(){
	var strBkColor = "FFB6C1";
	var strErr = new Array();
	var err_idx=0;

	var bolfirst = false;
	var bolsecond = false;

	var first_from = '00' + document.getElementById('date_first_from_m').value + document.getElementById('date_first_from_d').value;
	var second_from = '00' + document.getElementById('date_second_from_m').value + document.getElementById('date_second_from_d').value;
	var first_to = '';
	var second_to = '';
	if (document.getElementById('date_first_from_m').value > document.getElementById('date_first_to_m').value) {
		first_to = '01' + document.getElementById('date_first_to_m').value + document.getElementById('date_first_to_d').value;
	} else {
		first_to = '00' + document.getElementById('date_first_to_m').value + document.getElementById('date_first_to_d').value;
	}
	if (document.getElementById('date_second_from_m').value > document.getElementById('date_second_to_m').value) {
		second_to = '01' + document.getElementById('date_second_to_m').value + document.getElementById('date_second_to_d').value;
	} else {
		second_to = '00' + document.getElementById('date_second_to_m').value + document.getElementById('date_second_to_d').value;
	}

	if (
			document.getElementById('date_first_from_m').value != '-'
		||	document.getElementById('date_first_from_d').value != '-'
		||	document.getElementById('date_first_to_m').value != '-'
		||	document.getElementById('date_first_to_d').value != '-'
	){
		bolfirst = true;
	}
	if (
			document.getElementById('date_second_from_m').value != '-'
		||	document.getElementById('date_second_from_d').value != '-'
		||	document.getElementById('date_second_to_m').value != '-'
		||	document.getElementById('date_second_to_d').value != '-'
	){
		bolsecond = true;
	}

	// 全項目の背景色をリセット
	document.getElementById('date_first_from_m').style.backgroundColor='';
	document.getElementById('date_first_from_d').style.backgroundColor='';
	document.getElementById('date_first_to_m').style.backgroundColor='';
	document.getElementById('date_first_to_d').style.backgroundColor='';
	document.getElementById('date_second_from_m').style.backgroundColor='';
	document.getElementById('date_second_from_d').style.backgroundColor='';
	document.getElementById('date_second_to_m').style.backgroundColor='';
	document.getElementById('date_second_to_d').style.backgroundColor='';

	// 上期のいずれかが入力されている場合
	if (bolfirst) {
		// 上期（From・月）未入力時はエラー
		if (document.getElementById('date_first_from_m').value == '-') {
			strErr[err_idx++] = "上期（From・月）が未入力です。";
			document.getElementById('date_first_from_m').style.backgroundColor=strBkColor;
		}
		// 上期（From・日）未入力時はエラー
		if (document.getElementById('date_first_from_d').value == '-') {
			strErr[err_idx++] = "上期（From・日）が未入力です。";
			document.getElementById('date_first_from_d').style.backgroundColor=strBkColor;
		}
		// 上期（To・月）未入力時はエラー
		if (document.getElementById('date_first_to_m').value == '-') {
			strErr[err_idx++] = "上期（To・月）が未入力です。";
			document.getElementById('date_first_to_m').style.backgroundColor=strBkColor;
		}
		// 上期（To・日）未入力時はエラー
		if (document.getElementById('date_first_to_d').value == '-') {
			strErr[err_idx++] = "上期（To・日）が未入力です。";
			document.getElementById('date_first_to_d').style.backgroundColor=strBkColor;
		}
	}

	// 下期のいずれかが入力されている場合
	if (bolsecond) {
		// 下期（From・月）未入力時はエラー
		if (document.getElementById('date_second_from_m').value == '-') {
			strErr[err_idx++] = "上期（From・月）が未入力です。";
			document.getElementById('date_second_from_m').style.backgroundColor=strBkColor;
		}
		// 下期（From・日）未入力時はエラー
		if (document.getElementById('date_second_from_d').value == '-') {
			strErr[err_idx++] = "上期（From・日）が未入力です。";
			document.getElementById('date_second_from_d').style.backgroundColor=strBkColor;
		}
		// 下期（To・月）未入力時はエラー
		if (document.getElementById('date_second_to_m').value == '-') {
			strErr[err_idx++] = "上期（To・月）が未入力です。";
			document.getElementById('date_second_to_m').style.backgroundColor=strBkColor;
		}
		// 下期（To・日）未入力時はエラー
		if (document.getElementById('date_second_to_d').value == '-') {
			strErr[err_idx++] = "上期（To・日）が未入力です。";
			document.getElementById('date_second_to_d').style.backgroundColor=strBkColor;
		}
	}

	if (bolfirst && bolsecond && strErr.length == 0) {
		// 範囲が重複した場合、エラー
		if (
			(
					first_from <= second_from
				&&	second_from <= first_to
			) || (
					first_from <= '01' + second_from.substr(2, 4)
				&&	'01' + second_from.substr(2, 4) <= first_to
			)
		) {
			strErr[err_idx++] = "下期Fromが上期の期間内です。";
			document.getElementById('date_second_from_m').style.backgroundColor=strBkColor;
			document.getElementById('date_second_from_d').style.backgroundColor=strBkColor;
		}
		if (
				first_from <= second_to
			&&	second_to <= first_to
		) {
			strErr[err_idx++] = "下期Toが上期の期間内です。";
			document.getElementById('date_second_to_m').style.backgroundColor=strBkColor;
			document.getElementById('date_second_to_d').style.backgroundColor=strBkColor;
		}
	}

	var msg="";
	for (i = 0; i < strErr.length; i++) {
		msg = msg + "・" + strErr[i] + "\n";
	}

	// 入力チェック結果メッセージの表示
	if( strErr.length > 0 ){
		msg="内容に誤りがあります。入力項目を確認してください。\n"+msg;
		alert(msg);
		return false;
	}

	else{
		return true;
	}
}