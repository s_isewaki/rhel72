//**************************************************************************************************
// 権限チェックボックスコントロール（３画面で利用）
//
// employee_auth_group.php        職員登録⇒権限グループ
// employee_auth_setting.php      職員登録⇒任意職員選択⇒権限
// employee_auth_bulk_setting.php 職員登録⇒一括設定⇒[表示]リンク
//
//**************************************************************************************************
var AuthCBoxRelate = {
    doCheck : function(srcId, destId) {
        var srcIds = srcId.split(","); // 影響を与えるチェックボックスIDリストカンマ区切り（主に管理チェックボックス）
        var dest = ee(destId); // 影響を受けるチェックボックス（主に一般チェックボックス）
        if (!dest) alert("チェックボックス"+destId+"がありません");
        var isAnyChecked = "";
        for (var idx=0; idx<srcIds.length; idx++) {
            if (srcIds[idx] && ee(srcIds[idx]) && ee(srcIds[idx]).checked) { isAnyChecked=1; break; }
        }
        if (isAnyChecked) {
            dest.checked = true;
            dest.disabled = true;
        } else {
            var isLicense = lcs_list[destId]; // 被影響側にライセンスがあるか
            dest.disabled = (isLicense ? false : true);
        }
    }
}

