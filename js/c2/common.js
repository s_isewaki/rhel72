
var pcs = ["msie","chrome","safari","firefox","opera","trident"];
var no_pcs = ["ipod","ipad","android","mobile"];
var isNotPC = 0;
var ua = window.navigator.userAgent.toLowerCase();
for (var idx=0; idx<no_pcs.length; idx++) if (ua.indexOf(no_pcs[idx])>=0) { isNotPC = 1; }

var uAgent = navigator.userAgent.toLowerCase();
var isIE = (uAgent.indexOf("msie") != -1);

var str = function(s) { if (!s && s!=0) return ""; return ""+s; }
var substr2=function(s,p,l){s=str(s);if(!s||s.length==0)return "";if(s.length<=p)return "";if(!l || s.length<=p+l) return s.substring(p);return s.substring(p,p+l);}
var stopEvent = function(evt) { if(!evt) evt=window.event; try{evt.stopPropagation();}catch(ex){} try{evt.preventDefault();}catch(ex){} try{evt.cancelBubble=true;}catch(ex){} return false; }
var int = function(s) { if (!s) return 0; var num = parseInt(s,10); if (!num || isNaN(num)) return 0; return num; };
var ee = function(id) { return document.getElementById(id); }
var trim = function(s) { if (s==undefined) return ""; s+=""; return s.replace(/^[\s　]+|[\s　]+$/g, ''); }
var tryFocus = function(id) { if (!id) return; var e=ee(id); tryFocusByElem(e); }
var tryFocusByElem = function(e) { if (!e) return; if(!e || e.disabled || e.readOnly) return; e.focus(); try{e.select();}catch(ex){} }
var isEnterKey = function(elem, evt){if(!elem.disabled && !elem.readOnly) var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==13; }
var isKeyboradKey=function(elem, evt, k){if(!elem.disabled && !elem.readOnly) var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==k; }
var getComboValue = function(obj) { if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].value; }
var getComboText = function(obj) { if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].text; }
var uEnc = function(s) { return encodeURIComponent(s); }
var zeroSuppress = function(s) { return s.replace(/^0/, ""); }
var isOneOf = function(s, a) { for(var i=0; i<a.length; i++) if(s==a[i]) return true; return false; }
var setDisabled = function(elem, bool) { elem.disabled = bool; if (bool) $(elem).addClass("text_readonly"); else $(elem).removeClass("text_readonly"); }
var getWindowScrollTop = function(win) { if ('pageYOffset' in window) return win.pageYOffset; return Math.round (win.document.documentElement.scrollTop); }
function getAlias(s) { var a = str(s).split("\t"); if (a.length>1 && a[1]!="") return a[1]; return a[0]; }
var htmlEscape = function(str) {
    var ret = trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\n/g, '\\n');
    return eval('"'+ret.replace(/\&amp\;\#([x]?)([0-9A-Za-z]+)\;/g, '"+toEntity(\"$1\",$2)+"')+'"');
}
var getFormElementByName = function(form, name) { return $(form).find("[name="+name+"]").get(0); }

var htmlBinary = function(str) {
    var ret = trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\n/g, '\\n');
    return eval('"'+ret.replace(/\&amp\;\#([x]?)([0-9A-Za-z]+)\;/g, '"+toBinary(\"$1\",$2)+"')+'"');
}
var toEntity = function(x, n) {
    if (x) {
        var hex = eval("0x"+n);
        if (hex<0xFF) return '&amp;#'+x+n+";";
        return "&#0x"+n+";";
    }
    var ord = eval(n);
    if (ord < 128) return '&amp;#'+n+";";
    return "&#"+n+";";
}
var toBinary = function(x, n) {
    if (x) {
        var hex = eval("0x"+n);
        if (hex<0xFF) return '&amp;#'+x+n+";";
        return cFromCharCode(eval("0x"+n));
    }
    var ord = eval(n);
    if (ord < 128) return '&amp;#'+n+";";
    return cFromCharCode(eval(n));
}
var cFromCharCode = function(){
    var buf = '';
    for ( var i = 0, l = arguments.length; i < l; i++ ) {
        var n = arguments[i];
        if ( n < 0x10000 ) {
            buf += String.fromCharCode(n);
            continue;
        }
        n -= 0x10000;
        buf += String.fromCharCode(
            0xD800 + ( n >> 10 ),
            0xDC00 + ( n & 0x3FF )
        );
    }
    return buf;
};

var setComboValue = function(obj,v, t){
    var o=obj.options;
    for(var i=0; i<o.length; i++) {
        if(o[i].value==v) {
            obj.selectedIndex=i;
            return;
        }
    }
    if (v && !t) {
        o[o.length] = new Option(v);
        obj.selectedIndex = o.length-1;
        return;
    }
    if (v && t) {
        o[o.length] = new Option(t, v);
        obj.selectedIndex = o.length-1;
        return;
    }
    obj.selectedIndex=0;
}
function getClientWidth(){
    if (document.documentElement && document.documentElement.clientWidth) return document.documentElement.clientWidth;
    if (document.body.clientWidth) return document.body.clientWidth;
    if (window.innerWidth) return window.innerWidth;
    return 0;
}
function ymdToJpWeek(ymd) {
    var aYmd = ymdToArray(ymd);
    var dt = new Date(aYmd.yy, aYmd.zmm-1, aYmd.zdd);
    var ary = ["日","月","火","水","木","金","土"];
    return ary[dt.getDay()];
}
function ymdDateAdd(ymd, inc) {
    var yy = substr2(ymd, 0, 4);
    var mm = substr2(ymd, 4, 2);
    var dd = substr2(ymd, 6, 2);
    var dt = new Date(yy, int(mm)-1, int(dd)).getTime();
    var ndt = new Date(dt + (60*60*24*1000) * inc);
    return ndt.getFullYear() + ("00"+(ndt.getMonth()+1)).slice(-2) + ("00"+(ndt.getDate())).slice(-2);
}

function ymdToArray(ymdhm) {
    var yy = substr2(ymdhm, 0, 4);
    var mm = substr2(ymdhm, 4, 2);
    var dd = substr2(ymdhm, 6, 2);
    var hh = substr2(ymdhm, 8, 2);
    var mi = substr2(ymdhm, 10, 2);
    var ymd = yy + mm + dd;
    var hm = hh + mi;
    var zmm = zeroSuppress(mm);
    var zdd = zeroSuppress(dd);
    var zhh = zeroSuppress(hh);
    var dt = new Date(yy, zmm-1, zdd);
    var ary = ["","月","火","水","木","金","土","日"];
    var weekNumber = dt.getDay();
    if (weekNumber==0) weekNumber = 7;// 月(1)〜日(7)
    var weekJp = ary[weekNumber];
    return {
        yy:yy, mm:mm, dd:dd, hh:hh, mi:mi, ymd:ymd,
        zmm:zmm, zdd:zdd, zhh:zhh,
        weekNumber:weekNumber,
        md_jp : (mm ? mm+"月"+dd+"日" : ""),
        mdw_jp : (mm ? zmm+"月"+dd+"日("+ weekJp+")": ""),
        slash_mdw: (mm ? zmm+"/"+zdd+"("+ weekJp+")": ""),
        slash_ymd: (yy ? yy+"/"+mm+"/"+dd : ""),
        slash_zymd: (yy ? yy+"/"+zmm+"/"+zdd : ""),
        zmd_jp:(zmm ? zmm+"月"+zdd+"日" : ""),
        slash_md_hm:(yy ? yy+"/"+mm+"/"+dd+" "+hh+":"+mi : "")
    };
}


function encodeToUnicodeEntity(str) {
    var surrogate_1st = 0;
    var out = [];
    for (var i = 0; i < str.length; ++i) {
        var utf16_code = str.charCodeAt(i);
        if (surrogate_1st != 0) {
            if (utf16_code >= 0xDC00 && utf16_code <= 0xDFFF) {
                var surrogate_2nd = utf16_code;
                var unicode_code = (surrogate_1st - 0xD800) * (1 << 10) + (1 << 16) + (surrogate_2nd - 0xDC00);
                out.push("&#"+unicode_code.toString(10).toUpperCase() + ";");
            } else {
                // Malformed surrogate pair ignored.
            }
            surrogate_1st = 0;
        } else if (utf16_code >= 0xD800 && utf16_code <= 0xDBFF) {
            surrogate_1st = utf16_code;
        } else {
            out.push("&#"+utf16_code.toString(10).toUpperCase() + ";");
        }
    }
    return encodeURIComponent(out.join(""));
}

function c2ShowLoginPage() {
    function c2GetMainWin(win) {
        if (win.name=='comedix_main') return win;
        if (win.opener && win.opener.location.hostname==win.location.hostname) return c2GetMainWin(win.opener);
        if (win.top && win.top.location.hostname==win.location.hostname && win.top!=win) return c2GetMainWin(win.top);
        return win;
    }
    var offset=-1, end=-1, contentsRoot="";
    var key = "CMX_CONTENTS_ROOT=";
    offset = document.cookie.indexOf(key);
    if (offset>=0) end = document.cookie.indexOf(";", offset+key.length);
    if (end>=0) contentsRoot = unescape(document.cookie.substring(offset+key.length, end));
    var mainWin = c2GetMainWin(window);
    mainWin.location = contentsRoot+"login.php";
    if (window!=mainWin && !window.closed) window.close();
}

var AJAX_ERROR = "AJAX_ERROR";
var TYPE_JSON = "TYPE_JSON";
function getCustomTrueAjaxResult(msg, type_json, isCheckOnly) {
    var errMsg = "";
    if (msg=="SESSION_NOT_FOUND") errMsg = "画面呼び出しが不正です。（SESSION_NOT_FOUND）";
    else if (msg=="DATABASE_CONNECTION_ERROR") errMsg = "データベース接続に失敗しました。";
    else if (msg=="DATABASE_SELECT_ERROR") errMsg = "データベース検索に失敗しました。";
    else if (msg=="DATABASE_UPDATE_ERROR") errMsg = "データベース更新に失敗しました。";
    else if (msg=="SYSTEM_ERROR") errMsg = "システムエラーが発生しました。";
    else if (msg=="NO_AUTH") errMsg = "機能操作権限がありません。";
    else if (msg=="SESSION_EXPIRED") errMsg = "長時間利用されませんでしたのでタイムアウトしました。\n再度ログインしてください。";
    if (errMsg) {
        alert(errMsg);
        c2ShowLoginPage();
        return AJAX_ERROR;
    }
    if (substr2(msg,0,2)=="ng") {
        alert(msg.substring(2));
        return AJAX_ERROR;
    }
    if (isCheckOnly) return "";

    if (substr2(msg,0,2)!="ok") {
        alert(msg);
        return AJAX_ERROR;
    }
    if (type_json!=TYPE_JSON) return msg.substring(2);
    var ret = "";
    try { eval("var ret = "+msg.substring(2)+";"); } catch(ex) {
        if (ee("dump")) ee("dump").innerHTML = htmlEscape(msg);
        alert("通信エラーが発生しました。(1)\n"+msg.substring(2)); return AJAX_ERROR;
    }
    if (!ret) {
        alert("通信エラーが発生しました。(2)\n"+msg.substring(2)); return AJAX_ERROR;
    }
    return ret;
}

function isParentEnabling(formId, elem) {
    if (!elem) return true;
    if (elem.disabled) return false;
    if (elem.style.display=="none") return false;
    if (elem.id==formId) return true;
    return isParentEnabling(formId, elem.parentNode);
}
function checkInputError(formId) {
    var ret = 1;
    var errorElem = 0;
    $("#"+formId+" select").each(function(){
        if (!isParentEnabling(formId, this)) this.selectedIndex = 0;
    });
    $("#"+formId+" input[type=checkbox]").each(function(){
        if (!isParentEnabling(formId, this)) this.checked = false;
    });
    $("#"+formId+" input[type=text]").each(function(){
        if (!isParentEnabling(formId, this)) this.value = ""; // 自分または親がdisabledだったり、display=noneだったりすれば、値をカラにする
        var pattern = this.getAttribute("pattern");
        if (!pattern) return;
        pattern = ("^"+pattern+"$").replace("^^", "^").replace("$$","$");
        if (this.value.match(pattern)) return;
        if (!errorElem) errorElem = this;
        ret = 0;
    });
    if (!ret) {
        alert("入力値が正しくない箇所があります。");
        tryFocusByElem(errorElem);
    }
    return ret;
}

function serializeUnicodeEntity(wrapId) {
    var data = getFormData(wrapId);
    var serials = [];
    for (var idx in data) {
        var obj = data[idx];
        serials.push("uni_entity__"+obj.name+"="+encodeToUnicodeEntity(obj.value));
    }
    return serials.join("&");
}

function getFormData(wrapId) {
    var data = [];
    var pushData = function(elem) {
        var obj = {};
        obj["name"] = elem.name;
        obj["value"] = elem.value;
        data.push(obj);
    }
    $("#"+wrapId+" input[type=text]").each(    function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $("#"+wrapId+" input[type=password]").each(function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $("#"+wrapId+" input[type=hidden]").each(  function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $("#"+wrapId+" input[type=checkbox]").each(function(){ if(isParentEnabling(wrapId, this) && this.checked) pushData(this); });
    $("#"+wrapId+" input[type=radio]").each(   function(){ if(isParentEnabling(wrapId, this) && this.checked) pushData(this); });
    $("#"+wrapId+" select").each(              function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $("#"+wrapId+" textarea").each(            function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    return data;
}

function c2Ajax(opt) {
    if (!opt["error"]) {
        opt["error"] = function(XMLHttpRequest, textStatus, errorThrown){
            var msg = [
                "通信エラーが発生しました。",
                "url: "+opt["url"],
                "HttpStatus: "+XMLHttpRequest.status,
                "textStatus: "+textStatus,
                "errorThrown: "+errorThrown.message
            ];
             alert(msg.join("\n"));
        }
    }
    $.ajax(opt);
}

var activateJavascriptSeq = 0;
function activateJavascript(innerHTMLId) {
    var removeTags = [];
    $("#"+innerHTMLId+" script").each(function() {
        activateJavascriptSeq++;
        removeTags.push(this);
        var html = this.innerHTML;
        var script = document.createElement('script');
        script.setAttribute("type", "text/javascript");
        script.innerHTML = html;
        script.setAttribute("script_id", activateJavascriptSeq);
        $(this.parentNode).append(script);
    });
    for (var idx=0; idx<removeTags.length; idx++) {
        $(removeTags[idx]).remove();
    }
}





//**************************************************************************************************
// ナビ
//**************************************************************************************************
var gNavi = {
    globalMouseX : 0,
    globalMouseY : 0,
    $tblNavi : 0,
    iframeNavi : 0,
    tdNavi : 0,
    thNaviTitle : 0,
    activeMode : 1,
    initialize : function() {
        var self = this;
        var tag =
        '<table cellspacing="0" cellpadding="0" id="tblNavi" onmouseover="gNavi.hide(this)"><tr>' +
        '    <th id="thNaviTitle"></th><td id="tdNavi"></td>' +
        '</tr></table>';
        $("body").append(tag);
        this.$tblNavi = $("#tblNavi");
        this.iframeNavi = ee("iframeNavi");
        this.tdNavi = ee("tdNavi");
        this.thNaviTitle = ee("thNaviTitle");
        $("html").mousemove(function(evt){
            self.globalMouseX = evt.pageX;
            self.globalMouseY = evt.pageY;
        });
        $(document).on('mouseenter', '[navi],[navititle]', function(){ self.show(this); });
        $(document).on('mouseleave', '[navi],[navititle]', function(){ self.hide(); });
    },
    show : function(target){
        if (!gNavi.activeMode) return;
        var gmsg = trim($(target).attr("navi"));
        var gtitle = trim($(target).attr("navititle"));
        var bGmsg = (gmsg!="");
        var bGtitle = (gtitle!="");
        if (!bGmsg && !bGtitle) return;

        var win = window;
        if (parent && parent.floatingpage) win = parent.floatingpage;
        var scTop = $(win).scrollTop();
        var wh = win.innerHeight;
        if (document.all || win.opera) wh = win.document.documentElement.clientHeight;
        if (!wh) wh = win.document.getElementsByTagName("body")[0].clientHeight;
        if (!wh) wh = document.body.clientHeight;

        if (wh<1) return;

        var ww = win.innerWidth;
        if (!ww && document.documentElement) ww = document.documentElement.clientWidth;
        if (!ww) ww = document.body.clientWidth;
        if (!ww) return;

        this.tdNavi.innerHTML = gmsg.replace(/(\n)/g, "<br />");
        this.tdNavi.style.display = (bGmsg ? "" : "none");
        this.thNaviTitle.innerHTML = trim(gtitle);
        this.thNaviTitle.style.display = (bGtitle ? "" : "none");

        this.$tblNavi.css({top:-1000, width:"auto"});
        this.$tblNavi.show();

        var tblh = this.$tblNavi.get(0).offsetHeight;
        var tblw = this.$tblNavi.get(0).offsetWidth;
        if (tblw > ww - 20) { tblw = ww - 20; this.$tblNavi.css({"width":tblw}); }
        this.$tblNavi.hide();

        var wy = (wh - tblh - 20);
        if (wy <= this.globalMouseY-scTop) wy = this.globalMouseY - scTop - tblh - 30;

        this.$tblNavi.css({top:wy, left:18});

        this.$tblNavi.fadeIn(300);
    },
    hide : function() {
        if (this.$tblNavi) this.$tblNavi.stop(true, true).hide();
    }
};


