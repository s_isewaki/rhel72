//**************************************************************************************************
// マイページレイアウタ（４画面で利用）
//
// option/o_regist.php               オプション設定⇒表示設定
// employee_display_group.php        職員登録⇒表示グループ
// employee_display_setting.php      職員登録⇒任意職員選択⇒表示設定
// employee_display_bulk_setting.php 職員登録⇒一括設定⇒[表示]リンク
//
//**************************************************************************************************
var MypageLayouter = {
    initSectionTypeInfo : {},
    init : function(initSectionCount, initSectionTypeInfo) {
        this.sectionTypeInfo = initSectionTypeInfo;
        $("#div_evacuate").disableSelection();
        this.changeSectionCount(initSectionCount);
        $("#mypage_sections").show();
    },
    changeSectionCount : function(sectionCount) {
        var sTop = getWindowScrollTop(window);
        $("#mypage_sections div.mypage_item").each(function(){
            $("#div_evacuate").append(this);
        });
        for (var idx=1; idx<=sectionCount; idx++) {
            if (ee("mypage_section_type"+idx)) this.sectionTypeInfo[idx] = getComboValue("mypage_section_type"+idx);
        }
        $("#mypage_sections").html("");
        for (var idx=1; idx<=sectionCount; idx++) {
            if (!this.sectionTypeInfo[idx]) this.sectionTypeInfo[idx] = "100";
            var html =
            '<div id="mypage_section_select'+idx+'" style="padding-top:10px; position:relative;">'+
            '<select id="section_type'+idx+'" class="mypage_section_select" onchange="MypageLayouter.divideSection('+idx+', value)">'+
            '<option value="100"'+(this.sectionTypeInfo[idx]=="100"?" selected":"")+'>セクション'+idx+'：分割なし</option>'+
            '<option value="50_50"'+(this.sectionTypeInfo[idx]=="50_50"?" selected":"")+'>セクション'+idx+'：２分割（１対１）</option>'+
            '<option value="33_67"'+(this.sectionTypeInfo[idx]=="33_67"?" selected":"")+'>セクション'+idx+'：２分割（１対２）</option>'+
            '<option value="67_33"'+(this.sectionTypeInfo[idx]=="67_33"?" selected":"")+'>セクション'+idx+'：２分割（２対１）</option>'+
            '<option value="33_33_34"'+(this.sectionTypeInfo[idx]=="33_33_34"?" selected":"")+'>セクション'+idx+'：３分割（１対１対１）</option>'+
            '</select>'+
            '</div>'+
            '<div id="mypage_section_container'+idx+'" style="margin-top:3px; position:relative"></div>';
            $("#mypage_sections").append(html);
            this.divideSection(idx, this.sectionTypeInfo[idx]);
        }
        window.scrollTo(0,sTop);
    },
    divideSection : function(idx, divide) {
        var sTop = getWindowScrollTop(window);
        $("#mypage_sections div.mypage_item").each(function(){
            $("#div_evacuate").append(this);
        });
        $("#mypage_section_container"+idx).html("");
        if (!divide) divide = "100";
        var divs = divide.split("_");
        var html =
        '<table style="position:relative"><tr>'+
        '<td style="width:'+(divs[0]*7)+'px; position:relative">'+
        '<div class="section2 dropbox" id="mypage_section_sub'+idx+'_1" style="margin:0; padding:16px; position:relative"></div></td>'+
        (divs.length>1 ? '<td style="width:'+(divs[1]*7)+'px; padding-left:10px; position:relative">'+
        '<div class="section2 dropbox" style="margin:0; padding:16px; position:relative" id="mypage_section_sub'+idx+'_2"></div></td>' : '')+
        (divs.length>2 ? '<td style="width:'+(divs[2]*7)+'px; padding-left:10px; position:relative">'+
        '<div class="section2 dropbox" style="margin:0; padding:16px; position:relative" id="mypage_section_sub'+idx+'_3"></div></td>' : '')+
        '</tr></table>';
        $("#mypage_section_container"+idx).append(html);
        $("#mypage_section_container"+idx+" div.dropbox").sortable({revert:true, connectWith:"div.dropbox", update:function(event, ui){
            var elem = ui.item.get(0);
            var pid = elem.parentNode.id.replace("mypage_section_sub","");
            elem.setAttribute("section", pid);
         }});
        this.appendItems();
        window.scrollTo(0,sTop);
    },
    appendItems : function(isReFixId) {
        $("#div_evacuate div.mypage_item").each(function() {
            var secs = this.getAttribute("section").split("_");
            if (secs.length!=2) secs = [1, 1];
            var box = ee("mypage_section_sub"+secs[0]+"_"+secs[1]);
            if (!box) { secs[1] = 1; box = ee("mypage_section_sub"+secs[0]+"_"+secs[1]); }
            if (!box) { secs[0] = 1; box = ee("mypage_section_sub"+secs[0]+"_"+secs[1]); }
            if (isReFixId) this.setAttribute("section", secs[0]+"_"+secs[1]);
            $(box).append(this);
        });
    },
    tryReg : function(submitForm) {
        var sTop = getWindowScrollTop(window);
        $("#mypage_sections div.mypage_item").each(function(){
            $("#div_evacuate").append(this);
        });
        this.appendItems(1);
        window.scrollTo(0,sTop);

        vv = [];
        var sectionCount = int(getComboValue(ee("section_count")));
        for (var sidx=1; sidx<=sectionCount; sidx++) {
            var ssub = getComboValue(ee("section_type"+sidx));
            vv.push("[section]:"+sidx+":"+ssub);
            var secs = ssub.split("_");
            for (var didx=1; didx<=secs.length; didx++) {
                $("div#mypage_section_sub"+sidx+"_"+didx+" div.mypage_item").each(function(){
                    var item_index = this.getAttribute("item_index");
                    var field = this.getAttribute("field_name");
                    var width = getComboValue(ee("ddl_"+item_index));
                    vv.push(field+":"+sidx+"_"+didx+":"+width);
                });
            }
        }
        ee("mypage_layout_info").value = vv.join(",");
        if (submitForm) submitForm.submit();
    }
};
