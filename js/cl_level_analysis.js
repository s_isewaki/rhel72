// 所属追加
function add_affiliation(){

	document.terms.action = "#";
	document.terms.target = "";
	document.terms.mode.value = "add_affiliation";
	document.terms.submit();

}

// 所属削除
function del_affiliation(obj){

	var dom_obj_parent = obj.parentNode;
	dom_obj_parent.removeChild(obj);

	document.terms.affiliation_row_count.value = parseInt(document.terms.affiliation_row_count.value) - 1;

	analysis_reset();

}

function selected_text_save(value_obj_id, text_obj_id) {
    var value_obj = document.getElementById(value_obj_id);
    var n = value_obj.selectedIndex;
    document.getElementById(text_obj_id).value = value_obj.options[n].text;
    analysis_reset();
}

function analysis_reset()
{
	document.terms.action = "#";
	document.terms.target = "";
	document.terms.mode.value = "";
	document.terms.submit();
}

// 分析ボタン押下時
function out_analysis()
{
	if (!input_check()) {
		return false;
	}

	document.terms.action = "#";
	document.terms.target = "";
	document.terms.mode.value = "analysis";
	document.terms.submit();
}

// EXCEL出力ボタン押下時
function out_excel()
{
	if (!input_check()) {
		return false;
	}
	document.terms.action = "cl_level_analysis_excel.php";
	document.terms.target = "download";
	document.terms.submit();
}

// グラフボタン押下時
function out_graph()
{
	if (!input_check()) {
		return false;
	}
	window.open('', 'cl_level_analysis_chart', 'directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1080,height=700');
	document.terms.action = "cl_level_analysis_chart.php";
	document.terms.target = "cl_level_analysis_chart";
	document.terms.submit();
}

// 入力チェック
function input_check()
{
	var errinfo = new Array();
	var err_idx=0;

	if (document.terms.analysis_category.value == "") {
		document.terms.analysis_category.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '分析項目を入力してください。';
	} else {
		document.terms.analysis_category.style.backgroundColor="";
	}

	if (document.terms.analysis_items) {
		if (document.terms.analysis_items.value == "") {
			document.terms.analysis_items.style.backgroundColor="FFB6C1";
			errinfo[err_idx++] = '分析項目を入力してください。';
		} else {
			document.terms.analysis_items.style.backgroundColor="";
		}
	}

	if (document.terms.year_from) {
		if (document.terms.year_from.value == "-") {
			document.terms.year_from.style.backgroundColor="FFB6C1";
			errinfo[err_idx++] = '年度Fromを入力してください。';
		} else {
			document.terms.year_from.style.backgroundColor="";
		}
	}

	if (document.terms.year_to) {
		if (document.terms.year_to.value == "-") {
			document.terms.year_to.style.backgroundColor="FFB6C1";
			errinfo[err_idx++] = '年度Toを入力してください。';
		} else {
			document.terms.year_to.style.backgroundColor="";
		}
	}

	if (
		document.terms.year_from
		&& document.terms.year_to
	) {
		if (document.terms.year_from.value > document.terms.year_to.value) {
			document.terms.year_to.style.backgroundColor="FFB6C1";
			errinfo[err_idx++] = '年度Toに年度From以前が設定されています。';
		} else {
			document.terms.year_to.style.backgroundColor="";
		}
	}

	// 入力チェック結果メッセージの表示
	var msg="";
	for (i = 0; i < errinfo.length; i++) {
		msg = msg + "・" + errinfo[i] + "\n";
	}

	if( errinfo.length > 0 ){
		msg="申請内容に誤りがあります。入力項目を確認してください。\n"+msg;
		alert(msg);
		return false;
	} else {
		return true;
	}

}

function show_emp_list(row_1_key,row_2_key,column_1_key,column_2_key)
{

	var moduleName = "cl_level_analysis_emp_list_1";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();

	var class_id = '';
	var atrb_id = '';
	var dept_id = '';
	var room_id = '';

	// ログインユーザー
	params.login_emp_id = document.getElementById('login_emp_id').value;

	// 職種
	params.job = document.getElementById('job').value;

	// レベル
	params.level = document.getElementById('level').value;

	// 所属
	for (i = 0; i < document.getElementById('affiliation_row_count').value; i++) {
		if (i > 0) {
			class_id += ',';
			atrb_id += ',';
			dept_id += ',';
			if (document.getElementById('room_id[' + i + ']')) {
				room_id += ',';
			}
		}
		class_id += document.getElementById('class_id[' + i + ']').value;
		atrb_id += document.getElementById('atrb_id[' + i + ']').value;
		dept_id += document.getElementById('dept_id[' + i + ']').value;
		if (document.getElementById('room_id[' + i + ']')) {
			room_id += document.getElementById('room_id[' + i + ']').value;
		}
	}
	params.affiliation_row_count = document.getElementById('affiliation_row_count').value;
	params.class_id = class_id;
	params.atrb_id = atrb_id;
	params.dept_id = dept_id;
	params.room_id = room_id;

	// 年度（From）
	if (document.getElementById('year_from')) {
		params.year_from = document.getElementById('year_from').value;
	}

	// 年度（To）
	if (document.getElementById('year_to')) {
		params.year_to = document.getElementById('year_to').value;
	}

	// 分析項目
	params.analysis_items = document.getElementById('analysis_items').value;

	// 行・列キー
	params.row_1_key = row_1_key;
	params.row_2_key = row_2_key;
	params.column_1_key = column_1_key;
	params.column_2_key = column_2_key;

	params.p_session = document.getElementById('session').value;

	//ウィンドウサイズなどのオプション
	var w = 880;
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option,'cl_level_analysis_emp_list_1',params.p_session);

}