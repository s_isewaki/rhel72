jQuery.noConflict();
var j$ = jQuery;

function reload_page() {
    document.list_form.submit();
}

//===============================================
//実行アイコン
//===============================================
//作成ボタン/分析番号のクリック
function analysis_new(session, ID) {
    if(ID) {
        var url = "hiyari_analysis_regist.php?session="+session+"&mode=update&a_id="+ID;
    } else {
        var url = "hiyari_analysis_regist.php?session="+session+"";
    }
    var h = 400;//window.screen.availHeight;
    var w = 500;//window.screen.availWidth;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

    window.open(url, '_blank',option);
}

//削除ボタン
function analysis_action_delete() {
    var ids = get_selected_classification_folder_ids();
    if(! check_multi_select(ids))
    {
        return;
    }

    if(! confirm("削除してもよろしいですか？")) {
        return;
    }

    document.list_form.mode.value = 'delete';
    document.list_form.submit();

}

//検索ボタン
function show_search_area()
{
    var display = document.getElementById('search_area').style.display;
    if(display == ''){
        display = 'none';
        document.list_form.search_toggle.value = '';
    }
    else{
        display = '';
        document.list_form.search_toggle.value = 'open';
    }
    document.getElementById('search_area').style.display = display;

    // デフォルト設定
    if (document.list_form.search_default_date_start) {
        if (document.getElementById('search_torey_date_in_search_area_start').value == '') {

            document.getElementById('search_torey_date_in_search_area_start').value = document.list_form.search_default_date_start.value;
        }
        if (document.getElementById('search_torey_date_in_search_area_end').value == '') {
            document.getElementById('search_torey_date_in_search_area_end').value = document.list_form.search_default_date_end.value;
        }
    }
}

//選択されているリストID配列を謫ｾします。
function get_selected_classification_folder_ids()
{
    var result = new Array();
    var r_i = 0;

    var list_select_objs = document.getElementsByName("delete_key[]");
    for(var i = 0; i < list_select_objs.length ; i++)
    {
        list_select_obj = list_select_objs.item(i);
        if(list_select_obj.checked)
        {
            result[r_i] = list_select_obj.value;
            r_i++;
        }
    }

    return result;
}

//複数選択チェック
function check_multi_select(ids)
{
    if(ids.length < 1)
    {
        alert("選択されていません。");
        return false;
    }
    return true;
}

//===============================================
//SHEL分析のEXCEL出力
//===============================================
function output_excel(has_shel)
{
    if (has_shel){
        document.list_form.mode.value = 'excel';
        document.list_form.submit();
        document.list_form.mode.value = 'search';
    }
    else{
        alert('SHEL分析がありません。');
    }
}

//===============================================
//検索の実行
//===============================================
function search_torey()
{
    //入力チェック
    var st = document.getElementById('renewal_date_start_input').value;
    var ed = document.getElementById('renewal_date_end_input').value;
    if( (st != "" && !is_date_str(st)) || (ed != "" && !is_date_str(ed)) )
    {
        alert("更新年月日が正しくありません。");
        return;
    }

    var st = document.getElementById('assessment_date_start_input').value;
    var ed = document.getElementById('assessment_date_end_input').value;
    if( (st != "" && !is_date_str(st)) || (ed != "" && !is_date_str(ed)) )
    {
        alert("評価予定期日が正しくありません。");
        return;
    }

    document.list_form.page.value=1;
    document.list_form.mode.value="search";
    document.list_form.submit();
}

//YYYY/MM/DD形式の日付文字列であるか判定します。(100年)
function is_date_str(datestr)
{
    if(!datestr.match(/^\d{4}\/\d{2}\/\d{2}$/)){
        return false;
    }
    var vYear = datestr.substr(0, 4) - 0;
    var vMonth = datestr.substr(5, 2) - 1; // Javascriptは、0-11で表現
    var vDay = datestr.substr(8, 2) - 0;
    if(vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31){
        var vDt = new Date(vYear, vMonth, vDay); //※0100/01/01 より前だと1900年代と解釈される。
        vDt.setFullYear(datestr.substr(0, 4));
        vDt.setMonth(vMonth);
        vDt.setDate(vDay);
        if(isNaN(vDt)){
            return false;
        }else if(vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

//===============================================
//一覧の操作
//===============================================
//タイトルのクリック
function analysis_method_new(ID, METHOD, ANALYSIS, session) {
    if(ANALYSIS == '') {
        alert('分析事案を指定してから、再度アクセスしてください');
    } else {
        if(METHOD == '4M4E') {
            var url = "hiyari_analysis_4m4e.php?session="+session+"&a_id="+ID;
        } else if(METHOD == 'SHEL') {
            var url = "hiyari_analysis_shel.php?session="+session+"&a_id="+ID;
        } else if(METHOD == 'RCA') {
            var url = "hiyari_analysis_rca.php?session="+session+"&a_id="+ID;
        } else {
            var url = "hiyari_analysis_general.php?session="+session+"&a_id="+ID;
        }

        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,loaction=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

        window.open(url, '_blank', option);
    }
}
