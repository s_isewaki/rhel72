//院外研修一覧から研修を選択した時の処理
function return_selected(select_data)
{
	//呼び出し元画面にインターフェース関数が定義されている場合
	if(window.opener && !window.opener.closed && window.opener.cl_select_levelup_apply_emp_return)
	{
		//返却値を取得
		var result = m_return_data_list[select_data];
		//親画面へ通知
		window.opener.cl_select_levelup_apply_emp_return(result);
	}
	//自画面を終了します。
	window.close();
}


//行のハイライト制御
//class属性が指定されたclass_nameのTDタグをハイライトします。
function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

//class属性が指定されたclass_nameのTDタグのハイライトを解除します。
function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

//highlightCells()、dehighlightCells()専用関数
function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++)
	{
		if (cells[i].className != class_name)
		{
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}
