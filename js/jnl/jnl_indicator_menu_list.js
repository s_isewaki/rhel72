function changeFirst(e) {
    type = document.getElementById('hidden_first');
    secondDiv = document.getElementById('second_div');
    fourthDiv = document.getElementById('fourth_div');
    if (e.value==type.value) {
        return;
    }
    else {
        second = document.getElementById('second');
        second.innerHTML = '';
        if (e.value == 0) {
            data = document.getElementById('second_0');
            for (l=0; l < data.options.length; l++) {
                second.options[l] = new Option(data.options[l].text, data.options[l].value);
            }
            secondDiv.style.display = '';
            fourthDiv.style.display = 'none';
        }
        else if (e.value == 1) {
            data = document.getElementById('second_1');
            for (l=0; l<data.options.length; l++) {
                second.options[l] = new Option(data.options[l].text, data.options[l].value);
            }
            secondDiv.style.display = '';
            fourthDiv.style.display = 'none';
        }
        else if (e.value == 2) {
            data = document.getElementById('second_2');
            for (l=0; l<data.options.length; l++) {
                second.options[l] = new Option(data.options[l].text, data.options[l].value);
            }
            secondDiv.style.display = '';
            fourthDiv.style.display = 'none';
        }
        else if (e.value == 3) {
            data = document.getElementById('second_3');
            for (l=0; l<data.options.length; l++) {
                second.options[l] = new Option(data.options[l].text, data.options[l].value);
            }
            secondDiv.style.display = '';
            fourthDiv.style.display = 'none';
        }
        else {
            second.options[0] = new Option('選択してください', null);
            secondDiv.style.display = 'none';
            fourthDiv.style.display = 'none';
        }

        type.value = e.value;
        return changeSecond(second);
    }
}

function changeSecond(e) {
    type = document.getElementById('hidden_second');
    thirdDiv = document.getElementById('third_div');
    if (e.value==type.value) {
        return;
    }
    else {
        first = document.getElementById('first');
        third = document.getElementById('third');
        third.innerHTML = '';
        if (first.value == 0) {
            if (e.value == 1) {
                data = document.getElementById('third_0_1');
                for (l=0; l < data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 2) {
                data = document.getElementById('third_0_2');
                for (l=0; l < data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else {
                third.options[0] = new Option('選択してください', null);
                thirdDiv.style.display = 'none';
            }
        }
        else if (first.value == 1) {
            if (e.value == 1) {
                data = document.getElementById('third_1_1');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 2) {
                data = document.getElementById('third_1_2');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 3) {
                data = document.getElementById('third_1_3');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 4) {
                data = document.getElementById('third_1_4');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 5) {
                data = document.getElementById('third_1_5');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else {
                third.options[0] = new Option('選択してください', null);
                thirdDiv.style.display = 'none';
            }
        }
        else if (first.value == 2) {
            if (e.value == 1) {
                data = document.getElementById('third_2_1');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 2) {
                data = document.getElementById('third_2_2');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 3) {
                data = document.getElementById('third_2_3');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 4) {
                data = document.getElementById('third_2_4');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 5) {
                data = document.getElementById('third_2_5');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else {
                third.options[0] = new Option('選択してください', null);
                thirdDiv.style.display = 'none';
            }
        }
        else if (first.value == 3) {
            if (e.value == 1) {
                data = document.getElementById('third_3_1');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 2) {
                data = document.getElementById('third_3_2');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else if (e.value == 3) {
                data = document.getElementById('third_3_3');
                for (l=0; l<data.options.length; l++) {
                    third.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                thirdDiv.style.display = '';
            }
            else {
                third.options[0] = new Option('選択してください', null);
                thirdDiv.style.display = 'none';
            }
        }
        else {
            third.options[0] = new Option('選択してください', null);
            thirdDiv.style.display = 'none';
        }

        type.value = e.value;
        return changeThird(third);
    }
}

function changeThird(e) {
    type = document.getElementById('hidden_third');
    fourthDiv = document.getElementById('fourth_div');
    if (e.value==type.value) {
        return;
    }
    else {
        first = document.getElementById('first');
        second = document.getElementById('second');
        fourth = document.getElementById('fourth');
        fourth.innerHTML = '';
        if (e.value == '選択してください' || e.value == '') {
            fourth.options[0] = new Option('選択してください', null);
            fourthDiv.style.display = 'none';
        }
        else if (first.value == 0) {
            if (second.value == 1) {
                data = document.getElementById('fourth_0_1');
                for (l=0; l < data.options.length; l++) {
                    fourth.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                fourthDiv.style.display = '';
            }
            else if (second.value == 2) {
                data = document.getElementById('fourth_0_2');
                for (l=0; l < data.options.length; l++) {
                    fourth.options[l] = new Option(data.options[l].text, data.options[l].value);
                }
                fourthDiv.style.display = '';
            }
            else {
                fourthDiv.style.display = 'none';
            }
            type.value = e.value;
            return changeFourth(fourth);
        }
        else if (first.value !=0 && e != null && e.value!='') {
            type.value = e.value;
            return changeDisplay(first.value, second.value, null, e.value);
        }
        else {
            fourth.options[0] = new Option('選択してください', null);
            fourthDiv.style.display = 'none';
        }
    }
}

function changeFourth(e) {
    type = document.getElementById('hidden_fourth');
    fourthDiv = document.getElementById('fourth_div');
    if (e.value==type.value) {
        return;
    }
    else {
        first  = document.getElementById('first');
        second = document.getElementById('second');
        third  = document.getElementById('third');

        if (third != null && third.value != '' && e != null && e.value != 'null' && e.value != '' && e.value != '選択してください' ) {
            type.value = e.value;
            return changeDisplay(first.value, second.value, third.value, e.value);
        }
    }
}

function changeDisplay(inputCNum, inputScNum, plantNo, inputNo) {
    wkfwForm = document.getElementById("wkfw");
    noData = document.getElementById('target_file_'+inputCNum+'_'+inputScNum+'');
    if (noData != null && inputNo!='null') {

        target = '';
        for (i=0; i < noData.options.length; i++) {
            if (inputNo == noData.options[i].value) {
                target = noData.options[i].text;
            }
        }

        wkfwForm.action = target;
        wkfwForm.method = 'get';
        no = document.getElementById('_no');
        no.name = 'no';
        no.value = inputNo;
        if (plantNo) {
            plant = document.getElementById('_plant');
            plant.name = 'plant';
            if (plant) {
                plant.value = plantNo;
            }
        }
        wkfwForm.submit();
        return;
    }
}