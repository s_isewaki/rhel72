var xmlHttp;

function ajaxSend(){
    if (window.XMLHttpRequest){
        xmlHttp = new XMLHttpRequest();
    }
    else {
        if (window.ActiveXObject){
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else{
                  xmlHttp = null;
    }
  }
  return xmlHttp;
}

function checkStatus(){
  if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
    alert(xmlHttp.responseText);
  }
}

function chg1st(e, empId) {
    hide1st = document.getElementById('hidden_first');
    div2nd  = document.getElementById('second_div');
    div3rd  = document.getElementById('third_div');
    div4th  = document.getElementById('fourth_div');

    if (e == null || e.value == null || e.value==hide1st.value) {
        return;
    }
    else {
        sel2nd = document.getElementById('second');
        sel2nd.innerHTML = '';

        div2nd.style.display = 'none';
        div3rd.style.display = 'none';
        div4th.style.display = 'none';

        hide1st.value = e.value;
        if (e != null && e.value != null && e.value != '選択してください') {
            param = '{"emp_id":"'+empId+'", "ctg":"'+e.value+'"}';
            xmlHttp = ajaxSend();
            xmlHttp.onreadystatechange = setChg1st;
            xmlHttp.open("GET", "./jnl_application_indicator_ajax.php?func=getSubCtg&param="+param, true);
            xmlHttp.send(null);
        }
    }
}

function chg2nd(e, empId) {
    hide2nd = document.getElementById('hidden_second');
    div3rd  = document.getElementById('third_div');
    div4th  = document.getElementById('fourth_div');
    if (e == null || e.value == null || e.value == hide2nd.value) {
        return;
    }
    else {
        sel1st = document.getElementById('first');
        sel3rd = document.getElementById('third');
        sel3rd.innerHTML = '';

        hide2nd.value = e.value;
        div3rd.style.display = 'none';
        div4th.style.display = 'none';

        if (sel1st.value=='0') {
            param = '{"emp_id":"' + empId + '", "ctg":"' + sel1st.value + '", "sub_ctg":"' + e.value + '"}';
            xmlHttp = ajaxSend();
            xmlHttp.onreadystatechange = setChg2nd;
            xmlHttp.open("GET", "./jnl_application_indicator_ajax.php?func=getFid&param="+param, true);
            xmlHttp.send(null);
        }
        else if (sel1st.value != '' && sel1st.value != '選択してください') {
            param = '{"emp_id":"' + empId + '", "ctg":"' + sel1st.value + '", "sub_ctg":"' + e.value + '"}';
            xmlHttp = ajaxSend();
            xmlHttp.onreadystatechange = setChg2nd;
            xmlHttp.open("GET", "./jnl_application_indicator_ajax.php?func=getNo&param="+param, true);
            xmlHttp.send(null);
        }
    }
}

function chg3rd(e, empId) {
    hide3rd = document.getElementById('hidden_third');
    div4th  = document.getElementById('fourth_div');
    if (e == null || e.value == null || e.value==hide3rd.value) {
        return;
    }
    else {
        sel1st = document.getElementById('first');
        sel2nd = document.getElementById('second');
        sel4th = document.getElementById('fourth');
        sel4th.innerHTML = '';
        hide3rd.value = e.value;
        div4th.style.display = 'none';

        if (sel1st.value == '0') {
            param = '{"emp_id":"' + empId + '", "ctg":"' + sel1st.value + '", "sub_ctg":"' + sel2nd.value + '", "facility_id":"'+e.value+'"}';
            xmlHttp = ajaxSend();
            xmlHttp.onreadystatechange = setChg3rd;
            xmlHttp.open("GET", "./jnl_application_indicator_ajax.php?func=getNo&param="+param, true);
            xmlHttp.send(null);
        }
        else if(sel1st.value!='0' && e!=null && e.value!=null && e.value!='選択してください') {
//            alert('ctg='+sel1st.value+', sub_ctg='+sel2nd.value+', facility_id=null, no='+e.value);
            return changeDisplay(sel1st.value, sel2nd.value, null, e.value);
        }
    }
}

function chg4th(e, empId) {
    hide4th = document.getElementById('hidden_fourth');
    if (e == null || e.value == null || e.value == '選択してください' || e.value == hide4th.value) {
//        alert(e.value);
        return;
    }
    else {
        sel1st = document.getElementById('first');
        sel2nd = document.getElementById('second');
        sel3rd = document.getElementById('third');

        if (sel3rd != null && sel3rd.value != '' && e != null && e.value != null && e.value != '選択してください' ) {
            hide4th.value = e.value;
//            alert('ctg='+sel1st.value+', sub_ctg='+sel2nd.value+', facility_id='+sel3rd.value+', no='+e.value);
            return changeDisplay(sel1st.value, sel2nd.value, sel3rd.value, e.value);
        }
    }
}

function setChg1st() {
    var data;
    sel2nd = document.getElementById('second');
    div2nd = document.getElementById('second_div');
    empId  = document.getElementById('_emp_id').value;
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
        data = eval(xmlHttp.responseText);
        for (i in data) {
            sel2nd.options[i] = new Option(data[i]["name"], data[i]["num"]);
        }
        if (data.length > 1) {
            div2nd.style.display = '';
        }
        return chg2nd(sel2nd, empId);
    }
}

function setChg2nd() {
    var data;
    sel3rd = document.getElementById('third');
    div3rd = document.getElementById('third_div');
    empId = document.getElementById('_emp_id').value;

    if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
        data = eval(xmlHttp.responseText);
        for (i in data) {
            sel3rd.options[i] = new Option(data[i]["name"], data[i]["num"]);
        }
        if (data.length > 1) {
            div3rd.style.display = '';
        }
        return chg3rd(sel3rd, empId);
    }
}

function setChg3rd() {
    var data;
    sel4th = document.getElementById('fourth');
    div4th = document.getElementById('fourth_div');
    empId  = document.getElementById('_emp_id').value;

    if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
//alert(xmlHttp.responseText);
        data = eval(xmlHttp.responseText);
        for (i in data) {
            sel4th.options[i] = new Option(data[i]["name"], data[i]["num"]);
        }
        if (data.length > 1) {
            div4th.style.display = '';
        }
        return chg4th(sel4th, empId);
    }
}