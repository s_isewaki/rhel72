function init_page(mode, news_non_tag){
  showSubCategory();
  if (mode=="update"){
    //TinyMCEを使用しない場合はタグ抜きで表示する。
    if(!m_use_tyny_mce){
    	document.mainform.news.value = news_non_tag;
    }
  }
}

function showSubCategory(){
	switch (document.mainform.news_category.value){
		case '1':  // 全館
			document.mainform.class_id.style.display = 'none';
			document.mainform.job_id.style.display = 'none';
			break;
		case '2':  // 部門
			document.mainform.class_id.style.display = '';
			document.mainform.job_id.style.display = 'none';
			break;
		case '3':  // 職種
			document.mainform.class_id.style.display = 'none';
			document.mainform.job_id.style.display = '';
			break;
	}
}

function attachFile(session){
	window.open('hiyari_news_attach.php?session=' + session, 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e){
	if (e == undefined){
		e = window.event;
	}

	var btn_id;
	if (e.target){
		btn_id = e.target.getAttribute('id');
	}
	else{
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

var m_use_tyny_mce = false;
if(!tinyMCE.isOpera){
	m_use_tyny_mce = true;
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "preview,table,emotions,fullscreen,layer",
		//language : "ja_euc-jp",
		language : "ja",
		width : "100%",
		height : "300",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid",
		content_css : "tinymce/tinymce_content.css",
		theme_advanced_statusbar_location : "none",
		force_br_newlines : true,
		forced_root_block : '',
		force_p_newlines : false
	});
}
