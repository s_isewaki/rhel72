function training_insert()
{
	var errinfo = new Array();
	var err_idx=0;
	var re = new RegExp("[^0-9]+");

	// 必須チェック
	if (document.training_form.training_name.value == ''){
		document.training_form.training_name.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '研修名を入力してください。';
	} else {
		document.training_form.training_name.style.backgroundColor="";
	}

	// 2012/11/19 Yamagawa add(s)
	if (document.training_form.term_div.value == '') {
		document.training_form.term_div.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '申込期間を選択してください。';
	} else {
		document.training_form.term_div.style.backgroundColor="";
	}
	// 2012/11/19 Yamagawa add(e)

	if (document.training_form.training_purpose.value == ''){
		document.training_form.training_purpose.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '目的を入力してください。';
	} else {
		document.training_form.training_purpose.style.backgroundColor="";
	}

	if (document.training_form.training_opener_id.value == ''){
		document.training_form.training_opener_nm.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '主催者を選択してください。';
	} else {
		document.training_form.training_opener_nm.style.backgroundColor="";
	}

	if (document.training_form.training_teacher1_id.value == ''){
		document.training_form.training_teacher1_nm.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '講師１を選択してください。';
	} else {
		document.training_form.training_teacher1_nm.style.backgroundColor="";
	}

	if (document.training_form.training_require.value == ''){
		document.training_form.training_require.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '受講要件を入力してください。';
	} else {
		document.training_form.training_require.style.backgroundColor="";
	}

	if (document.training_form.max_training_time.value == ''){
		document.training_form.max_training_time.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '研修回数を入力してください。';
	} else if (re.test(document.training_form.max_training_time.value)) {
		document.training_form.max_training_time.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '研修回数は半角数値で入力してください。';
	} else if (parseInt(document.training_form.max_training_time.value) < 1) {
		document.training_form.max_training_time.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '研修回数は1以上を入力してください。';
	} else {
		document.training_form.max_training_time.style.backgroundColor="";
	}

	if (document.training_form.training_slogan.value == ''){
		document.training_form.training_slogan.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '目標を入力してください。';
	} else {
		document.training_form.training_slogan.style.backgroundColor="";
	}

	if (document.training_form.training_contents.value == ''){
		document.training_form.training_contents.style.backgroundColor="FFB6C1";
		errinfo[err_idx++] = '内容を入力してください。';
	} else {
		document.training_form.training_contents.style.backgroundColor="";
	}

	var msg="";
	for (i = 0; i < errinfo.length; i++) {
		msg = msg + "・" + errinfo[i] + "\n";
	}

	// 入力チェック結果メッセージの表示
	if( errinfo.length > 0 ){
		msg="申請内容に誤りがあります。入力項目を確認してください。\n"+msg;
		alert(msg);
		return false;
	}

	document.training_form.action = "cl_training_insert.php";
	document.training_form.target = "";
	document.training_form.insertflg.value = 'true';
	document.training_form.submit();
}

function openEmployeeList(item_id) {

	document.training_form.input_div.value = item_id;

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;

	window.open('', 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	document.training_form.action="cl_emp_list.php";
	document.training_form.target = "emplist";
	document.training_form.submit();
}

function clearEmployee(item_id){

	var name_obj_name = '';
	var name_obj_id = '';
	var id_obj_id = '';
	var name_obj;
	var id_obj;

	switch(item_id){
		case 'training_opener':
			name_obj_id = 'training_opener_nm';
			id_obj_id = 'training_opener_id';
			name_obj_name = '主催者';
			break;
		case 'training_teacher1':
			name_obj_id = 'training_teacher1_nm';
			id_obj_id = 'training_teacher1_id';
			name_obj_name = '講師１';
			break;
		case 'training_teacher2':
			name_obj_id = 'training_teacher2_nm';
			id_obj_id = 'training_teacher2_id';
			name_obj_name = '講師２';
			break;
		case 'training_teacher3':
			name_obj_id = 'training_teacher3_nm';
			id_obj_id = 'training_teacher3_id';
			name_obj_name = '講師３';
			break;
		case 'training_teacher4':
			name_obj_id = 'training_teacher4_nm';
			id_obj_id = 'training_teacher4_id';
			name_obj_name = '講師４';
			break;
		case 'training_teacher5':
			name_obj_id = 'training_teacher5_nm';
			id_obj_id = 'training_teacher5_id';
			name_obj_name = '講師５';
			break;
		case 'theme_present_before':
			name_obj_id = 'theme_present_before_nm';
			id_obj_id = 'theme_present_before_id';
			name_obj_name = '研修課題（前）　提出先';
			break;
		case 'theme_present_now':
			name_obj_id = 'theme_present_now_nm';
			id_obj_id = 'theme_present_now_id';
			name_obj_name = '研修課題（中）　提出先';
			break;
		case 'theme_present_after':
			name_obj_id = 'theme_present_after_nm';
			id_obj_id = 'theme_present_after_id';
			name_obj_name = '研修課題（後）　提出先';
			break;
	}

	if (confirm('「' + name_obj_name + '」を削除します。よろしいですか？')){
		document.getElementById(name_obj_id).value = '';
		document.getElementById(id_obj_id).value = '';
	}
}

function add_target_list(item_id, emp_id, emp_nm){

	var arr_add_emp_id = emp_id.split(',');
	if (arr_add_emp_id.length > 1) {
		alert('職員は１名のみ指定してください。');
		return;
	}

	document.getElementById(item_id + '_id').value = emp_id;
	document.getElementById(item_id + '_nm').value = emp_nm;

}