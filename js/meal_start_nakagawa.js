var TUIKAHIN_ITEM_MAX = 5;
var KEIKAN_TIME_MAX;    // 値はPHP側でセットされる
var radios = {};
function radioAlternate(radio){
  if (radios[radio.id] && radio.checked) radio.checked = false;
  radios[radio.id] = radio.checked;
}

function toDbl(s){
  if (!s) return 0;
  var v = parseFloat(s);
  if (isNaN(v)) return 0;
  if (v < 0) return 0;
  return v * 1;
}

function setText(tag, text)
{
  try { tag.innerText   = text ; } catch(e) { }
  try { tag.textContent = text ; } catch(e) { }
}

function getText(tag)
{
  return (tag.innerText || tag.textContent || "") ;
}

function getRadioInfo(name, nl)
{
  var retStr = (nl) ? null : "" ;
  var radios = document.getElementsByName(name);
  for (var i = 0 ; i < radios.length ; i++)
  {
    if (radios[i].checked) { retStr = radios[i].value ; break ; }
  }
  return retStr ;
}

function getCheckBoxInfo(cbx, lbl)
{
  var retStr = "" ;
  if (cbx.checked) { retStr = (lbl) ? getText(cbx.parentNode) : cbx.value ; }
  return retStr ;
}

function getCheckBoxInfo2(id, lbl)
{
  var cbx = document.getElementById(id) ;
  return getCheckBoxInfo(cbx, lbl) ;
}

// 期間文字列を取得する
function getKikanDate()
{
  var retStr = createDateStr('mstt_exec');
  retStr += ' ' + getRadioInfo('mstt_kikan_asahiruyu', false) + '　' + '〜';
  return retStr;
}

// yyyy年mm月dd日形式の日付文字列を生成する
function createDateStr(prefix)
{
  var retStr = '';
  var yy = $('select[name="' + prefix + '_y' + '"]').val();
  if (yy.length < 1) yy = '    ';
  retStr += (yy + '年');
  var mm = ('0' + $('select[name="' + prefix + '_m' + '"]').val()).slice(-2);
  if (mm.length < 1) mm = '  ';
  retStr += (mm + '月');
  var dd = document.tmplform.date_d1.value;
  var dd = ('0' + $('select[name="' + prefix + '_d' + '"]').val()).slice(-2);
  if (dd.length < 1) dd = '  ';
  retStr += (dd + '日');

  return retStr;
}

function trim(str)
{
  return str.replace(/^\s+|\s+$/g, "") ;
}

function sp(str)
{
  return str.replace(/\s\s+/, " ") ;
}

//****************************************************
// 入力チェック
//****************************************************
// 入力チェック
function InputCheck(){
  if (document.getElementById("mstt_riyu_3").checked) return true ;

  function $o       (id) { if (!document.getElementById(id)) alert(id +" is not exist!"); return document.getElementById(id); }
  function $checked (id) { return $o(id).checked ? true : false; }
  function $nocheck (id) { return $o(id).checked ? false : true; }
  function $selected(id) { return $o(id).selectedIndex > 0 ? true : false; }
  function $value   (id) { return $o(id).value.replace(/^[ 　]+/g, "").replace(/[ 　]+$/g, "").replace(/^[\r\n]+/g, "").replace(/[\r\n]+$/g, ""); }
  function $empty   (id) { return $value(id) ? false : true; }

  var out = [];

  if ($empty("mstt_disease_name")) out.push("傷病名を指定してください。");

  var pfix = "mstt_riyu_";
  if ($nocheck(pfix+"1") && $nocheck(pfix+"2") && $nocheck(pfix+"3")) out.push("理由を指定してください。");

  pfix = "date_";
  if (!$selected(pfix+"y1") || !$selected(pfix+"m1") || !$selected(pfix+"d1")) out.push("期間の年月日を指定してください。");

  pfix = "mstt_kikan_asahiruyu_";
  if ($nocheck(pfix+"1") && $nocheck(pfix+"2") && $nocheck(pfix+"3")) out.push("期間の朝昼夕を指定してください。");

  pfix = "mstt_";
  if ($nocheck(pfix+"keikou_nomi") && $nocheck(pfix+"keikan_nomi") && $nocheck(pfix+"keikou_heiyou")) {
    if ($nocheck("mstt_riyu_3")) out.push("経口・経管区分を指定してください。");
  }

  // 食種のチェック
  if (!$checked('mstt_keikan_nomi')){
    // 一般食のチェック
    var ippan_selected = false;
    pfix = 'mstt_syokusyu_ippan_';
    var i;
    for (i = 1; i <= 5; i++) {
      if ($checked(pfix + i)) {
        ippan_selected = true;
        break;
      }
    }
    
    if ($checked(pfix + 'other')) {
      ippan_selected = true;
    }
    
    if ($checked(pfix + 'other') && $empty(pfix + 'kcal')) {
      out.push('一般食のカロリーを入力してください。');
    }

    // エネルギーコントロール食のチェック
    var energy_selected = false;
    pfix = 'mstt_syokusyu_energy_';

    if ($checked(pfix + '1') || $checked(pfix + 'other')) {
      energy_selected = true;
    }
    
    if ($checked(pfix + 'other') && ($empty(pfix + 'kcal'))) {
      out.push('エネルギーコントロール食のカロリーを入力してください。');
    }

    // 貧血食のチェック
    var hinketu_selected = false;
    pfix = 'mstt_syokusyu_hinketu_';
    for (i = 1; i <= 5; i++) {
        if ($checked(pfix + i)) {
          hinketu_selected = true;
          break;
        }
      }
    
    if ($checked(pfix + 'other')) {
      hinketu_selected = true;
    }
    if ($checked(pfix + 'other') && ($empty(pfix + 'kcal'))) {
      out.push('貧血食のカロリーを入力してください。');
    }

    // 塩分制限のチェック
    pfix = 'mstt_syokusyu_enbun_';
    if ($checked(pfix + 'other') && $empty(pfix + 'g')) {
      out.push('塩分制限のグラム数を入力してください。');
    }
    
    if (!ippan_selected && !energy_selected && !hinketu_selected) {
      out.push('一般食、エネルギーコントロール食、貧血食の項目のどれかにチェックを入れてください。');
    }
  }

  // ゼリー項目の入力チェック
  if ($('#mstt_jelly_other').prop('checked') && !$('#mstt_jelly_other_text').val()) {
    out.push('ゼリーその他内容を入力してください。');
  }

  // おやつ項目の入力チェック
  oyatuInputCheck(out);

  // アレルギーおよび禁物項目のチェック
  allergyKinmotuCheck(out);

  // 追加品項目の入力チェック
  tuikahinInputCheck(out);
  
  // 経管栄養項目の入力チェック
  keikanInputCheck(out);

  if (!out.length) return true;
  alert(out.join("\n"));
  return false;
}

// おやつ項目の入力チェック
function oyatuInputCheck(out)
{
  if ($('#mstt_oyatu').prop('checked') && !$('#mstt_oyatu_text').val()) {
    out.push('おやつを選択してください。');
  }

  if ($('#mstt_oyatu_other').prop('checked') && !$('#mstt_oyatu_other_text').val()) {
    out.push('おやつその他に入力してください。');
  }
  
  if ($('#mstt_oyatu_ryou_other').prop('checked') && !$('#mstt_oyatu_ryou_other_text').val()) {
    out.push('おやつその他指示に入力してください。');
  }
}

// アレルギーおよび禁物項目のチェック
function allergyKinmotuCheck(out)
{
  if ($('#mstt_allergy_other').prop('checked') && !$('#mstt_allergy_other_text').val()) {
    out.push('アレルギーおよび禁物のその他に入力してください。');
  }

  if ($('#mstt_allergy_comment_other').prop('checked') && !$('#mstt_allergy_comment_other_text').val()) {
    out.push('アレルギーおよび禁物コメントのその他に入力してください。');
  }
}

// 追加品項目の入力チェック
function tuikahinInputCheck(out)
{
  var ryou;
  for (var i = 1; i <= mstt_tuikahin_length; i++) {
    // 時刻のチェック
    if ($('#mstt_tuikahin_time_' + i).prop('checked') && !$('#mstt_tuikahin_time_text_' + i).val()) {
      out.push('追加品(' + i + ')の時刻を入力してください。');
    }
    
    // 量チェック
    for (var j = 1; j <= TUIKAHIN_ITEM_MAX; j++) {
      ryou = $('#mstt_tuikahin' + j + '_ryou_' + i).val();
      if (ryou) {
        if (!$.isNumeric(ryou)) {
          out.push('追加品(' + i + ')の' + j + '番目の数量を数値で入力してください。');
        }
      }
    }
  }
}

// 経管栄養項目の入力チェック
function keikanInputCheck(out)
{
  var pfix = 'mstt_keikan_';
  var large_length = $('#mstt_keikan_large_length').val();
  // 経管栄養項目の未入力チェック
  var codes = [];
  var i, j, time, hour, min, tmp;
  for (i = 1; i <= large_length; i++) {
    keikanInputSmallCheck(i, codes, out);
    
    // 時刻の入力形式チェック
    for (j = 1; j <= KEIKAN_TIME_MAX; j++) {
      time = $('#mstt_keikan_time_' + i + '_' + j).val();
      if (time && !time.match(/\d{1,2}:\d{1,2}/)) {
        out.push('経管栄養' + i + '）の' + j + '番目の時刻の形式が正しくありません。');
        continue;
      }
      tmp = time.split(':');
      hour = tmp[0];
      min = tmp[1];
      if (hour < 0 || 24 < hour || min < 0 || 59 < min) {
        out.push('経管栄養' + i + '）の' + j + '番目の時刻が正しくありません。');
      }
    }
  }

  var keikoukeikan = $('input[name="mstt_keikou_keikan"]:checked').val();
  if (keikoukeikan && keikoukeikan.match(/経管/)) {
    if (codes.length == 0){
      out.push('経管栄養をひとつ以上指定してください。');
    }
  }
}

// 経管栄養小項目の入力チェック
function keikanInputSmallCheck(num, codes, out)
{
  var small_length = $('#mstt_keikan_small_length_' + num).val();
  var code, ryou;
  for (i = 1; i <= small_length; i++) {
    ryou = $('#mstt_keikan_ryou_' + num + '_' + i).val();
    code = $('#mstt_keikan_code_' + num + '_' + i).val();
    if (code) {
      // 薬剤が選択されていたら、配列にコードをセット
      codes.push(code);
    }
    
    if (ryou) {
      if (!$.isNumeric(ryou)) {    // 数量チェック(薬剤は数字のみ入力可)
        out.push('経管栄養' + num + '）の' + i + '番目の数量を数値で入力してください。');
      }
      
      if (!code) {    // 薬剤が未選択で、数量が入力されている
        out.push('経管栄養' + num + '）の' + i + '番目の薬剤を選択してください。');
      }
    }
  }
}

//****************************************************
// BMI
//****************************************************
// BMIの計算
function calcBMI()
{
  var height = $('#mstt_pt_height').val();
  var weight = $('#mstt_pt_weight').val();

  if (!height || !weight) {
    $('#mstt_pt_bmi').html('');
    return;
  }

  height = parseFloat(height) / 100;
  weight = parseFloat(weight);
  var bmi = weight / (height * height);
  if (bmi) {
    bmi = Math.round(bmi * 100) / 100;  // 四捨五入
    $('#mstt_pt_bmi_span').html(bmi);
    $('#mstt_pt_bmi').val(bmi);
  }
}

//****************************************************
// 背景色の変更
//****************************************************
// 変更項目の背景色を変更する
function diff(elem_id, copy_id, color_object_id) {
  var elem = document.getElementById(elem_id);
  var type = elem.type;
  if (!copy_id) copy_id = elem_id+"_copy";
  var copy_elem = document.getElementById(copy_id);
  var color_elem = color_object_id ? document.getElementById(color_object_id) : elem;

  if (type=="checkbox" || type=="radio"){
    var v = elem.value;
    if (!elem.checked) v = "";
    var color = (v == copy_elem.value) ? "transparent" : "#ffbbda";
    color_elem.style.backgroundColor = color;
  } else {
    var color = (elem.value == copy_elem.value) ? "#fff" : "#ffbbda";
    color_elem.style.backgroundColor = color;
    if (type.substring(0,6)=="select") elem.style.padding = 0;
  }
}

// 変更項目の背景色を変更する(塩分制限以外の食種)
function diffSyokusyu(elem_id, syokusyu_type) {
  var value, kcal_value, copy_value, kcal_copy_value;
  var other_check;
  var color;
  var syokusyuNames = new Array("mstt_syokusyu_ippan", "mstt_syokusyu_energy", "mstt_syokusyu_hinketu");
  for (var i = 0; i < syokusyuNames.length; i++) {
    if (syokusyu_type == syokusyuNames[i]) {
      value = $("input[name='mstt_syokusyu']:checked").val();
      value = value ? value : "";
    } else {
      value = "";
    }

    // 値を取得
    kcal_value = $("#" + syokusyuNames[i] + "_kcal").val();
    copy_value = $("#" + syokusyuNames[i] + "_copy").val();
    kcal_copy_value = $("#" + syokusyuNames[i] + "_kcal_copy").val();

    // 任意入力のチェック状態
    other_check = $("#" + syokusyuNames[i] + "_other").is(":checked");

    // 背景色を決定
    if ((value == copy_value)    // 任意入力以外で、選択値に変更が無い
    && ((kcal_copy_value == "" && !other_check) || (kcal_copy_value && other_check))) {        // 任意入力が無く、チェックもされていないか、任意入力があり、チェックされている
      color = "transparent";
    } else {
      color = "#ffbbda"
    }

    // 背景色をセット
    $("#" + syokusyuNames[i] + "_container").css("background-color", color);

    // 任意入力エリアの背景色
    diff(syokusyuNames[i] + "_kcal");
  }
}

//****************************************************
// 食種
//****************************************************
function radioCheckWhenTextExist(id, id_kcal, id_g){
    if (id_kcal && id_g) {
      document.getElementById(id+"_other").checked = ($(id+"_"+id_kcal).value != "" || $(id+"_"+id_g).value != "");
      diff(id+"_other", id+"_copy", id+"_container");
      diff(id+"_kcal");
      diff(id+"_g");

      return;
    }
    if (id_kcal) {
      document.getElementById(id+"_other").checked = ($(id+"_"+id_kcal).value != "");
      diff(id+"_other", id+"_copy", id+"_container");
      diff(id+"_kcal");
    }
    if (id_g) {
      document.getElementById(id+"_other").checked = ($(id+"_"+id_g).value != "");
      diff(id+"_other", id+"_copy", id+"_container");
      diff(id+"_g");
    }
}

// IDから食種を識別する
function identifySyokusyu(id) {
  var syokusyuNames = new Array("mstt_syokusyu_ippan", "mstt_syokusyu_energy", "mstt_syokusyu_hinketu", "mstt_syokusyu_enbun");
  // 食種を識別
  for (var i = 0; i < syokusyuNames.length; i++) {
    var name = "";
    if (id.substring(0, syokusyuNames[i].length) == syokusyuNames[i]) {
      name = syokusyuNames[i];
      if (name != "mstt_syokusyu_enbun") {
        // 塩分制限以外の食種をhidden要素にセット
        document.getElementById("mstt_syokusyu_type").value = name;
      }
      break;
    }
  }
  return name;
}

// 食種ラジオボタンチェック時の処理
function radioClicked(id) {
  // 食種を識別
  var syokusyu_type = identifySyokusyu(id);
  
  var copy_id = "";
  var color_id = "";
  var element = null;

  // 塩分制限以外
  if (syokusyu_type == "mstt_syokusyu_ippan"
   || syokusyu_type == "mstt_syokusyu_hinketu"
   || syokusyu_type == "mstt_syokusyu_energy") {
    if ((id == syokusyu_type + "_other") && document.getElementById(syokusyu_type + "_other").checked) {    // 任意入力の場合
      document.getElementById(syokusyu_type + "_kcal").disabled = false;
      disableSyokusyuInput(syokusyu_type);
    } else {
      disableSyokusyuInput("");
    }
    diffSyokusyu(id, syokusyu_type);
  }

  if (syokusyu_type == "mstt_syokusyu_enbun") {    // 塩分制限
    if (id == "mstt_syokusyu_enbun_other") {
        document.getElementById("mstt_syokusyu_enbun_g").disabled = false;
    } else {
      element = document.getElementById("mstt_syokusyu_enbun_g");
      element.value = "";
      element.disabled = true;
      diff("mstt_syokusyu_enbun_g");
    }
    diff(id, syokusyu_type + "_copy", syokusyu_type + "_container");
  }
}

// 塩分制限以外の食種のカロリー入力エリアを入力不可にする
function disableSyokusyuInput(selected_type) {
  var syokusyuNames = new Array("mstt_syokusyu_ippan", "mstt_syokusyu_energy", "mstt_syokusyu_hinketu");
  var selector;
  for (var i = 0; i < syokusyuNames.length; i++) {
    if (selected_type == syokusyuNames[i]) {
      // 選択された食種の入力エリアはスキップ
      continue;
    }
    selector = "#" + syokusyuNames[i] + "_kcal";
    $(selector).val("");
    $(selector).attr("disabled", "disabled");
  }
}

//****************************************************
// 時間帯
//****************************************************
// 時間帯チェックボックスクリック時
function onClickHour(id) {
  var hours = ['mstt_hour_asa', 'mstt_hour_hiru', 'mstt_hour_yu', 'mstt_hour_oyatu'];
  
  // 「全て」がチェックされた場合
  var length = hours.length;
  if (id == 'mstt_hour_all') {
    if ($('#' + id).prop('checked')) {
      for (var i = 0; i < length; i++) {
        $('#' + hours[i]).prop('checked', true);
        $('#' + hours[i]).attr('disabled', true);
        diff(hours[i], hours[i] + '_copy', hours[i] + '_container');
      }
    } else {
      for (var i = 0; i < length; i++) {
        $('#' + hours[i]).attr('disabled', false);
        diff(hours[i], hours[i] + '_copy', hours[i] + '_container');
      }
    }
  }

  diff(id, id + '_copy', id + '_container');
}

//****************************************************
// 主食
//****************************************************
function mstt_syusyoku_add(){
  if (mstt_syusyuku_length >= 10) return;
  document.getElementById("mstt_syusyoku_btn_div_"+mstt_syusyuku_length).style.display = "none";
  mstt_syusyuku_length++;
  document.getElementById("mstt_syusyoku_div_"+mstt_syusyuku_length).style.visibility = "";
  document.getElementById("mstt_syusyoku_div_"+mstt_syusyuku_length).style.display = "";
  document.getElementById("mstt_syusyoku_btn_div_"+mstt_syusyuku_length).style.display = "";
  document.getElementById("mstt_syusyoku_hidden_"+mstt_syusyuku_length).value = "1";
  resizeIframe();
}

function mstt_syusyoku_del(){
  if (mstt_syusyuku_length <= 1) return;
  document.getElementById("mstt_syusyoku_btn_div_"+mstt_syusyuku_length).style.display = "none";
  document.getElementById("mstt_syusyoku_div_"+mstt_syusyuku_length).style.visibility = "hidden";
  document.getElementById("mstt_syusyoku_div_"+mstt_syusyuku_length).style.display = "none";
  document.getElementById("mstt_syusyoku_hidden_"+mstt_syusyuku_length).value = "";
  mstt_syusyuku_length--;
  document.getElementById("mstt_syusyoku_btn_div_"+mstt_syusyuku_length).style.display = "";
  resizeIframe();
}

//****************************************************
// 副食
//****************************************************
function mstt_fukusyoku_add(){
  if (mstt_fukusyoku_length >= 10) return;
  document.getElementById("mstt_fukusyoku_btn_div_"+mstt_fukusyoku_length).style.display = "none";
  mstt_fukusyoku_length++;
  document.getElementById("mstt_fukusyoku_div_"+mstt_fukusyoku_length).style.visibility = "";
  document.getElementById("mstt_fukusyoku_div_"+mstt_fukusyoku_length).style.display = "";
  document.getElementById("mstt_fukusyoku_btn_div_"+mstt_fukusyoku_length).style.display = "";
  document.getElementById("mstt_fukusyoku_hidden_"+mstt_fukusyoku_length).value = "1";
  resizeIframe();
}

function mstt_fukusyoku_del(){
  if (mstt_fukusyoku_length <= 1) return;
  document.getElementById("mstt_fukusyoku_btn_div_"+mstt_fukusyoku_length).style.display = "none";
  document.getElementById("mstt_fukusyoku_div_"+mstt_fukusyoku_length).style.visibility = "hidden";
  document.getElementById("mstt_fukusyoku_div_"+mstt_fukusyoku_length).style.display = "none";
  document.getElementById("mstt_fukusyoku_hidden_"+mstt_fukusyoku_length).value = "";
  mstt_fukusyoku_length--;
  document.getElementById("mstt_fukusyoku_btn_div_"+mstt_fukusyoku_length).style.display = "";
  resizeIframe();
}

//****************************************************
// おやつ
//****************************************************
//おやつ分量クリック時の処理
function onClickOyatuRyouContainer() {
    if ($('#mstt_oyatu_ryou_other').prop('checked')) {
      $('#mstt_oyatu_ryou_other_text').removeAttr('disabled');
    } else {
      $('#mstt_oyatu_ryou_other_text').val('');
      if (!$('#mstt_oyatu_ryou_other_text').prop('disabled')) {
        $('#mstt_oyatu_ryou_other_text').attr('disabled', 'disabled');
        diff('mstt_oyatu_ryou_other_text');
      }
    }
}

//****************************************************
// 追加品
//****************************************************
function mstt_tuikahin_add(){
  if (mstt_tuikahin_length >= 3) return;
  document.getElementById("mstt_tuikahin_btn_div_"+mstt_tuikahin_length).style.display = "none";
  mstt_tuikahin_length++;
  document.getElementById("mstt_tuikahin_div_"+mstt_tuikahin_length).style.visibility = "";
  document.getElementById("mstt_tuikahin_div_"+mstt_tuikahin_length).style.display = "";
  document.getElementById("mstt_tuikahin_btn_div_"+mstt_tuikahin_length).style.display = "";
  document.getElementById("mstt_tuikahin_hidden_"+mstt_tuikahin_length).value = "1";
  resizeIframe();
}

function mstt_tuikahin_del(){
  if (mstt_tuikahin_length <= 1) return;
  document.getElementById("mstt_tuikahin_btn_div_"+mstt_tuikahin_length).style.display = "none";
  document.getElementById("mstt_tuikahin_div_"+mstt_tuikahin_length).style.visibility = "hidden";
  document.getElementById("mstt_tuikahin_div_"+mstt_tuikahin_length).style.display = "none";
  document.getElementById("mstt_tuikahin_hidden_"+mstt_tuikahin_length).value = "";
  mstt_tuikahin_length--;
  document.getElementById("mstt_tuikahin_btn_div_"+mstt_tuikahin_length).style.display = "";
  resizeIframe();
}

// 追加品アイテムリスト選択時の処理
function mstt_tuikahin_change(tkey, divcnt) {
  var code = $("#" + tkey + "_code_" + divcnt).val();
  var unit = mstt_tuikahin_unit[code];
  $("#" + tkey + "_unit_span_" + divcnt).html(unit);
  if (!unit) unit = "";
  $("#" + tkey + "_unit_" + divcnt).val(unit);
}

// チェックボックスのvalue属性にテキストボックスの値をセットする
function setTextToCheckboxValue(pfix, divcnt) {
  var text_id, checkbox_id;
  if (divcnt) {
    text_id = pfix + "_text_" + divcnt;
    checkbox_id = pfix + "_" + divcnt;
  } else {
    text_id = pfix + "_text";
    checkbox_id = pfix;
  }
  
  var value = $("#" + text_id).val();
  $("#" + checkbox_id).val(value);
}

// チェックボックスのチェックが外された時、テキストボックスの値をクリアする
function clearTextValue(pfix, divcnt) {
  var text_id, checkbox_id;
  if (divcnt) {
    text_id = pfix + "_text_" + divcnt;
    checkbox_id = pfix + "_" + divcnt;
  } else {
    text_id = pfix + "_text";
    checkbox_id = pfix;
  }

  if ($("#" + checkbox_id).prop('checked')) {
    $("#" + text_id).removeAttr("disabled");
  } else {
    $("#" + text_id).val("").attr("disabled", "disabled");
    diff(text_id);
  }
}

//****************************************************
// 経管栄養
//****************************************************
//経管栄養大項目を追加する
function mstt_keikan_large_add() {
  var large_length = Number($('#mstt_keikan_large_length').val());
  if (large_length >= 5) {
    alert('経管栄養項目の指定は5つまでです。');
    return;
  }

  // idがmstt_keikan_div_0の要素(雛形)から、クローンを作成
  var $clone = $('#mstt_keikan_div_0').clone(true);
  var uniqueid = large_length + 1;
  $clone.css('display', '').attr({'id':'mstt_keikan_div_' + uniqueid});
  $clone.find('.keikan_num_div').html(createKeikanNumStr(uniqueid));
  $clone.find('#mstt_keikan_frequency_0').attr({'id':'mstt_keikan_frequency_' + uniqueid, 'name':'mstt_keikan_frequency_' + uniqueid});  // 回数リスト
  $clone.find('#mstt_keikan_frequency_0_copy').attr({'id':'mstt_keikan_frequency_' + uniqueid + '_copy'});
  $clone.find('#mstt_keikan_frequency_div_0').attr({'id':'mstt_keikan_frequency_div_' + uniqueid});
  $clone.find('#mstt_keikan_large_del_0').attr({'id':'mstt_keikan_large_del_' + uniqueid});  // 大項目削除ボタン
  $clone.find('#mstt_keikan_large_del_div_0').attr({'id':'mstt_keikan_large_del_div_' + uniqueid}); 
  $clone.find('#mstt_keikan_small_add_0').attr({'id':'mstt_keikan_small_add_' + uniqueid});  // 小項目追加ボタン
  $clone.find('#mstt_keikan_small_del_0_1').attr({'id':'mstt_keikan_small_del_' + uniqueid + '_1'});  // 小項目削除ボタン
  $clone.find('#mstt_keikan_div_0_1').attr({'id':'mstt_keikan_div_' + uniqueid + '_1'});
  $clone.find('#mstt_keikan_code_0_1').attr({'id':'mstt_keikan_code_' + uniqueid + '_1', 'name':'mstt_keikan_code_' + uniqueid + '_1'});
  $clone.find('#mstt_keikan_code_0_1_copy').attr({'id':'mstt_keikan_code_' + uniqueid + '_1_copy'});
  $clone.find('#mstt_keikan_ryou_0_1').attr({'id':'mstt_keikan_ryou_' + uniqueid + '_1', 'name':'mstt_keikan_ryou_' + uniqueid + '_1'});    // 分量
  $clone.find('#mstt_keikan_ryou_0_1_copy').attr({'id':'mstt_keikan_ryou_' + uniqueid + '_1_copy'});
  $clone.find('#mstt_keikan_ryou_div_0_1').attr({'id':'#mstt_keikan_ryou_div_' + uniqueid + '_1'});
  $clone.find('#mstt_keikan_unit_0_1').attr({'id':'mstt_keikan_unit_' + uniqueid + '_1', 'name':'mstt_keikan_unit_' + uniqueid + '_1'});
  $clone.find('#mstt_keikan_unit_span_0_1').attr({'id':'mstt_keikan_unit_span_' + uniqueid + '_1'});
  $clone.find('#mstt_keikan_small_length_0').attr({'id':'mstt_keikan_small_length_' + uniqueid, 'name':'mstt_keikan_small_length_' + uniqueid});  // 経管栄養小項目数
  for (var i = 1; i <= KEIKAN_TIME_MAX; i++) {    // 注入時間
    $clone.find('#mstt_keikan_time_0_' + i).attr({'id':'mstt_keikan_time_' + uniqueid + '_' + i, 'name':'mstt_keikan_time_' + uniqueid + '[' + i + ']'});
    $clone.find('#mstt_keikan_time_0_' + i + '_copy').attr({'id':'mstt_keikan_time_' + uniqueid + '_' + i + '_copy'});
  }
  $clone.insertAfter('#mstt_keikan_div_' + large_length);
  large_length++;
  $('#mstt_keikan_large_length').val(large_length);
    
  resizeIframe();
}

// 経管栄養小項目を追加する
function mstt_keikan_small_add(id) {
  var fixed = 'mstt_keikan_small_add_';
  var large_num = id.substr(fixed.length);
  var small_length = Number($('#mstt_keikan_small_length_' + large_num).val());
  if (small_length >= 5) {
    alert('薬剤の指定は5つまでです。');
    return;
  }

  // 雛形からクローンを作成
  var $clone = $('#mstt_keikan_div_0_1').clone(true);
  var uniqueid_new = large_num + '_' + (small_length + 1);
  $clone.attr({'id':'mstt_keikan_div_' + uniqueid_new});
  $clone.find('#mstt_keikan_code_0_1').attr({'id':'mstt_keikan_code_' + uniqueid_new, 'name':'mstt_keikan_code_' + uniqueid_new});
  $clone.find('#mstt_keikan_code_0_1_copy').attr({'id':'mstt_keikan_code_' + uniqueid_new + '_copy'});
  $clone.find('#mstt_keikan_ryou_0_1').attr({'id':'mstt_keikan_ryou_' + uniqueid_new, 'name':'mstt_keikan_ryou_' + uniqueid_new});
  $clone.find('#mstt_keikan_ryou_0_1_copy').attr({'id':'mstt_keikan_ryou_' + uniqueid_new + '_copy'});
  $clone.find('#mstt_keikan_ryou_div_0_1').attr({'id':'mstt_keikan_ryou_div_' + uniqueid_new});
  $clone.find('#mstt_keikan_unit_0_1').attr({'id':'mstt_keikan_unit_' + uniqueid_new, 'name':'mstt_keikan_unit_' + uniqueid_new});
  $clone.find('#mstt_keikan_unit_span_0_1').attr({'id':'mstt_keikan_unit_span_' + uniqueid_new});
  $clone.find('#mstt_keikan_small_del_0_1').attr({'id':'mstt_keikan_small_del_' + uniqueid_new});  // 小項目削除ボタン
  if (small_length > 0) {
    $clone.find('.keikan_num_div').html('&nbsp;');
    $clone.find('#mstt_keikan_large_del_div_0').remove();
    $clone.find('#mstt_keikan_frequency_div_0').remove();
  } else {
    $clone.find('.keikan_num_div').html(createKeikanNumStr(large_num));
    $clone.find('#mstt_keikan_large_del_div_0').attr({'id':'mstt_keikan_large_del_div_' + large_num});
    $clone.find('#mstt_keikan_large_del_0').attr({'id':'mstt_keikan_large_del_' + large_num});
    $clone.find('#mstt_keikan_frequency_div_0').attr({'id':'mstt_keikan_frequency_div_' + large_num});
  }

  // 小項目追加ボタンの直上に小項目を追加
  $('#' + id).parent().before($clone);

  // 小項目数をセット
  $('#mstt_keikan_small_length_' + large_num).val(small_length + 1);
}

// 経管栄養大項目を削除する
function mstt_keikan_large_del(id) {
  var fixed = 'mstt_keikan_large_del_';
  var uniqueid = id.substr(fixed.length);  // 大項目要素のユニークID
  $('#mstt_keikan_div_' + uniqueid).remove();

  // 経管栄養大項目のIDを振り直す
  mstt_keikan_large_renumber();
  var large_length = Number($('#mstt_keikan_large_length').val());
  large_length--;
  $('#mstt_keikan_large_length').val(large_length);

  resizeIframe();
}

// 経管栄養小項目を削除する
function mstt_keikan_small_del(id) {
  var fixed = 'mstt_keikan_small_del_';
  var uniqueid = id.substr(fixed.length);  // 小項目要素のユニークID
  var splits = uniqueid.split('_');
  var large_num = splits[0];

  // 削除前に、回数項目と大項目削除ボタン項目を保持しておく
  $frequency_div = $('#mstt_keikan_frequency_div_' + large_num);
  $delete_btn_div = $('#mstt_keikan_large_del_div_' + large_num);
  $('#mstt_keikan_div_' + uniqueid).remove();

  // 経管栄養小項目のIDを振り直す
  mstt_keikan_small_renumber($('#mstt_keikan_div_' + large_num), large_num, large_num, $frequency_div, $delete_btn_div);
  
  // 小項目数をセット
  var small_length = Number($('#mstt_keikan_small_length_' + large_num).val());
  $('#mstt_keikan_small_length_' + large_num).val(small_length - 1);
}

// 経管栄養大項目のIDを振り直す
function mstt_keikan_large_renumber() {
    // 経管栄養大項目要素を抽出
    var $keikan_div = $('div').filter(function() {
      return this.id.match(/mstt_keikan_div_[1-9]+$/);
    });
    var size = $keikan_div.size();
    var uniqueid_old, uniqueid_new, uniqueid_str;
    var fixed = 'mstt_keikan_div_';
    var i, j;
    for (i = 0; i < size; i++) {
      uniqueid_old = $keikan_div[i].id.substr(fixed.length);
      uniqueid_new = i + 1;
      $keikan_div.eq(i).attr({'id':'mstt_keikan_div_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_frequency_' + uniqueid_old)    // 回数リスト
                       .attr({'id':'mstt_keikan_frequency_' + uniqueid_new, 'name':'mstt_keikan_frequency_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_frequency_' + uniqueid_old + '_copy') 
                       .attr({'id':'mstt_keikan_frequency_' + uniqueid_new + '_copy'});
      $keikan_div.eq(i).find('#mstt_keikan_frequency_div_' + uniqueid_old) 
                       .attr({'id':'mstt_keikan_frequency_div_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_large_del_' + uniqueid_old)    // 大項目削除ボタン
                       .attr({'id':'mstt_keikan_large_del_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_large_del_div_' + uniqueid_old)
                       .attr({'id':'mstt_keikan_large_del_div_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_small_add_' + uniqueid_old)    // 経管栄養小項目追加ボタン
                       .attr({'id':'mstt_keikan_small_add_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_small_length_' + uniqueid_old)  // 経管栄養小項目数
                       .attr({'id':'mstt_keikan_small_length_' + uniqueid_new, 'name':'mstt_keikan_small_length_' + uniqueid_new});
      for (j = 1; j <= KEIKAN_TIME_MAX; j++) {    // 注入時間
        $keikan_div.eq(i).find('#mstt_keikan_time_' + uniqueid_old + '_' + j)
                         .attr({'id':'mstt_keikan_time_' + uniqueid_new + '_' + j, 'name':'mstt_keikan_time_' + uniqueid_new + '[' + j + ']'});
        $keikan_div.eq(i).find('#mstt_keikan_time_' + uniqueid_old + '_' + j + '_copy')
                         .attr({'id':'mstt_keikan_time_' + uniqueid_new + '_' + j + '_copy'});
      }
                             
      // 経管栄養小項目のIDを振り直す
      mstt_keikan_small_renumber($keikan_div.eq(i), uniqueid_old, uniqueid_new);
    }
}

// 経管栄養小項目のIDを振り直す
function mstt_keikan_small_renumber($keikan_large_div, large_num_old, large_num_new, $frequency_div, $delete_btn_div) {
    // 経管栄養小項目要素を抽出
    var regexp = new RegExp('mstt_keikan_div_' + large_num_old + '_[1-9]+');
    var $keikan_div = $('div').filter(function() {
      return this.id.match(regexp);
    });

    var size = $keikan_div.size();
    var uniqueid_new, uniqueid_old, uniqueid_str, small_num;
    var fixed = 'mstt_keikan_div_';
    for (var i = 0; i < size; i++) {
      uniqueid_old = $keikan_div[i].id.substr(fixed.length);
      uniqueid_new = large_num_new + '_' + (i + 1);
      $keikan_div.eq(i).attr({'id':'mstt_keikan_div_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_code_' + uniqueid_old)    // 経管栄養内容リスト
                       .attr({'id':'mstt_keikan_code_' + uniqueid_new, 'name':'mstt_keikan_code_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_code_' + uniqueid_old + '_copy')
                       .attr({'id':'mstt_keikan_code_' + uniqueid_new + '_copy'});
      $keikan_div.eq(i).find('#mstt_keikan_ryou_' + uniqueid_old)    // 分量
                       .attr({'id':'mstt_keikan_ryou_' + uniqueid_new, 'name':'mstt_keikan_ryou_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_ryou_' + uniqueid_old + '_copy')
                       .attr({'id':'mstt_keikan_ryou_' + uniqueid_new + '_copy'});
      $keikan_div.eq(i).find('#mstt_keikan_ryou_div_' + uniqueid_old) 
                       .attr({'id':'mstt_keikan_ryou_div_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_small_del_' + uniqueid_old)    // 小項目削除ボタン
                       .attr({'id':'mstt_keikan_small_del_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_unit_' + uniqueid_old)    // 単位
                       .attr({'id':'mstt_keikan_unit_' + uniqueid_new, 'name':'mstt_keikan_unit_' + uniqueid_new});
      $keikan_div.eq(i).find('#mstt_keikan_unit_span_' + uniqueid_old)
                       .attr({'id':'mstt_keikan_unit_span_' + uniqueid_new});
      uniqueid_str = '&nbsp;';
      if (i == 0) {
        var splits = uniqueid_old.split('_');
        var small_num = splits[1];
        if (small_num != 1) {
          $keikan_div.eq(i).find('#mstt_keikan_ryou_div_' + uniqueid_new)
                           .after($frequency_div)
                           .after($delete_btn_div);
        }
        // 項番を表示
        uniqueid_str = createKeikanNumStr(large_num_new);
      }
      $keikan_div.eq(i).find('.keikan_num_div').html(uniqueid_str);
    }
}

// 経管栄養アイテムリスト選択時の処理
function mstt_keikan_change(id) {
  var fixed = 'mstt_keikan_code_';
  var uniqueid = id.substr(fixed.length);
  var code = $("#mstt_keikan_code_" + uniqueid).val();
  var unit = mstt_keikan_unit[code];
  $("#mstt_keikan_unit_span_" + uniqueid).html(unit);
  if (!unit) unit = "　　";
  $("#mstt_keikan_unit_" + uniqueid).val(unit);
}

// 経管栄養の項番文字列を生成
function createKeikanNumStr(num) {
  var numStr = String(num).replace(/[A-Za-z0-9]/g, function(s) {return String.fromCharCode(s.charCodeAt(0)+0xFEE0); }) + '）';
  return numStr;
}

//****************************************************
// PDF作成
//****************************************************
//食種情報を取得する
function getSyokusyuInfo(name, kcal, g)
{
  // 選択されたラジオボタンの項目名を取得
  var retStr;

  if (kcal) {
    retStr = getRadioInfo('mstt_syokusyu', true);
  } else {
    retStr = getRadioInfo(name, true);
  }
  
  if (retStr == null) {
    return "";
  }
  
  if (retStr.length > 0) {
    if (g == null) {
      return retStr + kcal;
    }
    if (kcal == null) {
      return retStr + g + "未満";
    }
    return retStr;
  }
  
  var kcalStr = "";
  var gStr = "";
  
  try {
    kcalStr = document.getElementById(name + "_" + kcal).value;
  } catch(e) {
  }
  
  try {
    gStr = document.getElementById(name + "_" + g).value;
  } catch(e) {
  }
  
  if (kcalStr.length > 0) {
    kcalStr += kcal;
  }
  
  if (gStr.length > 0) {
    gStr += g;
  }
  
  if (gStr.length < 1) {
    return kcalStr;
  }
  
  if (kcalStr.length < 1) {
    return gStr;
  }
  
  return kcalStr + "/" + gStr;
}

// 時間帯情報を取得する
function getHourInfo()
{
  var retStr = "";
  retStr += getCheckBoxInfo2("mstt_hour_asa", false);
  retStr += getCheckBoxInfo2("mstt_hour_hiru", false);
  retStr += getCheckBoxInfo2("mstt_hour_yu", false);
  retStr += getCheckBoxInfo2("mstt_hour_oyatu", false);

  return retStr;
}

// 主食・副食情報を取得する
function getSyokuInfo(id, syu)
{
  var retStr   = "" ;
  var syokuLen = syu ? mstt_syusyuku_length : mstt_fukusyoku_length;
  var ryouId   = syu ? "ryou_" : "ryo_";
  for (var i = 1 ; i <= syokuLen ; i++) {
    retStr += document.getElementById(id + i).value + " ";
    if (syu) {
      retStr += (document.getElementById(id + "other_" + i).value + " ");
    }

    var ryou;
    if (syu) {
      ryou = document.getElementById(id + ryouId + i).value;
      retStr += ryou ? ryou + "kcal相当" + " " : "";
    } else {
      retStr += getRadioInfo(id + ryouId + i, false) + " ";
    }
    retStr += "\n";
  }
  return retStr;
}

// アレルギー・禁物情報を取得する
function getAllergyKinmotuInfo(name, len)
{
  var retStr = '';
  var val;
  for (var i = 1; i <= len; i++) {
    val = getCheckBoxInfo2(name + i, false);
    if (val.length > 0) {
      retStr += val + ',';
    }
  }
  var other;
  if ($('#' + name + '_other').val()) {
    other = $('#' + name + '_other_text').val();    // 「その他」項目を取得
  }
  retStr += other ? other + ',' : '';
  return retStr.substring(0, retStr.length - 1);
}

// 追加品情報を取得する
function getTuikahinInfo()
{
  var retStr = "";
  var id = "mstt_tuikahin";
  var time;
  for (var i = 1 ; i <= mstt_tuikahin_length; i++)
  {
    var tmpStr = "";
    tmpStr += getCheckBoxInfo2(id + "_asa_" + i, false);
    tmpStr += getCheckBoxInfo2(id + "_hiru_" + i, false);
    tmpStr += getCheckBoxInfo2(id + "_yu_" + i, false);
    tmpStr += getCheckBoxInfo2(id + "_oyatu_" + i, false);
    if (tmpStr.length > 0) tmpStr += " ";
    retStr += tmpStr;

    tmpStr = document.getElementById(id + "_time_" + i).value;
    tmpStr = trim(tmpStr);
    time = "";
    if (tmpStr.match(/[0-9]/)) {
      time = tmpStr + "時 ";
    }
    retStr += time;
    tmpStr  = "";
    var text, ryou, unit, sep;
    for (var j = 1; j <= 5; j++) {
      text = $("#" + id + j + "_code_" + i + " option:selected").text();
      ryou = $("#" + id + j + "_ryou_" + i).val();
      unit = $("#" + id + j + "_unit_" + i).val();
      sep = 0;
      if (text.length > 0) {
        tmpStr += "[" + j + "]" + text;
      }
      
      ryou = ryou + unit;
      if (ryou) {
        tmpStr += ' ' + ryou;
      }
    }
    retStr += tmpStr;
    retStr += "\n";
  }
  return retStr;
}

// 投与ルート情報を取得する
function getTouyoRouteInfo()
{
  var retStr = "";
  retStr += getCheckBoxInfo(document.tmplform.mstt_touyo_route_ng, false) + " ";
  retStr += getCheckBoxInfo(document.tmplform.mstt_touyo_route_irou, false) + " ";
  retStr += getCheckBoxInfo(document.tmplform.mstt_touyo_route_cyourou, false) + " ";
  retStr += getCheckBoxInfo(document.tmplform.mstt_touyo_route_syokudourou, false) + " ";
  retStr += document.tmplform.mstt_touyo_route_other.value;
  return trim(sp(retStr));
}

//PDF印刷用フォームに経管栄養項目を追加する
function appendKeikanEiyouItems(formPdf)
{
  var large_length = $('#mstt_keikan_large_length').val();
  var small_length;
  formPdf.append($('<input/>').attr({'type':'hidden', 'name':'mstt_keikan_large_length', 'value':large_length}));
  
  var i, j, uniqueid, frequency, name, unit, ryou, time;
  for (i = 1; i <= large_length; i++) {
    frequency = $('#mstt_keikan_frequency_' + i).val();
    formPdf.append($('<input/>').attr({'type':'hidden', 'name':'mstt_keikan_frequency_' + i, 'value':frequency}));
    small_length = $('#mstt_keikan_small_length_' + i).val();
    formPdf.append($('<input/>').attr({'type':'hidden', 'name':'mstt_keikan_small_length_' + i, 'value':small_length}));
    
    for (j = 1; j <= KEIKAN_TIME_MAX; j++) {    // 注入時間
      time = $('#mstt_keikan_time_' + i + '_' + j).val();
      formPdf.append($('<input/>').attr({'type':'hidden', 'name':'mstt_keikan_time_' + i + '[]', 'value':time}));
    }
    
    for (j = 1; j <= small_length; j++) {
      uniqueid = i + '_' + j;
      name = $('#mstt_keikan_code_' + uniqueid + ' option:selected').text();
      formPdf.append($('<input/>').attr({'type':'hidden', 'name':'mstt_keikan_name_' + uniqueid, 'value':name}));
      ryou = $('#mstt_keikan_ryou_' + uniqueid).val();
      formPdf.append($('<input/>').attr({'type':'hidden', 'name':'mstt_keikan_ryou_' + uniqueid, 'value':ryou}));
      unit = $('#mstt_keikan_unit_' + uniqueid).val();
      formPdf.append($('<input/>').attr({'type':'hidden', 'name':'mstt_keikan_unit_' + uniqueid, 'value':unit}));
    }
  }
}

