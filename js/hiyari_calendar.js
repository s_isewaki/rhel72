jQuery.noConflict();
var j$ = jQuery;

//リロードします。※注意：この関数は別画面から呼び出されます。
function reload_page()
{
    document.list_form.submit();
}

//YYYY/MM/DD形式の日付文字列であるか判定します。(100年)
function is_date_str(datestr)
{
    if(!datestr.match(/^\d{4}\/\d{2}\/\d{2}$/)){
        return false;
    }
    var vYear = datestr.substr(0, 4) - 0;
    var vMonth = datestr.substr(5, 2) - 1; // Javascriptは、0-11で表現
    var vDay = datestr.substr(8, 2) - 0;
    if(vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31){
        var vDt = new Date(vYear, vMonth, vDay); //※0100/01/01 より前だと1900年代と解釈される。
        vDt.setFullYear(datestr.substr(0, 4));
        vDt.setMonth(vMonth);
        vDt.setDate(vDay);
        if(isNaN(vDt)){
            return false;
        }else if(vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

//子画面カレンダーを表示します。
function call_hiyari_calendar(caller, session)
{
    //画面左の日付検索
    if(caller == 'torey_search_st')
    {
        var obj_name = "torey_date_start_input";
    }
    else if(caller == 'torey_search_ed')
    {
        var obj_name = "torey_date_end_input";
    }
    //検索エリアの日付検索
    else if(caller == 'torey_search_st_in_search_area')
    {
        var obj_name = "search_torey_date_in_search_area_start";
    }
    else if(caller == 'torey_search_ed_in_search_area')
    {
        var obj_name = "search_torey_date_in_search_area_end";
    }
    //発生日検索
    else if(caller == 'torey_search_incident_date_st')
    {
        var obj_name = "search_incident_date_start";
    }
    else if(caller == 'torey_search_incident_date_ed')
    {
        var obj_name = "search_incident_date_end";
    }
    //画面左の日付検索
    else if(caller == 'renewal_search_st')
    {
        var obj_name = "renewal_date_start_input";
    }
    else if(caller == 'renewal_search_ed')
    {
        var obj_name = "renewal_date_end_input";
    }
    //検索エリアの日付検索
    else if(caller == 'assessment_search_st')
    {
        var obj_name = "assessment_date_start_input";
    }
    else if(caller == 'assessment_search_ed')
    {
        var obj_name = "assessment_date_end_input";
    }

    var obj = document.getElementById(obj_name);
    var ymd = obj.value;

    var param = "";
    if( (ymd != "" && is_date_str(ymd)) )
    {
        var y = ymd.substr(0, 4);
        var m = ymd.substr(5, 2);
        param = "&year="+y+"&month="+m;
    }

    window.open('hiyari_calendar.php?session=' + session + '&caller='+caller+param, 'torey_serch_calender', 'width=640,height=480,scrollbars=yes');
}

//子画面カレンダーからの結果を画面に反映させます。
function call_back_hiyari_calender(caller,ymd)
{
    //画面左の日付検索
    if(caller == 'torey_search_st')
    {
        var obj_name = "torey_date_start_input";
    }
    else if(caller == 'torey_search_ed')
    {
        var obj_name = "torey_date_end_input";
    }
    //検索エリアの日付検索
    else if(caller == 'torey_search_st_in_search_area')
    {
        var obj_name = "search_torey_date_in_search_area_start";
    }
    else if(caller == 'torey_search_ed_in_search_area')
    {
        var obj_name = "search_torey_date_in_search_area_end";
    }
    //発生日検索
    else if(caller == 'torey_search_incident_date_st')
    {
        var obj_name = "search_incident_date_start";
    }
    else if(caller == 'torey_search_incident_date_ed')
    {
        var obj_name = "search_incident_date_end";
    }
     //画面左の日付検索
    else if(caller == 'renewal_search_st')
    {
        var obj_name = "renewal_date_start_input";
    }
    else if(caller == 'renewal_search_ed')
    {
        var obj_name = "renewal_date_end_input";
    }
    //検索エリアの日付検索
    else if(caller == 'assessment_search_st')
    {
        var obj_name = "assessment_date_start_input";
    }
    else if(caller == 'assessment_search_ed')
    {
        var obj_name = "assessment_date_end_input";
    }

    j$("#"+obj_name).val(ymd);
}
