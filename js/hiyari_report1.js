/******************************************************************************
 * file name   : hiyari_report1.js
 * description : 報告画面　旧デザイン（hiyari_report1.tpl）用のJSファイル
 ******************************************************************************/
 
/**
 * 実名表示（匿名にマウスオーバー）
 */
function name_popup_disp(real_name, name_title, e) {
    popupDetailGreen(
        new Array(
            "<font size=\'3\' face=\'ＭＳ Ｐゴシック, Osaka\' class=\'j12\'>" + name_title + "</font>",
            "<font size=\'3\' face=\'ＭＳ Ｐゴシック, Osaka\' class=\'j12\'>" + real_name + "</font>"
        ), 250, 50, e
    );
}
 
//===============================================
// 報告書のドラッグアンドドロップ関連の処理
//===============================================
j$(function(){
  //ドラッグアンドドロップを初期設定
  constructDragDrop();
});

/**
 * ドラッグアンドドロップの初期設定
 */
function constructDragDrop()
{
  //---------------メモ---------------
  //当HTMLのネーミングルール
  //ドラッグアンドドロップ対象のDIVタグのid属性
  //・dd1_item_report_{report_id}    レポートドラッグのドラッグ「レポート」領域(class属性:dd1_item)
  //・dd1_handle_report_{report_id}  レポートドラッグのドラッグ元「レポート」領域(class属性:dd1_handle)
  //・dd1_target_report_{report_id}  レポートドラッグのドラッグ先「レポート」領域(class属性:dd1_target)
  //・dd1_target_folder_{folder_id}  レポートドラッグのドラッグ先「フォルダ」領域(class属性:dd1_target)
  //----------------------------------

  //全てのDIVオブジェクトに対して
  var divs = document.getElementsByTagName('div');
  for (var i = 0, j = divs.length; i < j; i++)
  {
    //ドラッグオブジェクトの場合
    if (divs[i].className == 'dd1_item')
    {
      var item_obj = divs[i];
      var item_obj_id = item_obj.id;
      var handle_obj_id = item_obj_id.replace("item","handle");

      //ドラッグアンドドロップ名「dd1」でドラッグオブジェクトを生成
      var doc = new YAHOO.util.DD(item_obj_id, 'dd1');

      //ドラッグ開始オブジェクトを設定
      doc.setHandleElId(handle_obj_id);

      //ドラッグアンドドロップ開始点
      doc.startPos = YAHOO.util.Dom.getXY(item_obj);

      //ドラッグアンドドロップ開始時の処理登録
      doc.startDrag = function (x, y)
      {
          //ドラッグオブジェクト取得
          var item_obj = this.getDragEl();

          //色を付ける。
          item_obj.style.backgroundColor = '#fefe83';

          //最前へ。
          item_obj.style.zIndex = 999;
      }

      //ドラッグアンドドロップ終了時の処理登録
      doc.endDrag = function (x, y)
      {
          //ドラッグオブジェクト取得
          var item_obj = this.getDragEl();

          //ドラッグ開始点へ。
          YAHOO.util.Dom.setXY(item_obj, this.startPos);

          //色消す。
          item_obj.style.backgroundColor = '';

          //最背へ。
          item_obj.style.zIndex = 0;
      }

      //ドラッグドロップのオブジェクト関係が正しいか検証します。
      doc.canAccept = function(item_obj, target_obj)
      {
          //対象チェック
          //ドラッグ先以外の場合はNG(YAHOO.util.DDオブジェクトがこの対象となる。)
          if(target_obj.className != 'dd1_target'){
              return false;
          }

          //自身へのドラッグの場合はNG
          var report_id = item_obj.id.replace("dd1_item_report_","");
          var target_report_id = target_obj.id.replace("dd1_target_report_","");
          if(report_id == target_report_id){
              return false;
          }

          //ドラッグ先がレポートの場合
          if(target_obj.id.indexOf("report") != -1){
              //「全て」以外のフォルダでドラッグした場合や、割付利用権限がない場合はNG
              is_report_classification_usable = j$("input[name='is_report_classification_usable']").val();
              if (folder_id != "" || !is_report_classification_usable){
                return false;
              }

              //ドラッグ元とドラッグ先が同一リンクの場合はNG
              var report_link_id1 = j$("#link_id_for_" + report_id).val();
              var report_link_id2 = j$("#link_id_for_" + target_report_id).val();
              if(report_link_id1 != "" && report_link_id1 == report_link_id2){
                  return false;
              }
          }

          //ゴミ箱フォルダでドラッグした場合
          var target_folder_id = target_obj.id.replace("dd1_target_folder_","");
          if (folder_id == "delete"){
              //「全て」以外のフォルダへのドラッグはNG
              if(target_obj.id.indexOf("folder") != -1){
                  if(target_folder_id != ""){
                      return false;
                  }
              }
          }

          //分類フォルダでドラッグした場合
          if (folder_id != "" && folder_id != "delete"){
              //「ゴミ箱」フォルダへのドラッグはNG
              if(target_obj.id.indexOf("folder") != -1){
                  if(target_folder_id == "delete"){
                      return false;
                  }
              }
          }

          //「ゴミ箱」フォルダへドラッグした場合
          if(target_obj.id.indexOf("folder") != -1){
              if(target_folder_id == "delete"){
                  //割付されている報告の場合はNG
                  if (j$("#link_status_for_" + report_id).val() != 0){
                    return false;
                  }
              }
          }

          return true;
      }

      doc.onDragEnter = function(e, id)
      {
          //ドラッグオブジェクト取得
          var item_obj   = this.getDragEl();
          //ドラッグ先オブジェクト取得
          var target_obj = YAHOO.util.DDM.getElement(id);

          //対象チェック
          if(!this.canAccept(item_obj, target_obj)){
              return;
          }

          //ドロップ色設定
          target_obj.style.backgroundColor = "orange";
      }

      doc.onDragOut = function(e, id)
      {
          //ドラッグオブジェクト取得
          var item_obj   = this.getDragEl();
          //ドラッグ先オブジェクト取得
          var target_obj = YAHOO.util.DDM.getElement(id);

          //対象チェック
          if(!this.canAccept(item_obj, target_obj)){
              return;
          }

          //ドロップ色クリア
          target_obj.style.backgroundColor = "";
      }

      //ドラッグアンドドロップ実行時の処理登録
      doc.onDragDrop = function (e, id)
      {
          //ドラッグオブジェクト取得
          var item_obj   = this.getDragEl();
          //ドラッグ先オブジェクト取得
          var target_obj = YAHOO.util.DDM.getElement(id);

          //対象チェック
          if(!this.canAccept(item_obj, target_obj)){
              return;
          }

          //ドロップ色クリア
          target_obj.style.backgroundColor = "";

          //ドロップ先がレポートの場合
          if(target_obj.id.indexOf("report") != -1){
              //レポートリンク追加
              var report_id = item_obj.id.replace("dd1_item_report_","");
              var target_report_id = target_obj.id.replace("dd1_target_report_","");
              rp_action_add_update_same_report_link(target_report_id,report_id);//対象レポ:target_report_id、追加レポ:report_id
          }
          //ドロップ先がフォルダの場合
          else if(target_obj.id.indexOf("folder") != -1){
              //レポートの分類変更
              var report_id = item_obj.id.replace("dd1_item_report_","");
              var target_folder_id = target_obj.id.replace("dd1_target_folder_","");
              rp_action_change_report_classification(target_folder_id,report_id);
          }
      }
    }
    //ドロップ先オブジェクトの場合
    else if (divs[i].className == 'dd1_target'){
      //ドロップ先オブジェクト生成
      var target_obj = divs[i];
      new YAHOO.util.DDTarget(target_obj.id, 'dd1');
    }
  }
}   
