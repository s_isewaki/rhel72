function link() {
	var form = document.createElement('form');
	form.target = '_blank';
	form.method = link.arguments[0];
	form.action = link.arguments[1];
	for (i = 2; i < link.arguments.length; i+=2) {
		var param = document.createElement('input');
		param.type  = 'hidden';
		param.name  = link.arguments[i];
		param.value = link.arguments[i+1];
		form.appendChild(param);
	}
	document.body.appendChild(form);
	form.submit();
}

function appli(appli,param) {
	try {
		var obj = new ActiveXObject('WScript.Shell');
		obj.Run(appli+param);
	} catch (e) {
		alert('プログラムが実行できませんでした。');
	}
}