function research()
{
	document.training_form.target = "";
	document.training_form.submit();
}

function report_regist()
{
	var wk_apply_id = '';
	var wk_report_date = '';

	if(confirm('報告日を登録しますか？')){
		for (i = 0; i < document.all.length; i++) {
			pos = document.all[i].id.indexOf('seminar_apply_id');
			if (pos == 0){
				if (wk_apply_id != ''){
					wk_apply_id += ',';
					wk_report_date += ',';
				}
				wk_apply_id += document.all[i].value;
				if (
					document.getElementById('report_year_' + document.all[i].value).value != '-'
					&& document.getElementById('report_month_' + document.all[i].value).value != '-'
					&& document.getElementById('report_day_' + document.all[i].value).value != '-'
				){
					wk_report_date += document.getElementById('report_year_' + document.all[i].value).value;
					wk_report_date += '-';
					wk_report_date += document.getElementById('report_month_' + document.all[i].value).value;
					wk_report_date += '-';
					wk_report_date += document.getElementById('report_day_' + document.all[i].value).value;
				}
			}
		}
		document.training_form.regist_flg.value = 'true';
		document.training_form.all_seminar_apply_id.value = wk_apply_id;
		document.training_form.all_report_date.value = wk_report_date;
		document.training_form.target = "";
		document.training_form.submit();
	}
	else{
		// 何もしない
	}
}

function get_level_date_regist(){

	// 2012/04/23 Yamagawa upd(s)
	/*
	var moduleName = "../../cl_training_progress";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.emp_id = document.training_form.target_emp_id.value;
	params.year = document.training_form.search_year.value;
	params.mode = 'detail';
	params.get_level_date_regist = 'true';
	params.auth_year_level1 = document.training_form.auth_year_level1.value;
	params.auth_month_level1 = document.training_form.auth_month_level1.value;
	params.auth_day_level1 = document.training_form.auth_day_level1.value;
	params.auth_year_level2 = document.training_form.auth_year_level2.value;
	params.auth_month_level2 = document.training_form.auth_month_level2.value;
	params.auth_day_level2 = document.training_form.auth_day_level2.value;
	params.auth_year_level3 = document.training_form.auth_year_level3.value;
	params.auth_month_level3 = document.training_form.auth_month_level3.value;
	params.auth_day_level3 = document.training_form.auth_day_level3.value;
	params.auth_year_level4 = document.training_form.auth_year_level4.value;
	params.auth_month_level4 = document.training_form.auth_month_level4.value;
	params.auth_day_level4 = document.training_form.auth_day_level4.value;
	params.auth_year_level5 = document.training_form.auth_year_level5.value;
	params.auth_month_level5 = document.training_form.auth_month_level5.value;
	params.auth_day_level5 = document.training_form.auth_day_level5.value;
	params.p_session = document.training_form.session.value;

	//ウィンドウサイズなどのオプション
	//var h = window.screen.availHeight;
	//var w = window.screen.availWidth;
	var w = 880;
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option,'cl_training_progress',params.p_session);
	*/
	document.training_form.level_date_regist.value = 'true';
	document.training_form.target = "_self";
	document.training_form.action = "./cl_common_call.php";
	document.training_form.submit();
	// 2012/04/23 Yamagawa upd(e)

}

function show_cal(seminar_apply_id){

}

function openEmployeeList(item_id) {

	document.training_form.input_div.value = item_id;

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;

	window.open('', 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	document.training_form.action="cl_emp_list.php";
	document.training_form.target = "emplist";
	document.training_form.submit();
}

function add_target_list(item_id, emp_id, emp_nm){

	var arr_add_emp_id = emp_id.split(',');
	if (arr_add_emp_id.length > 1) {
		alert('職員は１名のみ指定してください。');
		return;
	}

	document.getElementById(item_id + '_id').value = emp_id;
	document.getElementById(item_id + '_nm').value = emp_nm;

}

function show_subwin(apply_mode,trip_division,short_wkfw_name,seminar_apply_id){
	//------------------------------------------------------------------------------------------
	//子画面のモジュール名
	//------------------------------------------------------------------------------------------
	var moduleName = "cl_approval_application";

	//------------------------------------------------------------------------------------------
	//リクエストパラメータをオブジェクトで作成
	//------------------------------------------------------------------------------------------
	var params = new Object();
	params.apply_mode		= apply_mode;									// 申請・報告の区分
	params.trip_division	= trip_division;	// 日程
	params.short_wkfw_name	= short_wkfw_name;			// 役職
	params.seminar_apply_id	= seminar_apply_id;

	//------------------------------------------------------------------------------------------
	//ウィンドウサイズなどのオプション
	//var h = window.screen.availHeight;
	//var w = window.screen.availWidth;
	//w = 1024;
	//------------------------------------------------------------------------------------------
	var w = 1090;
	var h = 600;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	var session = document.training_form.session.value;
	//------------------------------------------------------------------------------------------
	//子画面を開く
	//------------------------------------------------------------------------------------------
	commonOpen(moduleName,params,option,'cl_approval_apprication',session);
}
