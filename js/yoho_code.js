var yoho_code = {
	'P医師の指示通り': '001',
	'P痛い時': '002',
	'P痛い時１日３回まで': '003',
	'P発熱時': '004',
	'P発熱時１日３回まで': '005',
	'P３８℃以上の発熱時': '006',
	'P発熱時または痛い時': '007',
	'P下痢の時': '008',
	'P便秘の時': '009',
	'P吐気がする時': '010',
	'P吐気が強い時': '011',
	'P嘔吐した時': '012',
	'P腹痛の時': '013',
	'P頭痛時': '014',
	'P胸が痛い時': '015',
	'P心臓の発作時': '016',
	'Pケイレンした時': '017',
	'P不安・イライラする時': '018',
	'P不穏時': '019',
	'P出血した時': '020',
	'Pかゆい時': '021',
	'P眠れない時': '022',
	'P尿が出ない時': '023',
	'P頻尿時': '024',
	'P血尿の時': '025',
	'P呼吸が苦しい時': '026',
	'Pタキソール投与３０分前': '027',
	'P咳がでる時': '028',
	'P痰がでる時': '029',
	'P血圧１８０以上の時': '030',
	'P舌下で服用': '031',
	'Pうがい用': '032',
	'P喘息の発作時': '033',
	'P食事できない時': '034',
	'P動悸時': '035',
	'P歯痛時': '036',
	'P麻酔が切れて痛い時': '037',

	'１日1回起床時': '101',
	'１日1回朝食直前': '102',
	'１日1回朝食前': '103',
	'１日1回朝食後': '104',
	'１日1回朝〜昼食間': '105',
	'１日1回朝食直後': '106',
	'１日1回朝食中': '107',
	'１日1回昼食直前': '108',
	'１日1回昼食前': '109',
	'１日1回昼食後': '110',
	'１日1回昼〜夕食間': '111',
	'１日1回昼食直後': '112',
	'１日1回昼食中': '113',
	'１日1回夕食直前': '114',
	'１日1回夕食前': '115',
	'１日1回夕食後': '116',
	'１日1回夕〜眠前食間': '117',
	'１日1回夕食直後': '118',
	'１日1回夕食中': '119',
	'１日1回眠前': '120',
	'１日1回': '121',
	'１日1回24時間おき': '122',
	'１日2回起床時、朝食直前': '201',
	'１日2回起床時、朝食前': '202',
	'１日2回起床時、朝食後': '203',
	'１日2回起床時、朝〜昼食間': '204',
	'１日2回起床時、朝食直後': '205',
	'１日2回起床時、朝食中': '206',
	'１日2回起床時、昼食直前': '207',
	'１日2回起床時、昼食前': '208',
	'１日2回起床時、昼食後': '209',
	'１日2回起床時、昼〜夕食間': '210',
	'１日2回起床時、昼食直後': '211',
	'１日2回起床時、昼食中': '212',
	'１日2回起床時、夕食直前': '213',
	'１日2回起床時、夕食前': '214',
	'１日2回起床時、夕食後': '215',
	'１日2回起床時、夕〜眠前食間': '216',
	'１日2回起床時、夕食直後': '217',
	'１日2回起床時、夕食中': '218',
	'１日2回眠前、朝食直前': '219',
	'１日2回眠前、朝食前': '220',
	'１日2回眠前、朝食後': '221',
	'１日2回眠前、朝食直後': '222',
	'１日2回眠前、朝食中': '223',
	'１日2回眠前、昼食直前': '224',
	'１日2回眠前、昼食前': '225',
	'１日2回眠前、昼食後': '226',
	'１日2回眠前、昼食直後': '227',
	'１日2回眠前、昼食中': '228',
	'１日2回眠前、夕食直前': '229',
	'１日2回眠前、夕食前': '230',
	'１日2回眠前、夕食後': '231',
	'１日2回眠前、夕食直後': '232',
	'１日2回眠前、夕食中': '233',
	'１日2回朝、昼食直前': '234',
	'１日2回朝、昼食前': '235',
	'１日2回朝、昼食後': '236',
	'１日2回朝〜昼、昼〜夕食間': '237',
	'１日2回朝、昼食直後': '238',
	'１日2回朝、昼食中': '239',
	'１日2回昼、夕食直前': '240',
	'１日2回昼、夕食前': '241',
	'１日2回昼、夕食後': '242',
	'１日2回昼〜夕、夕〜眠前食間': '243',
	'１日2回昼、夕食直後': '244',
	'１日2回昼、夕食中': '245',
	'１日2回朝、夕食直前': '246',
	'１日2回朝、夕食前': '247',
	'１日2回朝、夕食後': '248',
	'１日2回朝〜昼、夕〜眠前食間': '249',
	'１日2回朝、夕食直後': '250',
	'１日2回朝、夕食中': '251',
	'１日2回起床時、眠前': '252',
	'１日2回': '253',
	'１日2回12時間おき': '254',
	'１日3回起床時、朝昼食直前': '301',
	'１日3回起床時、朝昼食前': '302',
	'１日3回起床時、朝昼食後': '303',
	'１日3回朝〜昼、昼〜夕、夕〜眠前食間': '304',
	'１日3回起床時、朝昼食直後': '305',
	'１日3回起床時、朝昼食中': '306',
	'１日3回起床時、朝夕食直前': '307',
	'１日3回起床時、朝夕食前': '308',
	'１日3回起床時、朝夕食後': '309',
	'１日3回起床時、朝夕食直後': '310',
	'１日3回起床時、朝夕食中': '311',
	'１日3回起床時、昼夕食直前': '312',
	'１日3回起床時、昼夕食前': '313',
	'１日3回起床時、昼夕食後': '314',
	'１日3回起床時、昼夕食直後': '315',
	'１日3回起床時、昼夕食中': '316',
	'１日3回眠前、朝昼食直前': '317',
	'１日3回眠前、朝昼食前': '318',
	'１日3回眠前、朝昼食後': '319',
	'１日3回眠前、朝昼食直後': '320',
	'１日3回眠前、朝昼食中': '321',
	'１日3回眠前、朝夕食直前': '322',
	'１日3回眠前、朝夕食前': '323',
	'１日3回眠前、朝夕食後': '324',
	'１日3回眠前、朝夕食直後': '325',
	'１日3回眠前、朝夕食中': '326',
	'１日3回眠前、昼夕食直前': '327',
	'１日3回眠前、昼夕食前': '328',
	'１日3回眠前、昼夕食後': '329',
	'１日3回眠前、昼夕食直後': '330',
	'１日3回眠前、昼夕食中': '331',
	'１日3回朝昼夕食直前': '332',
	'１日3回朝昼夕食前': '333',
	'１日3回朝昼夕食後': '334',
	'１日3回朝昼夕食直後': '335',
	'１日3回朝昼夕食中': '336',
	'１日3回起床時、朝〜昼/昼〜夕食間': '337',
	'１日3回起床時、朝〜昼/夕〜眠前食間': '338',
	'１日3回起床時、昼〜夕/夕〜眠前食間': '339',
	'１日3回眠前、朝〜昼/昼〜夕食間': '340',
	'１日3回眠前、朝〜昼/夕〜眠前食間': '341',
	'１日3回眠前、昼〜夕/夕〜眠前食間': '342',
	'１日3回': '343',
	'１日3回8時間おき': '344',
	'１日4回起床時、朝昼夕食前': '402',
	'１日4回起床時、朝昼夕食後': '403',
	'１日4回起床時、朝昼夕食直後': '404',
	'１日4回起床時、朝昼夕食中': '405',
	'１日4回起床時、朝〜昼、昼〜夕、夕〜眠前食間': '406',
	'１日4回眠前、朝昼夕食直前': '407',
	'１日4回眠前、朝昼夕食前': '408',
	'１日4回眠前、朝昼夕食後': '409',
	'１日4回眠前、朝昼夕食直後': '410',
	'１日4回眠前、朝昼夕食中': '411',
	'１日4回': '412',
	'１日4回6時間おき': '413',
	'１日5回起床時、朝昼夕食直前、眠前': '501',
	'１日5回起床時、朝昼夕食前、眠前': '502',
	'１日5回起床時、朝昼夕食後、眠前': '503',
	'１日5回起床時、朝昼夕食直後、眠前': '504',
	'１日5回起床時、朝昼夕食中、眠前': '505',
	'１日5回起床時、': '505',
	'１日5回': '506',
	'１日5回5時間おき': '507',
	'１日6回4時間おき': '601',
	'１日7回': '701',
	'１日8回': '801',
	'１日8回3時間おき': '802',
	'１日9回': '901',
	
	'１日1回医師の指示通り': 'A01',
	'１日2回医師の指示通り': 'A02',
	'１日3回医師の指示通り': 'A03',
	'１日4回医師の指示通り': 'A04',
	'１日5回医師の指示通り': 'A05',
	'１日10回': 'A10',
	'１日11回': 'A11',
	'１日12回': 'A12',
	'１日13回': 'A13',
	'１日14回': 'A14',
	'１日15回': 'A15',

	'医師の指示通り': 'D01',
	'口内炎に使用': 'D02',
	'鼻炎に使用': 'D03',
	'うがい用': 'D04',
	'清拭用': 'D05',
	'洗浄用': 'D06',
	'散布すること': 'D07',
	'のどが痛い時': 'D08',
	'１日１回寝る前': 'D09',
	'１回１滴': 'D10',
	'１回２滴': 'D11',
	'１日１回': 'D12',
	'１日２回': 'D13',
	'１日３回': 'D14',
	'１日４回': 'D15',
	'１日５回': 'D16',
	'１日６回': 'D17',
	'胸痛時': 'D18',
	'発作時': 'D19',
	'鼻づまりの時': 'D20',
	'体温に暖めて使用': 'D21',
	'痛い時': 'D22',
	'かゆい時': 'D23',
	'厚く塗って下さい。': 'D24',
	'薄く塗って下さい。': 'D25',
	'入浴後に塗布': 'D26',
	'心臓の発作時': 'D27',
	'口が渇いた時': 'D28',
	'寝る前に挿入': 'D29',
	'１回１吸入': 'D30',
	'１回２吸入': 'D31',
	'１日４回まで': 'D32',
	'喘息の発作時': 'D33',
	'吸入薬使用後にうがいして下さい。': 'D34',
	'肛門内に注入': 'D35',
	'便秘の時': 'D36',
	'カテーテル潤滑消毒用': 'D37',
	'器具を使用する時': 'D38',
	'浣腸の時': 'D39',
	'１日３回まで痛い時': 'D40',
	'発熱時': 'D41',
	'１日３回まで発熱時': 'D42',
	'３８℃以上の発熱時': 'D43',
	'下痢の時': 'D44',
	'吐気がする時': 'D45',
	'吐気が強い時': 'D46',
	'嘔吐した時': 'D47',
	'腹痛の時': 'D48',
	'眠れない時': 'D49',
	'出血した時': 'D50',

	'G医師の指示通り': 'B01' ,
	'G口内炎に使用': 'B02' ,
	'G鼻炎に使用': 'B03' ,
	'Gうがい用': 'B04' ,
	'G清拭用': 'B05' ,
	'G洗浄用': 'B06' ,
	'G散布すること': 'B07' ,
	'Gのどが痛い時': 'B08' ,
	'G１日１回寝る前': 'B09' ,
	'G１回１滴': 'B10' ,
	'G１回２滴': 'B11' ,
	'G１日１回': 'B12' ,
	'G１日２回': 'B13' ,
	'G１日３回': 'B14' ,
	'G１日４回': 'B15' ,
	'G１日５回': 'B16' ,
	'G１日６回': 'B17' ,
	'G胸痛時': 'B18' ,
	'G発作時': 'B19' ,
	'G鼻づまりの時': 'B20' ,
	'G体温に暖めて使用': 'B21' ,
	'G痛い時': 'B22' ,
	'Gかゆい時': 'B23' ,
	'G厚く塗って下さい。': 'B24' ,
	'G薄く塗って下さい。': 'B25' ,
	'G入浴後に塗布': 'B26' ,
	'G心臓の発作時': 'B27' ,
	'G口が渇いた時': 'B28' ,
	'G寝る前に挿入': 'B29' ,
	'G１回１吸入': 'B30' ,
	'G１回２吸入': 'B31' ,
	'G１日４回まで': 'B32' ,
	'G喘息の発作時': 'B33' ,
	'G吸入薬使用後にうがいして下さい。': 'B34',
	'G肛門内に注入': 'B35' ,
	'G便秘の時': 'B36' ,
	'Gカテーテル潤滑消毒用': 'B37' ,
	'G器具を使用する時': 'B38' ,
	'G浣腸の時': 'B39' ,
	'G１日３回まで痛い時': 'B40' ,
	'G発熱時': 'B41' ,
	'G１日３回まで発熱時': 'B42' ,
	'G３８℃以上の発熱時': 'B43' ,
	'G下痢の時': 'B44' ,
	'G吐気がする時': 'B45' ,
	'G吐気が強い時': 'B46' ,
	'G嘔吐した時': 'B47' ,
	'G腹痛の時': 'B48' ,
	'G眠れない時': 'B49' ,
	'G出血した時': 'B50' ,
	'G１日１回１回１錠を': 'B51' ,
	'G１日２回１回１錠を': 'B52',
	'G１日３回１回１錠を': 'B53',
	'G１日４回１回１錠を': 'B54',
	'G１日５回１回１錠を': 'B55',
	'G１日６回１回１錠を': 'B56',
	'G毎日または隔日に少量ずつ塗布': 'B57',
	'G１日１〜数回塗布適量を': 'B58',
	'G１日数回塗布適量を': 'B59',
	'G１日数回': 'B60',
	'G１日１回医師の指示通り': 'C01',
	'G１日２回医師の指示通り': 'C02',
	'G１日３回医師の指示通り': 'C03',
	'G１日４回医師の指示通り': 'C04',
	'G１日５回医師の指示通り': 'C05',
	'G１日６回医師の指示通り': 'C06',
	'G１日７回医師の指示通り': 'C07',
	'G１日８回医師の指示通り': 'C08',
	'G１日９回医師の指示通り': 'C09',
	'G１日１０回医師の指示通り': 'C10',
	'G１日１回医師の指示通り': 'C01',
	'G１日２回医師の指示通り': 'C02',
	'G１日３回医師の指示通り': 'C03',
	'G１日４回医師の指示通り': 'C04',
	'G１日５回医師の指示通り': 'C05',
	'G１日６回医師の指示通り': 'C06',
	'G１日７回医師の指示通り': 'C07',
	'G１日８回医師の指示通り': 'C08',
	'G１日９回医師の指示通り': 'C09',
	'G１日１０回医師の指示通り': 'C10',
	'G在宅注射': 'Z01'
};


function getYohoCode(yoho_display_string,kubun){
	var yoho_comments = new Array();
	var yoho_str = "";
	var display_string = "";
	var match_pattern1 = /(\d+)年(\d+)月(\d+)日/g;

	yoho_display_string = yoho_display_string.replace(/じほう頓服/g,"じほう頓服<>");
	yoho_display_string = yoho_display_string.replace(/ /g,"");
	yoho_display_string = yoho_display_string.replace(/　/g,"");
	yoho_comments = yoho_display_string.split("|");
	yoho_str = yoho_comments[1] ? yoho_comments[0]+yoho_comments[1] : yoho_comments[0];
	if(kubun == "P"){
		yoho_str = yoho_comments[1];
		yoho_comments = yoho_str.split("<>");
		if(yoho_comments[0] == "じほう頓服") yoho_str = "P"+yoho_comments[1];
		else yoho_str = "999";
	}
	if(kubun == "G"){
		if(yoho_comments[0]) yoho_str = yoho_comments[0];
		if(yoho_comments[1]) yoho_str = yoho_comments[0] + yoho_comments[1];
		yoho_str = "G"+yoho_str;
	}
	if(yoho_comments[1] && yoho_comments[1].search(match_pattern1) >= 0){
		yoho_str = yoho_comments[0] ;
	}
	return yoho_code[yoho_str];
}

