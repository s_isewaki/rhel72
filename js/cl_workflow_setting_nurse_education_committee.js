function open_emplist(session) {

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'cl_emp_list.php';
	url += '?session=' + session + '&input_div=';
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function add_target_list(dummy, emp_id, emp_nm){

	var arr_add_emp_id = emp_id.split(',');
	var arr_add_emp_nm = emp_nm.split(',');

	var obj_id = document.getElementById('emp_id');
	var obj_nm = document.getElementById('emp_nm');
	var obj_nm_view = document.getElementById('emp_nm_view');

	var arr_id = obj_id.value.split(',');

	var id_build = obj_id.value;
	var nm_build = obj_nm.value;
	var nm_view_build = obj_nm_view.innerHTML;

	var bolalready = false;

	for (i = 0; i < arr_add_emp_id.length; i++){

		bolalready = false;
		for (j = 0; j < arr_id.length; j++){
			if (arr_add_emp_id[i] == arr_id[j]) {
				bolalready = true;
				break;
			}
		}

		if (!bolalready) {

			if (obj_id.value.length > 0) {
				id_build += ',';
				nm_build += ',';
				nm_view_build += ',';
			}

			id_build += arr_add_emp_id[i];
			nm_build += arr_add_emp_nm[i];
			nm_view_build += "<a href=\"javascript:delete_emp('" + arr_add_emp_id[i] + "', '" + arr_add_emp_nm[i] + "');\">";
			nm_view_build += arr_add_emp_nm[i];
			nm_view_build += "</a>";

		}
	}

	obj_id.value = id_build;
	obj_nm.value = nm_build;
	obj_nm_view.innerHTML = nm_view_build;

}

function delete_emp(emp_id, emp_nm){

	if(!confirm("「" + emp_nm + "」さんを削除します。よろしいですか？")) {
		return;
	}

	var obj_id = document.getElementById('emp_id');
	var obj_nm = document.getElementById('emp_nm');
	var obj_nm_view = document.getElementById('emp_nm_view');

	var arr_id = obj_id.value.split(',');
	var arr_nm = obj_nm.value.split(',');

	var id_build = '';
	var nm_build = '';
	var nm_view_build = '';

	var bolalready = false;

	for (i = 0; i < arr_id.length; i++){
		if (emp_id == arr_id[i]) {
			// 削除対象は再表示しない
		} else {

			if (id_build.length != 0) {
				id_build += ',';
				nm_build += ',';
				nm_view_build += ',';
			}

			id_build += arr_id[i];
			nm_build += arr_nm[i];

			// 画面表示部のみ生成し直す
			nm_view_build += "<a href=\"javascript:delete_emp('" + arr_id[i] + "', '" + arr_nm[i] + "');\">";
			nm_view_build += arr_nm[i];
			nm_view_build += "</a>";
		}
	}

	obj_id.value = id_build;
	obj_nm.value = nm_build;
	obj_nm_view.innerHTML = nm_view_build;

}

function emp_clear(){
	document.getElementById('emp_id').value = '';
	document.getElementById('emp_nm').value = '';
	document.getElementById('emp_nm_view').innerHTML = '';
}

function regist(){
	document.wkfw.regist_flg.value = 'true';
	document.wkfw.submit();
}
