//院外研修一覧から研修を選択した時の処理
function makeEvaluationList(levelup_apply_id,apply_id,evaluation_division,login_user,level,mode)
{
	// レベルアップ評価データ
	jQuery.getJSON(
		'cl/common/cl_levelup_evaluation_list_json.php'													,	// データ取得スクリプト
		{levelup_apply_id:levelup_apply_id,apply_id:apply_id,evaluation_division:evaluation_division,login_user:login_user,level:level}	,	// パラメーター
		function( param ) {			// 画面表示処理

			// 所属長データ設定
			setData_Supervisor(param);

			if(param.levelup_apply_id != ''){
				// 評価表データ設定
				setData_EvaluationList(param,mode);

				// 同僚評価者設定
				setData_ColleagueEvaluator(param);
			}
			//活性・非活性制御
			display_controle(param,mode);
		}
	);
}

function setData_Supervisor(param)
{
	var html = "";

	for (i = 0; i < param.supervisor_nm.length; i++) {
		if(i == 0){
			html = param.supervisor_nm[i].emp_full_nm;
		}else{
			html += "," + param.supervisor_nm[i].emp_full_nm;
		}
		if(i != param.supervisor_nm.length -1){
			html += "</br>";
		}
	}
	document.getElementById('supervisor_nm').innerHTML = html;
	//2012/05/17 K.Fujii ins(s)
	document.getElementById('hdn_supervisor_nm').value = html;
	//2012/05/17 K.Fujii ins(e)
	document.getElementById('clp_levelup_apply_id').value = param.levelup_apply_id;
	//2012/05/17 K.Fujii ins(s)
	document.getElementById('hdn_levelup_apply_id').value = param.levelup_apply_id;
	//2012/05/17 K.Fujii ins(e)
	document.getElementById('clp_levelup_emp_id').value = param.levelup_emp_id;
	document.getElementById('leveup_apply_count').value = param.leveup_apply_count;

	document.getElementById('clp_self_evaluation_date').value = param.self_evaluation_date;
	document.getElementById('clp_supervisor_emp_id').value = param.supervisor_emp_id;
	document.getElementById('clp_supervisor_evalution_date').value = param.supervisor_evalution_date;
	document.getElementById('clp_colleague_division').value = param.colleague_division;
	document.getElementById('clp_levelup_evaluation_id').value = param.levelup_evaluation_id;

	document.getElementById('levelup_apply_no').value = param.levelup_apply_no;
	document.getElementById('hdn_levelup_apply_no').value = param.levelup_apply_no;

	// 2012/09/24 Yamagawa add(s)
	if (document.all('applicant_nm')) {
		document.getElementById('applicant_nm').innerHTML = param.applicant_nm;
	}
	if (document.all('applicant_affiliation')) {
		document.getElementById('applicant_affiliation').innerHTML = param.applicant_affiliation;
	}
	// 2012/09/24 Yamagawa add(e)

}

// 評価表データ設定
function setData_EvaluationList(param,mode)
{

	var evaluation_list = param.evaluation_list;
	var eval_div = param.evaluation_division;
	var html = "";



	//同僚評価の表示判定
	//・本人・所属長の評価表(eval_div == "1")
	//・承認系の画面(mode == "ApvDetail")
	var colleague_disp_flg = false;
	if(eval_div == "1" && mode == "ApvDetail")
	{
		colleague_disp_flg = true;
	}

	//同僚評価の合計値(集計用)
	var total_eval3_value = 0;
	var total_eval4_value = 0;


	//データ件数を格納
	document.getElementById("clp_item_cnt").value = evaluation_list.length;

	html += '<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">';
	html += '	<tr>';
	html += '		<td width="50" height="22" align="center" valign="top" >';
	html += '			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>項目</b></font>';
	html += '		</td>';

	html += '		<td width="630" align="center" valign="top"  colspan="2">';
	html += '			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>評価内容</b></font>';
	html += '		</td>';

	if(eval_div == "1")
	{
		if(colleague_disp_flg)
		{
			html += '		<td width="60" align="center">';
			html += '			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>同僚</br>評価１</b></font>';
			html += '		</td>';
			html += '		<td width="60" align="center">';
			html += '			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>同僚</br>評価２</b></font>';
			html += '		</td>';
		}
		html += '		<td width="60" align="center">';
		html += '			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>自己</br>評価</b></font>';
		html += '		</td>';
		html += '		<td width="60" align="center">';
		html += '			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>所属長</br>評価</b></font>';
		html += '		</td>';
	}
	else
	{
		html += '		<td width="60" align="center">';
		html += '			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>同僚</br>評価</b></font>';
		html += '		</td>';
	}
	html += '	</tr>';

	var last_cat_id = "";
	var last_grp_id = "";
	for (i = 0; i < evaluation_list.length; i++) {
		if(last_cat_id != evaluation_list[i].category_id){
			//評価カテゴリ
			html += '<tr>';
			html += '		<td width="50" style=" align:right; writing-mode:tb-rl" rowspan="' + param.evaluation_list[i].cat_rowspan + '"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += '			' + param.evaluation_list[i].category_name;
			html += '		</font></td>';

			//評価基準_NO.
			html += '		<td width="40" align="center" valign="top" rowspan="' + param.evaluation_list[i].grp_rowspan + '"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += '			' + param.evaluation_list[i].group_no;
			html += '		</font></td>';

			//評価基準_文言
			html += '		<td width="590" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += '				' + param.evaluation_list[i].group_name;
			html += '		</font></td>';

			//評価
			if(colleague_disp_flg)
			{
				html += '		<td width="60" align="center">';
				html += '		</td>';
				html += '		<td width="60" align="center">';
				html += '		</td>';
			}
			html += '		<td width="60" align="center">';
			html += '		</td>';
			if(eval_div == "1")
			{
				html += '		<td width="60" align="center">';
				html += '		</td>';

			}
			html += '	</tr>';
		}else if(last_grp_id != evaluation_list[i].group_id) {
			//評価基準_NO.
			html += '<tr>';
			html += '		<td width="40" align="center" valign="top" rowspan="' + param.evaluation_list[i].grp_rowspan + '"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += '			' + param.evaluation_list[i].group_no;
			html += '		</font></td>';

			//評価基準_文言
			html += '		<td width="590" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += '				' + param.evaluation_list[i].group_name;
			html += '		</font></td>';

			//評価
			if(colleague_disp_flg)
			{
				html += '		<td width="60" align="center">';
				html += '		</td>';
				html += '		<td width="60" align="center">';
				html += '		</td>';
			}
			html += '		<td width="60" align="center">';
			html += '		</td>';
			if(eval_div == "1"){
				html += '		<td width="60" align="center">';
				html += '		</td>';
			}
			html += '	</tr>';
		}

		//必達項目の場合は色を付ける
		var item_line_color_html = '';
		if(param.evaluation_list[i].must_value != '')
		{
			item_line_color_html = 'bgcolor="#ffff66"';//黄色
		}


		//評価項目
		html += '<tr>';
		html += '	<td width="590" align="left" ' + item_line_color_html + ' ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
		html += '		' + param.evaluation_list[i].item_name;
		html += '	</font></td>';

		if(colleague_disp_flg)
		{
			if(param.evaluation_list[i].eval3 != '')
			{
				total_eval3_value = total_eval3_value + (param.evaluation_list[i].eval3 -0);//数値として加算
			}
			if(param.evaluation_list[i].eval4 != '')
			{
				total_eval4_value = total_eval4_value + (param.evaluation_list[i].eval4 -0);//数値として加算
			}
			html += '	<td width="60" align="center" ' + item_line_color_html + ' ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += param.evaluation_list[i].eval3;
			html += '	</font></td>';
			html += '	<td width="60" align="center" ' + item_line_color_html + ' ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += param.evaluation_list[i].eval4;
			html += '	</font></td>';
		}

		//評価
		html += '	<td width="60" align="center" ' + item_line_color_html + ' ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
		html += '		<select id="clp_cmb_eval_value1_' + i + '" name="clp_cmb_eval_value1_' + i + '" onchange="set_total_eval(1);" disabled>';
		html += '			<option value=""></option>';
		if(param.evaluation_list[i].eval_disp_4_flg == "1"){
			html += '			<option value="4"'; if(param.evaluation_list[i].eval1 == "4"){html +='selected';} html += '>4</option>';
		}
		if(param.evaluation_list[i].eval_disp_3_flg == "1"){
			html += '			<option value="3"'; if(param.evaluation_list[i].eval1 == "3"){html +='selected';} html += '>3</option>';
		}
		if(param.evaluation_list[i].eval_disp_2_flg == "1"){
			html += '			<option value="2"'; if(param.evaluation_list[i].eval1 == "2"){html +='selected';} html += '>2</option>';
		}
		if(param.evaluation_list[i].eval_disp_1_flg == "1"){
			html += '			<option value="1"'; if(param.evaluation_list[i].eval1 == "1"){html +='selected';} html += '>1</option>';
		}
		html += '		</select>';
		html += '	</font></td>';
		//2012/05/17 K.Fujii ins(s)
		html += '<input type="hidden" id="hdn_cmb_eval_value1_' + i + '" name="hdn_cmb_eval_value1_' + i + '" value="' + param.evaluation_list[i].eval1 + '">'
		html += '<input type="hidden" id="clp_evaluation_id_' + i + '" name="clp_evaluation_id_' + i + '" value="' + param.evaluation_list[i].evaluation_id + '">';
		html += '<input type="hidden" id="clp_category_id_' + i + '" name="clp_category_id_' + i + '" value="' + param.evaluation_list[i].category_id + '">';
		html += '<input type="hidden" id="clp_group_id_' + i + '" name="clp_group_id_' + i + '" value="' + param.evaluation_list[i].group_id + '">';
		html += '<input type="hidden" id="clp_item_id_' + i + '" name="clp_item_id_' + i + '" value="' + param.evaluation_list[i].item_id + '">';
		html += '<input type="hidden" id="clp_levelup_evaluation_value_id1_' + i + '" name="clp_levelup_evaluation_value_id1_' + i + '" value="' + param.evaluation_list[i].levelup_evaluation_value_id1 + '">';
		//2012/05/17 K.Fujii ins(e)
		if(eval_div == "1"){
			html += '	<td width="60" align="center" ' + item_line_color_html + ' ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
			html += '		<select id="clp_cmb_eval_value2_' + i + '" name="clp_cmb_eval_value2_' + i + '" onchange="set_total_eval(2);" disabled>';
			html += '			<option value=""></option>';
			if(param.evaluation_list[i].eval_disp_4_flg == "1"){
				html += '			<option value="4"'; if(param.evaluation_list[i].eval2 == "4"){html +='selected';} html += '>4</option>';
			}
			if(param.evaluation_list[i].eval_disp_3_flg == "1"){
				html += '			<option value="3"'; if(param.evaluation_list[i].eval2 == "3"){html +='selected';} html += '>3</option>';
			}
			if(param.evaluation_list[i].eval_disp_2_flg == "1"){
				html += '			<option value="2"'; if(param.evaluation_list[i].eval2 == "2"){html +='selected';} html += '>2</option>';
			}
			if(param.evaluation_list[i].eval_disp_1_flg == "1"){
				html += '			<option value="1"'; if(param.evaluation_list[i].eval2 == "1"){html +='selected';} html += '>1</option>';
			}
			html += '		</select>';
			html += '	</font></td>';
			//2012/05/17 K.Fujii ins(s)
			html += '<input type="hidden" id="hdn_cmb_eval_value2_' + i + '" name="hdn_cmb_eval_value2_' + i + '" value="' + param.evaluation_list[i].eval2 + '">'
			html += '<input type="hidden" id="clp_levelup_evaluation_value_id2_' + i + '" name="clp_levelup_evaluation_value_id2_' + i + '" value="' + param.evaluation_list[i].levelup_evaluation_value_id2 + '">';
			//2012/05/17 K.Fujii ins(e)
		}
		html += '</tr>';

		last_cat_id = evaluation_list[i].category_id;
		last_grp_id = evaluation_list[i].group_id;
	}

	//本人・師長評価の場合のみ、合計欄を表示する。
	if(eval_div == "1")
	{
		//合計
		html += '<tr>';
		html += '		<td align="center" valign="middle" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>合計</b></font></td>';
		if(colleague_disp_flg)
		{
			html += '		<td align="center" valign="middle"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">' + total_eval3_value + '</font></td>';
			html += '		<td align="center" valign="middle"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">' + total_eval4_value + '</font></td>';
		}
		html += '		<td align="center" valign="middle"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="total_eval1"></span></font></td>';
		html += '		<td align="center" valign="middle"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="total_eval2"></span></font></td>';
		html += '	</tr>';
	}

	html += '</table>';
	document.getElementById('clp_evaluation_list').innerHTML = html;
	//2012/05/17 K.fujii ins(s)
	document.getElementById('hdn_evaluation_list').value = html;
	document.getElementById('hdn_evaluation_list_length').value = evaluation_list.length;
	//2012/05/17 K.Fujii ins(e)

	//合計欄表示
	if(eval_div == "1")
	{
		set_total_eval(1);
		set_total_eval(2);
	}
}

//評価プルダウンの値を集計し、合計欄に合計値を設定
//eval_index 1:本人評価、2:師長評価
function set_total_eval(eval_index)
{
	//合計欄のSPANタグID
	var total_eval_span_id;
	//評価プルダウンのIDプリフィックス
	var eval_select_id_prefix;
	switch(eval_index)
	{
		case 1://本人評価
			total_eval_span_id = "total_eval1";
			eval_select_id_prefix = "clp_cmb_eval_value1_";
			break;
		case 2://師長評価
			total_eval_span_id = "total_eval2";
			eval_select_id_prefix = "clp_cmb_eval_value2_";
			break;
	}

	//合計欄
	var total_eval_span = document.getElementById(total_eval_span_id);

	//合計欄が存在する場合のみ処理を行う。
	if(total_eval_span)
	{
		//合計値(集計用)
		var total_eval_value = 0;

		//全プルダウンを取得
		var all_select_obj_arr = document.getElementsByTagName('select');
		for(var i = 0; i < all_select_obj_arr.length; i++)
		{
			var select_obj = all_select_obj_arr[i];

			//評価のプルダウンの場合
			if(select_obj.id.substring(0,eval_select_id_prefix.length) == eval_select_id_prefix)
			{
				//合計値に評価値を加算
				var val = select_obj.options[select_obj.selectedIndex].value;
				if(val != "")
				{
					total_eval_value = total_eval_value + (val - 0);//数値として加算
				}
			}
		}

		//合計値の表示を更新
		total_eval_span.innerHTML = total_eval_value;
	}
}



// 同僚評価者設定
function setData_ColleagueEvaluator(param)
{
	if(param.evaluation_division == "1"){
		var html = param.chief_emp_name + "," + param.colleague_emp_name;
		document.getElementById('colleague_emp_evaluator').innerHTML = html;
		//2012/05/17 K.Fujii ins(s)
		document.getElementById('hdn_colleague_emp_evaluator').value = html;
		//2012/05/17 K.Fujii ins(e)
	}
}

//活性・非活性制御
function display_controle(param, apl_kbn){

	var clp_item_cnt = parseInt(document.getElementById("clp_item_cnt").value);
	var eval_div = document.getElementById('evaluation_division').value;
	var leveup_apply_count = parseInt(param.leveup_apply_count);

	if(param.levelup_apply_id != ''){
		for(var i = 0;i<clp_item_cnt;i++){
			if(apl_kbn == "NewApl"){
				document.getElementById("clp_cmb_eval_value1_"+i).disabled = false;
			}else if(apl_kbn == "AplDetail"){
				if(eval_div == "1"){
					document.getElementById("clp_cmb_eval_value1_"+i).disabled = false;
					if(leveup_apply_count > 1){
						document.getElementById("clp_apply_btn").disabled = false;
					}
				}
			}else if(apl_kbn == "ApvDetail"){
				if(eval_div == "1"){
					document.getElementById("clp_cmb_eval_value2_"+i).disabled = false;
				}else{
					document.getElementById("clp_cmb_eval_value1_"+i).disabled = false;
				}
			}
		}
	}
	if(apl_kbn == "NewApl" || apl_kbn == "AplDetail"){
		if(eval_div == "1"){
			if(leveup_apply_count > 1){
				document.getElementById("clp_apply_btn").disabled = false;
			}
		}
	}
	// 2012/08/21 Yamagawa add(s)
	else if (apl_kbn == "ApvDetail") {
		if(eval_div == "1"){
			document.getElementById("clp_eval_copy").disabled = false;
		}
	}
	// 2012/08/21 Yamagawa add(e)
}

// 評価項目入力チェック
function CheckEvalationValue(kbn){

	var strBkColor = "FFB6C1";
	var err = "f";
	var result = true;
	var clp_item_cnt = parseInt(document.getElementById('clp_item_cnt').value);
	var eval_div = document.getElementById('evaluation_division').value

	var objEval1;
	var objEval2;

	var errMsg = ""

	for(var i = 0;i<clp_item_cnt;i++){
		if(eval_div == "1"){
			objEval1 = document.getElementById('clp_cmb_eval_value1_' + i);
			objEval2 = document.getElementById('clp_cmb_eval_value2_' + i);
			if(kbn == 1){
				if(objEval1.value == ""){
					err = "t";
					objEval1.style.backgroundColor = strBkColor;
				}else{
					objEval1.style.backgroundColor = "";
				}
			}else{
				if(objEval2.value == ""){
					err = "t";
					objEval2.style.backgroundColor = strBkColor;
				}else{
					objEval2.style.backgroundColor = "";
				}
			}
		}else{
			objEval1 = document.getElementById('clp_cmb_eval_value1_' + i);
			if(objEval1.value == ""){
				err = "t";
				objEval1.style.backgroundColor = strBkColor;
				var cnt_point= i+1
			}else{
				objEval1.style.backgroundColor = "";
			}
		}
	}

	if(err == "t"){
		result = false;
	}

	return result;

}
