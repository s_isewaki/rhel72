//==================================
//ラダー評価マスタJS
//==================================

	//---------------------------
	// レベル押下時のフラグ変更
	//---------------------------
	function change_level(target_level)
	{
		mainform.hdnlevel.value = target_level;
		mainform.submit();
	}

	//---------------------------
	// 各マスタ押下時のフラグ変更
	//---------------------------
	function change_mst(target_mst)
	{
		mainform.mode.value = "MstLoad";
		mainform.hdnmst.value = target_mst;
		mainform.submit();
	}

	//---------------------------
	// マスタ種類チェンジ時のコンボ値保持
	//---------------------------
	function change_mstcmb()
	{
		mainform.mode.value = "CatSel";
		mainform.submit();
	}

	//---------------------------
	// カテゴリチェンジ時のコンボ値保持
	//---------------------------
	function change_catcmb()
	{
		mainform.mode.value = "GrpSel";
		mainform.submit();
	}

	//---------------------------
	// ソート順序変更処理#
	//---------------------------
	function change_lank(rank_obj_name,add_value,index)
	{
		var rank_obj = document.getElementsByName(rank_obj_name)[0];
		var target_value = rank_obj.value;

		var change_flg = false;
		var all_inputs = document.getElementsByTagName("input");


		var AnsValue = parseInt(target_value) + add_value ;
		rank_obj.value = AnsValue;
		var AnsIndex = index + add_value;

		var objValue = document.getElementById(AnsIndex + "_RANK");
		objValue.value = parseInt(objValue.value) - add_value;
		
		mainform.mode.value = "rank_change";
		mainform.submit();
	}

	//---------------------------
	// 行追加
	//---------------------------
	function add_line(add_cnt)
	{
		mainform.hdnrec.value = add_cnt;
		mainform.mode.value = "ADD_LINE";
		mainform.submit();
	}

	//---------------------------
	// 行削除
	//---------------------------
	function del_line(del_cnt)
	{
		// 削除メッセージ出力
		if (document.getElementById(del_cnt + "_EXE").value != "") {
			strRes = confirm("削除します。よろしいですか？");
			if (strRes == false) {
				return false;
			}
		}
		mainform.hdnrec.value = del_cnt;
		mainform.mode.value = "DEL_LINE";
		mainform.submit();
	}

	//---------------------------
	// 更新ボタン押下時
	//---------------------------
	function output_update(dip_id)
	{
		var err_idx=0;

		// プルダウン未選択チェック
		// カテゴリ
		if (dip_id == 'evaluation_category'){

			//マスタ種類コンボチェック
			if(mainform.cmb_mst.value =="")
			{
				mainform.cmb_mst.style.backgroundColor="FFB6C1";
				alert("プルダウンが未選択です");
				return false;
			}else{
				mainform.cmb_mst.style.backgroundColor="";
			}
		} else if (dip_id == 'evaluation_group'){

			//マスタ種類コンボチェック
			if(mainform.cmb_mst.value =="")
			{
				mainform.cmb_mst.style.backgroundColor="FFB6C1";
				alert("マスタ種類プルダウンが未選択です");
				return false;
			}else{
				mainform.cmb_mst.style.backgroundColor="";
			}

			//カテゴリコンボチェック
			if(mainform.cmb_cate.value =="")
			{
				mainform.cmb_cate.style.backgroundColor="FFB6C1";
				alert("カテゴリプルダウンが未選択です");
				return false;
			}else{
				mainform.cmb_cate.style.backgroundColor="";
			}

		}

		for (i=0; i<mainform.dataCount.value; i++) {
			if (document.getElementById(i + "_TXT").value =="")
			{
				err_idx=1;
				document.getElementById(i + "_TXT").style.backgroundColor="FFB6C1";
			}else{
				document.getElementById(i + "_TXT").style.backgroundColor="";
			}



			if (dip_id == 'evaluation_group'){

				if (document.getElementById(i + "_SEQ").value.match(/[^0-9]+/))
				{
					err_idx=2;
					document.getElementById(i + "_SEQ").style.backgroundColor="FFB6C1";
				}else{
					document.getElementById(i + "_SEQ").style.backgroundColor="";
				}
			}
		}

		// 入力チェック結果メッセージの表示
		if( err_idx == 1 ){
			alert("更新内容に誤りがあります。入力項目を確認してください。");
			return false;
		}	else if( err_idx == 2 ){
			alert("数値を入力してください");
			return false;
		}

		mainform.mode.value = "UPDATA";
		mainform.submit();
	}

	//---------------------------
	// 非表示フラグチェック
	//---------------------------
	function chkValue(intLine)
	{
		var err_idx=0;

		if (document.getElementById(intLine + "_CHK").checked)
		{
			//2012/05/07 K.Fujii upd(s)
			//document.getElementById(intLine + "_CHK").value="1";
			document.getElementById(intLine + "_CHK").value="0";
			//2012/05/07 K.Fujii upd(e)
		}else{
			//2012/05/07 K.Fujii upd(s)
			//document.getElementById(intLine + "_CHK").value="0";
			document.getElementById(intLine + "_CHK").value="1";
			//2012/05/07 K.Fujii upd(e)
		}			
		
		mainform.mode.value = "rank_change";
		//mainform.submit();
	}


	//---------------------------
	// 項目ボタン押下時の処理
	//---------------------------
	function onclick_emp_list_btn(level,grop_id){

		var params = new Object();
		var session = document.getElementById('session').value;
		var moduleName = 'cl_workflow_mst_ludder_eval_item';

		if (level != ''){
			params.target_level = level;
		} else {
			params.target_level = '1';
		}
		params.master_kind_id = document.getElementById("cmb_mst").value;
		params.category_id = document.getElementById("cmb_cate").value;
		params.group_id = grop_id;
		params.mode = 'Call';
		params.p_session = session;

		//ウィンドウサイズなどのオプション
		wx = 720;
		wy = 600;
		var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + wx + ",height=" + wy;

		//子画面を開く
		commonOpen(moduleName,params,option,moduleName,session)
	}
