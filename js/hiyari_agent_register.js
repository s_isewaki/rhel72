function initPage(agent_ids, agent_names){
  if (document.getElementById('disp_area_1')){  
    document.getElementById('disp_area_ids_1').value   = agent_ids;
    document.getElementById('disp_area_names_1').value = agent_names;
    
    var html = "";
    if (agent_ids == ""){
      html = "&nbsp;";
    }
    else{
      var ids = agent_ids.split(",");
      var names = agent_names.split(",");
      for( var i=0 ; i < ids.length ; i++ ) {
        if (i > 0){
          html += ","
        }
        html += "<a href=\"javascript:delete_emp('" + ids[i] + "', '" + names[i] + "', '1');\">" + names[i] + "</a>";      
      }
    }
    document.getElementById('disp_area_1').innerHTML = html;
  }
}

function delete_emp(del_emp_id, del_emp_nm, input_div){
  if(confirm("「" + del_emp_nm + "」さんを削除します。よろしいですか？"))
  {
    ids   = "disp_area_ids_" + input_div;
    names = "disp_area_names_" + input_div;

    emp_ids = document.getElementById(ids).value;
    emp_nms = document.getElementById(names).value;

    new_emp_ids = "";
    new_emp_nms = "";

    cnt = 0;
    if(emp_ids != "") {

      arr_emp_id = emp_ids.split(",");
      arr_emp_nm = emp_nms.split(",");

      for(i=0; i<arr_emp_id.length; i++)
      {
        if(arr_emp_id[i] == del_emp_id)
        {
          continue;
        }
        else
        {
          if(cnt > 0)
          {
              new_emp_ids += ",";
            new_emp_nms += ",";
          }
            new_emp_ids += arr_emp_id[i];
          new_emp_nms += arr_emp_nm[i];
          cnt++;
        }
      }
    }
    document.getElementById(ids).value = new_emp_ids;
    document.getElementById(names).value = new_emp_nms;
    set_disp_area(new_emp_ids, new_emp_nms, input_div);
  }
}

// 日付チェック
function chkDate() {
  var start_date = document.getElementById('start_date').value;
  var end_date = document.getElementById('end_date').value;

  if(start_date != "" && end_date !="") {
    start_date = remove_slash(start_date);
    end_date = remove_slash(end_date);
    if(start_date > end_date) {
      alert('代行期間の前後が合っていません。');
      return false;
    }
  }
  return true;
}

// 代行者指定でログインユーザがセットされているかチェック
function chkTarget() {
  var emp_id = document.getElementById('emp_id').value;
  var emp_nm = document.getElementById('emp_nm').value;

  var ids = document.getElementById('disp_area_ids_1').value;

  arr_emp_id = ids.split(",");
  for(i=0; i<arr_emp_id.length; i++)
  {
    if(arr_emp_id[i] == emp_id)
    {
      alert("「" + emp_nm + "」さんは、決裁者のため代行者に設定できません。");
      return false;
    }
  }
  return true;
}

// 登録チェック
function regist(session) {
  if (chkDate() == true && chkTarget() == true) {
    document.mainform.action="hiyari_agent_register.php?session=" + session + "&mode=regist";
    document.mainform.submit();
  }
}

// 日付クリア
function date_clear(id) {
  var obj = document.getElementById(id);
  obj.value = '';
}

// 日付スラッシュ除去
function remove_slash(pTaget){
  var splitArray = null;
  var result = null;

  splitArray = pTaget.split('/');
  result = splitArray.join('');
  return result;
}

//子画面から呼ばれる関数
function add_emp_list(input_emp_id, input_emp_nm, input_div){
  ids   = "disp_area_ids_" + input_div;
  names = "disp_area_names_" + input_div;

  //既存入力されている職員
  emp_ids = document.getElementById(ids).value;
  emp_nms = document.getElementById(names).value;

  //子画面から入力された職員
  arr_input_emp_id   = input_emp_id.split(", ");
  arr_input_emp_name = input_emp_nm.split(", ");

  tmp_ids   = "";
  tmp_names = "";
  if(emp_ids != "")
  {
    arr_emp_id = emp_ids.split(",");

      for(i=0; i<arr_input_emp_id.length; i++)
    {
      dpl_flg = false;
      for(j=0; j<arr_emp_id.length; j++)
      {
        // 重複した場合
        if(arr_input_emp_id[i] == arr_emp_id[j])
        {
          dpl_flg = true;
        }
      }
      if(!dpl_flg)
      {
        if(tmp_ids != "")
        {
          tmp_ids   += ",";
          tmp_names += ",";
        }
        tmp_ids   += arr_input_emp_id[i];
        tmp_names += arr_input_emp_name[i];
      }
    }
  }
  else
  {
    for(i=0; i<arr_input_emp_id.length; i++)
    {
      if(tmp_ids != "")
      {
        tmp_ids   += ",";
        tmp_names += ",";
      }
      tmp_ids   += arr_input_emp_id[i];
      tmp_names += arr_input_emp_name[i];
    }
  }

  if(emp_ids != "" && tmp_ids != "")
  {
    emp_ids += ",";
  }
  emp_ids += tmp_ids;
  document.getElementById(ids).value = emp_ids;
  if(emp_nms != ""  && tmp_names != "") {
    emp_nms += ",";
  }
  emp_nms += tmp_names;
  document.getElementById(names).value = emp_nms;

  set_disp_area(emp_ids, emp_nms, input_div);
}


function set_disp_area(emp_id, emp_nm, input_div) {
  disp_area = "disp_area_" + input_div;

  arr_emp_id = emp_id.split(",");
  arr_emp_nm = emp_nm.split(",");
  disp_area_content = "";
  for(i=0; i<arr_emp_id.length; i++)
  {
    if(i > 0)
    {
      disp_area_content += ',';
    }

    disp_area_content += "<a href=\"javascript:delete_emp('" + arr_emp_id[i] + "', '" + arr_emp_nm[i] + "', '" + input_div + "');\">";
    disp_area_content += arr_emp_nm[i];
    disp_area_content += "</a>";
  }
  document.getElementById(disp_area).innerHTML = disp_area_content;
}


function call_emp_search(input_div, session){
  var childwin = null;

  dx = screen.availWidth - 10;
  dy = screen.top;
  base = 0;
  wx = 720;
  wy = 600;
  var url = 'hiyari_emp_list.php';
  url += '?session=' + session + '&input_div=' + input_div;
  childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

  childwin.focus();
}

function emp_clear(div){
  if(document.getElementById('disp_area_' + div)){
    document.getElementById('disp_area_' + div).innerHTML   = '&nbsp;';
    document.getElementById('disp_area_ids_' + div).value   = '';
    document.getElementById('disp_area_names_' + div).value = '';
  }

}
