var childwin;
function delete_analysis(del_analysis_id, del_analysis_nm, mode) {
  if(confirm("「" + del_analysis_nm + "」を削除します。よろしいですか？"))  {
      var ids   = "disp_area_ids_" + mode;
      var names = "disp_area_names_" + mode;

      var analysis_ids = document.getElementById(ids).value;
      var analysis_nms = document.getElementById(names).value;

      var new_analysis_ids = "";
      var new_analysis_nms = "";

      var cnt = 0;
      if(analysis_ids != "") {
          var arr_analysis_ids = analysis_ids.split(",");
          var arr_analysis_nms = analysis_nms.split(",");

          for(var i=0; i<arr_analysis_ids.length; i++) {
              if(arr_analysis_ids[i] == del_analysis_id) {
                  continue;
              } else {
                  if(cnt > 0) {
                      new_analysis_ids += ",";
                      new_analysis_nms += ",";
                  }
                  new_analysis_ids += arr_analysis_ids[i];
                  new_analysis_nms += arr_analysis_nms[i];
                  cnt++;
              }
          }
      }
      document.getElementById(ids).value = new_analysis_ids;
      document.getElementById(names).value = new_analysis_nms;
      set_analysis(new_analysis_ids, new_analysis_nms, mode);
  }
}

function delete_emp(del_emp_id, del_emp_nm, dummy)
{
  if(confirm("「" + del_emp_nm + "」さんを削除します。よろしいですか？"))
  {
      ids   = "disp_area_ids";
      names = "disp_area_names";

      emp_ids = document.getElementById(ids).value;
      emp_nms = document.getElementById(names).value;

      new_emp_ids = "";
      new_emp_nms = "";

      cnt = 0;
      if(emp_ids != "") {

          arr_emp_id = emp_ids.split(",");
          arr_emp_nm = emp_nms.split(",");

          for(i=0; i<arr_emp_id.length; i++)
          {
              if(arr_emp_id[i] == del_emp_id)
              {
                  continue;
              }
              else
              {
                  if(cnt > 0)
                  {
                      new_emp_ids += ",";
                      new_emp_nms += ",";
                  }
                  new_emp_ids += arr_emp_id[i];
                  new_emp_nms += arr_emp_nm[i];
                  cnt++;
              }
          }
      }
      document.getElementById(ids).value = new_emp_ids;
      document.getElementById(names).value = new_emp_nms;
      set_disp_area(new_emp_ids, new_emp_nms);
  }
}

//子画面から呼ばれる関数
function add_analysis_list(input_analysis_id, input_analysis_nm, mode) {
  ids   = "disp_area_ids_" + mode;
  names = "disp_area_names_" + mode;

  analysis_ids = document.getElementById(ids).value;
  analysis_nms = document.getElementById(names).value;

  // 分析事案が選択されているときの処理
  if(mode == 1 && analysis_nms != "") {
      alert("既に分析事案は選択されています");
      return;
  }

  arr_input_analysis_ids   = input_analysis_id.split(",");
  arr_input_analysis_names = input_analysis_nm.split(",");

  tmp_ids   = "";
  tmp_names = "";

  if(analysis_ids != "") {
      arr_analysis_ids = analysis_ids.split(",");
      for(var i=0; i<arr_input_analysis_ids.length; i++) {
          dpl_flg = false;
          for(var j=0; j<arr_analysis_ids.length; j++) {
              if(arr_input_analysis_ids[i] == arr_analysis_ids[j]) {
                  dpl_flg = true;
              }
          }
          if(!dpl_flg) {
              if(tmp_ids != "") {
                  tmp_ids   += ",　";
                  tmp_names += ",　";
              }
              tmp_ids   += arr_input_analysis_ids[i];
              tmp_names += arr_input_analysis_names[i];
          }
      }
  } else {
      for(i=0; i<arr_input_analysis_ids.length; i++) {
          if(tmp_ids != "") {
              tmp_ids   += ",　";
              tmp_names += ",　";
          }
          tmp_ids   += arr_input_analysis_ids[i];
          tmp_names += arr_input_analysis_names[i];
      }
  }

  if(analysis_ids != "" && tmp_ids != "") analysis_ids += ",";
  analysis_ids += tmp_ids;
  document.getElementById(ids).value = analysis_ids;
  if(analysis_nms != "" && tmp_names != "") analysis_nms += ",";
  analysis_nms += tmp_names;
  document.getElementById(names).value = analysis_nms;

  set_analysis(analysis_ids, analysis_nms, mode);
}

//子画面から呼ばれる関数
function add_emp_list(input_emp_id, input_emp_nm, dummy)
{
  ids   = "disp_area_ids";
  names = "disp_area_names";

  //既存入力されている職員
  emp_ids = document.getElementById(ids).value;
  emp_nms = document.getElementById(names).value;


  //子画面から入力された職員
  arr_input_emp_id   = input_emp_id.split(", ");
  arr_input_emp_name = input_emp_nm.split(", ");

  tmp_ids   = "";
  tmp_names = "";
  if(emp_ids != "")
  {
      arr_emp_id = emp_ids.split(",");

      for(i=0; i<arr_input_emp_id.length; i++)
      {
          dpl_flg = false;
          for(j=0; j<arr_emp_id.length; j++)
          {
              // 重複した場合
              if(arr_input_emp_id[i] == arr_emp_id[j])
              {
                  dpl_flg = true;
              }
          }
          if(!dpl_flg)
          {
              if(tmp_ids != "")
              {
                  tmp_ids   += ",";
                  tmp_names += ",";
              }
              tmp_ids   += arr_input_emp_id[i];
              tmp_names += arr_input_emp_name[i];
          }
      }
  }
  else
  {
      for(i=0; i<arr_input_emp_id.length; i++)
      {
          if(tmp_ids != "")
          {
              tmp_ids   += ",";
              tmp_names += ",";
          }
          tmp_ids   += arr_input_emp_id[i];
          tmp_names += arr_input_emp_name[i];
      }
  }

  if(emp_ids != "" && tmp_ids != "")
  {
      emp_ids += ",";
  }
  emp_ids += tmp_ids;
  document.getElementById(ids).value = emp_ids;
  if(emp_nms != ""  && tmp_names != "") {
      emp_nms += ",";
  }
  emp_nms += tmp_names;
  document.getElementById(names).value = emp_nms;

  set_disp_area(emp_ids, emp_nms);
}

function set_analysis(analysis_id, analysis_nm, mode) {
  arr_analysis_ids = analysis_id.split(",");
  arr_analysis_nms = analysis_nm.split(",");
  disp_area_content = "";
  for(var i=0; i<arr_analysis_ids.length; i++) {
      if(i > 0) disp_area_content += ",";

      disp_area_content += "<a href=\"javascript:delete_analysis('" + arr_analysis_ids[i] + "', '" + arr_analysis_nms[i] + "', " + mode + ")\">";
      disp_area_content += arr_analysis_nms[i];
      disp_area_content += "</a>";
  }
  document.getElementById('disp_area_'+mode).innerHTML = disp_area_content;
}

function set_disp_area(emp_id, emp_nm) {

  arr_emp_id = emp_id.split(",");
  arr_emp_nm = emp_nm.split(",");
  disp_area_content = "";
  for(i=0; i<arr_emp_id.length; i++){
      if(i > 0){
          disp_area_content += ',';
      }

      disp_area_content += "<a href=\"javascript:delete_emp('" + arr_emp_id[i] + "', '" + arr_emp_nm[i] + "');\">";
      disp_area_content += arr_emp_nm[i];
      disp_area_content += "</a>";
  }
  document.getElementById('disp_area').innerHTML = disp_area_content;
}

function call_analysis_search(input_mode,session) {
  childwin = null;

  dx = screen.availWidth - 10;
  dy = screen.top;
  base = 0;
  wx = 720;
  wy = 600;
  var url = 'hiyari_analysis_search.php';
  url += '?session=' + session + '&input_mode='+input_mode;
  childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

  childwin.focus();
}

function call_emp_search(session)
{
  childwin = null;

  dx = screen.availWidth - 10;
  dy = screen.top;
  base = 0;
  wx = 720;
  wy = 600;
  var url = 'hiyari_emp_list.php';
  url += '?session=' + session;
  childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

  childwin.focus();
}

function emp_clear()
{
  if(document.getElementById('disp_area')){
      document.getElementById('disp_area').innerHTML   = '';
      document.getElementById('disp_area_ids').value   = '';
      document.getElementById('disp_area_names').value = '';
  }

}

function analysis_regist(session, moveto_analysis) {
  // タイトルと分析事案は必須
  var OBJ = document.form_list;
  if ( OBJ.analysis_title.value == '' ) {
      alert('タイトルが入力されていません');
      return false;
  }
  if ( OBJ.disp_area_ids_1.value == '' ) {
      alert('分析事案が入力されていません');
      return false;
  }

  if (moveto_analysis){
      opener.location.href = 'hiyari_analysis_disp.php?session=' + session;
  }
  
  document.form_list.submit();  
}

function clear_progress_date() {
  document.form_list.elements['progress_date'].value = '';
}

// このウインドウが閉じられるときは子ウインドウも閉じる
window.onbeforeunload = function(event){
  if (  childwin && !childwin.closed ) {  
      childwin.close();  
  }
}
