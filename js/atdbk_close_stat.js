//締め状態取得
function get_close_stat_ajax(emp_id, start_date, end_date)
{
	var url = 'atdbk_close_stat_ajax.php';
	var params = $H({'session':_session,'emp_id':emp_id, 'start_date':start_date, 'end_date':end_date}).toQueryString();
	var myAjax = new Ajax.Request(
		url,
		{
			method: 'post',
			postBody: params,
			asynchronous: false
		});
		//同期処理後のデータ処理
		var transport = myAjax.transport;
		set_officetime_ajax_response(transport);
}

//ajax取得データの処理
function set_officetime_ajax_response(oXMLHttpRequest)
{
	if(oXMLHttpRequest.status == 200)
	{
		var response = new ajax_response_object(oXMLHttpRequest.responseText);
		if(response.ajax == "success")
		{
			close_stat = response.close_stat;
		}
		else
		{
			//処理失敗
			;
		}
	}
	else
	{
		//通信失敗
		;
	}
}

//レスポンスデータよりレスポンスオブジェクトを生成します。(コンストラクタ)
function ajax_response_object(response_text)
{
	var a1 = response_text.split("\n");
	for (var i = 0; i < a1.length; i++)
	{
		var line = a1[i];
		var sep_index = line.indexOf("=");
		if(sep_index == -1)
		{
			break;//最後の改行行と判定
		}
		var key = line.substring(0,sep_index);
		var val = line.substring(sep_index+1);

		var eval_string = "this." + key + " = '" + val.replace("'", "\'") + "';";
		eval(eval_string);
	}
}
