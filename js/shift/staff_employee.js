/**
 * CoMedix Shift JavaScript
 */


// DOM ready
$(function () {
    // Submit
    $('#form').submit(function () {
        $('p.err').hide();
        $('button.submit').attr('disabled', true);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'shift/staff_employee_submit.php',
            data: $('#form').serialize(),
            success: function (errmes) {
                if (errmes) {
                    $.each(errmes, function (id, mes) {
                        $('#' + id).html(mes).show();
                    });
                    $('button.submit').attr('disabled', false);
                }
                else {
                    if (confirm('更新が完了しました。一覧に戻りますか？')) {
                        location.href = $('#back').attr('href');
                        return false;
                    }
                    location.reload(true);
                }
            }
        });
        return false;
    });
});


// チェックボックス
function check_pattern(type, mode) {
    switch (mode) {
        case 'on':
            $('input.check' + type).attr('checked', true);
            break;

        case 'off':
            $('input.check' + type).attr('checked', false);
            break;

        case 'toggle':
            $('input.check' + type).attr('checked', function (e, v) {
                return !v;
            });
            break;
    }
    return false;
}

// 長期休暇フォームの追加
function add_holiday() {
    $("#holiday").append(['<span><select name="holiday[]">', shift_pattern, '</select> <input type="text" class="datepicker" name="start_date[]" size="11" maxlength="10" class="eng" /> ー <input type="text" class="datepicker" name="end_date[]" size="11" maxlength="10" class="eng" /> <button type="button" onclick="remove_span(this);">削除</button><br></span>'].join(''));
    $("input.datepicker").datepicker(dp);
}

// span行の削除
function remove_span(obj) {
    var span = obj.parentNode;
    span.parentNode.removeChild(span);
}

// textの削除
function clear_text(id) {
    $('#' + id).val("");
}

// 年加算
function add_year(before, after, years) {
    if($('#' + before).val().match(/^\d{4}\-\d{2}-\d{2}$/)){
        var year = String($('#' + before).val().substr(0, 4) - 0 + years);
        var md = $('#' + before).val().substr(4, 6);
        $('#' + after).val(year + md);
    }
}
