/**
 * CoMedix Shift JavaScript
 */
// DOM ready
$(function() {
    // グループを切り替えると記号一覧と残業理由一覧が変わる
    $('#tmcd_group_id').change(function() {
        $('#ptn').children().remove();
        $('#ptn').load('shift/pattern_pulldown.php', {
            id: $('#tmcd_group_id').val()
        });

        // ajaxでリストボックスのoptionを取得する。
        $.ajax({
            type: "POST",
            url: 'shift/overtime_reason_pulldown.php',
            datatype: "html",
            data: {
                group_id: $('#tmcd_group_id').val()
            },
            success: function(htmldata) {
                $(".js-reason").html(htmldata);
            }
        }).done(function() {
            $(".js-reason").change();
        });
    });

    // 実績を切り替えると時間が変わる
    $('#ptn').on('change', function() {
        if ($(this).val() !== '') {
            $.post('shift/pattern_officehours.php', {
                ptn: $(this).val(),
                gid: $('#tmcd_group_id').val(),
                date: d
            }, function(data) {
                $('#start_hour').val(data.start_hour);
                $('#start_min').val(data.start_min);
                $('#end_hour').val(data.end_hour);
                $('#end_min').val(data.end_min);
                $('#rest_start_hour').val(data.rest_start_hour);
                $('#rest_start_min').val(data.rest_start_min);
                $('#rest_end_hour').val(data.rest_end_hour);
                $('#rest_end_min').val(data.rest_end_min);
                $('#night_duty').val(data.night_duty);
            }, 'json');
        }
    });

    // デフォルトで残業理由は非表示
    $('.js-overtime2345').hide();
    $('.js-overtime-reason').hide();
    $('.js-overtime-reason-text').hide();

    // 残業時間を入れると残業理由欄が表示
    $('.js-overtime').on('change', function() {
        var flg = false;
        $($(this).data('group')).each(function() {
            if ($(this).val() !== '') {
                flg = true;
            }
        });

        if (flg) {
            $($(this).data('reason')).show();
            if ($(this).data('next') !== 'overtime6') {
                $($(this).data('next')).show();
            }
        }
        else {
            $($(this).data('reason')).hide();
        }
    });

    // リセットボタン
    $('.js-overtime-reset').on('click', function() {
        $($(this).data('reset')).val('').change();
    });

    // 残業理由フリー入力の表示
    $('.js-reason').on('change', function() {
        var val = $(this).val();
        var txt = $(this).find('option:selected').text();
        $(this).parent().find('.js-hidden').val(txt);
        if (val === '' || txt === '緊急呼出　手術' || txt === '緊急呼出　内視鏡' || txt === '緊急呼出　カテ') {
            $($(this).data('text')).show();
        }
        else {
            $($(this).data('text')).hide().find('input').val('');
        }
    });
    $('.js-overtime').change();
    $('.js-reason').change();

    // 登録確認
    $('#edit_form').submit(function() {
        var s;
        var e;
        var mes = '';

        // 勤務時間変更不可
        if ($('#start_time').get(0)) {
            s = $('#start_time').val();
            e = $('#end_time').val();
            if ($('#next_day_flag').val() !== '0') {
                e = (parseInt(e) + 2400) + '';
            }
        }
        // 勤務時間変更可
        else {
            s = $('#start_hour').val() + $('#start_min').val();
            e = $('#end_hour').val() + $('#end_min').val();
        }

        // 残業チェック
        for (var i = 1; i <= 5; i++) {
            var ov = $('#overtime' + i);
            if (ov.find('.shh').val() !== '') {
                var ovs = ov.find('.shh').val() + ov.find('.smm').val();
                var ove = ov.find('.ehh').val() + ov.find('.emm').val();
                if (ovs < e && s < ove) {
                    mes += '残業時間' + i + 'が勤務時間と重なっています。\n';
                }
            }
        }
        if (mes) {
            mes += 'この残業開始時刻でよろしいですか？';
            if (!confirm(mes)) {
                return false;
            }
        }
        return true;
    });
});
