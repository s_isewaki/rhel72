/**
 * CoMedix Shift JavaScript
 */

// DOM ready
$(function () {
    // 表示順 D&D
    $('#ward tbody').tableDnD({
        dragHandle: "dragHandle"
    });
    $("#ward tbody tr").hover(function () {
        $(this.cells[0]).addClass('showDragHandle');
        $(this).css('background-color', '#ffff7f');
    }, function () {
        $(this.cells[0]).removeClass('showDragHandle');
        $(this).css('background-color', '');
    });
});

// 部署削除
function removeWard(obj) {
    if (!confirm('削除してよろしいでしょうか?')) {
        return false;
    }

    $.ajax({
        type: "POST",
        url: 'shift/adm_ward_ajax.php',
        data: {
            mode: 'remove',
            group_id: $(obj).closest('tr').attr('id')
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert('削除に失敗しました。');
            location.reload();
        }
    });
}

// 並べ替え
function sortWard() {
    var ward = [];
    $("#ward tbody tr").each(function () {
        ward.push(this.id);
    });

    $.ajax({
        type: "POST",
        url: 'shift/adm_ward_ajax.php',
        data: {
            mode: 'order',
            group_id: ward.join(',')
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert('削除に失敗しました。');
            location.reload();
        }
    });
}
