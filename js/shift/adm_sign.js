/**
 * CoMedix Shift JavaScript
 */

// DOM ready
$(function () {
    // 表示順 D&D
    $('#sign tbody').tableDnD({
        dragHandle: "dragHandle"
    });
    $("#sign tbody tr").hover(function () {
        $(this.cells[0]).addClass('showDragHandle');
        $(this).css('background-color', '#ffff7f');

    }, function () {
        $(this.cells[0]).removeClass('showDragHandle');
        $(this).css('background-color', '');
    });
});

/**
 * 文字変更
 */
function change_font_name(obj, id) {
    $(id).html($(obj).val());
}

/**
 * 文字色変更
 */
function change_font_color(obj, id) {
    $(id).css('color', $(obj).val());
}

/**
 * 背景色変更
 */
function change_back_color(obj, id) {
    $(id).css('backgroundColor', $(obj).val());
}
