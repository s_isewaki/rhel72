/**
 * CoMedix Shift JavaScript
 */

/*
 * 出勤パターンを元に、勤務記号の文字、背景、文字色を設定
 */
i=0;
while ( i < 8 ){
    $('#preview_'+i).css('color', $('#default_kigo_'+i).find(':selected').data('fontcolor')).css('backgroundColor', $('#default_kigo_'+i).find(':selected').data('bgcolor')).html($('#default_kigo_'+i).find(':selected').data('name'));
    i++;
}

/**
 * 勤務パターン名を変更した時に勤務記号変更
 */
function change_atdptn_symbol(obj, id) {
    $(id).html($(obj).find(':selected').data('name')).css('color', $(obj).find(':selected').data('fontcolor')).css('backgroundColor', $(obj).find(':selected').data('bgcolor'));
}
