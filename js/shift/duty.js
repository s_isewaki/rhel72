/**
 * CoMedix Shift JavaScript
 */

var current_row;
var current_col;
var current_col_head;
var button = {
    hope: false,
    results: false,
    duty: false,
    comment: false,
    kadou: true,
    count: true
};
var is_change = false;

var shift = $('#shift');
var loading = $('#loading');

// DOM ready
$(function () {

    ///-----------------------------------------------------
    // テーブルハイライト
    //------------------------------------------------------
    $('#duty').mouseover(function (e) {
        if ($(e.target).hasClass('day')) {
            highlight_in($(e.target));
        }
    }).mouseout(function (e) {
        if ($(e.target).hasClass('day')) {
            highlight_out();
        }
    });

    ///-----------------------------------------------------
    // 表示切り替え
    //------------------------------------------------------
    // チーム
    $('#button_team').toggle(team_on, team_off);
    if (display.team === '0') {
        $('#button_team').click();
    }

    // 職種
    $('#button_job').toggle(job_on, job_off);
    if (display.job === '0') {
        $('#button_job').click();
    }

    // 役職
    $('#button_status').toggle(status_on, status_off);
    if (display.status === '0') {
        $('#button_status').click();
    }

    ///-----------------------------------------------------
    // ボタン
    //------------------------------------------------------
    // 実績表示
    var is_show_results = false;
    var is_show_only = false;
    $('#button_results').toggle(
        function () {
            if (is_show_only) {
                $("#duty tbody tr.duty td:contains('●')").parent('tr').next('tr').show();
                $("#duty tbody tr.results_duty td:contains('●')").parent('tr').show();
                $("#duty tbody tr.results_duty td:contains('●')").parent('tr').prev('tr').show();
            }
            else {
                $('#duty tbody tr.results_duty').show();
            }
            $('#button_results').text('実績非表示');
            is_show_results = true;
        },
        function () {
            $('#duty tr.results_duty').hide();
            $('#button_results').text('実績表示');
            is_show_results = false;
        }
    );

    // 当直のみ
    $('#button_only').toggle(
        function () {
            $('#duty tbody tr').hide();
            $("#duty tbody tr.duty td:contains('●')").parent('tr').show();
            if (is_show_results) {
                $("#duty tbody tr.duty td:contains('●')").parent('tr').next('tr').show();
                $("#duty tbody tr.results_duty td:contains('●')").parent('tr').show();
                $("#duty tbody tr.results_duty td:contains('●')").parent('tr').prev('tr').show();
            }
            $('#button_only').text('全職員表示');
            is_show_only = true;
        },
        function () {
            $('#duty tbody tr.duty').show();
            if (is_show_results) {
                $('#duty tbody tr.results_duty').show();
            }
            $('#button_only').text('当直のみ');
            is_show_only = false;
        }
    );

    ///-----------------------------------------------------
    // 当直表クリック
    //------------------------------------------------------
    var table_click = function (e) {
        var cell = $(e.target);
        if (!cell.hasClass('cell')) {
            return;
        }

        highlight_out();
        setSign(e.target);
    };
    $('#duty').click(table_click);

    ///-----------------------------------------------------
    // 登録
    //------------------------------------------------------
    $('#icon_duty').click(function () {
        highlight_out();

        if (!confirm('当直を登録します。')) {
            return false;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'shift/plan_ajax.php',
            data: {
                group_id: this_group_id,
                year: this_y,
                month: this_m,
                mode: 'duty',
                data: data
            },
            success: function (json) {
                duty_cells(json);
                loading.hide();
                data = json;
                alert('当直の登録が完了しました。');
                is_change = false;
            },
            beforeSend: function () {
                loading.show();
            }
        });
    });

    ///-----------------------------------------------------
    // 下書き
    //------------------------------------------------------
    $('#icon_draft').click(function () {
        //
    });

    ///-----------------------------------------------------
    // 印刷
    //------------------------------------------------------

    ///-----------------------------------------------------
    // Excel
    //------------------------------------------------------

    ///-----------------------------------------------------
    // 全画面
    //------------------------------------------------------
    $('#icon_fullscreen').click(fullscreen);

    ///-----------------------------------------------------
    // <a href="#hoge">のクリックでbeforeunloadイベントが走ってしまう対策
    //------------------------------------------------------
    $('a').click(function (e) {
        if (e.target.href && e.target.href.indexOf('javascript') === 0) {
            is_beforeunload = false;
            setTimeout(function () {
                is_beforeunload = true;
            }, 0);
        }
    });

    ///-----------------------------------------------------
    // セッション維持
    //------------------------------------------------------
    setInterval(function () {
        $.post('shift/session_keep_ajax.php');
    }, 300000);

    loading.hide();
});

///-----------------------------------------------------
// 当直テーブルにデータを埋める
//------------------------------------------------------
function duty_cells(data, type) {
    $('#duty tbody td.count').html(0);
    $('#duty tfoot td.count').html(0);

    if (data.emp) {
        $.each(data.emp, function (emp_id, emp) {
            var eid = ['#', emp_id].join('');

            // 職員情報(初期読み込み時のみセット)
            if (type === 'init') {
                // 名前+性別
                $(eid + '_name').html(emp.name).addClass('sex' + emp.sex);

                // チーム
                $(eid + '_team').html(emp.team);

                // 職種
                $(eid + '_job').html(emp.job_name);

                // 役職
                $(eid + '_status').html(emp.st_name);

                // 応援
                if (emp.assist === '1') {
                    $(eid + '_name').addClass('assist');
                }
            }
            // 勤務表
            if (emp.dates) {
                $.each(emp.dates, function (date, ptn) {
                    var id = [eid, date].join('_');

                    // 当直
                    if (ptn.duty && ptn.duty === '1') {
                        $(id + '_duty').html('●');
                    }

                    // 当直実績
                    if (ptn.results_duty && ptn.results_duty === '1') {
                        $(id + '_results_duty').html('●');
                    }
                });
            }

            // 行集計
            $(eid + '_count_duty').html(emp.count_duty);
            $(eid + '_count_dutyr').html(emp.count_dutyr);
        });
    }

    // 列集計
    if (data.date) {
        $.each(data.date, function (date, d) {
            $('#count_' + date).html(d.count_duty);
        });
    }

    $('#display_plan').hide();
    $('#display_draft').hide();

    // 下書きあり
    if (data.display_draft) {
        $('#display_draft').show();
    }

    // 登録あり
    if (data.display_plan) {
        $('#display_plan').show();
    }
}

///-----------------------------------------------------------------------------
// 当直表に記号をセット
//------------------------------------------------------------------------------
function setSign(cell_obj) {
    var cell = $(cell_obj);

    // 当直表セルでなければセットしないで終了
    if (!cell.hasClass('cell')) {
        return;
    }

    var ids = cell.attr('id').split('_');

    // 今の記号
    var now_duty = getSignData(ids[0], ids[1], ids[2]);
    var now_plan = getSignData(ids[0], ids[1], 'plan');

    // 連動記号なら変更不可
    if (pattern['p' + now_plan] && pattern['p' + now_plan].duty_connect === "1") {
        alert('勤務予定と連動している当直のため変更できません。');
        cell = null;
        return false;
    }

    // 変更フラグを立てる
    is_change = true;

    if (now_duty === '1') {
        // データを変更する
        setSignData(ids[0], ids[1], ids[2], "");

        // セルの表示を変更する
        cell.html('');

        // 合計値修正
        calcTotal(ids[0], ids[1], -1);
    }
    else {
        // データを変更する
        setSignData(ids[0], ids[1], ids[2], '1');

        // セルの表示を変更する
        cell.html('●');

        // 合計値修正
        calcTotal(ids[0], ids[1], 1);
    }
    cell = null;
}

///-----------------------------------------------------------------------------
// 勤務表データから現在の記号を取得
//------------------------------------------------------------------------------
function getSignData(emp_id, date, type) {
    if (!data.emp[emp_id].dates) {
        return null;
    }
    if (!data.emp[emp_id].dates[date]) {
        return null;
    }
    if (!data.emp[emp_id].dates[date][type]) {
        return null;
    }
    return data.emp[emp_id].dates[date][type];
}

///-----------------------------------------------------------------------------
// 勤務表データに記号をセット
//------------------------------------------------------------------------------
function setSignData(emp_id, date, type, pattern) {
    if (!data.emp[emp_id].dates) {
        data.emp[emp_id].dates = {};
    }
    if (!data.emp[emp_id].dates[date]) {
        data.emp[emp_id].dates[date] = {};
    }
    data.emp[emp_id].dates[date][type] = pattern;
}

///-----------------------------------------------------------------------------
// 合計値修正
//------------------------------------------------------------------------------
function calcTotal(emp_id, date, num) {
    var row = $('#' + emp_id + '_count_duty');
    var col = $('#count_' + date);

    // 行
    row.text(parseInt(row.text()) + num);

    // 列
    col.text(parseInt(col.text()) + num);
}

///-----------------------------------------------------------------------------
// テーブルハイライト in/out (IE6ではハイライトさせない)
//------------------------------------------------------------------------------
function highlight_in(cell) {
    if (!_ua.ltIE8) {
        var ids = cell.attr('id').split('_');
        (current_row = $("#duty tr." + ids[0] + " td")).addClass("hover");
        (current_col = $("#duty td." + ids[1])).addClass("hover");
        (current_col_head = $("#duty_head td." + ids[1])).addClass("hover");
    }
    else {
        var ids = cell.attr('id').split('_');
        (current_row = $("#duty tr." + ids[0] + " td")).addClass("hover");
        (current_col_head = $("#duty_head td." + ids[1])).addClass("hover");
    }
}
function highlight_out() {
    if (!_ua.ltIE8) {
        if (!current_row || !current_col || !current_col_head) {
            return;
        }
        current_row.removeClass("hover");
        current_col.removeClass("hover");
        current_col_head.removeClass("hover");
    }
    else {
        if (!current_row || !current_col_head) {
            return;
        }
        current_row.removeClass("hover");
        current_col_head.removeClass("hover");
    }
}

///-----------------------------------------------------------------------------
// ページ移動イベント
//------------------------------------------------------------------------------
var is_beforeunload = true;
$(window).bind('beforeunload', function () {
    if (is_beforeunload && is_change) {
        return;
    }
});

///-----------------------------------------------------------------------------
// チーム
//------------------------------------------------------------------------------
var td_team = $('#duty td.team');
var td_team_head = $('#duty_head td.team');
var td_team_width = $('#duty_head tr:first-child td.team').outerWidth();
var team_on = function () {
    td_team.css('display', 'none');
    td_team_head.css('display', 'none');
    $('#button_team').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_team_width;
        resizeTableWidth(tblWidth);
    }
};
var team_off = function () {
    if (tblWidth) {
        tblWidth += td_team_width;
        resizeTableWidth(tblWidth);
    }
    td_team.css('display', '');
    td_team_head.css('display', '');
    $('#button_team').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 職種
//------------------------------------------------------------------------------
var td_job = $('#duty td.job');
var td_job_head = $('#duty_head td.job');
var td_job_width = $('#duty_head tr:first-child td.job').outerWidth();
var job_on = function () {
    td_job.css('display', 'none');
    td_job_head.css('display', 'none');
    $('#button_job').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_job_width;
        resizeTableWidth(tblWidth);
    }
};
var job_off = function () {
    if (tblWidth) {
        tblWidth += td_job_width;
        resizeTableWidth(tblWidth);
    }
    td_job.css('display', '');
    td_job_head.css('display', '');
    $('#button_job').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 役職
//------------------------------------------------------------------------------
var td_status = $('#duty td.status');
var td_status_head = $('#duty_head td.status');
var td_status_width = $('#duty_head tr:first-child td.status').outerWidth();
var status_on = function () {
    td_status.css('display', 'none');
    td_status_head.css('display', 'none');
    $('#button_status').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_status_width;
        resizeTableWidth(tblWidth);
    }
};
var status_off = function () {
    if (tblWidth) {
        tblWidth += td_status_width;
        resizeTableWidth(tblWidth);
    }
    td_status.css('display', '');
    td_status_head.css('display', '');
    $('#button_status').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// resizeIframeEx
//------------------------------------------------------------------------------
var viewAreaHgt = 0; // 表示領域の高さ
var sclContHgt = 0; // スクロールさせるコンテンツ(#wrapCont)の高さ
var sclContTopRange = 0; // #wrapContの左上の座標
var tblWidth = 0;

function resizeIframeEx() {
    try {
        if (!fscreen) {
            elmFloatingpage = parent.document.getElementById('floatingpage');
            elmFloatingpage.removeAttribute("height"); // 不要な高さ削除
            elmFloatingpage.style.height = '1px';
        }

        /* 表示領域の高さ設定*/
        viewAreaHgt = parent.document.body.clientHeight;
        sclContTopRange = $('#wrapCont').offset().top + 17;

        if (!fscreen) {
            if (parent.callSideMenu == true) {
                sclContTopRange = sclContTopRange + 10;
            }

            // iframeに新しい高さを設定
            var iframeHgt = viewAreaHgt - 120; // (表示領域 - 全体のヘッダー)
            elmFloatingpage.style.height = iframeHgt + 'px';
        }

        // #wrapContに高さを設定
        sclContHgt = !fscreen ? iframeHgt - sclContTopRange : $(window).innerHeight() - sclContTopRange;
        $('#wrapCont').height(sclContHgt + 'px');

        /* 表示領域の幅設定 */
        if (parent.callSideMenu == false || fscreen) {
            // 一時的に#wrapHead&#wrapContに幅を設定
            $('#shift').width('1700px');
            $('#wrapHead').width('1700px');
            $('#wrapCont').width('1700px');

            // テーブル幅を取得
            tblWidth = $('#wrapHead table').outerWidth();
            resizeTableWidth(tblWidth);
        }
        if (!fscreen) {
            parent.callSideMenu = false;
        }

    } catch (e) {
        alert(e);
    }
}

function resizeTableWidth(width) {
    // 一時的に#wrapHead&#wrapContに幅を設定
    $('#shift').width('1700px');
    $('#wrapHead').width('1700px');
    $('#wrapCont').width('1700px');

    // ヘッダー・コンテンツの各テーブルに幅を設定
    $('#wrapHead table').width(width + 'px');
    $('#wrapCont table').width((width - 17) + 'px');

    // ヘッダー・コンテンツの各テーブルを囲っているBoxにテーブル幅+スクロールバー幅を設定
    var shiftWidth = (width > 1000) ? width : 1000;
    $('#shift').width(shiftWidth + 'px');
    $('#wrapHead').width(shiftWidth + 'px');
    $('#wrapCont').width(width + 'px');
}
