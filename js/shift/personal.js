/**
 * CoMedix Shift JavaScript
 */

/**
 * 残業入力ウインドウ
 */
var overtime_edit = function () {
    var date = $(this).data('date');

    var wx = 650;
    var wy = 650;
    var dx = screen.width - wx - 20;
    var base = 10;
    var url = [base_href, 'shift/personal_edit.php?date=', date].join('');
    var childwin = window.open(url, 'personal_edit', 'left=' + dx + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
    childwin.focus();
};

/**
 * 予定変更ウインドウ
 */
var plan_edit = function () {
    var date = $(this).data('date');

    var wx = 500;
    var wy = 300;
    var dx = screen.width - wx - 20;
    var base = 10;
    var url = [base_href, 'shift/personal_edit_plan.php?date=', date].join('');
    var childwin = window.open(url, 'personal_edit', 'left=' + dx + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
    childwin.focus();
};

/**
 * 実績変更ウインドウ
 */
var result_edit = function () {
    var date = $(this).data('date');

    var wx = 650;
    var wy = 700;
    var dx = screen.width - wx - 20;
    var base = 10;
    var url = [base_href, 'shift/personal_edit_result.php?date=', date].join('');
    var childwin = window.open(url, 'personal_edit', 'left=' + dx + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
    childwin.focus();
};

/**
 * DOM ready
 */
$(function () {
    // 残業入力
    $('#personal').find('span.js-edit-overtime').click(overtime_edit);

    // 予定変更
    $('#personal').find('span.js-edit-plan').click(plan_edit);

    // 実績変更
    $('#personal').find('span.js-edit-result').click(result_edit);

    // 月クール表示
    $('.btn_view').click(function () {
        $('<form/>', {method: 'post', action: 'shift/personal.php'})
            .append($('<input/>').attr({type: 'hidden', name: 'y', value: $(this).data('y')}))
            .append($('<input/>').attr({type: 'hidden', name: 'm', value: $(this).data('m')}))
            .append($('<input/>').attr({type: 'hidden', name: 'mode', value: $(this).data('mode')}))
            .appendTo(document.body)
            .submit();
    });

    $('#form-personal').on('submit', function () {
        if (confirm("この月の勤務表の内容を確定してよろしいですか?\n (この操作は取り消せません)")) {
            return true;
        }
        return false;
    });
});
