/**
 * CoMedix Shift JavaScript
 */

// DOM ready
$(function () {
    // 参照可能属性init
    $('#target_permission_area div').each(function (e) {
        // id
        var class_id = $(this).children('select.class').val();
        var atrb_id = $(this).children('select.atrb').val();
        var dept_id = $(this).children('select.dept').val();
        var room_id = $(this).children('select.room').val();

        // class
        if (class_id) {
            change_emp_class($(this).children('select.class'));
            if (atrb_id) {
                $(this).children('select.atrb').val(atrb_id);
            }
        }
        // atrb
        if ($(this).children('select.atrb').val()) {
            change_emp_atrb($(this).children('select.atrb'));
            if (dept_id) {
                $(this).children('select.dept').val(dept_id);
            }
        }
        // dept
        if ($(this).children('select.dept').val()) {
            change_emp_dept($(this).children('select.dept'));
            if (room_id) {
                $(this).children('select.room').val(room_id);
            }
        }
    });

});

// 職員の追加
function add_target_list(item_id, emp_id, emp_name) {
    var emp = emp_id.split(", ");
    var empname = emp_name.split(", ");
    var target;
    var form_name;

    switch (item_id) {
        case 'admin':
            target = $('#target_admin_area');
            form_name = 'administrator[]';
            break;

        case 'user':
            target = $('#target_user_area');
            form_name = 'access_user[]';
            break;

        case 'employee':
            target = $('#target_user_area');
            form_name = 'employee[]';
            break;

        case 'manager':
            //複数職員がわたって来た時は処理しない
            if(emp.length !== 1){
                return;
            }
            //登録対象者HTMLを更新
            $('#target_manager_area').html(emp_name);
            $('#manager').val(emp_id);
            return;
    }

    for (i = 0; i < emp.length; i++) {
        if ($('#emp_' + emp[i]).size() == 0) {
            target.html(
                target.html()
                + '<div id="emp_' + emp[i] + '">'
                + empname[i]
                + '<a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a><input type="hidden" name="'
                + form_name
                + '" value="'
                + emp[i]
                + '" /></div>'
                );
        }
    }
}

// 職員属性の追加
function add_permission(id) {
    var html =
        '<div><select onchange="change_emp_class(this);" class="class" name="access_class[]"><option value="">-</option>'
        + pulldown_class
        + '</select> &gt; <select onchange="change_emp_atrb(this);" class="atrb" name="access_atrb[]"><option value="">-</option>'
        + pulldown_atrb
        + '</select> &gt; <select onchange="change_emp_dept(this);" class="dept" name="access_dept[]"><option value="">-</option>'
        + pulldown_dept
        + '</select>';

    if (class_count == '4') {
        html = html + ' &gt; <select class="room" name="access_room[]"><option value="">-</option>'
            + pulldown_room
            + '</select>';
    }

    html = html + ' AND <select class="job" name="access_job[]"><option value="">-</option>'
        + pulldown_job
        + '</select> AND <select class="status" name="access_status[]"> <option value="">-</option>'
        + pulldown_st
        + '</select> <a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a></div>';

    $("#" + id).append(html);
}

// メールアドレスフィールドの追加
function add_email_fields(id) {
    var html =
        '<div><input type="text" name="email[]" value="" size="50" /> <a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a></div>';

    $("#" + id).append(html);
}
