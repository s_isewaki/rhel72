/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// DOM ready
$(function () {
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        buttonImage: 'img/calendar_link.gif',
        buttonImageOnly: true,
        showOn: 'button'
    });
});

// 組み合わせ設定フォームの追加
function add_ok_pattern() {
    $("#ok_pattern tbody").append(
        '<tr><td class="header">-</td><td><select name="seq_today[]">'
        + shift_pattern
        + '</select></td><td><select name="seq_next[]">'
        + shift_pattern
        + '</select></td><td><button type="button" onclick="remove_table_row(this);">削除</button></td></tr>'
        );
}

// 組み合わせ禁止フォームの追加
function add_ng_pattern() {
    $("#ng_pattern tbody").append(
        '<tr><td class="header">-</td><td><select name="ng_today[]">'
        + shift_pattern
        + '</select></td><td><select name="ng_next[]">'
        + shift_pattern
        + '</select></td><td><button type="button" onclick="remove_table_row(this);">削除</button></td></tr>'
        );
}

// 連続勤務シフトの上限フォームの追加
function add_up_limit() {
    $("#up_limit tbody").append(
        '<tr><td class="header">-</td><td><select name="limit_id[]">'
        + shift_pattern
        + '</select></td><td><input type="number" class="num" name="limit_day[]" size="3">日</td><td><button type="button" onclick="remove_table_row(this);">削除</button></td></tr>'
        );
}

// 勤務シフトの間隔フォームの追加
function add_interval() {
    $("#interval tbody").append(
        '<tr><td class="header">-</td><td><select name="interval_id[]">'
        + shift_pattern
        + '</select></td><td><input type="number" class="num" name="interval_day[]" size="3">日</td><td><button type="button" onclick="remove_table_row(this);">削除</button></td></tr>'
        );
}

// 性別固定シフトフォームの追加
function add_sex_pattern() {
    $("#sex_pattern tbody").append(
        '<tr><td class="header">-</td><td><select name="sex_pattern[]">'
        + shift_pattern
        + '</select></td><td>-</td><td><button type="button" onclick="remove_table_row(this);">削除</button></td></tr>'
        );
}

// 4週8休で休暇とするシフトフォームの追加
function add_hol_pattern() {
    $("#hol_pattern").append(
        '<span><select name="hol_pattern[]">'
        + shift_pattern
        + '</select><button type="button" onclick="remove_span(this);">削除</button><br></span>'
        );
}

// 決済欄役職フォームの追加
function add_st_name() {
    $("#st_name").append('<span><input type="text" name="st_name[]" size="30" maxlength="30" class="jpn"><button type="button" onclick="remove_span(this);">削除</button><br></span>');
}


// テーブル行の削除
function remove_table_row(obj) {
    var tr = obj.parentNode.parentNode;
    tr.parentNode.deleteRow(tr.sectionRowIndex);
}

// span行の削除
function remove_span(obj) {
    var span = obj.parentNode;
    span.parentNode.removeChild(span);
}

