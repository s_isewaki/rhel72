/**
 * CoMedix Shift JavaScript
 */

var add_staff_count = 0;

// DOM ready
$(function () {
    // 表示順 D&D
    $('#staff tbody').tableDnD({
        dragHandle: "dragHandle"
    })
        .on('mouseenter', 'tr', function () {
            $(this.cells[0]).addClass('showDragHandle');
            $(this).css('background-color', '#ffff7f');
        })
        .on('mouseleave', 'tr', function () {
            $(this.cells[0]).removeClass('showDragHandle');
            $(this).css('background-color', '');
        });
});

// 職員の追加
function add_target_list(item_id, emp_id, emp_name) {
    var table = $('#staff tbody');
    $('#err').hide();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: 'shift/staff_ajax_addstaff.php',
        data: {
            group_id: this_group_id,
            year: this_y,
            month: this_m,
            emp_id: emp_id,
            add_staff_count: add_staff_count
        },
        success: function (data) {
            if (data.err) {
                $('#err').html(data.err).show();
            }
            table.append(data.table);
            $('#staff tbody').tableDnDUpdate();
            add_staff_count += data.count;
        }
    });
}

// 職員削除
function removeStaff(obj) {
    if (!confirm('削除してよろしいでしょうか?')) {
        return false;
    }
    $(obj).closest('tr').remove();
    $('#staff tbody').tableDnDUpdate();
    add_staff_count--;
}
