// DOM ready
$(function() {
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        buttonImage: 'img/calendar_link.gif',
        buttonImageOnly: true,
        showOn: 'button'
    });

    $("body").on('click', '.js-editdate', function() {
        var $input = $("<input>").attr("type","text").attr("class","edit_date").val($(this).attr('data-date'));
        var $target = $(this).parent().prev();
        $target.html($input);
        $('.edit_date').datetimepicker({
            format:'Y-m-d_H:i',
            step:5
        });
        $(this).val('送信').removeClass('js-editdate').addClass('js-senddate');


        var $cancel_button = $("<input>").attr("type","button").attr("class","js-cancel").val('戻す');
        $(this).after($cancel_button);

    });

    $("body").on('click', '.js-senddate', function() {
        var $target = $(this).parent().prev().find('input');
        var $button = $(this);
        var $cancel_button = $(this).next();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "shift/adm_approval_log_ajax.php",
            data: {
                "id": $(this).data('id'),
                'before_approval_date': $(this).attr('data-date'),
                'after_approval_date': $target.val()
            },
            success: function(re_data) {
                if(re_data.update_flg) {
                    $target.after(re_data.msg).remove();
                    $button.attr('data-date', re_data.changed_date);
                    $button.val('変更').removeClass('js-senddate').addClass('js-editdate');
                    $cancel_button.remove();
                    }
                else {
                    alert(re_data.msg);
                }
            },
            error: function() {
                alert('承認日時の修正に失敗しました');
                location.href="shift/adm_approval_log.php";
            }
        });
    });

    $("body").on('click', '.js-cancel', function() {
        var $target = $(this).parent().prev().find('input');
        var $button = $(this).prev();
        var $cancel_button = $(this);
        $.ajax({
            type: "POST",
            url: "shift/adm_approval_log_ajax.php",
            data: {
                "mode": 'cancel',
                'before_approval_date': $button.attr('data-date')
            },
            success: function(re_data){
                $button.val('変更').removeClass('js-senddate').addClass('js-editdate');
                $cancel_button.remove();
                $target.after(re_data).remove();
            }
        });
    });

});
