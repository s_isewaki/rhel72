/**
 * CoMedix Shift JavaScript
 */

var current_stamp = {
    cell: '',
    style: {
        color: '#000',
        backgroundColor: ''
    }
};
var current_row;
var current_col;
var current_col_head;
var current_cell;
var is_stamp = false;
var is_change = false;

var shift = $('#shift');
var loading = $('#loading');

// DOM ready
$(function() {


    ///-----------------------------------------------------
    // 月クール表示
    ///-----------------------------------------------------
    $('.btn_view').click(function() {
        $('<form/>', {method: 'get', action: 'shift/results.php'})
            .append($('<input/>').attr({type: 'hidden', name: 'y', value: $(this).data('y')}))
            .append($('<input/>').attr({type: 'hidden', name: 'm', value: $(this).data('m')}))
            .append($('<input/>').attr({type: 'hidden', name: 'group_id', value: this_group_id}))
            .append($('<input/>').attr({type: 'hidden', name: 'mode', value: $(this).data('mode')}))
            .appendTo(document.body)
            .submit();
    });

    ///-----------------------------------------------------
    // テーブルハイライト
    //------------------------------------------------------
    $('#results').mouseover(function(e) {
        if ($(e.target).hasClass('day')) {
            highlight_in($(e.target));
        }
    }).mouseout(function(e) {
        if ($(e.target).hasClass('day')) {
            highlight_out();
        }
    });

    ///-----------------------------------------------------
    // 表示切り替え
    //------------------------------------------------------
    // チーム
    $('#button_team').toggle(team_on, team_off);
    if (display.team === '0') {
        $('#button_team').click();
    }

    // 職種
    $('#button_job').toggle(job_on, job_off);
    if (display.job === '0') {
        $('#button_job').click();
    }

    // 役職
    $('#button_status').toggle(status_on, status_off);
    if (display.status === '0') {
        $('#button_status').click();
    }

    // 稼働時間
    $('#button_kadou').toggle(kadou_on, kadou_off);
    if (display.kadou === '0') {
        $('#button_kadou').click();
    }

    // 集計
    $('#button_count').toggle(total_on, total_off);
    if (display.count === '0') {
        $('#button_count').click();
    }

    // 有休
    $('#button_holiday').toggle(holiday_on, holiday_off);
    if (display.holiday === '0') {
        $('#button_holiday').click();
    }

    // 夏休
    $('#button_summer').toggle(summer_on, summer_off);
    if (display.summer === '0') {
        $('#button_summer').click();
    }

    // 週休
    $('#button_week').toggle(week_on, week_off);
    if (display.week === '0') {
        $('#button_week').click();
    }

    ///-----------------------------------------------------
    // 2段目
    //------------------------------------------------------
    // 残業
    $('#button_zangyo').toggle(zangyo_on, zangyo_off);

    ///-----------------------------------------------------
    // 勤務表クリック
    //------------------------------------------------------
    var table_click = function(e) {
        var cell = $(e.target);
        if (!cell.hasClass('cell')) {
            return;
        }

        //[スタンプ]クリックセルに反映する
        if (is_stamp) {
            setSign(e.target, current_stamp);
            return;
        }

        highlight_out();
        var ids = $(e.target).attr('id').split('_');

        var wx = 650;
        var wy = 600;
        var dx = screen.width - wx - 20;
        var base = 10;
        var url = [base_href, 'shift/results_edit.php?emp_id=', ids[0].substr(1), '&date=', ids[1].substr(1)].join('');
        var childwin = window.open(url, 'results_edit', 'left=' + dx + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
        childwin.focus();
    };
    $('#results').click(table_click);

    ///-----------------------------------------------------
    // 登録
    //------------------------------------------------------
    $('#icon_results').click(function() {
        highlight_out();

        if (!confirm('勤務実績を登録します。')) {
            return false;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'shift/plan_ajax.php',
            data: {
                group_id: this_group_id,
                year: this_y,
                month: this_m,
                cours: cours,
                mode: 'results',
                data: data
            },
            success: function(json) {
                results_cells(json);
                loading.hide();
                data = json;
                alert('勤務実績の登録が完了しました。');
                is_change = false;
            },
            beforeSend: function() {
                loading.show();
            }
        });
    });

    ///-----------------------------------------------------
    // スタンプ
    //------------------------------------------------------
    $('#icon_stamp').toggle(function() {
        $("#stamp").show();
        $('#icon_stamp>img').attr('src', 'img/ico-illust-stamp-off.gif');
        is_stamp = true;
    }, function() {
        $("#stamp").hide();
        $('#icon_stamp>img').attr('src', 'img/ico-illust-stamp.gif');
        is_stamp = false;
    });
    // スタンプ画面クローズ
    $("#stamp_header span.close").click(function() {
        $('#icon_stamp').click();
    });
    // スタンプ画面ドラッグ＆ドロップ
    $("#stamp_header").mousedown(function(e) {
        $("#stamp_header").css('cursor', 'move');
        $("#stamp")
            .data("clickPointX", e.pageX - $("#stamp").offset().left)
            .data("clickPointY", e.pageY - $("#stamp").offset().top);

        $(document).mousemove(function(e) {
            $("#stamp").css({
                top: e.pageY - $("#stamp").data("clickPointY") + "px",
                left: e.pageX - $("#stamp").data("clickPointX") + "px"
            });
        });
    })
        .mouseup(function() {
            $(document).unbind("mousemove");
            $("#stamp_header").css('cursor', 'pointer');
        });

    ///-----------------------------------------------------
    // 再計算
    //------------------------------------------------------
    $('#icon_reload').click(function() {
        highlight_out();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'shift/plan_ajax.php',
            data: {
                group_id: this_group_id,
                year: this_y,
                month: this_m,
                mode: 'reload',
                data: data
            },
            success: function(json) {
                results_cells(json);
                loading.hide();
                data = json;
                alert('再計算が完了しました。');
            },
            beforeSend: function() {
                loading.show();
            }
        });
    });

    ///-----------------------------------------------------
    // コピー
    //------------------------------------------------------
    var icon_copy = function() {
        highlight_out();

        if (!confirm('勤務予定を実績にコピーします。\n実績がすでに入っている日にはコピーされません。')) {
            return false;
        }
        loading.show();
        $.each(data.emp, function(emp_id, emp) {
            var eid = '#' + emp_id;
            if (emp.dates) {
                $.each(emp.dates, function(date, ptn) {
                    var plan_pattern = pattern;
                    if ((!ptn.results || ptn.results === '0') && ptn.plan && ptn.plan !== '0') {
                        var id = eid + '_' + date;
                        setSignData(emp_id, date, 'results', ptn.plan);
                        setSignData(emp_id, date, 'results_duty', ptn.duty);

                        if (emp.assist === "1") {
                            setSignData(emp_id, date, 'results_pattern_id', ptn.plan_pattern_id);
                        }

                        if (typeof ptn.plan_pattern_id !== 'undefined') {
                            plan_pattern = data.pattern['ptn' + ptn.plan_pattern_id];
                        }
                        p = plan_pattern['p' + ptn.plan];
                        $(id + '_results').html(p.cell).css(p.style);
                        is_change = true;
                    }
                });
            }
        });
        loading.hide();
        alert('勤務予定をコピーしました。');
    };
    $('#icon_copy').click(icon_copy);

    ///-----------------------------------------------------
    // 全画面
    //------------------------------------------------------
    $('#icon_fullscreen').click(fullscreen);

    ///-----------------------------------------------------
    // <a href="#hoge">のクリックでbeforeunloadイベントが走ってしまう対策
    //------------------------------------------------------
    $('a').click(function(e) {
        if (e.target.href && e.target.href.indexOf('javascript') === 0) {
            is_beforeunload = false;
            setTimeout(function() {
                is_beforeunload = true;
            }, 0);
        }
    });

    ///-----------------------------------------------------
    // セッション維持
    //------------------------------------------------------
    setInterval(function() {
        $.post('shift/session_keep_ajax.php');
    }, 300000);

    loading.hide();
});

///-----------------------------------------------------
// 勤務実績表テーブルにデータを埋める
//------------------------------------------------------
function results_cells(data, type) {
    if (data.emp) {
        $.each(data.emp, function(emp_id, emp) {
            var eid = ['#', emp_id].join('');

            // 職員情報(初期読み込み時のみセット)
            if (type === 'init') {
                // 名前+性別
                $(eid + '_name').html(emp.name).addClass('sex' + emp.sex);

                // チーム
                $(eid + '_team').html(emp.team);

                // 職種
                $(eid + '_job').html(emp.job_name);

                // 役職
                $(eid + '_status').html(emp.st_name);

                // 応援
                if (emp.assist === '1') {
                    $(eid + '_name').addClass('assist');
                }
            }
            // 勤務表
            if (emp.dates) {
                $.each(emp.dates, function(date, ptn) {
                    var id = [eid, date].join('_');
                    var p;
                    var plan_pattern = pattern;
                    var results_pattern = pattern;

                    // 応援の勤務パターンか否か
                    if (ptn.plan_pattern_id && ptn.plan_pattern_id !== 'null' && ptn.plan_pattern_id != this_pattern_id) {
                        plan_pattern = data.pattern['ptn' + ptn.plan_pattern_id];
                    }
                    if (ptn.results_pattern_id && ptn.results_pattern_id !== 'null' && ptn.results_pattern_id != this_pattern_id) {
                        results_pattern = data.pattern['ptn' + ptn.results_pattern_id];
                    }

                    // 予定
                    if (p = plan_pattern['p' + ptn.plan]) {
                        $(id + '_plan').html(p.cell).css(p.style);
                    }

                    // 実績
                    if (p = results_pattern['p' + ptn.results]) {
                        $(id + '_results').html(p.cell).css(p.style);
                    }

                    // 残業時間
                    if (ptn.results_ovtm) {
                        $(id + '_results_ovtm').html(ptn.results_ovtm);
                    }

                    // 残業理由
                    if (ptn.results_ovtmrsn) {
                        $(id + '_results_ovtmrsn').html(ptn.results_ovtmrsn);
                    }

                    // 応援
                    if ((emp.assist === '1' && ptn.assist !== this_group_id) || (emp.assist !== '1' && ptn.assist && ptn.assist !== this_group_id)) {
                        $(id + '_plan').css({
                            color: '#808080'
                        });
                    }
                });
            }

            // 行集計
            if (emp.count_row) {
                $.each(emp.count_row, function(count, ptn) {
                    var id = eid + '_' + count;

                    // 予定
                    $(id + '_plan').html(ptn.plan);

                    // 実績
                    $(id + '_results').html(ptn.results);
                });
            }

            // 前月繰越
            $(eid + '_wh_over_plan').html(emp.wh_over.plan);
            $(eid + '_wh_over_results').html(emp.wh_over.results);

            // 当月稼働
            $(eid + '_wh_hour_plan').html(emp.wh_hour.plan);
            $(eid + '_wh_hour_results').html(emp.wh_hour.results);

            // 実働差
            $(eid + '_wh_diff_plan').html(emp.wh_diff.plan);
            $(eid + '_wh_diff_results').html(emp.wh_diff.results);

            // 夜勤時間
            $(eid + '_wh_night_plan').html(emp.wh_night.plan);
            $(eid + '_wh_night_results').html(emp.wh_night.results);

            // 有休付与
            $(eid + '_paid_h_all_plan').html(emp.paid_h_all.plan);
            $(eid + '_paid_h_all_results').html(emp.paid_h_all.results);

            // 有休取得
            $(eid + '_paid_h_get_plan').html(emp.paid_h_get.plan);
            $(eid + '_paid_h_get_results').html(emp.paid_h_get.results);

            // 有休
            $(eid + '_paid_h_plan').html(emp.paid_h.plan);
            $(eid + '_paid_h_results').html(emp.paid_h.results);

            // 週休付与
            $(eid + '_paid_w_all_plan').html(emp.paid_w_all.plan);
            $(eid + '_paid_w_all_results').html(emp.paid_w_all.results);

            // 週休取得
            $(eid + '_paid_w_get_plan').html(emp.paid_w_get.plan);
            $(eid + '_paid_w_get_results').html(emp.paid_w_get.results);

            // 週休
            $(eid + '_paid_w_plan').html(emp.paid_w.plan);
            $(eid + '_paid_w_results').html(emp.paid_w.results);

            // 夏休付与
            $(eid + '_paid_s_all_plan').html(emp.paid_s_all.plan);
            $(eid + '_paid_s_all_results').html(emp.paid_s_all.results);

            // 夏休取得
            $(eid + '_paid_s_get_plan').html(emp.paid_s_get.plan);
            $(eid + '_paid_s_get_results').html(emp.paid_s_get.results);

            // 夏休
            $(eid + '_paid_s_plan').html(emp.paid_s.plan);
            $(eid + '_paid_s_results').html(emp.paid_s.results);

        });
    }

    // 列集計
    if (data.date) {
        $.each(data.date, function(date, d) {
            var did = ['#', date].join('');

            if (d.count_col) {
                $.each(d.count_col, function(count, ptn) {
                    // 実績
                    $(did + '_' + count).html(ptn.results);
                });
            }
        });
    }

    // 平均夜勤時間
    $('#night_hours_avg').text('月平均夜勤時間：' + data.night_hours_avg.results + '時間');
    $('#night_hours_avg').attr('title', '夜勤従事者の延べ夜勤時間：' + data.night_hours.results + '時間 ÷ 夜勤従事者数：' + data.night_nurse_count.results + '人');
}

///-----------------------------------------------------
// 「現在のスタンプ」を設定
//------------------------------------------------------
function setCurrentStamp(ptn_id) {
    if (ptn_id != 0) {
        current_stamp = pattern['p' + ptn_id];
        $('#current_stamp')
            .html(current_stamp.cell)
            .css(current_stamp.style);
    }
    else {
        current_stamp = {
            cell: '',
            style: {
                color: '#000',
                backgroundColor: ''
            }
        };
        $('#current_stamp')
            .html('未設定')
            .css({
                fontSize: '9px',
                color: '#000',
                backgroundColor: ''
            });
    }
}

///-----------------------------------------------------------------------------
// 勤務表に記号をセット
//------------------------------------------------------------------------------
function setSign(cell_obj, sign) {
    var cell = $(cell_obj);

    // 実績セルでなければセットしないで終了
    if (!cell.hasClass('cell')) {
        return;
    }

    current_cell = cell;
    var ids = cell.attr('id').split('_');

    // 今の記号
    var now_sign = getSignData(ids[0], ids[1], ids[2]);

    // 変更してなければ終了
    if (now_sign == sign.id) {
        cell = null;
        return;
    }

    // データを変更する
    setSignData(ids[0], ids[1], ids[2], sign.id);

    // セルの表示を変更する
    cell.html(sign.cell).css(sign.style);
    cell = null;
}

///-----------------------------------------------------------------------------
// 勤務表データから現在の記号を取得
//------------------------------------------------------------------------------
function getSignData(emp_id, date, type) {
    if (!data.emp[emp_id].dates) {
        return null;
    }
    if (!data.emp[emp_id].dates[date]) {
        return null;
    }
    if (!data.emp[emp_id].dates[date][type]) {
        return null;
    }
    return data.emp[emp_id].dates[date][type];
}

///-----------------------------------------------------------------------------
// 勤務表データに記号をセット
//------------------------------------------------------------------------------
function setSignData(emp_id, date, type, pattern) {
    if (!data.emp[emp_id].dates) {
        data.emp[emp_id].dates = {};
    }
    if (!data.emp[emp_id].dates[date]) {
        data.emp[emp_id].dates[date] = {};
    }
    data.emp[emp_id].dates[date][type] = pattern;
}

///-----------------------------------------------------------------------------
// テーブルハイライト in/out (IE6ではハイライトさせない)
//------------------------------------------------------------------------------
function highlight_in(cell) {
    if (!_ua.ltIE8) {
        var ids = cell.attr('id').split('_');
        (current_row = $("#results tr." + ids[0] + '_' + ids[2] + " td")).addClass("hover");
        (current_col = $("#results td." + ids[1])).addClass("hover");
        (current_col_head = $("#results_head td." + ids[1])).addClass("hover");
    }
    else {
        var ids = cell.attr('id').split('_');
        (current_row = $("#results tr." + ids[0] + '_' + ids[2] + " td")).addClass("hover");
        (current_col_head = $("#results_head td." + ids[1])).addClass("hover");
    }
}
function highlight_out() {
    if (!_ua.ltIE8) {
        if (!current_row || !current_col || !current_col_head) {
            return;
        }
        current_row.removeClass("hover");
        current_col.removeClass("hover");
        current_col_head.removeClass("hover");
    }
    else {
        if (!current_row || !current_col_head) {
            return;
        }
        current_row.removeClass("hover");
        current_col_head.removeClass("hover");
    }
}

///-----------------------------------------------------------------------------
// ページ移動イベント
//------------------------------------------------------------------------------
var is_beforeunload = true;
$(window).bind('beforeunload', function() {
    if (is_beforeunload && is_change) {
        return;
    }
});

///-----------------------------------------------------------------------------
// チーム
//------------------------------------------------------------------------------
var td_team = $('#results td.team');
var td_team_head = $('#results_head td.team');
var td_team_width = $('#results_head tr:first-child td.team').outerWidth();
var team_on = function() {
    td_team.css('display', 'none');
    td_team_head.css('display', 'none');
    $('#button_team').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_team_width;
        resizeTableWidth(tblWidth);
    }
};
var team_off = function() {
    if (tblWidth) {
        tblWidth += td_team_width;
        resizeTableWidth(tblWidth);
    }
    td_team.css('display', '');
    td_team_head.css('display', '');
    $('#button_team').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 職種
//------------------------------------------------------------------------------
var td_job = $('#results td.job');
var td_job_head = $('#results_head td.job');
var td_job_width = $('#results_head tr:first-child td.job').outerWidth();
var job_on = function() {
    td_job.css('display', 'none');
    td_job_head.css('display', 'none');
    $('#button_job').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_job_width;
        resizeTableWidth(tblWidth);
    }
};
var job_off = function() {
    if (tblWidth) {
        tblWidth += td_job_width;
        resizeTableWidth(tblWidth);
    }
    td_job.css('display', '');
    td_job_head.css('display', '');
    $('#button_job').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 役職
//------------------------------------------------------------------------------
var td_status = $('#results td.status');
var td_status_head = $('#results_head td.status');
var td_status_width = $('#results_head tr:first-child td.status').outerWidth();
var status_on = function() {
    td_status.css('display', 'none');
    td_status_head.css('display', 'none');
    $('#button_status').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_status_width;
        resizeTableWidth(tblWidth);
    }
};
var status_off = function() {
    if (tblWidth) {
        tblWidth += td_status_width;
        resizeTableWidth(tblWidth);
    }
    td_status.css('display', '');
    td_status_head.css('display', '');
    $('#button_status').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 稼働時間
//------------------------------------------------------------------------------
var td_kadou = $('#results td.kadou');
var td_kadou_head = $('#results_head td.kadou');
var td_kadou_width = $('#results_head tr:first-child td.kadou').outerWidth();
var kadou_on = function() {
    td_kadou.css('display', 'none');
    td_kadou_head.css('display', 'none');
    $('#button_kadou').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_kadou_width;
        resizeTableWidth(tblWidth);
    }
};
var kadou_off = function() {
    if (tblWidth) {
        tblWidth += td_kadou_width;
        resizeTableWidth(tblWidth);
    }
    td_kadou.css('display', '');
    td_kadou_head.css('display', '');
    $('#button_kadou').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 集計
//------------------------------------------------------------------------------
var td_total = $('#results td.total');
var td_total_head = $('#results_head td.total');
var td_total_width = $('#results_head tr:first-child td.total').outerWidth();
var total_on = function() {
    td_total.css('display', 'none');
    td_total_head.css('display', 'none');
    $('#results tfoot tr.count').css('display', 'none');
    $('#button_count').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_total_width;
        resizeTableWidth(tblWidth);
    }
};
var total_off = function() {
    if (tblWidth) {
        tblWidth += td_total_width;
        resizeTableWidth(tblWidth);
    }
    td_total.css('display', '');
    td_total_head.css('display', '');
    $('#results tfoot tr.count').css('display', '');
    $('#button_count').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 有休
//------------------------------------------------------------------------------
var td_holiday = $('#results td.holiday');
var td_holiday_head = $('#results_head td.holiday');
var td_holiday_width = $('#results_head tr:first-child td.holiday').outerWidth();
var holiday_on = function() {
    td_holiday.css('display', 'none');
    td_holiday_head.css('display', 'none');
    $('#button_holiday').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_holiday_width;
        resizeTableWidth(tblWidth);
    }
};
var holiday_off = function() {
    if (tblWidth) {
        tblWidth += td_holiday_width;
        resizeTableWidth(tblWidth);
    }
    td_holiday.css('display', '');
    td_holiday_head.css('display', '');
    $('#button_holiday').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 夏休
//------------------------------------------------------------------------------
var td_summer = $('#results td.summer');
var td_summer_head = $('#results_head td.summer');
var td_summer_width = $('#results_head tr:first-child td.summer').outerWidth();
var summer_on = function() {
    td_summer.css('display', 'none');
    td_summer_head.css('display', 'none');
    $('#button_summer').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_summer_width;
        resizeTableWidth(tblWidth);
    }
};
var summer_off = function() {
    if (tblWidth) {
        tblWidth += td_summer_width;
        resizeTableWidth(tblWidth);
    }
    td_summer.css('display', '');
    td_summer_head.css('display', '');
    $('#button_summer').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 週休
//------------------------------------------------------------------------------
var td_week = $('#results td.week');
var td_week_head = $('#results_head td.week');
var td_week_width = $('#results_head tr:first-child td.week').outerWidth();
var week_on = function() {
    td_week.css('display', 'none');
    td_week_head.css('display', 'none');
    $('#button_week').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_week_width;
        resizeTableWidth(tblWidth);
    }
};
var week_off = function() {
    if (tblWidth) {
        tblWidth += td_week_width;
        resizeTableWidth(tblWidth);
    }
    td_week.css('display', '');
    td_week_head.css('display', '');
    $('#button_week').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 残業
//------------------------------------------------------------------------------
var td_ovtm = $('#results tr.results_ovtm');
var td_ovtmrsn = $('#results tr.results_ovtmrsn');
var td_results = $('#results tr.results td');
var zangyo_on = function() {
    td_ovtm.show();
    td_ovtmrsn.show();
    td_results.css('borderBottom', '1px solid #5279a5');
    $('#button_zangyo').text('残業非表示');
};
var zangyo_off = function() {
    td_ovtm.hide();
    td_ovtmrsn.hide();
    td_results.css('borderBottom', '2px solid #5279a5');
    $('#button_zangyo').text('残業表示');
};

///-----------------------------------------------------------------------------
// unloader
//------------------------------------------------------------------------------
var unloader = function() {
    $('div').unbind();
    $('table').unbind();
};

///-----------------------------------------------------------------------------
// resizeIframeEx
//------------------------------------------------------------------------------
var viewAreaHgt = 0; // 表示領域の高さ
var sclContHgt = 0; // スクロールさせるコンテンツ(#wrapCont)の高さ
var sclContTopRange = 0; // #wrapContの左上の座標
var tblWidth = 0;

function resizeIframeEx() {
    try {
        if (!fscreen) {
            elmFloatingpage = parent.document.getElementById('floatingpage');
            elmFloatingpage.removeAttribute("height"); // 不要な高さ削除
            elmFloatingpage.style.height = '1px';
        }

        /* 表示領域の高さ設定*/
        viewAreaHgt = parent.document.body.clientHeight;
        sclContTopRange = $('#wrapCont').offset().top + 17;

        if (!fscreen) {
            if (parent.callSideMenu == true) {
                sclContTopRange = sclContTopRange + 10;
            }

            // iframeに新しい高さを設定
            var iframeHgt = viewAreaHgt - 120; // (表示領域 - 全体のヘッダー)
            elmFloatingpage.style.height = iframeHgt + 'px';
        }

        // #wrapContに高さを設定
        sclContHgt = !fscreen ? iframeHgt - sclContTopRange : $(window).innerHeight() - sclContTopRange;
        $('#wrapCont').height(sclContHgt + 'px');

        /* 表示領域の幅設定 */
        if (parent.callSideMenu == false || fscreen) {
            // 一時的に#wrapHead&#wrapContに幅を設定
            $('#shift').width('2000px');
            $('#wrapHead').width('2000px');
            $('#wrapCont').width('2000px');

            // テーブル幅を取得
            tblWidth = $('#wrapHead table').outerWidth();
            resizeTableWidth(tblWidth);
        }
        if (!fscreen) {
            parent.callSideMenu = false;
        }

    } catch (e) {
        alert(e);
    }
}

function resizeTableWidth(width) {
    // 一時的に#wrapHead&#wrapContに幅を設定
    $('#shift').width('2000px');
    $('#wrapHead').width('2000px');
    $('#wrapCont').width('2000px');

    // ヘッダー・コンテンツの各テーブルに幅を設定
    $('#wrapHead table').width(width + 'px');
    $('#wrapCont table').width((width - 17) + 'px');

    // ヘッダー・コンテンツの各テーブルを囲っているBoxにテーブル幅+スクロールバー幅を設定
    var shiftWidth = (width > 1000) ? width : 1000;
    $('#shift').width(shiftWidth + 'px');
    $('#wrapHead').width(shiftWidth + 'px');
    if (_ua.ltIE8) {
        $('#wrapCont').width((width + 2) + 'px');
    }
    else if (_ua.IE) {
        $('#wrapCont').width((width + 1) + 'px');
    }
    else {
        $('#wrapCont').width(width + 'px');
    }
}
