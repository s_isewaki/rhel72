// DOM ready
$(function() {
    $('#this_group_id').val(this_group_id);
    $('#select_year').val(this_y);
    $('#select_month').val(this_m);

    $('#cours_year').val(view_y);
    $('#cours_month').val(view_m);

    $('#this_group_id').change(change_staff);
    $('#select_year').change(change_staff);
    $('#select_month').change(change_staff);

    $('#this_staff').change(change_staff_pulldown);
    load_staff();
});

function load_staff() {
    $('#this_staff').load('shift/staff_pulldown.php', {
        group_id: $('#this_group_id').val(),
        year: $('#select_year').val(),
        month: $('#select_month').val(),
        mode: 'pulldown'
    });
    $('#print_staff').load('shift/staff_pulldown.php', {
        group_id: $('#this_group_id').val(),
        year: $('#select_year').val(),
        month: $('#select_month').val(),
        mode: 'checkbox'
    }).show();
}

function change_staff() {
    $('#this_staff').children().remove();
    $('#print_staff').children().remove();
    load_staff();
}

function change_staff_pulldown() {
    if ($('#this_staff').val() === '') {
        $('#print_staff').show();
    }
    else {
        $('#print_staff').hide();
    }
}
function print_pdf(to_action, mode) {
    var staffs = [];
    $('input.this_staff:checked').each(function() {
        staffs.push($(this).val());
    });

    var $year = $('#select_year').val();
    var $month = $('#select_month').val();

    if (mode === 'cours') {
        $year = $('#cours_year').val();
        $month = $('#cours_month').val();
    }

    var url = [
        base_href,
        to_action,
        '?group_id=', $('#this_group_id').val(),
        '&mode=', mode,
        '&year=', $year,
        '&month=', $month,
        '&length=', $('#month_length').val(),
        '&staff=', $('#this_staff').val(),
        '&staffs=', staffs.join(':')
    ].join('');
    window.open(url, "pdf", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=1000,height=721");
}

function print_xls(to_action, mode) {

    var $year = $('#select_year').val();
    var $month = $('#select_month').val();

    if (mode === 'cours') {
        $year = $('#cours_year').val();
        $month = $('#cours_month').val();
    }

    var url = [
        base_href,
        to_action,
        '?group_id=', $('#this_group_id').val(),
        '&mode=', mode,
        '&year=', $year,
        '&month=', $month,
        '&length=', $('#month_length').val(),
        '&staff=', $('#this_staff').val()
    ].join('');
    location.href = url;
}

// チェックボックス
function check_staff(mode) {
    switch (mode) {
        case 'on':
            $('input.this_staff').attr('checked', true);
            break;

        case 'off':
            $('input.this_staff').attr('checked', false);
            break;

        case 'toggle':
            $('input.this_staff').attr('checked', function(e, v) {
                return !v;
            });
            break;
    }
    return false;
}
