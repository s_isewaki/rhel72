/**
 * CoMedix Shift JavaScript
 */

var current_stamp = {
    cell: '',
    style: {
        color: '#000',
        backgroundColor: ''
    }
};
var current_row;
var current_col;
var current_col_head;
var current_cell;
var line2 = false;
var button = {
    hope: false,
    results: false,
    duty: false,
    comment: false
};
var is_stamp = false;
var is_change = false;

var shift = $('#shift');
var loading = $('#loading');

// DOM ready
$(function() {

    ///-----------------------------------------------------
    // テーブルハイライト
    //------------------------------------------------------
    $('#plan').mouseover(function(e) {
        if ($(e.target).hasClass('day')) {
            highlight_in($(e.target));
        }
    }).mouseout(function(e) {
        if ($(e.target).hasClass('day')) {
            highlight_out();
        }
    });

    ///-----------------------------------------------------
    // 表示切り替え
    //------------------------------------------------------
    // チーム
    $('#button_team').toggle(team_on, team_off);
    if (display.team === '0') {
        $('#button_team').click();
    }

    // 職種
    $('#button_job').toggle(job_on, job_off);
    if (display.job === '0') {
        $('#button_job').click();
    }

    // 役職
    $('#button_status').toggle(status_on, status_off);
    if (display.status === '0') {
        $('#button_status').click();
    }

    // 稼働時間
    $('#button_kadou').toggle(kadou_on, kadou_off);
    if (display.kadou === '0') {
        $('#button_kadou').click();
    }

    // 集計
    $('#button_count').toggle(total_on, total_off);
    if (display.count === '0') {
        $('#button_count').click();
    }

    // 有休
    $('#button_holiday').toggle(holiday_on, holiday_off);
    if (display.holiday === '0') {
        $('#button_holiday').click();
    }

    // 夏休
    $('#button_summer').toggle(summer_on, summer_off);
    if (display.summer === '0') {
        $('#button_summer').click();
    }

    // 週休
    $('#button_week').toggle(week_on, week_off);
    if (display.week === '0') {
        $('#button_week').click();
    }

    ///-----------------------------------------------------
    // 2段目
    //------------------------------------------------------
    // 勤務希望
    $('#button_hope').click(function() {
        $('#plan tr.line2').hide();
        $('#shiftmenu_button button.line2').css('fontWeight', 'normal');

        if (line2 && button.hope) {
            line2 = false;
            button.hope = false;
            button.results = false;
            button.duty = false;
            button.comment = false;
        }
        else {
            $('#plan tr.hope').show();
            $('#button_hope').css('fontWeight', 'bold');
            line2 = true;
            button.hope = true;
            button.results = false;
            button.duty = false;
            button.comment = false;
        }
    });
    // 当直
    $('#button_duty').click(function() {
        $('#plan tr.line2').hide();
        $('#shiftmenu_button button.line2').css('fontWeight', 'normal');

        if (line2 && button.duty) {
            line2 = false;
            button.hope = false;
            button.results = false;
            button.duty = false;
            button.comment = false;
        }
        else {
            $('#plan tr.duty').show();
            $('#button_duty').css('fontWeight', 'bold');
            line2 = true;
            button.duty = true;
            button.hope = false;
            button.results = false;
            button.comment = false;
        }
    });
    // 予実績
    $('#button_results').click(function() {
        $('#plan tr.line2').hide();
        $('#shiftmenu_button button.line2').css('fontWeight', 'normal');

        if (line2 && button.results) {
            line2 = false;
            button.hope = false;
            button.results = false;
            button.duty = false;
            button.comment = false;
        }
        else {
            $('#plan tr.results').show();
            $('#button_results').css('fontWeight', 'bold');
            line2 = true;
            button.results = true;
            button.hope = false;
            button.duty = false;
            button.comment = false;
        }
    });
    // コメント
    $('#button_comment').click(function() {
        $('#plan tr.line2').hide();
        $('#shiftmenu_button button.line2').css('fontWeight', 'normal');

        if (line2 && button.comment) {
            line2 = false;
            button.hope = false;
            button.results = false;
            button.duty = false;
            button.comment = false;
        }
        else {
            $('#plan tr.comment').show();
            $('#button_comment').css('fontWeight', 'bold');
            line2 = true;
            button.comment = true;
            button.hope = false;
            button.results = false;
            button.duty = false;
        }
    });

    ///-----------------------------------------------------
    // 勤務表クリック
    //------------------------------------------------------
    $('#plan').click(function(e) {
        var cell = $(e.target);
        if (!cell.hasClass('cell')) {
            return;
        }

        //[スタンプ]クリックセルに反映する
        if (is_stamp) {
            setSign(e.target, current_stamp);
            return;
        }

        //キーボード表示
        $('#keyboard').show();

        //目印表示
        if (current_cell) {
            current_cell.removeClass('flag');   //一旦消す
        }
        cell.addClass('flag');   //目印表示

        //選択セルID保持
        current_cell = cell;
    });

    ///-----------------------------------------------------
    // 登録
    //------------------------------------------------------
    $('#icon_plan').click(function() {
        highlight_out();

        if (!confirm('勤務予定を登録します。')) {
            return false;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'shift/plan_ajax.php',
            data: {
                group_id: this_group_id,
                year: this_y,
                month: this_m,
                mode: 'plan',
                data: data
            },
            success: function(json) {
                plan_cells(json);
                loading.hide();
                data = json;
                alert('勤務予定の登録が完了しました。');
                is_change = false;
            },
            beforeSend: function() {
                loading.show();
            }
        });
    });

    ///-----------------------------------------------------
    // 下書き
    //------------------------------------------------------
    $('#icon_draft').click(function() {
        highlight_out();

        if (!confirm('下書き保存します。登録済みの予定や、以前の下書きは消去されますがよろしいでしょうか?')) {
            return false;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'shift/plan_ajax.php',
            data: {
                group_id: this_group_id,
                year: this_y,
                month: this_m,
                mode: 'draft',
                data: data
            },
            success: function(json) {
                plan_cells(json);
                loading.hide();
                data = json;
                alert('下書き保存が完了しました。');
                is_change = false;
            },
            beforeSend: function() {
                loading.show();
            }
        });
    });

    ///-----------------------------------------------------
    // スタンプ
    //------------------------------------------------------
    $('#icon_stamp').toggle(function() {
        $("#stamp").show();
        $('#icon_stamp>img').attr('src', 'img/ico-illust-stamp-off.gif');
        is_stamp = true;

        //Key入力モード終了
        $("#keyboard").hide();                                       //キー入力フローティングを非表示
        if (current_cell) {
            current_cell.removeClass('flag');   //一旦消す
        }

    }, function() {
        $("#stamp").hide();
        $('#icon_stamp>img').attr('src', 'img/ico-illust-stamp.gif');
        is_stamp = false;
    });
    // スタンプ画面クローズ
    $("#stamp_header span.close").click(function() {
        $('#icon_stamp').click();
    });
    // スタンプ画面ドラッグ＆ドロップ
    $("#stamp_header").mousedown(function(e) {
        $("#stamp_header").css('cursor', 'move');
        $("#stamp")
            .data("clickPointX", e.pageX - $("#stamp").offset().left)
            .data("clickPointY", e.pageY - $("#stamp").offset().top);

        $(document).mousemove(function(e) {
            $("#stamp").css({
                top: e.pageY - $("#stamp").data("clickPointY") + "px",
                left: e.pageX - $("#stamp").data("clickPointX") + "px"
            });
        });
    })
        .mouseup(function() {
            $(document).unbind("mousemove");
            $("#stamp_header").css('cursor', 'pointer');
        });

    ///-----------------------------------------------------
    // キーボード
    //------------------------------------------------------
    $("#keyboard_header span.close").click(function() {
        $("#keyboard").hide();                                       //キー入力フローティングを非表示
        if (current_cell) {
            current_cell.removeClass('flag');   //一旦消す
        }
    });
    // キーボード画面ドラッグ＆ドロップ
    $("#keyboard_header").mousedown(function(e) {
        $("#keyboard_header").css('cursor', 'move');
        $("#keyboard")
            .data("clickPointX", e.pageX - $("#keyboard").offset().left)
            .data("clickPointY", e.pageY - $("#keyboard").offset().top);

        $(document).mousemove(function(e) {
            $("#keyboard").css({
                top: e.pageY - $("#keyboard").data("clickPointY") + "px",
                left: e.pageX - $("#keyboard").data("clickPointX") + "px"
            });
        });
    })
        .mouseup(function() {
            $(document).unbind("mousemove");
            $("#keyboard_header").css('cursor', 'pointer');
        });

    ///-----------------------------------------------------
    // 再計算
    //------------------------------------------------------
    $('#icon_reload').click(function() {
        highlight_out();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'shift/plan_ajax.php',
            data: {
                group_id: this_group_id,
                year: this_y,
                month: this_m,
                mode: 'reload',
                data: data
            },
            success: function(json) {
                plan_cells(json);
                loading.hide();
                data = json;
                alert('再計算が完了しました。');
            },
            beforeSend: function() {
                loading.show();
            }
        });
    });

    ///-----------------------------------------------------
    // コピー
    //------------------------------------------------------
    $('#icon_copy').click(function() {
        highlight_out();
        show_copy_form();
    });

    $("#copy_from").datepicker("option", {
        maxDate: prev_end_date,
        numberOfMonths: 2,
        showButtonPanel: true
    });

    $("#copy_to").datepicker("option", {
        minDate: start_date,
        maxDate: end_date,
        numberOfMonths: 2,
        showButtonPanel: true
    });

    ///-----------------------------------------------------
    // チェック
    //------------------------------------------------------
    $('#icon_check').click(function() {
        highlight_out();


    });

    ///-----------------------------------------------------
    // 印刷
    //------------------------------------------------------

    ///-----------------------------------------------------
    // Excel
    //------------------------------------------------------

    ///-----------------------------------------------------
    // 全画面
    //------------------------------------------------------
    $('#icon_fullscreen').click(fullscreen);

    ///-----------------------------------------------------
    // 当月職員設定
    //------------------------------------------------------
    $('#button_plan_staff').click(function() {
        location.href = base_href + 'shift/staff_plan.php?y=' + this_y + '&m=' + this_m + '&group_id=' + this_group_id;
    });

    ///-----------------------------------------------------
    // 勤務希望取込
    //------------------------------------------------------
    $('#button_hope_import').click(function() {
        highlight_out();

        if (!confirm('勤務希望を予定に取り込みます。\n予定がすでに入っている日には取り込まれません。')) {
            return false;
        }
        loading.show();
        $.each(data.emp, function(emp_id, emp) {
            var eid = '#' + emp_id;
            if (emp.dates) {
                $.each(emp.dates, function(date, ptn) {
                    if ((!ptn.plan || ptn.plan === '0') && ptn.hope && ptn.hope !== '0') {
                        var id = eid + '_' + date;
                        setSignData(emp_id, date, 'plan', ptn.hope);
                        p = pattern['p' + ptn.hope];
                        $(id + '_plan').html(p.cell).css(p.style);
                        is_change = true;
                    }
                });
            }
        });
        loading.hide();
        alert('勤務希望を取り込みました。');
    });

    ///-----------------------------------------------------
    // 登録履歴
    //------------------------------------------------------
    $('#button_history').toggle(function() {
        highlight_out();
        $('#history').load('shift/plan_pdf_history.php', {
            group_id: this_group_id,
            year: this_y,
            month: this_m
        }, function() {
            $('#history').show();
        });
    },
        function() {
            $('#history').hide();
        });

    ///-----------------------------------------------------
    // 自動作成
    //------------------------------------------------------

    ///-----------------------------------------------------
    // 曜日集計表
    //------------------------------------------------------


    ///-----------------------------------------------------
    // <a href="#hoge">のクリックでbeforeunloadイベントが走ってしまう対策
    //------------------------------------------------------
    $('a').click(function(e) {
        if (e.target.href && e.target.href.indexOf('javascript') === 0) {
            is_beforeunload = false;
            setTimeout(function() {
                is_beforeunload = true;
            }, 0);
        }
    });

    ///-----------------------------------------------------
    // セッション維持
    //------------------------------------------------------
    setInterval(function() {
        $.post('shift/session_keep_ajax.php');
    }, 300000);

    loading.hide();
});

///-----------------------------------------------------
// 勤務予定表テーブルにデータを埋める
//------------------------------------------------------
function plan_cells(data, type) {
    if (data.emp) {
        $.each(data.emp, function(emp_id, emp) {
            var eid = ['#', emp_id].join('');

            // 職員情報(初期読み込み時のみセット)
            if (type === 'init') {
                // 名前+性別
                $(eid + '_name').html(emp.name).addClass('sex' + emp.sex);

                // チーム
                $(eid + '_team').html(emp.team);

                // 職種
                $(eid + '_job').html(emp.job_name);

                // 役職
                $(eid + '_status').html(emp.st_name);

                // 応援
                if (emp.assist === '1') {
                    $(eid + '_name').addClass('assist');
                }
            }
            // 勤務表
            if (emp.dates) {
                $.each(emp.dates, function(date, ptn) {
                    var id = [eid, date].join('_');
                    var p;
                    var plan_pattern = pattern;
                    var results_pattern = pattern;

                    // 応援の勤務パターンか否か
                    if (ptn.plan_pattern_id && ptn.plan_pattern_id !== 'null' && ptn.plan_pattern_id != this_pattern_id) {
                        plan_pattern = data.pattern['ptn' + ptn.plan_pattern_id];
                    }
                    if (ptn.results_pattern_id && ptn.results_pattern_id !== 'null' && ptn.results_pattern_id != this_pattern_id) {
                        results_pattern = data.pattern['ptn' + ptn.results_pattern_id];
                    }

                    // 予定
                    if (p = plan_pattern['p' + ptn.plan]) {
                        $(id + '_plan').html(p.cell).css(p.style);
                    }

                    // 希望
                    if (p = plan_pattern['p' + ptn.hope]) {
                        $(id + '_hope').html(p.cell).css(p.style);
                    }

                    // 実績
                    if (p = results_pattern['p' + ptn.results]) {
                        $(id + '_results').html(p.cell).css(p.style);
                    }

                    // 当直
                    if (ptn.duty && ptn.duty === '1') {
                        $(id + '_duty').html('●');
                    }

                    // コメント
                    $(id + '_comment').html(ptn.comment);

                    // 応援
                    if (ptn.plan !== '0' && ((emp.assist === '1' && ptn.assist !== this_group_id)
                        || (emp.assist !== '1' && ptn.assist && ptn.assist !== 'null' && ptn.assist !== this_group_id))) {
                        $(id + '_plan').css({
                            color: '#808080'
                        });
                        $(id + '_plan').removeClass('cell');
                    }
                    if (ptn.hope !== '0' && ((emp.assist === '1' && ptn.assist !== this_group_id)
                        || (emp.assist !== '1' && ptn.assist && ptn.assist !== 'null' && ptn.assist !== this_group_id))) {
                        $(id + '_hope').css({
                            color: '#808080'
                        });
                        $(id + '_hope').removeClass('cell');
                    }
                });
            }

            // 行集計
            if (emp.count_row) {
                $.each(emp.count_row, function(count, ptn) {
                    var id = eid + '_' + count;

                    // 予定
                    $(id + '_plan').html(ptn.plan);

                    // 希望
                    $(id + '_hope').html(ptn.hope);

                    // 実績
                    $(id + '_results').html(ptn.results);
                });
            }

            // 前月繰越
            $(eid + '_wh_over_plan').html(emp.wh_over.plan);
            $(eid + '_wh_over_results').html(emp.wh_over.results);

            // 当月稼働
            $(eid + '_wh_hour_plan').html(emp.wh_hour.plan);
            $(eid + '_wh_hour_results').html(emp.wh_hour.results);

            // 実働差
            $(eid + '_wh_diff_plan').html(emp.wh_diff.plan);
            $(eid + '_wh_diff_results').html(emp.wh_diff.results);

            // 夜勤時間
            $(eid + '_wh_night_plan').html(emp.wh_night.plan);
            $(eid + '_wh_night_results').html(emp.wh_night.results);

            // 有休付与
            $(eid + '_paid_h_all_plan').html(emp.paid_h_all.plan);
            $(eid + '_paid_h_all_hope').html(emp.paid_h_all.hope);
            $(eid + '_paid_h_all_results').html(emp.paid_h_all.results);

            // 有休取得
            $(eid + '_paid_h_get_plan').html(emp.paid_h_get.plan);
            $(eid + '_paid_h_get_hope').html(emp.paid_h_get.hope);
            $(eid + '_paid_h_get_results').html(emp.paid_h_get.results);

            // 有休
            $(eid + '_paid_h_plan').html(emp.paid_h.plan);
            $(eid + '_paid_h_hope').html(emp.paid_h.hope);
            $(eid + '_paid_h_results').html(emp.paid_h.results);

            // 週休付与
            $(eid + '_paid_w_all_plan').html(emp.paid_w_all.plan);
            $(eid + '_paid_w_all_hope').html(emp.paid_w_all.hope);
            $(eid + '_paid_w_all_results').html(emp.paid_w_all.results);

            // 週休取得
            $(eid + '_paid_w_get_plan').html(emp.paid_w_get.plan);
            $(eid + '_paid_w_get_hope').html(emp.paid_w_get.hope);
            $(eid + '_paid_w_get_results').html(emp.paid_w_get.results);

            // 週休
            $(eid + '_paid_w_plan').html(emp.paid_w.plan);
            $(eid + '_paid_w_hope').html(emp.paid_w.hope);
            $(eid + '_paid_w_results').html(emp.paid_w.results);

            // 夏休付与
            $(eid + '_paid_s_all_plan').html(emp.paid_s_all.plan);
            $(eid + '_paid_s_all_hope').html(emp.paid_s_all.hope);
            $(eid + '_paid_s_all_results').html(emp.paid_s_all.results);

            // 夏休取得
            $(eid + '_paid_s_get_plan').html(emp.paid_s_get.plan);
            $(eid + '_paid_s_get_hope').html(emp.paid_s_get.hope);
            $(eid + '_paid_s_get_results').html(emp.paid_s_get.results);

            // 夏休
            $(eid + '_paid_s_plan').html(emp.paid_s.plan);
            $(eid + '_paid_s_hope').html(emp.paid_s.hope);
            $(eid + '_paid_s_results').html(emp.paid_s.results);

        });
    }

    // 列集計
    if (data.date) {
        $.each(data.date, function(date, d) {
            var did = ['#', date].join('');

            if (d.count_col) {
                $.each(d.count_col, function(count, ptn) {
                    // 予定
                    $(did + '_' + count).html(ptn.plan);
                });
            }
        });
    }

    $('#display_plan').hide();
    $('#display_draft').hide();

    // 下書きあり
    if (data.display_draft) {
        $('#display_draft').show();
    }

    // 登録あり
    if (data.display_plan) {
        $('#display_plan').show();
    }

    // 平均夜勤時間
    $('#night_hours_avg').text('月平均夜勤時間：' + data.night_hours_avg.plan + '時間');
    $('#night_hours_avg').attr('title', '夜勤従事者の延べ夜勤時間：' + data.night_hours.plan + '時間 ÷ 夜勤従事者数：' + data.night_nurse_count.plan + '人');
}

///-----------------------------------------------------
// 「現在のスタンプ」を設定
//------------------------------------------------------
function setCurrentStamp(ptn_id) {
    if (ptn_id != 0) {
        current_stamp = pattern['p' + ptn_id];
        $('#current_stamp')
            .html(current_stamp.cell)
            .css(current_stamp.style);
    }
    else {
        current_stamp = {
            cell: '',
            style: {
                color: '#000',
                backgroundColor: ''
            }
        };
        $('#current_stamp')
            .html('未設定')
            .css({
                fontSize: '9px',
                color: '#000',
                backgroundColor: ''
            });
    }
}

///-----------------------------------------------------------------------------
// 勤務表に記号をセット
//------------------------------------------------------------------------------
function setSign(cell_obj, sign) {
    var cell = $(cell_obj);

    // 予定表セルでなければセットしないで終了
    if (!cell.hasClass('cell')) {
        return;
    }

    current_cell = cell;
    var ids = cell.attr('id').split('_');

    // 今の記号
    var now_sign_id = getSignData(ids[0], ids[1], ids[2]);
    var now_sign = {};
    if (now_sign_id && now_sign_id !== '0') {
        now_sign = pattern['p' + now_sign_id];
    }

    // 変更してなければ終了
    if (now_sign_id == sign.id) {
        return;
    }

    switch (ids[2]) {
        // 予定
        case 'plan':
            var now_hope = getSignData(ids[0], ids[1], 'hope');
            if (now_hope !== null && now_hope !== '0' && now_hope != sign.id) {
                if (!confirm('勤務希望と異なる予定を設定しますがよろしいですか?')) {
                    return;
                }
            }

            // 変更フラグを立てる
            is_change = true;
            break;

            // 希望
        case 'hope':
            if (now_sign_id !== null && now_sign_id !== '0') {
                if (!confirm('設定済みの勤務希望を上書きしますがよろしいですか?')) {
                    return;
                }
            }
            $.ajax({
                type: "POST",
                url: 'shift/plan_ajax_hope.php',
                data: {
                    group_id: this_group_id,
                    emp_id: ids[0],
                    date: ids[1],
                    pattern: sign.id
                }
            });
            break;

        default:
            break;

    }

    // データを変更する
    setSignData(ids[0], ids[1], ids[2], sign.id);

    // セルの表示を変更する
    cell.html(sign.cell).css(sign.style);

    // 組み合わせ
    var next;
    if (next = pattern_ok['p' + sign.id]) {
        if (moveFlag('right')) {
            setSign(current_cell, pattern['p' + next]);
        }
    }

    // 当直連動
    if (sign.duty_connect == "1" && ids[2] == 'plan') {
        $('#' + ids[0] + '_' + ids[1] + '_duty').html('●');
        setSignData(ids[0], ids[1], 'duty', "1");
    }
    else if (now_sign && now_sign.duty_connect == "1" && sign.duty_connect != "1" && ids[2] == 'plan') {
        $('#' + ids[0] + '_' + ids[1] + '_duty').html('');
        setSignData(ids[0], ids[1], 'duty', "");
    }

    cell = null;
}

///-----------------------------------------------------------------------------
// 勤務表データから現在の記号を取得
//------------------------------------------------------------------------------
function getSignData(emp_id, date, type) {
    if (!data.emp[emp_id].dates) {
        return null;
    }
    if (!data.emp[emp_id].dates[date]) {
        return null;
    }
    if (!data.emp[emp_id].dates[date][type]) {
        return null;
    }
    return data.emp[emp_id].dates[date][type];
}

///-----------------------------------------------------------------------------
// 勤務表データに記号をセット
//------------------------------------------------------------------------------
function setSignData(emp_id, date, type, pattern) {
    if (!data.emp[emp_id].dates) {
        data.emp[emp_id].dates = {};
    }
    if (!data.emp[emp_id].dates[date]) {
        data.emp[emp_id].dates[date] = {};
    }
    data.emp[emp_id].dates[date][type] = pattern;

    // 応援
    if (data.emp[emp_id].assist === '1' && type == 'plan') {
        data.emp[emp_id].dates[date]['plan_pattern_id'] = this_pattern_id;
        data.emp[emp_id].dates[date]['assist'] = this_group_id;
    }
}

///-----------------------------------------------------------------------------
// フローティングキー入力子画面クリック
//------------------------------------------------------------------------------
function funcKeyboardExec(key) {
    $('#key_' + key).click();
}

///-----------------------------------------------------------------------------
// フローティングキー入力子画面クリック処理
//------------------------------------------------------------------------------
function setPtn(id) {
    setSign(current_cell, pattern['p' + id]);
    moveFlag('right');
}

///-----------------------------------------------------------------------------
// フローティングキー入力子画面クリック処理(未設定)
//------------------------------------------------------------------------------
function setClear() {
    setSign(current_cell, {
        cell: '',
        style: {
            color: '#000',
            backgroundColor: ''
        }
    });
    moveFlag('right');
}

///-----------------------------------------------------------------------------
// 目印画像を指定IDの場所へ移動、指定ID情報を確保
// IN element_id :エレメントID                      ※Null時はグローバル変数『select_cell_id』を参照
// IN direction  :移動する方向  right,left,up,down  ※Null時はright
// IN steps      :横へ進むコマ数                    ※Null時は1コマ  up,downは１コマ固定([当直表示],[コメント表示],[予定表示]複数行表示なので２コマ移動)
//------------------------------------------------------------------------------
function moveFlag(direction) {
    if (!current_cell) {
        return false;
    }

    var ids = current_cell.attr('id').split('_');
    var moved;
    var col_idx = current_cell.index();
    var row_idx = current_cell.closest('tr').index();

    switch (direction) {
        // →
        case 'right':
            moved = current_cell.next('td');
            break;
            // ←
        case 'left':
            moved = current_cell.prev('td');
            break;
            // ↑
        case 'up':
            if (row_idx <= 0) {
                return;
            }
            if (line2) {
                if (button.hope) {
                    row_idx -= ids[2] === 'hope' ? 1 : 4;
                }
                else {
                    row_idx -= 5;
                }
            }
            else {
                row_idx -= 5;
            }
            moved = current_cell.closest('tbody').children('tr').eq(row_idx).children('td').eq(col_idx);
            moveScroll('up', moved);
            break;
            // ↓
        case 'down':
            if (line2) {
                if (button.hope) {
                    row_idx += ids[2] === 'hope' ? 4 : 1;
                }
                else {
                    row_idx += 5;
                }
            }
            else {
                row_idx += 5;
            }
            moved = current_cell.closest('tbody').children('tr').eq(row_idx).children('td').eq(col_idx);
            moveScroll('down', moved);
            break;
            // Enter
        case 'enter':
            if (line2) {
                if (button.hope) {
                    row_idx += ids[2] === 'hope' ? 4 : 1;
                }
                else {
                    row_idx += 5;
                }
            }
            else {
                row_idx += 5;
            }
            moved = current_cell.closest('tbody').children('tr').eq(row_idx).children('td').eq(4);
            moveScroll('down', moved);
            break;
        default:
            break;
    }
    if (!moved.hasClass('cell')) {
        return false;
    }

    if (!is_stamp) {
        current_cell.removeClass('flag');
        moved.addClass('flag');
        highlight_out();
        highlight_in(moved);
    }
    current_cell = moved;
    return true;
}

///-----------------------------------------------------------------------------
// キー入力によるテーブルのセル移動に、スクロールバーを連動させる
//------------------------------------------------------------------------------
function moveScroll(arrow_key, obj) {
    // 移動先の座標を取得
    if (obj.length) {
        var targetCell = obj.offset();
        targetCellY = targetCell.top - (sclContTopRange - 17);
    }
    else {
        targetCellY = null;
    }

    // 移動量
    moveY = '40px';

    // スクロールコンテンツを取得
    var ySclCont = $('#wrapCont');  // 縦

    // 押下キー毎にスクロールを制御
    switch (arrow_key) {
        // ↑
        case 'up':
            if ((sclContHgt / 4) > targetCellY) {
                ySclCont.animate({
                    "scrollTop": "-=20px"
                });
            }
            break;
            // ↓
        case 'down':
            if (targetCellY) {
                if ((sclContHgt - 60) < targetCellY) {
                    ySclCont.animate({
                        "scrollTop": "+=20px"
                    });
                }
            }
            break;
        default:
            break;
    }
}

///-----------------------------------------------------------------------------
// テーブルハイライト in/out (IE6ではハイライトさせない)
//------------------------------------------------------------------------------
function highlight_in(cell) {
    if (!_ua.ltIE8) {
        var ids = cell.attr('id').split('_');
        (current_row = $("#plan tr." + ids[0] + '_' + ids[2] + " td")).addClass("hover");
        (current_col = $("#plan td." + ids[1])).addClass("hover");
        (current_col_head = $("#plan_head td." + ids[1])).addClass("hover");
    }
    else {
        var ids = cell.attr('id').split('_');
        (current_row = $("#plan tr." + ids[0] + '_' + ids[2] + " td")).addClass("hover");
        (current_col_head = $("#plan_head td." + ids[1])).addClass("hover");
    }
}
function highlight_out() {
    if (!_ua.ltIE8) {
        if (!current_row || !current_col || !current_col_head) {
            return;
        }
        current_row.removeClass("hover");
        current_col.removeClass("hover");
        current_col_head.removeClass("hover");
    }
    else {
        if (!current_row || !current_col_head) {
            return;
        }
        current_row.removeClass("hover");
        current_col_head.removeClass("hover");
    }
}

///-----------------------------------------------------------------------------
// キー押下イベント
//------------------------------------------------------------------------------
$(document).keydown(function(e) {
    // イベントの上位伝播を防止
    e.preventDefault();
    e.stopPropagation();

    switch (e.keyCode) {
        case 192:	//[@];
        case 32:	//[スペース];
            setClear();//未設
            break;
        case 37:	//[←]
            moveFlag('left');
            break;
        case 39:	//[→]
            moveFlag('right');
            break;
        case 38:	//[↑]
            moveFlag('up');
            break;
        case 40:	//[↓]
            moveFlag('down');
            break;
        case 13:	//[Enter]
            moveFlag('enter');
            break;
        case 49:	//[1]
            funcKeyboardExec('1');
            break;
        case 50:	//[2]
            funcKeyboardExec('2');
            break;
        case 51:	//[3]
            funcKeyboardExec('3');
            break;
        case 52:	//[4]
            funcKeyboardExec('4');
            break;
        case 53:	//[5]
            funcKeyboardExec('5');
            break;
        case 54:	//[6]
            funcKeyboardExec('6');
            break;
        case 55:	//[7]
            funcKeyboardExec('7');
            break;
        case 56:	//[8]
            funcKeyboardExec('8');
            break;
        case 57:	//[9]
            funcKeyboardExec('9');
            break;
        case 48:	//[0]
            funcKeyboardExec('10');
            break;
        case 81:	//[Q]
            funcKeyboardExec('11');
            break;
        case 87:	//[W]
            funcKeyboardExec('12');
            break;
        case 69:	//[E]
            funcKeyboardExec('13');
            break;
        case 82:	//[R]
            funcKeyboardExec('14');
            break;
        case 84:	//[T]
            funcKeyboardExec('15');
            break;
        case 89:	//[Y]
            funcKeyboardExec('16');
            break;
        case 85:	//[U]
            funcKeyboardExec('17');
            break;
        case 73:	//[I]
            funcKeyboardExec('18');
            break;
        case 79:	//[O]
            funcKeyboardExec('19');
            break;
        case 80:	//[P]
            funcKeyboardExec('20');
            break;
        case 65:	//[A]
            funcKeyboardExec('21');
            break;
        case 83:	//[S]
            funcKeyboardExec('22');
            break;
        case 68:	//[D]
            funcKeyboardExec('23');
            break;
        case 70:	//[F]
            funcKeyboardExec('24');
            break;
        case 71:	//[G]
            funcKeyboardExec('25');
            break;
        case 72:	//[H]
            funcKeyboardExec('26');
            break;
        case 74:	//[J]
            funcKeyboardExec('27');
            break;
        case 75:	//[K]
            funcKeyboardExec('28');
            break;
        case 76:	//[L]
            funcKeyboardExec('29');
            break;
        case 90:	//[Z]
            funcKeyboardExec('30');
            break;
        case 88:	//[X]
            funcKeyboardExec('31');
            break;
        case 67:	//[C]
            funcKeyboardExec('32');
            break;
        case 86:	//[V]
            funcKeyboardExec('33');
            break;
        case 66:	//[B]
            funcKeyboardExec('34');
            break;
        case 78:	//[N]
            funcKeyboardExec('35');
            break;
        case 77:	//[M]
            funcKeyboardExec('36');
            break;
    }
});

///-----------------------------------------------------------------------------
// ページ移動イベント
//------------------------------------------------------------------------------
var is_beforeunload = true;
$(window).bind('beforeunload', function() {
    if (is_beforeunload && is_change) {
        return;
    }
});

///-----------------------------------------------------------------------------
// チーム
//------------------------------------------------------------------------------
var td_team = $('#plan td.team');
var td_team_head = $('#plan_head td.team');
var td_team_width = $('#plan_head tr:first-child td.team').outerWidth();
var team_on = function() {
    td_team.css('display', 'none');
    td_team_head.css('display', 'none');
    $('#button_team').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_team_width;
        resizeTableWidth(tblWidth);
    }
};
var team_off = function() {
    if (tblWidth) {
        tblWidth += td_team_width;
        resizeTableWidth(tblWidth);
    }
    td_team.css('display', '');
    td_team_head.css('display', '');
    $('#button_team').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 職種
//------------------------------------------------------------------------------
var td_job = $('#plan td.job');
var td_job_head = $('#plan_head td.job');
var td_job_width = $('#plan_head tr:first-child td.job').outerWidth();
var job_on = function() {
    td_job.css('display', 'none');
    td_job_head.css('display', 'none');
    $('#button_job').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_job_width;
        resizeTableWidth(tblWidth);
    }
};
var job_off = function() {
    if (tblWidth) {
        tblWidth += td_job_width;
        resizeTableWidth(tblWidth);
    }
    td_job.css('display', '');
    td_job_head.css('display', '');
    $('#button_job').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 役職
//------------------------------------------------------------------------------
var td_status = $('#plan td.status');
var td_status_head = $('#plan_head td.status');
var td_status_width = $('#plan_head tr:first-child td.status').outerWidth();
var status_on = function() {
    td_status.css('display', 'none');
    td_status_head.css('display', 'none');
    $('#button_status').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_status_width;
        resizeTableWidth(tblWidth);
    }
};
var status_off = function() {
    if (tblWidth) {
        tblWidth += td_status_width;
        resizeTableWidth(tblWidth);
    }
    td_status.css('display', '');
    td_status_head.css('display', '');
    $('#button_status').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 稼働時間
//------------------------------------------------------------------------------
var td_kadou = $('#plan td.kadou');
var td_kadou_head = $('#plan_head td.kadou');
var td_kadou_width = $('#plan_head tr:first-child td.kadou').outerWidth();
var kadou_on = function() {
    td_kadou.css('display', 'none');
    td_kadou_head.css('display', 'none');
    $('#button_kadou').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_kadou_width;
        resizeTableWidth(tblWidth);
    }
};
var kadou_off = function() {
    if (tblWidth) {
        tblWidth += td_kadou_width;
        resizeTableWidth(tblWidth);
    }
    td_kadou.css('display', '');
    td_kadou_head.css('display', '');
    $('#button_kadou').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 集計
//------------------------------------------------------------------------------
var td_total = $('#plan td.total');
var td_total_head = $('#plan_head td.total');
var td_total_width = $('#plan_head tr:first-child td.total').outerWidth();
var total_on = function() {
    td_total.css('display', 'none');
    td_total_head.css('display', 'none');
    $('#plan tfoot tr.count').css('display', 'none');
    $('#button_count').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_total_width;
        resizeTableWidth(tblWidth);
    }
};
var total_off = function() {
    if (tblWidth) {
        tblWidth += td_total_width;
        resizeTableWidth(tblWidth);
    }
    td_total.css('display', '');
    td_total_head.css('display', '');
    $('#plan tfoot tr.count').css('display', '');
    $('#button_count').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 有休
//------------------------------------------------------------------------------
var td_holiday = $('#plan td.holiday');
var td_holiday_head = $('#plan_head td.holiday');
var td_holiday_width = $('#plan_head tr:first-child td.holiday').outerWidth();
var holiday_on = function() {
    td_holiday.css('display', 'none');
    td_holiday_head.css('display', 'none');
    $('#button_holiday').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_holiday_width;
        resizeTableWidth(tblWidth);
    }
};
var holiday_off = function() {
    if (tblWidth) {
        tblWidth += td_holiday_width;
        resizeTableWidth(tblWidth);
    }
    td_holiday.css('display', '');
    td_holiday_head.css('display', '');
    $('#button_holiday').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 夏休
//------------------------------------------------------------------------------
var td_summer = $('#plan td.summer');
var td_summer_head = $('#plan_head td.summer');
var td_summer_width = $('#plan_head tr:first-child td.summer').outerWidth();
var summer_on = function() {
    td_summer.css('display', 'none');
    td_summer_head.css('display', 'none');
    $('#button_summer').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_summer_width;
        resizeTableWidth(tblWidth);
    }
};
var summer_off = function() {
    if (tblWidth) {
        tblWidth += td_summer_width;
        resizeTableWidth(tblWidth);
    }
    td_summer.css('display', '');
    td_summer_head.css('display', '');
    $('#button_summer').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// 週休
//------------------------------------------------------------------------------
var td_week = $('#plan td.week');
var td_week_head = $('#plan_head td.week');
var td_week_width = $('#plan_head tr:first-child td.week').outerWidth();
var week_on = function() {
    td_week.css('display', 'none');
    td_week_head.css('display', 'none');
    $('#button_week').css({
        fontWeight: 'normal',
        color: 'silver'
    });
    if (tblWidth) {
        tblWidth -= td_week_width;
        resizeTableWidth(tblWidth);
    }
};
var week_off = function() {
    if (tblWidth) {
        tblWidth += td_week_width;
        resizeTableWidth(tblWidth);
    }
    td_week.css('display', '');
    td_week_head.css('display', '');
    $('#button_week').css({
        fontWeight: 'bold',
        color: 'black'
    });
};

///-----------------------------------------------------------------------------
// resizeIframeEx
//------------------------------------------------------------------------------
var viewAreaHgt = 0; // 表示領域の高さ
var sclContHgt = 0; // スクロールさせるコンテンツ(#wrapCont)の高さ
var sclContTopRange = 0; // #wrapContの左上の座標
var tblWidth = 0;
var elmFloatingpage = "";

function resizeIframeEx() {
    try {
        if (!fscreen) {
            elmFloatingpage = parent.document.getElementById('floatingpage');
            elmFloatingpage.removeAttribute("height"); // 不要な高さ削除
            elmFloatingpage.style.height = '1px';
        }

        /* 表示領域の高さ設定*/
        viewAreaHgt = parent.document.body.clientHeight;
        sclContTopRange = $('#wrapCont').offset().top + 17;

        if (!fscreen) {
            if (parent.callSideMenu == true) {
                sclContTopRange = sclContTopRange + 10;
            }

            // iframeに新しい高さを設定
            var iframeHgt = viewAreaHgt - 120; // (表示領域 - 全体のヘッダー)
            elmFloatingpage.style.height = iframeHgt + 'px';
        }

        // #wrapContに高さを設定
        sclContHgt = !fscreen ? iframeHgt - sclContTopRange : $(window).innerHeight() - sclContTopRange;
        $('#wrapCont').height(sclContHgt + 'px');

        /* 表示領域の幅設定 */
        if (parent.callSideMenu == false || fscreen) {
            // 一時的に#wrapHead&#wrapContに幅を設定
            $('#shift').width('2000px');
            $('#wrapHead').width('2000px');
            $('#wrapCont').width('2000px');

            // テーブル幅を取得
            tblWidth = $('#wrapHead table').outerWidth();
            resizeTableWidth(tblWidth);
        }
        if (!fscreen) {
            parent.callSideMenu = false;
        }

    } catch (e) {
        alert(e);
    }
}

function resizeTableWidth(width) {
    // 一時的に#wrapHead&#wrapContに幅を設定
    $('#shift').width('2000px');
    $('#wrapHead').width('2000px');
    $('#wrapCont').width('2000px');

    // ヘッダー・コンテンツの各テーブルに幅を設定
    $('#wrapHead table').width(width + 'px');
    $('#wrapCont table').width((width - 17) + 'px');

    // ヘッダー・コンテンツの各テーブルを囲っているBoxにテーブル幅+スクロールバー幅を設定
    var shiftWidth = (width > 1000) ? width : 1000;
    $('#shift').width(shiftWidth + 'px');
    $('#wrapHead').width(shiftWidth + 'px');
    if (_ua.ltIE8) {
        $('#wrapCont').width((width + 2) + 'px');
    }
    else if (_ua.IE) {
        $('#wrapCont').width((width + 1) + 'px');
    }
    else {
        $('#wrapCont').width(width + 'px');
    }
}


///-----------------------------------------------------------------------------
// copy
//------------------------------------------------------------------------------
function show_copy_form() {
    $('#copy div.contents').css({
        top: ($(window).height() - $('#copy div.contents').height()) / 2 + 'px',
        left: ($(window).width() - $('#copy div.contents').width()) / 2 + 'px'
    });
    $('#copy').show();
    $('#copy div.background').click(copy_close);
}

function copy_close() {
    $('#copy').hide();
}

function copy_submit() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: 'shift/plan_ajax_copy.php',
        data: {
            group_id: this_group_id,
            year: this_y,
            month: this_m,
            copy_mode: $('input[name="copy_mode"]:checked').val(),
            copy_from: $('#copy_from').val(),
            copy_to: $('#copy_to').val(),
            copy_days: $('#copy_days').val()
        },
        success: function(json) {
            copy_cells(json);
            loading.hide();
            alert('コピーが完了しました。');
            is_change = true;
        },
        beforeSend: function() {
            copy_close();
            loading.show();
        }
    });
}

function copy_cells(data) {
    if (!data.emp) {
        return;
    }
    $.each(data.emp, function(emp_id, emp) {
        var eid = '#' + emp_id;
        if (emp.dates) {
            $.each(emp.dates, function(date, ptn) {
                var now = getSignData(emp_id, date, 'plan');
                if (!now || now === '0') {
                    var id = eid + '_' + date;
                    if ($(id + '_plan') && !$(id + '_plan').hasClass('retire')) {
                        setSignData(emp_id, date, 'plan', ptn.plan);
                        p = pattern['p' + ptn.plan];
                        $(id + '_plan').html(p.cell).css(p.style);
                        is_change = true;
                    }
                }
            });
        }
    });
}
