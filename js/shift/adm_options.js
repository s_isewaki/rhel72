/**
 * CoMedix Shift JavaScript
 */

// DOM ready
$(function () {
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        buttonImage: 'img/calendar_link.gif',
        buttonImageOnly: true,
        showOn: 'button'
    });

    $("#cours").submit(function () {
        if (!confirm('登録してよろしいでしょうか?')) {
            return false;
        }
    });
});

