/**
 * CoMedix Shift JavaScript
 */

var dp = {
    dateFormat: 'yy-mm-dd',
    buttonImage: 'img/calendar_link.gif',
    buttonImageOnly: true,
    showOn: 'button',
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: 1
};

/**
 * DOM ready
 */
$(function () {
    /**
     * DatePicker
     */
    $("input.datepicker").datepicker(dp);
});

/**
 * 終了時処理
 */
var unloader = function () {
    $('div').unbind();
    $('table').unbind();
    $('td').unbind();
    $("input.datepicker").datepicker('destroy');
};
$(window).unload(unloader);

/**
 * 行の削除
 */
function remove_row(obj) {
    $(obj).parent().remove();
}

/**
 * 職員選択子画面
 */
function openEmployeeList(session, base_href, item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = 'emplist_popup.php?session=' + session;
    if (base_href != '') {
        url = base_href + '/' + url;
    }
    if (item_id) {
        url = url + '&item_id=' + item_id;
    }
    childwin = window.open(url, 'emplistpopup', 'left=' + (dx - wx) + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
    childwin.focus();
}

/**
 * 職員選択子画面クローズ
 */
function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

/**
 * 部署選択
 */
function change_emp_class(obj) {
    $(obj).siblings('select.atrb').children('option[class]').hide();
    $(obj).siblings('select.atrb').children('option.parent' + $(obj).val()).show();
    $(obj).siblings('select.atrb').children('option:first-child').attr('selected', true);

    $(obj).siblings('select.dept').children('option[class]').hide();
    $(obj).siblings('select.dept').children('option:first-child').attr('selected', true);

    if ($(obj).siblings('select.room')) {
        $(obj).siblings('select.room').children('option[class]').hide();
        $(obj).siblings('select.room').children('option:first-child').attr('selected', true);
    }
}

function change_emp_atrb(obj) {
    $(obj).siblings('select.dept').children('option[class]').hide();
    $(obj).siblings('select.dept').children('option.parent' + $(obj).val()).show();
    $(obj).siblings('select.dept').children('option:first-child').attr('selected', true);

    if ($(obj).siblings('select.room')) {
        $(obj).siblings('select.room').children('option[class]').hide();
        $(obj).siblings('select.room').children('option:first-child').attr('selected', true);
    }
}

function change_emp_dept(obj) {
    if ($(obj).siblings('select.room')) {
        $(obj).siblings('select.room').children('option[class]').hide();
        $(obj).siblings('select.room').children('option.parent' + $(obj).val()).show();
        $(obj).siblings('select.room').children('option:first-child').attr('selected', true);
    }
}

/**
 * fullscreen
 */
var fullscreen = function () {
    highlight_out();
    var wx = window.screen.availWidth - 10;
    var wy = window.screen.availHeight - 30;
    var url;
    if (location.href.indexOf('?') === -1) {
        url = location.href + '?fullscreen=1';
    }
    else {
        url = location.href + '&fullscreen=1';
    }
    window.open(url, 'fullscreen', 'left=0,top=0,width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
};

/**
 * IEでconsole.logのエラーを出なくする
 **/
if (!('console' in window)) {
    window.console = {};
    window.console.log = function (str) {
        return str;
    };
}

/**
 * ブラウザ判定
 * http://w3g.jp/blog/tools/js_browser_sniffing
 */
var _ua = (function () {
    return {
        ltIE6: typeof window.addEventListener == "undefined" && typeof document.documentElement.style.maxHeight == "undefined",
        ltIE7: typeof window.addEventListener == "undefined" && typeof document.querySelectorAll == "undefined",
        ltIE8: typeof window.addEventListener == "undefined" && typeof document.getElementsByClassName == "undefined",
        IE: document.uniqueID,
        Firefox: window.sidebar,
        Opera: window.opera,
        Webkit: !document.uniqueID && !window.opera && !window.sidebar && window.localStorage && typeof window.orientation == "undefined",
        Mobile: typeof window.orientation != "undefined"
    }
})();
