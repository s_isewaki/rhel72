$(function(){
  /*--------------------------------------------------------------
	table.wrap_heightがあるテーブルセル高さ揃え
  -------------------------------------------------------------- */	
	var $tableTrInner = $('table.wrap_height tr');
	var tableSpanLen = $tableTrInner.length;
	var userAgent = window.navigator.userAgent.toLowerCase();
	var appVersion = window.navigator.appVersion.toLowerCase();

	for ( var i = 0; i<tableSpanLen; i++ ) {
		var $thisSpan = $tableTrInner.eq(i).find('span');
		var spanLen = $thisSpan.length;
		var spanArray = new Array;
		for( var inner = 0; inner<spanLen; inner++ ){
			spanArray[inner] = $thisSpan.eq(inner).height();
			var thisWidth = $thisSpan.eq(inner).parent('td').width();
			
			if (userAgent.indexOf("msie") > -1) {
				if (appVersion.indexOf("msie 6.0") > -1) {
					//ie6
				} else if (appVersion.indexOf("msie 7.0") > -1) {
					//ie7
					$thisSpan.eq(inner).parent('td').width(thisWidth);
				} else {
					//ie9 ie8
					$thisSpan.eq(inner).width(thisWidth);
				}
			} else {
				//ie以外
				$thisSpan.eq(inner).width(thisWidth);
			}
			
		}
		var thisMaxHeight = Math.max.apply(null, spanArray);
		$thisSpan.height(thisMaxHeight);
	}

  /*--------------------------------------------------------------
	チェックボックスのチェックがある時の背景
  -------------------------------------------------------------- */
  $('.checkbox').live('click', function(){
    $(this).closest('.checkbox_wrap').toggleClass('checked');
  
    var dl = $(this).closest('dl');
    if (dl.length > 0){
      dl.toggleClass('checked');
      return;
    }
    
    var li = $(this).closest('li');
    if (li.length > 0){
      li.toggleClass('checked');
    }

    var span = $(this).closest('label').closest('span');
    if (span.length > 0){
      span.toggleClass('checked')
    }
	});	
  
  /*--------------------------------------------------------------
	ラジオボタンのチェックがある時の背景
  -------------------------------------------------------------- */
  $('.radio').live('click', function(){
    check_radio($(this), true);
	});	
});

function check_radio(obj, addChecked){
  //span(選択肢に枠がある場合)
  var span = obj.closest('.box').find('span');
  if (span.length > 0){
    span.removeClass('checked');
    if (addChecked){
      obj.closest('span').addClass('checked')
    }
    return;
  }

  //li
  var li = obj.closest('tr').find('li');
  if (li.length > 0){
    li.removeClass('checked');
    if (addChecked){
      obj.closest('li').addClass('checked')
    }
    return;
  }
  
  //span(選択肢に枠がない場合)
  var span = obj.closest('td').find('span');
  if (span.length > 0){
    span.removeClass('checked');
    if (addChecked){
      obj.closest('span').addClass('checked')
    }
    return;
  }
}

function remove_checked_from_checkbox(obj)
{
  obj.closest('.checkbox_wrap').removeClass('checked');

  var dl = obj.closest('dl');
  if (dl.length > 0){
    dl.removeClass('checked');
    return;
  }
  
  var li = obj.closest('li');
  if (li.length > 0){
    li.removeClass('checked');
  }

  var span = obj.closest('label').closest('span');
  if (span.length > 0){
    span.removeClass('checked')
  }
}

/*=============================================================================
ラベル登録
=============================================================================*/
function rp_action_label(is_header)
{
    if (is_header){
      var pos = $('#label_header').offset();
      var x = pos.left - 35;
      var y = pos.top + 50;
      $('#label_form').css({'left': x + 'px', 'top': y + 'px'});
    }
    else{
      var pos = $('#label_footer').offset();
      var x = pos.left - 35;
      var y = pos.top - $('#label_form').height() - 5;
      $('#label_form').css({'left': x + 'px', 'top': y + 'px'});              
    }
    $('#label_form').toggle('fast');
    $('#b_scrollbar').tinyscrollbar();
}
function regist_label()
{
    $('#label_form').css('display', 'none');
    
    $.post(
        "hiyari_report_classification.php",
        $("#label_form").serialize(),
        function(data) {
            alert(data);
        }
    );
}

/*=============================================================================
報告部署
=============================================================================*/
//部署変更画面の呼び出し
function show_registrant_post_edit(session)
{
  var class_id_hidden = document.getElementById("registrant_class");
  var attribute_id_hidden = document.getElementById("registrant_attribute");
  var dept_id_hidden = document.getElementById("registrant_dept");
  var room_id_hidden = document.getElementById("registrant_room");

  var class_id = class_id_hidden.value;
  var attribute_id = attribute_id_hidden.value;
  var dept_id = dept_id_hidden.value;
  var room_id = room_id_hidden.value;

  var url = "hiyari_registrant_post_edit.php?session=" + session + "&class_id="+class_id+"&attribute_id="+attribute_id+"&dept_id="+dept_id+"&room_id="+room_id;
  window.open(url, "newwin", "width=640,height=480,scrollbars=yes");
}

//部署変更画面からの戻りを反映
function callback_registrant_post_edit(class_id,attribute_id,dept_id,room_id,class_name,attribute_name,dept_name,room_name)
{
  var class_name_span = document.getElementById("registrant_class_name");
  var attribute_name_span = document.getElementById("registrant_attribute_name");
  var dept_name_span = document.getElementById("registrant_dept_name");
  var room_name_span = document.getElementById("registrant_room_name");
  var class_id_hidden = document.getElementById("registrant_class");
  var attribute_id_hidden = document.getElementById("registrant_attribute");
  var dept_id_hidden = document.getElementById("registrant_dept");
  var room_id_hidden = document.getElementById("registrant_room");

  class_name_span.innerHTML = class_name;
  attribute_name_span.innerHTML = attribute_name;
  dept_name_span.innerHTML = dept_name;
  room_name_span.innerHTML = room_name;
  class_id_hidden.value = class_id;
  attribute_id_hidden.value = attribute_id;
  dept_id_hidden.value = dept_id;
  room_id_hidden.value = room_id;

  //報告部署による項目非表示設定を有効にするためポストバックする。
  document.FRM_MAIN.registrant_post_change.value='changed';
  document.FRM_MAIN.submit();
}
