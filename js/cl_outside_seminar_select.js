//院外研修一覧から研修を選択した時の処理
function return_selected(result)
{
	//呼び出し元画面にインターフェース関数が定義されている場合
	if(window.opener && !window.opener.closed && window.opener.cl_training_select_return)
	{
		var outside_seminer_id = opener.document.getElementById('clp_outside_seminar_id').value

		if(result.report_date == ""){
			alert("未報告の為、選択できません。");
			return false;
		}

		if(outside_seminer_id.indexOf(result.seminar_apply_id) != -1){
			alert("すでに選択されています。");
			return false;
		}
		//親画面へ通知
		window.opener.cl_training_select_return(result);
	}
	//自画面を終了します。
	window.close();
}


//行のハイライト制御
//class属性が指定されたclass_nameのTDタグをハイライトします。
function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

//class属性が指定されたclass_nameのTDタグのハイライトを解除します。
function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

//highlightCells()、dehighlightCells()専用関数
function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++)
	{
		if (cells[i].className != class_name)
		{
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}
