function popupDetailBlue(values, box_width, label_width, e) {
	popupDetail(values, box_width, label_width, '#5279a5', '#f6f9ff', e);
}

function popupDetailPink(values, box_width, label_width, e) {
	popupDetail(values, box_width, label_width, '#c71585', '#ffe4e1', e);
}

function popupDetailYellow(values, box_width, label_width, e) {
	popupDetail(values, box_width, label_width, '#b8860b', '#fffacd', e);
}

function popupDetailNaiyo(top_message, values, box_width, label_width, e) {
	popupDetail2(top_message, values, box_width, label_width, '#5279a5', '#f6f9ff', e);
}

function popupDetail(top_message, values, box_width, label_width, strong_color, pale_color, e) {
	var top_message = "";
	popupDetail2(top_message, values, box_width, label_width, strong_color, pale_color, e)
}

function popupDetail2(top_message, values, box_width, label_width, strong_color, pale_color, e) {
	var div = document.getElementById('popup');
	if (!div) {
		var selects = document.getElementsByTagName('select');
		for (var i = 0, j = selects.length; i < j; i++) {
			selects[i].style.visibility = 'hidden';
		}

		var table = document.createElement('table');
		table.style.borderCollapse = 'collapse';
		table.style.width = '100%';
		for (var i = 0, j = values.length; i < j; i += 2) {
			addDetailRow(table, values[i], values[i + 1], strong_color, pale_color);
		}
		table.rows[0].cells[0].style.width = String(label_width).concat('px');

		div = document.createElement('div');
		div.id = 'popup';
		div.style.position = 'absolute';
		div.style.width = String(box_width).concat('px');
		div.style.borderColor = '#5279a5';
		div.style.borderStyle = 'solid';
		div.style.borderWidth = '3px';
		div.style.backgroundColor = '#ffffff';
		div.style.padding = '3px';

		if(top_message != "")
		{
			var top_message_obj = document.createElement('span');
			top_message_obj.innerHTML = top_message;
			top_message_obj.style.width = String((box_width-6)).concat('px');
			div.appendChild(top_message_obj);
		}

		div.appendChild(table);
		document.body.appendChild(div);
	}

	if (e == undefined) {
		e = window.event;
	}

//	var iframe = top.document.getElementById('floatingpage');
//	var iframeOffsetTop = getOffsetTopFromBody(iframe);
	var iframeWidth = document.body.clientWidth;
//	var iframeHeight = top.document.body.clientHeight - iframeOffsetTop;

	var x = ((e.x) ? e.x : e.clientX) + 8;
	var y = ((e.y) ? e.y : e.clientY) + 8;
	if (x + div.clientWidth + 10 > iframeWidth) {
		x = iframeWidth - div.clientWidth - 10;
	}

	div.style.left = x+"px";
	div.style.top = (y + document.body.scrollTop) + "px";
}

function addDetailRow(table, header, value, border_color, header_color) {
	var row = table.insertRow(table.rows.length);
	row.style.height = '22px';
	addDetailCell(row, header, border_color, header_color);
	addDetailCell(row, value, border_color, '#ffffff');
}

function addDetailCell(row, str, border_color, bg_color) {
	var cell = row.insertCell(row.cells.length);
	cell.style.borderColor = border_color;
	cell.style.borderStyle = 'solid';
	cell.style.borderWidth = '1px';
	cell.style.padding = '2px';
	cell.style.backgroundColor = bg_color;
	cell.style.color = '#000000';
	cell.style.fontFamily = '�l�r �o�S�V�b�N, Osaka';
	cell.className = 'j12';
	cell.innerHTML = str;
}

function closeDetail() {
	var div = document.getElementById('popup');
	if (div) {
		document.body.removeChild(div);
	}

	var selects = document.getElementsByTagName('select');
	for (var i = 0, j = selects.length; i < j; i++) {
		selects[i].style.visibility = '';
	}
}

function getOffsetTopFromBody(element) {
	var offset = 0;
	while (element.offsetParent) {
		offset += element.offsetTop;
		element = element.offsetParent;
	}
	return offset;
}

