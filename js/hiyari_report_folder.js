/**
 * ファントルくん「報告」タブの「報告ファイル」用JSファイル
 */

/**
 * フォルダ追加
 */
function rp_action_new_classification_folder()
{
    var url = "hiyari_rp_classification_folder_input.php?session="+session+"&mode=new";
    show_rp_sub_window(url);
}

/**
 * フォルダ更新
 */
function rp_action_update_classification_folder()
{
    //単一選択
    var ids = get_selected_classification_folder_ids();
    if(! check_single_select(ids)){
        return;
    }

    var folder_id = ids[0];
    rp_action_update_classification_folder1(folder_id);
}

/**
 * フォルダ更新(フォルダ指定)
 */
function rp_action_update_classification_folder1(folder_id)
{
    var url = "hiyari_rp_classification_folder_input.php?session="+session+"&mode=update&folder_id="+folder_id;
    show_rp_sub_window(url);
}

/**
 * フォルダ削除
 */
function rp_action_delete_classification_folder()
{
    //複数選択
    var ids = get_selected_classification_folder_ids();
    if(! check_multi_select(ids)){
        return;
    }

    if(! confirm("削除してもよろしいですか？")){
        return;
    }

    document.list_form.mode.value="folder_delete";
    document.list_form.submit();
}

/**
 * フォルダ選択変更
 */
function rp_action_change_classification_folder(folder_id)
{
    document.list_form.page.value=1;
    document.list_form.folder_id.value=folder_id;
    document.list_form.submit();
}

/**
 * レポート分類変更
 */
function rp_action_change_report_classification(to_folder_id,report_id)
{
    document.list_form.mode.value="change_report_classification";
    //document.list_form.page.value=1;//ページ数を変更しない。(分類解除時にページオーバーするが、0件表示となるためOK)
    document.list_form.change_report_classification_report_id.value=report_id;
    document.list_form.change_report_classification_folder_id.value=to_folder_id;
    document.list_form.submit();
}

/**
 * 報告書リンク変更
 */
function rp_action_update_same_report_link(report_id)
{
    var url = "hiyari_rp_same_report_link.php?session="+session+"&target_report_id="+report_id;
    show_rp_sub_window(url);
}

/**
 * 報告書リンク追加
 */
function rp_action_add_update_same_report_link(report_id,add_report_id)
{
    var url = "hiyari_rp_same_report_link.php?session="+session+"&target_report_id="+report_id+"&add_report_id="+add_report_id;
    show_rp_sub_window(url);
}

/**
 * 選択されているリストID配列を取得
 */
function get_selected_classification_folder_ids()
{
    var result = new Array();
    var r_i = 0;

    var list_select_objs = document.getElementsByName("folder_select[]");
    for(var i = 0; i < list_select_objs.length ; i++){
        list_select_obj = list_select_objs.item(i);
        if(list_select_obj.checked){
            result[r_i] = list_select_obj.value;
            r_i++;
        }
    }

    return result;
}