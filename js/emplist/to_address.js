var maxCount = 100;

function to_address(name, addr, emp_id) {
    push_address(name, addr, emp_id, 'to', true);
}

function cc_address(name, addr, emp_id) {
    push_address(name, addr, emp_id, 'cc', true);
}

function bcc_address(name, addr, emp_id) {
    push_address(name, addr, emp_id, 'bcc', true);
}

function to_address_list(type_id) {
    push_address_list(type_id, 'to');
}

function cc_address_list(type_id) {
    push_address_list(type_id, 'cc');
}

function bcc_address_list(type_id) {
    push_address_list(type_id, 'bcc');
}

function push_address(name, addr, emp_id, type, counter) {
    try {
        if (addr === '') {
            alert('職員が存在しません');
            return;
        }

        var send;
        switch (type) {
            case 'to':
                send = parent.opener.document.compose.send_to;
                break;
            case 'cc':
                send = parent.opener.document.compose.send_to_cc;
                break;
            case 'bcc':
                send = parent.opener.document.compose.send_to_bcc;
                break;
        }

        var pwintype = typeof parent.opener.document.compose;
        var i;
        var count;
        if (send.value.length === 0) {
            count = 0;
        }
        else if (send.value.split(", ").length > 0) {
            count = send.value.split(", ").length;
        }

        var addrs = addr.split(", ");
        var names = name.split(", ");
        var emp_ids = emp_id.split(", ");
        if (count + addrs.length > maxCount) {
            alert(maxCount + "より多いアドレスは登録できません");
            return;
        }

        for (i = 0; i < addrs.length; i++) {

            if (addrs[i] === "") {
                continue;
            }

            var addr_str = names[i] + '<' + addrs[i] + '>';

            if (counter && emp_id) {
                set_wm_counter(emp_ids[i]);
            }

            if (pwintype !== "undefined") {
                if (send.value) {
                    var check_addr = '<' + addrs[i] + '>';
                    if (send.value.indexOf(check_addr) >= 0) {
                        continue;
                    }
                    send.value = send.value + ", " + addr_str;
                }
                else {
                    send.value = addr_str;
                }
            }
        }
        // address link 20150508
        if (parent.opener.setDispArea) {
        	var target_id;
	        switch (type) {
	            case 'to':
	                target_id = 1;
	                break;
	            case 'cc':
	                target_id = 2;
	                break;
	            case 'bcc':
	                target_id = 3;
	                break;
	        }
        	parent.opener.setDispArea(target_id, 2);
        }
    }
    catch (e) {
        alert("呼び出し元の画面が認識できませんでした。\n職員名簿画面を開き直してください。");
    }
}

function push_address_list(type_id, type) {
    var id = type_id.split('-');
    var myself = $('input[name="myself"]:checked').val();

    $.ajax({
        url: 'emplist/json.php',
        type: 'post',
        data: {
            type: id[0],
            id: id[1],
            myself: myself,
            mode: 'webmail',
            webmail: $('#webmail').val()
        },
        dataType: 'json',
        success: function(obj) {
            push_address(obj.name, obj.mail, obj.emp_id, type, false);
        }
    });
}