/**
 * Emplist
 * 職員名簿クラス
 */
var Emplist = new function() {
    /**
     * コンストラクタ
     */
    var self = function Emplist(name) {
        this.cache = {};
        this.active = null;
        this.tr = null;
        this.tab = $('#tab');
        this.lists = $('#lists');
        this.keyword = $('#keyword');
        this.search = $('#name_search');
        this.mail = $('#mail');
        this.webmail = parseInt($('#webmail').val());
        this.close = $('#close');
        this.button = this.tab.find('li');

        // デフォルトタブをアクティブ
        this.activate(name);

        // タブ切替
        this.tab.on('click', 'li', {context: this}, this.callbackClickTab);

        // 検索フォーム
        this.keyword
                .on('keypress', {context: this}, this.callbackPressForm)
                .focus();

        // 検索ボタン
        this.search.on('click', {context: this}, this.searchName);

        // グループ展開
        $(document)
                .on('click', '#emplist td.empgroup', {context: this}, this.callbackOpenGroup)
                .on('mouseenter mouseleave', '#emplist tbody tr', this.callbackHover);

        // 閉じるボタン
        this.close.on('click', this.callbackClose);

        // メール切替
        if (this.mail) {
            this.mail.on('click', {context: this}, this.toggleMail);
        }
    };

    self.prototype = {
        constructor: self,
        /**
         * 名前検索
         * @param {type} e イベントオブジェクト
         */
        searchName: function(e) {
            var self = e.data.context;
            if (self.keyword.val() === '') {
                return false;
            }
            $.ajax({
                context: self,
                url: 'emplist/list.php',
                type: 'post',
                data: {
                    type: 'search',
                    keyword: self.keyword.val(),
                    mode: $('#mode').val(),
                    item_id: $('#item_id').val(),
                    webmail: self.webmail
                },
                dataType: 'html',
                success: self.resultSearch
            });
        },
        /**
         * タブのアクティブ化
         * @param {type} name タブ名
         */
        activate: function(name) {
            $('#' + this.active).removeClass('active');
            $('#' + name).addClass('active');

            this.active = name;
            this.tabLoad();
        },
        /**
         * タブクリックのCallback
         * @param {type} e イベントオブジェクト
         */
        callbackClickTab: function(e) {
            var self = e.data.context;
            self.activate(e.target.id);
        },
        /**
         * タブに対応した職員名簿のロード
         */
        tabLoad: function() {
            // キャッシュあり and よく使う人以外 の場合
            // キャッシュを展開
            if (this.cache[this.active] && this.active !== 'used') {
                this.buildEmplist(this.cache[this.active]);
                return true;
            }

            $.ajax({
                context: this,
                url: 'emplist/list.php',
                type: 'post',
                data: {
                    type: this.active,
                    mode: $('#mode').val(),
                    item_id: $('#item_id').val(),
                    webmail: this.webmail
                },
                dataType: 'html',
                success: this.buildEmplist
            });
        },
        /**
         * 職員名簿の下階層のロード
         * @param {type} tr
         * @param {type} type
         * @param {type} id
         * @returns {undefined}
         */
        tabSubLoad: function(tr, type, id) {
            this.tr = tr;
            $.ajax({
                context: this,
                url: 'emplist/list.php',
                type: 'post',
                data: {
                    type: type,
                    id: id,
                    mode: $('#mode').val(),
                    item_id: $('#item_id').val(),
                    tr_class: tr.parent().attr('class'),
                    webmail: this.webmail
                },
                dataType: 'html',
                success: this.appendEmplist
            });
        },
        /**
         * 職員名簿の表示
         * @param {type} data
         */
        buildEmplist: function(data) {
            this.lists.html(data);
            this.stripeEmplist();

            // キャッシュが無い or 検索結果 の場合
            // 職員名簿をキャッシュに保存
            if (!this.cache[this.active] || this.active === 'query') {
                this.cache[this.active] = data;
            }
        },
        /**
         * 職員名簿の下階層の表示
         * @param {type} data
         */
        appendEmplist: function(data) {
            this.tr.parent().after(data);
            this.stripeEmplist();
        },
        /**
         * 検索結果の表示
         * @param {type} data
         */
        resultSearch: function(data) {
            if (this.tab.find('li').length === 6) {
                this.tab.find('ul').append('<li class="tab-nav" id="query">検索結果</li>');
            }

            $('#' + this.active).removeClass('active');
            $('#query').addClass('active');
            this.active = 'query';
            this.buildEmplist(data);
        },
        /**
         * 閉じるボタンクリックのCallback
         */
        callbackClose: function() {
            window.close();
        },
        /**
         * 行ハイライトのCallback
         */
        callbackHover: function() {
            $(this).toggleClass('hover');
        },
        /**
         * 検索フォームEnter入力のCallback
         * @param {type} e イベントオブジェクト
         */
        callbackPressForm: function(e) {
            if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
                e.data.context.searchName(e);
            }
        },
        /**
         * 職員名簿ストライプ表示
         */
        stripeEmplist: function() {
            var emplist = $('#emplist');
            emplist.find('tr.stripe').removeClass('stripe');
            emplist.find('tr:nth-child(2n)').addClass('stripe');
        },
        /**
         * 職員名簿の下階層の展開
         * @param {type} e イベントオブジェクト
         */
        callbackOpenGroup: function(e) {
            var self = e.data.context;
            var id = $(this).attr('id');

            // 閉じる
            if ($(this).hasClass('open')) {
                $('#emplist tr.' + id).remove();
                $(this)
                        .toggleClass('open')
                        .find('img').toggleClass('open');
            }
            // 開く
            else {
                $(this)
                        .toggleClass('open')
                        .find('img').toggleClass('open');

                var ids = id.split('-');
                switch (ids[0]) {
                    case 'class':
                        self.tabSubLoad($(this), 'atrb', ids[1]);
                        break;

                    case 'atrb':
                        self.tabSubLoad($(this), 'dept', ids[1]);
                        break;

                    case 'dept':
                        self.tabSubLoad($(this), 'empclass', ids[1]);
                        break;

                    default:
                        self.tabSubLoad($(this), 'emp' + ids[0], ids[1]);
                        break;
                }
            }
            self.stripeEmplist();
        },

        toggleMail: function (e) {
            var self = e.data.context;
            if (self.webmail) {
                self.mail.html('ウェブメールに切替');
                self.webmail = 0;
            }
            else {
                self.mail.html('E-Mailに切替');
                self.webmail = 1;
            }
            $('#webmail').val(self.webmail);
            self.cache = {};
            self.tabLoad();
        }
    };

    return self;
};

// global values
var mode = $('#mode').val();
var callback = $('#callback').val();

// DOM ready
$(function() {
    var el = new Emplist('used');

    // ファントルくんなら色替える
    if (mode === 'hiyari') {
        $('head').append('<style type="text/css">#emplist thead td {background-color: #35B341;} #emplist tbody tr.stripe {background-color: #DFFFDC;}</style>');
    }

    // ウェブメールなら幅替える
    if (mode === 'webmail') {
        $('head').append('<style type="text/css">#emplist {width: 775px;}</style>');
    }
});

/**
 * よく使う人カウンタ
 * @param {type} id 職員ID
 */
function set_wm_counter(id) {
    $.ajax({
        url: 'emplist/counter.php',
        type: 'post',
        data: {
            emp_id: id,
            session: $('#session').val()
        }
    });
}

/**
 * 登録対象者を追加する
 * @param {type} item_id
 * @param {type} name
 * @param {type} emp_id
 * @param {type} mail_address
 * @param {type} webmail (0:email, 1:webmail)
 */
function add_target(item_id, name, emp_id, mail_address, webmail) {
    if (emp_id === "") {
        alert('職員が存在しません');
        return;
    }
    set_wm_counter(emp_id);

    var callback = $('#callback').val();

    // スケジュール・設備予約の場合
    if (mode === 'schedule') {
        if (!parent.opener || parent.opener.closed || !parent.opener.add_target_list) {
            return;
        }
        parent.opener.add_target_list(emp_id, name);
    }
    // 委員会・責任者の場合
    else if (mode === 'project' && item_id === '0') {
        if (!parent.opener || parent.opener.closed || !parent.opener.update_target_html) {
            return;
        }
        parent.opener.document.pjt.incharge.value = name;
        parent.opener.document.pjt.incharge_name.value = name;
        parent.opener.document.pjt.incharge_id.value = emp_id;
        parent.opener.delete_target_list('1', emp_id);
        parent.opener.delete_target_list('2', emp_id);
        parent.opener.update_target_html('1');
        parent.opener.update_target_html('2');
    }
    // ファントルくん
    else if (mode === 'hiyari' && callback === '0') {
        if (!parent.opener || parent.opener.closed || !parent.opener.add_emp_list) {
            return;
        }
        parent.opener.add_emp_list(emp_id, name, item_id);
    }
    else if (mode === 'hiyari' && callback === '1') {
        if (!window.opener || window.opener.closed || !window.opener.add_emp_list) {
            return;
        }
        $.ajax({
            url: 'emplist/hiyari_profile.php',
            type: 'post',
            data: {
                emp_id: emp_id,
                name: name,
                item_id: item_id
            },
            dataType: 'json',
            success: function(obj) {
                window.opener.add_emp_list(obj.emp_id, obj.name, obj.item_id, obj.other_info);
                window.close();
            }
        });
    }
    else if (mode === 'hiyari' && callback === '2') {
        if (!window.opener || window.opener.closed || !window.opener.add_emp_list2) {
            return;
        }
        $.ajax({
            url: 'emplist/hiyari_profile.php',
            type: 'post',
            data: {
                emp_id: emp_id,
                name: name
            },
            dataType: 'json',
            success: function(obj) {
                window.opener.add_emp_list2(obj.emp_id, obj.name, obj.other_info);
                window.close();
            }
        });
    }
    // アドレス帳＞グループ作成
    else if (mode === 'address_group') {
        if (!parent.opener || parent.opener.closed || !parent.opener.addEmployees) {
            return;
        }
        parent.opener.addEmployees(emp_id, name, mail_address, webmail);
    }
    // ウェブメール＞トラブルシュート
    else if (mode === 'webmail_shoot') {
        if (!parent.opener || parent.opener.closed || !parent.opener.setMailId) {
            return;
        }
        parent.opener.setMailId(mail_address);
    }
    else if (callback) {
        if (!parent.opener || parent.opener.closed || !parent.opener[callback]) {
            return;
        }
        parent.opener[callback](item_id, emp_id, name);
    }
    else {
        if (!parent.opener || parent.opener.closed || !parent.opener.add_target_list) {
            return;
        }
        parent.opener.add_target_list(item_id, emp_id, name);
    }
}

/**
 * 登録対象者を一括追加する
 * @param {type} item_id
 * @param {type} type_id
 */
function add_members(item_id, type_id) {
    if (!parent.opener) {
        return;
    }
    if (parent.opener.closed) {
        return;
    }
    if (!parent.opener.add_target_list && !parent.opener.add_emp_list && !parent.opener.addEmployees) {
        return;
    }

    if (id === "") {
        alert('職員が存在しません');
        return;
    }

    var id = type_id.split('-');
    var webmail = (id[2] || '');

    $.ajax({
        url: 'emplist/json.php',
        type: 'post',
        data: {
            type: id[0],
            id: id[1],
            webmail: webmail,
            mode: mode
        },
        dataType: 'json',
        success: function(obj) {
            if (mode === 'schedule') {
                parent.opener.add_target_list(obj.emp_id, obj.name);
            }
            else if (mode === 'hiyari') {
                parent.opener.add_emp_list(obj.emp_id, obj.name, item_id);
            }
            else if (mode === 'address_group') {
                parent.opener.addEmployees(obj.emp_id, obj.name, obj.mail, webmail);
            }
            else {
                parent.opener.add_target_list(item_id, obj.emp_id, obj.name);
            }
        }
    });
}
