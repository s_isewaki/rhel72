/**
 * Addresslist
 * アドレス帳クラス
 */
var Addresslist = new function() {
    /**
     * コンストラクタ
     */
    var self = function Addresslist(name) {
        this.active = null;
        this.tab = $('#tab');
        this.lists = $('#lists');
        this.keyword = $('#keyword');
        this.search = $('#address_search');
        this.full = $('#full');
        this.close = $('#close');
        this.button = this.tab.find('li');

        // デフォルトタブをアクティブ
        this.activate(name);

        // タブ切替
        this.tab.on('click', 'li', {context: this}, this.callbackClickTab);

        // 検索フォーム
        this.keyword
                .on('keypress', {context: this}, this.callbackPressForm)
                .focus();

        // 検索ボタン
        this.search.on('click', {context: this}, this.searchName);
        this.full.on('click', {context: this}, this.searchFull);

        // グループ展開
        $(document)
                .on('click', '#emplist td.empgroup', {context: this}, this.callbackOpenGroup)
                .on('mouseenter mouseleave', '#emplist tbody tr', this.callbackHover);

        // 閉じるボタン
        this.close.on('click', this.callbackClose);
    };

    self.prototype = {
        constructor: self,
        /**
         * 名前検索
         * @param {type} e イベントオブジェクト
         */
        searchName: function(e) {
            var self = e.data.context;
            if (self.keyword.val() === '') {
                return false;
            }
            $.ajax({
                context: self,
                url: 'emplist/alist.php',
                type: 'post',
                data: {
                    type: self.active,
                    keyword: self.keyword.val(),
                    mode: $('#mode').val()
                },
                dataType: 'html',
                success: self.resultSearch
            });
        },
        /**
         * 全表示
         * @param {type} e イベントオブジェクト
         */
        searchFull: function(e) {
            var self = e.data.context;
            $.ajax({
                context: self,
                url: 'emplist/alist.php',
                type: 'post',
                data: {
                    type: self.active,
                    mode: $('#mode').val()
                },
                dataType: 'html',
                success: self.resultSearch
            });
        },
        /**
         * タブのアクティブ化
         * @param {type} name タブ名
         */
        activate: function(name) {
            $('#' + this.active).removeClass('active');
            $('#' + name).addClass('active');

            this.active = name;
            this.tabLoad();
        },
        /**
         * タブクリックのCallback
         * @param {type} e イベントオブジェクト
         */
        callbackClickTab: function(e) {
            var self = e.data.context;
            self.activate(e.target.id);
        },
        /**
         * タブに対応したアドレス帳のロード
         */
        tabLoad: function() {

            $.ajax({
                context: this,
                url: 'emplist/alist.php',
                type: 'post',
                data: {
                    type: this.active,
                    mode: $('#mode').val()
                },
                dataType: 'html',
                success: this.buildEmplist
            });
        },
        /**
         * アドレス帳の表示
         * @param {type} data
         */
        buildEmplist: function(data) {
            this.lists.html(data);
            this.stripeEmplist();
        },
        /**
         * 検索結果の表示
         * @param {type} data
         */
        resultSearch: function(data) {
            this.buildEmplist(data);
        },
        /**
         * 閉じるボタンクリックのCallback
         */
        callbackClose: function() {
            window.close();
        },
        /**
         * 行ハイライトのCallback
         */
        callbackHover: function() {
            $(this).toggleClass('hover');
        },
        /**
         * 検索フォームEnter入力のCallback
         * @param {type} e イベントオブジェクト
         */
        callbackPressForm: function(e) {
            if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
                e.data.context.searchName(e);
            }
        },
        /**
         * アドレス帳ストライプ表示
         */
        stripeEmplist: function() {
            var emplist = $('#emplist');
            emplist.find('tr.stripe').removeClass('stripe');
            emplist.find('tr:nth-child(2n)').addClass('stripe');
        },
        /**
         * アドレス帳の下階層の展開
         * @param {type} e イベントオブジェクト
         */
        callbackOpenGroup: function(e) {
            var self = e.data.context;
            var id = $(this).attr('id');

            // 閉じる
            if ($(this).hasClass('open')) {
                $('#emplist tr.' + id).remove();
                $(this)
                        .toggleClass('open')
                        .find('img').toggleClass('open');
            }
            // 開く
            else {
                $(this)
                        .toggleClass('open')
                        .find('img').toggleClass('open');
                var ids = id.split('-');
                self.tabSubLoad($(this), ids[1]);
            }
            self.stripeEmplist();
        },
        /**
         * アドレス帳の下階層のロード
         * @param {type} tr
         * @param {type} type
         * @param {type} id
         * @returns {undefined}
         */
        tabSubLoad: function(tr, id) {
            this.tr = tr;
            $.ajax({
                context: this,
                url: 'emplist/alist.php',
                type: 'post',
                data: {
                    type: 'subgroup',
                    id: id,
                    mode: $('#mode').val()
                },
                dataType: 'html',
                success: this.appendEmplist
            });
        },
        /**
         * アドレス帳の下階層の表示
         * @param {type} data
         */
        appendEmplist: function(data) {
            this.tr.parent().after(data);
            this.stripeEmplist();
        }
    };
    return self;
};

// global values
var mode = $('#mode').val();
var callback = $('#callback').val();

// DOM ready
$(function() {
    var el = new Addresslist('ps');
});

/**
 * 登録対象者を追加する
 * @param {type} item_id
 * @param {type} name
 * @param {type} emp_id
 */
function add_target(item_id, name, emp_id) {
    if (!parent.opener) {
        return;
    }
    if (parent.opener.closed) {
        return;
    }
    if (!parent.opener.add_target_list && !parent.opener.add_emp_list) {
        return;
    }

    if (emp_id === "") {
        alert('職員が存在しません');
        return;
    }
    set_wm_counter(emp_id);

    // スケジュール・設備予約の場合
    if (mode === 'schedule') {
        parent.opener.add_target_list(emp_id, name);
    }
    // 委員会・責任者の場合
    else if (mode === 'project' && item_id === '0') {
        parent.opener.document.pjt.incharge.value = name;
        parent.opener.document.pjt.incharge_name.value = name;
        parent.opener.document.pjt.incharge_id.value = emp_id;
        parent.opener.delete_target_list('1', emp_id);
        parent.opener.delete_target_list('2', emp_id);
        parent.opener.update_target_html('1');
        parent.opener.update_target_html('2');
    }
    // ファントルくん
    else if (mode === 'hiyari' && callback === '0') {
        parent.opener.add_emp_list(emp_id, name, item_id);
    }
    else if (mode === 'hiyari' && callback === '1') {
        $.ajax({
            url: 'emplist/hiyari_profile.php',
            type: 'post',
            data: {
                emp_id: emp_id,
                name: name,
                item_id: item_id
            },
            dataType: 'json',
            success: function(obj) {
                window.opener.add_emp_list(obj.emp_id, obj.name, obj.item_id, obj.other_info);
                window.close();
            }
        });
    }
    else if (mode === 'hiyari' && callback === '2') {
        $.ajax({
            url: 'emplist/hiyari_profile.php',
            type: 'post',
            data: {
                emp_id: emp_id,
                name: name
            },
            dataType: 'json',
            success: function(obj) {
                window.opener.add_emp_list2(obj.emp_id, obj.name, obj.other_info);
                window.close();
            }
        });
    }
    else {
        parent.opener.add_target_list(item_id, emp_id, name);
    }
}

/**
 * 登録対象者を追加する
 * @param {type} address_id
 * @param {type} name
 * @param {type} mail_address
 * @param {type} type (1:PC、2：携帯)
 */
function addAddress(address_id, name, mail_address, type) {
    if (!parent.opener || parent.opener.closed || !parent.opener.addAddress) {
        return;
    }

    // アドレス帳＞グループ作成の場合
    if (mode === 'address_group') {
        parent.opener.addAddress(address_id, name, mail_address, type);
    }
}
