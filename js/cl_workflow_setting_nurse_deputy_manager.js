function changeDisplay(class_id, atrb_id, dept_id, room_id) {

	var img_id = 'img';
	var row_id = 'row';
	var id_build = '';
	if (class_id != '') {
		id_build += class_id;
	}
	if (atrb_id != '') {
		id_build += '-';
		id_build += atrb_id;
	}
	if (dept_id != '') {
		id_build += '-';
		id_build += dept_id;
	}
	if (room_id != '') {
		id_build += '-';
		id_build += room_id;
	}
	img_id += id_build;
	row_id += id_build;

	var img = document.getElementById(img_id);
	if (img.className == 'close') {
		img.className = 'open';
	} else {
		img.className = 'close';
	}

	var rows = document.getElementsByTagName('tr');
	var arr_row_id;
	for (i = 0; i < rows.length; i++) {

		// 組織は表示したままにする
		if (rows[i].id.length <= 3) {
			continue;
		}

		// 選択行は表示したままにする
		if (rows[i].id == row_id){
			continue;
		}

		// 各所属ＩＤを取得
		arr_row_id = rows[i].id.substr(3).split('-');

		// 画像ＩＤを取得
		img_id = 'img' + rows[i].id.substr(3);

		if (img.className == 'open') {
			// 直下の所属を表示にする
			if (
				(
					class_id == ''
					&& arr_row_id.length == 1
				) || (
					class_id == arr_row_id[0]
					&& atrb_id == ''
					&& arr_row_id.length == 2
				) || (
					class_id == arr_row_id[0]
					&& atrb_id == arr_row_id[1]
					&& dept_id == ''
					&& arr_row_id.length == 3
				) || (
					class_id == arr_row_id[0]
					&& atrb_id == arr_row_id[1]
					&& dept_id == arr_row_id[2]
					&& room_id == ''
					&& arr_row_id.length == 4
				)
			){
				rows[i].style.display = 'block';
			}
		} else {
			// 配下の所属を全て非表示にする
			if (
				class_id == '' ||
				(
					class_id == arr_row_id[0]
					&& atrb_id == ''
				) || (
					class_id == arr_row_id[0]
					&& atrb_id == arr_row_id[1]
					&& dept_id == ''
				) || (
					class_id == arr_row_id[0]
					&& atrb_id == arr_row_id[1]
					&& dept_id == arr_row_id[2]
					&& room_id == ''
				)
			){
				rows[i].style.display = 'none';
				if (arr_row_id.length < 4) {
					document.getElementById(img_id).className = 'close';
				}
			}
		}
	}
}

function open_emplist(session,item_id) {

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'cl_emp_list.php';
	url += '?session=' + session + '&input_div=' + item_id;
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function add_target_list(item_id, emp_id, emp_nm){

	var arr_add_emp_id = emp_id.split(',');
	var arr_add_emp_nm = emp_nm.split(',');

	var obj_id = document.getElementById('emp_id' + item_id);
	var obj_nm = document.getElementById('emp_nm' + item_id);
	var obj_nm_view = document.getElementById('emp_nm_view' + item_id);

	var arr_id = obj_id.value.split(',');

	var id_build = obj_id.value;
	var nm_build = obj_nm.value;
	var nm_view_build = obj_nm_view.innerHTML;

	var bolalready = false;

	for (i = 0; i < arr_add_emp_id.length; i++){

		bolalready = false;
		for (j = 0; j < arr_id.length; j++){
			if (arr_add_emp_id[i] == arr_id[j]) {
				bolalready = true;
				break;
			}
		}

		if (!bolalready) {

			if (obj_id.value.length > 0) {
				id_build += ',';
				nm_build += ',';
				nm_view_build += ',';
			}

			id_build += arr_add_emp_id[i];
			nm_build += arr_add_emp_nm[i];
			nm_view_build += "<a href=\"javascript:delete_emp('" + item_id + "', '" + arr_add_emp_id[i] + "', '" + arr_add_emp_nm[i] + "');\">";
			nm_view_build += arr_add_emp_nm[i];
			nm_view_build += "</a>";

		}
	}

	obj_id.value = id_build;
	obj_nm.value = nm_build;
	obj_nm_view.innerHTML = nm_view_build;

}

function delete_emp(item_id, emp_id, emp_nm){

	if(!confirm("「" + emp_nm + "」さんを削除します。よろしいですか？")) {
		return;
	}

	var obj_id = document.getElementById('emp_id' + item_id);
	var obj_nm = document.getElementById('emp_nm' + item_id);
	var obj_nm_view = document.getElementById('emp_nm_view' + item_id);

	var arr_id = obj_id.value.split(',');
	var arr_nm = obj_nm.value.split(',');

	var id_build = '';
	var nm_build = '';
	var nm_view_build = '';

	var bolalready = false;

	for (i = 0; i < arr_id.length; i++){
		if (emp_id == arr_id[i]) {
			// 削除対象は再表示しない
		} else {

			if (id_build.length != 0) {
				id_build += ',';
				nm_build += ',';
				nm_view_build += ',';
			}

			id_build += arr_id[i];
			nm_build += arr_nm[i];

			// 画面表示部のみ生成し直す
			nm_view_build += "<a href=\"javascript:delete_emp('" + item_id + "', '" + arr_id[i] + "', '" + arr_nm[i] + "');\">";
			nm_view_build += arr_nm[i];
			nm_view_build += "</a>";
		}
	}

	obj_id.value = id_build;
	obj_nm.value = nm_build;
	obj_nm_view.innerHTML = nm_view_build;

}

function regist(){
	document.wkfw.regist_flg.value = 'true';
	document.wkfw.submit();
}
