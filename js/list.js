jQuery.noConflict();
var j$ = jQuery;

j$(function(){
/*--------------------------------------------------------------
	分類フォルダ　カスタムスクロールバー
  -------------------------------------------------------------- */
  if (j$('#sub_bunrui').size() > 0){
    j$('#b_scrollbar').tinyscrollbar();
    j$('#sub_bunrui input').click(function(e) {
      var thisLi = j$(this).closest('li');
      if(thisLi.hasClass('checked')) {
        thisLi.removeClass('checked')
        return;
      }
      thisLi.addClass('checked');
    });
  }
  
/*--------------------------------------------------------------
	チェックボックスのチェックがある時のtrの背景
  -------------------------------------------------------------- */
	j$('.checkbox').click(function(e) {
		var thisTr = j$(this).closest('tr');
		if(thisTr.hasClass('checked')) {
			thisTr.removeClass('checked')
			return;
		}
		thisTr.addClass('checked');
	});	
});
