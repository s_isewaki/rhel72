YAHOO.namespace("ui.calendar");
YAHOO.util.Event.addListener(window, "load", initcal);
YAHOO.util.Event.addListener(window, "load", click_fourweek);
YAHOO.util.Event.addListener("group_select", "change", group_select);

// カレンダー初期化
function initcal() {
	if (!document.getElementById("calContainer")) return;

	YAHOO.ui.calendar.cal = new YAHOO.widget.Calendar("cal","calContainer", { title:"日付を選択して下さい", close:true } );

	YAHOO.ui.calendar.cal.cfg.setProperty("MDY_YEAR_POSITION", 1);
	YAHOO.ui.calendar.cal.cfg.setProperty("MDY_MONTH_POSITION", 2);
	YAHOO.ui.calendar.cal.cfg.setProperty("MDY_DAY_POSITION", 3);

	YAHOO.ui.calendar.cal.cfg.setProperty("MY_YEAR_POSITION", 1);
	YAHOO.ui.calendar.cal.cfg.setProperty("MY_MONTH_POSITION", 2);

	YAHOO.ui.calendar.cal.cfg.setProperty("MONTHS_SHORT",   ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"]);
	YAHOO.ui.calendar.cal.cfg.setProperty("MONTHS_LONG",    ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"]);
	YAHOO.ui.calendar.cal.cfg.setProperty("WEEKDAYS_1CHAR", ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']);
	YAHOO.ui.calendar.cal.cfg.setProperty("WEEKDAYS_SHORT", ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']);
	YAHOO.ui.calendar.cal.cfg.setProperty("WEEKDAYS_MEDIUM",['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']);
	YAHOO.ui.calendar.cal.cfg.setProperty("WEEKDAYS_LONG",  ['<span style="color:red;">\u65E5</span>', "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", '<span style="color:blue;">\u571F</span>']);

	YAHOO.ui.calendar.cal.render();

	YAHOO.ui.calendar.cal.selectEvent.subscribe(handleSelect, YAHOO.ui.calendar.cal, true);
	YAHOO.util.Event.addListener("imgCalendar", "click", YAHOO.ui.calendar.cal.show, YAHOO.ui.calendar.cal, true);
}

// カレンダー選択
function handleSelect(type,args,obj) {
	var dates = args[0]; 
	var date = dates[0];

	var yy = document.getElementById("chk_start_y");
	var mm = document.getElementById("chk_start_m");
	var dd = document.getElementById("chk_start_d");
	
	for (var i=0;i<yy.options.length;i++) {
		if (yy.options[i].value == date[0]) {
			yy.selectedIndex = i;
			break;
		}
	}
	mm.selectedIndex = date[1];
	dd.selectedIndex = date[2];

	YAHOO.ui.calendar.cal.hide();
}

// 組み合わせ設定フォームの追加
function add_ok_pattern(pattern) {
	var table = document.getElementById("ok_pattern");
	var rows = table.rows.length;

	var row = table.insertRow(rows);

	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);

	cell1.setAttribute("class","listtitle");
	cell1.className = 'listtitle';
	cell4.setAttribute("class","listbutton");
	cell4.className = 'listbutton';

	cell1.innerHTML = "-";
	cell2.innerHTML = '<select name="seq_today[]">' + pattern + '</select>';
	cell3.innerHTML = '<select name="seq_next[]">' + pattern + '</select>';
	cell4.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
}

// 組み合わせ禁止フォームの追加
function add_ng_pattern(pattern) {
	var table = document.getElementById("ng_pattern");
	var rows = table.rows.length;

	var row = table.insertRow(rows);

	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);

	cell1.setAttribute("class","listtitle");
	cell1.className = 'listtitle';
	cell4.setAttribute("class","listbutton");
	cell4.className = 'listbutton';

	cell1.innerHTML = "-";
	cell2.innerHTML = '<select name="ng_today[]">' + pattern + '</select>';
	cell3.innerHTML = '<select name="ng_next[]">' + pattern + '</select>';
	cell4.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
}

// 連続勤務シフトの上限フォームの追加
function add_up_limit(pattern) {
	var table = document.getElementById("up_limit");
	var rows = table.rows.length;

	var row = table.insertRow(rows);

	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);

	cell1.setAttribute("class","listtitle");
	cell1.className = 'listtitle';
	cell4.setAttribute("class","listbutton");
	cell4.className = 'listbutton';

	cell1.innerHTML = "-";
	cell2.innerHTML = '<select name="limit_id[]">' + pattern + '</select>';
	cell3.innerHTML = '<input type="text" name="limit_day[]" size="3">日';
	cell4.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
}

// 勤務シフトの間隔フォームの追加
function add_interval(pattern) {
	var table = document.getElementById("interval");
	var rows = table.rows.length;

	var row = table.insertRow(rows);

	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);

	cell1.setAttribute("class","listtitle");
	cell1.className = 'listtitle';
	cell4.setAttribute("class","listbutton");
	cell4.className = 'listbutton';

	cell1.innerHTML = "-";
	cell2.innerHTML = '<select name="interval_id[]">' + pattern + '</select>';
	cell3.innerHTML = '<input type="text" name="interval_day[]" size="3">日';
	cell4.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
}

// 性別固定シフトフォームの追加
function add_sex_pattern(pattern) {
	var table = document.getElementById("sex_pattern");
	var rows = table.rows.length;

	var row = table.insertRow(rows);

	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);

	cell1.setAttribute("class","listtitle");
	cell1.className = 'listtitle';
	cell4.setAttribute("class","listbutton");
	cell4.className = 'listbutton';

	cell1.innerHTML = "-";
	cell2.innerHTML = '<select name="sex_pattern[]">' + pattern + '</select>';
	cell3.innerHTML = '-';
	cell4.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
}

// 4週8休で休暇とするシフトフォームの追加
function add_hol_pattern(pattern) {
	var parent = document.getElementById("hol_pattern");
	var span = document.createElement('span');
	span.innerHTML = '<select name="hol_pattern[]">' + pattern + '</select><button type="button" onclick="remove_span(this);">削除</button><br>';
	parent.appendChild(span);
}

// 決済欄役職フォームの追加
function add_st_name() {
	var parent = document.getElementById("st_name");
	var span = document.createElement('span');
	span.innerHTML = '<input type="text" name="st_name[]" size="30" maxlength="30" style="ime-mode:active;"><button type="button" onclick="remove_span(this);">削除</button><br>';
	parent.appendChild(span);
}

// テーブル行の削除
function remove_table_row(obj) {
	var tr = obj.parentNode.parentNode;
	tr.parentNode.deleteRow(tr.sectionRowIndex);
}

// span行の削除
function remove_span(obj) {
	var span = obj.parentNode;
	span.parentNode.removeChild(span);
}

// 4週8休のチェック on/off
function click_fourweek() {
	var flg = document.getElementById("four_week_chk_flg");
	if (!flg) return;
	if(flg.checked) {
		document.getElementById("four_week_start_date").style.display = "";
		document.getElementById("four_week_hol_pattern").style.display = "";
	}
	else {
		document.getElementById("four_week_start_date").style.display = "none";
		document.getElementById("four_week_hol_pattern").style.display = "none";
	}
}

// シフトグループ切り替え
function group_select() {
	var session = document.getElementById("session");
	var group_id = document.getElementById("group_select");
	location.href = 'duty_shift_auto.php?group_id=' + group_id.value + '&session=' + session.value;
}

// 入力チェック
function check_form() {

	// 組み合わせ指定チェック
	var seq_today = document.getElementsByName("seq_today[]");
	var seq_next = document.getElementsByName("seq_next[]");
	for(var i=0; i<seq_today.length; i++) {
		if (seq_today[i].value == "" || seq_next[i].value == "") {
			alert("「組み合わせ指定」で未設定の項目が存在します。");
			return false;
		}
		if (seq_today[i].value == seq_next[i].value) {
			alert("「組み合わせ指定」の「当日」「翌日」に同じシフトは指定できません。");
			return false;
		}
	}

	// 組み合わせ禁止チェック
	var ng_today = document.getElementsByName("ng_today[]");
	var ng_next = document.getElementsByName("ng_next[]");
	for(var i=0; i<ng_today.length; i++) {
		if (ng_today[i].value == "" || ng_next[i].value == "") {
			alert("「組み合わせ禁止」で未設定の項目が存在します。");
			return false;
		}

		// 組み合わせ指定と禁止の重複チェック
		for(var j=0; j<seq_today.length; j++) {
			if (seq_today[j].value == ng_today[i].value && seq_next[j].value == ng_next[i].value) {
				alert("「組み合わせ指定」と「組み合わせ禁止」に同じ組み合わせが存在します。");
				return false;
			}
		}
	}

	// 連続勤務シフトの上限チェック
	var limit_id = document.getElementsByName("limit_id[]");
	var limit_day = document.getElementsByName("limit_day[]");
	for(var i=0; i<limit_id.length; i++) {
		if (limit_id[i].value == "" || limit_day[i].value == "") {
			alert("「連続勤務シフトの上限」で未設定の項目が存在します。");
			return false;
		}
		for(var j=0; j<limit_id.length; j++) {
			if (i != j && limit_id[i].value == limit_id[j].value) {
				alert("「連続勤務シフトの上限」が重複設定されています。");
				return false;
			}
		}
	}

	// 勤務シフトの間隔チェック
	var interval_id = document.getElementsByName("interval_id[]");
	var interval_day = document.getElementsByName("interval_day[]");
	for(var i=0; i<interval_id.length; i++) {
		if (interval_id[i].value == "" || interval_day[i].value == "") {
			alert("「勤務シフトの間隔」で未設定の項目が存在します。");
			return false;
		}
		for(var j=0; j<interval_id.length; j++) {
			if (i != j && interval_id[i].value == interval_id[j].value) {
				alert("「勤務シフトの間隔」が重複設定されています。");
				return false;
			}
		}
	}

	// 性別固定シフトの間隔チェック
	var sex_pattern = document.getElementsByName("sex_pattern[]");
	for(var i=0; i<sex_pattern.length; i++) {
		if (sex_pattern[i].value == "") {
			alert("「性別固定シフト」で未設定の項目が存在します。");
			return false;
		}
		for(var j=0; j<sex_pattern.length; j++) {
			if (i != j && sex_pattern[i].value == sex_pattern[j].value) {
				alert("「性別固定シフト」が重複設定されています。");
				return false;
			}
		}
	}

	document.getElementById("form").submit();
}