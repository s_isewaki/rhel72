var childwin_1 = null;
var childwin_2 = null;

///-----------------------------------------------------------------------------
//「看護職員表作成」
///-----------------------------------------------------------------------------
function editNurse() {
    closeChildWin_1();
    closeChildWin_2();
    ///-----------------------------------------------------------------------------
    // 存在チェック（看護師／准看護師）
    ///-----------------------------------------------------------------------------
    if ((document.nurseForm1.nurse_cnt.value + document.nurseForm1.sub_nurse_cnt.value) == 0) {
        alert('看護師、准看護師が存在しません。');
        return;
    }
    ///-----------------------------------------------------------------------------
    //入力値チェック
    ///-----------------------------------------------------------------------------
    if (dataCheck() == false) {
        return;
    }
    ///-----------------------------------------------------------------------------
    //「看護職員表」ＷＩＮＤＯＷをＯＰＥＮ
    ///-----------------------------------------------------------------------------
    wx = 800;
    wy = 300;
    dx = screen.width / 2 - (wx / 2);
    dy = screen.top;
    base = screen.height / 2 - (wy / 2);
    childwin_1 = window.open('', 'nurseFormChild1', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    document.nurseForm1.create_yyyy.value = document.mainForm.inp_create_yyyy.value;                    //作成年
    document.nurseForm1.create_mm.value = document.mainForm.inp_create_mm.value;                        //作成月
    document.nurseForm1.create_dd.value = document.mainForm.inp_create_dd.value;                        //作成費
    document.nurseForm1.hospitalization_cnt.value = document.mainForm.inp_hospitalization_cnt.value;    //平均在院日数
    document.nurseForm1.hosp_patient_cnt.value = document.mainForm.hosp_patient_cnt.value;  //１日平均入院患者数
    //予定実績フラグ
    for (i=0; i<document.mainForm.elements['plan_result_flag'].length; i++) {
        if (document.mainForm.elements['plan_result_flag'][i].checked) {
            document.nurseForm1.elements['plan_result_flag'].value = document.mainForm.elements['plan_result_flag'][i].value;
            break;
        }
    }
    document.nurseForm1.submit();
}

///-----------------------------------------------------------------------------
//「看護補助者表作成」
///-----------------------------------------------------------------------------
function editSubNurse() {
    closeChildWin_1();
    closeChildWin_2();
    ///-----------------------------------------------------------------------------
    // 存在チェック（看護補助者）
    ///-----------------------------------------------------------------------------
    if (document.nurseForm2.assistance_nurse_cnt.value == 0) {
        alert('看護補助者が存在しません。');
        return;
    }
    ///-----------------------------------------------------------------------------
    //入力値チェック
    ///-----------------------------------------------------------------------------
    if (dataCheck() == false) {
        return;
    }
    ///-----------------------------------------------------------------------------
    //「看護補助者表」ＷＩＮＤＯＷをＯＰＥＮ
    ///-----------------------------------------------------------------------------
    wx = 800;
    wy = 300;
    dx = screen.width / 2 - (wx / 2);
    dy = screen.top;
    base = screen.height / 2 - (wy / 2);
    childwin_2 = window.open('', 'nurseFormChild2', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    document.nurseForm2.create_yyyy.value = document.mainForm.inp_create_yyyy.value;                    //作成年
    document.nurseForm2.create_mm.value = document.mainForm.inp_create_mm.value;                        //作成月
    document.nurseForm2.create_dd.value = document.mainForm.inp_create_dd.value;                        //作成費
    document.nurseForm2.hospitalization_cnt.value = document.mainForm.inp_hospitalization_cnt.value;    //平均在院日数
    document.nurseForm2.hosp_patient_cnt.value = document.mainForm.hosp_patient_cnt.value;  //１日平均入院患者数
    //予定実績フラグ
    for (i=0; i<document.mainForm.elements['plan_result_flag'].length; i++) {
        if (document.mainForm.elements['plan_result_flag'][i].checked) {
            document.nurseForm2.elements['plan_result_flag'].value = document.mainForm.elements['plan_result_flag'][i].value;
            break;
        }
    }
    document.nurseForm2.submit();
}

///-----------------------------------------------------------------------------
// 「看護職員表」（ＣＬＯＳＥ）
///-----------------------------------------------------------------------------
function closeChildWin_1() {
    if (childwin_1 != null && !childwin_1.closed) {
        childwin_1.close();
    }
    childwin_1 = null;
}
function closeChildWin_2() {
    if (childwin_2 != null && !childwin_2.closed) {
        childwin_2.close();
    }
    childwin_2 = null;
}

///-----------------------------------------------------------------------------
// 「再計算」
///-----------------------------------------------------------------------------
function dataRecomputation() {
    document.mainForm.recomputation_btn_flg.value = "1";
    document.mainForm.submit();
}

///-----------------------------------------------------------------------------
// 「施設基準再読込」
///-----------------------------------------------------------------------------
function dataReload() {
    document.mainForm.reload_btn_flg.value = "1";
    dataRecomputation();
}

///-----------------------------------------------------------------------------
// 「当月施設基準保存」
///-----------------------------------------------------------------------------
function dataInsert() {
    //入力値チェック
    if (dataCheck() == false) {
        return;
    }
    
    //登録
    if (confirm('「登録」します。よろしいですか？')) {
        document.mainForm.action="duty_shift_report_update_exe.php";
        document.mainForm.submit();
  }
}

///-----------------------------------------------------------------------------
// 入力値チェック
///-----------------------------------------------------------------------------
function dataCheck() {
    //作成年月日
    var wk_yyyy = document.mainForm.inp_create_yyyy.value;
    var wk_mm = document.mainForm.inp_create_mm.value;
    var wk_dd = document.mainForm.inp_create_dd.value;
    
    if (wk_yyyy.match(/[^0-9]/g) ||
        wk_mm.match(/[^0-9]/g) ||
        wk_dd.match(/[^0-9]/g)) {
        alert("作成年月日に数値以外が設定されています。");
        return false;
    }
    
    //１日平均入院患者数
    var wk = document.mainForm.hosp_patient_cnt.value;
    if (wk.match(/[^0-9]/g)) {
        alert("１日平均入院患者数に数値以外が設定されています。");
        return false;
    }
    
    //平均在院日数
    wk = document.mainForm.inp_hospitalization_cnt.value;
    if (wk.match(/[^0-9]/g)) {
        alert("平均在院日数に数値以外が設定されています。");
        return false;
    }
    
    return true;
}

//再作成
function remake(flag) {
    switch(flag) {
    case '1':
        msg = '実績から';
        break;
    case '2':
        msg = '予定から';
        break;
    default: //'3'
        msg = '前日までの実績と当日以降は予定から';
        break;
    }
    if (confirm(msg+'作成します。よろしいですか？')) {
        document.mainForm.action="duty_shift_report.php";
        document.mainForm.submit();
    }
}

//「ＥＸＣＥＬ作成」
function makeExcel() {
    document.xlsForm.create_yyyy.value = document.mainForm.inp_create_yyyy.value;                   //作成年
    document.xlsForm.create_mm.value = document.mainForm.inp_create_mm.value;                       //作成月
    document.xlsForm.create_dd.value = document.mainForm.inp_create_dd.value;                       //作成費
    document.xlsForm.hospitalization_cnt.value = document.mainForm.inp_hospitalization_cnt.value;   //平均在院日数
    document.xlsForm.hosp_patient_cnt.value = document.mainForm.hosp_patient_cnt.value; //１日平均入院患者数
    //予定実績フラグ タイトルに予定実績の文言追加 20120209
    for (i=0; i<document.mainForm.elements['plan_result_flag'].length; i++) {
        if (document.mainForm.elements['plan_result_flag'][i].checked) {
            document.xlsForm.elements['plan_result_flag'].value = document.mainForm.elements['plan_result_flag'][i].value;
            break;
        }
    }
    document.xlsForm.submit();
}

//看護協会版届出様式作成
function makeAssociate(_kango_space_line , _hojo_space_line) {
    document.associateForm.create_yyyy.value = document.mainForm.inp_create_yyyy.value;                 //作成年
    document.associateForm.create_mm.value = document.mainForm.inp_create_mm.value;                     //作成月
    document.associateForm.create_dd.value = document.mainForm.inp_create_dd.value;                     //作成費
    document.associateForm.hospitalization_cnt.value = document.mainForm.inp_hospitalization_cnt.value; //平均在院日数
    document.associateForm.hosp_patient_cnt.value = document.mainForm.hosp_patient_cnt.value;   //１日平均入院患者数
    document.associateForm.kango_space_line.value = _kango_space_line ;
    document.associateForm.hojo_space_line.value = _hojo_space_line ;
    document.associateForm.submit();
}

///-----------------------------------------------------------------------------
// 新様式の対象年月か判断する(平成２４年４月以降が新様式)
///-----------------------------------------------------------------------------
function isNewStylePeriod(year, month) {
    var date = new Date(year, month - 1, 1);
    var baseDate = new Date(2012, 3, 1);    // 判定基準日は2012/04/01
    if (date >= baseDate) {
        return true;
    } else {
        return false;
    }
}
