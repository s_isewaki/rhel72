$(function() {

	// シフトグループ切り替え
	$('#group_select').on('change', function() {
		var session = document.getElementById("session");
		var group_id = document.getElementById("group_select");
		location.href = 'duty_shift_need_cnt_standard.php?group_id=' + group_id.value + '&session=' + session.value;
	});

	// テーブルのDrap&Drop並べ替え
	$('#list > tbody').sortable({
		items: '> tr',
		axis: 'y',
		cancel: 'select,input',
		placeholder: 'droptarget',
		opacity: 0.4,
		scroll: false
	});
});

// テーブル行の削除
function remove_table_row(obj) {
	var tr = obj.parentNode.parentNode;
	tr.parentNode.deleteRow(tr.sectionRowIndex);
}

// テーブル行の追加
function add_table_row(obj,pattern,team,job,st,skill,skill2) {
	var table = document.getElementById("list_body");
	var idx = obj.parentNode.parentNode.sectionRowIndex + 1;
	if (/thead/i.test(obj.parentNode.parentNode.parentNode.nodeName)) {
		idx = 0;
	}
	var row = table.insertRow(idx);
	row.className = 'draggable';
	var cell01 = row.insertCell(0);
	var cell02 = row.insertCell(1);
	var cell03 = row.insertCell(2);
	var cell04 = row.insertCell(3);
	var cell05 = row.insertCell(4);
	var cell06 = row.insertCell(5);
	var cell07 = row.insertCell(6);
	var cell08 = row.insertCell(7);
	var cell09 = row.insertCell(8);
	var cell10 = row.insertCell(9);
	var cell11 = row.insertCell(10);
	var cell12 = row.insertCell(11);
	var cell13 = row.insertCell(12);
	var cell14 = row.insertCell(13);
	var cell15 = row.insertCell(14);
	var cell16 = row.insertCell(15);
	var cell17 = row.insertCell(16);

	cell01.innerHTML = '-';
	cell02.innerHTML = '<select name="atdptn_ptn_id[]">'+pattern+'</select>';
	cell03.innerHTML = '<select name="team[]"><option></option>'+team+'</select>';
	cell04.innerHTML = '<select name="job_id[]"><option></option>'+job+'</select> <select name="job_id2[]"><option></option>'+job+'</select> <select name="job_id3[]"><option></option>'+job+'</select>';
	cell05.innerHTML = '<select name="st_id[]"><option></option>'+st+'</select>';
	cell06.innerHTML = '<select name="need_skill[]"><option></option>'+skill+'</select>又は<select name="need_skill2[]"><option></option>'+skill2+'</select>';
	cell07.innerHTML = '<select name="sex[]"><option></option><option value="1">男性</option><option value="2">女性</option></select>';
	cell08.innerHTML = '<input name="mon_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell09.innerHTML = '<input name="tue_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell10.innerHTML = '<input name="wed_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell11.innerHTML = '<input name="thu_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell12.innerHTML = '<input name="fri_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell13.innerHTML = '<input name="sat_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell14.innerHTML = '<input name="sun_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell15.innerHTML = '<input name="holiday_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;">';
	cell16.innerHTML = '<button type="button" onclick="add_table_row(this,pattern,team,job,st,skill,skill2);">追加</button>';
	cell17.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';
}


// 入力チェック
function check_form() {
	var list = document.getElementById("list_body");
    var TR = list.getElementsByTagName('tr');

	var ptn = document.getElementsByName('atdptn_ptn_id[]');
	var team = document.getElementsByName('team[]');
	var job1 = document.getElementsByName('job_id[]');
	var job2 = document.getElementsByName('job_id2[]');
	var job3 = document.getElementsByName('job_id3[]');
	var st   = document.getElementsByName('st_id[]');
	var sex  = document.getElementsByName('sex[]');
	var skill = document.getElementsByName('need_skill[]');

	// 重複チェック
	var dup_trid = new Array();
	var dup_table = new Array();
	var dup_flg = false;
	for(var i=0; i<ptn.length; i++) {
		if (dup_trid[ptn[i].parentNode.parentNode.sectionRowIndex]) {
			continue;
		}
		dup_trid[ptn[i].parentNode.parentNode.sectionRowIndex] = true;
		var idx1 = (new Array(ptn[i].value,team[i].value,job1[i].value,st[i].value,skill[i].value,sex[i].value)).join("_");
		if (dup_table[idx1]) {
			ptn[i].style.backgroundColor = 'pink';
			team[i].style.backgroundColor = 'pink';
			job1[i].style.backgroundColor = 'pink';
			job2[i].style.backgroundColor = 'pink';
			job3[i].style.backgroundColor = 'pink';
			st[i].style.backgroundColor = 'pink';
			skill[i].style.backgroundColor = 'pink';
			sex[i].style.backgroundColor = 'pink';
			dup_flg = true;
		} else {
			dup_table[idx1] = true;
		}
		if (job2[i].value != "") {
			var idx2 = (new Array(ptn[i].value,team[i].value,job2[i].value,st[i].value,skill[i].value,sex[i].value)).join("_");
			if (dup_table[idx2]) {
				ptn[i].style.backgroundColor = 'pink';
				team[i].style.backgroundColor = 'pink';
				job1[i].style.backgroundColor = 'pink';
				job2[i].style.backgroundColor = 'pink';
				job3[i].style.backgroundColor = 'pink';
				st[i].style.backgroundColor = 'pink';
				skill[i].style.backgroundColor = 'pink';
				sex[i].style.backgroundColor = 'pink';
				dup_flg = true;
			} else {
				dup_table[idx2] = true;
			}
		}
		if (job3[i].value != "") {
			var idx3 = (new Array(ptn[i].value,team[i].value,job3[i].value,st[i].value,skill[i].value,sex[i].value)).join("_");
			if (dup_table[idx3]) {
				ptn[i].style.backgroundColor = 'pink';
				team[i].style.backgroundColor = 'pink';
				job1[i].style.backgroundColor = 'pink';
				job2[i].style.backgroundColor = 'pink';
				job3[i].style.backgroundColor = 'pink';
				st[i].style.backgroundColor = 'pink';
				skill[i].style.backgroundColor = 'pink';
				sex[i].style.backgroundColor = 'pink';
				dup_flg = true;
			} else {
				dup_table[idx3] = true;
			}
		}

	}
	if (dup_flg) {
		alert("重複している条件があります。");
		return false;
	}

	// 数値チェック
	var num_flg = false;
	var inputs = list.getElementsByTagName('input');
	for(var i=0; i<inputs.length; i++) {
		if(/_cnt/.test(inputs[i].name) && isNaN(inputs[i].value)) {
			inputs[i].style.backgroundColor = 'pink';
			num_flg = true;
		}
	}
	if (num_flg) {
		alert("数値以外の値が入力されています。");
		return false;
	}

	var select = document.getElementsByTagName('select');
	for(var i=0; i<select.length; i++) {
		select[i].style.backgroundColor = '';
	}
	var inputs = document.getElementsByTagName('input');
	for(var i=0; i<inputs.length; i++) {
		if(/_cnt/.test(inputs[i].name)) {
			inputs[i].style.backgroundColor = '';
		}
	}

	document.getElementById("form").submit();
}