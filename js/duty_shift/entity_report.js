/***
 * 右のセレクトボックスから左に追加
 * @param {type} nurse
 * @param {type} base
 * @returns {undefined}
 */
function selectItem(nurse, base) {
    var n = $('#' + nurse).find('option');
    var b = $('#' + base + ' option:selected');

    for (var i = 0; i < b.length; i++) {
        var flg = false;
        for (var j = 0; j < n.length; j++) {
            if ($(b[i]).val() === $(n[j]).val()) {
                flg = true;
                break;
            }
        }
        if (!flg) {
            $('#' + nurse).append('<option value="' + $(b[i]).val() + '">' + $(b[i]).text() + '</option>');
        }
    }
}

/***
 * 右のセレクトボックスから削除
 * @param {type} nurse
 * @returns {undefined}
 */
function deleteItem(nurse) {
    $('#' + nurse).find('option:selected').each(function() {
        if (!$(this).hasClass('disabled')) {
            $(this).remove();
        }
    });
}

/**
 * 選択行の追加
 * @param {type} row
 * @returns {undefined}
 */
function addDatRow(row) {
    var select = '<tr><td><select name="job_id[]">' + job + '</select></td><td><select name="ptn_id[]">' + ptn + '</select></td>';
    select += '<td><input id="addbtn" type="button" value="追加" onclick="addDatRow(this);" style="margin-left:2px;"><input type="button" value="削除" onclick="deleteDatRow(this);" style="margin-left:2px;"></td></tr>';
    $(row).parent().parent().after(select);
}

/***
 * 選択行の削除
 * @param {type} row
 * @returns {undefined}
 */
function deleteDatRow(row) {
    $(row).parent().parent().remove();
}

/***
 * domready
 */
var job;
var ptn;
$(function() {
    // job_list
    $.each(job_list, function(idx, row) {
        job += '<option value="' + row.id + '">' + row.name + '</option>';
    });

    // pattern_list
    $.each(pattern_list, function(idx, row) {
        ptn += '<option value="' + row.id + '">' + row.name + '</option>';
    });
});

/**
 * SUBMIT
 * @returns {unresolved}
 */
function updateSetting() {
    // 看護職員データ
    var n1 = [];
    $('#nurse1').find('option').each(function(idx,n) {
        n1.push(n.value);
    });
    var n2 = [];
    $('#nurse2').find('option').each(function(idx,n) {
        n2.push(n.value);
    });
    var n3 = [];
    $('#nurse3').find('option').each(function(idx,n) {
        n3.push(n.value);
    });

    // 看護職員重複チェック
    var errmsg = '';
    $.each(n1, function(idx,data) {
        if ($.inArray(data, n2) > -1) {
            errmsg += '看護職員(看護師)と看護職員(准看護師)に同じ職種が登録されています。\n';
            return false;
        }
    });
    $.each(n1, function(idx,data) {
        if ($.inArray(data, n3) > -1) {
            errmsg += '看護職員(看護師)と看護補助者に同じ職種が登録されています。\n';
            return false;
        }
    });
    $.each(n2, function(idx,data) {
        if ($.inArray(data, n3) > -1) {
            errmsg += '看護職員(准看護師)と看護補助者に同じ職種が登録されています。\n';
            return false;
        }
    });

    // 申し送り時間パターン重複チェック
    var ptn_job = $('select[name="job_id[]"]');
    var ptn_ptn = $('select[name="ptn_id[]"]');

    for (var i = 0; i < ptn_job.length - 1; i++) {
        for (var j = i + 1; j < ptn_job.length; j++) {
            if ($(ptn_job[i]).val() === $(ptn_job[j]).val() && $(ptn_ptn[i]).val() === $(ptn_ptn[j]).val()) {
                errmsg += '申し送り時間を差し引かないパターンに重複している項目があります。\n';
            }
        }
    }
    if (errmsg) {
        alert(errmsg);
        return false;
    }
    //委員会WG
    var pjt = [];
    $('#pjt').find('option').each(function(idx,n) {
        pjt.push(n.value);
    });

    $('#hidden_nurse1').val(n1.join(','));
    $('#hidden_nurse2').val(n2.join(','));
    $('#hidden_nurse3').val(n3.join(','));
    $('#hidden_pjt').val(pjt.join(','));

    $('#mainform').submit();
}