///-----------------------------------------------------------------------------
// resizeIframeEx
//------------------------------------------------------------------------------
var viewAreaHgt = 0; // 表示領域の高さ
var sclContHgt = 0; // スクロールさせるコンテンツ(#wrapCont)の高さ
var sclContTopRange = 0; // #wrapContの左上の座標
var tblWidth = 0;
var elmFloatingpage = "";
var movedCountWindow = false;

/**
 * iframe表示のときの勤務表リサイズ処理
 */
function resizeIframeEx() {
    try {
        elmFloatingpage = parent.document.getElementById('floatingpage');

        // 表示領域の高さ設定
        viewAreaHgt = parent.document.body.clientHeight;
        sclContTopRange = $('#region').offset().top + 20;

        // iframeに新しい高さを設定 (表示領域 - 全体のヘッダー)
        var iframeHgt = viewAreaHgt - 110;
        elmFloatingpage.style.height = iframeHgt + 'px';
        parent.callSideMenu = false;

        // 集計行の位置を動かしていなければ、リサイズする
        if (movedCountWindow === false) {
            sclContHgt = iframeHgt - sclContTopRange;

            if (count_row === 0) {
                if (sclContHgt < $('#data').height() + $('#countWindow').height()) {
                    var height = sclContHgt - $('#countWindow').height();
                    $('#region').height((height > 22 ? height : 22) + 'px');
                }
            }
            else {
                var rows = $('#data').find('tr');
                if (rows.length > count_row) {
                    var count_pos = rows.eq(count_row).offset().top - $('#region').offset().top;
                    $('#region').height((count_pos > 22 ? count_pos : 22) + 'px');
                }
            }

            // スクロールの位置調整
            $('#region').width(data.offsetWidth + 20);
            $('#outCountWindowBar').width(data.offsetWidth + 'px');
        }
    }
    catch (e) {
        alert(e);
    }
}

/**
 * フルスクリーン表示のときの勤務表リサイズ処理
 */
function resizeFullScreen() {
    try {
        var sclContHgt = document.body.clientHeight - $('#region').offset().top + 20;
        var dataHeight = $('#data').height();
        var countWindowHeight = $('#countWindow').height();

        $('#outCountWindowBar').width(data.offsetWidth + 'px');
        if (dataHeight > sclContHgt - countWindowHeight) {
            $('#region').height((sclContHgt - countWindowHeight) + 'px');
        }

        // スクロールの位置調整
        $('#region').width(data.offsetWidth + 20);
    }
    catch (e) {
        alert(e);
    }
}

/**
 * 記号の変更を検知してAjax処理を行う
 * @param {str} data セルのid
 * @param {str} idx セルの列番地
 * @param {str} day セルの行番地
 * @param {str} plan_hope_flg 希望表示しているかフラグ
 */
function changeCell(data, idx, day, plan_hope_flg) {
    if (window.gThruChangCell) return; // 2014/10 計算不要フラグがあれば無視
    // 集計設定Aでなければ処理しない
    if (change_switch !== 2) {
        return;
    }
    // IE6なら処理しない
    if (_ua.ltIE6) {
        return;
    }

    // 予定
    var ptn_base = 'atdptn_ptn_id_';
    var rsn_base = 'reason_2_';

    // 希望
    if (plan_hope_flg !== '0') {
        ptn_base = 'rslt_id_';
        rsn_base = 'rslt_reason_2_';
    }

    // 日別
    var pidDayArray = [];
    var ptnDayArray = [];
    var rsnDayArray = [];
    for (var d = 1; d <= document.mainform.elements['day_cnt'].value; d++) {
        var pid1 = 'pattern_id_' + idx + '_' + d;
        var ptn1 = ptn_base + idx + '_' + d;
        var rsn1 = rsn_base + idx + '_' + d;

        pidDayArray.push(document.mainform.elements[pid1].value);
        ptnDayArray.push(document.mainform.elements[ptn1].value);
        rsnDayArray.push(document.mainform.elements[rsn1].value);
    }

    // 職員別
    var pidEmpArray = [];
    var ptnEmpArray = [];
    var rsnEmpArray = [];
    var assEmpArray = [];
    var jobEmpArray = [];
    for (var e = 0; e < document.mainform.elements['data_cnt'].value; e++) {
        var pid2 = 'pattern_id_' + e + '_' + day;
        var ptn2 = ptn_base + e + '_' + day;
        var rsn2 = rsn_base + e + '_' + day;
        var ass = document.mainform.elements['assist_group_' + e].value.split(',');

        pidEmpArray.push(document.mainform.elements[pid2].value);
        ptnEmpArray.push(document.mainform.elements[ptn2].value);
        rsnEmpArray.push(document.mainform.elements[rsn2].value);
        assEmpArray.push(ass[day - 1]);
        jobEmpArray.push(document.mainform.elements['job_id[' + e + ']'].value);
    }

    $.ajax({
        url: 'duty_shift_sign_count_ajax.php',
        type: 'post',
        async: false,
        dataType: 'json',
        data: {
            mode: 'plan',
            data: data,
            plan_hope_flg: plan_hope_flg,
            session: document.mainform.elements['session'].value,
            group_id: document.mainform.elements['cause_group_id'].value,
            pattern_id: document.mainform.elements['pattern_id'].value,
            day_cnt: document.mainform.elements['day_cnt'].value,
            data_cnt: document.mainform.elements['data_cnt'].value,
            start_date: document.mainform.elements['duty_start_date'].value,
            end_date: document.mainform.elements['duty_end_date'].value,
            pid_day: pidDayArray,
            ptn_day: ptnDayArray,
            rsn_day: rsnDayArray,
            ass_day: document.mainform.elements['assist_group_' + idx].value.split(','),
            pid_emp: pidEmpArray,
            ptn_emp: ptnEmpArray,
            rsn_emp: rsnEmpArray,
            ass_emp: assEmpArray,
            job_emp: jobEmpArray
        },
        success: changedCount
    });
}

/**
 * Ajaxの応答を受けて、集計値の変更を行う
 */
function changedCount(data) {
    $.each(data, function(id, value) {
        $(id).text(value);
    });
    changedSummary();
}

/**
 * 集計値の変更を受けて、総計の変更を行う
 */
function changedSummary() {
    // 個人日数(予定)
    var total_0 = [];
    $.each($('td.sum_gyo_0'), function() {
        var id = $(this).attr('id').split('_');
        if (!total_0[id[2]]) {
            total_0[id[2]] = 0;
        }
        total_0[id[2]] += parseFloat($(this).html());
    });
    $.each(total_0, function(k, v) {
        $('#day_total_0_' + k).text(v);
    });

    // 個人日数(実績・希望)
    var total_1 = [];
    $.each($('td.sum_gyo_1'), function() {
        var id = $(this).attr('id').split('_');
        if (!total_1[id[2]]) {
            total_1[id[2]] = 0;
        }
        total_1[id[2]] += parseFloat($(this).html());
    });
    $.each(total_1, function(k, v) {
        $('#day_total_1_' + k).text(v);
    });

    // 項目別日数
    var total_e = [];
    $.each($('td.sum_retu'), function() {
        var id = $(this).attr('id').split('_');
        var idx = id[0].slice(7);
        if (!total_e[idx]) {
            total_e[idx] = 0;
        }
        total_e[idx] += parseFloat($(this).html());
    });
    $.each(total_e, function(k, v) {
        $('#emp_total_' + k).html(v + '&nbsp;');
    });
}

$(function() {

    ///-----------------------------------------------------
    // フルスクリーンのリサイズ
    ///-----------------------------------------------------
    if (fullscreen_flg) {
        resizeFullScreen();
    }

    ///-----------------------------------------------------
    // 集計行ドラッグ＆ドロップ
    ///-----------------------------------------------------
    $("#outCountWindowBar").on('mousedown', function(e) {
        e.preventDefault();
        $("#outCountWindow")
            .data("pageY", e.pageY)
            .data("height", $('#region').height())
            .data("clickPointY", e.pageY - $("#outCountWindow").offset().top);

        $(document).on({
            mousemove: function(e) {
                e.preventDefault();
                var win = $("#outCountWindow");
                var height = win.data("height") - (win.data("pageY") - e.pageY);
                if (height >= 22 && height <= $('#data').height()) {
                    win.css({
                        top: e.pageY - win.data("clickPointY") + "px"
                    });
                    $('#region').height(height + 'px');
                    movedCountWindow = true;
                }
            },
            mouseup: function() {
                $(document).off("mousemove");
            }
        });
    });

    ///-----------------------------------------------------
    // スタンプ、キー入力
    //------------------------------------------------------
    $("a.open").click(function() {
        $("#floatWindowKey").fadeIn("fast");
        $("#floatWindowStp").fadeIn("fast");
        return false;
    });

    $("#floatWindowKey a.close").click(function() {
        $("#floatWindowKey").fadeOut("fast");
        return false;
    });

    $("#floatWindowStp a.close").click(function() {
        $("#floatWindowStp").fadeOut("fast");
        return false;
    });

    $("#floatWindowKey dl dt").mousedown(function(e) {
        e.preventDefault();
        $("#floatWindowKey")
            .data("clickPointX", e.pageX - $("#floatWindowKey").offset().left)
            .data("clickPointY", e.pageY - $("#floatWindowKey").offset().top);

        $(document).mousemove(function(e) {
            e.preventDefault();
            $("#floatWindowKey").css({
                top: e.pageY - $("#floatWindowKey").data("clickPointY") + "px",
                left: e.pageX - $("#floatWindowKey").data("clickPointX") + "px"
            });
        });
    }).mouseup(function() {
        $(document).unbind("mousemove");
    });


    $("#floatWindowStp dl dt").mousedown(function(e) {
        e.preventDefault();
        $("#floatWindowStp")
            .data("clickPointX", e.pageX - $("#floatWindowStp").offset().left)
            .data("clickPointY", e.pageY - $("#floatWindowStp").offset().top);

        $(document).mousemove(function(e) {
            e.preventDefault();
            $("#floatWindowStp").css({
                top: e.pageY - $("#floatWindowStp").data("clickPointY") + "px",
                left: e.pageX - $("#floatWindowStp").data("clickPointX") + "px"
            });
        });
    }).mouseup(function() {
        $(document).unbind("mousemove");
    });

    ///-----------------------------------------------------
    // 月平均夜勤時間
    //------------------------------------------------------
    // 初期表示
    if ($("input[name='recomputation_flg']:checked").val() == '1') {
        $('#date_d1').attr('disabled', 'disabled');
    } else {
        $('#date_d1').removeAttr("disabled");
    }

    // 月平均夜勤ボタン
    $('#floatYakinAvg_button').click(function() {
        if ($("#floatYakinAvg").css('display') === 'none') {
            $("#floatYakinAvg").show();
        }
        else {
            $("#floatYakinAvg").hide();
        }
    });

    // 月平均夜勤画面クローズ
    $("#floatYakinAvg_header span.close").click(function() {
        $("#floatYakinAvg").hide();
    });

    // 月平均夜勤画面ドラッグ＆ドロップ
    $("#floatYakinAvg_header").mousedown(function(e) {
        e.preventDefault();
        $("#floatYakinAvg_header").css('cursor', 'move');
        $("#floatYakinAvg")
            .data("clickPointX", e.pageX - $("#floatYakinAvg").offset().left)
            .data("clickPointY", e.pageY - $("#floatYakinAvg").offset().top);

        $(document).mousemove(function(e) {
            e.preventDefault();
            $("#floatYakinAvg").css({
                top: e.pageY - $("#floatYakinAvg").data("clickPointY") + "px",
                left: e.pageX - $("#floatYakinAvg").data("clickPointX") + "px"
            });
        });
    })
        .mouseup(function() {
        $(document).unbind("mousemove");
        $("#floatYakinAvg").css('cursor', 'pointer');
    });

    // 夜勤計算のときはウインドウ表示
    if (view_YakinAvg) {
        $('#floatYakinAvg').show();
    }

    // 「１ヵ月計算/４週計算」切替時
    $("input[name='recomputation_flg']").change(function() {    // １ヵ月計算
        if ($("input[name='recomputation_flg']:checked").val() == '1') {
            // 日付を1日にセットする
            $('#date_d1').val('01');
            $('#date_d1').attr('disabled', 'disabled');
            $('#calendar_img').unbind('click');
            updateCal1();   // カレンダー
        } else {    // ４週計算
            $('#calendar_img').bind('click', function() {
                show_cal1();
            });
            $('#date_d1').removeAttr("disabled");
        }
    });

    ///-----------------------------------------------------
    //キー押下イベント
    ///-----------------------------------------------------
    document.onkeydown = function(e) {
        var shift, ctrl;

        //スタンプ選択時
        if (document.mainform.stamp_flg && document.mainform.stamp_flg.value == "1") {
            return;
        }

        //『週』選択時はキーイベント無効
        if (document.mainform.edit_start_day.value != document.mainform.edit_end_day.value) {
            return;
        }

        //コメント選択時 テキストボックスにフォーカスが掛かった状態ならばキーイベントはスルー
        if (document.mainform.plan_comment_flg && document.mainform.plan_comment_flg.value == 2) {
            var element = (document.activeElement || window.getSelection().focusNode);
            if (element.name) {
                if (element.name.indexOf('comment_') >= 0) {
                    return;
                }
            }
        }

        // Mozilla(Firefox, NN) and Opera
        if (e != null) {
            keycode = e.which;
            ctrl = typeof e.modifiers == 'undefined' ? e.ctrlKey : e.modifiers & Event.CONTROL_MASK;
            shift = typeof e.modifiers == 'undefined' ? e.shiftKey : e.modifiers & Event.SHIFT_MASK;

            // イベントの上位伝播を防止
            e.preventDefault();
            e.stopPropagation();

            // Internet Explorer
        } else {
            keycode = event.keyCode;
            ctrl = event.ctrlKey;
            shift = event.shiftKey;

            // イベントの上位伝播を防止
            event.returnValue = false;
            event.cancelBubble = true;
        }

        //******イベントを発生させる
        switch (keycode) {
            case 192:   //[@];
                //funcNotStampExec('');//未設
                setStampPtn('', '', 'stamp_');
                break;
            case 37:    //[←]
                markFlagChange('', 'left', 1);  //目印画像の左移動
                break;
            case 39:    //[→]
                markFlagChange('', 'right', 1); //目印画像の右移動
                break;
            case 38:    //[↑]
                markFlagChange('', 'up', 1);        //目印画像の上移動
                break;
            case 40:    //[↓]
                markFlagChange('', 'down', 1);  //目印画像の下移動
                break;
            case 49:    //[1]
                funcNotStampExec('0');
                break;
            case 50:    //[2]
                funcNotStampExec('1');
                break;
            case 51:    //[3]
                funcNotStampExec('2');
                break;
            case 52:    //[4]
                funcNotStampExec('3');
                break;
            case 53:    //[5]
                funcNotStampExec('4');
                break;
            case 54:    //[6]
                funcNotStampExec('5');
                break;
            case 55:    //[7]
                funcNotStampExec('6');
                break;
            case 56:    //[8]
                funcNotStampExec('7');
                break;
            case 57:    //[9]
                funcNotStampExec('8');
                break;
            case 48:    //[0]
                funcNotStampExec('9');
                break;
            case 81:    //[Q]
                funcNotStampExec('10');
                break;
            case 87:    //[W]
                funcNotStampExec('11');
                break;
            case 69:    //[E]
                funcNotStampExec('12');
                break;
            case 82:    //[R]
                funcNotStampExec('13');
                break;
            case 84:    //[T]
                funcNotStampExec('14');
                break;
            case 89:    //[Y]
                funcNotStampExec('15');
                break;
            case 85:    //[U]
                funcNotStampExec('16');
                break;
            case 73:    //[I]
                funcNotStampExec('17');
                break;
            case 79:    //[O]
                funcNotStampExec('18');
                break;
            case 80:    //[P]
                funcNotStampExec('19');
                break;
            case 65:    //[A]
                funcNotStampExec('20');
                break;
            case 83:    //[S]
                funcNotStampExec('21');
                break;
            case 68:    //[D]
                funcNotStampExec('22');
                break;
            case 70:    //[F]
                funcNotStampExec('23');
                break;
            case 71:    //[G]
                funcNotStampExec('24');
                break;
            case 72:    //[H]
                funcNotStampExec('25');
                break;
            case 74:    //[J]
                funcNotStampExec('26');
                break;
            case 75:    //[K]
                funcNotStampExec('27');
                break;
            case 76:    //[L]
                funcNotStampExec('28');
                break;
            case 90:    //[Z]
                funcNotStampExec('29');
                break;
            case 88:    //[X]
                funcNotStampExec('30');
                break;
            case 67:    //[C]
                funcNotStampExec('31');
                break;
            case 86:    //[V]
                funcNotStampExec('32');
                break;
            case 66:    //[B]
                funcNotStampExec('33');
                break;
            case 78:    //[N]
                funcNotStampExec('34');
                break;
            case 77:    //[M]
                funcNotStampExec('35');
                break;
        }
    };

    /***
     * 画面リサイズイベント
     */
    // フルスクリーン以外の場合 20151201
    if (!fullscreen_flg) {
	    $(window).resize(function() {
	        resizeIframeEx();
	    });
    }
});

/**
 * ウインドウ非表示
 */
function floatWindowHide(a, b, c) {
    if (a == '0') {
        document.getElementById('floatWindow' + b).style.display = "none";
        //ウインドウ表示
    } else {

        document.getElementById('floatWindow' + b).style.display = "";
        //初期表示位置
        if (b == 'Key') {
            var top = document.getElementById('floatWindow' + b).style.top;
            var left = document.getElementById('floatWindow' + b).style.left;
            if (top == '' && left == '') {
                document.getElementById('floatWindow' + b).style.top = 60;
                document.getElementById('floatWindow' + b).style.left = 915;
            }
        } else {
            document.getElementById('floatWindow' + b).style.top = 5;
            document.getElementById('floatWindow' + b).style.left = 390;
        }

        //もう一方のウインドウは非表示
        document.getElementById('floatWindow' + c).style.display = "none";
    }
}

/**
 * スタンプ有効無効設定
 */
function setStampFlg() {
    if (!document.mainform.data_exist_flg || !document.mainform.stamp_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    var flg = (document.mainform.stamp_flg.value == '') ? '1' : '';
    document.mainform.stamp_flg.value = flg;

    var btn = document.getElementById('stamp_img');
    if (flg == '') {
        btn.src = 'img/ico-illust-stamp.gif';
        //フローティング(スタンプ用)非表示
        floatWindowHide(0, 'Stp', 'Key');
    } else {
        btn.src = 'img/ico-illust-stamp-off.gif';
        //フローティング(スタンプ用)表示
        floatWindowHide(1, 'Stp', 'Key');
    }
    //キー入力用目印画像非表示
    $('#' + flagBack).removeClass('flag');
}


/**
 * 夜勤のみ表示
 */
var nightShiftOnlyFlag = false;    // フラグ
var scrollHeightStore = 0;
var offsetTopStore = 0;
function dispNightShiftOnly() {
    // 全員表示
    if (nightShiftOnlyFlag) {
        nightShiftOnlyFlag = false;
        $('#disp_night_shift').val('夜勤のみ表示');

        // 全員表示
        $('tr.day_shift').css({
            display: ''
        });

        // 表の高さを戻す
        $('#region').height(scrollHeightStore);
        $('#outCountWindow').css('top', offsetTopStore);
    }

    // 夜勤のみ表示
    else {
        nightShiftOnlyFlag = true;
        $('#disp_night_shift').val('全員表示');

        // 表の高さを保存
        scrollHeightStore = $('#region').height();
        offsetTopStore = $('#outCountWindow').offset().top;

        // 夜勤以外を非表示
        $('tr.day_shift').css({
            display: 'none'
        });

        // 表の高さを調整
        var data = $('#data');
        if (scrollHeightStore > data.height()) {
            $('#region').height(data.height());
            $('#outCountWindow').css('top', data.offset().top + data.height());
        }
    }
}

///-----------------------------------------------------------------------------
//勤務パターン登録子画面
///-----------------------------------------------------------------------------
function ptnUpdate(plan_hope_flg, emp_idx, atdptn_ptn_id, reason_2, day) {

    if (!document.mainform.data_exist_flg || !document.mainform.stamp_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();

    //登録権限があるかチェック
    if (document.mainform.create_flg.value == "") {
        return;
    }

    //---------------------------------------------------------------------------------------------
    //スタンプ選択時
    //---------------------------------------------------------------------------------------------
    if (document.mainform.stamp_flg.value != "") {
        setStamp(emp_idx, day, plan_hope_flg);
        return;
    }

    //---------------------------------------------------------------------------------------------
    //キー入力
    //---------------------------------------------------------------------------------------------
    //二行表示の確認
    if (document.mainform.plan_results_flg.value == 0
        && document.mainform.plan_hope_flg.value == 0
        && document.mainform.plan_duty_flg.value == 0
        && document.mainform.plan_comment_flg.value == 0) {

        var element_id = 'data' + (parseInt(emp_idx) + 1) + '_' + day;
    } else {

        if (plan_hope_flg == 0) {
            var element_id = 'data' + (parseInt(emp_idx * 2) + 1) + '_' + day;
        } else {
            var element_id = 'data' + (parseInt(emp_idx * 2) + 2) + '_' + day;
        }
    }

    var ele = document.getElementById(element_id);
    if (!ele) {
        alert('エレメントがみつかりません (id=' + element_id + ')');
        return false;
    }

    //選択セルエレメント 及び勤務シフト予定／希望フラグを保持
    document.getElementById("listPosition").value = element_id;
    document.getElementById('listPositionPlanHope').value = plan_hope_flg;

    //目印画像の表示 '0'指定で次マス移動無し
    markFlagChange('', 'right', 0);

    //フローティング(Key入力用)表示
    floatWindowHide(1, 'Key', 'Stp');

    //キー入力開始とみなしハイライト処理スルー開始
    //      ele.onmouseout();   //ハイライトを元に戻す
    document.mainform.keyInput_flg.value = 1;
}

///-----------------------------------------------------------------------------
///
///
///
///-----------------------------------------------------------------------------
function funcNotStampExec(col) {
    var ele = document.getElementById('not_stamp_' + col);
    if (!ele) {
        return;
    }
    ele.onclick();
}

///-----------------------------------------------------------------------------
/// 目印画像を指定IDの場所へ移動、指定ID情報を確保
/// IN element_id :エレメントID                      ※Null時はhidden『listPosition』を参照
/// IN direction  :移動する方向  right,left,up,down  ※Null時はright
/// IN steps      :横へ進むコマ数                    ※Null時は1コマ  up,downは１コマ固定([当直表示],[コメント表示],[予定表示]複数行表示なので２コマ移動)
///-----------------------------------------------------------------------------
var flagBack;
function markFlagChange(element_id, direction, steps) {

    var ele_id = element_id;
    var plan_hope_flg = $('#listPositionPlanHope').val();
    var i = parseInt(steps);
    var y = 1;
    var scrollStart = $('#region').offset().top;
    var scrollEnd = $('#outCountWindow').offset().top;

    if (element_id != '') {
        var ele = $('#' + element_id);
    } else {
        var now_ele = $("#listPosition").val();

        if (now_ele == "") {
            alert('設定位置をクリックしてください。');
            return;
        }

        //[当直表示],[コメント表示],[予定表示]２行表示時は上下２コマ進み
        if (document.mainform.plan_results_flg.value == 2
            || document.mainform.plan_comment_flg.value == 2
            || document.mainform.plan_duty_flg.value == 2) {
            y = 2;
        }

        var e = now_ele.split('_');
        var emp_idx = Number(e[0].slice(4));
        var day = e[1];

        switch (direction.toLowerCase()) {
            case 'right':
                ele_id = 'data' + (parseInt(emp_idx)) + '_' + (parseInt(day) + i);
                break;
            case 'left':
                ele_id = 'data' + (parseInt(emp_idx)) + '_' + (parseInt(day) - i);
                break;
            case 'up':
                ele_id = 'data' + (parseInt(emp_idx) - y) + '_' + (parseInt(day));
                break;
            case 'down':
                ele_id = 'data' + (parseInt(emp_idx) + y) + '_' + (parseInt(day));
                break;
            default:
                ele_id = 'data' + (parseInt(emp_idx)) + '_' + (parseInt(day) + i);
                break;
        }
        var ele = document.getElementById(ele_id);
    }

    if (!ele) {
        if (direction.toLowerCase() === 'right') {
            //次職員へ
            ele_id = 'data' + (parseInt(emp_idx) + y) + '_1';
            ele = document.getElementById(ele_id);

            if (!ele) {
                //先頭職員へ
                ele_id = 'data1_1';
                ele = document.getElementById(ele_id);

                if (!ele) {
                    return;
                }
            }
        }
        else {
            return;
        }
    }
    if (highlight_in2 && ele) highlight_in2("data", ele);

    var cursorTop = $('#' + ele_id).offset().top;
    var scrollTop = parseInt($('#region').scrollTop());
    if (scrollEnd < cursorTop + 25) {
        $('#region').scrollTop(scrollTop + 25);
    }
    if (scrollStart > cursorTop - 25) {
        $('#region').scrollTop(scrollTop - 25);
    }
    if (cursorTop < scrollStart) {
        $('#region').scrollTop(0);
    }

    // 旗の表示切り替え
    if (flagBack) {
        $('#' + flagBack).removeClass('flag');
    }
    $('#' + ele_id).addClass('flag');
    flagBack = ele_id;

    //hiddenへセット
    document.getElementById('listPosition').value = ele_id;

    if (!direction) {
        return;
    } else {
        if (direction.toLowerCase() == 'up' || direction.toLowerCase() == 'down') {
            //二行表示 [勤務希望]確認
            if (document.mainform.plan_hope_flg.value == 2) {
                if (plan_hope_flg == 0) {
                    plan_hope_flg = 2;
                } else {
                    plan_hope_flg = 0;
                }
                //移動先が勤務希望状態をセットする
                document.getElementById('listPositionPlanHope').value = plan_hope_flg;
            }
        }
    }
}

///-----------------------------------------------------------------------------
//ロード時の処理
///-----------------------------------------------------------------------------
function init_action() {
    if (window.name.indexOf('fullscreenFormChild') != -1) {
        if (window.opener.document.mainform && window.opener.document.mainform.elements['fullscreen_flg']) {
            window.opener.document.mainform.elements['fullscreen_flg'].value = "";
        }
    }
}

///-----------------------------------------------------------------------------
//応援追加
///-----------------------------------------------------------------------------
function staffAdd() {
    closeChildWin();
    //選択用職員一覧画面表示
    openEmployeeList('1');
}

///-----------------------------------------------------------------------------
//応援削除
///-----------------------------------------------------------------------------
function staffDelete(emp_id, emp_name) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    ///-----------------------------------------------------------------------------
    //最後の１人は削除不可
    ///-----------------------------------------------------------------------------
    var data_cnt = document.mainform.data_cnt.value;
    if (data_cnt <= 1) {
        return;
    }
    ///-----------------------------------------------------------------------------
    //削除可能チェック
    ///-----------------------------------------------------------------------------
    var wk_flg = "";
    for (i = 0; i < data_cnt; i++) {
        var wk1 = 'staff_id[' + i + ']';
        var staff_id = document.mainform.elements[wk1].value;
        if (staff_id == emp_id) {
            //スタッフ削除
            if (confirm('「' + emp_name + '」を削除します。よろしいですか？')) {
                wk_flg = "1";
                break;
            }
        }
    }
    ///-----------------------------------------------------------------------------
    //スタッフ削除
    ///-----------------------------------------------------------------------------
    if (wk_flg == "1") {
        document.mainform.add_staff_id.value = "";
        document.mainform.del_staff_id.value = emp_id;
        document.mainform.action = "duty_shift_menu.php";
        document.mainform.target = "";
        document.mainform.submit();
    }
}

///-----------------------------------------------------------------------------
//勤務希望取込
///-----------------------------------------------------------------------------
function dutyHopeUptake() {

    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    ///-----------------------------------------------------------------------------
    //勤務希望取込
    ///-----------------------------------------------------------------------------
    if (confirm('「勤務希望取込」します。よろしいですか？')) {
        document.mainform.hope_get_flg.value = "1";
        document.mainform.action = "duty_shift_menu.php";
        document.mainform.target = "";
        document.mainform.submit();
    }
}

///-----------------------------------------------------------------------------
// 自動シフト作成　開始日設定画面からの開始日データ反映
// 勤務シフト作成パターン(shift_case)を追加 20131227
///-----------------------------------------------------------------------------
//function add_start_dd(auto_start_day, auto_end_day, auto_ptn_id, auto_btn_flg) {
function add_start_dd(auto_start_day, auto_end_day, auto_ptn_id, auto_btn_flg, shift_case) {
    // 呼出時 月初・月末
    document.mainform.auto_start_day.value = parseInt(auto_start_day, 10);
    document.mainform.auto_end_day.value = parseInt(auto_end_day, 10);
    document.mainform.auto_ptn_id.value = auto_ptn_id;
    document.mainform.auto_btn_flg.value = auto_btn_flg;
    document.mainform.auto_flg.value = "1";
    document.mainform.shift_case.value = shift_case; // 勤務シフト作成パターンを追加 20131227
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();

    //画面（ＣＬＯＳＥ）
    closeChildWin();
}

///-----------------------------------------------------------------------------
// 過去月コピー　開始年月設定画面からの開始年月データ反映
///-----------------------------------------------------------------------------
function add_copy_yyyymm(copy_yyyy, copy_mm) {
    document.mainform.copy_yyyy.value = parseInt(copy_yyyy, 10);
    document.mainform.copy_mm.value = parseInt(copy_mm, 10);
    document.mainform.copy_before_month_flg.value = "1";
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();

    //画面（ＣＬＯＳＥ）
    closeChildWin();
}

///-----------------------------------------------------------------------------
//チェック
///-----------------------------------------------------------------------------
function dataCheck() {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    ///-----------------------------------------------------------------------------
    //チェック処理
    ///-----------------------------------------------------------------------------
    document.mainform.register_flg.value = "";
    document.mainform.check_flg.value = "1";
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//下書き保存／登録
///-----------------------------------------------------------------------------
function editData(flg) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから登録してください。');
        return false;
    }
    if (document.mainform.group_change_flg.value == '1') {
        alert('シフトグループの変更中のため処理できません。表示を待ってから登録してください。');
        return false;
    }
    if (document.mainform.regist_first_flg.value == '0') {
        return false;
    }
    closeChildWin();
    //下書き保存／登録
    if (flg == 1) {
        if (confirm('「下書き保存」します。よろしいですか？')) {
            document.mainform.regist_first_flg.value = '0';
            document.mainform.draft_flg.value = "1";
            document.mainform.action = "duty_shift_menu_update_exe.php";
            document.mainform.target = "";
            document.mainform.submit();
        }
    } else {
        var workdays_flg = document.mainform.workdays_flg.value;

        if (workdays_flg == "1"){
            //チェック機能 20131107
            document.mainform.regist_first_flg.value = '0';
            document.mainform.check_flg.value = "1";
            document.mainform.register_flg.value = "1";
            document.mainform.action = "duty_shift_menu.php";
            document.mainform.target = "";
            document.mainform.submit();
        }else{
            if (confirm('「登録」します。よろしいですか？')) {
                document.mainform.regist_first_flg.value = '0';
                document.mainform.draft_flg.value = "";
                document.mainform.action = "duty_shift_menu_update_exe.php";
                document.mainform.target = "";
                document.mainform.submit();
            }
        }
    }
}

///-----------------------------------------------------------------------------
//予定／実績表示
///-----------------------------------------------------------------------------
function showPlanResults(flg) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    //予定／実績表示
    document.mainform.plan_results_flg.value = flg;
    document.mainform.plan_hope_flg.value = 0;
    document.mainform.plan_duty_flg.value = 0;
    document.mainform.plan_comment_flg.value = 0;

    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//印刷
///-----------------------------------------------------------------------------
function dataPrint(total_print_flg, pdf_print_flg, selectmonth) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    //印刷ＷＩＮＤＯＷをＯＰＥＮ
    dx = screen.width;
    dy = screen.top;
    base_left = 0;
    base = 0;
    wx = 1100;
    wy = 600;
    childwin_print = window.open('', 'printFormChild', 'left=' + base_left + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
    document.mainform.action = "duty_shift_menu_print.php";
    document.mainform.target = "printFormChild";
    document.mainform.total_print_flg.value = total_print_flg;
    document.mainform.pdf_print_flg.value = pdf_print_flg;
    document.mainform.selectmonth.value = selectmonth;
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
/// Excel出力、PHP4用 2012.3.1 ( 前バージョン呼び出し用 )
///-----------------------------------------------------------------------------
function makeExcel4() {
    var total_print_flg = "0";
    if (document.mainform.total_disp_flg.value != "1") {
        total_print_flg = "1";
    }
    document.mainform.total_print_flg.value = total_print_flg;
    document.mainform.action = "duty_shift_menu_excel.php";
    document.mainform.target = "download";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
// Excel出力、PHP5用 2012.1.11
///-----------------------------------------------------------------------------
function makeExcel5_1_6(_ratio, _paper_size, _setfitpage, _mtop, _mbottom, _mleft, _mright, _mhead, _mfoot, _row_count, _row_type, _blancrows, _weekcolor, _hopecolor, _yearmonth, _week4start, _event) {
    document.mainform.excel_ratio.value = _ratio;
    document.mainform.excel_paper_size.value = _paper_size;
    document.mainform.excel_setfitpage.value = _setfitpage;
    document.mainform.excel_mtop.value = _mtop;
    document.mainform.excel_mbottom.value = _mbottom;
    document.mainform.excel_mleft.value = _mleft;
    document.mainform.excel_mright.value = _mright;
    document.mainform.excel_mhead.value = _mhead;
    document.mainform.excel_mfoot.value = _mfoot;
    document.mainform.excel_row_count.value = _row_count;
    document.mainform.excel_row_type.value = _row_type;
    document.mainform.excel_blancrows.value = _blancrows;
    document.mainform.excel_date.value = "";
    document.mainform.excel_weekcolor.value = _weekcolor;
    document.mainform.excel_hopecolor.value = _hopecolor;
    document.mainform.excel_yearmonth.value = _yearmonth;
    document.mainform.excel_week4start.value = _week4start;
    document.mainform.excel_event.value = _event;
    document.mainform.action = "duty_shift_menu_excel_5.php";
    document.mainform.target = "download";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
// Excel出力、PHP5用 2012.8.23 週間表
///-----------------------------------------------------------------------------
function makeExcel5_1_6week(_ratio, _paper_size, _setfitpage, _mtop, _mbottom, _mleft, _mright, _mhead, _mfoot, _row_count, _row_type, _blancrows, _date, _weekcolor, _hopecolor, _event) {
    document.mainform.excel_ratio.value = _ratio;
    document.mainform.excel_paper_size.value = _paper_size;
    document.mainform.excel_setfitpage.value = _setfitpage;
    document.mainform.excel_mtop.value = _mtop;
    document.mainform.excel_mbottom.value = _mbottom;
    document.mainform.excel_mleft.value = _mleft;
    document.mainform.excel_mright.value = _mright;
    document.mainform.excel_mhead.value = _mhead;
    document.mainform.excel_mfoot.value = _mfoot;
    document.mainform.excel_row_count.value = _row_count;
    document.mainform.excel_row_type.value = _row_type;
    document.mainform.excel_blancrows.value = _blancrows;
    document.mainform.excel_date.value = _date;
    document.mainform.excel_weekcolor.value = _weekcolor;
    document.mainform.excel_hopecolor.value = _hopecolor;
    document.mainform.excel_yearmonth.value = "";
    document.mainform.excel_week4start.value = "";
    document.mainform.excel_event.value = _event;
    document.mainform.action = "duty_shift_menu_excel_5week.php";
    document.mainform.target = "download";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//曜日別集計表
///-----------------------------------------------------------------------------
function makeExcelDow() {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    document.mainform.action = "duty_shift_menu_dow.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//全画面表示
///-----------------------------------------------------------------------------
function fullScreen() {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    //全画面ＷＩＮＤＯＷをＯＰＥＮ
    base_left = 0;
    base = 0;
    wx = window.screen.availWidth;
    wy = window.screen.availHeight;
    childwin_full = window.open('', 'fullscreenFormChild', 'left=' + base_left + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
    document.mainform.fullscreen_flg.value = "1";
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "fullscreenFormChild";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//作成期間変更
///-----------------------------------------------------------------------------
function termChg() {
    closeChildWin();
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//勤務希望表示
///-----------------------------------------------------------------------------
function showHope(flg) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    //勤務希望表示
    document.mainform.plan_results_flg.value = 0;
    document.mainform.plan_hope_flg.value = flg;
    document.mainform.plan_duty_flg.value = 0;
    document.mainform.plan_comment_flg.value = 0;

    //表示時
    if (flg == "2") {
        document.mainform.action = "duty_shift_menu.php";
    } else {
        //非表示時、希望データを保存
        document.mainform.hope_save_flg.value = "1";
        document.mainform.action = "duty_shift_menu_update_exe.php";
    }
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//当直表示
///-----------------------------------------------------------------------------
function showDuty(flg) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
    closeChildWin();
    //当直表示
    document.mainform.plan_results_flg.value = 0;
    document.mainform.plan_hope_flg.value = 0;
    document.mainform.plan_duty_flg.value = flg;
    document.mainform.plan_comment_flg.value = 0;

    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//ドラッグアンドコピー設定
///-----------------------------------------------------------------------------
function setDragDrop(flg) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }

    document.mainform.dragdrop_flg.value = flg;
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//集計表示非表示設定
///-----------------------------------------------------------------------------
function setTotalDisp(flg) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }

    document.mainform.total_disp_flg.value = flg;
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//休暇詳細表示非表示設定
///-----------------------------------------------------------------------------
function setTotalHolDisp(flg) {
    if (!document.mainform.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }

    document.mainform.hol_dt_flg.value = flg;
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//コメント表示
///-----------------------------------------------------------------------------
function showComment(flg) {
    closeChildWin();
    //コメント表示
    document.mainform.plan_results_flg.value = 0;
    document.mainform.plan_hope_flg.value = 0;
    document.mainform.plan_duty_flg.value = 0;
    document.mainform.plan_comment_flg.value = flg;

    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//計算
///-----------------------------------------------------------------------------
function compNightTime() {
    closeChildWin();
    ///-----------------------------------------------------------------------------
    //チェック（夜勤時間帯の設定されいること）
    ///-----------------------------------------------------------------------------
    if (document.mainform.prov_start_hhmm.value == "") {
        alert('対象シフトグループの夜勤時間帯が未設定のため計算できません。');
        return;
    }
    ///-----------------------------------------------------------------------------
    //必要人数設定
    ///-----------------------------------------------------------------------------
    document.mainform.del_staff_id.value = "";
    document.mainform.add_staff_id.value = "";
    document.mainform.add_staff_cnt.value = 0;
    document.mainform.comp_flg.value = "1";
    document.mainform.action = "duty_shift_menu.php";
    document.mainform.target = "";
    document.mainform.submit();
}

///-----------------------------------------------------------------------------
//指定日された選択入力可能にする
///-----------------------------------------------------------------------------
function showEdit(start_day, end_day) {
    closeChildWin();
    try {
        //登録権限があるかチェック
        if (document.mainform.create_flg.value == "") {
            return;
        }

        //指定日された選択入力可能にする
        if ((document.mainform.edit_start_day.value == start_day) &&
            (document.mainform.edit_end_day.value == end_day)) {
            //表示のみ
            document.mainform.edit_start_day.value = 0;
            document.mainform.edit_end_day.value = 0;
        } else {
            //選択入力可能
            document.mainform.edit_start_day.value = start_day;
            document.mainform.edit_end_day.value = end_day;
        }
        document.mainform.action = "duty_shift_menu.php";
        document.mainform.target = "";
        //スタンプ
        document.mainform.stamp_ptn.value = stamp_ptn;
        document.mainform.stamp_reason.value = stamp_reason;
        document.mainform.submit();
    }
    catch (err) {
    }
}

///-----------------------------------------------------------------------------
// スタッフ選択画面（ＣＬＯＳＥ）
// 印刷画面（ＣＬＯＳＥ）
// 印刷範囲指定画面（ＣＬＯＳＥ）
// 自動シフト条件設定画面（ＣＬＯＳＥ）
// 過去月コピー条件設定画面（ＣＬＯＳＥ）
///-----------------------------------------------------------------------------
function closeChildWin() {
    try {
        // スタッフ選択画面（ＣＬＯＳＥ）
        if (childwin_staff != null && !childwin_staff.closed) {
            childwin_staff.close();
        }
        childwin_staff = null;

        // 印刷画面（ＣＬＯＳＥ）
        if (childwin_print != null && !childwin_print.closed) {
            childwin_print.close();
        }
        childwin_print = null;

        // 印刷範囲指定画面（ＣＬＯＳＥ）
        if (childwin_print_select != null && !childwin_print_select.closed) {
            childwin_print_select.close();
        }
        childwin_print_select = null;

        // 自動シフト条件設定画面（ＣＬＯＳＥ）
        if (childwin_auto != null && !childwin_auto.closed) {
            childwin_auto.close();
        }
        childwin_auto = null;

        // 過去月コピー条件設定画面（ＣＬＯＳＥ）
        if (childwin_copy != null && !childwin_copy.closed) {
            childwin_copy.close();
        }
        childwin_copy = null;
        // 勤務パターン登録画面（ＣＬＯＳＥ）
        if (childwin_ptn_update != null && !childwin_copy.closed) {
            childwin_ptn_update.close();
        }
        childwin_ptn_update = null;
    }
    catch (err) {
    }
}

///-----------------------------------------------------------------------------
//希望取り込みデータ変更チェック
///-----------------------------------------------------------------------------
function chk_individual(gyo, retu) {
    var wk1 = 'individual_flg_' + gyo + '_' + retu;
    var wk2 = 'atdptn_ptn_id_' + gyo + '_' + retu;
    var individual_flg = "";
    if (document.mainform.elements[wk1]) {
        individual_flg = document.mainform.elements[wk1].value;
    }
    if (individual_flg != "") {
        if (confirm('「勤務希望取込」のデータが変更されます。よろしいですか？')) {
            document.mainform.elements[wk1].value = "";
        } else {
            document.mainform.elements[wk2].selectedIndex = document.mainform.elements[wk1].value;
        }
    }
}

///-----------------------------------------------------------------------------
//メッセージ画面（表示／非表示）
///-----------------------------------------------------------------------------
function showMemo(day, memo) {
    try {
        if (memo == "") {
            return;
        }

        //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
        var msg1 = '<table bgcolor="#dddddd" border="1">\n';
        msg1 += '<tr><td>' + memo + '</td></tr>\n';
        msg1 += '</table>\n';
        showMsg(msg1);
    }
    catch (err) {
    }
}

///-----------------------------------------------------------------------------
//エラー画面「行」（表示／非表示）
///-----------------------------------------------------------------------------
function showErrGyo(idx, memo) {
    try {
        //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
        var data1 = "警告";
        var data2 = memo;

        if (memo != "") {
            var msg1 = '<table bgcolor="#dddddd" border="1">\n';
            msg1 += '<tr><td colspan="2" bgcolor="orange" align=\"center\"><b>' + data1 + '</b></td></tr>\n';
            msg1 += '<tr><td>' + data2 + '</td></tr>\n';
            msg1 += '</table>\n';
            showMsg(msg1);
        }
    }
    catch (err) {
    }
}

///-----------------------------------------------------------------------------
//エラー画面「列」（表示／非表示）
///-----------------------------------------------------------------------------
function showErrRetu(idx, memo) {
    try {
        //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
        var data1 = "警告";
        var data2 = memo;

        if (memo != "") {
            var msg1 = '<table bgcolor="#dddddd" border="1">\n';
            msg1 += '<tr><td colspan="2" bgcolor="orange" align=\"center\"><b>' + data1 + '</b></td></tr>\n';
            msg1 += '<tr><td>' + data2 + '</td></tr>\n';
            msg1 += '</table>\n';
            showMsg(msg1);
        }
    }
    catch (err) {
    }
}

//子画面で更新ボタン押下
function callbackPtnUpdate(plan_hope_flg, emp_idx, day, atdptn_ptn_id, reason_2, copy_cnt) {

    // 指定情報を保存
    document.mainform.elements['last_ptn_id'].value = atdptn_ptn_id;
    document.mainform.elements['last_reason_2'].value = reason_2;
    document.mainform.elements['last_copy_cnt'].value = copy_cnt;

    //勤務希望アラート
    if (!alertHope(emp_idx, day, plan_hope_flg, atdptn_ptn_id.substr(0, 2), copy_cnt)) {
        return;
    }

    //対象日
    wk_day = day;

    //連続回数分繰返し
    for (cnt = 0; cnt < copy_cnt; cnt++) {

        //1日目
        //勤務パターンを設定
        if (plan_hope_flg == '0') {
            var wk1 = 'atdptn_ptn_id_' + emp_idx + '_' + wk_day;
            var wk2 = 'reason_2_' + emp_idx + '_' + wk_day;
            setUpdFlg(emp_idx);
        } else {
            var wk1 = 'rslt_id_' + emp_idx + '_' + wk_day;
            var wk2 = 'rslt_reason_2_' + emp_idx + '_' + wk_day;
            setRsltUpdFlg(emp_idx);
        }

        document.mainform.elements[wk1].value = atdptn_ptn_id;
        document.mainform.elements[wk2].value = reason_2;

        //組合せ設定
        var auto_set_flg = false;
        var auto_set_flg2 = false;
        var auto_set_flg3 = false;
        //2日目
        wk_day++;
        if (wk_day > end_day) {//終了日確認
            break;
        }
        if (wk_day <= end_day) {
            if (plan_hope_flg == '0') {
                var wk1 = 'atdptn_ptn_id_' + emp_idx + '_' + wk_day;
                var wk2 = 'reason_2_' + emp_idx + '_' + wk_day;
            } else {
                var wk1 = 'rslt_id_' + emp_idx + '_' + wk_day;
                var wk2 = 'rslt_reason_2_' + emp_idx + '_' + wk_day;
            }
            //組合せの確認
            for (var i = 0; i < arr_today.length; i++) {
                if (arr_today[i] == atdptn_ptn_id) {
                    document.mainform.elements[wk1].value = arr_nextday[i];
                    document.mainform.elements[wk2].value = '';
                    var flg = (plan_hope_flg == '2') ? 2 : 1;
                    setReason(emp_idx, wk_day, wk_pattern_id, arr_nextday[i], flg);
                    auto_set_flg = true;
                    break;
                }
            }
            if (auto_set_flg) {//組合せ設定できたら対象日を進める
                wk_day++;
                if (wk_day > end_day) {
                    break;
                }
            }
        }
        //3日目
        if (auto_set_flg == true && wk_day <= end_day) {
            if (plan_hope_flg == '0') {
                var wk2 = 'atdptn_ptn_id_' + emp_idx + '_' + (wk_day - 1);
                var wk3 = 'atdptn_ptn_id_' + emp_idx + '_' + wk_day;
                var wk4 = 'reason_2_' + emp_idx + '_' + wk_day;
            } else {
                var wk2 = 'rslt_id_' + emp_idx + '_' + (wk_day - 1);
                var wk3 = 'rslt_id_' + emp_idx + '_' + wk_day;
                var wk4 = 'rslt_reason_2_' + emp_idx + '_' + wk_day;
            }
            var yokuyoku_ptn_id = document.mainform.elements[wk2].value;

            //組合せの確認
            for (var i = 0; i < arr_today.length; i++) {
                if (arr_today[i] == yokuyoku_ptn_id) {
                    document.mainform.elements[wk3].value = arr_nextday[i];
                    document.mainform.elements[wk4].value = '';
                    var flg = (plan_hope_flg == '2') ? 2 : 1;
                    setReason(emp_idx, wk_day, wk_pattern_id, arr_nextday[i], flg);
                    auto_set_flg2 = true;
                    break;
                }
            }
            if (auto_set_flg2) {//組合せ設定できたら対象日を進める
                wk_day++;
                if (wk_day > end_day) {
                    break;
                }
            }
        }
        //4日目
        if (auto_set_flg2 == true && wk_day <= end_day) {
            if (plan_hope_flg == '0') {
                var wk2 = 'atdptn_ptn_id_' + emp_idx + '_' + (wk_day - 1);
                var wk3 = 'atdptn_ptn_id_' + emp_idx + '_' + wk_day;
                var wk4 = 'reason_2_' + emp_idx + '_' + wk_day;
            } else {
                var wk2 = 'rslt_id_' + emp_idx + '_' + (wk_day - 1);
                var wk3 = 'rslt_id_' + emp_idx + '_' + wk_day;
                var wk4 = 'rslt_reason_2_' + emp_idx + '_' + wk_day;
            }
            var yokuyokuyoku_ptn_id = document.mainform.elements[wk2].value;
            //組合せの確認
            for (var i = 0; i < arr_today.length; i++) {
                if (arr_today[i] == yokuyokuyoku_ptn_id) {
                    document.mainform.elements[wk3].value = arr_nextday[i];
                    document.mainform.elements[wk4].value = '';
                    var flg = (plan_hope_flg == '2') ? 2 : 1;
                    setReason(emp_idx, wk_day, wk_pattern_id, arr_nextday[i], flg);
                    auto_set_flg3 = true;
                    break;
                }
            }
            if (auto_set_flg3) {//組合せ設定できたら対象日を進める
                wk_day++;
                if (wk_day > end_day) {
                    break;
                }
            }
        }

    }
    document.mainform.emp_idx.value = emp_idx;
    //再表示
    showEdit(document.mainform.edit_start_day.value, document.mainform.edit_end_day.value);
}

function setReason(pos, day, tmcd_group_id, atdptn_id, flg) {

    if (flg == 1) {
        item_name = 'reason_2_' + pos + '_' + day;
    } else {
        item_name = 'rslt_reason_2_' + pos + '_' + day;
    }
    if (atdptn_id == '0000') {
        document.mainform[item_name].value = '';
        return '';
    }
    wk_atdptn_id = parseInt(atdptn_id.substring(0, 2), 10);
    wk_id = tmcd_group_id + '_' + wk_atdptn_id;

    if (arr_atdptn_reason[wk_id] == undefined) {
        document.mainform[item_name].value = '';
        return '';
    }
    document.mainform[item_name].value = arr_atdptn_reason[wk_id];
    return arr_atdptn_reason[wk_id];
}

function setUpdFlg(emp_idx) {
    var wk = 'upd_flg[' + emp_idx + ']';
    document.mainform.elements[wk].value = 1;
}
function setRsltUpdFlg(emp_idx) {
    var wk = 'rslt_upd_flg[' + emp_idx + ']';
    document.mainform.elements[wk].value = 1;
}
function setCmtUpdFlg(emp_idx) {
    var wk = 'cmt_upd_flg[' + emp_idx + ']';
    document.mainform.elements[wk].value = 1;
}
function update_no() {
    if (document.mainform.add_staff_id_all.value != '' ||
        document.mainform.delete_id_cnt.value > 0) {
        alert('職員の追加や削除をする場合は、先に登録をしてください。更新ボタンでは表示番号とチームの更新のみ行います。');
        return;
    }
    document.mainform.action = "duty_shift_menu_no_update_exe.php";
    document.mainform.target = "";
    document.mainform.submit();
}

function setStampPtn(ptn, reason, targetid) {
    if (stamp_ptn != '') {
        wk_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn + '_' + stamp_reason;
        cel = document.getElementById("stamp_" + wk_id);
    }
    wk_id = (ptn != '10') ? ptn : ptn + '_' + reason;

    cel = document.getElementById("stamp_" + wk_id);

    stamp_ptn = ptn;
    stamp_reason = reason;

    //未設定
    if (stamp_ptn == '') {
        var wk_back_color = '#ffffff';
        var wk_ptn_name = '未設定';
        var wk_font_color = '#000000';
    } else {
        wk_ptn_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn + '_' + stamp_reason;
        var wk_back_color = arr_bg_color[wk_ptn_id];
        var wk_ptn_name = arr_ptn_name[wk_ptn_id];
        var wk_font_color = arr_font_color[wk_ptn_id];
    }

    //------------------------------------------------------------------------------------------------------
    //キー入力時の[フローティング勤務シフト]クリック処理
    //------------------------------------------------------------------------------------------------------
    if (document.mainform.stamp_flg && document.mainform.stamp_flg.value == "") {

        var target_element = document.getElementById("listPosition").value;
        if (target_element == "") {
            alert('設定位置をクリックしてください。');
            return;
        }

        target_element = target_element.replace('data', '');

        var cutPosition = target_element.indexOf("_");
        var emp_idx = target_element.substr(0, cutPosition);
        var day = target_element.substr(cutPosition + 1);

        emp_idx = emp_idx - 1;

        //非スタンプ時だが、画面への反映やデータのhiddenセット等 スタンプ処理とやることは同じなのでスタンプ処理関数(setStamp)へ飛ばす
        var plan_hope_flg = document.getElementById('listPositionPlanHope').value;

        //二行表示の確認(希望・コメント・実績 非選択時)
        if (document.mainform.plan_results_flg.value == 0
            && document.mainform.plan_hope_flg.value == 0
            && document.mainform.plan_duty_flg.value == 0
            && document.mainform.plan_comment_flg.value == 0) {

            //一行表示
            setStamp(emp_idx, day, plan_hope_flg);
        } else {
            //二行表示
            setStamp((emp_idx - (emp_idx % 2)) / 2, day, plan_hope_flg);
        }
    }

    //------------------------------------------------------------------------------------------------------
    //スタンプ時
    //------------------------------------------------------------------------------------------------------
    //前回クリックされた勤務シフトは元の表示に戻す
    cell = document.getElementById('stmpPosition');
    var stmpPositionID = cell.value;
    //今回クリック情報を保持
    cell.value = targetid;

    //前回クリック元の表示に戻す
    if (stmpPositionID != '') {
        cell = document.getElementById(stmpPositionID);
        cell.style.fontWeight = 'normal';
        cell.style.fontStyle = 'normal';
    }

    //今回クリックされた箇所は太字
    cell = document.getElementById(targetid);
    cell.style.fontWeight = 'bold';
    cell.style.fontStyle = 'italic';

    //連続を1に設定
    document.getElementById("continuous_cnt").value = "1";
    cell = document.getElementById('stamp_cel');
    cell.style.backgroundColor = wk_back_color;
    cell.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="' + wk_font_color + '"><b>' + wk_ptn_name + '</b></font>';
}

//デフォルト背景色を取得する
function getDefaultBackColor(day) {
    //曜日欄
    var cell = document.getElementById('day_of_week' + day);

    //曜日欄の現設定背景色情報を取得
    var bgcolor;
    if (navigator.userAgent.indexOf("MSIE") >= 0) {
        bgcolor = cell.currentStyle.backgroundColor;
    } else {
        bgcolor = document.defaultView.getComputedStyle(cell, null).getPropertyValue("background-color");
    }
    return bgcolor;
}

//勤務希望アラート
function alertHope(emp_idx, day, plan_hope_flg, ptn, copy_cnt) {
    var wk_day = parseInt(day);
    var wk_id = 'staff_id[' + emp_idx + ']';
    var emp_id = 'e' + document.mainform.elements[wk_id].value;

    if (plan_hope_flg == '0') {
        var iv_days = wk_day;
        var iv_ptn = parseInt(ptn);
        var iv_flg = false;
        var iv_msg = "";
        for (var cnt = 0; cnt < copy_cnt; cnt++) {
            var pi = plan_individual[emp_id] ? plan_individual[emp_id]['d' + iv_days] : null;
            if (pi && iv_ptn != parseInt(pi['id'])) {
                iv_flg = true;
                iv_msg += pi['date'] + ' : ' + pi['nm'] + '\n';
            }
            iv_days++;

            //組み合わせ
            while (1) {
                if (!arr_seqptn[iv_ptn]) {
                    break;
                }
                iv_ptn = arr_seqptn[iv_ptn];
                var pi = plan_individual[emp_id] ? plan_individual[emp_id]['d' + iv_days] : null;
                if (pi && iv_ptn != pi['id']) {
                    iv_flg = true;
                    iv_msg += pi['date'] + ' : ' + pi['nm'] + '\n';
                }
                iv_days++;
            }
        }
        if (iv_flg) {
            if (!confirm('勤務希望と異なるシフトが設定されますがよろしいですか？\n' + iv_msg)) {
                return false;
            }
        }
    }
    return true;
}

//職員設定取込
function staffSetAdd() {
    if (confirm('シフト作成後に職員設定へ追加されている職員を取込みます。よろしいですか？')) {
        document.mainform.staff_set_add_flg.value = '1';
        document.mainform.action = "duty_shift_menu.php";
        document.mainform.target = "";
        document.mainform.submit();
    }
}

// 空白セルに一括割り当て
function setPatternToBlancCells(pattern, startDay, endDay) {
    var wk = pattern.split(':');
    var id = wk[0];    // 記号ID
    var str = wk[1];    // 記号
    var fontcolor = wk[2];    // 文字色
    var bgcolor = wk[3];    // 背景色

    var dataElements = $('.atdptn');    // class="atdptn"の要素(hidden)を取得
    var start = Number(startDay);       // 設定開始日
    var end = Number(endDay);         // 設定終了日

    var day;        // 日付
    var dataIdx;    // データインデックス
    var cell;       // セル要素
    var cellIdx;    // セル要素のインデックス

    // hidden要素をループして、記号と値をセットする
    var length = dataElements.length;
    for (var i = 0; i < length; i++) {
        wk = dataElements[i].name.split('_');
        dataIdx = wk[3];
        day = wk[4];
        if (Number(day) < start || end < Number(day)) {
            // 日付が指定範囲外ならばスキップ
            continue;
        }

        ////////// hidden要素に記号IDをセット
        if (dataElements[i].value != '0000') {    // 空白は記号IDが'0000'
            // 空白でなければスキップ
            continue;
        }
        dataElements[i].value = id;

        ////////// セルに記号をセット
        // 要素のインデックスを取得
        if (Number(document.mainform.plan_results_flg.value) != 0    // 予実績表示
            || Number(document.mainform.plan_hope_flg.value) != 0    // 勤務希望表示
            || Number(document.mainform.plan_duty_flg.value) != 0    // 当直表示
            || Number(document.mainform.plan_comment_flg.value) != 0    // コメント表示
            ) {
            // 2行表示
            cellIdx = Number(dataIdx) * 2 + 1;
        } else {
            // 1行表示
            cellIdx = Number(dataIdx) + 1;
        }

        // 処理中のhidden要素に対応するセル要素を取得
        cell = $('#' + 'data' + cellIdx + '_' + day).get()[0];
        if (!cell) {
            continue;    // 取得に失敗したらスキップ
        }

        // 文字をセット
        cell.innerHTML = str;          // 文字
        cell.style.color = fontcolor;    // 文字色

        // 背景色をセット
        if (bgcolor == '#ffffff') {    // 背景色の指定なし
            // デフォルト背景色をセットする
            cell.style.backgroundColor = getDefaultBackColor(day);
        } else {                       // 背景色の指定あり
            cell.style.backgroundColor = bgcolor;
        }

        setUpdFlg(dataIdx);    // 更新フラグを立てる
    }
}

//空白セルに一括割り当て （「平日／土／日／祝日に指定の勤務を一括設定する）20131225
function setPatternToBlancCellsWeekType(pattern, startDay, endDay, week1, week2, week3, week4) {

  var wk = pattern.split(':');
  var id = wk[0];           // 記号ID
  var str = wk[1];          // 記号
  var fontcolor = wk[2];    // 文字色
  var bgcolor = wk[3];      // 背景色
  var flag;

  var dataElements = $('.atdptn');    // class="atdptn"の要素(hidden)を取得
  var start = Number(startDay);       // 設定開始日
  var end = Number(endDay);             // 設定終了日

  var day;        // 日付
  var dataIdx;    // データインデックス
  var cell;       // セル要素
  var cellIdx;    // セル要素のインデックス

  // hidden要素をループして、記号と値をセットする
  var length = dataElements.length;

  for (var i = 0; i < length; i++) {
      wk = dataElements[i].name.split('_');
      dataIdx = wk[3];
      day = wk[4];

      if (Number(day) < start || end < Number(day)) {
          // 日付が指定範囲外ならばスキップ
          continue;
      }
      ////////// hidden要素に記号IDをセット
      if (dataElements[i].value != '0000') {    // 空白は記号IDが'0000'
          // 空白でなければスキップ
          continue;
      }

      flag = false;

      //平日
      if (arr_type[day] == 1 && week1 == 't') {
          flag = true;
      }

      if (arr_type[day] == 6 && week2 == 't') {
          flag = true;
      }
      if (arr_type[day] == 7 && week3 == 't') {
          flag = true;
      }
      // 祝日
      if (arr_type[day] == 8 && week4 == 't') {
          flag = true;
      }

      // 設定対象ではない
      if (flag != true)  continue;

      dataElements[i].value = id;

      ////////// セルに記号をセット
      // 要素のインデックスを取得
      if (Number(document.mainform.plan_results_flg.value) != 0    // 予実績表示
          || Number(document.mainform.plan_hope_flg.value) != 0    // 勤務希望表示
          || Number(document.mainform.plan_duty_flg.value) != 0    // 当直表示
          || Number(document.mainform.plan_comment_flg.value) != 0    // コメント表示
          ) {
          // 2行表示
          cellIdx = Number(dataIdx) * 2 + 1;
      } else {
          // 1行表示
          cellIdx = Number(dataIdx) + 1;
      }

      // 処理中のhidden要素に対応するセル要素を取得
      cell = $('#' + 'data' + cellIdx + '_' + day).get()[0];
      if (!cell) {
          continue;    // 取得に失敗したらスキップ
      }

      // 文字をセット
      cell.innerHTML = str;          // 文字
      cell.style.color = fontcolor;    // 文字色

      // 背景色をセット
      if (bgcolor == '#ffffff') {    // 背景色の指定なし
          // デフォルト背景色をセットする
          cell.style.backgroundColor = getDefaultBackColor(day);
      } else {                       // 背景色の指定あり
          cell.style.backgroundColor = bgcolor;
      }

      setUpdFlg(dataIdx);    // 更新フラグを立てる
  }

}

function start_auto_session_update()
{
    //10分(600*1000ms)後に開始
    setTimeout(auto_sesson_update, 600000);
}
function auto_sesson_update()
{
    //セッション更新
    document.session_update_form.submit();

    //10分(600*1000ms)後に再呼び出し
    setTimeout(auto_sesson_update, 600000);
}

/**
 * IEでconsole.logのエラーを出なくする
 **/
if (!('console' in window)) {
    window.console = {};
    window.console.log = function(str) {
        return str;
    };
}

/**
 * 表示期間に以前週表示
 **/
function show_before_week(wk_index) {
    document.mainform.week_index.value = wk_index;
    document.mainform.action = "duty_shift_menu.php?session=<?=$session?>";
    document.mainform.target = "";
    document.mainform.submit();
}

/**
 * 看護職員表・看護補助者表作成
 */
function makeExcelNurseShift(night_start_day, standard_id, report_kbn, type) {
    var action = '';
    if (type == 1) {
        action = 'duty_shift_report_nurse1_excel.php';
    } else {
        action = 'duty_shift_report_nurse2_excel.php';
    }

    $('<form></form>')
        .attr('method', 'post')
        .attr('action', action)
        .append('<input type="hidden" name="session" value="' + $("input[name='session']").val() + '">')
        .append('<input type="hidden" name="duty_yyyy" value="' + $("#duty_yyyy").val() + '">')
        .append('<input type="hidden" name="duty_mm" value="' + $("input[name='duty_mm']").val() + '">')
        .append('<input type="hidden" name="date_y1" value="' + $("#date_y1").val() + '">')
        .append('<input type="hidden" name="date_m1" value="' + $("#date_m1").val() + '">')
        .append('<input type="hidden" name="date_d1" value="' + $("#date_d1").val() + '">')
        .append('<input type="hidden" name="recomputation_flg" value="' + $("input[name='recomputation_flg']:checked").val() + '">')
        .append('<input type="hidden" name="draft_reg_flg" value="' + $("input[name='draft_reg_flg']").val() + '">')
        .append('<input type="hidden" name="night_start_day" value="' + night_start_day + '">')
        .append('<input type="hidden" name="standard_id" value="' + standard_id + '">')
        .append('<input type="hidden" name="group_id" value="' + $("#group_id").val() + '">')
        .append('<input type="hidden" name="report_kbn" value="' + report_kbn + '">')
        .append('<input type="hidden" name="plan_result_flag" value="3">')
        .appendTo(document.body)
        .submit();
}

/**
 * 連続勤務or週間勤務時間数チェック機能警告画面 20131107
 **/
function open_warn_msg() {
    wx = 700;
    wy = 600;
    base_left = (screen.width  - wx) / 2;
    base = (screen.height - wy) / 2;
    closeChildWin();
    childwin_print = window.open('', 'workdays', 'left=' + base_left + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
    document.mainform.action = "duty_shift_menu_check_workdays.php";
    document.mainform.target = "workdays";
    childwin_print.focus();
    document.mainform.submit();
}
