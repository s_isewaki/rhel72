/**
 * 列の追加
 */
function add_col() {
    var id = cols.length + 1;
    $('#count thead td.ptn').before('<td><input type="text" name="name[' + id + ']" value="" size="10" class="jpn" /></td>');

    $('#count tbody tr#count_row td.ptn').before('<td class="center"><input type="checkbox" name="count_row[' + id + ']" value="1" checked="checked" /></td>');
    $('#count tbody tr#count_col td.ptn').before('<td class="center"><input type="checkbox" name="count_col[' + id + ']" value="1" checked="checked" /></td>');
    $('#count tbody tr#count_job td.ptn').before(['<td class="center"><div><select class="job" name="count_job[', id, '][]"><option value="">-</option>',
        pulldown_job, '</select> <a href="javascript:void(0);" onclick="remove_job(this);">[X]</a></div><button type="button" onclick="add_job(this, ', "'",
        id, "'", ');">職種追加</button></td>'].join(''));
    $('#count tfoot td.ptn').before('<td><button type="button" onclick="remove_col(this)">列の削除</button></td>');

    $.each(rows, function() {
        $('#count tbody tr#' + this + ' td.ptn').before('<td class="center"><input type="text" name="value[p' + this + '][' + id + ']" value="" class="num" size="6" /></td>');
    });
    cols.push(id);
}

/**
 * 列の削除
 */
function remove_col(obj) {
    var idx = $(obj).parent().index();
    $('#count thead tr td').eq(idx).remove();
    $('#count tfoot tr td').eq(idx).remove();
    $('#count tbody tr').each(function() {
        $(this).children('td').eq(idx).remove();
    });
    cols.splice(idx - 1, 1);
}

/**
 * 行の追加
 */
function add_row() {
    var val = $('#row_pattern').val();
    if ($('#' + val).attr('id')) {
        alert('その勤務パターンは登録済みです。');
        return false;
    }
    var tr = '<tr id="' + val + '"><td class="center">' + $('#row_pattern option:selected').text() + '</td>';
    $.each(cols, function() {
        tr = tr + '<td class="center"><input class="num" type="text" name="value[p' + val + '][' + this + ']" value="" size="6" /></td>';
    });
    tr = tr + '<td class="ptn"><button type="button" onclick="remove_row(this)">&lt;&lt;削除</button></td></tr>';
    $('#count tbody').append(tr);
    rows.push($('#row_pattern').val());
}

/**
 * 行の削除
 */
function remove_row(obj) {
    var idx = $(obj).parent().parent().index();
    $(obj).parent().parent().remove();
    rows.splice(idx - 2, 1);
}

/**
 * 職種の追加
 */
function add_job(obj, id) {
    var tr = ['<div><select class="job" name="count_job[', id, '][]"><option value="">-</option>',
        pulldown_job, '</select> <a href="javascript:void(0);" onclick="remove_job(this);">[X]</a></div>'];
    $(obj).before(tr.join(''));
}

/**
 * 職種の削除
 */
function remove_job(obj) {
    $(obj).parent().remove();
}

$(function() {
    /**
     * マウスオーバーハイライト
     */
    $('td.input').hover(
            function() {
                $(this).parent().find('td').addClass('hover');
            },
            function() {
                $(this).parent().find('td').removeClass('hover');
            }
    );

    /**
     * 入力値チェック
     */
    $('#signform').submit(function() {
        var flg = true;

        $('input.num').each(function(idx, e) {
            if ($(e).val() !== '' && !$.isNumeric($(e).val())) {
                $(e).css({'backgroundColor': '#f00'});
                flg = false;
            }
        });

        if (!flg) {
            alert('集計値に数字以外が含まれています。');
            return false;
        }
        return true;
    });
});
