///-----------------------------------------------------------------------------
// resizeIframeEx
//------------------------------------------------------------------------------
var viewAreaHgt = 0; // 表示領域の高さ
var sclContHgt = 0; // スクロールさせるコンテンツ(#wrapCont)の高さ
var sclContTopRange = 0; // #wrapContの左上の座標
var tblWidth = 0;
var elmFloatingpage = "";
var movedCountWindow = false;


/**
 * 記号の変更を検知してAjax処理を行う
 * @param {str} data セルのid
 * @param {str} idx セルの列番地
 * @param {str} day セルの行番地
 * @param {str} plan_hope_flg 希望表示しているかフラグ
 */
function changeCell(data, idx, day, plan_hope_flg) {
    // 集計設定Aでなければ処理しない
    if (change_switch !== 2) {
        return;
    }
    // IE6なら処理しない
    if (_ua.ltIE6) {
        return;
    }

    var ptn_base = 'atdptn_ptn_id_';
    var rsn_base = 'reason_2_';

    // 日別
    var pidDayArray = [];
    var ptnDayArray = [];
    var rsnDayArray = [];
    for (var d = 1; d <= document.mainform.elements['day_cnt'].value; d++) {
        var pid1 = 'pattern_id_' + idx + '_' + d;
        var ptn1 = ptn_base + idx + '_' + d;
        var rsn1 = rsn_base + idx + '_' + d;

        pidDayArray.push(document.mainform.elements[pid1].value);
        ptnDayArray.push(document.mainform.elements[ptn1].value);
        rsnDayArray.push(document.mainform.elements[rsn1].value);
    }

    // 職員別
    var pidEmpArray = [];
    var ptnEmpArray = [];
    var rsnEmpArray = [];
    var assEmpArray = [];
    var jobEmpArray = [];
    for (var e = 0; e < document.mainform.elements['data_cnt'].value; e++) {
        var pid2 = 'pattern_id_' + e + '_' + day;
        var ptn2 = ptn_base + e + '_' + day;
        var rsn2 = rsn_base + e + '_' + day;
        var ass = document.mainform.elements['assist_group_' + e].value.split(',');

        pidEmpArray.push(document.mainform.elements[pid2].value);
        ptnEmpArray.push(document.mainform.elements[ptn2].value);
        rsnEmpArray.push(document.mainform.elements[rsn2].value);
        assEmpArray.push(ass[day - 1]);
        jobEmpArray.push(document.mainform.elements['job_id[' + e + ']'].value);
    }

    $.ajax({
        url: 'duty_shift_sign_count_ajax.php',
        type: 'post',
        dataType: 'json',
        data: {
            mode: 'plan',
            data: data,
            plan_hope_flg: plan_hope_flg,
            session: document.mainform.elements['session'].value,
            group_id: document.mainform.elements['group_id'].value,
            pattern_id: document.mainform.elements['pattern_id'].value,
            day_cnt: document.mainform.elements['day_cnt'].value,
            data_cnt: document.mainform.elements['data_cnt'].value,
            start_date: document.mainform.elements['duty_start_date'].value,
            end_date: document.mainform.elements['duty_end_date'].value,
            pid_day: pidDayArray,
            ptn_day: ptnDayArray,
            rsn_day: rsnDayArray,
            ass_day: document.mainform.elements['assist_group_' + idx].value.split(','),
            pid_emp: pidEmpArray,
            ptn_emp: ptnEmpArray,
            rsn_emp: rsnEmpArray,
            ass_emp: assEmpArray,
            job_emp: jobEmpArray
        },
        success: changedCount
    });
}

/**
 * Ajaxの応答を受けて、集計値の変更を行う
 */
function changedCount(data) {
    $.each(data, function(id, value) {
        $(id).text(value);
    });
    changedSummary();
}

/**
 * 集計値の変更を受けて、総計の変更を行う
 */
function changedSummary() {
    // 個人日数(予定)
    var total_0 = [];
    $.each($('td.sum_gyo_0'), function() {
        var id = $(this).attr('id').split('_');
        if (!total_0[id[2]]) {
            total_0[id[2]] = 0;
        }
        total_0[id[2]] += parseFloat($(this).html());
    })
    $.each(total_0, function(k, v) {
        $('#day_total_0_' + k).text(v);
    })

    // 個人日数(実績・希望)
    var total_1 = [];
    $.each($('td.sum_gyo_1'), function() {
        var id = $(this).attr('id').split('_');
        if (!total_1[id[2]]) {
            total_1[id[2]] = 0;
        }
        total_1[id[2]] += parseFloat($(this).html());
    })
    $.each(total_1, function(k, v) {
        $('#day_total_1_' + k).text(v);
    })

    // 項目別日数
    var total_e = [];
    $.each($('td.sum_retu'), function() {
        var id = $(this).attr('id').split('_');
        var idx = id[0].slice(7);
        if (!total_e[idx]) {
            total_e[idx] = 0;
        }
        total_e[idx] += parseFloat($(this).html());
    })
    $.each(total_e, function(k, v) {
        $('#emp_total_' + k).html(v + '&nbsp;');
    })
}

$(function() {

    ///-----------------------------------------------------
    // スタンプ
    //------------------------------------------------------
    $("a.open").click(function() {
        $("#floatWindowStp").fadeIn("fast");
        return false;
    });

    $("#floatWindowStp a.close").click(function() {
        $("#floatWindowStp").fadeOut("fast");
        return false;
    });

    $("#floatWindowStp dl dt").mousedown(function(e) {
        e.preventDefault();
        $("#floatWindowStp")
            .data("clickPointX", e.pageX - $("#floatWindowStp").offset().left)
            .data("clickPointY", e.pageY - $("#floatWindowStp").offset().top);

        $(document).mousemove(function(e) {
            e.preventDefault();
            $("#floatWindowStp").css({
                top: e.pageY - $("#floatWindowStp").data("clickPointY") + "px",
                left: e.pageX - $("#floatWindowStp").data("clickPointX") + "px"
            });
        });
    })
        .mouseup(function() {
        $(document).unbind("mousemove");
    });

});

/**
 * ウインドウ非表示
 */
function floatWindowHide(a, b) {
    if (a == '0') {
        document.getElementById('floatWindow' + b).style.display = "none";
    }
    else {
        document.getElementById('floatWindow' + b).style.display = "";
        //初期表示位置
        document.getElementById('floatWindow' + b).style.top = 230; //60
        document.getElementById('floatWindow' + b).style.left = 350; //390
    }
}

/**
 * IEでconsole.logのエラーを出なくする
 **/
if (!('console' in window)) {
    window.console = {};
    window.console.log = function(str) {
        return str;
    };
}


