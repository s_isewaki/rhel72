	
	
/**
* selectItem 全リストら指定された項目を選択済みボックスへコピーする
* @param srcname　全リスト
* @param dstname　選択したい項目
**/	
function selectItem(srcname, dstname) 
{
	srcobj = document.getElementById(srcname);
	dstobj = document.getElementById(dstname);
	for (var i = 0, j = srcobj.options.length; i < j; i++)
	{
		if (srcobj.options[i].selected) 
		{
			var id = srcobj.options[i].value;
			var name = srcobj.options[i].text;

			var in_group = false;
			for (var k = 0, l = dstobj.options.length; k < l; k++) 
			{
				if (dstobj.options[k].value == id) 
				{
					in_group = true;
					break;
				}
			}

			if (!in_group) 
			{
				addOption(dstobj, id, name, '');
			}
		}
	}
}

/**
* addOption 実データを設定する
* @param box　指定されたリストボックス
* @param value　指定されたリストボックスに設定する値
* @param text　指定されたリストボックスの名前
* @param selected　選択されている項目
**/	
function addOption(box, value, text, selected) 
{
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) 
	{
		box.selectedIndex = box.options.length -1;
		return;
	}
}


/**
* deleteItem 選択済みボックスから項目を削除する
* @param objname　指定されたリストボックスの名前（ID）
**/
function deleteItem(objname) 
{
	obj = document.getElementById(objname);
	for (var i = obj.options.length - 1; i >= 0; i--) 
	{
		if (obj.options[i].selected) 
		{
			obj.options[i] = null;
		}
	}
}

/**
* updateSetting 指定されたデータを登録するためにhiddenエリアにデータを設定し、submitする
**/	
function updateSetting() 
{

	//0001(夜勤明)設定
	document.mainform.job_list1.value = '';
	for (var i = 0, j = document.mainform.tgt_job1.options.length; i < j; i++) 
	{
		if (i > 0) 
		{
			document.mainform.job_list1.value += ',';
		}
		document.mainform.job_list1.value += document.mainform.tgt_job1.options[i].value;
	}

	//0002(日勤)設定
	document.mainform.job_list2.value = '';
	for (var i = 0, j = document.mainform.tgt_job2.options.length; i < j; i++) 
	{
		if (i > 0) 
		{
			document.mainform.job_list2.value += ',';
		}
		document.mainform.job_list2.value += document.mainform.tgt_job2.options[i].value;
	}

	//0003(半勤)設定
	document.mainform.job_list3.value = '';
	for (var i = 0, j = document.mainform.tgt_job3.options.length; i < j; i++) 
	{
		if (i > 0) 
		{
			document.mainform.job_list3.value += ',';
		}
		document.mainform.job_list3.value += document.mainform.tgt_job3.options[i].value;
	}

	//0004(夜勤入)設定
	document.mainform.job_list4.value = '';
	for (var i = 0, j = document.mainform.tgt_job4.options.length; i < j; i++) 
	{
		if (i > 0) 
		{
			document.mainform.job_list4.value += ',';
		}
			document.mainform.job_list4.value += document.mainform.tgt_job4.options[i].value;
		}

	//0005(公休)設定
	document.mainform.job_list5.value = '';
	for (var i = 0, j = document.mainform.tgt_job5.options.length; i < j; i++) 
	{
		if (i > 0) 
		{
			document.mainform.job_list5.value += ',';
    	}
		document.mainform.job_list5.value += document.mainform.tgt_job5.options[i].value;
	}

	//0006(年休)設定
	document.mainform.job_list6.value = '';
	for (var i = 0, j = document.mainform.tgt_job6.options.length; i < j; i++) 
	{
		if (i > 0) 
		{
			document.mainform.job_list6.value += ',';
    	}
		document.mainform.job_list6.value += document.mainform.tgt_job6.options[i].value;
	}

	document.mainform.postback.value = "1";
	document.mainform.action = "duty_shift_diary_shift_pattern.php";
	document.mainform.submit();
}
	


