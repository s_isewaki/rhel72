///-----------------------------------------------------------------------------
// resizeIframeEx
//------------------------------------------------------------------------------
var viewAreaHgt = 0; // 表示領域の高さ
var sclContHgt = 0; // スクロールさせるコンテンツ(#wrapCont)の高さ
var sclContTopRange = 0; // #wrapContの左上の座標
var tblWidth = 0;
var elmFloatingpage = "";
var movedCountWindow = false;

/**
 * iframe表示のときの勤務表リサイズ処理
 */
function resizeIframeEx() {
    try {
        elmFloatingpage = parent.document.getElementById('floatingpage');

        // 表示領域の高さ
        viewAreaHgt = parent.document.body.clientHeight;

        // スクロール領域のY座標
        sclContTopRange = $('#region').offset().top + 20;

        // iframeに新しい高さを設定 (表示領域 - 全体のヘッダー)
        var iframeHgt = viewAreaHgt - 110;
        elmFloatingpage.style.height = iframeHgt + 'px';
        parent.callSideMenu = false;

        // 集計行の位置を動かしていなければ、リサイズする
        if (movedCountWindow === false) {
            // スクロール領域の高さ
            sclContHgt = iframeHgt - sclContTopRange;

            if (count_row === 0) {
                if (sclContHgt < $('#data').height() + $('#countWindow').height()) {
                    var height = sclContHgt - $('#countWindow').height();
                    $('#region').height((height > 22 ? height : 22) + 'px');
                }
            }
            else {
                var rows = $('#data').find('tr');

                // テーブル行数が設定行数を超えている場合
                if (rows.length > count_row) {
                    var count_pos = rows.eq(count_row).offset().top - $('#region').offset().top;
                    $('#region').height((count_pos > 22 ? count_pos : 22) + 'px');
                }
            }

            // スクロールの位置調整
            $('#region').width(data.offsetWidth + 20);
            $('#outCountWindowBar').width(data.offsetWidth + 'px');
        }
    }
    catch (e) {
        alert(e);
    }
}

/**
 * セッションタイムアウト防止
 */
function start_auto_session_update() {
    //10分(600*1000ms)後に開始
    setTimeout(auto_sesson_update, 600000);
}
function auto_sesson_update() {
    //セッション更新
    document.session_update_form.submit();

    //10分(600*1000ms)後に再呼び出し
    setTimeout(auto_sesson_update, 600000);
}

/**
 * 記号の変更を検知してAjax処理を行う
 * @param {str} data セルのid
 * @param {str} idx セルの列番地
 * @param {str} day セルの行番地
 * @param {str} plan_hope_flg 希望表示しているかフラグ
 */
function changeCell(data, idx, day, plan_hope_flg) {
    // 集計設定Aでなければ処理しない
    if (change_switch !== 2) {
        return;
    }
    // IE6なら処理しない
    if (_ua.ltIE6) {
        return;
    }

    var ptn_base = 'rslt_id_';
    var rsn_base = 'rslt_reason_2_';

    // 日別
    var pidDayArray = [];
    var ptnDayArray = [];
    var rsnDayArray = [];
    for (var d = 1; d <= document.mainform.elements['day_cnt'].value; d++) {
        var pid1 = 'rslt_pattern_id_' + idx + '_' + d;
        var ptn1 = ptn_base + idx + '_' + d;
        var rsn1 = rsn_base + idx + '_' + d;

        pidDayArray.push(document.mainform.elements[pid1].value);
        ptnDayArray.push(document.mainform.elements[ptn1].value);
        rsnDayArray.push(document.mainform.elements[rsn1].value);
    }

    // 職員別
    var pidEmpArray = [];
    var ptnEmpArray = [];
    var rsnEmpArray = [];
    var assEmpArray = [];
    var jobEmpArray = [];
    for (var e = 0; e < document.mainform.elements['data_cnt'].value; e++) {
        var pid2 = 'rslt_pattern_id_' + e + '_' + day;
        var ptn2 = ptn_base + e + '_' + day;
        var rsn2 = rsn_base + e + '_' + day;
        var ass = document.mainform.elements['assist_group_' + e].value.split(',');

        pidEmpArray.push(document.mainform.elements[pid2].value);
        ptnEmpArray.push(document.mainform.elements[ptn2].value);
        rsnEmpArray.push(document.mainform.elements[rsn2].value);
        assEmpArray.push(ass[day - 1]);
        jobEmpArray.push(document.mainform.elements['job_id[' + e + ']'].value);
    }

    $.ajax({
        url: 'duty_shift_sign_count_ajax.php',
        type: 'post',
        dataType: 'json',
        data: {
            mode: 'result',
            data: data,
            plan_hope_flg: plan_hope_flg,
            session: document.mainform.elements['session'].value,
            group_id: document.mainform.elements['group_id'].value,
            pattern_id: document.mainform.elements['pattern_id'].value,
            day_cnt: document.mainform.elements['day_cnt'].value,
            data_cnt: document.mainform.elements['data_cnt'].value,
            start_date: document.mainform.elements['duty_start_date'].value,
            end_date: document.mainform.elements['duty_end_date'].value,
            pid_day: pidDayArray,
            ptn_day: ptnDayArray,
            rsn_day: rsnDayArray,
            ass_day: document.mainform.elements['assist_group_' + idx].value.split(','),
            pid_emp: pidEmpArray,
            ptn_emp: ptnEmpArray,
            rsn_emp: rsnEmpArray,
            ass_emp: assEmpArray,
            job_emp: jobEmpArray
        },
        success: changedCount
    });
}

/**
 * Ajaxの応答を受けて、集計値の変更を行う
 */
function changedCount(data) {
    $.each(data, function(id, value) {
        $(id).text(value);
    });
    changedSummary();
    var legal_hol_idx = $('input:hidden[name="legal_hol_idx"]').val();
    if (legal_hol_idx >= 0) {
        $.each(data, function(id, value) {
            changeHolRemain(id, legal_hol_idx)
        });
    }
}

/**
 * 集計値の変更を受けて、総計の変更を行う
 */
function changedSummary() {
    // 個人日数(予定)
    var total_0 = [];
    $.each($('td.sum_gyo_0'), function() {
        var id = $(this).attr('id').split('_');
        if (!total_0[id[2]]) {
            total_0[id[2]] = 0;
        }
        total_0[id[2]] += parseFloat($(this).html());
    })
    $.each(total_0, function(k, v) {
        $('#day_total_0_' + k).text(v);
    })

    // 個人日数(実績・希望)
    var total_1 = [];
    $.each($('td.sum_gyo_1'), function() {
        var id = $(this).attr('id').split('_');
        if (!total_1[id[2]]) {
            total_1[id[2]] = 0;
        }
        total_1[id[2]] += parseFloat($(this).html());
    })
    $.each(total_1, function(k, v) {
        $('#day_total_1_' + k).text(v);
    })

    // 項目別日数
    var total_e = [];
    $.each($('td.sum_retu'), function() {
        var id = $(this).attr('id').split('_');
        var idx = id[0].slice(7);
        if (!total_e[idx]) {
            total_e[idx] = 0;
        }
        total_e[idx] += parseFloat($(this).html());
    })
    $.each(total_e, function(k, v) {
        $('#emp_total_' + k).html(v + '&nbsp;');
    })
}

$(function() {

    ///-----------------------------------------------------
    // 集計行ドラッグ＆ドロップ
    ///-----------------------------------------------------
    $("#outCountWindowBar").on('mousedown', function(e) {
        e.preventDefault();
        $("#outCountWindow")
            .data("pageY", e.pageY)
            .data("height", $('#region').height())
            .data("clickPointY", e.pageY - $("#outCountWindow").offset().top);

        $(document).on({
            mousemove: function(e) {
                e.preventDefault();
                var win = $("#outCountWindow");
                var height = win.data("height") - (win.data("pageY") - e.pageY);
                if (height >= 44 && height <= $('#data').height()) {
                    win.css({
                        top: e.pageY - win.data("clickPointY") + "px"
                    });
                    $('#region').height(height + 'px');
                    movedCountWindow = true;
                }
            },
            mouseup: function() {
                $(document).off("mousemove");
            }
        });
    });

    ///-----------------------------------------------------
    // スタンプ
    //------------------------------------------------------
    $("a.open").click(function() {
        $("#floatWindowStp").fadeIn("fast");
        return false;
    });

    $("#floatWindowStp a.close").click(function() {
        $("#floatWindowStp").fadeOut("fast");
        return false;
    });

    $("#floatWindowStp dl dt").mousedown(function(e) {
        e.preventDefault();
        $("#floatWindowStp")
            .data("clickPointX", e.pageX - $("#floatWindowStp").offset().left)
            .data("clickPointY", e.pageY - $("#floatWindowStp").offset().top);

        $(document).mousemove(function(e) {
            e.preventDefault();
            $("#floatWindowStp").css({
                top: e.pageY - $("#floatWindowStp").data("clickPointY") + "px",
                left: e.pageX - $("#floatWindowStp").data("clickPointX") + "px"
            });
        });
    })
        .mouseup(function() {
        $(document).unbind("mousemove");
    });

    /***
     * 画面リサイズイベント
     */
    $(window).resize(function() {
        resizeIframeEx();
    });
});

/**
 * ウインドウ非表示
 */
function floatWindowHide(a, b) {
    if (a == '0') {
        document.getElementById('floatWindow' + b).style.display = "none";
    }
    else {
        document.getElementById('floatWindow' + b).style.display = "";
        //初期表示位置
        document.getElementById('floatWindow' + b).style.top = 5;
        document.getElementById('floatWindow' + b).style.left = 390;
    }
}

/**
 * IEでconsole.logのエラーを出なくする
 **/
if (!('console' in window)) {
    window.console = {};
    window.console.log = function(str) {
        return str;
    };
}

/**
 * 公休残を変更する
 */
function changeHolRemain(id, legal_hol_idx) {
    //id例：#sum_gyo2_1
    var pos = id.slice(8).split('_');
    //個人集計(sum_gyo)、実績 
    if (id.substr(1, 7) == 'sum_gyo' && pos[1] == legal_hol_idx) {
        var ids='#hol_remain' + pos[0];
        var val = $(ids).html();
        //今月分公休残登録済み
        var reg_ids='hol_remain_reg' + pos[0]; //hidden
        var reg_val = $('input:hidden[name="'+reg_ids+'"]').val();
        //val計算後公休残あること（前月の公休残がある場合）、reg_val今月分公休残登録済みが未登録の場合計算
        if (val != '' && reg_val == '') {
            var hol_remain=parseFloat(val);
            //前月残＋所定公休数
            var std_ids='prev_std_cnt' + pos[0]; //hidden
            var prev_std_cnt = $('input:hidden[name="'+std_ids+'"]').val();
            var ids='#sum_gyo' + pos[0] +'_' + legal_hol_idx;
            var sum_gyo_hol=parseFloat($(ids).html());
            //前月残＋所定公休数−当月実績公休数
            hol_remain = (prev_std_cnt - sum_gyo_hol);
            if (isNaN(hol_remain)) {
                hol_remain = '';
            }
            $('#hol_remain' + pos[0]).text(hol_remain);
        
            //公休残数計
            var total_1 = [];
            $.each($('td.hol_remain_1'), function() {
                if (!total_1[0]) {
                    total_1[0] = 0;
                }
                var wk = parseFloat($(this).html());
                if (!isNaN(wk)) {
                    total_1[0] += wk;
                }
            })
            $.each(total_1, function(k, v) {
                $('#hol_remain_total').text(v);
            })
        }
    }
}
