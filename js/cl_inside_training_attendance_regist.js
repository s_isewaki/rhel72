/**
 * output_to_csv
 */
function output_to_csv(type){

	switch (type){
		case 1:
			document.attendance_list.attendance_csv_flg.value = 'true';
			document.attendance_list.answer_csv_flg.value = 'false';
			break;

		case 2:
			document.attendance_list.answer_csv_flg.value = 'true';
			document.attendance_list.attendance_csv_flg.value = 'false';
			break;


		default:
			document.attendance_list.attendance_csv_flg.value = 'false';
			document.attendance_list.answer_csv_flg.value = 'false';
		}

	document.attendance_list.import_flg.value = 'false';
	document.attendance_list.update_flg.value = 'false';
	document.attendance_list.action = "./cl/common/cl_inside_training_attendance_regist_csv.php";
	document.attendance_list.submit();
}

function inport_csv(){
	if(document.getElementById("inputfile").value == ""){
		alert("インポートするファイルを選択してください");
		return false;
	}
	document.attendance_list.attendance_csv_flg.value = 'false';
	document.attendance_list.answer_csv_flg.value = 'false';
	document.attendance_list.update_flg.value = 'false';
	document.attendance_list.import_flg.value = 'true';
	document.attendance_list.target = "_self";
	document.attendance_list.action = "./cl_common_call.php";
	document.attendance_list.submit();
}

function syukketu_update(){
	var data_count = document.attendance_list.data_count.value;
	var roll_data = "";
	for(var i = 1;i<=data_count;i++){
		if(i != 1){roll_data += ",";}
		if(document.getElementById("roll"+i).checked){
			roll_data += "1";
		}else{
			roll_data += "0";
		}
	}
	document.attendance_list.roll_data.value = roll_data;
	document.attendance_list.attendance_csv_flg.value = 'false';
	document.attendance_list.answer_csv_flg.value = 'false';
	document.attendance_list.import_flg.value = 'false';
	document.attendance_list.update_flg.value = 'true';
	document.attendance_list.target = "_self";
	document.attendance_list.action = "./cl_common_call.php";
	document.attendance_list.submit();
}

function window_close(){
	if(window.opener && !window.opener.closed && window.opener.plan_regist_return)
	{
		//親画面更新
		window.opener.plan_regist_return();
	}
	//自画面を終了します。
	window.close();
}