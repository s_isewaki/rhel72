//focustonext.js
var nextElement = null;//項目の移動先(次)
var backElement = null;//項目の移動先（戻り）
function focusToNext(e)
{
//alert(nextElement);
if (nextElement == null) {
		return false;
}

	if((getKEYCODE(e) == 13)  || (getKEYCODE(e) == 39))//改行コード,→キーだったら
	{
		document.getElementById(nextElement).focus();
		if (document.getElementById(nextElement).type == 'text') {
			document.getElementById(nextElement).select();
		}
		return false;
	}

	if(getKEYCODE(e) == 37)//←キーだったら
	{
		document.getElementById(backElement).focus();
		if (document.getElementById(backElement).type == 'text') {
			document.getElementById(backElement).select();
		}
		return false;
	}

}

function getKEYCODE(e)
{

	var ua = navigator.userAgent;

	if(navigator.userAgent.indexOf("MSIE") != -1 || navigator.userAgent.indexOf("Trident") != -1)
	{					//IE
		return event.keyCode;
	}
	else if(navigator.userAgent.indexOf("Firefox") != -1)
	{		//firefox
		return (e.keyCode)? e.keyCode: e.charCode;
	}
	else if(navigator.userAgent.indexOf("Safari") != -1)
	{		//safari
		return event.keyCode;
	}
	else
	{
		return null;
	}
}
