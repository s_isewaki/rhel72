

function popupDetail(line_mode,values,e)
{
	if(line_mode == "data_line")
	{
		var top_message = "行をクリックするとコピー対象に追加されます。";
	}
	else//"entry_line"
	{
		var top_message = "行をクリックするとコピー対象から削除されます。";
	}
	top_message = "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j9'>" + top_message + "</font>";

	var box_width = 600;
	var label_width = 150;
	var strong_color = '#5279a5';
	var pale_color = '#f6f9ff';

	popupDetail2(top_message,values, box_width, label_width, strong_color, pale_color, e);
}


function popupDetail2(top_message, values, box_width, label_width, strong_color, pale_color, e) {
	var div = document.getElementById('popup');
	if (!div) {
		var selects = document.getElementsByTagName('select');
		for (var i = 0, j = selects.length; i < j; i++) {
			selects[i].style.visibility = 'hidden';
		}

		var table = document.createElement('table');
		table.style.borderCollapse = 'collapse';
		table.style.width = '100%';
		var is_values_over = false;
		for (var i = 0, j = values.length; i < j; i += 2) {
			if(i >= 6)
			{
				is_values_over = true;
				break;
			}
			addDetailRow(table, values[i], values[i + 1], strong_color, pale_color);
		}
		table.rows[0].cells[0].style.width = String(label_width).concat('px');

		div = document.createElement('div');
		div.id = 'popup';
		div.style.position = 'absolute';
		div.style.width = String(box_width).concat('px');
		div.style.borderColor = '#5279a5';
		div.style.borderStyle = 'solid';
		div.style.borderWidth = '3px';
		div.style.backgroundColor = '#ffffff';
		div.style.padding = '3px';

		if(top_message != "")
		{
			var top_message_obj = document.createElement('span');
			top_message_obj.innerHTML = top_message;
			top_message_obj.style.width = String((box_width-6)).concat('px');
			div.appendChild(top_message_obj);
		}

		div.appendChild(table);

		if(is_values_over)
		{
			var over_message_obj = document.createElement('span');
			var over_count = (values.length-6)/2;
			over_message_obj.innerHTML = "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>(他"+over_count+"項目)</font>";
			over_message_obj.style.width = String((box_width-6)).concat('px');
			div.appendChild(over_message_obj);
		}

		document.body.appendChild(div);
	}

	if (e == undefined) {
		e = window.event;
	}

	var iframe = window;
	var iframeOffsetTop = getOffsetTopFromBody(iframe);
	var iframeWidth = document.body.clientWidth;
	var iframeHeight = top.document.body.clientHeight - iframeOffsetTop;

	var x = ((e.x) ? e.x : e.clientX) + 8;
	var y = ((e.y) ? e.y : e.clientY) + 8;
	if (x + div.clientWidth + 10 > iframeWidth) {
		x = iframeWidth - div.clientWidth - 10;
	}
	if (y + div.clientHeight > iframeHeight && (y - (div.clientHeight + 16)) >0 ) {
		y -= (div.clientHeight + 16);
	}

	div.style.left = x+"px";
	div.style.top = (y + document.body.scrollTop) + "px";
}

function addDetailRow(table, header, value, border_color, header_color) {
	var row = table.insertRow(table.rows.length);
	row.style.height = '22px';
	addDetailCell(row, header, border_color, header_color);
	addDetailCell(row, value, border_color, '#ffffff');
}

function addDetailCell(row, str, border_color, bg_color) {
	var cell = row.insertCell(row.cells.length);
	cell.style.borderColor = border_color;
	cell.style.borderStyle = 'solid';
	cell.style.borderWidth = '1px';
	cell.style.padding = '2px';
	cell.style.backgroundColor = bg_color;
	cell.style.color = '#000000';
	cell.style.fontFamily = 'ＭＳ Ｐゴシック, Osaka';
	cell.className = 'j12';
	cell.innerHTML = str;
}

function closeDetail() {
	var div = document.getElementById('popup');
	if (div) {
		document.body.removeChild(div);
	}

	var selects = document.getElementsByTagName('select');
	for (var i = 0, j = selects.length; i < j; i++) {
		selects[i].style.visibility = '';
	}
}

function getOffsetTopFromBody(element) {
	var offset = 0;
	while (element.offsetParent) {
		offset += element.offsetTop;
		element = element.offsetParent;
	}
	return offset;
}
