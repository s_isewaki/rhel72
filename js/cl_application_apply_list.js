function reload_page() {
	document.apply.action="cl_application_apply_list.php";
	document.apply.submit();
}

function initPage() {

	cateOnChange();
}

function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}


function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}


function apply_search() {

	// ページ番号が存在する場合、初期化
	if (document.apply.page) {
		document.apply.page.value="";
	}
	//document.apply.action="cl_application_apply_list.php?session={$session}";
	document.apply.action="cl_application_apply_list.php";
	document.apply.submit();

}

function re_apply() {

	if (document.apply.elements['re_apply_chk[]'] == undefined) {
		alert('「再申請」するデータを選択してくだい。');
		return;
	}

	if (document.apply.elements['re_apply_chk[]'].length == undefined) {
		if (!document.apply.elements['re_apply_chk[]'].checked) {
			alert('「再申請」するデータを選択してくだい。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.apply.elements['re_apply_chk[]'].length; i < j; i++) {
			if (document.apply.elements['re_apply_chk[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('「再申請」するデータを選択してくだい。');
			return;
		}
	}

	if (confirm('再申請します。よろしいですか？')) {
		//document.apply.action="cl_application_list_re_apply.php?session={$session}";
		document.apply.action="cl_application_list_re_apply.php";
		document.apply.submit();
	}

}


function apply_cancel() {

	if (document.apply.elements['apply_cancel_chk[]'] == undefined) {
		alert('「申請取消」するデータを選択してくだい。');
		return;
	}

	if (document.apply.elements['apply_cancel_chk[]'].length == undefined) {
		if (!document.apply.elements['apply_cancel_chk[]'].checked) {
			alert('「申請取消」するデータを選択してくだい。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.apply.elements['apply_cancel_chk[]'].length; i < j; i++) {
			if (document.apply.elements['apply_cancel_chk[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('「申請取消」するデータを選択してくだい。');
			return;
		}
	}

	if (confirm('申請取消します。よろしいですか？')) {
		//document.apply.action="cl_application_list_apply_cancel.php?session={$session}";
		document.apply.action="cl_application_list_apply_cancel.php";
		document.apply.submit();
	}
}

function clearApplyDate() {
	document.apply.date_y1.selectedIndex = 0;
	document.apply.date_m1.selectedIndex = 0;
	document.apply.date_d1.selectedIndex = 0;
	document.apply.date_y2.selectedIndex = 0;
	document.apply.date_m2.selectedIndex = 0;
	document.apply.date_d2.selectedIndex = 0;
}

function setEventHandler() {
	YAHOO.ui.calendar.cal3.selectEvent.subscribe(clearApplyDate);
	YAHOO.ui.calendar.cal4.selectEvent.subscribe(clearApplyDate);
	YAHOO.util.Event.addListener(['date_m3', 'date_d3', 'date_y3', 'date_m4', 'date_d4', 'date_y4'], 'change', clearApplyDate);
}
