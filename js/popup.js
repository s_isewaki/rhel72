function popupDetailBlue(values, box_width, label_width, e, etc) {
	popupDetail(values, box_width, label_width, '#5279a5', '#f6f9ff', e, etc);
}

function popupDetailBlueImg(values, box_width, label_width, img, e, etc) {
	popupDetailImg(values, box_width, label_width, '#5279a5', '#f6f9ff', img, e, etc);
}

function popupDetailPink(values, box_width, label_width, e, etc) {
	popupDetail(values, box_width, label_width, '#c71585', '#ffe4e1', e, etc);
}

function popupDetailYellow(values, box_width, label_width, e, etc) {
	popupDetail(values, box_width, label_width, '#b8860b', '#fffacd', e, etc);
}

function popupDetail(values, box_width, label_width, strong_color, pale_color, e, etc) {
	if (document.readyState) {
		if (document.readyState != 'complete') {
			return;
		}
	}
	var div = document.getElementById('popup');
	if (!div) {
		var selects = document.getElementsByTagName('select');
		for (var i = 0, j = selects.length; i < j; i++) {
			selects[i].style.visibility = 'hidden';
		}

		var table = document.createElement('table');
		table.style.borderCollapse = 'collapse';
		table.style.width = '100%';
		for (var i = 0, j = values.length; i < j; i += 2) {
			addDetailRow(table, values[i], values[i + 1], strong_color, pale_color);
		}
		table.rows[0].cells[0].style.width = String(label_width).concat('px');

        // お知らせ・回覧板ブロックで、一番文字が長い「対象カテゴリ」の部分で「nowrap」を適用
        if (table.rows[2] && table.rows[2].cells[0]) {
            table.rows[2].cells[0].style.whiteSpace = "nowrap";
        }

		div = document.createElement('div');
		div.id = 'popup';
		div.style.position = 'absolute';
		div.style.width = String(box_width).concat('px');
		div.style.borderColor = '#5279a5';
		div.style.borderStyle = 'solid';
		div.style.borderWidth = '3px';
		div.style.backgroundColor = '#ffffff';
		div.style.padding = '3px';
		div.appendChild(table);

		if (etc) {
			var p = document.createElement('p');
			p.style.fontFamily = 'ＭＳ Ｐゴシック, Osaka';
			p.style.textAlign = 'right';
			p.style.marginTop = '2px';
			p.innerHTML = etc;
			div.appendChild(p);
		}

		document.body.appendChild(div);
	}

	var bodyWidth = document.body.clientWidth;
	var bodyHeight;

	var iframe = parent.document.getElementById('floatingpage');
    if (iframe) {
		var iframeOffsetTop = getOffsetTopFromBody(iframe);
		bodyHeight = parent.document.body.clientHeight - iframeOffsetTop;
	} else {
		bodyHeight = parent.document.body.clientHeight;
	}

	if (e == undefined) {
		e = window.event;
	}
	var scrollLeft = document.body.scrollLeft;
	var x = e.clientX + 8 + scrollLeft;
	var y = e.clientY + 8 + document.body.scrollTop;

	if (x - scrollLeft + div.clientWidth + 6 > bodyWidth) {
		x -= (div.clientWidth + 16);
		if (x < 0) {x = 0;}
	}

    var scrollTop = (self == parent) ? document.body.scrollTop : parent.document.body.scrollTop;
	if (y - scrollTop + div.clientHeight + 6 > bodyHeight) {
		y -= (y - scrollTop + div.clientHeight - bodyHeight + 16);
		if (y < 0) {y = 0;}
	}

	div.style.left = x + "px";
	div.style.top = y + "px";
}

function popupDetailImg(values, box_width, label_width, strong_color, pale_color, puimg, e, etc) {
	if (document.readyState) {
		if (document.readyState != 'complete') {
			return;
		}
	}
	var div = document.getElementById('popup');
	if (!div) {
		var selects = document.getElementsByTagName('select');
		for (var i = 0, j = selects.length; i < j; i++) {
			selects[i].style.visibility = 'hidden';
		}

		var table = document.createElement('table');
		table.style.borderCollapse = 'collapse';
		table.style.width = '100%';
		for (var i = 0, j = values.length; i < j; i += 2) {
			addDetailRow(table, values[i], values[i + 1], strong_color, pale_color);
		}
		table.rows[0].cells[0].style.width = String(label_width).concat('px');

		div = document.createElement('div');
		div.id = 'popup';
		div.style.position = 'absolute';
		div.style.width = String(box_width).concat('px');
		div.style.borderColor = '#5279a5';
		div.style.borderStyle = 'solid';
		div.style.borderWidth = '3px';
		div.style.backgroundColor = '#ffffff';
		div.style.padding = '3px';
		div.appendChild(table);

		if(puimg != "")
		{
			obj_img_name = puimg.split(";");


			var strUA = "";
			strUA = navigator.userAgent.toLowerCase();
			browse_type = "";

			//ブラウザの種類を判定
			if(strUA.indexOf("safari") != -1)
			{
				browse_type = "1";
				//document.write("Safari");
			}
			else if(strUA.indexOf("firefox") != -1)
			{
				browse_type = "2";
				//document.write("Firefox");
			}
			else if(strUA.indexOf("opera") != -1)
			{
				document.write("Opera");
				//browse_type = "3";
			}
			else if(strUA.indexOf("netscape") != -1)
			{
				browse_type = "4";
				//document.write("Netscape");
			}
			else if(strUA.indexOf("msie") != -1)
			{
				browse_type = "5";
				//document.write("Internet Explorer");
			}
			else if(strUA.indexOf("mozilla/4") != -1)
			{
				browse_type = "6";
				//document.write("Netscape.4");
			}
			else if(strUA.indexOf("Google Chrome") != -1)
			{
				browse_type = "7";
				//document.write("Google Chrome");
			}

			for (var cnt_imgcnt = 0; cnt_imgcnt < (obj_img_name.length -1); cnt_imgcnt++)
			{
				if(browse_type == "7")
				{
					//chrome
				var objimg = document.createElement("image");
				}
				else
				{
					//chrome以外
					var objimg = document.createElement("img");
				}
				objimg.src = obj_img_name[cnt_imgcnt];
				objimg.alt = "";
				objimg.name = "myFormImg";
				div.appendChild(objimg);
			}
		}

		if (etc) {
			var p = document.createElement('p');
			p.style.fontFamily = 'ＭＳ Ｐゴシック, Osaka';
			p.style.textAlign = 'right';
			p.style.marginTop = '2px';
			p.innerHTML = etc;
			div.appendChild(p);
		}

		document.body.appendChild(div);
	}

	var bodyWidth = document.body.clientWidth;
	var bodyHeight;

	var iframe = parent.document.getElementById('floatingpage');

    if (iframe) {
		var iframeOffsetTop = getOffsetTopFromBody(iframe);
		bodyHeight = parent.document.body.clientHeight - iframeOffsetTop;
	} else {
		bodyHeight = parent.document.body.clientHeight;
	}

	if (e == undefined) {
		e = window.event;
	}
	var scrollLeft = document.body.scrollLeft;
	var x = e.clientX + 8 + scrollLeft;
	var y = e.clientY + 8 + document.body.scrollTop;

	if (x - scrollLeft + div.clientWidth + 6 > bodyWidth) {
		x -= (div.clientWidth + 16);
		if (x < 0) {x = 0;}
	}

	var scrollTop = (self == parent) ? document.body.scrollTop : parent.document.body.scrollTop;
	if (y - scrollTop + div.clientHeight + 6 > bodyHeight) {
		y -= (y - scrollTop + div.clientHeight - bodyHeight + 16);
		if (y < 0) {y = 0;}
	}

	div.style.left = x + "px";
	div.style.top = y + "px";
}



function addDetailRow(table, header, value, border_color, header_color) {
	var row = table.insertRow(table.rows.length);
	row.style.height = '22px';
	addDetailCell(row, header, border_color, header_color);
	addDetailCell(row, value, border_color, '#ffffff');
}

function addDetailCell(row, str, border_color, bg_color) {
	var cell = row.insertCell(row.cells.length);
	cell.style.borderColor = border_color;
	cell.style.borderStyle = 'solid';
	cell.style.borderWidth = '1px';
	cell.style.padding = '2px';
	cell.style.backgroundColor = bg_color;
	cell.style.color = '#000000';
	cell.style.fontFamily = 'ＭＳ Ｐゴシック, Osaka';
//	cell.className = 'j12';
	cell.innerHTML = str;
}

function closeDetail() {
	var div = document.getElementById('popup');
	if (div) {
		document.body.removeChild(div);
	}

	var selects = document.getElementsByTagName('select');
	for (var i = 0, j = selects.length; i < j; i++) {
		selects[i].style.visibility = '';
	}
}

function getOffsetTopFromBody(element) {
	var offset = 0;
	while (element.offsetParent) {
		offset += element.offsetTop;
		element = element.offsetParent;
	}
	return offset;
}
