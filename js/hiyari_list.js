jQuery(function($){
/*--------------------------------------------------------------
	分類フォルダ　カスタムスクロールバー
  -------------------------------------------------------------- */
  if ($('#sub_bunrui').size() > 0){
    $('#b_scrollbar').tinyscrollbar();
    $('#sub_bunrui input').click(function(e) {
      var thisLi = $(this).closest('li');
      if(thisLi.hasClass('checked')) {
        thisLi.removeClass('checked')
        return;
      }
      thisLi.addClass('checked');
    });
  }
  
/*--------------------------------------------------------------
	チェックボックスのチェックがある時のtrの背景
  -------------------------------------------------------------- */
	$('.checkbox').click(function(e) {
		var thisTr = $(this).closest('tr');
		if(thisTr.hasClass('checked')) {
			thisTr.removeClass('checked')
			return;
		}
		thisTr.addClass('checked');
	});	
});
