	//---------------------------
	// 行追加
	//---------------------------
	function add_line(add_cnt)
	{
		dip_open(add_cnt,"ADD_LINE");
	}

	//---------------------------
	// 行削除
	//---------------------------
	function del_line(del_cnt)
	{
		// 削除メッセージ出力
		if (document.getElementById(del_cnt + "_EXE").value != ""){
			strRes = confirm("削除します。よろしいですか？");
			if (strRes == false) {
				return false;
			}
		}
		dip_open(del_cnt,"DEL_LINE");
	}

	//---------------------------
	// 更新ボタン押下時
	//---------------------------
	function output_update(cnt)
	{
		var err_idx=0;
		var err_idx_2=0;
		var err_idx_3=0;
		var chk =0;

		for (i=0; i<mainform.dataCount.value; i++) {
			if (document.getElementById(i + "_TXT").value =="")
			{
				err_idx=1;
				document.getElementById(i + "_TXT").style.backgroundColor="FFB6C1";
			}else{
				document.getElementById(i + "_TXT").style.backgroundColor="";
			}

			if (document.getElementById(i + "_TXT_MUT").value != "")
			{

				if ((parseInt(document.getElementById(i + "_TXT_MUT").value) >= 1) && (parseInt(document.getElementById(i + "_TXT_MUT").value) <= 4))
				{
					chk = document.getElementById(i + "_TXT_MUT").value;

					if (!document.getElementById(i + "_LEVEL_" + chk).checked)
					//if (document.getElementById('preticket22').checked) {
					{
						err_idx_3=1;
						document.getElementById(i + "_TXT_MUT").style.backgroundColor="FFB6C1";
					}else{
						document.getElementById(i + "_TXT_MUT").style.backgroundColor="";
					}

				}else{
					err_idx_2=1;
					document.getElementById(i + "_TXT_MUT").style.backgroundColor="FFB6C1";
				}
			}
		}

		// 入力チェック結果メッセージの表示
		if( err_idx == 1 ){
			alert("更新内容に誤りがあります。入力項目を確認してください。");
			return false;
		} else if ( err_idx_2 == 1 ){
			alert("必達評定はレベル�気�ら�犬覇�力してください。");
			return false;
		} else if ( err_idx_3 == 1 ){
			alert("必達評定はチェックした評価値を入力してください");
			return false;
		}

		dip_open(cnt,"UPDATA");
	}

	//---------------------------
	// 非表示フラグチェック
	//---------------------------
	function chkValue(intLine,No)
	{
		var err_idx=0;

		if (document.getElementById(intLine + "_LEVEL_" + No).checked)
		{
			document.getElementById(intLine + "_LEVEL_" + No).value="1";
		}else{
			document.getElementById(intLine + "_LEVEL_" + No).value="0";
		}			

		dip_open(intLine,"rank_change");
	}
	//---------------------------
	// 自画面コール
	//---------------------------
	function dip_open(cnt,mode_kbn)
	{
	
		mainform.hdnrec.value = cnt;
		mainform.mode.value = mode_kbn;
		document.mainform.target = "_self";
		document.mainform.action = "./cl_common_call.php";
		document.mainform.submit();
	
	}
