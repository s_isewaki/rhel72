/////////////////////////////////////////////////////////////////////////////
// 部署
/////////////////////////////////////////////////////////////////////////////
// 部署条件の行追加
function search_add(OBJ) {
    OBJ.postback_mode.value = "add_search";
    OBJ.submit();
}

// 部署条件の行削除
function search_delete(OBJ, no) {
    OBJ.postback_mode.value = "delete_search";
    OBJ.delete_line_no.value = no;
    OBJ.submit();
}

/////////////////////////////////////////////////////////////////////////////
// 条件保存
/////////////////////////////////////////////////////////////////////////////
function save_condition(frm)
{
  var name_area = document.getElementById("condition_name_area");
  if (name_area.style.display == 'none'){
    name_area.style.display = '';
    return;
  }

  var name = document.getElementById("condition_name");
  if (name.value == ""){
    alert('集計条件保存名が記入されていません');
    return;
  }
  
  frm.is_stats_condition_set.value=true;
  frm.submit();
}

/////////////////////////////////////////////////////////////////////////////
//項目の除外と除外解除
/////////////////////////////////////////////////////////////////////////////
//指定項目を除外
function item_no_disp_action()
{
    var is_ok;

    //V:縦軸の全項目非表示NGチェック
    is_ok = false;
    var vertical_box_arr = document.getElementsByName("vertical_axis_checkbox[]");
    for(var i = 0; i < vertical_box_arr.length; i++)
    {
        if(!vertical_box_arr[i].checked)
        {
            is_ok = true;
            break;
        }
    }
    if(!is_ok)
    {
        alert("全ての項目を除外することはできません。");
        return;
    }

    //H:横軸の全項目非表示NGチェック
    is_ok = false;
    var horizontal_box_arr = document.getElementsByName("horizontal_axis_checkbox[]");
    for(var i = 0; i < horizontal_box_arr.length; i++)
    {
        if(!horizontal_box_arr[i].checked)
        {
            is_ok = true;
            break;
        }
    }
    if(!is_ok)
    {
        alert("全ての項目を除外することはできません。");
        return;
    }

    //V:現在非表示の項目一覧を配列取得する。
    var old_vertical_no_disp_code_list_csv = document.result_edit_form.vertical_no_disp_code_list_csv.value;
    var vertical_no_disp_code_arr = new Array();
    if(old_vertical_no_disp_code_list_csv != "")
    {
        vertical_no_disp_code_arr = old_vertical_no_disp_code_list_csv.split(",");
    }

    //V:非表示指定された項目を配列追加する。
    for(var i = 0; i < vertical_box_arr.length; i++)
    {
        if(vertical_box_arr[i].checked)
        {
            var code = vertical_box_arr[i].value;
            if(code == "")
            {
                code = "empty";
            }
            vertical_no_disp_code_arr.push(code);
        }
    }

    //V:送信パラメータを更新する。
    var vertical_no_disp_code_csv = vertical_no_disp_code_arr.join(",");
    document.result_edit_form.vertical_no_disp_code_list_csv.value = vertical_no_disp_code_csv;

    //H:現在非表示の項目一覧を配列取得する。
    var old_horizontal_no_disp_code_list_csv = document.result_edit_form.horizontal_no_disp_code_list_csv.value;
    var horizontal_no_disp_code_arr = new Array();
    if(old_horizontal_no_disp_code_list_csv != "")
    {
        horizontal_no_disp_code_arr = old_horizontal_no_disp_code_list_csv.split(",");
    }

    //H:非表示指定された項目を配列追加する。
    for(var i = 0; i < horizontal_box_arr.length; i++)
    {
        if(horizontal_box_arr[i].checked)
        {
            var code = horizontal_box_arr[i].value;
            if(code == "")
            {
                code = "empty";
            }
            horizontal_no_disp_code_arr.push(code);
        }
    }

    //H:送信パラメータを更新する。
    var horizontal_no_disp_code_csv = horizontal_no_disp_code_arr.join(",");
    document.result_edit_form.horizontal_no_disp_code_list_csv.value = horizontal_no_disp_code_csv;


    //非表示実効として送信
    document.result_edit_form.vertical_no_disp_code_list_old_csv.value = "";
    document.result_edit_form.horizontal_no_disp_code_list_old_csv.value = "";
    document.result_edit_form.postback_mode.value = "no_disp";
    document.result_edit_form.submit();
}

//除外解除
function item_all_disp_action()
{
    document.result_edit_form.vertical_no_disp_code_list_old_csv.value = document.result_edit_form.vertical_no_disp_code_list_csv.value;
    document.result_edit_form.horizontal_no_disp_code_list_old_csv.value = document.result_edit_form.horizontal_no_disp_code_list_csv.value;
    document.result_edit_form.vertical_no_disp_code_list_csv.value = "";
    document.result_edit_form.horizontal_no_disp_code_list_csv.value = "";
    document.result_edit_form.postback_mode.value = "all_disp";
    document.result_edit_form.submit();
}

/////////////////////////////////////////////////////////////////////////////
//項目のドリルダウン
/////////////////////////////////////////////////////////////////////////////
//縦軸ドリルダウン（項目名リンク、ボタン）
function vertical_axis_detail_action(detail,val)
{
    //ドリルダウン内容を設定
    switch (detail) {
        case "post_attribute":
            document.result_edit_form.vertical_axises_detail.value = detail;
            document.result_edit_form.vertical_axises_post_class.value = val;
            break;
        case "post_dept":
            document.result_edit_form.vertical_axises_detail.value = detail;
            document.result_edit_form.vertical_axises_post_attribute.value = val;
            break;
        case "post_room":
            document.result_edit_form.vertical_axises_detail.value = detail;
            document.result_edit_form.vertical_axises_post_dept.value = val;
            break;
        case "score":
        case "danger":
        case "sensor":
        case "kousoku":
        case "mitten":
        case "scene":
        case "kind":
        case "content":
        case "detail_scene":
        case "detail_kind":
        case "detail_content":
        case "summary_selfremove":
        case "summary_stumble":
        case "summary_mitten":
        case "scene_content_selfremove":
        case "scene_content_summary_stumble":
        case "scene_content_summary_mitten":
        case "factor":
        case "detail_factor":
        case "influence":
        case "mental":
        case "correspondence":
        case "detail_correspondence":
        case "patient_state_detail":
        case "place_detail":
        case "asses_150_6_detail":
        case "asses_150_7_detail":
        case "asses_150_8_detail":
        case "asses_150_20_detail":
            document.result_edit_form.vertical_axises_detail.value = detail;
            document.result_edit_form.vertical_axises_summary_item.value = val;
            break;
        default:
            //なし
            break;
    }

    //ドリルダウンで変更される軸の非表示設定をクリア
    document.result_edit_form.vertical_no_disp_code_list_csv.value = "";

    document.result_edit_form.postback_mode.value = "drill_down";
    document.result_edit_form.submit();
}

//横軸ドリルダウン実効（項目名リンク、ボタン）
function horizontal_axis_detail_action(detail,val)
{
    //ドリルダウン内容を設定
    switch (detail)
    {
        case "post_attribute":
            document.result_edit_form.horizontal_axises_detail.value = detail;
            document.result_edit_form.horizontal_axises_post_class.value = val;
            break;
        case "post_dept":
            document.result_edit_form.horizontal_axises_detail.value = detail;
            document.result_edit_form.horizontal_axises_post_attribute.value = val;
            break;
        case "post_room":
            document.result_edit_form.horizontal_axises_detail.value = detail;
            document.result_edit_form.horizontal_axises_post_dept.value = val;
            break;
        case "score":
        case "danger":
        case "sensor":
        case "kousoku":
        case "mitten":
        case "scene":
        case "kind":
        case "content":
        case "detail_scene" :
        case "detail_kind" :
        case "detail_content" :
        case "summary_selfremove":
        case "summary_stumble":
        case "summary_mitten":
        case "scene_content_selfremove":
        case "scene_content_summary_stumble":
        case "scene_content_summary_mitten":
        case "factor":
        case "detail_factor":
        case "influence":
        case "mental":
        case "correspondence":
        case "detail_correspondence":
        case "patient_state_detail":
        case "place_detail":
        case "asses_150_6_detail":
        case "asses_150_7_detail":
        case "asses_150_8_detail":
        case "asses_150_20_detail":
            document.result_edit_form.horizontal_axises_detail.value = detail;
            document.result_edit_form.horizontal_axises_summary_item.value = val;
            break;
        default:
            //なし
            break;
    }

    //ドリルダウンで変更される軸の非表示設定をクリア
    document.result_edit_form.horizontal_no_disp_code_list_csv.value = "";

    document.result_edit_form.postback_mode.value = "drill_down";
    document.result_edit_form.submit();
}

//縦軸ドリルダウン戻し（縦軸を戻すボタン）
function vertical_axis_detail_return(link)
{
    //ドリルダウンを戻し値に設定
    document.result_edit_form.vertical_axises_detail.value = link;

    //ドリルダウンで変更される軸の非表示設定をクリア
    document.result_edit_form.vertical_no_disp_code_list_csv.value = "";

    document.result_edit_form.postback_mode.value = "drill_down_return";
    document.result_edit_form.submit();
}

//横軸ドリルダウン戻し（横軸を戻すボタン）
function horizontal_axis_detail_return(link)
{
    //ドリルダウンを戻し値に設定
    document.result_edit_form.horizontal_axises_detail.value = link;

    //ドリルダウンで変更される軸の非表示設定をクリア
    document.result_edit_form.horizontal_no_disp_code_list_csv.value = "";

    document.result_edit_form.postback_mode.value = "drill_down_return";
    document.result_edit_form.submit();
}

/////////////////////////////////////////////////////////////////////////////
//EXCEL出力
/////////////////////////////////////////////////////////////////////////////
function excel_download()
{
    document.xls.submit();
}

/////////////////////////////////////////////////////////////////////////////
//印刷
/////////////////////////////////////////////////////////////////////////////
function print_download() {
    document.print.submit();
}

/////////////////////////////////////////////////////////////////////////////
//グラフ
/////////////////////////////////////////////////////////////////////////////
function chart_disp(session, session_name, session_id)
{
    var url = "hiyari_rm_stats_chart.php?session="+session+"&"+session_name+"="+session_id;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1080,height=700";
    window.open(url, 'rm_stats_chart_window', option);
}

///////////////////////////////////////////////////////////////////////////// 
//項目絞り込み条件
/////////////////////////////////////////////////////////////////////////////
//クリア
function item_shibori_clear()
{
    var hidden = document.getElementById("is_item_shibori_clear");
    var hidden2 = document.getElementById("is_item_shibori_clear_of_result_edit_form");
    var area = document.getElementById("item_shibori_disp_area");
    
    area.innerHTML = "全てのデータ";
    hidden.value = "true";
    hidden2.value = "true";
    
    var isIE = /*@cc_on!@*/false;
    if (isIE) { 
      var area = document.getElementById("conditions2");
      if (area){  //新デザインのみ
        area.style.height = 133;
      }
    }
    
    var folder_select = document.getElementsByName("folder_select[]");
    for(var i = 0; i < folder_select.length; i++)
    {
    	if (folder_select[i].checked){
    		folder_select[i].checked = false;
    		folder_select[i].parentNode.className = 'checkbox_wrap';
    	}
    }
    
    var style_select = document.getElementsByName("style_select[]");
    for(var i = 0; i < style_select.length; i++)
    {
    	if (style_select[i].checked){
    		style_select[i].checked = false;
    		style_select[i].parentNode.className = 'checkbox_wrap';
    	}
    }
    
    document.mainform.shibori_info_clear.value = "true";
}

//変更
function item_shibori_change()
{
    var vertical_box_arr = document.getElementsByName("vertical_axis_checkbox[]");
    var horizontal_box_arr = document.getElementsByName("horizontal_axis_checkbox[]");

    //V:変更項目
    var vertical_code_csv = "";
    for(var i = 0; i < vertical_box_arr.length; i++)
    {
        if(vertical_box_arr[i].checked)
        {
            if(vertical_code_csv != "")
            {
                vertical_code_csv = vertical_code_csv + ",";
            }

            var code = vertical_box_arr[i].value;
            if(code == "")
            {
                code = "empty";
            }
            vertical_code_csv = vertical_code_csv + code;
        }
    }
    document.result_edit_form.vertical_item_shibori_change_csv.value = vertical_code_csv;

    //H:変更項目
    var horizontal_code_csv = "";
    for(var i = 0; i < horizontal_box_arr.length; i++)
    {
        if(horizontal_box_arr[i].checked)
        {
            if(horizontal_code_csv != "")
            {
                horizontal_code_csv = horizontal_code_csv + ",";
            }

            var code = horizontal_box_arr[i].value;
            if(code == "")
            {
                code = "empty";
            }
            horizontal_code_csv = horizontal_code_csv + code;
        }
    }
    document.result_edit_form.horizontal_item_shibori_change_csv.value = horizontal_code_csv;


    document.result_edit_form.postback_mode.value = "item_shibori_change";
    document.result_edit_form.submit();
}

/////////////////////////////////////////////////////////////////////////////
//子画面
/////////////////////////////////////////////////////////////////////////////
//統計分析レポート表示画面
function show_report_list_window(v_name, h_name, report_id_list)
{
    //報告書一覧データを設定
    document.report_id_list_form.report_id_list.value = report_id_list;

    //縦軸の項目名を設定
    document.report_id_list_form.vertical_item_label.value = v_name;

    //横軸の項目名を設定
    document.report_id_list_form.horizontal_item_label.value = h_name;

    //子画面に対して送信
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
    window.open('', 'rm_stats_reports_window', option);
    document.report_id_list_form.submit();
}

//集計条件呼出画面から
function load_stats_condition(id) {
//    document.result_edit_form.postback_mode.value = "load_stats_condition";
//    document.result_edit_form.get_stats_condition.value = id;
//    document.result_edit_form.submit();
    document.mainform.postback_mode.value = "load_stats_condition";
    document.mainform.get_stats_condition.value = id;
    document.mainform.submit();
}

//ライドボックスを開く 
function openbox(formtitle)
{
  var box = document.getElementById('box'); 
  document.getElementById('shadowing').style.display='block';
  box.style.display='block';
  
}

// ライドボックスを閉じる 
function closebox()
{
   document.getElementById('box').style.display='none';
   document.getElementById('shadowing').style.display='none';
}

//様式報告書・ラベル 
function add_box_item()
{
   document.mainform.boxflag.value = "true"; 
   document.mainform.is_item_shibori_clear.value = "false";
   document.mainform.submit();
}