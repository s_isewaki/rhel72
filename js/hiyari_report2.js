/******************************************************************************
 * file name   : hiyari_report2.js
 * description : 報告画面　新デザイン（hiyari_report2.tpl）用のJSファイル
 ******************************************************************************/
//ドラッグ＆ドロップ用変数
var is_dragging = false;
var selected_ids;

jQuery(function($){
    //==========================================================================
    //ウィンドウサイズの変更
    //==========================================================================
    $(window).resize(function(){
        //--------------------------
        //メインナビが画面に収まるように調整
        //--------------------------
        var ul = $('#main_nav');

        //最低幅を設定
        var min_width = ul.children().length * 66 + 7; //66:liの幅+マージン、7:左端と右端のマージンの計
        ul.css('min-width', min_width + 'px');

        //ウィンドウサイズに合わせた幅を算出
        //179:サブ、21:サブとメインの間、6：青の枠(3×2)
        var width = $(window).width() - 179 - 21 - 6;

        //IE6〜9の場合は、画面幅1024pxに収まるサイズに固定
        var userAgent = window.navigator.userAgent.toLowerCase();
        var appVersion = window.navigator.appVersion.toLowerCase();
        if (userAgent.indexOf("msie") > -1){
          if (appVersion.indexOf("msie 6.") != -1 ||
              appVersion.indexOf("msie 7.") != -1 ||
              appVersion.indexOf("msie 8.") != -1 ||
              appVersion.indexOf("msie 9.") != -1) {
              width = 790;
          }
        }

        //サイズを設定
        ul.css('width', width + 'px');
    });

    //==========================================================================
    //分類フォルダ　カスタムスクロールバー
    //==========================================================================
    if ($('#sub_bunrui').size() > 0){
        $('#b_scrollbar').tinyscrollbar();
        $('#sub_bunrui input').click(function(e) {
          $(this).closest('li').toggleClass('checked');
        });
    }

    //==========================================================================
    //チェックボックス
    //==========================================================================
    $('.checkbox').click(function(e) {
        //チェックボックスのチェックがある時のtr
        $(this).closest('tr').toggleClass('checked');
        //チェックボックスの見た目
        $(this).closest('.checkbox_wrap').toggleClass('checked');
    });

    //==========================================================================
    //匿名の実名をポップアップ表示
    //==========================================================================
    $('.anonymous').tooltip({
        position: {
            my: "left+20 center",
            at: "right center",
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" )
                .addClass( "arrow" )
                .addClass( feedback.vertical )
                .addClass( feedback.horizontal )
                .appendTo( this );
            }
        }
    });

    //==========================================================================
    //省略表示項目をポップアップで全表示
    //==========================================================================
    $('.ellipsis').tooltip({
        position: {
            my: "left+20 top",
            at: "right bottom",
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" )
                .addClass( "arrow" )
                .addClass( feedback.vertical )
                .addClass( feedback.horizontal )
                .appendTo( this );
            }
        }
    });

    //==========================================================================
    //ステータス
    //==========================================================================
    $('.rpt_status').tooltip({
        position: {
            my: "bottom",
            at: "top",
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" )
                .addClass( "arrow" )
                .addClass( feedback.vertical )
                .addClass( feedback.horizontal )
                .appendTo( this );
            }
        }
    });

    //==========================================================================
    //検索エリアのカレンダー
    //==========================================================================
    $('.calendar_text').datepicker({showOn: 'focus', showOtherMonths: true, selectOtherMonths: true});
    $('.calendar_btn').click(function(){
        $(this).prev('input').datepicker( 'show' );
    });

    //==========================================================================
    //報告書のドラッグアンドドロップ
    //==========================================================================
    $( ".draggable_report" ).draggable({ cursor: "move" ,revert:true });

    //---------------------------------------------------------------
    //ドラッグアイコンの表示・非表示
    //---------------------------------------------------------------
    $("tr.droppable_report").mouseenter(function(){
        if (!is_dragging){
          $(this).find('img.drag_handle').css('display', 'inline');
        }
    }).mouseleave(function(){
        if (!is_dragging){
          $(this).find('img.drag_handle').css('display', 'none');
        }
    });

    //---------------------------------------------------------------
    //報告書を分類ラベルへドラッグ＆ドロップ
    //---------------------------------------------------------------
    $( ".droppable_label" ).droppable({
        hoverClass: "label_hover",
        activate: function( event, ui ) {
            start_dragging(ui.draggable);
        },
        deactivate: function( event, ui ) {
            end_dragging(ui.draggable);
        },
        accept: function( draggable ) {
            var drag_report_id = draggable.prop("id").replace("draggable_report_", "");
            var drop_folder_id = $(this).prop("id");

            //ゴミ箱フォルダでドラッグした場合
            if (folder_id == "delete"){
                //「全て」以外のフォルダへのドラッグはNG
                if(drop_folder_id != ""){
                    return false;
                }
            }

            //分類フォルダでドラッグした場合
            if (folder_id != "" && folder_id != "delete"){
                //「ゴミ箱」フォルダへのドラッグはNG
                if(drop_folder_id == "delete"){
                    return false;
                }
            }

            //「ゴミ箱」フォルダへドラッグした場合
            if(drop_folder_id == "delete"){
                //割付されている報告の場合はNG
                if ($("#link_status_for_" + drag_report_id).val() > 0){
                  return false;
                }
            }

            return true;
        },
        drop: function( event, ui ) {
            //レポートの分類変更
            var drag_report_id = ui.draggable.prop("id").replace("draggable_report_","");
            var drop_folder_id = $(this).prop("id");
            rp_action_change_report_classification(drop_folder_id, drag_report_id);
        }
    });

    //---------------------------------------------------------------
    //割付利用権限がある場合、報告書を報告書へドラッグ＆ドロップ（事案番号関連付け）
    //---------------------------------------------------------------
    if (is_report_classification_usable){
        $( ".droppable_report" ).droppable({
            hoverClass: "report_hover",
            activate: function( event, ui ) {
                start_dragging(ui.draggable);
            },
            deactivate: function( event, ui ) {
                end_dragging(ui.draggable);
            },
            accept: function( draggable ) {
                //「全て」以外のフォルダでドラッグした場合はNG
                if (folder_id != ""){
                    return false;
                }
                
                var drag_report_id = draggable.prop("id").replace("draggable_report_", "");
                var drop_report_id = $(this).prop("id").replace("droppable_report_", "");

                //自身へのドラッグはNG
                if(drag_report_id == drop_report_id){
                    return false;
                }

                //ドラッグ元とドラッグ先が同一リンクの場合はNG
                var report_link_id1 = $("#link_id_for_" + drag_report_id).val();
                var report_link_id2 = $("#link_id_for_" + drop_report_id).val();
                if(report_link_id1 != "" && report_link_id1 == report_link_id2){
                    return false;
                }

                return true;
            },
            drop: function( event, ui ) {
                //レポートリンク追加
                var drag_report_id = ui.draggable.prop("id").replace("draggable_report_", "");
                var drop_report_id = $(this).prop("id").replace("droppable_report_", "");
                rp_action_add_update_same_report_link(drop_report_id,drag_report_id);
            }
        });
    }
});

//ドラッグ開始
function start_dragging(draggable){
    is_dragging = true;

    //選択されていた報告書のIDを保持
    selected_ids = get_selected_list_ids();

    //選択されていた報告書の選択を解除し、ドラッグする報告書を選択
    var drag_tr = draggable.closest('tr');
    var all_tr = drag_tr.closest('table').find('tr');
    all_tr.removeClass('checked');
    all_tr.find('.checkbox').attr('checked', false);
    drag_tr.addClass("checked");
    drag_tr.find('.checkbox').attr('checked', true);

    //アイコンを非表示にしてメッセージを表示
    draggable.find('img').css('display', 'none');
    draggable.find('p').css('display', 'block');
}

//ドラッグ終了
function end_dragging(draggable){
    is_dragging = false;

    //メッセージを非表示
    draggable.find('p').css('display', 'none');

    //ドラッグした報告書の選択を解除
    var drag_tr = draggable.closest('tr');
    drag_tr.removeClass('checked');
    drag_tr.find('.checkbox').attr('checked', false);

    //元々選択されていた報告書があれば選択
    if (selected_ids){
        for (var i=0; i < selected_ids.length; i++){
          var tr = $("#dd1_target_report_" + selected_ids[i]);
          tr.addClass("checked");
          tr.find('.checkbox').attr('checked', true);
        }
    }
}