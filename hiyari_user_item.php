<?
//==============================
//本処理
//==============================

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

$cate_code=10;

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ( $hiyari == "0" ) {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//利用済みグループコードの取得
//==============================
$used_grp_code_list = get_used_grp_code_list($con,$fname);

//==============================
//様式登録済みグループコードの取得
//==============================
$eis_grp_code_list = get_eis_grp_code_list($con,$fname);

//==============================
//グループ情報の取得
//==============================
if($is_postback != "true")
{
    $edit_item_list = get_group_list($con,$fname);
    $line_count = count($edit_item_list);
}

$item_type_list = array(
    'select' => 'セレクトボックス',
    'radio' => 'ラジオボタン',
    'checkbox' => 'チェックボックス',
    'calendar' => 'カレンダー',
    'text' => 'テキストボックス',
    'textarea' => 'テキストエリア',
);

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
    //==============================
    //ポストバックデータより現在の表示内容を作成
    //==============================
    $edit_item_list = "";
    $tmp_other_flgs = array();
    for($line_no=0;$line_no<$line_count;$line_no++)
    {
        $p_grp_code   = "grp_code_".$line_no;
        $p_grp_name = "grp_name_".$line_no;
        $p_item_type  = "item_type_".$line_no;

        $tmp_array = array();

        $tmp_array["grp_code"]   = $$p_grp_code;
        $tmp_array["grp_name"] = $$p_grp_name;
        $tmp_array["item_type"]  = $$p_item_type;

        $edit_item_list[$line_no] = $tmp_array;
    }

    //==============================
    //行追加
    //==============================
    if($postback_mode == "add_line")
    {
        $tmp_array = array();

        $tmp_array["grp_code"]   = '';
        $tmp_array["grp_name"] = '';
        $tmp_array["item_type"]  = '';

        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            //既存行に割り込み追加の場合
            if($add_line_no == $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $tmp_array;
                $new_line_no++;
            }

            $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
            $new_line_no++;
        }

        //最終行に追加の場合
        if($add_line_no == $line_count)
        {
            $tmp_edit_item_list[$add_line_no] = $tmp_array;
            $new_line_no++;
        }

        $edit_item_list = $tmp_edit_item_list;
        $line_count++;

    }
    //==============================
    //行削除
    //==============================
    elseif($postback_mode == "delete_line")
    {
        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            if($delete_line_no != $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                $new_line_no++;
            }
        }
        $edit_item_list = $tmp_edit_item_list;
        $line_count--;
    }

    //==============================
    //ＤＢ更新
    //==============================
    elseif($postback_mode == "update")
    {
        //==============================
        //入力チェック
        //==============================
        //いくつかは全てJavaScriptにて制限しているため、実際にここにかかるケースは通常はないものもある。

        $tmp_used_grp_code_list_count = 0;
        $tmp_eis_grp_code_list_count = 0;
        $used_grp_name_array = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            $check_item = $edit_item_list[$line_no];

            //使用済み項目数のカウント
            if(in_array($check_item["grp_code"],$used_grp_code_list))
            {
                $tmp_used_grp_code_list_count++;
            }

            //様式登録済み項目数のカウント
            if(in_array($check_item["grp_code"],$eis_grp_code_list))
            {
                $tmp_eis_grp_code_list_count++;
            }

            //初期表示文言は空欄ではならない。
            if($check_item["grp_name"] == "")
            {
                $error_message = "初期表示文言が入力されていません。";
                break;
            }

            //初期表示文言が重複してはならない。
            if(in_array($check_item["grp_name"],$used_grp_name_array))
            {
                $error_message = "初期表示文言｢{$check_item["grp_name"]}｣が重複しています。\\n初期表示文言を重複して設定することはできません。";
                break;
            }
            $used_grp_name_array[] = $check_item["grp_name"];
        }

        //使用済みのグループは削除してはならない。
        if($error_message == "")
        {
            if($tmp_used_grp_code_list_count != count($used_grp_code_list))
            {
                $error_message = "既に使用されている項目は削除できません。";
            }
        }
        //様式に登録済みのグループは削除してはならない。
        if($error_message == "")
        {
            if($tmp_eis_grp_code_list_count != count($eis_grp_code_list))
            {
                $error_message = "様式に登録されている項目は削除できません。";
            }
        }

        //==============================
        //入力チェック結果による処理分岐
        //==============================

        //エラーがある場合の処理
        if($error_message != "")
        {
            //ＤＢ更新を行わない。
        }

        //エラーが無い場合の処理
        else
        {
        
            //==============================
            //grp_codeの一覧の取得
            //==============================
            $grp_code_list = get_grp_code_list($con,$fname);

            //==============================
            //grp_codeの採番開始値の取得
            //==============================
            $new_grp_code = get_new_grp_code($con,$fname);

            //==============================
            //トランザクション開始
            //==============================
            pg_query($con,"begin transaction");

            //============================================================
            //グループ定義の更新
            //============================================================

            //==============================
            //デリート処理
            //==============================
            $sql  = "delete from inci_easyinput_group_mst";
            $cond = "where grp_code >= 10000";

            $result = delete_from_table($con, $sql, $cond, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            $sql  = "delete from inci_easyinput_item_mst";
            $cond = "where grp_code >= 10000";

            $result = delete_from_table($con, $sql, $cond, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            $sql  = "delete from inci_easyinput_item_type";
            $cond = "where grp_code >= 10000";

            $result = delete_from_table($con, $sql, $cond, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //==============================
            //インサート処理
            //==============================

            for($line_no=0;$line_no<$line_count;$line_no++)
            {
                //==============================
                //インサートデータの作成
                //==============================
                $update_item = $edit_item_list[$line_no];
                $grp_name = pg_escape_string($update_item['grp_name']);
                $grp_code = $update_item['grp_code'];
                $item_type = $update_item['item_type'];

                //新規追加のgrp_codeを設定
                if($grp_code == "")
                {
                    $grp_code = $new_grp_code;
                    $new_grp_code++;
                    $edit_item_list[$line_no]['grp_code'] = $grp_code;//表示情報保存のためにセット
                } else {
                    if(($idx = array_search($grp_code, $grp_code_list)) !== false) {
                        unset($grp_code_list[$idx]);
                    }
                }

                //==============================
                //グループ定義のインサート
                //==============================
                $sql = "";
                $sql .= " insert into inci_easyinput_group_mst(";
                $sql .= " grp_code,";
                $sql .= " grp_name";
                $sql .= " ) values(";
                $registry_data = array(
                    $grp_code,
                    $grp_name
                );

                $result = insert_into_table($con, $sql, $registry_data, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                //==============================
                //項目定義のインサート
                //==============================
                $sql = "";
                $sql .= " insert into inci_easyinput_item_mst(";
                $sql .= " grp_code,";
                $sql .= " easy_item_code,";
                $sql .= " easy_item_name";
                $sql .= " ) values(";
                $registry_data = array(
                    $grp_code,
                    10,
                    $grp_name
                );

                $result = insert_into_table($con, $sql, $registry_data, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                //==============================
                //項目種別定義のインサート
                //==============================
                $sql = "";
                $sql .= " insert into inci_easyinput_item_type(";
                $sql .= " grp_code,";
                $sql .= " easy_item_code,";
                $sql .= " item_type,";
                $sql .= " item_must_able";
                $sql .= " ) values(";
                $registry_data = array(
                    $grp_code,
                    10,
                    $item_type,
                    '1'
                );

                $result = insert_into_table($con, $sql, $registry_data, $fname);
                if ($result == 0) {
                    pg_query($con,"rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                if ($item_type == 'text' || $item_type == 'textarea' || $item_type == 'calendar'){
                    //==============================
                    //選択項目定義のデリート
                    //==============================
                    $sql  = "delete from inci_report_materials";
                    $cond = "where cate_code = $cate_code and grp_code = $grp_code and easy_item_code = 10";

                    $result = delete_from_table($con, $sql, $cond, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    //==============================
                    //選択項目定義のインサート
                    //==============================
                    $sql = "";
                    $sql .= " insert into inci_report_materials(";
                    $sql .= " cate_code,";
                    $sql .= " grp_code,";
                    $sql .= " easy_item_code,";
                    $sql .= " easy_name,";
                    $sql .= " easy_flag,";
                    $sql .= " easy_num";
                    $sql .= " ) values(";
                    $registry_data = array(
                        $cate_code,
                        $grp_code,
                        10,
                        $grp_name,
                        '1',
                        1
                    );

                    $result = insert_into_table($con, $sql, $registry_data, $fname);
                    if ($result == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }

                if ($item_type == 'select' || $item_type == 'radio' || $item_type == 'checkbox'){
                    //==============================
                    //軸定義のアップデート
                    //==============================
                    $sql = "update inci_axises_title set";
                    $set = array("default_title");
                    $setvalue = array($grp_name);
                    $cond = "where key = 'user_item_{$grp_code}_10'";
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con,"rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }

            //==============================
            //選択項目定義のデリート
            //==============================
            $sql  = "delete from inci_report_materials ";
            $cond = "where inci_report_materials.cate_code = $cate_code";
            $cond .= " and inci_report_materials.grp_code >= 10000";
            $cond .= " and not exists";
            $cond .= " (select 1 from inci_easyinput_group_mst g";
            $cond .= " where g.grp_code >= 10000";
            $cond .= " and g.grp_code = inci_report_materials.grp_code)";

            $result = delete_from_table($con, $sql, $cond, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //==============================
            //軸定義のデリート
            //==============================
            $sql  = "delete from inci_axises_title ";
            $cond = "where key like 'user#_item#_%' escape '#'";
            $cond .= " and not exists (";
            $cond .= "select 1 from inci_easyinput_item_type i";
            $cond .= " where i.grp_code >= 10000 and i.easy_item_code = 10";
            $cond .= " and (i.item_type = 'select' or i.item_type = 'radio' or i.item_type = 'checkbox')";
            $cond .= " and 'user_item_' || i.grp_code || '_10' = inci_axises_title.key)";

            $result = delete_from_table($con, $sql, $cond, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //==============================
            //トランザクション終了
            //==============================
            pg_query($con, "commit");
        }
    }
}

//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 項目設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">

//起動時の処理
function loadaction()
{
    <?
    //更新時エラーがある場合
    if($error_message != "")
    {
    ?>
    alert("<?=$error_message?>");
    <?
    }
    ?>

    <?
    if($postback_mode == "add_line")
    {
    ?>
    var focus_obj_name = "grp_name_<?=$add_line_no?>";
    var focus_obj = document.getElementsByName(focus_obj_name)[0];
    focus_obj.focus();
    <?
    }
    ?>
}

//ＤＢ更新処理
function update_data()
{
    if(update_check())
    {
        document.form1.postback_mode.value = "update";
        document.form1.submit();
    }
}

//行削除処理
function list_delete_line(line_no)
{
    document.form1.postback_mode.value = "delete_line";
    document.form1.delete_line_no.value = line_no;
    document.form1.submit();
}

//行追加処理
function list_add_line(line_no)
{
    document.form1.postback_mode.value = "add_line";
    document.form1.add_line_no.value = line_no;
    document.form1.submit();
}

//削除-使用済みチェック/全非表示チェック
function delete_check(line_no,used_item_flg,eis_item_flg)
{
    if(used_item_flg)
    {
        alert("既に使用されている項目は削除できません。");
        return false;
    }
    if(eis_item_flg)
    {
        alert("様式に登録されている項目は削除できません。");
        return false;
    }
    return true;
}
function update_check()
{
<?
for($line_no=0;$line_no<$line_count;$line_no++)
{
?>
    if(document.form1.grp_name_<?=$line_no?>.value == "")
    {
        alert("初期表示文言が未入力です。");
        return false;
    }
<?
}
?>
    return true;
}

function check_item_edit(item_type, item_type_text)
{
    if (item_type == 'text' || item_type == 'textarea' || item_type == 'calendar'){
        alert(item_type_text+"では項目設定できません。");
        return false;
    }
    return true;
}

function show_item_edit(grp_code, item_type)
{
    var other_code_cnt = 0;
    if (item_type == 'radio' || item_type == 'checkbox'){
        other_code_cnt = 1;
    }
    var url = "hiyari_item_edit.php?session=<?=$session?>&grp_code=" + grp_code + "&easy_item_code=10&other_code_cnt="+other_code_cnt;
    show_rp_sub_window(url);
}

function go_item_edit_used_item(line_no, grp_code, item_type, item_type_text)
{
    if (!check_item_edit(item_type, item_type_text)){
        return false;
    }
    show_item_edit(grp_code, item_type);
}

function go_item_edit(line_no, grp_code)
{
    var obj = document.getElementById('item_type_'+line_no);
    if (obj == null){
        return false;
    }
    var idx = obj.selectedIndex;
    if (idx < 0){
        return false;
    }
    var item_type = obj.options[idx].value;
    if (!check_item_edit(item_type, obj.options[idx].text)){
        return false;
    }

    if (grp_code == null){
        alert("更新してから項目ボタンをクリックください。");
        return false;
    }
    show_item_edit(grp_code, item_type);
}

function show_rp_sub_window(url)
{
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

    window.open(url, '_blank',option);
}
</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadaction()">
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">

<input type="hidden" name="session" value="<?=$session?>">

<input type="hidden" name="line_count" value="<?=$line_count?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="delete_line_no" value="">
<input type="hidden" name="add_line_no" value="">


<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- 本体 START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="600" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">


<!-- 上部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 上部更新ボタン等 START-->



<!-- 一覧 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
    <td width="300" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期表示文言</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行削除</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行追加</font>
    </td>
</tr>

<?
for($line_no=0;$line_no<$line_count;$line_no++){
    $edit_item = $edit_item_list[$line_no];

    $grp_code   = $edit_item["grp_code"];
    $grp_name   = $edit_item["grp_name"];
    $item_type  = $edit_item["item_type"];

    $used_item_flg_script = in_array($grp_code, $used_grp_code_list) ? 'true':'false';
    $eis_item_flg_script = in_array($grp_code, $eis_grp_code_list) ? 'true':'false';
?>
<input type="hidden" name="grp_code_<?=$line_no?>" value="<?=$grp_code?>">
<tr height="22">

<td align="left" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="text" name="grp_name_<?=$line_no?>" value="<?=h($grp_name)?>" style="width:290px" maxlength="50">
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($grp_code != null && in_array($grp_code, $used_grp_code_list)){ ?>
        <input type="hidden" id="item_type_<?=$line_no?>" name="item_type_<?=$line_no?>" value="<?=$item_type?>">
        <?=$item_type_list[$item_type]?>
<? } else { ?>
        <select id="item_type_<?=$line_no?>" name="item_type_<?=$line_no?>">
    <? foreach($item_type_list as $type => $name){ ?>
            <option value="<?=$type?>"<?=($type==$item_type)?" selected":""?>><?=h("$name")?>
    <? } ?>
        </select>
<? } ?>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($grp_code != null && in_array($grp_code, $used_grp_code_list)){ ?>
    <input type="button" value="項目" onclick="go_item_edit_used_item(<?=$line_no?>, <?=$grp_code?>, '<?=$item_type?>', '<?=$item_type_list[$item_type]?>');" >
<? } else { ?>
    <input type="button" value="項目" onclick="go_item_edit(<?=$line_no?>, <?=($grp_code!=null)?$grp_code:'null'?>);" >
<? } ?>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="button" value="行削除" onclick="if(delete_check(<?=$line_no?>,<?=$used_item_flg_script?>,<?=$eis_item_flg_script?>)){list_delete_line('<?=$line_no?>');}" >
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="button" name="add_line" value="行追加" onclick="list_add_line('<?=$line_no+1?>');" >
    </font>
</td>

</tr>
<?
}
?>


</table>
<!-- 一覧 END -->


<!-- 下部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 下部更新ボタン等 START-->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->

</td>
</tr>
</table>
<!-- 本体 END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
pg_close($con);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?

//========================================================================================================================
//内部関数
//========================================================================================================================

/**
 * 当画面の一覧表示データを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array 当画面の一覧表示データの配列。
 */
function get_group_list($con,$fname)
{
    $sql="";
    $sql.=" select";

    $sql.=" m.grp_code,";
    $sql.=" m.grp_name,";
    $sql.=" m_item.item_type";

    /* グループ情報 */
    $sql.=" from ";
    $sql.=" (";
    $sql.=" select * ";
    $sql.=" from inci_easyinput_group_mst";
    $sql.=" where grp_code >= 10000";
    $sql.=" ) m";

    /* 項目種別情報 */
    $sql.=" left join";
    $sql.=" (";
    $sql.=" select grp_code, item_type ";
    $sql.=" from inci_easyinput_item_type";
    $sql.=" where grp_code >= 10000 and easy_item_code = 10";
    $sql.=" ) m_item";
    $sql.=" on m_item.grp_code = m.grp_code";

    $sql.=" order by grp_code";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $tmp_list = pg_fetch_all($sel);

    $result_array = "";
    $line_no = 0;
    foreach($tmp_list as $tmp_data)
    {
        $result_array[$line_no] = $tmp_data;
        $line_no++;
    }

    return $result_array;
}


/**
 * 使用済みのグループコードを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array 使用済みグループコードの配列。
 */
function get_used_grp_code_list($con,$fname,$grp_code,$grp_code)
{
    $sql="";
    $sql.=" select distinct grp_code";
    $sql.=" from inci_easyinput_data";
    $sql.=" where grp_code >= 10000";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $tmp_array = pg_fetch_all($sel);

    $result_list = array();
    foreach($tmp_array as $tmp_data)
    {
        $result_list[] = $tmp_data["grp_code"];
    }

    return $result_list;
}

/**
 * 様式登録済みのグループコードを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array 様式登録済みグループコードの配列。
 */
function get_eis_grp_code_list($con,$fname,$grp_code,$grp_code)
{
    $sql="";
    $sql.=" select distinct use_grp_code";
    $sql.=" from inci_easyinput_set";
    $sql.=" where del_flag = false";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $tmp_array = pg_fetch_all($sel);

    $result_list_all = array();
    foreach($tmp_array as $tmp_data)
    {
        $result_list_all = array_merge($result_list_all, preg_split("/\t/", $tmp_data['use_grp_code'], -1, PREG_SPLIT_NO_EMPTY));
    }

    $tmp_array = array_unique($result_list_all);

    $result_list = array();
    foreach($tmp_array as $tmp_data)
    {
        if ($tmp_data >= 10000){
            $result_list[] = $tmp_data;
        }
    }

    return $result_list;
}

/**
 * 新しいグループコードを採番します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return integer 新しいグループコード。
 */
function get_new_grp_code($con,$fname)
{
    //10000、inci_easyinput_group_mst.grp_codeのうち最大の値+1。
    //つまり、初期項目以外は10000番代が割り当てられる。

    $sql="";
    $sql.=" select max(code1) as new_grp_code from";
    $sql.=" (";
    $sql.=" select max(grp_code)+1 as code1 from inci_easyinput_group_mst";
    $sql.=" union all";
    $sql.=" select 10000 as code1";
    $sql.=" ) t";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_result($sel,0,"new_grp_code");
}

/**
 * ユーザー定義グループを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array ユーザー定義グループの配列。
 */
function get_grp_code_list($con,$fname)
{
    $sql = "select grp_code from inci_easyinput_group_mst";
    $cond = "where grp_code >= 10000";

    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $tmp_list = pg_fetch_all($sel);

    $result_array = array();
    foreach($tmp_list as $tmp_data)
    {
        $result_array[] = $tmp_data['grp_code'];
    }

    return $result_array;
}
?>
