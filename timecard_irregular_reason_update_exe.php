<?
require_once("about_comedix.php");

//-----------------------------------------------------------------------------------------------------
// 新規作成　遅刻早退理由更新
// timecard_overtime_reason_update_exe.php より作成
//-----------------------------------------------------------------------------------------------------

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($reason == "") {
	echo("<script type=\"text/javascript\">alert('遅刻早退理由が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($reason) > 200) {
	echo("<script type=\"text/javascript\">alert('遅刻早退理由が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遅刻早退理由を更新
$sql = "update iregrsn set";
// ----- メディアテック Start -----
$set = array("reason", "ireg_no_deduction_flg", "tmcd_group_id");

if (empty($wktmgrp)) $wktmgrp = null;

$setvalue = array(pg_escape_string($reason), $ireg_no_deduction_flg, $wktmgrp);
// ----- メディアテック End -----
$cond = "where reason_id = $reason_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'timecard_irregular_reason.php?session=$session';</script>");
?>
