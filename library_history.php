<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 文書管理 | 履歴一覧</title>
<?
require("about_session.php");
require("about_authority.php");
require("library_common.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session=="0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($path == "3") ? 63 : 32;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 第1版の文書IDを取得
$sql = "select base_lib_id from libedition";
$cond = "where lib_id = $lib_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$base_lib_id = pg_fetch_result($sel, 0, "base_lib_id");

// ユーザ画面から呼ばれた場合
if ($path != "3") {

	// ログインユーザの職員IDを取得
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");

	// ログインユーザの職員情報を取得
	$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_class = pg_fetch_result($sel, 0, "emp_class");
	$emp_atrb = pg_fetch_result($sel, 0, "emp_attribute");
	$emp_dept = pg_fetch_result($sel, 0, "emp_dept");
	$emp_st = pg_fetch_result($sel, 0, "emp_st");

	// 文書履歴を取得
	$sql = "select e.edition_no, l.lib_id, l.lib_archive, l.lib_cate_id, l.lib_extension, l.lib_up_date from libinfo l inner join libedition e on e.lib_id = l.lib_id inner join libcate c on l.lib_cate_id = c.lib_cate_id";
	$cond = "where e.base_lib_id = $base_lib_id and l.lib_id <> $lib_id and l.lib_delete_flag = 'f' and c.libcate_del_flg = 'f' ";
	$cond .= "and (l.lib_archive != '3' or (l.lib_archive = '3' and (l.private_flg = 'f' or (l.private_flg = 't' and c.lib_link_id = '$emp_class')))) ";
	$cond .= "and (";

		// 参照：所属
		$cond .= "(";
			$cond .= "(l.ref_dept_flg = '1' or (l.ref_dept_flg = '2' and exists (select * from librefdept r where r.lib_id = l.lib_id and r.class_id = $emp_class and r.atrb_id = $emp_atrb and r.dept_id = $emp_dept))) ";
			$cond .= "and ";
			$cond .= "(l.ref_st_flg = '1' or (l.ref_st_flg = '2' and exists (select * from librefst r where r.lib_id = l.lib_id and r.st_id = $emp_st)))";
		$cond .= ") ";

		$cond .= "or ";

		// 参照：兼務所属
		$cond .= "(";
			$cond .= "(l.ref_dept_flg = '1' or (l.ref_dept_flg = '2' and exists (select * from concurrent cc inner join librefdept r on r.class_id = cc.emp_class and r.atrb_id = cc.emp_attribute and r.dept_id = cc.emp_dept where cc.emp_id = '$emp_id' and r.lib_id = l.lib_id))) ";
			$cond .= "and ";
			$cond .= "(l.ref_st_flg = '1' or (l.ref_st_flg = '2' and exists (select * from concurrent cc inner join librefst r on r.st_id = cc.emp_st where cc.emp_id = '$emp_id' and r.lib_id = l.lib_id)))";
		$cond .= ") ";

		$cond .= "or ";

		// 参照：職員
		$cond .= "exists (select * from librefemp r where r.lib_id = l.lib_id and r.emp_id = '$emp_id') ";

		$cond .= "or ";

		// 更新：所属
		$cond .= "(";
			$cond .= "(l.upd_dept_flg = '1' or (l.upd_dept_flg = '2' and exists (select * from libupddept u where u.lib_id = l.lib_id and u.class_id = $emp_class and u.atrb_id = $emp_atrb and u.dept_id = $emp_dept))) ";
			$cond .= "and ";
			$cond .= "(l.upd_st_flg = '1' or (l.upd_st_flg = '2' and exists (select * from libupdst u where u.lib_id = l.lib_id and u.st_id = $emp_st)))";
		$cond .= ") ";

		$cond .= "or ";

		// 更新：兼務所属
		$cond .= "(";
			$cond .= "(l.upd_dept_flg = '1' or (l.upd_dept_flg = '2' and exists (select * from concurrent cc inner join libupddept u on u.class_id = cc.emp_class and u.atrb_id = cc.emp_attribute and u.dept_id = cc.emp_dept where cc.emp_id = '$emp_id' and u.lib_id = l.lib_id))) ";
			$cond .= "and ";
			$cond .= "(l.upd_st_flg = '1' or (l.upd_st_flg = '2' and exists (select * from concurrent cc inner join libupdst u on u.st_id = cc.emp_st where cc.emp_id = '$emp_id' and u.lib_id = l.lib_id)))";
		$cond .= ") ";

		$cond .= "or ";

		// 更新：職員
		$cond .= "exists (select * from libupdemp u where u.lib_id = l.lib_id and u.emp_id = '$emp_id') ";
		$cond .= "or ";
		$cond .= "(l.lib_archive = '1' and c.lib_link_id = '$emp_id') ";
		$cond .= "or ";
		$cond .= "(l.lib_archive = '4' and l.private_flg = 't' and (exists (select * from project p where p.pjt_response = '$emp_id' and p.pjt_delete_flag = 'f' and to_char(p.pjt_id, 'FM999999999999') = c.lib_link_id) or exists (select * from promember m where m.emp_id = '$emp_id' and exists (select * from project p where p.pjt_delete_flag = 'f' and to_char(p.pjt_id, 'FM999999999999') = c.lib_link_id) and to_char(m.pjt_id, 'FM999999999999') = c.lib_link_id)))";

	$cond .= ")";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 文書履歴を配列に格納
	$docs = array();
	while ($row = pg_fetch_array($sel)) {
		$docs[$row["edition_no"]] = array(
			"id" => $row["lib_id"],
			"path" => get_document_path($row["lib_archive"], $row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]),
			"modified" => $row["lib_up_date"]
		);
	}

// 管理画面から呼ばれた場合
} else {

	// 文書履歴を取得
	$sql = "select e.edition_no, l.lib_id, l.lib_archive, l.lib_cate_id, l.lib_extension, l.lib_up_date from libinfo l inner join libedition e on e.lib_id = l.lib_id";
	$cond = "where e.base_lib_id = $base_lib_id and l.lib_delete_flag = 'f' and l.lib_id <> $lib_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 文書履歴を配列に格納
	$docs = array();
	while ($row = pg_fetch_array($sel)) {
		$docs[$row["edition_no"]] = array(
			"id" => $row["lib_id"],
			"path" => get_document_path($row["lib_archive"], $row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]),
			"modified" => $row["lib_up_date"]
		);
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>履歴一覧</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="80" align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">版番号</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照</font></td>
</tr>
<?
krsort($docs);//版番号(キー)の降順に表示
foreach($docs as $i=>$tmp_doc) {
	if (strlen($tmp_doc["modified"]) == 14) {
		$modified = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $tmp_doc["modified"]);
	} else if (strlen($tmp_doc["modified"]) == 8) {
		$modified = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_doc["modified"]);
	} else {
		$modified = "";
	}
	$tmp_lib_url = urlencode($tmp_doc["path"]);
	$tmp_btn = "<input type=\"button\" value=\"参照\" onclick=\"window.open('library_refer.php?s=$session&i={$tmp_doc["id"]}&u=$tmp_lib_url');\">";

	echo("<tr height=\"22\">\n");
	echo("<td align=\"right\" style=\"padding-right:6px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$i</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$modified</font></td>\n");
	echo("<td align=\"center\">$tmp_btn</td>\n");
	echo("</tr>\n");
}
?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
