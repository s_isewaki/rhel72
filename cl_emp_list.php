<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	showLoginPage();
	exit;
}

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

// 呼び元画面が「病棟評価表」画面の場合
if($call_screen == "WARD_GRADE")
{
    $arr_empmst = $obj->get_empmst($session);
    $login_emp_id = $arr_empmst[0]["emp_id"];

    $employee_div = "";

    // 該当職員が「評価者」どうか判断する
    if($obj->is_grader($login_emp_id))
    {
        $employee_div = "GRADER";
    }
    // 該当職員が「師長」どうか判断する
    if($obj->is_nursing_commander($login_emp_id))
    {
        $employee_div = "NURSING_COMMANDER";
    }
    // 該当職員が「特定職員」どうか判断する
    if($obj->is_particular_employee($login_emp_id))
    {
        $employee_div = "PARTICULAR_EMPLOYEE";
    }
}


if($is_postback)
{
	//======================================
	// 職員情報取得
	//======================================
/*
	if($query != "")
	{
*/
		$arr_emp_list = $obj->get_emp_list_book(pg_escape_string($query), $employee_div, $login_emp_id);
/*
	}
	else
	{
		$is_postback = false;
	}
*/
}


$cl_title = cl_title_name();


//==============================
// HTML
//==============================
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | 職員検索</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

// 検索処理
function search()
{
//	document.mainform.is_postback.value = true;
	document.mainform.action="cl_emp_list.php?session=<?=$session?>&call_screen=<?=$call_screen?>&is_postback=true";
	document.mainform.submit();
}

// コピー処理
function copy_emp(emp_id, emp_nm)
{
	if(window.opener && !window.opener.closed && window.opener.add_target_list)
    {
          window.opener.add_target_list('<?=$input_div?>', emp_id, emp_nm);
    }
}

function copy_emp_place(emp_id, emp_nm)
{
	var obj;

	set_wm_counter(emp_id);

	if (document.mainform.emp_id_field.value == "" && document.mainform.emp_nm_field.value == "")
	{

		copy_emp(emp_id, emp_nm);

	} else {

		if (window.opener.document.getElementById(document.mainform.emp_id_field.value))
		{
			obj = window.opener.document.getElementById(document.mainform.emp_id_field.value);
			if (obj.type == "text" || obj.type == "hidden")
			{
				obj.value = emp_id;
			} else {
				obj.innerHTML = emp_id;
			}
		}

		if (window.opener.document.getElementById(document.mainform.emp_nm_field.value))
		{
			obj = window.opener.document.getElementById(document.mainform.emp_nm_field.value);
			if (obj.type == "text" || obj.type == "hidden")
			{
				obj.value = emp_nm;
			} else {
				obj.innerHTML = emp_nm;
			}
		}
	}
}

// ajax 「よく使う人」カウントアップ処理
function set_wm_counter(ids)
{

	var url = 'cl_wm_counter.php';
	var params = $H({'session':'<?=$session?>','used_emp_ids':ids}).toQueryString();
	var myAjax = new Ajax.Request(
		url,
		{
			method: 'post',
			postBody: params
		});
}

// 2012/10/03 Yamagawa add(s)
function submitCheck(e) {

	if (!e) {
		var e = window.event;
	}

	if (e.keyCode == 13) {
		search();
	}

}
// 2012/10/03 Yamagawa add(e)

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" OnLoad="document.mainform.query.focus();">
<form name="mainform" action="#" method="post">

<!--
<input type="hidden" name="is_postback" value="<?=$is_postback?>">
-->
<input type="hidden" name="input_div" value="<?=$input_div?>">
<input type="hidden" id="emp_id_field" name="emp_id_field" value="<?=$emp_id_field?>">
<input type="hidden" id="emp_nm_field" name="emp_nm_field" value="<?=$emp_nm_field?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#35B341">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>職員検索</b></font></td>
<td>&nbsp</td>
<td width="10">&nbsp</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="10" height="10" alt=""><br>

<?
// 検索タブ表示
show_emp_list_header_tab($session, $fname, $input_div, $call_screen);
?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<!-- ここから -->

<table width="100%" border="0" cellspacing="0" cellpadding="5" class="list">
<tr height="30" >
<td bgcolor="#DFFFDC">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名&nbsp;</font>
<input type="text" name="query" maxlength="15" value="<?=$query?>" style="ime-mode: active;" onKeyPress="submitCheck(event);">&nbsp;
<input type="button" value="検索" onclick="search();">
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#F5FFE5"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
</tr>
</table>
<?
if($is_postback)
{

if(count($arr_emp_list) > 0)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#DFFFDC">
<td width="50"></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></td>
<td width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
</tr>
<?
foreach($arr_emp_list as $emp_info) {
?>
<tr height="22" bgcolor="#FFFFFF">
<!--
<td align="center"><input type="button" value="コピー" onclick="copy_emp('<?=$emp_info["emp_id"]?>','<?=$emp_info["emp_name"]?>');"></td>
-->
<td align="center"><input type="button" value="コピー" onclick="copy_emp_place('<?=$emp_info["emp_id"]?>','<?=$emp_info["emp_name"]?>');"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($emp_info["emp_name"])?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($emp_info["belong_to"])?></font></td>
</tr>
<?
}
?>
</table>
<?
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr bgcolor="#F5FFE5"><td><b>該当する人はいませんでした</b></td></tr>
</table>
<?
}
}
?>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</form>
</body>
</html>




<?
//==============================
// 職員名簿情報取得
//==============================
function get_emp_list($con, $fname, $query) {

	$arr = array();

	$query = str_replace(" ", "", $query);
	$query = str_replace("　", "", $query);
	$query_len = strlen($query);

	$sql = "select varchar(1) '1' as mail_type, empmst.emp_id, empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as name, login.emp_login_id as email, login.emp_login_mail, (select class_nm from classmst where class_id = empmst.emp_class) as class_nm, (select atrb_nm from atrbmst where atrb_id = empmst.emp_attribute) as atrb_nm, (select dept_nm from deptmst where dept_id = empmst.emp_dept) as dept_nm, (select room_nm from classroom where room_id = empmst.emp_room) as room_nm, (select site_id from config) as site_id from login inner join empmst on empmst.emp_id = login.emp_id where exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f') ";
	if ($query_len > 0) {
		$sql .= "and (empmst.emp_lt_nm || empmst.emp_ft_nm like '%$query%' or empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like '%$query%' or login.emp_login_id like '%$query%' or emp_email2 like '%$query%') ";
		// ウェブメールと異なりemailも条件に追加
	}

	$sql .= "order by emp_id, 1";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	while($row = pg_fetch_array($sel))
	{

        $belong_to = $row["class_nm"] . " > " . $row["atrb_nm"] . " > " . $row["dept_nm"];
        if ($row["room_nm"] != '') {
            $belong_to .= " > " . $row["room_nm"];
        }

		$arr[] = array(
                        "emp_id"    =>  $row["emp_id"],
						"emp_name"  => $row["name"],
                        "belong_to" => $belong_to
					   );

	}
	return $arr;
}


//==============================
//データベース接続を閉じる
//==============================
pg_close($con);
?>
