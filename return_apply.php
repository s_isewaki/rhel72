<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require("show_clock_in_common.ini");
require_once("about_postgres.php");
require_once("show_return_template.ini");
require_once("timecard_bean.php");

$fname = $PHP_SELF;

// 日付の設定
if ($date == "") {$date = date("Ymd");}

// データベースに接続
$con = connect2db($fname);

if ($session != "") {

	// セッションのチェック
	$session = qualify_session($session, $fname);
	if ($session == "0") {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}

	// ログインユーザの職員IDを取得
	$emp_id = get_emp_id($con, $session, $fname);
} else {

	// 職員の存在チェック
	$sql = "select emp_id from login";
	$cond = "where emp_login_id = '$id' and emp_login_pass = '$pass' and exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}

// 承認者が設定されているか確認
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select return_id from tmcdwkfw where emp_id = '$emp_id')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$return_id = pg_fetch_result($sel, 0, "emp_id");
	$return_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
}

// 承認者ワークフロー情報取得
$sql = "select * from atdbk_wkfw";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$rtn_wkfw_div = pg_fetch_result($sel, 0, "rtn_wkfw_div");
$wkfw_approve_num = pg_fetch_result($sel, 0, "rtncfm");


// 承認者が設定されている場合
if ($rtn_wkfw_div == "1" || $return_id != "") {

	// 勤務実績情報を取得
	$arr_result = get_atdbkrslt($con, $emp_id, $date, $fname);

	// 退勤後復帰理由一覧を取得
	$sql = "select reason_id, reason from rtnrsn";
	$cond = "where del_flg = 'f' order by reason_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";
?>
<title>CoMedix タイムカード | <?=$ret_str?>申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setOtherReasonDisabled() {
	if (document.getElementById('other_reason')) {
		document.getElementById('other_reason').style.display = (document.mainform.reason_id.value == 'other') ? '' : 'none';
	}
}

// 申請処理
function apply_regist() {

    var approve_num = 'approve_num';
    var apv_num = document.mainform.elements[approve_num].value;

	// 承認者が全く存在しない場合
    var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.mainform.elements[obj].value;

		if(emp_id != "") {
			regist_flg = true;
            break;
		}
	}

    if(!regist_flg)
    {
        alert('承認者が存在しないため、申請できません。');
        return;
    }

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.mainform.elements[obj_src].value;

        if(emp_id_src == '')
        {
            continue;
        }

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.mainform.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('承認者が重複していますが、申請しますか？'))
		{
			return;
		}
	}
	
	document.mainform.submit();

}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="setOtherReasonDisabled();">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$ret_str?>申請</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? if ($rtn_wkfw_div != "1" && $return_id == "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認者が設定されていません。承認者設定画面で<?=$ret_str?>申請の承認者を設定してください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="mainform" action="return_apply_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date)); ?></font></td>
</tr>
<?
for ($i = 1; $i <= 10; $i++) {
	if ($arr_result["o_start_time$i"] == "") {
		break;
	}
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$ret_str?>時刻<? echo($i); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["o_start_time$i"])); ?> 〜 <? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["o_end_time$i"])); ?></font></td>
</tr>
<?
}
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由</font></td>
<td>
<select name="reason_id" onchange="setOtherReasonDisabled();">
<?
while ($row = pg_fetch_array($sel)) {
	$tmp_reason_id = $row["reason_id"];
	$tmp_reason = $row["reason"];

	echo("<option value=\"$tmp_reason_id\">$tmp_reason\n");
}
?>
<option value="other">その他
</select><br>
<div id="other_reason" style="display:none;"><input type="text" name="reason" value="" size="50" maxlength="100" style="ime-mode:active;"></div>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" align="center">

<?
$wkfw_flg = ($rtn_wkfw_div != "1") ? false : true;
show_return_template($con, $session, $fname, $emp_id, $wkfw_flg);
?>
</td>
</tr>
</table>







<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="申請" onclick="apply_regist();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">

<input type="hidden" name="wkfw_approve_num" value="<? echo($wkfw_approve_num); ?>">


</form>
<? } ?>
</center>
</body>
<? pg_close($con); ?>
</html>
