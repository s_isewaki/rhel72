<!-- ************************************************************************ -->
<!-- 出勤表用勤務シフト作成　共通ＣＬＡＳＳ -->
<!-- ************************************************************************ -->

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");


class atdbk_duty_shift_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function atdbk_duty_shift_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// 全項目リストを初期化
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（ＤＢ読み込み）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 指定職員が勤務シフトの施設基準に適用しているかを判定
	// @param	$emp_id			職員ID
	//
	// @return	$ret			判定フラグ（true：適用している、false：適用していない）
	/*************************************************************************/
	function chk_institution_standard($emp_id) {

		$fname = $this->file_name;

		///-----------------------------------------------------------------------------
		//勤務シフトのライセンス権限判定
		///-----------------------------------------------------------------------------
		$sql = "select * from license";
		$cond = "where 1=1 ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$lcs_func9 = pg_fetch_result($sel, 0, "lcs_func9");
		///-----------------------------------------------------------------------------
		//勤務シフトのライセンス権限があり、勤務シフトに登録された職員の場合
		///-----------------------------------------------------------------------------
		$group_id_array = array();
		$standard_flg = false;
		if ($lcs_func9 == "t") {
			//-------------------------------------------------------------------
			//ＳＥＬＥＣＴ（勤務シフトスタッフ情報より職員が所属する全勤務シフトグループIDを取得）
			//-------------------------------------------------------------------
			$sql = "select * from duty_shift_staff";
			$cond = "where emp_id = '$emp_id' ";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$num = pg_numrows($sel);
			for ($i=0;$i<$num;$i++) {
				$group_id_array[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
			}
			//-------------------------------------------------------------------
			// 取得グループの施設基準の有無を判定
			//-------------------------------------------------------------------
			for ($i=0; $i<count($group_id_array); $i++) {
				$group_id = $group_id_array[$i]["group_id"];
				//-------------------------------------------------------------------
				//ＳＥＬＥＣＴ（勤務シフトグループ情報より施設基準IDを取得）
				//-------------------------------------------------------------------
				$sql = "select * from duty_shift_group";
				$cond = "where group_id = '$group_id' ";
				$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
				if ($sel == 0) {
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$standard_id = trim(pg_fetch_result($sel, 0, "standard_id"));
				//-------------------------------------------------------------------
				//施設基準IDが設定時
				//-------------------------------------------------------------------
				if ($standard_id != "") {
					$standard_flg = true;
				}
			}
		}
		return $standard_flg;
	}
}
?>

