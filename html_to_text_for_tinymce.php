<?
// 以下の関数はファントルくんのhiyari_mail_input.iniのコピー。
// メドレポート等で使用。
/**
 * HTML文字列をTEXTに変換します。(TinyMCE用)。
 * 注意：この変換は全てのHTMLを完全に変換するものではありません。
 * 
 * @param string $htmlstr ＨＴＭＬ文字列
 * @return string TEXT文字列
 */
function html_to_text_for_tinymce($htmlstr)
{
	$str = $htmlstr;
	
	//改行
	$str = str_replace("<br />","\n",$str);
	
	//改段落
	$str = str_replace("</p>","\n\n",$str);
	
	//テーブル
	$str = str_replace("</td>"," ",$str);
	$str = str_replace("</tr>","\n",$str);
	
	//列挙
	$str = str_replace("<li>"," ・",$str);
	$str = str_replace("</li>","\n",$str);
	$str = str_replace("</ul>","\n",$str);
	$str = str_replace("</ol>","\n",$str);
	
	//水平バー
	$str = str_replace("<hr />","\n----------\n",$str);
	
	//スペース
	$str = str_replace("&nbsp;"," ",$str);
	
	//特殊記号(「&」で始まり、小文字アルファベットが続き、「;」で終わる。)
	//ちなみに、「↓」等の記号文字は「&darr;」となっているため、この対象となる。
	$str = preg_replace("/&[a-z].*;/", "＊", $str);
	
	//インデントや位置指定などは無視。
	//顔画像の顔文字化もしない。
	//TinyMCEコマンド以外で入れられたタグは無視。
	
	//タグ抜き
	$str = strip_tags($str);
	
	return $str;
}

//--------------------------------
//文字化け対応
//--------------------------------
function html2text4tinymce2($htmlstr, $useTinyMCE, $flg=0)
{
  $str = $htmlstr ;
  if (!$useTinyMCE) return $str ;

  // 改行を示すタグはここで明示的に改行に変換する
  $str = str_replace("</p>",   "\n", $str) ;
  $str = str_replace("<br>",   "\n", $str) ;
  $str = str_replace("<br />", "\n", $str) ;

  // タグで括られたテキストをタグごと取り除く(書式と無関係なタグはTinyMCEによってあらかじめHTMLエンコードされているので残る)
	if($flg != 1) $str = strip_tags($str) ;

  // ｢×｣｢÷｣などhtml_entity_decodeでうまくデコードできない文字はここで変換する(必要に応じて書き足す)
  $str = str_replace(    "&nbsp;",   " ",  $str) ;
  $str = mb_ereg_replace("&times;",  "×", $str) ;
  $str = mb_ereg_replace("&divide;", "÷", $str) ;

  // html_entity_decodeでデコードする
  $str = html_entity_decode($str, ENT_QUOTES) ;

  // html_entity_decodeでもデコードできないHTML特殊文字は手動で変換する(必要に応じて書き足す)
  $str = mb_ereg_replace("&rarr;",   "→", $str) ;
  $str = mb_ereg_replace("&larr;",   "←", $str) ;
  $str = mb_ereg_replace("&uarr;",   "↑", $str) ;
  $str = mb_ereg_replace("&darr;",   "↓", $str) ;
  $str = mb_ereg_replace("&hArr;",   "⇔", $str) ;
  $str = mb_ereg_replace("&ne;",     "≠", $str) ;
  //ギリシャ文字
  $str = mb_ereg_replace("&alpha;",  "α", $str) ;
  $str = mb_ereg_replace("&beta;",   "β", $str) ;
  $str = mb_ereg_replace("&gamma;",  "γ", $str) ;
  $str = mb_ereg_replace("&delta;",  "δ", $str) ;
  $str = mb_ereg_replace("&epsilon;","ε", $str) ;
  $str = mb_ereg_replace("&zeta;",   "ζ", $str) ;
  $str = mb_ereg_replace("&eta;",    "η", $str) ;
  $str = mb_ereg_replace("&theta;",  "θ", $str) ;
  $str = mb_ereg_replace("&iota;",   "ι", $str) ;
  $str = mb_ereg_replace("&kappa;" , "κ", $str) ;
  $str = mb_ereg_replace("&lambda;", "λ", $str) ;
  $str = mb_ereg_replace("&mu;",     "μ", $str) ;
  $str = mb_ereg_replace("&nu;",     "ν", $str) ;
  $str = mb_ereg_replace("&xi;",     "ξ", $str) ;
  $str = mb_ereg_replace("&omicron;","ο", $str) ;
  $str = mb_ereg_replace("&pi;",     "π", $str) ;
  $str = mb_ereg_replace("&rho;",    "ρ", $str) ;
  $str = mb_ereg_replace("&sigma;",  "σ", $str) ;
  $str = mb_ereg_replace("&tau;",    "τ", $str) ;
  $str = mb_ereg_replace("&upsilon;","υ", $str) ;
  $str = mb_ereg_replace("&phi;",    "φ", $str) ;
  $str = mb_ereg_replace("&chi;",    "χ", $str) ;
  $str = mb_ereg_replace("&psi;",    "ψ", $str) ;
  $str = mb_ereg_replace("&omega;",  "ω", $str) ;
  $str = mb_ereg_replace("&Alpha;",  "Α", $str) ;
  $str = mb_ereg_replace("&Beta;",   "Β", $str) ;
  $str = mb_ereg_replace("&Gamma;",  "Γ", $str) ;
  $str = mb_ereg_replace("&Delta;",  "Δ", $str) ;
  $str = mb_ereg_replace("&Epsilon;","Ε", $str) ;
  $str = mb_ereg_replace("&Zeta;",   "Ζ", $str) ;
  $str = mb_ereg_replace("&Eta;",    "Η", $str) ;
  $str = mb_ereg_replace("&Theta;",  "Θ", $str) ;
  $str = mb_ereg_replace("&Iota;",   "Ι", $str) ;
  $str = mb_ereg_replace("&Kappa;",  "Κ", $str) ;
  $str = mb_ereg_replace("&Lambda;", "Λ", $str) ;
  $str = mb_ereg_replace("&Mu;",     "Μ", $str) ;
  $str = mb_ereg_replace("&Nu;",     "Ν", $str) ;
  $str = mb_ereg_replace("&Xi;",     "Ξ", $str) ;
  $str = mb_ereg_replace("&Omicron;","Ο", $str) ;
  $str = mb_ereg_replace("&Pi;",     "Π", $str) ;
  $str = mb_ereg_replace("&Rho;",    "Ρ", $str) ;
  $str = mb_ereg_replace("&Sigma;",  "Σ", $str) ;
  $str = mb_ereg_replace("&Tau;",    "Τ", $str) ;
  $str = mb_ereg_replace("&Upsilon;","Υ", $str) ;
  $str = mb_ereg_replace("&Phi;",    "Φ", $str) ;
  $str = mb_ereg_replace("&Chi;",    "Χ", $str) ;
  $str = mb_ereg_replace("&Psi;",    "Ψ", $str) ;
  $str = mb_ereg_replace("&Omega;",  "Ω", $str) ;

  return $str ;
}
function htmlspecialchars4tinymce2($htmlstr, $useTinyMCE)
{
  $str = $htmlstr ;
  if (!$useTinyMCE) return htmlspecialchars($str, ENT_COMPAT, 'ISO-8859-1') ;
  // まず、必要なタグだけを残して、その他のすべてのタグをstrip_tagsで取り除く。
  // (書式と無関係な｢<｣や｢>｣などは、TinyMCEによってあらかじめHTMLエンコード
  //  されている(｢&lt;｣や｢&gt;｣など)ので、書式と無関係なタグは取り除かれない。)
  // そして、基本的にはTinyMCEが吐き出したテキストをそのままXMLに登録する。
  $str = str_replace("</p>",      "<br />",          $str) ;
  $str = str_replace("<br>",      "<br />",          $str) ;
  $str = str_replace("<br />",    "&lt;br /&gt;",    $str) ;
  $str = str_replace("<strike>",  "&lt;strike&gt;",  $str) ;
  $str = str_replace("</strike>", "&lt;/strike&gt;", $str) ;
  $str = strip_tags($str) ;
  return $str ;
}
?>
