<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");


//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// トランザクションを開始
pg_query($con, "begin");

$sql  = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond , $fname);
if($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$self_emp_id = pg_fetch_result($sel, 0, "emp_id");

$arr_used_emp_id = split(",", $used_emp_ids);
foreach($arr_used_emp_id as $used_emp_id)
{

	$sql  = "select count_num from wm_counter";
	$cond = "where emp_id = '$self_emp_id' and used_emp_id = '$used_emp_id'";
    $sel  = select_from_table($con, $sql, $cond, $fname);
	if($sel == 0)
	{
		pg_exec($con, "rollback");
		pg_close($con);
		exit;
	}
	
	$rec_cnt = pg_num_rows($sel);

	if($rec_cnt == 0)
	{
		$sql = "insert into wm_counter (emp_id, used_emp_id, count_num) values (";
		$content = array($self_emp_id, $used_emp_id, 1);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_exec($con, "rollback");
			pg_close($con);
			exit;
		}
	}
	else
	{
		$count_num = pg_fetch_result($sel,0,"count_num");
		$count_num = $count_num + 1;

		$sql = "update wm_counter set";
		$set = array("count_num");
		$setvalue = array($count_num);
	   	$cond = "where emp_id = '$self_emp_id' and used_emp_id = '$used_emp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_exec($con,"rollback");
			pg_close($con);
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>


