<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 連絡先更新</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");
require_once("employee_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 文字コードのデフォルトはShift_JIS
if ($encoding == "") {$encoding = "1";}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	return true;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? show_menu2(); ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="employee_contact_bulk_insert.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? if ($result != "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<? } ?>
<? if ($result == "f") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">登録処理が実行できませんでした。</font></td>
<? } else if ($result == "t") {?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。</font></td>
<? } ?>
<? if ($result != "") { ?>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<dl style="margin-top:0;">
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>注意</b></font></dt>
<dd>
<ul style="margin:0;">
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員IDに該当する職員が存在しない場合、そのレコードは無視されます。</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名は更新されません。</font></li>
</ul>
</dd>
</dl>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></dt>
<dd><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記項目をカンマで区切り、レコードごとに改行します。職員IDのみ必須です。</font></dd>
<dd>
<ul>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名……形式不問（ただし姓と名を別カラムにしないこと）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">携帯端末・外線……半角数字12桁以内（別途ハイフンを使用可能）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線番号……半角10桁以内（文字種問わず）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内PHS/所内PHS
echo $_label_by_profile["PHS"][$profile_type];
?>……半角6桁以内（同上）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">E-Mail</font></li>
<?
require_once(dirname(__FILE__)."/class/Cmx/Core2/C2DB.php");
// 拡張empmst項目1から5のラベル取得
$sql = "select * from spfm_field_struct where table_id = 'spft_empmst' and field_id like 'empmst_ext%'";
$_rows = c2dbGetRows($sql);
$fieldLabels = array();
foreach ($_rows as $row) $fieldLabels[$row["field_id"]] = $row["field_label"];

// 拡張empmst項目1から5の表示可能判定と編集可能判定を取得
// ・利用判定：disp_empがall_okか、そうでないかを判定
// ・編集判定：edit_empがall_okか、そうでないかを判定
$sql =
" select field_id from spfm_field_struct_auth".
" where table_id = 'spft_empmst' and field_id like 'empmst_ext%'".
" and target_place = 'all' and disp_emp = 'all_ok'".
" order by order_no";
$fieldStructAuthRows = c2dbGetRows($sql);
$comma = "";
foreach ($fieldStructAuthRows as $row) {
	$comma .= ",";
	echo '<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.$fieldLabels[$row["field_id"]].'</font></li>';
}
?>
</ul>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">例</font></dt>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">0000000001,山田太郎,090-0000-0000,123,456,t-yamada@example.com<?=$comma?></font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">0000000002,すずき　はなこ,09011111111,,,<?=$comma?></font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">0000000003,,,789,,<?=$comma?></font></dd>
</dl>
</dd>
</dl>
</td>
</tr>
</table>
</body>
</html>
