<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 残業申請参照</title>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_clock_in_common.ini");
require_once("show_overtime_apply_detail.ini");
require_once("show_overtime_apply_history.ini");
require_once("application_imprint_common.ini");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("timecard_common_class.php");
require_once("ovtm_class.php"); //20140619

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$ovtm_class = new ovtm_class($con, $fname); //20140619

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}

// 残業申請情報を取得
$sql = "select ".
    "empmst.emp_lt_nm, ".
    "empmst.emp_ft_nm, ".
    "ovtmapply.target_date, ".
    "ovtmrsn.reason, ".
    "ovtmapply.reason as other_reason, ".
    "ovtmapply.apply_time, ".
    "ovtmapply.comment, ".
    "ovtmapply.end_time, ".
    "ovtmapply.next_day_flag, ".
    "ovtmapply.previous_day_flag, ".
    "ovtmapply.start_time, ".
    "ovtmapply.over_start_time, ".
    "ovtmapply.over_end_time, ".
    "ovtmapply.over_start_next_day_flag, ".
    "ovtmapply.over_end_next_day_flag, ".
    "ovtmapply.reason_detail, ".
    "ovtmapply.over_start_time2, ".
    "ovtmapply.over_end_time2, ".
    "ovtmapply.over_start_next_day_flag2, ".
    "ovtmapply.over_end_next_day_flag2 ";
for ($i=3; $i<=5; $i++) {
    $sql .=	", ovtmapply.over_start_time{$i}";
    $sql .=	", ovtmapply.over_end_time{$i}";
    $sql .=	", ovtmapply.over_start_next_day_flag{$i}";
    $sql .=	", ovtmapply.over_end_next_day_flag{$i}";
}
for ($i=1; $i<=5; $i++) {
    $sql .=	", ovtmapply.rest_start_time{$i}";
    $sql .=	", ovtmapply.rest_end_time{$i}";
    $sql .=	", ovtmapply.rest_start_next_day_flag{$i}";
    $sql .=	", ovtmapply.rest_end_next_day_flag{$i}";
}
//理由追加 20150106
for ($i=2; $i<=5; $i++) {
    $sql .=	", ovtmapply.reason_id{$i}";
    $sql .=	", ovtmapply.reason{$i}";
}
$sql .= " from ".
    "(ovtmapply inner join empmst on ovtmapply.emp_id = empmst.emp_id) ".
    "left join ovtmrsn on ovtmapply.reason_id = ovtmrsn.reason_id";
$cond = "where ovtmapply.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$target_date = pg_fetch_result($sel, 0, "target_date");
$reason = pg_fetch_result($sel, 0, "reason");
$other_reason = pg_fetch_result($sel, 0, "other_reason");
$apply_time = pg_fetch_result($sel, 0, "apply_time");
$comment = pg_fetch_result($sel, 0, "comment");
$end_time = pg_fetch_result($sel, 0, "end_time");
$next_day_flag = pg_fetch_result($sel, 0, "next_day_flag");
$previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
$start_time = pg_fetch_result($sel, 0, "start_time");
$over_start_time = pg_fetch_result($sel, 0, "over_start_time");
$over_end_time = pg_fetch_result($sel, 0, "over_end_time");
//残業終了が未設定時は退勤時刻
if ($over_end_time == "") {
	$over_end_time = $end_time;
}
$over_start_next_day_flag = pg_fetch_result($sel, 0, "over_start_next_day_flag");
$over_end_next_day_flag = pg_fetch_result($sel, 0, "over_end_next_day_flag");
$reason_detail = pg_fetch_result($sel, 0, "reason_detail");

$over_start_time2 = pg_fetch_result($sel, 0, "over_start_time2");
$over_end_time2 = pg_fetch_result($sel, 0, "over_end_time2");
$over_start_next_day_flag2 = pg_fetch_result($sel, 0, "over_start_next_day_flag2");
$over_end_next_day_flag2 = pg_fetch_result($sel, 0, "over_end_next_day_flag2");
//ovtmapplyをまとめて取得
$arr_ovtmapply = pg_fetch_array($sel, 0, PGSQL_ASSOC);

//個人別勤務時間帯履歴対応 20130220 start
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $target_date, $target_date);
// 勤務実績情報を取得
$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname, "1", $target_date);
$emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $target_date, $arr_result["tmcd_group_id"], $arr_result["pattern"]);
if ($emp_officehours["office_start_time"] != "") {
    $arr_result["office_start_time"] = $emp_officehours["office_start_time"];
    $arr_result["office_end_time"] = $emp_officehours["office_end_time"];
}
//個人別勤務時間帯履歴対応 20130220 end
//残業開始が未設定時は所定終了時刻
if ($over_start_time == "") {
	$over_start_time = $arr_result["office_end_time"];
}
$update_emp_id = $arr_result["update_emp_id"];
//所属
$update_org_name = get_orgname($con, $fname, $update_emp_id, $timecard_bean);
if ($update_org_name != "") {
	$update_org_name = substr($update_org_name, 1);
	$update_org_name = str_replace(" ", " &gt; ", $update_org_name);
}
//更新者氏名
$update_emp_name = get_emp_kanji_name($con, $update_emp_id, $fname);

$update_time = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/","$1年$2月$3日 $4:$5",$arr_result["update_time"]);

//前日か判定
$previous_day_value = "";
if($previous_day_flag == 1){
	$previous_day_value = "　前日";
}
else {
    $previous_day_value = "　　　　";
}

//翌日か判定
$next_day_value = "";
if($next_day_flag == 1){
	$next_day_value = "　翌日";
}
else {
    $next_day_value = "　　　　";
}
$over_start_next_day_flag_value = "";
if ($over_start_next_day_flag == 1){
	$over_start_next_day_flag_value = "　翌日";
}
$over_end_next_day_flag_value = "";
if ($over_end_next_day_flag == 1){
	$over_end_next_day_flag_value = "　翌日";
}
$over_start_next_day_flag2_value = "";
if ($over_start_next_day_flag2 == 1){
	$over_start_next_day_flag2_value = "　翌日";
}
$over_end_next_day_flag2_value = "";
if ($over_end_next_day_flag2 == 1){
	$over_end_next_day_flag2_value = "　翌日";
}

$arr_ovtm_data = $timecard_common_class->get_ovtm_month_total($emp_id, $target_date, "refer");

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function history_select(apply_id, target_apply_id) {

	document.apply.apply_id.value = apply_id;
	document.apply.target_apply_id.value = target_apply_id;
	document.apply.action="overtime_refer.php?session=<?=$session?>";
	document.apply.submit();
}

// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="resize_history_tbl();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>残業申請参照</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="page_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>


<form name="apply" action="" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="140"> <? // 25% ?>
<? show_overtime_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td><img src="img/spacer.gif" width="3" height="2" alt=""></td>
<td valign="top" width=""> <? // 75% ?>
<div id="dtl_tbl">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<? // width="180" ?>
<td width="90" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($apply_emp_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\d{2}$/", "$1/$2/$3 $4:$5", $apply_time)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $target_date)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">始業時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_start_time"])); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $start_time).$previous_day_value); 
//打刻
if ($ovtm_class->ovtm_tab_flg != "t") {
    echo "　打刻　";
    $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["start_btn_time"]);
    echo $wk_str;
    echo "　　出勤2　";
    $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["start_btn_time2"]);
    echo $wk_str;
}
?>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終業時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_end_time"])); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $end_time).$next_day_value);
//打刻
if ($ovtm_class->ovtm_tab_flg != "t") {
    echo "　打刻　";
    $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["end_btn_time"]);
    echo $wk_str;
    echo "　　退勤2　";
    $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["end_btn_time2"]);
    echo $wk_str;
}
?></font></td>
</tr>
<?
$ovtm_class->disp_btn_time_data($arr_result, "refer");
?>
<?
 // 追加 20140626
$maxline = ($ovtm_class->ovtm_tab_flg == "t") ? 5 : 2;
for ($i=1; $i<=$maxline; $i++) {
 ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時刻<? echo $i; ?></font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
    $idx = ($i == 1) ? "" : $i;
    $s_t = "over_start_time".$idx;
    $e_t = "over_end_time".$idx;
    $s_f = "over_start_next_day_flag".$idx;
    $e_f = "over_end_next_day_flag".$idx;
    $over_start_next_day_flag_value = "";
    if ($arr_ovtmapply[$s_f] == 1){
        $over_start_next_day_flag_value = "　翌日";
    }
    $over_end_next_day_flag_value = "";
    if ($arr_ovtmapply[$e_f] == 1){
        $over_end_next_day_flag_value = "　翌日";
    }
    
    $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$s_t]).$over_start_next_day_flag_value."〜".preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$e_t]).$over_end_next_day_flag_value;
    if ($arr_ovtmapply[$s_t] != "") {
        echo($wk_str);
        //休憩
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        $rest_start_next_day_flag_value = "";
        if ($arr_ovtmapply[$s_f] == 1){
            $rest_start_next_day_flag_value = "　翌日";
        }
        $rest_end_next_day_flag_value = "";
        if ($arr_ovtmapply[$e_f] == 1){
            $rest_end_next_day_flag_value = "　翌日";
        }
        
        if ($arr_ovtmapply[$s_t] != "") {
            echo "　休憩（";
            $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$s_t]).$rest_start_next_day_flag_value."〜".preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$e_t]).$rest_end_next_day_flag_value;
            echo($wk_str);
            
            echo "）";
        }
        //残業理由 20150106
		if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	        echo "&nbsp;";
	        if ($i == 1) {
	        	$reason_val = $reason;
		        $other_reason_val = $arr_ovtmapply["other_reason"];
	        }
	        else {
		        $other_reason_val = $arr_ovtmapply["reason".$idx];
		        if ($other_reason_val != "") {
			        $reason_val = "";
	        	}
	        	else {
		        	$wk_reason_id = $arr_ovtmapply["reason_id".$idx];
		        	$reason_val = $ovtm_class->get_ovtmrsn_reason($wk_reason_id);
	        	}
	        }
	        echo($reason_val . $other_reason_val);
        }
    }
 ?></font></td>
</tr>
<? } ?>
<?
$ovtm_class->disp_ovtm_total_time($arr_ovtm_data, "", "refer");

//残業時刻2以降にも理由を入力する場合は既存の理由を表示しない
if ($ovtm_class->ovtm_reason2_input_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reason . $other_reason); ?></font></td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由詳細</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $reason_detail)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者コメント</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $comment)); ?></font></td>
</tr>

<?
	$page = "overtime_refer";
show_application_apply_detail($con, $session, $fname, $target_apply_id, $page, $update_time, $update_org_name, $update_emp_name, $update_emp_id, $ovtm_class, $wherefrom);
?>

</td>
</tr>

</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">
</form>

</center>
</body>
</html>
<?

$update_cnt = 0;
$show_flg_name = "";
if($mode == 'show_flg_update') {

	// トランザクションを開始
	pg_query($con, "begin");

	$sql = "select apply_status, apv_fix_show_flg, apv_ng_show_flg, apv_bak_show_flg from ovtmapply";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$apply_status = pg_fetch_result($sel, 0, "apply_status");
	$apv_fix_show_flg = pg_fetch_result($sel, 0, "apv_fix_show_flg");
	$apv_ng_show_flg = pg_fetch_result($sel, 0, "apv_ng_show_flg");
	$apv_bak_show_flg = pg_fetch_result($sel, 0, "apv_bak_show_flg");

	if ($apply_status == "1" && $apv_fix_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_fix_show_flg";
	}

	if ($apply_status == "2" && $apv_ng_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_ng_show_flg";
	}

	if ($apply_status == "3" && $apv_bak_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_bak_show_flg";
	}

	if($update_cnt > 0) {

		$sql = "update ovtmapply set";
		$set = array($show_flg_name);
		$setvalue = array('f');
		$cond = "where apply_id = $apply_id and $show_flg_name = 't' and apply_status = '$apply_status'";	
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");
}

pg_close($con);
?>

<script type="text/javascript">
function page_close() {
<?
if($mode == 'show_flg_update' && $update_cnt > 0) {
?>	
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?	
} else {
?>	
	window.close();
<?
}
?> 

}
</script>
