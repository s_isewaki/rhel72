<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
$a = "2";//書庫：医療安全固定


//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}



//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//==============================
// ログインユーザの職員IDを取得
//==============================
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");




// カテゴリ名を取得
$cate_nm = lib_get_category_name($con, $c, $fname);

?>
<script type="text/javascript">

//パス生成
var cate_nm = '<?=$cate_nm?>';
var folder_path = '<? write_folder_path($con, $f, $fname); ?>';
if (folder_path == '')
{
	var path = new Array(cate_nm).join(' &gt; ');
}
else
{
	var path = new Array(cate_nm, folder_path).join(' &gt; ');
}

//呼び出し元画面へ直接設定
opener.document.getElementById('folder_path').innerHTML = path;
opener.document.mainform.archive.value = '<?=$a?>';
opener.document.mainform.category.value = '<?=$c?>';
opener.document.mainform.folder_id.value = '<?=$f?>';
opener.onChangeArchive(false);

//画面を閉じる
self.close();
</script>
<? pg_close($con);?>