<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜管理指標</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");

require_once("jnl_application_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
  echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
  echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
  exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);

require_once('jnl_indicator_data.php');
$indicatorData = new IndicatorData(array('fname' => $fname, 'con' => $con));
$noArr = $indicatorData->getNoForNo($no);
?>



<script type="text/javascript">
<!--

function selectYear(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type = document.getElementById('type');
    category = document.getElementById('category');
    plant = document.getElementById('plant');
    subCategory = document.getElementById('sub_category');
    displayYear = document.getElementById('display_year');
    displayMonth = document.getElementById('display_month');
    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value = 'plant';

        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
        alert(category.value);
        alert(subCategory.value);
        alert(no.value);
    }
    else if (subCategory==null) {
        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    displayYear.value = e.value;
    wkfwForm.submit();
}
function selectMonth(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type = document.getElementById('type');
    category = document.getElementById('category');
    plant = document.getElementById('plant');
    subCategory = document.getElementById('sub_category');
    displayYear = document.getElementById('display_year');
    displayMonth = document.getElementById('display_month');
    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value = 'plant';

        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
        alert(category.value);
        alert(subCategory.value);
        alert(no.value);
    }
    else if (subCategory==null) {
        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    displayMonth.value = e.value;
    wkfwForm.submit();
}

function clickDownLoad() {
  wkfwForm = document.getElementById("wkfw");
  wkfwForm.action = 'jnl_application_indicator_comp_prevyear_diseasewise_excelOPD.php';
  wkfwForm.method = 'get';
  wkfwForm.submit();
}

//-->
</script>
<script type="text/javascript" src="js/jnl/jnl_indicator_menu_list.js"></script>
<script type="text/javascript" src="js/jnl/jnl_indicator_ajax.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="決裁・管理指標"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_indicator_menu.php?session=<? echo($session); ?>"><b>管理指標</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form id="wkfw" name="wkfw" action="jnl_application_imprint.php" method="post" enctype="multipart/form-data">
    <input type="hidden" id="category" name="category" value="<?= $category?>" />
    <input type="hidden" id="sub_category" name="sub_category" value="<?= $sub_category?>" />
<?php
if (!empty($plant)) {
?>
    <input type="hidden" id="plant" name="plant" value="<?= $plant?>" />
    <input type="hidden" id="type" name="type" value="<?= $type?>" />
    <input type="hidden" id="hidden_type" name="hidden_type" value="<?= $category?>" />
<?php
}
?>
    <input type="hidden" id="no" name="no" value="<?= $no?>" />
<?php
require_once('jnl_indicator_display.php');
$indicatorDisplay = new IndicatorDisplay(array('no' => $no, 'last_no' => $last_no, 'con' => $con, 'session' => $session, 'display_year' => $display_year, 'display_month' => $display_month));
# menu list
echo $indicatorDisplay->getMenuList(array('category' => $category, 'sub_category' => $sub_category, 'no' => $no, 'plant' => $plant, 'type' => $type), null, null, $emp_id);

    //Get the current Month & year
    $display_month=$indicatorDisplay->getDisplayMonth();
    $display_year=$indicatorDisplay->getDisplayYear();
//    if($display_year==null || $display_month==null){
//        $display_month=date("m");
//        $display_year=date("Y");
//    }
    $i=intval($display_year);
    $prevYear = $i-1;
    $prepprevYear = $i-2;
    $sundayCountPrev=0;
    $saturdayCountPrev=0;
    //Get the total number of days in selected month
    $totalDaysPrev=date("j",mktime(0,0,0,$display_month+1,0, $prevYear));
    //Iterate through all the days to count the saturday's and sunday's
    for($i=1;$i<=$totalDaysPrev;$i++){

        if((int)$display_month==12 && $i==30){
            $saturdayCountPrev++;
        }else if((int)$display_month==12 && $i==31){
            $sundayCountPrev++;
        }else if((int)$display_month==1 && $i==1){
            $sundayCountPrev++;
        }else if((int)$display_month==1 && $i==2){
            $sundayCountPrev++;
        }else if((int)$display_month==1 && $i==3){
            $sundayCountPrev++;
        }else{
            switch(date("l",mktime(0,0,0,$display_month,$i, $prevYear))){
                case "Saturday":
                    $saturdayCountPrev++;
                    break;
                case "Sunday":
                    $sundayCountPrev++;
                    break;
                default:
                    break;
            }
        }
    }
    $totalDaysPrev=$totalDaysPrev-($saturdayCountPrev+    $sundayCountPrev);

// prev prev year

    $sundayCountPrevPrev=0;
    $saturdayCountPrevPrev=0;
    //Get the total number of days in selected month
    $totalDaysPrevPrev=date("j",mktime(0,0,0,$display_month+1,0, $prepprevYear));
    //Iterate through all the days to count the saturday's and sunday's
    for($i=1;$i<=$totalDaysPrevPrev;$i++){

        if((int)$display_month==12 && $i==30){
            $saturdayCountPrevPrev++;
        }else if((int)$display_month==12 && $i==31){
            $sundayCountPrevPrev++;
        }else if((int)$display_month==1 && $i==1){
            $sundayCountPrevPrev++;
        }else if((int)$display_month==1 && $i==2){
            $sundayCountPrevPrev++;
        }else if((int)$display_month==1 && $i==3){
            $sundayCountPrevPrev++;
        }else{
            switch(date("l",mktime(0,0,0,$display_month,$i, $prepprevYear))){
                case "Saturday":
                    $saturdayCountPrevPrev++;
                    break;
                case "Sunday":
                    $sundayCountPrevPrev++;
                    break;
                default:
                    break;
            }
        }
    }
    $totalDaysPrevPrev=$totalDaysPrevPrev-($saturdayCountPrevPrev+    $sundayCountPrevPrev);



//    $font = $indicatorDisplay->getFont();
//    $font['color'] = null;
//    if (!is_null($fontStyle) && is_array($fontStyle)) {
//        $font = $fontStyle;
//    }
    $fontTag = $indicatorDisplay->getFontTag();
    list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

    $currentYear = $display_year;
    $lastYear = $display_year - 1;

    $currentDayCount = $indicatorData->getDayCountOfMonth($currentYear, $display_month);
    $lastDayCount    = $indicatorData->getDayCountOfMonth($lastYear, $display_month);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="80%" />
            <td style="text-align: right;" align="right">
            <?php
            # 表示年月日指定用 select
            //Get the Year drop down
            echo $indicatorDisplay->getSelectDislpayYear(array(
                'display_year' => $display_year,
                'category'     => $category,
                'sub_category' => $sub_category,
                'no'           => $no,
                'type'         => $type,
                'plant'        => $plant,
            ));
            ?>
            </td>
            <td>
            <?php
            //Get the Month drop down
            echo $indicatorDisplay->getSelectDislpayMonth(array(
                'display_month' => $display_month,
                'category'     => $category,
                'sub_category' => $sub_category,
                'no'           => $no,
                'type'         => $type,
                'plant'        => $plant,
            ));
            $fontTag = $indicatorDisplay->getFontTag($fontArr);
            ?>
            </td>
            <td>
                <input type="button" value="Excel出力" onclick="clickDownLoad();">
            </td>
        </tr>
    </tbody>
</table>
<table width="30%" class="list" cellspacing="0" cellpadding="1">
    <tr>
        <td><?=sprintf($fontTag, $display_year.'年')?></td>
        <td style="text-align: left; background-color:#ffe9c8;"><?php echo $fontTagLeft."平日".$fontTagRight; ?></td>
        <td style="text-align:center;padding:0 2px 0 2px;"><?php echo $fontTagLeft.$currentDayCount['weekday'].'日'.$fontTagRight; ?></td>
        <td  style="text-align: left; background-color:#c8fcff;"><?php echo $fontTagLeft."土曜".$fontTagRight; ?></td>
        <td style="text-align:center;padding:0 2px 0 2px;"><?php echo $fontTagLeft.$currentDayCount['saturday'].'日'.$fontTagRight; ?></td>
        <td  style="text-align: left; background-color:lightpink;"><?=sprintf($fontTag, '休日')?></td>
        <td style="text-align:center;padding:0 2px 0 2px;"><?php echo $fontTagLeft.$currentDayCount['holiday'].'日'.$fontTagRight; ?></td>
    </tr>
    <tr>
        <td><?=sprintf($fontTag, ($display_year - 1).'年')?></td>
        <td style="text-align: left; background-color:#ffe9c8;"><?php echo $fontTagLeft."平日".$fontTagRight; ?></td>
        <td style="text-align:center;padding:0 2px 0 2px;"><?php echo $fontTagLeft.$lastDayCount['weekday'].'日'.$fontTagRight; ?></td>
        <td  style="text-align: left; background-color:#c8fcff;"><?php echo $fontTagLeft."土曜".$fontTagRight; ?></td>
        <td style="text-align:center;padding:0 2px 0 2px;"><?php echo $fontTagLeft.$lastDayCount['saturday'].'日'.$fontTagRight; ?></td>
        <td  style="text-align: left; background-color:lightpink;"><?=sprintf($fontTag, '休日') ?></td>
        <td style="text-align:center;padding:0 2px 0 2px;"><?php echo $fontTagLeft.$lastDayCount['holiday'].'日'.$fontTagRight; ?></td>
    </tr>
</table>
<?php
$dataList = $indicatorData->getDiseasewiseOPDCompPrevYearDetail($display_month, $display_year, array('year' => $display_year));
echo $indicatorDisplay->getDiseasewiseOPDCompPrevYearDisplay($dataList, $display_month, $display_year);
?>

<input type="hidden" name="session" value="<? echo($session); ?>">

</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>
