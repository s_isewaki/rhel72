<?/*

画面パラメータ
	$session
		セッションID
	$search_input
		検索入力文字列。省略時は無検索。
	$search_pattern
		"zen","kou","bubun"のいずれか。省略時は"bubun"。
	$subsys
		タイトル表示用の機能名
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("summary_common.ini");
require_once("get_menu_label.ini");
require_once("medis_version.ini");

//ページ名
$fname = $PHP_SELF;


//==============================
//パラメータ精査
//==============================
if(@$search_pattern != "zen" && @$search_pattern != "kou") {
	$search_pattern = "bubun";
}

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
/*
$summary = check_authority($session, 57, $fname);
if ($summary == "0"){
	showLoginPage();
	exit;
}
*/

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 組織タイプを取得
//==============================
$profile_type = get_profile_type($con, $fname);

//==============================
//追加病名の利用判定
//==============================
$sql = "select count(*) as cnt from org_byoumei";
$sel_a = select_from_table($con,$sql,"",$fname);
if($sel_a == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$use_org_byoumei_flg = pg_fetch_result($sel_a,0,'cnt') != 0;

//==============================
//検索処理
//==============================

// 検索文字解析
if (@$search_input != "") {
    // 検索文言整理
    if ($_POST['search_input'] != '') {
        $search_input = $search_input;
    } else {
        $search_input = mb_convert_encoding(rawurldecode(rawurldecode($search_input)), 'EUC-JP', 'UTF-8');
    }
	$search_input_sql = $search_input;
	$search_input_sql = mb_convert_kana($search_input_sql,"rn");//r:全角英字→半角,n:全角数字→半角
	$search_input_sql = strtoupper($search_input_sql);//小文字→大文字
	$search_input_sql = pg_escape_string($search_input_sql);

	//複数ワード指定(スペースで分割する)
	$search_input_sql = mb_ereg_replace("　"," ",$search_input_sql);//全角スペース除去
	$search_input_sql = trim($search_input_sql);//前後スペース除去
	$search_input_sql = preg_replace("/\s+/", " ", $search_input_sql);//連続スペース除去
	$search_input_sql_list = mb_split(" ",$search_input_sql);//スペース区切り

	//追加病名用キーワード
	$search_input_org_sql = pg_escape_string($search_input);
	$search_input_org_sql = mb_ereg_replace("　"," ",$search_input_org_sql);//全角スペース除去
	$search_input_org_sql = trim($search_input_org_sql);//前後スペース除去
	$search_input_org_sql = preg_replace("/\s+/", " ", $search_input_org_sql);//連続スペース除去
	$search_input_org_sql_list = mb_split(" ",$search_input_org_sql);//スペース区切り
}
else {
	$search_input_sql_list = array();//空配列
	$search_input_org_sql_list = array();//空配列
}

//一致条件作成
$mae_p = "%";
$ato_p = "%";
if($search_pattern == "zen") {
	$mae_p = "";
}
elseif($search_pattern == "kou") {
	$ato_p = "";
}


//MEDIS病名検索実行
$list_cnt = count($search_input_sql_list);
if($list_cnt > 0) {
	//SQL作成
	$sql = "";
	$sql .= " select ";
	$sql .= "   medis_byoumei.byoumei,";
	$sql .= "   medis_byoumei.icd10,";
	$sql .= "   medis_byoumei.byoumei_code,";
	$sql .= "   medis_byoumei.rece_code,";
	$sql .= "   medis_byoumei.byoumei_id,";
	$sql .= "   index_tbl.synonym_flg";
	$sql .= " from medis_byoumei";
	$sql .= " inner join ";
	$sql .= " (";
	$sql .= "   select ";
	$sql .= "   medis_byoumei_index_org.link_code,";
	$sql .= "   (";
	$sql .= "     case ";
	$sql .= "     WHEN ( min(medis_byoumei_index_org.synonym_class) = '2' or min(medis_byoumei_index_org.synonym_class) = '9') then false ";
	$sql .= "     else true ";
	$sql .= "     end ";
	$sql .= "   ) as synonym_flg";
	$sql .= "   from medis_byoumei_index_org";
	$sql .= "   where";
	$sql .= "   (";

	for($i=0;$i<$list_cnt;$i++) {
		if($i != 0) $sql .= " and ";
		$sql .= " (medis_byoumei_index_org.index_str like '".$mae_p.$search_input_sql_list[$i].$ato_p."' ";
		$tmp_kana = mb_convert_kana($search_input_sql_list[$i],"C");//C:全角ひらがな→全角カタカナ
		if ($tmp_kana !== $search_input_sql_list[$i]) {
			$sql .= " or medis_byoumei_index_org.index_str like '".$mae_p.$tmp_kana.$ato_p."' ";
		}
		$sql .= " ) ";
	}

	$sql .= "   )";
	$sql .= "   group by link_code";
	$sql .= " ) index_tbl";
	$sql .= " on medis_byoumei.byoumei_code = index_tbl.link_code where medis_byoumei.update_class<>'1'";
	$sql .= " order by byoumei_kana";
	$cond =  "";

	//SQL実行
	$sel = select_from_table($con,$sql,$cond,$fname);
	$sel_num = (int)@pg_numrows($sel);
} else {
	$sel = 0;
	$sel_num = 0;
}

//追加病名検索実行
if($list_cnt > 0) {
	//SQL作成
	$sql = "";
	$sql .= " select";
	$sql .=   " org_byoumei.byoumei,";
	$sql .=   " org_byoumei.icd10,";
	$sql .=   " org_byoumei.byoumei_code,";
	$sql .=   " org_byoumei.rece_code,";
	$sql .=   " org_byoumei.byoumei_id,";
	$sql .=   " hitted_org.synonym_flg";
	$sql .= " from";
	$sql .=   " org_byoumei";
	$sql .= " natural inner join";
	$sql .=   " (";
	$sql .=     " select";
	$sql .=       " org_id,";
	$sql .=       " (case when (min(synonym_class) = '2' or min(synonym_class) = '9') then false else true end) as synonym_flg";
	$sql .=     " from";
	$sql .=       " (";

	/* 追加病名内検索 START */
	$sql .=         " select org_id,synonym_class";
	$sql .=         " from";
	$sql .=           " (";

	/* index検索 START */
	$sql .=             " select org_id,(case when synonym_flg then '1' else '9' end) as synonym_class";
	$sql .=             " from org_byoumei_index";
	$sql .=             " where";
	for($i=0;$i<$list_cnt;$i++) {
		if($i != 0) $sql .=     " and ";
		$sql .=         " index_str like '".$mae_p.$search_input_sql_list[$i].$ato_p."'";
	}
	/* index検索 END */

	$sql .=             " union";

	/* 病名検索 START */
	$sql .=             " select org_id,'0' as synonym_class";
	$sql .=             " from org_byoumei";
	$sql .=             " where";
	for($i=0;$i<$list_cnt;$i++) {
		if($i != 0) {
			$sql .=     " and ";
		}
		$sql .=         " byoumei like '".$mae_p.$search_input_org_sql_list[$i].$ato_p."'";
	}
	/* 病名検索 END */

	$sql .=             " union";

	/* カナ検索 START */
	$sql .=             " select org_id,'0' as synonym_class";
	$sql .=             " from org_byoumei";
	$sql .=             " where";
	for($i=0;$i<$list_cnt;$i++) {
		if($i != 0) {
			$sql .=     " and ";
		}
		$sql .=         " byoumei_kana like '".$mae_p.$search_input_sql_list[$i].$ato_p."'";
	}
	/* カナ検索 END */


	$sql .=           " ) org_byoumei_index_all";
	/* 追加病名内検索 END */

	$sql .=         " union";

	/* MEDIS連携検索 START */
	$sql .=         " select org_id,synonym_class";
	$sql .=         " from";
	$sql .=           " (";
	$sql .=             " select org_id,byoumei_code from org_byoumei where byoumei_code <> ''";
	$sql .=           " ) a";
	$sql .=         " natural inner join";
	$sql .=           " (";
	$sql .=             " select link_code as byoumei_code, synonym_class";
	$sql .=             " from medis_byoumei_index_org";
	$sql .=             " where";
	for($i=0;$i<$list_cnt;$i++) {
		if($i != 0) {
			$sql .=     " and ";
		}
		$sql .=         " index_str like '".$mae_p.$search_input_sql_list[$i].$ato_p."'";
	}
	$sql .=           " ) b";
	/* MEDIS連携検索 END */

	$sql .=       " ) c";
	$sql .=     " group by org_id";
	$sql .=   " ) hitted_org";
	$sql .=   " order by (case when org_byoumei.byoumei_kana <> '' then org_byoumei.byoumei_kana else org_byoumei.byoumei end)";
	$cond =  "";

	//SQL実行
	$sel_org = select_from_table($con,$sql,$cond,$fname);
	$sel_num_org = pg_numrows($sel_org);
}
else {
	$sel_org = 0;
	$sel_num_org = 0;
}




//==========================================================================================
//HTML出力
//==========================================================================================
switch (@$subsys) {
case "patient":
	$subsys_title = "患者管理";
	break;
default:
	$subsys_title = get_report_menu_label($con, $fname);
	break;
}
?>
<title>CoMedix <?=$subsys_title?>｜病名選択</title>
<script type="text/javascript">

function load_action() {
	document.main_form.search_input.focus();
}

//病名が選択されたときの処理を行います。
function select_byoumei(byoumei,byoumei_code,icd10) {
	//呼び出し元画面に通知
	if(window.opener && !window.opener.closed && window.opener.call_back_summary_byoumei_select) {
		var result = new Object();
		result.byoumei = byoumei;
		result.byoumei_code = byoumei_code;
		result.icd10 = icd10;
		window.opener.call_back_summary_byoumei_select('<?=@$caller?>',result);
	}
	//自画面を終了
	window.close();
}





function highlightCells(class_name) {
	changeCellsColor(class_name, '#fefc8f');
}
function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}
function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
</head>
<body onload="load_action();">

<div style="padding:4px">

<? //ヘッダー 出力 ?>
<? summary_common_show_dialog_header("病名選択"); ?>


<?
//==============================
//入力フィールド 出力
//==============================
?>
<form name="main_form" action="summary_byoumei_select.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="caller" value="<?=@$caller?>">


<table class="prop_table" style="margin-top:4px">
	<tr>
	<th width="2%" style="padding:3px"><nobr>病名</nobr></th>
	<td width="85%" style="padding:3px"><nobr>
		<input type="text" name="search_input" value="<?=h(@$search_input)?>" style="width:350px;ime-mode: active">
		<input type="button" onclick="document.main_form.submit();" value="検索"/>
		<span style="color:#1f97f5">MEDIS標準病名マスターV<?=$MEDIS_VERSION?>対応</nobr>
	</td>
	<th width="5%" style="padding:3px"><nobr>検索方法</nobr></th>
	<td width="5%" style="padding:3px">
		<select name="search_pattern">
			<option value="zen"   <? if($search_pattern == "zen"  ){print "selected";} ?> >前方一致
			<option value="bubun" <? if($search_pattern == "bubun"){print "selected";} ?> >部分一致
			<option value="kou"   <? if($search_pattern == "kou"  ){print "selected";} ?> >後方一致
		</select>
	</td>
	</tr>
</table>

<?
if($use_org_byoumei_flg)
{
?>


<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin-top:10px; margin-bottom:2px; letter-spacing:1px; width:150px">追加病名</div>

<table class="list_table">
	<!-- 追加病名一覧HEADER START -->
	<tr>
	<th style="text-align:center" width="50"><nobr>同義語</nobr></th>
	<th style="text-align:center"><nobr>病名</nobr></th>
	<th style="text-align:center" width="50"><nobr>ICD10</nobr></th>
	<th style="text-align:center" width="100"><nobr>交換用コード</nobr></th>
	<th style="text-align:center" width="100"><nobr>傷病名コード</nobr></th>
	<th style="text-align:center" width="100"><nobr>管理番号</nobr></th>
	</tr>
	<!-- 追加病名一覧HEADER END -->
	<!-- 追加病名一覧DATA START -->
	<?
	$is_disp_over = false;
	for($i = 0; $i < $sel_num_org; $i++)
	{
		if($i >= 200)
		{
			$is_disp_over = true;
			break;
		}

		$synonym_flg  = pg_fetch_result($sel_org,$i,"synonym_flg");
		if($synonym_flg == 't')
		{
			$synonym_flg = "○";
		}
		else
		{
			$synonym_flg = "×";
		}
		$byoumei      = pg_fetch_result($sel_org,$i,"byoumei");
		$icd10        = pg_fetch_result($sel_org,$i,"icd10");
		$byoumei_code = pg_fetch_result($sel_org,$i,"byoumei_code");
		$rece_code    = pg_fetch_result($sel_org,$i,"rece_code");
		$byoumei_id   = pg_fetch_result($sel_org,$i,"byoumei_id");

		$byoumei_js      = str_replace("'","''",$byoumei);
		$icd10_js        = str_replace("'","''",$icd10);
		$byoumei_code_js = str_replace("'","''",$byoumei_code);
		$line_script = "";
		$line_script .= " class=\"org_line".$i."\"";
		$line_script .= " onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\"";
		$line_script .= " onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\"";
		$line_script .= " onclick=\"select_byoumei('".$byoumei_js."' , '".$byoumei_code_js."' , '".$icd10_js."')\"";

		$bgcolor = @$bgcolor=="#fff" ? "#f1f7fd" : "#fff";
	?>
	<tr style="background-color:<?=$bgcolor?>">
	<td <?=$line_script?> align="center"><nobr><?=h($synonym_flg)?></nobr></td>
	<td <?=$line_script?> align="left"  ><nobr><?=h($byoumei)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($icd10)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($byoumei_code)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($rece_code)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($byoumei_id)?></nobr></td>
	</tr>
	<? } ?>
</table>

<? if($is_disp_over) { ?>
<div>200件以上データがあります。</div>
<? } ?>
<!-- 追加病名一覧DATA END -->



<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin-top:10px; margin-bottom:2px; letter-spacing:1px; width:150px">MEDIS標準病名</div>


<?
}//$use_org_byoumei_flg
?>

<table class="list_table">
	<!-- MEDIS一覧HEADER START -->
	<tr>
	<th style="text-align:center" width="50"><nobr>同義語</nobr></th>
	<th style="text-align:center"><nobr>病名</nobr></th>
	<th style="text-align:center" width="50"><nobr>ICD10</nobr></th>
	<th style="text-align:center" width="100"><nobr>交換用コード</nobr></th>
	<th style="text-align:center" width="100"><nobr>傷病名コード</nobr></th>
	<th style="text-align:center" width="100"><nobr>管理番号</nobr></th>
	</tr>
	<!-- MEDIS一覧HEADER END -->
	<!-- MEDIS一覧DATA START -->
	<?
	$is_disp_over = false;
	for($i = 0; $i < $sel_num; $i++) {
		if($i >= 200) {
			$is_disp_over = true;
			break;
		}

		$synonym_flg  = pg_fetch_result($sel,$i,"synonym_flg");
		if($synonym_flg == 't') {
			$synonym_flg = "○";
		} else {
			$synonym_flg = "×";
		}
		$byoumei      = pg_fetch_result($sel,$i,"byoumei");
		$icd10        = pg_fetch_result($sel,$i,"icd10");
		$byoumei_code = pg_fetch_result($sel,$i,"byoumei_code");
		$rece_code    = pg_fetch_result($sel,$i,"rece_code");
		$byoumei_id   = pg_fetch_result($sel,$i,"byoumei_id");

		$byoumei_js      = str_replace("'","''",$byoumei);
		$icd10_js        = str_replace("'","''",$icd10);
		$byoumei_code_js = str_replace("'","''",$byoumei_code);
		$line_script = "";
		$line_script .= " class=\"list_line".$i."\"";
		$line_script .= " onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\"";
		$line_script .= " onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\"";
		$line_script .= " onclick=\"select_byoumei('".$byoumei_js."' , '".$byoumei_code_js."' , '".$icd10_js."')\"";

		$bgcolor = @$bgcolor=="#fff" ? "#f1f7fd" : "#fff";
	?>
	<tr style="background-color:<?=$bgcolor?>">
	<td <?=$line_script?> align="center"><nobr><?=h($synonym_flg)?></nobr></td>
	<td <?=$line_script?> align="left"  ><nobr><?=h($byoumei)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($icd10)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($byoumei_code)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($rece_code)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($byoumei_id)?></nobr></td>
	</tr>
	<? } ?>
</table>



<? if($is_disp_over) { ?>
<div>200件以上データがあります。</div>
<? } ?>
<!-- MEDIS一覧DATA END -->



</form>


</div>
</body>
</html>
<? pg_close($con); ?>
