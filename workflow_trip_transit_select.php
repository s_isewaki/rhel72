<?
// 出張旅費
// 経路選択
require_once("about_comedix.php");
require_once("workflow_trip_common.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if($session == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);
if($con == "0") {
	js_login_exit();
}

// 交通手段
$array_transport = get_trip_transport();
$trip_transport = get_trip_transport_select_options($session);

$mode = @$_REQUEST["mode"];
if ($mode == 1) {
	$transit = get_trip_transit();
}
else {
	$transit = get_trip_transit_emp($session);
}

pg_close($con);

function output_row($i, $transport, $oufuku, $from, $to, $fee) {
	global $array_transport, $array_oufuku;
?>
	<tr>
	<td><input type="checkbox" name="ids[]" value="<?=$i?>" /></td>
	<td><?eh($array_transport[$transport])?></td>
	<td><?eh($array_oufuku[$oufuku])?></td>
	<td><?eh($from)?></td>
	<td><?eh($to)?></td>
	<td style="text-align:right;"><?eh($fee)?>円</td>
	<td><button type="button" onclick="select_transit(<?=$i?>)">登録する</button></td>
	</tr>
	<script>transit[<?=$i?>] = {transport:<?=$transport?>, oufuku:<?=$oufuku?>, from:'<?=$from?>', to:'<?=$to?>', fee:'<?=$fee?>'};</script>
	<?
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | 経路選択</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
<script type="text/javascript">
var transit = Array();

//経路選択処理
function select_transit(i) {
	if(window.opener && !window.opener.closed && window.opener.add_tt1_row) {
		window.opener.add_tt1_row('<?=$trip_transport?>',transit[i]);
	}
	window.close();
}

//経路選択処理(一括)
function select_transit_all() {
	if(window.opener && !window.opener.closed && window.opener.add_tt1_row) {
		for (i=0; i<document.getElementsByName('ids[]').length; i++) {
			if (document.getElementsByName('ids[]')[i].checked) {
				window.opener.add_tt1_row('<?=$trip_transport?>',transit[document.getElementsByName('ids[]')[i].value]);
			}
		}
	}
	window.close();
}
</script>
<style>
body {
 margin           : 0;
 padding          : 0;
}
.transtable {
 border           : 0;
 border-collapse  : collapse;
 font-size        : 9pt;
 margin           : 5px;
 width            : 470px;
}
.transtable tbody td {
 border           : 1px solid black;
 padding          : 3px;
 white-space      : nowrap;
 text-align       : center;
}
.transtable thead td {
 border           : 1px solid black;
 font-weight      : bold;
 padding          : 5px 10px;
 text-align       : center;
 white-space      : nowrap;
}
.transit_mst_title {
 background-color : #5279a5;
 color            : #ffffff;
 padding          : 10px 20px;
 font-size        : 11pt;
 font-weight      : bold;
 margin-bottom    : 5px;
}
.button_all {
 margin-left      : 5px;
}
</style>
</head>
<body>
<div class="transit_mst_title">経路選択</div>
<button class="button_all" type="button" onclick="select_transit_all()">チェックした経路を一括で登録する</button>
<table class="transtable">
<thead>
<tr>
<td></td>
<td>交通機関</td>
<td>区分</td>
<td>出発</td>
<td>到着</td>
<td>金額</td>
<td>登録</td>
</tr>
</thead>
<tbody>
<?
$i = 1;
foreach($transit as $r) {
	if ($mode == 1) {
		output_row($i++, $r["transport_id"], 1, $r["from_name"], $r["to_name"], $r["fee"]);
		output_row($i++, $r["transport_id"], 2, $r["from_name"], $r["to_name"], $r["fee"] * 2);
	}
	else {
		output_row($i++, $r["transport_id"], $r["oufuku"], $r["from_name"], $r["to_name"], $r["fee"]);
	}
}
?>
</tbody>
</table>
<button class="button_all" type="button" onclick="select_transit_all()">チェックした経路を一括で登録する</button>
</body>
</html>
