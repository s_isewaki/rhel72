<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 残業理由</title>
<?php
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");

//-----------------------------------------------------------------------------------------------------
// timecard_overtime_reason.php
// 遅刻早退テンプレ機能のために改修
// ※「理由」タブのメインページとして機能
//-----------------------------------------------------------------------------------------------------

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 残業理由一覧を取得
$sql = "select reason_id, reason, over_no_apply_flag, tmcd_group_id, sort, hide_flag from ovtmrsn"; // メディアテック
$cond = "where del_flg = 'f' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$reason_list = array();
if (pg_num_rows($sel) > 0) {
	while ($row = pg_fetch_array($sel)) {
		$id_key = $row["reason_id"];
		$reason_list[$id_key]["reason_id"] = $id_key;
		$reason_list[$id_key]["reason"] = $row["reason"];
		$reason_list[$id_key]["over_no_apply_flag"] = $row["over_no_apply_flag"];
		$reason_list[$id_key]["tmcd_group_id"] = $row["tmcd_group_id"];
		$reason_list[$id_key]["sort"] = $row["sort"];
		$reason_list[$id_key]["hide_flag"] = $row["hide_flag"];
	}
}

// ソート処理
$key_total = array();
foreach ($reason_list as $key=>$item){
	$key_total[$key] = $item["sort"];
}
array_multisort($key_total, SORT_ASC, $reason_list);

// ----- メディアテック Start -----
// 勤務グループ一覧を取得
$get_group_sql = "select * from wktmgrp order by group_id";
$sel_grp = select_from_table($con, $get_group_sql, null, $fname);
if ($sel_grp == 0) {
	pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$group_list = array();
if(pg_num_rows($sel_grp) > 0){
	while ($row = pg_fetch_array($sel_grp)) {
		$group_id = $row["group_id"];
		$group_list[$group_id] = $row["group_name"];
	}
}
// ----- メディアテック End -----
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	// 行を追加する
	$(document).on("click", ".addList", function() {
	    $(".reason-tfoot > tr").eq(0).clone(true).insertAfter(
	      $(this).parent().parent()
	    );
  	});

	// 行を削除する
	$(document).on("click", ".removeList", function() {
		$(this).parent().parent().empty();
	});
	
	// 行を一つ上に移動させる
	$(document).on("click", ".upList", function() {
		var t = $(this).parent().parent();
		if($(t).prev("tr")) {
			$(t).insertBefore($(t).prev("tr")[0]);
		}
		$("tbody tr").css("background-color", "#ffffff");
		$(t).css("background-color", "#FFD2D2");
	});

	// 行を一つ下に移動させる
	$(document).on("click", ".downList", function() {
		var t = $(this).parent().parent();
		if($(t).next("tr")) {
			$(t).insertAfter($(t).next("tr")[0]);
		}
		$("tbody tr").css("background-color", "#ffffff");
		$(t).css("background-color", "#FFD2D2");
	});

});

function add_item(id) {
	var item = $(".reason-tfoot > tr").eq(0).clone(true);
	$('#tbody' + id).append(item);
}

function update_ovtmrsn(){

	if (!confirm("残業理由を登録します。よろしいですか？")){
		return false;	
	}
	
	$("#tbody tr").each(function(no){
		var classID = $(this).children('td.reason_name').children('input[type=text]').attr("class");
		var reason_name = $(this).children('td.reason_name').children('input[type=text]').val();
		var no_apply = ($(this).children('td.no_apply_flag').children('input[type=checkbox]').prop('checked')) ? "t" : "f";
		var wktmgrp = $(this).children('td.wktmgrp').children('select').val();
		var hide_flag = ($(this).children('td.hide_flag').children('input[type=checkbox]').prop('checked')) ? "t" : "f";

		if (reason_name){
			var reasonId = '<input type="hidden" name="addReasonId[' + no + ']" value=' + classID + '>';
			$(reasonId).insertAfter("#tbody");
	
			var reasonName = '<input type="hidden" name="addReasonName[' + no + ']" value=' + reason_name + '>';
			$(reasonName).insertAfter("#tbody");
	
			var noApplyFlag = '<input type="hidden" name="addNoApplyFlag[' + no + ']" value=' + no_apply + '>';
			$(noApplyFlag).insertAfter("#tbody");
	
			var wktmGroup = '<input type="hidden" name="addWktmGroup[' + no + ']" value=' + wktmgrp + '>';
			$(wktmGroup).insertAfter("#tbody");
	
			var hideFlag = '<input type="hidden" name="addHideFlag[' + no + ']" value=' + hide_flag + '>';
			$(hideFlag).insertAfter("#tbody");
		}
	});
	
	return true;
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}

table.reason-table{
	font-size:14px;
	border-collapse:collapse;
}
table.reason-table th,table.reason-table td{
	border:1px solid #aaa;
	padding:5px 8px;
	text-align:center;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<!-- atdbk_timecard.phpを atdbk_timecard_shift.phpに変更 20130129 -->
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<!--  遅刻早退テンプレート -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
&nbsp;&nbsp;<b>残業理由</b>
&nbsp;&nbsp;<a href="timecard_modify_reason.php?session=<? echo($session); ?>">勤務時間修正理由</a>
&nbsp;&nbsp;<a href="timecard_return_reason.php?session=<? echo($session); ?>">呼出勤務理由</a>
&nbsp;&nbsp;<a href="timecard_irregular_reason.php?session=<? echo($session); ?>">遅刻早退理由</a>
&nbsp;&nbsp;</font>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="15"><br>
<!--  遅刻早退テンプレート -->


<!--  新規対応スタート -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="update" action="timecard_overtime_reason_update.php" method="post">
<table class="reason-table" width="100%">
	<thead>
		<tr>
		  <th nowrap width="50%">残業理由</th>
		  <th nowrap width="8%">残業申請<br>不要理由</th>
		  <th nowrap width="10%">勤務グループ</th>
		  <th nowrap width="5%">非表示</th>
		  <th nowrap width="10%">表示順</th>
		  <th nowrap width="12%">
		  	<span>追加 / 削除</span>&nbsp;
		  	<input type="hidden" id="target_reason_<?=$reason_id?>" value="<?=$reason_id?>" /> 
		  	<input id="add<?=$reason_id?>" value="追加" type="button"  style="width:50px" onclick="add_item('<?=$reason_id?>');" />
		  </th>
		</tr>
	</thead>
	<tfoot class="reason-tfoot" style="display:none;">
		<tr>
		  <td class="reason_name">
		    <input type="text"  name="reason_name[]" class="new_reason_id"  size="120" maxlength="200" value="" />
		  </td>
		  <td class="no_apply_flag">
		    <input type="checkbox" name="no_apply_flag[]" value="">
		  </td>
		  <td class="wktmgrp">
		    <select name="wktmgrp[]">
			<option value="" label="" selected>
			    <?php
				    foreach ($group_list as $grp_id=>$item) {
				        echo "<option value=\"$grp_id\">$item</option>";
				    }
				?>
			</select>
		  </td>
		  <td class="hide_flag">
		    <input type="checkbox" name="hide_flag[]" value="">
		  </td>
		  <td nowrap>
		    <img src="images/up.gif" alt="↑" class="upList" />上へ　
		    <img src="images/down.gif" alt="↓" class="downList" />下へ
		  </td>
		  <td nowrap>
		    <input value="追加" type="button" class="addList" />　
		    <input value="削除" type="button" class="removeList" />
		  </td>
		</tr>
	</tfoot>
	<tbody id="tbody" class="reason-tbody">
		<?php 
			if (!empty($reason_list)){
				foreach ($reason_list as $item) {	
		?>
					<tr name="tr_reason_<?php echo($item["reason_id"]); ?>">
					  <td class="reason_name">
					    <input type="text" name="reason_name_<?php echo($item["reason_id"]); ?>>" class="<?php echo($item["reason_id"]); ?>" size="120" maxlength="200" value="<?php echo($item["reason"]); ?>" />
					  </td>
					  <td class="no_apply_flag">
					    <input type="checkbox" name="no_apply_flag_<?php echo($item["reason_id"]); ?>" value="t"  <?php echo(($item["over_no_apply_flag"] == "t") ? " checked" : "") ?>>
					  </td>
					  <td class="wktmgrp">
					     <select name="wktmgrp_<?php echo($item["reason_id"]); ?>">
						 <option value="" label="">
						    <?php
							    foreach ($group_list as $grp_id=>$grp_item) {
							    	if ($item["tmcd_group_id"] == $grp_id){
							    		echo "<option value=\"$grp_id\" selected>$grp_item</option>";
							    	}else{
							    		echo "<option value=\"$grp_id\">$grp_item</option>";
							    	}
							    }
							?>
						</select>
					  </td>
					  <td class="hide_flag">
					    <input type="checkbox" name="hide_flag_<?php echo($item["reason_id"]); ?>" value="t" <?php echo(($item["hide_flag"] == "t") ? " checked" : ""); ?>>
					  </td>
					  <td nowrap>
					    <img src="images/up.gif" alt="↑" class="upList" />上へ　
					    <img src="images/down.gif" alt="↓" class="downList" />下へ
					  </td>
					  <td nowrap>
					    <input value="追加" type="button" class="addList" />　
					    <input value="削除" type="button" class="removeList" />
					  </td>
					</tr>
		<?php
				} 
			} 
		?>
	</tbody>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td colspan="2" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
	<td>
		<font size="3" color="#ff0000" face="ＭＳ Ｐゴシック, Osaka" class="j12">※勤務グループ設定した場合、その勤務グループのみ有効です。</font>
	</td>
	<td align="right"><input type="submit" value="登録" onclick="return update_ovtmrsn();"></td>
</tr>
</table>
</form>
</body>
<? pg_close($con); ?>
</html>

<!--  新規対応エンド -->


