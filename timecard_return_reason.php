<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("timecard_bean.php");
require_once("atdbk_menu_common.ini");

//-----------------------------------------------------------------------------------------------------
// 遅刻早退テンプレ機能のために改修
//-----------------------------------------------------------------------------------------------------

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 退勤後復帰理由一覧を取得
$sql = "select reason_id, reason from rtnrsn";
$cond = "where del_flg = 'f' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
?>
<title>CoMedix 勤務管理 | <?=$ret_str?>理由</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteReason() {
	if (confirm('削除します。よろしいですか？')) {
		document.delform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<!--  遅刻早退テンプレート -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
&nbsp;&nbsp;<a href="timecard_overtime_reason.php?session=<? echo($session); ?>">残業理由</a>
&nbsp;&nbsp;<a href="timecard_modify_reason.php?session=<? echo($session); ?>">勤務時間修正理由</a>
&nbsp;&nbsp;<b>呼出勤務理由</b>
&nbsp;&nbsp;<a href="timecard_irregular_reason.php?session=<? echo($session); ?>">遅刻早退理由</a>
&nbsp;&nbsp;</font>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="15"><br>
<!--  遅刻早退テンプレート -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="regform" action="timecard_return_reason_insert.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$ret_str?>理由</font></td>
<td><input type="text" name="reason" value="<? echo($reason); ?>" size="50" maxlength="100" style="ime-mode:active;"></td>
</tr>
</table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="delform" action="timecard_return_reason_delete.php" method="post" style="border-top:#5279a5 solid 1px;padding-top:5px;">
<?
if (pg_num_rows($sel) > 0) {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\"><input type=\"button\" value=\"削除\" onclick=\"deleteReason();\"></td>\n");
	echo("</tr>\n");
	echo("</table>\n");

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr bgcolor=\"#f6f9ff\">\n");
	echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$ret_str}理由</font></td>\n");
	echo("</tr>\n");
	while ($row = pg_fetch_array($sel)) {
		$tmp_reason_id = $row["reason_id"];
		$tmp_reason = $row["reason"];

		echo("<tr>\n");
		echo("<td width=\"30\" align=\"center\"><input type=\"checkbox\" name=\"reason_ids[]\" value=\"$tmp_reason_id\"></td>\n");
		echo("<td><a href=\"timecard_return_reason_update.php?session=$session&reason_id=$tmp_reason_id\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_reason</font></a></td>\n");
		echo("</tr>\n");
	}
	echo("</table>\n");
} else {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">理由は登録されていません。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
