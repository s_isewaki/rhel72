<?php
$fname = $_SERVER["PHP_SELF"];

require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_schedule_to_do.ini");
require_once("time_check.ini");
require_once("project_check.ini");
require_once("show_schedule_common.ini");
require_once("show_schedule_chart.ini");

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// ���¤Υ����å�
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// �������塼��������¤����
$schd_place_auth = check_authority($session, 13, $fname);

if(!isset($_REQUEST['date'])){
    $date = mktime(0,0,0,date('m'), date('d'), date('Y'));
}
else {
    $date = $_REQUEST['date'];
}
$year = date('Y', $date);

$con = connect2db($fname);

// ��������桼���ο���ID�����
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// �������塼�뵡ǽ�ν�����̾�������
$default_view = get_default_view($con, $emp_id, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix �������塼�� | ͽ�����</title>

<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function downloadCSV(){
    document.csv.action = 'schedule_csv.php';
    document.csv.submit();

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="�������塼��"></a></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>�������塼��</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>�������̤�</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǯ</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12" color="#ffffff"><b>����</b></font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����������塼�븡��</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ޥ����롼��</font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������Ϣ��</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ץ����</font></a></td>
<td>&nbsp;</td>
<td width="20"><input type="button" value="CSV����" onclick="downloadCSV()"></td>
</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="80" height="22" align="center" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>ͽ�����</b></font></td>
<td width="5"></td>
<td width="80" height="22" align="center"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ͽ�꺣��</font></a></td>
<td width="5"></td>
<td width="80" height="22" align="center"><a href="schedule_list_past.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ͽ����</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
<?
// �������塼����������
$sql = "select schd_id, schd_title, schd_type, schd_start_time_v, schd_dur_v, schd_start_date, schd_imprt, schd_status from schdmst where emp_id = '$emp_id' union select schd_id, schd_title, schd_type, null, null, schd_start_date, schd_imprt, schd_status from schdmst2 where emp_id = '$emp_id' order by schd_start_date, schd_start_time_v";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// �������塼�������ɽ��
while ($row = pg_fetch_array($sel)) {
    $schd_id = $row["schd_id"];
    $schd_title = $row["schd_title"];
    $schd_type = $row["schd_type"];
    $schd_start_date = $row["schd_start_date"];
    $schd_start_time = substr($row["schd_start_time_v"], 0, 2).":".substr($row["schd_start_time_v"], 2, 2);
    $schd_dur = substr($row["schd_dur_v"], 0, 2).":".substr($row["schd_dur_v"], 2, 2);
    $schd_status = $row["schd_status"];
    $color = get_schedule_color($schd_type);

    $tmp_date = mktime(0, 0, 0, substr($schd_start_date, 5, 2), substr($schd_start_date, 8, 2), substr($schd_start_date, 0, 4));
    if ($schd_start_time != ":" && $schd_dur != ":") {
        $timeless = "";
        $formatted_time = substr($schd_start_time, 0, 5) . "-" . substr($schd_dur, 0, 5);
    } else {
        $timeless = "t";
        $formatted_time = "����ʤ�";
    }
    $status = ($schd_status == "2") ? "[̤��ǧ]" : "";

    $ret_url = urlencode("schedule_list_all.php?session=$session&date=$tmp_date");
    echo("<tr height=\"22\" bgcolor=\"$color\">\n");
    echo("<td width=\"100\"><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\"><a href=\"schedule_menu.php?session=$session&date=$tmp_date\">$schd_start_date</a></font></td>\n");
    echo("<td width=\"100\"><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\">$formatted_time</font></td>\n");
    echo("<td><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('schedule_update.php?session=$session&date=$tmp_date&time=$schd_start_time&schd_id=$schd_id&timeless=$timeless', 'newwin', 'width=640,height=560,scrollbars=yes');\">" . h("$schd_title$status") . "</a></font></td>\n");
    echo("<td width=\"50\" align=\"center\"><input type=\"button\" value=\"���\" onclick=\"location.href = 'schedule_delete_checker.php?session=$session&schd_id=$schd_id&ret_url=$ret_url&timeless=$timeless&rptopt=1';\"></td>\n");
    echo("</tr>\n");
}
?>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_schd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td>
</tr>
</table>
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo $session; ?>">
    <input type="hidden" name="start_date" value="0">
    <input type="hidden" name="end_date" value="0">
    <input type="hidden" name="pjt_f" value="0">
    <input type="hidden" name="time_f" value="0">
</form>

</body>
<? pg_close($con); ?>
</html>
