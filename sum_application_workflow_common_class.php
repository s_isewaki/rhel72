<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("summary_common.ini");

function select_from_table_or_die($con, $sql, $cond="", $fname=""){
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}
	return $sel;
}

function insert_into_table_or_die($con, $sql, $content, $fname){
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}
	return $ins;
}
/*
function delete_from_table_or_die($con, $sql, $content, $fname){
	$del = delete_from_table($con, $sql, "", $fname);
	if ($del == 0) {
		pg_query($this->_db_con, "rollback");
		pg_close($this->_db_con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	return $del;
}
*/
function update_set_table_or_die($con, $sql, $set, $setvalue, $cond, $fname){
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($this->_db_con, "rollback");
		pg_close($this->_db_con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	return $upd;
}

class application_workflow_common_class {

	var $file_name; // 呼び出し元ファイル名
	var $_db_con;   // DBコネクション

	/**
	 * コンストラクタ
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 */
	function application_workflow_common_class($con, $fname) {
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// 全項目リストを初期化
	}

	function select_from_table_or_die($sql, $cond=""){
		return select_from_table_or_die($this->_db_con, $sql, $cond, $this->file_name);
	}

	function insert_into_table_or_die($sql, $content){
		return insert_into_table_or_die($this->_db_con, $sql, $content, $this->file_name);
	}
/*
	function delete_from_table_or_die($sql, $content){
		return delete_from_table_or_die($this->_db_con, $sql, $content, $this->file_name);
	}
*/
	function update_set_table_or_die($sql, $set, $setvalue, $cond){
		return update_set_table_or_die($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
	}

//-------------------------------------------------------------------------------------------------------------------------
// フォルダツリー関連
//-------------------------------------------------------------------------------------------------------------------------

	// 職員の部署役職取得
	function get_emp_info($emp_id) {
		// 職員情報取得(複数部署役職対応)
		$sql  = "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from empmst where emp_id = '$emp_id' ";
		$sql .= "union ";
		$sql .= "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from concurrent where emp_id = '$emp_id'";

		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array("emp_class" => $row["emp_class"], "emp_attribute" => $row["emp_attribute"], "emp_dept" => $row["emp_dept"], "emp_room" => $row["emp_room"], "emp_st" => $row["emp_st"]);
		}

		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------
// アクセス権限関連
//-------------------------------------------------------------------------------------------------------------------------

	// 部門一覧取得
	function get_classmst() {
		$sql = "select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no";
		return $this->select_from_table_or_die($sql);
	}

	// 課一覧取得
	function get_atrbmst() {
		$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
		$sql .= " where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.order_no, atrbmst.order_no";
		$sel_atrb = $this->select_from_table_or_die($sql);
		return $sel_atrb;
	}

	// 科一覧取得
	function get_deptmst() {
		$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
		$sql .= " where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.order_no, atrbmst.order_no, deptmst.order_no";
		return $this->select_from_table_or_die($sql);
	}

	// 役職一覧取得
	function get_stmst() {
		$sql = "select st_id, st_nm from stmst where st_del_flg = 'f' order by order_no";
		return $this->select_from_table_or_die($sql);
	}

	// 結果通知管理情報取得
	function get_wkfwnoticemng($wkfw_id, $mode) {
		$sel = $this->select_from_table_or_die("select * from sum_wkfwnoticemng where wkfw_id = $wkfw_id");

		$notice_target_class_div = @pg_fetch_result($sel, 0, "target_class_div");
		$rslt_ntc_div0_flg = @pg_fetch_result($sel, 0, "rslt_ntc_div0_flg");
		$rslt_ntc_div1_flg = @pg_fetch_result($sel, 0, "rslt_ntc_div1_flg");
		$rslt_ntc_div2_flg = @pg_fetch_result($sel, 0, "rslt_ntc_div2_flg");
		$rslt_ntc_div3_flg = @pg_fetch_result($sel, 0, "rslt_ntc_div3_flg");
		$rslt_ntc_div4_flg = @pg_fetch_result($sel, 0, "rslt_ntc_div4_flg");
		$rslt_ntc_div5_flg = @pg_fetch_result($sel, 0, "rslt_ntc_div5_flg");
		$rslt_ntc_div6_flg = @pg_fetch_result($sel, 0, "rslt_ntc_div6_flg");

		return array(
					   "notice_target_class_div" => $notice_target_class_div,
					   "rslt_ntc_div0_flg" => $rslt_ntc_div0_flg,
					   "rslt_ntc_div1_flg" => $rslt_ntc_div1_flg,
					   "rslt_ntc_div2_flg" => $rslt_ntc_div2_flg,
					   "rslt_ntc_div3_flg" => $rslt_ntc_div3_flg,
					   "rslt_ntc_div4_flg" => $rslt_ntc_div4_flg,
					   "rslt_ntc_div5_flg" => $rslt_ntc_div5_flg,
					   "rslt_ntc_div6_flg" => $rslt_ntc_div6_flg
					  );
	}

	// 部署役職指定(結果通知)情報取得
	function get_wkfwnoticestdtl($wkfw_id, $st_div, $mode) {
		$sql   = "select a.*, b.st_nm ";
		$sql  .= "from sum_wkfwnoticestdtl a ";
		$sql  .= "inner join stmst b on a.st_id = b.st_id ";
		$sql  .= "where a.wkfw_id = $wkfw_id and st_div = $st_div";
		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array("st_id" => $row["st_id"], "st_nm" => $row["st_nm"]);
		}
		return $arr;
	}

	// 職員指定(結果通知)情報取得
	function get_wkfwnoticedtl($wkfw_id, $mode) {
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		$sql .= "from sum_wkfwnoticedtl a ";
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql .= " where a.wkfw_id = $wkfw_id ";
		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array("emp_id" => $row["emp_id"], "emp_lt_nm" => $row["emp_lt_nm"], "emp_ft_nm" => $row["emp_ft_nm"]);
		}
		return $arr;
	}

	// 委員会・ＷＧ指定(結果通知)情報取得
	function get_wkfwnoticepjtdtl($wkfw_id, $mode) {
		$sql  = "select pjt_id, pjt_name from project ";
		$sql .= "where pjt_id in (select parent_pjt_id from sum_wkfwnoticepjtdtl where wkfw_id = $wkfw_id) ";
		$sql .= "union all ";
		$sql .= "select pjt_id, pjt_name from project ";
		$sql .= "where pjt_id in (select child_pjt_id from sum_wkfwnoticepjtdtl where wkfw_id = $wkfw_id)";
		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		if(pg_numrows($sel) == 1) {
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm);
		}
		else if(pg_numrows($sel) == 2) {
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
			$pjt_child_id = pg_fetch_result($sel, 1, "pjt_id");
			$pjt_child_nm = pg_fetch_result($sel, 1, "pjt_name");
			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm, "pjt_child_id" => $pjt_child_id, "pjt_child_nm" => $pjt_child_nm);
		}
		return $arr;
	}

	// 部署役職指定(結果通知)(部署指定)情報取得
	function get_wkfwnoticesectdtl($wkfw_id, $mode) {
		$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
		$sql .= "from  sum_wkfwnoticesectdtl a ";
		$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
		$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
		$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
		$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
		$sql .= " where a.wkfw_id = $wkfw_id ";
		$sel = $this->select_from_table_or_die($sql);
		return array(
			"class_id" => pg_fetch_result($sel, 0, "class_id"),
			"atrb_id" => pg_fetch_result($sel, 0, "atrb_id"),
			"dept_id" => pg_fetch_result($sel, 0, "dept_id"),
			"room_id" => pg_fetch_result($sel, 0, "room_id"),
			"class_nm" => pg_fetch_result($sel, 0, "class_nm"),
			"atrb_nm" => pg_fetch_result($sel, 0, "atrb_nm"),
			"dept_nm" => pg_fetch_result($sel, 0, "dept_nm"),
			"room_nm" => pg_fetch_result($sel, 0, "room_nm")
		);
	}


//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー登録・更新・削除関連
//-------------------------------------------------------------------------------------------------------------------------

	// 受信者管理登録
	function regist_wkfwapvmng($wkfw_id, $apv_order, $approve_mng, $mode) {

		$deci_flg = $approve_mng["deci_flg"];
		$target_class_div = $approve_mng["target_class_div"];
		$multi_apv_flg = $approve_mng["multi_apv_flg"];
		$next_notice_div  = $approve_mng["next_notice_div"];
		$next_notice_recv_div  = $approve_mng["next_notice_recv_div"];
		$apv_div0_flg = $approve_mng["apv_div0_flg"];
		$apv_div1_flg = $approve_mng["apv_div1_flg"];
		$apv_div2_flg = $approve_mng["apv_div2_flg"];
		$apv_div3_flg = $approve_mng["apv_div3_flg"];
		$apv_div4_flg = $approve_mng["apv_div4_flg"];
		$apv_div5_flg = $approve_mng["apv_div5_flg"];
		$apv_div6_flg = $approve_mng["apv_div6_flg"];
		$apv_num		  = $approve_mng["apv_num"];
		$apv_common_group_id = $approve_mng["apv_common_group_id"];

		$sql = "insert into sum_wkfwapvmng ";
		$sql .= "(wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, next_notice_recv_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_div5_flg, apv_div6_flg, apv_num, apv_common_group_id) values(";
		$content = array($wkfw_id, $apv_order, $deci_flg, $target_class_div, $multi_apv_flg, $next_notice_div, $next_notice_recv_div, $apv_div0_flg, $apv_div1_flg, $apv_div2_flg, $apv_div3_flg, $apv_div4_flg, $apv_div5_flg, $apv_div6_flg, $apv_num, $apv_common_group_id);

		$this->insert_into_table_or_die($sql, $content);
	}

	// 部署役職指定登録
	function regist_wkfwapvpstdtl($wkfw_id, $apv_order, $st_id, $st_div, $mode) {
		$sql = "insert into sum_wkfwapvpstdtl (wkfw_id, apv_order, st_id, st_div) values(";
		$content = array($wkfw_id, $apv_order, $st_id, $st_div);
		$this->insert_into_table_or_die($sql, $content);
	}

	// 職員指定登録
	function regist_wkfwapvdtl($wkfw_id, $apv_order, $emp_id, $apv_sub_order, $mode) {
		$sql = "insert into sum_wkfwapvdtl (wkfw_id, apv_order, emp_id, apv_sub_order) values(";
		$content = array($wkfw_id, $apv_order, $emp_id, $apv_sub_order);
		$this->insert_into_table_or_die($sql, $content);
	}

	// 委員会・ＷＧ登録
	function regist_wkfwpjtdtl($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id, $mode) {
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;
		$sql = "insert into sum_wkfwpjtdtl (wkfw_id, apv_order, parent_pjt_id, child_pjt_id) values(";
		$content = array($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id);
		$this->insert_into_table_or_die($sql, $content);
	}

	// 部署役職(部署指定)
	function regist_wkfwapvsectdtl($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id, $mode) {
		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;
		$sql = "insert into sum_wkfwapvsectdtl (wkfw_id, apv_order, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id);
		$this->insert_into_table_or_die($sql, $content);
	}

	// 結果通知管理登録
	function regist_wkfwnoticemng($arr, $mode) {
		$wkfw_id	   = $arr["wkfw_id"];
		$target_class_div  = $arr["target_class_div"];
		$rslt_ntc_div0_flg = $arr["rslt_ntc_div0_flg"];
		$rslt_ntc_div1_flg = $arr["rslt_ntc_div1_flg"];
		$rslt_ntc_div2_flg = $arr["rslt_ntc_div2_flg"];
		$rslt_ntc_div3_flg = $arr["rslt_ntc_div3_flg"];
		$rslt_ntc_div4_flg = $arr["rslt_ntc_div4_flg"];
		$rslt_ntc_div5_flg = $arr["rslt_ntc_div5_flg"];
		$rslt_ntc_div6_flg = $arr["rslt_ntc_div6_flg"];
		$sql = "insert into sum_wkfwnoticemng ";
		$sql .= "(wkfw_id, target_class_div, rslt_ntc_div0_flg, rslt_ntc_div1_flg, rslt_ntc_div2_flg, rslt_ntc_div3_flg, rslt_ntc_div4_flg, rslt_ntc_div5_flg, rslt_ntc_div6_flg) values(";
		$content = array($wkfw_id, $target_class_div, $rslt_ntc_div0_flg, $rslt_ntc_div1_flg, $rslt_ntc_div2_flg, $rslt_ntc_div3_flg, $rslt_ntc_div4_flg, $rslt_ntc_div5_flg, $rslt_ntc_div6_flg);		$this->insert_into_table_or_die($sql, $content);
	}

	// 部署役職指定(結果通知)登録
	function regist_wkfwnoticestdtl($wkfw_id, $st_id, $st_div, $mode) {
		$content = array($wkfw_id, $st_id, $st_div);
		$this->insert_into_table_or_die("insert into sum_wkfwnoticestdtl (wkfw_id, st_id, st_div) values(", $content);
	}

	// 職員指定(結果通知)登録
	function regist_wkfwnoticedtl($wkfw_id, $emp_id, $mode) {
		$content = array($wkfw_id, $emp_id);
		$this->insert_into_table_or_die("insert into sum_wkfwnoticedtl (wkfw_id, emp_id) values(", $content);
	}

	// 委員会・ＷＧ指定(結果通知)登録
	function regist_wkfwnoticepjtdtl($wkfw_id, $pjt_parent_id, $pjt_child_id, $mode) {
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;
		$content = array($wkfw_id, $pjt_parent_id, $pjt_child_id);
		$this->insert_into_table_or_die("insert into sum_wkfwnoticepjtdtl (wkfw_id, parent_pjt_id, child_pjt_id) values(", $content);
	}

	// 部署役職指定(結果通知)(部署指定)登録
	function regist_wkfwnoticesectdtl($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id, $mode) {
		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;
		$sql = "insert into  sum_wkfwnoticesectdtl (wkfw_id, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id);
		$this->insert_into_table_or_die($sql, $content);
	}

//-------------------------------------------------------------------------------------------------------------------------
// 申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 受信者管理取得
	function get_wkfwapvmng($wkfw_id) {
		$sql  = "select wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, next_notice_recv_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_div5_flg, apv_div6_flg, apv_num, apv_common_group_id ";
		$sql .= "from sum_wkfwapvmng ";
		$sql .= " where wkfw_id = $wkfw_id order by apv_order";
		$sel = $this->select_from_table_or_die($sql);

		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array("wkfw_id" => $row["wkfw_id"],
							"apv_order" => $row["apv_order"],
							"deci_flg" => $row["deci_flg"],
							"target_class_div" => $row["target_class_div"],
							"multi_apv_flg" => $row["multi_apv_flg"],
							"next_notice_div" => $row["next_notice_div"],
							"next_notice_recv_div" => $row["next_notice_recv_div"],
							"apv_div0_flg" => $row["apv_div0_flg"],
							"apv_div1_flg" => $row["apv_div1_flg"],
							"apv_div2_flg" => $row["apv_div2_flg"],
							"apv_div3_flg" => $row["apv_div3_flg"],
							"apv_div4_flg" => $row["apv_div4_flg"],
							"apv_div5_flg" => $row["apv_div5_flg"],
							"apv_div6_flg" => $row["apv_div6_flg"],
							"apv_num" => $row["apv_num"],
							"apv_common_group_id" => $row["apv_common_group_id"]
					   );
		}
		return $arr;
	}

	// 部署役職指定情報取得
	function get_wkfwapvpstdtl($wkfw_id, $apv_order, $target_class_div, $emp_id) {
		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id);

		$arr = array();
		if($target_class_div == "4") {
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info) {
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "") {
					$fourth_post_exist_flg = true;
					break;
				}
			}
			if(!$fourth_post_exist_flg) return $arr;
		}

		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from (";
		$sql .= "select emp_id from empmst where ";

		// 「部署指定しない」以外
		if($target_class_div != 0) {
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info) {
				$emp_class = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept  = $emp_info["emp_dept"];
				$emp_room  = $emp_info["emp_room"];

				if($target_class_div == "4") {
					if($emp_room == "") continue;
				}

				if($idx > 0) $sql .= "or ";

				if     ($target_class_div == "1") $sql .= "emp_class = $emp_class ";
				else if($target_class_div == "2") $sql .= "emp_attribute = $emp_attribute ";
				else if($target_class_div == "3") $sql .= "emp_dept = $emp_dept ";
				else if($target_class_div == "4") $sql .= "emp_room = $emp_room ";
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from sum_wkfwapvpstdtl where empmst.emp_st = sum_wkfwapvpstdtl.st_id and wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = 0) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0) {
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info) {
				$emp_class = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept  = $emp_info["emp_dept"];
				$emp_room  = $emp_info["emp_room"];

				if($target_class_div == "4") {
					if($emp_room == "") continue;
				}

				if($idx > 0) $sql .= "or ";

				if     ($target_class_div == "1") $sql .= "emp_class = $emp_class ";
				else if($target_class_div == "2") $sql .= "emp_attribute = $emp_attribute ";
				else if($target_class_div == "3") $sql .= "emp_dept = $emp_dept ";
				else if($target_class_div == "4") $sql .= "emp_room = $emp_room ";
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from sum_wkfwapvpstdtl where concurrent.emp_st = sum_wkfwapvpstdtl.st_id and wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = 0) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$sql .= " order by emp.emp_id asc";

		$sel = $this->select_from_table_or_die($sql);

		$idx = 1;
		while($row = pg_fetch_array($sel)) {
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array(
				"emp_id" => $row["emp_id"],
				"emp_full_nm" => $emp_full_nm,
				"st_nm" => $row["st_nm"],
				"display_caption" => $row["st_nm"],
				"apv_sub_order" => $idx
			);
			$idx++;
		}
		return $arr;
	}

	// 職員指定情報取得
	function get_wkfwapvdtl($wkfw_id, $apv_order) {
		$sql   = "select a.emp_id, a.apv_sub_order, b.emp_lt_nm, b.emp_ft_nm, d.st_nm from sum_wkfwapvdtl a ";
		$sql  .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql  .= "inner join authmst c on a.emp_id = c.emp_id ";
		$sql  .= "and c.emp_del_flg = 'f' ";
		$sql  .= "inner join stmst d on b.emp_st = d.st_id ";
		$sql  .= " where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order ";
		$sql  .= " order by apv_sub_order asc";

		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
							"emp_full_nm" => $emp_full_nm,
							"st_nm" => $row["st_nm"],
							"display_caption" => $row["st_nm"],
							"apv_sub_order" => $row["apv_sub_order"]
					   );
		}
		return $arr;
	}

	// 委員会・ＷＧ指定情報取得
	function get_wkfwpjtdtl($wkfw_id, $apv_order) {
		$sql  = "select parent_pjt_id, child_pjt_id from sum_wkfwpjtdtl where wkfw_id = $wkfw_id and apv_order = $apv_order";
		$sel = $this->select_from_table_or_die($sql);
		return array("parent_pjt_id" => pg_fetch_result($sel, 0, "parent_pjt_id"), "child_pjt_id" => pg_fetch_result($sel, 0, "child_pjt_id"));
	}

	// 病棟担当組織に所属する職員
	function get_wkfw_ward_empclass_relation($ptif_id){
		// 患者が入院していたら、事業所と病棟を求める
		$sql =
			" select bldg_cd, ward_cd from inptmst".
			" where ptif_id = '".$ptif_id."'".
			" and inpt_in_dt <= '".date("Ymd")."'".
			" and inpt_in_dt is not null and inpt_in_dt <> ''".
			" limit 1";
		$sel = $this->select_from_table_or_die($sql);
		$bldg_cd = (int)(@pg_fetch_result($sel, 0, "bldg_cd"));
		$ward_cd = (int)(@pg_fetch_result($sel, 0, "ward_cd"));

		// sot_ward_empclass_relationを求める
		$sql =
			" select * from sot_ward_empclass_relation".
			" where bldg_cd = ". $bldg_cd.
			" and ward_cd = ".$ward_cd;
		$sel = $this->select_from_table_or_die($sql);
		$class_id = (int)(@pg_fetch_result($sel, 0, "class_id"));
		$atrb_id = (int)(@pg_fetch_result($sel, 0, "atrb_id"));
		$dept_id = (int)(@pg_fetch_result($sel, 0, "dept_id"));
		$room_id = (int)(@pg_fetch_result($sel, 0, "room_id"));

		// 紐つく従業員を求める
		$sql =
			" select emp.emp_id, emp.emp_lt_nm, emp.emp_ft_nm, st.st_nm from empmst emp".
			" inner join stmst st on (st.st_id = emp.emp_st and not st.st_del_flg)".
			" where emp_class = ".$class_id.
			" and emp_attribute = ".$atrb_id.
			" and emp_dept = ".$dept_id;
		if ($room_id!="") $sql .= "and emp_room = ".$room_id;
		$sql .= " order by emp_id";
		$sel = $this->select_from_table_or_die($sql);
		$idx = 0;
		$tmpary = array();
		while($row = pg_fetch_array($sel)) {
			$idx++;
			$tmpary[] = array(
				"emp_id" => $row["emp_id"],
				"emp_full_nm" => $row["emp_lt_nm"]." ".$row["emp_ft_nm"],
				"st_nm" => $row["st_nm"],
				"display_caption" => "病棟担当",
				"apv_sub_order" => $idx
			);
		}
		return $tmpary;
	}

	function get_wkfw_common_group($apv_common_group_id){
		// 共通グループ名を求める
		$sel = $this->select_from_table_or_die("select group_nm from comgroupmst where group_id = ". (int)$apv_common_group_id);
		$group_nm = pg_fetch_result($sel, 0, "group_nm");
		// 紐つく従業員を求める
		$sql =
			" select emp.emp_id, emp.emp_lt_nm, emp.emp_ft_nm, st.st_nm from empmst emp".
			" inner join stmst st on (st.st_id = emp.emp_st and not st.st_del_flg)".
			" inner join comgroup cg on (cg.emp_id = emp.emp_id and cg.group_id = ". (int)$apv_common_group_id.")".
			" order by emp_id";
		$sel = $this->select_from_table_or_die($sql);
		$idx = 0;
		$tmpary = array();
		while($row = pg_fetch_array($sel)) {
			$idx++;
			$tmpary[] = array(
				"emp_id" => $row["emp_id"],
				"emp_full_nm" => $row["emp_lt_nm"]." ".$row["emp_ft_nm"],
				"st_nm" => $row["st_nm"],
				"display_caption" => $group_nm,
				"apv_common_group_id" => $apv_common_group_id,
				"apv_sub_order" => $idx
			);
		}
		return $tmpary;
	}

	// 委員会・ＷＧメンバー取得
	function get_project_member($parent_pjt_id, $child_pjt_id) {
		$sql  = "select emp.project_no, emp.pjt_response as emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm ";
		$sql .= "from ";
		$sql .= "( ";

		$sql .= "select varchar(1) '1' as project_no, project.pjt_id, project.pjt_response ";
		$sql .= "from project ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

		if($child_pjt_id != "") {
			$sql .= "union all ";
			$sql .= "select varchar(1) '3' as project_no, project.pjt_id, project.pjt_response ";
			$sql .= "from project ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";

		$sql .= "inner join empmst on emp.pjt_response = empmst.emp_id ";
		$sql .= "inner join authmst on emp.pjt_response = authmst.emp_id and not emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";

		$sql .= "union all ";

		$sql .= "select emp.project_no, emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm ";
		$sql .= "from ";
		$sql .= "( ";

		$sql .= "select varchar(1) '2' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id ";
		$sql .= "from project ";
		$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

		if($child_pjt_id != "") {
			$sql .= "union all ";
			$sql .= "select varchar(1) '4' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id ";
			$sql .= "from project ";
			$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";

		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$sql .= "order by project_no ";

		$sel = $this->select_from_table_or_die($sql);

		$arr = array();
		$idx = 1;
		while($row = pg_fetch_array($sel)) {
			$emp_id  = $row["emp_id"];
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$st_nm   = $row["st_nm"];

			$dpl_flg = false;

			// 重複があればセットしない。
			foreach($arr as $arr_apv) {
				if($emp_id == $arr_apv["emp_id"]) $dpl_flg = true;
			}
			if(!$dpl_flg) {
				$arr[] = array("emp_id" => $row["emp_id"], "emp_full_nm" => $emp_full_nm, "st_nm" => $row["st_nm"], "apv_sub_order" => $idx);
				$idx++;
			}
		}
		return $arr;
	}


	// 受信者情報取得
	function get_wkfwapv_info($wkfw_id, $emp_id, $ptif_id) {
		$arr_wkfwapvmng = $this->get_wkfwapvmng($wkfw_id);
		$arr = array();
		foreach($arr_wkfwapvmng as $apvmng) {
			$apvmng["apv_setting_flg"] = "f";

			$arr_apv = array();
			$multi_apv_flg = $apvmng["multi_apv_flg"];
			$apv_div0_flg  = $apvmng["apv_div0_flg"];
			$apv_div1_flg  = $apvmng["apv_div1_flg"];
			$apv_div2_flg  = $apvmng["apv_div2_flg"];
			$apv_div3_flg  = $apvmng["apv_div3_flg"];
			$apv_div4_flg  = $apvmng["apv_div4_flg"];
			$apv_div5_flg  = $apvmng["apv_div5_flg"];
			$apv_div6_flg  = $apvmng["apv_div6_flg"];

			if($apv_div0_flg == "f" && $apv_div1_flg == "f" && $apv_div2_flg == "t" && $apv_div3_flg == "f" && $apv_div4_flg == "f" && $apv_div5_flg == "f" && $apv_div6_flg == "f") {
				$apvmng["apv_setting_flg"] = "t";
			}

			// 部署役職(申請者所属)指定
			if($apv_div0_flg == "t") {
				$tmpary = $this->get_wkfwapvpstdtl($wkfw_id, $apvmng["apv_order"], $apvmng["target_class_div"], $emp_id);
				for($i=0; $i<count($tmpary); $i++) $tmpary[$i]["apv_div"] = "0";
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $tmpary);// マージ処理
			}

			// 部署役職(部署指定)指定
			if($apv_div4_flg == "t") {
				$tmpary = $this->get_apvpstdtl($wkfw_id, $apvmng["apv_order"], "4");
				$st_sect_ids = "";
				foreach($tmpary as $apvpstdtl) {
					if($st_sect_ids != "") $st_sect_ids .= ",";
					$st_sect_ids .= $apvpstdtl["st_id"];
				}
				$arr_sect_dtl = $this->get_apvsectdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_post_sect = $this->get_emp_info_for_post_sect($arr_sect_dtl["class_id"], $arr_sect_dtl["atrb_id"], $arr_sect_dtl["dept_id"], $arr_sect_dtl["room_id"], $st_sect_ids);

				for($i=0; $i<count($arr_post_sect); $i++) $arr_post_sect[$i]["apv_div"] = "4";
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_post_sect);// マージ処理
			}

			// 職員指定
			if($apv_div1_flg == "t") {
				$tmpary = $this->get_wkfwapvdtl($wkfw_id, $apvmng["apv_order"]);
				for($i=0; $i<count($tmpary); $i++) $tmpary[$i]["apv_div"] = "1";
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $tmpary);// マージ処理
			}

			// 委員会・ＷＧ
			if($apv_div3_flg == "t") {
				$arr_pjt = $this->get_wkfwpjtdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_project_member = $this->get_project_member($arr_pjt["parent_pjt_id"], $arr_pjt["child_pjt_id"]);

				$selz = $this->select_from_table_or_die("select pjt_name from project where pjt_id = ".$arr_pjt["parent_pjt_id"]);
				$pjt_nm = pg_fetch_result($selz, 0, "pjt_name");
				if($arr_pjt["child_pjt_id"] != "") {
					$selz = $this->select_from_table_or_die("select pjt_name from project where pjt_id = ".$arr_pjt["child_pjt_id"]);
					$pjt_nm .= " > ";
					$pjt_nm .= pg_fetch_result($selz, 0, "pjt_name");
				}
				for($i=0; $i<count($arr_project_member); $i++) {
					$arr_project_member[$i]["apv_div"] = "3";
					$arr_project_member[$i]["pjt_nm"] = $pjt_nm;
					$arr_project_member[$i]["parent_pjt_id"] = $arr_pjt["parent_pjt_id"];
					$arr_project_member[$i]["child_pjt_id"]  = $arr_pjt["child_pjt_id"];
					$arr_project_member[$i]["display_caption"] = $pjt_nm;
				}
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_project_member);// マージ処理
			}

			// その他
			if($apv_div2_flg == "t") {
				$arr_setting_apv = "";
				for($i=0; $i<$apvmng["apv_num"]; $i++) $arr_setting_apv[] = array();
				for($i=0; $i<count($arr_setting_apv); $i++) $arr_setting_apv[$i]["apv_div"] = "2";
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_apv);// マージ処理
			}

			// 病棟担当組織に所属する職員
			if($apv_div5_flg == "t") {
				$tmpary = $this->get_wkfw_ward_empclass_relation($ptif_id);
				for($i=0; $i<count($tmpary); $i++) $tmpary[$i]["apv_div"] = "5";
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $tmpary);// マージ処理
			}

			// 共通グループ
			if($apv_div6_flg == "t") {
				$tmpary = $this->get_wkfw_common_group($apvmng["apv_common_group_id"]);
				for($i=0; $i<count($tmpary); $i++) $tmpary[$i]["apv_div"] = "6";
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $tmpary);// マージ処理
			}

			// 複数受信者「許可する」
			if($multi_apv_flg == "t") {
				if(count($arr_apv) > 0) {
					if(count($arr_apv) > 1) {
						$apv_sub_order = 1;
						foreach($arr_apv as $apv) {
							$arr_emp = array();
							$apvmng["apv_sub_order"] = $apv_sub_order;
							$arr_emp[] = array(
								"emp_id" => @$apv["emp_id"],
								"emp_full_nm" => @$apv["emp_full_nm"],
								"st_nm" => @$apv["st_nm"],
								"apv_div" => @$apv["apv_div"],
								"pjt_nm" => @$apv["pjt_nm"],
								"parent_pjt_id" => @$apv["parent_pjt_id"],
								"child_pjt_id" => @$apv["child_pjt_id"],
								"apv_common_group_id" => @$apv["apv_common_group_id"],
								"display_caption" => @$apv["display_caption"]
							);
							$apvmng["emp_infos"] = $arr_emp;

							if($apv["apv_div"] == "2") $apvmng["apv_setting_flg"] = "t";

							$arr[] = $apvmng;
							$apv_sub_order++;
						}
					}
					else {
						$arr_emp = array();
						$apvmng["apv_sub_order"] = "";
						$arr_emp[] = array(
							"emp_id" => $arr_apv[0]["emp_id"],
							"emp_full_nm" => $arr_apv[0]["emp_full_nm"],
							"st_nm" => @$arr_apv[0]["st_nm"],
							"apv_div" => @$arr_apv[0]["apv_div"],
							"pjt_nm" => @$arr_apv[0]["pjt_nm"],
							"parent_pjt_id" => @$arr_apv[0]["parent_pjt_id"],
							"child_pjt_id" => @$arr_apv[0]["child_pjt_id"],
							"apv_common_group_id" => @$arr_apv[0]["apv_common_group_id"],
							"display_caption" => @$arr_apv[0]["display_caption"]
						);
						$apvmng["emp_infos"] = $arr_emp;
						if(@$apv["apv_div"] == "2") $apvmng["apv_setting_flg"] = "t";
						$arr[] = $apvmng;
					}
				}
				else {
					$apvmng["emp_infos"] = array();
					$arr[] = $apvmng;
				}
			}
			// 複数受信者「許可しない」
			else {
				$apvmng["apv_sub_order"] = "";
				$apvmng["emp_infos"] = $arr_apv;
				$arr[] = $apvmng;
			}
		}

		return $arr;
	}

	// ワークフロー情報取得
	function get_wkfwmst($wkfw_id) {
		$sel = $this->select_from_table_or_die("select * from sum_wkfwmst where wkfw_id = $wkfw_id");
		return pg_fetch_all($sel);
	}
	// 表示用「送信番号」を作成する。
	// 申請日は年度で出力する。
	// 送信Noは4ケタだが万一のオーバーフローを想定し、4桁を超えたらそのまま返す
	function generate_apply_no_for_display($short_wkfw_name, $apply_date, $apply_no){
		$year = substr($apply_date, 0, 4);
		$md   = substr($apply_date, 4, 4);
		if($md >= "0101" and $md <= "0331") $year = $year - 1;
		$apply_nendo = $year . substr($apply_date, 4);
		if (strlen($apply_no) < 5){
			return $short_wkfw_name . "-" . substr($apply_nendo, 0, 6) . sprintf("%04d", $apply_no);
		} else {
			return $short_wkfw_name . "-" . substr($apply_nendo, 0, 6) . $apply_no;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 下書き・更新関連
//-------------------------------------------------------------------------------------------------------------------------
	// 申請・ワークフロー情報取得
	function get_apply_wkfwmst($apply_id) {
		$sql =
			" select".
			" ap.*".
			",emp.emp_lt_nm, emp.emp_ft_nm".
			",wk.wkfw_title, wk.wkfw_folder_id, wk.wkfw_content".
			",short_wkfw_name, wk.approve_label, wk.tmpl_id, wk.is_hide_approve_status".
			",cls.class_nm as apply_class_nm".
			",atrb.atrb_nm as apply_atrb_nm".
			",dept.dept_nm as apply_dept_nm".
			",room.room_nm as apply_room_nm".
			",su.diag_div, su.ptif_id, su.smry_cmt, su.summary_seq".
			",tmpl.tmpl_name".
			",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm".
			" from sum_apply ap".
			" inner join sum_wkfwmst wk on ap.wkfw_id = wk.wkfw_id".
			" inner join empmst emp on (ap.emp_id = emp.emp_id)".
			" left join summary su on (su.summary_id = ap.summary_id and su.emp_id = ap.emp_id and ap.ptif_id = su.ptif_id)".
			" left join ptifmst pt on (pt.ptif_id = su.ptif_id)".
			" left join classmst cls on (ap.emp_class = cls.class_id)".
			" left join atrbmst atrb on (ap.emp_attribute = atrb.atrb_id)".
			" left join deptmst dept on (ap.emp_dept = dept.dept_id)".
			" left join classroom room on (ap.emp_room = room.room_id)".
			" left join tmplmst tmpl on (tmpl.tmpl_id = wk.tmpl_id)".
			" where ap.apply_id = ".(int)$apply_id;
		$sel = $this->select_from_table_or_die($sql);
		return pg_fetch_all($sel);
	}

//-------------------------------------------------------------------------------------------------------------------------
// 受信
//-------------------------------------------------------------------------------------------------------------------------
	// 閲覧可能階層状態を変更
	// 呼び元は「一覧からの一括受信」「詳細履歴画面からの受信」の２箇所
	function  approve_application($apply_id, $apv_order, $next_notice_div, $next_notice_recv_div, $session, $wkfw_appr) {

		$next_notice_div = (int)$next_notice_div;
		$next_notice_recv_div = (int)$next_notice_recv_div;

		$received_all_complete = 0;

		//==============================
		// 同報タイプの場合
		//
		// 受信、実施していない人がゼロのとき、すべてＯＫ
		//==============================
		if (($wkfw_appr) == 1) {
			$sql =
				" select count(*) from sum_applyapv".
				" where apply_id = ".$apply_id.
				" and (".
				"    (next_notice_recv_div = 1 and (apv_stat_accepted is null or apv_stat_accepted <> '1'))".
				" or (next_notice_recv_div = 2 and (apv_stat_operated is null or apv_stat_operated <> '1'))".
				" )";
			$sel = $this->select_from_table_or_die($sql);
			if (pg_fetch_result($sel, 0, 0) == 0) $received_all_complete = 1;
		}

		//==============================
		// 同報タイプではなく、稟議回覧タイプの場合
		//
		// 閲覧可能な階層を更新する。
		// 閲覧可能な階層は運用上、戻しては見えなくなってしまうので戻さない。承認外しを行った場合はスルーさせる。
		//==============================
		if (($wkfw_appr) == 2) {

			// 最終階層番号取得
			$sql = "select max(apv_order) from sum_applyapv where apply_id = ".$apply_id;
			$sel = $this->select_from_table_or_die($sql);
			$last_apv_order = pg_fetch_result($sel, 0, 0);

			// 確認するフィールド
			$apv_stat_xxxxxx = $next_notice_recv_div==2 ? "apv_stat_operated" : "apv_stat_accepted";

			// 現在の閲覧可能階層番号
			$sql = "select visibility_apv_order from sum_apply where apply_id = ".$apply_id;
			$sel = $this->select_from_table_or_die($sql);
			$visibility_apv_order = pg_fetch_result($sel, 0, 0);

			// この階層の受信者総数
			$sql =
				" select count(apv_order) from sum_applyapv".
				" where apply_id = ".$apply_id.
				" and apv_order = " .$apv_order;
			$sel = $this->select_from_table_or_die($sql);
			$approve_ttl_count = pg_fetch_result($sel, 0, 0);

			// この階層の受信者のうち、受信登録者数（または実施登録者数）を取得
			$sql =
				" select count(apv_order) from sum_applyapv".
				" where ".$apv_stat_xxxxxx." = '1' and apply_id = ".$apply_id.
				" and apv_order = " .$apv_order;
			$sel = $this->select_from_table_or_die($sql);
			$approve_ok_count = pg_fetch_result($sel, 0, 0);
				// 非同期または権限並列指定の場合は、受信者がひとりでも存在すれば次の階層を閲覧可能に
				if ($next_notice_div == 1 || $next_notice_div == 3){
					if ($approve_ok_count) {
						$sql = "update sum_apply set visibility_apv_order = ". min($last_apv_order, $apv_order + 1). " where apply_id = " . $apply_id;
						$this->update_set_table_or_die($sql, array(), array(), "");
					}
					else {
						$sql = "update sum_apply set visibility_apv_order = ". min($last_apv_order, $apv_order). " where apply_id = " . $apply_id;
						$this->update_set_table_or_die($sql, array(), array(), "");
					}
				}
				// 同期(=2)または単数受信者(=0またはカラ)の場合は、全員受信済みなら次の階層を閲覧可能に
				else {
					if ($approve_ok_count == $approve_ttl_count) {
						$sql = "update sum_apply set visibility_apv_order = ". min($last_apv_order, $apv_order + 1). " where apply_id = " . $apply_id;
						$this->update_set_table_or_die($sql, array(), array(), "");
					}
					else {
						$sql = "update sum_apply set visibility_apv_order = ". min($last_apv_order, $apv_order). " where apply_id = " . $apply_id;
						$this->update_set_table_or_die($sql, array(), array(), "");
					}
				}

			// 受信者(実施者)がひとりでもいれば、
			// 非同期または権限並列指定の場合は、他の未受信者を「他者受信済み」にする。
			// ひとりもいなければ解除。
			if ($next_notice_div == 1 || $next_notice_div == 3){
				$upd_value = ($approve_ok_count ? 1 : 0);
				$sql =
					" update sum_applyapv set ".$apv_stat_xxxxxx."_by_other = ".$upd_value.
					" where apply_id = " . $apply_id.
					" and apv_order = ". $apv_order;
				$this->update_set_table_or_die($sql, array(), array(), "");

				if ($apv_stat_xxxxxx == "apv_stat_operated"){
					// 実施を済みにした場合は、受信も済みにする。
					if ($upd_value){
						$sql =
							" update sum_applyapv set apv_stat_accepted_by_other = ".$upd_value.
							" where apply_id = " . $apply_id.
							" and apv_order = ". $apv_order;
						$this->update_set_table_or_die($sql, array(), array(), "");
					}
					// 実施を解除する場合は、受信の数を一旦、数える。
					else {
						$sql =
							" select count(apv_order) from sum_applyapv".
							" where apv_stat_accepted = '1' and apply_id = ".$apply_id.
							" and apv_order = " .$apv_order;

						$sel = $this->select_from_table_or_die($sql);
						$approve_jusin_count = pg_fetch_result($sel, 0, 0);
						$sql =
							" update sum_applyapv set apv_stat_accepted_by_other = ". ($approve_jusin_count ? 1 : 0).
							" where apply_id = " . $apply_id.
							" and apv_order = ". $apv_order;
						$this->update_set_table_or_die($sql, array(), array(), "");
					}
				}
			}




			// ここから、次階層について再調査 -------------------




			// 現在の閲覧可能階層番号その２
			$sql = "select visibility_apv_order from sum_apply where apply_id = ".$apply_id;
			$sel = $this->select_from_table_or_die($sql);
			$visibility_apv_order = pg_fetch_result($sel, 0, 0);
			// 最終階層は、受信で終了か、実施で終了か
			$sql =
				" select next_notice_recv_div from sum_applyapv".
				" where apply_id = ".$apply_id.
				" and apv_order = " . $last_apv_order.
				" limit 1";
			$sel = $this->select_from_table_or_die($sql);
			$last_notice_recv_div = pg_fetch_result($sel, 0, 0);
			$apv_stat_xxxxxx = $last_notice_recv_div==2 ? "apv_stat_operated" : "apv_stat_accepted";

			// 最終階層に達している場合
			if ($last_apv_order >= $visibility_apv_order){

				// 最終階層の受信者総数
				$sql =
					" select count(*) from sum_applyapv".
					" where apply_id = ".$apply_id.
					" and apv_order = " .$last_apv_order;
				$sel = $this->select_from_table_or_die($sql);
				$approve_ttl_count = pg_fetch_result($sel, 0, 0);

				// 最終階層の受信者のうち、受信登録者数（または実施登録者数）を取得
				$sql =
					" select count(*) from sum_applyapv".
					" where (".$apv_stat_xxxxxx." = '1' or ".$apv_stat_xxxxxx."_by_other = 1) and apply_id = ".$apply_id.
					" and apv_order = " .$last_apv_order;
				$sel = $this->select_from_table_or_die($sql);
				$approve_ok_count = pg_fetch_result($sel, 0, 0);

				// 非同期または権限並列指定の場合は、受信者がひとりでも存在すればオール完了
				if ($next_notice_div == 1 || $next_notice_div == 3){
					if ($approve_ok_count) $received_all_complete = 1;
				}
				// 同期の場合か、単数の場合は、全員受信済みならオール完了
				else {
					if ($approve_ok_count == $approve_ttl_count) $received_all_complete = 1;
				}
			}
		}

		//==============================
		// 同報にせよ稟議回覧にせよ、すべての受信が完了した場合
		//==============================
		if ($received_all_complete){
			$sql = "update sum_apply set is_approve_complete = 1 where apply_id = " . $apply_id;
			$this->update_set_table_or_die($sql, array(), array(), "");
		} else {
			$sql = "update sum_apply set is_approve_complete = 0 where apply_id = " . $apply_id  . " and approve_complete_check_ymdhms is null";
			$this->update_set_table_or_die($sql, array(), array(), "");
		}
	}



//-------------------------------------------------------------------------------------------------------------------------
// ワークフロープレビュー
//-------------------------------------------------------------------------------------------------------------------------

	// 部署役職(部署指定)情報取得
	function get_apvsectdtl($wkfw_id, $apv_order) {
		$sql   = "select a.class_id, a.atrb_id, a.dept_id, a.room_id ";
		$sql  .= "from sum_wkfwapvsectdtl a ";
		$sql  .= " where wkfw_id = $wkfw_id and apv_order = $apv_order";
		$sel = $this->select_from_table_or_die($sql);
		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");
		return array("class_id" => $class_id, "atrb_id" => $atrb_id, "dept_id" => $dept_id, "room_id" => $room_id);
	}

	// 役職情報取得
	function get_apvpstdtl($wkfw_id, $apv_order, $st_div) {
		$sql   = "select st_id from sum_wkfwapvpstdtl where wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = $st_div order by st_id";
		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array("st_id" => $row["st_id"]);
		}
		return $arr;
	}

	// 職員情報取得(部署役職指定用)
	function get_emp_info_for_post_sect($class_id, $attribute_id, $dept_id, $room_id, $st_sect_id) {

		$sql  = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, c.st_nm ";
		$sql .= "from ( ";
		$sql .= "select varchar(1) '1' as type, emp_id, emp_lt_nm, emp_ft_nm, emp_st from empmst ";

		if($class_id != "")     $sql .= "where emp_class = $class_id ";
		if($attribute_id != "") $sql .= "and emp_attribute = $attribute_id ";
		if($dept_id != "")      $sql .= "and emp_dept = $dept_id ";
		if($room_id != "")      $sql .= "and emp_room = $room_id ";
		if($st_sect_id)         $sql .= "and emp_st in ($st_sect_id) ";

		$sql .= "union ";
		$sql .= "select varchar(1) '2' as type, sub_a.emp_id, sub_b.emp_lt_nm, sub_b.emp_ft_nm, sub_a.emp_st from concurrent sub_a ";
		$sql .= "inner join empmst sub_b on sub_a.emp_id = sub_b.emp_id ";

		if($class_id != "")     $sql .= "where sub_a.emp_class = $class_id ";
		if($attribute_id != "") $sql .= "and sub_a.emp_attribute = $attribute_id ";
		if($dept_id != "")      $sql .= "and sub_a.emp_dept = $dept_id ";
		if($room_id != "")      $sql .= "and sub_a.emp_room = $room_id ";
		if($st_sect_id)         $sql .= "and sub_a.emp_st in ($st_sect_id) ";

		$sql .= ") a ";
		$sql .= "inner join authmst b on a.emp_id = b.emp_id and not b.emp_del_flg ";
		$sql .= "left join stmst c on a.emp_st = c.st_id and not c.st_del_flg ";
		$sql .= "order by a.emp_id asc, a.type asc";

		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		$tmp_emp_id = "";
		$idx = 1;
		while($row = pg_fetch_array($sel)) {
			if($tmp_emp_id == $row["emp_id"]) continue;
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array(
				"emp_id" => $row["emp_id"],
				"emp_full_nm" => $emp_full_nm,
				"st_nm" => $row["st_nm"],
				"display_caption" => $row["st_nm"],
				"apv_sub_order" => $idx
			);
			$tmp_emp_id = $row["emp_id"];
			$idx++;
		}
		return $arr;
	}
//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー・本体情報関連
//-------------------------------------------------------------------------------------------------------------------------
	// ワークフローＩＤのＭＡＸ値取得
	function get_max_wkfw_id($mode) {
		$sel = $this->select_from_table_or_die("select max(wkfw_id) as max from sum_wkfwmst");
		return pg_fetch_result($sel, 0, "max");
	}

//-------------------------------------------------------------------------------------------------------------------------
// 共通
//-------------------------------------------------------------------------------------------------------------------------

	// 職員情報取得
	function get_empmst($session) {
		$sel = $this->select_from_table_or_die("select * from empmst where emp_id in (select emp_id from session where session_id='$session')");
		return pg_fetch_all($sel);
	}

//-------------------------------------------------------------------------------------------------------------------------
// その他
//-------------------------------------------------------------------------------------------------------------------------

	// マージ処理
	function merge_arr_emp_info($arr_all_emp_info, $arr_target_emp_info) {
		$arr_tmp_apv = array();
		foreach($arr_target_emp_info as $target_emp_info) {
			$dpl_flg = false;
			foreach($arr_all_emp_info as $all_emp_info) {
				if($target_emp_info["emp_id"] == $all_emp_info["emp_id"]) $dpl_flg = true;
			}
			if(!$dpl_flg) $arr_tmp_apv[] = $target_emp_info;
		}
		for($i=0; $i<count($arr_tmp_apv); $i++) array_push($arr_all_emp_info, $arr_tmp_apv[$i]);
		return $arr_all_emp_info;
	}

    // 室名称取得
    function get_room_nm($room_id)
    {
        $sql  = "select * from classroom";
        $cond = "where room_id = $room_id";

        $sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
        if($sel == 0)
        {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $room_nm = pg_result($sel,0,"room_nm");
        return $room_nm;
    }

/*
	// 申請結果通知登録
	function regist_applynotice($apply_id, $recv_emp_id, $rslt_ntc_div, $next_notice_recv_div) {
		$sql = "insert into sum_applynotice ( apply_id, recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div) values (";
		$content = array($apply_id, $recv_emp_id, "f", null, null, "f", $rslt_ntc_div);
		$this->insert_into_table_or_die($sql, $content);
	}
*/
/*
	// 年度の申請件数取得
	function get_new_apply_no_per_year_month(){
		$sel = $this->select_from_table_or_die("select max(apply_no) as max_apply_no from sum_apply where apply_date like '".date("Ym")."%'");
		return ((int)pg_fetch_result($sel, 0, "max_apply_no")) + 1;
	}
*/
/*
	// 添付ファイル登録
	function regist_applyfile($apply_id, $applyfile_no, $applyfile_name) {
		$content = array($apply_id, $applyfile_no, $applyfile_name, "f");
		$this->insert_into_table_or_die("insert into sum_applyfile (apply_id, applyfile_no, applyfile_name, delete_flg) values (", $content);
	}
*/

/*
	// 添付ファイル情報取得
	function get_applyfile($apply_id) {
		$sql  = "select applyfile_no, applyfile_name from sum_applyfile";
		$sql .= " where apply_id = ".(int)$apply_id;

		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array("applyfile_no" => $row["applyfile_no"], "applyfile_name" => $row["applyfile_name"]);

		}
		return $arr;
	}
*/
/*
	// 受信更新
	function update_applyapv($arr) {
		$wkfw_id = $arr["wkfw_id"];
		$apply_id   = $arr["apply_id"];
		$apv_order  = $arr["apv_order"];
		$apv_sub_order  = $arr["apv_sub_order"];
		$emp_id = $arr["emp_id"];
		$st_div = $arr["st_div"];
		$emp_class  = $arr["emp_class"];
		$emp_attribute  = $arr["emp_attribute"];
		$emp_dept   = $arr["emp_dept"];
		$emp_st = $arr["emp_st"];
		$emp_room   = $arr["emp_room"];
		$parent_pjt_id  = $arr["parent_pjt_id"];
		$multi_apv_flg = $arr["multi_apv_flg"];
		$next_notice_div  = $arr["next_notice_div"];
		$next_notice_recv_div = $arr["next_notice_recv_div"];
		$child_pjt_id   = $arr["child_pjt_id"];

		$sql = "update sum_applyapv set";
		$set = array("wkfw_id", "emp_id", "st_div", "emp_class", "emp_attribute", "emp_dept", "emp_st", "emp_room", "parent_pjt_id", "child_pjt_id", "multi_apv_flg", "next_notice_div", "next_notice_recv_div");
		$setvalue = array($wkfw_id, $emp_id, $st_div, $emp_class, $emp_attribute, $emp_dept, $emp_st, $emp_room, $parent_pjt_id, $child_pjt_id, $multi_apv_flg, $next_notice_div, $next_notice_recv_div);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order ";

		if($apv_sub_order != "") {
			$cond .= "and apv_sub_order = $apv_sub_order";
		}

		$this->update_set_table_or_die($sql, $set, $setvalue, $cond);
	}
*/
/*
//-------------------------------------------------------------------------------------------------------------------------
// 非同期・同期通知関連
//-------------------------------------------------------------------------------------------------------------------------
	// 非同期・同期受信登録
	function regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order) {
		$sql = "insert into sum_applyasyncrecv (apply_id, send_apv_order, send_apv_sub_order, recv_apv_order, recv_apv_sub_order, send_apved_order, apv_show_flg, delete_flg) values (";
		$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order, null, "f", "f");
		$this->insert_into_table_or_die($sql, $content);
	}
*/
/*
	// 指定した受信ステータス数取得
	function get_apvstatcnt_accepted($apply_id, $apv_stat_accepted) {
		$sql = "select count(*) as cnt from sum_applyapv ";
		$sql .= " where apply_id = $apply_id and apv_stat_accepted = '$apv_stat_accepted'";
		$sel = $this->select_from_table_or_die($sql);
		return pg_fetch_result($sel, 0, "cnt");
	}
*/
/*
	// 指定した実施ステータス数取得
	function get_apvstatcnt_operated($apply_id, $apv_stat_operated) {
		$sql = "select count(*) as cnt from sum_applyapv ";
		$sql .= " where apply_id = $apply_id and apv_stat_operated = '$apv_stat_operated'";
		$sel = $this->select_from_table_or_die($sql);
		return pg_fetch_result($sel, 0, "cnt");
	}
*/
/*
	// 同一階層で指定した受信ステータス数取得
	function get_same_hierarchy_apvstatcnt_accepted($apply_id, $apv_order, $apv_stat) {
		$sql =
			" select count(*) as cnt from sum_applyapv".
			" where apply_id = ".$apply_id." and apv_order = ".$apv_order.
			" and apv_stat_accepted = '$apv_stat'";
		$sel = $this->select_from_table_or_die($sql);
		return pg_fetch_result($sel, 0, "cnt");
	}
*/
/*
	function get_same_hierarchy_apvstatcnt_operated($apply_id, $apv_order, $apv_stat) {
		$sql =
			" select count(*) as cnt from sum_applyapv".
			" where apply_id = ".$apply_id." and apv_order = ".$apv_order.
			" and apv_stat_operated = '$apv_stat'";
		$sel = $this->select_from_table_or_die($sql);
		return pg_fetch_result($sel, 0, "cnt");
	}
*/
/*
	// 受信テーブル更新処理（権限並列用）
	function update_applyapv_accepted_for_parallel($apply_id, $apv_order, $apv_date, $apv_stat) {
		$sql = "update sum_applyapv set";
		$set = array("apv_stat_accepted", "other_apv_flg", "apv_date");
		$setvalue = array($apv_stat, "t", $apv_date);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_stat_accepted = '0'";
		$this->update_set_table_or_die($sql, $set, $setvalue, $cond);
	}

	// 受信テーブル更新処理（権限並列用）
	function update_applyapv_operated_for_parallel($apply_id, $apv_order, $apv_date, $apv_stat) {
		$sql = "update sum_applyapv set";
		$set = array("apv_stat_operated", "other_apv_flg", "apv_date");
		$setvalue = array($apv_stat, "t", $apv_date);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_stat_operated = '0'";
		$this->update_set_table_or_die($sql, $set, $setvalue, $cond);
	}
*/
//-------------------------------------------------------------------------------------------------------------------------
// 申請結果通知関連
//-------------------------------------------------------------------------------------------------------------------------
/*
	// 確認済みフラグ更新
	function update_confirmed_flg($apply_id, $session) {
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql = "update sum_applynotice set";
		$set = array("confirmed_flg");
		$setvalue = array("t");
		$cond = "where not confirmed_flg and apply_id = $apply_id and recv_emp_id = '$emp_id' ";
		$this->update_set_table_or_die($sql, $set, $setvalue, $cond);
	}
*/
/*
	// 全受信者数取得
	function get_allapvcnt($apply_id) {
		$sel = $this->select_from_table_or_die("select count(*) as cnt from sum_applyapv where apply_id = $apply_id");
		return pg_fetch_result($sel, 0, "cnt");
	}
*/
/*
	// 申請ステータス更新
	function update_applystat($apply_id, $apply_stat, $session) {
		$set = array("apply_stat");
		$setvalue = array($apply_stat);
		$cond = "where apply_id = $apply_id";
		$this->update_set_table_or_die("update sum_apply set", $set, $setvalue, $cond);
		// 申請結果通知更新
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];
		$this->update_send_applynotice($apply_id, $emp_id);
	}
*/
/*
	// 申請結果通知・送信者更新
	function update_send_applynotice($apply_id, $send_emp_id) {
		$date = date("YmdHi");

		$sql = "update sum_applynotice set";
		$set = array("send_emp_id", "send_date");
		$setvalue = array($send_emp_id, $date);
		$cond = "where apply_id = $apply_id";
		$this->update_set_table_or_die($sql, $set, $setvalue, $cond);
	}
*/
/*
	// 同一階層で指定した受信ステータス数取得
	function get_same_hierarchy_apvstatcnt($apply_id, $apv_order, $apv_stat) {
		$sql =
			" select count(*) as cnt from sum_applyapv".
			" where apply_id = ".$apply_id." and apv_order = ".$apv_order.
			" and (".
			"   (apv_stat_accepted = '$apv_stat' and next_notice_recv_div <> '2')".
			"	or".
			"	(apv_stat_operated = '$apv_stat' and next_notice_recv_div = '2')".
			" )";
		$sel = $this->select_from_table_or_die($sql);
		return pg_fetch_result($sel, 0, "cnt");
	}
*/
/*
	// 添付ファイル削除
	function delete_applyfile($apply_id) {
		$this->delete_from_table_or_die("delete from sum_applyfile where apply_id = $apply_id", "");
	}
*/
/*
	// 申請結果通知削除
	function delete_applynotice($apply_id) {
		$this->delete_from_table_or_die("delete from sum_applynotice where apply_id = $apply_id", "");
	}
*/
/*
	// 受信情報取得
	function get_applyapv($apply_id) {
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, c.st_nm,  ";
		$sql .= "d.pjt_name as parent_pjt_name, f.pjt_name as child_pjt_name, g.group_nm ";
		$sql .= "from sum_applyapv a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "left join stmst c on a.emp_st = c.st_id ";
		$sql .= "left join project d on a.parent_pjt_id = d.pjt_id ";
		$sql .= "left join project f on a.child_pjt_id = f.pjt_id ";
		$sql .= "left join comgroupmst g on g.group_id = a.apv_common_group_id ";
		$sql .= "where a.apply_id = ".(int)$apply_id." order by a.apv_order, a.apv_sub_order asc";

		$sel = $this->select_from_table_or_die($sql);
		$ary = (array)pg_fetch_all($sel);
		if (!$ary || !$ary[0]) return array();
		for($i = 0; $i < count($ary); $i++){
			if (!count($ary[$i])) continue;
			$st_div = $ary[$i]["st_div"];
			if ($st_div == "5") { $ary[$i]["display_caption"] = "病棟担当"; continue; }
			if ($st_div == "6") { $ary[$i]["display_caption"] = $ary[$i]["group_nm"]; continue; }
			if ($st_div == "3") {
				$ary[$i]["display_caption"] = $ary[$i]["parent_pjt_name"];
				if ($ary[$i]["child_pjt_name"] != "") $ary[$i]["display_caption"] .= " > ".$ary[$i]["child_pjt_name"];
				continue;
			}
			$ary[$i]["display_caption"] = $ary[$i]["st_nm"];
		}
		return $ary;
	}
*/
/*
	// 受信者候補情報取得
	function get_applyapvemp($apply_id, $apv_order) {
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, d.st_nm, c.emp_del_flg, ";
		$sql .= "f.pjt_name as parent_pjt_name, g.pjt_name as child_pjt_name ";
		$sql .= "from sum_applyapvemp a ";
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql .= "inner join authmst c on b.emp_id = c.emp_id ";
		$sql .= "left join stmst d on b.emp_st = d.st_id ";
		$sql .= "left join project f on a.parent_pjt_id = f.pjt_id ";
		$sql .= "left join project g on a.child_pjt_id = g.pjt_id ";
		$sql .= " where a.apply_id = $apply_id and a.apv_order = $apv_order order by a.person_order asc";

		$sel = $this->select_from_table_or_die($sql);
		return pg_fetch_all($sel);
	}
*/
/*
	// 申請結果通知情報取得
	function get_applynotice($apply_id) {
		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		$sql  .= "from sum_applynotice a ";
		$sql  .= "left join empmst b on a.recv_emp_id = b.emp_id ";
		$sql  .= " where a.apply_id = ".(int)$apply_id." order by a.oid ";

		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array(
				"apply_id" => $row["apply_id"],
				"recv_emp_id" => $row["recv_emp_id"],
				"confirmed_flg" => $row["confirmed_flg"],
				"send_emp_id" => $row["send_emp_id"],
				"send_date" => $row["send_date"],
				"delete_flg" => $row["delete_flg"],
				"rslt_ntc_div" => $row["rslt_ntc_div"],
				"emp_lt_nm" => $row["emp_lt_nm"],
				"emp_ft_nm" => $row["emp_ft_nm"]
			);
		}
		return $arr;
	}
*/
	/**
	 * ワークフローカテゴリ/フォルダ情報取得
	 * @return   array  ワークフローカテゴリ/フォルダ情報配列
	 */
/*
	function get_workflow_folder_list(&$wkfw_counts) {
		$category_list = $this->get_workflow_category();
		foreach($category_list as $i => $category) {
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree("", $wkfw_type);
			$category_list[$i] = $category;
		}
		// フォルダごとのワークフロー数を算出
		$wkfw_counts = $this->calc_workflow_for_count($category_list, "workflow", array(), "");
		return $category_list;
	}
*/

/*
	// 非同期・同期受信更新
	function update_apv_show_flg($apply_id, $send_apv_order, $send_apv_sub_order, $send_apved_order) {
		$sql = "update sum_applyasyncrecv set";
		$set = array("apv_show_flg", "send_apved_order");
		$setvalue = array("t", $send_apved_order);
		if($send_apv_sub_order != "") {
			$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order and send_apv_sub_order = $send_apv_sub_order";
		} else {
			$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order and send_apv_sub_order is null";
		}
		$this->update_set_table_or_die($sql, $set, $setvalue, $cond);
	}
*/

/*
	// 階層ごとの受信者情報取得
	function get_applyapv_per_hierarchy($apply_id, $apv_order) {
		$sel = $this->select_from_table_or_die("select * from sum_applyapv where apply_id = $apply_id and apv_order = $apv_order");
		return pg_fetch_all($sel);
	}
*/
/*
	// 非同期・同期受信情報取得
	function get_applyasyncrecv($apply_id, $recv_apv_order, $recv_apv_sub_order, $send_apved_order) {
		$sql  = "select * from sum_applyasyncrecv ";
		$sql .= "where apply_id = $apply_id and ";
		$sql .= "recv_apv_order = $recv_apv_order and ";
		if($recv_apv_sub_order != "") {
			$sql .= "recv_apv_sub_order = $recv_apv_sub_order and ";
		} else {
			$sql .= "recv_apv_sub_order is null and ";
		}
		$sql .= "send_apved_order <= $send_apved_order ";
		$sql .= "order by send_apved_order asc ";

		$sel = $this->select_from_table_or_die($sql);
		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array("apply_id" => $row["apply_id"],
							"send_apv_order" =>  $row["send_apv_order"],
							"send_apv_sub_order" =>  $row["send_apv_sub_order"],
							"recv_apv_order" =>  $row["recv_apv_order"],
							"recv_apv_sub_order" =>  $row["recv_apv_sub_order"],
							"send_apved_order" =>  $row["send_apved_order"]
					   );
		}
		return $arr;
	}
*/

/*
	// 部署役職指定情報取得(プレビュー用)
	function get_apvpstdtl_for_wkfwpreview($target_class_div, $st_id, $emp_id) {
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id);

		if($target_class_div == "4") {
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info) {
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "") {
					$fourth_post_exist_flg = true;
					break;
				}
			}
			if(!$fourth_post_exist_flg) return $arr;
		}


		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
		$sql .= "(";
		$sql .= "select emp_id from empmst where ";
		// 「部署指定しない」以外
		if($target_class_div != 0) {
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info) {
				$emp_class = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept  = $emp_info["emp_dept"];
				$emp_room  = $emp_info["emp_room"];

				if($target_class_div == "4") {
					if($emp_room == "") continue;
				}

				if($idx > 0) $sql .= "or ";

				if     ($target_class_div == "1") $sql .= "emp_class = $emp_class ";
				else if($target_class_div == "2") $sql .= "emp_attribute = $emp_attribute ";
				else if($target_class_div == "3") $sql .= "emp_dept = $emp_dept ";
				else if($target_class_div == "4") $sql .= "emp_room = $emp_room ";
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}

		$sql .= "empmst.emp_st in ($st_id) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0) {
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info) {
				$emp_class = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept  = $emp_info["emp_dept"];
				$emp_room  = $emp_info["emp_room"];

				if($target_class_div == "4") {
					if($emp_room == "") continue;
				}

				if($idx > 0) $sql .= "or ";

				if     ($target_class_div == "1") $sql .= "emp_class = $emp_class ";
				else if($target_class_div == "2") $sql .= "emp_attribute = $emp_attribute ";
				else if($target_class_div == "3") $sql .= "emp_dept = $emp_dept ";
				else if($target_class_div == "4") $sql .= "emp_room = $emp_room ";
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "concurrent.emp_st in ($st_id) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$sql .= " order by emp.emp_id asc ";

		$sel = $this->select_from_table_or_die($sql);
		$idx = 1;
		while($row = pg_fetch_array($sel)) {
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
							"emp_full_nm" => $emp_full_nm,
							"st_nm" => $row["st_nm"]
					   );
			$idx++;
		}
		return $arr;
	}
*/



/*
	// 申請者以外の結果通知者取得
	function get_wkfw_notice_for_apply($wkfw_id,
										$notice_target_class_div,
										$rslt_ntc_div0_flg,
										$rslt_ntc_div1_flg,
										$rslt_ntc_div2_flg,
										$rslt_ntc_div3_flg,
										$rslt_ntc_div4_flg,
										$emp_id) {
		return array();
		$arr_tmp = array();
		// 部署役職指定(申請書所属)
		if($rslt_ntc_div0_flg == "t") {
			$tmpary = $this->get_wkfwnoticestdtl($wkfw_id, "0", "ALIAS");
			$notice_st_id = "";
			foreach($tmpary as $wkfwnoticestdtl) {
				if($notice_st_id != "") $notice_st_id .= ",";
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$tmpary = $this->get_apvpstdtl_for_wkfwpreview($notice_target_class_div, $notice_st_id, $emp_id);
			for($i=0; $i<count($tmpary); $i++) $tmpary[$i]["rslt_ntc_div"] = "0";
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $tmpary);// マージ処理
		}

		// 部署役職指定(部署指定)
		if($rslt_ntc_div4_flg == "t") {
			$tmpary = $this->get_wkfwnoticesectdtl($wkfw_id, "ALIAS");
			$tmpary = $this->get_wkfwnoticestdtl($wkfw_id, "4", "ALIAS");
			$notice_st_id = "";
			foreach($tmpary as $wkfwnoticestdtl) {
				if($notice_st_id != "") $notice_st_id .= ",";
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$arr_post_sect = $this->get_emp_info_for_post_sect($tmpary["class_id"],
													$tmpary["atrb_id"],
													$tmpary["dept_id"],
													$tmpary["room_id"],
													$notice_st_id);

			for($i=0; $i<count($arr_post_sect); $i++) $arr_post_sect[$i]["rslt_ntc_div"] = "4";
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_post_sect);// マージ処理
		}

		// 職員指定
		if($rslt_ntc_div1_flg == "t") {
			$arr_emp_info = array();
			$tmpary = $this->get_wkfwnoticedtl($wkfw_id, "ALIAS");
			foreach($tmpary as $wkfwnoticedtl) {
				$sqlz  = "select empmst.*, stmst.st_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm ";
				$sqlz .= "from empmst ";
				$sqlz .= "inner join authmst on empmst.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
				$sqlz .= "left join stmst on empmst.emp_st = stmst.st_id ";
				$sqlz .= "left join classmst on empmst.emp_class = classmst.class_id ";
				$sqlz .= "left join atrbmst on empmst.emp_attribute = atrbmst.atrb_id ";
				$sqlz .= "left join deptmst on empmst.emp_dept = deptmst.dept_id ";
				$sqlz .= "left join classroom on empmst.emp_room = classroom.room_id ";
				$sqlz .= "where empmst.emp_id = '".pg_escape_string($wkfwnoticedtl["emp_id"])."'";
				$arr_empmst_detail = $this->select_from_table_or_die($sqlz);
				$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
				$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
				$emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
				$arr_emp_info[] = array(
							 "emp_id" => $arr_empmst_detail[0]["emp_id"],
							 "emp_full_nm" => $emp_full_nm,
							 "st_nm" => $arr_empmst_detail[0]["st_nm"]
							 );
			}

			for($i=0; $i<count($arr_emp_info); $i++) $arr_emp_info[$i]["rslt_ntc_div"] = "1";
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_emp_info);// マージ処理
		}

		// 委員会・ＷＧ指定
		if($rslt_ntc_div3_flg == "t") {
			$tmpary = $this->get_wkfwnoticepjtdtl($wkfw_id, "ALIAS");
			$arr_project_member = $this->get_project_member($tmpary[0]["pjt_parent_id"], $tmpary[0]["pjt_child_id"]);
			for($i=0; $i<count($arr_project_member); $i++) $arr_project_member[$i]["rslt_ntc_div"] = "3";
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_project_member);// マージ処理
		}

		// 申請者を除去する
		$arr = array();
		foreach($arr_tmp as $tmp) {
			$tmp_emp_id = $tmp["emp_id"];
			if($tmp_emp_id == $emp_id) continue;
			$arr[] = $tmp;
		}
		return $arr;
	}
*/

}
