<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function go_item_edit(grp_code, easy_item_code)
{
    var url = "hiyari_item_edit.php?session=<?=$session?>&grp_code=" + grp_code + "&easy_item_code=" + easy_item_code;
    show_rp_sub_window(url);
}

function go_incident_item_edit()
{
    var url = "hiyari_super_item.php?session=<?=$session?>";
    show_rp_sub_window(url);
}

function go_incident_item_2010_edit()
{
    var url = "hiyari_super_item_2010.php?session=<?=$session?>";
    show_rp_sub_window(url);
}

function go_incident_ic_patient_edit()
{
    var url = "hiyari_ic_super_patient.php?session=<?=$session?>";
    show_rp_sub_window(url);
}

function go_time_divide_setteing() {
    var url = "hiyari_item_time_setting.php?session=<?=$session?>";
    show_rp_sub_window(url);
}

function show_rp_sub_window(url)
{
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

    window.open(url, '_blank',option);
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.in_list {border-collapse:collapse;}
.in_list td {border:#FFFFFF solid 0px;}

</style>
</head>
<body style="color:#000; background-color:#fff; margin:0;">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- 本体 START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="600" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<form name="form1" action="hiyari_item_list.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_time_divide_setteing();">発見時間帯・発生時間帯</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(110,60);">発生場所</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(116,10);">発生部署</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(3000,10);">発見者</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(3050,30);">当事者職種</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(3500,150);">当事者以外の関連職種</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(3400,120);">勤務形態</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(200,10);">患者の数</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(230,60);">患者区分１</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(240,70);">患者区分２</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(290,120);">インシデント直前の患者の状態</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(130,90);">関連診療科</a></div>

<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> アセスメント・患者の状態</div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(150,3);">転倒・転落アセスメントスコア</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(150,10);">転倒・転落の危険度</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(170,10);">リスク回避器具</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(180,10);">拘束用具</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(190,10);">身体拘束行為</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(160,10);">ルート関連（自己抜去）</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(150,40);">日常生活自立度（認知症高齢者）</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(150,50);">日常生活自立度（寝たきり度）</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(150,60);">機能的自立度評価法（FIM）</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(150,70);">バーサルインデックス</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(150,80);">皮膚トラブル・レベル</a></div>

<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_incident_item_edit();">発生した場面と内容に関する情報</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_incident_item_2010_edit();">概要・場面・内容（2010年版）</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_incident_ic_patient_edit();">患者への影響と対応(カスタマイズ用)</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(600,10);">発生要因</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(125,85);">分類</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(96,20);">ヒヤリ・ハットの影響度</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(510,20);">再発防止に資する警鐘的事例</a></div>

<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> リスクの評価</div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(1500,10);">重大性</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(1502,10);">緊急性</a></div>
<div style="margin:0 0 4px 22px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(1504,10);">頻度</a></div>

<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(1510,10);">リスクの予測</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(1520,10);">システム改善の必要性</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(1530,10);">教育研修への活用</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(590,90);">事故調査委員会設置の有無</a></div>
<div style="margin:0 0 4px 10px;"><img src="img/point.gif" width="10" height="12"> <a href="javascript:go_item_edit(4000,4);">関係者・物・システム(事例詳細内容)</a></div>

</font>
</form>
<!-- 送信領域 END -->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->

</td>
</tr>
</table>
<!-- 本体 END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
pg_close($con);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
