<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?
///ini_set("display_errors","1");
ini_set("max_execution_time", 0);
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_menu_common_class.php");
require_once("duty_shift_total_common_class.php");
require_once("timecard_common_class.php");
require_once("show_attendance_pattern.ini");
require_once("date_utils.php");
require_once("show_timecard_common.ini");
require_once("atdbk_workflow_common_class.php");
require_once("atdbk_info_common.php");
//require_once("get_menu_label.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 勤務シフト作成
//$shift_menu_label = get_shift_menu_label($con, $fname);
$shift_menu_label = "勤務シフト作成";

$staff_id = split(",", $staff_ids);
$data_cnt = count($staff_id);

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_menu = new duty_shift_menu_common_class($con, $fname);
$obj_total = new duty_shift_total_common_class($con, $fname);
//ワークフロー関連共通クラス
$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

//期間、出勤表の締め日
$sql = "select * from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	for ($i = 1; $i <= 5; $i++) {
		$var_name = "fraction$i";
		$var_name_min = "fraction{$i}_min";
		$$var_name = pg_fetch_result($sel, 0, "fraction$i");
		if ($i != 3) {
			$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
		}
	}
	for ($i = 1; $i <= 5; $i++) {
		$var_name = "others$i";
		$$var_name = pg_fetch_result($sel, 0, "others$i");
	}
	$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
	$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
	$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
	//時間給者の休憩時間追加 20100929
	$rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
	$rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
	$rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
	$rest7 = intval(pg_fetch_result($sel, 0, "rest7"));
	$timecard_modify_flg = pg_fetch_result($sel, 0, "modify_flg");
	$delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
	$early_leave_time_flg = pg_fetch_result($sel, 0, "early_leave_time_flg");
	$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
	$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
} else {
	for ($i = 1; $i <= 5; $i++) {
		$var_name = "fraction$i";
		$var_name_min = "fraction{$i}_min";
		$$var_name = "";
		$$var_name_min = "";
	}
	for ($i = 1; $i <= 5; $i++) {
		$var_name = "others$i";
		$$var_name = "";
	}
	$rest1 = 0;
	$rest2 = 0;
	$rest3 = 0;
	//時間給者の休憩時間追加 20100929
	$rest4 = 0;
	$rest5 = 0;
	$rest6 = 0;
	$rest7 = 0;
	$timecard_modify_flg = "";
	$delay_overtime_flg = "1";
	$early_leave_time_flg = "2";
	$overtime_40_flg = "1";
}
//集計処理の設定を「集計しない」の固定とする 20100910
$others1 = "1";	//勤務時間内に外出した場合
$others2 = "1";	//勤務時間外に外出した場合
$others3 = "1";	//時間給者の休憩の集計
$others4 = "1";	//遅刻・早退に休憩が含まれた場合
$others5 = "2";	//早出残業時間の集計

$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
if ($modify_flg == "") {$modify_flg = "t";}


$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";

// 締め日が登録済みの場合
if ($closing != "") {

	$year = $duty_yyyy;
	$month = $duty_mm;

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
		case "1":  // 1日
			$closing_day = 1;
			break;
		case "2":  // 5日
			$closing_day = 5;
			break;
		case "3":  // 10日
			$closing_day = 10;
			break;
		case "4":  // 15日
			$closing_day = 15;
			break;
		case "5":  // 20日
			$closing_day = 20;
			break;
		case "6":  // 末日
			$start_year = $year;
			$start_month = $month;
			$start_day = 1;
			$end_year = $start_year;
			$end_month = $start_month;
			$end_day = days_in_month($end_year, $end_month);
			$calced = true;
			break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//当月〜翌月
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;

			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}

			$end_year = $year;
			$end_month = $month;
		}
	}

	$wk_start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$wk_end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
}

if ($closing_parttime == "") {
	$closing_parttime = $closing;
	$wk_start_date_parttime = $wk_start_date;
	$wk_end_date_parttime = $wk_end_date;
} else {
	$year = $duty_yyyy;
	$month = $duty_mm;

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing_parttime) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//当月〜翌月
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;

			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}

			$end_year = $year;
			$end_month = $month;
		}
	}

	$wk_start_date_parttime = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$wk_end_date_parttime = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
}

//グループ毎のテーブルから勤務時間を取得する
$timecard_common_class = new timecard_common_class($con, $fname, $staff_id, $wk_start_date, $wk_end_date);

//個人別所定時間を優先するための対応 20120309
$arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
$arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $wk_end_date);

//翌月初日までデータを取得 20130308
$wk_end_date_nextone = next_date($wk_end_date);
//$wk_end_date_nextmon = $wk_end_date;
// 処理日付の勤務日種別を取得
	$arr_type = array();
	$sql = "select date, type from calendar";
$cond = "where date >= '$wk_start_date' and date <= '$wk_end_date_nextone'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}


///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------


///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
if ($duty_mm == "") { $duty_mm = $now_mm; }
///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
$start_day = 1;
$end_day = $day_cnt;
///-----------------------------------------------------------------------------
//各変数値の初期値設定
///-----------------------------------------------------------------------------
$individual_flg = "";
$hope_get_flg = "";
//	$show_data_flg = "1";		//２行目表示データフラグ（""：予定のみ、１：勤務実績、２：勤務希望、３：当直）
if ($show_data_flg == "4") {
	$show_data_flg = "1";
}

if ($data_cnt == "") { $data_cnt = 0; }
//入力形式で表示する日（開始／終了）
$edit_start_day = 0;
$edit_end_day = 0;

//$group_id = $cause_group_id;
///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
///-----------------------------------------------------------------------------
// 指定グループの病棟名を取得
///-----------------------------------------------------------------------------
$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
if (count($wk_group) > 0) {
	$group_name = $wk_group[0]["group_name"];
	$pattern_id = $wk_group[0]["pattern_id"];
	$start_month_flg1 = $wk_group[0]["start_month_flg1"];
	$start_day1 = $wk_group[0]["start_day1"];
	$month_flg1 = $wk_group[0]["month_flg1"];
	$end_day1 = $wk_group[0]["end_day1"];
	$start_day2 = $wk_group[0]["start_day2"];
	$month_flg2 = $wk_group[0]["month_flg2"];
	$end_day2 = $wk_group[0]["end_day2"];
	$print_title = $wk_group[0]["print_title"];
}
if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}
if ($start_day2 == "" || $start_day2 == "0") {	$start_day2 = "0";}
if ($month_flg2 == "" || $end_day2 == "") {	$month_flg2 = "1";}
if ($end_day2 == "" || $end_day2 == "0") {	$end_day2 = "99";}
///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
$data_pattern = $obj->get_duty_shift_pattern_history_array($pattern_id, $duty_yyyy, $duty_mm, $data_atdptn);
$data_atdptn_all = $obj->get_atdptn_array("");
$data_pattern_all = $obj->get_duty_shift_pattern_history_array("", $duty_yyyy, $duty_mm, $data_atdptn_all);

$part_cnt = 0;
//職員ID
if ($data_cnt > 0) {
	$sql = "select a.emp_id, a.emp_personal_id, a.emp_lt_nm, a.emp_ft_nm, a.emp_sex, b.duty_form, b.irrg_type, b.wage, b.no_overtime from empmst a left join empcond b on a.emp_id = b.emp_id ";
	$cond_staff = "";
	for ($i=0; $i<$data_cnt; $i++) {
		if ($i>0) {
			$cond_staff .= ", ";
		}
		$cond_staff .= "'".$staff_id[$i]."'";
	}
	$cond = "where a.emp_id in ($cond_staff)";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$staff_info = array();
	while($row = pg_fetch_array($sel)) {
		$wk_emp_id = $row["emp_id"];
		$staff_info[$wk_emp_id]["emp_personal_id"] = $row["emp_personal_id"];
		$staff_info[$wk_emp_id]["duty_form"] = $row["duty_form"];
		$staff_info[$wk_emp_id]["staff_name"] = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
		$staff_info[$wk_emp_id]["sex"] = $row["emp_sex"];
		$staff_info[$wk_emp_id]["irrg_type"] = $row["irrg_type"];
		$staff_info[$wk_emp_id]["wage"] = $row["wage"];
		$staff_info[$wk_emp_id]["no_overtime"] = $row["no_overtime"];

		if ($row["duty_form"] == "2") {
			$part_cnt++;
		}
	}
}

//ＤＢ(atdptn)より出勤パターン情報取得
$atdptn_array = $obj->get_atdptn_array($pattern_id);

//
$total_conf = $obj_total->get_duty_shift_total_config($group_id);
$normal_overtime_flg = $total_conf["normal_overtime_flg"];
$delay_minute = $total_conf["delay_minute"];
$irrg_month_time = intval($total_conf["irrg_month_time"]);
$parttime_split_hour = $total_conf["parttime_split_hour"];
$hol_late_night_disp_flg = $total_conf["hol_late_night_disp_flg"];	//超勤休日深夜表示フラグ
$adjust_time_legal_hol_flg = $total_conf["adjust_time_legal_hol_flg"];	//繰越方法（調整時間・公休数）

$ptn_full = $obj_total->get_duty_shift_total_ptn($group_id, 1);
$tra_full = $obj_total->get_duty_shift_total_tra_ptn($group_id);

$ptn_part = $obj_total->get_duty_shift_total_ptn($group_id, 2);
$tra_part = $obj_total->get_duty_shift_total_ptn($group_id, 3);

// 手当情報取得
$arr_allowance = get_timecard_allowance($con, $fname);
$allow_full = $obj_total->get_duty_shift_total_allow($group_id, 1);
$allow_part = $obj_total->get_duty_shift_total_allow($group_id, 2);

//調整時間
$last_yyyy = $duty_yyyy;
$last_mm = $duty_mm - 1;
if ($last_mm == 0) {
	$last_yyyy--;
	$last_mm = 12;
}
//情報取得
$emp_id_str = "";
for ($i=0; $i<count($staff_id); $i++) {
	if ($i>0) {
		$emp_id_str .= ",";
	}
	$emp_id_str .= "'$staff_id[$i]'";
}
$sql = "select * from duty_shift_total_data ";
$cond = "where group_id = '$group_id' and ((duty_yyyy = $last_yyyy and duty_mm = $last_mm) or (duty_yyyy = $duty_yyyy and duty_mm = $duty_mm)) and emp_id in ($emp_id_str)";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_adjust = array();
$num = pg_num_rows($sel);
$curr_cnt = 0; //今月分登録済みか確認用
for ($i=0; $i<$num; $i++) {
	$emp_id = pg_fetch_result($sel, $i, "emp_id");
	$wk_duty_mm = pg_fetch_result($sel, $i, "duty_mm");
	//今月分
	if ($wk_duty_mm == $duty_mm) {
		$arr_adjust["$emp_id"]["adjust_time"] = pg_fetch_result($sel, $i, "adjust_time");
		$arr_adjust["$emp_id"]["adjust_time_next"] = pg_fetch_result($sel, $i, "adjust_time_next");
		$arr_adjust["$emp_id"]["memo"] = pg_fetch_result($sel, $i, "memo");
        $curr_cnt++;
	} else {
		//前月分
		$arr_adjust["$emp_id"]["adjust_time_last"] = pg_fetch_result($sel, $i, "adjust_time_next");
		$arr_adjust["$emp_id"]["memo_last"] = pg_fetch_result($sel, $i, "memo"); //前月備考がある場合はコピーする 20110720
	}
}

// 「退勤後復帰」ボタン表示フラグを取得
$sql = "select ret_btn_flg from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rolllback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");
//対象年月の公休数を取得 20100615
if ($adjust_time_legal_hol_flg == "2") {
	$legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $year, $month, $closing, $closing_month_flg, $pattern_id);
}

//休日出勤時間除外パターン
$hol_except_ptn = $obj_total->get_duty_shift_hol_except_ptn($group_id);
//休日出勤と判定する日設定取得 20100730
$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $emp_id);
//公休と判定する日設定 20100810
$arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");
?>

<title>CoMedix <? echo($shift_menu_label); ?> | 勤務集計表</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function updateData() {

	document.mainform.action = "duty_shift_total_update_exe.php";
	document.mainform.submit();
}

//調整時間変更時処理
function changeTime(pos) {
<?
//公休数対応
if ($adjust_time_legal_hol_flg != "2") {
	echo("irrg_mon = document.getElementById('irrg_mon').value;");
} else {
	echo("irrg_mon = document.getElementById('spend_hol_'+pos).value;");
}
//	irrg_mon = document.getElementById("irrg_mon").value;
$wk_str = ($adjust_time_legal_hol_flg != "2") ? "調整時間" : "公休数";
?>

	time1 = document.getElementById("adj_"+pos).value;
	time2 = document.getElementById("montime_"+pos).value;
	if (time1 == "") time1 = "0";
	if (time2 == "") time2 = "0";
	if (irrg_mon == "") irrg_mon = "0";
	if (!time1.match(/^-?\d$|^-?\d+\.?\d+$/g)) {
		alert('<?=$wk_str?>前繰越には半角数字を入力してください。');
		return false;
	}
	time3 = eval(time1) + eval(time2) - eval(irrg_mon);
	if (time3 == 0 || isNaN(time3)) time3 = "";
	document.getElementById("adjnext_"+pos).value = time3;
}
	//「ＥＸＣＥＬ作成」
	function makeExcel() {
		document.xlsForm.action = 'duty_shift_total_excel.php';
		document.xlsForm.target = 'download';
		document.xlsForm.submit();
	}
	//PDF印刷
	function printPdf() {
		window.open("",
			"pdf", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=1000,height=721");

		document.xlsForm.action = 'duty_shift_total_pdf.php';
		document.xlsForm.target = 'pdf';
		document.xlsForm.submit();
	}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? //echo("group_id=$group_id"); ?>
<? //echo("data_cnt=$data_cnt"); ?>

<?
if ($err_msg_2 != "") {
	echo($err_msg_2);
	echo("<br>\n");
} else {
	?>

	<!-- ------------------------------------------------------------------------ -->
	<!-- タイトル -->
	<!-- ------------------------------------------------------------------------ -->
	<?
	$wk_cnt = $data_cnt;
	if ($data_cnt <= 0) {
		$wk_cnt = 1;
	}
	?>

		<br>

		<!-- center -->
		<table>
		<tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- 病棟 -->
			<!-- ------------------------------------------------------------------------ -->
			<td width="1000" align="center">
			<table align="left">
			<tr height="22">
				<td width="400"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務集計表（<? echo($group_name) ?>）</font></td>
				<td align="center" width="120"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($duty_yyyy) ?>年　<? echo($duty_mm) ?>月</font></td>
			</tr>
			</table>
			</td>
    <td align="right" width="240"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
    if ($curr_cnt == 0) {
        echo("繰越情報が未登録です");
    }
                 ?></font></td>
		</tr>
		<tr>
		<td width="1000"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">常勤</font>
		</td>
		<td align="right" width="240">
		<input type="button" value="PDF印刷" onclick="printPdf();">
		<input type="button" value="Excel出力" onclick="makeExcel();">
		<input type="button" value="登録" onclick="updateData();">
		</td>
		</tr>
		</table>
		<!-- /center -->
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務表 -->
		<!-- ------------------------------------------------------------------------ -->
		<form name="mainform" method="post">
			<!-- ------------------------------------------------------------------------ -->
			<!-- 見出し -->
			<!-- ------------------------------------------------------------------------ -->
			<table id="header" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
			<tr height="22">
			<td width="80" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
			<td width="100" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
			<td width="40" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤<br>日数</font></td>
			<td width="40" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤<br>回数</font></td>
			<td width="60" align="center" rowspan="1" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休</font></td>
			<td width="80" align="center" rowspan="1" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻・早退</font></td>
		<?
	for ($i=0; $i<count($ptn_full); $i++) {
			?>
			<td width="40" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<?
		for ($j=0; $j<count($atdptn_array); $j++) {
			if ($atdptn_array[$j]["id"] == $ptn_full[$i]) {
				echo($atdptn_array[$j]["name"]);
				break;
			}
		}
			?>
			</font></td>
			<?
		}
			?>
			<td width="60" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">休日出勤</font></td>
<?
	//超勤休日深夜表示
	$wk_colspan = ($hol_late_night_disp_flg != "f") ? "4" : "3";
?>
			<td width="120" align="center" rowspan="1" colspan="<?=$wk_colspan?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">超勤時間</font></td>
			<td width="180" align="center" rowspan="1" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
	$wk_str = ($adjust_time_legal_hol_flg != "2") ? "調整時間" : "公休数";
	echo($wk_str);
?>
</font></td>

	<?
	//手当
	foreach($arr_allowance as $allowance)
	{
		$tmp_allow_id = $allowance["allow_id"];
		$tmp_allow_contents = $allowance["allow_contents"];
		$tmp_del_flg = $allowance["del_flg"];
		if ($allow_full[$tmp_allow_id] == "t") {
			?>
			<td width="40" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_allow_contents);?></font></td>
			<?
		}
	}
			?>
			<td width="40" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交通<br>費</font></td>
			<td width="120" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>

</tr>
<tr>
			<td width="20" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公</font></td>
			<td width="20" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">欠</font></td>
			<td width="20" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">有</font></td>
			<td width="40" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回数</font></td>
			<td width="40" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間</font></td>
			<td width="60" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間数</font></td>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">普通</font></td>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">深夜</font></td>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休日</font></td>
<?
	if ($hol_late_night_disp_flg != "f") {
?>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">休日<br>深夜</font></td>
<? } ?>
			<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前繰越</font></td>
			<td width="50" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">今月分</font></td>
			<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">次月<br>繰越</font></td>
</tr>

	<?
	$emp_ids1 = "";
	$wk_pos = 0;
	$exl_dat1 = array();
	for ($emp_idx=0; $emp_idx<$data_cnt; $emp_idx++) {

		$wk_emp_id = $staff_id[$emp_idx];
		//常勤のみ
		if ($staff_info[$wk_emp_id]["duty_form"] == "2") {
			continue;
		}
		$wk_pos++;
		if ($emp_ids1 != "") {
			$emp_ids1 .= ",";
		}
		$emp_ids1 .= $wk_emp_id;
		$wage = $staff_info[$wk_emp_id]["wage"];

		//一括修正申請情報
		$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($wk_emp_id, $wk_start_date, $wk_end_date);

        //個人別所定時間を優先するための対応 20120309
        $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($wk_emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

        // 実績データをまとめて取得
		$arr_atdbkrslt = array();
		$sql =	"SELECT ".
			"atdbkrslt.*, ".
			"atdptn.workday_count, ".
			"atdptn.base_time, ".
			"atdptn.over_24hour_flag, ".
			"atdptn.out_time_nosubtract_flag, ".
			"atdptn.after_night_duty_flag, ".
            "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
            "atdptn.no_count_flag, ".
            "tmmdapply.apply_id     AS tmmd_apply_id, ".
			"tmmdapply.apply_status AS tmmd_apply_status, ".
			"ovtmapply.apply_id     AS ovtm_apply_id, ".
			"ovtmapply.apply_status AS ovtm_apply_status, ".
			"rtnapply.apply_id      AS rtn_apply_id, ".
			"rtnapply.apply_status  AS rtn_apply_status ".
			"FROM ".
			"atdbkrslt ".
			"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
			"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
			"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL AND ovtmapply.apply_status != '4' ".
			"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ";
		$cond = "where atdbkrslt.emp_id = '$wk_emp_id' and ";
        $cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$wk_end_date_nextone' order by atdbkrslt.date"; //翌月初日までデータを取得 20130308
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		//出勤日数
		$work_day = 0;
		$work_count = 0;
		$sum_hol = array();
		$arr_ptn_full_cnt = array();
		$delay_cnt = 0;
		$delay_time = 0;
		$arr_allow_count = array();
		$late_night = 0;
		$hol_over = 0;
		$hol_late_night = 0;
		$hol_work = 0;
		$normal_over = 0;
		$this_month_time1 = 0;
		$this_month_time2 = 0;
		$tra_cnt = 0;
        while ($row = pg_fetch_array($sel)) {
            $date = $row["date"];
            $arr_atdbkrslt[$date] = $row;
            /*
            $arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
            $arr_atdbkrslt[$date]["reason"] = $row["reason"];
            $arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
            $arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
            $arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
            $arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
            $arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
            $arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
            for ($j = 1; $j <= 10; $j++) {
                $start_var = "o_start_time$j";
                $end_var = "o_end_time$j";
                $arr_atdbkrslt[$date][$start_var] = $row[$start_var];
                $arr_atdbkrslt[$date][$end_var] = $row[$end_var];
            }
            $arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
            $arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
            $arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
            $arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
            $arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
            $arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
            $arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
            $arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
            $arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
            $arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
            $arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
            $arr_atdbkrslt[$date]["meeting_start_time"] = $row["meeting_start_time"];
            $arr_atdbkrslt[$date]["meeting_end_time"] = $row["meeting_end_time"];
            $arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
            $arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
            $arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
            $arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
            $arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
            $arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
            //残業時刻2追加 20110621
            $arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
            $arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
            $arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
            $arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
            $arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
            */
        }
        foreach ($arr_atdbkrslt as $date => $row) {
            //翌月初日は処理しない 20130308
            if ($date >= $wk_end_date_nextone) {
                break;
            }
			// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
			if (($row["start_time"] != "" && $row["end_time"] != "") || $row["after_night_duty_flag"] == "1") {
				//休暇以外の場合に計算 20110817
				if ($row["pattern"] != "10") {
					$work_day += $row["workday_count"];
					//出勤回数をカウント(ゼロ以外の場合はすべて１として集計) 20130124
					if ($row["workday_count"] != "0" && $row["workday_count"] != "")
					{
						$work_count += 1;
					}
				}
			}

			$tmcd_group_id = $row["tmcd_group_id"];
			$pattern = $row["pattern"];
			// 処理日付の勤務日種別を取得
			$type = $arr_type[$date];
			//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
			$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
			//所定時間
			$previous_day_flag = $row["previous_day_flag"];
			$next_day_flag = $row["next_day_flag"];
			$over_24hour_flag =  $row["over_24hour_flag"];
			$start_time =  $row["start_time"];
			$end_time =  $row["end_time"];
			$reason =  $row["reason"];
			$night_duty =  $row["night_duty"];
			$ovtm_apply_status =  $row["ovtm_apply_status"];
			$rtn_apply_status =  $row["rtn_apply_status"];
			$tmmd_apply_status =  $row["tmmd_apply_status"];
			$over_start_time =  $row["over_start_time"];
			$over_end_time =  $row["over_end_time"];
			$over_start_next_day_flag = $row["over_start_next_day_flag"]; //20101027
			$over_end_next_day_flag = $row["over_end_next_day_flag"];
			$over_start_time2 =  $row["over_start_time2"];
			$over_end_time2 =  $row["over_end_time2"];
			$over_start_next_day_flag2 = $row["over_start_next_day_flag2"];
			$over_end_next_day_flag2 = $row["over_end_next_day_flag2"];
			$early_over_time =  $row["early_over_time"];

			//休憩時刻追加 20100921
			$rest_start_time = $row["rest_start_time"];
			$rest_end_time = $row["rest_end_time"];
			//外出時間を差し引かないフラグ 20110727
			$out_time_nosubtract_flag = $row["out_time_nosubtract_flag"];
			//明け追加 20110819
			$after_night_duty_flag = $row["after_night_duty_flag"];
            //時間帯画面の前日フラグ 20121120
            $atdptn_previous_day_flag = $row["atdptn_previous_day_flag"];
            //勤務時間を計算しないフラグ 20130225
            $no_count_flag = $row["no_count_flag"];

			//if ($pattern != "") {
			//	$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
			//	$end5   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );
			//}
			//深夜残業の開始終了時間
			if ($start5 == "") {
				$start5 = "2200";
			}
			if ($end5 == "") {
				$end5 = "0500";
			}
			//前日・翌日の深夜帯も追加する
			$arr_time5 = $timecard_common_class->get_late_night_shift_array($date, $start5, $end5);  // 深夜帯

			$today_holiday_flg = false;

			//休暇公休が、カウントされない不具合対応、残業時刻の判断より計算を先に行う 20110817
			if ($row["reason"] != "") {
				switch ($row["reason"]) {
					case "1":  // 有休休暇
					case "37": // 年休
						$sum_hol[2] += 1;
						break;
					case "2":  // 午前有休
					case "3":  // 午後有休
					case "38": // 午前年休
					case "39": // 午後年休
						$sum_hol[2] += 0.5;
						break;
					case "57":  // 半有半欠
						$sum_hol[2] += 0.5;
						$sum_hol[1] += 0.5;
						break;
					case "6":  // 一般欠勤
					case "7":  // 病傷欠勤
						$sum_hol[1] += 1;
						break;
					case "48": // 午前欠勤
					case "49": // 午後欠勤
						$sum_hol[1] += 0.5;
						break;
					case "24":  // 公休
					//case "22":  // 法定休暇
					//case "23":  // 所定休暇
					case "45":  // 希望(公休)
					case "46":  // 待機(公休)
					case "47":  // 管理当直前(公休)
						$sum_hol[0] += 1;
						break;
					case "35":  // 午前公休
					case "36":  // 午後公休
						$sum_hol[0] += 0.5;
						break;
					case "58":  // 半特半有
						$sum_hol[2] += 0.5;
						break;
					case "59":  // 半特半公
						$sum_hol[0] += 0.5;
						break;
					case "60":  // 半特半欠
						$sum_hol[1] += 0.5;
						break;
					case "54":  // 半夏半公
						$sum_hol[0] += 0.5;
						break;
					case "55":  // 半夏半有
						$sum_hol[2] += 0.5;
						break;
					case "56":  // 半夏半欠
						$sum_hol[1] += 0.5;
						break;
					case "62":  // 半正半有
						$sum_hol[2] += 0.5;
						break;
					case "63":  // 半正半公
						$sum_hol[0] += 0.5;
						break;
					case "64":  // 半正半欠
						$sum_hol[1] += 0.5;
						break;
					case "44":  // 半有半公
						$sum_hol[2] += 0.5;
						$sum_hol[0] += 0.5;
						break;
				}
			}
			// 休日となる設定と事由があう場合に公休に計算する 20100810
			// 法定休日
			if ($arr_type[$date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
				if ($row["reason"] == "22") {
					$sum_hol[0] += 1;
				}
			}
			// 所定休日
			if ($arr_type[$date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
				if ($row["reason"] == "23") {
					$sum_hol[0] += 1;
				}
			}
			// 年末年始
			if ($arr_type[$date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
				if ($row["reason"] == "61") {
					$sum_hol[0] += 1;
				}
				if ($row["reason"] == "62" || $row["reason"] == "63" || $row["reason"] == "64") {
					$sum_hol[0] += 0.5;
				}
			}
			//手当
			if ($row["allow_id"] != "") {
				$wk_idx = $row["allow_id"];
				$arr_allow_count[$wk_idx] += $row["allow_count"];
			}
            //出勤退勤時刻が設定済、または、公休か事由なし休暇で残業時刻が設定済
            //勤務時間を計算しないフラグが"1"以外 20130225
            if (($start_time != "" && $end_time != "" && $no_count_flag != "1") ||
					(($pattern == "10" || $after_night_duty_flag == "1") && $over_start_time != "" && $over_end_time != "")) {
				//事由に関わらず処理 20100916	 && ($reason == "22" || $reason == "23" || $reason == "24" || $reason == "34" || $reason == "")
				//休日で残業時刻がない場合は計算しない 20101005
				if ($pattern == "10" && $over_start_time == "" && $over_end_time == "" && $row["o_start_time1"] == "") { //20140218呼出も確認
					continue;
				}
				//出勤退勤時刻が未設定の場合、残業時刻を設定
				//$start_time = ($start_time == "") ? $over_start_time : $start_time;
				//$end_time = ($end_time == "") ? $over_end_time : $end_time;
				//事由に関わらず休暇で残業時刻がありの場合 20101027
				if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20111118
					 $over_start_time != "" && $over_end_time != "") {
					//残業時刻を設定
					$start_time = $over_start_time;
					$end_time = $over_end_time;
				}

				//残業終了時刻がある場合は、退勤時刻へ設定する
				if ($over_end_time != "") {
					//退勤時刻よりあとの場合 20110620
					if ($over_end_time > $end_time) {
						$end_time = $over_end_time;
					}
				}
				//日またがり対応
				if ($start_time > $end_time &&
						$previous_day_flag != "1") {
					$next_day_flag = "1";
				}
				/*
				if ($over_end_time != "") {
					//日またがり対応
					if ($over_end_next_day_flag == "1" ||
							($start_time > $over_end_time &&
								$previous_day_flag != "1")) {
						$next_day_flag = "1";
					} else {
						//残業終了時刻が当日の場合
						$next_day_flag = "0";
					}
					$end_time = $over_end_time;
				} else {
					//日またがり対応
					if ($start_time > $end_time &&
						$previous_day_flag != "1") {
						$next_day_flag = "1";
					}
				}
*/

				if ($pattern != "") {
					$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
					$end2   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
					$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
					$end4   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );

                    //個人別所定時間を優先する場合 20120309
                    if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
                        $wk_weekday = $arr_weekday_flg[$date];
                        $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $date); //個人別勤務時間帯履歴対応 20130220
                        if ($arr_empcond_officehours["officehours2_start"] != "") {
                            $start2 = $arr_empcond_officehours["officehours2_start"];
                            $end2 = $arr_empcond_officehours["officehours2_end"];
                            $start4 = $arr_empcond_officehours["officehours4_start"];
                            $end4 = $arr_empcond_officehours["officehours4_end"];
                        }
                    }

				} else {
					$start2 = "";
					$end2   = "";
					$start4 = "";
					$end4   = "";
				}

				//残業開始が予定終了時刻より前の場合に設定
				if ($over_start_time != "") {
					//残業開始日時
					if ($over_start_next_day_flag == "1" ||
							($start_time > $over_start_time &&
								$previous_day_flag != 1)) {
						$over_start_date = next_date($date);
					} else {
						$over_start_date = $date;
					}

					$over_start_date_time = $over_start_date.$over_start_time;
				}

				//残業
				$overtime_link_type = $timecard_common_class->get_link_type_overtime($date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);

				//退復 rtn_apply_statusからlink_type取得
				$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $row["o_start_time1"], $rtn_apply_status);

				//一括修正のステータスがある場合は設定
				$tmp_status = $arr_tmmdapplyall_status[$date]["apply_status"];
				if ($tmmd_apply_status == "" && $tmp_status != "") {
					$tmmd_apply_status = $tmp_status;
				}
				//時間変更 tmmd_apply_statusからlink_type取得
				$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);

				//残業承認状態
				$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);

                // 所定開始時刻が前日の場合 20121120
                if ($atdptn_previous_day_flag == "1") {
                    $tmp_prev_date = last_date($date);
					$start2_date_time = $tmp_prev_date.$start2;
					//24時間以上勤務の場合 20100203
					if ($over_24hour_flag == "1") {
						$tmp_prev_date = $date;
					}
					$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
				} else {
					$start2_date_time = $date.$start2;
					//24時間以上勤務の場合 20100203
					if ($over_24hour_flag == "1") {
						$tmp_end_date = next_date($date);
					} else {
						$tmp_end_date = $date;
					}
					$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
				}
				//休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
				if ($rest_start_time != "" && $rest_end_time != "") {
					$start4 = $rest_start_time;
					$end4 = $rest_end_time;
				}
				$start4_date_time = $date.$start4;
				$end4_date_time = $timecard_common_class->get_schedule_end_date_time($date, $start4, $end4);

				// 時間帯データを配列に格納
				$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
				// 24時間超える場合の翌日の休憩は配列にいれない 20101027
				if ($start2 < $start4) {
					$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, "");  // 休憩
				} else {
					$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
				}

				$start_date_time = $timecard_common_class->get_timecard_start_date_time($date, $start_time, $previous_day_flag);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($date, $end_time, $next_day_flag);

				// 残業管理をする場合
				if ($staff_info[$wk_emp_id]["no_overtime"] != "t") {
					$start2_date_time = str_replace(":", "", $start2_date_time); // 20100730
					// 残業申請中の場合は定時退勤時刻を退勤時刻として計算
					if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
						//早出残業の確認 20100728
						if ($start_date_time < $start2_date_time) {
							$start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
						}
						if ($end_date_time > $end2_date_time) {
							$end_date_time = $end2_date_time;
						}
					}
					//早出残業しない場合、所定にする 20100730
					if ($others5 != "1" && $start_date_time < $start2_date_time) {
						$start_date_time  = $start2_date_time;
					}
				}
				$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);

				//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
				if (count($arr_tmp_work_time) > 0){
					// 勤務パターンで時刻指定がない場合でも、端数処理する
					if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
					//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						//基準日時
						//残業時刻が入っている場合は、端数処理しない 20111118
						if ($over_start_time != "") {
                            //残業開始時刻翌日フラグ確認 20120517
                            if ($over_start_next_day_flag == "1") {
                                $fixed_start_time = next_date($date).substr($start_date_time, 8, 4);
                            }
                            else {
                                $fixed_start_time = $start_date_time;
                            }
                        }
						else {
							$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
						}
						$start_time_info = array();
					} else {
					//残業時刻が入っている場合は、端数処理しない 20111118
						if ($over_start_time != "") {
                            //残業開始時刻翌日フラグ確認 20120517
                            if ($over_start_next_day_flag == "1") {
                                $fixed_start_time = next_date($date).substr($start_date_time, 8, 4);
                            }
                            else {
                                $fixed_start_time = $start_date_time;
                            }
                        }
						else {
							$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
							$fixed_start_time = $start_time_info["fixed_time"];
						}
					//遅刻した場合、分を取得
					//端数処理をしないに強制的に設定
					$fraction3 = "2";
					$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);

					$wk_delay = $start_time_info["diff_minutes"];
					//遅刻時間
					if ($delay_minute != "" && $wk_delay >= $delay_minute) {
						$delay_cnt++;
					}
					$delay_time += $wk_delay;
				}

				// 勤務パターンで時刻指定がない場合でも、端数処理する
				if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
					//基準日時
					//残業時刻が入っている場合は、端数処理しない 20111118
						if ($over_end_time != "") {
							$fixed_end_time = $end_date_time;
						} else {
							$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
						}
//echo("fixed_end_time=$fixed_end_time");
					$end_time_info = array();
				} else {
				//早退
					//残業時刻が入っている場合は、端数処理しない 20100806
					if ($over_end_time != "") {
						$fixed_end_time = $end_date_time;
					} else {
						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
						$fixed_end_time = $end_time_info["fixed_time"];
					}

					//端数処理をしないに強制的に設定
					$fraction3 = "2";
					$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
					$wk_delay = $end_time_info["diff_minutes"];
					//早退時間
					if ($delay_minute != "" && $wk_delay >= $delay_minute) {
						$delay_cnt++;
					}
					$delay_time += $wk_delay;
				}

				switch ($reason) {
					case "2":  // 午前有休
					case "38": // 午前年休
						$tmp_start_time = $fixed_start_time;
						$tmp_end_time = $fixed_end_time;

						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

						$arr_specified_time = $arr_time2;

						//休憩を引く前の実働時間
						$arr_effective_time_wk = $arr_effective_time;
						break;
					case "3":  // 午後有休
					case "39": // 午後年休
						$tmp_start_time = $fixed_start_time;
						$tmp_end_time = $fixed_end_time;
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

						$arr_specified_time = $arr_time2;

						//休憩を引く前の実働時間
						$arr_effective_time_wk = $arr_effective_time;
						break;
					default:
						$tmp_start_time = $fixed_start_time;
						$tmp_end_time = $fixed_end_time;
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
						//休憩を引く前の実働時間
						$arr_effective_time_wk = $arr_effective_time;
						if ($wage != "4" || ($start4 != "" && $end4 != "")) { //休憩時刻がある場合を追加 20100925
							$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
						}

						$arr_specified_time = array_diff($arr_time2, $arr_time4);

						break;
				}
				//予定終了時刻から残業開始時刻の間を除外する
				if ($over_start_time != "") {
					$wk_end_date_time = $end2_date_time;

					$arr_jogai = array();

					/* 20100713
					//基準日時
					$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
					}
					*/
					//残業開始時刻があとの場合
					if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
						//時間配列を取得する（休憩扱い）
						$arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);
						//休憩扱いの時間配列を除外する
						$arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
					}

				}

				$out_time = $row["out_time"];
				$ret_time = $row["ret_time"];
				//外出復帰日時の取得
				$out_date_time = $date.$out_time;
				$ret_date_time = $timecard_common_class->get_schedule_end_date_time($date, $out_time, $ret_time);

					if ($out_time_nosubtract_flag != "1") {
						$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);

						$out_shoteinai = count(array_intersect($arr_effective_time, $arr_out_time));
						$out_shoteigai = count(array_intersect(array_diff($arr_effective_time, $arr_time2) ,$arr_out_time));
					} else {
						$arr_out_time = array();
						$out_shoteinai = 0;
						$out_shoteigai = 0;
					}
				//残業時間計算に残業時刻の時間帯を使用 20101027
				$arr_over_time = array();
				//残業開始、終了時刻がある場合
				if ($over_start_time != "" && $over_end_time != "") {
						//残業開始時刻 20111118
						$over_start_date = ($over_start_next_day_flag == "1") ? next_date($date) : $date;
						$over_start_date_time = $over_start_date.$over_start_time;
						//残業終了時刻 20100705
					$over_end_date = ($over_end_next_day_flag == "1") ? next_date($date) : $date;
					$over_end_date_time = $over_end_date.$over_end_time;
					$arr_over_time = $timecard_common_class->get_minute_time_array($over_start_date_time,$over_end_date_time);
				//残業時刻未設定は計算しない 20110825
				//} else {
				//	//残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
				//	if ($end2_date_time < $fixed_end_time) {
				//		$arr_over_time = $timecard_common_class->get_minute_time_array($end2_date_time,$fixed_end_time);
				//	}
				}
				//休日で残業時刻がなく、呼出がある場合の休日残業計算 20140218
				if ($pattern == "10" && $over_start_time == "" && $over_end_time == "" && $row["o_start_time1"] != "") {
					$arr_effective_time = array();
				}

				//退復
				//申請状態 "0":申請不要 "3":承認済 のみ確認、残業、修正申請は確認しない 20140121
				if ($return_link_type == "0" || $return_link_type == "3") {
						for ($i = 1; $i <= 10; $i++) {
							$start_var = "o_start_time$i";
							if ($row[$start_var] == "") {
								break;
							}

							$end_var = "o_end_time$i";
							if ($row[$end_var] == "") {
								break;
							}

							//端数処理
							if ($fraction1 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
								//開始日時
								$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $row[$start_var]);
								//基準日時
								$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
								$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
								$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
							} else {
								$tmp_ret_start_time = str_replace(":", "", $row[$start_var]);
							}
							if ($fraction2 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
								//終了日時
								$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $row[$end_var]);
								//基準日時
								$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
								$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

								$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
							} else {
								$tmp_ret_end_time = str_replace(":", "", $row[$end_var]);
							}
							$arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);

							$arr_effective_time = array_merge($arr_effective_time, $arr_ret);
							$arr_over_time = array_merge($arr_over_time, $arr_ret);
						}
				}
					//休憩除外
					//$arr_over_time = array_diff($arr_over_time, $arr_time4); //所定内の残業対応 20121022

				//深夜の配列
				//$arr_late_night = array_intersect($arr_effective_time, $arr_time5);
					$arr_late_night = array_intersect($arr_over_time, $arr_time5);
					//深夜かつ残業時間内（所定時間帯を除外）
				$arr_late_night = array_diff($arr_late_night, $arr_time2);
				//深夜以外の配列
				//if ($wage != "4") {
					$arr_except_late_night = array_diff($arr_effective_time, $arr_time5);
					$arr_except_late_night2 = array_diff($arr_over_time, $arr_time5);
					//} else {
					//時間給の場合は、休憩の引く前の実働時間
				//	$arr_except_late_night = array_diff($arr_effective_time_wk, $arr_time5);
				//}

				//休日勤務情報
				$last_hol_work_time = 0;
				$today_hol_work_time = 0;
				//前日フラグ
				if ($previous_day_flag == "1") {
					//前日の配列
					$last_date = last_date($date);
					$arr_lastday = $timecard_common_class->get_minute_time_array($last_date."0000", $date."0000");
					$arr_lastday_effective = array_intersect($arr_except_late_night, $arr_lastday);
						$arr_lastday_effective2 = array_intersect($arr_except_late_night2, $arr_lastday);
						//前日の残業
					$lastday_over = count(array_diff($arr_lastday_effective2, $arr_time2));
					//前日の深夜
					$last_late_night = count(array_intersect($arr_late_night, $arr_lastday));

					//前日が休日の場合休日に合計(日曜か祝日)
					//if ($timecard_common_class->get_you_kinmu_nissu($arr_type["$last_date"]) == 0) {
					//if ($timecard_common_class->check_sunday_or_holiday($last_date)) {
						//	//休日出勤と判定する日設定取得 20091222
						//$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $emp_id);
					//休日出勤の判定変更 20100730
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$last_date], $pattern, $reason, "1") == true) { //勤務パターンと事由変更 20120518

						//休日残業
						$hol_over += $lastday_over;
						//休日出勤
						//残業管理をしない場合
						if ($staff_info[$wk_emp_id]["no_overtime"] == "t") {
							$last_hol_work_time = count($arr_lastday_effective);
						} else {
							$last_hol_work_time = count(array_intersect($arr_lastday_effective, $arr_time2));
						}
						if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターン以外 20100722
							$hol_work += $last_hol_work_time;
						}
						//休日深夜
						$hol_late_night += $last_late_night;
					} else {
						//普通残業
						$normal_over += $lastday_over;
						//深夜
						$late_night += $last_late_night;
					}
				}
				$next_date = next_date($date);
				//当日の配列
				$arr_today = $timecard_common_class->get_minute_time_array($date."0000", $next_date."0000");
				$arr_today_effective = array_intersect($arr_except_late_night, $arr_today);
					$arr_today_effective2 = array_intersect($arr_except_late_night2, $arr_today);

				//当日の残業
					if ($over_start_time != "" && $over_end_time != "") {
						$today_over = count($arr_today_effective2); //所定を除かない
					} else {
						$today_over = count(array_diff($arr_today_effective2, $arr_time2)); //所定除外
					}
				//外出を残業から減算
				if ($others1 == "4" && $today_over > $out_shoteinai) {
					$today_over -= $out_shoteinai;
				}
				if ($others2 == "4" && $today_over > $out_shoteigai) {
					$today_over -= $out_shoteigai;
				}

				//当日の深夜
				$today_late_night = count(array_intersect($arr_late_night, $arr_today));
				//日祝フラグ
				//$today_holiday_flg = $timecard_common_class->check_sunday_or_holiday($date);
				//休日出勤の判定変更 20100730
                    $today_holiday_flg = check_timecard_holwk(
                            $arr_timecard_holwk_day, $arr_type[$date], $pattern, $reason, "1"); //勤務パターンと事由変更 20120518
				//勤務パターンが休日（公休）か事由なし休暇で勤務時間がある場合、残業に集計 20100722
				$today_effective_time = count(array_intersect($arr_effective_time, $arr_today));
				//休暇で残業時刻がある場合 20100917 呼出がある場合を追加 20140218
				if ($pattern == "10" && (($over_start_time != "" && $over_end_time != "") || $row["o_start_time1"] != "")) {
						if ($overtime_approve_flg != "1" ||
							($row["o_start_time1"] != "" && ($return_link_type == "0" || $return_link_type == "3"))) { //申請中以外の場合 呼出20140218
							//深夜以外（普通）
							$wk_today_normal_over = $today_effective_time - $today_late_night;

                            //日祝に関わらず集計 20120517
							//休日残業
							$hol_over += $wk_today_normal_over;
							//休日深夜
							$hol_late_night += $today_late_night;

						}

				} else {
					//当日が休日の場合、休日に合計(日曜か祝日)
					if (($pattern != "10" && $today_holiday_flg == true) ||
								$reason == "16") {
						//休日残業
						$hol_over += $today_over;
						//休日出勤
						//残業管理をしない場合
						if ($staff_info[$wk_emp_id]["no_overtime"] == "t") {
							$today_hol_work_time = count($arr_today_effective);
						} else {
							$today_hol_work_time = count(array_intersect($arr_today_effective, $arr_time2));
						}
						if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターン以外 20100722
							$hol_work += $today_hol_work_time;
						}
						//休日深夜
						$hol_late_night += $today_late_night;
					} else {
						//普通残業
						$normal_over += $today_over;
						//深夜
						$late_night += $today_late_night;
					}
				}
					//翌日またがりの確認 20110128
					if (count($arr_late_night) > 0) {
						//$wk_late_night_end_date = substr($arr_late_night[count($arr_late_night)-1], 0, 8);
                        $wk_late_night_end_date = substr(max($arr_late_night), 0, 8); //20130308
                    } else {
						$wk_late_night_end_date = "";
					}
					$next_date = next_date($date);
					//翌日フラグ
				if ($next_day_flag == "1" || $wk_late_night_end_date == $next_date) {
					//翌日の配列
					$arr_nextday = $timecard_common_class->get_minute_time_array($next_date."0000", $next_date."2359");
					$arr_nextday_effective = array_intersect($arr_except_late_night, $arr_nextday);
						$arr_nextday_effective2 = array_intersect($arr_except_late_night2, $arr_nextday);
						//翌日の残業
					if ($over_start_time != "" && $over_end_time != "") {
							$nextday_over = count($arr_nextday_effective2); //所定を除かない
					} else {
							$nextday_over = count(array_diff($arr_nextday_effective2, $arr_time2)); //所定除外
					}
					//翌日の深夜
					$next_late_night = count(array_intersect($arr_late_night, $arr_nextday));
					//翌日が休日の場合休日に合計(日曜か祝日)
					//if ($timecard_common_class->get_you_kinmu_nissu($arr_type["$next_date"]) == 0) {
//					if ($timecard_common_class->check_sunday_or_holiday($next_date)) {
					//休日出勤の判定変更 20100730
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1") == true) { //勤務パターンと事由変更 20130308
						//休日残業
						$hol_over += $nextday_over;
						//休日出勤
						if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターンにないこと 20100722
							//残業管理をしない場合
							if ($staff_info[$wk_emp_id]["no_overtime"] == "t") {
								$hol_work += count($arr_nextday_effective);
							} else {
								$hol_work += count(array_intersect($arr_nextday_effective, $arr_time2));
							}
						}
						//休日深夜
						$hol_late_night += $next_late_night;
					} else {
						//普通残業
						$normal_over += $nextday_over;
						//深夜
						$late_night += $next_late_night;
					}
				}
				//勤務時間
				//if ($wage != "4") {
					$effective_time = count($arr_effective_time);
				//} else {
				//	$effective_time = count($arr_effective_time_wk);
				//}
				//外出を減算
				if ($effective_time > $out_shoteinai) {
					$effective_time -= $out_shoteinai;
				}
				if ($effective_time > $out_shoteigai) {
					$effective_time -= $out_shoteigai;
				}
				//時間給者の休憩
				//if ($staff_info[$wk_emp_id]["wage"] == "4" && $others3 == "1") {
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					$wk_hol_rest_time = 0;
					if ($effective_time > 1200) {  // 20時間を超える場合 20100929
						$effective_time -= $rest7;
						$wk_hol_rest_time = $rest7;
					} else if ($effective_time > 720) {  // 12時間を超える場合
						$effective_time -= $rest6;
						$wk_hol_rest_time = $rest6;
					} else if ($effective_time > 540) {  // 9時間を超える場合
						$effective_time -= $rest3;
						$wk_hol_rest_time = $rest3;
					} else if ($effective_time > 480) {  // 8時間を超える場合
						$effective_time -= $rest2;
						$wk_hol_rest_time = $rest2;
					} else if ($effective_time > 360) {  // 6時間を超える場合
						$effective_time -= $rest1;
						$wk_hol_rest_time = $rest1;
					} else if ($effective_time > 240 && $start_time < "1200") {  // 4時間超え午前開始の場合
						$effective_time -= $rest4;
						$wk_hol_rest_time = $rest4;
					} else if ($effective_time > 240 && $start_time >= "1200") {  // 4時間超え午後開始の場合
						$effective_time -= $rest5;
						$wk_hol_rest_time = $rest5;
					}
					if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターン以外 20100722
						//開始した日が休日の場合に休日勤務から休憩を減算
						if ($last_hol_work_time > 0 || $today_hol_work_time > 0) {
							$hol_work -= $wk_hol_rest_time;
						}
					}
					//休日残業時の休憩除外 20101027
					if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
						if ($overtime_approve_flg != "1") { //申請中以外の場合
							if ($today_holiday_flg == false) {
								$normal_over -= $wk_hol_rest_time;

							} else {
								$hol_over -= $wk_hol_rest_time;
							}
						}
					}

				}

				$this_month_time1 += $effective_time ;
			}
			//パターン回数
			if ($row["pattern"] != "") {
				$wk_idx = $row["pattern"];
				$arr_ptn_full_cnt[$wk_idx]++;
				//交通費
				$tra_cnt += $tra_full[$wk_idx]["cnt"];

			}
			//早出残業
			if ($early_over_time != "") {
				//申請中の場合は計算しない	// 残業未承認か確認
				if (!($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3"))) {

					$early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
					$wk_early_over_time = hmm_to_minute($early_over_time);
					//合計する
					$this_month_time1 += $wk_early_over_time ;
					if ($today_holiday_flg == true) {
						//休日残業
						$hol_over += $wk_early_over_time;
					} else {
						//普通残業
						$normal_over += $wk_early_over_time;
					}

				}
			}
			//残業２
				if ($over_start_time2 != "" && $over_end_time2 != "") {
					//申請中の場合は計算しない	// 残業未承認か確認
					if (!($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3"))) {
						//開始日時
						$over_start_date_time2 = ($over_start_next_day_flag2 == 1) ? next_date($date).$over_start_time2 : $date.$over_start_time2;
						//終了日時
						$over_end_date_time2 = ($over_end_next_day_flag2 == 1) ? next_date($date).$over_end_time2 : $date.$over_end_time2;

						$work_times_info = $timecard_common_class->get_work_times_info($date, $start4, $end4, $start5, $end5);
						//残業時間データ
						$arr_overtime_data2 = get_overtime_data($work_times_info, $date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time2, $over_end_date_time2);

						//普通残業
						$normal_over += $arr_overtime_data2["normal_over"];
						//深夜
						$late_night += $arr_overtime_data2["late_night"];
						//休日残業
						$hol_over += $arr_overtime_data2["hol_over"];
						//休日深夜
						$hol_late_night += $arr_overtime_data2["hol_late_night"];
					}
				}
			}
		}
		//普通残業時間に深夜、休日残業を含む
		if ($normal_overtime_flg == "2") {
			$normal_over += $late_night;
			$normal_over += $hol_over;
			$normal_over += $hol_late_night;
			$late_night = "";
			$hol_over = "";
			$hol_late_night = "";
		}
		//残業管理をしない場合
		if ($staff_info[$wk_emp_id]["no_overtime"] == "t") {
			$normal_over = "";
			$late_night = "";
			$hol_over = "";
			$hol_late_night = "";
		}
		echo("<tr>");
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($staff_info[$wk_emp_id]["emp_personal_id"]);
		echo("</font></td>");
		// 氏名
		$wk = $staff_info[$wk_emp_id]["staff_name"];
		$color = ( $staff_info[$wk_emp_id]["sex"] == 1 ) ? 'blue' : 'black';
		echo("<td id=\"$wk_td_id\" width=\"100\" align=\"center\" bgcolor=\"$wk_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$color\">$wk</font></td>\n");

		//出勤日数
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$work_day = ($work_day == 0) ? "" : $work_day;
		echo($work_day);
		echo("</font></td>");

		//出勤回数 20130124
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$work_count = ($work_count == 0) ? "" : $work_count;
		echo($work_count);
		echo("</font></td>");


		//休 公/欠/有
		for ($j=0; $j<3; $j++) {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo($sum_hol[$j]);
			echo("</font></td>");
		}
		//取得公休数
		if ($adjust_time_legal_hol_flg == "2") {
			echo("<input type=\"hidden\" name=\"spend_hol_{$wk_pos}\" value=\"{$sum_hol[0]}\">\n");
		}

		//遅刻・早退
		//回数
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$delay_cnt = ($delay_cnt == 0) ? "" : $delay_cnt;
		echo($delay_cnt);
		echo("</font></td>");
		//時間
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(minute_to_hmm($delay_time));
		echo("</font></td>");
		for ($j=0; $j<count($ptn_full); $j++) {

			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$wk_idx = $ptn_full[$j];
			echo($arr_ptn_full_cnt[$wk_idx]);
			echo("</font></td>");
		}
		//休日出勤
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$hol_work = round_time($hol_work, $fraction4, $fraction4_min);
		echo(minute_to_hmm($hol_work));
		echo("</font></td>");
		//普通
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$normal_over = round_time($normal_over, $fraction4, $fraction4_min);
		echo(minute_to_hmm($normal_over));
		echo("</font></td>");
		//深夜
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$late_night = round_time($late_night, $fraction4, $fraction4_min);
		echo(minute_to_hmm($late_night));
		echo("</font></td>");
		//休日
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$hol_over = round_time($hol_over, $fraction4, $fraction4_min);
		echo(minute_to_hmm($hol_over));
		echo("</font></td>");
		//休日深夜
		if ($hol_late_night_disp_flg != "f") {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$hol_late_night = round_time($hol_late_night, $fraction4, $fraction4_min);
			echo(minute_to_hmm($hol_late_night));
			echo("</font></td>");
		}
		//前繰越
		if ($arr_adjust["$wk_emp_id"]["adjust_time"] != "") {
			$last_adjust = $arr_adjust["$wk_emp_id"]["adjust_time"];
		} else {
			//前月データの次月繰越
			$last_adjust = $arr_adjust["$wk_emp_id"]["adjust_time_last"];
		}
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<input type=\"text\" name=\"adj_{$wk_pos}\" size=\"8\" maxlength=\"7\" value=\"{$last_adjust}\" onChange=\"changeTime({$wk_pos});\" style=\"text-align:right\">");
		echo("</font></td>");
		//今月分
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($adjust_time_legal_hol_flg != "2") {
			$this_month_time1 = round_time($this_month_time1, $fraction4, $fraction4_min);
			$this_month_time1 = $timecard_common_class->minute_to_h_hh($this_month_time1);
		} else {
			$this_month_time1 = $legal_hol_cnt;
		}
		echo($this_month_time1);

		echo("<input type=\"hidden\" name=\"montime_{$wk_pos}\" value=\"$this_month_time1\">\n");
		//前繰越
		if ($last_adujst == "") {
			$last_adujst = 0;
		}
		if ($adjust_time_legal_hol_flg != "2") {
			$this_month_time2 = $last_adjust + $this_month_time1 - $irrg_month_time ;
		} else {
			//次月繰越＝前繰越＋今月分ー今月の取得公休日数
			$this_month_time2 = $last_adjust + $legal_hol_cnt - $sum_hol[0];
		}

		$next_month_time = $this_month_time2;

		echo("</font></td>");
		//次月繰越
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<input type=\"text\" name=\"adjnext_{$wk_pos}\" size=\"8\" maxlength=\"7\" readOnly style=\"border:none\" style=\"text-align:right\" value=\"{$next_month_time}\">");
		echo("</font></td>");
		//手当
		foreach($arr_allowance as $allowance)
		{
			$tmp_allow_id = $allowance["allow_id"];
			$tmp_allow_contents = $allowance["allow_contents"];
			$tmp_del_flg = $allowance["del_flg"];
			if ($allow_full[$tmp_allow_id] == "t") {
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo($arr_allow_count[$tmp_allow_id]);
				echo("</font></td>");

			}
		}
		//交通費
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$tra_cnt = ($tra_cnt == 0) ? "" : $tra_cnt;
		echo($tra_cnt);
		echo("</font></td>");
		//備考
		if ($arr_adjust["$wk_emp_id"]["memo"] != "") {
			$memo = $arr_adjust["$wk_emp_id"]["memo"];
		} else {
			// 前月に入力がある場合コピー 20110720
			$memo = $arr_adjust["$wk_emp_id"]["memo_last"];
		}
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<input type=\"text\" name=\"memo1_{$wk_pos}\" size=\"20\" value=\"{$memo}\">");
		echo("</font></td>");
		echo("</tr>\n");
		/*
		$wk_data = array();
		$wk_data[] = "=\"".$staff_info[$wk_emp_id]["emp_personal_id"]."\"";
		$wk_data[] = $staff_info[$wk_emp_id]["staff_name"];
		$wk_data[] = $work_day;
		for ($j=0; $j<3; $j++) {
			$wk_data[] = $sum_hol[$j];
		}
		$wk_data[] = minute_to_hmm($delay_time);

		$exl_dat1[] = "";
		*/
	}

			?>


			</table>

<br>
<? if ($part_cnt > 0) { ?>
		<table>
		<tr>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非常勤（<? echo($group_name) ?>）</font>
		</td>
		</tr>
		</table>

			<table id="header" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
			<tr height="44">
			<td width="80" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
			<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パート氏名</font></td>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤<br>日数</font></td>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤<br>回数</font></td>
			<td width="160" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間
		<?
		//分割集計がある場合は時刻表示
		if ($parttime_split_hour != "") {
			echo("（{$parttime_split_hour}:00まで）");
		}
 ?>
			</font></td>
		<?
		//分割集計がある場合
		if ($parttime_split_hour != "") {
?>
			<td width="160" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間（<?echo($parttime_split_hour);?>:00以降）</font></td>

			<?
		}
		for ($i=0; $i<count($ptn_part); $i++) {
			?>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<?
			for ($j=0; $j<count($atdptn_array); $j++) {
				if ($atdptn_array[$j]["id"] == $ptn_part[$i]) {
					echo($atdptn_array[$j]["name"]);
					break;
				}
			}
			?>
			</font></td>
			<?
		}
			?>
			<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休日勤務</font></td>
		<?
		//手当
		foreach($arr_allowance as $allowance)
		{
			$tmp_allow_id = $allowance["allow_id"];
			$tmp_allow_contents = $allowance["allow_contents"];
			$tmp_del_flg = $allowance["del_flg"];
			if ($allow_part[$tmp_allow_id] == "t") {
			?>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_allow_contents);?></font></td>
				<?
			}
		}
			?>
			<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交通費</font></td>
			<td width="120" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>

</tr>
		<?
        //翌月初日までデータを取得 20130308
        $wk_end_date_parttime_nextone = next_date($wk_end_date_parttime);
        // 処理日付の勤務日種別を取得
		$arr_type = array();
		$sql = "select date, type from calendar";
        $cond = "where date >= '$wk_start_date_parttime' and date <= '$wk_end_date_parttime_nextone'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$date = $row["date"];
			$arr_type[$date] = $row["type"];
		}

		$emp_ids2 = "";
		$wk_pos = 0;
		for ($emp_idx=0; $emp_idx<$data_cnt; $emp_idx++) {

			$wk_emp_id = $staff_id[$emp_idx];
			//非常勤のみ
			if ($staff_info[$wk_emp_id]["duty_form"] != "2") {
				continue;
			}
			$wk_pos++;
			if ($emp_ids2 != "") {
				$emp_ids2 .= ",";
			}
			$emp_ids2 .= $wk_emp_id;
			$wage = $staff_info[$wk_emp_id]["wage"];

			//一括修正申請情報
			$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($wk_emp_id, $wk_start_date, $wk_end_date);

            //個人別所定時間を優先するための対応 20120309
            $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($wk_emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

            // 実績データをまとめて取得
			$arr_atdbkrslt = array();
			$sql =	"SELECT ".
				"atdbkrslt.*, ".
				"atdptn.workday_count, ".
				"atdptn.base_time, ".
				"atdptn.over_24hour_flag, ".
				"atdptn.out_time_nosubtract_flag, ".
				"atdptn.after_night_duty_flag, ".
                "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
                "atdptn.no_count_flag, ".
                "tmmdapply.apply_id     AS tmmd_apply_id, ".
				"tmmdapply.apply_status AS tmmd_apply_status, ".
				"ovtmapply.apply_id     AS ovtm_apply_id, ".
				"ovtmapply.apply_status AS ovtm_apply_status, ".
				"rtnapply.apply_id      AS rtn_apply_id, ".
				"rtnapply.apply_status  AS rtn_apply_status ".
				"FROM ".
				"atdbkrslt ".
				"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
				"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
				"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL AND ovtmapply.apply_status != '4' ".
				"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ";
			$cond = "where atdbkrslt.emp_id = '$wk_emp_id' and ";
            $cond .= " atdbkrslt.date >= '$wk_start_date_parttime' and atdbkrslt.date <= '$wk_end_date_parttime_nextone' order by atdbkrslt.date";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//出勤日数
			$work_day = 0;
			$work_count = 0;
			$sum_hol = array();
			$arr_ptn_part_cnt = array();
			$delay_cnt = 0;
			$arr_allow_count = array();
			$work_time1 = 0;
			$work_time2 = 0;
			$tra_cnt = 0;
			$hol_work = 0;
			while ($row = pg_fetch_array($sel)) {
				$date = $row["date"];
                $arr_atdbkrslt[$date] = $row;
                /*
				$arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
				$arr_atdbkrslt[$date]["reason"] = $row["reason"];
				$arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
				$arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
				$arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
				$arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
				$arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
				$arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
				for ($j = 1; $j <= 10; $j++) {
					$start_var = "o_start_time$j";
					$end_var = "o_end_time$j";
					$arr_atdbkrslt[$date][$start_var] = $row[$start_var];
					$arr_atdbkrslt[$date][$end_var] = $row[$end_var];
				}
				$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
				$arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
				$arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
				$arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
				$arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
				$arr_atdbkrslt[$date]["meeting_start_time"] = $row["meeting_start_time"];
				$arr_atdbkrslt[$date]["meeting_end_time"] = $row["meeting_end_time"];
				$arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
				$arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
				$arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
				$arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
				$arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
				$arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
				$arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];

                $arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
                $arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
                $arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
                $arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
                $arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
                $arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];

                $arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
                $arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
                $arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
                $arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
                */
            }
            foreach ($arr_atdbkrslt as $date => $row) {
                //翌月初日は処理しない 20130308
                if ($date >= $wk_end_date_parttime_nextone) {
                    break;
                }
                $tmcd_group_id = $row["tmcd_group_id"];
				$pattern = $row["pattern"];
				$reason = $row["reason"];
				// 処理日付の勤務日種別を取得
				$type = $arr_type[$date];
				//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
				$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
				$start_time =  $row["start_time"];
				$end_time =  $row["end_time"];
				$previous_day_flag = $row["previous_day_flag"];
				$next_day_flag = $row["next_day_flag"];

				$out_time = $row["out_time"];
				$ret_time = $row["ret_time"];

				$over_24hour_flag =  $row["over_24hour_flag"];
				$night_duty =  $row["night_duty"];
				$ovtm_apply_status =  $row["ovtm_apply_status"];
				$rtn_apply_status =  $row["rtn_apply_status"];
				$tmmd_apply_status =  $row["tmmd_apply_status"];
				$over_start_time =  $row["over_start_time"];
				$over_end_time =  $row["over_end_time"];
				//20111122
				$over_start_next_day_flag =  $row["over_start_next_day_flag"];
				$over_end_next_day_flag =  $row["over_end_next_day_flag"];
				//残業２ 20111118
				$over_start_time2 =  $row["over_start_time2"];
				$over_end_time2 =  $row["over_end_time2"];
				$over_start_next_day_flag2 =  $row["over_start_next_day_flag2"];
				$over_end_next_day_flag2 =  $row["over_end_next_day_flag2"];

				$early_over_time =  $row["early_over_time"];
				//休憩時刻追加 20100921
				$rest_start_time = $row["rest_start_time"];
				$rest_end_time = $row["rest_end_time"];
				//外出時間を差し引かないフラグ 20110727
				$out_time_nosubtract_flag = $row["out_time_nosubtract_flag"];
				//明け追加 20110819
				$after_night_duty_flag = $row["after_night_duty_flag"];
                //時間帯画面の前日フラグ 20121120
                $atdptn_previous_day_flag = $row["atdptn_previous_day_flag"];
                //勤務時間を計算しないフラグ 20130225
                $no_count_flag = $row["no_count_flag"];
                //外出復帰日時の取得
				if ($out_time_nosubtract_flag != "1") {
					$out_date_time = $date.$out_time;
					$ret_date_time = $timecard_common_class->get_schedule_end_date_time($date, $out_time, $ret_time);

					$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
				} else {
					$arr_out_time = array();
				}


				//手当
				if ($row["allow_id"] != "") {
					$wk_idx = $row["allow_id"];
					$arr_allow_count[$wk_idx] += $row["allow_count"];
				}
				// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
				if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {
					//休暇以外の場合に計算 20110817
					if ($row["pattern"] != "10") {
						$work_day += $row["workday_count"];
						//出勤回数をカウント(ゼロ以外の場合はすべて１として集計) 20130124
						if ($row["workday_count"] != "0" && $row["workday_count"] != "")
						{
							$work_count += 1;
						}
					}
				}
                //勤務時間を計算しないフラグが"1"以外 20130225
                if (($start_time != "" && $end_time != "" && $no_count_flag != "1") ||
						($over_start_time != "" && $over_end_time != "") ||
						($over_start_time2 != "" && $over_end_time2 != "") //20111122
					) { //残業時間だけ入っている場合を追加 20100906
					//休日で残業時刻がない場合は計算しない 20101005
					if ($pattern == "10" && $over_start_time == "" && $over_end_time == "" && $row["o_start_time1"] == "") { //20140218呼出も確認
						continue;
					}
					if ($start_time != "" && $end_time == "") {
						continue;
					}
					//位置変更 20140219
					if (str_replace(":", "", $end_time) < str_replace(":", "", $start_time) && $previous_day_flag != "1") {
						$next_day_flag = "1";
					}
					if ($pattern != "") {
						$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
						$end2   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
						$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
						$end4   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
                        //個人別所定時間を優先する場合 20120309
                        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
                            $wk_weekday = $arr_weekday_flg[$date];
                            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $date); //個人別勤務時間帯履歴対応 20130220
                            if ($arr_empcond_officehours["officehours2_start"] != "") {
                                $start2 = $arr_empcond_officehours["officehours2_start"];
                                $end2 = $arr_empcond_officehours["officehours2_end"];
                                $start4 = $arr_empcond_officehours["officehours4_start"];
                                $end4 = $arr_empcond_officehours["officehours4_end"];
                            }
                        }

                        //所定開始より残業１が早い場合、残業１と残業２を入れ替える 20111124
						$wk_compare_start2 = str_replace(":", "", $start2);
						if ($over_end_time != "" && $over_start_time <= $wk_compare_start2 && $over_end_time <= $wk_compare_start2) { //20140219 残業終了時刻0000対応
							$wk_chg_start = $over_start_time;
							$wk_chg_end = $over_end_time;
							$wk_chg_start_flag = $over_start_next_day_flag;
							$wk_chg_end_flag = $over_end_next_day_flag;
							$over_start_time = $over_start_time2;
							$over_end_time = $over_end_time2;
							$over_start_next_day_flag = $over_start_next_day_flag2;
							$over_end_next_day_flag = $over_end_next_day_flag2;
							$over_start_time2 = $wk_chg_start;
							$over_end_time2 = $wk_chg_end;
							$over_start_next_day_flag2 = $wk_chg_start_flag;
							$over_end_next_day_flag2 = $wk_chg_end_flag;
						}
					}

                    // 所定開始時刻が前日の場合 20121120
                    if ($atdptn_previous_day_flag == "1") {
                        $tmp_prev_date = last_date($date);
						$start2_date_time = $tmp_prev_date.$start2;
						//24時間以上勤務の場合 20100203
						if ($over_24hour_flag == "1") {
							$tmp_prev_date = $date;
						}
						$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
					} else {
						$start2_date_time = $date.$start2;
						//24時間以上勤務の場合 20100203
						if ($over_24hour_flag == "1") {
							$tmp_end_date = next_date($date);
						} else {
							$tmp_end_date = $date;
						}
						$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
					}
                    //所定時間帯の範囲内の残業対応 20121022
                    $over_start_time_org = "";
                    $over_end_time_org = "";
                    if ($start2 != "" && //所定時間帯がある場合
                            $over_start_time != "" && //残業時刻１設定済み
                            $over_end_time != "" &&
                            $start_time != $over_start_time) { //勤務開始と残業開始が同じ場合を除く
                        $chk_over_end_date_time = $timecard_common_class->get_timecard_end_date_time($date, $over_end_time, $over_end_next_day_flag);
                        $wk_start2_date_time = str_replace(":", "", $start2_date_time);
                        $wk_end2_date_time = str_replace(":", "", $end2_date_time);
                        if ($wk_start2_date_time < $chk_over_end_date_time && //残業時刻終了が所定範囲内
                                $chk_over_end_date_time < $wk_end2_date_time) {
                            $over_start_time_org = $over_start_time;
                            $over_end_time_org = $over_end_time;
                            $over_start_time = "";
                            $over_end_time = "";
                        }
                    }

                    //休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
					if ($rest_start_time != "" && $rest_end_time != "") {
						$start4 = $rest_start_time;
						$end4 = $rest_end_time;
					}
					$start4_date_time = $date.$start4;
					$end4_date_time = $timecard_common_class->get_schedule_end_date_time($date, $start4, $end4);

					// 時間帯データを配列に格納
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
					$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
					//　公休残業対応 20100906
					//if (($start_time == "" && $end_time == "") && ($over_start_time != "" && $over_end_time != "")) { //残業時間を計算にしようする 20100906
					$legal_hol_over_flg = false; //休日残業フラグ 20140219
                    //　公休残業対応 20120507
                    if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20111118
                            $over_start_time != "" && $over_end_time != "") {
                        $start_time = $over_start_time;
						$end_time = $over_end_time;
						$legal_hol_over_flg = true; //休日残業フラグ 20140219
					}

					//退勤時刻から残業開始時刻の間を除外する 20120119
					$wk_jogai_flg = false;
					$wk_jogai1 = 0;
					$wk_jogai2 = 0;
					//残業開始時刻が退勤時刻から離れているか確認
                    if ($over_start_time != "" && $ovtm_apply_status == "1" && $end_time != "" && $end2_date_time != "") { //所定終了時刻が設定済みか確認 20120517
						$chk_end_date_time = $timecard_common_class->get_timecard_end_date_time($date, $end_time, $next_day_flag);
						if ($chk_end_date_time > $end2_date_time) {
							$chk_end_date_time = $end2_date_time;
						}
						else {
							//所定より早い退勤の場合、端数処理
							if ($fraction2 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
								$tmp_office_start_time = substr($chk_end_date_time, 0, 10) . "00";
								$chk_end_date_time = date_utils::move_time($tmp_office_start_time, $chk_end_date_time, $moving_minutes, $fraction2);

							}
						}
						$chk_over_start_date_time = $timecard_common_class->get_timecard_end_date_time($date, $over_start_time, $over_start_next_day_flag);
						if ($chk_over_start_date_time > $chk_end_date_time) {
							$wk_jogai_flg = true;
							//分割時刻なし
							if ($parttime_split_hour == "") {
								$wk_jogai1 = date_utils::get_diff_minute($chk_over_start_date_time, $chk_end_date_time);
							}
							//分割時刻あり
							else {
								//18:00まで
								$wk_parttime_split_hhmm = $date.$parttime_split_hour."00";
								$wk_jogai1 = $timecard_common_class->get_intersect_times($start2_date_time, $wk_parttime_split_hhmm, $chk_end_date_time, $chk_over_start_date_time);
								//18:00以降
								$wk_jogai2 = $timecard_common_class->get_intersect_times($wk_parttime_split_hhmm, next_date($date)."2359", $chk_end_date_time, $chk_over_start_date_time);

							}
						}

					}

					//残業申請状態が承認済みの場合 20110822
					if ($ovtm_apply_status == "1") {
						//　退勤時刻に残業終了を設定
                        if ($over_end_time != "" &&
                                $over_end_time > $end_time //退勤時刻より残業終了が後の場合 20121019
                        ) {
							$end_time = $over_end_time;
							$next_day_flag = $over_end_next_day_flag; //20111122
						}
					}
					//承認以外
					else {
						//所定終了がある場合
						if ($end2 != "") {
							//退勤時刻があとの場合 20111118
							$wk_end2 = str_replace(":", "", $end2);
							if ($end_time > $wk_end2) {
								//退勤時刻に所定を設定
								$end_time = $end2;
							}
						}
					}

					$start_date_time = $timecard_common_class->get_timecard_start_date_time($date, $start_time, $previous_day_flag);
					$end_date_time   = $timecard_common_class->get_timecard_end_date_time($date, $end_time, $next_day_flag);

					$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);
					//残業開始時刻があり、出勤時刻と同じ場合 20111122
					if ($over_start_time != "" && $start_time == $over_start_time) {
                        //残業開始翌日フラグを確認 20120517
                        if ($over_start_next_day_flag == "1") {
                            $fixed_start_time = next_date(substr($start_date_time, 0, 8)) .$over_start_time;
                        }
                        else {
                            $fixed_start_time = substr($start_date_time, 0, 8) .$over_start_time;
                        }
					}
					else {
						// 勤務パターンで時刻指定がない場合でも、端数処理する
						if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							//基準日時
							$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
						} else {
							$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
							$fixed_start_time = $start_time_info["fixed_time"];
						}
					}
					// 勤務パターンで時刻指定がない場合でも、端数処理する
					if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						//基準日時
						//残業時刻が入っている場合は、端数処理しない 20111118
						if ($over_end_time != "") {
							$fixed_end_time = substr($end_date_time, 0, 8).$over_end_time;
						} else {
							$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
						}
	//echo("fixed_end_time=$fixed_end_time");
					} else {
					//早退
					//残業時刻が入っている場合は、端数処理しない 20100806
						if ($over_end_time != "") {
							$fixed_end_time = substr($end_date_time, 0, 8).$over_end_time;
						} else {
							$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
							$fixed_end_time = $end_time_info["fixed_time"];
						}
					}
					$start_time = substr($fixed_start_time, 8, 4);
					$end_time = substr($fixed_end_time, 8, 4);
					//開始時刻が0000の場合で前日フラグがある場合、調整
                    if ($start_time == "0000" && $previous_day_flag == "1") {
                        $previous_day_flag = "0";
                    }

					$parttime_split_hhmm = $parttime_split_hour."00";

					//退勤が翌日になる場合の対応 20100805
					if ($over_end_time != "") { //20111122
						$wk_end_date_time = $timecard_common_class->get_timecard_end_date_time($date, $end_time, $over_end_next_day_flag);
					}
					else {
						$wk_end_date_time = $timecard_common_class->get_timecard_end_date_time($date, $end_time, $next_day_flag);
					}
					$wk_parttime_split_hhmm = $date.$parttime_split_hhmm;
					//分割設定なし、分割設定以前
					if ($parttime_split_hour == "" || $wk_end_date_time < $wk_parttime_split_hhmm) {
						//前日フラグ判定 20120116
						if ($previous_day_flag == "1") {
							$wk_start_date_time = last_date($date).$start_time;
						}
						else {
                            //残業開始翌日フラグを確認 20120517
                            if ($over_start_time != "" && $over_start_next_day_flag == "1") {
                                $wk_start_date_time = next_date($date).$start_time;
                            }
                            else {
                                $wk_start_date_time = $date.$start_time;
                            }
						}
						//$wk_end_date_time = $date.$end_time;
						$wk_minutes1 = date_utils::get_diff_minute($wk_end_date_time, $wk_start_date_time);

						//休憩の確認、時間給者かつ休憩除外
//						if ($wage == "4" && $start2 == "" && $end2 == "") {  // 時間給かつ勤務パターンで時刻指定がない場合 20100915
						//退勤時刻から残業開始時刻の間を除外する 20120119
						if ($wk_jogai_flg) {
							$wk_minutes1 -= $wk_jogai1;
						}
						$rest_time = 0; //時間給者の時間が正しくない不具合対応 20110818
						if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
						  //休日残業は休憩を計算しない 20140219
						  if (!$legal_hol_over_flg) {
							if ($wk_minutes1 > 1200) {  // 20時間を超える場合 20100929
								$rest_time = $rest7;
							} else if ($wk_minutes1 > 720) {  // 12時間を超える場合
								$rest_time = $rest6;
							} else if ($wk_minutes1 > 540) {  // 9時間を超える場合
								$rest_time = $rest3;
							} else if ($wk_minutes1 > 480) {  // 8時間を超える場合
								$rest_time = $rest2;
							} else if ($wk_minutes1 > 360) {  // 6時間を超える場合
								$rest_time = $rest1;
							} else if ($wk_minutes1 > 240 && $start_time < "1200") {  // 4時間超え午前開始の場合
								$rest_time = $rest4;
							} else if ($wk_minutes1 > 240 && $start_time >= "1200") {  // 4時間超え午後開始の場合
								$rest_time = $rest5;
							}
						  }
						} else {
							if ($start4 != "" && $end4 != "") {
								//$wk_start_date_time = $date.preg_replace("/(\d{2}):(\d{2})/", "$1$2", $start4);
								//$wk_end_date_time = $date.preg_replace("/(\d{2}):(\d{2})/", "$1$2", $end4);
								//$rest_time = date_utils::get_diff_minute($wk_end_date_time, $wk_start_date_time);
								//実働時間内の休憩時間確認 20100925
								$rest_time = count(array_intersect($arr_tmp_work_time, $arr_time4));
							} else {
								$rest_time = 0;
							}
						}
						$wk_minutes1 -= $rest_time;
						$wk_minutes1 -= count($arr_out_time); //外出
						$work_time1 += $wk_minutes1;
						$wk_minutes2 = 0;
					} else {
						//分割設定以降
                        //残業開始翌日フラグを確認 20120517
                        if ($over_start_time != "" && $over_start_next_day_flag == "1") {
                            $wk_start_date_time1 = next_date($date).$over_start_time;
                        }
                        else {
                            $wk_start_date_time1 = $date.$start_time;
                        }
						$wk_end_date_time1 = $date.$parttime_split_hhmm;
						$arr_work_time1 = $timecard_common_class->get_minute_time_array($wk_start_date_time1, $wk_end_date_time1);  // 分割時刻まで
						$arr_work_time1 = array_diff($arr_work_time1, $arr_out_time);

                        //残業開始翌日フラグを確認 20120517
                        if ($over_start_time != "" && $over_start_next_day_flag == "1") {
                            $wk_start_date_time2 = next_date($date).$over_start_time;
                        }
                        //残業開始が分割時刻以降、開始時刻と残業開始が同じ場合 //20111122
						else if ($over_start_time != "" && $over_start_time > $parttime_split_hhmm && $over_start_time == $start_time) {
							$wk_start_date_time2 = $date.$over_start_time;
						}
						else {
							$wk_start_date_time2 = $date.$parttime_split_hhmm;
						}
						//$wk_end_date_time2 = $date.$end_time;
						if ($over_end_time != "") { //20111122
							$wk_end_date_time2 = $timecard_common_class->get_timecard_end_date_time($date, $end_time, $over_end_next_day_flag);
						}
						else {
							$wk_end_date_time2 = $timecard_common_class->get_timecard_end_date_time($date, $end_time, $next_day_flag);
						}
						$arr_work_time2 = $timecard_common_class->get_minute_time_array($wk_start_date_time2, $wk_end_date_time2);  // 分割時刻以降
						$arr_work_time2 = array_diff($arr_work_time2, $arr_out_time);

						//休憩の確認、時間給者かつ休憩除外
						//if ($staff_info[$wk_emp_id]["wage"] == "4" && $others3 == "1") {
//						if ($wage == "4" && $start2 == "" && $end2 == "") {  // 時間給かつ勤務パターンで時刻指定がない場合 20100915
						if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ勤務パターンで時刻指定がない場合 20100925
							$wk_minutes1 = count($arr_work_time1);
							$wk_minutes2 = count($arr_work_time2);
							$wk_minutes = $wk_minutes1 + $wk_minutes2;
							//退勤時刻から残業開始時刻の間を除外する 20120119
							if ($wk_jogai_flg) {
								$wk_minutes -= $wk_jogai1;
								$wk_minutes -= $wk_jogai2;
							}
						  //休日残業は休憩を計算しない 20140219
						  if (!$legal_hol_over_flg) {
							if ($wk_minutes > 1200) {  // 20時間を超える場合 20100929
								$rest_time = $rest7;
								$wk_minutes1 -= $rest7;
							} else if ($wk_minutes > 720) {  // 12時間を超える場合
								$rest_time = $rest6;
								$wk_minutes1 -= $rest6;
							} else if ($wk_minutes > 540) {  // 9時間を超える場合
								$rest_time = $rest3;
								$wk_minutes1 -= $rest3;
							} else if ($wk_minutes > 480) {  // 8時間を超える場合
								$wk_minutes1 -= $rest2;
								$rest_time = $rest2;
							} else if ($wk_minutes > 360) {  // 6時間を超える場合
								$rest_time = $rest1;
								$wk_minutes1 -= $rest1;
							} else if ($wk_minutes > 240 && $start_time < "1200") {  // 4時間超え午前開始の場合
								$rest_time = $rest4;
								$wk_minutes1 -= $rest4;
							} else if ($wk_minutes > 240 && $start_time >= "1200") {  // 4時間超え午後開始の場合
								$rest_time = $rest5;
								$wk_minutes1 -= $rest5;
							}
						  }
						} elseif ($start4 != "" && $end4 != "") {//休憩時刻がある場合
							$start4_date_time = $date.$start4;
							$end4_date_time = $timecard_common_class->get_schedule_end_date_time($date, $start4, $end4);
							$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩

							$wk_minutes1 = count(array_diff($arr_work_time1, $arr_time4));
							$wk_minutes2 = count(array_diff($arr_work_time2, $arr_time4));
						} else {
							$wk_minutes1 = count($arr_work_time1);
							$wk_minutes2 = count($arr_work_time2);
						}
						//退勤時刻から残業開始時刻の間を除外する 20120119
						if ($wk_jogai_flg) {
							$wk_minutes1 -= $wk_jogai1;
							$wk_minutes2 -= $wk_jogai2;
						}
						$work_time1 += $wk_minutes1;
						$work_time2 += $wk_minutes2;
					}

					//残業承認状態確認 20100906
					//残業
					$overtime_link_type = $timecard_common_class->get_link_type_overtime($date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag);
					//呼出・退復の時間計算 20111219
					//退復 rtn_apply_statusからlink_type取得
					$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $row["o_start_time1"], $rtn_apply_status);

					//申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない
					if ($return_link_type == "0" || $return_link_type == "3") {
						for ($i = 1; $i <= 10; $i++) {
							$start_var = "o_start_time$i";
							if ($row[$start_var] == "") {
								break;
							}

							$end_var = "o_end_time$i";
							if ($row[$end_var] == "") {
								break;
							}

							//端数処理
							if ($fraction1 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
								//開始日時
								$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $row[$start_var]);
								//基準日時
								$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
								$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
								$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
							} else {
								$tmp_ret_start_time = str_replace(":", "", $row[$start_var]);
							}
							if ($fraction2 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
								//終了日時
								$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $row[$end_var]);
								//基準日時
								$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
								$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

								$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
							} else {
								$tmp_ret_end_time = str_replace(":", "", $row[$end_var]);
							}
							//$arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);


							$wk_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $tmp_ret_start_time);
							$wk_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $tmp_ret_end_time);
							//　分割対応の時間取得
							$wk_ret_minutes1 = 0;
							$wk_ret_minutes2 = 0;
							//分割設定なし
							if ($parttime_split_hour == "") {
								$wk_ret_minutes1 =  date_utils::get_diff_minute($wk_end_date_time, $wk_start_date_time);
							}
							//分割設定あり
							else {
								//分割前
								if ($wk_end_date_time <= $wk_parttime_split_hhmm) {
									$wk_ret_minutes1 =  date_utils::get_diff_minute($wk_end_date_time, $wk_start_date_time);
								}
								//分割後
								elseif ($wk_start_date_time >= $wk_parttime_split_hhmm) {
									$wk_ret_minutes2 =  date_utils::get_diff_minute($wk_end_date_time, $wk_start_date_time);
								}
								//分割時刻をまたがる場合
								else {
									$wk_ret_minutes1 =  date_utils::get_diff_minute($wk_parttime_split_hhmm, $wk_start_date_time);
									$wk_ret_minutes2 =  date_utils::get_diff_minute($wk_end_date_time, $wk_parttime_split_hhmm);
								}
							}
							$work_time1 += $wk_ret_minutes1;
							$work_time2 += $wk_ret_minutes2;

							//休日勤務、日またがりか判断、日別の休日を考慮した時間を取得
							$wk_ret_start_date = substr($wk_start_date_time, 0, 8);
							$wk_ret_end_date = substr($wk_end_date_time, 0, 8);

                            if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターン以外 20120402
                                //日またがりでない場合
                                if ($wk_ret_start_date == $wk_ret_end_date) {
                                    //休日勤務
                                    if (check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$wk_ret_start_date], $pattern, $reason, "1") == true
                                        ) {
                                        $hol_work += $wk_ret_minutes1;
                                        $hol_work += $wk_ret_minutes2;
                                    }

                                }
                                //日またがりの場合
                                else {
                                    //日別の休日を考慮した時間を取得
                                    $today_hol_flag = (
                                            check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$wk_ret_start_date], $pattern, $reason, ""));
                                    $next_date = next_date($wk_ret_start_date);
//                                    $nextday_hol_flag = (	check_timecard_holwk(
//                                                $arr_timecard_holwk_day, $arr_type[$next_date], $pattern, $reason, "1")); //20130308
                                    $nextday_hol_flag = (	check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1")); //20130308
                                    $wk_holtime = $timecard_common_class->get_holtime_today_nextday($wk_ret_start_date, $tmp_ret_start_time, "", $tmp_ret_end_time, "1", $today_hol_flag, $nextday_hol_flag);
                                    $hol_work += $wk_holtime;
                                }

                            }
                        }
                    }



					//一括修正のステータスがある場合は設定
					$tmp_status = $arr_tmmdapplyall_status[$date]["apply_status"];
					if ($tmmd_apply_status == "" && $tmp_status != "") {
						$tmmd_apply_status = $tmp_status;
					}
					//時間変更 tmmd_apply_statusからlink_type取得
					$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);

					//残業承認状態
					$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);

					//休出の判断
					//if (($pattern != "10" && $timecard_common_class->get_you_kinmu_nissu($arr_type["$date"]) == 0) ||
					//		$reason == "16") { //事由が振替出勤
					//休日出勤の判定変更 20100805
					//if ($pattern == "10" && ($reason == "22" || $reason == "23" || $reason == "24" || $reason == "34" || $reason == "")) {
                    if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターン以外 20120402
                        //休日出勤の判定変更、事由に関わらず休暇の場合 20100916
                        if ($pattern == "10" && $reason != "71") { // 普通残業 20120518
                            if ($overtime_approve_flg == "2") {
								;
								//休日残業を休日出勤に集計しない 20140219
                                //$hol_work += $wk_minutes1;
                                //$hol_work += $wk_minutes2;
                            }
                        } elseif (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$date], $pattern, $reason, "") == true &&
                                ($overtime_approve_flg != "1")) {
                            $hol_work += $wk_minutes1;
                            $hol_work += $wk_minutes2;
                        }
                    }
					//残業時刻２が入力されている場合、時間集計 20111121
					if ($over_start_time2 != "" && $over_end_time2 != "") {
						$arr_split_time = $timecard_common_class->get_parttime_split($parttime_split_hour, $date, $over_start_time2, $over_start_next_day_flag2, $over_end_time2, $over_end_next_day_flag2);
						$work_time1 += $arr_split_time[0];
						$work_time2 += $arr_split_time[1];
                        if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターン以外 20120402
                            //日またがりでない場合
                            if ($over_start_next_day_flag2 == "" && $over_end_next_day_flag2 == "") {
                                //休日勤務
                                if (
                                        check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$date], $pattern, $reason, "") == true
                                    ) {
                                    $hol_work += $arr_split_time[0];
                                    $hol_work += $arr_split_time[1];
                                }
                            }
                            //日またがりの場合
                            else {
                                //日別の休日を考慮した時間を取得
                                $today_hol_flag = (
                                        check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$date], $pattern, $reason, ""));
                                $next_date = next_date($date);
                                $nextday_hol_flag = (	check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1"));
                                $wk_holtime = $timecard_common_class->get_holtime_today_nextday($date, $over_start_time2, $over_start_next_day_flag2, $over_end_time2, $over_end_next_day_flag2, $today_hol_flag, $nextday_hol_flag);
                                $hol_work += $wk_holtime;
                            }
                        }
					}
                    //残業時刻org(所定時間帯の範囲内の残業対応)が入力されている場合、時間集計 20121022
                    if ($over_start_time_org != "" && $over_end_time_org != "") {
                        $arr_split_time = $timecard_common_class->get_parttime_split($parttime_split_hour, $date, $over_start_time_org, $over_start_next_day_flag, $over_end_time_org, $over_end_next_day_flag);
                        $work_time1 += $arr_split_time[0];
                        $work_time2 += $arr_split_time[1];
                        if (in_array($pattern, $hol_except_ptn) == false) { //休日出勤時間除外するパターン以外
                            //日またがりでない場合
                            if ($over_start_next_day_flag == "" && $over_end_next_day_flag == "") {
                                //休日勤務
                                if (
                                    check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$date], $pattern, $reason, "") == true
                                    ) {
                                    $hol_work += $arr_split_time[0];
                                    $hol_work += $arr_split_time[1];
                                }
                            }
                            //日またがりの場合
                            else {
                                //日別の休日を考慮した時間を取得
                                $today_hol_flag = (
                                        check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$date], $pattern, $reason, ""));
                                $next_date = next_date($date);
                                $nextday_hol_flag = (   check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1"));
                                $wk_holtime = $timecard_common_class->get_holtime_today_nextday($date, $over_start_time_org, $over_start_next_day_flag, $over_end_time_org, $over_end_next_day_flag, $today_hol_flag, $nextday_hol_flag);
                                $hol_work += $wk_holtime;
                            }
                        }
                    }
                }
				//debug
				//echo("<!--");
				//echo("$fixed_start_time ");
				//echo("$fixed_end_time ");
				//echo("$wk_minutes1 ");
				//echo("$wk_minutes2 ");
				//echo("$wk_holtime ");
				//echo("$hol_work ");

				//echo("-->\n");

				//パターン回数
				if ($row["pattern"] != "") {
					$wk_idx = $row["pattern"];
					$arr_ptn_part_cnt[$wk_idx]++;

					//交通費
					if (in_array($wk_idx, $tra_part)) {
						$tra_cnt++;
					}
				}

			}
			echo("<tr>");
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo($staff_info[$wk_emp_id]["emp_personal_id"]);
			echo("</font></td>");
			// 氏名
			$wk = $staff_info[$wk_emp_id]["staff_name"];
			$color = ( $staff_info[$wk_emp_id]["sex"] == 1 ) ? 'blue' : 'black';
			echo("<td id=\"$wk_td_id\" width=\"100\" align=\"center\" bgcolor=\"$wk_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$color\">$wk</font></td>\n");

			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$work_day = ($work_day == 0) ? "" : $work_day;
			echo($work_day);
			echo("</font></td>");

			//出勤回数 20130124
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$work_count = ($work_count == 0) ? "" : $work_count;
			echo($work_count);
			echo("</font></td>");

			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$work_time1 = round_time($work_time1, $fraction4, $fraction4_min);
			echo(minute_to_hmm($work_time1));
			echo("</font></td>");
			//分割集計がある場合
			if ($parttime_split_hour != "") {
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				$work_time2 = round_time($work_time2, $fraction4, $fraction4_min);
				echo(minute_to_hmm($work_time2));
				echo("</font></td>");
			}
			for ($j=0; $j<count($ptn_part); $j++) {

				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				$wk_idx = $ptn_part[$j];
				echo($arr_ptn_part_cnt[$wk_idx]);
				echo("</font></td>");
			}
			//休日勤務
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$hol_work = round_time($hol_work, $fraction4, $fraction4_min);
			echo(minute_to_hmm($hol_work));
			echo("</font></td>");
			//手当
			foreach($arr_allowance as $allowance)
			{
				$tmp_allow_id = $allowance["allow_id"];
				$tmp_allow_contents = $allowance["allow_contents"];
				$tmp_del_flg = $allowance["del_flg"];
				if ($allow_part[$tmp_allow_id] == "t") {
					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
					echo($arr_allow_count[$tmp_allow_id]);
					echo("</font></td>");

				}
			}
			//交通費
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$tra_cnt = ($tra_cnt == 0) ? "" : $tra_cnt;
			echo($tra_cnt);
			echo("</font></td>");
			//備考
			if ($arr_adjust["$wk_emp_id"]["memo"] != "") {
				$memo = $arr_adjust["$wk_emp_id"]["memo"];
			} else {
				// 前月に入力がある場合コピー 20110720
				$memo = $arr_adjust["$wk_emp_id"]["memo_last"];
			}
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo("<input type=\"text\" name=\"memo2_{$wk_pos}\" size=\"20\" value=\"{$memo}\">");
			echo("</font></td>");
			echo("</tr>\n");
		}
?>
</table>
<? } // end of if ($part_cnt > 0) ?>
		<table>
		<tr>
		<td align="right" width="1000">
		</td>
		<td align="right" width="240">
		<input type="button" value="PDF印刷" onclick="printPdf();">
		<input type="button" value="Excel出力" onclick="makeExcel();">
		<input type="button" value="登録" onclick="updateData();">
		</td>
		</tr>
</table>
	<?
	echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
	echo("<input type=\"hidden\" name=\"duty_yyyy\" value=\"$duty_yyyy\">\n");
	echo("<input type=\"hidden\" name=\"duty_mm\" value=\"$duty_mm\">\n");

	echo("<input type=\"hidden\" name=\"group_id\" value=\"$group_id\">\n");
	echo("<input type=\"hidden\" name=\"pattern_id\" value=\"$pattern_id\">\n");
	echo("<input type=\"hidden\" name=\"staff_ids\" value=\"$staff_ids\">\n");
	echo("<input type=\"hidden\" name=\"emp_ids1\" value=\"$emp_ids1\">\n");
	echo("<input type=\"hidden\" name=\"emp_ids2\" value=\"$emp_ids2\">\n");
	echo("<input type=\"hidden\" name=\"irrg_mon\" value=\"$irrg_month_time\">\n");
	echo("<input type=\"hidden\" name=\"adjust_time_legal_hol_flg\" value=\"$adjust_time_legal_hol_flg\">\n");
?>
		</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
	<!-- ------------------------------------------------------------------------ -->
	<!-- ＨＩＤＤＥＮ（集計表用） -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="xlsForm" method="post" action="duty_shift_total_excel.php" target="download">

	<?
	echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
	echo("<input type=\"hidden\" name=\"duty_yyyy\" value=\"$duty_yyyy\">\n");
	echo("<input type=\"hidden\" name=\"duty_mm\" value=\"$duty_mm\">\n");

	echo("<input type=\"hidden\" name=\"group_id\" value=\"$group_id\">\n");
	echo("<input type=\"hidden\" name=\"pattern_id\" value=\"$pattern_id\">\n");
	echo("<input type=\"hidden\" name=\"staff_ids\" value=\"$staff_ids\">\n");
	echo("<input type=\"hidden\" name=\"emp_ids1\" value=\"$emp_ids1\">\n");
	echo("<input type=\"hidden\" name=\"emp_ids2\" value=\"$emp_ids2\">\n");
	echo("<input type=\"hidden\" name=\"irrg_mon\" value=\"$irrg_month_time\">\n");
	echo("<input type=\"hidden\" name=\"adjust_time_legal_hol_flg\" value=\"$adjust_time_legal_hol_flg\">\n");
?>
	</form>

	<?
}
	?>

</body>

<? pg_close($con); ?>

</html>

