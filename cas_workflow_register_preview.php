<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cas_workflow_register_preview.ini");
require_once("show_select_values.ini");
require_once("cas_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session, $CAS_MANAGE_AUTH, $fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$cas_title = get_cas_title_name();

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?>｜プレビュー</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<?
	// テンプレート情報取得
	$datfilename = "cas_workflow/tmp/{$session}_d.php";
	if (!$fp = fopen($datfilename, "r")) {
		echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	$wkfw_content = fread($fp, filesize($datfilename));

	fclose($fp);

	$num = 0;
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}

	if ($num > 0) {
		// 外部ファイルを読み込む
		write_yui_calendar_use_file_read_0_12_2();
	}
	// カレンダー作成、関数出力
	write_yui_calendar_script2($num);

?>
<script type="text/javascript">
<!--
// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}

img.close {
	background-image:url("images/folder_close_bro.gif");
	cursor:hand;
}
img.open {
	background-image:url("images/folder_open_bro.gif");
	cursor:hand;
}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();">
<center>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>プレビュー</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<?
	show_application_template_for_wkfwpreview($con, $session, $fname, $wkfw_type, $wkfw_folder_id, $wkfw_appr, $wkfw_content_type, $wkfw_title, $wkfw_content, $short_wkfw_name, $apply_title_disp_flg);
?>

</center>
</body>
<? pg_close($con); ?>
</html>
