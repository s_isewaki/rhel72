<?
ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("aclg_tools_array.php");
require_once("sot_util.php");

$initerror = true;
for (;;){
	//セッションのチェック
	$session = qualify_session($session,$fname);
	if(!$session) break;
	// アクセスログ権限チェック
	$checkauth = check_authority($session,92,$fname);
	if(!$checkauth) break;
	//DB接続
	$con = connect2db($fname);
	if(!$con) break;
	$initerror = false;
	break;
}
// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// DBコネクションの作成
$con = connect2db($fname);

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
    $lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}

?>
<title>CoMedix アクセスログ | 日別一覧（管理画面）</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list th,.list td { border:#5279a5 solid 1px; text-align:center }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onresize="adjustListWidth()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="header_menu_table">
	<tr bgcolor="#f6f9ff">
		<td width="32" height="32" class="spacing">
			<a href="aclg_menu.php?session=<? echo($session); ?>">
				<img src="img/icon/b_dmy_log.gif" width="32" height="32" border="0" alt="アクセスログ">
			</a>
		</td>
		<td width="100%">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;
				<a href="aclg_menu.php?session=<? echo($session); ?>"><b>アクセスログ</b></a>
			</font>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>

<!-- center -->
		<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->

<!-- right -->
		<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr height="22">
					<td width="150" align="center" bgcolor="#bdd1e7">
						<a href="./aclg_menu.php?session=<? echo($session); ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧（ユーザ）</font>
						</a>
					</td>
					<td width="5">&nbsp;</td>
					<td width="150" align="center" bgcolor="#5279a5">
						<a href="./aclg_menu_admin.php?session=<? echo($session); ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>日別一覧（管理画面）</b></font>
						</a>
					</td>
					<td width="5">&nbsp;</td>
					<td width="80" align="center" bgcolor="#bdd1e7">
						<a href="./aclg_tools.php?session=<? echo($session); ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">機能別詳細</font>
						</a>
					</td>
					<td width="5">&nbsp;</td>
					<td width="80" align="center" bgcolor="#bdd1e7">
						<a href="./aclg_config.php?session=<? echo($session); ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定</font>
						</a>
					</td>
					<td></td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>

<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<?
//==========================================================================
// 検索条件
//==========================================================================
$min_op_ymd = date("Y");
$min_op_y = (int)substr($min_op_ymd, 0, 4);
$max_op_y = date("Y");


//過去5年分を表示
$min_op_y = $min_op_y -5;

if (@$_REQUEST["mode"]!="search"){
    $date_y = date("Y");
    $date_m = date("m");
}
$date_ym = ($date_y*100)+$date_m;
$day_max = date("d", strtotime($date_y."-".($date_m<12?$date_m+1:1)."-1 -1 day"));
?>

			<div style="padding:4px;" id="div_search_box"><?=$font?>
				<form id="aclgform" name="aclgform" action="aclg_menu_admin.php" method="get">
					<input type="hidden" name="session" value="<?=$session ?>">
					<input type="hidden" name="mode" value="search">
					<select name="date_y">
<? for ($i=$max_op_y; $i >= $min_op_y; $i--){ ?><option value="<?=$i?>" <?= $i==$date_y?"selected":"" ?>><?=$i?></option><?}?>
					</select>&nbsp;年&nbsp;
					<select name="date_m">
<? for ($i=1; $i <= 12; $i++){ ?><option value="<?=$i?>" <?= $i==$date_m?"selected":"" ?>><?=$i?></option><?}?>
					</select>&nbsp;月&nbsp;
					<input type="button" value="検索"  onclick="post_setting('');" />
					<input type="button" value="エクセル出力" onclick="post_setting('2');" />
					<input type="hidden" name="regstat" id="regstat" value="">
				</form>
				</font>
			</div>

<?
//==========================================================================
// 一覧
//==========================================================================

for($i=1; $i<=count($tools); $i++)
{
	// ライセンス無しは飛ばす
	if ($i==22 && $lcs_func[31]!="t") continue;

	$sql = "select substr(wdate,7,2) as setDate, setCount from
	(
	    select substr(ac_date,0,9) as wdate, count(substr(ac_date,0,9)) as setCount
	    from aclg_data where cate_id='0006' and detail_id='2' and status1='".$tools[$i][1]."'
	    and substr(ac_date,0,7) ='".$date_ym."'
	    group by substr(ac_date,0,9)
	) subTable";
	$cond = "";

	$sel = select_from_table($con, $sql,$cond,$fname);
	$data = array();
	while($row = pg_fetch_array($sel))
	{

		for($j = 1; $j <= $day_max; $j++)
		{

			$chg_day = sprintf("%02d", $j);

			if($row["setdate"] == $chg_day)
			{
				//echo($row["setcount"]);
				$tools[$i]["coutData"][$j] = $row["setcount"];
			}
		}
	}
}

// 基本機能
echo "<div id=\"div_data_field\">";
echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\" style=\"width:100%\">";
echo "<tr style=\"background-color:#fefcdf\">";
echo "<th style=\"text-align:left; background-color:#fefcdf; white-space:nowrap\">".$font."基本機能</font></th>";
for($i=1; $i<=$day_max; $i++) {
	echo "<td>";
	echo $i>0 ? $i : "";
	echo "</td>";
}
echo "</tr>";

for($i=1; $i<=16; $i++)
{
	if($i == 1 || $i == 6 || $i == 7 || $i == 16)
	{
		//「ログイン」、「タスク」、「伝言メモ」、「パスワード変更」は表示しない
		continue;
	}

	echo "<tr>";
	echo "<td style=\"text-align:left; background-color:#f6f9ff; white-space:nowrap\">$font".$tools[$i][0]."</font></td>";
	for($j = 1; $j <= $day_max; $j++)
	{
		echo "<td width=\"3%\">".@$tools[$i]["coutData"][$j]."</td>";
	}
	echo "</tr>";
}
//echo "</table>";
//echo "</div>";
//echo "<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"10\"><br>";
echo "<tr><td colspan=\"".($day_max + 1)."\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"10\"><br></td></tr>";

// 業務機能
//echo "<div style=\"overflow-x:scroll\" id=\"div_data_field\">";
//echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\" style=\"width:100%\">";
echo "<tr style=\"background-color:#fefcdf\">";
echo "<th style=\"text-align:left; background-color:#fefcdf; white-space:nowrap\">".$font."業務機能</font></th>";
for($i=1; $i<=$day_max; $i++) {
	echo "<td>";
	echo $i>0 ? $i : "";
	echo "</td>";
}
echo "</tr>";
$idxs = array(17, 22);
foreach ($idxs as $i) {
	// ライセンス無しは飛ばす
	if ($i==22 && $lcs_func[31]!="t") continue;
	echo "<tr>";
	echo "<td style=\"text-align:left; background-color:#f6f9ff; white-space:nowrap\">$font".$tools[$i][0]."</font></td>";
	for($j = 1; $j <= $day_max; $j++) {
		echo "<td width=\"3%\">".@$tools[$i]["coutData"][$j]."</td>";
	}
	echo "</tr>";
}
echo "</table>";
echo "</div>";
echo "<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"10\"><br>";

/*
// 管理機能
echo "<div style=\"overflow-x:scroll\" id=\"div_data_field\">";
echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\" style=\"width:100%\">";
echo "<tr style=\"background-color:#f6f9ff\">";
echo "<th style=\"text-align:left; background-color:#f6f9ff; white-space:nowrap\">".$font."管理機能</font></th>";
for($i=1; $i<=$day_max; $i++) {
	echo "<td>";
	echo $i>0 ? $i : "";
	echo "</td>";
}
echo "</tr>";
for($i=17; $i<=20; $i++) {
	echo "<tr>";
	echo "<td style=\"text-align:left; background-color:#f6f9ff; white-space:nowrap\">$font".$tools[$i][0]."</font></td>";
	for($j = 1; $j <= $day_max; $j++) {
		echo "<td width=\"3%\">".@$tools[$i]["coutData"][$j]."</td>";
	}
	echo "</tr>";
}
echo "</table>";
echo "</div>";
//}
*/
?>
<script type="text/javascript">
function adjustListWidth(){
	if (!document.getElementById("div_data_field")) {
		return;
	}
	document.getElementById("div_data_field").style.width =
		document.getElementById("header_menu_table").clientWidth - 20;
	}

function post_setting(status){

	aclgForm = document.getElementById("aclgform");

	if(status == '2')
	{
		//エクセル出力処理
		aclgForm.action = 'aclg_menu_excel.php';
	}
	else
	{
		//検索・表示処理
		aclgForm.action = 'aclg_menu_admin.php';
	}

	document.aclgform.regstat.value=status;
    aclgForm.method = 'post';
	document.aclgform.submit();

}


</script>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
