<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$auth_nc_reg = check_authority($session, 10, $fname);
if ($auth_nc_reg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$nmrm_start = $nmrm_start_year . $nmrm_start_month . $nmrm_start_day . $nmrm_start_hour . $nmrm_start_minute;

$nmrm_end = $nmrm_end_year . $nmrm_end_month . $nmrm_end_day . $nmrm_end_hour . $nmrm_end_minute;

$nmrm_lmt_flg = ($nmrm_lmt_flg == "on") ? "t" : "f";

// テーマ - 文字列長
if (strlen($nmrm_thm) > 40) {
	echo("<script language='javascript'>alert('テーマが長すぎます');");
	echo("history.back();</script>");
	exit;
}

// 開始日時 - 日付
if (!checkdate($nmrm_start_month, $nmrm_start_day, $nmrm_start_year)) {
	echo("<script language='javascript'>alert('開始日が無効です');");
	echo("history.back();</script>");
	exit;
}

// 終了日時 - 日付
if (!checkdate($nmrm_end_month, $nmrm_end_day, $nmrm_end_year)) {
	echo("<script language='javascript'>alert('終了日が無効です');");
	echo("history.back();</script>");
	exit;
}

// 終了日時 - 日時
if ($nmrm_end <= $nmrm_start) {
	echo("<script language='javascript'>alert('終了日時を開始日時より後にしてください');");
	echo("history.back();</script>");
	exit;
}

//==============================================================================
// 登録処理
//==============================================================================

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// ネット会議更新SQLの実行
$sql_nmrm = "update nmrmmst set";
$set_nmrm = array("nmrm_thm", "nmrm_start", "nmrm_end", "nmrm_lmt_flg");
$setvalue_nmrm = array($nmrm_thm, $nmrm_start, $nmrm_end, $nmrm_lmt_flg);
$cond_nmrm = "where nmrm_id = '$nmrm_id'";
$up_nmrm = update_set_table($con, $sql_nmrm, $set_nmrm, $setvalue_nmrm, $cond_nmrm, $fname);
if ($up_nmrm == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// ネット会議メンバー削除SQLの実行
$sql_nmme_del = "delete from nmmemst";
$cond_nmme_del = "where nmrm_id = '$nmrm_id'";
$del_nmme = delete_from_table($con, $sql_nmme_del, $cond_nmme_del, $fname);
if ($del_nmme == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// ネット会議メンバー登録SQLの実行
if (is_array($member)) {
	foreach ($member as $emp_id) {
		$sql_nmme_in = "insert into nmmemst (nmrm_id, emp_id) values (";
		$content_nmme_in = array($nmrm_id, $emp_id);
		$in_nmme = insert_into_table($con, $sql_nmme_in, $content_nmme_in, $fname);
		if ($in_nmme == 0) {
			pg_query("rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続をクローズ
pg_close($con);

// ネット会議一覧画面に遷移
	echo("<script language='javascript'>location.href='net_conference_menu.php?session=$session';</script>");
	exit;
?>
