<?
ob_start() ;
if (!$_REQUEST["print_preview"]) // 一括印刷画面でないとき
{
  $forPDF = ($_REQUEST["direct_template_pdf_print_requested"]) ? true : false ;

  require_once("about_postgres.php");
  require_once("about_session.php");
  require_once("about_authority.php");
  require_once("show_select_values.ini");
  require_once("yui_calendar_util.ini");
  require_once("sot_util.php");
  require_once("summary_common.ini");
  require_once("get_values.php");
  require_once("get_menu_label.ini");
  require_once("fpdf153/mbfpdf.php");

  //====================================================================
  //セッションチェック、権限チェック、DB接続
  //====================================================================
  $fname = $_SERVER["PHP_SELF"];
  $session = qualify_session($session,$fname);
  $summary_check = @check_authority($session,57,$fname);
  $con = @connect2db($fname);
  if(!$session || !$summary_check || !$con){
    echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    ob_end_flush() ;
    exit;
  }

  //=================================================
  // 対象期間(指定の無いto日付は本日とする)
  //=================================================
  $date_y1  = (int)@$_REQUEST["date_y1"] ; // 指定なし,"-","00" いずれでも値は0になる(以下同じ)
  $date_m1  = (int)@$_REQUEST["date_m1"] ;
  $date_d1  = (int)@$_REQUEST["date_d1"] ;
  $date_y2_ = (int)@$_REQUEST["date_y" ] ;
  $date_m2_ = (int)@$_REQUEST["date_m" ] ;
  $date_d2_ = (int)@$_REQUEST["date_d" ] ;
  $date_y2 = ($date_y2_) ? $date_y2_ : (int)date("Y") ;
  $date_m2 = ($date_m2_) ? $date_m2_ : (int)date("m") ;
  $date_d2 = ($date_d2_) ? $date_d2_ : (int)date("d") ;
  $from_yyyymmdd = (($date_y1 == 0) ? "0000" : $date_y1) . (($date_m1 < 10) ? "0" . $date_m1 : $date_m1) . (($date_d1 < 10) ? "0" . $date_d1 : $date_d1) ;
  $to_yyyymmdd   = (($date_y2 == 0) ? "0000" : $date_y2) . (($date_m2 < 10) ? "0" . $date_m2 : $date_m2) . (($date_d2 < 10) ? "0" . $date_d2 : $date_d2) ;

  //ログイン職員ＩＤ
  //$emp_id = get_emp_id($con, $session, $fname);

  //=================================================
  // 病棟などが指定されていなければデフォルトを設定する
  // ＠＠＠ 事業所(棟)欄は無指定にできないので、この条件は常にfalse ＠＠＠
  //=================================================
  if (@$apply_date_default && @$sel_bldg_cd=="" && @$sel_ward_cd==""){
    $sql =
      " select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
      " inner join empmst emp on (".
      "   emp.emp_class = swer.class_id".
      "   and emp.emp_attribute = swer.atrb_id".
      "   and emp.emp_dept = swer.dept_id".
      "   and emp.emp_id = '".pg_escape_string($emp_id)."'".
      " )";
    $emp_ward = @$c_sot_util->select_from_table($sql);
    $_REQUEST["sel_bldg_cd"] = @pg_fetch_result($emp_ward, 0, "bldg_cd");
    $_REQUEST["sel_ward_cd"] = @pg_fetch_result($emp_ward, 0, "ward_cd");
    $sel_bldg_cd= $_REQUEST["sel_bldg_cd"];
    $sel_ward_cd= $_REQUEST["sel_ward_cd"];
  }

  $sel_team_id = (int)@$_REQUEST["sel_team_id"]; // ←PDF印刷時は無指定

  $THIS_WORKSHEET_ID = "1"; // [sotmst][sot_id]の固定値

  // 項目定義
  $fimItems = array(
      array("undou"=>"運動",
        array("selfcare"=>"セルフケア",      array("meal"=>"食事", "seiyou"=>"整容", "seisiki"=>"清拭", "kouiupr"=>"更衣・上半身", "kouilwr"=>"更衣・下半身")),
        array("haisetu"=>"排泄コントロール", array("toilet"=>"トイレ動作", "nyou"=>"排尿管理", "ben"=>"排便管理")),
        array("ijou"=>"移乗",                array("bedisu"=>"ベッド・椅子・車椅子", "toilet"=>"トイレ")),
        array("idou"=>"移動",                array("yokusou"=>"浴槽・シャワー", "kurumaisu"=>"歩行・車椅子", "kaidan"=>"階段"))),
      array("ninchi"=>"認知",
        array("comm"=>"コミュニケーション",  array("rikai"=>"理解", "hyoushutu"=>"表出")),
        array("shakai"=>"社会的認知",        array("kouryu"=>"社会的交流", "kaiketu"=>"問題解決", "kioku"=>"記憶")))
      );
  // 自立点数定義
  $fimMarks = array(
      "",
      "全介助（患者自身で２５％未満）",
      "最大介助（患者自身で２５％以上）",
      "中等度介助（患者自身で５０％以上）",
      "患者自身で７５％以上",
      "監視",
      "修正自立（補助具使用）",
      "完全自立（時間、安全性含め）"
      );

  $pt_id   = @$_REQUEST["sel_ptif_id"] ;
  $pt_list = array();
  $in_list = array(); // 患者ID=>入院情報(患者名,事業所(棟),病棟,病室名)
  // 患者(ID)が指定されたとき
  if ($pt_id)
  {
    $pt_list[] = $pt_id ;
    $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, $to_yyyymmdd) ;
    $inpt_kj_nm = @$nyuin_info["inpt_lt_kj_nm"] . " " . @$nyuin_info["inpt_ft_kj_nm"] ;
    $in_info = array(
            "inpt_kj_nm" => $inpt_kj_nm,
            "bldg_cd"    => @$nyuin_info["bldg_cd"],
            "bldg_name"  => @$nyuin_info["bldg_name"],
            "ward_cd"    => @$nyuin_info["ward_cd"],
            "ward_name"  => @$nyuin_info["ward_name"],
            "ptrm_cd"    => @$nyuin_info["ptrm_room_no"],
            "ptrm_name"  => @$nyuin_info["ptrm_name"],
            "inpt_in_dt" => @$nyuin_info["inpt_in_dt"]
    );
    $in_list[$pt_id] = $in_info ;
  }
  // 患者(ID)が指定されなかったとき（一括印刷の場合）
  else if ($forPDF)
  {
    //=================================================
    // 対象患者のリストアップ
    // 検索範囲から以下のリストアップ
    // ・対象期間内に
    // ・指定病棟に入院していること
    //=================================================
    $bldg_cd = @$_REQUEST["sel_bldg_cd"     ] ;
    $ward_cd = @$_REQUEST["sel_ward_cd"     ] ;
    $room_no = @$_REQUEST["sel_ptrm_room_no"] ;
    $cond_bldg = ($bldg_cd) ? " and pt.bldg_cd = " .      $bldg_cd : "" ;
    $cond_ward = ($ward_cd) ? " and pt.ward_cd = " .      $ward_cd : "" ;
    $cond_room = ($room_no) ? " and pt.ptrm_room_no = " . $room_no : "" ;

    // 入院情報を取得
    $sql =
      " select" .
      "   pt.ptif_id" .
      "  ,pt.inpt_lt_kj_nm || ' ' || pt.inpt_ft_kj_nm as inpt_kj_nm" .
      "  ,pt.inpt_in_dt" .
      "  ,bg.bldg_cd" .
      "  ,bg.bldg_name" .
      "  ,wd.ward_cd" .
      "  ,wd.ward_name" .
      "  ,rm.ptrm_room_no" .
      "  ,rm.ptrm_name" .
      " from" .
      "   inptmst pt" .
      "   left join bldgmst bg on (bg.bldg_cd = pt.bldg_cd)" .
      "   left join wdmst   wd on (wd.bldg_cd = pt.bldg_cd and wd.ward_cd = pt.ward_cd)" .
      "   left join ptrmmst rm on (rm.bldg_cd = pt.bldg_cd and rm.ward_cd = pt.ward_cd and rm.ptrm_room_no = pt.ptrm_room_no)" .
      " where" .
      "       1 = 1" .
      $cond_bldg .
      $cond_ward .
      $cond_room .
      "   and pt.inpt_in_dt <= '" . $to_yyyymmdd . "'" .
      " order by" .
      "   bg.bldg_cd" .
      "  ,wd.ward_cd" .
      "  ,rm.ptrm_room_no" ;
    $sel = select_from_table($con, $sql, "", $fname) ;
    
    // 取得した入院情報から対象患者リストを作成
    while ($row = pg_fetch_array($sel))
    {
      $pt_id = $row["ptif_id"] ;
      $pt_list[] = $pt_id ;
      $in_info = array(
              "inpt_kj_nm" => $row["inpt_kj_nm"],
              "bldg_cd"    => $row["bldg_cd"],
              "bldg_name"  => $row["bldg_name"],
              "ward_cd"    => $row["ward_cd"],
              "ward_name"  => $row["ward_name"],
              "ptrm_cd"    => $row["ptrm_room_no"],
              "ptrm_name"  => $row["ptrm_name"],
              "inpt_in_dt" => $row["inpt_in_dt"]
      );
      $in_list[$pt_id] = $in_info ;
    }

    // 入院履歴情報を取得
    $sql =
      " select" .
      "   pt.ptif_id" .
      "  ,pt.inpt_lt_kj_nm || ' ' || pt.inpt_ft_kj_nm as inpt_kj_nm" .
      "  ,pt.inpt_in_dt" .
      "  ,bg.bldg_cd" .
      "  ,bg.bldg_name" .
      "  ,wd.ward_cd" .
      "  ,wd.ward_name" .
      "  ,rm.ptrm_room_no" .
      "  ,rm.ptrm_name" .
      " from" .
      "   inpthist pt" .
      "   left join bldgmst bg on (bg.bldg_cd = pt.bldg_cd)" .
      "   left join wdmst   wd on (wd.bldg_cd = pt.bldg_cd and wd.ward_cd = pt.ward_cd)" .
      "   left join ptrmmst rm on (rm.bldg_cd = pt.bldg_cd and rm.ward_cd = pt.ward_cd and rm.ptrm_room_no = pt.ptrm_room_no)" .
      " where" .
      "       1 = 1" .
      $cond_bldg .
      $cond_ward .
      $cond_room .
      "   and pt.inpt_in_dt is not null" .
      "   and pt.inpt_in_dt <> ''" .
      "   and ((pt.inpt_in_dt <= '" . $to_yyyymmdd . "' and pt.inpt_out_dt >= '" . $from_yyyymmdd . "') or (pt.inpt_in_dt <= '" . $to_yyyymmdd . "' and (pt.inpt_out_dt is null or pt.inpt_out_dt >= '" . $from_yyyymmdd . "')))" .
      " order by" .
      "   bg.bldg_cd" .
      "  ,wd.ward_cd" .
      "  ,rm.ptrm_room_no" ;
    $sel = select_from_table($con, $sql, "", $fname);
    
    // 取得した入院履歴情報から、入院情報と重複しない患者の情報をリストに追加する
    while ($row = pg_fetch_array($sel))
    {
      $pt_id = $row["ptif_id"] ;
      if (in_array($pt_id, $pt_list)) continue ;
      $pt_list[] = $pt_id ;
      $in_info = array(
              "inpt_kj_nm" => $row["inpt_kj_nm"],
              "bldg_cd"    => $row["bldg_cd"],
              "bldg_name"  => $row["bldg_name"],
              "ward_cd"    => $row["ward_cd"],
              "ward_name"  => $row["ward_name"],
              "ptrm_cd"    => $row["ptrm_room_no"],
              "ptrm_name"  => $row["ptrm_name"],
              "inpt_in_dt" => $row["inpt_in_dt"]
      );
      $in_list[$pt_id] = $in_info ;
    }
  }

  // 処理対象のXMLファイルを取得する
  $inPhrase = " sm.ptif_id in (" ;
  foreach ($pt_list as $idx => $pt_id) $inPhrase .= ("'" . $pt_id . "',") ;
  $inPhrase = rtrim($inPhrase, ",") . ")" ;

  $sql =
    " select" .
    "   sx.ptif_id" .
    "  ,sx.xml_file_name" .
    "  ,sx.apply_id" .
    " from" .
    "   summary sm" .
    "   inner join tmplmst tm on (sm.tmpl_id = tm.tmpl_id)" .
    "   inner join sum_xml sx on (sm.ptif_id = sx.ptif_id and sm.summary_id = sx.summary_id)" .
    " where" .
    $inPhrase .
    "   and tm.tmpl_name = 'FIM評価表'" .
    "   and (sx.delete_flg is null or sx.delete_flg <> '1')" .
    " order by" .
    "   sx.ptif_id" ;
  $sel = $c_sot_util->select_from_table($sql) ;
  $rows = pg_fetch_all($sel) ;
  $rows[]["ptif_id"] = "" ; // 最後の患者IDに対する処理を行わせるため

  // すべてのXMLファイルからコンテンツを取得する
  $xml = new template_xml_class() ;
  $contsAll = array() ;
  $conts = null ;
  $currPtId = "" ;

  $hyouka_avl = "22009999" ;   // 平均点  例：20101206 → 22009999
  $dtstr_avl = "99/99" ;       // 平均点  例：12/06 → 99/99

  foreach ($rows as $idx => $row)
  {
    $pt_id = $row["ptif_id"] ;

    if ($pt_id != $currPtId)
    {
      if ($conts != null)
      {
        // 構築した配列をキー(評価日)で昇順ソートする
        ksort($conts) ;

        // FIM評価値の計を求めて平均を算出する
        foreach ($fimItems as $idx0 => $item0)
        {
          foreach ($item0 as $id1 => $item1)
          {
            if (is_string($item1))
            {
              $tagName2 = $tagName1 . $id1 . "_"   ; // 例：fimh_undou_
            }
            else
            {
              foreach ($item1 as $id2 => $item2)
              {
                if (is_string($item2))
                {
                  $tagName3 = $tagName2 . $id2 . "_" ; // 例：fimh_undou_selfcare_
                }
                else
                {
                  foreach ($item2 as $id3 => $item3)
                  {
                    $tagName4 = $tagName3 . $id3     ; // 例：fimh_undou_selfcare_meal
                    // FIM評価行の分
                    $conts[$hyouka_avl][$tagName4] = "" ;             // FIM評価値       平均点
                    $conts[$hyouka_avl][$tagName4 . "_avlval"] = 0 ;
                    $conts[$hyouka_avl][$tagName4 . "_avlcnt"] = 0 ;
                    foreach ($conts as $key => $val)
                    {
                      if (strcmp($key, $hyouka_avl))
                      {
                        if ($val[$tagName4] != "")
                        {
                          $conts[$hyouka_avl][$tagName4 . "_avlval"] += intval($val[$tagName4]) ;
                          $conts[$hyouka_avl][$tagName4 . "_avlcnt"] += 1 ;
                        }
                      }
                      else
                      {
                        $avlval = floor(($conts[$hyouka_avl][$tagName4 . "_avlval"] * 10) / $conts[$hyouka_avl][$tagName4 . "_avlcnt"]) / 10 ;
                        $conts[$hyouka_avl][$tagName4] = ($avlval > 0) ? (string)$avlval : "" ;
                      }
                    }
                  }
                }
              }
            }
          }
          // 小計行の分
          $conts[$hyouka_avl][$tagName2 . "subtotal"] = "" ;          // FIM評価小計     平均点
          $conts[$hyouka_avl][$tagName2 . "subtotal_avlval"] = 0 ;
          $conts[$hyouka_avl][$tagName2 . "subtotal_avlcnt"] = 0 ;
          foreach ($conts as $key => $val)
          {
            if (strcmp($key, $hyouka_avl))
            {
              if ($val[$tagName2 . "subtotal"] != "")
              {
                $conts[$hyouka_avl][$tagName2 . "subtotal_avlval"] += intval($val[$tagName2 . "subtotal"]) ;
                $conts[$hyouka_avl][$tagName2 . "subtotal_avlcnt"] += 1 ;
              }
            }
            else
            {
              $avlval = floor(($conts[$hyouka_avl][$tagName2 . "subtotal_avlval"] * 10) / $conts[$hyouka_avl][$tagName2 . "subtotal_avlcnt"]) / 10 ;
              $conts[$hyouka_avl][$tagName2 . "subtotal"] = ($avlval > 0) ? (string)$avlval : "" ;
            }
          }
        }
        // 合計行の分
        $conts[$hyouka_avl][$tagName1 . "total"] = "" ;               // FIM評価合計     平均点
        $conts[$hyouka_avl][$tagName1 . "total_avlval"] = 0 ;
        $conts[$hyouka_avl][$tagName1 . "total_avlcnt"] = 0 ;
        foreach ($conts as $key => $val)
        {
          if (strcmp($key, $hyouka_avl))
          {
            if ($val[$tagName1 . "total"] != "")
            {
              $conts[$hyouka_avl][$tagName1 . "total_avlval"] += intval($val[$tagName1 . "total"]) ;
              $conts[$hyouka_avl][$tagName1 . "total_avlcnt"] += 1 ;
            }
          }
          else
          {
            $avlval = floor(($conts[$hyouka_avl][$tagName1 . "total_avlval"] * 10) / $conts[$hyouka_avl][$tagName1 . "total_avlcnt"]) / 10 ;
            $conts[$hyouka_avl][$tagName1 . "total"] = ($avlval > 0) ? (string)$avlval : "" ;
          }
        }

        $ptInfos = array("pt_info"=>$in_list[$currPtId]) ;
        $contsAll[$currPtId] = $ptInfos + $conts ;
      }
      $conts = array() ;
      $currPtId = $pt_id ;
    }
    $xmlFileName = $row["xml_file_name"] ;
    $applyId     = $row["apply_id"     ] ;
    $data = $xml->get_xml_object_array_from_db($con, $fname, $xmlFileName) ;
    $worksheet = @$data->nodes["template"]->nodes["FIMHyoukahyou"] ;
    $hyouka_ymd = @$worksheet->nodes["hyouka_ymd"]->value ; // 例：20101206
    // データの日付が無効であればスキップ
    if (!isValidDate($from_yyyymmdd, $to_yyyymmdd, $hyouka_ymd, $in_list[$pt_id]["inpt_in_dt"])) {
        continue;
    }
    $conts[$hyouka_ymd]["apply_id"] = $applyId ;
    $tagName1 = "fimh_" ;

    foreach ($fimItems as $idx0 => $item0)
    {
      foreach ($item0 as $id1 => $item1)
      {
        if (is_string($item1))
        {
          $tagName2 = $tagName1 . $id1 . "_"   ; // 例：fimh_undou_
          $node1    = @$worksheet->nodes[$id1] ; // 例：undou
        }
        else
        {
          foreach ($item1 as $id2 => $item2)
          {
            if (is_string($item2))
            {
              $tagName3 = $tagName2 . $id2 . "_" ; // 例：fimh_undou_selfcare_
              $node2    = @$node1->nodes[$id2]   ; // 例：selfcare
            }
            else
            {
              foreach ($item2 as $id3 => $item3)
              {
                $tagName4 = $tagName3 . $id3     ; // 例：fimh_undou_selfcare_meal
                $node3    = @$node2->nodes[$id3] ; // 例：meal
                $conts[$hyouka_ymd][$tagName4] = $node3->value ;
              }
            }
          }
        }
      }
      $conts[$hyouka_ymd][$tagName2 . "subtotal"] = @$node1->nodes["subtotal"]->value ;

    }
    $conts[$hyouka_ymd][$tagName1 . "total"] = @$worksheet->nodes["total"]->value ;
  }


  // $contsAll配列を最初に取得した患者の順でソートする
  // (画面表示のときは、FIMデータが無い患者も含める)
  $sortedContsAll = array() ;
  foreach ($pt_list as $idx => $pt_id)
  {
    $ptConts = @$contsAll[$pt_id] ;
    if ($forPDF)
    {
      if (!$ptConts) continue ;
    }
    else
    {
      if (!$ptConts) $ptConts = array("pt_info"=>$in_list[$pt_id]) ;
    }
    $sortedContsAll[$pt_id] = $ptConts ;
  }

  if ($forPDF)
  {
    $ikkatu = array_key_exists("date_y1", $_REQUEST) ; // 一括印刷設定画面からのポストバック時はtrue
    if (count($sortedContsAll) < 1)
    {
      if ($ikkatu)
      {
        $alert    = "alert('指定した印刷条件では、印刷対象患者は存在しませんでした。');" ;
        $location = "window.location.href='sot_worksheet_fim.php?session=".$session."&date_y1=".$_REQUEST["date_y1"]."&date_m1=".$_REQUEST["date_m1"]."&date_d1=".$_REQUEST["date_d1"]."&date_y=".$_REQUEST["date_y"]."&date_m=".$_REQUEST["date_m"]."&date_d=".$_REQUEST["date_d"]."&print_preview=y&sel_ptif_id=".$_REQUEST["sel_ptif_id"]."&sel_bldg_cd=".$_REQUEST["sel_bldg_cd"]."&sel_ward_cd=".$_REQUEST["sel_ward_cd"]."';" ;
        echo $c_sot_util->javascript($alert . $location) ;
        ob_end_flush() ;
      }
    }
    else
    {
      exec_pdf_print($sortedContsAll, $ikkatu) ;
    }
    pg_close($con) ;
    die ;
  }

  // ＠＠＠↑PDF印刷時はここまでで処理終了＠＠＠

  // 先頭のデータのみを取得する
  $conts = @current($sortedContsAll) ;

  //$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'";
  //$sel = select_from_table($con, $sql, "", $fname);
  //$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));
  if (count($sortedContsAll) > 0) {
    $ptif_id   = @key($sortedContsAll) ;
    $ptif_name = @$conts["pt_info"]["inpt_kj_nm"] ;
    $bldg_cd   = @$conts["pt_info"]["bldg_cd"   ] ;
    $ward_cd   = @$conts["pt_info"]["ward_cd"   ] ;
    $ptrm_cd   = @$conts["pt_info"]["ptrm_cd"   ] ;
  } else {
    $ptif_id   = "" ;
    $ptif_name = "" ;
    $bldg_cd   = @$_REQUEST["sel_bldg_cd"     ] ;
    $ward_cd   = @$_REQUEST["sel_ward_cd"     ] ;
    $ptrm_cd   = @$_REQUEST["sel_ptrm_room_no"] ;
  }

  // ここで｢患者情報｣要素を削除する
  //array_splice($conts, 0, 1) ; // ←これだとキーがただのインデックス値に変わってしまう
  unset($conts["pt_info"]) ;

  $sql = "select tmpl_id from tmplmst where tmpl_file = 'tmpl_FIMHyoukahyou.php'" ;
  $sel = select_from_table($con, $sql, "", $fname);
  $tmpl_id = @pg_fetch_result($sel, 0, "tmpl_id");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | FIM評価表</title>
<? write_yui_calendar_use_file_read_0_12_2();// 外部ファイルを読み込む ?>
<? write_yui_calendar_script2(1);// カレンダー作成、関数出力 ?>
<script type="text/javascript">

<? //*************************************
   // 患者検索ダイアログのポップアップ
   //************************************* ?>
function popupKanjaSelect(){
  // 入力エリアに患者IDが入力されていた場合、検索結果を表示する
  var sel_ptif_id = document.getElementById("sel_ptif_id").value;
  if (sel_ptif_id != "") {
    callbackKanjaSelect(sel_ptif_id);
    return;
  }
  
  window.open(
    "sot_kanja_select.php?session=<? echo $session; ?>&js_callback=callbackKanjaSelect&sel_bldg_cd=<? echo $bldg_cd; ?>&sel_ward_cd=<? echo $ward_cd; ?>&sel_ptif_id=<? echo $ptif_id; ?>&sel_sort_by_byoto=1",
    "kanjaSelectWindow",
    "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height="+window.screen.availHeight+",left=0,top=0"
  );
}
<? //*************************************
   // 患者検索結果を受けるコールバック関数
   //************************************* ?>
function callbackKanjaSelect(ptif_id){
  document.search.sel_ptif_id.value = ptif_id;
  document.search.submit();
}

<? //*************************************
   // PDF 印刷
   //************************************* ?>
function popupPrintPreview(){
  if (document.getElementById("sel_ptif_id").value == "")
  {
    alert("患者を指定してください。");
    return;
  }
  window.open("sot_worksheet_fim.php?session=<? echo $session; ?>&sel_ptif_id=<? echo $ptif_id; ?>&direct_template_pdf_print_requested=1",
    "", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=800,height=700");
}

<? //*************************************
   // PDF 一括印刷 (印刷設定画面)
   //************************************* ?>
function popupPrintSetting(){
  window.open("sot_worksheet_fim.php?session=<? echo $session; ?>&date_y1=<? echo @$date_y; ?>&date_m1=<? echo @$date_m; ?>&date_d1=<? echo @$date_d; ?>&print_preview=y&sel_ptif_id=<? echo $ptif_id; ?>&sel_bldg_cd=<? echo $bldg_cd; ?>&sel_ward_cd=<? echo $ward_cd; ?>",
    "", "directories=no,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no,width=600,height=330");
}

<? //*************************************
   // 編集テンプレートを開く
   //************************************* ?>
function popupFimUpdate(dt, apply_id){
  if (document.getElementById("sel_ptif_id").value == "")
  {
    alert("患者を指定してください。");
    return;
  }
  if (!dt){
    window.open("sum_application_apply_new.php?session=<? echo $session; ?>&tmpl_id=<? echo $tmpl_id; ?>&ptif_id=<? echo $ptif_id; ?>&ymd="+dt,
      "", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=600,height=700");
  } else {
    window.open("sum_application_approve_detail.php?session=<? echo $session; ?>&apply_id="+apply_id+"&exec_mode=apply",
      "", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=980,height=700");
  }
}

</script>
<style type="text/css" media="all">
  .suggest_div { font-size:12px; padding:4px; background-color:#f7ffd2; border:2px solid #ddff3e; color:#777; white-space:nowrap }
  .suggest_result { background-color:#f7ffd2 }
  .suggest_result th { background:0; border:1px solid #ddff3e; padding:2px 6px; background-color:#f7ffd2 }
  .suggest_result td { background:0; border:1px solid #ddff3e; padding:2px 6px; background-color:#f7ffd2 }
</style>

<style type="text/css">
  .listp_table_2 { border-collapse:separate; border-spacing:1px; background-color:#9bc8ec; }
  .listp_table_2 tr { background-color:#fff }
  .listp_table_2 th { padding:3px 1px; font-weight:normal; text-align:left; background-color:#e1effb; }
  .listp_table_2 td { padding:1px; }

  .fimTbl        { table-layout:fixed; }
  .fimTbl td     { padding:3px; }
  .fimTbl td.val { padding:3px; text-align:right; }
  .fimTbl th     { text-align:center; padding:3px; }
  .fimTbl th.vh  { vertical-align:top; }
  .fimTbl th.kei { padding-left:5px; text-align:left ; background-color:#ddd; }
  .fimTbl td.kei { text-align:right; background-color:#ddd; }
</style>

</head>
<body onload="initcal();">

  <?
  $param = "&date_y=".@$date_y."&date_m=".@$date_m."&date_d=".@$date_d."&sel_bldg_cd=".$bldg_cd."&sel_ward_cd=".$ward_cd."&sel_ptrm_room_no=".$ptrm_cd."&sel_team_id=".$sel_team_id;
  summary_common_show_sinryo_top_header($session, $fname, "FIM評価表", $param);
  summary_common_show_main_tab_menu($con, $fname, "看護支援", 1);
?>

<script type="text/javascript">
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
  combobox[1] = document.getElementById("sel_bldg_cd");
  combobox[2] = document.getElementById("sel_ward_cd");
  combobox[3] = document.getElementById("sel_ptrm_room_no");
  if (node < 3) combobox[3].options.length = 1;
  if (node < 2) combobox[2].options.length = 1;

  <? // 初期化用 自分コンボを再選択?>
  var cmb = combobox[node];
  if (node > 0 && cmb.value != selval) {
    for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
  }

  var idx = 1;
  if (node == 1){
    for(var i in ward_selections){
      if (ward_selections[i].bldg_cd != selval) continue;
      combobox[2].options.length = combobox[2].options.length + 1;
      combobox[2].options[idx].text  = ward_selections[i].name;
      combobox[2].options[idx].value = ward_selections[i].code;
      idx++;
    }
  }
  if (node == 2){
    for(var i in room_selections){
      if (room_selections[i].bldg_cd != combobox[1].value) continue;
      if (room_selections[i].ward_cd != selval) continue;
      combobox[3].options.length = combobox[3].options.length + 1;
      combobox[3].options[idx].text  = room_selections[i].name;
      combobox[3].options[idx].value = room_selections[i].code;
      idx++;
    }
  }
}

function searchByDay(day_incriment){
  var yy = document.search.date_y;
  var mm = document.search.date_m;
  var dd = document.search.date_d;
  var y = (parseInt(yy.value,10) ? yy.value : yy.options[1].value);
  var m = (parseInt(mm.value,10) ? mm.value : 1)*1;
  var d = (parseInt(dd.value,10) ? dd.value : 1)*1;
  var dt = new Date(y, m-1, d);
  dt.setTime(dt.getTime() + day_incriment * 86400000);

  var idx_y = yy.options[1].value - dt.getFullYear() + 1;
  if (idx_y < 1) return;
  yy.selectedIndex = idx_y;
  mm.selectedIndex = dt.getMonth()+1;
  dd.selectedIndex = dt.getDate();
document.search.sel_ptif_id.value = "" ;
  document.search.submit();
}

function searchByToday(){
  var dt = new Date();
  document.search.date_y.selectedIndex = document.search.date_y.options[1].value - dt.getFullYear() + 1;
  document.search.date_m.selectedIndex = dt.getMonth()+1;
  document.search.date_d.selectedIndex = dt.getDate();
document.search.sel_ptif_id.value = "" ;
  document.search.submit();
}

</script>
  <?
  $EMPTY_OPTION = '<option value="">　　　　</option>';

  // チーム一覧
  $team_options = array();
  $sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
  while($row = pg_fetch_array($sel)){
    $team_options[] =
      $selected = ($row["team_id"] == $sel_team_id ? " selected" : "");
    $team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
  }

  // 事業所選択肢(部門一覧)
  $bldg_data_options = array();
  $sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
  $mst = (array)pg_fetch_all($sel);
  foreach($mst as $key => $row) {
    $selected = ($row["bldg_cd"] == $bldg_cd ? " selected" : "");
    $bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
  }

  // 病棟選択肢 JavaScript変数として出力する
  $ary = array();
  $sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
  $mst = (array)pg_fetch_all($sel);
  foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
  echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

  // 病室選択肢 JavaScript変数として出力する
  $mst = $c_sot_util->getPatientsRoomInfo();
  $ary = array();
  foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
  echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";
?>

<form name="search" method="get">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="excelout" value="">
<input type="hidden" name="direct_template_pdf_print_requested" value="">
<table class="list_table" style="margin-top:4px; margin-bottom:8px" cellspacing="1">
  <tr>
    <th>事業所(棟)</th>
    <td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
    <th>病棟</th>
    <td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
    <th>病室</th>
    <td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
    <script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
    <script type="text/javascript">sel_changed(2, "<?=$ward_cd?>");</script>
    <script type="text/javascript">sel_changed(3, "<?=$ptrm_cd?>");</script>
    <th bgcolor="#fefcdf">チーム</th>
    <td>
      <select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
    </td>
  </tr>
  <tr>
    <th>日付</th>
    <td colspan="7">
      <div style="float:left">
        <div style="float:left">
          <select id="date_y" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>&nbsp;/
          <select id="date_m" name="date_m"><? show_select_months($date_m, true); ?></select>&nbsp;/
          <select id="date_d" name="date_d"><? show_select_days($date_d, true); ?></select>
        </div>
        <div style="float:left">
          <img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
          <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
        </div>
        <div style="float:left; padding-left:4px">
          <input type="button" onclick="searchByDay('-7');" value="≪" title="１週間前"/><!--
        --><input type="button" onclick="searchByDay('-1');" value="<" title="１日前"/><!--
        --><input type="button" onclick="searchByDay('1');" value=">" title="１日後"/><!--
        --><input type="button" onclick="searchByDay('7');" value="≫" title="１週間後"/>
        </div>
        <div style="float:left;">
          <input type="button" onclick="searchByToday();" value="本日"/>
        </div>
        <div style="clear:both"></div>
      </div>
      <div style="float:right">
        <input type="button" onclick="document.search.sel_ptif_id.value=''; document.search.submit();" value="検索"/>
        <input type="button" onclick="popupPrintSetting();" value="一括印刷"/>
        <input type="button" onclick="popupPrintPreview();" value="印刷" <? if (count($conts) < 1) echo("disabled") ; ?> />
      </div>
      <div style="clear:both"></div>
    </td>
  </tr>
</table>

<? summary_common_show_worksheet_tab_menu("FIM評価表", $param); ?>

<? //=========================================================================?>
<? // 検索指定領域（患者）                                                    ?>
<? //=========================================================================?>
<? if (!array_key_exists("sel_ptif_id", $_REQUEST)) { $ptif_id = "" ; $ptif_name = "" ; } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<table class="list_table" cellspacing="1">
  <tr>
    <th width="50" style="white-space:nowrap; text-align:center">患者ID</th>
    <td width="">
      <div style="float:left">
          <input type="text" style="width:100px; ime-mode:disabled" name="sel_ptif_id" id="sel_ptif_id" value="<?=$ptif_id?>"/><!--
      --><span id="sel_ptif_name" style="padding-left:2px; padding-right:2px"><?=$ptif_name?></span><!--
      --><input type="button" onclick="popupKanjaSelect();" value="患者検索"/><!--
      --><input type="button" onclick="document.search.sel_ptif_id.value=''; document.search.submit();" value="クリア"/>
      </div>
      <? // 入院してれば表示 ?>
      <? $nyuin_prev_next_curr_info = $c_sot_util->get_nyuin_info_with_prev_next($con, $fname, $ptif_id, date("Ymd")); ?>
      <? $tmp_nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $ptif_id, date("Ymd")); ?>

      <? $prev_ptif_id = @$nyuin_prev_next_curr_info["prev"]["ptif_id"]; ?>
      <? if (@$nyuin_prev_next_curr_info["prev"]["ward_cd"] != $tmp_nyuin_info["ward_cd"]) $prev_ptif_id = ""; ?>
      <? $next_ptif_id = @$nyuin_prev_next_curr_info["next"]["ptif_id"]; ?>
      <? if (@$nyuin_prev_next_curr_info["next"]["ward_cd"] != $tmp_nyuin_info["ward_cd"]) $next_ptif_id = ""; ?>
      <div style="float:left; padding:4px;">
        <? if ($prev_ptif_id){ ?>
        <a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$prev_ptif_id?>'; document.search.submit(); return false;">前患者</a>
        <? } else { ?>
        <span style="color:#aaa; white-space:nowrap">前患者</span>
        <? } ?>
      </div>
      <div style="float:left; padding:4px;">
          <? if ($next_ptif_id){ ?>
          <a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$next_ptif_id?>'; document.search.submit(); return false;">次患者</a>
          <? } else { ?>
          <span style="color:#aaa; white-space:nowrap">次患者</span>
          <? } ?>
      </div>
      <div style="clear:both"></div>
    </td>
    <th width="70" style="text-align:center; white-space:nowrap">
      患者名検索
    </th>
    <td width="220" style="text-align:right">
      <div style="float:left">
        <input type="text" style="width:200px" onkeyup="inquirySuggest(this.value, '')" onfocus="inquirySuggest(this.value, 'show');" onblur="inquirySuggest(this.value, 'hide');" /><br />
        <div id="patient_suggest_div" style="position:absolute; width:200px; z-index:1000; display:none;">
          <div id="patient_suggest_script" style="padding-top:4px; text-align:left;"></div>
        </div>
      </div>
      <br style="clear:both" />
    </td>
  </tr>
</table>

</form>

<? //=========================================================================?>
<? // 患者名サジェスト                                                        ?>
<? //=========================================================================?>
<script type="text/javascript">

<?//--------------------------------------------?>
<?// 患者名サジェスト：サーバへ送信             ?>
<?//--------------------------------------------?>
var suggestExistResult = false;
var suggestLinkClicked = false;
var currentSuggestString = "";
function inquirySuggest(ptif_name, disp){
  var curDisp = document.getElementById("patient_suggest_div").style.display;
  var newDisp = (disp=="hide" && !suggestExistResult ? "none" : "");
  if (curDisp != newDisp) {
    document.getElementById("patient_suggest_div").style.display = newDisp;
  }
  if (disp=="hide") {
    currentSuggestString = "";
    return;
  }

  if (ptif_name=="") {
    suggestExistResult = false;
    document.getElementById("patient_suggest_script").innerHTML = '' +
      '<div class="suggest_div">'+
      '・患者名/患者IDを指定してください<br/>'+
      '・指定事業所/病棟範囲内で検索<br/>'+
      '・本日入院中の患者、該当10件まで</div>';
    currentSuggestString = "";
    return;
  }

  if (currentSuggestString == ptif_name) return;
  currentSuggestString = ptif_name;

  var postval =
    "session=<?=$session?>"+
    "&sel_ptif_name="+encodeURIComponent(encodeURIComponent(ptif_name)) +
    "&sel_bldg_cd="+document.search.sel_bldg_cd.value +
    "&sel_ward_cd="+document.search.sel_ward_cd.value;
  if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
  else {
    try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
    catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
  }
  ajaxObj.onreadystatechange = returnInquirySuggest;
  ajaxObj.open("POST", "sot_kanja_suggest.php", true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval);
}
<?//--------------------------------------------?>
<?// 患者名サジェスト：サーバからの応答リスト   ?>
<?//--------------------------------------------?>
function returnInquirySuggest(){
  if (ajaxObj == null) return;
  if (ajaxObj.readyState != 4) return;
  if (typeof(ajaxObj.status) != "number") return;
  if (ajaxObj.status != 200) return;
  if (!ajaxObj.responseText) return;
  try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
  catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
  if (!ret) return;
  var out = [];
  if (!ret.list_length) {
    suggestExistResult = false;
    document.getElementById("patient_suggest_script").innerHTML = "<div class='suggest_div'>（該当はありません）</div>";
    return;
  }
  out.push('<table class="suggest_result">');
  for (var i = 0; i < ret.list_length; i++){
    out.push('<tr><th>'+ret.list[i].id+'</th><td><a href="" class="always_blue" onclick="suggestLinkClick(\''+ret.list[i].id+'\'); return false;">'+ret.list[i].name+'</a></td></tr>');
  }
  out.push("</table>");
  document.getElementById("patient_suggest_script").innerHTML = out.join("");
  suggestExistResult = true;
}

<?//--------------------------------------------?>
<?// 患者名サジェスト：サジェストの患者IDのクリック   ?>
<?//--------------------------------------------?>
function suggestLinkClick(ptif_id){
  suggestLinkClicked = true;
  callbackKanjaSelect(ptif_id);
}

</script>

<?//--------------------------------------------?>
<?// FIM評価表の表示                            ?>
<?//--------------------------------------------?>
  <?
  // HTML表組み時のrowspan値をセットする
  $rowsp = array() ;
  $tagName1 = "fimh_" ;
  foreach ($fimItems as $idx0 => $item0)
  {
    foreach ($item0 as $id1 => $item1)
    {
      if (is_string($item1))
      {
        $tagName2 = $tagName1 . $id1 . "_"   ; // 例：fimh_undou_
        $rowsp[$tagName2] = 0 ;
      }
      else
      {
        foreach ($item1 as $id2 => $item2)
        {
          if (is_string($item2))
          {
            $tagName3 = $tagName2 . $id2 . "_" ; // 例：fimh_undou_selfcare_
          }
          else
          {
            $rowsp[$tagName3] = count($item2) ;
            $rowsp[$tagName2] += $rowsp[$tagName3] ;
          }
        }
      }
    }
    $rowsp[$tagName2]++ ; // 小計行の分
  }

  $CELL_W = array(60, 110, 120, 55) ;
  $tableW = $CELL_W[0] + $CELL_W[1] + $CELL_W[2] + $CELL_W[3] * count($conts) ;

  // 初期表示時は表を表示しないで終了する
  if (!array_key_exists("sel_ptif_id", $_REQUEST) || $_REQUEST["sel_ptif_id"] === "") {
    echo("<div style=\"padding:10px; color:#aaa; text-align:center\">患者を指定してください。</div>");
    die ;
  }

  // ↓ここからHTML出力
?>

<div style="margin-top:12px;"></div>
<!--<div style="width:100%; overflow-x:scroll; margin-top:12px;">-->
<table class="listp_table_2 fimTbl" style="width:<?=$tableW?>px;">
 <tr>
  <th style="width:<?=$CELL_W[0]?>px;">項目</th><th style="width:<?=$CELL_W[1]?>px;">分類</th><th style="width:<?=$CELL_W[2]?>px;">評価項目</th>
  <?
  foreach ($conts as $key => $val)
  {
    $dtStr = @substr($c_sot_util->slash_ymd($key), 5) ;
    if (strcmp($dtStr, $dtstr_avl))
    {
?>
  <th style="width:<?=$CELL_W[3]?>px;"><a href="" onclick="popupFimUpdate('<?=$key?>','<?=$val["apply_id"]?>'); return false;" class="always_blue"><?=$dtStr?></a></th>
  <?
    } else {
?>
  <th style="width:<?=$CELL_W[3]?>px;">平均</th>
    <?
    }
  }
?>
  <th style="width:<?=$CELL_W[3]?>px;"><a href="" onclick="popupFimUpdate('',''); return false;" class="always_blue">追加</a></th>
 </tr>
  <?
  $tagName1 = "fimh_" ;
  foreach ($fimItems as $idx0 => $item0)
  {
    foreach ($item0 as $id1 => $item1)
    {
      if (is_string($item1))
      {
        $tagName2 = $tagName1 . $id1 . "_"   ; // 例：fimh_undou_
?>
  <tr><th class="vh" rowspan="<?=$rowsp[$tagName2]?>"><?=$item1?>項目</th>
        <?
      }
      else
      {
        $idx2 = 0 ;
        foreach ($item1 as $id2 => $item2)
        {
          $tr2 = ($idx2 == 0) ? "" : "<tr>" ;
          if (is_string($item2))
          {
            $tagName3 = $tagName2 . $id2 . "_" ; // 例：fimh_undou_selfcare_
?>
  <?=$tr2?><th class="vh" rowspan="<?=$rowsp[$tagName3]?>"><?=$item2?></th>
            <?
          }
          else
          {
            $idx3 = 0 ;
            foreach ($item2 as $id3 => $item3)
            {
              $tr3 = ($idx3 == 0) ? "" : "<tr>" ;
              $tagName4 = $tagName3 . $id3     ; // 例：fimh_undou_selfcare_meal
?>
  <?=$tr3?>
   <td><?=$item3?></td>
              <?
              // 評価値表示ループ
              foreach ($conts as $key => $val)
              {
?>
  <td class="val"><?=$val[$tagName4]?></td>
              <?
              }
?>
  <td></td>
  </tr>
              <?
              $idx3++ ;
            }
            $idx2++ ;
          }
        }
      }
    }
?>
  <tr><th class="kei" colspan="2">小計</th>
    <?
    // 小計表示ループ
    foreach ($conts as $key => $val)
    {
?>
  <td class="kei"><?=$val[$tagName2 . "subtotal"]?></td>
    <?
    }
?>
  <td class="kei"></td>
  </tr>
    <?
  }
?>
  <tr><th class="kei" colspan="3">合計</th>
  <?
  // 合計表示ループ
  foreach ($conts as $key => $val)
  {
?>
  <td class="kei"><?=$val["fimh_total"]?></td>
  <?
  }
?>
  <td class="kei"></td>
  </tr>
</table>
<!--<div style="margin-top:12px;"></div>
</div>-->

</body>
<? pg_close($con); ?>
</html>

<? die; } else { // if (!$_REQUEST["print_preview"]) END ?>
<?
// *****************************************************************************
// *****************************************************************************
// 一括印刷画面
// *****************************************************************************
// *****************************************************************************
$THIS_WORKSHEET_ID = 1;
//ob_start();
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");
//ob_end_clean();

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname); // 57:メドレポート
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}
//$emp_id = get_emp_id($con, $session, $fname);//ログイン職員ＩＤ

//=================================================
// 検索対象日付がない場合は本日とする
//=================================================
$date_y1 = (int)(@$_REQUEST["date_y1"] ? $_REQUEST["date_y1"] : date("Y"));
$date_m1 = (int)(@$_REQUEST["date_m1"] ? $_REQUEST["date_m1"] : date("m"));
$date_d1 = (int)(@$_REQUEST["date_d1"] ? $_REQUEST["date_d1"] : date("d"));
if (!checkdate($date_m1, $date_d1, $date_y1)) list($date_y1, $date_m1, $date_d1) = split("/", date("Y/m/d", strtotime("today")));
$from_yyyymmdd = $date_y1. ((int)@$date_m1 <10 ? "0".(int)@$date_m1 : $date_m1) . ((int)@$date_d1 <10 ? "0".(int)@$date_d1 : $date_d1);

$date_y = (int)(@$_REQUEST["date_y"] ? $_REQUEST["date_y"] : date("Y"));
$date_m = (int)(@$_REQUEST["date_m"] ? $_REQUEST["date_m"] : date("m"));
$date_d = (int)(@$_REQUEST["date_d"] ? $_REQUEST["date_d"] : date("d"));
if (!checkdate($date_m, $date_d, $date_y)) list($date_y, $date_m, $date_d) = split("/", date("Y/m/d", strtotime("today")));
$to_yyyymmdd = $date_y. ((int)@$date_m <10 ? "0".(int)@$date_m : $date_m) . ((int)@$date_d <10 ? "0".(int)@$date_d : $date_d);

$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'";
$sel = select_from_table($con, $sql, "", $fname);
$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));

if (!$sel_bldg_cd) $sel_bldg_cd = 1;
if (!$sel_ward_cd)
{
  $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["sel_ptif_id"], $from_yyyymmdd);
  $ptif_ward_cd = @$nyuin_info["ward_cd"];
  $sel_ward_cd = $ptif_ward_cd ;
//  $ward_name = @$nyuin_info["ward_name"];
}

$sel_team_id = (int)@$_REQUEST["sel_team_id"]; // ←未使用

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix FIM評価表印刷</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css" />
<? require_once("yui_calendar_util.ini"); ?>
<? write_yui_calendar_use_file_read_0_12_2(); ?>
<? write_yui_calendar_script2(2); ?>
<?
  $bldg_list = array();
  $bldg_data_options = array();
  $bldg_data_options[] = $EMPTY_OPTION;
  $ward_list = array();
  $ward_data_options = array();
  $ward_data_options[] = $EMPTY_OPTION;

  // 事業所と病棟選択肢
  $sql =
    " select bldg.bldg_name, bldg.bldg_cd, wd.ward_cd, wd.ward_name from wdmst wd".
    " inner join bldgmst bldg on (bldg.bldg_cd = wd.bldg_cd)".
    " where wd.ward_del_flg = 'f'".
    " and bldg.bldg_del_flg = 'f'".
    " order by bldg.bldg_cd, wd.ward_cd";
  $sel = $c_sot_util->select_from_table($sql);
  while($row = pg_fetch_array($sel)){
    $selected1 = ($row["bldg_cd"] == $sel_bldg_cd ? " selected" : "");
    if (!@$bldg_list[$row["bldg_cd"]]){
      $bldg_list[$row["bldg_cd"]] = $row["bldg_name"];
      $bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected1.'>'.$row["bldg_name"].'</option>';
    }

    $ward_list[$row["bldg_cd"]][$row["ward_cd"]] = $row["ward_name"];
    if ($selected1){
      $selected2 = ($row["ward_cd"] == $sel_ward_cd ? " selected" : "");
      $ward_data_options[] = '<option value="'.$row["ward_cd"].'"'.$selected2.'>'.$row["ward_name"].'</option>';
    }
  }
?>
<script type="text/javascript">
  var bldg_list = {};
<? foreach ($bldg_list as $bldg_cd => $bldg_name){ ?>
    bldg_list["<?=$bldg_cd?>"] = [];
  <? foreach ($ward_list[$bldg_cd] as $ward_cd => $ward_name){ ?>
      bldg_list["<?=$bldg_cd?>"].push({"ward_cd":"<?=$ward_cd?>","ward_name":"<?=str_replace('"','\"',$ward_name)?>"});
  <? } ?>
<? } ?>
  function bldgChanged(bldg_id){
    var dropdown = document.getElementById("sel_ward_cd");
    dropdown.options.length = bldg_list[bldg_id].length + 1;
    for (var i = 0; i < bldg_list[bldg_id].length; i++){
      dropdown.options[i+1].value = bldg_list[bldg_id][i].ward_cd;
      dropdown.options[i+1].text  = bldg_list[bldg_id][i].ward_name;
    }
    dropdown.selectedIndex = 0;
  }

  function popupKanjaSelect(){
    window.open(
      "sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=1&sel_ward_cd=<?=@$ptif_ward_cd?>&sel_ptif_id=<?=@$sel_ptif_id?>&sel_sort_by_byoto=1",
      "kanjaSelectWindow",
      "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height="+window.screen.availHeight+",left=0,top=0"
    );
  }
  function startPrint(){
    var wardidx = document.getElementById("sel_ward_cd").options.selectedIndex;
    var wardcd = document.getElementById("sel_ward_cd").options[wardidx].value;
    if (wardcd=="" && document.getElementById("sel_ptif_id").value=="") {
      alert("病棟または患者を指定してください。");
      return;
    }
    if (!confirm("印刷PDF作成処理を開始します。\nこの処理には時間がかかる場合があります。\n\nPDF作成処理を開始してよろしいですか？")) return;
    document.search.submit();
  }

  function callbackKanjaSelect(ptif_id, ptif_name){
    document.getElementById("sel_ptif_id").value = ptif_id;
    document.getElementById("sel_ptif_name").value = ptif_name;
  }
  function clearKanja(){
    document.getElementById("sel_ptif_id").value = "";
    document.getElementById("sel_ptif_name").value = "";
  }
</script>
</head>
<body onload="initcal();">
<center>
<div style="width:590px; margin-top:4px">

<!-- タイトルヘッダ -->
<?= summary_common_show_dialog_header("FIM評価表印刷"); ?>

<!-- 検索条件エリア -->
<form name="search" method="get">
<input type="hidden" name="direct_template_pdf_print_requested" value="1">
<input type="hidden" name="session" value="<?= $session ?>">
<input type="submit" value="dummy" style="display:none"/>
<div class="dialog_searchfield">
  <table style="width:100%">
    <tr>
      <td style="white-space:nowrap">
        <div style="margin-top:4px;">事業所(棟)</div>
      </td>
      <td>
        <div style="float:left; margin-top:2px">
          <select onchange="bldgChanged(this.value);" name="sel_bldg_cd"><?=implode("\n", $bldg_data_options)?></select>
        </div>
        <div style="float:left; margin-top:4px; margin-left:4px">病棟</div>
        <div style="float:left; margin-top:2px; margin-left:4px">
          <select name="sel_ward_cd" id="sel_ward_cd"><option value=""></option><?=implode("\n", $ward_data_options)?></select>
        </div>
        <div style="clear:both"></div>
      </td>
      <td></td>
    </tr>
    <tr>
      <td style="white-space:nowrap">
        <div style="margin-top:4px;">期間</div>
      </td>
      <td>
        <table>
        <tr>
          <td>
            <select id="date_y1" name="date_y1"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y1, false); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select>
          </td>
          <td>
            <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
              <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
          </td>
          <td>
            &nbsp;〜<select id="date_y" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>/<select id="date_m" name="date_m"><? show_select_months($date_m, true); ?></select>/<select id="date_d" name="date_d"><? show_select_days($date_d, true); ?></select>
          </td>
          <td>
            <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
              <div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
          </td>
        </tr>
        </table>
      </td>
      <td></td>
    </tr>
    <tr>
      <td style="width:10%">患者ID</td>
      <td>
        <div style="float:left">
          <input type="text" id="sel_ptif_id" name="sel_ptif_id" style="width:90px; ime-mode:active; background-color:#d9edfd; border:1px solid #aaa; padding:2px" value="<?=@$_REQUEST["sel_ptif_id"]?>" readonly="readonly"/><!--
      --><input type="text" id="sel_ptif_name" name="sel_ptif_name" style="width:120px; ime-mode:active; background-color:#d9edfd; border:1px solid #aaa; padding:2px" value="<?=$ptif_name?>" readonly="readonly" /><!--
      --><input type="button" onclick="popupKanjaSelect();" value="患者検索"/><input type="button" onclick="clearKanja();" value="患者クリア"/>
        </div>
        <div style="clear:both"></div>
      </td>
      <td style="text-align:right; padding-right:8px">
      </td>
    </tr>
    <tr>
      <td colspan="3">
      <div style="background-color:#dbeefd; border:1px solid #aaa; margin:4px; padding:4px">
      ※ 病棟または患者を指定してください。<br/>
      ※ 患者を指定しない場合は、期間内・指定病棟に入院していた患者情報が印刷されます。<br/>
      ※ 患者を指定した場合、指定病棟情報は無視されます。<br/>
      <br/>
      ※ 印刷用PDF作成には時間がかかる場合があります。<br/>
      　 PDF作成処理完了後は、ダウンロードダイアログが表示されます。<br/>
      </div>
      </td>
    </tr>
    <tr>
      <td colspan="3" style="text-align:right; padding-right:8px">
        <input type="button" onclick="startPrint();" value="印刷開始"/>
      </td>
    </tr>
  </table>
</div>
</form>
</div>
</center>
</body>
</html>
<? } ?>
<? ob_end_flush() ; ?>

<? //-------------------------------------------------------------------- ?>
<? // PDF印刷スクリプトを出力する                                         ?>
<? //-------------------------------------------------------------------- ?>
<?
function exec_pdf_print($conts, $ikkatu)
{
 global $fimItems   ;
 global $c_sot_util ;

  class CustomMBFPDF extends MBFPDF
  {
    var $MOST_LEFT  =  10 ;
    var $MOST_RIGHT = 287 ;
    var $MOST_TOP   =  12 ;
    var $MOST_BTM   = 198 ;

    var $PAT_HD_Y = 25 ; // 患者情報行のY位置
    var $CNT_HD_Y = 32 ; // 内容ヘッダ患者情報行のY位置

    var $CNT_CELL_W = array(18, 35, 39, 12) ;

    var $CNT_LINE_H = 6.8 ;

    var $MAX_CNT_CT = 15 ;

    var $fim_items ;
    var $sotUtil   ;

    var $currPtId ;
    var $currPtDt ;

    var $prevIdx ;
    var $nextIdx ;

    var $currY ;

    var $dtstr_avl = "99/99" ;  // 平均点  例：12/06 → 99/99

    function CustomMBFPDF($fimItems_, $c_sot_util_)
    {
      $this->fim_items = $fimItems_   ;
      $this->sotUtil   = $c_sot_util_ ;
      $this->MBFPDF("L", "mm", "A4") ; // A4横：297 x 210
      $this->SetAutoPageBreak(false) ;
      $this->AddMBFont(GOTHIC, "EUC-JP") ;
      $this->SetFont(GOTHIC, '', 10) ;
      $this->SetMargins($this->MOST_LEFT, $this->MOST_TOP) ;
    }

    function writeTitleHeader()
    {
      $this->setXY($this->MOST_LEFT, $this->MOST_TOP) ;
      $this->SetFontSize(15) ;
      $this->Cell(0, 5.0, "FIM評価表", "", 1, "C") ;
      $this->setY($this->MOST_TOP + 7) ;
      $this->SetFontSize(10) ;
      $this->Cell(0, 5.0, date("Y/m/d H:i:s"). "　　", "", 0, "R") ;
    }

    function writePtHeader()
    {
      $this->setXY($this->MOST_LEFT, $this->PAT_HD_Y) ;
      $this->SetFontSize(10) ;
      $ptIf = $this->currPtDt["pt_info"] ;
      $this->Cell(0, 5.0, "病棟/病室：" . $ptIf["ward_name"] . "　" . $ptIf["ptrm_name"] . "　　患者：" . $this->currPtId . "　" . $ptIf["inpt_kj_nm"], "", 0, "L") ;
    }

    function writeContsHeader()
    {
      $this->SetFillColor(210) ;
      $this->SetXY($this->MOST_LEFT, $this->CNT_HD_Y) ;
      $this->Cell($this->CNT_CELL_W[0], $this->CNT_LINE_H, "項目",     "LT",  0, "C", 1) ;
      $this->Cell($this->CNT_CELL_W[1], $this->CNT_LINE_H, "分類",     "LT",  0, "C", 1) ;
      $this->Cell($this->CNT_CELL_W[2], $this->CNT_LINE_H, "評価項目", "LTR", 0, "C", 1) ;
      $idx = 0 ;
      $cnt = 0 ;
      foreach ($this->currPtDt as $date => $ptDt)
      {
        if ($cnt == $this->MAX_CNT_CT) break ;
        if ($idx >= $this->nextIdx)
        {
          $dtStr = @substr($this->sotUtil->slash_ymd($date), 5) ;
          if (strcmp($dtStr, $this->dtstr_avl))
          {
            $this->Cell($this->CNT_CELL_W[3], $this->CNT_LINE_H, $dtStr, "LTR", 0, "C", 1) ;
          } else {
            $this->Cell($this->CNT_CELL_W[3], $this->CNT_LINE_H, "平均", "LTR", 0, "C", 1) ;
          }
          $cnt++ ;
        }
        $idx++ ;
      }
      $this->prevIdx = $this->nextIdx ;
      $this->nextIdx = $idx ;
      $this->Cell($this->CNT_CELL_W[3], $this->CNT_LINE_H, " ", "", 1) ;
      $this->currY = $this->getY() ;
      return ($this->nextIdx < count($this->currPtDt)) ;
    }

    function writeConts()
    {
      $isContinued = true ;
      while ($isContinued)
      {
        $this->writePtHeader() ;
        $isContinued = $this->writeContsHeader() ;
        $this->writeContsPage() ;
        if ($isContinued) $this->AddPage() ;
      }
    }

    function writeContsPage()
    {
      $this->SetFillColor(210) ;
      $tempY1 = $this->currY ;
      $tempY2 = $this->currY ;
      $this->SetXY($this->MOST_LEFT, $this->currY) ;
      $tagName1 = "fimh_" ;
      foreach ($this->fim_items as $idx0 => $item0)
      {
        foreach ($item0 as $id1 => $item1)
        {
          if (is_string($item1))
          {
            $tagName2 = $tagName1 . $id1 . "_" ; // 例：fimh_undou_
            $this->Cell($this->CNT_CELL_W[0], $this->CNT_LINE_H, $item1 . "項目", "", 0, "L") ;
          }
          else
          {
            foreach ($item1 as $id2 => $item2)
            {
              if (is_string($item2))
              {
                $tagName3 = $tagName2 . $id2 . "_" ; // 例：fimh_undou_selfcare_
                $this->SetX($this->MOST_LEFT + $this->CNT_CELL_W[0]) ;
                $this->Cell($this->CNT_CELL_W[1], $this->CNT_LINE_H, $item2, "", 0, "L") ;
              }
              else
              {
                foreach ($item2 as $id3 => $item3)
                {
                  $tagName4 = $tagName3 . $id3 ; // 例：fimh_undou_selfcare_meal
                  $this->SetX($this->MOST_LEFT + $this->CNT_CELL_W[0] + $this->CNT_CELL_W[1]) ;
                  $this->Cell($this->CNT_CELL_W[2], $this->CNT_LINE_H, $item3, "TR", 0, "L") ;
                  $this->writeContsData($tagName4) ;
                }
                $shdrh2 = $this->getY() - $tempY2 ;
                $this->SetXY($this->MOST_LEFT + $this->CNT_CELL_W[0], $tempY2) ;
                $this->Cell($this->CNT_CELL_W[1], $shdrh2, "", "TR", 1) ;
                $tempY2 = $this->getY() ;
              }
            }
          }
        }
        $this->SetX($this->MOST_LEFT + $this->CNT_CELL_W[0]) ;
        $this->Cell($this->CNT_CELL_W[1] + $this->CNT_CELL_W[2], $this->CNT_LINE_H, "小計", "T", 0, "L", 1) ;
        $this->writeContsData($tagName2 . "subtotal", 1) ;
        $tempY2 = $this->getY() ;
        $shdrh1 = $tempY2 - $tempY1 ;
        $this->SetXY($this->MOST_LEFT, $tempY1) ;
        $this->Cell($this->CNT_CELL_W[0], $shdrh1, "", "LTR", 1) ;
        $tempY1 = $this->getY() ;
      }
      $this->SetX($this->MOST_LEFT) ;
      $this->Cell($this->CNT_CELL_W[0] + $this->CNT_CELL_W[1] + $this->CNT_CELL_W[2], $this->CNT_LINE_H, "合計点", "LTB", 0, "L", 1) ;
      $this->writeContsData("fimh_total", 1, "1") ;
    }

    function writeContsData($tagName, $fill = 0, $border = "LTR") // 例：fimh_undou_selfcare_meal
    {
      $this->SetFillColor(210) ;
      $idx = 0 ;
      $cnt = 0 ;
      foreach ($this->currPtDt as $date => $ptDt)
      {
        if ($cnt == $this->MAX_CNT_CT) break ;
        if ($idx >= $this->prevIdx)
        {
          $this->Cell($this->CNT_CELL_W[3], $this->CNT_LINE_H, $ptDt[$tagName], $border, 0, "R", $fill) ;
          $cnt++ ;
        }
        $idx++ ;
      }
      $this->Cell($this->CNT_CELL_W[3], $this->CNT_LINE_H, " ", "", 1) ;
    }

  }

  $pdf = new CustomMBFPDF($fimItems, $c_sot_util) ;
  $pdf->Open() ;

  foreach ($conts as $ptId => $ptDt)
  {
    $pdf->currPtId = $ptId ;
    $pdf->currPtDt = $ptDt ;
    $pdf->nextIdx  = 1 ; // データ配列の先頭は"pt_info"なのでとばす
    $pdf->AddPage() ;
    $pdf->writeTitleHeader() ;
    $pdf->writeConts() ;
  }

  ob_end_clean() ;
  if ($ikkatu) $pdf->Output("fim".date("Ymd_His").".pdf", "D");
  else         $pdf->Output();

}

/**
 * 評価対象日が表示対象として有効であるか判定する
 * 
 * @param string $from_yyyymmdd 指定期間開始日
 * @param string $to_yyyymmdd   指定期間終了日
 * @param string $hyouka_ymd    評価対象日
 * @param string $inpt_in_dt    入院日
 * 
 * @return boolean true:有効/false:無効
 */
function isValidDate($from_yyyymmdd, $to_yyyymmdd, $hyouka_ymd, $inpt_in_dt) {
    if ($from_yyyymmdd === "00000000") {     // 一括印刷以外
        if ($hyouka_ymd >= $inpt_in_dt) {
            // 直近の1入院期間のみ表示対象
            return true;
        } else {
            return false;
        }
    } else {    // 一括印刷
        if (($from_yyyymmdd <= $hyouka_ymd) && ($hyouka_ymd <= $to_yyyymmdd)) {
            // 指定期間内であれば表示対象
            return true;
        } else {
            return false;
        }
    }
}
?>
