<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
ob_start();
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("show_select_values.ini");
require_once("sum_exist_workflow_check_exec.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("./get_values.ini");
ob_end_clean();


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}


// 入力チェック
// 年月日を全て指定、月だけの指定の場合のみOKとする
if (@$action == "検索") {
	$ok_flg = false;
	$start_ymd_len = strlen($start_yr.$start_mon.$start_day);
	$end_ymd_len = strlen($end_yr.$end_mon.$start_day);
	// 年月日確認、未指定は"-"
	if (($start_ymd_len == 8 && $end_ymd_len == 8) ||
		($start_ymd_len == 8 && $end_ymd_len == 3) ||
		($start_ymd_len == 3 && $end_ymd_len == 8)) {
		$ok_flg = true;
	}
	// 月のみ
	if ($start_yr == "-" && $start_day == "-" && $end_yr == "-" && $end_day == "-") {
		if (($strat_mon != "-" && $end_mon != "-") ||
			($strat_mon != "-" && $end_mon == "-") ||
			($strat_mon == "-" && $end_mon != "-")) {
			$ok_flg = true;
		}
	}
	if ($ok_flg == false) {
		echo("<script type=\"text/javascript\">alert('期間指定は年月日をともに指定するか、月のみを指定してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}


//利用者IDを取得
$emp_id = get_emp_id($con, $session, $fname);

summary_common_write_operate_log("access", $con, $fname, $session, "", "", "");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

// 病床検索/居室検索
$bed_search_title = $_label_by_profile["BED_SEARCH"][$profile_type];

// 期間
if (@$start_yr == "")  $start_yr = date('Y');
if (@$start_mon == "") $start_mon = date('m');
if (@$start_day == "") $start_day = date('d');
if (@$end_yr == "")    $end_yr = date('Y');
if (@$end_mon == "")   $end_mon = date('m');
if (@$end_day == "")   $end_day = date('d');



?>
<script type="text/javascript">
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
	combobox[1] = document.getElementById("sel_bldg_cd");
	combobox[2] = document.getElementById("sel_ward_cd");
	combobox[3] = document.getElementById("sel_ptrm_room_no");
	if (node < 3) combobox[3].options.length = 1;
	if (node < 2) combobox[2].options.length = 1;

	<? // 初期化用 自分コンボを再選択?>
	var cmb = combobox[node];
	if (node > 0 && cmb.value != selval) {
		for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
	}

	var idx = 1;
	if (node == 1){
		for(var i in ward_selections){
			if (ward_selections[i].bldg_cd != selval) continue;
			combobox[2].options.length = combobox[2].options.length + 1;
			combobox[2].options[idx].text  = ward_selections[i].name;
			combobox[2].options[idx].value = ward_selections[i].code;
			idx++;
		}
	}
	if (node == 2){
		for(var i in room_selections){
			if (room_selections[i].bldg_cd != combobox[1].value) continue;
			if (room_selections[i].ward_cd != selval) continue;
			combobox[3].options.length = combobox[3].options.length + 1;
			combobox[3].options[idx].text  = room_selections[i].name;
			combobox[3].options[idx].value = room_selections[i].code;
			idx++;
		}
	}
}
<? //++++++++++++++++++++++++++++++++++++++?>
<? // 患者検索ダイアログのポップアップ     ?>
<? //++++++++++++++++++++++++++++++++++++++?>
function popupKanjaSelect(){
	window.open(
		"sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=1&sel_ward_cd=<?=@$ptif_ward_cd?>&sel_ptif_id=<?=@$sel_ptif_id?>&sel_sort_by_byoto=1",
		"",
		"directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height=480"
	);
}

<? //++++++++++++++++++++++++++++++++++++++?>
<? // 患者検索結果を受けるコールバック関数 ?>
<? //++++++++++++++++++++++++++++++++++++++?>
function callbackKanjaSelect(ptif_id){
	document.getElementById("sel_ptif_id").value = ptif_id;
	document.mainform.submit();
}

</script>
<?
//=================================================
// 病棟などが指定されていなければデフォルトを設定する
//=================================================
if (@$action != "検索" && @$sel_bldg_cd=="" && @$sel_ward_cd==""){
	$sql =
		" select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
		" inner join empmst emp on (".
		"   emp.emp_class = swer.class_id".
		"   and emp.emp_attribute = swer.atrb_id".
		"   and emp.emp_dept = swer.dept_id".
		"   and emp.emp_id = '".pg_escape_string($emp_id)."'".
		" )";
	$emp_ward = @$c_sot_util->select_from_table($sql);
	$_REQUEST["sel_bldg_cd"] = @pg_fetch_result($emp_ward, 0, "bldg_cd");
	$_REQUEST["sel_ward_cd"] = @pg_fetch_result($emp_ward, 0, "ward_cd");
	$sel_bldg_cd= $_REQUEST["sel_bldg_cd"];
	$sel_ward_cd= $_REQUEST["sel_ward_cd"];
}
$sel_team_id = (int)@$_REQUEST["sel_team_id"];

$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'";
$sel = select_from_table($con, $sql, "", $fname);
$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));

$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["sel_ptif_id"], $target_yyyymmdd);
$ptif_ward_cd = @$nyuin_info["ward_cd"];
$ward_name = @$nyuin_info["ward_name"];


$EMPTY_OPTION = '<option value="">　　　　</option>';

// チーム一覧
$team_options = array();
$sel2 = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel2)){
	$team_options[] =
	$selected = ($row["team_id"] == @$_REQUEST["sel_team_id"] ? " selected" : "");
	$team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}

// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel2 = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel2);
foreach($mst as $key => $row) {
	$selected = ($row["bldg_cd"] == @$_REQUEST["sel_bldg_cd"] ? " selected" : "");
	$bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel2 = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel2);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$ary = array();
$sel2 = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ptrm_room_no, ptrm_name from ptrmmst where ptrm_del_flg = 'f' order by ptrm_room_no");
$mst = (array)pg_fetch_all($sel2);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";




// 検索
if (@$action == "検索") {

	// 件数取得
	$sql =
		" select".
		" summary.*, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_job".
		",jobmst.job_nm, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, tmplmst.tmpl_name".
		",sum_apply.apply_id".
		" from summary".
		" left join empmst on empmst.emp_id = summary.emp_id ".
		" left join jobmst on jobmst.job_id = empmst.emp_job ".
		" left join ptifmst on ptifmst.ptif_id = summary.ptif_id ".
		" left join tmplmst on tmplmst.tmpl_id = summary.tmpl_id ".
		" left join divmst on divmst.diag_div = summary.diag_div ".
		" left join inptmst on inptmst.ptif_id = summary.ptif_id ".
		" left join sum_apply on (sum_apply.summary_id = summary.summary_id and sum_apply.ptif_id = summary.ptif_id and sum_apply.delete_flg = 'f')".
		" left join sot_ward_team_relation t on (".
		"   t.bldg_cd = inptmst.bldg_cd and t.ward_cd = inptmst.ward_cd and t.ptrm_room_no = inptmst.ptrm_room_no".
		"   and t.team_id = " . $sel_team_id.
		" )".
		" where 1=1";

	if ($sel_diag != "")     $sql .= " and divmst.div_id = '".pg_escape_string($sel_diag)."'"; // 診療記録区分
	if ($tmpl_id=="-")       $sql .= " and summary.tmpl_id is null"; // テンプレート
	else if ($tmpl_id!="")   $sql .= " and summary.tmpl_id = '".pg_escape_string($tmpl_id)."'"; // テンプレート
	if ($recorder_id != "")  $sql .= " and summary.emp_id = '".pg_escape_string($recorder_id)."'"; // 記載者
	if ($start_yr!="-" && $start_mon!="-" && $start_day!="-") {
		$sql .= " and summary.cre_date >= '".pg_escape_string($start_yr.$start_mon.$start_day)."'";// 開始年月日
	}
	if ($end_yr!="-" && $end_mon!="-" && $end_day!="-") {
		$sql .= " and summary.cre_date <= '".pg_escape_string($end_yr.$end_mon.$end_day)."'"; // 終了年月日
	}
	if ($start_yr == "-" && $start_mon != "-" && $start_day == "-") {
		$sql .= " and substring(summary.cre_date from 5 for 2) >= '$start_mon'";// 開始月
	}
	if ($end_yr == "-" && $end_mon != "-" && $end_day == "-") {
		$sql .= "(substring(summary.cre_date from 5 for 2) <= '$end_mon')";// 終了月
	}
	if ($search_str != "") {// 記載内容
		$sql .= " and (1=0 ";
		$arr_key = split(" ", $search_str);
		for ($i=0; $i<count($arr_key); $i++) $sql .= " or smry_cmt like '%".pg_escape_string($arr_key[$i])."%'";
		$sql .= ")";
	}

	if ($_REQUEST["sel_ptif_id"]) $sql .= " and summary.ptif_id = '".pg_escape_string($_REQUEST["sel_ptif_id"])."'"; // 患者
	if ($_REQUEST["sel_bldg_cd"]) $sql .= " and inptmst.bldg_cd = ".(int)$_REQUEST["sel_bldg_cd"]; // 事業所
	if ($_REQUEST["sel_ward_cd"]) $sql .= " and inptmst.ward_cd = ".(int)$_REQUEST["sel_ward_cd"]; // 病棟
	if ($_REQUEST["sel_ptrm_room_no"]) $sql .= " and inptmst.ptrm_room_no = ".(int)$_REQUEST["sel_ptrm_room_no"]; // 病室
	if ($sel_team_id) $sql .= " and t.team_id is not null"; // チーム



	$sel = select_from_table($con, "select count(*) from (".$sql.") d", "", $fname);
	$cnt = pg_fetch_result($sel, 0, 0);

	$sel = select_from_table($con, $sql." order by summary.cre_date desc, summary.summary_id desc limit 100", "", $fname);
	$num = pg_numrows($sel);
}


?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <?=$bed_search_title?></title>
<script type="text/javascript" src="js/popup.js"></script>

<script type="text/javascript">
function initPage() {
	resetWardOptions('<? echo(@$ward_cd); ?>');
}

function show_tmpl_window(pt_id, emp_id, xml_file, tmpl_id, summary_id, enc_diag){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=100,top=100,width=900,height=700";
	var url = 'summary_tmpl_read.php'
			+'?session=<?=$session?>'
			+'&sansho_flg=true'
			+'&pt_id='+pt_id
			+'&emp_id='+emp_id
			+'&xml_file='+xml_file
			+'&tmpl_id='+tmpl_id
			+'&summary_id='+summary_id
			+'&enc_diag='+enc_diag;
	window.open(url, 'tmpl_window',option);
}

function show_naiyo_window(pt_id, emp_id, summary_id){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=100,top=100,width=900,height=700";
	var url = 'summary_naiyo_shosai.php'
			+'?session=<?=$session?>'
			+'&pt_id='+pt_id
			+'&emp_id='+emp_id
			+'&sum_id='+summary_id
	window.open(url, 'tmpl_window',option);
}

function check_all() {
	if (document.mainform.check_all_flg.value == 'off') {
		document.mainform.check_all_flg.value = 'on';
//		document.getElementById("check_all_btn").src = icoSubeteOff.src;
		document.getElementById("check_all_btn").value = "全てOFF";
		tmp_checked = true;
	} else {
		document.mainform.check_all_flg.value = 'off';
//		document.getElementById("check_all_btn").src = icoSubeteOn.src;
		document.getElementById("check_all_btn").value = "全てON";
		tmp_checked = false;
	}
	for (var i = 0, j = document.mainform.elements.length; i < j; i++) {
		if (document.mainform.elements[i].type == 'checkbox') {
			document.mainform.elements[i].checked = tmp_checked;
		}
	}
}

var childwin = null;
function openEmployeeList() {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=@$emp_id?>';
	url += '&item_id=recorder';
	url += '&mode=';
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	childwin.focus();
}

function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// 職員名簿からの選択職員を設定
function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");
	if(emp_ids.length > 1) {
		alert('職員は１名のみ指定してください。');
		return false;
	}

	document.mainform.recorder_id.value = emp_id;
	document.mainform.recorder.value = emp_name;

}

// CSV出力
function execOutput(select_flg, csv_or_report) {

	var ids = "";
	var first_flg = true;
	for (var i = 0, j = document.mainform.elements.length; i < j; i++) {
		if (document.mainform.elements[i].type == 'checkbox') {
			if (document.mainform.elements[i].checked) {
				if (first_flg == false) {
					ids += ",";
				}
				first_flg = false;
				ids += document.mainform.elements[i].value;
			}
		}
	}

	document.csv.is_print_history.value = (document.getElementById("is_print_history").checked ? "1" : "");


	// 条件設定
	document.csv.ids.value = ids;
	document.csv.select_flg.value = select_flg;
	document.csv.sel_diag.value = document.mainform.sel_diag.value;
	document.csv.tmpl_id.value = document.mainform.tmpl_id.value;
	document.csv.recorder_id.value = document.mainform.recorder_id.value;
	document.csv.start_yr.value = document.mainform.start_yr.value;
	document.csv.start_mon.value = document.mainform.start_mon.value;
	document.csv.start_day.value = document.mainform.start_day.value;
	document.csv.end_yr.value = document.mainform.end_yr.value;
	document.csv.end_mon.value = document.mainform.end_mon.value;
	document.csv.end_day.value = document.mainform.end_day.value;
	document.csv.search_str.value = document.mainform.search_str.value;
	var dt = new Date();
	document.csv.action = (csv_or_report == "csv" ? 'summary_search_data_csv.php' : "summary_pdf_order_dump.php?t="+dt.getMilliseconds());
	document.csv.submit();
}



</script>
<style type="text/css">
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border:#5279a5 solid 1px;}
</style>
</head>
<body>

<? summary_common_show_sinryo_top_header($session, $fname, "複合検索"); ?>

<? summary_common_show_main_tab_menu($con, $fname, "複合検索", $is_workflow_exist); ?>

<form name="mainform" action="summary_search_data.php" method="get">
<input type="hidden" name="action" value="検索"/>
<table class="list_table" cellspacing="1">

<tr>
	<th>事業所(棟)</th>
	<td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
	<th>病棟</th>
	<td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
	<th>病室</th>
	<td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
	<script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
	<script type="text/javascript">sel_changed(2, "<?=@$_REQUEST["sel_ward_cd"]?>");</script>
	<script type="text/javascript">sel_changed(3, "<?=@$_REQUEST["sel_ptrm_room_no"]?>");</script>
	<th bgcolor="#fefcdf">チーム</th>
	<td>
		<select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
	</td>
</tr>



<tr>
	<th width="5%" style="white-space:nowrap">患者ID</th>
	<td colspan="7">
		<div style="float:left">
				<input type="text" style="width:100px; ime-mode:disabled" name="sel_ptif_id" id="sel_ptif_id" value="<?=@$_REQUEST["sel_ptif_id"]?>"/><!--
		--><span id="sel_ptif_name" style="padding-left:2px; padding-right:2px"><?=$ptif_name?></span><!--
		--><input type="button" onclick="popupKanjaSelect();" value="患者検索"/>
		</div>
		<? // 入院してれば表示 ?>
		<? $nyuin_prev_next_curr_info = $c_sot_util->get_nyuin_info_with_prev_next($con, $fname, @$_REQUEST["sel_ptif_id"], date("Ymd")); ?>
		<? $prev_ptif_id = @$nyuin_prev_next_curr_info["prev"]["ptif_id"]; ?>
		<? $next_ptif_id = @$nyuin_prev_next_curr_info["next"]["ptif_id"]; ?>
		<div style="float:left; padding:4px;">
			<? if ($prev_ptif_id){ ?>
			<a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$prev_ptif_id?>'; document.search.submit(); return false;">前患者</a>
			<? } else { ?>
			<span style="color:#aaa; white-space:nowrap">前患者</span>
			<? } ?>
		</div>
		<div style="float:left; padding:4px;">
				<? if ($next_ptif_id){ ?>
				<a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$next_ptif_id?>'; document.search.submit(); return false;">次患者</a>
				<? } else { ?>
				<span style="color:#aaa; white-space:nowrap">次患者</span>
				<? } ?>
		</div>
		<div style="clear:both"></div>
	</td>
</tr>











<tr>
<th width="11%" style="white-space:nowrap"><?=$_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type] ?></th>
<td>
<?
	// 操作者の職種を取得
	$sql = "select emp_job from empmst where emp_id = '".pg_escape_string($emp_id)."'";
	$sel_emp = select_from_table($con,$sql,"",$fname);
	$emp_job_id = pg_fetch_result($sel_emp, 0, "emp_job");
	// 診療記録区分の一覧を取得
	$sql =
		" select * from (".
		"   select *, case when div_sort_order = '' then null else div_sort_order end as sort_order from divmst".
		") d".
		" where div_del_flg = 'f' and (div_hidden_flg is null or div_hidden_flg <> 1) order by sort_order, div_id";
	$sel_divmst = select_from_table($con,$sql,"",$fname);
	$opts = array();
	while($row = pg_fetch_array($sel_divmst)){
		$div_display_auth_stid_list = explode(",", $row["div_modify_auth_stid_list"]);// 参照権限職種カンマリスト
		if ((int)$row["div_modify_auth_flg"]){// 参照権限つきの診療区分か
			if ($emp_job_id == "" || !in_array($emp_job_id, $div_display_auth_stid_list)) continue;
		}
		$selected = (@$sel_diag == $row["div_id"]) ? " selected": "";
		$opts[] = "<option value=\"".$row["div_id"]."\" $selected>".$row["diag_div"]."</option>\n";
	}
?>
	<select id="sel_diag" width="250" name="sel_diag">
		<option value=""></option>
		<?= implode("\n", $opts); ?>
	</select>
</td>
<th style="white-space:nowrap">テンプレート</th>
<td><select name="tmpl_id">
<option value=""></option>
<option value="-" <? if (@$tmpl_id == "-") { echo("selected"); } ?>>フリー入力</option>
<?
	//========================================
	//プルダウンに表示するテンプレート一覧を取得
	//========================================
	$tbl = "select tmpl_id, tmpl_name"
			." from tmplmst";
	$cond = " order by tmpl_id";
	$tmpl_list_sel = select_from_table($con,$tbl,$cond,$fname);
	$tmpl_list_num = pg_numrows($tmpl_list_sel);
	for($itmpl=0;$itmpl<$tmpl_list_num;$itmpl++){
		$select_tmpl_id = pg_result($tmpl_list_sel,$itmpl,"tmpl_id");
		$select_tmpl_name = pg_result($tmpl_list_sel,$itmpl,"tmpl_name");
		$select_tmpl_name_html = h($select_tmpl_name);

		$selected_element = "";
		if($select_tmpl_id == @$tmpl_id) {
			$selected_element = "selected";
		}
		?>
		<option value="<?=$select_tmpl_id?>" <?=$selected_element?> ><?=$select_tmpl_name_html?></option>
		<?
	}
?>
</select>
</td>
<td colspan="4"><table><tr>
<td><input type="button" onclick="openEmployeeList();" value="記載者"/></td>
<td><input type="text" name="recorder" size="20" value="<?=@$recorder?>" readOnly></td>
<td><input type="button" onclick="document.mainform.recorder.value = '';document.mainform.recorder_id.value = '';" value="クリア" /></td>
</tr></table>
</td>
</tr>
<tr>
<th style="white-space:nowrap">期間指定</th>
<td colspan="3"><select id='start_yr' name='start_yr'><?
show_select_years(15, $start_yr, true);
?></select>年
<select id='start_mon' name='start_mon'><?
show_select_months($start_mon, true);
?></select>月
<select id='start_day' name='start_day'><?
show_select_days($start_day, true);
?></select>日
&nbsp;〜&nbsp;
<select id='end_yr' name='end_yr'><?
show_select_years(15, $end_yr, true);
?></select>年
<select id='end_mon' name='end_mon'><?
show_select_months($end_mon, true);
?></select>月
<select id='end_day' name='end_day'><?
show_select_days($end_day, true);
?></select>日
</td>
<th style="white-space:nowrap">記載内容</th>
<td colspan="3"><input type="text" name="search_str" size="30" value="<?=@$search_str?>"></td>
</tr>
</table>

<div class="search_button_div" style="border-bottom:1px solid #fdfdfd"><input type="button" onclick="closeEmployeeList(); document.mainform.submit();" value="検索" /></div>


<? if (@$action == "検索") { ?>
<?   if ($cnt == 0){ ?>
<div style="padding-top:5px">条件に合うデータはありませんでした。</div>
<?   } else { ?>
<div style="background-color:#F4F4F4; padding-top:5px; padding-bottom:5px; border-top:1px solid #d8d8d8">
	<table style="width:100%">
		<tr>
			<td><?=$cnt?>件抽出&nbsp;<?=$num?>件表示</td>
			<td style="text-align:right">
				<label><input type="checkbox" checked="checked" value="1" id="is_print_history">更新履歴も印刷</label>
				<input type="button" onclick="execOutput(0, 'print');" value="印刷" />
				<input type="button" onclick="execOutput(1, 'print');" value="全てを印刷" />
				<input type="button" onclick="execOutput(0, 'csv');" value="CSV出力" />
				<input type="button" onclick="execOutput(1, 'csv');" value="全てをCSV出力" />
			</td>
		</tr>
	</table>
	<div style="clear:both"></div>
</div>




<table class="list_table" cellspacing="1">
	<tr>
		<th width='72' style="text-align:center; padding:0">
			<input type="button" onclick="check_all();" value="全てON" id="check_all_btn" />
			<input type="hidden" name="check_all_flg" value="off">
		</th>
		<th style="text-align:center">作成年月日</th>
		<th style="text-align:center"><?=$_label_by_profile["PATIENT"][$profile_type]// 患者/利用者 ?>ID</th>
		<th style="text-align:center"><?=$_label_by_profile["PATIENT_NAME"][$profile_type]// 患者氏名/利用者氏名 ?></th>
		<th style="text-align:center">プロブレム</th>
		<th style="text-align:center"><?=$_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type]// 診療記録区分/記録区分 ?></th>
		<th style="text-align:center">テンプレート</th>
		<th style="text-align:center">記載者</th>
		<th style="text-align:center">職種</th>
		<th style="text-align:center"><img src="img/repo-jyuuyou.gif" alt="記事重要度"></th>
	</tr>
<?
//========================================
//対象データの表示
//========================================
	for($i=0;$i<$num;$i++){
		$summary_id = pg_result($sel,$i,"summary_id");
		$summary_seq = (int)@pg_result($sel,$i,"summary_seq");
		$apply_id = (int)@pg_result($sel,$i,"apply_id");
		$diag_div = pg_result($sel,$i,"diag_div");
		$problem = pg_result($sel,$i,"problem");
		$smry_cmt = pg_result($sel,$i,"smry_cmt");
		$sum_emp_id = pg_result($sel,$i,"emp_id");
		$cre_date = pg_result($sel,$i,"cre_date");
		$disp_date = substr($cre_date, 0, 4)."/".substr($cre_date, 4, 2)."/".substr($cre_date, 6, 2);
		$kisaisha_id = pg_result($sel,$i,"emp_id");
		$lt_nm = pg_result($sel,$i,"emp_lt_nm");
		$ft_nm = pg_result($sel,$i,"emp_ft_nm");
		$kisaisha = $lt_nm." ".$ft_nm;
		$shokushu_id = pg_result($sel,$i,"emp_job");
		$shokushu = pg_result($sel,$i,"job_nm");
		$priority = pg_result($sel,$i,"priority");

		$ptif_id = pg_result($sel,$i,"ptif_id");
		$ptif_name = pg_result($sel,$i,"ptif_lt_kaj_nm")." ".pg_result($sel,$i,"ptif_ft_kaj_nm");
		$tmpl_id = pg_result($sel,$i,"tmpl_id");
		$tmpl_name = pg_result($sel,$i,"tmpl_name");
		$emp_id = pg_result($sel,$i,"emp_id");
		$enc_diag_div = urlencode($diag_div);

		//HTML用文字変換
		$problem_html = h($problem);
		$diag_div_html = h($diag_div);
		$kisaisha_html = h($kisaisha);
		$shokushu_html = h($shokushu);

		$problem_html_disp = $problem_html;
		if(mb_strlen($problem_html_disp, "EUC-JP")>6) {
			$problem_html_disp = mb_substr($problem_html_disp,0,6, "EUC-JP");
		}
		$diag_div_html_disp = $diag_div_html;
		if(mb_strlen($diag_div_html_disp, "EUC-JP")>6) {
			$diag_div_html_disp = mb_substr($diag_div_html_disp,0,6, "EUC-JP");
		}
		$shokushu_html_disp = $shokushu_html;
	?>
	<tr>
		<td align='center'>
		<input type="checkbox" id="id[]" name="id[]" value="<? echo("{$sum_emp_id}_{$ptif_id}_{$summary_id}"); ?>">
		</td>
		<td align='center'>
		<?
		$xml_file = "{$sum_emp_id}_{$ptif_id}_{$summary_id}.xml";
		if ($summary_seq) {
			$xml_file = $summary_seq .".xml";
			if ($apply_id) $xml_file = $summary_seq ."_".$apply_id.".xml";
		}

		//テンプレート有無により、子画面を変更
		if ($tmpl_id != "") {
			$show_script = "show_tmpl_window('$ptif_id', '$emp_id', '$xml_file', '$tmpl_id', '$summary_id', '$enc_diag_div');";
		} else {
			$show_script = "show_naiyo_window('$ptif_id', '$emp_id', '$summary_id');";
		}
		 ?>
			<a href='javascript:void(0);' style="text-decoration:none" onclick="<?=$show_script?>"><?=$disp_date?></a>
		</td>
		<td align='center'><?=$ptif_id?></td>
		<td align='center'><?=$ptif_name?></td>
		<td align='center'><?=$problem_html_disp?></td>
		<td align='center'><?=$diag_div_html_disp?></td>
		<td align='center'><?=$tmpl_name?></td>
		<td align='center'><?=$kisaisha_html?></td>
		<td align='center'><?=$shokushu_html_disp?></td>
		<td align='center'><?=$priority?></td>
	</tr>
<? } ?>
	</table>

<? } ?>
<? } ?>
<input type="hidden" name="session" value="<? echo $session ?>">
<input type="hidden" name="recorder_id" value="<? echo @$recorder_id ?>">
</form>

<form name="csv" method="get" target="_blank">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="ids" value="">
<input type="hidden" name="select_flg" value="">
<input type="hidden" name="sel_diag" value="">
<input type="hidden" name="tmpl_id" value="">
<input type="hidden" name="recorder_id" value="">
<input type="hidden" name="start_yr" value="">
<input type="hidden" name="start_mon" value="">
<input type="hidden" name="start_day" value="">
<input type="hidden" name="end_yr" value="">
<input type="hidden" name="end_mon" value="">
<input type="hidden" name="end_day" value="">
<input type="hidden" name="search_str" value="">
<input type="hidden" name="is_print_history" value="1">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
<? pg_close($con); ?>
</html>
