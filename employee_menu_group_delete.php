<?
require("about_session.php");
require("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
require("employee_auth_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// チェックされた表示グループをループ
foreach ($group_ids as $tmp_group_id) {
	
	// メニューグループ情報を削除
	$sql = "delete from menugroup";
	$cond = "where group_id = $tmp_group_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if($tmp_group_id == $default_group_id)
	{
		//メニューグループ初期設定の内容もクリアする

		// system_config
		$conf = new Cmx_SystemConfig();
	
		$ret_val = $conf->set("default_set.menu_group_id","");
	
	
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 表示グループ画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'employee_menu_group.php?session=$session';</script>");
?>
