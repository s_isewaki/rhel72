<?
ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}
if (!$uploaded) {
	echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
	echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
	exit;
}

// 文字コードの設定
switch ($encoding) {
case "1":
	$file_encoding = "SJIS";
	break;
case "2":
	$file_encoding = "EUC-JP";
	break;
case "3":
	$file_encoding = "UTF-8";
	break;
default:
	exit;
}

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// 正しいカラム数
$column_count = 11;

// CSVデータを配列に格納
$patients = array();
$patient_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

	// 文字コードを変換
	$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

	// EOFを削除
	$line = str_replace(chr(0x1A), "", $line);

	// 空行は無視
	if ($line == "") {
		continue;
	}

	// カラム数チェック
	$patient_data = split(",", $line);
	if (count($patient_data) != $column_count) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$patients[$patient_no] = $patient_data;
	$patient_no++;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 病棟データを配列に格納
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$wards = array();
while ($row = pg_fetch_array($sel)) {
	$wards[$row["bldg_cd"] . "-" . $row["ward_cd"]] = $row["ward_name"];
}

// 病室データを配列に格納
$sql = "select bldg_cd, ward_cd, ptrm_room_no, ptrm_name, ptrm_bed_cur from ptrmmst";
$cond = "where ptrm_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$rooms = array();
$bed_counts = array();
while ($row = pg_fetch_array($sel)) {
	$rooms[$row["bldg_cd"] . "-" . $row["ward_cd"] . "-" . $row["ptrm_room_no"]] = $row["ptrm_name"];
	$bed_counts[$row["bldg_cd"] . "-" . $row["ward_cd"] . "-" . $row["ptrm_room_no"]] = $row["ptrm_bed_cur"];
}

// 診療科データを配列に格納
$sql = "select sect_id, sect_nm from sectmst";
$cond = "where sect_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sects = array();
while ($row = pg_fetch_array($sel)) {
	$sects[$row["sect_id"]] = $row["sect_nm"];
}

// 担当医データを配列に格納
$sql = "select sect_id, dr_id, dr_nm from drmst";
$cond = "where dr_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
mb_regex_encoding("EUC-JP");
$doctors = array();
while ($row = pg_fetch_array($sel)) {
	$dr_nm = mb_convert_kana($row["dr_nm"], "s");
	$dr_nm = mb_ereg_replace(" " , "", $dr_nm);
	$doctors[$row["sect_id"] . "-" . $row["dr_id"]] = $dr_nm;
}

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 入院データを登録
foreach ($patients as $patient_no => $patient_data) {
	insert_patient_data($con, $patient_no, $patient_data, $wards, $rooms, $bed_counts, $sects, $doctors, $emp_id, $encoding, $session, $fname);
}

// トランザクションをコミット
//pg_query($con, "rollback");
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=t&encoding=$encoding';</script>");

// 患者データを登録
function insert_patient_data($con, $patient_no, $patient_data, $wards, $rooms, $bed_counts, $sects, $doctors, $emp_id, $encoding, $session, $fname) {

	// 患者IDの入力チェック
	$ptif_id = trim($patient_data[0]);
	if ($ptif_id == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('患者IDは必須です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (preg_match("/[^0-9a-zA-Z]/", $ptif_id) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('患者IDに使える文字は半角英数字のみです。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($ptif_id) > 12) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('患者IDが長すぎます。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 患者氏名（漢字）の入力チェック
	$pt_nm = trim(mb_convert_kana($patient_data[1], "s"));
	$pt_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_nm);
	list($lt_nm, $ft_nm) = explode(" ", $pt_nm);
	if ($lt_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）を入力してください。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($lt_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎます。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($ft_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）を入力してください。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($ft_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 患者氏名（かな）の入力チェック
	$pt_kana_nm = trim(mb_convert_kana($patient_data[2], "HVcs"));
	$pt_kana_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_kana_nm);
	list($lt_kana_nm, $ft_kana_nm) = explode(" ", $pt_kana_nm);
	if ($lt_kana_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（カナ）を入力してください。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($lt_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（カナ）が長すぎます。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($ft_kana_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（カナ）を入力してください。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($ft_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（カナ）が長すぎます。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 生年月日のチェック
	$birth = trim($patient_data[3]);
	if (preg_match("/^\d{8}$/", $birth) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$birth_yr = substr($birth, 0, 4);
	$birth_mon = substr($birth, 4, 2);
	$birth_day = substr($birth, 6, 2);
	if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$birth = sprintf("%04d%02d%02d", $birth_yr, $birth_mon, $birth_day);

	// 性別の入力チェック
	$sex = trim($patient_data[4]);
	if ($sex != "0" && $sex != "1" && $sex != "2") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('性別が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 病棟名の入力チェック
	$ward_name = trim($patient_data[5]);
	if ($ward_name == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('病棟名は必須です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$ward_key = array_search($ward_name, $wards);
	if ($ward_key == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('病棟名が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	list($bldg_cd, $ward_cd) = split("-", $ward_key);

	// 病室名の入力チェック
	$room_name = trim($patient_data[6]);
	if ($room_name == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('病室名は必須です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$room_keys = array_keys($rooms, $room_name);
	if (count($room_keys) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('病室名が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$room_found = false;
	foreach ($room_keys as $room_key) {
		list($cfm_bldg_cd, $cfm_ward_cd, $room_cd) = split("-", $room_key);
		if ($cfm_bldg_cd == $bldg_cd && $cfm_ward_cd == $ward_cd) {
			$room_found = true;
			break;
		}
	}
	if (!$room_found) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定病室が指定病棟に存在しません。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// ベッド番号の入力チェック
	$bed = trim($patient_data[7]);
	if ($bed == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ベッド番号は必須です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$bed = intval($bed);
	if ($bed < 1 || $bed > $bed_counts[$bldg_cd . "-" . $ward_cd . "-" . $room_cd]) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ベッド番号が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 入院日時の入力チェック
	$in_dttm = trim($patient_data[8]);
	if ($in_dttm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院日時は必須です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($in_dttm) == 8) {
		$in_dttm .= "1000";
	}
	if (preg_match("/^\d{12}$/", $in_dttm) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院日時が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$in_yr = substr($in_dttm, 0, 4);
	$in_mon = substr($in_dttm, 4, 2);
	$in_day = substr($in_dttm, 6, 2);
	if (!checkdate($in_mon, $in_day, $in_yr)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院日が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$in_dt = sprintf("%04d%02d%02d", $in_yr, $in_mon, $in_day);
	$in_hr = substr($in_dttm, 8, 2);
	if (!$in_hr < 0 || $in_hr > 24) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院時刻が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$in_min = (substr($in_dttm, 10, 2) < 30) ? "00" : "30";
	$in_tm = sprintf("%02d%02d", $in_hr, $in_min);

	// 診療科の入力チェック
	$sect_name = trim($patient_data[9]);
	if ($sect_name == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('診療科は必須です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$sect_cd = array_search($sect_name, $sects);
	if ($sect_cd == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('診療科が不正です。レコード番号：{$patient_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 担当医の入力チェック
	$doctor_name = trim($patient_data[10]);
	if ($doctor_name == "") {
		$doctor_cd = 0;
	} else {
		$doctor_name = mb_convert_kana($doctor_name, "s");
		$doctor_name = mb_ereg_replace(" " , "", $doctor_name);
		$doctor_keys = array_keys($doctors, $doctor_name);
		if (count($doctor_keys) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('担当医が不正です。レコード番号：{$patient_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
		$doctor_cd = null;
		foreach ($doctor_keys as $doctor_key) {
			list($cfm_sect_cd, $tmp_doctor_cd) = split("-", $doctor_key);
			if ($cfm_sect_cd == $sect_cd) {
				$doctor_cd = $tmp_doctor_cd;
				break;
			}
		}
		if (is_null($doctor_cd)) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定担当医が指定診療科に存在しません。レコード番号：{$patient_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'bed_bulk_inpatient_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}

	// 登録値の編集
	$keyword = get_keyword($lt_kana_nm);

	// 患者未登録の場合は登録
	$sql = "select count(*) from ptifmst";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
		$sql = "insert into ptifmst (ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_birth, ptif_sex, ptif_keywd, ptif_in_out) values (";
		$content = array($ptif_id, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $birth, $sex, $keyword, "1");
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 指定患者が入院予定登録済みの場合は更新せず復帰
	$sql = "select count(*) from inptres";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		return;
	}

	// 指定患者や指定病床が入院登録済みの場合は更新せず復帰
	$sql = "select count(*) from inptmst";
	$cond = "where ptif_id = '$ptif_id' or (ptif_id is not null and ptif_id != '' and bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_cd and inpt_bed_no = '$bed')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		return;
	}

	// 病床データを更新
	$sql = "update inptmst set";
	$set = array("ptif_id", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_keywd", "inpt_vacant_flg", "inpt_res_flg", "inpt_in_flg", "inpt_in_dt", "inpt_in_tm", "inpt_enti_id", "inpt_sect_id", "dr_id", "nurse_id", "inpt_up_dt", "inpt_up_tm", "inpt_op_no", "inpt_sample_blood", "inpt_ope_dt_flg", "in_updated");
	$setvalue = array($ptif_id, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $keyword, "f", "f", "t", $in_dt, $in_tm, "1", $sect_cd, $doctor_cd, "0", date("Ymd"), date("Hi"), $emp_id, null, null, date("YmdHis"));
	$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_cd and inpt_bed_no = '$bed'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 入院状況データの追加
	$sql = "select max(pt_cond_id) from inptcond";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$cond_id = intval(pg_fetch_result($sel, 0, 0)) + 1;
	$sql = "insert into inptcond (pt_cond_id, ptif_id, cond_check, date) values (";
	$content = array($cond_id, $ptif_id, "1", $in_dt);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
