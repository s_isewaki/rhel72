<?php

ob_start();
require_once("about_comedix.php");
require_once("settings_common.php");
require_once("get_values.ini");
require_once("timecard_tantou_class.php");

/**
 * 時間外統計資料　残業理由別CSVの生成と出力.
 *
 */

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
if ($mmode == "usr") {
	$emp_id = get_emp_id($con, $session, $fname);
	$timecard_tantou_class = new timecard_tantou_class($con, $fname);
	$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
}

$duty_type_flag = get_settings_value("employment_working_other_flg", "f"); // 雇用区分表示

$cls = intval($_POST["cls"]);
$cls_name = ($_POST["cls_name"]);
$atrb = intval($_POST["atrb"]);
$atrb_name = ($_POST["atrb_name"]);
$dept = intval($_POST["dept"]);
$dept_name = ($_POST["dept_name"]);
$start_date = $_POST["select_start_yr"] . $_POST["select_start_mon"] . $_POST["select_start_day"];
$end_date = $_POST["select_end_yr"] . $_POST["select_end_mon"]. $_POST["select_end_day"];


/*
 * 残業別理由データを取得
 */
$sql  = "SELECT o.emp_id, o.target_date, o.reason_id, o.reason as other, ";
$sql .= "o.reason_id2, o.reason2 as other2, o.reason_id3, o.reason3 as other3, o.reason_id4, o.reason4 as other4, o.reason_id5, o.reason5 as other5, "; //追加
$sql .= "m2.reason as reason2, m3.reason as reason3, m4.reason as reason4, m5.reason as reason5, ";
$sql .= "o.reason_detail, m.reason, m.over_no_apply_flag,";
$sql .= "f.emp_personal_id, f.emp_lt_nm, f.emp_ft_nm, e.st_nm, g.job_nm,";
$sql .= "classmst.class_nm,atrbmst.atrb_nm,deptmst.dept_nm,ec.duty_form, ec.duty_form_type,";
$sql .= "r.over_start_time as over_start_time1,r.over_end_time as over_end_time1,r.over_start_next_day_flag as over_start_next_day_flag1,r.over_end_next_day_flag as over_end_next_day_flag1,";
$sql .= "r.rest_start_time1,r.rest_end_time1,r.rest_start_next_day_flag1,r.rest_end_next_day_flag1,";
$sql .= "r.over_start_time2,r.over_end_time2,r.over_start_next_day_flag2,r.over_end_next_day_flag2,r.rest_start_time2,r.rest_end_time2,r.rest_start_next_day_flag2,r.rest_end_next_day_flag2,";
$sql .= "r.over_start_time3,r.over_end_time3,r.over_start_next_day_flag3,r.over_end_next_day_flag3,r.rest_start_time3,r.rest_end_time3,r.rest_start_next_day_flag3,r.rest_end_next_day_flag3,";
$sql .= "r.over_start_time4,r.over_end_time4,r.over_start_next_day_flag4,r.over_end_next_day_flag4,r.rest_start_time4,r.rest_end_time4,r.rest_start_next_day_flag4,r.rest_end_next_day_flag4,";
$sql .= "r.over_start_time5,r.over_end_time5,r.over_start_next_day_flag5,r.over_end_next_day_flag5,r.rest_start_time5,r.rest_end_time5,r.rest_start_next_day_flag5,r.rest_end_next_day_flag5 ";
$sql .= "FROM ovtmapply o ";
$sql .= "INNER JOIN  empmst f on o.emp_id = f.emp_id ";
$sql .= "LEFT JOIN stmst e ON f.emp_st = e.st_id AND e.st_del_flg = 'f' ";
$sql .= "LEFT JOIN jobmst g ON f.emp_job = g.job_id AND g.job_del_flg = 'f' ";
$sql .= "LEFT JOIN classmst ON f.emp_class = classmst.class_id AND classmst.class_del_flg = 'f' ";
$sql .= "LEFT JOIN atrbmst ON f.emp_attribute = atrbmst.atrb_id AND atrbmst.atrb_del_flg = 'f' ";
$sql .= "LEFT JOIN deptmst ON f.emp_dept = deptmst.dept_id AND deptmst.dept_del_flg = 'f' ";
$sql .= "LEFT JOIN empcond ec ON f.emp_id = ec.emp_id ";
$sql .= "LEFT JOIN atdbkrslt r ON o.emp_id = r.emp_id AND o.target_date = r.date ";
$sql .= "LEFT JOIN ovtmrsn m ON o.reason_id = m.reason_id AND m.del_flg = 'f' ";
$sql .= "LEFT JOIN ovtmrsn m2 ON o.reason_id2 = m2.reason_id AND m2.del_flg = 'f' ";
$sql .= "LEFT JOIN ovtmrsn m3 ON o.reason_id3 = m3.reason_id AND m3.del_flg = 'f' ";
$sql .= "LEFT JOIN ovtmrsn m4 ON o.reason_id4 = m4.reason_id AND m4.del_flg = 'f' ";
$sql .= "LEFT JOIN ovtmrsn m5 ON o.reason_id5 = m5.reason_id AND m5.del_flg = 'f' ";

$cond  = "WHERE o.target_date >= '$start_date' AND o.target_date <= '$end_date'  AND o.apply_status in ('0','1') AND o.delete_flg = 'f' ";
if (!empty($cls) && $cls != "-"){
	$cond .= "and f.emp_class = $cls ";

	if (!empty($atrb) && $atrb != "-"){
		$cond .= "and f.emp_attribute = $atrb ";
			
		if (!empty($dept) && $dept != "-"){
			$cond .= "and f.emp_dept = $dept ";
		}
	}
}
//担当所属
if ($mmode == "usr") {
    $cond .= " and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond .= " or (";
        if ($r["class_id"]) $cond .= "     f.emp_class=".$r["class_id"];
        if ($r["atrb_id"])  $cond .= "     and f.emp_attribute=".$r["atrb_id"];
        if ($r["dept_id"])  $cond .= "     and f.emp_dept=".$r["dept_id"];
        if ($r["class_id"]) $cond .= " )";
    }
    //兼務
    $cond_ccr = " or exists (select * from concurrent where concurrent.emp_id = o.emp_id and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond_ccr .= " or (";
        if ($r["class_id"]) $cond_ccr .= "     concurrent.emp_class=".$r["class_id"];
        if ($r["atrb_id"])  $cond_ccr .= "     and concurrent.emp_attribute=".$r["atrb_id"];
        if ($r["dept_id"])  $cond_ccr .= "     and concurrent.emp_dept=".$r["dept_id"];
        if ($r["class_id"]) $cond_ccr .= " )";
    }
    $cond_ccr .= "))";
    $cond .= $cond_ccr;
    $cond .= ") ";
}
$cond .= "ORDER BY o.target_date, o.emp_id ";
$sel = select_from_table($con, $sql, $cond, $fname);
if($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$array_info = array();
while($row = pg_fetch_array($sel)){
	
	$array_item = array();
	$target_date = $row["target_date"];
	$array_item[] = $target_date;			 // 日付
	$array_item[] = $row["emp_personal_id"]; // 職員ID
	
	$array_item[] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"] ;   // 職員名
	$array_item[] = $row["job_nm"];									// 職種
	$array_item[] = $row["st_nm"];									// 役職
	
	switch($row["duty_form"]){
		case "1":
			$e_duty = "常勤";
			break;
		case "2":
			$e_duty = "非常勤";
			break;
		case "3":
			$e_duty = "短時間正職員";
	}
	$array_item[] = $e_duty;  // 雇用勤務形態
	
	if ($duty_type_flag == "t"){
		switch($row["duty_form_type"]){
			case "1":
				$e_duty_type = "正職員";
				break;
			case "2":
				$e_duty_type = "再雇用職員";
				break;
			case "3":
				$e_duty_type = "嘱託職員";
				break;
			case "4":
				$e_duty_type = "臨時職員";
				break;
			case "5":
				$e_duty_type = "パート職員";
				break;
		}
		$array_item[] = $e_duty_type; // 雇用区分
	}
	
	$array_item[] = $row["class_nm"]; // 施設
	$array_item[] = $row["atrb_nm"];  // 部門
	$array_item[] = $row["dept_nm"];  // 科

	$apply_flag = $row["over_no_apply_flag"];
	
	for ($i = 1; $i < 6; $i++){
		if ($apply_flag == "t"){
			$array_item[] = ""; // 残業時刻
		}else{
			// 残業開始日時
			$tmp_start = ($row["over_start_next_day_flag" . $i] == "1") ? date("Ymd", strtotime($target_date. " +1 days")) : $target_date;
			$over_start = strtotime($tmp_start . $row["over_start_time" . $i]);
			
			// 残業終了日時
			$tmp_end = ($row["over_end_next_day_flag" . $i] == "1") ? date("Ymd", strtotime($target_date. " +1 days")) : $target_date;
			$over_end = strtotime($tmp_end . $row["over_end_time" . $i]);
			
			// 残業時間計算
			$over_count = $over_end - $over_start;
			
			// 休憩開始日時
			$tmp_start_rest = ($row["rest_start_next_day_flag" . $i] == "1") ? date("Ymd", strtotime($target_date. " +1 days")) : $target_date;
			$rest_start = strtotime($tmp_start_rest . $row["rest_start_time" . $i]);
			
			// 休憩終了日時
			$tmp_end_rest = ($row["rest_end_next_day_flag" . $i] == "1") ? date("Ymd", strtotime($target_date. " +1 days")) : $target_date;
			$rest_end = strtotime($tmp_end_rest . $row["rest_end_time" . $i]);
			
			// 休憩時間計算
			$rest_count = $rest_end - $rest_start;
			
			// 順残業時間
			$result_count = $over_count - $rest_count;
			$result_time = gmdate("H:i", $result_count);
			
			$array_item[] = $result_time; // 残業時刻
		}
	}
	
	if ($apply_flag == "t"){
		$array_item[] = "";
		$array_item[] = "";
		$array_item[] = $row["reason_id"]; // 残業不要理由ID
		$array_item[] = $row["reason"]; // 残業不要理由
	}else{
		$array_item[] = $row["reason_id"]; // 残業理由ID
		$array_item[] = $row["reason"]; // 残業理由
		$array_item[] = "";
		$array_item[] = "";
	}
	
	$array_item[] = $row["other"]; // そのた残業理由
	//残業理由2-5
	for ($i=2; $i<=5; $i++) {
		$array_item[] = $row["reason_id".$i];
		$array_item[] = $row["reason".$i];
		$array_item[] = $row["other".$i];
	}
	$array_item[] = str_replace(array("\r\n","\r","\n"), '', $row["reason_detail"]); // 理由詳細
	
	$array_info[] = $array_item;
}

// 情報をCSV形式で出力
$csv = get_list_csv($array_info, $cls_name, $atrb_name, $dept_name, $start_date, $end_date, $duty_type_flag);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "OvertimeReason_" . $start_date . "-" . $end_date . ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

function diff_time($Hi1, $Hi2) {
	if (empty($Hi1) || empty($Hi2)) return null;
	$h1 = intval(substr($Hi1, 0, 2));
	$m1 = intval(substr($Hi1, 2, 2));
	$h2 = intval(substr($Hi2, 0, 2));
	$m2 = intval(substr($Hi2, 2, 2));
	$min = ($h2 * 60 + $m2) - ($h1 * 60 + $m1);
	return intval($min / 60).":".sprintf("%02d", $min % 60);
}

/**
 * CSVデータの生成.
 * @param array $data 職員別月別残業時間集計結果(連想配列) 時間外合計で降順に並び替え済
 * @param int $overtime_criteria 残業時間の閾値.
 * @param string 集計対象期間 開始日
 * @return string CSV形式に整形された閾値以上の残業者リスト.
 */
function get_list_csv($array_info, $cls_name, $atrb_name, $dept_name, $start_date, $end_date, $duty_type_flag) {
	
	if ($duty_type_flag == "t"){
		$titles = array(
				"日付",
				"職員ID",
				"氏名",
				"職種",
				"役職",
				"雇用勤務形態",
				"雇用区分",
				$cls_name,
				$atrb_name,
				$dept_name,
				"残業時刻1",
				"残業時刻2",
				"残業時刻3",
				"残業時刻4",
				"残業時刻5",
				"残業理由ID",
				"残業理由",
				"残業不要理由ID",
				"残業不要理由",
				"その他の理由",
				"残業理由2ID",
				"残業理由2",
				"その他の理由2",
				"残業理由3ID",
				"残業理由3",
				"その他の理由3",
				"残業理由4ID",
				"残業理由4",
				"その他の理由4",
				"残業理由5ID",
				"残業理由5",
				"その他の理由5",
				"理由詳細"
		);
	}else{
		$titles = array(
				"日付",
				"職員ID",
				"氏名",
				"職種",
				"役職",
				"雇用勤務形態",
				$cls_name,
				$atrb_name,
				$dept_name,
				"残業時刻1",
				"残業時刻2",
				"残業時刻3",
				"残業時刻4",
				"残業時刻5",
				"残業理由ID",
				"残業理由",
				"残業不要理由ID",
				"残業不要理由",
				"その他の理由",
				"残業理由2ID",
				"残業理由2",
				"その他の理由2",
				"残業理由3ID",
				"残業理由3",
				"その他の理由3",
				"残業理由4ID",
				"残業理由4",
				"その他の理由4",
				"残業理由5ID",
				"残業理由5",
				"その他の理由5",
				"理由詳細"
		);
	}
	
	$item_num = count($titles);
	$num =count($array_info);
	
	
	$buf = "";
	$buf .= $start_date . "," . $end_date;
	$buf .= "\r\n";

	// タイトル
	for ($j=0;$j<$item_num;$j++) {
		if ($j != 0) {
			$buf .= ",";
		}
		$buf .= $titles[$j];
	}
	$buf .= "\r\n";
	
	// 内容
	for($i=0;$i<$num;$i++){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$buf .= $array_info[$i][$j];
		}
		$buf .= "\r\n";
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}
?>	
