<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
//require_once("about_session.php");
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");

require_once("show_class_name.ini");
require_once("get_values.ini");

require_once("work_admin_timecard_common.php");
require_once("timecard_tantou_class.php");

define("WBR", "&#8203;"); // HTML強制改行文字

// ページ名func
$fname = $PHP_SELF;
// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// リンク出力用変数の設定
if ($page == "") {
	$page = 0;
}
$limit = 100;
$url_srch_name = urlencode($srch_name);

// DBコネクション作成
$con = connect2db($fname);
$timecard_tantou_class = new timecard_tantou_class($con, $fname);

$emp_id = get_emp_id($con, $session, $fname);
// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}
$arr_class_name = get_class_name_array($con, $fname);
$mmode = "usr"; //menumode usr:権限ありの場合ユーザ側で対象者検索を使用可 mng:管理側

$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
// 組織情報を取得し、配列に格納
list($arr_cls, $arr_clsatrb, $arr_atrbdept) = $timecard_tantou_class->get_clsatrb_info($timecard_tantou_branches);
// 該当する職員を検索
$sel_emp = false;

	$sql = "select count(*) from empmst";

	//検索条件SQLを取得 work_admin_timecard_common.php
    $cond = get_cond_work_admin($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id);


	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_cnt = pg_fetch_result($sel, 0, 0);

	$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, cm.class_nm, am.atrb_nm, dm.dept_nm from empmst ".
    " left outer join classmst cm on (cm.class_id = empmst.emp_class)".
    " left outer join atrbmst am on (am.atrb_id = empmst.emp_attribute)".
    " left outer join deptmst dm on (dm.dept_id = empmst.emp_dept)";
	$offset = $page * $limit;

	//ソート順
	if ($sort == "") {
		$sort = "3";
	}
	switch ($sort) {
		case "1":
			$orderby = "empmst.emp_personal_id ";
			break;
		case "2":
			$orderby = "empmst.emp_personal_id desc ";
			break;
		case "3":
			$orderby = "empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id ";
			break;
		case "4":
			$orderby = "empmst.emp_kn_lt_nm desc, empmst.emp_kn_ft_nm desc, empmst.emp_personal_id ";
			break;
	}
	$cond .= " order by $orderby offset $offset limit $limit";
	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}


?>
<title>CoMedix 出勤表｜職員検索</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
var session = '';
function initPage() {
	classOnChange('<? echo($atrb); ?>', '<? echo($dept); ?>');
}

function classOnChange(atrb_id, dept_id) {
	if (!document.mainform) {
		return;
	}

	var class_id = document.mainform.cls.value;

	deleteAllOptions(document.mainform.atrb);

	addOption(document.mainform.atrb, '-', '----------', atrb_id);

<? foreach ($arr_clsatrb as $tmp_clsatrb) { ?>
	if (class_id == '<? echo $tmp_clsatrb["class_id"]; ?>') {
	<? foreach($tmp_clsatrb["atrbs"] as $tmp_atrb) { ?>
		addOption(document.mainform.atrb, '<? echo $tmp_atrb["id"]; ?>', '<? echo $tmp_atrb["name"]; ?>', atrb_id);
	<? } ?>
	}
<? } ?>

	atrbOnChange(dept_id);
}

function atrbOnChange(dept_id) {
	var atrb_id = document.mainform.atrb.value;

	deleteAllOptions(document.mainform.dept);

	addOption(document.mainform.dept, '-', '----------', dept_id);

<? foreach ($arr_atrbdept as $tmp_atrbdept) { ?>
	if (atrb_id == '<? echo $tmp_atrbdept["atrb_id"]; ?>') {
	<? foreach($tmp_atrbdept["depts"] as $tmp_dept) { ?>
		addOption(document.mainform.dept, '<? echo $tmp_dept["id"]; ?>', '<? echo $tmp_dept["name"]; ?>', dept_id);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
	}
}
<? //共通にする ?>
var dlgResizableOption = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
var windowStocker = {};
function window_open(url, wname, width, height) {
    var x = (screen.width - width) / 2;
    var y = (screen.height - height) / 2;
    opt = "left=" + x + ",top=" + y + ",width=" + width + ",height=" + height + dlgResizableOption;
    if (windowStocker[wname]) {
        try { windowStocker[wname].close(); } catch(ex){}
    }
    windowStocker[wname] = window.open(url, wname, opt);
}

var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=10';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");

	document.mainform.btn_event.value = "add_emp";
	document.mainform.add_emp_ids.value = emp_ids.join(",");
	document.mainform.action="timecard_tantou.php";
	document.mainform.submit();
}
function deleteEmployee() {
    //if ($("input:checkbox[name='emp_id']:checked").size() == 0) {
    //    alert("削除対象の職員チェックボックスが選択されていません。");
    //    return;
    //}
	var emp_ids = new Array();
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			if (document.mainform.elements['emp_id[]'].checked) {
				emp_ids.push(document.mainform.elements['emp_id[]'].value);
			}
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.mainform.elements['emp_id[]'][i].checked) {
					emp_ids.push(document.mainform.elements['emp_id[]'][i].value);
				}
			}
		}
	}
    if (emp_ids.length == 0) {
	    alert('対象者が選択されていません。');
	    return;
    }
    if (!confirm("チェックを行った職員を削除します。よろしいですか?")) return;
	
	document.mainform.btn_event.value = "del_emp";
	document.mainform.action="timecard_tantou.php";
	document.mainform.submit();
}
function show_tantou_branch(emp_id) {
    var url = "timecard_tantou.php?btn_event=show_tantou_branch&session="+session+"&target_emp_id="+emp_id;
    window_open(url, "branch_edit", 600, 380);
}
function delete_branch_select(emp_id, branch_id) {
    if (!confirm("この所属への権限を削除します。よろしいですか？")) return;
    document.mainform.target_branch_id.value = branch_id;
    document.mainform.target_emp_id.value = emp_id;
    document.mainform.btn_event.value = "del_employee_branch";
    document.mainform.submit();
}
//ソート
function set_sort(id) {

	sort_key = document.mainform.sort.value;

	if (id == "EMP_ID") {
		sort_key = (sort_key == "1") ? "2" : "1";
	} else if (id == "EMP_NAME") {
		sort_key = (sort_key == "3") ? "4" : "3";
	} else if (id == "REG_DATE") {
		sort_key = (sort_key == "6") ? "5" : "6";
	}
	document.mainform.sort.value = sort_key;
	document.mainform.submit();
}
//チェックボックスONOFF
function checkAll() {

	stat = (document.mainform.checkall.value == '解除') ? false : true;
	document.mainform.checkall.value = (document.mainform.checkall.value == '解除') ? '選択' : '解除';
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			document.mainform.elements['emp_id[]'].checked = stat;
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				document.mainform.elements['emp_id[]'][i].checked = stat;
			}
		}
	}
}
/**
 * よく使う人カウンタ
 * @param {type} id 職員ID
 */
function set_wm_counter(id) {
    $.ajax({
        url: 'emplist/counter.php',
        type: 'post',
        data: {
            emp_id: id,
            session: $('#session').val()
        }
    });
}
/**
 * 登録対象者を追加する
 * @param {type} item_id
 * @param {type} name
 * @param {type} emp_id
 */
function add_target(item_id, name, emp_id) {
    if (emp_id === "") {
        alert('職員が存在しません');
        return;
    }
    //set_wm_counter(emp_id);
    if (!parent.opener || parent.opener.closed || !parent.opener.add_target_list) {
        return;
    }
    parent.opener.add_target_list(item_id, emp_id, name);
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.block3 {border-collapse:collapse;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="3" leftmargin="3" marginheight="3" marginwidth="3" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr height="32" bgcolor="#5279a5">
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>職員検索</b></font></td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="timecard_emp_search.php" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?></font></td>
		<td>
			<select name="cls" onchange="classOnChange();">
				<option value="-">----------
<?
foreach ($arr_cls as $tmp_cls) {
	$tmp_id = $tmp_cls["id"];
	$tmp_name = $tmp_cls["name"];
	echo("<option value=\"$tmp_id\"");
	if ($tmp_id == $cls) {
		echo(" selected");
	}
	echo(">$tmp_name");
}
?>
			</select>
		</td>
		<td align="right" bgcolor="#f6f9ff" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?></font></td>
		<td nowrap valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="atrb" onchange="atrbOnChange();">
			</select>
		</td>
		<td align="right" bgcolor="#f6f9ff" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?></font></td>
		<td nowrap valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="dept">
			</select>
		</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff" width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
		<td><input type="text" name="srch_id" value="<? echo($srch_id); ?>" size="15" maxlength="12" style="ime-mode:inactive;"></td>
		<td align="right" bgcolor="#f6f9ff" width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名</font></td>
		<td colspan="3"><input type="text" name="srch_name" value="<? echo($srch_name); ?>" size="25" maxlength="50" style="ime-mode:active;"></td>
		<td align="left" style="border:none"><input type="submit" value="検索" onClick="document.mainform.sort.value=3;"></td>
	</tr>
</table>
<table width="" border="0" cellspacing="0" cellpadding="2">
<tr>
<td width="100" align="left">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">該当</font><? echo $emp_cnt; ?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人</font>
</td>
<td width="200"></td>
<td>
</td>
</tr>
</table>
<? show_navi($emp_cnt, $limit, $page); ?>
<? $arr_emp_list = show_emp_list($sel_emp, $branches_list); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="1">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="check_all" value="1">
<input type="hidden" name="btn_event" value="">
<input type="hidden" name="add_emp_ids" value="">
<input type="hidden" name="target_emp_id" value="" />
<input type="hidden" name="target_branch_id" value="" />
<input type="hidden" name="sort" value="<? echo($sort); ?>" />
</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
<?
// ページ切り替えリンクを表示
function show_navi($num, $limit, $page) {
	echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"><br>\n");

    global $session, $url_srch_name, $cls, $atrb, $dept, $srch_id;

	$total_page = ceil($num/$limit);
	$check_next = $num % $limit;
	$check_last = $total_page - 1;

	if ($total_page >= 2) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
  		echo("<tr>\n");
		echo("<td align=\"center\">\n");
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("ページ");
		echo("&nbsp;");
		echo("</font>");

		if ($page > 0) {
			$back = $page - 1;

			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$back&srch_flg=1&srch_id=$srch_id\">←</a></font>");
		}

		for ($i = 0; $i < $total_page; $i++) {
			$j = $i + 1;
			if ($page != $i) {
                echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> <a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$i&srch_flg=1&srch_id=$srch_id\">$j</a> </font>");
			} else {
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("［{$j}］");
				echo("</font>");
			}
		}

		if ($check_last != $page) {
			$next = $page + 1;

            echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$next&srch_flg=1&srch_id=$srch_id\">→</a></font>");
		}

		echo("</td>\n");
  		echo("</tr>\n");
		echo("</table>\n");
	}
}

// 担当者一覧を表示
function show_emp_list($sel, $branches_list) {
	if (!$sel) {
		return;
	}

    global $session, $url_srch_name, $cls, $atrb, $dept, $page, $emp_id, $check_all, $srch_id, $sort;

	$item_id = $_REQUEST["item_id"];

	$arr_emp_list = array();
	
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"40\" align=\"center\">");
	echo("</td>\n");
	echo("<td width=\"100\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

	$arrow = "";
	if ($sort == "1") {
		$arrow = "img/up.gif";
	}
	if ($sort == "2") {
		$arrow = "img/down.gif";
	}
	echo("<a href=\"javascript:void();\" onClick=\"set_sort('EMP_ID');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員ID</a>");
	echo("</font></td>\n");
	echo("<td width=\"160\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

	$arrow = "";
	if ($sort == "3") {
		$arrow = "img/up.gif";
	}
	if ($sort == "4") {
		$arrow = "img/down.gif";
	}
	echo("<a href=\"javascript:void();\" onClick=\"set_sort('EMP_NAME');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員氏名</a>");
	echo("</font></td>\n");
	echo("<td width=\"180\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">所属</font></td>\n");
	echo("</tr>\n");

	while ($row = pg_fetch_array($sel)) {
		$tmp_id = $row["emp_id"];
		$tmp_personal_id = $row["emp_personal_id"];
		$tmp_name = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
        $ary = array($row["class_nm"]);
        if ($row["atrb_nm"]) $ary[]=$row["atrb_nm"];
        if ($row["dept_nm"]) $ary[]=$row["dept_nm"];
        
        $branch_disp = str_replace("\n", WBR."＞", h(implode("\n", $ary)));

		$tmp_class = $row["emp_class"];
		$tmp_attribute = $row["emp_attribute"];
		$tmp_dept = $row["emp_dept"];
		$buf = $tmp_id . "," . $tmp_personal_id . "," . $tmp_name . "," . $tmp_class . "," . $tmp_attribute . "," . $tmp_dept;
		$arr_emp_list[$tmp_id] = $buf;
		
		echo("<tr height=\"22\">\n");
		echo("<td width=\"60\" align=\"center\"><input type=\"button\" id=\"checkall\" name=\"checkall\" value=\"選択\" onclick=\"add_target('{$item_id}', '{$tmp_name}', '{$tmp_id}');\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_personal_id</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_name</font></td>\n");
        //所属
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$branch_disp");
		echo("</font></td>\n");

		echo("</tr>\n");
	}

	echo("</table>\n");
	return $arr_emp_list;
}


?>
