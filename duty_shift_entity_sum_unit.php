<HTML>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- -- FRAME及びIFRAMEを使わない画面分割のCSSを含む -->
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
html {height:100%;}
td.w180{ width:180;}
td.w80 { width:80; }
td.w135{ width:135;}
td.w136{ width:136;}
td.w138{ width:138;}
td.w44 { width:44.5; }
td.w45 { width:45; }
td.w46 { width:46; }
body{
	margin:0;
	padding:0;
	height:100%;
}
#NameInput {

	margin-left:10px;
//	margin-right:10%;
//	width:860px;
	width:1260;
	overflow:hidden;
	background-color:#FFFFFF;
}
#ValueInput {
	margin-top:-1px;
//	margin-right:10%;
	margin-left:10px;
	height:540;
	width:1260px;
	overflow:scroll;
	background-color:#FFFFFF;
}

</style>

<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務表集計設定</title>
<?

//ini_set("display_errors","1");
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
    ///-----------------------------------------------------------------------------
    // 勤務パターン情報を取得
    ///-----------------------------------------------------------------------------
    $wktmgrp_array = $obj->get_wktmgrp_array();
    if (count($wktmgrp_array) <= 0) {
        $err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
    }
    if ($pattern_id == "") {
        $pattern_id = $wktmgrp_array[0]["id"];
    }
    $group_id = $pattern_id; // 更新処理プログラムduty_shift_entity_sumunit_update_exe.phpへ持って行く
    ///-----------------------------------------------------------------------------
    //ＤＢ(atdptn)より出勤パターン情報取得
    ///-----------------------------------------------------------------------------
    $atdptn_array = $obj->get_atdptn_array($pattern_id);

    ///-----------------------------------------------------------------------------
    //勤務シフトパターン情報を取得
    ///-----------------------------------------------------------------------------
    $pattern_array = $obj->get_duty_shift_pattern_array($pattern_id, $atdptn_array);
    $atdptn_ptn_name = $pattern_array[$i]["atdptn_ptn_name"]; // 出勤パターン名
    $font_name = $pattern_array[$i]["font_name"]; // 出勤パターンの記号、○、前、後、深、休、など

    if (count($pattern_array) <= 0) {
        //-------------------------------------------------------------------
        //データがない時
        //-------------------------------------------------------------------
        $wk_atdptn_ptn_id = "";
        $m = 0;
        for($i=0; $i<count($atdptn_array); $i++) {
            //-------------------------------------------------------------------
            //休暇の有無判定
            //-------------------------------------------------------------------
            // 休暇を名称ではなくIDで確認
            if ($atdptn_array[$i]["id"] == "10") {
                $wk_atdptn_ptn_id = $atdptn_array[$i]["id"];        //出勤パターンＩＤ
                $wk_atdptn_ptn_name = $atdptn_array[$i]["name"];    //出勤パターン名称
                $wk_count_kbn_gyo = "9999";                         //集計区分(行)  （9999：その他）
                $wk_count_kbn_retu = "9999";                        //集計区分(列)  （9999：その他）
                $wk_font_name = "";                                 //表示文字
                $wk_font_color_id = $font_color_array[0]["color"];  //文字色
                $wk_back_color_id = $back_color_array[0]["color"];  //背景色
                $wk_count_kbn_gyo_name = "";                        //行名
                $wk_count_kbn_retu_name = "";                       //列名
            } else {
                //-------------------------------------------------------------------
                //出勤パターン値設定
                //-------------------------------------------------------------------
                $pattern_array[$m]["pattern_id"] = $pattern_id;                     //出勤グループＩＤ
                $pattern_array[$m]["atdptn_ptn_id"] = $atdptn_array[$i]["id"];      //出勤パターンＩＤ
                $pattern_array[$m]["atdptn_ptn_name"] = $atdptn_array[$i]["name"];  //出勤パターン名称
                $pattern_array[$m]["reason"] = " ";             //事由
                $m++;
            }
        }
        //-------------------------------------------------------------------
        //休暇が存在する場合
        //-------------------------------------------------------------------
        if ($wk_atdptn_ptn_id != "") {
            $pattern_array = $obj->setReasonPatternArray2($pattern_array, $pattern_id,
                                                $wk_atdptn_ptn_id, $wk_atdptn_ptn_name,
                                                $wk_count_kbn_gyo, $wk_count_kbn_retu,
                                                $wk_count_kbn_gyo_name, $wk_count_kbn_retu_name,
                                                $wk_font_name,
                                                $wk_font_color_id, $wk_back_color_id);
            // 出勤表の休暇種別等画面の非表示設定を反映
            $arr_reason_display_flag = $obj->get_reason_display_flag();
            for ($j=0; $j<count($pattern_array); $j++) {
                $tmp_reason_id = $pattern_array[$j]["reason"];
                if ($arr_reason_display_flag["$tmp_reason_id"] != "") {
                    $pattern_array[$j]["reason_display_flag"] = $arr_reason_display_flag["$tmp_reason_id"];
                }
            }
        }
    }

    $data_cnt = count($pattern_array);

    ///-----------------------------------------------------------------------------
    // 集計方法変更データの各種を取得、atdptn_id,atdptn_nm,font_nameは既存機能を利用
    ///-----------------------------------------------------------------------------

    // 「集計変更を有効にする」スイッチを取得
    $sql = "SELECT sum_change_sw FROM duty_shift_entity_sum_change_sw";
    $cond = "WHERE group_id = $group_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
    }
    $change_switch = pg_fetch_result($sel,0,"sum_change_sw");

    // 集計単位名称を取得
    $sql = "SELECT sum_unit_name FROM duty_shift_entity_sum_unit_name ";
    $cond = "WHERE group_id = $group_id ORDER BY sum_unit_name_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
    }
    $unit_name_count = check_record($sel); // 名称の数（仕様は最大13）
    $unit_name_arr = array();
    for($c=0; $c<$unit_name_count; $c++){
	$unit_name_arr[$c+1] = pg_fetch_result($sel, $c, "sum_unit_name");
    }

    // 集計単位値を取得、group_id、atdptn(勤務パターン)、reason(休暇タイプ)、単位id(sum_unit_name_id)を順に条件として取り出す
    $get_unit_value = array();
    $sql = "SELECT sum_unit_value FROM duty_shift_entity_sum_unit_value AS VLE ";
    for($r=0; $r<$data_cnt; $r++){
//	$atdptn_id = $pattern_array[$r]["atdptn_ptn_id"];
	$font_name = $pattern_array[$r]["font_name"];
	$reason = $pattern_array[$r]["reason"];
	if ( $reason == "" ){$reason = 0;}
	for($c=1; $c<=13; $c++){ // 格納されている値が1〜13
		$cond = "WHERE sum_unit_name_id IN (SELECT NAM.sum_unit_name_id FROM duty_shift_entity_sum_unit_name AS NAM WHERE NAM.group_id = $group_id AND VLE.group_id = $group_id AND VLE.font_name='$font_name' AND VLE.reason=$reason AND NAM.sum_unit_name_id=$c)";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if (pg_num_rows($sel) == 0){ break; }
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$get_unit_value[$r][$c] = pg_fetch_result($sel,0,'sum_unit_value');
	}
    }

pg_close($con); // DB閉じる

?>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 画面遷移／タブ -->
    <!-- ------------------------------------------------------------------------ -->
    <?
        // 画面遷移
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_title($session, $section_admin_auth);           //duty_shift_common.ini
        echo("</table>\n");

        // タブ
        $arr_option = "";
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_tab_menuitem($session, $fname, $arr_option);    //duty_shift_manage_tab_common.ini
        echo("</table>\n");

        // 下線
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- データ不備時 -->
    <!-- ------------------------------------------------------------------------ -->
    <?
//        if ($err_msg_1 != "") {
//            echo($err_msg_1);
//            echo("<br>\n");
//        } else {
    ?>

<!-- ＊＊＊注意＊＊＊　開発中のactionはリカーシブ設定-->
<!--FORM name="shiftsummry" action="duty_shift_entity_sum_unit.php" method="post"-->

        <div style="margin:10px;padding:10px;font-size:16px;font-weight:bold;background-color:yellow;border:1px solid orange;">
           集計設定Bは、後方互換性のために残している機能です。<br>新規に集計設定を行う場合は<a href="duty_shift_entity_sign_count.php?session=<?=h($session)?>">集計設定A</a>をご利用下さい。
        </div>

        <div style="padding:5px; font-size:14px;">
            [<a href="duty_shift_entity_sign_count.php?session=<?=h($session)?>">集計設定A</a>]
            &nbsp;&nbsp;
            [<a href="?session=<?=h($session)?>">集計設定B</a>]
        </div>
        <hr>

<FORM name="shiftsummry" action="duty_shift_entity_sum_unit_update_exe.php" method="post">

<table width="700" border="0" cellspacing="0" cellpadding="2">
        <?
            echo("<tr height=\"22\">\n");
            ///-----------------------------------------------------------------------------
            // 勤務パターングループ名
            ///-----------------------------------------------------------------------------
            echo("<td width=\"25%\" align=\"center\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務パターングループ名</font></td>\n");
            echo("<td width=\"\"><select name=\"pattern_id\" onchange=\"this.form.action='$fname'; this.form.submit();\">\n");
            for($i=0;$i<count($wktmgrp_array);$i++) {
                $wk_id= $wktmgrp_array[$i]["id"];
                $wk_name = $wktmgrp_array[$i]["name"];
                echo("<option value=\"$wk_id\"");
                if ($pattern_id == $wk_id) {
                    echo(" selected");
                }
                echo(">$wk_name\n");
            }
            echo("</select></td>\n");
            echo("</tr>\n");

        ?>
</table>
<BR>

<div id="NameInput">
<?
   if ( $change_switch == 1 ){$checkd="checked";}else{$checkd="";}
?>
<TABLE BORDER="0">
<TR><TD WIDTH="400">
<!--	///----------------------------------------------------------------------------->
<!--	// 有効チェックボックス-->
<!--	///----------------------------------------------------------------------------->
<input type="checkbox" name="summary_style" value="ON" <? echo $checkd ?>>
<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">集計設定Bを有効にする</font><BR>
</TD>
<TD>
<!--	///----------------------------------------------------------------------------->
<!--	// 登録ボタン
<!--	///----------------------------------------------------------------------------->
<input type="submit" value="登録"><BR>
</TD>
</TR>
</TABLE>

<!--	///----------------------------------------------------------------------------->
<!--	// 入力域 -->
<!--	///----------------------------------------------------------------------------->

<TABLE BORDER="0" WIDTH="1220" cellspacing="0" cellpadding="0" class="list" style="table-layout: fixed; ">

<?
//	$data_cnt = count($pattern_array);

	// 見出し

	echo "<TR height=\"30\" align=\"center\" valign=\"center\" >\n";
//	$disp = "<TD COLSPAN=2 CLASS=\"w180\">&nbsp</TD>\n";
	$disp = "<TD CLASS=\"w135\"><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp</font></TD>\n";
	$disp = $disp."<TD CLASS=\"w45\"><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp</font></TD>\n";
	echo $disp;
	for( $c=1 ; $c <= 13 ; $c++ ){
		$disp = "<TD CLASS=\"w80\"><input type=\"text\" name=\"unit_name_$c\"";
		echo $disp;
		$disp = " size=\"6\" style=\"text-align:left;font-size:11;\" maxlength=\"8\" value=\"".$unit_name_arr[$c]."\"></TD>\n";
		echo $disp;
	}
	echo "</TR>\n";
?>
</TABLE>
</div>

<div id="ValueInput">
<TABLE BORDER="0" WIDTH="1220" cellspacing="0" cellpadding="0" class="list" style="table-layout: fixed; ">

<?
	for( $r=0 ; $r < $data_cnt ; $r++ ){
		echo "<TR height=\"30\" align=\"center\" valign=\"center\" >\n";
		// 勤務パターン名
		if( $pattern_array[$r]["atdptn_ptn_id"] == 10 ){
			$name = "(".$pattern_array[$r]["atdptn_ptn_name"].")";
		}else{
			$name=$pattern_array[$r]["atdptn_ptn_name"];
		}
//		$disp= " <TD width=\"135\" ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$name."&nbsp".$pattern_array[$r]["reason_name"]."</font></TD>\n";
		$disp= " <TD CLASS=\"w135\" ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$name."&nbsp".$pattern_array[$r]["reason_name"]."</font></TD>\n";
		echo $disp;
		// 勤務パターン記号
//		$disp = " <TD width=\"45\" align=\"center\"><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$pattern_array[$r]["font_name"]."</TD>\n";
		$disp = " <TD CLASS=\"w45\" align=\"center\"><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$pattern_array[$r]["font_name"]."</TD>\n";
		echo $disp;
		// HIDDENデータ、atdptn 勤務シフトパターン
		for( $c=1 ; $c <= 13 ; $c++ ){
			// 入力欄
//			echo " <TD width=\"80\"><input type=\"text\" name=\"unit_value_";
			echo " <TD CLASS=\"w80\"><input type=\"text\" name=\"unit_value_";
			$getValue = $get_unit_value[$r][$c];
			if( $getValue == "" || $getValue ==0){$getValue="00.0";}
			$disp = $r."_".$c."\" size=\"4\" style=\"text-align:right;font-size:11;\" maxlength=\"4\" value=\"".$getValue."\"></TD>\n";
			echo $disp;
		}
		echo "</TR>\n";
	}
	echo "</TABLE>\n</div>\n";

//        <!-- ------------------------------------------------------------------------ -->
//        <!-- ＨＩＤＤＥＮ -->
//        <!-- ------------------------------------------------------------------------ -->

            echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
            echo("<input type=\"hidden\" name=\"data_cnt\" value=\"$data_cnt\">\n");
            echo("<input type=\"hidden\" name=\"group_id\" value=\"$group_id\">\n");
            for ($i=0; $i<$data_cnt; $i++) {
                //出勤パターンＩＤ
                $wk = $pattern_array[$i]["atdptn_ptn_id"];
                echo("<input type=\"hidden\" name=\"atdptn_ptn_id_$i\" value=\"$wk\">\n");
                //省略名
                $wk = $pattern_array[$i]["font_name"];
                echo("<input type=\"hidden\" name=\"font_name_$i\" value=\"$wk\">\n");
                //事由
                $wk = $pattern_array[$i]["reason"];
                echo("<input type=\"hidden\" name=\"reason_$i\" value=\"$wk\">\n");
                //連携コード
                $wk = $pattern_array[$i]["sfc_code"];
                echo("<input type=\"hidden\" name=\"sfc_code_$i\" value=\"$wk\">\n");
                //分担表
                $wk = $pattern_array[$i]["allotment_order_no"];
                echo("<input type=\"hidden\" name=\"allotment_order_no_$i\" value=\"$wk\">\n");
            }

// この課題で新たに追加したテーブル
// duty_shift_entity_sum_change_sw (変更スイッチ)
// duty_shift_entity_sum_unit_value (変更値)
// duty_shift_entity_sum_unit_name (集計名)

?>

</FORM>

</BODY>
</html>
