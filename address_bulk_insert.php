<?
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// アドレス帳権限のチェック
$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}

// 正常にアップロードされた場合、登録処理を実行
if ($uploaded) {
	$result = "t";

	// ファイルの読み込み
	$lines = file($_FILES["csvfile"]["tmp_name"]);

	// トランザクションの開始
	$con = connect2db($fname);

	// アクセスログ
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

	pg_query($con, "begin transaction");

	// 管理画面
	if ($admin_flg == "t") {
		$emp_id = $target_emp_id;
		$next_php = "address_admin_bulk_register.php";
	} else {
	// ユーザ画面
		// ログインユーザの職員IDを取得
		$sql = "select emp_id from session";
		$cond = "where session_id = '$session'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$emp_id = pg_result($sel, 0, "emp_id");
		$next_php = "address_bulk_register.php";
	}

	// 登録日時を設定
	$date = date("YmdHi");

	// 1行分ずつinsert
	foreach ($lines as $record_no => $line) {

		// アドレスIDの採番
		$sql = "select max(address_id) as max_address_id from address";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$max_address_id = pg_fetch_result($sel, 0, "max_address_id");
		if ($max_address_id == "") {
			$address_id = 1;
		} else {
			$address_id = $max_address_id + 1;
		}

		// レコードをカラムに分割
		$line = trim(mb_convert_encoding($line, "EUC-JP", "auto"));
		$line = str_replace(chr(0x1A), "", $line);  // EOFを削除
		if ($line == "") {  // 空行は無視
			continue;
		}
		// 項目見出し行は無視
		if (strpos($line, "姓（漢字）,名（漢字）") !== false) {
			continue;
		}
		$values = split(",", $line);
		if (count($values) != 20) {
			$result = "f";
			pg_query($con, "rollback");
			pg_close($con);
			break;
		}

		// 登録値を配列に格納
		$content = array();
		$content[0] = $emp_id;  // 職員ID
		$content[1] = $address_id;  // アドレスID
		$content[2] = $date;  // 登録日時
		$content[3] = $values[0];  // 姓（漢字）
		$content[4] = $values[1];  // 名（漢字）
		$content[5] = $values[2];  // 姓（カナ）
		$content[6] = $values[3];  // 名（カナ）
		$content[7] = $values[4];  // 会社名
		$content[8] = $values[5];  // 部門名１
		$content[9] = $values[6];  // 部門名２
		$content[10] = $values[7];  // 役職１
		$content[11] = $values[8];  // 役職２
		list($content[12], $content[13]) = split("-", $values[9]);  // 郵便番号
		$content[14] = to_prefecture_cd($values[10]);  // 都道府県名
		$content[15] = $values[11];  // 住所１
		$content[16] = $values[12];  // 住所２
		$content[17] = $values[13];  // 住所３
		list($content[18], $content[19], $content[20]) = split("-", $values[14]);  // 電話
		list($content[21], $content[22], $content[23]) = split("-", $values[15]);  // FAX
		list($content[24], $content[25], $content[26]) = split("-", $values[16]);  // 携帯／PHS
		$content[27] = $values[17];  // E-MAIL（PC）
		$content[28] = $values[18];  // E-MAIL（携帯）
		$content[29] = $values[19];  // 備考
                $content[30] = "f";  // 削除フラグ
                $content[31] = $shared_flg;  // 共有フラグ
		ksort($content);  // 配列をキー順にソート（splitで逆転してしまうため）

                if(trim($content[3]) === ""){
                    alert_msg("姓（漢字）を入力してください。", $record_no);
                }
                else if(strlen($content[3]) > 20){
                    alert_msg("姓（漢字）を10文字以内で入力してください。", $record_no);
                }

                if(trim($content[4]) === ""){
                    alert_msg("名（漢字）を入力してください。", $record_no);
                }
                if(strlen($content[4]) > 20){
                    alert_msg("名（漢字）を10文字以内で入力してください。", $record_no);
                }

                if(strlen($content[5]) > 20){
                    alert_msg("姓（カナ）を10文字以内で入力してください。", $record_no);
                }

                if(strlen($content[6]) > 20){
                    alert_msg("名（カナ）を10文字以内で入力してください。", $record_no);
                }

                if(trim($content[7]) === ""){
                    alert_msg("会社名を入力してください。", $record_no);
                }

                if(strlen($content[7]) > 50){
                    alert_msg("会社名を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[8]) > 50){
                    alert_msg("部門名１を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[9]) > 50){
                    alert_msg("部門名２を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[10]) > 50){
                    alert_msg("役職１を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[11]) > 50){
                    alert_msg("役職１を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[12]) > 3){
                    alert_msg("郵便番号１を3文字以内で入力してください。", $record_no);
                }

                if(preg_match("/[^0-9]/",$content[12])){
                    alert_msg("郵便番号１を数字で入力してください。", $record_no);
                }

                if(strlen($content[13]) > 4){
                    alert_msg("郵便番号２を4文字以内で入力してください。", $record_no);
                }

                if(preg_match("/[^0-9]/",$content[13])){
                    alert_msg("郵便番号２を数字で入力してください。", $record_no);
                }

                if($content[14] === "" && trim($values[10]) !== ""){
                    alert_msg("都道府県名を正しく入力してください。", $record_no);
                }

                if(strlen($content[15]) > 50){
                    alert_msg("住所１を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[16]) > 50){
                    alert_msg("住所２を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[17]) > 50){
                    alert_msg("住所３を25文字以内で入力してください。", $record_no);
                }

                if(strlen($content[18]) > 6 || strlen($content[19]) > 6 || strlen($content[20]) > 6){
                    alert_msg("電話を6文字以内で入力してください。", $record_no);
                }

                if(preg_match("/[^0-9]/",$content[18]) || preg_match("/[^0-9]/",$content[19]) || preg_match("/[^0-9]/",$content[20])){
                    alert_msg("電話を数字で入力してください。", $record_no);
                }

                if(strlen($content[21]) > 6 || strlen($content[22]) > 6 || strlen($content[23]) > 6){
                    alert_msg("FAXを6文字以内で入力してください。", $record_no);
                }

                if(preg_match("/[^0-9]/",$content[21]) || preg_match("/[^0-9]/",$content[22]) || preg_match("/[^0-9]/",$content[23])){
                    alert_msg("FAXを数字で入力してください。", $record_no);
                }

                if(strlen($content[25]) > 4 || strlen($content[26]) > 4){
                    alert_msg("携帯／PHSを4文字以内で入力してください。", $record_no);
                }
                if(strlen($content[24]) > 3){
                    alert_msg("携帯／PHS１を4文字以内で入力してください。", $record_no);
                }
                
                if(preg_match("/[^0-9]/",$content[24]) || preg_match("/[^0-9]/",$content[25]) || preg_match("/[^0-9]/",$content[26])){
                    alert_msg("携帯／PHSを数字で入力してください。", $record_no);
                }

                if(strlen($content[27]) > 50){
                    alert_msg("E-MAIL（PC）を25文字以内で入力してください。", $record_no);
                }

                if(!preg_match("|^[0-9a-zA-Z_./?-]+@([0-9a-zA-Z-]+\.)+[0-9a-zA-Z-]+$|",$content[27]) && $content[27] !== ""){
                    alert_msg("正しいE-MAIL（PC）を入力してください。", $record_no);
                }

                if(strlen($content[28]) > 50){
                    alert_msg("E-MAIL（携帯）を25文字以内で入力してください。", $record_no);
                }

                if(!preg_match("|^[0-9a-zA-Z_./?-]+@([0-9a-zA-Z-]+\.)+[0-9a-zA-Z-]+$|",$content[28]) && $content[28] !== ""){
                    alert_msg("正しいE-MAIL（携帯）を入力してください。", $record_no);
                }

                $sql = "insert into address (emp_id, address_id, date, name1, name2, name_kana1, name_kana2, company, department1, department2, post1, post2, postcode1, postcode2, prefectures, address1, address2, address3, tel1, tel2, tel3, fax1, fax2, fax3, mobile1, mobile2, mobile3, email_pc, email_mobile, memo, address_del_flg, shared_flg) values (";
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションのコミット
	if ($result == "t") {
		pg_query($con, "commit");
		pg_close($con);
	}
} else {
	$result = "f";
}

// 画面遷移
echo("<script type=\"text/javascript\">location.href = '$next_php?session=$session&result=$result&shared_flg=$shared_flg';</script>");

// 都道府県名からコードに変換
function to_prefecture_cd($name) {
    $name=trim($name);
    if($name !== ""){
        $prefecture_items = array(
            "北海道","青森県","岩手県","宮城県","秋田県","山形県","福島県"
            ,"茨城県","栃木県","群馬県","埼玉県","千葉県","東京都","神奈川県"
            ,"新潟県","富山県","石川県","福井県"
            ,"山梨県","長野県","岐阜県","静岡県","愛知県"
            ,"三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県"
            ,"鳥取県","島根県","岡山県","広島県","山口県"
            ,"徳島県","香川県","愛媛県","高知県"
            ,"福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県","沖縄県"
        );
        return strval(array_search($name, $prefecture_items));
    }
    else{
        return "";
    }
}

function alert_msg($msg,$record_no){
    ++$record_no;
    echo("<script language='javascript'>alert(\"{$msg} レコード番号:{$record_no}\");</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
?>
