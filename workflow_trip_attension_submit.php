<?php
// 出張旅費
// 注意事項マスタ・フォーム登録
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// 入力項目の確認
if (empty($_REQUEST["attension_id"])) {
	js_alert_exit('IDが入力されていません。');
}
if (empty($_REQUEST["title"])) {
	js_alert_exit('名称が入力されていません。');
}
if (strlen($_REQUEST["attension_id"]) > 20) {
	js_alert_exit('IDは20文字以内に収めて下さい。');
}
if (strlen($_REQUEST["title"]) > 100) {
	js_alert_exit('名称は100文字以内に収めて下さい。');
}


// データベースに接続
$con = connect2db($fname);

// ID重複チェック
if ($_REQUEST["mode"] == 'new') {
	$sql = "SELECT * FROM trip_attension WHERE attension_id='{$_REQUEST["attension_id"]}'";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		js_error_exit();
	}
	if (pg_num_rows($sel) > 0) {
		pg_close($con);
		js_alert_exit('IDが重複しています。');
	}
}

// トランザクションを開始
pg_query($con, "begin");

// 追加
if ($_REQUEST["mode"] == 'new') {
	$sql = "insert into trip_attension (attension_id,title,attension,created_on,updated_on) values (";
	$content = array(
		p($_REQUEST["attension_id"]),
		p($_REQUEST["title"]),
		p($_REQUEST["attension"]),
		date('Y-m-d H:i:s'),
		date('Y-m-d H:i:s')
	);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 更新
else {
	$set = array(
		"title" => p($_REQUEST["title"]),
		"attension" => p($_REQUEST["attension"]),
		"updated_on" => date('Y-m-d H:i:s')
	);
	$sql = "UPDATE trip_attension SET ";
	$cond = "WHERE attension_id='{$_REQUEST["attension_id"]}'";
	$upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

?>
<script type="text/javascript">
location.href = 'workflow_trip_attension.php?session=<?=$session?>';
</script>