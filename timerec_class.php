<?php
ini_set("memory_limit", "512M");
/**
 * タイムレコーダ打刻インポート用クラス
 * class timerec_class
 *
 * Description for class timerec_class
 *
 * @author:
*/
require_once("about_postgres.php");
require_once("timecard_bean.php");
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');

/** 夜間で日跨り、かつ長時間にわたる勤務を特定する条件(N時以降に勤務パターンの開始時間がn時以降) */
define('THRESHOLD_MID_NIGHT_PATTERN_START_HOUR', "12:00");

/** 夜間で日跨り、かつ長時間にわたる勤務を特定する条件(勤務パターンの終了時刻と開始時刻の差) */
define('THRESHOLD_MID_NIGHT_PATTERN_TIMEZONE_NUM', 16);

/** 夜間で日跨り、かつ長時間にわたる勤務を特定する条件(勤務パターンの終了時刻と退勤打刻の時間との差の範囲) */
define('THRESHOLD_MID_NIGHT_PATTERN_TIME_RANGE', 9);

class timerec_class  {

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $mode;		// モード 1:画面 2:バッチ処理
	var $result_msg; //画面用登録結果メッセージ、配列
	var $dberr_msg=""; //DBエラーがあった場合、最後のエラー
	var $arr_atdbkrslt = array(); //実績データ
	var $timecard_bean;	//タイムカード情報

	/** 1日5回までの出退勤を取り込む(true:取り込む　false： 取り込まない) */
	var $import_5time_flg;

	/** 前日の稼動実績に含める時間の閾値の有無(true:閾値となる時間あり　false：閾値なし ) */
	var $has_threshold;

	/** 前日の稼動実績に含める時間の閾値( 0500  の場合  4:59 ⇒ 前日、5:00 ⇒ 当日) */
	var $timecard_base_time;

	// 連携データ処理時の職員マッチングキー
	var $matching_key;

	/**
	 * timerec_class constructor
	 *
	 * @param
	 */
	function timerec_class($con, $fname) {

		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;

		//タイムカード情報の取得
		$this->timecard_bean = new timecard_bean();
		$this->timecard_bean->select($con, $fname);

		// system_config 読込み
		$conf = new Cmx_SystemConfig();
		$this->import_5time_flg = ($conf->get('timecard.import_5time_flg')=='t')? true : false;

		if(is_null($conf->get('timecard.import_base_time'))) {
			$this->has_threshold = false;
			$this->timecard_base_time = null;
		}else{
			$this->has_threshold = true;
			$this->timecard_base_time = sprintf('%02d', $conf->get('timecard.import_base_time')).'00';  // '5' -> '0500'
		}
	}

	/**
	 * 打刻データインポート処理
	 *
	 * @param string $csv_file_name データファイル名
	 * @param string $mode 処理モード 1:画面 2:バッチ処理
	 * @param string $encoding 文字コード 1:SJIS 2:EUC 3:UTF-8
	 * @param string $maker_flg タイムレコーダの種別 1:マックス 2:アマノ 3:Fitware 4:セコム
	 * @return 処理結果 0:正常終了 1:パラメータエラー -1:DBエラー（$timerec->get_dberr_msg()でDBエラー内容取得）
	 * 　　　　　$timerec->get_result_msg()で画面用の処理結果を取得できる。
	 */
 	function dakoku_file($csv_file_name, $mode, $encoding, $maker_flg="1") {
		if ($csv_file_name == "" || $mode == "" || $encoding == "") {
			return 1;
		}
        $this->mode = $mode;
        //勤務パターン10以外を休暇として使用している場合の対応（綾瀬循環器病院様用） 20130725
        $sql = "select prf_org_cd from profile";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            $this->dberr_msg = pg_last_error($this->_db_con);
            if ($this->mode == "2") {
                return -1;
            }
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $org_cd = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "prf_org_cd") : "";

		//出退区分（アマノ用、コードから文字列を特定）
		$arr_dakoku_kubun = array("", "出勤", "退勤", "外出", "復帰"); //4戻り=復帰
		//出退区分（Fitware用、コードから文字列を特定）
		$arr_dakoku_kubun2 = array("", "出勤", "外出", "復帰", "退勤");


		// 文字コードの設定
		switch ($encoding) {
			case "1":
				$file_encoding = "SJIS";
				break;
			case "2":
				$file_encoding = "EUC-JP";
				break;
			case "3":
				$file_encoding = "UTF-8";
				break;
			default:
				return 1;
		}
		// EUC-JPで扱えない文字列は「・」に変換するよう設定
		mb_substitute_character(0x30FB);

		//勤務パターンと事由の組合せ
		$arr_atdptn_reason = $this->get_atdptn_reason();

		// CSVデータを配列に格納
		$shifts = array();
		$no = 0;

		//行番号の配列、エラーメッセージ用
		$arr_lineno = array();
		// 職員ID
		$emp_personal_ids = array();

		//データの開始日、終了日
		$start_date = "";
		$end_date = "";

		$lines = file($csv_file_name);
		foreach ($lines as $line) {

			// 先頭に、BOMがある場合は除く 20100301
			if ($no == 0) {
				$wk_bom1 = ord(substr($line, 0, 1));
				$wk_bom2 = ord(substr($line, 1, 1));
				$wk_bom3 = ord(substr($line, 2, 1));

				if ($wk_bom1 == 0xef && $wk_bom2 == 0xbb && $wk_bom3 == 0xbf) {
					$line = substr($line, 3);
				}
			}
			// 文字コードを変換
			$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

			// EOFを削除
			$line = str_replace(chr(0x1A), "", $line);

			// 空行は無視
			if ($line == "") {
				continue;
			}

			//タイムカード種別が1:マックスの場合
			if ($maker_flg == "1") {
				// カンマで分割し配列へ設定
				$shift_data = split(",", $line);
			}
			//2:アマノの場合
			else if ($maker_flg == "2" ){
				//固定長のデータから各項目を取得し配列へ設定
				$shift_data = array();
				//端末番号
				$shift_data[0] = intval(substr($line, 30,  2));
				//職員マッチングキー
				$shift_data[1] = ltrim(substr($line, 18, 10), "0");
				//打刻日時
				$wk_datetime = substr($line, 2, 12);
				$shift_data[2] = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $wk_datetime);
				//打刻区分
				$wk_idx = intval(substr($line, 16, 2));
				$shift_data[3] = ($wk_idx >= 1 && $wk_idx <= 4) ? $arr_dakoku_kubun[$wk_idx] : "";
			}
			//3:Fitwareの場合
			else if ($maker_flg == "3" ) {
				//CSVのデータから各項目を取得し配列へ設定
				$shift_data = array();
				// カンマで分割し配列へ設定、フォーマットの違いを吸収
				$wk_csv_data = split(",", $line);
				//端末番号
				$shift_data[0] = $wk_csv_data[2];
				//職員マッチングキー
				$shift_data[1] = $wk_csv_data[0];
				//打刻日時
				$wk_datetime = $wk_csv_data[3].substr($wk_csv_data[4], 0, 4);
				$shift_data[2] = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $wk_datetime);
				//打刻区分
				$wk_idx = intval($wk_csv_data[5]);
				$shift_data[3] = ($wk_idx >= 0 && $wk_idx <= 4) ? $arr_dakoku_kubun2[$wk_idx] : "";
			}
			//4:セコムの場合
			else {
				//CSVのデータから各項目を取得し配列へ設定
				$shift_data = array();
				// カンマで分割し配列へ設定、フォーマットの違いを吸収
				$wk_csv_data = split(",", $line);
				//ダミー
				$shift_data[0] = "";
				//職員ID
				$shift_data[1] = $wk_csv_data[0];
				//打刻日時
				$shift_data[2] = $wk_csv_data[1]." ".substr($wk_csv_data[3], 0, 5);;
				//打刻区分
				$wk_idx = intval($wk_csv_data[2]);
				$shift_data[3] = ($wk_idx >= 0 && $wk_idx <= 2) ? $arr_dakoku_kubun[$wk_idx] : "";
			}

			$shifts[$no] = $shift_data;
			$wk_id = $shift_data[1];
			$emp_personal_ids[$wk_id] = $wk_id;
			//年月日
			if ($shift_data[2] != "") {
				list($wk_ymd, $wk_time) = split(" ", $shift_data[2]);
				list($wk_yyyy, $wk_mm, $wk_dd) = split("/", $wk_ymd);
				$wk_date = sprintf("%04d%02d%02d",$wk_yyyy, $wk_mm, $wk_dd);
				if ($start_date == "" ||
						$wk_date < $start_date) {
					$start_date = $wk_date;
				}
				if ($end_date == "" ||
						$wk_date > $end_date) {
					$end_date = $wk_date;
				}
			}
			//行番号配列
			$arr_lineno[] = $no;
			$no++;

		}
		// 複数日分のデータがある場合、日時でソートする 20110512
		if ($start_date != $end_date) {
			$arr_dt = array();
			foreach ($shifts as $no => $shift_data) {
				list($wk_ymd, $wk_hhmm) = split(" ", $shift_data[2]);
				list($wk_yyyy, $wk_mm, $wk_dd) = split("/", $wk_ymd);
				list($wk_hh, $wk_min) = split(":", $wk_hhmm);
				$datetime = sprintf("%04d%02d%02d%02d%02d",$wk_yyyy, $wk_mm, $wk_dd,$wk_hh, $wk_min);
				$arr_dt[$no] = $datetime;
			}
			array_multisort($arr_dt, SORT_ASC, $shifts, $arr_lineno);
		}

		// トランザクションを開始
		pg_query($this->_db_con, "begin");
		//Fitwareの場合はカード製造番号使用
		if ($maker_flg == "3") {
			$matching_key = "2";
		}
		else {
			//職員マッチングキーをDBから取得
			$matching_key = $this->get_matching_key();
			$this->matching_key = $matching_key; // 20140922
		}
		if ($this->get_dberr_msg() != "") {
			return -1;
		}
		//職員情報
		$arr_emp_id = $this->get_emp_id($emp_personal_ids, $matching_key, $maker_flg);
		if ($this->get_dberr_msg() != "") {
			return -1;
		}

		//予約データ取得
		$arr_atdbk = $this->get_arr_atdbk("atdbk", $arr_emp_id, $start_date, $end_date);
		if ($this->get_dberr_msg() != "") {
			return -1;
		}
		//実績データ取得
		$this->arr_atdbkrslt = $this->get_arr_atdbk("atdbkrslt", $arr_emp_id, $start_date, $end_date);
		if ($this->get_dberr_msg() != "") {
			return -1;
		}

		//処理件数
		$reg_cnt = 0;
		$err_cnt = 0;

		//メッセージ
		$this->result_msg = array();
		$this->result_msg[] = "インポート処理開始<br>";
		foreach ($shifts as $no => $shift_data) {

			$rtn_info = $this->update_shift_data($shift_data, $arr_emp_id, $arr_atdbk, $arr_atdptn_reason, $maker_flg, $org_cd);
			if ($this->get_dberr_msg() != "") {
				return -1;
			}

			//エラー情報設定
			if ($rtn_info["rtncd"] == 0) {
				$reg_cnt++;
			} elseif ($rtn_info["rtncd"] == 1) {
				list($wk_ymd, $wk_hhmm) = split(" ", $shift_data[2]);
				list($wk_yyyy, $wk_mm, $wk_dd) = split("/", $wk_ymd);
				$arr_msg = $rtn_info["errmsg"];
				for ($i=0; $i<count($arr_msg); $i++) {
//					$err_data = implode(",", $shift_data);
					//行番号配列から
					$wk_no = $arr_lineno[$no];
					$err_data = $lines[$wk_no];
					// 先頭に、BOMがある場合は除く 20100301
					if ($wk_no == 0) {
						$wk_bom1 = ord(substr($err_data, 0, 1));
						$wk_bom2 = ord(substr($err_data, 1, 1));
						$wk_bom3 = ord(substr($err_data, 2, 1));

						if ($wk_bom1 == 0xef && $wk_bom2 == 0xbb && $wk_bom3 == 0xbf) {
							$err_data = substr($err_data, 3);
						}
					}
					// 文字コードを変換
					$err_data = trim(mb_convert_encoding($err_data, "EUC-JP", $file_encoding));

					// EOFを削除
					$err_data = str_replace(chr(0x1A), "", $err_data);
					$wk_line_no = $wk_no+1;
					$this->result_msg[] = "{$wk_line_no}行目：".$arr_msg[$i]." ".$err_data."<br>\n";
					//エラーデータ登録
					if (!checkdate($wk_mm, $wk_dd, $wk_yyyy)) {
						$wk_date = date("Ymd");
					} else {
						$wk_date = sprintf("%04d%02d%02d",$wk_yyyy, $wk_mm, $wk_dd);
					}
					//職員ID
					if ($matching_key == "1") {
						if($maker_flg == "1") {
							$wk_emp_personal_id =  $shift_data[1];
						} else {
							//アマノの場合はマスタの職員ID
							//職員マスタに未登録の場合は、データの職員ID
							if ($arr_emp_id["$shift_data[1]"]["emp_personal_id"] == "") {
								$wk_emp_personal_id =  $shift_data[1];
							} else {
								$wk_emp_personal_id = $arr_emp_id["$shift_data[1]"]["emp_personal_id"];
							}
						}
					} else {
						//職員マスタに未登録の場合は、カード製造番号
						if ($arr_emp_id["$shift_data[1]"]["emp_personal_id"] == "") {
							$wk_emp_personal_id =  $shift_data[1];
						} else {
							$wk_emp_personal_id = $arr_emp_id["$shift_data[1]"]["emp_personal_id"];
						}
					}
					$this->insert_import_error($wk_date, $wk_emp_personal_id, $err_data, $arr_msg[$i]);
					if ($this->get_dberr_msg() != "") {
						return -1;
					}
				}
				$err_cnt++;
			}
			//$rtn_info["rtncd"] == 2は処理なし
		}

		// トランザクションをコミット
		pg_query($this->_db_con, "commit");

		$this->result_msg[] = "インポート処理終了<br>";
		$this->result_msg[] = "正常登録：{$reg_cnt}件<br>";
		$this->result_msg[] = "エラー　：{$err_cnt}件<br>";

		return 0;
	}

	/**
	 * インポートデータ登録
	 *
	 * @param array $shift_data 入力データの配列
	 * @param array $arr_emp_id 職員情報
	 * @param array $arr_atdbk 予定情報
	 * @param array $arr_atdptn_reason 勤務パターングループ、勤務パターンをキーとする事由の配列
	 * @param string $maker_flg 1:マックス 2:アマノ 3:Fitware 4:セコム
     * @param string $org_cd 医療機関コード 勤務パターン10以外を休暇として使用している場合の対応（綾瀬循環器病院様用） 20130725
	 * @return array 結果情報の配列。キーrtncd 0:正常終了 1:エラー errmsg エラーメッセージの配列
	 *
	 */
	function update_shift_data($shift_data, $arr_emp_id, $arr_atdbk, $arr_atdptn_reason, $maker_flg, $org_cd)
	{

		$arr_msg = array();
		$rtn_info = array();
		$rtn_info["rtncd"] = 0;

		//入力データチェック
		//カラム数チェック
		$column_count = 4;
		if (count($shift_data) != $column_count) {
			$rtn_info["rtncd"] = 1;
			$arr_msg[] = "カラム数エラー";
			$rtn_info["errmsg"] = $arr_msg;
			return $rtn_info;
		}

		//予約データ確認
		//職員ID確認
		$emp_personal_id = $shift_data[1];
		//Fitwareの場合は先頭１桁を除く
		if ($maker_flg == "3") {
			$emp_personal_id = substr($emp_personal_id, 1, 15);
		}

		if ($this->matching_key == "2" && (strlen($emp_personal_id) == 4) && ($org_cd == '0410210017')) { // 20140911
			// 職員マッチングキー=カードid、かつ、職員idが4桁の場合はログインidと解釈し職員のログインidと付き合わせる(石巻様特別処理)
			$emp_id = "";
			foreach ($arr_emp_id as $key => $emp){
				if($emp_personal_id==$emp['emp_login_id']){
					$emp_id = $emp['emp_id'];
				}
			}
		}else{
			$emp_id = $arr_emp_id["$emp_personal_id"]["emp_id"];
		}

		if ($emp_id == "") {
			$rtn_info["rtncd"] = 1;
			$arr_msg[] = "職員未登録エラー";
		}
		//日付 範囲、妥当性
		if ($shift_data[2] == "") {
			$rtn_info["rtncd"] = 1;
			$arr_msg[] = "打刻日時未設定エラー";
		} else {
			list($wk_ymd, $wk_hhmm) = split(" ", $shift_data[2]);
			list($wk_yyyy, $wk_mm, $wk_dd) = split("/", $wk_ymd);
			list($wk_hh, $wk_min) = split(":", $wk_hhmm);
			// $date -> タイムレコードデータ, インポートデータの「打刻日」
			$date = sprintf("%04d%02d%02d",$wk_yyyy, $wk_mm, $wk_dd);
			// $time -> タイムレコードデータ, インポートデータの「打刻時刻」
			$time = sprintf("%02d%02d",$wk_hh, $wk_min);
			if (!checkdate($wk_mm, $wk_dd, $wk_yyyy) ||
					($wk_hh < 0 || $wk_hh > 23 || $wk_min < 0 || $wk_min > 59)) {
				$rtn_info["rtncd"] = 1;
				$arr_msg[] = "打刻日時エラー";
			}
		}

		//打刻区分確認 戻り追加 20110419
		$arr_ptn_str = array("出勤", "退勤", "呼出", "呼退", "退復", "復退", "外出", "復帰", "戻り");


		//変換設定に存在しない
		if (!in_array($shift_data[3],$arr_ptn_str)) {
			$rtn_info["rtncd"] = 1;
			$arr_msg[] = "打刻区分エラー";
		}

		if ($rtn_info["rtncd"] == 1) {
			$rtn_info["errmsg"] = $arr_msg;
			return $rtn_info;
		}

		//エラーチェックと更新処理
		$data_atdbk = $arr_atdbk["$emp_id"]["$date"];
		if ($data_atdbk["pattern"] != "") {
			$pattern = $data_atdbk["pattern"];
		}
		//出勤予定未登録時の標準の勤務パターンを使用
		else {
			$pattern = $arr_emp_id["$emp_personal_id"]["atdptn_id"];
		}
		if ($data_atdbk["reason"] != "") {
			$reason = $data_atdbk["reason"];
		}
		if ($data_atdbk["night_duty"] != "") {
			$night_duty = $data_atdbk["night_duty"];
		}
		if ($data_atdbk["allow_id"] != "") {
			$allow_id = $data_atdbk["allow_id"];
		}
		if ($data_atdbk["tmcd_group_id"] != "") {
			$tmcd_group_id = $data_atdbk["tmcd_group_id"];
		}
		//グループが未設定の場合、職員登録＞勤務条件の出勤グループから設定
		if ($tmcd_group_id == "") {
			$tmcd_group_id = $arr_emp_id["$emp_personal_id"]["tmcd_group_id"];
		}
		//事由が未設定で時間帯の組合せにある場合、自動設定
		if ($reason == "" && $arr_atdptn_reason[$tmcd_group_id][$pattern] != "") {
			$reason = $arr_atdptn_reason[$tmcd_group_id][$pattern];
		}
		// 対象日の勤務実績
		$data_atdbkrslt = $this->arr_atdbkrslt["$emp_id"]["$date"];
		// 対象日前日の勤務実績
		$last_date = date("Ymd", strtotime("-1 day", $this->to_timestamp($date)));
		$last_atdbkrslt = $this->arr_atdbkrslt["$emp_id"]["$last_date"];
		//退復、復退の数取得。登録済がある場合次に設定する
		$taifuku_cnt = 0;
		$arr_o_start_time = array();
		$arr_o_end_time = array();
		for ($i=1 ; $i<=10; $i++) {
			$wk_key1 = "o_start_time".$i;
			if ($data_atdbkrslt["$wk_key1"] != "") {
				array_push($arr_o_start_time, $data_atdbkrslt["$wk_key1"]);
			}
			$wk_key2 = "o_end_time".$i;
			if ($data_atdbkrslt["$wk_key2"] == "") {
				break;
			} else {
				array_push($arr_o_end_time, $data_atdbkrslt["$wk_key2"]);
			}
			$taifuku_cnt++;
		}

		//打刻区分
		switch ($shift_data[3]) {
			case "出勤":
				$status = "1";
				if(($this->has_threshold==true) && ($time < $this->timecard_base_time)){

					/**
					 * 下記条件の場合は前日からの続きの勤務とせず(出勤打刻2?5にセットしない)、当日の出勤打刻扱いとして登録・更新する
					 *
					 *［判定する条件］
					 *
					 * (1)出勤打刻時間が0:00?閾値の間の時間で、かつ、
					 * (2)当日の勤務パターン有で、かつ、
					 * (3)当日が深夜残業(開始打刻時間が0:00前後の勤務パターン)
					 *
					 */

					// 勤務実績または、勤務予定のいずれかから、当日の勤務パターン取得
					$today_pattern = "";
					$today_previous_day_possible_flag = "";

					if(count($data_atdbkrslt)== 0){
						$today_atdbk = $arr_atdbk["$emp_id"]["$date"];
						if(count($last_atdbk)>=0){
							$today_pattern = $today_atdbk['pattern'];
							$today_previous_day_possible_flag = $today_atdbk["a_previous_day_possible_flag"];
						}
					}else{
						$today_pattern = $data_atdbkrslt['pattern'];
						$today_previous_day_possible_flag = $data_atdbkrslt["a_previous_day_possible_flag"];
					}

					if($today_pattern!="" && $today_previous_day_possible_flag=="1"){
						// 当日の出勤打刻扱いとして登録または更新する(前日フラグ:off)
						if(count($data_atdbkrslt)== 0){
							$this->insertAtdbkRslt($emp_id, $date, $time, $date, $today_atdbk, "0");
						}else{
							$this->updateAtdbkRslt($emp_id, $date, $time, $date, $data_atdbkrslt, "0");
						}
						continue;
					}

					// 前日の稼動実績に含める時間の閾値を適用する場合、閾値の時間より早い時間の実績は前日の稼動実績として登録する
					if(count($last_atdbkrslt)== 0) {
						// 前日の稼動実績を登録
						if ($next_atdbk["pattern"] != "") {
							$pattern = $next_atdbk["pattern"];
						}
						//出勤予定未登録時の標準の勤務パターンを使用
						else {
							$pattern = $arr_emp_id["$emp_personal_id"]["atdptn_id"];
						}
						$reason = ""; //20111115 前日の事由が設定される不具合
						if ($next_atdbk["reason"] != "") {
							$reason = $next_atdbk["reason"];
						}
						$night_duty = "";
						if ($next_atdbk["night_duty"] != "") {
							$night_duty = $next_atdbk["night_duty"];
						}
						$allow_id = null; //20111122 ""ではインサート時にエラーとなるため
						if ($next_atdbk["allow_id"] != "") {
							$allow_id = $next_atdbk["allow_id"];
						}
						$tmcd_group_id = "";
						if ($next_atdbk["tmcd_group_id"] != "") {
							$tmcd_group_id = $next_atdbk["tmcd_group_id"];
						}
						//グループが未設定の場合、職員登録＞勤務条件の出勤グループから設定
						if ($tmcd_group_id == "") {
							$tmcd_group_id = $arr_emp_id["$emp_personal_id"]["tmcd_group_id"];
						}
						//事由が未設定で時間帯の組合せにある場合、自動設定
						if ($reason == "" && $arr_atdptn_reason[$tmcd_group_id][$pattern] != "") {
							$reason = $arr_atdptn_reason[$tmcd_group_id][$pattern];
						}

						$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, start_time, start_btn_time, reg_prg_flg, start_date, previous_day_flag, start_btn_date1, status ";
						$sql .= ") values (";
						$content = array($emp_id, $last_date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $last_date, '0', $date, $status);
						$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
						if ($ins == 0) {
							$this->dberr_msg = pg_last_error($this->_db_con);
							pg_query($this->_db_con,"rollback");
							if ($this->mode == "2") {
								return -1;
							}
							pg_close($this->_db_con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["date"] = $last_date;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["pattern"] = $pattern;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["reason"] = $reason;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["tmcd_group_id"] = $tmcd_group_id;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["start_time"] = $time;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["start_btn_time"] = $time;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["previous_day_flag"] = '0';
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["start_btn_date1"] = $date;
					}else{
						// 前日の稼動実績を更新
						//時刻設定なし　更新
						if ($last_atdbkrslt["start_time"] == "") {
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["start_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["start_btn_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["start_btn_date1"] = $date;
							$set = array("start_time", "start_btn_time", "reg_prg_flg", "start_date", "previous_day_flag", "start_btn_date1", "status");
							$setvalue = array($time, $time, '1', $last_date, '1', $date, $status);
							//勤務パターン等を予定から実績へコピー
							if ($last_atdbkrslt["pattern"] == "" && $pattern != "") {
								array_push($set, "pattern");
								array_push($setvalue, $pattern);
								$this->arr_atdbkrslt["$emp_id"]["$last_date"]["pattern"] = $pattern;
							}
							if ($last_atdbkrslt["reason"] == "" && $reason != "") {
								array_push($set, "reason");
								array_push($setvalue, $reason);
								$this->arr_atdbkrslt["$emp_id"]["$last_date"]["reason"] = $reason;
							}
							if ($last_atdbkrslt["night_duty"] == "" && $night_duty != "") {
								array_push($set, "night_duty");
								array_push($setvalue, $night_duty);
								$this->arr_atdbkrslt["$emp_id"]["$last_date"]["night_duty"] = $night_duty;
							}
							if ($last_atdbkrslt["allow_id"] == "" && $allow_id != "") {
								array_push($set, "allow_id");
								array_push($setvalue, $allow_id);
								$this->arr_atdbkrslt["$emp_id"]["$last_date"]["allow_id"] = $allow_id;
							}
							if ($last_atdbkrslt["tmcd_group_id"] == "" && $tmcd_group_id != "") {
								array_push($set, "tmcd_group_id");
								array_push($setvalue, $tmcd_group_id);
								$this->arr_atdbkrslt["$emp_id"]["$last_date"]["tmcd_group_id"] = $tmcd_group_id;
							}

							if($this->import_5time_flg){
								if($this->tcTimeInBtnTimeList(true, $last_atdbkrslt, $time)){
									// 勤務実績を更新しない
									$rtn_info["rtncd"] = 2;
								}else{
									// 上限まで打刻可能なら]勤務実績.開始時刻に追加
									if(!$this->updateByTcTime($emp_id, true, $last_atdbkrslt, $time, $date)){
										$rtn_info["rtncd"] = 1;
										$arr_msg[] = "打刻上限エラー";
									}
								}
							}else{

								$this->update_atdbkrslt_column($set, $setvalue, $emp_id, $last_date);
								if ($this->get_dberr_msg() != "") {
									return -1;
								}
							}

						} else {
							if ($last_atdbkrslt["start_time"] == $time) {
								//時刻一致　処理なし
								$rtn_info["rtncd"] = 2;
							}else{
								if($this->import_5time_flg){
									if($this->tcTimeInBtnTimeList(true, $last_atdbkrslt, $time)){
										// 勤務実績を更新しない
										$rtn_info["rtncd"] = 2;
									}else{
										// 上限まで打刻可能なら]勤務実績.開始時刻に追加
										if(!$this->updateByTcTime($emp_id, true, $last_atdbkrslt, $time, $date)){
											$rtn_info["rtncd"] = 1;
											$arr_msg[] = "打刻上限エラー";
										}
									}
								}else{
									//時刻不一致　重複エラー
									$rtn_info["rtncd"] = 1;
									$arr_msg[] = "重複エラー";
								}
							}
						}
					}
				}else{
					//時間帯設定の勤務開始時刻が前日であるの設定か前日になる場合があるの設定を確認し、ある場合は翌日へ登録 20110413
					//翌日
					$next_date = date("Ymd", strtotime("+1 day", $this->to_timestamp($date)));
					$next_atdbk = $arr_atdbk["$emp_id"]["$next_date"];

					//前日勤務パターンがある場合、終了予定時刻より後か確認
					$after_end_time_flg = false;
					if ($next_atdbk["a_previous_day_possible_flag"] == "1" ||
							$next_atdbk["a_previous_day_flag"] == "1") {

						//休暇か未設定の場合 20111011
						if ($pattern == "10" || $pattern == "") {
							//時間帯設定の閾値を確認 20141211
							$previous_day_threshold_hour = $next_atdbk["a_previous_day_threshold_hour"];
							if ($previous_day_threshold_hour == "") {
								$previous_day_threshold_hour = "22";
							}
							$previous_day_threshold_time = $previous_day_threshold_hour."00";
							if ($time >= $previous_day_threshold_time) {
								//翌日データに登録できるようにフラグ設定
								$after_end_time_flg = true;
							}
							//当日データとする
							else {
								$after_end_time_flg = false;
							}
						}
	                    //当日の予定あり、実績なしの場合は、まず当日に更新 20121105
	                    elseif ($pattern != "" &&
	                            $data_atdbkrslt["pattern"] == "" &&
	                            $org_cd != "0002170231" //綾瀬循環器病院様を除く 20130725
	                            ) {
	                        $after_end_time_flg = false;
	                    }
						//休暇以外、未設定以外
						elseif ($pattern != "10" && $pattern != "" && $tmcd_group_id != "") {
							// 勤務実績の前日フラグの状態も見た上で当日の出勤時刻と同じ場合は登録済みと判断する、登録済みとして翌日へ登録しない 20140917 (打刻日付も確認する 20150202)
	                        if ($data_atdbkrslt["start_time"] == $time && $data_atdbkrslt["start_date"] == $next_date && $data_atdbkrslt["previous_day_flag "]=="1") {
	                            $rtn_info["rtncd"] = 2;
	                            return $rtn_info;
	                        }
							//終了予定時刻取得
	                        $sql =	"select officehours2_start, officehours2_end from officehours ";
							$cond = "WHERE tmcd_group_id = $tmcd_group_id and pattern = '$pattern' ";
							$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
							if ($sel == 0) {
								$this->dberr_msg = pg_last_error($this->_db_con);
								pg_query($this->_db_con, "rollback");
								if ($this->mode == "2") {
									return -1;
								}
								pg_close($this->_db_con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$officehours2_start = "";
							$officehours2_end = "";
							if (pg_num_rows($sel) > 0) {
	                            $officehours2_start = pg_fetch_result($sel, 0, "officehours2_start");
	                            $officehours2_end = pg_fetch_result($sel, 0, "officehours2_end");
							}
							if ($officehours2_end != "") {
		                        $wk_officehours2_start = substr($officehours2_start, 0, 2).substr($officehours2_start, 3, 2);
		                        $wk_officehours2_end = substr($officehours2_end, 0, 2).substr($officehours2_end, 3, 2);
		                        //日またがり対応 20140729 準夜の判断追加 20160106
		                        if ($wk_officehours2_end < $wk_officehours2_start &&
		                        	$wk_officehours2_start >= "1500" &&
		                        	$wk_officehours2_start <= "1800") {
		                            $wk_end_hour = intval(substr($officehours2_end, 0, 2), 10) + 24;
		                            $wk_officehours2_end = sprintf("%02d", $wk_end_hour).substr($officehours2_end, 3, 2);
		                        }
	                        }
	                        else {
								//時間帯設定の閾値を確認 20141211
								$previous_day_threshold_hour = $next_atdbk["a_previous_day_threshold_hour"];
								if ($previous_day_threshold_hour == "") {
									$previous_day_threshold_hour = "22";
								}
								$wk_officehours2_end = $previous_day_threshold_hour."00";
	                        }
	                        if ($time > $wk_officehours2_end) {
								$after_end_time_flg = true;
							}
						}

					}
					//前日開始の場合、時刻確認
					if (($next_atdbk["a_previous_day_possible_flag"] == "1" ||
								$next_atdbk["a_previous_day_flag"] == "1") && $after_end_time_flg) {
						$next_atdbkrslt = $this->arr_atdbkrslt["$emp_id"]["$next_date"];
						if ($next_atdbk["pattern"] != "") {
							$pattern = $next_atdbk["pattern"];
						}
						//出勤予定未登録時の標準の勤務パターンを使用
						else {
							$pattern = $arr_emp_id["$emp_personal_id"]["atdptn_id"];
						}
						$reason = ""; //20111115 前日の事由が設定される不具合
						if ($next_atdbk["reason"] != "") {
							$reason = $next_atdbk["reason"];
						}
						$night_duty = "";
						if ($next_atdbk["night_duty"] != "") {
							$night_duty = $next_atdbk["night_duty"];
						}
						$allow_id = null; //20111122 ""ではインサート時にエラーとなるため
						if ($next_atdbk["allow_id"] != "") {
							$allow_id = $next_atdbk["allow_id"];
						}
						$tmcd_group_id = "";
						if ($next_atdbk["tmcd_group_id"] != "") {
							$tmcd_group_id = $next_atdbk["tmcd_group_id"];
						}
						//グループが未設定の場合、職員登録＞勤務条件の出勤グループから設定
						if ($tmcd_group_id == "") {
							$tmcd_group_id = $arr_emp_id["$emp_personal_id"]["tmcd_group_id"];
						}
						//事由が未設定で時間帯の組合せにある場合、自動設定
						if ($reason == "" && $arr_atdptn_reason[$tmcd_group_id][$pattern] != "") {
							$reason = $arr_atdptn_reason[$tmcd_group_id][$pattern];
						}
						//実績データなしの場合に作成
						if (count($next_atdbkrslt) == 0) {
							$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, start_time, start_btn_time, reg_prg_flg, start_date, previous_day_flag, start_btn_date1, status ";
							$sql .= ") values (";
							$content = array($emp_id, $next_date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $next_date, '1', $date, $status);
							$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
							if ($ins == 0) {
								$this->dberr_msg = pg_last_error($this->_db_con);
								pg_query($this->_db_con,"rollback");
								if ($this->mode == "2") {
									return -1;
								}
								pg_close($this->_db_con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["date"] = $next_date;
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["pattern"] = $pattern;
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["reason"] = $reason;
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["tmcd_group_id"] = $tmcd_group_id;
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["start_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["start_btn_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["previous_day_flag"] = '1';
							$this->arr_atdbkrslt["$emp_id"]["$next_date"]["start_btn_date1"] = $date;
						}
						//実績データあり
						else {
							//時刻設定なし　更新
							if ($next_atdbkrslt["start_time"] == "") {
								$this->arr_atdbkrslt["$emp_id"]["$next_date"]["start_time"] = $time;
								$this->arr_atdbkrslt["$emp_id"]["$next_date"]["start_btn_time"] = $time;
								$this->arr_atdbkrslt["$emp_id"]["$next_date"]["start_btn_date1"] = $date;
								$set = array("start_time", "start_btn_time", "reg_prg_flg", "start_date", "previous_day_flag", "start_btn_date1");
								$setvalue = array($time, $time, '1', $next_date, '1', $date);
								//勤務パターン等を予定から実績へコピー
								if ($next_atdbkrslt["pattern"] == "" && $pattern != "") {
									array_push($set, "pattern");
									array_push($setvalue, $pattern);
									$this->arr_atdbkrslt["$emp_id"]["$next_date"]["pattern"] = $pattern;
								}
								if ($next_atdbkrslt["reason"] == "" && $reason != "") {
									array_push($set, "reason");
									array_push($setvalue, $reason);
									$this->arr_atdbkrslt["$emp_id"]["$next_date"]["reason"] = $reason;
								}
								if ($next_atdbkrslt["night_duty"] == "" && $night_duty != "") {
									array_push($set, "night_duty");
									array_push($setvalue, $night_duty);
									$this->arr_atdbkrslt["$emp_id"]["$next_date"]["night_duty"] = $night_duty;
								}
								if ($next_atdbkrslt["allow_id"] == "" && $allow_id != "") {
									array_push($set, "allow_id");
									array_push($setvalue, $allow_id);
									$this->arr_atdbkrslt["$emp_id"]["$next_date"]["allow_id"] = $allow_id;
								}
								if ($next_atdbkrslt["tmcd_group_id"] == "" && $tmcd_group_id != "") {
									array_push($set, "tmcd_group_id");
									array_push($setvalue, $tmcd_group_id);
									$this->arr_atdbkrslt["$emp_id"]["$next_date"]["tmcd_group_id"] = $tmcd_group_id;
								}
								$this->update_atdbkrslt_column($set, $setvalue, $emp_id, $next_date);
								if ($this->get_dberr_msg() != "") {
									return -1;
								}

							} else {
								if ($next_atdbkrslt["start_time"] == $time) {
									//時刻一致　処理なし
									$rtn_info["rtncd"] = 2;
								}else{
									if($this->import_5time_flg){
										if($this->tcTimeInBtnTimeList(true, $next_atdbkrslt, $time)){
											// 勤務実績を更新しない
											$rtn_info["rtncd"] = 2;
										}else{
											// 上限まで打刻可能なら]勤務実績.開始時刻に追加
											if(!$this->updateByTcTime($emp_id, true, $next_atdbkrslt, $time, $date)){
												$rtn_info["rtncd"] = 1;
												$arr_msg[] = "打刻上限エラー";
											}
										}
									}else{
										//時刻不一致　重複エラー
										$rtn_info["rtncd"] = 1;
										$arr_msg[] = "重複エラー";
									}
								}
							}
						}
					}
					//通常のケース、勤務開始時刻が前日設定以外
					else {
						//実績データなし　作成
						if (count($data_atdbkrslt) == 0) {
							$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, start_time, start_btn_time, reg_prg_flg, start_date, start_btn_date1, status ";
							$sql .= ") values (";
							$content = array($emp_id, $date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $date, $date, $status);
							$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
							if ($ins == 0) {
								$this->dberr_msg = pg_last_error($this->_db_con);
								pg_query($this->_db_con,"rollback");
								if ($this->mode == "2") {
									return -1;
								}
								pg_close($this->_db_con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$this->arr_atdbkrslt["$emp_id"]["$date"]["date"] = $date;
							$this->arr_atdbkrslt["$emp_id"]["$date"]["pattern"] = $pattern;
							$this->arr_atdbkrslt["$emp_id"]["$date"]["reason"] = $reason;
							$this->arr_atdbkrslt["$emp_id"]["$date"]["tmcd_group_id"] = $tmcd_group_id;
							$this->arr_atdbkrslt["$emp_id"]["$date"]["start_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$date"]["start_btn_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$date"]["start_btn_date1"] = $date;

						}
						//実績データあり
						else {
							//時刻設定なし　更新
							if ($data_atdbkrslt["start_time"] == "") {
								//$set配列への設定を変更 1日5回まで取り込む場合の勤務パターン等のコピー用20141016
								if($this->import_5time_flg){
									$set = array();
									$setvalue = array();
								}
								else {
									$this->arr_atdbkrslt["$emp_id"]["$date"]["start_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["start_btn_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["start_btn_date1"] = $date;
									$set = array("start_time", "start_btn_time", "reg_prg_flg", "start_date", "start_btn_date1", "status");
									$setvalue = array($time, $time, '1', $date, $date, $status);
								}
								//勤務パターン等を予定から実績へコピー
								if ($data_atdbkrslt["pattern"] == "" && $pattern != "") {
									array_push($set, "pattern");
									array_push($setvalue, $pattern);
									$this->arr_atdbkrslt["$emp_id"]["$date"]["pattern"] = $pattern;
								}
								if ($data_atdbkrslt["reason"] == "" && $reason != "") {
									array_push($set, "reason");
									array_push($setvalue, $reason);
									$this->arr_atdbkrslt["$emp_id"]["$date"]["reason"] = $reason;
								}
								if ($data_atdbkrslt["night_duty"] == "" && $night_duty != "") {
									array_push($set, "night_duty");
									array_push($setvalue, $night_duty);
									$this->arr_atdbkrslt["$emp_id"]["$date"]["night_duty"] = $night_duty;
								}
								if ($data_atdbkrslt["allow_id"] == "" && $allow_id != "") {
									array_push($set, "allow_id");
									array_push($setvalue, $allow_id);
									$this->arr_atdbkrslt["$emp_id"]["$date"]["allow_id"] = $allow_id;
								}
								if ($data_atdbkrslt["tmcd_group_id"] == "" && $tmcd_group_id != "") {
									array_push($set, "tmcd_group_id");
									array_push($setvalue, $tmcd_group_id);
									$this->arr_atdbkrslt["$emp_id"]["$date"]["tmcd_group_id"] = $tmcd_group_id;
								}
								if($this->import_5time_flg){
									if($this->tcTimeInBtnTimeList(true, $data_atdbkrslt, $time)){
										// 勤務実績を更新しない
										$rtn_info["rtncd"] = 2;
									}else{
										// 上限まで打刻可能なら]勤務実績.開始時刻に追加
										if(!$this->updateByTcTime($emp_id, true, $data_atdbkrslt, $time, $date)){
											$rtn_info["rtncd"] = 1;
											$arr_msg[] = "打刻上限エラー";
										}
										//エラーでなければ、勤務パターン等を予定からコピー 20141016
										else {
											if (count($set) > 0) {
												$this->update_atdbkrslt_column($set, $setvalue, $emp_id, $date);
												if ($this->get_dberr_msg() != "") {
													return -1;
												}
											}
										}
									}
								}else{

									$this->update_atdbkrslt_column($set, $setvalue, $emp_id, $date);
									if ($this->get_dberr_msg() != "") {
										return -1;
									}
								}

							} else {
								if ($data_atdbkrslt["start_time"] == $time) {
									//時刻一致　処理なし
									$rtn_info["rtncd"] = 2;
								}else{
									if($this->import_5time_flg){
										if($this->tcTimeInBtnTimeList(true, $data_atdbkrslt, $time)){
											// 勤務実績を更新しない
											$rtn_info["rtncd"] = 2;
										}else{
											// 上限まで打刻可能なら]勤務実績.開始時刻に追加
											if(!$this->updateByTcTime($emp_id, true, $data_atdbkrslt, $time, $date)){
												$rtn_info["rtncd"] = 1;
												$arr_msg[] = "打刻上限エラー";
											}
										}
									}else{
										//時刻不一致　重複エラー
										$rtn_info["rtncd"] = 1;
										$arr_msg[] = "重複エラー";
									}
								}
							}
						}
					}
				}
				//翌日以降の休暇を実績へ登録
				//勤怠自動バッチで行うためここでは不要 20110210
				//$this->next_hol_set($date, $emp_id, $data_atdbk);
				//if ($this->get_dberr_msg() != "") {
				//	return -1;
				//}
				break;
			case "退勤":
				$status = "3";
				if($this->has_threshold){
					if($time < $this->timecard_base_time){
						// 打刻時間の閾値が設定されている場合で、退勤の打刻が閾値以前の時間の場合は前日の退勤として打刻
						//実績データなし　作成
						if (count($last_atdbkrslt) == 0) {
							$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, end_time, end_btn_time, reg_prg_flg, end_date, end_btn_date1, status ";
							$sql .= ") values (";
							$content = array($emp_id, $last_date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $last_date, $date, $status);
							$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
							if ($ins == 0) {
								$this->dberr_msg = pg_last_error($this->_db_con);
								pg_query($this->_db_con,"rollback");
								if ($this->mode == "2") {
									return -1;
								}
								pg_close($this->_db_con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["date"] = $last_date;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["pattern"] = $pattern;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["reason"] = $reason;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["tmcd_group_id"] = $tmcd_group_id;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["end_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["end_btn_time"] = $time;
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["end_btn_date1"] = $date;
						}
						//実績データあり
						else {
							if($this->import_5time_flg){
								if($this->tcTimeInBtnTimeList(false, $last_atdbkrslt, $time)){
									// 勤務実績を更新しない
									$rtn_info["rtncd"] = 2;
								}else{
									// 上限まで打刻可能なら]勤務実績.退勤時刻に追加
									if(!$this->updateByTcTime($emp_id, false, $last_atdbkrslt, $time, $date)){
										$rtn_info["rtncd"] = 1;
										$arr_msg[] = "打刻上限エラー";
									}
								}
							}
						}
						//会議研修時刻の更新 20151119
						if ($this->update_meeting_time($emp_id, $last_date, $arr_atdbk, $arr_emp_id, $emp_personal_id) == -1) {
							return -1;
						}

					}else{ // 退勤の打刻が閾値以降の時間の場合

						// 20140922
						/**
						 * 夜間で日跨り、かつ長時間にわたる勤務(石巻赤十字病院様における『夜勤』)に対する退勤時刻の考慮.
						 *
						 * 下記の条件(1)?(7)が成立する場合の退勤打刻は前日扱いとし、翌日フラグをONにして<br>
						 * 勤務実績を登録(勤務実績レコードがない場合)・更新(勤務実績レコードがすでにある場合)する.
						 *
						 *［判定する条件］
						 *
						 * (1)医療施設コードが石巻様、かつ
						 * (2)前日の勤務パターン有、かつ
						 * (3)前日の勤務パターンの開始時刻>12:00(*1)、かつ
						 * (4)前日の勤務パターンの開始時刻>終了時刻、かつ
						 * (5)前日の勤務パターンの終了時刻と開始時刻の差>16時間(*1)、かつ
						 * (6)前日の勤務パターンの終了時刻と退勤打刻の時間との差が±9時間(*1)
						 * (7)前日の退勤打刻がまだ打刻されていない
						 *
						 * (*1)数値については定数化
						 *
						 */

						$targetPattern = "";
						if (is_null($last_atdbkrslt)) {
							$targetPattern = $arr_atdbk["$emp_id"]["$last_date"]["pattern"];
						}else{
							$targetPattern = $this->arr_atdbkrslt["$emp_id"]["$last_date"]["pattern"];
						}
						//標準の処理とするため(1)の条件を除外する$org_cd == '0410210017' && 
						if($targetPattern!=""){

							// 勤務パターンの開始時間・終了時間の取得
							$sql =	"select officehours2_start, officehours2_end from officehours ";
							$cond = "WHERE tmcd_group_id = $tmcd_group_id and pattern = '$targetPattern' ";
							$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
							if ($sel == 0) {
								$this->dberr_msg = pg_last_error($this->_db_con);
								pg_query($this->_db_con, "rollback");
								if ($this->mode == "2") {
									return -1;
								}
								pg_close($this->_db_con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							if (pg_num_rows($sel) > 0) {
								$officehours2_start = pg_fetch_result($sel, 0, "officehours2_start");
								$officehours2_end = pg_fetch_result($sel, 0, "officehours2_end");
							}

							if($officehours2_start >= THRESHOLD_MID_NIGHT_PATTERN_START_HOUR &&
							   $officehours2_start > $officehours2_end &&
							   $this->calcDiffHour($officehours2_start, $officehours2_end, false) > THRESHOLD_MID_NIGHT_PATTERN_TIMEZONE_NUM &&
							   $this->calcDiffHour2($officehours2_end, substr($time,0,2).":".substr($time,2,2), true) < THRESHOLD_MID_NIGHT_PATTERN_TIME_RANGE &&
							   $this->endBtnTimeIsEmpty($last_atdbkrslt)
							){
								if(count($last_atdbkrslt)== 0){
									$this->insertAtdbkRslt($emp_id, $last_date, $time, $date, $arr_atdbk["$emp_id"]["$last_date"], "0", "1", false);
								}else{
									$this->updateAtdbkRslt($emp_id, $last_date, $time, $date, $last_atdbkrslt, "0", "1", false);
								}
								//会議研修時刻の更新 20151119
								if ($this->update_meeting_time($emp_id, $last_date, $arr_atdbk, $arr_emp_id, $emp_personal_id) == -1) {
									return -1;
								}
								continue;
							}
						}
						// 20140922

						// 前日の勤務実績がない場合
						if (is_null($last_atdbkrslt)) {
							// 当日実績の退勤時刻として登録
							if (is_null($data_atdbkrslt)) {
								$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, end_time, end_btn_time, reg_prg_flg, end_date, end_btn_date1, status ";
								$sql .= ") values (";
								$content = array($emp_id, $date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $date, $date, $status);
								$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
								if ($ins == 0) {
									$this->dberr_msg = pg_last_error($this->_db_con);
									pg_query($this->_db_con,"rollback");
									if ($this->mode == "2") {
										return -1;
									}
									pg_close($this->_db_con);
									echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
									echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
									exit;
								}
								$this->arr_atdbkrslt["$emp_id"]["$date"]["date"] = $date;
								$this->arr_atdbkrslt["$emp_id"]["$date"]["pattern"] = $pattern;
								$this->arr_atdbkrslt["$emp_id"]["$date"]["reason"] = $reason;
								$this->arr_atdbkrslt["$emp_id"]["$date"]["tmcd_group_id"] = $tmcd_group_id;
								$this->arr_atdbkrslt["$emp_id"]["$date"]["end_time"] = $time;
								$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_time"] = $time;
								$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_date1"] = $date;
							}else{
							    // 当日実績の退勤時刻として更新
								if($this->import_5time_flg){
									if($this->tcTimeInBtnTimeList(false, $data_atdbkrslt, $time)){
										// 勤務実績を更新しない
										$rtn_info["rtncd"] = 2;
									}else{
										// 上限まで打刻可能なら]勤務実績.退勤時刻に追加
										if(!$this->updateByTcTime($emp_id, false, $data_atdbkrslt, $time, $date)){
											$rtn_info["rtncd"] = 1;
											$arr_msg[] = "打刻上限エラー";
										}
									}
								}
							}
							//会議研修時刻の更新 20151119
							if ($this->update_meeting_time($emp_id, $date, $arr_atdbk, $arr_emp_id, $emp_personal_id) == -1) {
								return -1;
							}
						}
						//前日の実績データあり
						else {
							if($this->import_5time_flg){
								// 当日の勤務実績として登録、あるいは更新する
								if (is_null($data_atdbkrslt)) {
									$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, end_time, end_btn_time, reg_prg_flg, end_date, end_btn_date1, status ";
									$sql .= ") values (";
									$content = array($emp_id, $date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $date, $date, $status);
									$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
									if ($ins == 0) {
										$this->dberr_msg = pg_last_error($this->_db_con);
										pg_query($this->_db_con,"rollback");
										if ($this->mode == "2") {
											return -1;
										}
										pg_close($this->_db_con);
										echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
										echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
										exit;
									}
									$this->arr_atdbkrslt["$emp_id"]["$date"]["date"] = $date;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["pattern"] = $pattern;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["reason"] = $reason;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["tmcd_group_id"] = $tmcd_group_id;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_date1"] = $date;

								}else{
									// 当日実績の退勤時刻として更新
									if($this->tcTimeInBtnTimeList(false, $data_atdbkrslt, $time)){
										// 勤務実績を更新しない
										$rtn_info["rtncd"] = 2;
									}else{
										// 上限まで打刻可能なら]勤務実績.退勤時刻に追加
										if(!$this->updateByTcTime($emp_id, false, $data_atdbkrslt, $time, $date)){
											$rtn_info["rtncd"] = 1;
											$arr_msg[] = "打刻上限エラー";
										}
									}
								}
								//会議研修時刻の更新 20151119
								if ($this->update_meeting_time($emp_id, $date, $arr_atdbk, $arr_emp_id, $emp_personal_id) == -1) {
									return -1;
								}
							}
						}
					}
				}else{
					//出勤時刻設定なしで、前日の出勤が設定あり、前日の退勤が未設定の場合、前日のデータへ更新 20110114
					if ($data_atdbkrslt["start_time"] == "" && $last_atdbkrslt["start_time"] != "" && $last_atdbkrslt["end_time"] == "") {
						//前日の出勤時刻設定あり　前日のレコードに更新
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["end_time"] = $time;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["end_btn_time"] = $time;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["end_btn_date1"] = $date;
						//会議・外出・病棟外対応 20100831
						$arr_tmp_set = array("end_time", "end_btn_time", "end_date", "next_day_flag", "end_btn_date1", "status");
						$arr_tmp_setvalue = array($time, $time, $date, 1, $date, $status);
						//勤務パターングループ、予定、または、勤務条件から取得
						$wk_tmcd_group_id = ($arr_atdbk["$emp_id"]["$last_date"]["tmcd_group_id"] != "") ?
							$arr_atdbk["$emp_id"]["$last_date"]["tmcd_group_id"] : $arr_emp_id["$emp_personal_id"]["tmcd_group_id"];

						// 実績の勤務パターンがなく、予定の勤務パターンがある場合、その勤務パターンの時間帯データの会議時刻を使用
						if ($wk_tmcd_group_id != "" &&
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["org_pattern"] == "" &&
								$arr_atdbk["$emp_id"]["$last_date"]["pattern"] != "") {
							$arr_meeting_time = $this->get_meeting_time($wk_tmcd_group_id, $arr_atdbk["$emp_id"]["$last_date"]["pattern"]);
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["a_meeting_start_time"] = $arr_meeting_time["meeting_start_time"];
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["a_meeting_end_time"] = $arr_meeting_time["meeting_end_time"];
						}

						if ($this->arr_atdbkrslt["$emp_id"]["$last_date"]["meeting_start_time"] == "" &&
								$this->arr_atdbkrslt["$emp_id"]["$last_date"]["a_meeting_start_time"] != "") {
							array_push($arr_tmp_set, "meeting_start_time");
							array_push($arr_tmp_setvalue, $this->arr_atdbkrslt["$emp_id"]["$last_date"]["a_meeting_start_time"]);
						}
						if ($this->arr_atdbkrslt["$emp_id"]["$last_date"]["meeting_end_time"] == "" &&
								$this->arr_atdbkrslt["$emp_id"]["$last_date"]["a_meeting_end_time"] != "") {
							array_push($arr_tmp_set, "meeting_end_time");
							array_push($arr_tmp_setvalue, $this->arr_atdbkrslt["$emp_id"]["$last_date"]["a_meeting_end_time"]);
						}
						$this->update_atdbkrslt_column($arr_tmp_set, $arr_tmp_setvalue, $emp_id, $last_date);
						if ($this->get_dberr_msg() != "") {
							return -1;
						}

					} else {
						//退勤時刻設定なし　更新、出勤時刻の設定有無に関わらず更新する 20110114
						if ($data_atdbkrslt["end_time"] == "") {
							//当日出勤時刻なし、前日出勤時刻あり、前日退勤時刻が同じ場合は、処理なし 20110512 (打刻日付も確認する 20150202)
							if ($data_atdbkrslt["start_time"] == "" && $last_atdbkrslt["start_time"] != "" && $last_atdbkrslt["end_time"] == $time && $last_atdbkrslt["end_date"] == $date) {
								$rtn_info["rtncd"] = 2;
							} else {
								//実績データなし　作成
								if (count($data_atdbkrslt) == 0) {
									$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, end_time, end_btn_time, reg_prg_flg, end_date, end_btn_date1, status ";
									$sql .= ") values (";
									$content = array($emp_id, $date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $date, $date, $status);
									$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
									if ($ins == 0) {
										$this->dberr_msg = pg_last_error($this->_db_con);
										pg_query($this->_db_con,"rollback");
										if ($this->mode == "2") {
											return -1;
										}
										pg_close($this->_db_con);
										echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
										echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
										exit;
									}
									$this->arr_atdbkrslt["$emp_id"]["$date"]["date"] = $date;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["pattern"] = $pattern;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["reason"] = $reason;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["tmcd_group_id"] = $tmcd_group_id;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_date1"] = $date;
								}
								//実績データあり
								else {
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_time"] = $time;
									$this->arr_atdbkrslt["$emp_id"]["$date"]["end_btn_date1"] = $date;
									//会議・外出・病棟外対応 20100831
									$arr_tmp_set = array("end_time", "end_btn_time", "end_date", "end_btn_date1", "status");
									$arr_tmp_setvalue = array($time, $time, $date, $date, $status);
									//勤務パターングループ、予定、または、勤務条件から取得
									$wk_tmcd_group_id = ($arr_atdbk["$emp_id"]["$date"]["tmcd_group_id"] != "") ?
										$arr_atdbk["$emp_id"]["$date"]["tmcd_group_id"] : $arr_emp_id["$emp_personal_id"]["tmcd_group_id"];

									// 実績の勤務パターンがなく、予定の勤務パターンがある場合、その勤務パターンの時間帯データの会議時刻を使用
									if ($wk_tmcd_group_id != "" &&
											$this->arr_atdbkrslt["$emp_id"]["$date"]["org_pattern"] == "" &&
											$arr_atdbk["$emp_id"]["$date"]["pattern"] != "") {
										$arr_meeting_time = $this->get_meeting_time($wk_tmcd_group_id, $arr_atdbk["$emp_id"]["$date"]["pattern"]);
										$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_start_time"] = $arr_meeting_time["meeting_start_time"];
										$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_end_time"] = $arr_meeting_time["meeting_end_time"];
									}
									if ($this->arr_atdbkrslt["$emp_id"]["$date"]["meeting_start_time"] == "" &&
											$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_start_time"] != "") {
										array_push($arr_tmp_set, "meeting_start_time");
										array_push($arr_tmp_setvalue, $this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_start_time"]);
									}
									if ($this->arr_atdbkrslt["$emp_id"]["$date"]["meeting_end_time"] == "" &&
											$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_end_time"] != "") {
										array_push($arr_tmp_set, "meeting_end_time");
										array_push($arr_tmp_setvalue, $this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_end_time"]);
									}
									$this->update_atdbkrslt_column($arr_tmp_set, $arr_tmp_setvalue, $emp_id, $date);
									if ($this->get_dberr_msg() != "") {
										return -1;
									}
								}
							}
						} else {
							if ($data_atdbkrslt["end_time"] == $time) {
								//時刻一致　処理なし
								$rtn_info["rtncd"] = 2;
							}else{
								if($this->import_5time_flg){
									if($this->tcTimeInBtnTimeList(false, $data_atdbkrslt, $time)){
										// 勤務実績を更新しない
										$rtn_info["rtncd"] = 2;
									}else{
										// 上限まで打刻可能なら]勤務実績.退勤時刻に追加
										if(!$this->updateByTcTime($emp_id, false, $data_atdbkrslt, $time, $date)){
											$rtn_info["rtncd"] = 1;
											$arr_msg[] = "打刻上限エラー";
										}
									}
								}else{
									//時刻不一致　重複エラー
									$rtn_info["rtncd"] = 1;
									$arr_msg[] = "重複エラー";
								}
							}
						}
					}
				}
				break;
			case "外出":
				$status = "2";
				//出勤時刻設定なし　勤務開始未登録エラー
				if ($data_atdbkrslt["start_time"] == "") {
					$rtn_info["rtncd"] = 1;
					$arr_msg[] = "勤務開始未登録エラー";
				} else {
					//外出時刻設定なし　更新
					if ($data_atdbkrslt["out_time"] == "") {
						$this->arr_atdbkrslt["$emp_id"]["$date"]["out_time"] = $time;
						$this->update_atdbkrslt_column(array("out_time", "status"), array($time, $status), $emp_id, $date);
						if ($this->get_dberr_msg() != "") {
							return -1;
						}
					} else {
						//時刻不一致　重複エラー
						if ($data_atdbkrslt["out_time"] != $time) {
							$rtn_info["rtncd"] = 1;
							$arr_msg[] = "重複エラー";
						}
						//時刻一致　処理なし
						else {
							$rtn_info["rtncd"] = 2;
						}
					}
				}
				break;
			case "復帰":
			case "戻り": //戻り追加 20110419
				$status = "1";
				//出勤時刻設定なし　勤務開始未登録エラー
				if ($data_atdbkrslt["start_time"] == "") {
					$rtn_info["rtncd"] = 1;
					$arr_msg[] = "勤務開始未登録エラー";
				} else {
					//復帰時刻設定なし　更新
					if ($data_atdbkrslt["ret_time"] == "") {
						$this->arr_atdbkrslt["$emp_id"]["$date"]["ret_time"] = $time;
						$this->update_atdbkrslt_column(array("ret_time", "status"), array($time, $status), $emp_id, $date);
						if ($this->get_dberr_msg() != "") {
							return -1;
						}
					} else {
						//時刻不一致　重複エラー
						if ($data_atdbkrslt["ret_time"] != $time) {
							$rtn_info["rtncd"] = 1;
							$arr_msg[] = "重複エラー";
						}
						//時刻一致　処理なし
						else {
							$rtn_info["rtncd"] = 2;
						}
					}
				}
				break;
			case "退復":
			case "呼出":
				$status = "1";
				$taifuku_cnt++;
				$varname = "o_start_time".$taifuku_cnt;
				//退勤時刻設定あり　更新
				//前日の退勤時刻設定なし　更新（出勤退勤時刻なしでも退復は登録可能とする）
				if ($data_atdbkrslt["end_time"] != "" ||
					$last_atdbkrslt["end_time"] == "") {
					if (!in_array($time ,$arr_o_start_time)) {
						$this->arr_atdbkrslt["$emp_id"]["$date"]["$varname"] = $time;
						$this->update_atdbkrslt_column(array($varname, "status"), array($time, $status), $emp_id, $date);
						if ($this->get_dberr_msg() != "") {
							return -1;
						}
					} else {
						//登録済 処理なし
						$rtn_info["rtncd"] = 2;
					}
				} else {

					//前日の退勤時刻設定あり　前日のレコードに更新
					$last_taifuku_cnt = 0;
					$arr_o_start_time = array();
					$arr_o_end_time = array();
					for ($i=1 ; $i<=10; $i++) {
						$wk_key1 = "o_start_time".$i;
						if ($last_atdbkrslt["$wk_key1"] != "") {
							array_push($arr_o_start_time, $last_atdbkrslt["$wk_key1"]);
						}
						$wk_key2 = "o_end_time".$i;
						if ($last_atdbkrslt["$wk_key2"] == "") {
							break;
						} else {
							array_push($arr_o_end_time, $last_atdbkrslt["$wk_key2"]);
						}
						$last_taifuku_cnt++;
					}
					$last_taifuku_cnt++;
					if (!in_array($time ,$arr_o_start_time)) {
						$varname = "o_start_time".$last_taifuku_cnt;
						$this->arr_atdbkrslt["$emp_id"]["$last_date"]["$varname"] = $time;
						$this->update_atdbkrslt_column(array($varname, "status"), array($time, $status), $emp_id, $last_date);
						if ($this->get_dberr_msg() != "") {
							return -1;
						}
					} else {
						//登録済 処理なし
						$rtn_info["rtncd"] = 2;
					}
				}
				break;
			case "復退":
			case "呼退":
				$status = "3";
				$taifuku_cnt++;
				$varname_start = "o_start_time".$taifuku_cnt;
				$varname = "o_end_time".$taifuku_cnt;
				//登録済 処理なし
				if (in_array($time ,$arr_o_end_time)) {
					$rtn_info["rtncd"] = 2;
				}
				//退復時刻設定あり　更新
				elseif ($data_atdbkrslt["$varname_start"] != "") {
					$this->arr_atdbkrslt["$emp_id"]["$date"]["$varname"] = $time;
					$this->update_atdbkrslt_column(array($varname, "status"), array($time, $status), $emp_id, $date);
					if ($this->get_dberr_msg() != "") {
						return -1;
					}
				} else {

					$last_taifuku_cnt = 0;
					$arr_o_start_time = array();
					$arr_o_end_time = array();
					for ($i=1 ; $i<=10; $i++) {
						$wk_key1 = "o_start_time".$i;
						if ($last_atdbkrslt["$wk_key1"] != "") {
							array_push($arr_o_start_time, $last_atdbkrslt["$wk_key1"]);
						}
						$wk_key2 = "o_end_time".$i;
						if ($last_atdbkrslt["$wk_key2"] == "") {
							break;
						} else {
							array_push($arr_o_end_time, $last_atdbkrslt["$wk_key2"]);
						}
						$last_taifuku_cnt++;
					}
					$last_taifuku_cnt++;
					$varname_start = "o_start_time".$last_taifuku_cnt;
					$varname = "o_end_time".$last_taifuku_cnt;
					if (!in_array($time ,$arr_o_end_time)) {
						//前日の退復時刻設定なし　更新
						if ($last_atdbkrslt["$varname_start"] == "") {
							$rtn_info["rtncd"] = 1;
							$ret_str = ($this->timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出出勤";
							$arr_msg[] = "{$ret_str}未登録エラー";
						}
						//前日の退復時刻設定あり　前日のレコードに更新
						else {
							$this->arr_atdbkrslt["$emp_id"]["$last_date"]["$varname"] = $time;
							$this->update_atdbkrslt_column(array($varname, "status"), array($time, $status), $emp_id, $last_date);
							if ($this->get_dberr_msg() != "") {
								return -1;
							}
						}
					} else {
						//登録済 処理なし
						$rtn_info["rtncd"] = 2;
					}
				}
				break;
		}

		//エラー時
		if ($rtn_info["rtncd"] == 1) {
			$rtn_info["errmsg"] = $arr_msg;
		}
		return $rtn_info;
	}


	/**
	 * 職員マッチングキー取得
	 *
	 * @return string 職員マッチングキー（DBエラー時は-1、$this->get_dberr_msg()でエラー内容取得）
	 *
	 */
	function get_matching_key() {

		$matching_key = "";

		$sql = "select matching_key from timecard ";
		$cond = "";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0){
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con,"rollback");
			if ($this->mode == "2") {
				return -1;
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		if ($num > 0) {
			$matching_key = pg_fetch_result($sel, 0, "matching_key");
		}
		//未設定時のデフォルト
		if ($matching_key == "") {
			$matching_key = "1";
		}
		return $matching_key;
	}

	/**
	 * 職員マッチングキー更新
	 *
	 * @param string 職員マッチングキー
	 * @return 処理結果 0:正常終了 -1:DBエラー（$this->get_dberr_msg()でエラー内容取得）
	 *
	 */
	function update_matching_key($matching_key) {

		//DBの職員マッチングキー
		$old_matching_key = $this->get_matching_key();

		//変更がある場合に更新
		if ($matching_key != $old_matching_key) {
			$sql = "update timecard set ";
			$cond = "";
			$set = array("matching_key");
			$setvalue = array($matching_key);
			$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) {
				$this->dberr_msg = pg_last_error($this->_db_con);
				pg_query($this->_db_con,"rollback");
				if ($this->mode == "2") {
					return -1;
				}
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

		}
		return 0;
	}

	/**
	 * 職員情報取得
	 *
	 * @param array $emp_personal_ids 職員ID
	 * @param string $matching_key 1:職員ID 2:カード製造番号
     * @param string $maker_flg 1:マックス 2:アマノ 3:Fitware 4:セコム
	 * @return array 職員情報の配列[emp_personal_id or emp_idm][emp_id]
	 *
	 */
	function get_emp_id($emp_personal_ids, $matching_key, $maker_flg) {

		$arr_gid = array();

        	//20140909 itou 石巻赤十字 医療機関コード($org_cd)を取得 -----start-------
        	$sql = "select prf_org_cd from profile";
        	$cond = "";
        	$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        	if ($sel == 0) {
            	$this->dberr_msg = pg_last_error($this->_db_con);
            	if ($this->mode == "2") {
                	return -1;
            	}
            	pg_close($this->_db_con);
            	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            	exit;
        	}
        	$org_cd = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "prf_org_cd") : "";
		//20140909 itou ----- end -------

		if (count($emp_personal_ids) == 0) {
			return $arr_gid;
		}

		$cond_add = "";
		foreach ($emp_personal_ids as $emp_personal_id => $dummy) {
			if ($cond_add != "") {
				$cond_add .= " or ";
			}

			if ($matching_key == "1") {
				$col = " a.emp_personal_id ";
			} else {
				$col = " a.emp_idm ";
			}
			//タイムレコーダ別にIDの比較条件を変更 20111214
            if ($maker_flg == "1" || $maker_flg == "4") { //マックス,セコム
				$cond_add .= " $col ";
				$cond_add .= " = '$emp_personal_id' ";
			}
			elseif ($maker_flg == "2") { //アマノ
				//アマノの場合は職員IDを数値の長さ分比較する
				$len_key = strlen($emp_personal_id);


				//20140909 itou 医療機関コードが石巻赤十字でアマノマッチングキーが４桁の時はemp_login_idとマッチ
				//無理やりだが、$colの値を上書きしてしまう。（ここまではうまく動作する）
				if ((strlen($emp_personal_id) == 4) && ($org_cd == '0410210017')) {
					$col = " f.emp_login_id ";	//$colの値を書き換える

				}
				//20140909 itou ---------end---------------


				$cond_add .= " substr($col, length($col) - $len_key + 1, $len_key) ";
				$cond_add .= " = '$emp_personal_id' ";
			}
			elseif ($maker_flg == "3") { //Fitware
				//カードIDの先頭１桁を除いて比較
				$cond_add .= " substr($col, 2, 15) "; //SQLでは1始まり
				$wk_emp_personal_id = substr($emp_personal_id, 1, 15);
				$cond_add .= " = '$wk_emp_personal_id' "; //先頭1桁除外
			}

		}

		$sql = "select a.emp_id, a.emp_personal_id, a.emp_idm, b.atdptn_id, b.tmcd_group_id, d.pattern_id, f.emp_login_id from empmst a left join empcond b on b.emp_id = a.emp_id left join duty_shift_staff c on c.emp_id = a.emp_id left join duty_shift_group d on d.group_id = c.group_id left join authmst e on e.emp_id = a.emp_id left join login f on f.emp_id = a.emp_id"; // itou 20140908
        $cond = "where ($cond_add) and e.emp_del_flg = 'f'";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0){
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con,"rollback");
			if ($this->mode == "2") {
				return $arr_gid;
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$data = pg_fetch_all($sel);

		for ($i=0; $i<count($data); $i++) {

			if ($matching_key == "1") {
				$wk_emp_id = $data[$i]["emp_personal_id"];
			} else {
				$wk_emp_id = $data[$i]["emp_idm"];
			}

			//2:アマノの場合 職員IDを数値化してマッチング
			if ($maker_flg == "2") {
				$wk_emp_id = ltrim($wk_emp_id, "0");
			}
			elseif ($maker_flg == "3") { //Fitwareの場合、先頭１桁を除く 20111214
				$wk_emp_id = substr($wk_emp_id, 1, 15);
			}

			$arr_gid["$wk_emp_id"]["emp_id"] = $data[$i]["emp_id"];
			$arr_gid["$wk_emp_id"]["emp_personal_id"] = $data[$i]["emp_personal_id"];

			// 取得した職員情報にログインidを追加(石巻様カスタマイズの一環として)
			$arr_gid["$wk_emp_id"]["emp_login_id"] = $data[$i]["emp_login_id"];

			$arr_gid["$wk_emp_id"]["atdptn_id"] = $data[$i]["atdptn_id"];
			if ($data[$i]["tmcd_group_id"] != "") {
				//勤務条件の出勤パターングループ
				$arr_gid["$wk_emp_id"]["tmcd_group_id"] = $data[$i]["tmcd_group_id"];
			} else {
				//勤務シフト作成のシフトグループ（病棟）の出勤パターングループ
				$arr_gid["$wk_emp_id"]["tmcd_group_id"] = $data[$i]["pattern_id"];
			}
		}
		return $arr_gid;
	}

	/**
	 * 予定実績データ取得
	 *
	 * @param string $dbname DB名 "atdbk" or "atdbkrslt"
	 * @param array $arr_emp_id 職員情報
	 * @param string $start_date 開始日
	 * @param string $end_date 終了日
	 * @return mixed 取得データ（DBエラー時は空、$this->get_dberr_msg()でエラー内容取得）
	 *
	 */
	function get_arr_atdbk($dbname, $arr_emp_id, $start_date, $end_date) {

		$cond_add = "";
		foreach ($arr_emp_id as $wk_dummy_id => $wk_data) {
			$wk_emp_id = $wk_data["emp_id"];
			if ($cond_add == "") {
				$cond_add .= " and (a.emp_id = '$wk_emp_id' ";
			} else {
				$cond_add .= "or a.emp_id = '$wk_emp_id' ";
			}
		}
		if ($cond_add != "") {
			$cond_add .= ") ";
		}

		//開始日は2日前から取得
		$wk_start_date = date("Ymd", strtotime("-2 day", $this->to_timestamp($start_date)));
		//終了日は1日後まで取得 20110413
		$wk_end_date = date("Ymd", strtotime("+1 day", $this->to_timestamp($end_date)));

		//データ取得
		//会議・外出・病棟外対応 20100831
		if ($dbname == "atdbkrslt") {
			$sql = "select a.*, ".
				"atdptn.previous_day_possible_flag as a_previous_day_possible_flag, ". // 20140916 実績で『開始が前日になる場合があるフラグ』を判定するために追加
				"atdptn.meeting_start_time as a_meeting_start_time, ".
				"atdptn.meeting_end_time as a_meeting_end_time ".
				"from $dbname a ".
				"LEFT JOIN atdptn ON (a.tmcd_group_id = atdptn.group_id AND a.pattern = CAST(atdptn.atdptn_id as varchar)) ";
		} else {
			$sql = "select a.*, ".
				"atdptn.previous_day_possible_flag as a_previous_day_possible_flag, ".
				"atdptn.previous_day_flag as a_previous_day_flag, ".
				"atdptn.previous_day_threshold_hour as a_previous_day_threshold_hour ".
				"from $dbname a ".
				"LEFT JOIN atdptn ON (a.tmcd_group_id = atdptn.group_id AND a.pattern = CAST(atdptn.atdptn_id as varchar)) ";
		}
		$cond = "where a.date >= '$wk_start_date' and a.date <= '$wk_end_date' ";
		$cond .= $cond_add;

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0){
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con,"rollback");
			if ($this->mode == "2") {
				return array();
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);

		$arr_atdbk = array();

		for ($i=0; $i<$num; $i++) {
			$wk_emp_id = pg_fetch_result($sel, $i, "emp_id");
			$wk_date = pg_fetch_result($sel, $i, "date");
			$arr_atdbk["$wk_emp_id"]["$wk_date"] = pg_fetch_array($sel, $i, PGSQL_ASSOC);
			$arr_atdbk["$wk_emp_id"]["$wk_date"]["org_pattern"] = $arr_atdbk["$wk_emp_id"]["$wk_date"]["pattern"];
		}
		return $arr_atdbk;
	}


	/**
	 * 実績データの項目更新
	 *
	 * @param array $arr_set カラム名の配列
	 * @param array $arr_setvalue データの配列
	 * @param string $emp_id 職員ID
	 * @param string $duty_date 日付
	 * @return 処理結果 0:正常終了 -1:DBエラー（$this->get_dberr_msg()でエラー内容取得）
	 *
	 */
	function update_atdbkrslt_column($arr_set, $arr_setvalue, $emp_id, $date) {

		for ($i=0; $i<count($arr_set); $i++) {
			$varname = $arr_set[$i];
			$this->arr_atdbkrslt["$emp_id"]["$date"]["$varname"] = $arr_setvalue[$i];
		}

		$sql = "update atdbkrslt set ";
		$cond = "where emp_id = '$emp_id' and date = '$date' ";
		$upd = update_set_table($this->_db_con, $sql, $arr_set, $arr_setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			echo(pg_last_error($this->_db_con));
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con, "rollback");
			if ($this->mode == "2") {
				return -1;
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return 0;
	}

	/**
	 * 翌日以降の休暇を実績へ登録
	 * @param  $duty_date  当日YYYYMMDD形式
	 * @param  $emp_id     職員ID
	 * @return 処理結果 0:正常終了 -1:DBエラー（$this->get_dberr_msg()でエラー内容取得）
	 *
	 * 未使用 20110210
	 * 勤怠自動バッチで行うため
	 * 数日分まとめて登録する場合に、先にできた休暇のレコードで重複エラーを起こす問題もあった
	 */
	function next_hol_set($duty_date, $emp_id) {

		$tmp_date = $duty_date;

		while (1) {
			$tmp_date = date("Ymd", strtotime("+1 day", $this->to_timestamp($tmp_date)));
			//翌日予定の勤務パターンを確認
			//勤務日数換算が0日の場合を条件追加 20091201
			$sql =	"SELECT ".
				"a.pattern, a.reason, a.night_duty, a.tmcd_group_id, a.allow_id, b.pattern as rslt_pattern, b.date as rslt_date, c.workday_count ".
				"FROM ".
				"atdbk a ".
				"LEFT JOIN atdbkrslt b ON (a.emp_id = b.emp_id and a.date = b.date) ".
				"LEFT JOIN atdptn c ON a.tmcd_group_id = c.group_id AND a.pattern = CAST(c.atdptn_id AS varchar) ";
			$cond = "WHERE a.emp_id = '$emp_id' and a.date = '$tmp_date'";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				$this->dberr_msg = pg_last_error($this->_db_con);
				pg_query($this->_db_con, "rollback");
				if ($this->mode == "2") {
					return -1;
				}
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			if (pg_num_rows($sel) > 0) {
				$pattern       = pg_fetch_result($sel, 0, "pattern");
				$rslt_pattern  = pg_fetch_result($sel, 0, "rslt_pattern");
				$rslt_date     = pg_fetch_result($sel, 0, "rslt_date");
				$workday_count = pg_fetch_result($sel, 0, "workday_count");
			} else {
				break;
			}

			//（予定が休暇か勤務日数換算が0日）の場合で実績が未設定の場合、実績を更新
			if (($pattern == "10"|| $workday_count == 0) && $rslt_pattern == "") {
				$reason       = pg_fetch_result($sel, 0, "reason");
				$night_duty       = pg_fetch_result($sel, 0, "night_duty");
				$tmcd_group_id       = pg_fetch_result($sel, 0, "tmcd_group_id");
				$allow_id       = pg_fetch_result($sel, 0, "allow_id");
				//実績レコードがない場合、登録
				if ($rslt_date == "") {

					$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, reg_prg_flg ";
					$sql .= ") values (";

					$content = array($emp_id, $tmp_date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, "1");
					$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
					if ($ins == 0) {
						$this->dberr_msg = pg_last_error($this->_db_con);
						pg_query($this->_db_con, "rollback");
						if ($this->mode == "2") {
							return -1;
						}
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				} else {
					//更新
					$sql = "update atdbkrslt set";
					$set = array("pattern", "reason", "night_duty", "tmcd_group_id", "allow_id", "reg_prg_flg");
					$setvalue = array($pattern, $reason, $night_duty, $tmcd_group_id, $allow_id, "1");
					$cond = "WHERE emp_id = '$emp_id' and date = '$tmp_date'";

					$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0) {
						$this->dberr_msg = pg_last_error($this->_db_con);
						pg_query($this->_db_con, "rollback");
						if ($this->mode == "2") {
							return -1;
						}
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			} else {
				//休暇以外か実績のパターンが設定済になるまで繰り返す
				break;
			}
		}
		return 0;
	}

	/**
	 * エラーデータ登録
	 *
	 * @param string $duty_date 日付
	 * @param string $emp_personal_id 職員ID
	 * @param string $err_data エラーデータ
	 * @param string $err_reason エラー理由
	 * @return 処理結果 0:正常終了 -1:DBエラー（$this->get_dberr_msg()でエラー内容取得）
	 *
	 */
	function insert_import_error($duty_date, $emp_personal_id, $err_data, $err_reason) {

		$sql = "insert into timecard_import_error (duty_date, emp_personal_id, data, error_reason, regist_time ";
		$sql .= ") values (";
		$wk_emp_personal_id = ($emp_personal_id == null) ? " " : $emp_personal_id; //nullの場合、" "とする 20110307
		//項目が長すぎる場合の対応
		if (strlen($duty_date) > 8) {
			$wk_duty_date = substr($duty_date, 0, 8);
		}
		else {
			$wk_duty_date = $duty_date;
		}
		if (strlen($wk_emp_personal_id) > 20) {
			$wk_emp_personal_id = substr($wk_emp_personal_id, 0, 20);
		}

		$content = array($wk_duty_date, $wk_emp_personal_id, $err_data, $err_reason, date("YmdHis"));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con,"rollback");
			if ($this->mode == "2") {
				return -1;
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return 0;
	}


	/**
	 * 処理結果取得
	 *
	 * @return array 処理結果
	 *
	 */
	function get_rslt_msg() {
		return $this->result_msg;
	}

	/**
	 * DBエラー取得
	 *
	 * @return string DBエラー
	 *
	 */
	function get_dberr_msg() {
		return $this->dberr_msg;
	}

	/**
	 * 日付をタイムスタンプに変換
	 *
	 * @param string $yyyymmdd 年月日
	 * @return int	タイムスタンプ
	 *
	 */
	function to_timestamp($yyyymmdd) {
		$y = substr($yyyymmdd, 0, 4);
		$m = substr($yyyymmdd, 4, 2);
		$d = substr($yyyymmdd, 6, 2);
		return mktime(0, 0, 0, $m, $d, $y);
	}


	/**
	 * get_atdptn_reason 勤務パターン、事由の組合せ取得
	 *
	 * @return array 勤務パターングループ、勤務パターンをキーとする事由の配列
	 *
	 */
	function get_atdptn_reason() {

		$sql = "select group_id, atdptn_id, reason  from atdptn";
		$cond = "WHERE reason is not null order by group_id, atdptn_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if ($sel == 0) {
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con, "rollback");
			if ($this->mode == "2") {
				return -1;
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		$arr_data = array();

		for ($i=0; $i<$num; $i++) {
			$wk_group_id = pg_fetch_result($sel, $i, "group_id");
			$wk_atdptn_id = pg_fetch_result($sel, $i, "atdptn_id");
			$arr_data["$wk_group_id"]["$wk_atdptn_id"] = pg_fetch_result($sel, $i, "reason");
		}
		return $arr_data;
	}


	/**
	 * 会議時刻取得
	 *
	 * @param mixed $tmcd_group_id 勤務パターングループ
	 * @param mixed $atdptn_id 勤務パターン
	 * @return mixed 会議時刻
	 *
	 */
	function get_meeting_time($tmcd_group_id, $atdptn_id) {

		$sql = "select meeting_start_time, meeting_end_time from atdptn ";
		$cond = "where group_id = $tmcd_group_id and atdptn_id = $atdptn_id ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0){
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con,"rollback");
			if ($this->mode == "2") {
				return -1;
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		$arr_data = array();
		if ($num > 0) {
			$arr_data["meeting_start_time"] = pg_fetch_result($sel, 0, "meeting_start_time");
			$arr_data["meeting_end_time"] = pg_fetch_result($sel, 0, "meeting_end_time");
		}
		return $arr_data;
	}

	/**
	 * 繰り返し打刻(start_btn_time/end_btn_time)一覧にタイムカード打刻時間と一致する時刻が含まれるかどうか.
	 * @param boolean $isStartBtnTime true: 検索対象は出勤打刻時刻, false:　検索対象は退勤打刻時刻.
	 * @param array $data_atdbkrslt 勤務実績レコード.
	 * @param　string $tcTimeValue　タイムカード打刻時間(HHMM).
	 * @return  boolean true:打刻時間は取り込み済み, false: 打刻時間は取り込まれていない
	 */
	function tcTimeInBtnTimeList($isStartBtnTime, $data_atdbkrslt, $tcTimeValue) {
		$ret = false;
		$arr_btn_times = $this->buildBtmTimeList($data_atdbkrslt);

		$isStart = 's';
		if($isStartBtnTime){
			$isStart = 'e';
		}
		foreach ($arr_btn_times as $dakoku){
			if($tcTimeValue==$dakoku['btn_time'] && $isStart = $dakoku['startEnd']){
				$ret = true;
			}
		}
		return $ret;
	}

	/**
	 * タイムカード打刻時間による打刻の追加.
	 * <p>繰り返し打刻(start_btn_time/end_btn_time)一覧に追加.
	 * @param boolean $isStartBtnTime true: 検索対象は出勤打刻時刻, false:　検索対象は退勤打刻時刻.
	 * @param array $data_atdbkrslt 勤務実績レコード.
	 * @param　string $tcTimeValue　タイムカード打刻時間(HHMM).
	 * @param　string $tcDateValue　タイムカード打刻日付(YYYYMMDD) 20141215.
	 * @return  boolean true:タイムカード打刻時間を追加, false: 追加可能な上限回数に達しているため追加しない
	 */
	function updateByTcTime($emp_id, $isStartBtnTime, $data_atdbkrslt, $tcTimeValue, $tcDateValue) {
		$ret = false;

		$marge = $this->buildBtmTimeList($data_atdbkrslt);

		// タイムカード打刻時間を出退勤打刻配列に追加
		$arr =array();
		$sort_key_date = intval($data_atdbkrslt["date"]);
		if(count($marge)>0){
			if($marge[0]["btn_time"]>$tcTimeValue){
				$sort_key_date++;
			}
		}
		$arr["targetDate"] = $sort_key_date;
		$arr["btn_time"] = $tcTimeValue;
		$arr["btn_date"] = $tcDateValue;
		$arr["startEnd"] = ($isStartBtnTime)? 's': 'e';
		array_push($marge,$arr);

		// ソート用日付,時刻でソート
		foreach($marge as $key => $row){
			$sortKeyDate[$key] = $row["targetDate"];
			$time[$key] = $row["btn_time"];
		}
		array_multisort($sortKeyDate, SORT_ASC, $time, SORT_ASC, $marge);

		// 勤務実績のレコードイメージの配列
		$arr_btn_times_with_padding = array_fill(0, 10, "");
		$arr_btn_dates_with_padding = array_fill(0, 10, "");

		// 出退勤打刻の時間の整合性を考慮しながらセット
		$prev_startEnd = "";

		// セットしていく先頭インデクスの決定
		if(count($marge)>0){
			$i = 0;
			if($marge[0]['startEnd']=='s'){
				$i = -1;
			}
		}

		foreach ($marge as $time_attr){
			if($prev_startEnd==$time_attr['startEnd']){
				$i = $i+2;
				$arr_btn_times_with_padding[$i] = $time_attr['btn_time'];
				$arr_btn_dates_with_padding[$i] = $time_attr['btn_date'];
			}else{
				$i++;
				$arr_btn_times_with_padding[$i] = $time_attr['btn_time'];
				$arr_btn_dates_with_padding[$i] = $time_attr['btn_date'];
			}
			$prev_startEnd = $time_attr['startEnd'];
		}

		// 出勤・退勤の打刻の繰り返し数>5(配列としては10)を超えている場合は打刻上限エラーの扱い
		if(count($arr_btn_times_with_padding)>=11){
			return false;
		}

		// 翌日フラグセット判定 20140918
		if(count($marge)>0){
			//翌日フラグ不具合対応 20141112
			$nextDayFlg = "0";
			//配列2番目が退勤の場合にその時刻で判断
			if($marge[1]['startEnd']=='e') {
				if($marge[0]['targetDate']<$marge[1]['targetDate']){
					// 日跨ぎがあった場合、翌日フラグON
					$nextDayFlg = "1";
				}else{
				   // 出退勤が同日内の場合、翌日フラグOFF
					$nextDayFlg = "0";
				}
			}
		}

		// すでに前日フラグがONになっている勤務実績の場合は
		// 翌日フラグセット判定の結果にかかわらず翌日フラグをOFFにする 20140918
		if($data_atdbkrslt['previous_day_flag']=="1"){
			$nextDayFlg = "0";
		}

		// 勤務実績の更新
		$targetDateKbn = "end_date";
		if($isStartBtnTime){
			$targetDateKbn = "start_date";
		}

		$status = ($isStartBtnTime) ? "1" : "3";
		$set = array(
						"start_time",
						"end_time",
						"next_day_flag", // 20140918
						"reg_prg_flg",
						"start_btn_time",
						"end_btn_time",
						$targetDateKbn,
						"start_btn_time2",
						"end_btn_time2",
						"start_btn_time3",
						"end_btn_time3",
						"start_btn_time4",
						"end_btn_time4",
						"start_btn_time5",
						"end_btn_time5",
						"start_btn_date1",
						"end_btn_date1",
						"start_btn_date2",
						"end_btn_date2",
						"start_btn_date3",
						"end_btn_date3",
						"start_btn_date4",
						"end_btn_date4",
						"start_btn_date5",
						"end_btn_date5",
						"status"
		);

		$setvalue = array(
						$arr_btn_times_with_padding[0],
						$arr_btn_times_with_padding[1],
						$nextDayFlg,  					// 20140918
						'1',
						$arr_btn_times_with_padding[0],
						$arr_btn_times_with_padding[1],
						$data_atdbkrslt["date"],
						$arr_btn_times_with_padding[2],
						$arr_btn_times_with_padding[3],
						$arr_btn_times_with_padding[4],
						$arr_btn_times_with_padding[5],
						$arr_btn_times_with_padding[6],
						$arr_btn_times_with_padding[7],
						$arr_btn_times_with_padding[8],
						$arr_btn_times_with_padding[9],
						$arr_btn_dates_with_padding[0],
						$arr_btn_dates_with_padding[1],
						$arr_btn_dates_with_padding[2],
						$arr_btn_dates_with_padding[3],
						$arr_btn_dates_with_padding[4],
						$arr_btn_dates_with_padding[5],
						$arr_btn_dates_with_padding[6],
						$arr_btn_dates_with_padding[7],
						$arr_btn_dates_with_padding[8],
						$arr_btn_dates_with_padding[9],
						$status
		);

		$ret = array();
		$this->update_atdbkrslt_column($set, $setvalue, $emp_id, $data_atdbkrslt["date"]);
		if ($this->get_dberr_msg() != "") {
			return -1;
		}else{
			$ret = true;
			// 更新が成功したら、対象日の勤務実績を更新
			$w_date = $data_atdbkrslt["date"];

			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_time"] = $setvalue[0];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_time"] =  $setvalue[1];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["next_day_flag"] = $setvalue[2];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["reg_prg_flg"] =  $setvalue[3];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_time"] =  $setvalue[4];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_time"] =  $setvalue[5];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["$targetDateKbn"] =  $setvalue[6];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_time2"] =  $setvalue[7];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_time2"] =  $setvalue[8];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_time3"] =  $setvalue[9];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_time3"] =  $setvalue[10];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_time4"] =  $setvalue[11];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_time4"] =  $setvalue[12];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_time5"] =  $setvalue[13];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_time5"] =  $setvalue[14];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_date1"] =  $setvalue[15];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_date1"] =  $setvalue[16];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_date2"] =  $setvalue[17];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_date2"] =  $setvalue[18];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_date3"] =  $setvalue[19];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_date3"] =  $setvalue[20];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_date4"] =  $setvalue[21];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_date4"] =  $setvalue[22];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["start_btn_date5"] =  $setvalue[23];
			$this->arr_atdbkrslt["$emp_id"]["$w_date"]["end_btn_date5"] =  $setvalue[24];
		}
		return $ret;
	}


	function buildBtmTimeList($data_atdbkrslt) {
		$ret = array();
		$targetDate =  $data_atdbkrslt["date"];
		$prevTime = "";

		if(strlen($data_atdbkrslt["start_btn_time"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["start_btn_time"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["start_btn_time"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["start_btn_time"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["start_btn_date1"];
			$dakokuDateTime["startEnd"] = 's';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["end_btn_time"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["end_btn_time"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["end_btn_time"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["end_btn_time"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["end_btn_date1"];
			$dakokuDateTime["startEnd"] = 'e';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["start_btn_time2"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["start_btn_time2"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["start_btn_time2"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["start_btn_time2"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["start_btn_date2"];
			$dakokuDateTime["startEnd"] = 's';
			array_push($ret,$dakokuDateTime);
		}


		if(strlen($data_atdbkrslt["end_btn_time2"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["end_btn_time2"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["end_btn_time2"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["end_btn_time2"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["end_btn_date2"];
			$dakokuDateTime["startEnd"] = 'e';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["start_btn_time3"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["start_btn_time3"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["start_btn_time3"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["start_btn_time3"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["start_btn_date3"];
			$dakokuDateTime["startEnd"] = 's';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["end_btn_time3"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["end_btn_time3"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["end_btn_time3"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["end_btn_time3"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["end_btn_date3"];
			$dakokuDateTime["startEnd"] = 'e';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["start_btn_time4"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["start_btn_time4"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["start_btn_time4"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["start_btn_time4"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["start_btn_date4"];
			$dakokuDateTime["startEnd"] = 's';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["end_btn_time4"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["end_btn_time4"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["end_btn_time4"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["end_btn_time4"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["end_btn_date4"];
			$dakokuDateTime["startEnd"] = 'e';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["start_btn_time5"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["start_btn_time5"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["start_btn_time5"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["start_btn_time5"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["start_btn_date5"];
			$dakokuDateTime["startEnd"] = 's';
			array_push($ret,$dakokuDateTime);
		}

		if(strlen($data_atdbkrslt["end_btn_time5"])>0){
			$dakokuDateTime = array();

			if($prevTime>$data_atdbkrslt["end_btn_time5"]){
				$targetDate++;
			}
			$prevTime = $data_atdbkrslt["end_btn_time5"];

			$dakokuDateTime["targetDate"] = $targetDate;
			$dakokuDateTime["btn_time"] = $data_atdbkrslt["end_btn_time5"];
			$dakokuDateTime["btn_date"] = $data_atdbkrslt["end_btn_date5"];
			$dakokuDateTime["startEnd"] = 'e';
			array_push($ret,$dakokuDateTime);
		}

		return $ret;
	}

	/**
	 * 勤務実績の新規登録.
	 * @param integer $emp_id 職員id.
	 * @param string $target_date 対象年月日(YYYYMMDD).
	 * @param string $time 打刻時刻(hhmm).
	 * @param string $tcdate 打刻日付(YYYYMMDD) 20141215.
	 * @param ArrayObject $target_atdbk (勤務予定).
	 * @param integer $previous_day_flag 前日フラグ(1: on, 0:off).
	 * @return number
	 */
	function insertAtdbkRslt($emp_id, $target_date, $time, $tcdate, $target_atdbk, $previous_day_flag, $next_day_flag=null, $timeIsStartTime=true){
		if ($target_atdbk["pattern"] != "") {
			$pattern = $target_atdbk["pattern"];
		}
		//出勤予定未登録時の標準の勤務パターンを使用
		else {
			$pattern = $arr_emp_id["$emp_personal_id"]["atdptn_id"];
		}
		$reason = ""; //20111115 前日の事由が設定される不具合
		if ($target_atdbk["reason"] != "") {
			$reason = $target_atdbk["reason"];
		}
		$night_duty = "";
		if ($target_atdbk["night_duty"] != "") {
			$night_duty = $target_atdbk["night_duty"];
		}
		$allow_id = null; //20111122 ""ではインサート時にエラーとなるため
		if ($target_atdbk["allow_id"] != "") {
			$allow_id = $target_atdbk["allow_id"];
		}
		$tmcd_group_id = "";
		if ($target_atdbk["tmcd_group_id"] != "") {
			$tmcd_group_id = $target_atdbk["tmcd_group_id"];
		}
		//グループが未設定の場合、職員登録＞勤務条件の出勤グループから設定
		if ($tmcd_group_id == "") {
			$tmcd_group_id = $arr_emp_id["$emp_id"]["tmcd_group_id"];
		}
		//事由が未設定で時間帯の組合せにある場合、自動設定
		if ($reason == "" && $arr_atdptn_reason[$tmcd_group_id][$pattern] != "") {
			$reason = $arr_atdptn_reason[$tmcd_group_id][$pattern];
		}

		$add_columnName = "";
		if(!is_null($next_day_flag)){
			$add_columnName = ", next_day_flag ";
		}

		if($timeIsStartTime){
			$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, start_time, start_btn_time, reg_prg_flg, start_date, previous_day_flag, start_btn_date1, status $add_columnName ";
			$status = "1";
		}else{
			$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, end_time, end_btn_time, reg_prg_flg, end_date, previous_day_flag, end_btn_date1, status $add_columnName ";
			$status = "3";
		}

		$sql .= ") values (";
		$content = array($emp_id, $target_date, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $time, $time, '1', $target_date, $previous_day_flag, $tcdate, $status);

		if(!is_null($next_day_flag)){
			array_push($content, $next_day_flag);
		}

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			$this->dberr_msg = pg_last_error($this->_db_con);
			pg_query($this->_db_con,"rollback");
			if ($this->mode == "2") {
				return -1;
			}
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$this->arr_atdbkrslt["$emp_id"]["$target_date"]["date"] = $target_date;
		$this->arr_atdbkrslt["$emp_id"]["$target_date"]["pattern"] = $pattern;
		$this->arr_atdbkrslt["$emp_id"]["$target_date"]["reason"] = $reason;
		$this->arr_atdbkrslt["$emp_id"]["$target_date"]["tmcd_group_id"] = $tmcd_group_id;

		if($timeIsStartTime){
			$this->arr_atdbkrslt["$emp_id"]["$target_date"]["start_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$target_date"]["start_btn_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$target_date"]["start_btn_date1"] = $tcdate;
		}else{
			$this->arr_atdbkrslt["$emp_id"]["$target_date"]["end_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$target_date"]["ebd_btn_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$target_date"]["end_btn_date1"] = $tcdate;
		}

		$this->arr_atdbkrslt["$emp_id"]["$target_date"]["previous_day_flag"] = $previous_day_flag;
	}


	/**
	 * 勤務実績の更新.
	 * @param integer $emp_id 職員id.
	 * @param string $target_date 対象年月日(YYYYMMDD).
	 * @param string $time 打刻時刻(hhmm).
	 * @param string $tcdate 打刻日付(YYYYMMDD) 20141215.
	 * @param ArrayObject $target_atdbkrslt 勤務実績の連想配列.
	 * @param integer $previous_day_flag 前日フラグ(1: on, 0:off).
	 * @return number
	 */
	function updateAtdbkRslt($emp_id, $target_date, $time, $tcdate, $target_atdbkrslt, $previous_day_flag, $next_day_flag=null, $timeIsStartTime=true){

		$wk_date = $target_atdbkrslt["date"];

		if($timeIsStartTime){
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["start_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["start_btn_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["start_btn_date1"] = $tcdate;
			$set = array("start_time", "start_btn_time", "reg_prg_flg", "start_date", "previous_day_flag", "start_btn_date1", "status");
			$status = "1";
		}else{
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["end_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["end_btn_time"] = $time;
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["end_btn_date1"] = $tcdate;
			$set = array("end_time", "end_btn_time", "reg_prg_flg", "end_date", "previous_day_flag", "end_btn_date1", "status");
			$status = "3";
		}

		$setvalue = array($time, $time, '1', $wk_date, $previous_day_flag, $tcdate, $status);

		if(!is_null($next_day_flag)){
			array_push($set, "next_day_flag");
			array_push($setvalue, $next_day_flag);
		}

		if ($target_atdbkrslt["pattern"] == "" && $pattern != "") {
			array_push($set, "pattern");
			array_push($setvalue, $pattern);
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["pattern"] = $pattern;
		}
		if ($target_atdbkrslt["reason"] == "" && $reason != "") {
			array_push($set, "reason");
			array_push($setvalue, $reason);
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["reason"] = $reason;
		}
		if ($target_atdbkrslt["night_duty"] == "" && $night_duty != "") {
			array_push($set, "night_duty");
			array_push($setvalue, $night_duty);
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["night_duty"] = $night_duty;
		}
		if ($target_atdbkrslt["allow_id"] == "" && $allow_id != "") {
			array_push($set, "allow_id");
			array_push($setvalue, $allow_id);
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["allow_id"] = $allow_id;
		}
		if ($target_atdbkrslt["tmcd_group_id"] == "" && $tmcd_group_id != "") {
			array_push($set, "tmcd_group_id");
			array_push($setvalue, $tmcd_group_id);
			$this->arr_atdbkrslt["$emp_id"]["$wk_date"]["tmcd_group_id"] = $tmcd_group_id;
		}

		if($this->import_5time_flg){
			if($this->tcTimeInBtnTimeList(true, $target_atdbkrslt, $time)){
				// 勤務実績を更新しない
				$rtn_info["rtncd"] = 2;
			}else{
				// 上限まで打刻可能なら]勤務実績.開始時刻に追加
				if(!$this->updateByTcTime($emp_id, $timeIsStartTime, $target_atdbkrslt, $time, $tcdate)){
					$rtn_info["rtncd"] = 1;
					$arr_msg[] = "打刻上限エラー";
				}
			}
		}

		$this->update_atdbkrslt_column($set, $setvalue, $emp_id, $wk_date);
		if ($this->get_dberr_msg() != "") {
			return -1;
		}
	}


	/**
	 * 時間差の算出(前日の勤務パターンの終了時刻と開始時刻の差).
	 * @param unknown $start 開始時間(HH:MM)
	 * @param unknown $end 終了時間(HH:MM)
	 * @param unknown $returnValueIsAbsolute 1:時間差は絶対値で返却, 0:計算結果をそのまま返却
	 * @return number
	 */
	function calcDiffHour($start, $end, $returnValueIsAbsolute){
		$diff = 0;
		list($endH,$endM) = split(":", $end);
		list($startH, $startM) = split(":", $start);

		if($start>$end){
			$diff = mktime($endH, $endM, 0, 1, 2, 1970) - mktime($startH, $startM, 0, 1, 1, 1970);
		}else{
			$diff = mktime($endH, $endM, 0, 1, 1, 1970)  - mktime($startH, $startM, 0, 1, 1, 1970);
		}

		if($returnValueIsAbsolute && $diff<0){
			$diff = $diff * -1;
		}
		return $diff / 3600;
	}

	/**
	 * 時間差の算出2(前日の勤務パターンの終了時刻と退勤打刻の時間との差).
	 * @param unknown $start 開始時間(HH:MM)
	 * @param unknown $end 終了時間(HH:MM)
	 * @param unknown $returnValueIsAbsolute 1:時間差は絶対値で返却, 0:計算結果をそのまま返却
	 * @return number
	 */
	function calcDiffHour2($start, $end, $returnValueIsAbsolute){
		$diff = 0;
		list($endH,$endM) = split(":", $end);
		list($startH, $startM) = split(":", $start);

		$diff = mktime($endH, $endM, 0, 1, 1, 1970)  - mktime($startH, $startM, 0, 1, 1, 1970);

		if($returnValueIsAbsolute && $diff<0){
			$diff = $diff * -1;
		}
		return $diff / 3600;
	}

	/**
	 * 勤務実績の退勤打刻がまだ打刻されていないかどうか
	 * @param array $atdbkrslt 勤務実績データ
	 * @return boolean true: 退勤打刻なし(あるいは勤務実績データなし), false: 退勤打刻あり
	 */
	function endBtnTimeIsEmpty($atdbkrslt){
		$ret = false;
		if(is_null($atdbkrslt)){
			return true;
		}

		if(array_key_exists('end_btn_time', $atdbkrslt)){
			if(is_null($atdbkrslt['end_btn_time'])){
				return true;
			}
            $wk_chk = $atdbkrslt['end_btn_time']; //未設定チェック追加20141016
            if ($wk_chk == "") {
                return true;
            }
		}else{
			return true;
		}

		$atdbkrsltProperties="";
		foreach ($atdbkrslt as $key => $atdbkrsltProperty) {
			if($key=='end_btn_time2'||$key=='end_btn_time3'||$key=='end_btn_time4'||$key=='end_btn_time5'){
				$atdbkrsltProperties += $atdbkrsltProperty;
			}
		}

		if($atdbkrsltProperties!=""){
			return false;
		}

		return $ret;
	}

	/**
	 * 会議研修時刻の更新 20151119
	 * 
	 * @param string $emp_id 内部用職員ID
	 * @param string $date 日付
	 * @param array $arr_atdbk 勤務予定レコード
	 * @param array $arr_emp_id 職員情報
	 * @param string $emp_personal_id 職員ID
	 * @return  integer 0:正常終了 -1:エラー
	 */
	function update_meeting_time($emp_id, $date, $arr_atdbk, $arr_emp_id, $emp_personal_id) {

		$arr_tmp_set = array();
		$arr_tmp_setvalue = array();

		//勤務パターングループ、予定、または、勤務条件から取得
		$wk_tmcd_group_id = ($arr_atdbk["$emp_id"]["$date"]["tmcd_group_id"] != "") ?
			$arr_atdbk["$emp_id"]["$date"]["tmcd_group_id"] : $arr_emp_id["$emp_personal_id"]["tmcd_group_id"];

		// 実績の勤務パターンがなく、予定の勤務パターンがある場合、その勤務パターンの時間帯データの会議時刻を使用
		if ($wk_tmcd_group_id != "" &&
				$this->arr_atdbkrslt["$emp_id"]["$date"]["org_pattern"] == "" &&
				$arr_atdbk["$emp_id"]["$date"]["pattern"] != "") {
			$arr_meeting_time = $this->get_meeting_time($wk_tmcd_group_id, $arr_atdbk["$emp_id"]["$date"]["pattern"]);
			$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_start_time"] = $arr_meeting_time["meeting_start_time"];
			$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_end_time"] = $arr_meeting_time["meeting_end_time"];
		}
		if ($this->arr_atdbkrslt["$emp_id"]["$date"]["meeting_start_time"] == "" &&
				$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_start_time"] != "") {
			array_push($arr_tmp_set, "meeting_start_time");
			array_push($arr_tmp_setvalue, $this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_start_time"]);
		}
		if ($this->arr_atdbkrslt["$emp_id"]["$date"]["meeting_end_time"] == "" &&
				$this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_end_time"] != "") {
			array_push($arr_tmp_set, "meeting_end_time");
			array_push($arr_tmp_setvalue, $this->arr_atdbkrslt["$emp_id"]["$date"]["a_meeting_end_time"]);
		}
		if (count($arr_tmp_set) > 0) {
			$this->update_atdbkrslt_column($arr_tmp_set, $arr_tmp_setvalue, $emp_id, $date);
			if ($this->get_dberr_msg() != "") {
				$this->dberr_msg = pg_last_error($this->_db_con);
				pg_query($this->_db_con, "rollback");
				if ($this->mode == "2") {
					return -1;
				}
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		return 0;
	}

}

?>