<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$check_auth = check_authority($session, 21, $fname);
if ($check_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// チェックされた病棟をループ
if (is_array($del_ward)) {
	foreach ($del_ward as $bldgwd) {
		list($bldg_cd, $ward_cd) = split("-", $bldgwd);

		// 病棟名の取得
		$sql = "select ward_name from wdmst";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$ward_name = pg_fetch_result($sel, 0, "ward_name");

		// 指定病棟に属する病室の数を取得
		$sql = "select count(*) from ptrmmst";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$ptrm_count = pg_fetch_result($sel, 0, 0);

		// 病室が存在する場合はエラーとする
		if ($ptrm_count > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert('{$ward_name}は病室が登録されているため削除できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// 病棟情報の論理削除
		$sql = "update wdmst set";
		$set = array("ward_del_flg");
		$setvalue = array("t");
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 病棟一覧画面を再表示
echo("<script language=\"javascript\">location.href = 'ward_list.php?session=$session'</script>");
?>
