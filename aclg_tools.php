<?
ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("aclg_tools_array.php");
require_once("yui_calendar_util.ini");
require_once("show_select_values.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 92, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//DB接続
$con = connect2db($fname);
if ($con == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// DBコネクションの作成
$con = connect2db($fname);

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
    $lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}


?>
<title>CoMedix アクセスログ | 機能別詳細</title>
<?
		// 外部ファイルを読み込む
		write_yui_calendar_use_file_read_0_12_2();

		// カレンダー作成、関数出力
		write_yui_calendar_script2(2);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td { border:#5279a5 solid 1px; text-align:left }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onresize="adjustListWidth();" onload="initcal();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="header_menu_table">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="aclg_menu.php?session=<? echo($session); ?>"><img src="img/icon/b_dmy_log.gif" width="32" height="32" border="0" alt="アクセスログ"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="aclg_menu.php?session=<? echo($session); ?>"><b>アクセスログ</b></a></font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td width="5">
<img src="img/spacer.gif" width="5" height="1" alt="">
</td>

<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!--
<td colspan="5" width="5">
<img src="img/spacer.gif" width="5" height="1" alt="">
</td>

<td>
 -->
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="150" align="center" bgcolor="#bdd1e7"><a href="./aclg_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧（ユーザ）</font></a></td>
<td width="5">&nbsp;</td>
<td width="150" align="center" bgcolor="#bdd1e7"><a href="./aclg_menu_admin.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧（管理画面）</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="./aclg_tools.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>機能別詳細</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="./aclg_config.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定</font></a></td>
<td></td>
</tr>
</table>
</td></tr>

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>
</td></tr>

</table>

</td></tr>

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="5" alt=""></td>
</tr>
</table>
</td></tr>

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">

<td valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="160" border="0" cellspacing="0" cellpadding="0" id="left_sub_menu">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? $targets = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,  17,18,19,22); ?>
<? foreach ($targets as $i) { ?>
<tr>
<?
	// ライセンス無しは飛ばす
	if ($i==22 && $lcs_func[31]!="t") continue;
	// スタッフ・ポートフォリオの詳細ログは一旦保留
	if ($i==22) continue;

if($logcate == $tools[$i][1]) {
		$str_bolds = "<td align=\"center\" bgcolor=\"#FFDDFD\">&nbsp;{$font}<b>・</b></font></td><td width=\"90%\" bgcolor=\"#FFDDFD\"><b>";
		$str_bolde = "</b>";
	} else {
		$str_bolds = "<td align=\"center\">&nbsp;{$font}<b>・</b></font></td><td width=\"90%\">";
		$str_bolde = "";
	}
	echo($str_bolds);
?>
	<?=$font ?><a href="aclg_tools.php?session=<?=$session ?>&set_prty=<?=$set_prty ?>&logcate=<?=$tools[$i][1] ?>&aclg_init=t&emp_name=<?echo(urlencode($emp_name));?>&date_y1=<?echo($date_y1);?>&date_m1=<?echo($date_m1);?>&date_d1=<?echo($date_d1);?>&date_y2=<?echo($date_y2);?>&date_m2=<?echo($date_m2);?>&date_d2=<?echo($date_d2);?>&page=0&set_sort=<?echo($set_sort);?>"><?=$tools[$i][0] ?>
	</a>
<?
	echo($str_bolde);
?>
	</font>
</td>
</tr>
<? } ?>
</table>
</td>
<td bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<!-- td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td -->

</tr>
</table>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td width="100%" valign="top">
<?
//==========================================================================
// 検索条件
//==========================================================================
if(empty($date_y1)) {
	$date_y1 = date("Y");
}
if(empty($date_m1)) {
	$date_m1 = date("m");
}
if(empty($date_d1)) {
	$date_d1 = date("d");
}
if(empty($date_y2)) {
	$date_y2 = date("Y");
}
if(empty($date_m2)) {
	$date_m2 = date("m");
}
if(empty($date_d2)) {
	$date_d2 = date("d");
}

$date_ymd1 = $date_y1.$date_m1.$date_d1;
$date_ymd2 = $date_y2.$date_m2.$date_d2;
?>
<table>
<tr>

<td>
<form name="apply" id="apply" action="aclg_tools.php" method="post">
<script type="text/javascript">
	function adjustListWidth(){
		if (!document.getElementById("div_data_field")) return;
		document.getElementById("div_data_field").style.width =
			document.getElementById("header_menu_table").clientWidth - document.getElementById("left_sub_menu").clientWidth - 20;
	}

function post_setting(status){

	if(document.apply.date_y1.value=="-" ||
	document.apply.date_y2.value=="-" ||
	document.apply.date_m1.value=="-" ||
	document.apply.date_m2.value=="-" ||
	document.apply.date_d1.value=="-" ||
	document.apply.date_d2.value=="-" )
	{
		alert("日付を指定して下さい");
		return;
	}



	if(status == '1')
	{
		//ログ削除処理
		ret = confirm("指定された期間で削除しますがよろしいですか");
		if(ret == false)
		{
			return;
		}
	}

	document.apply.regstat.value=status;

	applyForm = document.getElementById("apply");

	//検索・表示処理
    applyForm.action = 'aclg_tools.php';

    applyForm.method = 'post';
	document.apply.submit();
}

function csv_setting(){
	if(document.apply.date_y1.value=="-" ||
	document.apply.date_y2.value=="-" ||
	document.apply.date_m1.value=="-" ||
	document.apply.date_m2.value=="-" ||
	document.apply.date_d1.value=="-" ||
	document.apply.date_d2.value=="-" )
	{
		alert("日付を指定して下さい");
		return;
	}


	applyForm = document.getElementById("apply");

	//CSV出力処理
    applyForm.action = 'aclg_tools_csv.php';

    applyForm.method = 'post';
	document.apply.submit();
}
<?
//構造体
$str_func = array();
$str_priority = array();

$str_func["00"] = "登録";
$str_func["01"] = "更新";
$str_func["02"] = "削除";
$str_func["03"] = "ダウンロード";
$str_func["04"] = "メール送信";
$str_func["05"] = "メール返信";
$str_func["06"] = "メール転送";
$str_func["07"] = "承認";
$str_func["08"] = "差戻し";
$str_func["09"] = "再申請";
$str_func["10"] = "印刷";
$str_func["11"] = "参照";

$str_priority["0"] = "重要情報";
$str_priority["1"] = "一般情報";
$str_priority["2"] = "付加情報";

//==========================================================================
// 一覧
//==========================================================================
if($regstat == "1")
{
	//ログの削除処理

	$cond = "";
	if($date_ymd1 != '') {
		$cond .= "where $date_ymd1 <= to_number(substr(ac_date,1,8),'00000000')";
	}

	if($date_ymd2 != '') {
		if($date_ymd1 != '') {
			$cond .= " and to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
		else {
			$cond .= "where to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
	}

	if($logcate != "")
	{
		if($date_ymd1 != '' || $date_ymd2 != '')
		{
			$cond .= " and aclg_data.cate_id = '$logcate'";
		}
		else {
			$cond .= "where aclg_data.cate_id = '$logcate'";
		}

	}

	$cond .= " order by ac_date";
	$sql = "INSERT into aclg_data_bak SELECT * FROM aclg_data ".$cond;

	$sel = insert_into_table_no_content($con, $sql, $fname);
	//$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$sql = "delete FROM aclg_data ";
	$cond = "";
	if($date_ymd1 != '') {
		$cond .= "where $date_ymd1 <= to_number(substr(ac_date,1,8),'00000000')";
	}

	if($date_ymd2 != '') {
		if($date_ymd1 != '') {
			$cond .= " and to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
		else {
			$cond .= "where to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
	}

	if($logcate != '')
	{
		if($date_ymd1 != '' || $date_ymd2 != '')
		{
			$cond .= " and aclg_data.cate_id = '$logcate'";
		}
		else {
			$cond .= "where aclg_data.cate_id = '$logcate'";
		}

	}

	if($emp_name != "")
	{
		if($date_ymd1 != '' || $date_ymd2 != '' || $logcate != '')
		{
			$cond .= " and aclg_data.cate_id = '$logcate'";
		}
		else {
			$cond .= "where aclg_data.cate_id = '$logcate'";
		}

	}


	$sel = delete_from_table($con, $sql, $cond, $fname);
	//$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

if($aclg_init == 't' && $date_y1 != '' )
{
	$limit = 20;
	$sql = "select aclg_data.emp_id,aclg_data.data,empmst.emp_lt_nm,empmst.emp_ft_nm,ac_date, aclg_data.cate_id,aclg_data.detail_id, aclg_data_link.log_cate,aclg_data_link.log_detail,aclg_data_link.priority,aclg_data_link.log_type,aclg_data.status1 from aclg_data";
	$cond = " join aclg_data_link on aclg_data.cate_id = aclg_data_link.cate_id and aclg_data.detail_id = aclg_data_link.detail_id and aclg_data.filename = aclg_data_link.filename and aclg_data.cate_id != '0006' ";
	if($set_prty != "0")
	{
		$cond .= " and aclg_data_link.priority = '".$set_prty."' ";
	}

	$cond .= " join empmst on empmst.emp_id=aclg_data.emp_id ";
	if($emp_name != "")
	{
		$fullnm = str_replace(" ", "", $emp_name);
		$fullnm = str_replace("　", "", $fullnm);
		$cond .= "and (emp_lt_nm like '%$emp_name%' or emp_ft_nm like '%$emp_name%' or emp_kn_lt_nm like '%$emp_name%' or emp_kn_ft_nm like '%$emp_name%' or (emp_lt_nm || emp_ft_nm) like '%$fullnm%' or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$fullnm%')";
	}

	if($date_ymd1 != '') {
		$cond .= "where $date_ymd1 <= to_number(substr(ac_date,1,8),'00000000')";
	}

	if($date_ymd2 != '') {
		if($date_ymd1 != '') {
			$cond .= " and to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
		else {
			$cond .= "where to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
	}

	if($logcate != "")
	{
		if($date_ymd1 != '' || $date_ymd2 != '')
		{
			$cond .= " and aclg_data.cate_id = '$logcate'";
		}
		else {
			$cond .= "where aclg_data.cate_id = '$logcate'";
		}
	}

	$cond .= " order by ac_date";

	// [降順]に並べ替える
	if ($set_sort === '2') {
		$cond .= " desc";
	}

	$cond .= " limit $limit";
	if ($page > 0) {
		$offset = $page * $limit;
		$cond .= " offset $offset";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>

	</script>
		<input type="hidden" name="session" value="<?=$session ?>">
		<input type="hidden" name="logcate" value="<?=$logcate ?>">
		<input type="hidden" name="regstat" value="">
		<input type="hidden" name="aclg_init" value="t">
		<select id="date_y1" name="date_y1"><? show_select_years(10, $date_y1, true); ?></select><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>年</font><select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>月</font><select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>日</font>
	</td>
	<td>
		<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br />
	</td>
	<td>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
		</font>
	</td>
	<td>&nbsp;<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>〜</font></td>
	<td><select id="date_y2" name="date_y2"><? show_select_years(10, $date_y2, true); ?></select><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>年</font><select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>月</font><select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>日</font>
	</td>
	<td>
		<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br />
	</td>
	<td>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
		</font>
	</td>
</tr>
<tr>
	<td colspan="4"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>職員名：<input type="text" name="emp_name" value="<? echo($emp_name); ?>" size="30" maxlength="40" style="ime-mode:active;"></font></td>
	<td>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>レベル：
<?
echo "<select name=\"set_prty\">";
echo "<option value=\"0\"";
echo ($set_prty === '0') ? "selected></option>" : "></option>";
echo "<option value=\"1\"";
echo ($set_prty === '1') ? "selected>重要情報</option>" : ">重要情報</option>";
echo "<option value=\"2\"";
echo ($set_prty === '2') ? "selected>一般情報</option>" : ">一般情報</option>";
echo "<option value=\"3\"";
echo ($set_prty === '3') ? "selected>付加情報</option>" : ">付加情報</option>";
echo "</select>";
?>
		</font>
	</td>
</tr>
</table>
<table style="width:100%">
<tr>
	<td colspan="7">
		<input type="button" value="検索" style="width:150px;" onclick="post_setting('');" />
		<input type="button" value="csv出力"  onclick="csv_setting();"/>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>アクセス日時：
<?
		echo "<select name=\"set_sort\" onChange=\"this.form.action = 'aclg_tools.php'; this.form.submit();\">";
		echo "<option value=\"1\"";
		echo ($set_sort === '1') ? "selected>昇順</option>" : ">昇順</option>";
		echo "<option value=\"2\"";
		echo ($set_sort === '2') ? "selected>降順</option>" : ">降順</option>";
		echo "</select>";
?>
		</font>
	</td>
	<td align="right">
		<input type="button" value="指定期間ログ削除"  onclick="post_setting('1');"/>
	</td>
</tr>
</table>
<?
if (	isset($date_y1) || isset($date_y2) || isset($date_m1) ||
		isset($date_m2) || isset($date_d1) || isset($date_d2)
	) {
		if ($date_ymd1 != '' or $date_ymd2 != '') {
			show_next($set_prty,$con, $session, $fname, $tools, $emp_name, $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_ymd1, $date_ymd2, $logcate, $set_sort, $page);
		}
}
?>
</form>
<div id="div_data_field">
<table border="0" cellspacing="0" cellpadding="2" class="list" style="width:100%">
<tr>
<td><b><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>アクセス日時</font></b></td>
<td><b><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>職員名</font></b></td>
<td><b><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>機能</font></b></td>
<td><b><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>処理</font></b></td>
<td><b><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>レベル</font></b></td>
<td><b><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>内容</font></b></td>
</tr>
<?
while($row = @pg_fetch_array($sel)) {
	switch ($row["priority"]){
		case '1':
			$priority = "重要情報";
			break;
		case '2':
			$priority = "一般情報";
			break;
		case '3':
			$priority = "付加情報";
			break;
		default :
			$priority = "取得しない";
			break;
	}
	$yyyy = substr($row["ac_date"],0,4);
	$mm = substr($row["ac_date"],4,2);
	$dd = substr($row["ac_date"],6,2);
	$HH = substr($row["ac_date"],8,2);
	$MM = substr($row["ac_date"],10,2);
	$SS = substr($row["ac_date"],12,2);

	//内容
	$aclg_contents = null;
	switch ($row["cate_id"]) {
		case "0001":// ログイン
		case "0002":// 職員登録
		case "0003"://マスターメンテナンス
		case "0006":// 日別一覧
		case "0101":// ウェブメール
		case "0103"://スケジュール
		case "0104":// 委員会・WG
		case "0105":// お知らせ・回覧板
		case "0106":// タスク
		case "0107":// 伝言メモ
		case "0108":// 設備予約
		case "0109":// 決裁・申請
		case "0110":// 掲示板・電子会議室
		case "0111":// Q&A
		case "0112":// 文書管理
		case "0113":// アドレス帳
		case "0114":// リンクライブラリ
		case "0115":// 内線電話帳
		case "0201":// ファントルくん
		case "0202":// スタッフ・ポートフォリオ
			$aclg_contents = $row["status1"];
			break;
		default:
			$aclg_contents = $row["data"];
			break;
	}
?>
<tr>
<td width=160><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?php echo $yyyy."/".$mm."/".$dd." ".$HH.":".$MM.":".$SS; ?></font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?php echo $row["emp_lt_nm"]." ".$row["emp_ft_nm"]; ?></font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?php echo $row["log_cate"]; ?></font></td>
<?
/*
 * 「報告書の進捗管理の編集（GRM以外編集ロック含む）」はGRMの略称が変わるため
 * aclg_data_link テーブル内の[log_cate]の値は「報告書の進捗管理の編集」としているので、
 * 表示時に当該ログのみ（[略称]以外編集ロック）の文字列を付加しています。
 */
?>
<? if ($row['cate_id'] == '0201' && $row['detail_id'] == '13') {
	// GRMの略称を取得
	$fsql = "select auth_short_name from inci_auth_mst";
	$fcond = "where auth='SM'";
	$fsel = select_from_table($con, $fsql, $fcond, $fname);
	if ($fsel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$auth_short_name = pg_fetch_result($fsel, 0, "auth_short_name");
?>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?php echo $row["log_detail"]."（".$auth_short_name."以外編集ロック含む）"; ?></font></td>
<? } else { ?>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?php echo $row["log_detail"]; ?></font></td>
<? } ?>
<td width=70><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?php echo $priority; ?></font></td>
<td style="word-break: break-all;"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?php echo $aclg_contents ?></font></td>
</tr>
<? } ?>
</table>
</div>
<!--
<? if (count($data) > 0) { ?>
<div style="overflow-x:scroll" id="div_data_field">
<table border="0" cellspacing="0" cellpadding="2" class="list" style="width:100%">
<tr style="background-color:#f6f9ff">
<td style="text-align:left; background-color:#f6f9ff; white-space:nowrap"><?=$font?>基本機能</font></td>
<? for ($i=1; $i<=$day_max; $i++){ ?><td><?= $i>0 ? $i : "" ?></td><? } ?>
</tr>
<? foreach($data as $optype_id => $daylist){ ?>
<tr>
<td style="text-align:left; background-color:#f6f9ff; white-space:nowrap"><?=$font?><?=@$opnames[$optype_id]?></font></td>
<? for($i = 1; $i <= $day_max; $i++){ ?>
<td width="3%"><?=@$daylist[$i]?></td>
<? } ?>
</tr>
<? } ?>
</table>
</div>
<? } ?>
-->


</td>
</tr>
</table>
</td></tr>
</td>
</table>
</td></tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_next($set_prty,$con, $session, $fname, $tools, $emp_name, $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_ymd1, $date_ymd2, $logcate, $set_sort, $page) {
	$limit = 20;
	$sql = "select aclg_data.emp_id,aclg_data.data,empmst.emp_lt_nm,empmst.emp_ft_nm,ac_date, aclg_data.cate_id,aclg_data.detail_id, aclg_data_link.log_cate,aclg_data_link.log_detail,aclg_data_link.priority,aclg_data_link.log_type from aclg_data";
	$cond = " join aclg_data_link on aclg_data.cate_id = aclg_data_link.cate_id and aclg_data.detail_id = aclg_data_link.detail_id and aclg_data.filename = aclg_data_link.filename and aclg_data.cate_id != '0006' ";
	if($set_prty != "0")
	{
		$cond .= " and aclg_data_link.priority = '".$set_prty."' ";
	}

	$cond .= " join empmst on empmst.emp_id=aclg_data.emp_id ";
	if($emp_name != "")
	{
		$fullnm = str_replace(" ", "", $emp_name);
		$fullnm = str_replace("　", "", $fullnm);
		$cond .= "and (emp_lt_nm like '%$emp_name%' or emp_ft_nm like '%$emp_name%' or emp_kn_lt_nm like '%$emp_name%' or emp_kn_ft_nm like '%$emp_name%' or (emp_lt_nm || emp_ft_nm) like '%$fullnm%' or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$fullnm%')";
	}

	if($date_ymd1 != '') {
		$cond .= "where $date_ymd1 <= to_number(substr(ac_date,1,8),'00000000')";
	}

	if($date_ymd2 != '') {
		if($date_ymd1 != '') {
			$cond .= " and to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
		else {
			$cond .= "where to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
	}

	if($logcate != "")
	{
		if($date_ymd1 != '' || $date_ymd2 != '')
		{
			$cond .= " and aclg_data.cate_id = '$logcate'";
		}
		else {
			$cond .= "where aclg_data.cate_id = '$logcate'";
		}
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$aclg_count = intval(pg_numrows($sel));

	$total_page = ceil($aclg_count / $limit);

	if ($total_page <= 1) {
		return;
	}

	$dispPage = ($limit * $page) + 1;

	echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">（{$dispPage}件目 / {$aclg_count}件中）</font></td>");
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	echo("ページ&nbsp;");

	if ($page > 0) {
		echo("<a href=\"aclg_tools.php?session={$session}&logcate={$logcate}&aclg_init=t&emp_name=".urlencode($emp_name)."&set_prty={$set_prty}&date_y1={$date_y1}&date_m1={$date_m1}&date_d1={$date_d1}&date_y2={$date_y2}&date_m2={$date_m2}&date_d2={$date_d2}&set_sort={$set_sort}&page=0\">|&lt;&lt;</a>&nbsp;");
		echo("<a href=\"aclg_tools.php?session={$session}&logcate={$logcate}&aclg_init=t&emp_name=".urlencode($emp_name)."&set_prty={$set_prty}&date_y1={$date_y1}&date_m1={$date_m1}&date_d1={$date_d1}&date_y2={$date_y2}&date_m2={$date_m2}&date_d2={$date_d2}&set_sort={$set_sort}&page=" . ($page - 1) ."\">←</a>&nbsp;");
	} else {
		echo("&nbsp;");
	}
	$min = $page - 4;
	$max = $page + 4;
	if(($total_page - 1) < $max){
		$max = ($max - ($total_page - 1)) + 4;
	} else {
		$max = 4;
	}

	if(0 > $min){
		$min = (0 - $min) + 4;
	} else {
		$min = 4;
	}

	for($i = $max; $i > 0; $i--){
		if(($page - $i) >= 0){
			echo("<a href=\"aclg_tools.php?session={$session}&logcate={$logcate}&aclg_init=t&emp_name=".urlencode($emp_name)."&set_prty={$set_prty}&date_y1={$date_y1}&date_m1={$date_m1}&date_d1={$date_d1}&date_y2={$date_y2}&date_m2={$date_m2}&date_d2={$date_d2}&set_sort={$set_sort}&page=".($page - $i)."\">".(($page - $i) + 1)."</a>&nbsp;");
		}
	}

	echo("[".($page + 1)."]&nbsp;");

	for($i = 1; $i <= $min; $i++){
		if(($page + $i) <= ($total_page - 1)){
			echo("<a href=\"aclg_tools.php?session={$session}&logcate={$logcate}&aclg_init=t&emp_name=".urlencode($emp_name)."&set_prty={$set_prty}&date_y1={$date_y1}&date_m1={$date_m1}&date_d1={$date_d1}&date_y2={$date_y2}&date_m2={$date_m2}&date_d2={$date_d2}&set_sort={$set_sort}&page=".($page + $i)."\">".(($page + $i) + 1)."</a>&nbsp;");
		}
	}
	if ($page < ($total_page - 1)) {
		echo("<a href=\"aclg_tools.php?session={$session}&logcate={$logcate}&aclg_init=t&emp_name=".urlencode($emp_name)."&set_prty={$set_prty}&date_y1={$date_y1}&date_m1={$date_m1}&date_d1={$date_d1}&date_y2={$date_y2}&date_m2={$date_m2}&date_d2={$date_d2}&set_sort={$set_sort}&page=".($page + 1)."\">→</a>&nbsp;");
		echo("<a href=\"aclg_tools.php?session={$session}&logcate={$logcate}&aclg_init=t&emp_name=".urlencode($emp_name)."&set_prty={$set_prty}&date_y1={$date_y1}&date_m1={$date_m1}&date_d1={$date_d1}&date_y2={$date_y2}&date_m2={$date_m2}&date_d2={$date_d2}&set_sort={$set_sort}&page=".($total_page - 1)."\">&gt;&gt;|</a>&nbsp;");
	} else {
		echo("&nbsp;");
	}
	echo("</font></td>\n");
	echo("</tr>\n");
	echo("</table><br />\n");
}
