<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

if (strlen($note) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('備考は全角50文字までで入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

$con = connect2db($fname);
pg_query($con, "begin");

// 備考をDELETE〜INSERT
$sql = "delete from bed_repnote";
$cond = "where note_key = '$key'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

if ($note != "") {
	$sql = "insert into bed_repnote (note_key, note_val) values (";
	$content = array($key, $note);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">opener.location.reload(true); self.close();</script>");
