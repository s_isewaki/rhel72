<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | タイムカード入力</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("atdbk_menu_common.ini");

require_once './kintai/common/mdb_wrapper.php';
require_once './kintai/common/user_info.php';
require_once './kintai/common/master_util.php';
require_once './kintai/common/code_util.php';
require_once './kintai/common/date_util.php';

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
//出張有無使用フラグ
$opt_trip_flag = file_exists("opt/trip_flag");

//フォームデータ
$yyyymm = $_REQUEST["yyyymm"];


// 勤務管理権限の取得
$work_admin_auth = check_authority($session, 42, $fname);

//データアクセス
try {

	MDBWrapper::init(); // おまじない
	$log = new common_log_class(basename(__FILE__));
	$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

	$db->beginTransaction(); // トランザクション開始

	//職員情報
	$ukey  = $session;
	$uType = "s";
	$user_info   = UserInfo::get($ukey, $uType);
	$base_info   = $user_info->get_base_info();
	$job_info    = $user_info->get_job_info();
	$class_info  = $user_info->get_class_info();
	$attr_info   = $user_info->get_attr_info();
	$dept_info   = $user_info->get_dept_info();
	$croom_info  = $user_info->get_class_room_info();
	$cond_info   = $user_info->get_emp_condition();
	
	//組織階層
	$class_cnt  = MasterUtil::get_organization_level_count();
	
	//所属
	$shozoku = $class_info->class_nm."＞".$attr_info->atrb_nm."＞".$dept_info->dept_nm;
	if($class_cnt == 4){
		if($croom_info->room_nm != ""){
			$shozoku .= "＞".$croom_info->room_nm;
		}
	}
	
	
	//締め日情報を取得
	$closing_info  = MasterUtil::get_closing();
	if(is_null($closing_info)){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if($cond_info->duty_form == "2"){
		if($closing_info["closing_parttime"] != ""){
			$closing = $closing_info["closing_parttime"];
		}else{
			$closing = $closing_info["closing"];
		}
	}else{
		$closing = $closing_info["closing"];
	}
	
	//期間取得（月度考慮）
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);
	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);
	$prdArr = DateUtil::get_cutoff_period($cond_info->duty_form, $year, $month);
	
	//データ取得
	$sql  = "";
	$sql .= "SELECT ";
	$sql .= " atdbkrslt.emp_id, ";
	$sql .= " atdbkrslt.date, ";
	$sql .= " atdbkrslt.pattern, ";
	$sql .= " atdbkrslt.reason, ";
	$sql .= " atdbkrslt.tmcd_group_id, ";
	$sql .= " atdbkrslt.meeting_start_time, ";
	$sql .= " atdbkrslt.meeting_end_time, ";
	if ($opt_trip_flag) {
		$sql .= " atdbk_trip.trip_flag, ";
	}
	$sql .= " atdptn.atdptn_nm, ";
	$sql .= " atdptn.workday_count, ";
	$sql .= " atdptn.atdptn_order_no, ";
	$sql .= " atdbk_reason_mst.default_name, ";
	$sql .= " atdbk_reason_mst.display_name, ";
	$sql .= " timecard_paid_hol_hour.calc_minute, ";
	$sql .= " wktmgrp.group_name, ";
	$sql .= " kintai_ovtm.apply_date, ";
	$sql .= " kintai_ovtm.apply_stat, ";
	$sql .= " kintai_ovtm.cmmd_start_time1, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag1, ";
	$sql .= " kintai_ovtm.cmmd_end_time1, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag1, ";
	$sql .= " kintai_ovtm.cmmd_rest_time1, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.cmmd_start_time2, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag2, ";
	$sql .= " kintai_ovtm.cmmd_end_time2, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag2, ";
	$sql .= " kintai_ovtm.cmmd_rest_time2, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.cmmd_start_time3, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag3, ";
	$sql .= " kintai_ovtm.cmmd_end_time3, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag3, ";
	$sql .= " kintai_ovtm.cmmd_rest_time3, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.rslt_start_time1, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag1, ";
	$sql .= " kintai_ovtm.rslt_end_time1, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag1, ";
	$sql .= " kintai_ovtm.rslt_rest_time1, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.rslt_start_time2, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag2, ";
	$sql .= " kintai_ovtm.rslt_end_time2, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag2, ";
	$sql .= " kintai_ovtm.rslt_rest_time2, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.rslt_start_time3, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag3, ";
	$sql .= " kintai_ovtm.rslt_end_time3, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag3, ";
	$sql .= " kintai_ovtm.rslt_rest_time3, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.mdfy_start_time1, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag1, ";
	$sql .= " kintai_ovtm.mdfy_end_time1, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag1, ";
	$sql .= " kintai_ovtm.mdfy_rest_time1, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag1, ";
	$sql .= " kintai_ovtm.mdfy_start_time2, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag2, ";
	$sql .= " kintai_ovtm.mdfy_end_time2, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag2, ";
	$sql .= " kintai_ovtm.mdfy_rest_time2, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag2, ";
	$sql .= " kintai_ovtm.mdfy_start_time3, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag3, ";
	$sql .= " kintai_ovtm.mdfy_end_time3, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag3, ";
	$sql .= " kintai_ovtm.mdfy_rest_time3, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag3 ";
	$sql .= "FROM ";
	$sql .= " atdbkrslt ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdptn ";
	$sql .= "ON ";
	$sql .= " CAST(atdptn.atdptn_id AS varchar) = atdbkrslt.pattern AND ";
	$sql .= " atdptn.group_id  = atdbkrslt.tmcd_group_id ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdbk_reason_mst ";
	$sql .= "ON ";
	$sql .= " atdbk_reason_mst.reason_id = atdbkrslt.reason ";
	$sql .= "LEFT JOIN ";
	$sql .= " timecard_paid_hol_hour ";
	$sql .= "ON ";
	$sql .= " timecard_paid_hol_hour.emp_id       = atdbkrslt.emp_id AND ";
	$sql .= " timecard_paid_hol_hour.start_date   = atdbkrslt.date ";
	$sql .= "LEFT JOIN ";
	$sql .= " kintai_ovtm ";
	$sql .= "ON ";
	$sql .= " kintai_ovtm.emp_id = atdbkrslt.emp_id AND ";
	$sql .= " kintai_ovtm.date   = atdbkrslt.date ";
	if ($opt_trip_flag) {
		$sql .= "LEFT JOIN ";
		$sql .= " atdbk_trip ";
		$sql .= "ON ";
		$sql .= " atdbkrslt.emp_id = atdbk_trip.emp_id AND ";
		$sql .= " atdbkrslt.date   = atdbk_trip.date ";
	}
	$sql .= "LEFT JOIN ";
	$sql .= " wktmgrp ";
	$sql .= "ON ";
	$sql .= " wktmgrp.group_id   = atdbkrslt.tmcd_group_id ";
	$sql .= "WHERE ";
	$sql .= " atdbkrslt.emp_id = '".$base_info->emp_id."' AND ";
	$sql .= " (atdbkrslt.date >= '".$prdArr["start"]."' AND atdbkrslt.date <= '".$prdArr["end"]."') ";
	$retRec = $db->find($sql);
	
	$rData = array();	//データ
	$ovtm_ttl = 0;

	//表示日数
	$dcnt = DateUtil::date_diff($prdArr["start"], $prdArr["end"]);
	for($i=0;$i<$dcnt;$i++){
		$arcnt = date("Ymd", strtotime($prdArr["start"]." ".$i." day")); 
		$rData[$i]["Ymd"] = $arcnt;
		//背景色
		$rData[$i]["bgcolor"] = getBgColor($arcnt);
	}

	$ttlType = array();	//集計データ
	$ttlMeeting = 0;	//会議集計
	$ttlTimehol = 0;	//時間休集計
	for($out=0;$out<count($rData);$out++){
		//日付
		$rData[$out]["date"] = intval(substr($rData[$out]["Ymd"], 4, 2))."月".intval(substr($rData[$out]["Ymd"], 6, 2))."日";
		//曜日
		$rData[$out]["wday"] = DateUtil::day_of_week($rData[$out]["Ymd"]);

		for($i=0;$i<count($retRec);$i++){
			if($rData[$out]["Ymd"] == $retRec[$i]["date"]){
				//勤務実績
				$rData[$out]["atdptn_nm"] = $retRec[$i]["atdptn_nm"];
				//勤務実績集計
				$g = $retRec[$i]["tmcd_group_id"];
				//$p = $retRec[$i]["pattern"];
				$o = $retRec[$i]["atdptn_order_no"];	//表示順考慮
				//$ttlType[$g][$p]["name"] = $retRec[$i]["atdptn_nm"];
				$ttlType[$g][$o]["group"] = $retRec[$i]["group_name"];
				$ttlType[$g][$o]["name"]  = $retRec[$i]["atdptn_nm"];
				$ttlType[$g][$o]["gid"]   = $retRec[$i]["tmcd_group_id"];
				$ttlType[$g][$o]["pid"]   = $retRec[$i]["pattern"];
				$ttlType[$g][$o]["order"] = $retRec[$i]["atdptn_order_no"];
				$ttlType[$g][$o]["cnt"]   = floatval($ttlType[$g][$o]["cnt"]) + floatval($retRec[$i]["workday_count"]);
				
				//事由
				if($retRec[$i]["display_name"] != ""){
					$rData[$out]["reason_nm"] = $retRec[$i]["display_name"];
				}else{
					$rData[$out]["reason_nm"] = $retRec[$i]["default_name"];
				}
				
				// 出張
				if ($opt_trip_flag) {
					$rData[$out]["trip_flag"] = $retRec[$i]["trip_flag"];
				}
				
				//申請状態 0 1 2 3
                if(/*$retRec[$i]["apply_stat"] == 0 || $retRec[$i]["apply_stat"] == 1*/ in_array($retRec[$i]["apply_stat"], array(0,1,2,3))){
                    //申請状態
					$rData[$out]["apply_stat"] = CodeUtil::get_apply_status($retRec[$i]["apply_stat"]);
					for($j=1;$j<=3;$j++){
						if($retRec[$i]["mdfy_cancel_flag".$j] == 1){
							$rData[$out]["ovtm_t".$j] = "";
							$rData[$out]["ovtm_r".$j] = "";
							$rData[$out]["ovtm_b".$j] = "";
						}else{
							$fldName = "";
							if($retRec[$i]["mdfy_start_time".$j] != ""){
								$fldName = "mdfy";
							}else if($retRec[$i]["rslt_start_time".$j] != ""){
								$fldName = "rslt";
							}else{
								$fldName = "";
								$rData[$out]["ovtm_t".$j] = "";
								$rData[$out]["ovtm_r".$j] = "";
								$rData[$out]["ovtm_b".$j] = "";
							}
							if($fldName != ""){
								//時間外
								$rData[$out]["ovtm_t".$j] = "";
								if($retRec[$i][$fldName."_start_next_flag".$j] == 1){
									$rData[$out]["ovtm_t".$j] .= "翌";
								}
								$rData[$out]["ovtm_t".$j] .= intval(substr($retRec[$i][$fldName."_start_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_start_time".$j], 2, 2);
								$rData[$out]["ovtm_t".$j] .= "〜";
								if($retRec[$i][$fldName."_end_next_flag".$j] == 1){
									$rData[$out]["ovtm_t".$j] .= "翌";
								}
								$rData[$out]["ovtm_t".$j] .= intval(substr($retRec[$i][$fldName."_end_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_end_time".$j], 2, 2);
								//理由
								if(intval($retRec[$i][$fldName."_ovtmrsn_id".$j]) > 0){
									$sql  = "";
									$sql .= "SELECT ";
									$sql .= " reason ";
									$sql .= "FROM ";
									$sql .= " ovtmrsn ";
									$sql .= "WHERE ";
									$sql .= " reason_id = ".$retRec[$i][$fldName."_ovtmrsn_id".$j];
									$retRsn = $db->find($sql);
									$rData[$out]["ovtm_r".$j] = $retRsn[0]["reason"];
									
								}else{
									$rData[$out]["ovtm_r".$j] = "";
								}
								//休憩
								if($retRec[$i][$fldName."_rest_time".$j] != "" && $retRec[$i][$fldName."_rest_time".$j] != "0000"){
									$rData[$out]["ovtm_b".$j] = intval(substr($retRec[$i][$fldName."_rest_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_rest_time".$j], 2, 2);
								}else{
									$rData[$out]["ovtm_b".$j] = "";
								}
							}
						}
					}
					
					//時間外
					$sql = "";
					$sql .= "SELECT ";
					$sql .= " sum(minute) ";
					$sql .= "FROM ";
					$sql .= " kintai_ovtm_sum ";
					$sql .= "WHERE ";
					$sql .= " emp_id = '".$base_info->emp_id."' AND ";
					$sql .= " date = '".$retRec[$i]["date"]."'";
					$retOvtm = $db->one($sql);
						
					if(count($retOvtm) > 0 && $retRec[$i]["apply_stat"] == 1){
						$rData[$out]["ovtm_time"] = DateUtil::convert_time($retOvtm);
						$ovtm_ttl += $retOvtm;
					}else{
						$rData[$out]["ovtm_time"] = "";
					}

				}else{
					//申請状態
					$rData[$out]["apply_stat"] = "";
					//時間外、理由、休憩1〜3
					for($j=1;$j<=3;$j++){
						$rData[$out]["ovtm_t".$j] = "";
						$rData[$out]["ovtm_r".$j] = "";
						$rData[$out]["ovtm_b".$j] = "";
					}
		
				}
				//会議研修
				$rData[$out]["meeting"] = DateUtil::diff_time($retRec[$i]["meeting_start_time"], $retRec[$i]["meeting_end_time"]);
				$ttlMeeting += DateUtil::convert_min($rData[$out]["meeting"]);
				//時間有給
				$rData[$out]["hol_min"] = DateUtil::convert_time($retRec[$i]["calc_minute"]);
				$ttlTimehol += DateUtil::convert_min($rData[$out]["hol_min"]);
			}
		}
	
	}

	$db->endTransaction(); // トランザクション終了
} catch (BusinessException $bex) {
	$message = $bex->getMessage();
	echo("<script type='text/javascript'>alert('".$message."');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} catch (FatalException $fex) { 
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

function getBgColor($date) {
	if(is_cal_holiday($date) == true){
		return "#FADEDE";
	}else{
		return "#FFFFFF";
	}

}
function is_cal_holiday($date) {
	$sql = "select type from calendar where date = :date";
	$value = get_mdb()->one(
		$sql, array("text"), array("date" => $date));

	if (in_array($value, array("6", "7"))) {
		return true;
	}
	return false;
}
function get_mdb() {
	$log = new common_log_class(basename(__FILE__));
	return new MDB($log);
}

// データベースに接続
$con = connect2db($fname);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./kintai/js/tmpl_common.js?<?php echo time(); ?>"></script>
<script type="text/javascript">
<!--
<?php if ($opt_trip_flag) { ?>
j$(document).ready(function() {
	j$("[id^=trip_]").change(function() {
		var session = j$("#session").val();
		var date = j$(this).attr("id").split('_')[1];
		var flag = j$(this).val();
		var emp_id = j$("#emp_id").val();
		update_trip(session, date, emp_id, flag, 5);
	});
});
<? } ?>

//一括修正画面
function openTimecardAll() {
	base_left = 0;
	base = 0;
	wx = window.screen.availWidth;
	wy = window.screen.availHeight;
	url = 'atdbk_timecard_all.php?session=<?=$session?>&yyyymm=<? echo($yyyymm); ?>&view=<? echo($view); ?>&check_flg=<? echo($check_flg); ?>';
	window.open(url, 'TimecardAll', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
}

function print_xls(){

	if(document.frmKintai.emp_id.value == ""){
		alert("職員が選択されていません。");
		return false;
	}

	document.frmKintai.action = "./kintai_ovtmlist_xls.php";
	document.frmKintai.method = "POST";
	document.frmKintai.submit();

}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
td.lbl {padding-right:1em;}
td.txt {padding-right:0.5em;}
td.tm {width:2.8em;text-align:right;}
td.mark {width:2.8em;text-align:center;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="frmKintai" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<?php 
if ($work_admin_auth == "1") { 
?>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?php echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<?php 
} 
?>
				</tr>
			</table>
<?php
	// メニュー表示
	show_atdbk_menuitem($session, $fname, "");
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
<?php
if ($closing == "") {  // 締め日未登録の場合 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。管理者に連絡してください。</font></td>
				</tr>
			</table>
<?php
} else { 
?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                    <?
    // タイムカードタブ直下のメニュー表示
    timecard_menu_view($con, $fname, $session, $yyyymm, "url_ovtmlist");
?>
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td height="22">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="kintai_ovtmlist.php?session=<?php echo($session); ?>&yyyymm=<?php echo($last_yyyymm); ?>&view=<?php echo($view); ?>">&lt;前月</a>&nbsp;<?php echo("{$year}年{$month}月度"); ?>&nbsp;<a href="kintai_ovtmlist.php?session=<?php echo($session); ?>&yyyymm=<?php echo($next_yyyymm); ?>&view=<?php echo($view); ?>">翌月&gt;</a></font>&nbsp;
								</td>
								<td >
									<input type="button" value="印刷" onclick="return print_xls();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td height="22" width="">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									職員ID：<?php echo $base_info->emp_personal_id; ?>&nbsp;&nbsp;
									職員氏名：<?php echo $base_info->emp_lt_nm."&nbsp;".$base_info->emp_ft_nm; ?>&nbsp;&nbsp;
									所属：<?php echo $shozoku; ?>&nbsp;&nbsp;
									（
										<?php echo $job_info->job_nm; ?>&nbsp;
										<?php echo CodeUtil::get_duty_form($cond_info->duty_form); ?>&nbsp;
										<?php echo CodeUtil::get_salary_type($cond_info->wage); ?>
									）
									</font>
								</td>
								<td align="right" width="">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<!--りすと-->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
			<table border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="center" nowrap width="56" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
					<td align="center" nowrap width="18"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜<br>日</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請状態</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議<br>研修</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間<br>有休</font></td>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出張<br>有無</font></td>
<? } ?>
				</tr>
<?php
for($i=0;$i<count($rData);$i++){
?>				
				<tr bgcolor="<?php echo $rData[$i]["bgcolor"]; ?>">
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap height="30">
<? } else { ?>
					<td align="center" nowrap height="24">
<? } ?>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["date"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["wday"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["atdptn_nm"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["reason_nm"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["apply_stat"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["meeting"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["hol_min"]; ?></font></td>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap>
						<select name="trip_<?php echo $rData[$i]["Ymd"]; ?>" id="trip_<?php echo $rData[$i]["Ymd"]; ?>">
							<option value=""></option>
							<?php $selected = $rData[$i]["trip_flag"] === "1" ? "selected":""; ?>
							<option value="1" <?php echo $selected; ?> >有</option>
						</select>
					</td>
<? } ?>
				</tr>
<?php
}
?>				
				<tr>
					<td align="center" nowrap width="56" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
					<td align="center" nowrap width="18"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜<br>日</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請状態</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議<br>研修</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間<br>有休</font></td>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出張<br>有無</font></td>
<? } ?>
				</tr>
				<tr>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap height="3" colspan="8"></td>
<? } else { ?>
					<td align="center" nowrap height="3" colspan="7"></td>
<? } ?>
				</tr>
				<tr>
					<td align="right" nowrap height="24" colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo DateUtil::convert_time($ttlMeeting); ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo DateUtil::convert_time($ttlTimehol); ?></font></td>
<?php if ($opt_trip_flag) { ?>
					<td/>
<? } ?>
				</tr>
			</table>
		</td>
		<td valign="top">
<div style="overflow-x:scroll; overflow-y:hidden; overflow:auto; border:#5279a5 solid 0px; width:520px">
			<table border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="center" nowrap width="90" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外1</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由1</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩1</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外2</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由2</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩2</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外3</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計<br>(日次)</font></td>
				</tr>
<?php
for($i=0;$i<count($rData);$i++){
?>				
				<tr bgcolor="<?php echo $rData[$i]["bgcolor"]; ?>">
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap height="30">
<? } else { ?>
					<td align="center" nowrap height="24">
<? } ?>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_t1"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_r1"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_b1"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_t2"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_r2"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_b2"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_t3"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_r3"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_b3"]; ?></font></td>
					<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_time"]; ?></font></td>
				</tr>
<?php
}
?>				
				<tr>
					<td align="center" nowrap width="90" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外1</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由1</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩1</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外2</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由2</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩2</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外3</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計<br>(日次)</font></td>
				</tr>
				<tr>
					<td align="center" nowrap height="3" colspan="10"></td>
				</tr>
				<tr>
					<td align="right" nowrap height="24" colspan="9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計(月次)</font></td>
					<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo DateUtil::convert_time($ovtm_ttl); ?></font></td>
				</tr>
			</table>
</div>			
		</td>
	</tr>
</table>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績集計</font>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?php
//ふくすうしふと
for($i=0;$i<=max(array_keys($ttlType));$i++){
	$group = "";
	$total = "";
	for($j=0;$j<=max(array_keys($ttlType[$i]));$j++){
		if($ttlType[$i][$j]["cnt"] > 0){
			if(count($ttlType) > 1){
				$group  = $ttlType[$i][$j]["group"]."<br>";
			}
			$total .= $ttlType[$i][$j]["name"]."：".$ttlType[$i][$j]["cnt"]."&nbsp;";
		}
	}
	if($total != ""){
		echo $group.$total."<br>";
	}
}
?>
</font>
<?php
}
?>
<input type="hidden" id="session" name="session" value="<?php echo $session; ?>">
<input type="hidden" id="emp_id" name="emp_id"  value="<?php echo $base_info->emp_id; ?>">
<input type="hidden" name="yyyymm"  value="<?php echo $yyyymm; ?>">

</form>
</body>
</html>
