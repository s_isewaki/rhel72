<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// 表示対象となる管理項目・補助項目の一覧を取得
// 　補助項目
// 　　項目タイプ、管理項目ID、管理項目名、補助項目ID、補助項目名、補助項目属性、計算式
// 　補助項目のない管理項目
// 　　項目タイプ、管理項目ID、管理項目名、それ以降はnull
$sql = "select 'assist' as type, statitem.statitem_id, statitem.name, statassist.statassist_id, statassist.statassist_name, statassist.assist_type, statassist.calc_var1_type, statassist.calc_var1_const_id, statassist.calc_var1_item_id, statassist.calc_var1_assist_id, statassist.calc_operator, statassist.calc_var2_type, statassist.calc_var2_const_id, statassist.calc_var2_item_id, statassist.calc_var2_assist_id, statassist.calc_format from statassist inner join statitem on statassist.statitem_id = statitem.statitem_id where statassist.del_flg = 'f' union select 'item', statitem_id, name, null, null, null, null, null, null, null, null, null, null, null, null, null from statitem where del_flg = 'f' and not exists (select * from statassist where statassist.statitem_id = statitem.statitem_id and statassist.del_flg = 'f')";
$cond = "order by 2, 4";
$sel_item = select_from_table($con, $sql, $cond, $fname);
if ($sel_item == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_count = pg_num_rows($sel_item);

// 施設一覧を取得
$sql = "select char(1) '1' as area_type, statfcl_id, name, area_id, (select order_no from area where area.area_id = statfcl.area_id) as order_no from statfcl where area_id is not null and del_flg = 'f' union select '2', statfcl_id, name, area_id, statfcl_id from statfcl where area_id is null and del_flg = 'f'";
$cond = "order by 1, order_no";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$fcl_count = pg_num_rows($sel_fcl);

// エリア情報を取得
$sql = "select area_id, area_name from area";
$cond = "";
$sel_area = select_from_table($con, $sql, $cond, $fname);
if ($sel_area == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$areas = array();
while ($row = pg_fetch_array($sel_area)) {
	$areas[$row["area_id"]] = $row["area_name"];
}

// イントラメニュー名を取得
$sql = "select menu1 from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu1 = pg_fetch_result($sel, 0, "menu1");
?>
<title>マスターメンテナンス | <? echo($intra_menu1); ?>設定 | プレビュー</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_stat.php?session=<? echo($session); ?>"><b><? echo($intra_menu1); ?></b></a> &gt; <a href="stat_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_stat.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#5279a5"><a href="stat_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>プレビュー</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="stat_master_item_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理項目一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="stat_master_item_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理項目登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="stat_master_assist_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">補助項目登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_fcl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_fcl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_const_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定数一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_const_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定数登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<?
if ($item_count == 0 && $fcl_count == 0) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">管理項目・施設が登録されていません。</font>");
} else if ($item_count == 0) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">管理項目が登録されていません。</font>");
} else if ($fcl_count == 0) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">施設が登録されていません。</font>");
} else {
	show_preview($sel_item, $sel_fcl, $areas);
}
?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_preview($sel_item, $sel_fcl, $areas) {
	define(COLUMNS_PER_ROW, 5);

	$inputable_assists = array();
	$pre_item_id = "";
	$pre_item_name = "";
	$items = array();
	while ($row = pg_fetch_array($sel_item)) {
		$tmp_item_id = $row["statitem_id"];
		$tmp_assist_id = $row["statassist_id"];

		if ($tmp_assist_id == "") {
			$tmp_id = $tmp_item_id;
			$tmp_name = $row["name"];
		} else {
			$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
			$tmp_name = $row["name"] . "<br>&nbsp;&gt; " . $row["statassist_name"];
		}

		if (!$inputable_assists[$tmp_item_id] && $row["assist_type"] == "1") {
			$inputable_assists[$tmp_item_id] = true;
		}

		if ($tmp_item_id != $pre_item_id) {
			if ($inputable_assists[$pre_item_id]) {
				$items[] = array(
					"id" => $pre_item_id,
					"name" => $pre_item_name . "<br>合計",
					"is_sum" => true
				);
			}

			$pre_item_id = $tmp_item_id;
			$pre_item_name = $row["name"];
		}

		$items[] = array(
			"id" => $tmp_id,
			"name" => $tmp_name
		);
	}
	if ($inputable_assists[$pre_item_id]) {
		$items[] = array(
			"id" => $pre_item_id,
			"name" => $pre_item_name . "<br>合計",
			"is_sum" => true
		);
	}
	$rows = array_chunk($items, COLUMNS_PER_ROW, true);

	$fcls = array();
	for ($i = 0, $j = pg_num_rows($sel_fcl); $i < $j; $i++) {
		$tmp_fcl_id = pg_fetch_result($sel_fcl, $i, "statfcl_id");
		$tmp_fcl_name = pg_fetch_result($sel_fcl, $i, "name");
		$tmp_area_id = pg_fetch_result($sel_fcl, $i, "area_id");

		$fcls[] = array(
			"is_area_row" => false,
			"id" => $tmp_fcl_id,
			"name" => $tmp_fcl_name
		);
		if ($tmp_area_id == "") {
			continue;
		}

		if ($i < ($j - 1)) {
			$next_row_area_id = pg_fetch_result($sel_fcl, $i + 1, "area_id");
		} else {
			$next_row_area_id = "";
		}
		if ($next_row_area_id == $tmp_area_id) {
			continue;
		}

		$fcls[] = array(
			"is_area_row" => true,
			"id" => $tmp_area_id,
			"name" => $areas[$tmp_area_id]
		);
	}

	foreach ($rows as $items) {
		$col_count = count($items);
		$table_width = $col_count * 120 + 150;

		echo("<table width=\"$table_width\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");

		echo("<tr height=\"22\" bgcolor=\"#f6f9ff\" valign=\"top\">\n");
		echo("<td width=\"150\"></td>\n");
		foreach ($items as $tmp_item) {
			$item_id = $tmp_item["id"];
			$item_name = $tmp_item["name"];
			echo("<td width=\"120\">\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
			echo("<tr valign=\"top\">\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$item_name</font></td>\n");
			echo("</tr>\n");
			echo("</table>\n");
			echo("</td>\n");
		}
		echo("</tr>\n");

		foreach ($fcls as $tmp_fcl) {
			$tmp_is_area_row = $tmp_fcl["is_area_row"];
			$tmp_id = $tmp_fcl["id"];
			$tmp_name = $tmp_fcl["name"];

			echo("<tr height=\"22\" valign=\"top\">\n");
			echo("<td bgcolor=\"#f6f9ff\">\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
			echo("<tr valign=\"top\">\n");
			if ($tmp_is_area_row) {
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_name}合計</font></td>\n");
			} else {
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_name</font></td>\n");
			}
			echo("<td width=\"13\">");
			echo("</td>\n");
			echo("</tr>\n");
			echo("</table>\n");
			echo("</td>\n");

			$pre_item_id = "";
			$item_sum = null;
			foreach ($items as $tmp_item) {
				echo("<td align=\"right\"><span style=\"font-size:1.6em;font-family:'Times New Roman';\">");
				echo("</span></td>\n");
			}
			echo("</tr>\n");
		}

		echo("</table>\n");
		echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"><br>\n");
	}
}
?>
