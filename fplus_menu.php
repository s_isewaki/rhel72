<?
require_once("about_comedix.php");
require_once("show_fplus_category_list.ini");
require_once("fplus_application_template.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("fplus_draft_template.ini");
require_once("fplus_common_class.php");
require_once("fplus_common.ini");
require_once("get_values_for_template.ini");
require_once("library_common.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$checkauth = check_authority($session, 78, $fname);
if($checkauth=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$fplusadm_auth = check_authority($session, 79, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new fplus_common_class($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 報告</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<?
if($wkfw_id != "")
{

	if($apply_id == "")
	{
		// ワークフロー情報取得
		$arr_wkfwmst	   = $obj->get_wkfwmst($wkfw_id);
		$wkfw_content	  = $arr_wkfwmst[0]["wkfw_content"];
		$wkfw_content_type = $arr_wkfwmst[0]["wkfw_content_type"];
		$approve_label	 = $arr_wkfwmst[0]["approve_label"];

		// 本文形式タイプのデフォルトを「テキスト」とする
		if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

		// 本文形式タイプのデフォルトを「テンプレート」の場合
		if($wkfw_content_type == "2")
		{
			$wkfw_history_no = $obj->get_max_wkfw_history_no($wkfw_id);
		}

		$num = 0;
		$pos = 0;
		while (1) {
			$pos = strpos($wkfw_content, 'show_cal', $pos);
			if ($pos === false) {
				break;
			} else {
				$num++;
			}
			$pos++;
		}

		if ($num > 0) {
			// 外部ファイルを読み込む
			write_yui_calendar_use_file_read_0_12_2();
		}
		// カレンダー作成、関数出力
		write_yui_calendar_script2($num);

		if ($mode == "apply_printwin") {

		//---------テンプレートの場合XML形式のテキスト$contentを作成---------

			if ($wkfw_content_type == "2") {
				// XML変換用コードの保存
				$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
				$savexmlfilename = "fplus/workflow/tmp/{$session}_x.php";
				$fp = fopen($savexmlfilename, "w");

				if (!$fp) {
					echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//					echo("<script language='javascript'>history.back();</script>");
					$print_failed = true;
				}
				if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
					fclose($fp);
					echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//					echo("<script language='javascript'>history.back();</script>");
					$print_failed = true;
				}
				fclose($fp);

				include( $savexmlfilename );

			}
			$savexmlfilename = "fplus/workflow/tmp/{$session}_d.php";
			$fp = fopen($savexmlfilename, "w");

			if (!$fp) {
				echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//				echo("<script language='javascript'>history.back();</script>");
				$print_failed = true;
			}

			if ($content == "") {
				$content = " ";
			}
			if(!fwrite($fp, $content, 2000000)) {
				fclose($fp);
				echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//				echo("<script language='javascript'>history.back();</script>");
				$print_failed = true;
			}

			fclose($fp);
		}
	}
	else
	{
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$wkfw_content	  = $arr_apply_wkfwmst[0]["wkfw_content"];
		$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
		$apply_content	 = $arr_apply_wkfwmst[0]["apply_content"];
		$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
		$approve_label	 = $arr_apply_wkfwmst[0]["approve_label"];

		// ワークフロー情報取得
		/*
		$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
		$wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
		$wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
		$apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");
		*/
		// 本文形式タイプのデフォルトを「テキスト」とする
		if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

		// 形式をテキストからテンプレートに変更した場合の古いデータ対応
		// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
		if ($wkfw_content_type == "2") {
			if (strpos($apply_content, "<?xml") === false) {
				$wkfw_content_type = "1";
			}
		}

		if($wkfw_history_no != "")
		{
			$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
			$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
		}

		$num = 0;
		if ($wkfw_content_type == "2") {
			$pos = 0;
			while (1) {
				$pos = strpos($wkfw_content, 'show_cal', $pos);
				if ($pos === false) {
					break;
				} else {
					$num++;
				}
				$pos++;
			}
		}

		if ($num > 0) {
			// 外部ファイルを読み込む
			write_yui_calendar_use_file_read_0_12_2();
		}
		// カレンダー作成、関数出力
		write_yui_calendar_script2($num);

		if ($mode == "apply_printwin") {

		//---------テンプレートの場合XML形式のテキスト$contentを作成---------

			if ($wkfw_content_type == "2") {
				// XML変換用コードの保存
				$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
				$savexmlfilename = "fplus/workflow/tmp/{$session}_x.php";
				$fp = fopen($savexmlfilename, "w");

				if (!$fp) {
					echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//					echo("<script language='javascript'>history.back();</script>");
					$print_failed = true;
				}
				if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
					fclose($fp);
					echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//					echo("<script language='javascript'>history.back();</script>");
					$print_failed = true;
				}
				fclose($fp);

				include( $savexmlfilename );

			}
			$savexmlfilename = "fplus/workflow/tmp/{$session}_d.php";
			$fp = fopen($savexmlfilename, "w");

			if (!$fp) {
				echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//				echo("<script language='javascript'>history.back();</script>");
				$print_failed = true;
			}

			if ($content == "") {
				$content = " ";
			}
			if(!fwrite($fp, $content, 2000000)) {
				fclose($fp);
				echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//				echo("<script language='javascript'>history.back();</script>");
				$print_failed = true;
			}
			fclose($fp);

		}
	}
}

$approve_label = ($approve_label != "2") ? "承認" : "確認";
?>
<script type="text/javascript">



function init() {


	if('<?=$back?>' == 't') {
<?

		$approve_num = $_POST[approve_num];
		for($i=1; $i<=$approve_num;$i++) {

			$regist_emp_id = "regist_emp_id$i";
			$radio_emp_id = "radio_emp_id$i";
			$approve_name = "approve_name$i";

?>
			var radio_emp_id = 'radio_emp_id' + <?=$i?>;
			if(document.getElementById('<?=$radio_emp_id?>')) {
				for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {
					if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
						document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
					}
				}
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
			if(document.getElementById('<?=$approve_name?>')) {
				document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>';
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
<?
		}
?>
	}


<?
	if($apply_id == "")
	{
		//新規の場合は初期処理
?>

		//エピネット用初期処理ここから
		var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

		if(epi_element != undefined)
		{
			if(epi_element.value == "Episys107A")
			{
				//confirm(epi_element.value);
				fc_init_107A();
			}
			else if(epi_element.value == "Episys107B")
			{
				//confirm(epi_element.value);
				fc_init_107B();
			}else if(epi_element.value == "Episys107AO"){
                                fc_init_107AO();
                        }else if(epi_element.value == "Episys107BO"){
                                fc_init_107BO();
                        }
		}
		//エピネット用初期処理ここまで

		// k-fujii 医療事故報告用初期処理
		// 緊急報告受付
		var epi_element = document.getElementById("IMMEDIATE_REPORT");
		if(epi_element != undefined)
		{
			fc_init_immediate_repo();
		}
		// 第一報
		var epi_element = document.getElementById("FIRST_REPORT");
		if(epi_element != undefined)
		{
			fc_init_first_repo();
		}
		// 第二報
		var epi_element = document.getElementById("SECOND_REPORT");
		if(epi_element != undefined)
		{
			fc_init_second_repo();
		}
		// コンサルテーション依頼書
		var epi_element = document.getElementById("CONSULTATION_REQUEST");
		if(epi_element != undefined)
		{
			fc_init_cons_req();
		}
		// コンサルテーション報告書
		var epi_element = document.getElementById("CONSULTATION_REPORT");
		if(epi_element != undefined)
		{
			fc_init_cons_repo();
		}
		// 医療機器不具合報告
		var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
		if(epi_element != undefined)
		{
			fc_init_apparatus_fault_repo();
		}
		// ヒヤリハット件数表
		var epi_element = document.getElementById("NUMBER_OF_OCCURRENCES");
		if(epi_element != undefined)
		{
			fc_init_number_of_occurrences();
		}

		//MRSA系初期処理ここから
		var epi_element = document.getElementById("MRSA_REPORT");
		if(epi_element != undefined)
		{
			fc_init();
		}
		//MRSA系初期処理ここまで
	
<?
	}
	else
	{
		//下書き保存の場合は更新初期処理
?>
		//エピネット用初期処理ここから
		var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

		if(epi_element != undefined)
		{
		
			if(epi_element.value == "Episys107A")
			{
				//confirm(epi_element.value);
				fc_upd_init_107A();
			}
			if(epi_element.value == "Episys107B")
			{
				//confirm(epi_element.value);
				fc_upd_init_107B();
                        }
			if(epi_element.value == "Episys107AO"){
                                fc_upd_init_107AO();
                        }
                        if(epi_element.value == "Episys107BO"){
                                fc_upd_init_107BO();
                                
                        }
		}
		//エピネット用初期処理ここまで

		// k-fujii 医療事故報告用初期処理ここから
		// 緊急報告受付
		var epi_element = document.getElementById("IMMEDIATE_REPORT");
		if(epi_element != undefined)
		{
			fc_upd_init_immediate_repo();
		}
		// 第一報
		var epi_element = document.getElementById("FIRST_REPORT");
		if(epi_element != undefined)
		{
			fc_upd_init_first_repo();
		}
		// 第二報
		var epi_element = document.getElementById("SECOND_REPORT");
		if(epi_element != undefined)
		{
			fc_upd_init_second_repo();
		}
		// コンサルテーション依頼書
		var epi_element = document.getElementById("CONSULTATION_REQUEST");
		if(epi_element != undefined)
		{
			fc_upd_init_cons_req();
		}
		// コンサルテーション報告書
		var epi_element = document.getElementById("CONSULTATION_REPORT");
		if(epi_element != undefined)
		{
			fc_upd_init_cons_repo();
		}
		// 医療機器不具合報告
		var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
		if(epi_element != undefined)
		{
			fc_upd_init_apparatus_fault_repo();
		}
		// ヒヤリハット件数表
		var epi_element = document.getElementById("NUMBER_OF_OCCURRENCES");
		if(epi_element != undefined)
		{
			fc_upd_init_number_of_occurrences();
		}
		// 医療事故報告用初期処理ここまで

		//MRSA系初期処理ここから
		var epi_element = document.getElementById("MRSA_REPORT");
		if(epi_element != undefined)
		{
			fc_init();
		
		}
		//MRSA系初期処理ここまで


<?
	
	}
?>
	
	
}

function attachFile() {
	window.open('fplus_apply_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

// 報告処理
function apply_regist(apv_num, precond_num) {


	//エピネット用入力チェック処理ここから
	var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

	if(epi_element != undefined)
	{
		if(epi_element.value == "Episys107A")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107A();
			if(ret == false)
			{
				return false;
			}
		}
		else if(epi_element.value == "Episys107B")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107B();
			if(ret == false)
			{
				return false;
			}
		}
                else if(epi_element.value == "Episys107AO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107AO();
			if(ret == false)
			{
				return false;
			}
		}
                else if(epi_element.value == "Episys107BO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107BO();
			if(ret == false)
			{
				return false;
			}
		}
	}
	//エピネット用入力チェック処理ここまで

	// 医療事故報告用入力チェック処理ここから
	// 緊急報告
	var epi_element = document.getElementById("IMMEDIATE_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_immediate_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第一報
	var epi_element = document.getElementById("FIRST_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_first_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第二報
	var epi_element = document.getElementById("SECOND_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_second_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション依頼書
	var epi_element = document.getElementById("CONSULTATION_REQUEST");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_req();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション報告書
	var epi_element = document.getElementById("CONSULTATION_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療機器不具合報告
	var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_apparatus_fault_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// ヒヤリハット件数表
	var epi_element = document.getElementById("NUMBER_OF_OCCURRENCES");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_number_of_occurrences();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療事故報告用入力チェック処理ここまで

	//MRSA報告書入力チェック処理ここから
	var epi_element = document.getElementById("MRSA_REPORT");
	if(epi_element != undefined)
	{

		//感染症報告
		//confirm(epi_element.value);
		ret = fc_regist_check();
		if(ret == false)
		{
			return false;
		}
	}
	//MRSA報告書入力チェック処理ここまで



	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('受付確定の報告書を設定してくだい。');
			return;
		}
	}

	// 承認者が全く存在しない場合
	var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;

		if(emp_id != "") {
			regist_flg = true;
			break;
		}
	}

	if(!regist_flg)
	{
		alert('受付者が存在しないため、報告できません。');
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;

		if(emp_id_src == "")
		{
			continue;
		}
		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('受付者が重複していますが、報告しますか？'))
		{
				return;
		}
	}
	
	document.apply.action="fplus_application_submit.php";
	document.apply.submit();

}

// 下書き保存
function draft_regist()
{
	document.apply.action="fplus_application_submit.php?draft=on";
	document.apply.submit();
}

function apply_printwin(id) {
	document.apply.mode.value = "apply_printwin";
	if(id == "")
	{
		document.apply.action="fplus_menu.php";
	}
	else
	{
		document.apply.action="fplus_menu.php?apply_id=" + id;
	}
	var default_target = document.apply.target;
	document.apply.target = 'printframe';
	document.apply.submit();
	document.apply.target = default_target;
}


function apply_draft(id, apv_num, precond_num)
{


	//エピネット用入力チェック処理ここから
	var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

	if(epi_element != undefined)
	{
		if(epi_element.value == "Episys107A")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107A();
			if(ret == false)
			{
				return false;
			}
		}
		else if(epi_element.value == "Episys107B")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107B();
			if(ret == false)
			{
				return false;
			}
		}
                else if(epi_element.value == "Episys107AO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107AO();
			if(ret == false)
			{
				return false;
			}
		}else if(epi_element.value == "Episys107BO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107BO();
			if(ret == false)
			{
				return false;
			}
		}
	}
	//エピネット用入力チェック処理ここまで

	// 医療事故報告用ここから
	// 緊急報告
	var epi_element = document.getElementById("IMMEDIATE_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_immediate_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第一報
	var epi_element = document.getElementById("FIRST_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_first_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第二報
	var epi_element = document.getElementById("SECOND_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_second_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション依頼書
	var epi_element = document.getElementById("CONSULTATION_REQUEST");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_req();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション報告書
	var epi_element = document.getElementById("CONSULTATION_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療機器不具合報告
	var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_apparatus_fault_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// ヒヤリハット件数表
	var epi_element = document.getElementById("NUMBER_OF_OCCURRENCES");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_number_of_occurrences();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療事故報告用ここまで


	//MRSA報告書入力チェック処理ここから
	var epi_element = document.getElementById("MRSA_REPORT");
	if(epi_element != undefined)
	{
		//感染症報告（MRSA）
		//confirm(epi_element.value);
		ret = fc_regist_check();
		if(ret == false)
		{
			return false;
		}

	}
	//MRSA報告書入力チェック処理ここまで



	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('受付確定の報告書を設定してくだい。');
			return;
		}
	}

	// 承認者が全く存在しない場合
	var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;

		if(emp_id != "") {
			regist_flg = true;
			break;
		}
	}

	if(!regist_flg)
	{
		alert('受付者が存在しないため、報告できません。');
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;

		if(emp_id_src == "")
		{
			continue;
		}

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;

			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('受付者が重複していますが、報告しますか？'))
		{
				return;
		}
	}

	document.apply.action="fplus_draft_submit.php?apply_id=" + id;
	document.apply.submit();

}

function draft_update(id)
{
	document.apply.action="fplus_draft_submit.php?apply_id=" + id + "&draft=on";
	document.apply.submit();
}

function draft_delete(id)
{
	document.apply.action="fplus_draft_delete.php?apply_id=" + id;
	document.apply.submit();
}


// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
	defaultRows: 1,

	initialize: function(field)
	{
		this.defaultRows = Math.max(field.rows, 1);
		this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
		Event.observe(field, "click", this.resizeNeeded);
		Event.observe(field, "keyup", this.resizeNeeded);
	},

	resizeNeeded: function(event)
	{
		var t = Event.element(event);
		var lines = t.value.split('\n');
		var newRows = lines.length + 1;
		var oldRows = t.rows;
		for (var i = 0; i < lines.length; i++)
		{
			var line = lines[i];
			if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
		}
		if (newRows > t.rows) t.rows = newRows;
		if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
	}
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
		var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
		var lines = t.value.split('\n');
		var newRows = lines.length + 1;
		var oldRows = t.rows;
		for (var k = 0; k < lines.length; k++)
		{
			var line = lines[k];
			if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
		}
		if (newRows > t.rows) t.rows = newRows;
		if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init();
<? if ($wkfw_id != "") { ?>
initcal();
<? } ?>
<?
if ($mode == "apply_printwin" && !$print_failed)
{

if($wkfw_id != "" && $apply_id == "")
{
?>
window.open('fplus_application_template_print.php?session=<?=$session?>&fname=<?=$fname?>&wkfw_id=<?=$wkfw_id?>&back=f&mode=apply_print', 'apply_print', 'width=670,height=700,scrollbars=yes');
<?
}
else if($wkfw_id != "" && $apply_id != "")
{
?>
window.open('fplus_draft_template_print.php?session=<?=$session?>&fname=<?=$fname?>&target_apply_id=<?=$apply_id?>&back=f&mode=apply_print', 'apply_print', 'width=670,height=700,scrollbars=yes');
<?
}
?>

<?
}
?>

if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();if (window.refreshApproveOrders) {refreshApproveOrders();}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplus_menu.php?session=<? echo($session); ?>"><b>報告</b></a></font></td>
<? if ($fplusadm_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplusadm_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#FF99CC"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="wkfw_id" value="<?=$wkfw_id?>">
<input type="hidden" name="wkfw_type" value="<?=$wkfw_type?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" width="25%">
<?
show_application_cate_list($con, $session, $fname, $wkfw_type, $wkfw_id, $apply_id);
?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="75%">
<?
if($wkfw_id != "" && $apply_id == "")
{
	show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode);
}
else if($wkfw_id != "" && $apply_id != "")
{
	show_draft_template($con, $session, $fname, $apply_id, $apply_title, $content, $file_id, $filename, $back, $mode);
}
?>
</td>
</tr>
</table>
</form>

</td>
</tr>
</table>
<iframe name="printframe" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
<?

// workflow/tmpディレクトリ作成
$obj->create_wkfwtmp_directory();
// apply/tmpディレクトリ作成
$obj->create_applytmp_directory();
pg_close($con);
?>
