<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// イントラメニュー設定権限の取得
$intra_setting_auth = check_authority($session, 51, $fname);

// データベースに接続
$con = connect2db($fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu3 = pg_fetch_result($sel, 0, "menu3");
$menu3_1 = pg_fetch_result($sel, 0, "menu3_1");
$menu3_2 = pg_fetch_result($sel, 0, "menu3_2");
$menu3_3 = pg_fetch_result($sel, 0, "menu3_3");

// 権限情報を取得
$authorities = get_authority($session, $fname);
?>
<title>イントラネット | <? echo($menu3); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_life.php?session=<? echo($session); ?>"><b><? echo($menu3); ?></b></a></font></td>
<? if ($intra_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_master_life.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu3)); ?>" align="center" bgcolor="#5279a5"><a href="intra_life.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu3); ?></b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="intra_life_coupon.php?session=<? echo($session); ?>"><img src="img/icon/intra_life_coupon.gif" alt="<? echo($menu3_1); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu3_1); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="intra_life_welfare.php?session=<? echo($session); ?>"><img src="img/icon/intra_life_welfare.gif" alt="<? echo($menu3_2); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu3_2); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
<? if ($authorities[6] == "t") { ?>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="qa_category_menu.php?session=<? echo($session); ?>&qa=2"><img src="img/icon/intra_life_qa.gif" alt="<? echo($menu3_3); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu3_3); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
