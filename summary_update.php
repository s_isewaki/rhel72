<?
/*

画面パラメータ
	$session
		セッションID
	$date
		必須。
		初期表示する作成日付。
	$summary_problem
		プロブレム初期表示値
	$summary_div
		診療区分の初期表示値
	$summary_div_id
		診療区分の初期表示値
	$priority
		記事優先度初期表示値
	$no_tmpl_window
		"1"以外の場合にテンプレートページを起動時に表示
	$summary_id
		診療記録ID
	$pt_id
		患者ID
	$xml_file
		テンプレートXMLファイル名
		これが指定された場合に限り、内容欄にテンプレートの内容を表示する。
		これが指定されていない場合、$summary_naiyoが使用される。
	$tmpl_id
		テンプレートID
	$problem_id
		プロブレムID
	$summary_naiyo
		$xml_fileが設定されていない場合の内容欄初期表示値
	$sect_id
		診療科ID
	$p_sect_id
		診療科ID(&sectはIEのみURLパラメータとして使用できないためGETアクセスの場合はこちらを使用する)
	$key1
	$key2

当画面への遷移パターン
・内容一覧画面で更新ボタン押下
・保存時チェックで入力エラー時
・テンプレート画面で入力完了時 (TinyMCEとの関係上、この設計を変えることは困難)
(・プロブレムリストからはJavascript呼び出し。)

*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require_once("./show_select_values.ini");
require_once("./get_values.ini");
require_once("./conf/sql.inf");
require_once("label_by_profile_type.ini");
require_once("html_to_text_for_tinymce.php");
require_once("sum_application_workflow_common_class.php");
require_once("sum_application_template.ini");
require_once("sot_util.php");
require_once("sum_exist_workflow_check_exec.ini");
require_once("tmpl_common.ini");

//==================================================
//初期処理
//==================================================

//パラメータ精査
if(@$p_sect_id != "" && @$sect_id == "") {
	$sect_id = $p_sect_id;
}



//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}

//$disabled_kousin_sousin_flg = 1;

//利用者ID取得
$emp_id = get_emp_id($con, $session, $fname);


//日付を年月日に分割する。
$year = substr($date, 0, 4);
$month = substr($date, 4, 2);
$day = substr($date, 6, 2);

// 診療記録区分IDを取得
if (!@$summary_div_id && @$summary_div){
	$sql = "select div_id from divmst where diag_div like '".pg_escape_string(@$summary_div)."'";
	$sel_divmst = select_from_table($con, $sql, "", $fname);
	$summary_div_id = pg_fetch_result($sel_divmst, 0, 0);
}

// 診療科一覧を取得
$sql =
		" select * from (".
		"   select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
		") d";
$sql = "select sect_id, sect_nm from (".$sql.") d";
$cond = "where sect_del_flg = 'f' and (sect_hidden_flg is null or sect_hidden_flg <> 1) order by sort_order, sect_id";
$sel_sect = select_from_table($con, $sql, $cond, $fname);
if ($sel_sect == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$sects = pg_fetch_all($sel_sect);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];


// ワークフローの取得
$sql =
	" select * from sum_apply".
	" where summary_id = ".(int)$summary_id .
	" and ptif_id = '".pg_escape_string($pt_id)."'".
	" and delete_flg = 'f' order by apply_id desc";
$sum_apply = select_from_table($con, $sql, "", $fname);
$apply_id = (int)@pg_fetch_result($sum_apply, 0, "apply_id");
$dairi_emp_id = @pg_fetch_result($sum_apply, 0, "dairi_emp_id");
$dairi_kakunin_ymdhms = @pg_fetch_result($sum_apply, 0, "dairi_kakunin_ymdhms");
$seiki_emp_id = @pg_fetch_result($sum_apply, 0, "emp_id");

if (!$tmpl_id){
	$wkfw_id = @pg_fetch_result($sum_apply, 0, "wkfw_id");
	$wkfwmst = select_from_table($con, "select * from sum_wkfwmst where wkfw_id = ".(int)$wkfw_id, "", $fname);
	$tmpl_id = @pg_fetch_result($wkfwmst, 0, "tmpl_id");
} else {
	$tmplmst = select_from_table($con, "select * from tmplmst where tmpl_id = ".(int)$tmpl_id, "", $fname);
	$wkfwmst = select_from_table($con, "select * from sum_wkfwmst where tmpl_id = ".(int)$tmpl_id, "", $fname);
	$wkfw_id = @pg_fetch_result($wkfwmst, 0, "wkfw_id");
}
$wkfw_enabled = @pg_fetch_result($wkfwmst, 0, "wkfw_enabled");
$is_hide_approve_status = @pg_fetch_result($wkfwmst, 0, "is_hide_approve_status");

$obj = new application_workflow_common_class($con, $fname);


//==============================
//HTML出力
//==============================

?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜更新</title>
<script type="text/javascript">
var m_use_tyny_mce = false;
</script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<?


//テンプレートが未使用orOperaの場合だけTinyMCEを使用
if(@$xml_file == ""){
?>
<script type="text/javascript">
if(!tinyMCE.isOpera)
{
	m_use_tyny_mce = true;
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "preview,table,emotions,fullscreen,layer",
		//language : "ja_euc-jp",
		language : "ja",
		width : "100%",
		height : "400",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		content_css : "tinymce/tinymce_content.css",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid",
		theme_advanced_statusbar_location : "none",
		force_br_newlines : true,
		forced_root_block : '',
		force_p_newlines : false
	});
}
</script>
<?
}

?>
<script type="text/javascript">
	// この子画面の現在の高さを求める
	function getCurrentHeight(){
		if (!window.parent) return 0;
		var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
		h = Math.max(h, document.body.scrollHeight);
		h = Math.max(h, 300);
		return (h*1+50);
	}
	// この子画面の最初の高さを親画面(summary_rireki.php)に通知しておく。onloadで呼ばれる。
	// 一旦サイズを小さくしたのち、正しい値を再通知するようにしないといけない。
	function setDefaultHeight(){
		if (!window.parent) return;
		window.parent.setDefaultHeight("migi", 300);
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.setDefaultHeight("migi",h);
	}
	// この子画面がリサイズされたら、高さを再測定して親画面(summary_rireki.php)に通知する。
	// 「サマリ内容ポップアップ」が開くときも呼ばれる。onloadでも呼ばれる。
	// 親画面はこの子画面のIFRAMEサイズを変更する。
	// 一旦大きくしたIFRAMEのサイズは通常小さくできない。
	// よって、getCurrentHeightでサイズ測定する前に、画面を一旦元に戻さないといけない。
	function resizeIframeHeight(){
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.resizeIframeHeight("migi", h);
	}
	// この子画面の高さを、最初の高さに戻す。「サマリ内容ポップアップ」が閉じるとき呼ばれる。
	// 親画面(summary_rireki.php)は、この子画面IFRAMEの高さを最初の値に戻す。
	function resetIframeHeight(){
		if (!window.parent) return;
		parent.resetIframeHeight("migi");
	}

	//起動時の処理を行います。
	function load_action() {
		set_summary_problem();
		start_auto_session_update();

		if (window.OnloadSub) { OnloadSub(); };
		if (window.refreshApproveOrders) {refreshApproveOrders();}
		setDefaultHeight();
		resizeIframeHeight();
	}

//プロブレムリスト選択画面を表示します。
var problem_list_select_window_open_option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";
function showProblemListSelectWindow() {
	var url = "summary_problem_select_new.php?session=<?=$session?>&pt_id=<?=$pt_id?>";
	window.open(url, 'problem_list_select_window',problem_list_select_window_open_option);
}

//プロブレムリスト選択完了時にコールバックされます。
function problem_list_selected_call_back(problem_id,problem_no,problem) {
	set_postbackdata();
	document.postback.summary_problem.value = "#" + problem_no + " " + problem;
	document.postback.problem_id.value = problem_id;
	postback();
}


function diag_sel(){
	if(document.touroku.xml_file.value != ""){
		for(i=0; i<document.touroku.sel_diag.options.length; i++) {
			if(document.touroku.sel_diag.options[i].text == document.touroku.diag.value) {
				document.touroku.sel_diag.selectedIndex = i;
				break;
			}
		}
		alert('テンプレートを使用しているため<?=$summary_kubun_title?>は変更出来ません');
	}
}
function show_tmpl_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=600,height=700";
	window.open(url, 'tmpl_window',option);
}
function show_copy_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";
	window.open(url, 'tmpl_window',option);
}
//定型文選択画面
function show_fixedform_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=10,top=10,width=600,height=700";
	window.open(url, 'fixedform_window',option);
}
//定型文の設定
function call_back_fixedform_select(caller, result) {
	if(!tinyMCE.isOpera) {
		tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent() + result.sentence);
	} else {
		document.touroku.summary_naiyo.value += result.sentence;
	}
}

<?
require_once("summary_ymd_select_util.ini");
write_common_ymd_ctl_script();
?>
function attachFile() {
	window.open('sum_apply_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=250,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
	document.getElementById('attach').height = 0;
	resizeParentIframeHeight();
}

// 送信処理
function apply_regist(apv_num, precond_num) {
	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.touroku.elements[obj].value;
		if(apply_id == "") {
			alert('受信確定の送信書を設定してくだい。');
			return;
		}
	}

	// 受信者が全く存在しない場合
  var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.touroku.elements[obj].value;

		if(emp_id != "") {
			regist_flg = true;
            break;
		}
	}

    if(!regist_flg) {
//        alert('受信者が存在しないため、送信できません。');
        return;
    }

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.touroku.elements[obj_src].value;
    if(emp_id_src == "") continue;
		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.touroku.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg) {
		if (!confirm('受信者が重複していますが、送信しますか？')) {
			return;
		}
	}
	document.touroku.apply_stat.value = "1";
	document.touroku.is_apply_stat_change_only.value = "";
	tourokuSubmit();

}

function tourokuSubmit(){
	document.touroku.summary_naiyo_escaped_amp.value = document.touroku.summary_naiyo.value.split("&").join("&amp;");
	document.touroku.submit();
}

function apply_regist2(aply_stat){
	document.touroku.is_apply_stat_change_only.value = "";
	if (aply_stat != "") {
		document.touroku.apply_stat.value = aply_stat;
		document.touroku.is_apply_stat_change_only.value = "1";
	}
	tourokuSubmit();
}
</script>
</head>
<body style="background-color:#f4f4f4" onload="load_action()">

<!-- セッションタイムアウト防止 START -->
<script type="text/javascript">
	function start_auto_session_update(){
		setTimeout(auto_sesson_update,60000);	//1分(60*1000ms)後に開始
	}
	function auto_sesson_update(){
		document.session_update_form.submit();	//セッション更新
		setTimeout(auto_sesson_update,60000);	//1分(60*1000ms)後に再呼び出し
	}
</script>
<form name="session_update_form" action="summary_session_update.php" method="post" target="session_update_frame">
	<input type="hidden" name="session" value="<?=$session?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->



<form name="postback" action="summary_update.php" method="post">
	<input type="hidden" name="summary_id" value="">
	<input type="hidden" name="session" value="">
	<input type="hidden" name="date" value="">
	<input type="hidden" name="summary_problem" value="">
	<input type="hidden" name="summary_div" value="">
	<input type="hidden" name="summary_div_id" value="">
	<input type="hidden" name="priority" value="">
	<input type="hidden" name="pt_id" value="">
	<input type="hidden" name="xml_file" value="">
	<input type="hidden" name="tmpl_id" value="">
	<input type="hidden" name="problem_id" value="">
	<input type="hidden" name="summary_naiyo" value="">
	<input type="hidden" name="sect_id" value="">
	<input type="hidden" name="no_tmpl_window" value="1">
	<input type="hidden" name="dairi_emp_id" value="">
	<input type="hidden" name="seiki_emp_id" value="">
</form>

<script type="text/javascript">
function set_postbackdata() {
	document.postback.summary_id.value = "<?=$summary_id?>";
	document.postback.session.value = "<?=$session?>";
	document.postback.date.value =
		  document.touroku.sel_yr.options[document.touroku.sel_yr.selectedIndex].value
		+ document.touroku.sel_mon.options[document.touroku.sel_mon.selectedIndex].value
		+ document.touroku.sel_day.options[document.touroku.sel_day.selectedIndex].value;
	document.postback.summary_problem.value = document.touroku.summary_problem.value;
	document.postback.summary_div.value =
		document.touroku.sel_diag.options[document.touroku.sel_diag.selectedIndex].text;
	document.postback.summary_div_id.value =
		document.touroku.sel_diag.options[document.touroku.sel_diag.selectedIndex].value;
	document.postback.priority.value =
		document.touroku.priority.options[document.touroku.priority.selectedIndex].text;
	document.postback.pt_id.value = "<?=$pt_id?>";
	document.postback.xml_file.value = document.touroku.xml_file.value;
	document.postback.tmpl_id.value = document.touroku.tmpl_id.value;
	document.postback.problem_id.value = document.touroku.problem_id.value;
	document.postback.sect_id.value = document.touroku.sect_id.value;
	if(m_use_tyny_mce) {
		document.postback.summary_naiyo.value = tinyMCE.activeEditor.getContent();
	} else {
		document.postback.summary_naiyo.value = document.touroku.summary_naiyo.value;
	}
	document.postback.no_tmpl_window.value = "1";
	document.postback.dairi_emp_id.value = document.getElementById("dairi_emp_id").value;
	document.postback.seiki_emp_id.value = document.getElementById("seiki_emp_id").value;

}
function postback() {
	document.postback.submit();
}
</script>



<form name="touroku" action="summary_update_data.php" method="post">
<?
//==================================================
//プロブレム等の表のHTML出力
//==================================================
?>
<table width="100%" class="list_table" cellspacing="1">
	<tr>
		<th width="20%"><a href="summary_rireki_table.php?session=<?=$session?>&pt_id=<?=$pt_id?>&key1=<?=@$key1?>&key2=<?=@$ekey2?>&mode=section" target="hidari"><?=$_label_by_profile["SECTION"][$profile_type] ?></a></th>
		<td colspan="3">
			<div style="float:left">
				<select name="sect_id">
					<option value=""></option>
					<? foreach ($sects as $tmp_sect) { ?>
					<option value="<?=$tmp_sect["sect_id"]?>"<?=($tmp_sect["sect_id"] == $sect_id) ? " selected" : ""?>><?=$tmp_sect["sect_nm"]?></option>
					<? } ?>
				</select>
				<input type="hidden" name="outreg" value="">
			</div>
			<? // 患者検索ダイアログを開く。現在の患者の病棟をダイアログに渡す。 ?>
			<? $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, ($year*10000)+($month*100)+$day); ?>
			<div style="float:right">
				<? if ($is_workflow_exist){ ?>
				<input type="button" onclick="parent.location.href='sot_worksheet_ondoban.php?session=<?=$session?>&sel_ptif_id=<?=$pt_id?>';" value="温度板"/>
				<? } ?>
				<input type="button" onclick="popupKanjaSelect();" value="入院一覧"/>
				<script type="text/javascript">
					function popupKanjaSelect(){
						var sect = document.touroku.sect_id.value;
						window.open(
							"sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=<?=@$nyuin_info["bldg_cd"]?>&sel_ward_cd=<?=@$nyuin_info["ward_cd"]?>&sel_sort_by_byoto=1&sel_ptif_id=<?=$pt_id?>&sel_sect_key=1_"+sect,<?//診療科(sel_sect_id)の第一パラメータ(enti_id)はとりあえず1固定にしておく?>
							"",
							"directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,left=0,top=0,width=630,height=480"
						);
					}
					function callbackKanjaSelect(ptif_id){
						window.parent.location.href='summary_rireki.php?session=<?=$session?>&pt_id='+ptif_id;
					}
				</script>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
	<tr>
		<th>
			<a href="summary_rireki_table.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$ekey2); ?>&mode=problem" target="hidari">プロブレム</a>
		</th>
		<td colspan='3'>
			<div style="float:left">
				<input type="hidden" id="summary_problem" name="summary_problem" value="<?=@$problem?>">
				<script type="text/javascript">
					function set_summary_problem() {
						if(document.touroku.problem_id.options.length > 0) {
							var pl_index = document.touroku.problem_id.selectedIndex;
							var pl_value = document.touroku.problem_id.options[pl_index].text;
							document.touroku.summary_problem.value = pl_value;
						}
					}
				</script>
				<select id='problem_id' name='problem_id' onChange="set_summary_problem();">
				<option value="0" <?if(@$problem_id == 0){?>selected<?}?> >#0 未選択</option>
				<?
				//==============================
				//プロブレムリスト取得
				//==============================
				$sql = "select summary_problem_list.* from summary_problem_list ";
				$cond = "where summary_problem_list.ptif_id = '$pt_id'";
				$cond .= " order by summary_problem_list.problem_list_no";
				$sel_plist = select_from_table($con, $sql, $cond, $fname);
				if ($sel_plist == 0) {
					pg_close($con);
					require_once("summary_common.ini");
					summary_common_show_error_page_and_die();
				}
				$num_plist = pg_numrows($sel_plist);
				for($i=0;$i<$num_plist;$i++)
				{
					$pl_problem_id = pg_result($sel_plist,$i,"problem_id");
					$pl_problem_list_no = pg_result($sel_plist,$i,"problem_list_no");
					$pl_problem = pg_result($sel_plist,$i,"problem");
					$pl_problem_value = "#$pl_problem_list_no $pl_problem";
					$pl_selected = "";
					if(@$problem_id == @$pl_problem_id)
					{
						$pl_selected = "selected";
					}
					?>
					<option value="<?=$pl_problem_id?>" <?=$pl_selected?>><?=$pl_problem_value?></option>
					<?
				}
				?>
				</select>
			</div>
			<div style="float:right">
				<input type="button" onclick="showProblemListSelectWindow();" value="NEW"/>
			</div>
			<div style="clear:both"></div>
			</td>
	</tr>
	<tr>
		<th><nobr>
			<a href="summary_rireki_table.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$key2); ?>" target="hidari"><?=$_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type]// 診療記録区分 ?></a>
		</nobr></th>
		<?
			$summary_naiyo = str_replace("<BR>", "\n", @$summary_naiyo); //旧データ対応
			$summary_naiyo = str_replace("<br/>", "\n", @$summary_naiyo); //新データ対応
			// opera対応
			if (strpos($_SERVER["HTTP_USER_AGENT"], "Opera") !== FALSE) {
				$summary_naiyo = html_to_text_for_tinymce($summary_naiyo);
			}
		?>

		<? $div_info = summary_common_get_sinryo_kubun_info($con, $fname, $session, $emp_id, $summary_div_id, "", "yes"); // 編集権限つきリスト ?>
		<td>
			<select id="sel_diag" name="sel_diag" DataMember="診療区分" onChange="diag_sel();">
				<?=$div_info["dropdown_options"]?>
			</select>
			<script type="text/javascript">
				var enc_div_array = {
					<? foreach($div_info["list"] as $didx => $drow){ ?>
					"<?=$drow["div_id"]?>":"<?=$drow["enc_diag"]?>",
					<? } ?>
					"":""
				};
			</script>
		</td>

		<th><nobr><img src="img/repo-jyuuyou.gif" alt="記事重要度">&nbsp;記事重要度</nobr></th>
		<td width='30'>
			<select id='priority' name='priority'>
			<option value="1" <? if($priority == "1"){echo("selected");} ?>>1</option>
			<option value="2" <? if($priority == "2"){echo("selected");} ?>>2</option>
			<option value="3" <? if($priority == "3"){echo("selected");} ?>>3</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>
			<a href="summary_rireki_list.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>" target="hidari">作成年月日</a>
		</th>
		<td colspan='3'>
			<select id='sel_yr' name='sel_yr'><? show_select_years_span(date("Y") - 14, date("Y") + 1, $year, false); ?></select>年
			<select id='sel_mon' name='sel_mon'><? show_select_months($month); ?></select>月
			<select id='sel_day' name='sel_day'><? show_select_days($day); ?></select>日
			<input type="button" onclick="set_ymd_select_add_day(document.touroku.sel_yr,document.touroku.sel_mon,document.touroku.sel_day,-1);" value="1日前"/>
			<input type="button" onclick="set_ymd_select_add_day(document.touroku.sel_yr,document.touroku.sel_mon,document.touroku.sel_day,1);" value="1日後"/>
		</td>
	</tr>
	<tr>
		<th>記載者</th>
		<td colspan="4">
			<script type="text/javascript">
				function cancelDairi(){
					if (!confirm("代行者の編集を行えないようにしますか？")) return false;
//					document.getElementById("sousinsya").innerHTML = document.getElementById("dairi_sousinsya").innerHTML;
//					document.getElementById("seiki_emp_id").value = document.getElementById("dairi_emp_id").value;
					document.getElementById("dairi_sousinsya_title").style.display = "none";
					document.getElementById("dairi_sousinsya").innerHTML = "";
//					document.getElementById("dairi_emp_id").value = "";
					document.getElementById("dairi_finish").value = "1";
					alert("設定しました。テンプレート登録後に、代理送信者は無効になります。");
				}
			</script>
			<?
				if (!@$seiki_emp_id) $seiki_emp_id = $emp_id;
				$sql = "select emp_job, emp_lt_nm, emp_ft_nm from empmst where emp_id = '".pg_escape_string($seiki_emp_id)."'";
				$sel2 = select_from_table($con, $sql, "", $fname);
				$emp_info = pg_fetch_array($sel2);
				$sel2 = select_from_table($con, "select job_nm from jobmst where job_id = ".(int)$emp_info["emp_job"], "", $fname);
				$job_info = pg_fetch_array($sel2);
				$job_name = @$job_info["job_nm"];
				if ($job_name) $job_name = "（".$job_name."）";

				$daikou_caption = "代行者";
				if ($seiki_emp_id == $emp_id){
					$daikou_caption = '<a href="#" onclick="cancelDairi();" class="always_blue">代行者</a>';
				}

				$sql = "select emp_job, emp_lt_nm, emp_ft_nm from empmst where emp_id = '".pg_escape_string($dairi_emp_id)."'";
				$sel2 = select_from_table($con, $sql, "", $fname);
				$emp_info_d = pg_fetch_array($sel2);
				$sel2 = select_from_table($con, "select job_nm from jobmst where job_id = ".(int)$emp_info_d["emp_job"], "", $fname);
				$job_info_d = @pg_fetch_array($sel2);
				$job_name_d = @$job_info_d["job_nm"];
				if ($job_name_d) $job_name_d = "（".$job_name_d."）";

			?>
			<span id="sousinsya" style="padding-right:10px"><?=$emp_info["emp_lt_nm"]." ".$emp_info["emp_ft_nm"]?><?=$job_name?></span>
			<? if (!$dairi_kakunin_ymdhms){ ?>
			<span id="dairi_sousinsya_title" style="padding:2px; background-color:#eff9fd; border:1px solid #9bc8ec;<?=(!$dairi_emp_id)?" display:none;":""?>"><?=$daikou_caption?></span>
			<span id="dairi_sousinsya" style="padding-left:4px">
				<?=@$emp_info_d["emp_lt_nm"]." ".@$emp_info_d["emp_ft_nm"]?><?=$job_name_d?></span>
			<? } ?>
			<input type="hidden" id="dairi_emp_id" name="dairi_emp_id" value="<?=$dairi_emp_id?>"/>
			<input type="hidden" id="seiki_emp_id" name="seiki_emp_id" value="<?=$seiki_emp_id?>"/>
			<input type="hidden" id="dairi_finish" name="dairi_finish" value=""/>
		</td>
	</tr>
</table>


<div style="padding-top:4px; margin-top:4px; padding-bottom:4px; border-bottom:1px solid #fdfdfd;">
	<table style="width:100%"><tr>




<? //-------------------------------------------------------------------------?>
<? // 送信済・受付済・実施済状態取得                                          ?>
<? //-------------------------------------------------------------------------?>
<? if($wkfw_enabled && $apply_id){
	$border_color = array("border:1px solid #d22846;", "border:1px solid #bdc122;");
	$back_color = array("background-color:#e98b9b;", "background-color:#e7e98b;");
	$accepted_count = 0;
	$operated_count = 0;

	// 否認差戻しテーブル（誰かがこのオーダに、否認または差し戻ししたか）
	$sql = "select * from sum_apply_hinin_sasimodosi where apply_id = ". $apply_id;
	$rs_hinin_sasimodosi = select_from_table($con, $sql, "", $fname);
	$rows_hinin_sasimodosi = pg_fetch_all($rs_hinin_sasimodosi);

	$sql = "select apply_stat from sum_apply where apply_id = ".$apply_id;
	$sel = select_from_table($con, $sql, "", $fname);
	$apply_stat = (int)pg_fetch_result($sel, 0, "apply_stat");
	$is_sending = ($apply_stat == 1 ? 1 : 0);

	$sql = "select count(*) from sum_applyapv where apply_id = ". $apply_id;
	$sel = select_from_table($con, $sql, "", $fname);
	$acceptor_count = (int)pg_fetch_result($sel, 0, 0);

	$sql = "select count(*) from sum_applyapv where apply_id = ". $apply_id . " and next_notice_recv_div = 2";
	$sel = select_from_table($con, $sql, "", $fname);
	$operator_count = (int)pg_fetch_result($sel, 0, 0);

	if ($apply_stat > 0){
		$sql =
			"select count(*) from sum_applyapv".
			" where apply_id = ". $apply_id. " and (apv_stat_accepted = '1' or apv_stat_accepted_by_other = '1')";
		$sel = select_from_table($con, $sql, "", $fname);
		$accepted_count = (int)pg_fetch_result($sel, 0, 0);

		$sql =
			"select count(*) from sum_applyapv".
			" where apply_id = ". $apply_id. " and (apv_stat_operated = '1' or apv_stat_operated_by_other = '1') and next_notice_recv_div = 2";
		$sel = select_from_table($con, $sql, "", $fname);
		$operated_count = (int)pg_fetch_result($sel, 0, 0);
	}
	$is_accepted = ($accepted_count == $acceptor_count);
	$is_operated = ($operated_count == $operator_count);
	if (!$operator_count && $is_operated) $is_operated = 0;
	if (!$acceptor_count && $is_accepted) $is_accepted = 0;

	$exist_accepted_sasimodosi = 0;
	$exist_accepted_hinin = 0;
	$exist_operated_sasimodosi = 0;
	$exist_operated_hinin = 0;
	if (is_array($rows_hinin_sasimodosi)){
		foreach ($rows_hinin_sasimodosi as $tkey => $trow){
			if ($trow["update_ymdhm"]) continue;
			if ($trow["hinin_or_sasimodosi"]=="sasimodosi") {
				if ($trow["from_apply_stat_accepted"]) $exist_accepted_sasimodosi = 1;
				if ($trow["from_apply_stat_operated"]) $exist_operated_sasimodosi = 1;
			}
			if ($trow["hinin_or_sasimodosi"]=="hinin") {
				if ($trow["from_apply_stat_accepted"]) $exist_accepted_hinin = 1;
				if ($trow["from_apply_stat_operated"]) $exist_operated_hinin = 1;
			}
		}
	}

	$is_sending_caption_list = array("未送信", "送信済", "送信取消");
	$is_sending_caption =  $is_sending_caption_list[$apply_stat];
	$is_accepted_caption = ($is_accepted ? "受付済" : "未受付");
	if ($accepted_count > 0 && $acceptor_count > $accepted_count) $is_accepted_caption = "一部受付";
	if ($exist_accepted_sasimodosi) $is_accepted_caption = "差戻し";
	if ($exist_accepted_hinin) $is_accepted_caption = "否認";
	$is_operated_caption = ($is_operated ? "実施済" : "未実施");
	if ($operated_count > 0 && $operator_count > $operated_count) $is_operated_caption = "一部実施";
	if ($exist_operated_sasimodosi) $is_operated_caption = "差戻し";
	if ($exist_operated_hinin) $is_operated_caption = "否認";
?>
	<td>
	<span style="padding:2px; white-space:nowrap; <?=$back_color[$is_sending]?> <?=$border_color[$is_sending]?>"><?=$is_sending_caption?></span>
	<? if (!$is_hide_approve_status){ ?>
		<? if ($acceptor_count > 0){ ?>
		<span style="padding:2px; white-space:nowrap; <?=$back_color[$is_accepted]?> <?=$border_color[$is_accepted]?>"><?=$is_accepted_caption?></span>
		<? } ?>
		<? if ($operator_count > 0){ ?>
		<span style="padding:2px; white-space:nowrap; <?=$back_color[$is_operated]?> <?=$border_color[$is_operated]?>"><?=$is_operated_caption?></span>
		<? } ?>
	<? } ?>
	</td>
<? } ?>




<?
//==================================================
//内容のタイトルと各種ボタンのHTML出力
//==================================================
$arr_wkfwapv = $obj->get_wkfwapv_info((int)$wkfw_id, $seiki_emp_id, $pt_id);
$approve_num = count($arr_wkfwapv);   // 承認者数


//==================================================
//現在登録中の送信相手をマージ
//==================================================
$sql =
	" select".
	" sum_applyapv.emp_id, coalesce(sum_applyapv.wkfw_apv_order, sum_applyapv.apv_order) as apv_order, sum_applyapv.apv_sub_order".
	",empmst.emp_lt_nm, empmst.emp_ft_nm".
	",stmst.st_nm".
	" from sum_applyapv".
	" left join empmst on (empmst.emp_id = sum_applyapv.emp_id)".
	" left join stmst on (stmst.st_id = empmst.emp_st)".
	" where apply_id = ". $apply_id;
	" and multi_apv_flg = false"; // 単数選択の場合のみ
$sel1 = select_from_table($con, $sql, "", $fname);
while ($rowapv = pg_fetch_array($sel1)){
	if (!$rowapv["emp_id"]) continue; // 現在登録中の職員がいなければ、マージできない
	foreach($arr_wkfwapv as $idx1 => $row_wkfwapv){
		if ($rowapv["apv_order"] != $row_wkfwapv["apv_order"] || $rowapv["apv_sub_order"] != $row_wkfwapv["apv_sub_order"]) continue; // 階層が違っていれば飛ばす

		// infoに職員が含まれていれば、（ラジオボタンなどの場合）それに選択状態マークをつける
		foreach(@$row_wkfwapv["emp_infos"] as $idx2 => $row_info){
			if (@$row_info["emp_id"] == $rowapv["emp_id"]){
				$arr_wkfwapv[$idx1]["info_registing_index"] = $idx2;
			}
		}
		// 職員選択肢がなければ（登録時に指定の場合）、追加する
		if (!@$arr_wkfwapv[$idx1]["emp_infos"][0]["emp_id"]){
			$arr_wkfwapv[$idx1]["emp_infos"][0]["emp_id"] = $rowapv["emp_id"];
			$arr_wkfwapv[$idx1]["emp_infos"][0]["emp_full_nm"] = $rowapv["emp_lt_nm"]." ".$rowapv["emp_ft_nm"];
			$arr_wkfwapv[$idx1]["emp_infos"][0]["st_nm"] = $rowapv["st_nm"];
			$arr_wkfwapv[$idx1]["emp_infos"][0]["display_caption"] = $rowapv["st_nm"];
			$arr_wkfwapv[$idx1]["info_registing_index"] = 0;
		}
	}
}
?>

<? $disabled_cyusi = (@$disabled_kousin_sousin_flg || @$apply_stat != "1" || @$re_apply_id ? "disabled" : ""); ?>
<? //if (!$wkfw_enabled) $disabled_cyusi = "1"; ?>
<? $disabled_kousin = (@$disabled_kousin_sousin_flg || @$apply_stat=="1" || @$re_apply_id ? "disabled" : ""); ?>
<? //if (!$wkfw_enabled) $disabled_kousin = ""; ?>
<? $disabled_kousin_sousin = (@$disabled_kousin_sousin_flg || @$re_apply_id ? "disabled" : ""); ?>
<? if (!$wkfw_enabled) $disabled_kousin_sousin = "disabled"; ?>
	<td style="padding-left:4px; text-align:right; white-space:nowrap;">
	<? if (@$xml_file){ ?>
	<input type="button" onclick="apply_regist2('2');" value="送信取消" <?=$disabled_cyusi?> />
	<? } ?>
	<input type="button" onclick="apply_regist2('');" value="更新" <?=$disabled_kousin?> />
	<? if (@$xml_file){ ?>
	<input type="button" onclick="apply_regist('<?=$approve_num?>', '<?=count(@$precond_wkfw_id)?>');" value="更新＋送信" <?=$disabled_kousin_sousin?> />
	<? } ?>
	</td></tr></table>
</div>


<div style="border-top:1px solid #d8d8d8; padding-top:4px; padding-bottom:4px">
<table style="width:100%"><tr>
	<td>
	<input type="button" onclick="parent.hidari.location.href='./summary_rireki_table.php?session=<?=$session?>&pt_id=<?=$pt_id?>';location.href='./summary_new.php?session=<?=$session?>&pt_id=<?=$pt_id?>&key1=<?=@$key1?>&key2=<?=@$key2?>';" value="新規作成に戻る"/>
	</td>
	<td style="text-align:right;">
  <? $disabled_tmpl = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
	<input type="button" onclick="showTemplate();" value="テンプレート" <?=$disabled_tmpl?>/>
	<script type="text/javascript">
		function showTemplate(){
			var diag_selection = document.getElementById("sel_diag").options[document.getElementById("sel_diag").selectedIndex];
			var url =
					'summary_tmpl_read.php'
				+ '?session=<?=$session?>'
				+ '&pt_id=<?=$pt_id?>'
				+ '&emp_id=<?=$emp_id?>'
				+ '&xml_file=<?=@$xml_file?>'
				+ '&tmpl_id=<?=$tmpl_id?>'
				+ '&summary_id=<?=$summary_id?>'
				+ '&enc_diag=' + enc_div_array[diag_selection.value]
				+ '&div_id=' + diag_selection.value
				+ '&cre_date='+document.touroku.sel_yr.options[document.touroku.sel_yr.selectedIndex].value
				+ '&seiki_emp_id='+document.getElementById("seiki_emp_id").value
				+ '&dairi_emp_id='+document.getElementById("dairi_emp_id").value
				+ document.touroku.sel_mon.options[document.touroku.sel_mon.selectedIndex].value
				+ document.touroku.sel_day.options[document.touroku.sel_day.selectedIndex].value;
			show_tmpl_window(url);
		}
	</script>

  <? $disabled_tmplcopy = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
	<input type="button" onclick="showTemplateCopy();" value="コピー" <?=$disabled_tmplcopy?>/>
	<script type="text/javascript">
		function showTemplateCopy(){
			var diag_selection = document.getElementById("sel_diag").options[document.getElementById("sel_diag").selectedIndex];
			var url =
					'summary_copy.php'
				+ '?session=<?=$session?>'
				+ '&pt_id=<?=$pt_id?>'
				+ '&emp_id=<?=$emp_id?>'
				+ '&xml_file=<?=@$xml_file?>'
				+ '&tmpl_id=<?=$tmpl_id?>'
				+ '&summary_id=<?=$summary_id?>'
				+ '&enc_diag=' + enc_div_array[diag_selection.value]
				+ '&div_id=' + diag_selection.value
				+ '&seiki_emp_id='+document.getElementById("seiki_emp_id").value
				+ '&dairi_emp_id='+document.getElementById("dairi_emp_id").value;
			show_copy_window(url);
		}
	</script>

  <? $disabled_teikeibun = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
	<input type="button" onclick="popupTeikeibun();" value="定型文" <?=$disabled_teikeibun?>/>
	<script type="text/javascript">
		function popupTeikeibun(){
			var url =
					'summary_fixedform_select.php'
				+ '?session=<?=$session?>'
				+ '&caller=summary_naiyo'
				+ '&multi_flg='
			show_fixedform_window(url);
		}
	</script>
	</td>
	</tr>
</table>
</div>

<div style="clear:both"></div>

<?
//==================================================
//内容入力領域のHTML出力
//==================================================
?>
	<? $readonly = (@$xml_file != "" ? "readonly" : ""); ?>
	<input type="hidden" name="summary_naiyo_escaped_amp" value =""/>
	<textarea id="summary_naiyo" style="width:98%; height:400px; ime-mode:active;" size="54" name="summary_naiyo" rows="1" <?=$readonly?>><?
	if (strlen($summary_naiyo) < 1){
		require_once("summary_tmpl_util.ini");
		$naiyo_textarea_value = get_summary_naiyo_html_for_tmpl_textarea_value($con,$fname,$tmpl_id,@$xml_file);
		echo($naiyo_textarea_value);
	} else if (@$xml_file) {
		echo $summary_naiyo;
	} else {
		echo str_replace("\n", "<br />", h($summary_naiyo));
	}
?>
</textarea>

<?
//==================================================
//下部の送信情報HTML出力
//==================================================
if($wkfw_enabled){
	show_application_template($con, $session, $fname, $arr_wkfwapv, $wkfw_id, @$apply_title, @$content, @$file_id, @$filename, @$back, @$mode, $apply_id);
}
?>

<?
//==================================================
//下部のボタンHTML出力
//==================================================
?>
<div style="margin-top:4px; text-align:right">
	<div style="float:left">
	<input type="button" onclick="parent.hidari.location.href='./summary_rireki_table.php?session=<?=$session?>&pt_id=<?=$pt_id?>';location.href='./summary_new.php?session=<?=$session?>&pt_id=<?=$pt_id?>&key1=<?=@$key1?>&key2=<?=@$key2?>';" value="新規作成に戻る"/>
	</div>
	<div style="float:right">
	<? if (@$xml_file){ ?>
	<input type="button" onclick="apply_regist2('2');" value="送信取消" <?=$disabled_cyusi?> />
	<? } ?>
	<input type="button" onclick="apply_regist2('');" value="更新" <?=$disabled_kousin?> />
	<? if (@$xml_file){ ?>
	<input type="button" onclick="apply_regist('<?=$approve_num?>', '<?=count(@$precond_wkfw_id)?>');" value="更新＋送信" <?=$disabled_kousin_sousin?> />
	<? } ?>
	</div>
	<div style="clear:both"></div>
</div>


<?
//==================================================
//送信HIDDENパラメータHTML出力
//==================================================
?>
<input type="hidden" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="apply_stat" value="<?=@$apply_stat?>">
<input type="hidden" name="is_apply_stat_change_only" value="">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="diag" value="<? echo($summary_div); ?>">
<input type="hidden" name="div_id" value="<? echo($summary_div_id); ?>">
<input type="hidden" name="key1" value="<? echo(@$key1); ?>">
<input type="hidden" name="key2" value="<? echo(@$key2); ?>">
<input type="hidden" name="summary_id" value="<? echo($summary_id); ?>">
<input type="hidden" name="xml_file" value="<? echo(@$xml_file); ?>">
<input type="hidden" name="tmpl_id" value="<? echo($tmpl_id); ?>">
<?
//==================================================
//起動時テンプレート表示JavaScript出力
//==================================================
?>
<?
if(@$xml_file != ""){
	if(@$no_tmpl_window != "1")
	{
		?>
		<script language="javascript">
		var url = './summary_tmpl_read.php'
			+ '?session=<?=$session?>'
			+ '&pt_id=<?=$pt_id?>'
			+ '&emp_id=<?=$emp_id?>'
			+ '&xml_file=<?=@$xml_file?>'
			+ '&tmpl_id=<?=$tmpl_id?>'
			+ '&summary_id=<?=$summary_id?>'
			+ '&cre_date=<?=$cre_date?>'
			+ '&enc_diag=<?=urlencode($summary_div)?>'
			+ '&div_id=<?=$summary_div_id?>';
			+ '&dairi_emp_id=<?=$dairi_emp_id?>';
			+ '&seiki_emp_id=<?=$seiki_emp_id?>';
		show_tmpl_window(url);
		</script>
		<?
	}
}
?>


</form>
</body>
</html>
