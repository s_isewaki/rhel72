<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");

//医療安全
$a = "2";


//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
if (check_authority($session, 48, $fname) == "0")
{
    showLoginPage();
    exit;
}



//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}



//==================================================
// ツリー情報
//==================================================
$tree = lib_get_category_tree($con, $a, null, $fname, "admin", $doc_counts);



//==================================================
// 一覧情報
//==================================================

//カテゴリが未指定の場合：カテゴリの一覧が表示対象
if ($c == "")
{
    //カテゴリ情報
    $category_list = lib_get_admin_category_list($con, $a, $fname);
}

//カテゴリが指定されている場合：フォルダ(カテゴリ)内のフォルダとコンテンツの一覧が表示対象
if ($c != "")
{
    //フォルダ情報
    $sql = "select el_folder.folder_id, el_folder.folder_name, el_folder.upd_time from el_folder left join el_tree on el_folder.folder_id = el_tree.child_id";
    $parent_id_cond = ($f == "") ? "is null" : " = $f";
    $cond = "where el_folder.lib_archive = '$a' and el_folder.lib_cate_id = '$c' and el_tree.parent_id $parent_id_cond order by el_folder.folder_name";
    $sel_child_folder = select_from_table($con, $sql, $cond, $fname);
    if ($sel_child_folder == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //コンテンツ情報
    $document_list = lib_get_admin_document_list($con, $a, $c, $f, $fname);
}



//==================================================
// フォルダ更新フラグの設定
//==================================================
//管理者であるため、全て更新可能。
$folder_modify_flg = true;



//==================================================
// ボタンのdisabled状況を設定
//==================================================
$parent_button_disabled      = ($c == "");
$folder_button_disabled      = !$folder_modify_flg;
$document_button_disabled    = !$folder_modify_flg || ($c == "");
$folder_edit_button_disabled = !$folder_modify_flg || ($c == "");
$delete_button_disabled      = !$folder_modify_flg;



//==================================================
// デフォルトの並び順を設定
//==================================================
// コンテンツ名の昇順
if ($o == "")
{
    $o = "1";
}



//==================================================
// ドラッグ＆ドロップ使用フラグを取得
//==================================================
$dragdrop_flg = lib_get_dragdrop_flg($con, $fname);




//画面名
$PAGE_TITLE = "書庫";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">
<script type="text/javascript" src="js/yui/build/event/event-min.js"></script>
<script type="text/javascript" src="js/yui/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript">


//起動時の処理
function initPage()
{
    //フォルダーツリー作成
    drawTree();


    <?
    if ($dragdrop_flg == "t")
    {
    ?>
    //ドラッグアンドドロップ作成
    constructDragDrop();
    <?
    }
    ?>
}



//フォルダーツリーを生成
var tree = null;
function drawTree()
{
    <?
    if (count($tree) == 0)
    {
    ?>
        var div = document.getElementById('folders');
        div.style.padding = '2px';
        div.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダは登録されていません。</font>';
        return;
    <?
    }
    else
    {
    ?>
        tree = new YAHOO.widget.TreeView('folders');
        var root = tree.getRoot();
        <?
        lib_write_tree_js($tree, $a, $c, $f, $o, $doc_counts, $session, $fname, $parent_folder_id, $dragdrop_flg);
        ?>
        tree.draw();
    <?
    }
    ?>
}



//ドラッグアンドドロップを生成
function constructDragDrop()
{
    var divs = document.getElementsByTagName('div');
    for (var i = 0, j = divs.length; i < j; i++)
    {
        if (divs[i].className == 'doc')
        {
            var doc = new YAHOO.util.DD(divs[i].id, 'docs');
            doc.setHandleElId('h_'.concat(divs[i].id));

            doc.startPos = YAHOO.util.Dom.getXY(divs[i]);

            doc.startDrag = function (x, y)
            {
                var el = this.getDragEl();
                el.style.backgroundColor = '#fefe83';
                el.style.zIndex = 999;
            }

            doc.onDragDrop = function (e, id)
            {
                var nodeIndex = id.match(/\d+$/);
                tree.getNodeByIndex(nodeIndex).getNodeHtml().match(/(\d+),(\d*)<\/span>/);
                var c = RegExp.$1;
                var f = RegExp.$2;
                var lib_id = this.getDragEl().id.match(/\d+$/);
                var url = 'hiyari_el_move.php?session=<?=$session?>&c='.concat(c).concat('&f=').concat(f).concat('&o=<?=$o?>&lib_id=').concat(lib_id)<?if($path == "3"){?>.concat('&path=3')<?}?>;
                location.href = url;
            }

            doc.endDrag = function (x, y)
            {
                var el = this.getDragEl();
                YAHOO.util.Dom.setXY(el, this.startPos);
                el.style.backgroundColor = '';
                el.style.zIndex = 0;
            }
        }
    }
}


function expandAncestor(node)
{
    if (node.parent)
    {
        node.parent.expand();
        expandAncestor(node.parent);
    }
}



//全て開く
function expandAll()
{
    if (tree)
    {
        tree.expandAll();
        document.getElementById('expand').style.display = 'none';
        document.getElementById('collapse').style.display = '';
    }
}
//全て閉じる
function collapseAll()
{
    if (tree)
    {
        tree.collapseAll();
        document.getElementById('collapse').style.display = 'none';
        document.getElementById('expand').style.display = '';
    }
}





//コンテンツ、フォルダの削除
function deleteDocument()
{
    var checked_cid = getCheckedIds(document.mainform.elements['cid[]']);
    var checked_did = getCheckedIds(document.mainform.elements['did[]']);
    var checked_fid = getCheckedIds(document.mainform.elements['fid[]']);

    if (checked_cid.length == 0 && checked_did.length == 0 && checked_fid.length == 0)
    {
        alert('削除対象が選択されていません。');
        return;
    }

    if (!confirm('選択されたデータを削除します。よろしいですか？'))
    {
        return;
    }

    document.mainform.action = 'hiyari_el_delete.php';
    addHiddenElement(document.mainform, 'archive', '<?=$a?>');
    addHiddenElement(document.mainform, 'category', '<?=$c?>');
    addHiddenElement(document.mainform, 'folder_id', '<?=$f?>');
    addHiddenElement(document.mainform, 'o', '<?=$o?>');
    document.mainform.submit();
}
//※deleteDocument()用
function getCheckedIds(boxes)
{
    var checked_ids = new Array();
    if (boxes)
    {
        if (!boxes.length)
        {
            if (boxes.checked)
            {
                checked_ids.push(boxes.value);
            }
        }
        else
        {
            for (var i = 0, j = boxes.length; i < j; i++)
            {
                if (boxes[i].checked)
                {
                    checked_ids.push(boxes[i].value);
                }
            }
        }
    }
    return checked_ids;
}
//※deleteDocument()用
function addHiddenElement(frm, name, value)
{
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    frm.appendChild(input);
}



//「上の階層へ」
function moveToParent()
{
    <?
    //書庫へ移動
    if ($f == "")
    {
    ?>
        location.href = '<?=$fname?>?session=<?=$session?>&a=<?=$a?>&o=<?=$o?>';
    <?
    }
    //カテゴリへ移動
    else if ($parent_folder_id == "")
    {
    ?>
        location.href = '<?=$fname?>?session=<?=$session?>&a=<?=$a?>&c=<?=$c?>&o=<?=$o?>';
    <?
    }
    //親フォルダへ移動
    else
    {
    ?>
        location.href = '<?=$fname?>?session=<?=$session?>&a=<?=$a?>&c=<?=$c?>&f=<?=$parent_folder_id?>&o=<?=$o?>';
    <?
    }
    ?>
}




//コンテンツ登録画面を表示します。
function registDocument()
{
    var url = "hiyari_el_register.php?session=<?=$session?>&archive=<?=$a?>&category=<?=$c?>&folder_id=<?=$f?>&o=<?=$o?>&path=3";
    show_sub_window(url);
}

//フォルダ作成画面を表示します。
function createFolder()
{
    var url = "hiyari_el_folder_register.php?session=<?=$session?>&archive=<?=$a?>&cate_id=<?=$c?>&parent=<?=$f?>&path=3";
    show_sub_window(url);
}

//フォルダ更新画面を表示します。
function updateFolder()
{
    var url = "hiyari_el_folder_update.php?session=<?=$session?>&archive=<?=$a?>&cate_id=<?=$c?>&folder_id=<?=$f?>&path=3";
    show_sub_window(url);
}

//参照履歴画面を表示します。
function show_refer_log_page()
{
    var url = "hiyari_el_admin_refer_log.php?session=<?=$session?>";
    show_sub_window(url);
}

//子画面を表示します。
function show_sub_window(url)
{
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
    window.open(url, '_blank',option);
}

//リロードします。※注意：この関数は別画面から呼び出されます。
function reload_page()
{
    location.href = "<?=$fname?>?session=<?=$session?>&a=<?=$a?>&c=<?=$c?>&f=<?=$f?>&o=<?=$o?>";
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.list td td {border-width:0;}
.selectedLabel {font-weight:bold; color:black; padding:1px;}
</style>
</head>



<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<form name="mainform" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>






<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">



<table width="100%" border="0" cellspacing="0" cellpadding="0">


<!-- 上部ボタン  START -->
<tr valign="top">
<td align="right" colspan="3">
<input type="button" value="参照履歴" onclick="show_refer_log_page()">
<input type="button" value="上の階層へ"<? if ($parent_button_disabled) {echo(" disabled");} ?> onclick="moveToParent();">
<input type="button" value="フォルダ作成"<? if ($folder_button_disabled) {echo(" disabled");} ?> onclick="createFolder();">
<input type="button" value="コンテンツ登録"<? if ($document_button_disabled) {echo(" disabled");} ?> onclick="registDocument();">
<input type="button" value="フォルダ名更新"<? if ($folder_edit_button_disabled) {echo(" disabled");} ?> onclick="updateFolder();">
<input type="button" value="削除"<? if ($delete_button_disabled) {echo(" disabled");} ?> onclick="deleteDocument();">
</td>
</tr>
<!-- 上部ボタン  END -->


<tr height="2">
<td colspan="3">
</td>
</tr>


<!-- フォルダ/コンテンツ START -->
<tr valign="top">
<!-- フォルダ START -->
<td>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
    <tr>
    <td>

        <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr height="22" bgcolor="#DFFFDC">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
        <td align="right">
        <span id="expand"><input type="button" value="全て開く" onclick="expandAll();"></span>
        <span id="collapse" style="display:none;"><input type="button" value="全て閉じる" onclick="collapseAll();"></span>
        </td>
        </tr>
        </table>

    </td>
    </tr>
    <tr>
    <td valign="top" height="400" bgcolor="#FFFFFF"><div id="folders"></div></td>
    </tr>
    </table>

</td>
<!-- フォルダ END -->
<td></td>
<!-- コンテンツ START -->
<td>

    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22" bgcolor="#DFFFDC">

    <td align="center" width="40">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font>
    </td>

    <td>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <?
        //コンテンツ名：昇順
        if ($o == "1")
        {
            echo("<a href=\"$fname?session=$session&a=$a&c=$c&f=$f&o=2\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">コンテンツ名</a>");
        }
        //コンテンツ名：降順
        else
        {
            echo("<a href=\"$fname?session=$session&a=$a&c=$c&f=$f&o=1\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">コンテンツ名</a>");
        }
        ?>
        </font>
    </td>

    <td width="50">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感想</font>
    </td>

    <td width="95">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <?
        //更新日時：昇順
        if ($o == "3")
        {
            echo("<a href=\"$fname?session=$session&a=$a&c=$c&f=$f&o=4\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">更新日</a>");
        }
        //更新日時：降順
        else
        {
            echo("<a href=\"$fname?session=$session&a=$a&c=$c&f=$f&o=3\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">更新日</a>");
        }
        ?>
        </font>
    </td>

    <td width="75">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <?
        //サイズ：昇順
        if ($o == "5")
        {
            echo("<a href=\"$fname?session=$session&a=$a&c=$c&f=$f&o=6\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">サイズ</a>");
        }
        //サイズ：降順
        else
        {
            echo("<a href=\"$fname?session=$session&a=$a&c=$c&f=$f&o=5\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">サイズ</a>");
        }
        ?>
        </font>
    </td>

    <td width="50">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font>
    </td>

    </tr>

    <?
    //一覧出力
    show_document_list($category_list, $sel_child_folder, $document_list, $a, $c, $f, $o, $folder_modify_flg, $doc_counts, $session, $dragdrop_flg);
    ?>
    </table>

</td>
<!-- コンテンツ END -->
</tr>
<!-- フォルダ/コンテンツ END -->
</table>





        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
</table>
<!-- 本体 END -->



<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->


</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="path" value="3">
</form>
</body>
</html>
<? pg_close($con); ?>
<?



//========================================================================================================================================================================================================
//内部関数
//========================================================================================================================================================================================================

// フォルダツリー表示JavaScriptを出力
function lib_write_tree_js($tree, $a, $c, $f, $o, $doc_counts, $session, $fname, &$parent_folder_id, $dragdrop_flg)
{
    $is_safari = (strpos($_SERVER["HTTP_USER_AGENT"], "Safari") !== false);

    //医療安全
    $a = 2;

    foreach ($tree as $tmp_folder)
    {
        $tmp_type = $tmp_folder["type"];
        $tmp_id = $tmp_folder["id"];
        $tmp_nm = str_replace("'", "\\'", h($tmp_folder["name"]));

        if ($tmp_type == "category")
        {
            $tmp_elm = "folder$tmp_id";
            if (is_array($doc_counts))
            {
                $tmp_doc_count = "(" . intval($doc_counts[$tmp_id]["-"]) . ")";
            }
            else
            {
                $tmp_doc_count = "";
            }

            if ($is_safari) {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm$tmp_doc_count</font><span style=\"display:none;\">$tmp_id,</span>$lock_icon', root, false);\n");
            } else {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm<font color=\"#444\">$tmp_doc_count</font></font><span style=\"display:none;\">$tmp_id,</span>', root, false);\n");
            }
            echo("$tmp_elm.onLabelClick = function () {location.href = '$fname?session=$session&a=$a&c=$tmp_id&o=$o'; return false;}\n");
            if ($dragdrop_flg == "t")
            {
                echo("new YAHOO.util.DDTarget($tmp_elm.labelElId, 'docs');\n");
            }
            if ($c == $tmp_id && $f == "")
            {
                echo("$tmp_elm.labelStyle = 'ygtvlabel selectedLabel';\n");
            }
        }
        else if ($tmp_type == "folder")
        {
            $tmp_cate_id = $tmp_folder["cate_id"];
            $tmp_parent_id = $tmp_folder["parent_id"];
            $tmp_parent_elm = ($tmp_parent_id == "") ? "folder$tmp_cate_id" : "folder{$tmp_cate_id}_{$tmp_parent_id}";
            $tmp_elm = "folder{$tmp_cate_id}_{$tmp_id}";
            if (is_array($doc_counts))
            {
                $tmp_doc_count = "(" . intval($doc_counts[$tmp_cate_id][$tmp_id]) . ")";
            }
            else
            {
                $tmp_doc_count = "";
            }

            if ($is_safari) {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm$tmp_doc_count</font><span style=\"display:none;\">$tmp_cate_id,$tmp_id</span>$lock_icon', $tmp_parent_elm, false);\n");
            } else {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm<font color=\"#444\">$tmp_doc_count</font></font><span style=\"display:none;\">$tmp_cate_id,$tmp_id</span>', $tmp_parent_elm, false);\n");
            }
            echo("$tmp_elm.onLabelClick = function () {location.href = '$fname?session=$session&a=$a&c=$tmp_cate_id&f=$tmp_id&o=$o'; return false;}\n");
            if ($dragdrop_flg == "t")
            {
                echo("new YAHOO.util.DDTarget($tmp_elm.labelElId, 'docs');\n");
            }
            if ($f == $tmp_id)
            {
                $parent_folder_id = $tmp_parent_id;
                echo("$tmp_elm.labelStyle = 'ygtvlabel selectedLabel';\n");
                echo("expandAncestor($tmp_elm);\n");
            }
        }

        lib_write_tree_js($tmp_folder, $a, $c, $f, $o, $doc_counts, $session, $fname, $parent_folder_id, $dragdrop_flg);
    }
}

// コンテンツ一覧を出力
function show_document_list($category_list, $sel_child_folder, $document_list, $a, $c, $f, $o, $folder_modify_flg, $doc_counts, $session, $dragdrop_flg)
{
    //医療安全
    $a = 2;

    $docs = array();
    if ($c == "")
    {
        foreach ($category_list as $tmp_category)
        {
            $tmp_category_id = $tmp_category["id"];
            $tmp_doc_count = $doc_counts[$tmp_category_id]["-"];
            $tmp_folder_modify_flg = ($folder_modify_flg);
            $tmp_folder_delchk_flg = ($folder_modify_flg && ($tmp_doc_count == 0));
            $tmp_modified = $tmp_category["modified"];

            $docs[] = array(
                "id" => $tmp_category_id,
                "name" => $tmp_category["name"],
                "type" => "category",
                "file_type" => "フォルダ",
                "size" => 0,
                "modify_flg" => $tmp_folder_modify_flg,
                "delchk_flg" => $tmp_folder_delchk_flg,
                "path" => "",
                "modified" => $tmp_modified
            );
        }
    }
    else
    {
        if (is_resource($sel_child_folder))
        {
            while ($row = pg_fetch_array($sel_child_folder))
            {
                $tmp_folder_id = $row["folder_id"];
                $tmp_doc_count = $doc_counts[$c][$tmp_folder_id];
                $tmp_folder_modify_flg = ($folder_modify_flg);
                $tmp_folder_delchk_flg = ($folder_modify_flg && ($tmp_doc_count == 0));

                $docs[] = array(
                    "id" => $tmp_folder_id,
                    "name" => $row["folder_name"],
                    "type" => "folder",
                    "file_type" => "フォルダ",
                    "size" => 0,
                    "modify_flg" => $tmp_folder_modify_flg,
                    "delchk_flg" => $tmp_folder_delchk_flg,
                    "path" => "",
                    "modified" => $row["upd_time"]
                );
            }
        }
        if (is_array($document_list))
        {
            $docs = array_merge($docs, $document_list);
        }
    }

    if (count($docs) == 0)
    {
        return;
    }

    switch ($o)
    {
        case "1":  // 名前の昇順
            usort($docs, "sort_docs_by_name");
            break;
        case "2":  // 名前の降順
            usort($docs, "sort_docs_by_name_desc");
            break;
        case "3":  // 更新日時の昇順
            usort($docs, "sort_docs_by_modified");
            break;
        case "4":  // 更新日時の降順
            usort($docs, "sort_docs_by_modified_desc");
            break;
        case "5":  // サイズの昇順
            usort($docs, "sort_docs_by_size");
            break;
        case "6":  // サイズの降順
            usort($docs, "sort_docs_by_size_desc");
            break;
    }

    foreach ($docs as $tmp_doc)
    {
        $tmp_did = $tmp_doc["id"];
        $tmp_dnm = h($tmp_doc["name"]);
        $tmp_type = $tmp_doc["type"];
        $tmp_file_type = $tmp_doc["file_type"];
        $tmp_size = $tmp_doc["size"];
        $tmp_modify_flg = $tmp_doc["modify_flg"];
        $tmp_path = $tmp_doc["path"];
        $tmp_modified = $tmp_doc["modified"];

        if ($tmp_type == "category" || $tmp_type == "folder")
        {
            $tmp_delchk_flg = ($tmp_doc["delchk_flg"]) ? "t" : "f";
        }
        else
        {
            $tmp_delchk_flg = $tmp_modify_flg;
        }

        // 削除チェックボックス
        if ($tmp_delchk_flg == "t")
        {
            if ($tmp_type == "category")
            {
                $cbox = "<input type=\"checkbox\" name=\"cid[]\" value=\"$tmp_did\">";
            }
            else if ($tmp_type == "folder")
            {
                $cbox = "<input type=\"checkbox\" name=\"fid[]\" value=\"$tmp_did\">";
            }
            else
            {
                $cbox = "<input type=\"checkbox\" name=\"did[]\" value=\"$tmp_did\">";
            }
        }
        else
        {
            $cbox = "<input type=\"checkbox\" disabled>";
        }

        // コンテンツのボックスのクラス名
        $tmp_div_class = ($tmp_type == "document" && $tmp_modify_flg == "t") ? "doc" : "";

        // アイコン
        if ($tmp_type == "category" || $tmp_type == "folder")
        {
            $icon = "<img src=\"img/icon/folder.gif\" alt=\"\" width=\"16\" height=\"16\" border=\"0\" style=\"vertical-align:middle;\">";
        }
        else
        {
            $cursor = ($tmp_modify_flg == "t" && $dragdrop_flg == "t") ? "cursor:move;" : "";
            switch ($tmp_file_type)
            {
                case "Word":
                    $img = "word.jpg";
                    break;
                case "Excel":
                    $img = "excel.jpg";
                    break;
                case "PowerPoint":
                    $img = "powerpoint.jpg";
                    break;
                case "PDF":
                    $img = "pdf.jpg";
                    break;
                case "テキスト":
                    $img = "text.jpg";
                    break;
                case "JPEG":
                    $img = "jpeg.jpg";
                    break;
                case "GIF":
                    $img = "gif.jpg";
                    break;
                default:
                    $img = "other.jpg";
                    break;
            }
            $icon = "<img id=\"h_doc$tmp_did\" src=\"img/icon/$img\" alt=\"\" width=\"16\" height=\"16\" border=\"0\" style=\"vertical-align:middle;$cursor\">";
        }

        // 名前のリンク先
        if ($tmp_type == "category")
        {
            $tmp_anchor = "<a href=\"$fname?session=$session&a=$a&c=$tmp_did&o=$o\">$tmp_dnm</a>";
        }
        else if ($tmp_type == "folder")
        {
            $tmp_anchor = "<a href=\"$fname?session=$session&a=$a&c=$c&f=$tmp_did&o=$o\">$tmp_dnm</a>";
        }
        else
        {
            $tmp_lib_url = urlencode($tmp_path);
            if (lib_get_filename_flg() > 0)
            {
                $tmp_anchor = "<a href=\"hiyari_el_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url\">$tmp_dnm</a>";
            }
            else
            {
                $tmp_anchor = "<a href=\"hiyari_el_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url\" onclick=\"window.open('hiyari_el_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url'); return false;\">$tmp_dnm</a>";
            }
        }

        //感想
        $kansou_url = "hiyari_el_kansou.php?session={$session}&lib_id={$tmp_did}&path=3";
        if($tmp_type == "document")
        {
            $tmp_kansou = "<a href=\"javascript:show_sub_window('$kansou_url')\">感想</a>";
        }
        else
        {
            $tmp_kansou = "&nbsp;";
        }

        // 更新日
        if (strlen($tmp_modified) >= 8)
        {
            $tmp_modified_str = preg_replace("/^(\d{4})(\d{2})(\d{2})(.*)$/", "$1/$2/$3", $tmp_modified);
        }
        else
        {
            $tmp_modified_str = "-";
        }

        // サイズ
        $size = format_size($tmp_size);

        // 権限
        if ($tmp_type == "category")
        {
            $auth_update_url = "hiyari_el_folder_update.php?session=$session&archive=$a&cate_id=$tmp_did&folder_id=&o=$o&path=3";
        }
        else if ($tmp_type == "folder")
        {
            $auth_update_url = "hiyari_el_folder_update.php?session=$session&archive=$a&cate_id=$c&folder_id=$tmp_did&o=$o&path=3";
        }
        else
        {
            $auth_update_url = "hiyari_el_update.php?session=$session&a=$a&c=$c&f=$f&o=$o&lib_id=$tmp_did&path=3";
        }
        $tmp_auth = ($tmp_modify_flg == "t") ? "更新" : "参照";
        $tmp_auth = "<a href=\"javascript:show_sub_window('$auth_update_url')\">$tmp_auth</a>";

        echo("<tr height=\"22\" bgcolor=\"#FFFFFF\">\n");
        echo("<td align=\"center\">$cbox</td>\n");
        echo("<td style=\"padding:0;\"><div id=\"doc$tmp_did\" class=\"$tmp_div_class\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr valign=\"top\"><td>$icon</td><td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_anchor</font></td></tr></table></div></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_kansou</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_modified_str</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$size</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_auth</font></td>\n");
        echo("</tr>\n");
    }
}

//※show_document_list()専用関数：サイズをフォーマット
function format_size($size)
{
    if ($size == 0)
    {
        return "-";
    }
    else if ($size <= 1023)
    {
        return "{$size}バイト";
    }
    else if ($size >= 1024 && $size <= 1048576)
    {
        $ret = number_format(($size / 1024), 2) . "KB";
        $ret = str_replace(".00", "", $ret);
        return preg_replace("/(.*)\.(\d)0/", "$1.$2", $ret);
    }
    else
    {
        $ret = number_format(($size / 1024 / 1024), 2) . "MB";
        $ret = str_replace(".00", "", $ret);
        return preg_replace("/(.*)\.(\d)0/", "$1.$2", $ret);
    }
}


// コンテンツ一覧を名前の昇順でソート
function sort_docs_by_name($doc1, $doc2)
{
    return strcasecmp($doc1["name"], $doc2["name"]);
}

// コンテンツ一覧を名前の降順でソート
function sort_docs_by_name_desc($doc1, $doc2)
{
    return sort_docs_by_name($doc1, $doc2) * -1;
}

// コンテンツ一覧を更新日時の昇順でソート
function sort_docs_by_modified($doc1, $doc2)
{
    return strcasecmp($doc1["modified"], $doc2["modified"]);
}

// コンテンツ一覧を更新日時の降順でソート
function sort_docs_by_modified_desc($doc1, $doc2)
{
    return sort_docs_by_modified($doc1, $doc2) * -1;
}

// コンテンツ一覧をサイズの昇順でソート
function sort_docs_by_size($doc1, $doc2)
{
    return strnatcasecmp($doc1["size"], $doc2["size"]);
}

// コンテンツ一覧をサイズの降順でソート
function sort_docs_by_size_desc($doc1, $doc2)
{
    return sort_docs_by_size($doc1, $doc2) * -1;
}
?>
