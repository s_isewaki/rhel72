<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("get_values.ini");
require("./conf/sql.inf");

define("IMAGETYPE_GIF", 1);
define("IMAGETYPE_JPEG", 2);
define("IMAGETYPE_PNG", 3);

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($photo_name == "") {
	echo("<script language='javascript'>alert('写真が選択されていません');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} else {
	$photo_info = @getimagesize($photo);
	if ($photo_info === false) {
		$photo_format = "";
	} else {
		$photo_format = $photo_info[2];
	}

	if ($photo_format != IMAGETYPE_GIF && $photo_format != IMAGETYPE_JPEG && $photo_format != IMAGETYPE_PNG) {
		echo("<script language='javascript'>alert('登録できる画像はGIF、JPEG、PNGのみです');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
}

// 職員IDの取得
$con = connect2db($fname);
$emp_id = get_emp_id($con, $session, $fname);
pg_close($con);

// ファイル名の作成
switch ($photo_format) {
case IMAGETYPE_GIF:
	$file_name = "$emp_id.gif";
	$del_files = array("$emp_id.jpg", "$emp_id.png");
	break;
case IMAGETYPE_JPEG:
	$file_name = "$emp_id.jpg";
	$del_files = array("$emp_id.gif", "$emp_id.png");
	break;
case IMAGETYPE_PNG:
	$file_name = "$emp_id.png";
	$del_files = array("$emp_id.gif", "$emp_id.jpg");
	break;
}

//// 保存先ディレクトリがなければ作成
if (!is_dir("profile")) {
	mkdir("profile", 0755);
}
if (!is_dir("profile/img")) {
	mkdir("profile/img", 0755);
}

// ファイルの保存
copy($photo, "profile/img/$file_name");

// 不要ファイルの削除
foreach ($del_files as $del_file) {
	if (is_file("profile/img/$del_file")) {
		unlink("profile/img/$del_file");
	}
}

echo("<script language='javascript'>opener.location.href = 'main_menu.php?session=$session';");
echo("self.close();</script>");
?>
