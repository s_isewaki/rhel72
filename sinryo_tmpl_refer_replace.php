<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix テンプレートファイル差し替え</title>
</head>
<body style="margin:5px; font-size:13px">

<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 59, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}


//==============================================================================
// ファイルアップロードを行った場合は、以下の実施
// ・ファイルのテンポラリ登録
// ・ファイルの再リード
// ・HTMLにどさっとコンテンツを書いて、親画面に読み込ませる(sinryo_tmpl_refer_set.phpと同じ手法で）
//==============================================================================
$filename = @$_FILES["upload_file"]["name"];
if ($filename != "") {

	if ($filename != $tmpl_file_name){
		echo("<script language=\"javascript\">alert('同名ファイルをアップロード指定してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	// ファイルアップロードチェック
	define(UPLOAD_ERR_OK, 0);
	define(UPLOAD_ERR_INI_SIZE, 1);
	define(UPLOAD_ERR_FORM_SIZE, 2);
	define(UPLOAD_ERR_PARTIAL, 3);
	define(UPLOAD_ERR_NO_FILE, 4);

	switch ($_FILES["upload_file"]["error"]) {
	case UPLOAD_ERR_INI_SIZE:
	case UPLOAD_ERR_FORM_SIZE:
		echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	case UPLOAD_ERR_PARTIAL:
	case UPLOAD_ERR_NO_FILE:
		echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	// ファイル保存用ディレクトリがなければ作成
	if (!is_dir("workflow")) mkdir("workflow", 0755);
	if (!is_dir("workflow/tmp")) mkdir("workflow/tmp", 0755);

	// アップロードされたファイルを削除して保存
	$savefilename = "workflow/tmp/{$session}_{$filename}";
	if (file_exists($savefilename)) unlink($savefilename);
	copy($_FILES["upload_file"]["tmp_name"], $savefilename);
	chmod($savefilename, 0666);

	// 内容読み込み
	if (!$fp = fopen($savefilename, "r")) {
		echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	$content = fread($fp, filesize($savefilename));
	fclose($fp);

	// 内容チェック
	//$chkarray = array("INSERT ", "INSERT	", "UPDATE ", "UPDATE	", "DELETE ", "DELETE	", "CONNECT2DB", "SELECT_FROM_ TABLE", "PG_CONNECT", "PG_EXEC", "<FORM");
	$chkarray = array("INSERT ", "INSERT	", "UPDATE ", "UPDATE	", "CONNECT2DB", "SELECT_FROM_ TABLE", "PG_CONNECT", "PG_EXEC", "<FORM");
	$content_upper = strtoupper($content);
	foreach($chkarray as $chkstr){
		if(!(strpos($content_upper, $chkstr) === false)){
			echo("<script language=\"javascript\">alert('指定できない文字列があります($chkstr)。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
	}

	// テンプレートのバージョンの記述があれば1、なければ0
	// 旧テンプレートはcreate_Xxxx_xml.phpというXML変換テンプレートが作成されるが、
	// このXML変換テンプレートは一時ファイルを保持していかなかった。
	// 新しくXML変換テンプレート用に差し替え機能を設けるのも費用対効果的に損なので、
	// 新型テンプレートのみXML差し替えを行えるものとする。
	$template_version_description = (strpos($content, "COMEDIX_TEMPLATE_VERSION_DEFINITION") === false ? 0 : 1);
	if (!$template_version_description){
		echo("<script language=\"javascript\">alert('このテンプレート書式は差し替え機能に対応していません。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

?>
	<textarea id="sinryo_tmpl_refer_set_textarea" style="display:none"><?= h($content) ?></textarea>
	<script type="text/javascript">
		if (opener.document.tmpl) {
			opener.document.tmpl.tmpl_file.value = "<? echo($tmpl_file_name); ?>";
			opener.document.tmpl.tmpl_content.value = document.getElementById("sinryo_tmpl_refer_set_textarea").value;
		}
		self.close();
	</script>
	</body>
	</html>
	<? die; ?>
<? } ?>

<?
//==============================================================================
// 初期表示時
//==============================================================================
?>

<? // ヘッダ部 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#5279a5">
		<td style="padding-left:4px; font-size:14px; color:#fff" height="32"><b>テンプレートファイル差し替え</b></td>
		<td style="text-align:right; padding-right:4px"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
</table>

<form action="sinryo_tmpl_refer_replace.php" method="post" enctype="multipart/form-data" name="tmpl">
	<input type="hidden" name="session" value="<?=$session?>"/>

<div style="margin-top:8px;">
	テンプレートファイル名：
	<input type="text" name="tmpl_file_name" value="<?=$_REQUEST["tmpl_file_name"]?>" size="100" readonly="readonly" style="border:0;"/>
</div>
<div style="margin-top:8px;">
	<input type="file" name="upload_file" size="80">
	※<? echo(ini_get("upload_max_filesize")); ?>まで&nbsp;
	<input type="submit" value="本文差し替え" name="add">
</div>

</form>
</body>
</html>
