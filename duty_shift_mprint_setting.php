<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?php
//管理帳票設定
//duty_shift_mprint_setting.php

require_once("about_comedix.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_mprint_common_class.php");
require_once("get_menu_label.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 勤務シフト作成
$shift_menu_label = get_shift_menu_label($con, $fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_mprint = new duty_shift_mprint_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);

///-----------------------------------------------------------------------------
// スタッフ情報を取得
///-----------------------------------------------------------------------------
$set_array = array();
if ($reload_flg == "1") {
    ///-----------------------------------------------------------------------------
    //再表示の場合、元データ取得
    ///-----------------------------------------------------------------------------
    $wk_array = array();
    $wk_emp_id_array = split(",", $emp_id_array);
    $m=0;
    foreach ($wk_emp_id_array as $emp_id) {
        $wk_array[$m]["id"] = $emp_id;
        $m++;
    }
    ///-----------------------------------------------------------------------------
    //指定職員IDの追加／削除
    ///-----------------------------------------------------------------------------
    if ($del_emp_id != "") {
        //削除の場合
        $m=0;
        for($i=0; $i<count($wk_array); $i++) {
            if ($wk_array[$i]["id"] != $del_emp_id) {
                $set_array[$m] = $wk_array[$i];
                $m++;
            }
        }
    } else {
        //追加データの場合
        $m=0;
        $wk_add_array = array();
        $wk_emp_id_array = split(",", $add_emp_id_array);
        foreach ($wk_emp_id_array as $emp_id) {
            $wk_add_array[$m]["id"] = $emp_id;
            $m++;
        }
        $m=0;
        if ($add_idx == -1) {
            for($k=0; $k<count($wk_add_array); $k++) {
                $set_array[$m] = $wk_add_array[$k];
                $m++;
            }
        }
        if ($emp_id_array != "") {
            for($i=0; $i<count($wk_array); $i++) {
                if ($i == $add_idx) {
                    $set_array[$m] = $wk_array[$i];
                    $m++;
                    for($k=0; $k<count($wk_add_array); $k++) {
                        $set_array[$m] = $wk_add_array[$k];
                        $m++;
                    }
                } else {
                    $set_array[$m] = $wk_array[$i];
                    $m++;
                }
            }
        }
    }
    ///-----------------------------------------------------------------------------
    //データ再設定
    ///-----------------------------------------------------------------------------
    $staff_name_array = $obj_mprint->get_duty_shift_staff_name($set_array);
    $staff_array = array();

    for($i=0;$i<count($set_array);$i++){
        $staff_array[$i]["id"] = $set_array[$i]["id"];			//スタッフＩＤ（職員ＩＤ）
        $staff_array[$i]["no"] = $i + 1;						//表示順
        $wk_id = $set_array[$i]["id"];
        $staff_array[$i]["name"] = $staff_name_array["$wk_id"];			//スタッフ名称
    }
} else {
    ///-----------------------------------------------------------------------------
    //初期表示の場合
    ///-----------------------------------------------------------------------------
    $staff_array = $obj_mprint->get_duty_shift_staff_array();
}

//帳票一覧取得
//既存データの取得
if ($reload_flg == "1") {
    $arr_layout_mst = array();
    for ($i=0; $i<count($layout_id); $i++) {
        $arr_layout_mst[$i]["layout_id"] = $layout_id[$i];
        $arr_layout_mst[$i]["layout_name"] = $layout_name[$i];
    }
}
else {
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst();
}
?>
<title>CoMedix <? echo($shift_menu_label); ?> ｜ 管理帳票</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
	jQuery.noConflict();
	jQuery(document).ready(function($) {
		$('#container > tbody').sortable({
			items: '> tr',
			axis: 'y',
			cancel: 'input,a,button',
			placeholder: 'droptarget',
			opacity: 0.4,
			scroll: false
		});
	});



	var childwin = null;

	///-----------------------------------------------------------------------------
	// openEmployeeList
	///-----------------------------------------------------------------------------
	function openEmployeeList(item_id) {
		dx = screen.width;
		dy = screen.top;
		base = 0;
		wx = 720;
		wy = 600;
		var url = './emplist_popup.php';
		url += '?session=<?=$session?>';
		url += '&emp_id=<?=$emp_id?>';
		url += '&mode=19';
		url += '&item_id='+item_id;
		childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

		childwin.focus();
	}
	///-----------------------------------------------------------------------------
	// closeEmployeeList
	///-----------------------------------------------------------------------------
	function closeEmployeeList() {
		if (childwin != null && !childwin.closed) {
			childwin.close();
		}
		childwin = null;
	}
	///-----------------------------------------------------------------------------
	// add_target_list
	///-----------------------------------------------------------------------------
	function add_target_list(item_id, emp_id, emp_name)
	{
		var emp_ids = emp_id.split(", ");
		var emp_names = emp_name.split(", ");

		///-----------------------------------------------------------------------------
		//変更用（先頭の人で判定）
		///-----------------------------------------------------------------------------
		wk_emp_id = emp_ids[0];
		wk_emp_name = emp_names[0];
		///-----------------------------------------------------------------------------
		//重複チェック（重複データは対象外）
		///-----------------------------------------------------------------------------
		var wk = document.mainform.emp_id_array.value;
		var emp_ids2 = wk.split(",");
		var ok_emp_ids="";
		var err_flg = 0;
		for(i=0;i<emp_ids.length;i++){
			err_flg = 0;
			for(k=0;k<emp_ids2.length;k++){
				if (emp_ids[i] == emp_ids2[k]) {
					err_flg = 1;
					break;
				}
			}
			if (err_flg == 0){
				if (ok_emp_ids != "") {
					ok_emp_ids += ",";
				}
				ok_emp_ids += emp_ids[i];
			}
		}

		if (ok_emp_ids == "") {
			alert('職員の重複指定はできません。');
			return;
		}
		///-----------------------------------------------------------------------------
		//画面再表示
		///-----------------------------------------------------------------------------
		document.mainform.reload_flg.value = "1";
		document.mainform.del_emp_id.value = "";
		document.mainform.add_emp_id_array.value = ok_emp_ids;
		document.mainform.action="duty_shift_mprint_setting.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 追加
	///-----------------------------------------------------------------------------
	function addEmp(idx)
	{
		//追加行
		document.mainform.add_idx.value = idx;
		document.mainform.del_emp_id.value = "";
		//職員名簿表示
		openEmployeeList('1');
	}
	///-----------------------------------------------------------------------------
	// 削除
	///-----------------------------------------------------------------------------
	function delEmp(emp_id)
	{
		//画面再表示
		document.mainform.reload_flg.value = "1";
		document.mainform.add_idx.value = "";
		document.mainform.del_emp_id.value = emp_id;
		document.mainform.action="duty_shift_mprint_setting.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 更新
	///-----------------------------------------------------------------------------
	function editData() {
		//職員登録子画面をＣＬＯＳＥ
		closeEmployeeList();

		//更新処理
		document.mainform.action="duty_shift_mprint_setting_update.php"
		document.mainform.submit();
	}
    // テーブル行の削除
    function remove_table_row(obj) {
	    var tr = obj.parentNode.parentNode;
	    tr.parentNode.deleteRow(tr.sectionRowIndex);
    }

    // テーブル行の追加
    // obj ボタン、flg 0:見出し行 1:データ行
    function add_table_row(obj, flg) {
        var table = document.getElementById("container");
        if (flg == 0) {
            var idx = 1;
        }
        else {
            var idx = obj.parentNode.parentNode.rowIndex + 1;
        }

        var row = table.insertRow(idx);
        row.className = 'mprintrow';
        row.style.cursor = 'move';
        row.style.height = '26';
        var cell01 = row.insertCell(0);
        var cell02 = row.insertCell(1);
        var cell03 = row.insertCell(2);
        var cell04 = row.insertCell(3);
        var cell05 = row.insertCell(4);
        var cell06 = row.insertCell(5);

        cell01.innerHTML = '-';
        cell02.innerHTML = '<input type="text" name="layout_name[]" size="50" maxlength="60" style="ime-mode:active;" value=""><input type="hidden" name="layout_id[]" value="-1">';
        cell03.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#C0C0C0">【帳票定義】</font>';
        cell04.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#C0C0C0">【帳票表示】</font>';
        cell05.innerHTML = '<button type="button" onclick="add_table_row(this,1);">追加</button>';
        cell06.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';

        cell01.align = 'center';
        cell03.align = 'center';
        cell04.align = 'center';
        cell05.align = 'center';
        cell06.align = 'center';

        cell01.width = '50';
        cell02.width = '340';
        cell03.width = '100';
        cell04.width = '100';
        cell05.width = '75';
        cell06.width = '75';

    }
    //帳票定義画面
    function openLayout(layout_id) {

	    wx = 820;
	    wy = 750;
	    dx = screen.width;
	    dy = screen.top;
	    base = screen.height / 2 - (wy / 2);
	    var url = 'work_admin_csv_layout.php';
	    url += '?session=<?=$session?>';
	    url += '&layout_id='+layout_id;
	    url += '&from_flg=2';
	    childwin_print = window.open(url, 'printLayoutPopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	    childwin_print.focus();

    }
    //帳票表示子画面
    function openPreview(layout_id) {

        base_left = 0;
        base = 0;
        wx = window.screen.availWidth;
        wy = 280;
	    var url = 'duty_shift_mprint_setting_preview.php';
	    url += '?session=<?=$session?>';
	    url += '&layout_id='+layout_id;
	    childwin_preview = window.open(url, 'printPreviewPopup', 'left='+base_left +',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	    childwin_preview.focus();

    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list, .list td {border:#5279a5 solid 1px;}
tr.mprintrow {cursor:move;}
tr.droptarget {background-color:#ffff99; height:60px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?
// 画面遷移
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_manage_title($session, $section_admin_auth);			//duty_shift_common.ini
echo("</table>\n");

// タブ
$arr_option = "&group_id=" . $group_id;
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_manage_tab_menuitem($session, $fname, $arr_option);	//duty_shift_manage_tab_common.ini
echo("</table>\n");

// 下線
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 表 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<table width="400" border="0" cellspacing="0" cellpadding="2">
        <tr>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        全てのシフトグループを集計できる職員</font></td>
		<td align="right"><input type="button" value="更新" onclick="editData();" ></td>
        </tr>
		</table>
		<table width="400" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr>
		<td width="250" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        職員
        </font></td>
		<td width="75" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input type="button" value="追加" onclick="addEmp(-1);">
        </font></td>
		<td width="75" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除
        </font></td>
        </tr>
<?
for ($i=0; $i<count($staff_array); $i++) {
    $wk_id = $staff_array[$i]["id"];
    
    echo("<tr>");
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$staff_array[$i]["name"]}</font></td> \n");
    // 追加／削除
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    echo("<input type=\"button\" value=\"追加\" onclick=\"addEmp($i);\"> \n");
    echo("</font></td> \n");
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    echo("<input type=\"button\" value=\"削除\" onclick=\"delEmp('$wk_id');\"> \n");
    echo("</font></td> \n");
    
    echo("</tr>\n");
}
?>
		</table>
       
		<!-- ------------------------------------------------------------------------ -->
		<!-- 一覧（見出し） -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr height="24">
		<td colspan="6" style="text-align:right;border:0;"><input type="button" value="更新" onclick="editData();" ></td>
		</tr>
        </table>
		<table width="750" id="container" class="list" border="0" cellspacing="0" cellpadding="0">
		<thead>

<?

echo("<tr height=\"24\"> \n");
echo("<td width=\"50\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">表示順</font></td> \n");
echo("<td width=\"340\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">帳票名称</font></td> \n");

echo("<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">【帳票定義】</font></td> \n");
echo("<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">【帳票表示】</font></td> \n");
// 追加／削除
echo("<td width=\"75\" align=\"center\" >");
echo("<input type=\"button\" value=\"追加\" onclick=\"add_table_row(this, 0);\"> \n");
echo("</td> \n");
echo("<td width=\"75\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除");
echo("</font></td> \n");
echo("</tr> \n");

echo("</thead>\n");

for($i=0; $i<count($arr_layout_mst); $i++) {
    echo("<tr class=\"mprintrow\" height=\"26\"> \n");
    
    echo "<input type=\"hidden\" name=\"layout_id[]\" value=\"".$arr_layout_mst[$i]['layout_id']."\">"; // テーブルのDrap&Drop並べ替え対応
    // 表示順
    echo("<td width=\"50\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    $disp_num = $i + 1;
    echo ( $disp_num ); // テーブルのDrap&Drop並べ替え対応。選択式から連番を表示するだけに変更
    echo("</font></td>\n");
    
    // 帳票名称
    echo("<td width=\"340\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    echo("<input type=\"text\" name=\"layout_name[]\" size=\"50\" maxlength=\"60\" style=\"ime-mode:active;\" value=\"");
    echo($arr_layout_mst[$i]['layout_name']);
    echo("\"></font></td>\n");
    echo("<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    //帳票定義
    echo("<a href=\"javascript:void(0);\" onclick=\"openLayout('{$arr_layout_mst[$i]['layout_id']}');\">【帳票定義】</a>");
    echo("</font></td>\n");
    echo("<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    //帳票表示
    echo("<a href=\"javascript:void(0);\" onclick=\"openPreview('{$arr_layout_mst[$i]['layout_id']}');\">【帳票表示】</a>");
    echo("");
    
    echo("</font></td>\n");
    echo("<td width=\"75\" align=\"center\" >");
    echo("<button type=\"button\" onclick=\"add_table_row(this, 1);\">追加</button> \n");
    echo("</td>\n");
    echo("<td width=\"75\" align=\"center\" >");
    echo("<button type=\"button\" onclick=\"remove_table_row(this);\">削除</button> \n");
    
    echo("</td> \n");
    echo("</tr> \n");
}
echo("</table> \n");
 		?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「更新」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
<table width="750" border="0" cellspacing="0" cellpadding="0">
		<tr height="24"><td colspan="6" style="text-align:right;border:0;">
<?
        echo("<input type=\"button\" value=\"更新\" onclick=\"editData();\">\n");
		?>
		</td></tr>
        </table>


		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="emp_id_array" value="<? show_emp_id_array($staff_array); ?>">
		<input type="hidden" name="add_idx" value="<? echo($add_idx); ?>">
		<input type="hidden" name="del_emp_id" value="<? echo($del_emp_id); ?>">
		<input type="hidden" name="add_emp_id_array" value="<? echo($add_emp_id_array); ?>">
		<input type="hidden" name="reload_flg" value="<? echo($reload_flg); ?>">
</form>

</td>
</tr>
</table>

<div id="comment">
<p>※順番はドラッグ＆ドロップで入れ替えられます。</p>

</div>

</body>
<? pg_close($con); ?>
</html>

<?
///-----------------------------------------------------------------------------
// show_emp_id_array
///-----------------------------------------------------------------------------
function show_emp_id_array($staff_array) {
    for ($i = 0; $i < count($staff_array); $i++) {
        if ($i > 0) {
            echo(",");
        }
        echo($staff_array[$i]["id"]);
    }
}
?>