<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// メール送信グループ一覧を配列に格納
$groups = array();
$sql = "select id, name from adbkgrp";
$cond = "where emp_id = '$emp_id' order by id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
while ($row = pg_fetch_array($sel)) {
    $groups[$row["id"]] = $row["name"];
}

// 初期表示時に表示するグループを設定
if ($group_id == "" && count($groups) > 0) {
    $group_id = key($groups);
}
if ($group_id == "") {
    $group_id = 0;
}

// メール送信グループ情報を配列に格納
$sql = "select case when m.type = '1' or m.type = '2' then m.emp_id
                    when m.type = '3' or m.type = '4' then cast(m.address_id as text)
                    end as id,
               case when m.type = '1' or m.type = '2' then trim(e.emp_lt_nm || ' ' || e.emp_ft_nm)
                    when m.type = '3' or m.type = '4' then trim(a.name1     || ' ' || a.name2)
                    end as name,
               case m.type when '1' then get_mail_login_id(m.emp_id)
                           when '2' then e.emp_email2
                           when '3' then a.email_pc
                           when '4' then a.email_mobile
                    end as mail,
               m.type
        from adbkgrpmem m
        left join empmst  e on m.emp_id = e.emp_id
        left join authmst u on e.emp_id = u.emp_id
        left join address a on m.address_id = a.address_id";
$cond = "where m.adbkgrp_id = $group_id
           and (not u.emp_del_flg or u.emp_del_flg is null)
         order by m.order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$addresses = array();
while ($row = pg_fetch_array($sel)) {
    $addresses[$row["id"] . ":" . $row["type"]] = array("name" => $row["name"], "mail" => $row["mail"]);
}

// アドレス帳管理権限を取得
$adbk_admin_auth = check_authority($session, 64, $fname);

pg_close($con);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix アドレス帳 | グループ作成</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var childwin = null;
function openEmployeeList() {
    var dx = screen.availWidth - 10;
    var wx = 720;
    var url = 'emplist_popup.php?session=<?=$session?>&mode=address_group';
    childwin = window.open(url, 'list', 'left='+(dx-wx)+',top=0,width='+wx+',height=600,scrollbars=yes,resizable=yes');
    childwin.focus();
}

function openAddressList() {
    var dx = screen.availWidth - 10;
    var wx = 720;
    var url = 'emplist/addresslist.php?session=<?=$session?>&mode=address_group';
    childwin = window.open(url, 'list', 'left='+(dx-wx)+',top=0,width='+wx+',height=600,scrollbars=yes,resizable=yes');
    childwin.focus();
}

function closeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

function addEmployees(emp_id, emp_name, mail_address, webmail) {
    var emp_ids = emp_id.split(', ');
    var emp_names = emp_name.split(', ');
    var mail_addresses = mail_address.split(', ');
    var mail_type = (webmail == '1') ? '1' : '2';

    for (var i = 0, j = emp_ids.length; i < j; i++) {
        var in_group = false;
        var tmp_address_id = emp_ids[i] + ':' + mail_type;
        for (var k = 0, l = document.mainform.addresses.options.length; k < l; k++) {
            if (document.mainform.addresses.options[k].value == tmp_address_id) {
                in_group = true;
                break;
            }
        }
        if (!in_group) {
            var tmp_address_name = emp_names[i] + ' <' + mail_addresses[i] + '>';
            addOption(document.mainform.addresses, tmp_address_id, tmp_address_name);
        }
    }
    setAddressesIds();
}

function addAddress(address_id, name, mail_address, type) {
    var mail_type = (type == '1') ? '3' : '4';

    var in_group = false;
    var tmp_address_id = address_id + ':' + mail_type;
    for (var k = 0, l = document.mainform.addresses.options.length; k < l; k++) {
        if (document.mainform.addresses.options[k].value == tmp_address_id) {
            in_group = true;
            break;
        }
    }
    if (!in_group) {
        var tmp_name = name + ' <' + mail_address + '>';
        addOption(document.mainform.addresses, tmp_address_id, tmp_name);
    }
    setAddressesIds();
}

function setAddressesIds() {
    var addresses = [];
    for (var i = 0, j = document.mainform.addresses.options.length; i < j; i++) {
        addresses.push(document.mainform.addresses.options[i].value);
    }
    document.mainform.addresses_ids.value = addresses.join(',');
//    alert(document.mainform.addresses_ids.value);
}

function raiseAddress() {
    var addresses = document.mainform.addresses;
    var selectedIndex = addresses.selectedIndex;
    if (selectedIndex < 1) {
        return;
    }

    selectedValue = addresses.options[selectedIndex].value;
    selectedText  = addresses.options[selectedIndex].text;

    upperValue = addresses.options[selectedIndex - 1].value;
    upperText  = addresses.options[selectedIndex - 1].text;

    addresses.options[selectedIndex - 1].value    = selectedValue;
    addresses.options[selectedIndex - 1].text     = selectedText;
    addresses.options[selectedIndex - 1].selected = true;

    addresses.options[selectedIndex].value    = upperValue;
    addresses.options[selectedIndex].text     = upperText;
    addresses.options[selectedIndex].selected = false;

    setAddressesIds();
}

function dropAddress() {
    var addresses = document.mainform.addresses;
    var selectedIndex = addresses.selectedIndex;
    if (selectedIndex < 0 || selectedIndex >= addresses.options.length - 1) {
        return;
    }

    selectedValue = addresses.options[selectedIndex].value;
    selectedText  = addresses.options[selectedIndex].text;

    underValue = addresses.options[selectedIndex + 1].value;
    underText  = addresses.options[selectedIndex + 1].text;

    addresses.options[selectedIndex + 1].value    = selectedValue;
    addresses.options[selectedIndex + 1].text     = selectedText;
    addresses.options[selectedIndex + 1].selected = true;

    addresses.options[selectedIndex].value    = underValue;
    addresses.options[selectedIndex].text     = underText;
    addresses.options[selectedIndex].selected = false;

    setAddressesIds();
}

function deleteAddress() {
    for (var i = document.mainform.addresses.options.length - 1; i >= 0; i--) {
        if (document.mainform.addresses.options[i].selected) {
            document.mainform.addresses.options[i] = null;
        }
    }
    setAddressesIds();
}

function deleteAllAddress() {
    deleteAllOptions(document.mainform.addresses);
    setAddressesIds();
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
    if (selected == value) {
        box.selectedIndex = box.options.length -1;
        return;
    }
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function editGroup() {
    var group_id = '';
    if (document.subform.elements['group_ids[]']) {
        if (document.subform.elements['group_ids[]'].length) {
            for (var i = 0, j = document.subform.elements['group_ids[]'].length; i < j; i++) {
                if (document.subform.elements['group_ids[]'][i].checked) {
                    group_id = document.subform.elements['group_ids[]'][i].value;
                    break;
                }
            }
        } else if (document.subform.elements['group_ids[]'].checked) {
            group_id = document.subform.elements['group_ids[]'].value;
        }
    }

    if (group_id == '') {
        alert('編集対象が選択されていません。');
        return;
    }

    window.open('address_group_update.php?session=<?php eh($session); ?>&group_id='.concat(group_id), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function registerGroup() {
    window.open('address_group_register.php?session=<?php eh($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteGroup() {
    var selected = false;
    if (document.subform.elements['group_ids[]']) {
        if (document.subform.elements['group_ids[]'].length) {
            for (var i = 0, j = document.subform.elements['group_ids[]'].length; i < j; i++) {
                if (document.subform.elements['group_ids[]'][i].checked) {
                    selected = true;
                    break;
                }
            }
        } else if (document.subform.elements['group_ids[]'].checked) {
            selected = true;
        }
    }

    if (!selected) {
        alert('削除対象が選択されていません。');
        return;
    }

    if (confirm('削除します。よろしいですか？')) {
        document.subform.submit();
    }
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="address_menu.php?session=<? echo($session); ?>"><img src="img/icon/b13.gif" width="32" height="32" border="0" alt="アドレス帳"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_menu.php?session=<? echo($session); ?>"><b>アドレス帳</b></a></font></td>
<? if ($adbk_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="address_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規作成</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="address_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_contact_update.php?session=<? echo($session); ?>&emp_id=&search_shared_flg=<? echo($search_shared_flg); ?>&search_name=<? echo($url_name); ?>&search_submit=<? echo($url_submit); ?>&page=<? echo($page); ?>&admin_flg=<? echo($admin_flg); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本人連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="address_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>グループ作成</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td>
<form name="subform" action="address_group_delete.php" method="post">
<table width="180" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
メール送信グループ<br>
<input type="button" value="編集" onclick="editGroup();">
<input type="button" value="作成" onclick="registerGroup();">
<input type="button" value="削除" onclick="deleteGroup();">
</font></td>
</tr>
<tr height="150" valign="top">
<td>
<?
if ($group_id == 0) {
    echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成されていません。</font>");
} else {
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
    foreach ($groups as $tmp_group_id => $tmp_mygruop_nm) {
        echo("<tr height=\"22\" valign=\"middle\">\n");
        echo("<td width=\"1\"><input type=\"checkbox\" name=\"group_ids[]\" value=\"$tmp_group_id\"></td>\n");
        echo("");
        if ($tmp_group_id != $group_id) {
            echo("<td style=\"padding-left:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"address_group.php?session=$session&group_id=$tmp_group_id\">$tmp_mygruop_nm</a></font></td>\n");
        } else {
            echo("<td style=\"padding-left:3px;background-color:#ffff66;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_mygruop_nm</font></td>\n");
        }
        echo("</tr>\n");
    }
    echo("</table>\n");
}
?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<?php eh($session); ?>">
</form>
</td>
<td><img src="img/spacer.gif" alt="" width="2" height="1"></td>
<td width="100%">
<form name="mainform" action="address_group_member_update_exe.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象送信先</font></td>
<td>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="2">
<td colspan="3"></td>
</tr>
<tr>
<td></td>
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象</font></td>
<td></td>
</tr>
<tr>
<td align="center" style="padding-right:2px;"><input type="button" value="↑" onclick="raiseAddress();"><br><br><input type="button" value="↓" onclick="dropAddress();"></td>
<td><select name="addresses" size="15" style="width:350px;"><? show_addresses_options($addresses); ?></select></td>
<td style="padding-left:2px;">
<input type="button" value="職員名簿" style="width:6.5em;margin-bottom:2px;" onclick="openEmployeeList();"><br>
<input type="button" value="アドレス帳" style="width:6.5em;" onclick="openAddressList();"><br>
<br>
<input type="button" value="削除" style="width:6.5em;" onclick="deleteAddress();">
</td>
</tr>
<tr height="22">
<td></td>
<td style="padding-top:2px;"><input type="button" value="全て削除" onclick="deleteAllAddress();"></td>
<td></td>
</tr>
<tr height="2">
<td colspan="3"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新" onclick="closeList();" <? if ($group_id == 0) {echo(" disabled");} ?>></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="addresses_ids" value="<? show_addresses_ids($addresses); ?>">
<input type="hidden" name="group_id" value="<?php eh($group_id); ?>">
</form>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>
<?
function show_addresses_options($addresses) {
    foreach ($addresses as $tmp_id => $tmp_address) {
        echo("<option value=\"{$tmp_id}\">{$tmp_address["name"]} &lt;{$tmp_address["mail"]}&gt;\n");
    }
}

function show_addresses_ids($addresses) {
    echo(implode(",", array_keys($addresses)));
}
