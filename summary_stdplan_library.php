<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?
require_once("about_comedix.php");
require_once("summary_tmpl_util.ini");
require_once("summary_common.ini");

//==============================
//ページ名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//パラメータを取得
//==============================
$is_postback = isset($_POST['is_postback']) ? $_POST['is_postback'] : '';
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$postback_mode = isset($_POST['postback_mode']) ? $_POST['postback_mode'] : '';
$med_wkfw_id = isset($_GET['med_wkfw_id']) ? intval($_GET['med_wkfw_id']) : -1;

$data['problem_name'] = isset($_POST['problem_name']) ? $_POST['problem_name'] : '';
$data['def_value'] = isset($_POST['def_value']) ? replaceBreakToTag($_POST['def_value']) : '';
$data['s_detail'] = isset($_POST['s_detail']) ? replaceBreakToTag($_POST['s_detail']) : '';
$data['e_detail'] = isset($_POST['e_detail']) ? replaceBreakToTag($_POST['e_detail']) : '';
$data['risk_detail'] = isset($_POST['risk_detail']) ? replaceBreakToTag($_POST['risk_detail']) : '';
$data['outcome'] = isset($_POST['outcome']) ? replaceBreakToTag($_POST['outcome']) : '';
$data['kyoyo_op'] = isset($_POST['kyoyo_op']) ? replaceBreakToTag($_POST['kyoyo_op']) : '';
$data['kyoyo_tp'] = isset($_POST['kyoyo_tp']) ? replaceBreakToTag($_POST['kyoyo_tp']) : '';
$data['kyoyo_ep'] = isset($_POST['kyoyo_ep']) ? replaceBreakToTag($_POST['kyoyo_ep']) : '';
$data['kango_op'] = isset($_POST['kango_op']) ? replaceBreakToTag($_POST['kango_op']) : '';
$data['kango_tp'] = isset($_POST['kango_tp']) ? replaceBreakToTag($_POST['kango_tp']) : '';
$data['kango_ep'] = isset($_POST['kango_ep']) ? replaceBreakToTag($_POST['kango_ep']) : '';
$data['kaigo_op'] = isset($_POST['kaigo_op']) ? replaceBreakToTag($_POST['kaigo_op']) : '';
$data['kaigo_tp'] = isset($_POST['kaigo_tp']) ? replaceBreakToTag($_POST['kaigo_tp']) : '';
$data['kaigo_ep'] = isset($_POST['kaigo_ep']) ? replaceBreakToTag($_POST['kaigo_ep']) : '';
$data['rihabiri_op'] = isset($_POST['rihabiri_op']) ? replaceBreakToTag($_POST['rihabiri_op']) : '';
$data['rihabiri_tp'] = isset($_POST['rihabiri_tp']) ? replaceBreakToTag($_POST['rihabiri_tp']) : '';
$data['rihabiri_ep'] = isset($_POST['rihabiri_ep']) ? replaceBreakToTag($_POST['rihabiri_ep']) : '';
$data['usable'] = isset($_POST['usable']) ? $_POST['usable'] : 'f';
$data['observation_items'] = isset($_POST['observation_items']) ? $_POST['observation_items'] : array();

//==============================
// 権限チェック
//==============================
$auth = ($mode == 'detail' ) ? 57 : 59;
$summary = check_authority($session, $auth, $fname);
if ($summary == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$submit_name = ($mode == 'update') ? '更新' : '登録';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ポストバック時の処理
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ($is_postback == 'true') {
    if ($postback_mode == 'save') {    // 保存
        if ($mode == 'insert') {
            // 登録
            $result = insertStandardPlanLibraryData($con, $data, $fname);
        } elseif ($mode == 'update') {
            // 更新
            $result = updateStandardPlanLibraryData($con, $med_wkfw_id, $data, $fname);
        }
        if ($result === false) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
    
    echo "<script language='javascript'>";
    echo "if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}";
    echo "window.close();";
    echo "</script>";
    pg_close($con);
    exit;
} elseif ($mode != 'insert') {
    if (($data = getEachStandardPlanLibraryData($con, $med_wkfw_id, $fname)) === false) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if ($mode == 'update') {
        // <br>タグを改行コードに置換する
        $keys = array('def_value', 's_detail', 'e_detail', 'risk_detail', 'outcome',
                'kyoyo_op', 'kyoyo_tp', 'kyoyo_ep',
                'kango_op', 'kango_tp', 'kango_ep',
                'kaigo_op', 'kaigo_tp', 'kaigo_ep',
                'rihabiri_op', 'rihabiri_tp', 'rihabiri_ep');
        foreach ($keys as $key) {
            $data[$key] = replaceTagToBreak($data[$key]);
        }
        $observationItemsHTML = createObservationItemsHTML($data['observation_items']);
    } elseif ($mode == 'detail') {
        $observationItemsHTML = createObservationItemsHTML($data['observation_items'], false);
    }
}

pg_close($con);    // DB切断

/**
 * 標準計画ライブラリ個別データを取得する
 *
 * @param resource $con         データベース接続リソース
 * @param integer  $med_wkfw_id ワークフローID
 * @param string   $fname       ページ名
 *
 * @return mixed 成功時:取得したレコードの配列/結果の行数が 0だった場合、又はその他のエラーが発生:false
 */
function getEachStandardPlanLibraryData($con, $med_wkfw_id, $fname)
{
    // 標準チーム計画ライブラリ・ワークフローマスターテーブルから取得
    $sql = "select * from sum_med_std_wkfwmst where med_wkfw_id = {$med_wkfw_id}";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        return false;
    }
    $data = pg_fetch_assoc($sel);

    // 標準チーム計画ライブラリ・ワークフロー・観察項目テーブルから取得
    $sql = "select item_cd, item_name from tmplitem " .
            "where mst_cd = 'A263' and item_cd in (" .
            "select item_cd from sum_med_std_wkfwobse where med_wkfw_id = {$med_wkfw_id}) " .
            "order by item_cd";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        return false;
    }

    $observation_items = pg_fetch_all($sel);
    $data['observation_items'] = $observation_items;

    return $data;
}

/**
 * 標準計画ライブラリデータを新規登録する
 *
 * @param resource $con   データベース接続リソース
 * @param array    $data  登録するデータ
 * @param string   $fname ページ名
 *
 * @return boolean 成功時:true/失敗時:false
 */
function insertStandardPlanLibraryData($con, $data, $fname)
{
    // トランザクションの開始
    pg_query($con, "begin");

    // ワークフローIDの採番
    $sql = "select case when max(med_wkfw_id) isnull then 1 else max(med_wkfw_id)+1 end as new_med_wkfw_id from sum_med_std_wkfwmst";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        return false;
    }
    $med_wkfw_id = pg_fetch_result($sel, 0, 'new_med_wkfw_id');
    $usable = $data['usable'] == '可' ? 'true' : 'false';

    // 標準チーム計画ライブラリ・ワークフローマスターテーブルへの登録
    $sql = "insert into sum_med_std_wkfwmst (" .
            "med_wkfw_id," .
            "problem_name," .
            "create_date," .
            "def_value," .
            "s_detail," .
            "e_detail," .
            "risk_detail," .
            "outcome," .
            "kyoyo_op," .
            "kyoyo_tp," .
            "kyoyo_ep," .
            "kango_op," .
            "kango_tp," .
            "kango_ep," .
            "kaigo_op," .
            "kaigo_tp," .
            "kaigo_ep," .
            "rihabiri_op," .
            "rihabiri_tp," .
            "rihabiri_ep," .
            "usable" .
            ") values (" .
            $med_wkfw_id . "," .
            "'" . pg_escape_string($data['problem_name']) . "'," .
            "current_timestamp," .
            "'" . pg_escape_string($data['def_value']) . "'," .
            "'" . pg_escape_string($data['s_detail']) . "'," .
            "'" . pg_escape_string($data['e_detail']) . "'," .
            "'" . pg_escape_string($data['risk_detail']) . "'," .
            "'" . pg_escape_string($data['outcome']) . "'," .
            "'" . pg_escape_string($data['kyoyo_op']) . "'," .
            "'" . pg_escape_string($data['kyoyo_tp']) . "'," .
            "'" . pg_escape_string($data['kyoyo_ep']) . "'," .
            "'" . pg_escape_string($data['kango_op']) . "'," .
            "'" . pg_escape_string($data['kango_tp']) . "'," .
            "'" . pg_escape_string($data['kango_ep']) . "'," .
            "'" . pg_escape_string($data['kaigo_op']) . "'," .
            "'" . pg_escape_string($data['kaigo_tp']) . "'," .
            "'" . pg_escape_string($data['kaigo_ep']) . "'," .
            "'" . pg_escape_string($data['rihabiri_op']) . "'," .
            "'" . pg_escape_string($data['rihabiri_tp']) . "'," .
            "'" . pg_escape_string($data['rihabiri_ep']) . "'," .
            "'$usable'" .
            ")";
    $in = insert_into_table_no_content($con, $sql, $fname);
    if(!$in){
        pg_query($con,"rollback");
        return false;
    }

    // 標準チーム計画ライブラリ・ワークフロー・観察項目テーブルへの登録
    $observation_items = $data['observation_items'];
    foreach($observation_items as $item_cd) {
        $sql = "insert into sum_med_std_wkfwobse (med_wkfw_id, item_cd) values (" .
                $med_wkfw_id . "," . "'{$item_cd}')";
        $in = insert_into_table_no_content($con, $sql, $fname);
        if($in == 0){
            pg_query($con,"rollback");
            return false;
        }
    }

    // トランザクションのコミット
    pg_query($con, "commit");
    return true;
}

/**
 * 標準計画ライブラリデータを更新する
 *
 * @param resource $con         データベース接続リソース
 * @param integer  $med_wkfw_id ワークフローID
 * @param array    $data        更新するデータ
 * @param string   $fname       ページ名
 *
 * @return boolean 成功時:true/失敗時:false
 */
function updateStandardPlanLibraryData($con, $med_wkfw_id, $data, $fname)
{
    // トランザクションの開始
    pg_query($con, "begin");

    // 標準チーム計画ライブラリ・ワークフローマスターテーブルテーブル更新
    $usable = $data['usable'] == '可' ? 'true' : 'false';
    $sql = "update sum_med_std_wkfwmst set";
    $set = array(
            'problem_name',
            'def_value',
            's_detail',
            'e_detail',
            'risk_detail',
            'outcome',
            'kyoyo_op',
            'kyoyo_tp',
            'kyoyo_ep',
            'kango_op',
            'kango_tp',
            'kango_ep',
            'kaigo_op',
            'kaigo_tp',
            'kaigo_ep',
            'rihabiri_op',
            'rihabiri_tp',
            'rihabiri_ep',
            'usable'
    );
    $setvalue = array(
            pg_escape_string($data['problem_name']),
            pg_escape_string($data['def_value']),
            pg_escape_string($data['s_detail']),
            pg_escape_string($data['e_detail']),
            pg_escape_string($data['risk_detail']),
            pg_escape_string($data['outcome']),
            pg_escape_string($data['kyoyo_op']),
            pg_escape_string($data['kyoyo_tp']),
            pg_escape_string($data['kyoyo_ep']),
            pg_escape_string($data['kango_op']),
            pg_escape_string($data['kango_tp']),
            pg_escape_string($data['kango_ep']),
            pg_escape_string($data['kaigo_op']),
            pg_escape_string($data['kaigo_tp']),
            pg_escape_string($data['kaigo_ep']),
            pg_escape_string($data['rihabiri_op']),
            pg_escape_string($data['rihabiri_tp']),
            pg_escape_string($data['rihabiri_ep']),
            $usable
    );
    $cond = "where med_wkfw_id = {$med_wkfw_id}";

    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if (!$upd) {
        pg_query($con, "rollback");
        return false;
    }

    // 標準チーム計画ライブラリ・ワークフロー・観察項目更新
    // まず、ワークフローIDに該当する観察項目を削除
    $sql = "delete from sum_med_std_wkfwobse where med_wkfw_id = '$med_wkfw_id'";
    $del = delete_from_table($con, $sql, "", $fname);
    if ($del == 0)
    {
        pg_query($con,"rollback");
        return false;
    }

    // 選択された観察項目を登録しなおす
    $observation_items = $data['observation_items'];
    foreach($observation_items as $item_cd) {
        $sql = "insert into sum_med_std_wkfwobse (med_wkfw_id, item_cd) values (" .
                $med_wkfw_id . "," . "'{$item_cd}')";
        $in = insert_into_table_no_content($con, $sql, $fname);
        if($in == 0){
            pg_query($con,"rollback");
            return false;
        }
    }

    // トランザクションのコミット
    pg_query($con, "commit");
    return true;
}

/**
 * 観察項目HTMLコードを生成する
 *
 * @param array   $observation_items 観察項目
 * @param boolean $isShowCheckBox    チェックボックス表示の有無
 *
 * @return string 観察項目HTMLコード
 */
function createObservationItemsHTML($observation_items, $isShowCheckBox = true)
{
    foreach ($observation_items as $value) {
        $observationItemsHTML .= '<div id="' . $value['item_cd'] . '_div">';
        if ($isShowCheckBox) {
            $observationItemsHTML .= '<input type="checkbox" class="observation_item" id="' . $value['item_cd'] . '" name="' . $value['item_cd'] . '">';
        }
        $observationItemsHTML .= $value['item_name'] . '</div>';
    }
    return $observationItemsHTML;
}

/**
 * 文字列内の<BR>タグを改行コードに変換する
 *
 * @param string $string 変換対象文字列
 *
 * @return string 変換した文字列
 */
function replaceTagToBreak($string)
{
    return preg_replace('/<BR>/i', "\n", $string);
}

/**
 * 文字列内の改行コードを<BR>タグに変換する
 *
 * @param string $string 変換対象文字列
 *
 * @return string 変換した文字列
 */
function replaceBreakToTag($string)
{
    return str_replace(array("\r\n", "\r", "\n"), '<br>', $string);
}

/**
 * 各データのHTMLを生成
 * 
 * 閲覧時は<div>、編集時は<textarea>を使用
 * 
 * @param array  $data 表示データ
 * @param string $id   エレメントID
 * @param string $mode 処理モード
 * 
 * @return 生成したHTML
 */
function createEachDataHTML($data, $id, $mode)
{
    // <br>以外のHTMLタグをエスケープ(DBに登録されているデータの改行位置に<br>タグを利用しているので)
    $escaped = strip_tags($data[$id], '<br>');
    if ($mode == 'detail') {
        if ($escaped == '') {
            return "<div class=\"detail\" style=\"height: 2px;\"></div>";;
        }
        return "<div class=\"detail\" style=\"width: 285px;\">{$escaped}</div>";
    } else {
        return "<textarea id=\"{$id}\" name=\"{$id}\" onkeyup=\"adjustTextAreaHeight(this.id, 2);\" onchange=\"adjustTextAreaHeight(this.id, 2);\">{$escaped}</textarea><br>";
    }
}
?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<style type="text/css">
textarea {
  width: 280px;
  border-color:#9bc8ec;
  border-style: solid;
  border-width: 1px;
  resize: none;
  overflow: hidden;
  ime-mode: active;
}

div.detail {
  border-style: solid;
  border-width:1px;
  border-color: #9bc8ec;
  background-color: white;
}

.observation_item {}
</style>
<title>CoMedix メドレポート｜標準看護計画作成</title>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/adjustTextAreaHeight.js"></script>
<script type="text/javascript">
/****************************************************************
 * popupObservationList()
 * 要素をアイテム要素オブジェクトに追加する
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function popupObservationList() {
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=400,height=300";
    window.open('summary_observation_item_list.php?session=<? echo $session; ?>', 'observationWin', option);
}

/****************************************************************
 * deleteObservationItem()
 * 観察項目を削除する
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function deleteObservationItem() {
    // 観察項目を取得
    var elements = document.getElementsByClassName('observation_item');
    var observation = document.getElementById('left_observation');
    var length = elements.length;
    var innerHTMLNew = '';
    for (var i = 0; i < length; i++) {
        var id = elements[i].id;
        var check = document.getElementById(id);
        if (check.checked) {
            continue;
        }
        innerHTMLNew += '<div id="' + id + '_div">' + document.getElementById(id + '_div').innerHTML + '</div>';
    }
    observation.innerHTML = innerHTMLNew;
}

/****************************************************************
 * save()
 * 保存(insert/update)
 *
 * 引　数：mode モード
 *         med_wkfw_id ワークフローID
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function save(mode, med_wkfw_id) {
    // 入力チェック
    if (document.standardform.problem_name.value == '') {
        alert('プロブレムが未入力です。');
        return;
    }

    // 観察一覧の項目を取得
    var elements = document.getElementsByClassName('observation_item');
    for (var i = 0; i < elements.length; i++) {
        var tag = document.createElement('input');
        tag.setAttribute('type', 'hidden');
        tag.setAttribute('name', 'observation_items[' + i + ']');
        tag.setAttribute('value', elements[i].id);
        document.standardform.appendChild(tag);
    }
    
    document.standardform.postback_mode.value = "save";
    document.standardform.action += '?mode=' + mode;
    if (mode == 'update') {
        document.standardform.action += '&med_wkfw_id=' + med_wkfw_id;
    }

    // データを更新(このスクリプトにsubmit)
    document.standardform.submit();
}

window.onload = function() {
    var dmy = document.getElementById("textarea_dummy");
    if (!dmy) {
        return;
    }
    var textAreaIdArray = new Array('def_value', 's_detail', 'e_detail', 'risk_detail', 'outcome',
                                    'kyoyo_op', 'kyoyo_tp', 'kyoyo_ep',
                                    'kango_op', 'kango_tp', 'kango_ep',
                                    'kaigo_op', 'kaigo_tp', 'kaigo_ep',
                                    'rihabiri_op', 'rihabiri_tp', 'rihabiri_ep');
    var length = textAreaIdArray.length;
    for (var i = 0; i < length; i++) {
        adjustTextAreaHeight(textAreaIdArray[i], 2);
    }
    iframeResize();
}

/****************************************************************
 * iframeResize()
 * スクロールバーが出ないようにインラインフレームの高さを調節する
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function iframeResize() {
    var parent = window.parent.document.getElementById('stdplan_detail');
    if (!parent) {
        return;
    }
    var pageHight = document.body.scrollHeight + 30; // ページの高さを取得
    parent.style.height = pageHight + 'px'; // iframeの高さを変更
}

/****************************************************************
 * reload_page()
 * リロード
 *
 * 引　数：med_wkfw_id ワークフローID
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function reload_page(med_wkfw_id)
{
  location.href = "summary_stdplan_library.php?session=<? echo $session; ?>&mode=detail&med_wkfw_id=" + med_wkfw_id;
}
</script>
</head>

<body>
<textarea id="textarea_dummy" rows="2" onkeyup="tx();" onchange="tx();"
style="overflow:hidden; position:absolute; left:0; top:-2000px; display:none;"></textarea>
<div style="padding:4px">
<form name="standardform" action="summary_stdplan_library.php" method="post">
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">

<? if ($mode != 'detail') { ?>
<!-- ヘッダエリア -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr style="background-color:#4baaf8;">
    <td style="font-size:14px; color:#fff; padding:4px; letter-spacing:1px"><b>標準看護計画作成</b></td>
    <td align="right">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="right"><nobr>
            <input id="Button1" type="button" value="<? echo $submit_name; ?>" onclick="save('<? echo $mode; ?>', <? echo $med_wkfw_id; ?>);">
          </nobr>
          </td>
        </tr>
      </table>
    </td>
    <td width="10">&nbsp;</td>
    <td width="32" align="center">
      <a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a>
    </td>
  </tr>
</table>
<? } ?>
<table width="550" class="list_table" style="margin-top:4px">
  <tr>
    <td>
<!-- inner table-->
<!-- 冒頭部 -->
      <table width="580" border="0">
        <tr>
          <td width="80" style="font-weight: bold;">&nbsp;&nbsp;プロブレム</td>
          <td width="280">
<? if ($mode == 'detail') { ?>
            <div class="detail" style="width: 400px;"><? echo !empty($data['problem_name']) ? h($data['problem_name']) : '&nbsp;'; ?></div>
<? } else { ?>
            <input type="text" style="width: 200px; ime-mode: active;" id="problem_name" name="problem_name" value="<? echo h($data['problem_name']); ?>" maxlength="120">
<? } ?>
          </td>
<? if ($mode != 'detail') { ?>
          <td align="right">
            利用区分
            <input type="radio" id="usable_ok" name="usable" value="可" <? echo $data['usable'] == 't' ? 'checked' : ''; ?>>可
            <input type="radio" id="usable_ng" name="usable" value="不可" <? echo $data['usable'] == 'f' ? 'checked' : ''; ?>>不可
          </td>
<? } ?>
        </tr>
      </table>

      <table class="list_table" style="width: 580px; margin-top:10px; margin-top:10px; margin-left: 11px; margin-bottom: 11px;">
        <tr>
          <th style="width: 285px;" align="center">患者の問題及びアウトカム</th>
          <th style="width: 285px;" align="center">チーム実践</th>
        </tr>

        <tr>
<!-- 患者の問題及びアウトカムstart -->
          <td valign="top" bgcolor="#efefef">
            <table>
              <tr>
                <td bgcolor="#efefef">定義</td>
              </tr>
            </table>
            <? echo createEachDataHTML($data, 'def_value', $mode); ?>
            <br>
            S:症状・兆候<br>
            <? echo createEachDataHTML($data, 's_detail', $mode); ?>
            <br>
            E:原因・関連因子<br>
            <? echo createEachDataHTML($data, 'e_detail', $mode); ?>
            <br>
            危険因子<br>
            <? echo createEachDataHTML($data, 'risk_detail', $mode); ?>
            <br>
            アウトカム<br>
            <? echo createEachDataHTML($data, 'outcome', $mode); ?>
            <br>
            観察項目&nbsp;
<? if ($mode != 'detail') {?>
            <input type="button" value="行追加" onclick="popupObservationList();">&nbsp;<input type="button" value="削除" onclick="deleteObservationItem();">
<? } ?>
            <div style="width: 285px;">
            <table class="list_table" style="margin-top:4px">
              <tr>
                <td bgcolor="#fafafa" id="left_observation"><? echo $observationItemsHTML; ?></td>
              </tr>
            </table>
            </div>
            <br><br>
          </td>
<!-- 患者の問題及びアウトカムend -->
<!-- チーム実践start -->
          <td valign="top" bgcolor="#ffdfdf">
            <table width="100%" border="0">
              <tr>
                <td bgcolor="#ffdfdf">
<!-- 拡張機能有効時は「共用」と記入欄エリア名を表示する
                  ＜共用＞<br>
-->
                  O-P:観察計画<br>
                  <? echo createEachDataHTML($data, 'kyoyo_op', $mode); ?>
                  <br>
                  T-P:実践計画<br>
                  <? echo createEachDataHTML($data, 'kyoyo_tp', $mode); ?>
                  <br>
                  E-P:教育・指導計画<br>
                  <? echo createEachDataHTML($data, 'kyoyo_ep', $mode); ?>
                  <br>
                </td>
              </tr>
            </table>
<!-- 拡張機能start -->
<!--
            <table width="100%" border="0">
              <tr>
                <td bgcolor="#dfffdf">
                  ＜看護＞<br>
                  O-P:観察計画<br>
                  <? echo createEachDataHTML($data, 'kango_op', $mode); ?>
                  <br>
                  T-P:実践計画<br>
                  <? echo createEachDataHTML($data, 'kango_tp', $mode); ?>
                  <br>
                  E-P:教育・指導計画<br>
                  <? echo createEachDataHTML($data, 'kango_ep', $mode); ?>
                  <br>
                </td>
              </tr>
            </table>

            <table width="100%" border="0">
              <tr>
                <td bgcolor="#dfdfff">
                  ＜介護＞<br>
                  O-P:観察計画<br>
                  <? echo createEachDataHTML($data, 'kaigo_op', $mode); ?>
                  <br>
                  T-P:実践計画<br>
                  <? echo createEachDataHTML($data, 'kaigo_tp', $mode); ?>
                  <br>
                  E-P:教育・指導計画<br>
                  <? echo createEachDataHTML($data, 'kaigo_ep', $mode); ?>
                  <br>
                </td>
              </tr>
            </table>

            <table width="100%" border="0">
              <tr>
                <td bgcolor="#ffffdf">
                  ＜リハビリ＞<br>
                  O-P:観察計画<br>
                  <? echo createEachDataHTML($data, 'rihabiri_op', $mode); ?>
                  <br>
                  T-P:実践計画<br>
                  <? echo createEachDataHTML($data, 'rihabiri_tp', $mode); ?>
                  <br>
                  E-P:教育・指導計画<br>
                  <? echo createEachDataHTML($data, 'rihabiri_ep', $mode); ?>
                  <br>
                </td>
              </tr>
            </table>
-->
<!-- 機能拡張end -->
          </td>
<!-- チーム実践end -->
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>
