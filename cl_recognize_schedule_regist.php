<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_title_name.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

//====================================
// 登録更新処理
//====================================
// 2011.12.12 Add by matsuura
if($nurse_type == ""){
	$nurse_type = "1";
}

if($mode == "REGIST")
{
	// トランザクションを開始
	pg_query($con, "begin");

	// 2011.12.06 Edit by matsuura
//    $arr_recognize_schedule = $obj->get_cl_recognize_schedule($apply_id);
	$arr_recognize_schedule = $obj->get_cl_recognize_schedule2($apply_id);

//	echo("=====> post = " . print_r($_POST));

	// 既に登録データが存在する場合
	if(count($arr_recognize_schedule) == 0)
    {
    	// 2011.12.07 Edit by matsuura
//		regist_schedule($obj, $_POST);
		regist_schedule($obj, $_POST, $nurse_type);
    }
    else
	{
    	// 2011.12.07 Edit by matsuura
//		update_schedule($obj, $_POST);
		update_schedule($obj, $_POST, $nurse_type);
	}

	// トランザクションをコミット
	pg_query($con, "commit");


	echo("<script type=\"text/javascript\">");
	echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
	echo("window.close();</script>");
	exit;
}


//====================================
// 初期処理
//====================================
if($apply_id != "")
{
	// 認定スケジュール情報取得 2011.12.05 Edit by matsuura
//	$arr_recognize_schedule = $obj->get_cl_recognize_schedule($apply_id);
	$arr_recognize_schedule = $obj->get_cl_recognize_schedule2($apply_id);

	$emp_full_nm = "";

	// 既に登録データが存在する場合
	if(count($arr_recognize_schedule) > 0)
	{
		$apply_id    = $arr_recognize_schedule[0]["apply_id"];

		$apply_date  = $arr_recognize_schedule[0]["apply_date"];
        $apply_date  = ($apply_date == "") ? "" : substr($apply_date, 0, 4)."/".substr($apply_date, 4, 2)."/".substr($apply_date, 6, 2);

		$level       = $arr_recognize_schedule[0]["level"];
		$no1_date    = $arr_recognize_schedule[0]["no1_date"];
		$no2_date    = $arr_recognize_schedule[0]["no2_date"];
		$no3_date    = $arr_recognize_schedule[0]["no3_date"];
		$no4_date    = $arr_recognize_schedule[0]["no4_date"];
		$no5_date    = $arr_recognize_schedule[0]["no5_date"];
		$no6_date    = $arr_recognize_schedule[0]["no6_date"];
		$no7_date    = $arr_recognize_schedule[0]["no7_date"];
		$no8_date    = $arr_recognize_schedule[0]["no8_date"];
		$no9_date    = $arr_recognize_schedule[0]["no9_date"];
		$no10_date   = $arr_recognize_schedule[0]["no10_date"];
		$no11_date   = $arr_recognize_schedule[0]["no11_date"];

		// 2011.12.05 Add by matsuura
		$no31_date   = $arr_recognize_schedule[0]["no31_date"];
		$no32_date   = $arr_recognize_schedule[0]["no32_date"];
		$no33_date   = $arr_recognize_schedule[0]["no33_date"];
		$read_nurse  = $arr_recognize_schedule[0]["nurse_type"];

//		echo("read_nurse = " . $read_nurse . "  nurse_type = " . $nurse_type);

		$emp_lt_nm   = $arr_recognize_schedule[0]["emp_lt_nm"];
		$emp_ft_nm   = $arr_recognize_schedule[0]["emp_ft_nm"];
		$emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;

		if($level == "2")
		{
			$date_y1 = substr($no1_date, 0, 4);
			$date_m1 = substr($no1_date, 4, 2);
			$date_d1 = substr($no1_date, 6, 2);

			$date_y2 = substr($no3_date, 0, 4);
			$date_m2 = substr($no3_date, 4, 2);
			$date_d2 = substr($no3_date, 6, 2);

			$date_y4 = substr($no6_date, 0, 4);
			$date_m4 = substr($no6_date, 4, 2);
			$date_d4 = substr($no6_date, 6, 2);

			$date_y7 = substr($no10_date, 0, 4);
			$date_m7 = substr($no10_date, 4, 2);
			$date_d7 = substr($no10_date, 6, 2);

		}
		else if($level == "3")
		{
			$date_y1 = substr($no2_date, 0, 4);
			$date_m1 = substr($no2_date, 4, 2);
			$date_d1 = substr($no2_date, 6, 2);

			$date_y2 = substr($no4_date, 0, 4);
			$date_m2 = substr($no4_date, 4, 2);
			$date_d2 = substr($no4_date, 6, 2);

			$date_y4 = substr($no7_date, 0, 4);
			$date_m4 = substr($no7_date, 4, 2);
			$date_d4 = substr($no7_date, 6, 2);

			$date_y7 = substr($no11_date, 0, 4);
			$date_m7 = substr($no11_date, 4, 2);
			$date_d7 = substr($no11_date, 6, 2);
		}

		// 2011.12.05 Add by matsuura
		if($nurse_type == "2" || $nurse_type == "3"){
			$date_y31 = substr($no31_date, 0, 4);
			$date_m31 = substr($no31_date, 4, 2);
			$date_d31 = substr($no31_date, 6, 2);

			$date_y32 = substr($no32_date, 0, 4);
			$date_m32 = substr($no32_date, 4, 2);
			$date_d32 = substr($no32_date, 6, 2);

			$date_y33 = substr($no33_date, 0, 4);
			$date_m33 = substr($no33_date, 4, 2);
			$date_d33 = substr($no33_date, 6, 2);
		}

		$date_y3 = substr($no5_date, 0, 4);
		$date_m3 = substr($no5_date, 4, 2);
		$date_d3 = substr($no5_date, 6, 2);

		$date_y5 = substr($no8_date, 0, 4);
		$date_m5 = substr($no8_date, 4, 2);
		$date_d5 = substr($no8_date, 6, 2);

		// 2011.12.07 Edit by matsuura
//		$date_y6 = substr($no8_date, 0, 4);
//		$date_m6 = substr($no8_date, 4, 2);
//		$date_d6 = substr($no8_date, 6, 2);
		$date_y6 = substr($no9_date, 0, 4);
		$date_m6 = substr($no9_date, 4, 2);
		$date_d6 = substr($no9_date, 6, 2);
	}
	// 登録データが存在しない場合
	else
	{
        // 申請情報取得
        $arr_apply_workflow = $obj->get_apply_wkfwmst($apply_id);

        $emp_lt_nm = $arr_apply_workflow[0]["emp_lt_nm"];
        $emp_ft_nm = $arr_apply_workflow[0]["emp_ft_nm"];

        $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
        $emp_id    = $arr_apply_workflow[0]["emp_id"];

        $apply_content = $arr_apply_workflow[0]["apply_content"];
        $apply_date = $obj->get_apply_date($apply_content);
	}
}

// 委員会スケジュール情報取得
// 認定委員会情報取得
$arr_cl_committee = $obj->get_cl_committee("1");

$cl_title = cl_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cl_title?>｜認定スケジュール登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">

function regist_recognize_schedule(mode)
{

	for(i=1; i<=7; i++)
	{
		date_y = document.getElementById('date_y' + i).value;
		date_m = document.getElementById('date_m' + i).value;
		date_d = document.getElementById('date_d' + i).value;

		if(isNaN(date_y) || isNaN(date_m) || isNaN(date_d))
		{
			if(date_y == "-" && date_m == "-" && date_d == "-")
			{
				// 何もしない
			}
			else
			{
			    alert('入力された評価予定日に入力漏れがあります。');
				return;
			}
		}

	}
	if (confirm('登録します。よろしいですか？'))
	{
		document.apply.action="cl_recognize_schedule_regist.php?session=<?=$session?>&mode=REGIST";
		document.apply.submit();
    }

}

/**
 * 認定スケジュール登録コントロール
 * @author 2011.12.05 Add by matsuura
 */
function regist_recognize_schedule2()
{
//	alert("regist_recognize_schedule2 start");

	for(i=1; i<=7; i++)
	{
		date_y = document.getElementById('date_y' + i).value;
		date_m = document.getElementById('date_m' + i).value;
		date_d = document.getElementById('date_d' + i).value;

		if(isNaN(date_y) || isNaN(date_m) || isNaN(date_d))
		{
			if(date_y == "-" && date_m == "-" && date_d == "-")
			{
				// 何もしない
			}
			else
			{
			    alert('入力された評価予定日に入力漏れがあります。');
				return;
			}
		}
	}

//	alert("nurse_type = " + <?= $nurse_type ?>);
	// 2011.12.20 Edit by matsuura
	<?	if($nurse_type == "2" || $nurse_type == "3"){	?>
			for(i = 31; i <= 32; i++){
				// 2011.12.20 Edit by matsuura
				if(i == 31){
					<? if($level == "3" && $nurse_type == "2"){ ?>
							continue;
					<? } ?>
				}

				date_y = document.getElementById('date_y' + i).value;
				date_m = document.getElementById('date_m' + i).value;
				date_d = document.getElementById('date_d' + i).value;

				if(isNaN(date_y) || isNaN(date_m) || isNaN(date_d))
				{
					if(date_y == "-" && date_m == "-" && date_d == "-")
					{
						// 何もしない
					}
					else
					{
					    alert('入力された評価予定日に入力漏れがあります。');
						return;
					}
				}
			}
	<?	} ?>

//	alert("check OK");
//	strBuf = "";
//	for(i = 31; i < 33; i++){
//		strBuf = strBuf + "date_y" + i + " = " + document.getElementById('date_y' + i).value;
//		strBuf = strBuf + " date_m" + i + " = " + document.getElementById('date_m' + i).value;
//		strBuf = strBuf + " date_d" + i + " = " + document.getElementById('date_d' + i).value + "\n";
//	}
//	alert("strBuf = " + strBuf);

	if (confirm('登録します。よろしいですか？'))
	{
		document.apply.action="cl_recognize_schedule_regist.php?session=<?=$session?>&mode=REGIST&nurse_type=<?=$nurse_type?>";
		document.apply.submit();
    }

}

</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

</style>
<?
// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力  2011.12.06 Edit by matsuura
// write_yui_calendar_script2(7);
//$arr_field = array("1", "2", "3", "4", "5", "6", "7", "31", "32");
//	write_yui_calendar_script_array($arr_field);

// 2011.12.20 Edit by matsuura
if($nurse_type == "1"){
	// 病棟のとき
	write_yui_calendar_script2(7);
}else if($level == "3" & $nurse_type == "2"){
	// レベルIII、外来のとき
	$arr_field = array("1", "2", "3", "4", "5", "6", "7", "32");
	write_yui_calendar_script_array($arr_field);
}else{
	// 上記以外のとき
	$arr_field = array("1", "2", "3", "4", "5", "6", "7", "31", "32");
	write_yui_calendar_script_array($arr_field);
}

?>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();">

<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="apply_id" value="<?=$apply_id?>">
<input type="hidden" name="apply_date" value="<?=$apply_date?>">
<input type="hidden" name="emp_id" value="<?=$emp_id?>">
<input type="hidden" name="level" value="<?=$level?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>認定スケジュール登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者：</font></td>
<td ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($emp_full_nm);?></font></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願日：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr bgcolor="#E5F6CD">
	<td colspan="2">&nbsp;</td>
	<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価予定日</font></td>
</tr>

<!-- 2011.12.05 Edit by matsuura -->
<tr>
	<!-- <td width="120" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td> -->
	<!-- 2011.12.15 Edit by matsuura -->
	<?	// if($nurse_type == "2" || $nurse_type == "3"){
		if($nurse_type == "3" || ($nurse_type == "2" && $level == "2")){ ?>
			<td width="120" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td>
	<?	}else{ ?>
			<td width="120" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td>
	<?	} ?>

	<!-- 2011.12.15 Edit by matsuura -->
	<!-- <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1&nbsp;<?// if($level == "2"){?>No.1<?// }else if($level == "3"){?>No.2<? //} ?></font></td> -->
	<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1&nbsp;
		<?if($level == "2" || ($level == "3" && $nurse_type == "3")){?>
			No.1
		<?}else if($level == "3" ){?>
			No.2
		<?}?>
	</font></td>

	<td>
		<table border="0" cellspacing="0" cellpadding="0" class="block_in">
		<tr>
			<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="date_y1" id="date_y1">
			<? show_select_years_for_cas(2, $date_y1 ,true); ?>
			</select>年
			<select name="date_m1" id="date_m1">
			<? show_select_months($date_m1 ,true); ?>
			</select>月
			<select name="date_d1" id="date_d1">
			<? show_select_days($date_d1 ,true); ?>
			</select>日</font>
			</td>
			<td>
			<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
			<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
			<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
			</font>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
<td>
	<!-- 2011.12.15 Edit by matsuura -->
	<!-- <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2&nbsp;<? // if($level == "2"){?> No.3<? // }else if($level == "3"){?> No.4<?// }?></font> -->
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2&nbsp;
	<?if($level == "2" || ($level == "3" && $nurse_type == "3")){?>
		No.3
	<?}else if($level == "3"){?>
		No.4
	<?}?></font>
	</td>
	</td>

	<td>
	<table border="0" cellspacing="0" cellpadding="0" class="block_in">
		<tr>
		<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<select name="date_y2" id="date_y2">
		<? show_select_years_for_cas(2, $date_y2 ,true); ?>
		</select>年
		<select name="date_m2" id="date_m2">
		<? show_select_months($date_m2 ,true); ?>
		</select>月
		<select name="date_d2" id="date_d2">
		<? show_select_days($date_d2 ,true); ?>
		</select>日</font>
		</td>
		<td>
		<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/>
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
		</font>
		</td>
		</tr>
	</table>
</td>
</tr>

<!-- 2011.12.15 Add by matsuura レベルIII のとき、No31 はない -->
<?	// if($nurse_type == "2" || $nurse_type == "3"){
	if($nurse_type == "3" || ($nurse_type == "2" && $level == "2")){ ?>
		<tr>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?if($level == "2"){ ?>評価3&nbsp;No.31
																	 <?}else if($level == "3"){?>評価1&nbsp;No.31<?}?></font>
			</td>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" class="block_in">
				<tr>
				<td>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<select name="date_y31" id="date_y31">
				<? show_select_years_for_cas(2, $date_y31 ,true); ?>
				</select>年
				<select name="date_m31" id="date_m31">
				<? show_select_months($date_m31 ,true); ?>
				</select>月
				<select name="date_d31" id="date_d31">
				<? show_select_days($date_d31 ,true); ?>
				</select>日</font>
				</td>
				<td>
				<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal31();"/>
				<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
				<div id="cal31Container" style="position:absolute;display:none;z-index:10000;"></div>
				</font>
				</td>
				</tr>
			</table>
		</td>
		</tr>
<?	} ?>

<tr>
<td rowspan="4">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">教育能力/自己学習能力</font>
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1&nbsp;No.5</font>
</td>
<td>
<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="date_y3" id="date_y3">
<? show_select_years_for_cas(2, $date_y3 ,true); ?>
</select>年
<select name="date_m3" id="date_m3">
<? show_select_months($date_m3 ,true); ?>
</select>月
<select name="date_d3" id="date_d3">
<? show_select_days($date_d3 ,true); ?>
</select>日</font>
</td>
<td>
<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal3();"/>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal3Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2&nbsp;<?if($level == "2"){?>No.6<?}else if($level == "3"){?>No.7<?}?></font>
</td>
<td>
<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="date_y4" id="date_y4">
<? show_select_years_for_cas(2, $date_y4 ,true); ?>
</select>年
<select name="date_m4" id="date_m4">
<? show_select_months($date_m4 ,true); ?>
</select>月
<select name="date_d4" id="date_d4">
<? show_select_days($date_d4 ,true); ?>
</select>日</font>
</td>
<td>
<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal4();"/>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal4Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価3&nbsp;No.8</font>
</td>
<td>
<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="date_y5" id="date_y5">
<? show_select_years_for_cas(2, $date_y5 ,true); ?>
</select>年
<select name="date_m5" id="date_m5">
<? show_select_months($date_m5 ,true); ?>
</select>月
<select name="date_d5" id="date_d5">
<? show_select_days($date_d5 ,true); ?>
</select>日</font>
</td>
<td>
<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal5();"/>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal5Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td>
</tr>
</table>
</td>
</tr>
<tr>
	<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価4&nbsp;No.9</font>
	</td>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" class="block_in">
		<tr>
			<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="date_y6" id="date_y6">
			<? show_select_years_for_cas(2, $date_y6 ,true); ?>
			</select>年
			<select name="date_m6" id="date_m6">
			<? show_select_months($date_m6 ,true); ?>
			</select>月
			<select name="date_d6" id="date_d6">
			<? show_select_days($date_d6 ,true); ?>
			</select>日</font>
			</td>
			<td>
			<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal6();"/>
			<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
			<div id="cal6Container" style="position:absolute;display:none;z-index:10000;"></div>
			</font>
			</td>
		</tr>
		</table>
	</td>
</tr>

<!-- 2011.12.08 Edit by matsuura -->
<tr>
	<?	if($nurse_type == "2" || $nurse_type == "3"){ ?>
			<td rowspan="2">
	<?	}else{ ?>
			<td>
	<?	} ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リーダーシップ能力</font>
	</td>
	<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1&nbsp;<?if($level == "2"){?>No.10<?}else if($level == "3"){?>No.11<?}?></font>
	</td>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" class="block_in">
		<tr>
			<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="date_y7" id="date_y7">
			<? show_select_years_for_cas(2, $date_y7 ,true); ?>
			</select>年
			<select name="date_m7" id="date_m7">
			<? show_select_months($date_m7 ,true); ?>
			</select>月
			<select name="date_d7" id="date_d7">
			<? show_select_days($date_d7 ,true); ?>
			</select>日</font>
			</td>
			<td>
			<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal7();"/>
			<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
			<div id="cal7Container" style="position:absolute;display:none;z-index:10000;"></div>
			</font>
			</td>
		</tr>
		</table>
	</td>
</tr>
	<!-- 2011.12.12 Edit by matsuura -->
	<?	if($nurse_type == "2" || $nurse_type == "3"){ ?>
		<tr>
			<td>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?if($level == "2"){?>評価3&nbsp No.32<?}else if($level == "3"){?>評価1&nbsp No.32<?}?></font>
			</td>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" class="block_in">
				<tr>
					<td>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
					<select name="date_y32" id="date_y32">
					<? show_select_years_for_cas(2, $date_y32 ,true); ?>
					</select>年
					<select name="date_m32" id="date_m32">
					<? show_select_months($date_m32 ,true); ?>
					</select>月
					<select name="date_d32" id="date_d32">
					<? show_select_days($date_d32 ,true); ?>
					</select>日</font>
					</td>
					<td>
					<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal32();"/>
					<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
					<div id="cal32Container" style="position:absolute;display:none;z-index:10000;"></div>
					</font>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	<?	} ?>

</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr>
<td>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>*No.5は発表会開催</font>
</td>
<td align="right">
<input type="button" value="登録" onclick="regist_recognize_schedule2();">
</td>
</tr>
</table>

</center>

<img src="img/spacer.gif" alt="" width="1" height="10"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000"><b>認定委員会の今後の開催予定</b></font></td>
</tr>
</table>


<!-- 認定委員会のスケジュール -->

<?
foreach($arr_cl_committee as $cl_committee)
{
    $tmp_pjt_name = $cl_committee["parent_pjt_nm"];
    if($cl_committee["child_pjt_nm"] != "")
    {
        $tmp_pjt_name .= " > ";
        $tmp_pjt_name .= $cl_committee["child_pjt_nm"];
    }
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=htmlspecialchars($tmp_pjt_name);?></b></font></td>
</tr>
</table>

<table width="500" border="0" cellspacing="0" cellpadding="1" class="list">

<?
// 今後予定の委員会スケジュール情報取得
$arr_proschd = $obj->get_proschd($cl_committee["parent_pjt_id"], $cl_committee["child_pjt_id"]);
$cnt = 0;
foreach($arr_proschd as $proschd)
{
    $schd_id         = $proschd["pjt_schd_id"];
    $pjt_id          = $proschd["pjt_id"];
    $schd_title      = $proschd["pjt_schd_title"];
    $schd_type       = $proschd["pjt_schd_type"];

    $schd_start_date = $proschd["pjt_schd_start_date"];
    $schd_start_time = substr($proschd["pjt_schd_start_time_v"],0,2).":".substr($proschd["pjt_schd_start_time_v"],2,2);
    $schd_dur = substr($proschd["pjt_schd_dur_v"],0,2).":".substr($proschd["pjt_schd_dur_v"],2,2);

    $tmp_date = mktime(0, 0, 0, substr($schd_start_date, 5, 2), substr($schd_start_date, 8, 2), substr($schd_start_date, 0, 4));
    if ($schd_start_time != ":" && $schd_dur != ":") {
        $timeless = "";
        $formatted_time = substr($schd_start_time, 0, 5) . "-" . substr($schd_dur, 0, 5);
    } else {
	    $timeless = "t";
        $formatted_time = "指定なし";
    }
?>

<?
if($cnt == 0)
{
?>
<tr bgcolor="#E5F6CD">

<?
if($cl_committee["parent_pjt_id"] != "" && $cl_committee["child_pjt_id"] != "")
{
?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・ＷＧ</font></td>
<?
}
?>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
</tr>
<?
}
?>

<tr>

<?
if($cl_committee["parent_pjt_id"] != "" && $cl_committee["child_pjt_id"] != "")
{
    if($pjt_id == $cl_committee["parent_pjt_id"])
    {
?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$cl_committee["parent_pjt_nm"]?></font></td>
<?
    }
    else
    {
?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$cl_committee["child_pjt_nm"]?></font></td>
<?
    }
}
?>

<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$schd_start_date?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$formatted_time?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('project_schedule_update.php?session=<?=$session?>&pjt_schd_id=<?=$schd_id?>&timeless=<?=$timeless?>&pjt_id=<?=$pjt_id?>&date=<?=$tmp_date?>&time=<?=$schd_start_time?>', 'newwin', 'width=640,height=480,scrollbars=yes');"><?=htmlspecialchars($schd_title);?></a></font></td>
</tr>

<?
$cnt++;
}
?>
</table>
<?
}
?>


</body>
</form>
</html>



<?
/**
 *
 * 認定スケジュール登録処理
 * @param object $obj　cl_application_workfle_common_class オブジェクト
 * @param array $inputs　各フィールド配列
 * @param string $nurse_type　看護種別
 * @author 2011.12.06 Edit by matsuura
 */
function regist_schedule($obj, $inputs, $nurse_type)
{
//	echo("regist_schedule start  inputs = ");
//	echo(print_r($inputs));

	$level = $inputs["level"];
    $apply_date = ereg_replace("/", "", $inputs["apply_date"]);

    $date_1 = ($inputs["date_y1"]."".$inputs["date_m1"]."".$inputs["date_d1"] == "---") ? "" : $inputs["date_y1"]."".$inputs["date_m1"]."".$inputs["date_d1"];
    $date_2 = ($inputs["date_y2"]."".$inputs["date_m2"]."".$inputs["date_d2"] == "---") ? "" : $inputs["date_y2"]."".$inputs["date_m2"]."".$inputs["date_d2"];
    $date_3 = ($inputs["date_y3"]."".$inputs["date_m3"]."".$inputs["date_d3"] == "---") ? "" : $inputs["date_y3"]."".$inputs["date_m3"]."".$inputs["date_d3"];
    $date_4 = ($inputs["date_y4"]."".$inputs["date_m4"]."".$inputs["date_d4"] == "---") ? "" : $inputs["date_y4"]."".$inputs["date_m4"]."".$inputs["date_d4"];
    $date_5 = ($inputs["date_y5"]."".$inputs["date_m5"]."".$inputs["date_d5"] == "---") ? "" : $inputs["date_y5"]."".$inputs["date_m5"]."".$inputs["date_d5"];
    $date_6 = ($inputs["date_y6"]."".$inputs["date_m6"]."".$inputs["date_d6"] == "---") ? "" : $inputs["date_y6"]."".$inputs["date_m6"]."".$inputs["date_d6"];
    $date_7 = ($inputs["date_y7"]."".$inputs["date_m7"]."".$inputs["date_d7"] == "---") ? "" : $inputs["date_y7"]."".$inputs["date_m7"]."".$inputs["date_d7"];

    // 2011.12.06 Add by matsuura
    $date_12 = ($inputs["date_y12"]."".$inputs["date_m12"]."".$inputs["date_d12"] == "---") ? "" : $inputs["date_y12"]."".$inputs["date_m12"]."".$inputs["date_d12"];
    $date_31 = ($inputs["date_y31"]."".$inputs["date_m31"]."".$inputs["date_d31"] == "---") ? "" : $inputs["date_y31"]."".$inputs["date_m31"]."".$inputs["date_d31"];
    $date_32 = ($inputs["date_y32"]."".$inputs["date_m32"]."".$inputs["date_d32"] == "---") ? "" : $inputs["date_y32"]."".$inputs["date_m32"]."".$inputs["date_d32"];
    $date_33 = ($inputs["date_y33"]."".$inputs["date_m33"]."".$inputs["date_d33"] == "---") ? "" : $inputs["date_y33"]."".$inputs["date_m33"]."".$inputs["date_d33"];

    $arr_data = "";

	if($level == "2")
	{
		$arr_data = array(
		"apply_id" => $inputs["apply_id"],
		"apply_date" => $apply_date,
		"emp_id" => $inputs["emp_id"],
		"level" => $inputs["level"],

		"no1_date" => $date_1,
		"no1_apply_id" => null,
		"no2_date" => "",
		"no2_apply_id" => null,
		"no3_date" => $date_2,
		"no3_apply_id" => null,
		"no4_date" => "",
		"no4_apply_id" => null,
		"no5_date" => $date_3,
		"no5_apply_id" => null,
		"no6_date" => $date_4,
		"no6_apply_id" => null,
		"no7_date" => "",
		"no7_apply_id" => null,
		"no8_date" => $date_5,
		"no8_apply_id" => null,
		"no9_date" => $date_6,
		"no9_apply_id" => "",
		"no10_date" => $date_7,
		"no10_apply_id" => null,
		"no11_date" => "",
		"no11_apply_id" => null,
		"no12_date" => $date_12,
		"no12_apply_id" => null,

		// 2011.12.06 Add by matsuura
		"no31_date" => $date_31,
		"no31_apply_id" => null,
		"no32_date" => $date_32,
		"no32_apply_id" => null,
		"no33_date" => $date_33,
		"no33_apply_id" => null,
		"nurse_type" => $nurse_type,

		"disp_flg" => "f",
		"levelup_apply_id" => null
		);
	}
	else
	{
		$arr_data = array(
		"apply_id" => $inputs["apply_id"],
		"apply_date" => $apply_date,
		"emp_id" => $inputs["emp_id"],
		"level" => $inputs["level"],
		"no1_date" => "",
		"no1_apply_id" => null,
		"no2_date" => $date_1,
		"no2_apply_id" => null,
		"no3_date" => "",
		"no3_apply_id" => null,
		"no4_date" => $date_2,
		"no4_apply_id" => null,
		"no5_date" => $date_3,
		"no5_apply_id" => null,
		"no6_date" => "",
		"no6_apply_id" => null,
		"no7_date" => $date_4,
		"no7_apply_id" => null,
		"no8_date" => $date_5,
		"no8_apply_id" => null,
		"no9_date" => $date_6,
		"no9_apply_id" => "",
		"no10_date" => "",
		"no10_apply_id" => null,
		"no11_date" => $date_7,
		"no11_apply_id" => null,
		"no12_date" => $date_12,
		"no12_apply_id" => null,

		// 2011.12.06 Add by matsuura
		"no31_date" => $date_31,
		"no31_apply_id" => null,
		"no32_date" => $date_32,
		"no32_apply_id" => null,
		"no33_date" => $date_33,
		"no33_apply_id" => null,
		"nurse_type" => $nurse_type,

		"disp_flg" => "f",
		"levelup_apply_id" => null
		);
	}

//	echo("arr_data = ");
//	echo(print_r($arr_data));
	// 2011.12.07 Edit by matsuura
//	$obj->regist_cl_recognize_schedule($arr_data);
	$obj->regist_cl_recognize_schedule2($arr_data);
}

/**
 * 認定スケジュール更新処理
 * @param object $obj　オブジェクト
 * @param array $inputs　各フィールド配列
 * @param string $nurse_type　看護種別
 * @author 2011.12.06 Edit by matsuura
 */
function update_schedule($obj, $inputs, $nurse_type)
{
//	echo("update_schedule start  inputs = ");
//	echo(print_r($inputs));

	$level = $inputs["level"];
	$arr_data = "";

    $date_1 = ($inputs["date_y1"]."".$inputs["date_m1"]."".$inputs["date_d1"] == "---") ? "" : $inputs["date_y1"]."".$inputs["date_m1"]."".$inputs["date_d1"];
    $date_2 = ($inputs["date_y2"]."".$inputs["date_m2"]."".$inputs["date_d2"] == "---") ? "" : $inputs["date_y2"]."".$inputs["date_m2"]."".$inputs["date_d2"];
    $date_3 = ($inputs["date_y3"]."".$inputs["date_m3"]."".$inputs["date_d3"] == "---") ? "" : $inputs["date_y3"]."".$inputs["date_m3"]."".$inputs["date_d3"];
    $date_4 = ($inputs["date_y4"]."".$inputs["date_m4"]."".$inputs["date_d4"] == "---") ? "" : $inputs["date_y4"]."".$inputs["date_m4"]."".$inputs["date_d4"];
    $date_5 = ($inputs["date_y5"]."".$inputs["date_m5"]."".$inputs["date_d5"] == "---") ? "" : $inputs["date_y5"]."".$inputs["date_m5"]."".$inputs["date_d5"];
    $date_6 = ($inputs["date_y6"]."".$inputs["date_m6"]."".$inputs["date_d6"] == "---") ? "" : $inputs["date_y6"]."".$inputs["date_m6"]."".$inputs["date_d6"];
    $date_7 = ($inputs["date_y7"]."".$inputs["date_m7"]."".$inputs["date_d7"] == "---") ? "" : $inputs["date_y7"]."".$inputs["date_m7"]."".$inputs["date_d7"];

    // 2011.12.06 Add by matsuura
    $date_12 = ($inputs["date_y12"]."".$inputs["date_m12"]."".$inputs["date_d12"] == "---") ? "" : $inputs["date_y12"]."".$inputs["date_m12"]."".$inputs["date_d12"];
    $date_31 = ($inputs["date_y31"]."".$inputs["date_m31"]."".$inputs["date_d31"] == "---") ? "" : $inputs["date_y31"]."".$inputs["date_m31"]."".$inputs["date_d31"];
    $date_32 = ($inputs["date_y32"]."".$inputs["date_m32"]."".$inputs["date_d32"] == "---") ? "" : $inputs["date_y32"]."".$inputs["date_m32"]."".$inputs["date_d32"];
    $date_33 = ($inputs["date_y33"]."".$inputs["date_m33"]."".$inputs["date_d33"] == "---") ? "" : $inputs["date_y33"]."".$inputs["date_m33"]."".$inputs["date_d33"];

    if($level == "2")
	{
		$arr_data = array("apply_id"  => $inputs["apply_id"],
                          "no1_date"   => $date_1,
		                  "no3_date"   => $date_2,
		                  "no5_date"   => $date_3,
		                  "no6_date"   => $date_4,
		                  "no8_date"   => $date_5,
		                  "no9_date"   => $date_6,
		                  "no10_date"  => $date_7,

							// 2011.12.06 Add by matsuura
							"no12_date" => $date_12,
							"no31_date" => $date_31,
							"no32_date" => $date_32,
							"no33_date" => $date_33,
							"nurse_type" => $nurse_type
		);

//		echo("arr_data = ");
//		echo(print_r($arr_data));
		// 2011.12.07 Edit by matsuura
//		$obj->update_cl_recognize_schedule_for_level2($arr_data);
		$obj->update_cl_recognize_schedule2_for_level2($arr_data);
	}
	else if($level == "3")
	{
		$arr_data = array("apply_id"  => $inputs["apply_id"],
                          "no2_date"   => $date_1,
		                  "no4_date"   => $date_2,
		                  "no5_date"   => $date_3,
		                  "no7_date"   => $date_4,
		                  "no8_date"   => $date_5,
		                  "no9_date"   => $date_6,
		                  "no11_date"  => $date_7,

							// 2011.12.06 Add by matsuura
							"no12_date" => $date_12,
							"no31_date" => $date_31,
							"no32_date" => $date_32,
							"no33_date" => $date_33,
							"nurse_type" => $nurse_type
		);

//		echo("arr_data = ");
//		echo(print_r($arr_data));
		// 2011.12.07 Edit by matsuura
//		$obj->update_cl_recognize_schedule_for_level3($arr_data);
		$obj->update_cl_recognize_schedule2_for_level3($arr_data);
	}
}



pg_close($con);
?>