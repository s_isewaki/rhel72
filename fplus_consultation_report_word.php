<?php
require_once("about_comedix.php");
require_once ( './libs/phpword/PHPWord.php' );
require_once ( './libs/phpword/PHPWord/IOFactory.php' );
require_once ( './libs/phpword/PHPWord/Writer/Word2007.php' );

// データベースに接続
$con = connect2db($fname);

// 病院名検索
if($data_hospital_id!=""){
	//検索処理
	$sql=	"SELECT hospital_name, hospital_directer FROM fplus_hospital_master ";
	$cond = "WHERE hospital_id = $data_hospital_id AND del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hospital_name = pg_fetch_result($sel, 0, "hospital_name");
}
pg_close($con);
// オブジェクト
$PHPWord = new PHPWord();


//====================================================================
// ヘッダ設定
//====================================================================
header("Content-Disposition: attachment; filename=fplus_medical_accident_info_".Date("Ymd_Hi").".docx;");
header("Content-Transfer-Encoding: binary");


// 日本語はUTF-8へ変換する。(mb_convert_encoding関数を使用)
// 変換しない場合は文書ファイルが壊れて生成される。

// デフォルトのフォント
$PHPWord->setDefaultFontName(mb_convert_encoding('ＭＳ 明朝','UTF-8','EUC-JP'));
$PHPWord->setDefaultFontSize(10.5);

// プロパティ
$properties = $PHPWord->getProperties();
$properties->setTitle(mb_convert_encoding('医療安全コンサルテーション報告書','UTF-8','EUC-JP'));
$properties->setCreator(mb_convert_encoding('Medi-System','UTF-8','EUC-JP')); 
$properties->setSubject(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setCompany(mb_convert_encoding('（株）メディシステムソリューション','UTF-8','EUC-JP'));
$properties->setCategory(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setCreated(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setModified(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setKeywords(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setLastModifiedBy(mb_convert_encoding('Medi-System','UTF-8','EUC-JP'));

// セクションとスタイル、単位に注意(1/144インチ)
$section = $PHPWord->createSection();
$sectionStyle = $section->getSettings();
$sectionStyle->setLandscape();
$sectionStyle->setPortrait();
$sectionStyle->setMarginLeft(1700); // メートル法では約15.9mmになります。
$sectionStyle->setMarginRight(1700);
$sectionStyle->setMarginTop(1700); // メートル法では約10.6mmになります。
$sectionStyle->setMarginBottom(1700);

//前文
$PHPWord->addFontStyle('intro_pStyle', array('size'=>10.5,'spacing'=>100));
$PHPWord->addParagraphStyle('intro_align', array('align'=>'left'));
$section->addText(mb_convert_encoding($hospital_name.' 病院長','UTF-8','EUC-JP'), 'intro_pStyle', 'intro_align');
$section->addText(mb_convert_encoding($hospital_directer.' 様','UTF-8','EUC-JP'), 'intro_pStyle', 'intro_align');
$section->addText(mb_convert_encoding(' 貴院より依頼のありました問題点に関して、コンサルテーション医からの意見を取りまとめましたので、ご査収ください。','UTF-8','EUC-JP'), 'intro_pStyle', 'intro_align');
$align = array('align'=>'right');
$section->addText(mb_convert_encoding('医療安全対策専門役 '.$data_medical_safety_charge,'UTF-8','EUC-JP'), 'intro_pStyle', $align);
$section->addTextBreak(1);

//タイトル
$PHPWord->addFontStyle('title_pStyle', array('size'=>14,'spacing'=>100));
$PHPWord->addParagraphStyle('title_align', array('align'=>'center'));
$section->addText(mb_convert_encoding('KKR医療安全コンサルテーション報告書','UTF-8','EUC-JP'), 'title_pStyle', 'title_align');
$section->addTextBreak(1);

// 本文
$PHPWord->addFontStyle('contents_pStyle', array('size'=>10.5,'spacing'=>100));
$PHPWord->addParagraphStyle('contents_align', array('align'=>'left'));
// 表形式
// Define table style arrays
$styleTable = array('cellMarginLeft'=>80, 'cellMarginRight'=>80);
// Add table style
$PHPWord->addTableStyle('myOwnTableStyle', $styleTable);
// Add table
$table = $section->addTable('myOwnTableStyle');
// タイトル行
$table->addRow(400);
$LName=mb_convert_encoding('タイトル','UTF-8','EUC-JP');
$RName=mb_convert_encoding($data_title,'UTF-8','EUC-JP');
$styleCell = array('valign'=>'center','borderSize'=>6);
$table->addCell(1366,$styleCell)->addText($LName,'contents_pStyle','contents_align');
$table->addCell(7330,$styleCell)->addText($RName,'contents_pStyle','contents_align');

$section->addTextBreak(1);

// 表形式
$table = $section->addTable('myOwnTableStyle');
// 内容テーブル
$table->addRow(400);
$styleCell_l = array('valign'=>'center','borderSize'=>6);
$styleCell_r = array('valign'=>'center','halign'=>'center','borderSize'=>6);
$align = array('align'=>'center');
$LName=mb_convert_encoding(mb_convert_encoding('問題点��',"sjis-win","eucJP-win"),"UTF-8","sjis-win");
$RName=mb_convert_encoding('問題点に対する助言内容','UTF-8','EUC-JP');
$table->addCell(1366,$styleCell_l)->addText($LName,'contents_pStyle','contents_align');
$table->addCell(7330,$styleCell_r)->addText($RName,'contents_pStyle',$align);

// 問題点の数
$problem_count = (int)$data_problem_count;

$table->addRow(400);
$styleCell_l = array('valign'=>'top','borderSize'=>6);
$styleCell = array('valign'=>'center','borderSize'=>6);
$LName=mb_convert_encoding('問題点1','UTF-8','EUC-JP');
$RName=mb_convert_encoding('問題点1','UTF-8','EUC-JP');
$table->addCell(1366,$styleCell_l)->addText($LName,'contents_pStyle','contents_align');
$cell = $table->addCell(7330,$styleCell);
$cell->addText($RName,'contents_pStyle','contents_align');

// 入力データを改行で分割
$ary_first_problem = preg_split("/\n/", $data_first_problem);
for($i=0;$i<count($ary_first_problem);$i++){
	$RName=mb_convert_encoding("　$ary_first_problem[$i]",'UTF-8','EUC-JP');
	$cell->addText($RName,'contents_pStyle','contents_align');
}
$cell->addText();
$RName=mb_convert_encoding('助言内容','UTF-8','EUC-JP');
$cell->addText($RName,'contents_pStyle','contents_align');

// 入力データを改行で分割
$ary_first_advice = preg_split("/\n/", $data_first_advice);
for($i=0;$i<count($ary_first_advice);$i++){
	$RName=mb_convert_encoding("　$ary_first_advice[$i]",'UTF-8','EUC-JP');
	$cell->addText($RName,'contents_pStyle','contents_align');
}
$cell->addText();

$styleCell = array('valign'=>'center','borderSize'=>6);
for($i=2;$i<=$problem_count;$i++){
	$table->addRow(400);
	$LName=mb_convert_encoding('問題点'.$i,'UTF-8','EUC-JP');
	$RName=mb_convert_encoding('問題点'.$i,'UTF-8','EUC-JP');
	$table->addCell(1366,$styleCell_l)->addText($LName,'contents_pStyle','contents_align');
	$cell = $table->addCell(7330,$styleCell);
	$cell->addText($RName,'contents_pStyle','contents_align');

	if($i==2){
		// 入力データを改行で分割
		$ary_second_problem = preg_split("/\n/", $data_second_problem);
		for($j=0;$j<count($ary_second_problem);$j++){
			$RName=mb_convert_encoding("　$ary_second_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==3){
		// 入力データを改行で分割
		$ary_third_problem = preg_split("/\n/", $data_third_problem);
		for($j=0;$j<count($ary_third_problem);$j++){
			$RName=mb_convert_encoding("　$ary_third_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==4){
		// 入力データを改行で分割
		$ary_fourth_problem = preg_split("/\n/", $data_fourth_problem);
		for($j=0;$j<count($ary_fourth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_fourth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==5){
		// 入力データを改行で分割
		$ary_fifth_problem = preg_split("/\n/", $data_fifth_problem);
		for($j=0;$j<count($ary_fifth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_fifth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==6){
		// 入力データを改行で分割
		$ary_sixth_problem = preg_split("/\n/", $data_sixth_problem);
		for($j=0;$j<count($ary_sixth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_sixth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==7){
		// 入力データを改行で分割
		$ary_seventh_problem = preg_split("/\n/", $data_seventh_problem);
		for($j=0;$j<count($ary_seventh_problem);$j++){
			$RName=mb_convert_encoding("　$ary_seventh_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==8){
		// 入力データを改行で分割
		$ary_eighth_problem = preg_split("/\n/", $data_eighth_problem);
		for($j=0;$j<count($ary_eighth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_eighth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==9){
		// 入力データを改行で分割
		$ary_nineth_problem = preg_split("/\n/", $data_nineth_problem);
		for($j=0;$j<count($ary_nineth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_nineth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==10){
		// 入力データを改行で分割
		$ary_tenth_problem = preg_split("/\n/", $data_tenth_problem);
		for($j=0;$j<count($ary_tenth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_tenth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	$RName=mb_convert_encoding('助言内容','UTF-8','EUC-JP');
	$cell->addText($RName,'contents_pStyle','contents_align');
	if($i==2){
		// 入力データを改行で分割
		$ary_second_advice = preg_split("/\n/", $data_second_advice);
		for($j=0;$j<count($ary_second_advice);$j++){
			$RName=mb_convert_encoding("　$ary_second_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==3){
		// 入力データを改行で分割
		$ary_third_advice = preg_split("/\n/", $data_third_advice);
		for($j=0;$j<count($ary_third_advice);$j++){
			$RName=mb_convert_encoding("　$ary_third_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==4){
		// 入力データを改行で分割
		$ary_fourth_advice = preg_split("/\n/", $data_fourth_advice);
		for($j=0;$j<count($ary_fourth_advice);$j++){
			$RName=mb_convert_encoding("　$ary_fourth_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==5){
		// 入力データを改行で分割
		$ary_fifth_advice = preg_split("/\n/", $data_fifth_advice);
		for($j=0;$j<count($ary_fifth_advice);$j++){
			$RName=mb_convert_encoding("　$ary_fifth_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==6){
		// 入力データを改行で分割
		$ary_sixth_advice = preg_split("/\n/", $data_sixth_advice);
		for($j=0;$j<count($ary_sixth_advice);$j++){
			$RName=mb_convert_encoding("　$ary_sixth_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==7){
		// 入力データを改行で分割
		$ary_seventh_advice = preg_split("/\n/", $data_seventh_advice);
		for($j=0;$j<count($ary_seventh_advice);$j++){
			$RName=mb_convert_encoding("　$ary_seventh_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==8){
		// 入力データを改行で分割
		$ary_eighth_advice = preg_split("/\n/", $data_eighth_advice);
		for($j=0;$j<count($ary_eighth_advice);$j++){
			$RName=mb_convert_encoding("　$ary_eighth_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==9){
		// 入力データを改行で分割
		$ary_nineth_advice = preg_split("/\n/", $data_nineth_advice);
		for($j=0;$j<count($ary_nineth_advice);$j++){
			$RName=mb_convert_encoding("　$ary_nineth_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	elseif($i==10){
		// 入力データを改行で分割
		$ary_tenth_advice = preg_split("/\n/", $data_tenth_advice);
		for($j=0;$j<count($ary_tenth_advice);$j++){
			$RName=mb_convert_encoding("　$ary_tenth_advice[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
}

// 出力
$WORD_OUT =  PHPWord_IOFactory::createWriter($PHPWord,'WORD2007');
//$WORD_OUT->save('./fplus/workflow/tmp/test.docx');
$WORD_OUT->save('php://output');

exit;
