<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_authority.php");
require("about_session.php");

$fname=$PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($trashbox == "") {
	echo("<script type=\"text/javascript\">alert('チェックボックスをオンにしてください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションの開始
pg_query($con, "begin");

// 削除対象の文書カテゴリID
$cate_ids = array();

foreach ($trashbox as $tmp_pjt_id) {

	// 委員会・WG情報を論理削除
	$sql = "update project set";
	$set = array("pjt_delete_flag","pjt_sort_id","wg_sort_id");
	$setvalue = array("t",null,null);
	$cond = "where pjt_id = $tmp_pjt_id or pjt_parent_id = $tmp_pjt_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 文書カテゴリIDを取得
	$sql = "select lib_cate_id from libcate";
	$cond = "where lib_archive = '4' and lib_link_id = '$tmp_pjt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$tmp_cate_id = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, 0) : "";

	if ($tmp_cate_id != "") 
	{
		$cate_ids[] = $tmp_cate_id;
		
		
		//委員会の中にファイルが入っているか確認
		$sql = "select count(*) from libcate,libinfo,project";
		$cond = "where libcate.lib_archive = '4' and libcate.lib_cate_id = libinfo.lib_cate_id and libcate.lib_archive = libinfo.lib_archive";
		$cond .= " and to_number(libcate.lib_link_id,'000') = pjt_id and libcate.lib_cate_id = '". $tmp_cate_id."'";
		$sel_lib = select_from_table($con, $sql, $cond, $fname);
		if ($sel_lib === '0') {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if(pg_fetch_result($sel_lib, 0, "count") != '0' and $del_flg != "t"){
			$sql = "select pjt_name from project";
			$cond = "where pjt_id = $tmp_pjt_id";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$tmp_pjt_nm = pg_fetch_result($sel, 0, "pjt_name");
	
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$tmp_pjt_nm}」はファイルが存在するため削除できません');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// ワークフローの保存先として使われていたら削除不可とする
		$sql = "select count(*) from wkfwmst";
		$cond = "where lib_archive = '4' and lib_cate_id = $tmp_cate_id and wkfw_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$sql = "select pjt_name from project";
			$cond = "where pjt_id = $tmp_pjt_id";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$tmp_pjt_name = pg_fetch_result($sel, 0, "pjt_name");

			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$tmp_pjt_name}」はワークフローの文書保存先として指定されているため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

//委員会の中のファイルにリンクされているか確認
		$sql = "select count(*) from libfolder";
                $cond = "where folder_id in (select folder_id from libfolder where link_id in (select folder_id from libfolder where lib_cate_id = '$tmp_cate_id'))";
		$sel_lib = select_from_table($con, $sql, $cond, $fname);
		if ($sel_lib === '0') {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if(pg_fetch_result($sel_lib, 0, "count") !== '0' and $del_flg != "t"){
			$sql = "select pjt_name from project";
			$cond = "where pjt_id = $tmp_pjt_id";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$tmp_pjt_nm = pg_fetch_result($sel, 0, "pjt_name");
	
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$tmp_pjt_nm}」はリンクされているフォルダが存在するため削除できません');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 文書カテゴリ情報を削除
		$sql = "delete from libcate";
		$cond = "where lib_cate_id = '$tmp_cate_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書ツリー情報を削除
		$sql = "delete from libtree";
		$cond = "where parent_id in (select folder_id from libfolder where lib_cate_id = '$tmp_cate_id') or child_id in (select folder_id from libfolder where lib_cate_id = '$tmp_cate_id')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// リンクフォルダ情報を削除
		$tables = array("libfolderrefemp", "libfolderupdemp", "libfolder");
		foreach ($tables as $tmp_table) {
			$sql = "delete from $tmp_table";
			$cond = "where folder_id in (select folder_id from libfolder where link_id in (select folder_id from libfolder where lib_cate_id = '$tmp_cate_id'))";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		// 文書フォルダ情報を削除
		$sql = "delete from libfolder";
		$cond = "where lib_cate_id = '$tmp_cate_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照ログを削除
		$sql = "delete from libreflog";
		$cond = "where lib_id in (select lib_id from libinfo where lib_cate_id = $tmp_cate_id)";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書情報を削除
		$sql = "delete from libinfo";
		$cond = "where lib_cate_id = '$tmp_cate_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");

	//ソート順番の振りなおし処理
	//削除後の対象委員会のソートIDを振りなおし
	
	$sql = "select * from project ";
	$cond = " where pjt_id =".$tmp_pjt_id;
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	if(pg_result($sel, 0, "pjt_parent_id") == "")
	{
		//委員会情報
		
		$tmp_class_id = pg_result($sel, 0, "class_id");
		$tmp_atrb_id = pg_result($sel, 0, "atrb_id");
		
		
		if($tmp_class_id == "")
		{$str_query = "and class_id is null ";	}
		else
		{$str_query = "and  class_id=".$tmp_class_id;}
		
		
		if($tmp_atrb_id == "")
		{$str_artb_query = "and atrb_id is null ";}
		else
		{$str_artb_query = "and  atrb_id=".$tmp_atrb_id;}
		
		$sql = "select pjt_id from project ";
		$cond = " where pjt_parent_id is null and pjt_delete_flag = 'f' ".$str_query.$str_artb_query." order by pjt_sort_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == "0") 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$update_sort_id = 1;
		while ($row = pg_fetch_array($sel)) 
		{
			
			// ソートIDを更新
			$sql = "update project set";
			$set = array("pjt_sort_id");
			$setvalue = array($update_sort_id);
			$cond = " where pjt_id=".$row["pjt_id"];
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$update_sort_id++;
		}
		
	}
	else
	{
		//WG情報
		
		$sql = "select pjt_id from project ";
		$cond = " where pjt_delete_flag = 'f' and pjt_parent_id=".pg_result($sel, 0, "pjt_parent_id")." order by wg_sort_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == "0") 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$update_sort_id = 1;
		while ($row = pg_fetch_array($sel)) 
		{
			
			// ソートIDを更新
			$sql = "update project set";
			$set = array("wg_sort_id");
			$setvalue = array($update_sort_id);
			$cond = " where pjt_id=".$row["pjt_id"];
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$update_sort_id++;
		}
		
	}

	// トランザクションをコミット
	pg_query($con, "commit");
	
	
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 文書を削除
$cwd = getcwd();
foreach ($cate_ids as $tmp_cate_id) {
	exec("rm -fr $cwd/docArchive/project/cate$tmp_cate_id");
}

if($adm_flg=="t") 
{
	// 委員会・WG一覧画面を再表示(管理者権限あり)
	echo("<script type=\"text/javascript\">location.href = 'project_menu_adm.php?session=$session';</script>");

}
else
{
	// 委員会・WG一覧画面を再表示(一般ユーザ)
	echo("<script type=\"text/javascript\">location.href = 'project_menu.php?session=$session';</script>");
}

?>
