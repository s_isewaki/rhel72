<?php

/**
 * 記号のリアルタイム集計Ajax
 *
 */
ob_start();
require_once("Cmx.php");
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");

//ページ名
$fname = $_SERVER['PHP_SELF'];

//セッションのチェック
$session = qualify_session($_POST['session'], $fname);
if ($session === "0") {
    header('HTTP/1.1 403 Forbidden');
    echo '0';
    exit;
}

//DBコネクション
$con = connect2db($fname);
if ($con === "0") {
    header('HTTP/1.1 403 Forbidden');
    echo '0';
    exit;
}

preg_match("/data(\d+)_(\d+)/", $_POST['data'], $idx);

$obj = new duty_shift_common_class($con, $fname);

// title
$title_gyo_array = $obj->get_total_title_array($_POST['pattern_id'], "1", null);
$title_retu_array = $obj->get_total_title_array($_POST['pattern_id'], "2", null);

// count_valueテーブル
$cvalues = $obj->count_values($_POST['pattern_id']);


// 行集計(日付)
$gyo_array = array();
for ($d = 1; $d <= $_POST['day_cnt']; $d++) {
    // パターン
    $plan_pattern = $_POST['ptn_day'][$d - 1];
    $plan_assist = $_POST['pid_day'][$d - 1];

    // 休暇のパターンが3桁でくる場合の対応
    if (strlen($plan_pattern) === 3 and substr($plan_pattern, 0, 2) === '10') {
        $plan_pattern = sprintf("10%02d", substr($plan_pattern, 2, 1));
    }

    // 組み合わせのパターンが4桁でくる場合の対応
    if (strlen($plan_pattern) === 4 and substr($plan_pattern, 0, 2) !== '10') {
        $plan_pattern = sprintf("%02d00", substr($plan_pattern, 0, 2));
    }

    // 集計項目毎
    foreach ($title_gyo_array as $k => $name) {
        // 異なる勤務パターンならカウントしない
        if ($_POST['pattern_id'] === $plan_assist) {
            $gyo_array[$k] += (float) $cvalues[$plan_pattern][$name['count_id']];
        }
    }
}

// 列集計(職員)
// 希望の場合は集計しない
$retu_array = array();
if ($_POST['plan_hope_flg'] === '0' or $_POST['mode'] === 'result') {
    for ($e = 0; $e < $_POST['data_cnt']; $e++) {
        $pattern = $_POST['ptn_emp'][$e];
        $job_id = $_POST['job_emp'][$e];
        $assist = $_POST['ass_emp'][$e];

        // 休暇のパターンが3桁でくる場合の対応
        if (strlen($pattern) === 3 and substr($pattern, 0, 2) === '10') {
            $pattern = sprintf("10%02d", substr($pattern, 2, 1));
        }

        // 組み合わせのパターンが4桁でくる場合の対応
        if (strlen($pattern) === 4 and substr($pattern, 0, 2) !== '10') {
            $pattern = sprintf("%02d00", substr($pattern, 0, 2));
        }

        // 集計項目毎
        foreach ($title_retu_array as $k => $name) {
            // 職種フィルタ
            if (empty($name['job_filter']) or in_array($job_id, $name['job_filter'])) {
                // 異なるシフトグループならカウントしない
                if ($_POST['group_id'] === $assist) {
                    $retu_array[$k] += (float) $cvalues[$pattern][$name['count_id']];
                }
            }
        }
    }
}

// JSON
$result = array();
foreach ($gyo_array as $num => $value) {
    $result["#sum_gyo{$idx[1]}_{$num}"] = $value;
}
foreach ($retu_array as $num => $value) {
    $num2 = $num + 1;
    $result["#summary{$num2}_{$idx[2]}"] = $value;
}

ob_end_clean();
echo cmx_json_encode($result);
