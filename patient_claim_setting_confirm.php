<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="patient_claim_setting.php">
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="pt_id" value="<? echo $pt_id; ?>">
<input type="hidden" name="rec_name" value="<? echo $rec_name; ?>">
<input type="hidden" name="rec_post1" value="<? echo $rec_post1; ?>">
<input type="hidden" name="rec_post2" value="<? echo $rec_post2; ?>">
<input type="hidden" name="rec_province" value="<? echo $rec_province; ?>">
<input type="hidden" name="rec_address1" value="<? echo $rec_address1; ?>">
<input type="hidden" name="rec_address2" value="<? echo $rec_address2; ?>">
<input type="hidden" name="rec_tel1" value="<? echo $rec_tel1; ?>">
<input type="hidden" name="rec_tel2" value="<? echo $rec_tel2; ?>">
<input type="hidden" name="rec_tel3" value="<? echo $rec_tel3; ?>">
<input type="hidden" name="rec_email" value="<? echo $rec_email; ?>">
<input type="hidden" name="out_left" value="<? echo $out_left; ?>">
<input type="hidden" name="in_left" value="<? echo $in_left; ?>">
<input type="hidden" name="note2" value="<? echo $note2; ?>">
<input type="hidden" name="key1" value="<? echo $key1; ?>">
<input type="hidden" name="key2" value="<? echo $key2; ?>">
<input type="hidden" name="page" value="<? echo $page; ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");
require("./get_values.ini");
require("label_by_profile_type.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 患者登録権限のチェック
$reg_auth = check_authority($session,22,$fname);
if($reg_auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 外来未収金額の入力値チェック
if (strlen($out_left) > 0) {
	if (positive_number_validation($out_left) == 0) {
		echo("<script language=\"javascript\">alert(\"外来未収金額は半角数字で入力してください\");</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}

// 入院未収金額の入力値チェック
if (strlen($in_left) > 0) {
	if (positive_number_validation($in_left) == 0) {
		echo("<script language=\"javascript\">alert(\"{$_label_by_profile["IN"][$profile_type]}未収金額は半角数字で入力してください\");</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}

// 登録値の編集
if ($out_left == "") {
	$out_left = null;
}
if ($in_left == "") {
	$in_left = null;
}

// 患者情報の更新
$sql = "update ptifmst set";
$set = array("ptif_req_nm", "ptif_req_zip1", "ptif_req_zip2", "ptif_req_prv", "ptif_req_addr1", "ptif_req_addr2", "ptif_req_tel1", "ptif_req_tel2", "ptif_req_tel3", "ptif_req_email", "ptif_out_unpaid", "ptif_in_unpaid", "ptif_note");
$setvalue = array($rec_name, $rec_post1, $rec_post2, $rec_province, $rec_address1, $rec_address2, $rec_tel1, $rec_tel2, $rec_tel3, $rec_email, $out_left, $in_left, $note2);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
echo(pg_last_error($con));
pg_close($con);
exit;
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 請求画面を再表示
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script language=\"javascript\">location.href=\"patient_claim_setting.php?session=$session&pt_id=$pt_id&key1=$url_key1&key2=$url_key2&page=$page\"</script>");
?>
</body>
