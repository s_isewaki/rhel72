<?php
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;

///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script type='text/javascript'>showLoginPage(window);</script>");
    exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script type='text/javascript'>showLoginPage(window);</script>");
    exit;
}

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script type='text/javascript'>showLoginPage(window);</script>");
    exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);

///-----------------------------------------------------------------------------
// 勤務パターン情報を取得
///-----------------------------------------------------------------------------
$wktmgrp_array = $obj->get_wktmgrp_array();
if (count($wktmgrp_array) <= 0) {
    $err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
}
if ($pattern_id == "") {
    $pattern_id = $wktmgrp_array[0]["id"];
}

///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
///-----------------------------------------------------------------------------
$atdptn_array = $obj->get_atdptn_array($pattern_id);

///-----------------------------------------------------------------------------
//カラー情報（色）を取得
///-----------------------------------------------------------------------------
$font_color_array = $obj->get_font_color_array();
$back_color_array = $obj->get_back_color_array();

///-----------------------------------------------------------------------------
//勤務シフトパターン情報の取得
///-----------------------------------------------------------------------------
$pattern_array = $obj->get_duty_shift_pattern_array($pattern_id, $atdptn_array);
if (count($pattern_array) <= 0) {
    //-------------------------------------------------------------------
    //データがない時
    //-------------------------------------------------------------------
    $wk_atdptn_ptn_id = "";
    $m = 0;
    for ($i = 0; $i < count($atdptn_array); $i++) {
        //-------------------------------------------------------------------
        //休暇の有無判定
        //-------------------------------------------------------------------
        // 休暇を名称ではなくIDで確認
        if ($atdptn_array[$i]["id"] == "10") {
            $wk_atdptn_ptn_id = $atdptn_array[$i]["id"];        //出勤パターンＩＤ
            $wk_atdptn_ptn_name = $atdptn_array[$i]["name"];    //出勤パターン名称
            $wk_count_kbn_gyo = "9999";                         //集計区分(行)  （9999：その他）
            $wk_count_kbn_retu = "9999";                        //集計区分(列)  （9999：その他）
            $wk_font_name = "";                                 //表示文字
            $wk_font_color_id = $font_color_array[0]["color"];  //文字色
            $wk_back_color_id = $back_color_array[0]["color"];  //背景色
            $wk_count_kbn_gyo_name = "";                        //行名
            $wk_count_kbn_retu_name = "";                       //列名
        }
        else {
            //-------------------------------------------------------------------
            //出勤パターン値設定
            //-------------------------------------------------------------------
            $pattern_array[$m]["pattern_id"] = $pattern_id;                     //出勤グループＩＤ
            $pattern_array[$m]["atdptn_ptn_id"] = $atdptn_array[$i]["id"];      //出勤パターンＩＤ
            $pattern_array[$m]["atdptn_ptn_name"] = $atdptn_array[$i]["name"];  //出勤パターン名称
            $pattern_array[$m]["reason"] = " ";             //事由
            $m++;
        }
    }
    //-------------------------------------------------------------------
    //休暇が存在する場合
    //-------------------------------------------------------------------
    if ($wk_atdptn_ptn_id != "") {
        $pattern_array = $obj->setReasonPatternArray2($pattern_array, $pattern_id,
                                            $wk_atdptn_ptn_id, $wk_atdptn_ptn_name,
                                            $wk_count_kbn_gyo, $wk_count_kbn_retu,
                                            $wk_count_kbn_gyo_name, $wk_count_kbn_retu_name,
                                            $wk_font_name,
                                            $wk_font_color_id, $wk_back_color_id);

        // 出勤表の休暇種別等画面の非表示設定を反映
        $arr_reason_display_flag = $obj->get_reason_display_flag();
        for ($j = 0; $j < count($pattern_array); $j++) {
            $tmp_reason_id = $pattern_array[$j]["reason"];
            if ($arr_reason_display_flag["$tmp_reason_id"] != "") {
                $pattern_array[$j]["reason_display_flag"] = $arr_reason_display_flag["$tmp_reason_id"];
            }
        }
    }
}
$data_cnt = count($pattern_array);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜管理｜勤務シフト記号登録</title>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(function() {
    $('input.font_name').bind('click change keydown keypress keyup', change_preview);
    $('select.font_color_id').bind('click change', change_preview);
    $('select.back_color_id').bind('click change', change_preview);
});
function change_preview() {
    var base = $(this).parent().parent().parent();
    var name    = base.find('input.font_name').val();
    var color   = base.find('select.font_color_id').val();
    var bgcolor = base.find('select.back_color_id').val();
    var preview = base.find('td.preview');
    preview.text(name);
    preview.css({color: color, backgroundColor: bgcolor});
}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.preview {
    font-size: 12px;
    font-family: MSゴシック, Osaka, sans-serif;
    padding: 2px 3px;
    text-align: center;
}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 画面遷移／タブ -->
    <!-- ------------------------------------------------------------------------ -->
    <?
        // 画面遷移
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_title($session, $section_admin_auth);           //duty_shift_common.ini
        echo("</table>\n");

        // タブ
        $arr_option = "";
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_tab_menuitem($session, $fname, $arr_option);    //duty_shift_manage_tab_common.ini
        echo("</table>\n");

        // 下線
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- データ不備時 -->
    <!-- ------------------------------------------------------------------------ -->
    <?
        if ($err_msg_1 != "") {
            echo($err_msg_1);
            echo("<br>\n");
        } else {
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 入力エリア -->
    <!-- ------------------------------------------------------------------------ -->
    <form name="sign" action="duty_shift_entity_sign_update_exe.php" method="post">
        <!-- ------------------------------------------------------------------------ -->
        <!-- 勤務パターングループ名 -->
        <!-- 登録ボタン -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="800" border="0" cellspacing="0" cellpadding="2">
        <?
            echo("<tr height=\"22\">\n");
            ///-----------------------------------------------------------------------------
            // 勤務パターングループ名
            ///-----------------------------------------------------------------------------
            echo("<td width=\"25%\" align=\"center\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務パターングループ名</font></td>\n");
            echo("<td width=\"\"><select name=\"pattern_id\" onchange=\"this.form.action='$fname'; this.form.submit();\">\n");
            for($i=0;$i<count($wktmgrp_array);$i++) {
                $wk_id= $wktmgrp_array[$i]["id"];
                $wk_name = $wktmgrp_array[$i]["name"];
                echo("<option value=\"$wk_id\"");
                if ($pattern_id == $wk_id) {
                    echo(" selected");
                }
                echo(">$wk_name\n");
            }
            echo("</select></td>\n");
            ///-----------------------------------------------------------------------------
            // 登録ボタン
            ///-----------------------------------------------------------------------------
            echo("<td colspan=\"2\" align=\"right\"><input type=\"submit\" value=\"登録\"></td>\n");
            echo("</tr>\n");
        ?>
        </table>
        <!-- ------------------------------------------------------------------------ -->
        <!-- パターン一覧（見出し） -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr height="22" bgcolor="#f6f9ff">
        <td width="5%" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
        <td width="18%" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターン</font></td>
        <td width="18%" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計区分(行)</font></td>
        <td width="18%" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計区分(列)</font></td>
        <td align="center" colspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務シフト表示</font></td>
        <td width="7%" align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタンプ<br>非表示</font></td>
        </tr>
        <tr height="22" bgcolor="#f6f9ff">
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文字</font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文字色</font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">背景色</font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プレビュー</font></td>
        </tr>
        <!-- ------------------------------------------------------------------------ -->
        <!-- パターン一覧（データ） -->
        <!-- ------------------------------------------------------------------------ -->
        <?
            for ($i=0; $i<$data_cnt; $i++) {
                // 出勤表の休暇種別等画面の非表示を確認
                if($pattern_array[$i]["reason_display_flag"] == "f") {
                    continue;
                }
                echo("<tr height=\"22\">\n");

                ///-----------------------------------------------------------------------------
                // 出力値設定
                ///-----------------------------------------------------------------------------
                for ($k = 0; $k < count($atdptn_array); $k++) {
                    if ($pattern_array[$i]["atdptn_ptn_id"] == $atdptn_array[$k]["id"]) {
                        $gyo_id = $pattern_array[$i]["count_kbn_gyo"];
                        $retu_id = $pattern_array[$i]["count_kbn_retu"];
                        $font_name = $pattern_array[$i]["font_name"];
                        $font_color_id = $pattern_array[$i]["font_color_id"];
                        $back_color_id = $pattern_array[$i]["back_color_id"];
                        $stamp_disp_flg = $pattern_array[$i]["stamp_disp_flg"];
                        break;
                    }
                }

                // 表示順
                echo("<td width=\"5%\" align=\"center\">\n");
                echo("<select id=\"order_no_$i\" name=\"order_no_$i\">\n");

                for ($k=0; $k<=$data_cnt; $k++) {
                    // selected
                    if ($i + 1 == $k) {
                        $selected = " selected";
                    } else {
                        $selected = "";
                    }
                    $order_val = $k;
                    echo("<option value=\"$order_val\" $selected>$order_val</option>\n");
                }

                echo("</select>");
                echo("</td>\n");
                ///-----------------------------------------------------------------------------
                // 出勤パターン
                ///-----------------------------------------------------------------------------
                $wk = $pattern_array[$i]["atdptn_ptn_name"];
                // 半有半公等
                if (($pattern_array[$i]["reason"] >= "45" && //44
                     $pattern_array[$i]["reason"] <= "47")) {
                    $wk = $pattern_array[$i]["reason_name"];
                } else
                // 34:休暇（事由無し）は事由非表示
                if ($pattern_array[$i]["reason_name"] != "" && $pattern_array[$i]["reason"] != "34") {
                    $wk .= "(" . $pattern_array[$i]["reason_name"] . ")";
                }
                echo("<td width=\"18%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
                ///-----------------------------------------------------------------------------
                // 集計区分(行)
                ///-----------------------------------------------------------------------------
                echo("<td width=\"18%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
                echo("<select name=\"count_kbn_gyo_$i\">");
                //休暇の場合は、optionを休暇のみにする 2008/06/06
                if($pattern_array[$i]["atdptn_ptn_id"] != "10") {
                    //その他
                    echo("<option value=\"9999\">その他 \n");
                }
                //出勤パターン一覧表示
                for ($k=0; $k<count($atdptn_array); $k++) {
                    //休暇の場合は、optionを休暇のみにする 2008/06/06
                    if ($pattern_array[$i]["atdptn_ptn_id"] == "10" &&
                        $atdptn_array[$k]["id"] != "10") {
                        continue;
                    }
                    $wk_id = $atdptn_array[$k]["id"];
                    $wk_name = $atdptn_array[$k]["name"];
                    echo("<option value=\"$wk_id\"");
                    if ($gyo_id == $wk_id) {
                        echo(" selected");
                    }
                    echo(">$wk_name \n");
                }
                echo("</select>\n");
                echo("</font></td>\n");
                ///-----------------------------------------------------------------------------
                // 集計区分(列)
                ///-----------------------------------------------------------------------------
                echo("<td width=\"18%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
                echo("<select name=\"count_kbn_retu_$i\">");
                //休暇の場合は、optionを休暇のみにする 2008/06/06
                if($pattern_array[$i]["atdptn_ptn_id"] != "10") {
                    //その他
                    echo("<option value=\"9999\">その他 \n");
                }
                //出勤パターン一覧表示
                for ($k=0; $k<count($atdptn_array); $k++) {
                    //休暇の場合は、optionを休暇のみにする 2008/06/06
                    if ($pattern_array[$i]["atdptn_ptn_id"] == "10" &&
                        $atdptn_array[$k]["id"] != "10") {
                        continue;
                    }
                    $wk_id = $atdptn_array[$k]["id"];
                    $wk_name = $atdptn_array[$k]["name"];
                    echo("<option value=\"$wk_id\"");
                    if ($retu_id == $wk_id) {
                        echo(" selected");
                    }
                    echo(">$wk_name \n");
                }
                echo("</select>\n");
                echo("</font></td>\n");

                ///-----------------------------------------------------------------------------
                // 表示文字
                ///-----------------------------------------------------------------------------
                print('<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">');
                printf('<input class="font_name" name="font_name_%s" type="text" value="%s" size="12" maxlength="6" style="ime-mode:active;">', $i, h($font_name));
                print('</font></td>');

                ///-----------------------------------------------------------------------------
                // 文字色
                ///-----------------------------------------------------------------------------
                print('<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">');
                printf('<select class="font_color_id" name="font_color_id_%s">', $i);
                foreach ($font_color_array as $c) {
                    printf('<option %s value="%s" style="background-color:%s;color:%s;">%s</option>',
                        ($font_color_id === $c['color'] ? 'selected' : ''),
                        $c['color'], $c['color'], $c['fcolor'], $c['name']);
                }
                echo("</select>");
                echo("</font></td>");

                ///-----------------------------------------------------------------------------
                // 背景色
                ///-----------------------------------------------------------------------------
                print('<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">');
                printf('<select class="back_color_id" name="back_color_id_%s">', $i);
                foreach ($back_color_array as $c) {
                    printf('<option %s value="%s" style="background-color:%s;color:%s;">%s</option>',
                        ($back_color_id === $c['color'] ? 'selected' : ''),
                        $c['color'], $c['color'], $c['fcolor'], $c['name']);
                }
                echo("</select>");
                echo("</font></td>");

                ///-----------------------------------------------------------------------------
                // プレビュー
                ///-----------------------------------------------------------------------------
                printf('<td class="preview" style="color:%s; background-color:%s;">%s</td>', h($font_color_id), h($back_color_id), h($font_name));

		///-----------------------------------------------------------------------------
		// スタンプ非表示
		///-----------------------------------------------------------------------------
		echo("<td width=\"\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($stamp_disp_flg == 'f'){
			$display_flag_checked = " checked ";
		}
		else {
			$display_flag_checked = "";
		}

		echo("<input type=\"checkbox\" name=\"stamp_disp_flg_{$i}\" {$display_flag_checked}>");
		echo("</font></td>\n");

                echo("</tr>\n");
            }
        ?>
        </table>
        <!-- 登録ボタン -->
        <table width="800" border="0" cellspacing="0" cellpadding="2">
        <?
            echo("<tr height=\"22\">\n");
            echo("<td colspan=\"2\" align=\"right\"><input type=\"submit\" value=\"登録\"></td>\n");
            echo("</tr>\n");
        ?>
        </table>
        <!-- ------------------------------------------------------------------------ -->
        <!-- ＨＩＤＤＥＮ -->
        <!-- ------------------------------------------------------------------------ -->
        <?
            echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
            echo("<input type=\"hidden\" name=\"data_cnt\" value=\"$data_cnt\">\n");
            for ($i=0; $i<$data_cnt; $i++) {
                //出勤パターンＩＤ
                $wk = $pattern_array[$i]["atdptn_ptn_id"];
                echo("<input type=\"hidden\" name=\"atdptn_ptn_id_$i\" value=\"$wk\">\n");
                //事由
                $wk = $pattern_array[$i]["reason"];
                echo("<input type=\"hidden\" name=\"reason_$i\" value=\"$wk\">\n");
                //連携コード
                $wk = $pattern_array[$i]["sfc_code"];
                echo("<input type=\"hidden\" name=\"sfc_code_$i\" value=\"$wk\">\n");
                //分担表
                $wk = $pattern_array[$i]["allotment_order_no"];
                echo("<input type=\"hidden\" name=\"allotment_order_no_$i\" value=\"$wk\">\n");
                //看護日誌出力区分
                $wk = $pattern_array[$i]["output_kbn"];
                echo("<input type=\"hidden\" name=\"output_kbn_$i\" value=\"$wk\">\n");
            }
        ?>
    </form>

    <?
        }
    ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
