<?php
/*
 * work_admin_csv_download_common.php
 * 出勤表と勤務シフト作成から共通で使用するため関数を当ファイル内に定義する
 */
require_once("show_timecard_common.ini");
require_once("show_attendance_pattern.ini");
require_once("holiday.php");
require_once("timecard_common_class.php");
require_once("atdbk_common_class.php");
require_once("date_utils.php");
require_once("work_admin_csv_common.ini");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("duty_shift_total_common_class.php");
require_once("atdbk_info_common.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
//require_once("work_admin_csv_download_common.php");
require_once("settings_common.php"); //遅刻早退控除 20131212
require_once("ovtm_class.php");

function get_attendance_book_csv($con, $emp_id_list,
    $start_date_fulltime, $end_date_fulltime,
    $start_date_parttime, $end_date_parttime,
    $c_yyyymm, $ovtmscr_tm, $fname, $arr_reason_id, $arr_disp_flg,
    $group_id, $atdbk_common_class, $arr_csv_list, $header_flg, $arr_job_id_list, $closing, $closing_month_flg, $arr_emp_id_layout, $timecard_bean, $shift_group_id, $csv_layout_id, $obj_hol_hour, $calendar_name_class, $from_flg, $encode_flg, $total_flg=1, $org_yyyymm) {


    //ワークフロー関連共通クラス
    $atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

    $obj_total = new duty_shift_total_common_class($con, $fname);
    //残業申請関連クラス
    $ovtm_class = new ovtm_class($con, $fname);

	// 雇用区分 20141209
	$list_duty_form_type = array(
		"1" => "正職員",
		"2" => "再雇用職員",
		"3" => "嘱託職員",
		"4" => "臨時職員",
		"5" => "パート職員"
	);

    //レイアウト画面からの場合
    if ($emp_id_list != "") {
        $emp_ids = explode(",", $emp_id_list);
    } else {
        //検索画面からレイアウト指定でダウンロードの場合
        $emp_ids = $arr_emp_id_layout;
    }

    if ($start_date_fulltime <= $start_date_parttime) {
        $wk_start_date = $start_date_fulltime;
    } else {
        $wk_start_date = $start_date_parttime;
    }
    if ($end_date_fulltime >= $end_date_parttime) {
        $wk_end_date = $end_date_fulltime;
    } else {
        $wk_end_date = $end_date_parttime;
    }

    //実際の指定開始日 20130425
    $wf_start_date = $wk_start_date;

    //集計グループ
    if ($total_flg == 2) {
        $wk_emp_id = array();
        foreach ($emp_ids as $tmp_emp_id_key) {
            list($tmp_group_id2, $tmp_emp_id) = explode("_", $tmp_emp_id_key);
            $wk_emp_ids[] = $tmp_emp_id;
        }
    }
    else {
        $wk_emp_ids = $emp_ids;
    }

    //グループ毎のテーブルから勤務時間を取得する
    $timecard_common_class = new timecard_common_class($con, $fname, $wk_emp_ids, $wk_start_date, $wk_end_date);

    /*見出しを設定から取得する、下は固定だった時の見出し例
        $buf = "職員ID,職員氏名,要勤務日数,出勤日数,休日出勤,代出,振出,代休,振休,年休・有休,欠勤,その他,遅刻,早退,基準時間,稼働時間,残業時間,深夜勤務,法定内勤務,法定内残業,法定外残業,休日勤務";
    */
    //休日出勤日数勤務パターン設定 20101013
    $arr_ptn_id = get_ptn_id_list($con, $fname, $csv_layout_id, $from_flg);
    //所定時間の設定 20101013
    $arr_config = get_timecard_csv_config($con, $fname, $csv_layout_id, $from_flg);
    $fixed_set_pattern_id = $arr_config["fixed_set_pattern_id"];
    $fixed_set_atdptn_ptn_id = $arr_config["fixed_set_atdptn_ptn_id"];
    $fixed_set_hour = $arr_config["fixed_set_hour"];
    $fixed_set_minute = $arr_config["fixed_set_minute"];
    $fixed_sub_time = intval($fixed_set_hour) * 60 + intval($fixed_set_minute);
    $total_item_title = $arr_config["total_item_title"];
    for ($i=2; $i<=8; $i++) {
        $varname = "total_item_title".$i;
        $$varname = $arr_config["$varname"];
    }
    $time_format = $arr_config["time_format"];
    if ($time_format == "") {
        $time_format = "1";
    }
    $output_target = $arr_config["output_target"];
    //見出し
    $arr_items = get_items($con, $fname, $shift_group_id, $from_flg);
    if ($header_flg == "t") {
        $arr_itemname = array();
        for ($i=0; $i<count($arr_items); $i++) {
            $tmp_id = $arr_items[$i]["id"];
            $tmp_name = $arr_items[$i]["name"];
            $arr_itemname[$tmp_id] = $tmp_name;
        }
        $arr_itemname["9_0"] = "";  //空セル
        $arr_itemname["2_74"] = ($total_item_title != "") ? $total_item_title : "集計項目１";   //集計項目１のヘッダータイトル 20101102
        $arr_itemname["2_69"] = ($total_item_title2 != "") ? $total_item_title2 : "集計項目２"; //集計項目２のヘッダータイトル 20101208
        $arr_itemname["2_68"] = ($total_item_title3 != "") ? $total_item_title3 : "集計項目３";
        $arr_itemname["2_67"] = ($total_item_title4 != "") ? $total_item_title4 : "集計項目４";
        $arr_itemname["2_66"] = ($total_item_title5 != "") ? $total_item_title5 : "集計項目５";
        $arr_itemname["2_65"] = ($total_item_title6 != "") ? $total_item_title6 : "集計項目６";
        $arr_itemname["2_64"] = ($total_item_title7 != "") ? $total_item_title7 : "集計項目７";
        $arr_itemname["2_63"] = ($total_item_title8 != "") ? $total_item_title8 : "集計項目８";

        $arr_title = array();
        for ($i=0; $i<count($arr_csv_list); $i++) {
            $arr_title[] = $arr_itemname[$arr_csv_list[$i]];
        }
        $buf = join(",", $arr_title);

        $buf .= "\r\n";
    } else {
        $buf = "";
    }
    //画面用
    $arr_buf = array();
    $arr_buf[] = $arr_title;

    // 端数処理情報を取得
    $sql = "select * from timecard";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0) {
        for ($i = 1; $i <= 5; $i++) {
            $var_name = "fraction$i";
            $var_name_min = "fraction{$i}_min";
            $$var_name = pg_fetch_result($sel, 0, "fraction$i");
            $$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
        }
        for ($i = 1; $i <= 5; $i++) {
            $var_name = "others$i";
            $$var_name = pg_fetch_result($sel, 0, "others$i");
        }
        $rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
        $rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
        $rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
        //時間給者の休憩時間追加 20100929
        $rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
        $rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
        $rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
        $rest7 = intval(pg_fetch_result($sel, 0, "rest7"));

        $night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
        //当直手当
        $night_allowance       = intval(pg_fetch_result($sel, 0, "night_allowance"));
        $night_allowance_sat   = intval(pg_fetch_result($sel, 0, "night_allowance_sat"));
        $night_allowance_hol   = intval(pg_fetch_result($sel, 0, "night_allowance_hol"));
        $delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
        $early_leave_time_flg = pg_fetch_result($sel, 0, "early_leave_time_flg");
        $modify_flg = pg_fetch_result($sel, 0, "modify_flg");
    } else {
        for ($i = 1; $i <= 5; $i++) {
            $var_name = "fraction$i";
            $var_name_min = "fraction{$i}_min";
            $$var_name = "";
            $$var_name_min = "";
        }
        for ($i = 1; $i <= 5; $i++) {
            $var_name = "others$i";
            $$var_name = "";
        }
        $rest1 = 0;
        $rest2 = 0;
        $rest3 = 0;
        //時間給者の休憩時間追加 20100929
        $rest4 = 0;
        $rest5 = 0;
        $rest6 = 0;
        $rest7 = 0;

        $night_workday_count = "";
        $night_allowance       = 0;
        $night_allowance_sat   = 0;
        $night_allowance_hol   = 0;
        $delay_overtime_flg = "1";
        $early_leave_time_flg = "2";
        $modify_flg = "t";
    }
    //週40時間超過分は計算しないを固定とする 20100525（ここでは未使用 20130425追加分とは別）
    $overtime_40_flg = "2";

    //集計処理の設定を「集計しない」の固定とする 20100910
    $others1 = "1"; //勤務時間内に外出した場合
    $others2 = "1"; //勤務時間外に外出した場合
    $others3 = "1"; //時間給者の休憩の集計
    $others4 = "1"; //遅刻・早退に休憩が含まれた場合
    $others5 = "2"; //早出残業時間の集計

    // 「退勤後復帰」ボタン表示フラグを取得
    $sql = "select ret_btn_flg from config";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

    // 指定範囲の勤務日種別を取得
    $wd = date("w", to_timestamp($wk_start_date));
    while ($wd != 0) {
        $wk_start_date = last_date($wk_start_date);
        $wd = date("w", to_timestamp($wk_start_date));
    }

    //翌月初日までデータを取得 20130308
    $wk_end_date_nextone = next_date($wk_end_date);
    $arr_type = array();
    $sql = "select date, type from calendar";
    $cond = "where date >= '$wk_start_date' and date <= '$wk_end_date_nextone'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $date = $row["date"];
        $arr_type[$date] = $row["type"];
    }
    //勤務集計表の項目 20101019
    $tra_full = array();
    $tra_part = array();
    $tra_full_chk_group = array();
    $tra_part_chk_group = array();

    $total_conf = $obj_total->get_duty_shift_total_config($shift_group_id);
    $parttime_split_hour = ($total_conf["parttime_split_hour"] == "") ? "18" : $total_conf["parttime_split_hour"];
    //交通費
    $tra_flg = (in_array("2_77", $arr_csv_list)) ? true : false;

    //当直
    $night_allow_flg = (in_array("3_0", $arr_csv_list) || in_array("3_1", $arr_csv_list) || in_array("3_2", $arr_csv_list) || in_array("5_0", $arr_csv_list)) ? true : false;
    //非常勤18:00まで、以降の分割集計
    $split_time_flg = (in_array("2_78", $arr_csv_list) || in_array("2_79", $arr_csv_list)) ? true : false;

    $allow_flg = false;
    $ptn_cnt_flg = false;
    //手当て、勤務パターン回数
    for ($i=0;$i<count($arr_csv_list);$i++) {
        $wk_kind = substr($arr_csv_list[$i], 0, 1);
        switch ($wk_kind) {
            case "4": //手当
            case "6": //手当金額
                $allow_flg = true;
                break;
            case "7": //勤務パターン手当金額
            case "8": //勤務パターン回数
                $ptn_cnt_flg = true;
                break;
        }
        //どちらも見つかったら終了
        if ($allow_flg &&   $ptn_cnt_flg) {
            break;
        }
    }


    //公休と判定する日設定 20100810
    $arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");

    //集計項目取得 20101102
    if (in_array("2_74", $arr_csv_list)) {
        $arr_csv_total_item = get_csv_total_item($con, $fname, $csv_layout_id, $from_flg);

    } else {
        $arr_csv_total_item = array();
    }

    //集計項目２取得
    $wk_get_flg = false;
    for ($i=2; $i<=8; $i++) {
        $wk_code = "2_".(69+2-$i);
        if (in_array($wk_code, $arr_csv_list)) {
            $wk_get_flg = true;
            break;
        }
    }
    if ($wk_get_flg) {
        $arr_csv_total_item2 = get_csv_total_item_all($con, $fname, $csv_layout_id, $from_flg);

    } else {
        $arr_csv_total_item2 = array();
    }

    //CSV集計項目設定取得
    $arr_total_setting = get_timecard_csv_total_setting($con, $fname, $csv_layout_id, "");

    //個人別所定時間を優先するための対応 20120309
    $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $wk_end_date);

    //週40時間を表示するか 20130425
    $week_forty_flg = (in_array("11_40", $arr_csv_list)) ? true : false;

    $arr_shift_group_name = array();
    //"1_7"シフトグループ名
    if ($from_flg == "2" && in_array("1_7", $arr_csv_list)) {

        if ($shift_group_id == "-") {
            if ($total_flg == 1) {
                list($start_ym, $end_ym) = get_month_info($con, $fname, $shift_group_id, $start_date_fulltime, $end_date_fulltime);
                $cond_kikan = " cast(duty_shift_plan_staff.duty_yyyy as varchar)||lpad(cast(duty_shift_plan_staff.duty_mm as varchar), 2, '0') >= '$start_ym' and cast(duty_shift_plan_staff.duty_yyyy as varchar)||lpad(cast(duty_shift_plan_staff.duty_mm as varchar), 2, '0') <= '$end_ym'";
                $sql_gname = "select distinct on (duty_shift_plan_staff.emp_id) duty_shift_plan_staff.emp_id, duty_shift_group.group_name from duty_shift_plan_staff left join duty_shift_group on duty_shift_group.group_id = duty_shift_plan_staff.group_id ";
                $sql_gname .= " where ($cond_kikan) ";
                $sql_gname .= " order by duty_shift_plan_staff.emp_id, duty_shift_plan_staff.duty_yyyy, duty_shift_plan_staff.duty_mm, duty_shift_group.order_no ";
                $sel_gname = select_from_table($con, $sql_gname, "", $fname);
                if ($sel_gname == 0) {
                    pg_close($con);
                    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                    echo("<script language='javascript'>showErrorPage(window);</script>");
                    exit;
                }
                while ($row = pg_fetch_array($sel_gname))
                {
                    $wk_emp_id = $row["emp_id"];
                    $arr_shift_group_name[$wk_emp_id] = $row["group_name"];
                }
            }
            else {
                $sql_gname = "select group_id, group_name from duty_shift_group ";
                $sel_gname = select_from_table($con, $sql_gname, "", $fname);
                if ($sel_gname == 0) {
                    pg_close($con);
                    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                    echo("<script language='javascript'>showErrorPage(window);</script>");
                    exit;
                }
                $arr_shift_group_name = array();
                while ($row = pg_fetch_array($sel_gname))
                {
                    $arr_shift_group_name[$row["group_id"]] = $row["group_name"];
                }
            }
        }
        else {
            $arr_group_info = get_group_info($con, $fname, $shift_group_id);
        }

    }

    //
    $month_chk_date = date("Ymd", strtotime("+1 month",date_utils::to_timestamp_from_ymd($wf_start_date) ));
    if ($wk_end_date < $month_chk_date) {
        $one_month_flg = true;
    }
    else {
        $one_month_flg = false;
    }
    //遅刻早退控除
    if (in_array("12_0", $arr_csv_list)) {
        $late_fraction = get_settings_value("kintai_late_fraction", "2");
        $late_fraction_min = get_settings_value("kintai_late_fraction_min", "2");
    }
    // 選択された全職員をループ
    foreach ($emp_ids as $tmp_emp_id_key) {

        //遅刻早退控除
        if (in_array("12_0", $arr_csv_list)) {
            $late_early_total = 0;
            $arr_late_data = get_late_early_leave($con, $fname, $tmp_emp_id_key, $wk_start_date, $wk_end_date);
        }
        //1ヶ月を超える場合のチェック用
        $month_chk_date = date("Ymd", strtotime("+1 month",date_utils::to_timestamp_from_ymd($wf_start_date) ));
        $tmp_group_id2 = "";
        if ($total_flg == 1 || $from_flg != "2") {
            $tmp_emp_id = $tmp_emp_id_key;
        }
        else {
            list($tmp_group_id2, $tmp_emp_id) = explode("_", $tmp_emp_id_key);
            $arr_duty_shift_staff_history = get_duty_shift_staff_history($con, $fname, $tmp_emp_id, $wf_start_date);
        }
        $hoteigai_sums_month = array_fill(0, 2, 0); //index 0:法定外残業（深夜・休日残業除外） 1:法定外深夜残業（休日除外）複数月ある場合用
        $over_fix_time_total = array_fill(0,2,0);
        $over_fix_time = array_fill(0,2,0); //index 0:60時間超法定外残業 1:60時間深夜残業

        //＝＝＝＝＝＝＝＝＝＝＝＝週40時間超残業対応 20130425＝＝＝＝＝＝＝＝＝＝＝＝＝
        $tmp_count = 1;
        if ($week_forty_flg){
            $tmp_week = ($timecard_bean->over_disp_forty_week == 0) ? "日" : "月";
            $tmp_week_start = get_weekday(to_timestamp($wf_start_date));
            if ($tmp_week != $tmp_week_start){
                //起算曜日の日付取得
                for ($i = 1; $i < 7; $i++){
                    $buf_start_date = date("Ymd", strtotime("-$i day",date_utils::to_timestamp_from_ymd($wf_start_date) ));
                    $buf_week = get_weekday(to_timestamp($buf_start_date));
                    if ($tmp_week == $buf_week){
                        break;
                    }
                }

                $buf_end_date = date("Ymd", strtotime("-1 day",date_utils::to_timestamp_from_ymd($wf_start_date) ));    //前月の開始

                $reserve_start_date = $wk_start_date;
                $reserve_end_date = $wk_end_date;

                $tmp_count +=1;
            }
        }

        $previous_month_time = "";
        for ($lp = 1; $lp <= $tmp_count; $lp++)
        {
            //週40時間超残業時間(前月の部分データ取得) 20130425
            $buf_first_flg = false;
            if ($tmp_count>1){
                if ($lp == 1){
                    $wk_start_date = $buf_start_date;
                    $wk_end_date = $buf_end_date;
                    $buf_first_flg = true;
                }else{
                    $wk_start_date = $reserve_start_date;
                    $wk_end_date = $reserve_end_date;
                }
            }

            list($dbg_st_msec, $dbg_st_sec) = explode(" ", microtime());
            $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($tmp_emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

            // ************ oose add start *****************
            // 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
            // 給与支給区分・祝日計算方法、残業管理を取得
            // 職員ID・職員氏名を取得 職種IDも取得
            $sql = "select a.emp_personal_id, a.emp_lt_nm, a.emp_ft_nm, a.emp_job, b.class_nm, c.atrb_nm, d.dept_nm, e.job_nm, f.duty_form, f.wage, f.hol_minus, f.no_overtime, f.specified_time, f.am_time, f.pm_time, g.group_id, f.unit_price5, f.unit_price6, f.unit_price7, f.commuting_distance, f.commuting_type, h.st_nm, f.salary_id, a.emp_sex, a.emp_birth, a.emp_join, f.duty_form_type, f.org_corp_cd from empmst a ";
            $sql .= "left join classmst b on a.emp_class = b.class_id and not b.class_del_flg ";
            $sql .= "left join atrbmst c on a.emp_attribute  = c.atrb_id and not c.atrb_del_flg ";
            $sql .= "left join deptmst d on a.emp_dept = d.dept_id and not d.dept_del_flg ";
            $sql .= "left join jobmst e on a.emp_job = e.job_id and not e.job_del_flg ";
            $sql .= "left join empcond f on a.emp_id = f.emp_id ";
            $sql .= "left join duty_shift_staff g on a.emp_id = g.emp_id ";
            $sql .= "left join stmst h on a.emp_st = h.st_id ";
            $cond = "where a.emp_id = '$tmp_emp_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            if (pg_num_rows($sel) > 0) {
                $tmp_emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
                $tmp_emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
                $tmp_emp_job_id = pg_fetch_result($sel, 0, "emp_job");
                $tmp_class_nm = pg_fetch_result($sel, 0, "class_nm");
                $tmp_atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
                $tmp_dept_nm = pg_fetch_result($sel, 0, "dept_nm");
                $tmp_job_nm = pg_fetch_result($sel, 0, "job_nm");
                $duty_form = pg_fetch_result($sel, 0, "duty_form");
                $wage = pg_fetch_result($sel, 0, "wage");
                $hol_minus = pg_fetch_result($sel, 0, "hol_minus");
                $no_overtime = pg_fetch_result($sel, 0, "no_overtime");
                $tmp_group_id = pg_fetch_result($sel, 0, "group_id");
                $tmp_main_group_id = $tmp_group_id; //管理帳票の確認用
                $empcond_specified_time = pg_fetch_result($sel, 0, "specified_time");
                $unit_price5 = pg_fetch_result($sel, 0, "unit_price5");
                $unit_price6 = pg_fetch_result($sel, 0, "unit_price6"); //20150626
                $unit_price7 = pg_fetch_result($sel, 0, "unit_price7");
                $commuting_distance = pg_fetch_result($sel, 0, "commuting_distance");
                $commuting_type = pg_fetch_result($sel, 0, "commuting_type");
                $paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
                $am_time = pg_fetch_result($sel, 0, "am_time");
                $pm_time = pg_fetch_result($sel, 0, "pm_time");
                if ($from_flg == "2" && in_array("1_7", $arr_csv_list)) {
                    if ($shift_group_id == "-") {
                        if ($total_flg == 1) {
                            $tmp_group_name = $arr_shift_group_name[$tmp_emp_id];
                        }
                        else {
                            $tmp_group_name = $arr_shift_group_name[$tmp_group_id2];
                        }
                    }
                    else {
                        $tmp_group_name = $arr_group_info[0]["group_name"];
                    }
                }
                $tmp_st_nm = pg_fetch_result($sel, 0, "st_nm");
                $salary_id = pg_fetch_result($sel, 0, "salary_id");
                $emp_sex_cd = pg_fetch_result($sel, 0, "emp_sex"); //追加 20141209
                $emp_birth = pg_fetch_result($sel, 0, "emp_birth"); //追加 20141209
                $emp_join = pg_fetch_result($sel, 0, "emp_join"); //追加 20141209
                $duty_form_type = pg_fetch_result($sel, 0, "duty_form_type"); //追加 20141209
                $org_corp_cd = pg_fetch_result($sel, 0, "org_corp_cd"); //追加 20150706
            } else {
                $tmp_emp_personal_id = "";
                $tmp_emp_name = "";
                $tmp_emp_job_id = "";
                $tmp_class_nm = "";
                $tmp_atrb_nm = "";
                $tmp_dept_nm = "";
                $tmp_job_nm = "";
                $duty_form = "";
                $wage = "";
                $hol_minus = "";
                $no_overtime = "";
                $tmp_group_id = "";
                $empcond_specified_time = "";
                $unit_price5 = "";
                $unit_price6 = "";
                $unit_price7 = "";
                $commuting_distance = "";
                $paid_specified_time = "";
                $am_time = "";
                $pm_time = "";
                $tmp_group_name = "";
                $tmp_st_nm = "";
                $salary_id = "";
                $emp_sex_cd = "";
                $emp_birth = "";
                $emp_join = "";
                $duty_form_type = "";
                $org_corp_cd = "";
            }
            if ($duty_form == "1") {
                $tmp_duty_form = "常勤";
            } elseif ($duty_form == "2") {
                $tmp_duty_form = "非常勤";
            } elseif ($duty_form == "3") {
                $tmp_duty_form = "短時間正職員";
            } else {
                $tmp_duty_form = "";
            }
            if ($emp_sex_cd == "1") {
                $tmp_emp_sex = "男性";
            }
            elseif ($emp_sex_cd == "2") {
                $tmp_emp_sex =  "女性";
            }
            else {
                $tmp_emp_sex =  "未設定";
            }
            if (trim($emp_birth) != "") {
            	$tmp_emp_birth = date("Y/n/j", date_utils::to_timestamp_from_ymd($emp_birth));
            } else {
                $tmp_emp_birth = "";
            }
            if (trim($emp_join) != "") {
            	$tmp_emp_join = date("Y/n/j", date_utils::to_timestamp_from_ymd($emp_join));
            } else {
                $tmp_emp_join = "";
            }
            if ($duty_form_type != "") {
                $tmp_duty_form_type = $list_duty_form_type[$duty_form_type];
            } else {
                $tmp_duty_form_type = "";
            }

            //勤務シフト作成からの場合
            if ($from_flg == 2) {
                if ($shift_group_id != "-") {
                    $tmp_group_id = $shift_group_id;
                }
                elseif ($tmp_group_id2 != "") {
                    $tmp_group_id = $tmp_group_id2;
                }
            }
            //交通費設定
            if ($tra_flg) {
                //グループ別に情報を保存する
                if ($duty_form != "2") {
                    if (!in_array($tmp_group_id, $tra_full_chk_group)) {
                        $tra_full[$tmp_group_id] = $obj_total->get_duty_shift_total_tra_ptn($tmp_group_id);
                        $tra_full_chk_group[] = $tmp_group_id;
                    }
                } elseif ($duty_form == "2") {
                    if (!in_array($tmp_group_id, $tra_part_chk_group)) {
                        $tra_part[$tmp_group_id] = $obj_total->get_duty_shift_total_ptn($tmp_group_id, 3);
                        $tra_part_chk_group[] = $tmp_group_id;
                    }
                }
            }
            $tra_cnt = 0;

            $hol_work = 0;  //休日出勤
            $work_time1 = 0;    //18:00まで
            $work_time2 = 0;    //18:00以降
            $fixed_set_cnt = 0; //所定時間差引き回数
            $retention_time = 0; //滞留時間
            $return_count_total = 0; //呼出回数 20121109
            $return_time_total = 0; //呼出時間 20121109

            //週40時間超残業 20130425
            if ($buf_first_flg){
                $c_yyyymm = substr($buf_start_date, 0, 6);
                $start_date = $buf_start_date;
                $end_date = $buf_end_date;
            }else{
                if ($duty_form == 2) {
                    //非常勤
                    $c_yyyymm = substr($start_date_parttime, 0, 6);
                    $start_date = $start_date_parttime;
                    $end_date = $end_date_parttime;
                } else {
                    //常勤
                    $c_yyyymm = substr($start_date_fulltime, 0, 6);
                    $start_date = $start_date_fulltime;
                    $end_date = $end_date_fulltime;
                }
            }

            //翌月初日までデータを取得(実績用) 20130308
            $wk_end_date_nextone2 = next_date($end_date);
            // ************ oose add end *****************
            //一括修正申請情報
            $arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($tmp_emp_id, $start_date, $end_date);

            //所定労働時間履歴を使用 20121127
            if ($empcond_specified_time != "") {
                $paid_specified_time = $empcond_specified_time;
                $day1_time = $empcond_specified_time;
                $empcond_spec_time_flg = true;
            }
            else {
                $arr_timehist = $calendar_name_class->get_calendarname_time($start_date);
                $day1_time = $arr_timehist["day1_time"];
                $am_time = $arr_timehist["am_time"];
                $pm_time = $arr_timehist["pm_time"];
                $empcond_spec_time_flg = false;
            }

            // 変則労働適用情報を配列で取得
            $arr_irrg_info = get_irrg_info_array($con, $tmp_emp_id, $fname);

//常に締めデータ以外から集計 20141106
            //追加残業項目がある場合は、締めデータ以外から集計する 20100917 残業時間（深夜残業・休日残業除外）,深夜残業時間,休日残業時間
            //if (in_array("2_87", $arr_csv_list) || in_array("2_88", $arr_csv_list) || in_array("2_89", $arr_csv_list) || in_array("2_80", $arr_csv_list) || in_array("2_84", $arr_csv_list) || in_array("2_85", $arr_csv_list)) {
                $tmp_closed = false;
            //} else {

                // 対象年月の締め状況を取得
            //    $sql = "select count(*) from atdbkclose";
            //    $cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
            //    $sel = select_from_table($con, $sql, $cond, $fname);
            //    if ($sel == 0) {
            //        pg_close($con);
            //        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            //        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            //        exit;
            //    }
            //    $tmp_closed = (pg_fetch_result($sel, 0, 0) > 0);
            //}

            //休日出勤と判定する日設定取得 20091222
            $arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $tmp_emp_id);

            // 集計結果格納用配列を初期化
            $idx = 21 + count($arr_allow);
            $sums = array_fill(0, $idx, 0);
            // 追加事由分
            $add_sums = array_fill(0, count($arr_reason_id), 0);

            $hotei_sums = array_fill(0, 4, 0);
            // 残業時間
            $zan_sums = array_fill(0, 4, 0); //index 0:残業時間（深夜残業・休日残業除外） 1:深夜残業 2:休日残業 3:休日深夜残業

            // 法定外残業時間 20101206
            $hoteigai_sums = array_fill(0, 8, 0); //index 0:法定外残業（深夜・休日残業除外） 1:法定外深夜残業（休日除外） 2:法定外休日残業（深夜除外） 3:法定外休日深夜残業 4:休日勤務手当時間 5:休日勤務手当時間（深夜） 6:６０時間超計算用休日残業 7:６０時間超計算用休日深夜
            $hoteigai_sums_month = array_fill(0, 2, 0); //index 0:法定外残業（深夜・休日残業除外） 1:法定外深夜残業（休日除外）複数月ある場合用
            $hoteigai_sums_hol_month = array_fill(0, 2, 0); //index 0:法定外休日残業（深夜除外） 1:法定外休日深夜残業

            //有休換算日数計算用 20110204 idx 0:半日以外 1:午前 2:午後 3:時間有休 4:有休日数×所定労働時間（履歴対応）
            $paid_sums = array_fill(0, 5, 0);

            // 規定時間超残業 20130405
            $sixty_value = 3600;
            //休日対応
            $over_fix_time = array_fill(0,4,0); //index 60時間超(0:法定外残業 1:深夜残業 2:休日残業 3:休日深夜) 20141021
            $over_fix_time_total = array_fill(0,4,0); //index 上と同じ 複数月ある場合用 20141021

            //月45時間超残業 20150715
            $fortyfive_value = 2700;
            //休日対応
            $over_fix_time2 = array_fill(0,4,0); //index 45時間超(0:法定外残業 1:深夜残業 2:休日残業 3:休日深夜)
            $over_fix_time_total2 = array_fill(0,4,0); //index 上と同じ 複数月ある場合用


            //週40時間超えた残業 20130425
            $forty_last_day = ($timecard_bean->over_disp_forty_week == 0) ? "土" : "日";
            $week_forty_value = 2400;
            $week_forty_overtime_total = 0;
            $tmp_arr_week_forty = 0;

            // 締め未済の場合
            if (!$tmp_closed) {

                // 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
                $tmp_date = $start_date;

                $wd = date("w", to_timestamp($tmp_date));
                if ($arr_irrg_info["irrg_type"] == "1") {
                    while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
                        $tmp_date = last_date($tmp_date);
                        $wd = date("w", to_timestamp($tmp_date));
                    }
                } else {
                    //変則労働時間・週以外の場合、日曜から
                    while ($wd != 0) {
                        $tmp_date = last_date($tmp_date);
                        $wd = date("w", to_timestamp($tmp_date));
                    }
                }
                $counter_for_week = 1;
                $holiday_count = 0;

                //データ検索用開始日
                $wk_start_date = $tmp_date;

                // 処理日付の勤務予定情報を取得
                $sql =  "SELECT ".
                    "atdbk.date, ".
                    "atdbk.pattern, ".
                    "atdbk.reason, ".
                    "atdbk.night_duty, ".
                    "atdbk.prov_start_time, ".
                    "atdbk.prov_end_time, ".
                    "atdbk.tmcd_group_id, ".
                    "atdptn.workday_count ".
                    "FROM ".
                    "atdbk ".
                    "LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = to_char(atdptn.atdptn_id,'FM999999999999') ";
                $cond = "where emp_id = '$tmp_emp_id' and ";
                $cond .= " date >= '$wk_start_date' and date <= '$end_date'";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $arr_atdbk = array();
                while ($row = pg_fetch_array($sel)) {
                    $date = $row["date"];
                    $arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
                    $arr_atdbk[$date]["pattern"] = $row["pattern"];
                    $arr_atdbk[$date]["reason"] = $row["reason"];
                    $arr_atdbk[$date]["night_duty"] = $row["night_duty"];
                    $arr_atdbk[$date]["allow_id"] = $row["allow_id"];
                    $arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
                    $arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
                    $arr_atdbk[$date]["workday_count"] = $row["workday_count"];
                }

                // 処理日付の勤務実績を取得
                $sql =  "SELECT ".
                    "atdbkrslt.*, ".
                    "atdptn.workday_count, ".
                    "atdptn.base_time, ".
                    "atdptn.previous_day_flag as atdptn_previous_day_flag, ". //所定前日フラグ 20110909
                    "atdptn.over_24hour_flag, ".
                    "atdptn.out_time_nosubtract_flag, ".
                    "atdptn.after_night_duty_flag, ".
                    "atdptn.no_count_flag, ".
                    "tmmdapply.apply_id     AS tmmd_apply_id, ".
                    "tmmdapply.apply_status AS tmmd_apply_status, ".
                    "ovtmapply.apply_id     AS ovtm_apply_id, ".
                    "ovtmapply.apply_status AS ovtm_apply_status, ".
                    "rtnapply.apply_id      AS rtn_apply_id, ".
                    "rtnapply.apply_status  AS rtn_apply_status, ".
                    "COALESCE(atdbk_reason_mst.day_count, 0) AS reason_day_count, ".
                    "duty_shift_plan_assist.group_id AS assist_group_id ".
                    "FROM ".
                    "atdbkrslt ".
                    "LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
                    "LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
                    "LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
                    "LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ".
                    "LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ".
                    "LEFT JOIN duty_shift_plan_assist ON duty_shift_plan_assist.emp_id = atdbkrslt.emp_id AND duty_shift_plan_assist.duty_date = atdbkrslt.date "
                    ;
                $cond = "where atdbkrslt.emp_id = '$tmp_emp_id' and ";
                $cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$wk_end_date_nextone2'"; //20130308
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $arr_atdbkrslt = array();
                while ($row = pg_fetch_array($sel)) {
                    $date = $row["date"];
                    //まとめて設定
                    $arr_atdbkrslt[$date] = $row;
                    /*
                    $arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
                    $arr_atdbkrslt[$date]["reason"] = $row["reason"];
                    $arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
                    $arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
                    $arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
                    $arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
                    $arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
                    $arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
                    for ($i = 1; $i <= 10; $i++) {
                        $start_var = "o_start_time$i";
                        if ($row[$start_var] == "") {
                            break;
                        }
                        $end_var = "o_end_time$i";
                        $arr_atdbkrslt[$date][$start_var] = $row[$start_var];
                        $arr_atdbkrslt[$date][$end_var] = $row[$end_var];
                    }
                    $arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
                    $arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
                    $arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
                    $arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
                    $arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
                    $arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
                    $arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
                    $arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
                    $arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
                    $arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
                    $arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
                    $arr_atdbkrslt[$date]["reason_day_count"] = $row["reason_day_count"];
                    $arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
                    //残業時刻追加 20100114
                    $arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
                    $arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
                    $arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
                    $arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
                    $arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
                    //残業時刻2追加 20110616
                    $arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
                    $arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
                    $arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
                    $arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
                    $arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
                    $arr_atdbkrslt[$date]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
                    //法定内残業
                    $arr_atdbkrslt[$date]["legal_in_over_time"] = $row["legal_in_over_time"];
                    //早出残業 20100601
                    $arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];
                    //休憩時刻追加 20100921
                    $arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
                    $arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
                    //外出時間を差し引かないフラグ 20110727
                    $arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
                    //明け追加 20110819
                    $arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
                    //勤務時間を計算しないフラグ 20130225
                    $arr_atdbkrslt[$date]["no_count_flag"] = $row["no_count_flag"];

                    $arr_atdbkrslt[$date]["assist_group_id"] = $row["assist_group_id"];
                    */
                }

                //時間有休追加 20111207
                if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                    //時間有休データをまとめて取得
                    $arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($tmp_emp_id, $wk_start_date, $end_date);

                }
                //法定内勤務、法定内残業、法定外残業
                $hoteinai = 0;
                $hoteinai_zan = 0;
                $hoteigai = 0;

                //支給換算日数 20090908
                $paid_day_count = 0;

                $kinmu_cnt = 0;
                //休日出勤テーブル 2008/10/23
                $arr_hol_work_date = array();
                while ($tmp_date <= $end_date) {

                    //勤務シフト作成からの場合
                    if ($from_flg == 2) {
                        //グループ、職員
                        if ($total_flg == 2) {
                            $cont_flg = false;
                            $assist_group_id = $arr_atdbkrslt[$tmp_date]["assist_group_id"];
                            //応援
                            if ($assist_group_id != "") {
                                if ($assist_group_id != $tmp_group_id2) {
                                    $cont_flg = true;
                                }
                            }
                            //応援以外、履歴確認
                            else {
                                if (count($arr_duty_shift_staff_history) > 0) {
                                    if ($tmp_group_id2 != $arr_duty_shift_staff_history[0]["group_id"]) {
                                        $cont_flg = true;
                                    }
                                }
                                else {
                                    if ($tmp_main_group_id != "" && $tmp_main_group_id != $tmp_group_id2) {
                                        $cont_flg = true;
                                    }
                                }
                            }
                            //
                            if ($cont_flg) {
                                $tmp_week = get_weekday(to_timestamp($tmp_date));
                                if ($forty_last_day == $tmp_week){
                                    if ($previous_month_time != ""){
                                        $tmp_arr_week_forty += $previous_month_time;    //前月のデータを加算する
                                        $previous_month_time = "";
                                    }


                                    $forty_surplus = 0;
                                    if ($tmp_arr_week_forty > $week_forty_value){
                                        $forty_surplus = $tmp_arr_week_forty - $week_forty_value;   //週40時間越えた分を計上
                                    }
                                    //キー:日付＋曜日=>結果代入
                                    $week_forty_overtime = ($forty_surplus == 0) ? "0:00" : minute_to_hmm($forty_surplus);
                                    $week_forty_overtime_total += $forty_surplus; //週40時間合計

                                    $tmp_arr_week_forty = 0;

                                }
                                //期間が長い場合で月が変わった場合
                                if ($tmp_date >= $month_chk_date) {
                                    //休日対応
                                    $hoteigai_sums_month = array_fill(0, 2, 0); //index 0:法定外残業（深夜・休日残業除外） 1:法定外深夜残業（休日除外）複数月ある場合用
                                    $over_fix_time_total[0] += $over_fix_time[0];
                                    $over_fix_time_total[1] += $over_fix_time[1];
                                    $over_fix_time_total[2] += $over_fix_time[2];
                                    $over_fix_time_total[3] += $over_fix_time[3];
                                    $over_fix_time = array_fill(0,4,0); //index 0:60時間超法定外残業 1:60時間深夜残業

                                    $over_fix_time_total2[0] += $over_fix_time2[0];
                                    $over_fix_time_total2[1] += $over_fix_time2[1];
                                    $over_fix_time_total2[2] += $over_fix_time2[2];
                                    $over_fix_time_total2[3] += $over_fix_time2[3];
                                    $over_fix_time2 = array_fill(0,4,0);

                                    $month_chk_date = date("Ymd", strtotime("+1 month",date_utils::to_timestamp_from_ymd($month_chk_date) ));
                                }
                                $tmp_date = next_date($tmp_date);
                                $counter_for_week++;
                                continue;
                            }
                        }
                        //期間確認
                        if ($tmp_date >= $wf_start_date) {

                            if (!($arr_timecard_holwk_day[0]["legal_holiday"] == "t" && $arr_type[$tmp_date] == "4" ||
                                        $arr_timecard_holwk_day[0]["national_holiday"] == "t" && $arr_type[$tmp_date] == "6" ||
                                        $arr_timecard_holwk_day[0]["newyear_holiday"] == "t" && $arr_type[$tmp_date] == "7" ||
                                        $arr_timecard_holwk_day[0]["prescribed_holiday"] == "t" && $arr_type[$tmp_date] == "5")) {
                                $kinmu_cnt++;
                            }

                        }
                    }

                    if ($counter_for_week >= 8) {
                        $counter_for_week = 1;
                    }
                    if ($counter_for_week == 1) {
                        //前日までの計
                        $last_day_total = 0;
                        $work_time_for_week = 0;
                        if ($arr_irrg_info["irrg_type"] == "1") {
                            $holiday_count = 0;
                        }
                        //1日毎の法定外残を週単位に集計 20090708
                        $wk_week_hoteigai = 0;
                    }
                    $work_time = 0;
                    $wk_return_time = 0;

                    $start_time_info = array();
                    $end_time_info = array(); //退勤時刻情報 20100910

                    $time3 = 0; //外出時間（所定時間内） 20100910
                    $time4 = 0; //外出時間（所定時間外） 20100910
                    $tmp_rest2 = 0; //休憩時間 20100910
                    $time3_rest = 0; //休憩時間（所定時間内）20100910
                    $time4_rest = 0; //休憩時間（所定時間外）20100910
                    $arr_hol_time = array(); // 休日勤務情報 20101224
                    // 法定外残業時間(日別) 20121206
                    $hoteigai_sums_day = array_fill(0, 8, 0); //index 0:法定外残業（深夜・休日残業除外） 1:法定外深夜残業（休日除外） 2:法定外休日残業（深夜除外） 3:法定外休日深夜残業 4:休日勤務手当時間 5:休日勤務手当時間（深夜） 6:６０時間超計算用休日残業 7:６０時間超計算用休日深夜 20141029
                    // 残業時間(日別) 20130822 ※日別の集計をし条件により休憩を減算後、月合計へ集計
                    $zan_sums_day = array_fill(0, 4, 0); //index 0:残業時間（深夜残業・休日残業除外） 1:深夜残業（休日含む） 2:休日残業 3:休日深夜残業

                    $holiday_name = get_holiday_name($tmp_date);
                    if ($holiday_name != "") {
                        $holiday_count++;
                    }

                    // 処理日付の勤務日種別を取得
                    $type = $arr_type[$tmp_date];

                    // 処理日付の勤務予定情報を取得
                    $prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
                    $prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
                    $prov_reason = $arr_atdbk[$tmp_date]["reason"];
                    $prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
                    $prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

                    $prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
                    $prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
                    $prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
                    $prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
                    $prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];

                    if ($prov_workday_count == "") {
                        $prov_workday_count = 0;
                    }

                    // 処理日付の勤務実績を取得
                    $pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
                    $reason = $arr_atdbkrslt[$tmp_date]["reason"];
                    $night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
                    $allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
                    $start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
                    $end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
                    $out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
                    $ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

                    for ($i = 1; $i <= 10; $i++) {
                        $start_var = "o_start_time$i";
                        $end_var = "o_end_time$i";
                        $$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
                        $$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

                        if ($$start_var != "") {
                            $$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
                        }
                        if ($$end_var != "") {
                            $$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
                        }
                    }

                    $tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
                    $meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

                    $workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

                    $previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
                    $next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

                    $tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
                    $tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
                    $ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
                    $ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
                    $rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
                    $rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];
                    $reason_day_count = $arr_atdbkrslt[$tmp_date]["reason_day_count"];
                    $base_time = $arr_atdbkrslt[$tmp_date]["base_time"];

                    if ($workday_count == "") {
                        $workday_count = 0;
                    }
                    if ($previous_day_flag == "") {
                        $previous_day_flag = 0;
                    }
                    if ($next_day_flag == "") {
                        $next_day_flag = 0;
                    }
                    if ($reason_day_count == "") {
                        $reason_day_count = 0;
                    }
                    //残業時刻追加 20100114
                    $allow_count = $arr_atdbkrslt[$tmp_date]["allow_count"];
                    $over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
                    $over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
                    $over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
                    $over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
                    if ($over_start_time != "") {
                        $over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
                    }
                    if ($over_end_time != "") {
                        $over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
                    }
                    //残業時刻2追加 20110616
                    $over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
                    $over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
                    $over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
                    $over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
                    if ($over_start_time2 != "") {
                        $over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
                    }
                    if ($over_end_time2 != "") {
                        $over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
                    }
                    $over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
                    $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];
                    //法定内残業
                    $legal_in_over_time = $arr_atdbkrslt[$tmp_date]["legal_in_over_time"];
                    if ($legal_in_over_time != "") {
                        $legal_in_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $legal_in_over_time);
                    }
                    //早出残業 20100601
                    $early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
                    if ($early_over_time != "") {
                        $early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
                    }
                    //休憩時刻追加 20100921
                    $rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
                    $rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
                    if ($rest_start_time != "") {
                        $rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
                    }
                    if ($rest_end_time != "") {
                        $rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
                    }
                    //外出時間を差し引かないフラグ 20110727
                    $out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];
                    //明け追加 20110819
                    $after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
                    //勤務時間を計算しないフラグ 20130225
                    $no_count_flag = $arr_atdbkrslt[$tmp_date]["no_count_flag"];

                    //当直の有効判定 20101109
                    $night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

                    $rslt_workday_count = "";

                    if ($tmp_date >= $start_date) {

                        //所定時間 20101014
                        if ($fixed_set_pattern_id == $tmcd_group_id &&
                                $fixed_set_atdptn_ptn_id == $pattern) {
                            $fixed_set_cnt++;
                        }

                        //公休か事由なし休暇で残業時刻がありの場合 20100802
                        //事由に関わらず休暇で残業時刻がありの場合へ変更 20100916
                        if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
                                $over_start_time != "" && $over_end_time != "") {
                            $legal_hol_over_flg = true;
                            //残業時刻を設定
                            $start_time = $over_start_time;
                            $end_time = $over_end_time;

                            $wk_base_time = 0;
                        } else {
                            $legal_hol_over_flg = false;
                        }

                        // 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
                        if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {

                            //日またがり対応 20101027
                            if ($start_time > $end_time &&
                                    $previous_day_flag == "0") {
                                $next_day_flag = "1";
                            }

                            //休暇時は計算しない 20090806
                            if ($pattern != "10") {
                                $rslt_workday_count = $workday_count;
                            }
                            if($night_duty_flag)
                            {
                                //曜日に対応したworkday_countを取得する
                                $rslt_workday_count = $rslt_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
                            }
                            $sums[1] = $sums[1] + $rslt_workday_count;
                            //休日出勤のカウント方法変更 20091127
                            if (check_timecard_holwk(
                                        $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {
                                //休日出勤日数勤務パターン設定にある場合を除く 20101013
                                $wk_ptn_id = $tmcd_group_id."_".$pattern;
                                if (!in_array($wk_ptn_id, $arr_ptn_id)) {
                                    $sums[2] += 1;
                                }
                            }
                        }

                        // 事由ベースの日数計算
                        $reason_day_count = $atdbk_common_class->get_reason_day_count($reason);
                        //reason_day_countは支給換算日数となる 20090908
                        $paid_time = 0;
                        if ($reason_day_count > 0) {
                            $am_hol_days = 0;
                            $pm_hol_days = 0;
                            $paid_hol_days = 0;
                            //半日有休を区別して集計 20110204
                            switch ($reason) {
                                case "2": //午前有休
                                case "38": //午前年休
                                case "67": //午前特別
                                case "50": //午前夏休
                                case "69": //午前正休
                                    $paid_sums[1] += $reason_day_count;
                                    $am_hol_days = 0.5;
                                    break;
                                case "3": //午後有休
                                case "39": //午後年休
                                case "68": //午後特別
                                case "51": //午後夏休
                                case "70": //午後正休
                                    $paid_sums[2] += $reason_day_count;
                                    $pm_hol_days = 0.5;
                                    break;
                                case "44":  // 半有半公
                                case "55":  // 半夏半有
                                case "57":  // 半有半欠
                                case "58":  // 半特半有
                                case "62":  // 半正半有
                                    $paid_sums[0] += $reason_day_count;
                                    $paid_hol_days = 0.5;
                                    break;
                                default: //上記以外
                                    if ($pattern == "10") { //20140106 パターンが休暇の場合
                                        $paid_sums[0] += $reason_day_count;
                                        $paid_hol_days = 1.0;
                                    }
                            }
                            //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 start
                            $wk_weekday = $arr_weekday_flg[$tmp_date];
                            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date);
                            if ($arr_empcond_officehours["part_specified_time"] != "") {
                                $paid_specified_time = $arr_empcond_officehours["part_specified_time"];
                                $am_time = "";
                                $pm_time = "";
                                $empcond_spec_time_flg = true;
                            }
                            //20130521 勤務条件の所定時間を2番目に優先とする
                            //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 end
                            //所定時間履歴を使用 20121127
                            //勤務条件にない場合
                            if ($empcond_spec_time_flg == false) {
                                $arr_timehist = $calendar_name_class->get_calendarname_time($tmp_date);
                                $paid_specified_time = $arr_timehist["day1_time"];
                                $am_time = $arr_timehist["am_time"];
                                $pm_time = $arr_timehist["pm_time"];
                            }
                            //半日有給
                            $min = 0;
                            //半日午前所定時間がある場合は計算し加算
                            if ($am_time != "") {
                                $hour2 = (int)substr($am_time, 0, 2);
                                $min2 = $hour2 * 60 + (int)substr($am_time, 2, 3);
                                //日数分を加算する(0.5単位のため、1回分の計算として2倍する）
                                $min += $min2 * $am_hol_days * 2;
                            }
                            else {
                                //ない場合は、半日以外の所定に加算
                                $paid_hol_days += $am_hol_days;
                            }
                            //半日午後所定時間がある場合は計算し加算
                            if ($pm_time != "") {
                                $hour2 = (int)substr($pm_time, 0, 2);
                                $min2 = $hour2 * 60 + (int)substr($pm_time, 2, 3);
                                //日数分を加算する(0.5単位のため、1回分の計算として2倍する）
                                $min += $min2 * $pm_hol_days * 2;
                            }
                            else {
                                //ない場合は、半日以外の所定に加算
                                $paid_hol_days += $pm_hol_days;
                            }

                            //半日以外の換算日数
                            if ($paid_specified_time != "") {
                                //所定労働時間を分単位にする
                                $hour2 = (int)substr($paid_specified_time, 0, 2);
                                $min2 = $hour2 * 60 + (int)substr($paid_specified_time, 2, 3);
                                //日数分を加算する
                                $min += $min2 * $paid_hol_days;
                            }
                            $paid_time = $min;
                            $paid_sums[4] += $min;
                        }
                        //時間有休追加 20111207
                        $paid_time_hour = 0;
                        if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                            $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                            $paid_time_hour = $paid_hol_min;
                            $paid_sums[3] += $paid_hol_min; //時間有休計
                        }
                        switch ($reason) {
                            case "14":  // 代替出勤
                                $sums[3] += 1;
                                break;
                            case "15":  // 振替出勤
                                $sums[4] += 1;
                                break;
                            case "65":  // 午前振出
                            case "66":  // 午後振出
                                $sums[4] += 0.5;
                                break;
                            case "4":  // 代替休暇
                                $sums[5] += 1;
                                break;
                            case "18":  // 半前代替休
                            case "19":  // 半後代替休
                                $sums[5] += 0.5;
                                break;
                            case "17":  // 振替休暇
                                $sums[6] += 1;
                                break;
                            case "20":  // 半前振替休
                            case "21":  // 半後振替休
                                $sums[6] += 0.5;
                                break;
                            case "1":  // 有休休暇
                            case "37": // 年休
                                if ($pattern == "10") { //20140106 パターンが休暇の場合
                                    $sums[7] += 1;
                                }
                                break;
                            case "2":  // 午前有休
                            case "3":  // 午後有休
                            case "38": // 午前年休
                            case "39": // 午後年休
                                $sums[7] += 0.5;
                                break;
                            case "57":  // 半有半欠
                                $sums[7] += 0.5;
                                $sums[8] += 0.5;
                                break;
                            case "6":  // 一般欠勤
                            case "7":  // 病傷欠勤
                                $sums[8] += 1;
                                break;
                            case "48": // 午前欠勤
                            case "49": // 午後欠勤
                                $sums[8] += 0.5;
                                break;
                            case "8":  // その他休
                                $sums[9] += 1;
                                break;
                            case "52":  // 午前その他休
                            case "53":  // 午後その他休
                                $sums[9] += 0.5;
                                break;
                            case "24":  // 公休
                            case "45":  // 希望(公休)
                            case "46":  // 待機(公休)
                            case "47":  // 管理当直前(公休)
                                $add_sums[0] += 1;
                                break;
                            case "35":  // 午前公休
                            case "36":  // 午後公休
                                $add_sums[0] += 0.5;
                                break;
                            case "5":   // 特別休暇
                                $add_sums[1] += 1;
                                break;
                            case "67":  // 午前特別
                            case "68":  // 午後特別
                                $add_sums[1] += 0.5;
                                break;
                            case "58":  // 半特半有
                                $add_sums[1] += 0.5;
                                $sums[7] += 0.5;
                                break;
                            case "59":  // 半特半公
                                $add_sums[1] += 0.5;
                                $add_sums[0] += 0.5;
                                break;
                            case "60":  // 半特半欠
                                $add_sums[1] += 0.5;
                                $sums[8] += 0.5;
                                break;
                            case "25":  // 産休
                                $add_sums[2] += 1;
                                break;
                            case "26":  // 育児休業
                                $add_sums[3] += 1;
                                break;
                            case "27":  // 介護休業
                                $add_sums[4] += 1;
                                break;
                            case "28":  // 傷病休職
                                $add_sums[5] += 1;
                                break;
                            case "29":  // 学業休職
                                $add_sums[6] += 1;
                                break;
                            case "30":  // 忌引
                                $add_sums[7] += 1;
                                break;
                            case "31":   // 夏休
                                $add_sums[8] += 1;
                                break;
                            case "50":  // 午前夏休
                            case "51":  // 午後夏休
                                $add_sums[8] += 0.5;
                                break;
                            case "54":  // 半夏半公
                                $add_sums[8] += 0.5;
                                $add_sums[0] += 0.5;
                                break;
                            case "55":  // 半夏半有
                                $add_sums[8] += 0.5;
                                $sums[7] += 0.5;
                                break;
                            case "56":  // 半夏半欠
                                $add_sums[8] += 0.5;
                                $sums[8] += 0.5;
                                break;
                            case "72":  // 半夏半特
                                $add_sums[8] += 0.5;
                                $add_sums[1] += 0.5;
                                break;
                            case "61":  // 年末年始
                                $add_sums[9] += 1;
                                break;
                            case "69":  // 午前正休
                            case "70":  // 午後正休
                                $add_sums[9] += 0.5;
                                break;
                            case "62":  // 半正半有
                                $add_sums[9] += 0.5;
                                $sums[7] += 0.5;
                                break;
                            case "63":  // 半正半公
                                $add_sums[9] += 0.5;
                                $add_sums[0] += 0.5;
                                break;
                            case "64":  // 半正半欠
                                $add_sums[9] += 0.5;
                                $sums[8] += 0.5;
                                break;
                            case "32":   // 結婚休
                                $add_sums[10] += 1;
                                break;
                            case "40":   // リフレッシュ休暇
                                $add_sums[11] += 1;
                                break;
                            case "42":   // 午前リフレッシュ休暇
                            case "43":   // 午後リフレッシュ休暇
                                $add_sums[11] += 0.5;
                                break;
                            case "41":  // 初盆休暇
                                $add_sums[12] += 1;
                                break;
                            case "44":  // 半有半公
                                $sums[7] += 0.5;
                                $add_sums[0] += 0.5;
                                break;
                        }
                        // 休日となる設定と事由があう場合に公休に計算する 20100810
                        // 法定休日
                        if ($arr_type[$tmp_date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
                            if ($reason == "22") {
                                $add_sums[0] += 1;
                            }
                        }
                        // 集計行に法定休日追加 20110121
                        if ($reason == "22") {
                            $add_sums[13] += 1;
                        }
                        // 所定休日
                        if ($arr_type[$tmp_date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
                            if ($reason == "23") {
                                $add_sums[0] += 1;
                            }
                        }
                        // 集計行に所定休日追加 20110121
                        if ($reason == "23") {
                            $add_sums[14] += 1;
                        }
                        // 年末年始
                        if ($arr_type[$tmp_date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
                            if ($reason == "61") {
                                $add_sums[0] += 1;
                            }
                            if ($reason == "62" || $reason == "63" || $reason == "64") {
                                $add_sums[0] += 0.5;
                            }
                        }
                    }

                    // 各時間帯の開始時刻・終了時刻を変数に格納
                    //type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
                    $tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
                    $start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
                    $end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
                    $start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
                    $end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
                    $start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
                    $end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

                    //個人別所定時間を優先する場合 20120309
                    if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
                        $wk_weekday = $arr_weekday_flg[$tmp_date];
                        $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130207
                        if ($arr_empcond_officehours["officehours2_start"] != "") {
                            $start2 = $arr_empcond_officehours["officehours2_start"];
                            $end2 = $arr_empcond_officehours["officehours2_end"];
                            $start4 = $arr_empcond_officehours["officehours4_start"];
                            $end4 = $arr_empcond_officehours["officehours4_end"];
                        }
                    }
// 所定開始より残業１があった場合、残業２で入れ替えると出勤表の残業時間が合わないため、以下の処理をコメントアウトにする　20140917
//                     //所定開始より残業１が早い場合、残業１と残業２を入れ替える 20111124
//                     if ($start2 != "" && $over_end_time != "") {
//                         if ($over_end_time <= $start2 && $over_end_next_day_flag != "1") { //残業終了時刻翌日フラグ設定なしの場合を追加 20120214
//                             $wk_chg_start = $over_start_time;
//                             $wk_chg_end = $over_end_time;
//                             $wk_chg_start_flag = $over_start_next_day_flag;
//                             $wk_chg_end_flag = $over_end_next_day_flag;
//                             $over_start_time = $over_start_time2;
//                             $over_end_time = $over_end_time2;
//                             $over_start_next_day_flag = $over_start_next_day_flag2;
//                             $over_end_next_day_flag = $over_end_next_day_flag2;
//                             $over_start_time2 = $wk_chg_start;
//                             $over_end_time2 = $wk_chg_end;
//                             $over_start_next_day_flag2 = $wk_chg_start_flag;
//                             $over_end_next_day_flag2 = $wk_chg_end_flag;
//                             //休憩追加 20140716
//                             $wk_chg_rest_start = $rest_start_time1;
//                             $wk_chg_rest_end = $rest_end_time1;
//                             $wk_chg_rest_start_flag = $rest_start_next_day_flag1;
//                             $wk_chg_rest_end_flag = $rest_end_next_day_flag1;
//                             $rest_start_time1 = $rest_start_time2;
//                             $rest_end_time1 = $rest_end_time2;
//                             $rest_start_next_day_flag1 = $rest_start_next_day_flag2;
//                             $rest_end_next_day_flag1 = $rest_end_next_day_flag2;
//                             $rest_start_time2 = $wk_chg_rest_start;
//                             $rest_end_time2 = $wk_chg_rest_end;
//                             $rest_start_next_day_flag2 = $wk_chg_rest_start_flag;
//                             $rest_end_next_day_flag2 = $wk_chg_rest_end_flag;
//                         }
//                     }
                    // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
                    if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
                        $start2 = $prov_start_time;
                        $end2 = $prov_end_time;
                    }
                    $over_calc_flg = false; //残業時刻 20100525
                    //残業時刻追加 20100114
                    //残業開始が予定終了時刻より前の場合に設定
                    if ($over_start_time != "") {
                        //残業開始日時
                        if ($over_start_next_day_flag == "1") {
                            $over_start_date = next_date($tmp_date);
                        } else {
                            $over_start_date = $tmp_date;
                        }

                        $over_start_date_time = $over_start_date.$over_start_time;
                        $over_calc_flg = true;
                        //残業終了時刻 20100705
                        $over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
                        $over_end_date_time = $over_end_date.$over_end_time;
                    }
                    // 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
                    $effective_time = 0;

                    //一括修正のステータスがある場合は設定
                    $tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
                    if ($tmmd_apply_status == "" && $tmp_status != "") {
                        $tmmd_apply_status = $tmp_status;
                        $tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
                    }
                    //tmmd_apply_statusからlink_type取得
                    $modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
                    $modify_apply_id  = $tmmd_apply_id;

                    // 残業情報を取得
                    //ovtm_apply_statusからlink_type取得

                    $night_duty_time_array  = array();
                    $duty_work_time         = 0;
                    if ($night_duty_flag){
                        //日数換算の数値ｘ通常勤務日の労働時間の時間
                        $duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
                    }

                    $overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
                    $overtime_apply_id  = $ovtm_apply_id;

                    //rtn_apply_statusからlink_type取得
                    $return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
                    $return_apply_id  = $rtn_apply_id;

                    // 所定労働・休憩開始終了日時の取得
                    // 所定開始時刻が前日の場合 20121120
                    if ($atdptn_previous_day_flag == "1") {
                        $tmp_prev_date = last_date($tmp_date);
                        $start2_date_time = $tmp_prev_date.$start2;
                        //24時間以上勤務の場合 20100203
                        if ($over_24hour_flag == "1") {
                            $tmp_prev_date = $tmp_date;
                        }
                        $end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
                    } else {
                        $start2_date_time = $tmp_date.$start2;
                        //24時間以上勤務の場合 20100203
                        if ($over_24hour_flag == "1") {
                            $tmp_end_date = next_date($tmp_date);
                        } else {
                            $tmp_end_date = $tmp_date;
                        }
                        $end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
                    }
                    //休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
                    if ($rest_start_time != "" && $rest_end_time != "") {
                        $start4 = $rest_start_time;
                        $end4 = $rest_end_time;
                    }
                    //休憩時刻の日またがり確認 20141104
                    $wk_start2 = ($start2 != "") ? $start2 : $start_time; //所定時刻なしの休憩時間計算不具合対応 20141128
			        if ($previous_day_flag == "1" || $atdptn_previous_day_flag == "1" ) { //前日フラグがある場合 20150302
						$tmp_last_date = last_date($tmp_date);
						$start4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_last_date, $wk_start2, $start4);
						$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_last_date, $wk_start2, $end4);
					}
					else {
	                    $start4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $wk_start2, $start4);
	                    $end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $wk_start2, $end4);
                    }

					//20150702
					$arr_atdbkrslt[$tmp_date]["start4_date_time"] = $start4_date_time;
					$arr_atdbkrslt[$tmp_date]["end4_date_time"] = $end4_date_time;

                    //外出復帰日時の取得
                    $out_date_time = $tmp_date.$out_time;
                    $ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

                    //時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持
                    if ($tmp_date >= $start_date) {
                        $work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
                    }

                    // 処理当日の所定労働時間を求める
                    if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
                        $specified_time = 0;
                    } else {
                        $specified_time = date_utils::get_diff_minute($end2_date_time, $start2_date_time);
                        if ($start4 != "" && $end4 != "") {
                            $wk_rest_time = date_utils::get_diff_minute($end4_date_time, $start4_date_time);
                            $specified_time -= $wk_rest_time;
                        }

                    }

                    //出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 start
                    if ($over_start_time != "" && $over_end_time != "") {
                        if ($start_time == "") {
                            $start_time = $start2;
                            $previous_day_flag = $atdptn_previous_day_flag; //時間帯設定の前日フラグ
                        }
                        if ($end_time == "") {
                            $end_time = $end2;
                            //日またがり対応
                            if ($start_time > $end_time &&
                                    $previous_day_flag == "0") {
                                $next_day_flag = "1";
                            }
                        }
                    }
                    //出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 end
                    // 出退勤とも打刻済みの場合
                    //勤務時間を計算しないフラグが"1"以外 20130225
                    if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {

                        // ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
                        $start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
                        $end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
                        if (empty($start2_date_time) || empty($end2_date_time)) {
                            $start2_date_time = $start_date_time;
                            $end2_date_time    = $end_date_time;
                        }

                        $overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);
                        // 残業管理をする場合、（しない場合には退勤時刻を変えずに稼動時間の計算をする）
                        if ($no_overtime != "t") {
                            // 残業未承認か確認
                            if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
                                if (date_utils::to_timestamp_from_ymdhi($start2_date_time) > date_utils::to_timestamp_from_ymdhi($start_date_time)) { //所定より出勤が早い場合には所定にする 20100914 時刻に:があるため遅刻時間の表示されない不具合 20101005
                                    $start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
                                }
                                if ($end_date_time > $end2_date_time) {
                                    $end_date_time = $end2_date_time;
                                }
                            }
                        }

                        $wk_work_time = date_utils::get_diff_minute($end_date_time, $start_date_time);
                        //出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
                        if ($wk_work_time > 0){
                            //休日で残業がある場合 20101028
                            if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
                                //深夜残業の開始終了時間
                                $start5 = "2200";
                                $end5 = "0500";
                            }
                            $late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報

                            // 確定出勤時刻／遅刻回数を取得
                            //残業開始時刻があり、出勤時刻と同じ場合 20111122
                            if ($over_start_time != "" && $start_time == $over_start_time) {
                                $fixed_start_time = substr($start_date_time, 0, 8) .$over_start_time;
                            }
                            else {
                                // 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
                                if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
                                    //端数処理する分
                                    $moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
                                    //基準日時
                                    $tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
                                    $fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
                                    $start_time_info = array();
                                } else {
                                    //時間計算変更 20101109
                                    $start_time_info = $timecard_common_class->get_start_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction1, $fraction1_min, $fraction3);
                                    $fixed_start_time = $start_time_info["fixed_time"];
                                }
                            }

                            // 確定退勤時刻／早退回数を取得
                            // 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
                            if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
                                //端数処理する分
                                $moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                                //基準日時
                                $tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
                                $fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
                                $end_time_info = array();
                            } else {
                                //残業時刻が入っている場合は、端数処理しない 20100806
                                $wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
                                if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time && $end_date_time >= $end2_date_time) { //時間が所定終了よりあとか確認20111128 //早退の場合を除く条件追加 20140902
                                    $fixed_end_time = $wk_over_end_date_time;
                                } else {
                                    //時間計算変更 20101109
                                    $end_time_info = $timecard_common_class->get_end_time_info2($start_date_time, $end_date_time, $start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time, $fraction2, $fraction2_min, $fraction3);
                                    $fixed_end_time = $end_time_info["fixed_time"];
                                }
                                //※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
                                //残業申請画面を表示しないは除く 20100811
                                if ($timecard_bean->over_time_apply_type != "0") {
                                    if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
                                        //退勤時刻が所定よりあとの場合設定 20101021
                                        if ($fixed_end_time > $end2_date_time) {
                                            $fixed_end_time = $end2_date_time;
                                        }
                                    }
                                }
                            }
                            //時間有休がある場合は、遅刻早退を計算しない 20111207 $paid_timeを追加 20130507 $paid_time > 0 ||削除 20130530
                            if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
                                //時間有休と相殺する場合
                                if ($ovtm_class->sousai_sinai_flg != "t") {
                                    //遅刻
                                    $tikoku_time = 0;
                                    $start_time_info["diff_minutes"] = 0;
                                    $start_time_info["diff_count"] = 0;
                                    //早退
                                    $sotai_time = 0;
                                    $end_time_info["diff_minutes"] = 0;
                                    $end_time_info["diff_count"] = 0;
                                }
                            }
                            if ($tmp_date >= $start_date) {
                                //遅刻
                                $sums[10] += $start_time_info["diff_count"];
                                $sums[12] += $start_time_info["diff_minutes"];
                            }
                            if ($tmp_date >= $start_date) {
                                //早退
                                $sums[11] += $end_time_info["diff_count"];
                                $sums[13] += $end_time_info["diff_minutes"];
                            }

                            // 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
                            // 以前は半年休用の計算で分かれていたが、以下は実質同じ処理となった
                            switch ($reason) {
                                case "2":  // 午前有休
                                case "38": // 午前年休
                                    $tmp_start_time = $fixed_start_time;
                                    $tmp_end_time = $fixed_end_time;

                                    break;
                                case "3":  // 午後有休
                                case "39": // 午後年休
                                    $tmp_start_time = $fixed_start_time;
                                    $tmp_end_time = $fixed_end_time;

                                    break;
                                default:
                                    $tmp_start_time = $fixed_start_time;
                                    $tmp_end_time = $fixed_end_time;

                                    break;
                            }


                            //休日勤務時間（分）と休日出勤を取得 2008/10/23
                            if ($tmp_date >= $start_date) {
                                //休日出勤の判定方法変更 20100721
                                if (check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {

                                    //所定開始時刻と残業開始時刻が一致している場合
                                    if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {
                                        $wk_end_date_time = $over_end_date_time;
                                    } else {
                                        $wk_end_date_time = $tmp_end_time;
                                    }
                                    //計算用関数変更 20101109
                                    //開始時刻が0000の場合で前日フラグがある場合、調整
                                    $wk_previous_day_flag = $previous_day_flag;
                                    if (substr($fixed_start_time, 8, 4) == "0000" && $wk_previous_day_flag == "1") {
                                        $wk_previous_day_flag = "0";
                                    }
                                    $arr_hol_time = $timecard_common_class->get_holiday_work2($arr_type, $tmp_date, $fixed_start_time, $wk_previous_day_flag, $wk_end_date_time, $next_day_flag, $reason, $work_times_info, $out_time, $ret_time);

                                    $hotei_sums[3] += $arr_hol_time[3];

                                    // 休日出勤した日付を配列へ設定
                                    for ($i=4; $i<7; $i++) {
                                        if ($arr_hol_time[$i] != "") {
                                            $arr_hol_work_date[$arr_hol_time[$i]] = 1;
                                        }
                                    }
                                }
                            }

                            // 普外時間を算出（分単位）
                            // 外出時間を差し引かないフラグ対応 20110727
                            if ($out_time_nosubtract_flag != "1") {
                                if ($out_time != "" && $ret_time != "") { //外出 20101109
                                    $time3 = date_utils::get_diff_minute($ret_date_time, $out_date_time);
                                }
                            }

                            // 実働時間を算出（分単位）
                            $effective_time = date_utils::get_diff_minute($fixed_end_time, $fixed_start_time);
                            $tmp_start_time = str_replace(":", "", $start_time);
                            if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
                                //残業開始時刻が所定終了時刻と離れている場合の対応 20131112
                                if ($over_start_time != "") {
                                    $wk_end_date_time = $end2_date_time;
                                    //残業開始時刻があとの場合
                                    if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
                                        $wk_jogai = date_utils::get_diff_minute($over_start_date_time, $wk_end_date_time);
                                        $effective_time -= $wk_jogai;
                                    }
                                }
                                if ($effective_time > 1200) {  // 20時間を超える場合 20100929
                                    $effective_time -= $rest7;
                                    $time3_rest = 0;
                                    $time4_rest = $rest7;
                                } else if ($effective_time > 720) {  // 12時間を超える場合
                                    $effective_time -= $rest6;
                                    $time3_rest = 0;
                                    $time4_rest = $rest6;
                                } else if ($effective_time > 540) {  // 9時間を超える場合
                                    $effective_time -= $rest3;
                                    $time3_rest = 0;
                                    $time4_rest = $rest3;
                                } else if ($effective_time > 480) {  // 8時間を超える場合
                                    $effective_time -= $rest2;
                                    $time3_rest = $rest2;
                                    $time4_rest = 0;
                                } else if ($effective_time > 360) {  // 6時間を超える場合
                                    $effective_time -= $rest1;
                                    $time3_rest = $rest1;
                                    $time4_rest = 0;
                                } else if ($effective_time > 240 && $tmp_start_time < "1200") {  // 4時間超え午前開始の場合
                                    $effective_time -= $rest4;
                                    $time3_rest = $rest4;
                                    $time4_rest = 0;
                                } else if ($effective_time > 240 && $tmp_start_time >= "1200") {  // 4時間超え午後開始の場合
                                    $effective_time -= $rest5;
                                    $time3_rest = $rest5;
                                    $time4_rest = 0;
                                }
                                //休日残業では休憩を表示しない 20100916
                                if ($legal_hol_over_flg) {
                                    $time3_rest = 0;
                                    $time4_rest = 0;
                                }
                                //休日で残業がない場合,出勤退勤がある場合 20100917
                                if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
                                    $time3_rest = 0;
                                    $time4_rest = 0;
                                }
                            } else {
                                //休憩時間 20101109
                                if ($start4 != "" && $end4 != "") {
                                    //所定労働時間との重なりを計算する 20140820
                                    if ($start2_date_time != "" && $end2_date_time != "") {
                                        $tmp_rest2 = $timecard_common_class->get_intersect_times($start2_date_time, $end2_date_time, $start4_date_time, $end4_date_time);
                                    }
                                    else {
                                        $tmp_rest2 = date_utils::get_diff_minute($end4_date_time, $start4_date_time);
                                    }
                                }
                            }

                            $work_time = $effective_time - $time3 - $time4;
                            //休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
                            if ($pattern == "10" && !$legal_hol_over_flg) {
                                $work_time = 0;
                            }
                            // 退勤後復帰回数・時間を算出
                            $return_count = 0;
                            $return_time = 0;
                            $return_late_time = 0; //20100715
                            //非常勤の分割時刻対応 20120521
                            $return_time_part1 = 0;
                            $return_time_part2 = 0;
                            $parttime_split_hhmm = $parttime_split_hour."00";
                            $wk_parttime_split_hhmm = $tmp_date.$parttime_split_hhmm;
                            //申請状態 "0":申請不要 "3":承認済 //勤務時間を計算しないフラグ確認 20130225
                            if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
                                for ($i = 1; $i <= 10; $i++) {
                                    $start_var = "o_start_time$i";
                                    if ($$start_var == "") {
                                        break;
                                    }
                                    $return_count++;

                                    $end_var = "o_end_time$i";
                                    if ($$end_var == "") {
                                        break;
                                    }

                                    //端数処理 20090709
                                    if ($fraction1 > "1") {
                                        //端数処理する分
                                        $moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
                                        //開始日時
                                        $tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
                                        //基準日時
                                        $tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
                                        $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
                                        $tmp_ret_start_time = substr($tmp_date_time, 8, 4);
                                    } else {
                                        $tmp_ret_start_time = str_replace(":", "", $$start_var);
                                    }
                                    if ($fraction2 > "1") {
                                        //端数処理する分
                                        $moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                                        //終了日時
                                        $tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
                                        //基準日時
                                        $tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
                                        $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

                                        $tmp_ret_end_time = substr($tmp_date_time, 8, 4);
                                    } else {
                                        $tmp_ret_end_time = str_replace(":", "", $$end_var);
                                    }
                                    $wk_ret_start = $tmp_date.$tmp_ret_start_time;
                                    $wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
                                    $wk_ret_time = date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
                                    $return_time += $wk_ret_time;

                                    //非常勤、分割集計18:00まで、18:00以降
                                    if ($split_time_flg && //csvの項目に選択されている場合
                                            $duty_form == "2") {
                                        if ($parttime_split_hour == "" || $wk_ret_end <= $wk_parttime_split_hhmm) {
                                            $return_time_part1 += date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
                                            $return_time_part2 += 0;
                                        }
                                        else {
                                            if ($wk_ret_start < $wk_parttime_split_hhmm) {
                                                $return_time_part1 += date_utils::get_diff_minute($wk_parttime_split_hhmm, $wk_ret_start);
                                                $return_time_part2 += date_utils::get_diff_minute($wk_ret_end, $wk_parttime_split_hhmm);
                                            }
                                            else {
                                                $return_time_part1 += 0;
                                                $return_time_part2 += date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
                                            }
                                        }
                                    }

                                    // 月の深夜勤務時間に退勤後復帰の深夜分を追加
                                    $return_late_time += $timecard_common_class->get_times_info_calc($late_night_info, $wk_ret_start, $wk_ret_end);
                                    //深夜残業
                                    $wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");

                                    $zan_sums_day[1] += $wk_late_night;
                                    //当日が休日の場合、休日に合計。
                                    if (($pattern != "10" && check_timecard_holwk(
                                                    $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) ||
                                            $reason == "16" ||
                                            ($pattern == "10"&& (($reason != "71"
                                                        && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f")))) {
                                        //当日の残業
                                        $today_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                        $zan_sums_day[2] += $today_over;
                                        //休日深夜 20101012
                                        $wk_today_late = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                        $zan_sums_day[3] += $wk_today_late;
                                    }
                                    //翌日の残業
                                    $next_date = next_date($tmp_date);
                                    if (check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //20130308
                                        $nextday_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "2", $timecard_bean);
                                        $zan_sums_day[2] += $nextday_over;
                                        $wk_nextday_late = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "2");
                                        $zan_sums_day[3] += $wk_nextday_late;
                                    }
                                    //法定外残業、深夜、休日集計
                                    if ($wk_ret_time > 0) {
                                        //当日が休日の場合
                                        if (($pattern != "10" && check_timecard_holwk(
                                                        $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true && $reason != "71") ||
                                                $reason == "16" ||
                                                ($pattern == "10" && (($reason != "71"
                                                            && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //事由が普通残業の場合を除く 20110128
                                            ) {
                                            $hoteigai_sums_day[2] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                            $hoteigai_sums_day[3] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                            // 事由が休日出勤の場合 2014/8/15
                                            if ($reason=="16") {
                                                $hoteigai_sums_day[4] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                                $hoteigai_sums_day[5] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                            }
                                        } else {
                                            $hoteigai_sums_day[0] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                            $hoteigai_sums_day[1] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                        }
                                        //翌日
                                        $next_date = next_date($tmp_date);
                                        if ($next_day_flag == "1" || $over_end_next_day_flag == "1" ||
                                                substr($wk_ret_end, 0, 8) == $next_date) { //復帰後退勤が翌日
                                            //休日出勤の判定
                                            if (check_timecard_holwk(
                                                        $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //20130308
                                                $hoteigai_sums_day[2] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                                $hoteigai_sums_day[3] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                                // 事由が休日出勤の場合 2014/8/15
                                                if ($reason=="16") {
                                                    $hoteigai_sums_day[4] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                                    $hoteigai_sums_day[5] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                                }
                                            } else {
                                                $hoteigai_sums_day[0] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                                $hoteigai_sums_day[1] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                            }
                                        }

                                    }
                                }
                            }
                            $wk_return_time = $return_time;
                            //呼出時間 20121109
                            $return_time_total += $return_time;
                            //呼出回数 20121109
                            $return_count_total += $return_count;

                            // 前日までの計 = 週計
                            $last_day_total = $work_time_for_week;

                            $work_time_for_week += $work_time;
                            $work_time_for_week += $wk_return_time;
                            if ($tmp_date < $start_date) {
                                $tmp_date = next_date($tmp_date);
                                $counter_for_week++;
                                continue;
                            }

                            // 深夜勤務時間を算出（分単位）
                            //残業開始、終了時刻がある場合 20101027
                            if ($over_start_time != "" && $over_end_time != "") {
                                $time2 = $timecard_common_class->get_times_info_calc($late_night_info, $over_start_date_time,$over_end_date_time);
                                //休憩除外 20140716
                                $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                                $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                                if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                                    $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                                    $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;
                                }
                                else {
                                    $rest_start_date_time1 = "";
                                    $rest_end_date_time1 = "";
                                }
                                $time2_rest = $timecard_common_class->get_times_info_calc($late_night_info, $rest_start_date_time1,$rest_end_date_time1);
                                //debug
                                //$tmp_emp_personal_id = "rest=".$time2_rest;
                                $time2 -= $time2_rest;

                            } else {
                                //$time2 = count(array_intersect($arr_effective_time, $arr_time5));
                                // 20101109
                                $time2 = $timecard_common_class->get_times_info_calc($late_night_info, $fixed_start_time, $fixed_end_time);
                                if ($time2 > 0) {
                                    //休憩
                                    $wk_start4_date_time = ($next_day_flag == "1") ? $work_times_info[2]["rest_start"] : $start4_date_time;
                                    $wk_end4_date_time = ($next_day_flag == "1") ? $work_times_info[2]["rest_end"] : $end4_date_time;
                                    $wk_rest_time = $timecard_common_class->get_intersect_times($fixed_start_time, $fixed_end_time, $wk_start4_date_time, $wk_end4_date_time);
                                    if ($wk_rest_time > 0) {
                                        //深夜時間帯か再確認
                                        $wk_s1 = max($fixed_start_time, $wk_start4_date_time);
                                        $wk_e1 = min($fixed_end_time, $wk_end4_date_time);
                                        $wk_rest_time = $timecard_common_class->get_times_info_calc($late_night_info, $wk_s1, $wk_e1);
                                        $time2 -= $wk_rest_time;
                                    }
                                }


                            }

                            //残業３−５追加 20140716
                            //残業３−５を追加、２−５の繰り返し処理とする
                            //残業２
                            for ($z_idx=2; $z_idx<=5; $z_idx++) {
                                $s_t = "over_start_time".$z_idx;
                                $e_t = "over_end_time".$z_idx;
                                $s_f = "over_start_next_day_flag".$z_idx;
                                $e_f = "over_end_next_day_flag".$z_idx;
                                $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                                $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                                $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                                $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                                if ($over_start_time_value != "" && $over_end_time_value != "") {
                                    //開始日時
                                    $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                                    //終了日時
                                    $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;

                                    $overtime2_late_time = $timecard_common_class->get_times_info_calc($late_night_info, $over_start_date_time_value, $over_end_date_time_value);
                                    //休憩除外 20140716
                                    $rs_t = "rest_start_time".$z_idx;
                                    $re_t = "rest_end_time".$z_idx;
                                    $rs_f = "rest_start_next_day_flag".$z_idx;
                                    $re_f = "rest_end_next_day_flag".$z_idx;
                                    $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                                    $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                                    $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                                    $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];
                                    if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                        $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                        $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                                    }
                                    else {
                                        $rest_start_date_time_value = "";
                                        $rest_end_date_time_value = "";
                                    }

                                    $overtime2_late_time_rest = $timecard_common_class->get_times_info_calc($late_night_info, $rest_start_date_time_value, $rest_end_date_time_value);
                                    $overtime2_late_time -= $overtime2_late_time_rest;
                                    $time2 += $overtime2_late_time;

                                }
                            }
                            $time2 += $return_late_time; //20100715
                            $sums[17] += $time2;

                            // 20100616 残業時間（深夜残業・休日残業除外）、深夜残業、休日残業
                            //残業承認時に計算、申請画面を表示しない場合は無条件に計算
                            if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
                                    $timecard_bean->over_time_apply_type == "0") {
                                //残業開始、終了時刻がある場合
                                if ($over_start_time != "" && $over_end_time != "") {
                                    ;
                                } else {
                                    /* 20120214 残業未設定じ計算しない  */
                                    $over_start_date_time = ""; //20101109
                                    $over_end_date_time = "";
                                }
                                $wk_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");

                                //休憩除外 20140716
                                $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                                $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                                if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                                    $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                                    $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;

//                                    $wk_late_night_rest = date_utils::get_time_difference($rest_end_date_time1,$rest_start_date_time1);
                                    $wk_late_night_rest = $timecard_common_class->get_late_night($work_times_info, $rest_start_date_time1,$rest_end_date_time1, "");
                                    //debug
                                    //$tmp_emp_personal_id .= "$tmp_date rest=".$wk_late_night_rest;

                                    $wk_late_night -= $wk_late_night_rest;
                                }

                                $zan_sums_day[1] += $wk_late_night;
                                //debug
                                //$tmp_emp_personal_id .= " $tmp_date late=".$zan_sums_day[1];

                                $wk_ptn_id = $tmcd_group_id."_".$pattern;
                                //休日残業
                                //前日フラグ
                                if ($previous_day_flag == "1") {
                                    //前日
                                    $last_date = last_date($tmp_date);
                                    //前日の残業
                                    $lastday_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "0");


                                    //休日出勤の判定変更 20100804
                                    if (check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$last_date], $pattern, $reason, "1", $timecard_bean) == true) {
                                        //事由が普通残業の場合を除く 20110128
                                        //if ($reason != "71") {
                                            $zan_sums_day[2] += $lastday_over;
                                        //}
                                        //休日深夜 20101012
                                        $wk_lastday_late = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "0");
                                        $zan_sums_day[3] += $wk_lastday_late;
                                    }
                                }
                                //当日が休日の場合、休日に合計。判定変更 20100804
                                if (($pattern != "10" && check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) ||
                                        $reason == "16" ||
                                        ($pattern == "10" && $over_start_time != "" && $over_end_time != "" &&
                                            $overtime_approve_flg != "1" && (($reason != "71"
                                                    && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //休暇で残業時刻がある場合 20101022
                                    ) {
                                    $next_date = next_date($tmp_date);
                                    //当日の残業
                                    $today_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "1");
                                    //事由が普通残業の場合を除く 20110128
                                    //if ($reason != "71") {
                                        $zan_sums_day[2] += $today_over;
                                    //}

                                    //休日深夜 20101012
                                    $wk_today_late = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "1");
                                    $zan_sums_day[3] += $wk_today_late;
                                }
                                //翌日フラグ
                                if ($over_end_next_day_flag == "1") { //20120214
                                    //翌日の配列
                                    $next_date = next_date($tmp_date);
                                    //翌日の残業
                                    $nextday_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "2");
                                    if (check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //20130308
                                        //事由が普通残業の場合を除く 20110128
                                        //if ($reason != "71") {
                                            $zan_sums_day[2] += $nextday_over;
                                        //}
                                        $wk_nextday_late = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "2");
                                        $zan_sums_day[3] += $wk_nextday_late;
                                    }

                                }
                            }
                            //休日出勤（勤務集計表）
                            if (in_array("2_76", $arr_csv_list)) {
                                //勤務パターングループ、勤務パターンID 20110118
                                $wk_ptn_id = $tmcd_group_id."_".$pattern;
                                //前日フラグ
                                if ($previous_day_flag == "1") {
                                    //前日の配列
                                    $last_date = last_date($tmp_date);
                                    //休日出勤の判定変更 20100804
                                    if (check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$last_date], $pattern, $reason, "1") == true) {
                                        if (in_array($wk_ptn_id, $arr_ptn_id) == false || //休日出勤時間除外するパターン以外
                                                $duty_form == "2") { //または、非常勤の場合
                                            //残業管理をしない場合
                                            if ($no_overtime == "t") {
                                                $last_hol_work_time = $timecard_common_class->get_hol_work($work_times_info, $fixed_start_time, $fixed_end_time, "", "", 0);
                                            } else {
                                                $last_hol_work_time = $timecard_common_class->get_hol_work($work_times_info, $fixed_start_time, $fixed_end_time, $start2_date_time, $end2_date_time, 0);
                                            }
                                            $hol_work += $last_hol_work_time;
                                        }
                                    }
                                }
                                //当日が休日の場合、休日に合計。判定変更 20100804
                                if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") { //休日残業20101105
                                    $today_hol_work_time = 0;
                                } else {
                                    if (($pattern != "10" && check_timecard_holwk(
                                                    $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "") == true) ||
                                            $reason == "16"
                                        ) {
                                        $next_date = next_date($tmp_date);
                                        //休日出勤
                                        if (in_array($wk_ptn_id, $arr_ptn_id) == false || //休日出勤時間除外するパターン以外
                                                $duty_form == "2") { //または、非常勤の場合
                                            //残業管理をしない場合
                                            if ($no_overtime == "t") {
                                                $today_hol_work_time = $timecard_common_class->get_hol_work($work_times_info, $fixed_start_time, $fixed_end_time, "", "", 1);
                                            } else {
                                                $today_hol_work_time = $timecard_common_class->get_hol_work($work_times_info, $fixed_start_time, $fixed_end_time, $start2_date_time, $end2_date_time, 1);
                                            }
                                            if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20101025
                                                //休憩を減算
                                                $today_hol_work_time = $today_hol_work_time - $time3_rest - $time4_rest;
                                            }
                                            $hol_work += $today_hol_work_time;
                                        }
                                    }
                                }
                                //翌日フラグ
                                if ($next_day_flag == "1") {
                                    //翌日の配列
                                    $next_date = next_date($tmp_date);
                                    //休日出勤の判定変更 20100804
                                    if (check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1") == true) { //20130308
                                        //休日出勤
                                        if (in_array($wk_ptn_id, $arr_ptn_id) == false || //休日出勤時間除外するパターン以外
                                                $duty_form == "2") { //または、非常勤の場合
                                            //残業管理をしない場合
                                            if ($no_overtime == "t") {
                                                $next_hol_work_time = $timecard_common_class->get_hol_work($work_times_info, $fixed_start_time, $fixed_end_time, "", "", 2);
                                            } else {
                                                $next_hol_work_time = $timecard_common_class->get_hol_work($work_times_info, $fixed_start_time, $fixed_end_time, $start2_date_time, $end2_date_time, 2);
                                            }
                                            $hol_work += $next_hol_work_time;
                                        }
                                    }
                                }

                                //残業時刻２が入力されている場合、時間集計 20111121
                                if ($over_start_time2 != "" && $over_end_time2 != "") {
                                    //日別の休日を考慮した時間を取得
                                    $today_hol_flag = (
                                            check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, ""));
                                    $next_date = next_date($tmp_date);
                                    $nextday_hol_flag = (   check_timecard_holwk(
                                                $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1")); //20130308
                                    $wk_holtime = $timecard_common_class->get_holtime_today_nextday($tmp_date, $over_start_time2, $over_start_next_day_flag2, $over_end_time2, $over_end_next_day_flag2, $today_hol_flag, $nextday_hol_flag, $timecard_common_class);
                                    $hol_work += $wk_holtime;
                                }
                                //非常勤で休日の場合、呼出分も集計 20120521
                                if ($duty_form == "2" &&
                                        check_timecard_holwk(
                                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "") == true) {
                                    $hol_work += $return_time;
                                }
                            }

                            //非常勤、分割集計18:00まで、18:00以降
                            if ($split_time_flg && //csvの項目に選択されている場合
                                    $duty_form == "2" ) { //残業申請中を除く 条件削除 20110720
			                    //分割集計の処理についてだけ、所定開始より残業１が早い場合、残業１と残業２を入れ替える 20141125
			                    if ($start2 != "" && $over_end_time != "" && $over_end_time <= $start2 && $over_end_next_day_flag != "1") {
		                             $sp_over_start_time = $over_start_time2;
		                             $sp_over_end_time = $over_end_time2;
		                             $sp_over_start_next_day_flag = $over_start_next_day_flag2;
		                             $sp_over_end_next_day_flag = $over_end_next_day_flag2;
		                             $sp_over_start_time2 = $over_start_time;
		                             $sp_over_end_time2 = $over_end_time;
		                             $sp_over_start_next_day_flag2 = $over_start_next_day_flag;
		                             $sp_over_end_next_day_flag2 = $over_end_next_day_flag;
			                    }
			                    else {
		                             $sp_over_start_time = $over_start_time;
		                             $sp_over_end_time = $over_end_time;
		                             $sp_over_start_next_day_flag = $over_start_next_day_flag;
		                             $sp_over_end_next_day_flag = $over_end_next_day_flag;
		                             $sp_over_start_time2 = $over_start_time2;
		                             $sp_over_end_time2 = $over_end_time2;
		                             $sp_over_start_next_day_flag2 = $over_start_next_day_flag2;
		                             $sp_over_end_next_day_flag2 = $over_end_next_day_flag2;
			                    }
                                $fixed_start_time = str_replace(":", "", $fixed_start_time);
                                $fixed_end_time = str_replace(":", "", $fixed_end_time);
                                $tmp_start_time = substr($fixed_start_time, 8, 4);
                                //開始時刻が0000の場合で前日フラグがある場合、調整
                                if ($tmp_start_time == "0000" && $previous_day_flag == "1") {
                                    $previous_day_flag = "0";
                                }
                                //所定時間帯の範囲内の残業対応 20121022
                                $sp_over_start_time_org = "";
                                $sp_over_end_time_org = "";
                                if ($start2 != "" && //所定時間帯がある場合
                                        $sp_over_start_time != "" && //残業時刻１設定済み
                                        $sp_over_end_time != "" &&
                                        $start_time != $sp_over_start_time) { //勤務開始と残業開始が同じ場合を除く
                                    $chk_over_end_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $sp_over_end_time, $sp_over_end_next_day_flag);
                                    $wk_start2_date_time = str_replace(":", "", $start2_date_time);
                                    $wk_end2_date_time = str_replace(":", "", $end2_date_time);
                                    if ($wk_start2_date_time < $chk_over_end_date_time && //残業時刻終了が所定範囲内
                                            $chk_over_end_date_time < $wk_end2_date_time) {
                                        $sp_over_start_time_org = $sp_over_start_time;
                                        $sp_over_end_time_org = $sp_over_end_time;
                                        $sp_over_start_time = "";
                                        $sp_over_end_time = "";
                                    }
                                }
                                //残業時刻がある場合 20111121
                                if ($sp_over_end_time != "") {
                                    $tmp_end_time = $sp_over_end_time;
                                    $next_day_flag = $sp_over_end_next_day_flag;
                                }
                                else {
                                    $tmp_end_time = substr($fixed_end_time, 8, 4);
                                }
                                $parttime_split_hhmm = $parttime_split_hour."00";

                                //退勤が翌日になる場合の対応
                                $wk_end_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_end_time, $next_day_flag);

                                //退勤時刻から残業開始時刻の間を除外する 20120119
                                $wk_jogai_flg = false;
                                $wk_jogai1 = 0;
                                $wk_jogai2 = 0;
                                //残業開始時刻が退勤時刻から離れているか確認
                                if ($sp_over_start_time != "" && $ovtm_apply_status == "1" && $end_time != "") {
                                    $chk_end_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
                                    if ($chk_end_date_time > $end2_date_time) {
                                        $chk_end_date_time = $end2_date_time;
                                    }
                                    else {
                                        //所定より早い退勤の場合、端数処理
                                        if ($fraction2 > "1") {
                                            //端数処理する分
                                            $moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                                            $tmp_office_start_time = substr($chk_end_date_time, 0, 10) . "00";
                                            $chk_end_date_time = date_utils::move_time($tmp_office_start_time, $chk_end_date_time, $moving_minutes, $fraction2);

                                        }
                                    }
                                    $chk_over_start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $sp_over_start_time, $sp_over_start_next_day_flag);
                                    if ($chk_over_start_date_time > $chk_end_date_time) {
                                        $wk_jogai_flg = true;
                                        //分割時刻なし
                                        if ($parttime_split_hour == "") {
                                            $wk_jogai1 = date_utils::get_diff_minute($chk_over_start_date_time, $chk_end_date_time);
                                        }
                                        //分割時刻あり
                                        else {
                                            //18:00まで
                                            $wk_parttime_split_hhmm = $tmp_date.$parttime_split_hour."00";
                                            $wk_jogai1 = $timecard_common_class->get_intersect_times($start2_date_time, $wk_parttime_split_hhmm, $chk_end_date_time, $chk_over_start_date_time);
                                            //18:00以降
                                            $wk_jogai2 = $timecard_common_class->get_intersect_times($wk_parttime_split_hhmm, next_date($tmp_date)."2359", $chk_end_date_time, $chk_over_start_date_time);

                                        }
                                    }

                                }
                                $wk_parttime_split_hhmm = $tmp_date.$parttime_split_hhmm;
                                //分割設定なし、分割設定以前
                                if ($parttime_split_hour == "" || $wk_end_date_time < $wk_parttime_split_hhmm) {
                                    $wk_start_date_time = $tmp_date.$tmp_start_time;
                                    if ($previous_day_flag == "1") {
                                        $wk_start_date_time = date_utils::add_day_ymdhi($wk_start_date_time, -1);
                                    }
                                    $wk_minutes1 = date_utils::get_diff_minute($wk_end_date_time, $wk_start_date_time);

                                    //退勤時刻から残業開始時刻の間を除外する 20120119
                                    if ($wk_jogai_flg) {
                                        $wk_minutes1 -= $wk_jogai1;
                                    }
                                    //休憩の確認、時間給者かつ休憩除外
                                    $rest_time = 0;
                                    if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
                                        if ($wk_minutes1 > 1200) {  // 20時間を超える場合 20100929
                                            $rest_time = $rest7;
                                        } else if ($wk_minutes1 > 720) {  // 12時間を超える場合
                                            $rest_time = $rest6;
                                        } else if ($wk_minutes1 > 540) {  // 9時間を超える場合
                                            $rest_time = $rest3;
                                        } else if ($wk_minutes1 > 480) {  // 8時間を超える場合
                                            $rest_time = $rest2;
                                        } else if ($wk_minutes1 > 360) {  // 6時間を超える場合
                                            $rest_time = $rest1;
                                        } else if ($wk_minutes1 > 240 && $tmp_start_time < "1200") {  // 4時間超え午前開始の場合
                                            $rest_time = $rest4;
                                        } else if ($wk_minutes1 > 240 && $tmp_start_time >= "1200") {  // 4時間超え午後開始の場合
                                            $rest_time = $rest5;
                                        }
                                    } else {
                                        if ($start4 != "" && $end4 != "") {
                                            //実働時間内の休憩時間確認 20100925
                                            $rest_time = $timecard_common_class->get_intersect_times($start_date_time, $end_date_time, $start4_date_time, $end4_date_time);
                                        } else {
                                            $rest_time = 0;
                                        }
                                    }
                                    $wk_minutes1 -= $rest_time;
                                    $wk_minutes1 -= $time3; //外出 20101109

                                    $work_time1 += $wk_minutes1;
                                    $wk_minutes2 = 0;
                                } else {
                                    //分割設定以降
                                    $wk_start_date_time1 = $tmp_date.$tmp_start_time;
                                    $wk_end_date_time1 = $tmp_date.$parttime_split_hhmm;
                                    $time3 = $timecard_common_class->get_intersect_times($wk_start_date_time1, $wk_end_date_time1, $out_date_time, $ret_date_time);

                                    //残業開始が分割時刻以降の場合 //20111122
                                    if ($sp_over_start_time != "" && $sp_over_start_time > $parttime_split_hhmm && $sp_over_start_time == $start_time) {
                                        $wk_start_date_time2 = $tmp_date.$over_start_time;
                                    }
                                    else {
                                        $wk_start_date_time2 = $tmp_date.$parttime_split_hhmm;
                                    }
                                    //残業時刻がある場合 20111121
                                    if ($sp_over_end_time != "") {
                                        $tmp_end_time = $sp_over_end_time;
                                        $next_day_flag = $sp_over_end_next_day_flag;
                                    }

                                    $wk_end_date_time2 = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_end_time, $next_day_flag);
                                    $time4 = $timecard_common_class->get_intersect_times($wk_start_date_time2, $wk_end_date_time2, $out_date_time, $ret_date_time);

                                    //start が18:00より前か確認
                                    if ($wk_start_date_time1 < $wk_end_date_time1) {
                                        $wk_minutes1 = date_utils::get_time_difference($wk_end_date_time1, $wk_start_date_time1);
                                    }
                                    else {
                                        $wk_minutes1 = 0;
                                    }
                                    $wk_minutes2 = date_utils::get_time_difference($wk_end_date_time2, $wk_start_date_time2);
                                    //休憩の確認、時間給者かつ休憩除外
                                    if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ勤務パターンで時刻指定がない場合 20100925
                                        $wk_minutes = $wk_minutes1 + $wk_minutes2;
                                        //退勤時刻から残業開始時刻の間を除外する 20120119
                                        if ($wk_jogai_flg) {
                                            $wk_minutes -= $wk_jogai1;
                                            $wk_minutes -= $wk_jogai2;
                                        }
                                        if ($wk_minutes > 1200) {  // 20時間を超える場合 20100929
                                            $rest_time = $rest7;
                                            $wk_minutes1 -= $rest7;
                                        } else if ($wk_minutes > 720) {  // 12時間を超える場合
                                            $rest_time = $rest6;
                                            $wk_minutes1 -= $rest6;
                                        } else if ($wk_minutes > 540) {  // 9時間を超える場合
                                            $rest_time = $rest3;
                                            $wk_minutes1 -= $rest3;
                                        } else if ($wk_minutes > 480) {  // 8時間を超える場合
                                            $wk_minutes1 -= $rest2;
                                            $rest_time = $rest2;
                                        } else if ($wk_minutes > 360) {  // 6時間を超える場合
                                            $rest_time = $rest1;
                                            $wk_minutes1 -= $rest1;
                                        } else if ($wk_minutes > 240 && $tmp_start_time < "1200") {  // 4時間超え午前開始の場合
                                            $rest_time = $rest4;
                                            $wk_minutes1 -= $rest4;
                                        } else if ($wk_minutes > 240 && $tmp_start_time >= "1200") {  // 4時間超え午後開始の場合
                                            $rest_time = $rest5;
                                            $wk_minutes1 -= $rest5;
                                        }
                                    } elseif ($start4 != "" && $end4 != "") {//休憩時刻がある場合
                                        $start4_date_time = $tmp_date.$start4;
                                        $end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);
                                        $wk_rest1 = $timecard_common_class->get_intersect_times($wk_start_date_time1, $wk_end_date_time1, $start4_date_time, $end4_date_time);
                                        $wk_rest2 = $timecard_common_class->get_intersect_times($wk_start_date_time2, $wk_end_date_time2, $start4_date_time, $end4_date_time);
                                        $wk_minutes1 -= $wk_rest1;
                                        $wk_minutes2 -= $wk_rest2;
                                    }
                                    $wk_minutes1 -= $time3; //外出 20101109
                                    $wk_minutes2 -= $time4; //外出 20101109
                                    //退勤時刻から残業開始時刻の間を除外する 20120119
                                    if ($wk_jogai_flg) {
                                        $wk_minutes1 -= $wk_jogai1;
                                        $wk_minutes2 -= $wk_jogai2;
                                    }
                                    $work_time1 += $wk_minutes1;
                                    $work_time2 += $wk_minutes2;
                                }
                                //残業時刻２が入力されている場合、時間集計 20111121
                                if ($sp_over_start_time2 != "" && $sp_over_end_time2 != "") {
                                    $arr_split_time = $timecard_common_class->get_parttime_split($parttime_split_hour, $tmp_date, $sp_over_start_time2, $sp_over_start_next_day_flag2, $sp_over_end_time2, $sp_over_end_next_day_flag2);
                                    $work_time1 += $arr_split_time[0];
                                    $work_time2 += $arr_split_time[1];
                                }
                                if ($sp_over_start_time_org != "" && $sp_over_end_time_org != "") {
                                    $arr_split_time = $timecard_common_class->get_parttime_split($parttime_split_hour, $tmp_date, $sp_over_start_time_org, $sp_over_start_next_day_flag, $sp_over_end_time_org, $sp_over_end_next_day_flag);
                                    $work_time1 += $arr_split_time[0];
                                    $work_time2 += $arr_split_time[1];
                                }
                                //呼出分 20120521
                                $work_time1 += $return_time_part1;
                                $work_time2 += $return_time_part2;
                            }
                        } else {
                            // 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
                            // 月の開始日前は処理しない
                            if ($tmp_date < $start_date) {
                                $tmp_date = next_date($tmp_date);
                                $counter_for_week++;
                                continue;
                            }
                        }
                    } else {

                        // 事由が「代替休暇」「振替休暇」の場合、
                        // 所定労働時間を稼働時間にプラス
                        // 「有給休暇」を除く $reason == "1" || 2008/10/14
                        if ($reason == "4" || $reason == "17") {
                            if ($tmp_date >= $start_date) {
                                $sums[15] += $specified_time;
                                $work_time_for_week += $specified_time;
                            }
                        }
                        // 退勤後復帰時間を算出 20100413
                        $return_time = 0;
                        $return_count = 0;
                        //申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない //勤務時間を計算しないフラグ確認 20130225
                        if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
                            for ($i = 1; $i <= 10; $i++) {
                                $start_var = "o_start_time$i";
                                if ($$start_var == "") {
                                    break;
                                }
                                $return_count++;

                                $end_var = "o_end_time$i";
                                if ($$end_var == "") {
                                    break;
                                }
                                //端数処理
                                if ($fraction1 > "1") {
                                    //端数処理する分
                                    $moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
                                    //開始日時
                                    $tmp_start_date_time = $tmp_date.$$start_var;
                                    //基準日時
                                    $tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
                                    $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
                                    $tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
                                } else {
                                    $tmp_ret_start_time = str_replace(":", "", $$start_var);
                                }
                                if ($fraction2 > "1") {
                                    //端数処理する分
                                    $moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                                    //終了日時
                                    $tmp_end_date_time = $tmp_date.$$end_var;
                                    //基準日時
                                    $tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
                                    $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

                                    $tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
                                } else {
                                    $tmp_ret_end_time = str_replace(":", "", $$end_var);
                                }
                                $wk_ret_start = $tmp_date.$tmp_ret_start_time;
                                $wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
                                $wk_ret_time = date_utils::get_diff_minute($wk_ret_end, $wk_ret_start);
                                $return_time += $wk_ret_time;

                                //前日・翌日の深夜帯
                                // 月の深夜勤務時間に退勤後復帰の深夜分を追加
                                $return_late_time += $timecard_common_class->get_times_info_calc($late_night_info, $wk_ret_start, $wk_ret_end);
                                $sums[17] += $return_late_time;
                                //法定外残業、深夜、休日集計
                                if ($wk_ret_time > 0) {
                                    //当日が休日の場合
                                    if (($pattern != "10" && check_timecard_holwk(
                                                    $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true && $reason != "71") ||
                                            $reason == "16" ||
                                            ($pattern == "10" && (($reason != "71"
                                                        && $reason != "24" && !in_array($reason, $timecard_bean->arr_hol_ovtm_except_reason)) || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //事由が普通残業の場合を除く 20110128
                                        ) {
                                        $hoteigai_sums_day[2] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                        $hoteigai_sums_day[3] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                        // 事由が休日出勤の場合 2014/8/15
                                        if ($reason=="16") {
                                            $hoteigai_sums_day[4] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                            $hoteigai_sums_day[5] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                        }
                                    } else {
                                        $hoteigai_sums_day[0] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                        $hoteigai_sums_day[1] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                                    }
                                    //翌日
                                    if ($next_day_flag == "1" || $over_end_next_day_flag == "1") {
                                        $next_date = next_date($tmp_date);
                                        //休日出勤の判定
                                        if (check_timecard_holwk(
                                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //20130308
                                            $hoteigai_sums_day[2] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                            $hoteigai_sums_day[3] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                            // 事由が休日出勤の場合 2014/8/15
                                            if ($reason=="16") {
                                                $hoteigai_sums_day[4] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                                $hoteigai_sums_day[5] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                            }
                                        } else {
                                            $hoteigai_sums_day[0] += $timecard_common_class->get_day_time($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                            $hoteigai_sums_day[1] += $timecard_common_class->get_late_night($work_times_info, $wk_ret_start, $wk_ret_end, "2");
                                        }
                                    }

                                }
                            }
                        }
                        $wk_return_time = $return_time;
                        //呼出時間 20121109
                        $return_time_total += $return_time;
                        //呼出回数 20121109
                        $return_count_total += $return_count;

                        // 前日までの計 = 週計
                        $last_day_total = $work_time_for_week;

                        $work_time_for_week += $work_time;
                        $work_time_for_week += $wk_return_time;
                        if ($tmp_date < $start_date) {
                            $tmp_date = next_date($tmp_date);
                            $counter_for_week++;
                            continue;
                        }
                    }


                    // 残業時間の処理を変更 20090629
                    $wk_hoteigai = 0;
                    $wk_hoteinai_zan = 0;
                    $wk_hoteinai = 0;

                    //日数換算
                    $total_workday_count = "";
                    if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
                    {
                        //休暇時は計算しない 20090806
                        if ($pattern != "10") {
                            if($workday_count != "")
                            {
                                $total_workday_count = $workday_count;
                            }
                        }

                        if($night_duty_flag)
                        {
                            //曜日に対応したworkday_countを取得する
                            $total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

                        }
                    }
                    else if ($workday_count == 0){
                        //日数換算に0が指定された場合のみ0を入れる
                        $total_workday_count = $workday_count;
                    }
                    //法定外残業の基準時間
                    if ($base_time == "") {
                        $wk_base_time = 8 * 60;
                    } else {
                        $base_hour = intval(substr($base_time, 0, 2));
                        $base_min = intval(substr($base_time, 2, 2));
                        $wk_base_time = $base_hour * 60 + $base_min;
                    }
                    //公休か事由なし休暇で残業時刻がありの場合 20100802
                    if ($legal_hol_over_flg) {
                        $wk_base_time = 0;
                    }
                    //残業時間 20100209 変更 20100910
                    $wk_zangyo = 0;
                    $kinmu_time = 0;
                    //出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
                    //勤務時間を計算しないフラグが"1"以外 20130225
                    if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
                        //残業承認時に計算、申請画面を表示しない場合は無条件に計算
                        if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
                                $timecard_bean->over_time_apply_type == "0"
                            ) {

                            //残業開始、終了時刻がある場合
                            if ($over_start_time != "" && $over_end_time != "") {
                                $wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
                                //休憩除外 20140716
                                $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                                $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                                if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                                    $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                                    $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;

                                    $wk_zangyo_rest = date_utils::get_time_difference($rest_end_date_time1,$rest_start_date_time1);

                                    $wk_zangyo -= $wk_zangyo_rest;
                                }
                                //残業時刻未設定は計算しない 20110825
                                //} else {
                                //  //残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
                                //  if ($end2_date_time < $fixed_end_time) {
                                //      $wk_zangyo = date_utils::get_time_difference($fixed_end_time,$end2_date_time);
                                //  }
                            }
                            $next_date = next_date($tmp_date); //20141107 休日残業不具合対応
                            //残業３−５追加 20140716
                            //残業３−５を追加、２−５の繰り返し処理とする
                            //残業２
                            for ($z_idx=2; $z_idx<=5; $z_idx++) {
                                $s_t = "over_start_time".$z_idx;
                                $e_t = "over_end_time".$z_idx;
                                $s_f = "over_start_next_day_flag".$z_idx;
                                $e_f = "over_end_next_day_flag".$z_idx;
                                $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                                $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                                $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                                $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                                //残業2
                                if ($over_start_time_value != "" && $over_end_time_value != "") {
                                    //開始日時
                                    $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                                    //終了日時
                                    $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;

                                    //休憩除外 20140716
                                    $rs_t = "rest_start_time".$z_idx;
                                    $re_t = "rest_end_time".$z_idx;
                                    $rs_f = "rest_start_next_day_flag".$z_idx;
                                    $re_f = "rest_end_next_day_flag".$z_idx;
                                    $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                                    $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                                    $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                                    $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];
                                    if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                        $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                        $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                                    }
                                    else {
                                        $rest_start_date_time_value = "";
                                        $rest_end_date_time_value = "";
                                    }
                                    //残業時間データ
                                    $arr_overtime_data2 = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time_value, $over_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class, $rest_start_date_time_value, $rest_end_date_time_value, $end2_date_time); //20130308
                                    //計算
                                    //普通残業
                                    $normal_over = $arr_overtime_data2["normal_over"];
                                    //深夜
                                    $late_night = $arr_overtime_data2["late_night"];
                                    //休日残業
                                    $hol_over = $arr_overtime_data2["hol_over"];
                                    //休日深夜
                                    $hol_late_night = $arr_overtime_data2["hol_late_night"];
                                    $overtime_data2 = $normal_over + $late_night + $hol_over + $hol_late_night;
                                    $wk_zangyo += $overtime_data2;
                                    //$zan_sums index 0:残業時間（深夜残業・休日残業除外） 1:深夜残業 2:休日残業 3:休日深夜残業

                                    $zan_sums_day[0] += $normal_over;
                                    $zan_sums_day[1] += $late_night + $hol_late_night;
                                    //debug
                                    //$tmp_emp_personal_id .= " $tmp_date late=".$zan_sums_day[1];
                                    //事由が普通残業の場合は加算しない
                                    //if ($reason != "71") {
                                    $zan_sums_day[2] += $hol_over;
                                    //}
                                    $zan_sums_day[3] += $hol_late_night;

                                }
                            }
                            //早出残業
                            if ($early_over_time != "") {
                                $wk_early_over_time = hmm_to_minute($early_over_time);
                                $wk_zangyo += $wk_early_over_time;
                            }
                        }
                        //呼出
                        $wk_zangyo += $wk_return_time;

                        //休日残業で休憩時刻がある場合 20100921
                        if ($legal_hol_over_flg == true && $tmp_rest2 > 0) {
                            $wk_zangyo -= $tmp_rest2;
                        }
                        //所定開始時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
                        if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
                            $overtime_only_flg = true;
                            //遅刻
                            $tikoku_time = 0;
                            //早退
                            $sotai_time = 0;
                            //残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100922
                            if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
                                    $timecard_bean->over_time_apply_type == "0"
                                ) {
                                $wk_zangyo -= $tmp_rest2;
                            } else {
                                $tmp_rest2 = 0;
                                $time_rest = "";
                            }
                        } else {
                            $overtime_only_flg = false;
                            //時間有休と相殺しない場合で時間有休ある場合 20140807
                            $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                            if ($ovtm_class->sousai_sinai_flg == "t" && $paid_hol_min != "") {
                                //遅刻
                                $tikoku_time = 0;
                                //早退
                                $sotai_time = 0;
                            }
                            else {
                                //遅刻
                                $tikoku_time = $start_time_info["diff_minutes"];
                                //早退
                                $sotai_time = $end_time_info["diff_minutes"];
                            }
                        }
                        $tikoku_time2 = 0;
                        // 遅刻時間を残業時間から減算する場合
                        if ($timecard_bean->delay_overtime_flg == "1") {
                            if ($wk_zangyo >= $tikoku_time) { //20100913
                                $wk_zangyo -= $tikoku_time;
                            } else { // 残業時間より多い遅刻時間対応 20100925
                                $tikoku_time2 = $tikoku_time - $wk_zangyo;
                                $wk_zangyo = 0;
                            }
                        }

                        //勤務時間＝所定労働時間−休憩時間−外出時間−早退−遅刻＋残業時間 20100907
                        //　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
                        //　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
                        //所定
                        if ($pattern == "10" || //休暇は所定を0とする 20100916
                                $overtime_only_flg == true ||   //残業のみ計算する 20100917
                                $after_night_duty_flag == "1"  //明けの場合 20110819
                            ) {
                            $shotei_time = 0;
                        } elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
                            $shotei_time = date_utils::get_diff_minute($fixed_end_time, $fixed_start_time);
                        } else {
                            $shotei_time = date_utils::get_diff_minute($end2_date_time, $start2_date_time);
                        }
                        //時間有休追加 20111207
                        //if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                        //    $shotei_time -= $paid_time_hour;
                        //}
                        $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                        if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_hol_min != "") {
                            //時間有休と相殺しない場合
                            if ($ovtm_class->sousai_sinai_flg == "t") {
                                //遅刻早退が時間有休より多い場合、その分は勤務時間から減算されるようにする
                                if ($start_time_info["diff_minutes"] != "") {
                                    if ($start_time_info["diff_minutes"] >= $paid_hol_min) {
                                        $shotei_time -= $start_time_info["diff_minutes"];
                                    }
                                    else {
                                        $shotei_time -= $paid_hol_min;
                                    }
                                }
                                if ($end_time_info["diff_minutes"] != "") {
                                    if ($end_time_info["diff_minutes"] >= $paid_hol_min) {
                                        $shotei_time -= $end_time_info["diff_minutes"];
                                    }
                                    else {
                                        $shotei_time -= $paid_hol_min;
                                    }
                                }
                            }
                            else {
                                $shotei_time -= $paid_hol_min;
                            }
                        }
                        //休憩 $tmp_rest2 $time3_rest $time4_rest
                        //外出 $time3 $time4
                        $kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
                        // 遅刻時間を残業時間から減算しない場合
                        if ($timecard_bean->delay_overtime_flg == "2") {
                            $kinmu_time -= $tikoku_time;
                        }
                        // 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
                        else {
                            if ($overtime_approve_flg == "1") {
                                $kinmu_time -= $tikoku_time;
                            } else { //遅刻時間不具合対応 20101005
                                // 残業時間より多い遅刻時間対応 20100925
                                $kinmu_time -= $tikoku_time2;
                            }
                        }
                        // 早退時間を残業時間から減算しない場合
                        if ($timecard_bean->early_leave_time_flg == "2") {
                            $kinmu_time -= $sotai_time;
                        }
                        //時間給者 20100915
                        if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
                            $kinmu_time -= ($time3_rest + $time4_rest);
                        } else {
                            if ($overtime_only_flg != true && $legal_hol_over_flg != true) {    //残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
                                //月給制日給制、実働時間内の休憩時間
                                $kinmu_time -= $tmp_rest2;
                            }
                        }

                        //法定内、法定外残業の計算
                        if ($wk_zangyo > 0) {
                            //法定内入力有無確認
                            if ($legal_in_over_time != "") {
                                $wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
                                if ($wk_hoteinai_zan <= $wk_zangyo) {
                                    $wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
                                } else {
                                    $wk_hoteigai = 0;
                                }

                            } else {
                                //法定外残業の基準時間を超える分を法定外残業とする
                                if ($kinmu_time > $wk_base_time) {
                                    $wk_hoteigai = $kinmu_time - $wk_base_time;
                                }
                                //
                                if ($wk_zangyo - $wk_hoteigai > 0) {
                                    $wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
                                } else {
                                    //マイナスになる場合
                                    $wk_hoteinai_zan = 0;
                                    $wk_hoteigai = $wk_zangyo;
                                }
                            }
                            //遅刻早退で法定内残業がある場合、法定外残業とする 20141024 start
                            if ($ovtm_class->delay_overtime_inout_flg == "t") {
								//時間有休、外出を追加 20141203
								if ($wk_hoteinai_zan > 0 &&
									(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"]) > 0 ||
									 $paid_hol_min > 0 ||
									 $time3 > 0)) {
									$wk_furikae_kouho = max(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"] + $time3), $paid_hol_min);
									$wk_furikae = min($wk_hoteinai_zan, $wk_furikae_kouho);
                                    $wk_hoteinai_zan -= $wk_furikae;
                                    $wk_hoteigai += $wk_furikae;
                                }
                            }
                            //遅刻早退で法定内残業がある場合、法定外残業とする 20141024 end
                        }
                        // 法定外残業時間 20101206 $wk_hoteigaiを$wk_zangyoに変更(法定内残業の不具合対応) 20130507
                        if ($wk_zangyo > 0 && (($over_start_time != "" && $over_end_time != "") ||
                                    ($over_start_time2 != "" && $over_end_time2 != ""))) { //残業時刻があるか確認 20120928
                            $next_date = next_date($tmp_date); //20141107 休日残業不具合対応
                            //残業１、残業２の法定外時間の休日深夜考慮を取得
                            //$hoteigai_sums index 0:法定外残業（深夜・休日残業除外） 1:法定外深夜残業（休日除外） 2:法定外休日残業（深夜除外） 3:法定外休日深夜残業
                            //残業１
                            if ($over_start_time != "" && $over_end_time != "") {
                                //開始日時
                                $over_start_date_time = ($over_start_next_day_flag == 1) ? next_date($tmp_date).$over_start_time : $tmp_date.$over_start_time;
                                //終了日時
                                $over_end_date_time = ($over_end_next_day_flag == 1) ? next_date($tmp_date).$over_end_time : $tmp_date.$over_end_time;

                                //休憩除外 20140716
                                $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                                $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                                if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                                    $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                                    $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;
                                }
                                else {
                                    $rest_start_date_time1 = "";
                                    $rest_end_date_time1 = "";
                                }
                                //残業時間データ
                                $arr_overtime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time, $over_end_date_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class, $rest_start_date_time1, $rest_end_date_time1, $end2_date_time); //20130308
                                $hoteigai_sums_day[0] += $arr_overtime_data["normal_over"];
                                $hoteigai_sums_day[1] += $arr_overtime_data["late_night"];
                                $hoteigai_sums_day[2] += $arr_overtime_data["hol_over"];
                                $hoteigai_sums_day[3] += $arr_overtime_data["hol_late_night"];
                                // 事由が休日出勤の場合 2014/8/15
                                if ($reason=="16") {
                                    $hoteigai_sums_day[4] += $arr_overtime_data["hol_over"];
                                    $hoteigai_sums_day[5] += $arr_overtime_data["hol_late_night"];
                                }
                                $hoteigai_sums_day[6] += $arr_overtime_data["hol_over_sixty_data"];
                                $hoteigai_sums_day[7] += $arr_overtime_data["hol_late_night_sixty_data"];
                            }
                            //残業３−５追加 20140716
                            //残業３−５を追加、２−５の繰り返し処理とする
                            //残業２
                            for ($z_idx=2; $z_idx<=5; $z_idx++) {
                                $s_t = "over_start_time".$z_idx;
                                $e_t = "over_end_time".$z_idx;
                                $s_f = "over_start_next_day_flag".$z_idx;
                                $e_f = "over_end_next_day_flag".$z_idx;
                                $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                                $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                                $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                                $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                                //残業2
                                if ($over_start_time_value != "" && $over_end_time_value != "") {
                                    //開始日時
                                    $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                                    //終了日時
                                    $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;

                                    //休憩除外 20140716
                                    $rs_t = "rest_start_time".$z_idx;
                                    $re_t = "rest_end_time".$z_idx;
                                    $rs_f = "rest_start_next_day_flag".$z_idx;
                                    $re_f = "rest_end_next_day_flag".$z_idx;
                                    $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                                    $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                                    $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                                    $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];
                                    if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                        $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                        $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                                    }
                                    else {
                                        $rest_start_date_time_value = "";
                                        $rest_end_date_time_value = "";
                                    }
                                    //残業時間データ
                                    $arr_overtime_data2 = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time_value, $over_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class, $rest_start_date_time_value, $rest_end_date_time_value, $end2_date_time); //20130308
                                    $hoteigai_sums_day[0] += $arr_overtime_data2["normal_over"];
                                    $hoteigai_sums_day[1] += $arr_overtime_data2["late_night"];
                                    $hoteigai_sums_day[2] += $arr_overtime_data2["hol_over"];
                                    $hoteigai_sums_day[3] += $arr_overtime_data2["hol_late_night"];
	                                // 事由が休日出勤の場合 2014/8/15
	                                if ($reason=="16") {
	                                    $hoteigai_sums_day[4] += $arr_overtime_data2["hol_over"];
	                                    $hoteigai_sums_day[5] += $arr_overtime_data2["hol_late_night"];
	                                }
                                    $hoteigai_sums_day[6] += $arr_overtime_data2["hol_over_sixty_data"];
                                    $hoteigai_sums_day[7] += $arr_overtime_data2["hol_late_night_sixty_data"];
                                }
                            }

                            //休憩時間無
                            //時間給者、深夜、深夜以外の順で引く
                            if ($wage == "4" && $start4 == "" && $end4 == "") {
                                ;
                            }
                            //休憩時間有
                            else if ($start4 != "" && $end4 != "") {
                                //休日残業$legal_hol_over_flg
                                //所定開始時刻と残業開始時刻が一致している場合$overtime_only_flg
                                if ($legal_hol_over_flg || $overtime_only_flg) {
                                    $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $start4_date_time, $end4_date_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"]); //20130308
                                    if ($hoteigai_sums_day[0] >= $arr_resttime_data["normal_over"]) {
                                        $hoteigai_sums_day[0] -= $arr_resttime_data["normal_over"];
                                    }
                                    if ($hoteigai_sums_day[1] >= $arr_resttime_data["late_night"]) {
                                        $hoteigai_sums_day[1] -= $arr_resttime_data["late_night"];
                                    }
                                    if ($hoteigai_sums_day[2] >= $arr_resttime_data["hol_over"]) {
                                        $hoteigai_sums_day[2] -= $arr_resttime_data["hol_over"];
                                        if ($reason=="16"){ // 2014/8/15追加
                                            $hoteigai_sums_day[4] -= $arr_resttime_data["hol_over"];
                                        }
                                    }
                                    if ($hoteigai_sums_day[3] >= $arr_resttime_data["hol_late_night"]) {
                                        $hoteigai_sums_day[3] -= $arr_resttime_data["hol_late_night"];
                                        if ($reason=="16"){ // 2014/8/15追加
                                            $hoteigai_sums_day[5] -= $arr_resttime_data["hol_late_night"];
                                        }
                                    }
                                    //20130822
                                    if ($zan_sums_day[0] >= $arr_resttime_data["normal_over"]) {
                                        $zan_sums_day[0] -= $arr_resttime_data["normal_over"];
                                    }
                                    if ($zan_sums_day[1] >= ($arr_resttime_data["late_night"]+$arr_resttime_data["hol_late_night"])) { //深夜（休日含む）
                                        $zan_sums_day[1] -= ($arr_resttime_data["late_night"]+$arr_resttime_data["hol_late_night"]);
                                    }
                                    if ($zan_sums_day[2] >= $arr_resttime_data["hol_over"]) {
                                        $zan_sums_day[2] -= $arr_resttime_data["hol_over"];
                                    }
                                    if ($zan_sums_day[3] >= $arr_resttime_data["hol_late_night"]) {
                                        $zan_sums_day[3] -= $arr_resttime_data["hol_late_night"];
                                    }
                                }
                            }

                        }
                        //法定内残業が深夜残業、休日残業と重複しないよう再計算
                        $night_or_hol_time = $hoteigai_sums_day[1] + $hoteigai_sums_day[2] + $hoteigai_sums_day[3];
                        if ($night_or_hol_time > $wk_hoteigai) {
                            //深夜休日が法定外より多い場合、法定内残業から減算する
                            $wk_time = $night_or_hol_time - $wk_hoteigai;
                            if ($wk_time > 0) {
                                if ($wk_hoteinai_zan > $wk_time) {
                                    $wk_hoteinai_zan -= $wk_time;
                                } else {
                                    //法定内分を法定外へ加算する場合はコメントをはずす 20130312
                                    //$wk_hoteigai += $wk_hoteinai_zan;
                                    $wk_hoteinai_zan = 0;
                                }
                            }
                        }
                        //法定内時間
                        if ($wk_hoteinai_zan > 0) {
                            //法定外（休日・深夜以外）から法定内分を除く
                            if ($hoteigai_sums_day[0] >= $wk_hoteinai_zan) {
                                $hoteigai_sums_day[0] -= $wk_hoteinai_zan;
                            }
                            else {
                                $hoteigai_sums_day[0] = 0;
                            }

                        }
                        //合計
                        //法定内
                        $wk_hoteinai = $kinmu_time - $wk_zangyo;
                        $hoteinai += $wk_hoteinai;
                        //法定内残業
                        $hoteinai_zan += $wk_hoteinai_zan;
                        //法定外残
                        $hoteigai += $wk_hoteigai;
                        //勤務時間
                        $sums[15] += $kinmu_time;
                        //残業時間
                        $sums[16] += $wk_zangyo;
                        //法定外残業集計(1) 20121206
                        for ($i=0; $i<8; $i++) {
                            $hoteigai_sums[$i] += $hoteigai_sums_day[$i];
                        }
                        //法定外残業集計(2) 20121206
                        for ($i=0; $i<4; $i++) {
                            $zan_sums[$i] += $zan_sums_day[$i]; //残業時間 20130822
                        }
                        //月別
                        if ($tmp_date >= $month_chk_date) {
                            $hoteigai_sums_month = array_fill(0, 2, 0); //index 0:法定外残業（深夜・休日残業除外） 1:法定外深夜残業（休日除外）複数月ある場合用
                        }
                        $hoteigai_sums_month[0] += $hoteigai_sums_day[0];
                        $hoteigai_sums_month[1] += $hoteigai_sums_day[1];

                        //週40時間超えた残業 20130425
                        $tmp_arr_week_forty += $kinmu_time - $wk_zangyo + $wk_hoteinai_zan;
                        //期間が1ヶ月以上の場合、別に集計
                        if ($tmp_date >= $month_chk_date) {

                            //休日対応
                            $over_fix_time_total[0] += $over_fix_time[0];
                            $over_fix_time_total[1] += $over_fix_time[1];
                            $over_fix_time_total[2] += $over_fix_time[2];
                            $over_fix_time_total[3] += $over_fix_time[3];
                            $over_fix_time = array_fill(0,4,0); //index 0:60時間超法定外残業 1:60時間深夜残業
                            $over_fix_time_total2[0] += $over_fix_time2[0];
                            $over_fix_time_total2[1] += $over_fix_time2[1];
                            $over_fix_time_total2[2] += $over_fix_time2[2];
                            $over_fix_time_total2[3] += $over_fix_time2[3];
                            $over_fix_time2 = array_fill(0,4,0);

                            $month_chk_date = date("Ymd", strtotime("+1 month",date_utils::to_timestamp_from_ymd($month_chk_date) ));
                        }

                        //45時間超の計算 $fortyfive_value(2700) 20150715
                        $fortyfive_over = $hoteigai_sums_month[0] + $hoteigai_sums_month[1];
                        // 法定休暇以外の休日残業とする休暇は45,60時間超の残業時間の計算に含める
                        if ($timecard_bean->sixty_overtime_nonlegal_flg == "t"){
                            $hoteigai_sums_hol_month[0] += $hoteigai_sums_day[6]; //６０時間超計算用休日残業
                            $hoteigai_sums_hol_month[1] += $hoteigai_sums_day[7]; //６０時間超計算用休日深夜

                            $fortyfive_over +=  $hoteigai_sums_hol_month[0] +  $hoteigai_sums_hol_month[1];
                            $hol_fortyfive = $hoteigai_sums_day[6]; //45時間超計算用休日残業
                            $hol_fortyfive_night = $hoteigai_sums_day[7]; //45時間超計算用休日深夜
                        }else{
                            $hol_fortyfive = 0;
                            $hol_fortyfive_night = 0;
                        }

                        if ($fortyfive_over > $fortyfive_value && $sixty_over <= $sixty_value){
                            if (($fortyfive_over - $fortyfive_value) > ($hoteigai_sums_day[0] + $hoteigai_sums_day[1] + $hol_fortyfive + $hol_fortyfive_night)){
                                //休日対応
                                //60
								if ($fortyfive_over <= $sixty_value) {
	                                $over_fix_time2[0] += $hoteigai_sums_day[0];     //法定外残業
	                                $over_fix_time2[1] += $hoteigai_sums_day[1]; //深夜残業
	                                $over_fix_time2[2] += $hol_fortyfive;        //休日残業
	                                $over_fix_time2[3] += $hol_fortyfive_night;  //休日深夜
                                }
                                else {
                                	$wk_nokori = $sixty_value - ($fortyfive_over - $hoteigai_sums_day[0] - $hoteigai_sums_day[1] - $hol_fortyfive - $hol_fortyfive_night);
									if ($wk_nokori > $hoteigai_sums_day[0]) {
										$over_fix_time2[0] += $hoteigai_sums_day[0];
										$wk_nokori -= $hoteigai_sums_day[0];
									}
									else {
										$over_fix_time2[0] += $wk_nokori;
										$wk_nokori = 0;
									}
									if ($wk_nokori > 0) {
										if ($wk_nokori > $hol_fortyfive) {
											$over_fix_time2[2] += $hol_fortyfive;
											$wk_nokori -= $hol_fortyfive;
										}
										else {
											$over_fix_time2[2] += $wk_nokori;
											$wk_nokori = 0;
										}
									}
									if ($wk_nokori > 0) {
										if ($wk_nokori > $hoteigai_sums_day[1]) {
											$over_fix_time2[1] += $hoteigai_sums_day[1];
										}
										else {
											$over_fix_time2[1] += $wk_nokori;
											$wk_nokori = 0;
										}

									}
									if ($wk_nokori > 0) {
										if ($wk_nokori > $hol_fortyfive_night) {
											$over_fix_time2[3] += $hol_fortyfive_night;
										}
										else {
											$over_fix_time2[3] += $wk_nokori;
										}

									}
                                	
                                }

                            }else{
                                //休日対応 優先順に計上、休日深夜＞平日深夜＞休日残業＞平日残業
                                $surplus = $fortyfive_over-$fortyfive_value;
                                //休日深夜
                                if ($hol_fortyfive_night > 0) {
                                    if ($surplus > $hol_fortyfive_night) {
                                        $over_fix_time2[3] += $hol_fortyfive_night;
                                        $surplus -= $hol_fortyfive_night;
                                    }
                                    else {
                                        $over_fix_time2[3] += $surplus;
                                        $surplus = 0;
                                    }
                                }
                                //平日深夜
                                if ($hoteigai_sums_day[1] > 0) {
                                    if ($surplus > $hoteigai_sums_day[1]) {
                                        $over_fix_time2[1] += $hoteigai_sums_day[1];
                                        $surplus -= $hoteigai_sums_day[1];
                                    }
                                    else {
                                        $over_fix_time2[1] += $surplus;
                                        $surplus = 0;
                                    }
                                }
                                //休日残業
                                if ($hol_fortyfive > 0) {
                                    if ($surplus > $hol_fortyfive) {
                                        $over_fix_time2[2] += $hol_fortyfive;
                                        $surplus -= $hol_fortyfive;
                                    }
                                    else {
                                        $over_fix_time2[2] += $surplus;
                                        $surplus = 0;
                                    }
                                }
                                //法定外残業
                                if ($hoteigai_sums_day[0] > 0) {
                                    $over_fix_time2[0] += $surplus;
                                    $surplus = 0;
                                }
                            }

                        }

                        //60時間超の計算 $sixty_value(3600) 20130405
                        $sixty_over = $hoteigai_sums_month[0] + $hoteigai_sums_month[1];
                        // 法定休暇以外の休日残業とする休暇は６０時間超の残業時間の計算に含める 20140919
                        if ($timecard_bean->sixty_overtime_nonlegal_flg == "t"){
                            //$hoteigai_sums_hol_month[0] += $hoteigai_sums_day[6]; //６０時間超計算用休日残業
                            //$hoteigai_sums_hol_month[1] += $hoteigai_sums_day[7]; //６０時間超計算用休日深夜

                            $sixty_over +=  $hoteigai_sums_hol_month[0] +  $hoteigai_sums_hol_month[1];
                            $hol_sixty = $hoteigai_sums_day[6]; //６０時間超計算用休日残業
                            $hol_sixty_night = $hoteigai_sums_day[7]; //６０時間超計算用休日深夜
                        }else{
                            $hol_sixty = 0;
                            $hol_sixty_night = 0;
                        }

                        if ($sixty_over > $sixty_value){
                            if (($sixty_over - $sixty_value) > ($hoteigai_sums_day[0] + $hoteigai_sums_day[1] + $hol_sixty + $hol_sixty_night)){
                                //休日対応 20141021
                                $over_fix_time[0] += $hoteigai_sums_day[0];     //法定外残業
                                $over_fix_time[1] += $hoteigai_sums_day[1]; //深夜残業
                                $over_fix_time[2] += $hol_sixty;        //休日残業
                                $over_fix_time[3] += $hol_sixty_night;  //休日深夜

                            }else{
                                //休日対応 優先順に計上、休日深夜＞平日深夜＞休日残業＞平日残業
                                $surplus = $sixty_over-$sixty_value;
                                //休日深夜
                                if ($hol_sixty_night > 0) {
                                    if ($surplus > $hol_sixty_night) {
                                        $over_fix_time[3] += $hol_sixty_night;
                                        $surplus -= $hol_sixty_night;
                                    }
                                    else {
                                        $over_fix_time[3] += $surplus;
                                        $surplus = 0;
                                    }
                                }
                                //平日深夜
                                if ($hoteigai_sums_day[1] > 0) {
                                    if ($surplus > $hoteigai_sums_day[1]) {
                                        $over_fix_time[1] += $hoteigai_sums_day[1];
                                        $surplus -= $hoteigai_sums_day[1];
                                    }
                                    else {
                                        $over_fix_time[1] += $surplus;
                                        $surplus = 0;
                                    }
                                }
                                //休日残業
                                if ($hol_sixty > 0) {
                                    if ($surplus > $hol_sixty) {
                                        $over_fix_time[2] += $hol_sixty;
                                        $surplus -= $hol_sixty;
                                    }
                                    else {
                                        $over_fix_time[2] += $surplus;
                                        $surplus = 0;
                                    }
                                }
                                //法定外残業
                                if ($hoteigai_sums_day[0] > 0) {
                                    $over_fix_time[0] += $surplus;
                                    $surplus = 0;
                                }
                                //残業時間の一部が60時間を超える場合には、分割して計上する（深夜残業優先計上）
                                /*
                                if ($surplus > ($hoteigai_sums_day[1] + $hol_late_night + $hol_sixty_night)){

                                    $over_fix_time[0] += $surplus - $hoteigai_sums_day[1] - $hol_late_night - $hol_sixty_night;
                                    $over_fix_time[1] += $hoteigai_sums_day[1] + $hol_late_night + $hol_sixty_night;
                                }else{
                                    $over_fix_time[1] += $surplus;
                                }
                                */
                            }

                        }

                    }
                    //週40時間超残業 20130425
                    if (!$buf_first_flg){
                        $tmp_week = get_weekday(to_timestamp($tmp_date));
                        if ($forty_last_day == $tmp_week){
                            if ($previous_month_time != ""){
                                $tmp_arr_week_forty += $previous_month_time;    //前月のデータを加算する
                                $previous_month_time = "";
                            }


                            $forty_surplus = 0;
                            if ($tmp_arr_week_forty > $week_forty_value){
                                $forty_surplus = $tmp_arr_week_forty - $week_forty_value;   //週40時間越えた分を計上
                            }
                            //キー:日付＋曜日=>結果代入
                            $week_forty_overtime = ($forty_surplus == 0) ? "0:00" : minute_to_hmm($forty_surplus);
                            $week_forty_overtime_total += $forty_surplus; //週40時間合計

                            $tmp_arr_week_forty = 0;
                        }
                    }


                    if ($tra_flg &&
                            $pattern != "") {
                        //交通費
                        //常勤・非常勤
                        if ($duty_form != "2") {
                            $tra_cnt += $tra_full[$tmp_group_id][$pattern];
                        } elseif ($duty_form == "2") {
                            if (in_array($pattern, $tra_part[$tmp_group_id])) {
                                $tra_cnt++;
                            }
                        }

                    }
                    //滞留時間 "2_59" 20111229
                    $end2_next_day_flag = 0;
                    if ($end_time != "" && $end2 != "") {
                        //所定終了翌日フラグ設定
                        if (($start2 > $end2 && $atdptn_previous_day_flag != "1") || $over_24hour_flag == "1") {
                            $end2_next_day_flag = 1;
                        }
                        //所定終了日時
                        if ($end2_next_day_flag) {
                            $end2_date_time = next_date($tmp_date).$end2;
                        } else {
                            $end2_date_time = $tmp_date.$end2;
                        }
                        //退勤日時
                        if ($next_day_flag) {
                            $end_date_time = next_date($tmp_date).$end_time;
                        } else {
                            $end_date_time = $tmp_date.$end_time;
                        }
                        $wk_retention_min = date_utils::get_diff_minute($end_date_time ,$end2_date_time);
                        if ($wk_retention_min > 0) {
                            $retention_time += $wk_retention_min;
                        }
                    }

                    //遅刻早退控除計算
                    if (in_array("12_0", $arr_csv_list)) {
                        //控除しないフラグ確認
                        $wk_late_no_deduction_flag = false;
                        $wk_leave_no_deduction_flag = false;
                        $wk_kind_cnt = count($arr_late_data[$tmp_date]);
                        if ($wk_kind_cnt > 0) {
                            for ($l_idx=0; $l_idx<$wk_kind_cnt; $l_idx++) {
                                if ($arr_late_data[$tmp_date][$l_idx]["apply_stat"] == "1" &&
                                        $arr_late_data[$tmp_date][$l_idx]["ireg_no_deduction_flg"] == "t") {
                                    if ($arr_late_data[$tmp_date][$l_idx]["kind_flag"] == "1") {
                                        $wk_late_no_deduction_flag = true;
                                    }
                                    else {
                                        $wk_leave_no_deduction_flag = true;
                                    }
                                }
                            }
                        }
                        //遅刻
                        if ($start_time_info["diff_minutes"] > 0 &&
                                $wk_late_no_deduction_flag == false) {
                            //休憩を考慮した時間 20140512
                            $l_diff_time = $start_time_info["diff_minutes"];
                            $l_diff_time = round_time($l_diff_time, $late_fraction, $late_fraction_min); //2番目"2":切り上げ 3番目2:30分
                            $late_early_total += $l_diff_time;
                        }
                        //早退
                        if ($end_time_info["diff_minutes"] > 0 &&
                                $wk_leave_no_deduction_flag == false) {
                            //休憩を考慮した時間 20140512
                            $l_diff_time = $end_time_info["diff_minutes"];
                            $l_diff_time = round_time($l_diff_time, $late_fraction, $late_fraction_min); //2番目"2":切り上げ 3番目2:30分
                            $late_early_total += $l_diff_time;
                        }
                    }
                    $tmp_date = next_date($tmp_date);
                    $counter_for_week++;
                } // end of while

                // 早退時間を残業時間から減算する設定の場合、法定内残業、法定外残業の順に減算する
                if ($early_leave_time_flg == "1") {

                    list($hoteinai_zan, $hoteigai) = $atdbk_common_class->get_overtime_early_leave($hoteinai_zan, $hoteigai, $sums[13]);
                    // 早退時間を残業時業から減算する 20100713
                    $sums[16] -= $sums[13];
                }
                // 要勤務日数を取得 20091222
                $wk_year = substr($c_yyyymm, 0, 4);
                $wk_mon = substr($c_yyyymm, 4, 2);
                if ($from_flg != "2") {
                    //当月日数
                    $days_in_month = days_in_month($wk_year, $wk_mon);
                    //対象年月の公休数を取得 20100615
                    if ($closing_month_flg == "2") { //締め日の月度が終了日の月の場合
                        $wk_year2 = substr($wk_end_date, 0, 4);
                        $wk_mon2 = substr($wk_end_date, 4, 2);
                    } else {
                        $wk_year2 = $wk_year;
                        $wk_mon2 = $wk_mon;
                    }
                    $wk_tmcd_group_id = get_timecard_group_id($con, $tmp_emp_id, $fname);
                    $legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $wk_year2, $wk_mon2, $closing, $closing_month_flg, $wk_tmcd_group_id);
                    $sums[0] = $days_in_month - $legal_hol_cnt;
                }
                else {
                    //公休調整数
                    //調整数を取得
                    if ($tmp_group_id2 == "") {
                        $wk_tmcd_group_id = $tmcd_group_id;
                        if ($wk_tmcd_group_id == "") {
                            $wk_tmcd_group_id = get_timecard_group_id($con, $tmp_emp_id, $fname);
                        }

                        $legal_hol_cnt = get_legal_hol_adjust_cnt($con, $fname, $wf_start_date, $wk_end_date, $shift_group_id, $wk_tmcd_group_id);
                    }
                    //複数グループの場合
                    else {
                        $legal_hol_cnt = 0;
                    }
                    $sums[0] = $kinmu_cnt - $legal_hol_cnt;

                }

                // 法定内勤務
                // 法定内 = 勤務時間 - 残業時間  20100713
                $hotei_sums[0] = $sums[15] - $sums[16];
                // 法定内残業
                $hotei_sums[1] = $hoteinai_zan;
                // 法定外残業
                $hotei_sums[2] = $hoteigai;

                // 変則労働期間が月の場合
                if ($arr_irrg_info["irrg_type"] == "2") {

                    // 所定労働時間を基準時間とする
                    $tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
                    if ($hol_minus == "t") {
                        $tmp_irrg_minutes -= (8 * 60 * $holiday_count);
                    }
                    $sums[14] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

                    // 稼働時間−基準時間を残業時間とする
                    $sums[16] = $sums[15] - $sums[14];
                }
                else {
                    // 基準時間の計算を変更 2008/10/21
                    // 要勤務日数 × 所定労働時間
                    $sums[14] = $sums[0] * date_utils::hi_to_minute($day1_time);
                }

                //端数処理前稼働時間 20121002
                $wk_kadou_time_org = $sums[15];
                // 月集計値の稼働時間・残業時間・深夜勤務を端数処理
                for ($i = 15; $i <= 17; $i++) {
                    if ($sums[$i] < 0) {
                        $sums[$i] = 0;
                    }
                    //残業集計項目の３０分未満は、切り捨て、３０分以上は切り上げて１時間単位にする場合は元々の端数処理をしない
                    if ($i>=16 && $arr_config["fraction_flg"] == "t") {
                    	continue;
                    }
                    $sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
                }

                //残業表示をするように変更 20130507
                // 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
                //              if ($no_overtime == "t") {
                //                  $sums[16] = 0;
                //                  $hotei_sums[0] = 0;
                //                  $hotei_sums[1] = 0;
                //                  $hotei_sums[2] = 0;
                //                  for ($i=0; $i<4; $i++) {
                //                      $zan_sums[$i] = 0;
                //                  }
                //              }

                // 当直
                if ($night_allow_flg) {
                    $arr_night_duty_date = get_night_duty_date($con, $fname, $tmp_emp_id, $start_date, $end_date, "atdbkrslt");
                    foreach($arr_night_duty_date as $night_duty_date)
                    {
                        $date = $night_duty_date["date"];
                        $timestamp = to_timestamp($date);

                        // 日付から休日かを求める
                        //$holiday = ktHolidayName($timestamp);
                        // 日付から曜日を求める
                        $wd = get_weekday($timestamp);

                        if($arr_type[$date] == "6" || $wd == "日") //20150213 $holiday != "" から変更
                        {
                            $sums[20]++;
                        }
                        else if($wd == "土")
                        {
                            $sums[19]++;
                        }
                        else
                        {
                            $sums[18]++;
                        }
                    }
                }
                // 手当
                if ($allow_flg) {
                    $arr_allow_summary = get_allow_summary_for_manage($con, $fname, $tmp_emp_id, $start_date, $end_date, "atdbkrslt");
                    $allow_cnt = 20;
                    foreach($arr_allow_summary as $allow_summary)
                    {
                        $allow_cnt++;
                        $sums[$allow_cnt] = $allow_summary["allow_cnt"];
                    }
                }

                // 締め済みの場合
            } else {

                // 月集計値の取得
                $sql = "select days1, days2, days3, days4, days5, days6, days7, days8, days9, days10, count1, count2, time1, time2, time3, time4, time5, time6, days11, days12, days13, days14, days15, days16, days17, days18, days19, days20, days21, days22, time7, time8, time9, time10, days23 from wktotalm";
                $cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $sums[0] += pg_fetch_result($sel, 0, "days1");
                $sums[1] += pg_fetch_result($sel, 0, "days2");
                $sums[2] += pg_fetch_result($sel, 0, "days3");
                $sums[3] += pg_fetch_result($sel, 0, "days4");
                $sums[4] += pg_fetch_result($sel, 0, "days5");
                $sums[5] += pg_fetch_result($sel, 0, "days6");
                $sums[6] += pg_fetch_result($sel, 0, "days7");
                $sums[7] += pg_fetch_result($sel, 0, "days8");
                $sums[8] += pg_fetch_result($sel, 0, "days9");
                $sums[9] += pg_fetch_result($sel, 0, "days10");
                $sums[10] += pg_fetch_result($sel, 0, "count1");
                $sums[11] += pg_fetch_result($sel, 0, "count2");
                $sums[12] += hmm_to_minute(pg_fetch_result($sel, 0, "time1"));
                $sums[13] += hmm_to_minute(pg_fetch_result($sel, 0, "time2"));
                $sums[14] += hmm_to_minute(pg_fetch_result($sel, 0, "time3"));
                $sums[15] += hmm_to_minute(pg_fetch_result($sel, 0, "time4"));
                $sums[16] += hmm_to_minute(pg_fetch_result($sel, 0, "time5"));
                $sums[17] += hmm_to_minute(pg_fetch_result($sel, 0, "time6"));
                // 追加事由
                $add_sums[0] = pg_fetch_result($sel, 0, "days11");
                $add_sums[1] = pg_fetch_result($sel, 0, "days12");
                $add_sums[2] = pg_fetch_result($sel, 0, "days13");
                $add_sums[3] = pg_fetch_result($sel, 0, "days14");
                $add_sums[4] = pg_fetch_result($sel, 0, "days15");
                $add_sums[5] = pg_fetch_result($sel, 0, "days16");
                $add_sums[6] = pg_fetch_result($sel, 0, "days17");
                $add_sums[7] = pg_fetch_result($sel, 0, "days18");
                $add_sums[8] = pg_fetch_result($sel, 0, "days19");
                $add_sums[9] = pg_fetch_result($sel, 0, "days20");
                $add_sums[10] = pg_fetch_result($sel, 0, "days21");
                $add_sums[11] = pg_fetch_result($sel, 0, "days22");
                $add_sums[12] = pg_fetch_result($sel, 0, "days23");
                // 年末年始追加対応
                if ($add_sums[12] != "") {
                    $wk_nissu = $add_sums[9];
                    $add_sums[9] = $add_sums[12];
                    $add_sums[12] = $add_sums[11];
                    $add_sums[11] = $add_sums[10];
                    $add_sums[10] = $wk_nissu;
                } else {
                    $add_sums[12] = $add_sums[11];
                    $add_sums[11] = $add_sums[10];
                    $add_sums[10] = $add_sums[9];
                    $add_sums[9] = "0";
                }

                //法定内勤務〜法定外残業、休日勤務
                $hotei_sums[0] = hmm_to_minute(pg_fetch_result($sel, 0, "time7"));
                $hotei_sums[1] = hmm_to_minute(pg_fetch_result($sel, 0, "time8"));
                $hotei_sums[2] = hmm_to_minute(pg_fetch_result($sel, 0, "time9"));
                $hotei_sums[3] = hmm_to_minute(pg_fetch_result($sel, 0, "time10"));


                // 当直
                if ($night_allow_flg) {
                    $arr_night_duty_date = get_night_duty_date($con, $fname, $tmp_emp_id, $start_date, $end_date, "wktotald");
                    foreach($arr_night_duty_date as $night_duty_date)
                    {
                        $date = $night_duty_date["date"];
                        $timestamp = to_timestamp($date);

                        // 日付から休日かを求める
                        //$holiday = ktHolidayName($timestamp);
                        // 日付から曜日を求める
                        $wd = get_weekday($timestamp);

                        if($arr_type[$date] == "6" || $wd == "日") //20150213 $holiday != "" から変更
                        {
                            $sums[20]++;
                        }
                        else if($wd == "土")
                        {
                            $sums[19]++;
                        }
                        else
                        {
                            $sums[18]++;
                        }
                    }
                }
                // 手当
                if ($allow_flg) {
                    $arr_allow_summary = get_allow_summary_for_manage($con, $fname, $tmp_emp_id, $start_date, $end_date, "wktotald");
                    $allow_cnt = 20;
                    foreach($arr_allow_summary as $allow_summary)
                    {
                        $allow_cnt++;
                        $sums[$allow_cnt] = $allow_summary["allow_cnt"];
                    }
                }

            } // end of if 締め済み

            //前月の分を取得（週40時間超残業) 20130425
            if ($buf_first_flg){
                $previous_month_time = $tmp_arr_week_forty;
                continue;
            }

            //残業時間（深夜残業・休日残業除外）
            $zan_sums[0] = $sums[16] - $zan_sums[1] - $zan_sums[2];

            //出勤時間を０とする出力対象を常勤、非常勤で選択可能とする 20091113
            if ($output_target == "2" && $duty_form == "2") { //常勤のみの場合、非常勤の出勤時間を０とする
                $sums[15] = 0;
            }
            if ($output_target == "3" && $duty_form != "2") {//非常勤のみの場合、常勤の出勤時間を０とする
                $sums[15] = 0;
            } //その他は出力
            //遅刻早退時間計 20101012
            $tikoku_sotai_time = $sums[12] + $sums[13];
            //時間形式追加対応 1:"hh:mm" 2:"hh.hh" 3:"hh.mm" 20131017
            $tikoku_sotai_time =  $timecard_common_class->minute_to_time_format($tikoku_sotai_time, $time_format);

            //所定時間 "2_80" 勤務時間 + 有給休暇日数×所定労働時間 - 法定外残業時間 - 当直回数×設定時間 20101203
            //所定労働時間(empcond.specified_time、calendarname.day1_time、優先使用)
            //半日有休、所定労働時間履歴対応 20121128
            $wk_shotei = $sums[15] +  $paid_sums[4] - $hotei_sums[2] - ($fixed_set_cnt * $fixed_sub_time);
			//所定時間（法定内残業含まない） 20150831
            $wk_shotei2 = $sums[15] +  $paid_sums[4] - $hotei_sums[2] - $hotei_sums[1] - ($fixed_set_cnt * $fixed_sub_time);
            // 月集計値を表示用に編集
            $sums_raw_values = array();// 2014/8/15追加
            for ($i = 12; $i <= 17; $i++) {
                $sums_raw_values[$i] = $sums[$i]; // 2014/8/15 コロン形式などに変換される前の値を保持するように
                $sums[$i] =  $timecard_common_class->minute_to_time_format($sums[$i], $time_format);
            }

            //休日出勤回数、設定職種の場合のみ出力 20090902
            //ただし設定職種が0件の場合は全て出力
            if (count($arr_job_id_list) > 0) {
                if (!in_array($tmp_emp_job_id, $arr_job_id_list)) {
                    $sums[2] = 0;
                }
            }

            //履歴取得
            if ($from_flg == "2") { //勤務シフト作成からの場合（出勤表側は画面に合わせ最新を使用）
                $wk_target_date = next_date($end_date);
                $arr_emp_info_hist = get_emp_info_history($con, $fname, $tmp_emp_id, $wk_target_date);
                if ($arr_emp_info_hist["duty_form"] != "") {
                    $tmp_duty_form = $arr_emp_info_hist["duty_form"];
                }
                if ($arr_emp_info_hist["job_nm"] != "") {
                    $tmp_job_nm = $arr_emp_info_hist["job_nm"];
                }
                if ($arr_emp_info_hist["st_nm"] != "") {
                    $tmp_st_nm = $arr_emp_info_hist["st_nm"];
                }
            }

            //出力順変更

            //項目値情報 $item[key] = $value
            $item = array();
            //職員情報 "1_?"
            $item["1_0"] = $tmp_emp_personal_id;
            $item["1_1"] = $tmp_emp_name;
            $item["1_2"] = $tmp_class_nm;
            $item["1_3"] = $tmp_atrb_nm;
            $item["1_4"] = $tmp_dept_nm;
            $item["1_5"] = $tmp_job_nm;
            $item["1_6"] = $tmp_duty_form;
            $item["1_7"] = $tmp_group_name;
            $item["1_8"] = $tmp_st_nm;
            $item["1_9"] = $salary_id;
            $item["1_10"] = $tmp_emp_sex;
            $item["1_11"] = $tmp_emp_birth;
            $item["1_12"] = $tmp_emp_join;
            $item["1_13"] = $tmp_duty_form_type;
            $item["1_14"] = $org_corp_cd;
            $item["1_15"] = substr($org_yyyymm, 0, 4)."年".intval(substr($org_yyyymm, 4, 2))."月";
            //要勤務日数〜早退日数 "2_?"
            for ($i=0; $i<12; $i++) {
                $id = "2_".$i;
                $item[$id] = $sums[$i];
            }
            //遅刻早退日数（計）"2_75" 20101022
            $item["2_75"] = $sums[10] + $sums[11];
            //基準時間、勤務時間
            for ($i=12; $i<14; $i++) {
                $id = "2_".$i;
                $j = $i + 2;
                $item[$id] = $sums[$j];
            }
            //残業時間（深夜、休日含む）、深夜勤務時間
            for ($i=14; $i<16; $i++) {
                $id = "2_".$i;
                $j = $i + 2;
                $item[$id] = $timecard_common_class->minute_to_time_format(calc_fraction($sums_raw_values[$j], $arr_config), $time_format);
            }
            
            //法定内勤務〜休日勤務
            for ($i=16; $i<20; $i++) {
                $id = "2_".$i;
                $j = $i - 16;
                $item[$id] =  $timecard_common_class->minute_to_time_format(calc_fraction($hotei_sums[$j], $arr_config), $time_format);
            }
            //公休日数〜初盆休暇日数、年末年始を追加、法定、所定を追加
            for ($i=20; $i<35; $i++) {
                $id = "2_".$i;
                $j = $i - 20;
                $item[$id] = $add_sums[$j];
            }
            //年月設定 20100917
            $wk_year = substr($c_yyyymm, 0, 4);
            $wk_mon = substr($c_yyyymm, 4, 2);
            //年休残追加 20100705
            $item["2_86"] = $timecard_common_class->get_nenkyu_zan($wk_year, $wk_mon, $tmp_emp_id, $timecard_bean->closing_paid_holiday, $start_date, $end_date);

            //時間有休 20111207
            $item["2_61"] = "";
            $item["2_60"] = "";
            $item["12_5"] = "";
            $item["12_6"] = "";
            if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                //１日分の所定労働時間(分)
                $specified_time_per_day = $obj_hol_hour->get_specified_time($tmp_emp_id, $day1_time);
                //当月分
                $wk_day = $paid_sums[3] / $specified_time_per_day;
                $item["2_61"] = $obj_hol_hour->edit_nenkyu_zan($wk_day);
                $item["12_5"] = $timecard_common_class->minute_to_time_format($paid_sums[3], $time_format);
                //残時間 12_6
		        $wk_output_flg = "3"; //hh:mm
		        //時間有休がある場合の年休残文字列取得
		        $ret_str = $obj_hol_hour->get_nenkyu_zan_str($wk_year, $wk_mon, $tmp_emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $start_date, $end_date, $specified_time_per_day, $item["2_86"], $duty_form, $wk_output_flg, $timecard_common_class);

				$arr_day_hhmm = explode("(", $ret_str);
				$item["2_86"] = $arr_day_hhmm[0];
				$wk_zan_min = hmm_to_minute(str_replace(")", "", $arr_day_hhmm[1]));
				$item["12_6"] = $timecard_common_class->minute_to_time_format($wk_zan_min , $time_format);
                //時間有休残日数　時間から日数換算
                if ($wk_zan_min > 0) {
	                $wk_zan_hol_hour = $wk_zan_min / $specified_time_per_day;
                }
                else {
	                $wk_zan_hol_hour = 0;
                }
                $item["2_60"] = $obj_hol_hour->edit_nenkyu_zan($wk_zan_hol_hour);
            }

            //残業時間（深夜残業・休日残業除外） "2_87"
            $item["2_87"] =  $timecard_common_class->minute_to_time_format(calc_fraction($zan_sums[0], $arr_config), $time_format);
            //深夜残業時間 "2_88"
            $item["2_88"] =  $timecard_common_class->minute_to_time_format(calc_fraction($zan_sums[1], $arr_config), $time_format);
            //休日残業時間 "2_89"
            $item["2_89"] =  $timecard_common_class->minute_to_time_format(calc_fraction($zan_sums[2], $arr_config), $time_format);
            //法定外残業（深夜・休日残業除外） "2_70"
            $item["2_70"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[0], $arr_config), $time_format);
            //法定外深夜残業（休日除外） "2_71"
            $item["2_71"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[1], $arr_config), $time_format);
            //法定外休日残業（深夜除外） "2_72"
            $item["2_72"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[2], $arr_config), $time_format);
            //法定外休日深夜残業 "2_73"
            $item["2_73"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[3], $arr_config), $time_format);
            //休日勤務手当時間 "2_45" 2014/8/15追加
            $item["2_45"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[4], $arr_config), $time_format);
            //休日勤務手当時間（深夜） "2_46" 2014/8/15追加
            $item["2_46"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[5], $arr_config), $time_format);
            // 法定外休日残業（深夜・休日手当除外）2_42 ＝ 法定外休日残業（深夜除外） - 休日勤務手当時間 2014/8/15追加
            $item["2_42"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[2] - $hoteigai_sums[4], $arr_config), $time_format);
            // 法定外休日深夜残業（休日手当除外）2_43 ＝ 法定外休日深夜残業 - 休日勤務手当時間（深夜） 2014/8/15追加
            $item["2_43"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[3] - $hoteigai_sums[5], $arr_config), $time_format);

            //支給時間 "2_90"
            $paid_time_total = 0; // 2014/8/15 ifの外側に定義するように修正
            if (in_array("2_90", $arr_csv_list) || in_array("2_44", $arr_csv_list)) { // 2014/8/15 2_44の場合も。
                // 支給時間＝端数処理前稼働時間＋有給取得日数×所定労働時間＋（半日有休分の時間）20121002
                $paid_time_total = $wk_kadou_time_org + $paid_sums[4];
                //時間有休追加 20111207
                if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                    $paid_time_total += $paid_sums[3];
                }
                //端数処理 20121002
                $paid_time_total = round_time($paid_time_total, $timecard_bean->fraction4, $timecard_bean->fraction4_min);
                $item["2_90"] =  $timecard_common_class->minute_to_time_format($paid_time_total, $time_format);
            }
            // （支給-基準）時間 2_44 ＝ 支給時間 − 基準時間 − 法定外残業時間 − 休日勤務手当時間 // 2014/8/15追加 20150624変更
            $item["2_44"] =  $timecard_common_class->minute_to_time_format(calc_fraction($paid_time_total - $sums_raw_values[14] - $hotei_sums[2] - $hoteigai_sums[4], $arr_config), $time_format); // 2014/8/15追加

            //所定時間 "2_80" 勤務時間 - 法定外残業時間 - 当直回数×設定時間 20101013
            $item["2_80"] =  $timecard_common_class->minute_to_time_format($wk_shotei, $time_format);

            //所定時間 "12_12" 勤務時間 - 法定外残業時間 - 法定内残業時間 - 当直回数×設定時間 20150831
            $item["12_12"] =  $timecard_common_class->minute_to_time_format($wk_shotei2, $time_format);

            //遅刻時間等追加 20101012
            $item["2_81"] = $sums[12]; //遅刻時間
            $item["2_82"] = $sums[13]; //早退時間
            $item["2_83"] = $tikoku_sotai_time; //遅刻早退時間（計）
            $item["12_0"] = $timecard_common_class->minute_to_time_format($late_early_total, $time_format); //遅刻早退控除 20131209
            //滞留時間 "2_59" 20111229
            $item["2_59"] =  $timecard_common_class->minute_to_time_format($retention_time, $time_format);

            //深夜残業（休日除外）=深夜-休日深夜
            $wk_late_except_hol = $zan_sums[1] - $zan_sums[3];
            $item["2_84"] =  $timecard_common_class->minute_to_time_format(calc_fraction($wk_late_except_hol, $arr_config), $time_format);
            //休日深夜残業時間 "2_85"
            $item["2_85"] =  $timecard_common_class->minute_to_time_format(calc_fraction($zan_sums[3], $arr_config), $time_format);

            //複数月ある場合、別に集計した分を追加
            if (!$one_month_flg) {
                $over_fix_time[0] += $over_fix_time_total[0];
                $over_fix_time[1] += $over_fix_time_total[1];
                //休日対応
                $over_fix_time[2] += $over_fix_time_total[2];
                $over_fix_time[3] += $over_fix_time_total[3];

                $over_fix_time2[0] += $over_fix_time_total2[0];
                $over_fix_time2[1] += $over_fix_time_total2[1];
                $over_fix_time2[2] += $over_fix_time_total2[2];
                $over_fix_time2[3] += $over_fix_time_total2[3];
            }

            //45時間超法定外残業 "10_45"
            $item["10_45"] =  $timecard_common_class->minute_to_time_format(calc_fraction($over_fix_time2[0] + $over_fix_time2[2], $arr_config), $time_format);
            //45時間超深夜残業 "10_46"
            $item["10_46"] =  $timecard_common_class->minute_to_time_format(calc_fraction($over_fix_time2[1] + $over_fix_time2[3], $arr_config), $time_format);
            //45時間超残業 "10_47" 60時間超法定外残業+60時間超深夜残業
            $item["10_47"] =  $timecard_common_class->minute_to_time_format(calc_fraction($over_fix_time2[0] + $over_fix_time2[1] + $over_fix_time2[2] + $over_fix_time2[3], $arr_config), $time_format);

            //60時間超法定外残業 "10_60" 20130405 休日対応 20141028
            $item["10_60"] =  $timecard_common_class->minute_to_time_format(calc_fraction($over_fix_time[0] + $over_fix_time[2], $arr_config), $time_format);

            //法定外残業（深夜・休日・６０時間超除外）"2_40" // 2014/8/15追加
            $item["2_40"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[0] - $over_fix_time[0], $arr_config), $time_format);

            //法定外残業（休日勤務手当除外）"12_7" // 20150624
            $item["12_7"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hotei_sums[2] - $hoteigai_sums[4], $arr_config), $time_format);

            //60時間超深夜残業 "10_61" 20130405 休日対応 20141028
            $item["10_61"] =  $timecard_common_class->minute_to_time_format(calc_fraction($over_fix_time[1] + $over_fix_time[3], $arr_config), $time_format);
            //60時間超残業 "10_62" 60時間超法定外残業+60時間超深夜残業
            $item["10_62"] =  $timecard_common_class->minute_to_time_format(calc_fraction($over_fix_time[0] + $over_fix_time[1] + $over_fix_time[2] + $over_fix_time[3], $arr_config), $time_format);

            //法定外深夜残業（休日・６０時間超除外）"2_41" // 2014/8/15追加
            $item["2_41"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[1] - $over_fix_time[1], $arr_config), $time_format);
            //平日残業 (法定外残業（深夜・休日残業除外）+法定外深夜残業（休日除外）)から60超除外 20141021
            $item["12_2"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[0] + $hoteigai_sums[1] - $over_fix_time[0] - $over_fix_time[1], $arr_config), $time_format);
            //休日残業 (法定外休日残業（深夜除外）+法定外休日深夜残業)から60超除外 20141021
            $item["12_3"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[2] + $hoteigai_sums[3] - $over_fix_time[2] - $over_fix_time[3], $arr_config), $time_format);
            //深夜計 (法定外深夜残業（休日除外）+法定外休日深夜残業)から60超除外 20141021
            $item["12_4"] =  $timecard_common_class->minute_to_time_format(calc_fraction($hoteigai_sums[1] + $hoteigai_sums[3] - $over_fix_time[1] - $over_fix_time[3], $arr_config), $time_format);

            //週40時間超残業 "11_40" 20130425
            $item["11_40"] =  $timecard_common_class->minute_to_time_format(calc_fraction($week_forty_overtime_total, $arr_config), $time_format);

            //休日出勤時間数 2_76
            $item["2_76"] =  $timecard_common_class->minute_to_time_format($hol_work, $time_format);

            //交通費回数 2_77
            $item["2_77"] = $tra_cnt;

            //非常勤18:00まで 2_78
            $work_time1 = round_time($work_time1, $fraction4, $fraction4_min);
            $item["2_78"] =  $timecard_common_class->minute_to_time_format($work_time1, $time_format);

            //非常勤18:00まで 2_79
            $work_time2 = round_time($work_time2, $fraction4, $fraction4_min);
            $item["2_79"] =  $timecard_common_class->minute_to_time_format($work_time2, $time_format);
            //呼出回数 20121109
            $item["2_58"] = $return_count_total;
            //呼出時間 20121109
            $item["2_57"] =  $timecard_common_class->minute_to_time_format($return_time_total, $time_format);

            //当直（平日）回数〜当直（休日）回数 "3_?"
            for ($i=0; $i<3; $i++) {
                $id = "3_".$i;
                $j = $i + 18;
                $item[$id] = $sums[$j];
            }
            // 手当 "4_?"
            $allow_cnt = 20;
            foreach($arr_allow_summary as $allow_summary)
            {
                $id = "4_".$allow_summary["allow_id"];
                $allow_cnt++;
                $item[$id] = $sums[$allow_cnt];
            }
            //手当金額（当直） "5_0"
            $allow_night[0] = $sums[18] * $night_allowance;
            $allow_night[1] = $sums[19] * $night_allowance_sat;
            $allow_night[2] = $sums[20] * $night_allowance_hol;
            $item["5_0"] = $allow_night[0] + $allow_night[1] + $allow_night[2];
            //手当金額単価
            $arr_allow_price = array();
            for ($i=0; $i<count($arr_items); $i++) {
                $tmp_id = $arr_items[$i]["id"];
                if (substr($tmp_id, 0, 1) == "6" || substr($tmp_id, 0, 1) == "7") {
                    $tmp_allow_price = $arr_items[$i]["allow_price"];
                    $arr_allow_price[$tmp_id] = $tmp_allow_price;
                }
                if (substr($tmp_id, 0, 1) == "8") {
                    break;
                }
            }
            // 手当金額＝回数×金額 "6_?"
            if ($allow_flg) {
                foreach ($arr_allow_summary as $allow_summary) {
                    $tmp_id = "6_".$allow_summary["allow_id"];
                    $tmp_allow_cnt = $allow_summary["allow_cnt"];
                    $item[$tmp_id] = $tmp_allow_cnt * $arr_allow_price[$tmp_id];
                }
            }
            //勤務パターン別回数
            if ($ptn_cnt_flg || in_array("2_74", $arr_csv_list)) {
                $arr_shift_cnt = get_arr_shift_cnt($con, $fname, $atdbk_closed, $tmp_emp_id, $start_date, $end_date);
            }

            //集計項目１ "2_74" 20101102
            $wk_total1_cnt = 0;
            for ($i=0; $i<count($arr_csv_total_item); $i++) {
                $arr_wk_id = explode("_", $arr_csv_total_item[$i]);
                $wk_group_id = $arr_wk_id[0];
                $wk_atdptn_id = $arr_wk_id[1];
                $cnt = intval($arr_shift_cnt[$wk_group_id ][$wk_atdptn_id]);
                $wk_total1_cnt += $cnt;
            }
            $item["2_74"] = $wk_total1_cnt;

            //集計項目２ "2_69"〜"2_63" 20110222
            $ptn_cnt_flg2 = false;
            for ($i=2; $i<=8; $i++) {
                $wk_code = "2_".(69+2-$i);
                if (in_array($wk_code, $arr_csv_list)) {
                    $ptn_cnt_flg2 = true;
                    break;
                }
            }
            if ($ptn_cnt_flg2) {
                $arr_shift_ptn = get_arr_shift_ptn($con, $fname, $tmp_emp_id, $start_date, $end_date);
                //集計項目の計算"2_69"〜"2_63"
                for ($i=2; $i<=8; $i++) {
                    $wk_code = "2_".(69+2-$i);
                    if (in_array($wk_code, $arr_csv_list)) {
                        $wk_total2_cnt = count_total_item($arr_shift_ptn, $arr_csv_total_item2, $arr_total_setting, $i, $start_date, $end_date);
                        $item["$wk_code"] = $wk_total2_cnt;
                    }
                }
            }
            //交通費 "2_62"
            if (in_array("2_62", $arr_csv_list)) {
                $wk_tra_cnt = 0;
                $arr_ptn_id_list = get_csv_total_tra($con, $fname, $csv_layout_id, $from_flg);
                for ($i=0; $i<count($arr_ptn_id_list); $i++) {
                    // 交通費回数 "0_0"(パターンIDとの区別用ID) csv用IDの2_77の交通費回数
                    if ($arr_ptn_id_list[$i]["id"] == "0_0") {
                        $wk_cnt = $tra_cnt; //
                    } else {
                        $arr_wk_id = explode("_", $arr_ptn_id_list[$i]["id"]);
                        $wk_group_id = $arr_wk_id[0];
                        $wk_atdptn_id = $arr_wk_id[1];
                        $wk_cnt = intval($arr_shift_cnt[$wk_group_id ][$wk_atdptn_id]);
                    }
                    $coefficient = $arr_ptn_id_list[$i]["coefficient"];
                    $wk_tra_cnt += ($wk_cnt * $coefficient);
                }

                //回数×交通費単価
                $unit_price5 = ($unit_price5 == "") ? 0 : $unit_price5;
                $item["2_62"] = $wk_tra_cnt * $unit_price5;
            }
            //交通費単価 "12_8"
            $item["12_8"] = $unit_price5;
            //自転車等の交通費1日単価 "12_9"
            $item["12_9"] = $unit_price6;
            //定期代 "12_10"
            $item["12_10"] = $unit_price7;
            //片道通勤距離 "12_11"
            $item["12_11"] = $commuting_distance;
            //交通用具 "12_13"
            $item["12_13"] = $commuting_type;

            //欠勤時間数 "12_1"
            if (in_array("12_1", $arr_csv_list)) {
            	$item["12_1"] = get_absent_time($con, $fname, $tmp_emp_id, $start_date, $end_date, $day1_time, $timecard_common_class, $arr_atdbkrslt);
			}
			//ゼロデータを出力しない設定対応 20150305
			$zero_flg = true;

            $arr_csv_dat = array();

            for ($i=0; $i<count($arr_csv_list); $i++) {
                $wk_kind = substr($arr_csv_list[$i], 0, 1);
                switch ($wk_kind) {
                    case "7": //勤務パターン手当金額
                        $arr_wk_id = explode("_", $arr_csv_list[$i]);
                        $wk_group_id = $arr_wk_id[1];
                        $wk_atdptn_id = $arr_wk_id[2];
                        $cnt = intval($arr_shift_cnt[$wk_group_id ][$wk_atdptn_id]);
                        $tanka = intval($arr_allow_price[$arr_csv_list[$i]]);
                        $arr_csv_dat[] = $cnt * $tanka;
                        if ($cnt * $tanka > 0) {
                        	$zero_flg = false;
                        }
                        break;
                    case "8": //勤務パターン回数
                        $arr_wk_id = explode("_", $arr_csv_list[$i]);
                        $wk_group_id = $arr_wk_id[1];
                        $wk_atdptn_id = $arr_wk_id[2];
                        $cnt = intval($arr_shift_cnt[$wk_group_id ][$wk_atdptn_id]);
                        $arr_csv_dat[] = $cnt;
                        if ($cnt > 0) {
                        	$zero_flg = false;
                        }
                        break;
                    case "9": //空セル
                        $arr_csv_dat[] = "";
                        break;
                    default: //上記以外
                        $arr_csv_dat[] = $item[$arr_csv_list[$i]];
                        //属性(1_*),要勤務日数(2_0),基準時間(2_12)を除く
                        if ($wk_kind != "1" && $arr_csv_list[$i] != "2_0" && $arr_csv_list[$i] != "2_12") {
	                        //"","0:00",0以外
	                        $wk_val = $item[$arr_csv_list[$i]];
	                        if ($wk_val != "" && $wk_val != "0:00" && $wk_val != 0) {
	                        	$zero_flg = false;
	                        }
                        }
                }
            }
            list($dbg_ed_msec, $dbg_ed_sec) = explode(" ", microtime());

			//ゼロデータを出力しない設定対応 20150305
			if ($arr_config["zerodata_no_out_flg"] == "t" &&
				$zero_flg) {
				continue;
			}
            if ($encode_flg == "1") {
                $buf .= join(",", $arr_csv_dat) . "\r\n";
            }
            else {
                $arr_buf[] = $arr_csv_dat;
            }
        }

    } // end of foreach

    //勤務シフト作成の管理帳票以外 csv_disp_flg
    if ($encode_flg == "1") {
        return mb_convert_encoding($buf, "SJIS", "EUC-JP");
    }
    else {
        return $arr_buf;
    }
}

function get_allow($con, $fname)
{
    $sql   = "select allow_id, allow_contents from tmcdallow ";
    $cond  = "where not del_flg order by allow_id asc ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $arr = array();
    while ($row = pg_fetch_array($sel))
    {
        $arr[] = array( "allow_id" => $row["allow_id"], "allow_contents" => $row["allow_contents"]);
    }

    return $arr;

}

// シフト集計を配列で取得（管理画面CSV用）$arr_shift_cnt[$group_id][$atdptn_id]=$cnt
function get_arr_shift_cnt($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date)
{
    $arr_shift_cnt  = array();
    // 締め済みではない場合
    $table_name = "atdbkrslt";
    if($atdbk_closed){
        // 締め済みの場合
        $table_name = "wktotald";
    }

    $today = date("Ymd");
    //////////////////////////////////////////
    // 勤務シフト集計
    //////////////////////////////////////////
    $sql   ="SELECT ".
        "c.group_id, ".
        "c.group_name, ".
        "b.atdptn_id, ".
        "b.atdptn_nm, ".
        "count(a.pattern) as pattern_cnt ".
        "FROM ".
        "$table_name a, ".
        "atdptn b, ".
        "wktmgrp c ".
        "WHERE ".
        "a.tmcd_group_id = b.group_id AND ".
        "a.pattern = to_char(b.atdptn_id,'FM999999999999') AND ".
        "a.emp_id = '$emp_id' AND ".
        "a.date >= '$start_date' AND ".
        "a.date <= '$end_date' AND ".
        "b.atdptn_nm <> '' AND ".
        "a.tmcd_group_id = c.group_id ".
        /* パターンがあればカウントへ変更 20110203
        "AND ( ". //打刻済みをカウントする 20090602
        "((b.atdptn_id = 10 OR ".
        "b.workday_count = 0) AND ".
        "a.date <= '$today') OR ".
        "(b.atdptn_id != 10 AND ".
        "a.start_time is not null ".
        "and a.end_time is not null) ) ".
        */
        "GROUP BY ".
        "c.group_id, ".
        "c.group_name, ".
        "b.atdptn_id, ".
        "b.atdptn_nm ".
        "ORDER BY ".
        "c.group_id, ".
        "b.atdptn_id ";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel))
    {
        $work_group_id = $row["group_id"];
        $work_atdptn_id = $row["atdptn_id"];
        $arr_shift_cnt[$work_group_id][$work_atdptn_id] = $row["pattern_cnt"];
    }

    return $arr_shift_cnt;
}

/**
 * レイアウトIDの条件にあう職員取得
 *   レイアウト調整画面からの場合は、指定職員をソートする
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $srch_name 職員名
 * @param mixed $cls 施設
 * @param mixed $atrb 部
 * @param mixed $dept 科
 * @param mixed $duty_form_jokin 常勤
 * @param mixed $duty_form_hijokin 非常勤
 * @param mixed $group_id 出勤グループ
 * @param mixed $shift_group_id シフトグループ
 * @param mixed $csv_layout_id 月集計CSVレイアウト
 * @param mixed $arr_config 月集計CSVの設定
 * @param mixed $emp_id_list 指定職員ID（設定時：レイアウト調整画面から、未設定時：検索画面のダウンロードボタンから）
 * @param mixed $srch_id 職員ID
 * @param mixed $sus_flg 利用停止者も含める "checked":含める  "":含めない
 * @return mixed 職員ID
 *
 */
function get_arr_emp_id_from_layout_id($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $arr_config, $emp_id_list, $srch_id, $sus_flg) {

    $sql = "select empmst.emp_id, emp_personal_id, emp_lt_nm, emp_ft_nm from empmst ";
    $sql .= "left join classmst on classmst.class_id = empmst.emp_class ";
    $sql .= "left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute ";
    $sql .= "left join deptmst on deptmst.dept_id = empmst.emp_dept ";
    $sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";

	//利用停止者も含める
    if ($sus_flg != "") {
		$cond = "where true ";
    }
	else {
	    $cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') ";
    }
    //検索条件　職員ID
    if ($srch_id != "") {
        $tmp_srch_id = str_replace(" ", "", $srch_id);
        $cond .= " and (empmst.emp_personal_id like '%$tmp_srch_id%')";
    }
    //検索条件　職員名
    if ($srch_name != "") {
        $tmp_srch_name = str_replace(" ", "", $srch_name);
        $cond .= " and (emp_lt_nm like '%$tmp_srch_name%' or emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm like '%$tmp_srch_name%' or emp_kn_ft_nm like '%$tmp_srch_name%' or emp_lt_nm || emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$tmp_srch_name%')";
    }
    //検索条件　属性：事務所
    if ($cls != "" && substr($cls, 0, 1) != "-") {
        $cond .= " and ((empmst.emp_class = $cls) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $cls))";
    }
    //検索条件　属性：課
    if ($atrb != "" && substr($atrb, 0, 1) != "-") {
        $cond .= " and ((empmst.emp_attribute = $atrb) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = $atrb))";
    }
    //検索条件　属性：科
    if ($dept != "" && substr($dept, 0, 1) != "-") {
        $cond .= " and ((empmst.emp_dept = $dept) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = $dept))";
    }
    //検索条件　雇用・勤務形態
    if ($duty_form_jokin != $duty_form_hijokin) {
        if ($duty_form_hijokin == "checked") {
            //非常勤
            $cond .= " and ";
        }
        else {
            //常勤
            $cond .= " and NOT ";
        }
        $cond .= "EXISTS(SELECT * FROM empcond WHERE empcond.emp_id = empmst.emp_id AND duty_form = 2)";
    }
    //検索条件　出勤グループ
    if ($group_id != "" && $group_id != "-") {
        $cond .= " and exists (select * from empcond where empcond.emp_id = empmst.emp_id and empcond.tmcd_group_id = $group_id)";
    }

    //検索条件　シフトグループ
    if ($shift_group_id != "" && $shift_group_id != "-") {
        $cond .= " and exists (select * from duty_shift_staff where duty_shift_staff.emp_id = empmst.emp_id and duty_shift_staff.group_id = '$shift_group_id')"; //職員設定の情報から
        //月毎の情報の場合は変更$cond .= " and exists (select * from duty_shift_plan_staff where duty_shift_plan_staff.emp_id = empmst.emp_id and duty_shift_plan_staff.group_id = '$shift_group_id' and duty_shift_plan_staff.duty_yyyy = $wk_year and duty_shift_plan_staff.duty_mm = $wk_month)";
    }

    //検索条件　月集計CSVレイアウト
    if ($csv_layout_id != "" && $csv_layout_id != "-") {
        $cond .= " and exists (select * from empcond where empcond.emp_id = empmst.emp_id and (empcond.csv_layout_id = '$csv_layout_id'";
        if ($csv_layout_id == "01") {
            $cond .= " or empcond.csv_layout_id is null";
        }
        $cond .= "))";
    }
    //職員ID
    if ($emp_id_list != "") {
        $emp_ids = explode(",", $emp_id_list);
        $emp_id_str = "";
        for ($i=0; $i<count($emp_ids); $i++) {
            if ($i > 0) {
                $emp_id_str .= ",";
            }
            $emp_id_str .= "'".$emp_ids[$i]."'";
        }
        $cond .= "and empmst.emp_id in ($emp_id_str) ";
    }
	//ユーザ画面からの場合、所属権限を確認
	global $mmode, $timecard_tantou_branches;
	if ($mmode == "usr") {
        $cond .= " and (1 <> 1";
        foreach ($timecard_tantou_branches as $r) {
            if ($r["class_id"]) $cond .= " or (";
            if ($r["class_id"]) $cond .= "     empmst.emp_class=".$r["class_id"];
            if ($r["atrb_id"])  $cond .= "     and empmst.emp_attribute=".$r["atrb_id"];
            if ($r["dept_id"])  $cond .= "     and empmst.emp_dept=".$r["dept_id"];
            if ($r["class_id"]) $cond .= " )";
        }
        //兼務
        $cond_ccr = " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and (1 <> 1";
        foreach ($timecard_tantou_branches as $r) {
            if ($r["class_id"]) $cond_ccr .= " or (";
            if ($r["class_id"]) $cond_ccr .= "     concurrent.emp_class=".$r["class_id"];
            if ($r["atrb_id"])  $cond_ccr .= "     and concurrent.emp_attribute=".$r["atrb_id"];
            if ($r["dept_id"])  $cond_ccr .= "     and concurrent.emp_dept=".$r["dept_id"];
            if ($r["class_id"]) $cond_ccr .= " )";
        }
        $cond_ccr .= "))";
        $cond .= $cond_ccr;
        $cond .= ")";
	}

    //ソートキー指定数
    $sort_num = 0;
    for ($i=1; $i<=5; $i++) {
        $varname = "sortkey".$i;
        if ($arr_config[$varname] > 0) {
            $sort_num++;
        }
    }
    if ($sort_num == 0) {
        $cond .= " order by emp_personal_id";
    } else {
        //出力順項目
        $arr_sortitem = array("", "emp_personal_id", "emp_kn_lt_nm, emp_kn_ft_nm", "classmst.link_key, empmst.emp_class", "atrbmst.link_key, empmst.emp_attribute", "deptmst.link_key1, empmst.emp_dept", "jobmst.link_key, jobmst.order_no");
        $cond .= " order by ";
        $first_flg = true;
        for ($i=1; $i<=5; $i++) {
            $varname = "sortkey".$i;
            $idx = $arr_config[$varname];
            $wk_sortitem = $arr_sortitem[$idx];
            if ($idx > 0) {
                if ($first_flg) {
                    $first_flg = false;
                } else {
                    $cond .= ", ";
                }
                $cond .= $wk_sortitem;
            }
        }
    }

    $sel_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_emp == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $arr_emp_id = array();
    while ($row = pg_fetch_array($sel_emp)) {
        $arr_emp_id[] = $row["emp_id"];
    }
    return $arr_emp_id;
}

/**
 * 管理帳票からの条件にあう職員取得
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $shift_group_id シフトグループ
 * @param string $arr_config 月集計CSVの設定
 * @param string $page 1始まり
 * @param string $limit 0より大きい:1頁あたり件数 0:全て -1:件数取得のため指定しない
 * @param string $start_date 開始日
 * @param string $end_date 終了日
 * @param string $total_flg 集計単位 "",1:職員別、 2:グループ別職員別
 * @return mixed 職員ID
 *
 */
function get_arr_emp_id_for_mprint($con, $fname, $shift_group_id, $arr_config, $page, $limit, $start_date, $end_date, $total_flg=1) {

    $arr_group_info = get_group_info($con, $fname, $shift_group_id);

    $start_month_flg1 = $arr_group_info[0]["start_month_flg1"];
    $start_day1 = $arr_group_info[0]["start_day1"];
    $month_flg1 = $arr_group_info[0]["month_flg1"];
    $end_day1 = $arr_group_info[0]["end_day1"];

    $next_date = next_date($end_date);
    $wk_histdate = substr($next_date, 0, 4)."-".substr($next_date, 4, 2)."-".substr($next_date, 6, 2)." 00:00:00";

    $arr_ym = array();
    if ($start_month_flg1 == "1" && $start_day1 == "1") {
        $start_ym = substr($start_date, 0 ,6);
        $end_ym = substr($end_date, 0 ,6);

    }
    else {
        //開始終了の年月
        $start_ym = get_target_month($start_date, $start_month_flg1, $end_day1);
        $end_ym = get_target_month($end_date, $start_month_flg1, $end_day1);
    }

    //管理帳票で件数を取得
    if ($limit == -1) {
        $sql = "select count(*) as cnt from ";
    }
    //職員IDを取得
    else {
        $sql = "select empmst.group_id, empmst.emp_id, emp_personal_id, emp_lt_nm, emp_ft_nm from ";
    }
    //複数月、グループにあるような場合、職員1レコードになるようにまとめる
    $sql .= " (select a.*, b.emp_personal_id, b.emp_lt_nm, b.emp_ft_nm, b.emp_class, b.emp_attribute, b.emp_dept, case when c.job_id is null then b.emp_job else c.job_id end as emp_job, b.emp_kn_lt_nm, b.emp_kn_ft_nm from (   ";
    if ($total_flg == 1) {
        $sql .= " select  distinct on (emp_id) ";
    }
    else {
        $sql .= " select  distinct on (duty_shift_plan_staff.group_id, emp_id) ";
    }
    $sql .= " duty_shift_group.group_id, emp_id, no, order_no from duty_shift_plan_staff left join duty_shift_group on duty_shift_group.group_id = duty_shift_plan_staff.group_id ";
    $sql .= " where ";
    if ($shift_group_id != "-") {
        $sql .= " duty_shift_plan_staff.group_id = '$shift_group_id' and ";
    }
    $sql .=" cast(duty_shift_plan_staff.duty_yyyy as varchar)||lpad(cast(duty_shift_plan_staff.duty_mm as varchar), 2, '0') >= '$start_ym' and cast(duty_shift_plan_staff.duty_yyyy as varchar)||lpad(cast(duty_shift_plan_staff.duty_mm as varchar), 2, '0') <= '$end_ym'   ";
    if ($total_flg == 1) {
        $sql .= " order by emp_id, duty_yyyy, duty_mm, order_no, no ";
    }
    else {
        $sql .= " order by duty_shift_plan_staff.group_id, emp_id, duty_yyyy, duty_mm ";
    }
    $sql .= " ) a ";
    $sql .= " left join empmst b on b.emp_id = a.emp_id ";
    $sql .= " left join (select distinct on (emp_id) * from job_history where histdate >= '$wk_histdate' order by emp_id, histdate) c on c.emp_id = a.emp_id ";
    $sql .= " order by a.group_id, a.no) ";

    $sql .= " empmst ";
    $sql .= "left join classmst on classmst.class_id = empmst.emp_class ";
    $sql .= "left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute ";
    $sql .= "left join deptmst on deptmst.dept_id = empmst.emp_dept ";
    $sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";
    //ソートキーにシフトグループがある場合
    $is_sort_shift_group = false;
    for ($i=1; $i<=5; $i++) {
        $varname = "sortkey".$i;
        $idx = $arr_config[$varname];
        if ($idx == 7) {
            $is_sort_shift_group = true;
            break;
        }
    }
    //ソート順にシフトグループがある場合
    if ($is_sort_shift_group) {
        $sql .= "left join duty_shift_group on duty_shift_group.group_id = empmst.group_id ";
    }

    $cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') ";

    //シフトグループ指定時、ソートキーのシフトグループを除外、応援がある場合順番が変わるため
    if ($shift_group_id != "-" && $is_sort_shift_group) {
        for ($i=1; $i<=5; $i++) {
            $varname = "sortkey".$i;
            if ($arr_config[$varname] == 7) {
                $arr_config[$varname] = 0;
            }
        }
    }

    //ソートキー指定数
    $sort_num = 0;
    for ($i=1; $i<=5; $i++) {
        $varname = "sortkey".$i;
        if ($arr_config[$varname] > 0) {
            $sort_num++;
        }
    }
    //職員IDを取得
    if ($limit >= 0) {
        if ($sort_num == 0) {
            $cond .= " order by emp_personal_id";
        } else {
            //出力順項目
            $arr_sortitem = array("", "emp_personal_id", "emp_kn_lt_nm, emp_kn_ft_nm", "classmst.link_key, empmst.emp_class", "atrbmst.link_key, empmst.emp_attribute", "deptmst.link_key1, empmst.emp_dept", "jobmst.link_key, jobmst.order_no", "duty_shift_group.order_no", "empmst.no");
            $cond .= " order by ";
            $first_flg = true;
            for ($i=1; $i<=5; $i++) {
                $varname = "sortkey".$i;
                $idx = $arr_config[$varname];
                $wk_sortitem = $arr_sortitem[$idx];
                if ($idx > 0) {
                    if ($first_flg) {
                        $first_flg = false;
                    } else {
                        $cond .= ", ";
                    }
                    $cond .= $wk_sortitem;
                }
            }
        }
    }
    if ($limit > 0) {

        $offset = ($page-1) * $limit;
        $cond .= " offset $offset limit $limit";
    }

    $sel_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_emp == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $arr_emp_id = array();

    //管理帳票で件数を取得
    if ($limit == -1) {
        $cnt =  pg_fetch_result($sel_emp, 0, "cnt");
        return $cnt;
    }
    else {
        while ($row = pg_fetch_array($sel_emp)) {
            if ($total_flg == 1) {
                $arr_emp_id[] = $row["emp_id"];
            }
            else {
                $arr_emp_id[] = $row["group_id"]."_".$row["emp_id"];
            }
        }
        return $arr_emp_id;
    }
}

// 集計項目用パターンデータ取得 $arr_shiftptn[$date]["tmcd_group_id", "atdptn_id", "day_of_week"]
function get_arr_shift_ptn($con, $fname, $emp_id, $start_date, $end_date)
{
    $arr_shift_ptn  = array();
    $table_name = "atdbkrslt";

    //////////////////////////////////////////
    // 勤務シフト
    //////////////////////////////////////////
    $sql   = "SELECT a.date, date_part('dow',to_timestamp(a.date, 'yyyymmdd')) as day_of_week, a.tmcd_group_id as group_id, a.pattern as atdptn_id, a.reason ".
        "FROM ".
        "$table_name a ";
    $cond = "WHERE ".
        "a.emp_id = '$emp_id' AND ".
        "a.date >= '$start_date' AND ".
        "a.date <= '$end_date' AND ".
        "a.pattern != '' ".
        "ORDER BY ".
        "a.date ";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel))
    {
        $work_date = $row["date"];
        $arr_shift_ptn[$work_date]["group_id"] = $row["group_id"];
        $arr_shift_ptn[$work_date]["atdptn_id"] = $row["atdptn_id"];
        $arr_shift_ptn[$work_date]["day_of_week"] = $row["day_of_week"];
        $arr_shift_ptn[$work_date]["reason"] = $row["reason"];
    }

    return $arr_shift_ptn;
}

//集計項目計算
function count_total_item($arr_shift_ptn, $arr_csv_total_item2, $arr_total_setting, $total_item_no, $start_date, $end_date) {

    $cnt = 0;

    $arr_comp_id = array();
    $arr_keisu = array();
    $item_num = count($arr_csv_total_item2[$total_item_no]);
    for ($j=0; $j<$item_num; $j++) {
        $wk_id = $arr_csv_total_item2[$total_item_no][$j]["id"];
        $arr_comp_id[] = $wk_id;
        $arr_keisu[$wk_id] = $arr_csv_total_item2[$total_item_no][$j]["coefficient"];
    }
    //計算条件
    $start_day_flg = $arr_total_setting[$total_item_no]["start_day_flg"];
    $end_day_flg = $arr_total_setting[$total_item_no]["end_day_flg"];
    $week_day_flg = $arr_total_setting[$total_item_no]["week_day_flg"];
    if ($start_day_flg == "") {
        $start_day_flg = "1";
    }
    if ($end_day_flg == "") {
        $end_day_flg = "1";
    }
    if ($week_day_flg == "") {
        $week_day_flg = "1";
    }

    $tmp_date = $start_date;
    while ($tmp_date <= $end_date) {

        if ($arr_shift_ptn[$tmp_date]["group_id"] != "") {

            //事由
            $wk_reason = $arr_shift_ptn[$tmp_date]["reason"];
            if ($wk_reason != "") {
                $wk_id = "0_".$wk_reason;
            }
            if ($wk_reason != "" && in_array($wk_id, $arr_comp_id)) {
                ;
            }
            else {
                $wk_id = $arr_shift_ptn[$tmp_date]["group_id"]."_".$arr_shift_ptn[$tmp_date]["atdptn_id"];
            }

            //計算条件
            $wk_day_of_week = $arr_shift_ptn[$tmp_date]["day_of_week"];

            $count_flg = false;

            if (
                (($start_day_flg == "1" || //開始日カウントする
                            ($start_day_flg == "2" && $tmp_date == $start_date) || //開始日のみカウントする
                            ($start_day_flg == "3" && $tmp_date != $start_date)) && //開始日はカウントしない
                        ($end_day_flg == "1" || //終了日カウントする
                            ($end_day_flg == "2" && $tmp_date == $end_date) || //終了日のみカウントする
                            ($end_day_flg == "3" && $tmp_date != $end_date)) && //終了日はカウントしない
                        ($week_day_flg == "1" || //全ての曜日
                            ($week_day_flg == "2" && $wk_day_of_week == "0"))) || //日曜のみ
                    ((
                            ($start_day_flg == "2" && $tmp_date == $start_date) || //開始日のみカウントする
                            ($end_day_flg == "2" && $tmp_date == $end_date)  //終了日のみカウントする
                            ) &&
                        ($week_day_flg == "1" || //全ての曜日
                            ($week_day_flg == "2" && $wk_day_of_week == "0"))) //日曜のみ
                ) {
                $count_flg = true;
            }

            if ($count_flg && in_array($wk_id, $arr_comp_id)) {
                $wk_keisu = $arr_keisu[$wk_id];
                $cnt += $wk_keisu;
            }
        }
        $tmp_date = next_date($tmp_date);
    }

    return $cnt;
}

// 集計項目取得 [集計項目番号][no-1(通番)]["id"(1_1:group_id, atdptn_id), "coefficient"(係数)]
function get_csv_total_item_all($con, $fname, $layout_id, $from_flg="") {

    $arr_ptn_list = array();
    //CSVレイアウト調整
    if ($from_flg != "2") {
        $tablename = "timecard_csv_total_item2";
        $wk_layout_id = "'$layout_id'";
    }
    //帳票定義
    else {
        $tablename = "duty_shift_mprint_total_item2";
        $wk_layout_id = "$layout_id";
    }
    $sql = "select * from $tablename";
    $cond = "where layout_id = $wk_layout_id order by total_item_no, no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num = pg_numrows($sel);

    for ($i=0; $i<$num; $i++) {
        $total_item_no = pg_result($sel,$i,"total_item_no");
        $no = pg_result($sel,$i,"no");
        $idx = $no - 1;
        $arr_ptn_list[$total_item_no][$idx]["id"] = pg_result($sel,$i,"id");
        $arr_ptn_list[$total_item_no][$idx]["coefficient"] = pg_result($sel,$i,"coefficient");
    }
    return $arr_ptn_list;
}


/**
 * シフトグループ情報取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $group_id グループID
 * @return mixed グループ情報の配列
 *
 */
function get_group_info($con, $fname, $group_id) {
    $sql = "select * from duty_shift_group ";
    if ($group_id != "" && $group_id != "-") {
        $cond = "where group_id = '$group_id' ";
    }
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $group_array = array();
    $num = pg_num_rows($sel);
    if ($num > 0) {
        $group_array = pg_fetch_all($sel);
    }
    for ($i=0; $i<$num; $i++) {
        if ($group_array[$i]["start_month_flg1"] == "") {$group_array[$i]["start_month_flg1"] = "1";}
        if ($group_array[$i]["start_day1"] == "" || $group_array[$i]["start_day1"] == "0") {$group_array[$i]["start_day1"] = "1";}
        if ($group_array[$i]["month_flg1"] == "" || $group_array[$i]["end_day1"] == "0") {$group_array[$i]["month_flg1"] = "1";} //終了日確認
        if ($group_array[$i]["end_day1"] == "" || $group_array[$i]["end_day1"] == "0") {  $group_array[$i]["end_day1"] = "99";}
    }
    return $group_array;
}

//シフト作成の開始月、終了月のデータを取得する
//$start_date開始日, $end_date終了日と対応する、月を配列で返す。
//グループ設定、シフト作成期間が前月からなどの場合を調整
function get_month_info($con, $fname, $shift_group_id, $start_date, $end_date) {
    $arr_group_info = get_group_info($con, $fname, $shift_group_id);
    $start_month_flg1 = $arr_group_info[0]["start_month_flg1"];
    $start_day1 = $arr_group_info[0]["start_day1"];
    $month_flg1 = $arr_group_info[0]["month_flg1"];
    $end_day1 = $arr_group_info[0]["end_day1"];

    $arr_ym = array();
    if ($start_month_flg1 == "1" && $start_day1 == "1") {
        $start_ym = substr($start_date, 0 ,6);
        $end_ym = substr($end_date, 0 ,6);

    }
    else {
        //開始終了の年月
        $start_ym = get_target_month($start_date, $start_month_flg1, $end_day1);
        $end_ym = get_target_month($end_date, $start_month_flg1, $end_day1);

    }

    return array($start_ym, $end_ym);
}

//処理フラグ
// $flg 1:休日以外、2:休日、3:全部
function get_calendar_daycnt($con, $fname, $flg, $arr_timecard_holwk_day, $start_date, $end_date) {

    $arr_hol_type = array();
    if ($arr_timecard_holwk_day[0]["legal_holiday"] == "t") {
        $arr_hol_type[] = "'4'";
    }
    if ($arr_timecard_holwk_day[0]["national_holiday"] == "t") {
        $arr_hol_type[] = "'5'";
    }
    if ($arr_timecard_holwk_day[0]["newyear_holiday"] == "t") {
        $arr_hol_type[] = "'6'";
    }
    if ($arr_timecard_holwk_day[0]["prescribed_holiday"] == "t") {
        $arr_hol_type[] = "'7'";
    }
    $sql = "select count(*) as cnt from calendar ";

    $cond = "where date >= '$start_date' and date <= '$end_date' ";
    if (count($arr_hol_type) > 0) {
        $cond_type = join(",", $arr_hol_type);
        //条件によりSQLの組み立てを変える
        //1:休日以外
        if ($flg == "1") {
            $cond .= " and type not in ($cond_type)";
        }
        //2:休日
        elseif ($flg == "2") {
            $cond .= " and type in ($cond_type)";
        }
        //3:全て（条件なし）
    }
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $cnt = pg_fetch_result($sel, 0, "cnt");
    return $cnt;
}
/**
 * 履歴情報取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $emp_id 職員情報
 * @param mixed $date 日付
 * @return mixed 職員情報
 *
 */
function get_emp_info_history($con, $fname, $emp_id, $date) {

    $wk_yyyy = substr($date, 0, 4);
    $wk_mm = substr($date, 4, 2);
    $wk_dd = substr($date, 6, 2);

    //基準日以降の履歴を取得し対象月の最新のデータを設定、未設定時は、呼出元のempmst,empcondの項目を使用
    $wk_ymd = $wk_yyyy."-".$wk_mm."-".$wk_dd." 00:00:00";
    //履歴には変更前のIDがレコードに登録されている
    $sql = "select a.emp_id, a.duty_form, a.histdate  from empcond_duty_form_history a ";
    $cond = " where a.histdate >= '$wk_ymd' "
        . " and a.emp_id = '$emp_id' "
        . " order by a.histdate limit 1 ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_info = array();
    $mode_name = "duty_form";
    //取得
    while ($row = pg_fetch_array($sel)) {

        $duty_form = $row[$mode_name];
        if ($duty_form == "1") {
            $tmp_duty_form = "常勤";
        } elseif ($duty_form == "2") {
            $tmp_duty_form = "非常勤";
        } elseif ($duty_form == "3") {
            $tmp_duty_form = "短時間正職員";
        } else {
            $tmp_duty_form = "";
        }

        $arr_info[$mode_name] = $tmp_duty_form;
    }

    //職種
    $sql = "select a.emp_id, a.job_id, c.job_nm, a.histdate from job_history a "
        . " left join jobmst c on c.job_id = a.job_id ";
    $cond = " where a.histdate >= '$wk_ymd' "
        . " and a.emp_id  = '$emp_id' "
        . " order by a.histdate limit 1 ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $mode_name = "job_nm";
    //取得
    while ($row = pg_fetch_array($sel)) {
        $arr_info[$mode_name] = $row[$mode_name];
    }

    //役職
    $sql = "select a.emp_id, a.st_id, c.st_nm, a.histdate from st_history a "
        . " left join stmst c on c.st_id = a.st_id ";
    $cond = " where a.histdate >= '$wk_ymd' "
        . " and a.emp_id = '$emp_id' "
        . " order by a.histdate limit 1";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $mode_name = "st_nm";
    //取得
    while ($row = pg_fetch_array($sel)) {
        $arr_info[$mode_name] = $row[$mode_name];
    }


    return $arr_info;
}


/**
 * 職員設定グループの履歴取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $emp_id 職員ID
 * @param mixed $start_date 日付
 * @return mixed 履歴の配列
 *
 */
function get_duty_shift_staff_history($con, $fname, $emp_id, $start_date) {
    $wk_yyyy = substr($start_date, 0, 4);
    $wk_mm = substr($start_date, 4, 2);
    $wk_dd = substr($start_date, 6, 2);
    $wk_ymd = $wk_yyyy."-".$wk_mm."-".$wk_dd." 00:00:00";
    //履歴には変更前のIDがレコードに登録されている
    $sql = "select a.* from duty_shift_staff_history a "
        ." where a.histdate >= '$wk_ymd' "
        ." and a.emp_id = '$emp_id' "
        . " order by a.histdate ";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_group_id_history = array();
    while ($row = pg_fetch_array($sel)) {
        $arr_group_id_history[] = $row;
    }
    return $arr_group_id_history;

}


/**
 * 公休調整数を取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $start_date 開始日
 * @param mixed $end_date 終了日
 * @param mixed $shift_group_id シフトグループ
 * @param mixed $tmcd_group_id 勤務パターングループ
 * @return mixed 公休調整数
 *
 */
function get_legal_hol_adjust_cnt($con, $fname, $start_date, $end_date, $shift_group_id, $tmcd_group_id){
    //年月
    list($start_ym, $end_ym) = get_month_info($con, $fname, $shift_group_id, $start_date, $end_date);
    $start_y = substr($start_ym, 0, 4);
    $end_y = substr($end_ym, 0, 4);

    $arr_legal_hol = array();
    $legal_hol_adjust = 0;
    for ($wk_y = $start_y; $wk_y <= $end_y; $wk_y++) {
        //公休調整数を取得し範囲内の場合、集計
        $arr_legal_hol = get_legal_hol_group($con, $fname, $wk_y, $tmcd_group_id);

        for ($i=1; $i<=12; $i++) {
            $wk_ym = $wk_y.sprintf("%02d", $i);
            if ($start_ym <= $wk_ym && $wk_ym <= $end_ym) {
                $key1 = "sign_".$i;
                $key2 = "mon_".$i;
                $sign = $arr_legal_hol[0][$key1];
                $adjust_day = $arr_legal_hol[0][$key2];
                //マイナスの場合
                if ($sign == "2") {
                    $adjust_day = $adjust_day * (-1);
                }

                $legal_hol_adjust += $adjust_day;
            }
        }
    }
    return $legal_hol_adjust;
}


/**
 * 締め日を考慮した月度を取得
 *
 * @param mixed $target_date 日付
 * @param mixed $start_month_flg1 開始日の月 0:前月 1:当月
 * @param mixed $end_day1 締め日
 * @return mixed 年月YYYYMM形式
 *
 */
function get_target_month($target_date, $start_month_flg1, $end_day1) {

    $target_day = substr($target_date, 6, 2);
    //当月から翌月
    if ($start_month_flg1 == "1") {
        // 指定日＞締め日、当月
        if ($target_day > $end_day1) {
            $yyyymm = substr($target_date, 0, 6);
        }
        // 指定日＜＝締め日、前月
        else {
            $yyyymm = date("Ym", strtotime("-1 month", date_utils::to_timestamp_from_ymd($target_date)));
        }
    }
    //前月から当月
    else {
        // 指定日＞締め日、翌月
        if ($target_day > $end_day1) {
            $yyyymm = date("Ym", strtotime("+1 month", date_utils::to_timestamp_from_ymd($target_date)));
        }
        // 指定日＜＝締め日、当月
        else {
            $yyyymm = substr($target_date, 0, 6);
        }
    }
    return $yyyymm;
}

function get_late_early_leave($con, $fname, $emp_id, $start_date, $end_date) {
    //
    $sql = "select a.emp_id, a.start_date, a.start_time, a.end_date, a.end_time, a.kind_flag, b.apply_stat, c.ireg_no_deduction_flg, c.reason "
        ." from kintai_late a "
        ." left join apply b on b.apply_id = a.apply_id "
        ." left join iregrsn c on c.reason_id = a.reason_id ";
    $cond = "where a.emp_id = '$emp_id' and  a.start_date >= '$start_date' and "
        ." a.start_date <= '$end_date' and (a.kind_flag = '1' or a.kind_flag = '2') "
        . " order by a.start_date ";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_late_data = array();
    while ($row = pg_fetch_array($sel)) {
        $date = $row["start_date"];
        $arr_late_data[$date][] = $row;
    }
    return $arr_late_data;

}

function get_absent_time($con, $fname, $emp_id, $start_date, $end_date, $specified_time, $timecard_common_class, $arr_atdbkrslt) {

    //休暇種別画面の種類が病欠で0.5日となる事由の件数を取得
    $sql = "SELECT count(*) as cnt from atdbkrslt a";
    $cond = "where a.emp_id = '$emp_id' and a.date >= '$start_date' and a.date <= '$end_date' and "
        ." a.reason in ( "
        ." select reason_id from atdbk_reason_mst where kind_flag = '6' and holiday_count = '0.5') ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $cnt = pg_fetch_result($sel, 0, "cnt");

    //return $specified_time;

    //所定労働時間を分単位にする
    $hour2 = (int)substr($specified_time, 0, 2);
    $min2 = $hour2 * 60 + (int)substr($specified_time, 2, 2);
    $half_time_min = intval($min2 / 2, 10);
    $wk_time = $half_time_min * $cnt;
//申請
	$wk_end_date = next_date($end_date);
    $sql = "SELECT start_date, start_time, end_date, end_time from kintai_late a";
    $cond = "where a.emp_id = '$emp_id' and a.start_date >= '$start_date' and a.start_date <= '$wk_end_date' and "
        ." a.apply_stat = '1' and a.transfer = 2 "; //
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //欠勤振替データ取得
    $wk_furikae = 0;
    while ($row = pg_fetch_array($sel)) {
        $tmp_start_date = $row["start_date"];
        $start_time = $row["start_time"];
        $tmp_end_date = $row["end_date"];
        $end_time = $row["end_time"];
		//休憩時間を除く
    	$tmp_rest_date = $tmp_start_date;
		$tmp_next_date = next_date($tmp_start_date);
    	if ($arr_atdbkrslt[$tmp_next_date]["atdptn_previous_day_flag"] == "1") {
    		$tmp_rest_date = $tmp_next_date;
    	}
    	elseif ($arr_atdbkrslt[$tmp_start_date]["pattern"] == "10" ||
    		$arr_atdbkrslt[$tmp_start_date]["after_night_duty_flag"] == "1") {
    		$tmp_rest_date = last_date($tmp_start_date);
    	}
        //$wk_min = date_utils::get_diff_minute($end_date.$end_time, $start_date.$start_time);
		if ($tmp_rest_date >= $start_date) { //前月からの日またがりを除く
			$wk_min = $timecard_common_class->get_diff_times(
				$tmp_start_date.$start_time,
				$tmp_end_date.$end_time,
				$arr_atdbkrslt[$tmp_rest_date]["start4_date_time"],
				$arr_atdbkrslt[$tmp_rest_date]["end4_date_time"]
				);
	        $wk_furikae += $wk_min;
        }
    }
    $wk_time += $wk_furikae;

    //$wk_time = ($wk_time == 0) ? "0:00" : minute_to_hmm($wk_time);
    //3:"hh.mm"形式
    $wk_time = $timecard_common_class->minute_to_time_format($wk_time, "3");
    return $wk_time;
}

//端数処理、３０分未満は、切り捨て、３０分以上は切り上げ
function calc_fraction($times, $arr_config) {
	if ($arr_config["fraction_flg"] != "t") {
		return $times;
	}
	$hours = intval($times / 60);
	$minutes = $times % 60;

	if ($minutes >= 30) {
		$hours++;
	}
	return $hours * 60;
}

?>