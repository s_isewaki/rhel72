<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("webmail_alias_functions.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
    $sql = "select * from classroom";
    $cond = "where room_del_flg = 'f' order by order_no";
    $sel_room = select_from_table($con, $sql, $cond, $fname);
    if ($sel_room == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
} else {
    $sel_room = null;
}

// 所属変更履歴を取得
if (!empty($_REQUEST["class_history_id"])) {
    $class_history_id = pg_escape_string($_REQUEST["class_history_id"]);
    $sql = "SELECT * FROM class_history WHERE class_history_id=$class_history_id";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $hist = pg_fetch_assoc($sel);
}
else {
    $hist = array("histdate" => date('Y-m-d H:i:s'));
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 所属履歴</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    change_atrb('<?php eh($hist["atrb_id"]);?>', '<?php eh($hist["dept_id"]);?>', '<?php eh($hist["room_id"]);?>');

    $('#class').change(function() {
        change_atrb();
    });
    $('#atrb').change(function() {
        change_dept();
    });
    $('#dept').change(function() {
        change_room();
    });
});

// 部署2変更
function change_atrb(atrb_id, dept_id, room_id) {
    $('#atrb').children().remove();

    var class_id = $('#class option:selected').val();
<? while ($row = pg_fetch_assoc($sel_atrb)) { ?>
    if (class_id == '<?php eh($row["class_id"]);?>') {
        $('#atrb').append('<option value="<?php eh($row["atrb_id"]);?>"><?php eh($row["atrb_nm"]);?></option>');
    }
<? } ?>

    if ($('#atrb').children().length == 0) {
        $('#atrb').append('<option value="0">（未登録）</option>');
    }
    $('#atrb').width(); // IE6でエラーになる対策
    if (atrb_id) {
        $('#atrb').val(atrb_id);
    }
    change_dept(dept_id, room_id);
}

// 部署3変更
function change_dept(dept_id, room_id) {
    $('#dept').children().remove();

    var atrb_id = $('#atrb option:selected').val();
<? while ($row = pg_fetch_assoc($sel_dept)) { ?>
    if (atrb_id == '<?php eh($row["atrb_id"]);?>') {
        $('#dept').append('<option value="<?php eh($row["dept_id"]);?>"><?php eh($row["dept_nm"]);?></option>');
    }
<? } ?>

    if ($('#dept').children().length == 0) {
        $('#dept').append('<option value="0">（未登録）</option>');
    }
    $('#dept').width(); // IE6でエラーになる対策
    if (dept_id) {
        $('#dept').val(dept_id);
    }
    change_room(room_id);
}

// 部署4変更
function change_room(room_id) {
    if (!$('#room')) {
        return;
    }

    $('#room').children().remove();

    var dept_id = $('#dept option:selected').val();
<? while ($row = pg_fetch_assoc($sel_room)) { ?>
    if (dept_id == '<?php eh($row["dept_id"]);?>') {
        if ($('#room').children().length == 0) {
            $('#room').append('<option value="0">　　　　　</option>');
        }
        $('#room').append('<option value="<?php eh($row["room_id"]);?>"><?php eh($row["room_nm"]);?></option>');
    }
<? } ?>

    if ($('#room').children().length == 0) {
        $('#room').append('<option value="0">（未登録）</option>');
    }
    $('#room').width(); // IE6でエラーになる対策
    if (room_id) {
        $('#room').val(room_id);
    }
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>&key1=<? echo(urlencode($key1)); ?>&key2=<? echo(urlencode($key2)); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_detail.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_class_history_form.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_history_id=<? echo($class_history_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>所属履歴</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="employee_contact_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_condition_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_auth_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_display_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="employee_alias_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<br />
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■所属変更履歴の追加/変更</font><br />
<br />
<form action="employee_class_history_submit.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?></font></td>
    <td>
    <select id="class" name="class_id">
<?php
while ($row = pg_fetch_assoc($sel_class)) {
    printf('<option value="%s" %s>%s</option>', h($row["class_id"]), ($row["class_id"] == $hist["class_id"] ? 'selected' : ''), h($row["class_nm"]) );
}
?>
</select>
</tr>
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?></font></td>
    <td><select id="atrb" name="atrb_id"></select></td>
</tr>
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?></font></td>
    <td><select id="dept" name="dept_id"></select></td>
</tr>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?></font></td>
    <td><select id="room" name="room_id"></select></td>
</tr>
<? } ?>
<tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変更日時</font></td>
    <td>
        <input type="text" name="histdate" value="<?=substr($hist["histdate"],0,19)?>" size="25"><br />
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　9999-99-99 99:99:99の形式で入力してください</font>
    </td>
</tr>
</table>

<p>
<button type="submit">登録</button>
</p>

<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="class_history_id" value="<? echo($class_history_id); ?>">
</form>

</td>
</tr>
</table>

</body>
</html>
<? pg_close($con); ?>