<?php
require_once('jnl_indicator.php');
require_once('jnl_indicator_facility.php');
require_once('jnl_indicator_nurse_home.php');
require_once('jnl_indicator_return.php');
require_once('jnl_indicator_data.php');
require_once('jnl_indicator_ajax.php');
/**
 * 管理指標 データ表示出力用クラス
 */
class IndicatorDisplay extends Indicator
{
    /**
     * エクセル表示に出力するメタタグ
     * @access protected
     */
    var $excelMeta = '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';

    var $colorArr = array(
         0 => array('name' => 'red',         'value' => '#FFC8ED')
       , 1 => array('name' => 'yellow',      'value' => '#FFE6D7')
       , 2 => array('name' => 'blue',        'value' => '#D2F4FA')
       , 3 => array('name' => 'green',       'value' => '#CFFFCF')
       , 4 => array('name' => 'ash',         'value' => '#888888')
       , 5 => array('name' => 'weekday',     'value' => '#ffe9c8')
       , 6 => array('name' => 'saturday',    'value' => '#c8fcff')
       , 7 => array('name' => 'holiday',     'value' => '#ffd7d7')
       , 8 => array('name' => 'lightpink',   'value' => 'lightpink')

       , 9 => array('name' => 'cadet_blue',  'value' => '#8ed2ff')
      , 10 => array('name' => 'orange',      'value' => '#ffba75')
      , 11 => array('name' => 'purple',      'value' => '#dba4ff')
      , 12 => array('name' => 'light_green', 'value' => '#a4ffb8')
      , 13 => array('name' => 'cadet_green', 'value' => '#b8ffa4')
      , 14 => array('name' => 'light_brown', 'value' => '#ffd2b7')
      , 15 => array('name' => 'pink',        'value' => '#ffc8e3')

      , 16 => array('name' => 'salmon',      'value' => '#FA8072')
    );

    var $columnAlphaNameArr = array(
         1 =>  'A',  2 =>  'B',  3 =>  'C',  4 =>  'D',  5 =>  'E',  6 =>  'F',  7 =>  'G',  8 =>  'H',  9 =>  'I', 10 =>  'J'
      , 11 =>  'K', 12 =>  'L', 13 =>  'M', 14 =>  'N', 15 =>  'O', 16 =>  'P', 17 =>  'Q', 18 =>  'R', 19 =>  'S', 20 =>  'T'
      , 21 =>  'U', 22 =>  'V', 23 =>  'W', 24 =>  'X', 25 =>  'Y', 26 => 'Z',  27 => 'AA', 28 => 'AB', 29 => 'AC', 30 => 'AD'
      , 31 => 'AE', 32 => 'AF', 33 => 'AG', 34 => 'AH', 35 => 'AI', 36 => 'AJ', 37 => 'AK', 38 => 'AL', 39 => 'AM', 40 => 'AN'
      , 41 => 'AO', 42 => 'AP', 43 => 'AQ', 44 => 'AR', 45 => 'AS', 46 => 'AT', 47 => 'AU', 48 => 'AV', 49 => 'AW', 50 => 'AX'
    );

    var $msoNumberFormatArr = array(
         'percent'             => array('name' => 'mso-number-format', 'value' => '\#\,\#\#0\.0%')
       , 'decimal'             => array('name' => 'mso-number-format', 'value' => '\#\,\#\#0\.0')
       , 'comma'               => array('name' => 'mso-number-format', 'value' => '\#\,\#\#0')
       , 'unit'                => array('name' => 'mso-number-format', 'value' => '\#\,\#\#0\.0\0022単位\0022')
       , 'comma_parenthesis'   => array('name' => 'mso-number-format', 'value' => '\\\(\#\,\#\#0\\\)')
       , 'decimal_parenthesis' => array('name' => 'mso-number-format', 'value' => '\\\(\#\,\#\#0\.0\\\)')
       , 'percent_parenthesis' => array('name' => 'mso-number-format', 'value' => '\\\(\#\,\#\#0\.0%\\\)')
    );

    /**
     * エクセル上の月別データの並び順(カラムのアルファベット)
     * @access protected
     */
    var $monthCellArr = array(
         0 =>  'C',  1 =>  'D',  2 =>  'E',  3 =>  'F',  4 =>  'G',  5 =>  'H',  6 =>  'I',  7 =>  'J',  8 =>  'K',  9 =>  'L'
      , 10 =>  'M', 11 =>  'N', 12 =>  'O', 13 =>  'P', 14 =>  'Q', 15 =>  'R', 16 =>  'S', 17 =>  'T', 18 =>  'U', 19 =>  'V'
      , 20 =>  'W', 21 =>  'X', 22 =>  'Y', 23 =>  'Z', 24 => 'AA', 25 => 'AB', 26 => 'AC', 27 => 'AD', 28 => 'AE', 29 => 'AF'
      , 30 => 'AG', 31 => 'AH', 32 => 'AI', 33 => 'AJ', 34 => 'AL', 35 => 'AM', 36 => 'AN', 37 => 'AO', 38 => 'AP', 39 => 'AQ'
      , 40 => 'AR', 41 => 'AS', 42 => 'AT', 43 => 'AU', 44 => 'AV', 45 => 'AW', 46 => 'AX', 47 => 'AY', 48 => 'AZ', 49 => 'BA'
    );

    /**
     * IndicatorData クラス格納オブジェクト
     * @var IndicatorData
     */
    var $indicatorData;

    /**
     * IndicatorFacility クラス格納オブジェクト
     * @var IndicatorFacility
     */
    var $indicatorFacility;

    /**
     * IndicatorNurseHome クラス格納オブジェクト
     * @var IndicatorNurseHome
     */
    var $indicatorNurseHome;

    /**
     * IndicatorReturn クラス格納オブジェクト
     * @var IndicatorReturn
     */
    var $indicatorReturn;

    var $lastOpenReportNo;

    /**
     * コンストラクタ
     * @param  array $arr
     * @return void
     */
    function IndicatorDisplay($arr)
    {
        // 親クラスのコンストラクタ
        parent::Indicator($arr);
        $no     = $this->getArrValueByKey('no',      $arr);
        $lastNo = $this->getArrValueByKey('last_no', $arr);
        $type   = $this->getArrValueByKey('type',    $arr);

        $thisYear  = date('Y');
        $thisMonth = date('m');
        $today     = date('d');

        # 違う画面へ遷移する場合
        if ($no != $lastNo) {
            if (is_null($type)) {
                switch ($no) {
                    # その年、その月、その日を表示
                    default:
                        $this->setDisplayYear();
                        $this->setDisplayMonth();
                        $this->setDisplayDay();
                        break;

                    # 前日を表示(月またぎ、年またぎにも対応)
                    case  '62': # [病院] 病院別患者日報
                    case  '63': # [病院] 病院別患者日報(シンプル版)
                    case  '73': # [病院] 病院別患者日報(累積統計表)
                    case '100': # [老健] 施設別患者日報
                    case '101': # [老健] 施設別患者日報(シンプル版)
                    case '102': # [老健] 施設別患者日報(累積統計表)
                        $this->setDisplayYear(date('Y', mktime(0, 0, 0, $thisMonth, $today - 1, $thisYear)));
                        $this->setDisplayMonth(date('m', mktime(0, 0, 0, $thisMonth, $today - 1, $thisYear)));
                        $this->setDisplayDay(date('d', mktime(0, 0, 0, $thisMonth, $today - 1, $thisYear)));
                        break;

                    # 前月を表示(年またぎ対応)
                    case  '71': # [病院]         病院月間報告書患者数総括
                    case  '72': # [病院]         病院月間報告書(行為件数)
                    case  '76': # [施設別：病院] 外来診療行為別明細
                    case  '77': # [施設別：病院] 外来日当点
                    case  '78': # [施設別：病院] 入院診療行為別明細
                    case  '79': # [施設別：病院] 入院日当点
                    case  '80': # [施設別：病院] 診療報酬総括表(医科)
                    case  '81': # [施設別：病院] 診療報酬総括表(歯科)
                    case '108': # [老健]         月間加算項目・算定実績一覧
                    case '125': # [施設別：老健] 療養費明細書
                    case '126': # [施設別：老健] 予防給付療養費明細書
                        $this->setDisplayYear(date('Y', mktime(0, 0, 0, $thisMonth - 1, 1, $thisYear)));
                        $this->setDisplayMonth(date('m', mktime(0, 0, 0, $thisMonth - 1, 1, $thisYear)));
                        break;
                }

                $startYearMonth = $this->getStartYearMonth();
                list($lastYear) = explode('/', $startYearMonth);

                $this->setLastYear($lastYear);
                $this->setCurrentYear($lastYear + 1);
            }
            else if($type == 'fiscal') {
                $this->setCurrentYear();
                $currentYear = $this->getCurrentYear();
                $this->setDisplayYear($currentYear);
                $this->setLastYear($currentYear - 1);
            }
        }
        else if (is_null($type)) {
            $displayYear = $this->getArrayValueByKeyForEmptyIsNull('display_year', $arr);
            $this->setDisplayYear($displayYear);
            $this->setDisplayMonth($this->getArrayValueByKeyForEmptyIsNull('display_month', $arr));
            $this->setDisplayDay($this->getArrayValueByKeyForEmptyIsNull('display_day', $arr));
            $startYearMonth = $this->getStartYearMonth();
            list($lastYear) = explode('/', $startYearMonth);

            $this->setLastYear($lastYear);
            $this->setCurrentYear($lastYear + 1);
        }
        # 年度のセレクトボックスの場合
        else if($type == 'fiscal') {
            $currentYear = $this->getArrValueByKey('current_year', $arr);
            $this->setCurrentYear($currentYear);
            $currentYear = $this->getCurrentYear();
            $this->setDisplayYear($currentYear);
            $this->setLastYear($currentYear - 1);
        }
    }

    /**
     * 前年度との比較表示用 method
     * @param  array $dataArr  表示対象データ
     * @param  array $plantArr 病院リスト
     * @param  array $arr      表示オプション
	 * @param  array $optionArr      表示年度
	 *                         total       => '表示列名'     施設合計
     *                         average     => '表示列名'     施設平均
     *                         raito       => null or mixed  増減率
     *                         amg_total   => null or mixed  AMG合計
     *                         amg_average => null or mixed  AMG平均
     *
     * @return mixed           表示データ(html table)
     */
	function getComparisonDisplayInThePreviousYear($dataArr, $plantArr, $arr, $optionArr)//20110419
    {
        # 各表示フラグ
        $total      = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr); # 施設合計
        $average    = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr); # 施設平均
        $raito      = $this->getArrayValueByKeyForEmptyIsNull('raito',       $arr); # 増減率
        $amgTotal   = $this->getArrayValueByKeyForEmptyIsNull('amg_total',   $arr); # AMG合計
        $amgAverage = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr); # AMG平均

		//20110419
/*
        $lastYear    = $this->getLastYear();
        $currentYear = $this->getCurrentYear();
        $startMonth  = $this->getFiscalYearStartMonth();
*/
		if($optionArr == null)
		{$currentYear = $this->getCurrentYear();}
		else
			{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
		$lastYear		= $currentYear - 1;
		$startMonth	= $this->getFiscalYearStartMonth();
		//20110419
		

        // 増減率がある場合は 1 施設 4 行になる
        $rowspan = (!is_null($raito))? 4 : 3;

        $thColorArr   = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));  # 青
        $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green')); # 緑

        $thColor   = $thColorArr['name'].   ':'. $thColorArr['value'];
        $compColor = $compColorArr['name']. ':'. $compColorArr['value'];

        $fontTag = $this->getFontTag();
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

        $dateTable = '<table class="list" width="100%"><tbody>';

        // header
        $tableHeader = '<tr><td style="'.$thColor.'" colspan="2" />';

        for ($i=0; $i < 12; $i++) {
            $dispMonth = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
            $tableHeader .= '<td style="text-align: center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $dispMonth. '月'). '</td>';
        }

        $tableHeader .= (!is_null($total))?   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['total']). '</td>'   : '';
        $tableHeader .= (!is_null($average))? '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['average']). '</td>' : '';
        $tableHeader .= '</tr>';

        $dateTable   .= $tableHeader;

        $plantCount    = count($plantArr);
        $amgTotalRow   = array();
        $amgAverageRow = array();
        $totalRowNum   = is_null($total)? 0 : 12;
        $averageRowNum = is_null($average)? 0 : is_null($total)? 12 : 13;

        // data
        if (is_array($dataArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $facilityId  = $this->getArrValueByKey('facility_id', $plantVal);
                //$lastYear    = $this->getLastYear();
                //$currentYear = $this->getCurrentYear();
				if($optionArr == null)
					{$currentYear = $this->getCurrentYear();}
				else
					{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
				$lastYear		= $currentYear - 1;
				
                $lastRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center;">'.sprintf($fontTag, $lastYear.'年度').'</td>';

                $currentRowHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, $plantVal['facility_name']).'</td>'
                                       .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $compRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';

                $raitoRowHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減率').'</td>';

                $amgTotalHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG合計').'</td>'
                                     .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $amgAverageHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG平均').'</td>'
                                       .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $lastRow    = array();
                $currentRow = array();
                $compRow    = array();
                $raitoRow   = array();

                $lastTotal    = 0;
                $currentTotal = 0;
                $compTotal    = 0;
                $raitoToral   = 0;

                $lastTotalStatus    = 3;
                $currentTotalStatus = 3;
                $compTotalStatus    = 3;
                $raitoToralStatus   = 3;

                $currentYearMonthCount = 0;
                $lastYearMonthCount    = 0;

                for ($i=0; $i < 12; $i++) {
                    //$currentYear = $this->getCurrentYear();
                    //$lastYear    = $this->getLastYear();
					if($optionArr == null)
						{$currentYear = $this->getCurrentYear();}
					else
						{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
					$lastYear		= $currentYear - 1;
					
                    $month       = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth  + $i - 12;
                    $currentYear = (($startMonth + $i) <= 12)? $currentYear     : $currentYear + 1;
                    $lastYear    = (($startMonth + $i) <= 12)? $lastYear        : $lastYear    + 1;

                    $currentData = 0;
                    $lastData    = 0;
                    $compData    = 0;
                    $raitoData   = 0;

                    $currentStatus = $this->checkCellDataStatus('current', array('year' => $currentYear, 'main_data' => array('month' => $month)));
                    $lastStatus    = $this->checkCellDataStatus('last',    array('year' => $lastYear,    'main_data' => array('month' => $month)));

                    foreach ($dataArr as $dataVal) {
                        if ($facilityId == $this->getArrValueByKey('facility_id', $dataVal) && $month == $this->getArrValueByKey('month', $dataVal)) {
                            if (!is_null($this->getArrValueByKey($currentYear, $dataVal))) {
                                $currentData = $currentData + $this->getArrValueByKey($currentYear, $dataVal);
                                $currentStatus = $this->checkCellDataStatus('current', array('year' => $currentYear, 'main_data' => $dataVal));
                                $currentYearMonthCount = $currentYearMonthCount + 1;
                                $currentPlantMonthCount[$i] = $currentPlantMonthCount[$i] + 1;
                            }
                            if (!is_null($this->getArrValueByKey($lastYear, $dataVal))) {
                                $lastData    = $lastData + $this->getArrValueByKey($lastYear, $dataVal);
                                $lastStatus    = $this->checkCellDataStatus('last', array('year' => $lastYear, 'main_data' => $dataVal));
                                $lastYearMonthCount = $lastYearMonthCount + 1;
                                $lastPlantMonthCount[$i] = $lastPlantMonthCount[$i] + 1;
                            }
                        }
                    }

                    $compData  = $this->getNumberOfEmptyFloors($lastData, $currentData, 1);
                    $raitoData = $this->getUtilizationRates($compData, $lastData, 1);

                    $compStatus  = $this->checkCellDataStatus('comp', array($lastStatus, $currentStatus));
                    $raitoStatus = $this->checkCellDataStatus('comp', array($compStatus, $lastStatus));

                    $currentRow[$i]['data'] = $currentData;
                    $lastRow[$i]['data']    = $lastData;
                    $compRow[$i]['data']    = $compData;
                    $raitoRow[$i]['data']   = $raitoData;

                    $currentRow[$i]['status'] = $currentStatus;
                    $lastRow[$i]['status']    = $lastStatus;
                    $compRow[$i]['status']    = $compStatus;
                    $raitoRow[$i]['status']   = $raitoStatus;

                    $raitoRow[$i]['disp_type'] = 'float';

                    $currentTotal = $currentTotal + $currentData;
                    $lastTotal    = $lastTotal    + $lastData;

                    $currentTotalStatus = $this->checkCellDataStatus('comp', array($currentTotalStatus, $currentStatus));
                    $lastTotalStatus    = $this->checkCellDataStatus('comp', array($lastTotalStatus,    $lastStatus));

                    $amgTotalRow['current'][$i]['data'] = $amgTotalRow['current'][$i]['data'] + $currentData;
                    $amgTotalRow['last'][$i]['data']    = $amgTotalRow['last'][$i]['data'] + $lastData;
                    $amgTotalRow['comp'][$i]['data']    = $this->getNumberOfEmptyFloors($amgTotalRow['last'][$i]['data'], $amgTotalRow['current'][$i]['data'], 1);
                    $amgTotalRow['raito'][$i]['data']   = $this->getUtilizationRates($amgTotalRow['comp'][$i]['data'], $amgTotalRow['last'][$i]['data'], 1);

                    $amgTotalRow['last'][$i]['status']    = $this->checkCellDataStatus('amg', array($amgTotalRow['last'][$i]['status'],    $lastRow[$i]['status']));
                    $amgTotalRow['current'][$i]['status'] = $this->checkCellDataStatus('amg', array($amgTotalRow['current'][$i]['status'], $currentRow[$i]['status']));
                    $amgTotalRow['comp'][$i]['status']    = $this->checkCellDataStatus('amg', array($amgTotalRow['comp'][$i]['status'],    $compRow[$i]['status']));
                    $amgTotalRow['raito'][$i]['status']   = $this->checkCellDataStatus('amg', array($amgTotalRow['raito'][$i]['status'],   $raitoRow[$i]['status']));

                    $amgTotalRow['raito'][$i]['disp_type'] = 'float';
                }

                $compTotal  = $this->getNumberOfEmptyFloors($lastTotal, $currentTotal, 1);
                $raitoToral = $this->getUtilizationRates($compTotal, $lastTotal, 1);

                $compTotalStatus    = $this->checkCellDataStatus('comp', array($currentTotalStatus, $lastTotalStatus));
                $raitoToralStatus   = $this->checkCellDataStatus('comp', array($compTotalStatus, $lastTotalStatus));

                // 施設合計表示の場合
                if (!is_null($total)) {
                    $currentRow[$totalRowNum]['data'] = $currentTotal;
                    $lastRow[$totalRowNum]['data']    = $lastTotal;
                    $compRow[$totalRowNum]['data']    = $compTotal;
                    $raitoRow[$totalRowNum]['data']   = $raitoToral;

                    $currentRow[$totalRowNum]['status'] = $currentTotalStatus;
                    $lastRow[$totalRowNum]['status']    = $lastTotalStatus;
                    $compRow[$totalRowNum]['status']    = $compTotalStatus;
                    $raitoRow[$totalRowNum]['status']   = $raitoToralStatus;

                    $raitoRow[$totalRowNum]['disp_type'] = 'float';
                }

                // 施設平均表示の場合
                if (!is_null($average)) {
                    $currentRow[$averageRowNum]['data'] = round($currentTotal / $currentYearMonthCount, 1);
                    $lastRow[$averageRowNum]['data']    = round($lastTotal / $lastYearMonthCount, 1);
                    $compRow[$averageRowNum]['data']    = $this->getNumberOfEmptyFloors($lastRow[$averageRowNum]['data'], $currentRow[$averageRowNum]['data'], 1);
                    $raitoRow[$averageRowNum]['data']   = $this->getUtilizationRates($compRow[$averageRowNum]['data'], $lastRow[$averageRowNum]['data'], 1);

                    $currentRow[$averageRowNum]['status'] = $currentTotalStatus;
                    $lastRow[$averageRowNum]['status']    = $lastTotalStatus;
                    $compRow[$averageRowNum]['status']    = $compTotalStatus;
                    $raitoRow[$averageRowNum]['status']   = $raitoToralStatus;

                    $currentRow[$averageRowNum]['disp_type'] = 'float';
                    $lastRow[$averageRowNum]['disp_type']    = 'float';
                    $compRow[$averageRowNum]['disp_type']    = 'float';
                    $raitoRow[$averageRowNum]['disp_type']   = 'float';
                }

                $dateTable .= $currentRowHeader.$this->convertCellData($currentRow, $fontTag).'</tr>'
                             .$lastRowHeader.$this->convertCellData($lastRow, $fontTag).'</tr>'
                             .$compRowHeader.$this->convertCellData($compRow, $fontTag, array($compColorArr)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) { $dateTable .= $raitoRowHeader.$this->convertCellData($raitoRow, $fontTag, array('bgColor' => $compColorArr), true).'</tr>'; }
            }
        }

        $amgTotalSideTotal_lastData    = 0;
        $amgTotalSideTotal_currentData = 0;
        $amgTotalSideTotal_compData    = 0;
        $amgTotalSideTotal_raitoData   = 0;

        $amgTotalSideAverage_lastData    = 0;
        $amgTotalSideAverage_currentData = 0;
        $amgTotalSideAverage_compData    = 0;
        $amgTotalSideAverage_raitoData   = 0;

        $amgTotalSideTotal_lastStatus    = 3;
        $amgTotalSideTotal_currentStatus = 3;
        $amgTotalSideTotal_compStatus    = 3;
        $amgTotalSideTotal_raitoStatus   = 3;

        $amgTotalSideAverage_lastStatus    = 3;
        $amgTotalSideAverage_currentStatus = 3;
        $amgTotalSideAverage_compStatus    = 3;
        $amgTotalSideAverage_raitoStatus   = 3;

        $amgAverageSideTotal_lastData    = 0;
        $amgAverageSideTotal_currentData = 0;
        $amgAverageSideTotal_compData    = 0;
        $amgAverageSideTotal_raitoData   = 0;

        $amgAverageSideTotal_lastStatus    = 3;
        $amgAverageSideTotal_currentStatus = 3;
        $amgAverageSideTotal_compStatus    = 3;
        $amgAverageSideTotal_raitoStatus   = 3;

        $amgAverageSideAverage_currentData = 0;
        $amgAverageSideAverage_lastData    = 0;
        $amgAverageSideAverage_compData    = 0;
        $amgAverageSideAverage_raitoData   = 0;

        $amgAverageSideAverage_lastStatus    = 3;
        $amgAverageSideAverage_currentStatus = 3;
        $amgAverageSideAverage_compStatus    = 3;
        $amgAverageSideAverage_raitoStatus   = 3;

        foreach ($amgTotalRow['current'] as $rowKey => $rowData) {
            $amgAverage_currentData = round($rowData['data'] / $currentPlantMonthCount[$rowKey], 1);

            $amgTotalSideTotal_currentData   = $amgTotalSideTotal_currentData + $rowData['data'];
            $amgAverageSideTotal_currentData = $amgAverageSideTotal_currentData + $amgAverage_currentData;

            $amgTotalSideTotal_currentStatus     = $this->checkCellDataStatus('comp', array($amgTotalSideTotal_currentStatus,     $rowData['status']));
            $amgTotalSideAverage_currentStatus   = $this->checkCellDataStatus('comp', array($amgTotalSideAverage_currentStatus,   $rowData['status']));
            $amgAverageSideTotal_currentStatus   = $this->checkCellDataStatus('comp', array($amgAverageSideTotal_currentStatus,   $rowData['status']));
            $amgAverageSideAverage_currentStatus = $this->checkCellDataStatus('comp', array($amgAverageSideAverage_currentStatus, $rowData['status']));

            $amgAverageRow['current'][$rowKey]['data']   = $amgAverage_currentData;
            $amgAverageRow['current'][$rowKey]['status'] = $rowData['status'];
        }

        foreach ($amgTotalRow['last'] as $rowKey => $rowData) {
            $amgAverage_lastData = round($rowData['data'] / $lastPlantMonthCount[$rowKey], 1);

            $amgTotalSideTotal_lastData   = $amgTotalSideTotal_lastData + $rowData['data'];
            $amgAverageSideTotal_lastData = $amgAverageSideTotal_lastData + $amgAverage_lastData;

            $amgTotalSideTotal_lastStatus     = $this->checkCellDataStatus('comp', array($amgTotalSideTotal_lastStatus,     $rowData['status']));
            $amgTotalSideAverage_lastStatus   = $this->checkCellDataStatus('comp', array($amgTotalSideAverage_lastStatus,   $rowData['status']));
            $amgAverageSideTotal_lastStatus   = $this->checkCellDataStatus('comp', array($amgAverageSideTotal_lastStatus,   $rowData['status']));
            $amgAverageSideAverage_lastStatus = $this->checkCellDataStatus('comp', array($amgAverageSideAverage_lastStatus, $rowData['status']));

            $amgAverageRow['last'][$rowKey]['data']   = $amgAverage_lastData;
            $amgAverageRow['last'][$rowKey]['status'] = $rowData['status'];
        }

        foreach ($amgTotalRow['comp'] as $rowKey => $rowData) {
            $amgAverageRow['comp'][$rowKey]['data']   = $this->getNumberOfEmptyFloors($amgAverageRow['last'][$rowKey]['data'], $amgAverageRow['current'][$rowKey]['data'], 1);
            $amgAverageRow['comp'][$rowKey]['status'] = $this->checkCellDataStatus('comp', array($amgAverageRow['current'][$rowKey]['status'], $amgAverageRow['last'][$rowKey]['status']));
        }

        foreach ($amgTotalRow['raito'] as $rowKey => $rowData) {
            $amgAverageRow['raito'][$rowKey]['data'] = $this->getUtilizationRates($amgAverageRow['comp'][$rowKey]['data'], $amgAverageRow['last'][$rowKey]['data'], 1);
            $amgAverageRow['raito'][$rowKey]['status'] = $this->checkCellDataStatus('comp', array($amgAverageRow['comp'][$rowKey]['status'], $amgAverageRow['last'][$rowKey]['status']));
        }

        // 施設合計表示の場合
        if (!is_null($total)) {
            $amgTotalSideTotal_compData  = $this->getNumberOfEmptyFloors($amgTotalSideTotal_lastData, $amgTotalSideTotal_currentData, 1);
            $amgTotalSideTotal_raitoData = $this->getUtilizationRates($amgTotalSideTotal_compData, $amgTotalSideTotal_lastData, 1);

            $amgTotalSideTotal_compStatus  = $this->checkCellDataStatus('comp', array($amgTotalSideTotal_lastStatus, $amgTotalSideTotal_currentStatus));
            $amgTotalSideTotal_raitoStatus = $this->checkCellDataStatus('comp', array($amgTotalSideTotal_lastStatus, $amgTotalSideTotal_compStatus));

            $amgAverageSideTotal_compData  = $this->getNumberOfEmptyFloors($amgAverageSideTotal_lastData, $amgAverageSideTotal_currentData, 1);
            $amgAverageSideTotal_raitoData = $this->getUtilizationRates($amgAverageSideTotal_compData, $amgAverageSideTotal_lastData, 1);

            $amgAverageSideTotal_compStatus  = $this->checkCellDataStatus('comp', array($amgAverageSideTotal_currentStatus, $amgAverageSideTotal_lastStatus));
            $amgAverageSideTotal_raitoStatus = $this->checkCellDataStatus('comp', array($amgAverageSideTotal_compStatus, $amgAverageSideTotal_lastStatus));

            // AMG合計
            $amgTotalRow['current'][$totalRowNum]['data'] = round($amgTotalSideTotal_currentData, 1);
            $amgTotalRow['last'][$totalRowNum]['data']    = round($amgTotalSideTotal_lastData,    1);
            $amgTotalRow['comp'][$totalRowNum]['data']    = round($amgTotalSideTotal_compData,    1);
            $amgTotalRow['raito'][$totalRowNum]['data']   = $amgTotalSideTotal_raitoData;

            $amgTotalRow['last'][$totalRowNum]['status']    = $amgTotalSideTotal_lastStatus;
            $amgTotalRow['current'][$totalRowNum]['status'] = $amgTotalSideTotal_currentStatus;
            $amgTotalRow['comp'][$totalRowNum]['status']    = $amgTotalSideTotal_compStatus;
            $amgTotalRow['raito'][$totalRowNum]['status']   = $amgTotalSideTotal_raitoStatus;

            $amgTotalRow['raito'][$totalRowNum]['disp_type'] = 'float';

            // AMG平均
            $amgAverageRow['current'][$totalRowNum]['data'] = round($amgAverageSideTotal_currentData, 1);
            $amgAverageRow['last'][$totalRowNum]['data']    = round($amgAverageSideTotal_lastData,    1);
            $amgAverageRow['comp'][$totalRowNum]['data']    = round($amgAverageSideTotal_compData,    1);
            $amgAverageRow['raito'][$totalRowNum]['data']   = $amgAverageSideTotal_raitoData;

            $amgAverageRow['last'][$totalRowNum]['status']    = $amgAverageSideTotal_lastStatus;
            $amgAverageRow['current'][$totalRowNum]['status'] = $amgAverageSideTotal_currentStatus;
            $amgAverageRow['comp'][$totalRowNum]['status']    = $amgAverageSideTotal_compStatus;
            $amgAverageRow['raito'][$totalRowNum]['status']   = $amgAverageSideTotal_raitoStatus;

            $amgAverageRow['current'][$totalRowNum]['disp_type'] = 'float';
            $amgAverageRow['last'][$totalRowNum]['disp_type']    = 'float';
            $amgAverageRow['comp'][$totalRowNum]['disp_type']    = 'float';
            $amgAverageRow['raito'][$totalRowNum]['disp_type']   = 'float';
        }

        // 施設平均表示の場合
        if (!is_null($average)) {
            $amgTotalSideAverage_currentData = round($amgTotalSideTotal_currentData / $this->getDenominatorOfAverageValueOfAverageValue($currentPlantMonthCount), 1);
            $amgTotalSideAverage_lastData    = round($amgTotalSideTotal_lastData / $this->getDenominatorOfAverageValueOfAverageValue($lastPlantMonthCount), 1);
            $amgTotalSideAverage_compData    = $this->getNumberOfEmptyFloors($amgTotalSideAverage_lastData, $amgTotalSideAverage_currentData, 1);
            $amgTotalSideAverage_raitoData   = $this->getUtilizationRates($amgTotalSideAverage_compData, $amgTotalSideAverage_lastData, 1);

            $amgTotalSideAverage_compStatus  = $this->checkCellDataStatus('comp', array($amgTotalSideAverage_currentStatus, $amgTotalSideAverage_lastStatus));
            $amgTotalSideAverage_raitoStatus = $this->checkCellDataStatus('comp', array($amgTotalSideAverage_compStatus, $amgTotalSideAverage_lastStatus));

            $amgAverageSideAverage_currentData = round($amgAverageSideTotal_currentData / $this->getDenominatorOfAverageValueOfAverageValue($currentPlantMonthCount), 1);
            $amgAverageSideAverage_lastData    = round($amgAverageSideTotal_lastData / $this->getDenominatorOfAverageValueOfAverageValue($lastPlantMonthCount), 1);
            $amgAverageSideAverage_compData    = $this->getNumberOfEmptyFloors($amgAverageSideAverage_lastData, $amgAverageSideAverage_currentData, 1);
            $amgAverageSideAverage_raitoData   = $this->getUtilizationRates($amgAverageSideAverage_compData, $amgAverageSideAverage_lastData, 1);

            $amgAverageSideAverage_compStatus  = $this->checkCellDataStatus('comp', array($amgAverageSideAverage_currentStatus, $amgAverageSideAverage_lastStatus));
            $amgAverageSideAverage_raitoStatus = $this->checkCellDataStatus('comp', array($amgAverageSideAverage_compStatus, $amgAverageSideAverage_lastStatus));

            // AMG合計
            $amgTotalRow['current'][$averageRowNum]['data'] = $amgTotalSideAverage_currentData;
            $amgTotalRow['last'][$averageRowNum]['data']    = $amgTotalSideAverage_lastData;
            $amgTotalRow['comp'][$averageRowNum]['data']    = $amgTotalSideAverage_compData;
            $amgTotalRow['raito'][$averageRowNum]['data']   = $amgTotalSideAverage_raitoData;

            $amgTotalRow['current'][$averageRowNum]['status'] = $amgTotalSideAverage_currentStatus;
            $amgTotalRow['last'][$averageRowNum]['status']    = $amgTotalSideAverage_lastStatus;
            $amgTotalRow['comp'][$averageRowNum]['status']    = $amgTotalSideAverage_compStatus;
            $amgTotalRow['raito'][$averageRowNum]['status']   = $amgTotalSideAverage_raitoStatus;

            $amgTotalRow['current'][$averageRowNum]['disp_type'] = 'float';
            $amgTotalRow['last'][$averageRowNum]['disp_type']    = 'float';
            $amgTotalRow['comp'][$averageRowNum]['disp_type']    = 'float';
            $amgTotalRow['raito'][$averageRowNum]['disp_type']   = 'float';

            // AMG平均
            $amgAverageRow['current'][$averageRowNum]['data'] = $amgAverageSideAverage_currentData;
            $amgAverageRow['last'][$averageRowNum]['data']    = $amgAverageSideAverage_lastData;
            $amgAverageRow['comp'][$averageRowNum]['data']    = $amgAverageSideAverage_compData;
            $amgAverageRow['raito'][$averageRowNum]['data']   = $amgAverageSideAverage_raitoData;

            $amgAverageRow['current'][$averageRowNum]['status'] = $amgAverageSideAverage_currentStatus;
            $amgAverageRow['last'][$averageRowNum]['status']    = $amgAverageSideAverage_lastStatus;
            $amgAverageRow['comp'][$averageRowNum]['status']    = $amgAverageSideAverage_compStatus;
            $amgAverageRow['raito'][$averageRowNum]['status']   = $amgAverageSideAverage_raitoStatus;

            $amgAverageRow['current'][$averageRowNum]['disp_type'] = 'float';
            $amgAverageRow['last'][$averageRowNum]['disp_type']    = 'float';
            $amgAverageRow['comp'][$averageRowNum]['disp_type']    = 'float';
            $amgAverageRow['raito'][$averageRowNum]['disp_type']   = 'float';
        }

        if ($plantCount!=1) {
            // AMG合計表示の場合
            if (!is_null($amgTotal)) {
                $dateTable .= $amgTotalHeader.$this->convertCellData($amgTotalRow['current'], $fontTag).'</tr>'
                              .$lastRowHeader.$this->convertCellData($amgTotalRow['last'], $fontTag).'</tr>'
                              .$compRowHeader.$this->convertCellData($amgTotalRow['comp'], $fontTag, array($compColorArr)).'</tr>';
                if (!is_null($raito)) {
                    $dateTable .= $raitoRowHeader
                                 .$this->convertCellData($amgTotalRow['raito'], $fontTag, array($compColorArr), true).'</tr>';
                }
            }
            // AMG平均表示の場合
            if (!is_null($amgAverage)) {

                $dateTable .= $amgAverageHeader.$this->convertCellData($amgAverageRow['current'], $fontTag).'</tr>'
                                 .$lastRowHeader.$this->convertCellData($amgAverageRow['last'], $fontTag).'</tr>'
                                 .$compRowHeader.$this->convertCellData($amgAverageRow['comp'], $fontTag, array($compColorArr)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) {
                    $dateTable .= $raitoRowHeader.$this->convertCellData($amgAverageRow['raito'], $fontTag, array($compColorArr), true).'</tr>';
                }
            }
        }

        $dateTable .= '</tbody></table>';
        return $dateTable;
    }

    /**
     * 前年度との比較表示用(平均値算出用) method
     * @param  array $dataArr  表示対象データ
     * @param  array $plantArr 病院リスト
     * @param  array $arr      表示オプション
	* @param  array $optionArr      表示年度
	 *                         total       => '表示列名'     施設合計
     *                         average     => '表示列名'     施設平均
     *                         raito       => null or mixed  増減率
     *                         amg_total   => null or mixed  AMG合計
     *                         amg_average => null or mixed  AMG平均
     *
     * @return mixed           表示データ(html table)
     */
	function getComparisonDisplayInThePreviousYearAverage($dataArr, $plantArr, $arr, $optionArr)
    {
        // 各表示フラグ
        $total      = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr); // 施設合計
        $average    = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr); // 施設平均
        $raito      = $this->getArrayValueByKeyForEmptyIsNull('raito',       $arr); // 増減率
        $amgTotal   = $this->getArrayValueByKeyForEmptyIsNull('amg_total',   $arr); // AMG合計
        $amgAverage = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr); // AMG平均

/*
        $lastYear    = $this->getLastYear();
        $currentYear = $this->getCurrentYear();
        $startMonth  = $this->getFiscalYearStartMonth();
*/
		if($optionArr == null)
		{$currentYear = $this->getCurrentYear();}
		else
		{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
		$lastYear		= $currentYear - 1;
		$startMonth	= $this->getFiscalYearStartMonth();
		
        $fontTag     = $this->getFontTag();

        // 増減率がある場合は 1 施設 4 行になる
        $rowspan = (!is_null($raito))? 4 : 3;

        $thColorArr   = array('name' => 'background-color', 'value' => $this->getRgbColor('blue')); // 青
        $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green')); // 緑

        $thColor   = $this->getRgbColor('blue', 'background-color');  // 青
        $compColor = $this->getRgbColor('green', 'background-color'); // 緑

        $dateTable = '<table class="list" width="100%"><tbody>';

        // header
        $tableHeader = '<tr><td style="'.$thColor.'" colspan="2" />';

        for ($i=0; $i < 12; $i++) {
            $dispMonth = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
            $tableHeader .= '<td style="text-align: center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $dispMonth. '月'). '</td>';
        }

        $tableHeader .= (!is_null($total))?   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['total']). '</td>'   : '';
        $tableHeader .= (!is_null($average))? '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['average']). '</td>' : '';
        $tableHeader .= '</tr>';

        $dateTable   .= $tableHeader;

        $amgTotalRow    = array();
        $amgAverageRow  = array();
        $targetMonthNum = 0;
        $totalRowNum    = 12;
        $averageRowNum  = is_null($total)? 12 : 13 ;

        $cPlantCnt[] = array();
        $lPlantCnt[] = array();

        // data
        if (is_array($dataArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $facilityId  = $plantVal['facility_id'];
				
                //$lastYear    = $this->getLastYear();
                //$currentYear = $this->getCurrentYear();
				if($optionArr == null)
					{$currentYear = $this->getCurrentYear();}
				else
					{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
				$lastYear		= $currentYear - 1;
				
                $lastRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center;">'.sprintf($fontTag, $lastYear.'年度').'</td>';

                $currentRowHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, $plantVal['facility_name']).'</td>'
                                       .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $compRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';

                $raitoRowHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減率').'</td>';

                $amgTotalHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG合計').'</td>'
                                     .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $amgAverageHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG平均').'</td>'
                                       .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $lastRow    = array();
                $currentRow = array();
                $compRow    = array();
                $raitoRow   = array();

                $cPlantCnt[$plantKey] = null;
                $lPlantCnt[$plantKey] = null;

                for ($m=0; $m < 12; $m++) {
                    //$lastYear    = $this->getLastYear();
                    //$currentYear = $this->getCurrentYear();
					if($optionArr == null)
						{$currentYear = $this->getCurrentYear();}
					else
						{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
					$lastYear		= $currentYear - 1;
					
					$month       = (($startMonth + $m) <= 12)? $startMonth + $m : $startMonth + $m - 12;
                    $lastYear    = (($startMonth + $m) <= 12)? $lastYear        : $lastYear + 1;
                    $currentYear = (($startMonth + $m) <= 12)? $currentYear     : $currentYear + 1;
                    $currentData = 0;
                    $lastData    = 0;

                    $lastStatus    = $this->checkCellDataStatus('last',    array('year' => $lastYear,    'main_data' => array('month' => $month)));
                    $currentStatus = $this->checkCellDataStatus('current', array('year' => $currentYear, 'main_data' => array('month' => $month)));

                    foreach ($dataArr as $mainData) {
                        if ($facilityId == $this->getArrValueByKey('facility_id', $mainData)
                         && $month == $this->getArrValueByKey('month', $mainData)
                         && ( ($currentYear == date('Y') && $month <= date('m')) || $currentYear < date('Y') )
                        ) {
                            $current = $this->getArrValueByKey($currentYear, $mainData);
                            $last    = $this->getArrValueByKey($lastYear, $mainData);
                            if (!is_null($current)) {
                                $currentData = round($current / $mainData[$currentYear.'_count'], 1);
								if($arr["name"] == "外来平均患者数")
								{
									//"外来平均患者数"の場合は日・祝日を日付カウントからはずすので必ず基準の日数から足りなくなってしまうので必ず赤色になってしまう
									//暫定として必ず白色にしています。それ以外は日付カウントとその日までの日数と同じであれば赤でなくなる
									$currentStatus = 3;
								}
								else
								{
									$currentStatus = $this->checkCellDataStatus('current', array('year' => $currentYear, 'main_data' => $mainData));
								}

                                $cPlantCnt[$plantKey] = $cPlantCnt[$plantKey] + 1;
                                $cMonthCnt[$m] = empty($cMonthCnt[$m])? 1 : ($cMonthCnt[$m] + 1);

                            }
                            if (!is_null($last)) {
								
/////////////////////////////////////////////////////////////////////////////////////////////////////
//////	ここから↓は2009年度の日報データが過去データとして1日付けに一括で入力されているので
//////	平均数値が出ません。なので固定でその月の日数を分母に入れることで暫定的に対処しています。
//////	2011年度になったら必要なくなるので↓の処理を消してください。そして1行コメント部分をコメントをはずしてください
/////////////////////////////////////////////////////////////////////////////////////////////////////
								

								if(($lastYear == "2009") && (
									($mainData['month'] == "04") ||
									($mainData['month'] == "06") ||
									($mainData['month'] == "09") ||
									($mainData['month'] == "11") 
								))
								{
									$lastData = round($last / 30, 1);
									
								}
								elseif(($lastYear == "2009") && (
											($mainData['month'] == "05") ||
											($mainData['month'] == "07") ||
											($mainData['month'] == "08") ||
											($mainData['month'] == "10") ||
											($mainData['month'] == "12") 
											))
								{
									$lastData = round($last / 31, 1);
									
										
								}
								elseif(($lastYear == "2010") && (
											($mainData['month'] == "01") ||
											($mainData['month'] == "03") 
											))
								{
									
									$lastData = round($last / 31, 1);
									
								}
								elseif(($lastYear == "2010") && ($mainData['month'] == "02"))
								{
									
									$lastData = round($last / 28, 1);
									
								}
								else
								{
									$lastData = round($last / $mainData[$lastYear.'_count'], 1);
								}

								//↓のコメントをはずして
								//$lastData = round($last / $mainData[$lastYear.'_count'], 1);
								
/////////////////////////////////////////////////////////////////////////////////////////////////////
//////	ここまで↑
/////////////////////////////////////////////////////////////////////////////////////////////////////
								
								
								
                                $lastStatus = $this->checkCellDataStatus('last', array('year' => $lastYear, 'main_data' => $mainData));
                                $lPlantCnt[$plantKey] = $lPlantCnt[$plantKey] + 1;
                                $lMonthCnt[$m] = empty($lMonthCnt[$m])? 1 : ($lMonthCnt[$m] + 1);
                            }
                        }
                    }

                    $currentRow[$m]['data']   = $currentData;
                    $lastRow[$m]['data']      = $lastData;
                    $compRow[$m]['data']      = $this->getNumberOfEmptyFloors($lastData, $currentData, 1);
                    $raitoRow[$m]['data']     = $this->getUtilizationRates($compRow[$m]['data'], $lastData, 1);
                    $lastRow[$m]['status']    = $lastStatus;
                    $currentRow[$m]['status'] = $currentStatus;
                    $compRow[$m]['status']    = $this->checkCellDataStatus('comp',  array($lastRow[$m]['status'], $currentRow[$m]['status']));
                    $raitoRow[$m]['status']   = $this->checkCellDataStatus('raito', array($compRow[$m]['status']));

                    $currentRow[$m]['disp_type'] = 'float';
                    $lastRow[$m]['disp_type']    = 'float';
                    $compRow[$m]['disp_type']    = 'float';
                    $raitoRow[$m]['disp_type']   = 'float_percent';

                    $amgTotalRow['last'][$m]['data']      = round($amgTotalRow['last'][$m]['data'] + $lastRow[$m]['data'], 1);
                    $amgTotalRow['current'][$m]['data']   = round($amgTotalRow['current'][$m]['data'] + $currentRow[$m]['data'], 1);
                    $amgTotalRow['comp'][$m]['data']      = empty($amgTotalRow['comp'][$m]['data'])? 0 : $amgTotalRow['comp'][$m]['data'];
                    $amgTotalRow['raito'][$m]['data']     = $this->getUtilizationRates($amgTotalRow['comp'][$m]['data'], $amgTotalRow['last'][$m]['data'], 1);
                    $amgTotalRow['last'][$m]['status']    = $this->checkCellDataStatus('amg', array($amgTotalRow['last'][$m]['status'],    $lastRow[$m]['status']));
                    $amgTotalRow['current'][$m]['status'] = $this->checkCellDataStatus('amg', array($amgTotalRow['current'][$m]['status'], $currentRow[$m]['status']));
                    $amgTotalRow['comp'][$m]['status']    = $this->checkCellDataStatus('amg', array($amgTotalRow['comp'][$m]['status'],    $compRow[$m]['status']));
                    $amgTotalRow['raito'][$m]['status']   = $this->checkCellDataStatus('amg', array($amgTotalRow['raito'][$m]['status'],   $raitoRow[$m]['status']));

                    $amgTotalRow['last'][$m]['disp_type']    = 'float';
                    $amgTotalRow['current'][$m]['disp_type'] = 'float';
                    $amgTotalRow['comp'][$m]['disp_type']    = 'float';
                    $amgTotalRow['raito'][$m]['disp_type']   = 'float_percent';
                }

                $lastTotal    = 0;
                $currentTotal = 0;
                $compTotal    = 0;
                $raitoToral   = 0;

                $lastTotalStatus    = 3;
                $currentTotalStatus = 3;
                $compTotalStatus    = 3;
                $raitoToralStatus   = 3;

                foreach ($lastRow as $rowData) {
                    $lastTotal       = round($lastTotal + $rowData['data'], 1);
                    $lastTotalStatus = $this->checkCellDataStatus('total', array($lastTotalStatus, $rowData['status']));
                }

                foreach ($currentRow as $rowData) {
                    $currentTotal       = round($currentTotal + $rowData['data'], 1);
					$currentTotalStatus = $this->checkCellDataStatus('total', array($currentTotalStatus, $rowData['status']));
               }

                $compTotal        = $this->getNumberOfEmptyFloors($lastTotal, $currentTotal, 1);
                $raitoToral       = $this->getUtilizationRates($compTotal, $lastTotal, 1);
                $compTotalStatus  = $this->checkCellDataStatus('total', array($lastTotalStatus, $currentTotalStatus));
                $raitoToralStatus = $this->checkCellDataStatus('total', array($compTotalStatus, $lastTotalStatus));

                // 施設合計表示の場合
                if (!is_null($total)) {
                    $lastRow[$totalRowNum]['data']      = $lastTotal;
                    $currentRow[$totalRowNum]['data']   = $currentTotal;
                    $compRow[$totalRowNum]['data']      = $this->getNumberOfEmptyFloors($lastTotal, $currentTotal, 1);
                    $raitoRow[$totalRowNum]['data']     = $this->getUtilizationRates($compRow[$totalRowNum]['data'], $lastTotal, 1);
                    $lastRow[$totalRowNum]['status']    = $lastTotalStatus;
                    $currentRow[$totalRowNum]['status'] = $currentTotalStatus;
                    $compRow[$totalRowNum]['status']    = $compTotalStatus;
                    $raitoRow[$totalRowNum]['status']   = $raitoToralStatus;

                    $lastRow[$totalRowNum]['disp_type']    = 'float';
                    $currentRow[$totalRowNum]['disp_type'] = 'float';
                    $compRow[$totalRowNum]['disp_type']    = 'float';
                    $raitoRow[$totalRowNum]['disp_type']   = 'float_percent';
                }

                // 施設平均表示の場合
                if (!is_null($average)) {
                    $currentRow[$averageRowNum]['data']   = round($currentTotal / $cPlantCnt[$plantKey], 1);
                    $lastRow[$averageRowNum]['data']      = round($lastTotal / $lPlantCnt[$plantKey], 1);
                    $compRow[$averageRowNum]['data']      = $this->getNumberOfEmptyFloors($lastRow[$averageRowNum]['data'], $currentRow[$averageRowNum]['data'], 1);
                    $raitoRow[$averageRowNum]['data']     = $this->getUtilizationRates($compRow[$averageRowNum]['data'], $lastRow[$averageRowNum]['data'], 1);
                    $lastRow[$averageRowNum]['status']    = $lastTotalStatus;
                    $currentRow[$averageRowNum]['status'] = $currentTotalStatus;
                    $compRow[$averageRowNum]['status']    = $compTotalStatus;
                    $raitoRow[$averageRowNum]['status']   = $raitoToralStatus;

                    $lastRow[$averageRowNum]['disp_type']    = 'float';
                    $currentRow[$averageRowNum]['disp_type'] = 'float';
                    $compRow[$averageRowNum]['disp_type']    = 'float';
                    $raitoRow[$averageRowNum]['disp_type']   = 'float_percent';
                }

                $dateTable .= $currentRowHeader.$this->convertCellData($currentRow, $fontTag).'</tr>'
                             .$lastRowHeader.$this->convertCellData($lastRow, $fontTag).'</tr>'
                             .$compRowHeader.$this->convertCellData($compRow, $fontTag, array($compColorArr)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) { $dateTable .= $raitoRowHeader.$this->convertCellData($raitoRow, $fontTag, array('bgColor' => $compColorArr)).'</tr>'; }
            }
        }

        $lastAmgTotal    = 0;
        $currentAmgTotal = 0;
        $compAmgTotal    = 0;
        $raiotAmgTotal   = 0;

        $lastAmgAverage    = 0;
        $currentAmgAverage = 0;
        $compAmgAverage    = 0;
        $raiotAmgAverage   = 0;

        $lastAmgStatus    = 3;
        $currentAmgStatus = 3;
        $compAmgStatus    = 3;
        $raiotAmgStatus   = 3;

        foreach ($amgTotalRow['last'] as $rowKey => $rowData) {
            $amgAverageRow['last'][$rowKey]['data']      = round(($rowData['data'] / $lMonthCnt[$rowKey]), 1);
            $amgAverageRow['last'][$rowKey]['status']    = $rowData['status'];
            $amgAverageRow['last'][$rowKey]['disp_type'] = 'float';
            $lastAmgAverage = round($lastAmgAverage + $amgAverageRow['last'][$rowKey]['data'], 1);
            $lastAmgTotal   = round($lastAmgTotal + $rowData['data'], 1);
            $lastAmgStatus  = $this->checkCellDataStatus('total', array($lastAmgStatus, $rowData['status'], $amgAverageRow['last'][$rowKey]['status']));
        }

        foreach ($amgTotalRow['current'] as $rowKey => $rowData) {
            $amgAverageRow['current'][$rowKey]['data']      = round(($rowData['data'] / $cMonthCnt[$rowKey]), 1);
            $amgAverageRow['current'][$rowKey]['status']    = $rowData['status'];
            $amgAverageRow['current'][$rowKey]['disp_type'] = 'float';
            $currentAmgAverage = ($amgAverageRow['current'][$rowKey]['data']!=0)? bcadd($currentAmgAverage, $amgAverageRow['current'][$rowKey]['data'], 1) : $currentAmgAverage;
            $currentAmgTotal   = $currentAmgTotal + $rowData['data'];
			
			if($arr["name"] == "外来平均患者数")
			{
				//"外来平均患者数"の場合は日・祝日を日付カウントからはずすので必ず基準の日数から足りなくなってしまうので必ず赤色になってしまう
				//暫定として必ず白色にしています。それ以外は日付カウントとその日までの日数と同じであれば赤でなくなる
				$currentTotalStatus = 3;
			}
			else
			{	
				$currentAmgStatus  = $this->checkCellDataStatus('total', array($currentAmgStatus, $rowData['status'], $amgAverageRow['current'][$rowKey]['status']));
			}
		}

        foreach ($amgTotalRow['comp'] as $rowKey => $rowData) {
            $amgTotalRow['comp'][$rowKey]['data']     = round(($amgTotalRow['current'][$rowKey]['data'] - $amgTotalRow['last'][$rowKey]['data']), 1);
            $amgAverageRow['comp'][$rowKey]['data']   = round(($amgAverageRow['current'][$rowKey]['data'] - $amgAverageRow['last'][$rowKey]['data']), 1);
            $amgAverageRow['comp'][$rowKey]['status'] = $this->checkCellDataStatus('total', array($amgAverageRow['last'][$rowKey]['status'], $amgAverageRow['current'][$rowKey]['status']));
            $amgAverageRow['comp'][$rowKey]['disp_type'] = 'float';
            $compAmgStatus  = $this->checkCellDataStatus('total', array($compAmgStatus, $rowData['status'], $amgAverageRow['last'][$rowKey]['status']));
        }

        foreach ($amgTotalRow['raito'] as $rowKey => $rowData) {
            $amgTotalRow['raito'][$rowKey]['data']        = $this->getUtilizationRates($amgTotalRow['comp'][$rowKey]['data'], $amgTotalRow['last'][$rowKey]['data'], 1);
            $amgAverageRow['raito'][$rowKey]['data']      = $this->getUtilizationRates($amgAverageRow['comp'][$rowKey]['data'], $amgAverageRow['last'][$rowKey]['data'], 1);
            $amgAverageRow['raito'][$rowKey]['status']    = $this->checkCellDataStatus('total', array($amgTotalRow['comp'][$rowKey]['status'], $amgTotalRow['current'][$rowKey]['status']));
            $amgAverageRow['raito'][$rowKey]['disp_type'] = 'float_percent';
            $raiotAmgStatus  = $this->checkCellDataStatus('total', array($raiotAmgStatus, $rowData['status'], $amgAverageRow['comp'][$rowKey]['status'], $amgAverageRow['last'][$rowKey]['status']));
        }

        // 施設合計表示の場合
        if (!is_null($total)) {
            // AMG合計
            $amgTotalRow['last'][$totalRowNum]['data']    = round($lastAmgTotal,    1);
            $amgTotalRow['current'][$totalRowNum]['data'] = round($currentAmgTotal, 1);
            $amgTotalRow['comp'][$totalRowNum]['data']    = bcsub($currentAmgTotal, $lastAmgTotal, 1);
            $amgTotalRow['raito'][$totalRowNum]['data']   = $this->getUtilizationRates($amgTotalRow['comp'][$totalRowNum]['data'], $amgTotalRow['last'][$totalRowNum]['data'], 1);

            $amgTotalRow['last'][$totalRowNum]['status']    = $lastAmgStatus;
            $amgTotalRow['current'][$totalRowNum]['status'] = $currentAmgStatus;
            $amgTotalRow['comp'][$totalRowNum]['status']    = $this->checkCellDataStatus('total', array($amgTotalRow['last'][$totalRowNum]['status'], $amgTotalRow['current'][$totalRowNum]['status']));
            $amgTotalRow['raito'][$totalRowNum]['status']   = $this->checkCellDataStatus('total', array($amgTotalRow['comp'][$totalRowNum]['status'], $amgTotalRow['last'][$totalRowNum]['status']));

            $amgTotalRow['last'][$totalRowNum]['disp_type']    = 'float';
            $amgTotalRow['current'][$totalRowNum]['disp_type'] = 'float';
            $amgTotalRow['comp'][$totalRowNum]['disp_type']    = 'float';
            $amgTotalRow['raito'][$totalRowNum]['disp_type']   = 'float_percent';

            // AMG平均
            $amgAverageRow['last'][$totalRowNum]['data']    = round($lastAmgAverage,    1);
            $amgAverageRow['current'][$totalRowNum]['data'] = round($currentAmgAverage, 1);
            $amgAverageRow['comp'][$totalRowNum]['data']    = bcsub($amgAverageRow['current'][$totalRowNum]['data'], $amgAverageRow['last'][$totalRowNum]['data'], 1);
            $amgAverageRow['raito'][$totalRowNum]['data']   = $this->getUtilizationRates($amgAverageRow['comp'][$totalRowNum]['data'], $amgAverageRow['last'][$totalRowNum]['data'], 1);

            $amgAverageRow['last'][$totalRowNum]['status']    = $lastAmgStatus;
            $amgAverageRow['current'][$totalRowNum]['status'] = $currentAmgStatus;
            $amgAverageRow['comp'][$totalRowNum]['status']    = $this->checkCellDataStatus('total', array($amgAverageRow['current'][$totalRowNum]['status'], $amgAverageRow['last'][$totalRowNum]['status']));
            $amgAverageRow['raito'][$totalRowNum]['status']   = $this->checkCellDataStatus('total', array($amgAverageRow['comp'][$totalRowNum]['status'], $amgAverageRow['last'][$totalRowNum]['status']));

            $amgAverageRow['last'][$totalRowNum]['disp_type']    = 'float';
            $amgAverageRow['current'][$totalRowNum]['disp_type'] = 'float';
            $amgAverageRow['comp'][$totalRowNum]['disp_type']    = 'float';
            $amgAverageRow['raito'][$totalRowNum]['disp_type']   = 'float_percent';
        }

        // 施設平均表示の場合
        if (!is_null($average)) {
            // AMG合計
            $amgTotalRow['current'][$averageRowNum]['data'] = round(bcdiv($currentAmgTotal, $this->getDenominatorOfAverageValueOfAverageValue($cMonthCnt), 2), 1);
            $amgTotalRow['last'][$averageRowNum]['data']    = round(bcdiv($lastAmgTotal,    $this->getDenominatorOfAverageValueOfAverageValue($lMonthCnt), 2), 1);
            $amgTotalRow['comp'][$averageRowNum]['data']    = round(bcsub($amgTotalRow['current'][$averageRowNum]['data'], $amgTotalRow['last'][$averageRowNum]['data'], 3), 1);
            $amgTotalRow['raito'][$averageRowNum]['data']   = $this->getUtilizationRates($amgTotalRow['comp'][$averageRowNum]['data'], $amgTotalRow['last'][$averageRowNum]['data'], 4);

            $amgTotalRow['last'][$averageRowNum]['status']    = $lastAmgStatus;
            $amgTotalRow['current'][$averageRowNum]['status'] = $currentAmgStatus;
            $amgTotalRow['comp'][$averageRowNum]['status']    = $this->checkCellDataStatus('total', array($amgTotalRow['current'][$averageRowNum]['status'], $amgTotalRow['last'][$averageRowNum]['status']));
            $amgTotalRow['raito'][$averageRowNum]['status']   = $this->checkCellDataStatus('total', array($amgTotalRow['comp'][$averageRowNum]['status'], $amgTotalRow['last'][$averageRowNum]['status']));

            $amgTotalRow['last'][$averageRowNum]['disp_type']    = 'float';
            $amgTotalRow['current'][$averageRowNum]['disp_type'] = 'float';
            $amgTotalRow['comp'][$averageRowNum]['disp_type']    = 'float';
            $amgTotalRow['raito'][$averageRowNum]['disp_type']   = 'float_percent';

            // AMG平均
            $amgAverageRow['current'][$averageRowNum]['data'] = round($currentAmgAverage / $this->getDenominatorOfAverageValueOfAverageValue($cMonthCnt), 1);
            $amgAverageRow['last'][$averageRowNum]['data']    = round($lastAmgAverage    / $this->getDenominatorOfAverageValueOfAverageValue($lMonthCnt), 1);
            $amgAverageRow['comp'][$averageRowNum]['data']    = $this->getNumberOfEmptyFloors($amgAverageRow['last'][$averageRowNum]['data'], $amgAverageRow['current'][$averageRowNum]['data'], 1);
            $amgAverageRow['raito'][$averageRowNum]['data']   = $this->getUtilizationRates($amgAverageRow['comp'][$averageRowNum]['data'], $amgAverageRow['last'][$averageRowNum]['data'], 1);

            $amgAverageRow['last'][$averageRowNum]['status']    = $lastAmgStatus;
            $amgAverageRow['current'][$averageRowNum]['status'] = $currentAmgStatus;
            $amgAverageRow['comp'][$averageRowNum]['status']    = $this->checkCellDataStatus('total', array($amgAverageRow['current'][$averageRowNum]['status'], $amgAverageRow['last'][$averageRowNum]['status']));
            $amgAverageRow['raito'][$averageRowNum]['status']   = $this->checkCellDataStatus('total', array($amgAverageRow['comp'][$averageRowNum]['status'], $amgAverageRow['last'][$averageRowNum]['status']));

            $amgAverageRow['last'][$averageRowNum]['disp_type']    = 'float';
            $amgAverageRow['current'][$averageRowNum]['disp_type'] = 'float';
            $amgAverageRow['comp'][$averageRowNum]['disp_type']    = 'float';
            $amgAverageRow['raito'][$averageRowNum]['disp_type']   = 'float_percent';
        }

        if (count($plantArr)!=1) {
            // AMG合計表示の場合
            if (!is_null($amgTotal)) {
                $dateTable .= $amgTotalHeader.$this->convertCellData($amgTotalRow['current'], $fontTag).'</tr>'
                              .$lastRowHeader.$this->convertCellData($amgTotalRow['last'], $fontTag).'</tr>'
                              .$compRowHeader.$this->convertCellData($amgTotalRow['comp'], $fontTag, array($compColorArr)).'</tr>';
                if (!is_null($raito)) {
                    $dateTable .= $raitoRowHeader
                                 .$this->convertCellData($amgTotalRow['raito'], $fontTag, array($compColorArr)).'</tr>';
                }
            }
            // AMG平均表示の場合
            if (!is_null($amgAverage)) {

                $dateTable .= $amgAverageHeader.$this->convertCellData($amgAverageRow['current'], $fontTag).'</tr>'
                                 .$lastRowHeader.$this->convertCellData($amgAverageRow['last'], $fontTag).'</tr>'
                                 .$compRowHeader.$this->convertCellData($amgAverageRow['comp'], $fontTag, array($compColorArr)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) { $dateTable .= $raitoRowHeader.$this->convertCellData($amgAverageRow['raito'], $fontTag, array($compColorArr)).'</tr>'; }
            }
        }

        $dateTable .= '</tbody></table>';
        return $dateTable;
    }

    /**
     * 稼働率表示画面
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array $optionArr      表示年度
	 * @return string
     */
    function getUtilizationRatesDisplayData($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr      = '';
        $fontTag     = $this->getFontTag();
        $total       = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr); // 施設合計
        $average     = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr); // 施設平均
        $amgAverage  = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr); // AMG平均
       
		//$lastYear    = $this->getLastYear();
        //$currentYear = $this->getCurrentYear();
       // $startMonth  = $this->getFiscalYearStartMonth();
		if($optionArr == null)
			{$currentYear = $this->getCurrentYear();}
		else
			{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
		$lastYear		= $currentYear - 1;
		$startMonth	= $this->getFiscalYearStartMonth();
		
        # attr
        $colspan_2        = array('name' => 'colspan',          'value' => 2);
        $rowspan_3        = array('name' => 'rowspan',          'value' => 3);

        # style
        $bgColor_blue     = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_green    = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
        $textAlign_center = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right  = array('name' => 'text-align',       'value' => 'right');

        $topHeaderArr   = array();
        $mainDataArr    = array();
        $leftSideArr    = array();
        $topHeaderArr[] = array('data' => '&nbsp;', 'attr' => array($colspan_2));

        for ($m=0; $m<12; $m++) {
            $month          = (($startMonth+$m)>12)? ($startMonth+$m)-12: ($startMonth+$m);
            $monthName      = sprintf('%d月', $month);
            $topHeaderArr[] = array('data' => $monthName);
        }

        $cDataCount = array();
        $lDataCount = array();
        foreach ($plantArr as $plantKey => $plantVal) {
            $facilityId = $plantVal['facility_id'];
            $leftSideArr[$plantKey]['current'] = array(
                0 =>array('data' => $plantVal['facility_name'], 'attr' => array($rowspan_3), 'style' => array('bgColor' => $bgColor_blue))
              , 1 =>array('data' => $currentYear.'年度')
            );
            $leftSideArr['amg_average']['current'] = array(
                0 =>array('data' => 'AMG平均', 'attr' => array($rowspan_3), 'style' => array('bgColor' => $bgColor_blue))
              , 1 =>array('data' => $currentYear.'年度')
            );
            $leftSideArr[$plantKey]['last'][] = array('data' => $lastYear.'年度');
            $leftSideArr[$plantKey]['comp'][] = array('data' => '増減');

            $cSideAverage = array();
            $lSideAverage = array();

            for ($m=0; $m<12; $m++) {
                $month = (($startMonth + $m) > 12)? ($startMonth + $m) - 12 : ($startMonth+$m);
                $cYear = (($startMonth + $m) > 12)? $currentYear + 1 : $currentYear;
                $lYear = (($startMonth + $m) > 12)? $lastYear + 1 : $lastYear;

                $cStatus = $this->checkCellDataStatus('current', array('year' => $cYear, 'main_data' => array('month' => $month)));
                $lStatus = $this->checkCellDataStatus('last',    array('year' => $lYear, 'main_data' => array('month' => $month)));

                $mainDataArr[$plantKey]['current'][$m] = array('data' => 0, 'status' => $cStatus, 'disp_type' => 'float');
                $mainDataArr[$plantKey]['last'][$m]    = array('data' => 0, 'status' => $lStatus, 'disp_type' => 'float');
                $mainDataArr[$plantKey]['comp'][$m]    = array(
                    'data'      => 0
                  , 'status'    => $this->checkCellDataStatus('comp', array($cStatus, $lStatus))
                  , 'disp_type' => 'float'
                );

                foreach ($dataArr as $dataVal) {
                    if ($facilityId == $dataVal['facility_id'] && $month == $dataVal['month']) {
                        if ($this->getArrValueByKey($cYear.'_gokei', $dataVal)) {
                            $cStatus = $this->checkCellDataStatus('current', array('year' => $cYear, 'main_data' => $dataVal));
                            $mainDataArr[$plantKey]['current'][$m] = array(
                                'data' => $this->getUtilizationRates($dataVal[$cYear.'_gokei'], $dataVal[$cYear.'_act'], 1)
                              , 'status' => $cStatus
                              , 'disp_type' => 'float'
                            );
                            $cSideAverage['data'][] = $mainDataArr[$plantKey]['current'][$m]['data'];
                            $cDataCount[$m] = $cDataCount[$m] + 1;
                        }
                        if ($this->getArrValueByKey($lYear.'_gokei', $dataVal)) {
                            $lStatus = $this->checkCellDataStatus('last', array('year' => $lYear, 'main_data' => $dataVal));
                            $mainDataArr[$plantKey]['last'][$m] = array(
                                'data' => $this->getUtilizationRates($dataVal[$lYear.'_gokei'], $dataVal[$lYear.'_act'], 1)
                              , 'status' => $lStatus
                              , 'disp_type' => 'float'
                            );
                            $lSideAverage['data'][] = $mainDataArr[$plantKey]['last'][$m]['data'];
                            $lDataCount[$m] = $lDataCount[$m] + 1;
                        }

                        $mainDataArr[$plantKey]['comp'][$m] = array(
                            'data'      => $this->getNumberOfEmptyFloors($mainDataArr[$plantKey]['last'][$m]['data'], $mainDataArr[$plantKey]['current'][$m]['data'], 1),
                            'status'    => $this->checkCellDataStatus('comp', array($mainDataArr[$plantKey]['last'][$m]['status'], $mainDataArr[$plantKey]['current'][$m]['status'])),
                            'disp_type' => 'float'
                        );
                    }
                }

                $cSideAverage['status'][] = $mainDataArr[$plantKey]['current'][$m]['status'];
                $lSideAverage['status'][] = $mainDataArr[$plantKey]['last'][$m]['status'];
            }

            $mainDataArr[$plantKey]['current'][12] = array(
                'data'      => round($this->getAddArrayValue($cSideAverage['data']) / count($cSideAverage['data']), 1),
                'status'    => $this->checkCellDataStatus('comp', $cSideAverage['status']),
                'disp_type' => 'float'
            );
            $mainDataArr[$plantKey]['last'][12] = array(
                'data'      => round($this->getAddArrayValue($lSideAverage['data']) / count($lSideAverage['data']), 1),
                'status'    => $this->checkCellDataStatus('comp', $lSideAverage['status']),
                'disp_type' => 'float'
            );
            $mainDataArr[$plantKey]['comp'][12] = array(
                'data'      => $this->getNumberOfEmptyFloors($mainDataArr[$plantKey]['last'][12]['data'], $mainDataArr[$plantKey]['current'][12]['data'], 1),
                'status'    => $this->checkCellDataStatus('comp', array($mainDataArr[$plantKey]['last'][12]['status'], $mainDataArr[$plantKey]['current'][12]['status'])),
                'disp_type' => 'float'
            );
        }

        if (!is_null($total))   { $topHeaderArr[] = array('data' => $total); }
        if (!is_null($average)) { $topHeaderArr[] = array('data' => $average); }

        $rtnStr .= '<table class="list" width="100%" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)).'</tr>';

        for ($r=0; $r < count($leftSideArr); $r++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSideArr[$r]['current'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($mainDataArr[$r]['current'], $fontTag, array('textAlign' => $textAlign_right), true)
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[$r]['last'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($mainDataArr[$r]['last'], $fontTag, array('textAlign' => $textAlign_right), true)
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[$r]['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center))
                          .$this->convertCellData($mainDataArr[$r]['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_right), true)
                      .'</tr>';
        }


        if (!is_null($amgAverage)) {

            $amgAvrgArr = array();
            foreach ($mainDataArr as $mainDataVal) {
                for ($m=0; $m<12; $m++) {
                    $amgAvrgArr['current'][$m]['data']   = $amgAvrgArr['current'][$m]['data'] + $mainDataVal['current'][$m]['data'];
                    $amgAvrgArr['last'][$m]['data']      = $amgAvrgArr['last'][$m]['data']    + $mainDataVal['last'][$m]['data'];
                    $amgAvrgArr['current'][$m]['status'] = $this->checkCellDataStatus('comp', array($amgAvrgArr['current'][$m]['status'], $mainDataVal['current'][$m]['status']));
                    $amgAvrgArr['last'][$m]['status']    = $this->checkCellDataStatus('comp', array($amgAvrgArr['last'][$m]['status'], $mainDataVal['last'][$m]['status']));

                    $amgAvrgArr['current'][$m]['disp_type'] = 'float';
                    $amgAvrgArr['last'][$m]['disp_type']    = 'float';
                }
            }

            $cAvrgTotal     = array();
            $cAvrgDataCount = array();
            $lAvrgTotal     = array();
            $lAvrgDataCount = array();

            for ($m=0; $m<12; $m++) {
                $amgAvrgArr['current'][$m]['data'] = round($amgAvrgArr['current'][$m]['data'] / $cDataCount[$m], 1);
                $amgAvrgArr['last'][$m]['data']    = round($amgAvrgArr['last'][$m]['data'] / $lDataCount[$m], 1);
                $amgAvrgArr['comp'][$m]['data']    = $this->getNumberOfEmptyFloors($amgAvrgArr['last'][$m]['data'], $amgAvrgArr['current'][$m]['data'], 1);

                $amgAvrgArr['comp'][$m]['disp_type'] = 'float';

                $cAvrgTotal[$m] = $amgAvrgArr['current'][$m]['data'];
                $lAvrgTotal[$m] = $amgAvrgArr['last'][$m]['data'];
                if ($cDataCount[$m] > 0) { $cAvrgDataCount[$m] = $cAvrgDataCount[$m] + 1; }
                if ($lDataCount[$m] > 0) { $lAvrgDataCount[$m] = $lAvrgDataCount[$m] + 1; }

                $amgAvrgArr['comp'][$m]['status']    = $this->checkCellDataStatus('comp', array($amgAvrgArr['current'][$m]['status'], $amgAvrgArr['last'][$m]['status']));
                $amgAvrgArr['current'][12]['status'] = $this->checkCellDataStatus('comp', array($amgAvrgArr['current'][12]['status'], $amgAvrgArr['current'][$m]['status']));
                $amgAvrgArr['last'][12]['status']    = $this->checkCellDataStatus('comp', array($amgAvrgArr['last'][12]['status'],    $amgAvrgArr['last'][$m]['status']));
            }

            $amgAvrgArr['current'][12]['data'] = round($this->getAddArrayValue($cAvrgTotal) / count($cAvrgDataCount), 1);
            $amgAvrgArr['last'][12]['data']    = round($this->getAddArrayValue($lAvrgTotal) / count($lAvrgDataCount), 1);
            $amgAvrgArr['comp'][12]['data']    = $this->getNumberOfEmptyFloors($amgAvrgArr['last'][12]['data'], $amgAvrgArr['current'][12]['data'], 1);
            $amgAvrgArr['comp'][12]['status']  = $this->checkCellDataStatus('comp', array($amgAvrgArr['current'][12]['status'], $amgAvrgArr['last'][12]['status']));

            $amgAvrgArr['current'][12]['disp_type'] = 'float';
            $amgAvrgArr['last'][12]['disp_type']    = 'float';
            $amgAvrgArr['comp'][12]['disp_type']    = 'float';

            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSideArr['amg_average']['current'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($amgAvrgArr['current'], $fontTag, array('textAlign' => $textAlign_right), true)
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[0]['last'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($amgAvrgArr['last'], $fontTag, array('textAlign' => $textAlign_right), true)
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[0]['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center))
                          .$this->convertCellData($amgAvrgArr['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_right), true)
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.63 [病院] 病院別患者日報(シンプル版) のデータ出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function getDailySimpleReportAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr       = '';
        $totalArr     = array();
        $totalDataArr = array();
        $totalStatus  = 3;

        $year    = $this->getArrayValueByKeyForEmptyIsNull('year',  $dateArr);
        $month   = $this->getArrayValueByKeyForEmptyIsNull('month', $dateArr);
        $day     = $this->getArrayValueByKeyForEmptyIsNull('day',   $dateArr);
        if ($day > $this->getLastDay($year, $month)) { $day = 1; }

        # timestamp
        $date    = mktime(0, 0, 0, $month, $day, $year);

        # font tag
        $fontTag = $this->getFontTag();

        $bgColorBlue = $this->getRgbColor('blue', 'background-color');

        # attr
        $rowspan_2        = array('name' => 'rowspan',          'value' => 2);
        $colspan_2        = array('name' => 'colspan',          'value' => 2);
        $colspan_6        = array('name' => 'colspan',          'value' => 6);
        # style
        $bgColor_blue     = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right  = array('name' => 'text-align',       'value' => 'right');

        $header = '<table class="list" width="100%">'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '病院名',           'attr' => array($rowspan_2))
                                 , 1 => array('data' => '外来<br />患者数', 'attr' => array($rowspan_2))
                                 , 2 => array('data' => '入院合計',         'attr' => array($colspan_6))
                                 , 3 => array('data' => '入退院数',         'attr' => array($colspan_2))
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)
                         )
                     .'</tr>'
                     .'<tr>'
                          .$this->convertCellData(
                             array(0 => array('data' => '患者数')
                                 , 1 => array('data' => '空床数')
                                 , 2 => array('data' => '稼働病床<br />稼働率', 'attr' => array($colspan_2))
                                 , 3 => array('data' => '許可病床<br />稼働率', 'attr' => array($colspan_2))
                                 , 4 => array('data' => '入院')
                                 , 5 => array('data' => '退院')
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)
                         )
                     .'</tr>';

        $rtnStr .= $header;

        foreach ($plantArr as $plantVal) {
            $tdArr = array();
            $tdArr[] = $this->convertCellData(array(0 => array('data' => $plantVal['facility_name'])), $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            foreach ($dataArr as $dataVal) {
                $dataVal['year']  = $year;
                $dataVal['month'] = $month;
                $dataVal['day']   = $day;

                if ($this->getArrValueByKey('facility_id', $plantVal) == $this->getArrValueByKey('facility_id', $dataVal)) {
                    $status      = $this->checkCellDataStatus('daily_report', $dataVal);
                    $totalStatus = ($totalStatus > $status)? $status : $totalStatus;

                    if ($status == 1) {
                        $nullArr = array();
                        for ($i=0; $i <= 8; $i++) { $nullArr[$i] = array('status' => 1, 'date' => '&nbsp;'); }
                        $tdArr[] = $this->convertCellData($nullArr, '%s', array());
                    }
                    else{
                        // 入院合計：稼働病床稼働率
                        $gokeiGa      = $dataVal['gokei_ga'];
                        $patientCount = $dataVal['patient_count'];
                        $patientCount = is_null($patientCount)? 0 : $patientCount;
                        $actCount     = $dataVal['act_count'];
                        $pmtCount     = $dataVal['pmt_count'];

                        $emptyBedCount = ($actCount - $patientCount);
                        $movement_sickbed_utilization_rates   = $this->getUtilizationRates($patientCount, $actCount, 1);
                        $permission_sickbed_utilization_rates = $this->getUtilizationRates($patientCount, $pmtCount, 1);

                        $tdArr[] = $this->convertCellData(
                            array(
                                0 => array('status' => $status, 'data' => $gokeiGa),                                          # 外来患者数
                                1 => array('status' => $status, 'data' => $patientCount),                                     # 入院合計：患者数
                                2 => array('status' => $status, 'data' => $emptyBedCount),                                    # 入院合計：空床数
                                3 => array(
                                    'status' => $status,
                                    'data'   => $this->getUtilizationRatesTrigona($movement_sickbed_utilization_rates, 90),   # 入院合計：稼働病床稼働率(▲)
                                    'style'  => array('textAlign' => array('name' => 'text-align', 'value' => 'center')),
                                ),
                                4 => array(
                                    'status'    => $status,
                                    'data'      => $movement_sickbed_utilization_rates,                                       # 入院合計：稼働病床稼働率
                                    'disp_type' => 'float_percent'
                                ),
                                5 => array(
                                    'status' => $status,
                                    'data'   => $this->getUtilizationRatesTrigona($permission_sickbed_utilization_rates, 90), # 入院合計：許可病床稼働率(▲)
                                    'style'  => array('textAlign' => array('name' => 'text-align', 'value' => 'center'))
                                ),
                                6 => array(
                                    'status'    => $status,
                                    'data'      => $permission_sickbed_utilization_rates,                                     # 入院合計：許可病床稼働率
                                    'disp_type' => 'float_percent'
                                ),
                                7 => array('status' => $status, 'data' => $dataVal['nyuin']),                                 # 入退院数：入院
                                8 => array('status' => $status, 'data' => $dataVal['taiin']),                                 # 入退院数：退院
                            ),
                            $fontTag,
                            array('textAlign' => $textAlign_right)
                        );

                        $totalDataArr['gokei_ga']        = bcadd($totalDataArr['gokei_ga'],        $gokeiGa);
                        $totalDataArr['patient_count']   = bcadd($totalDataArr['patient_count'],   $patientCount);
                        $totalDataArr['empty_bed_count'] = bcadd($totalDataArr['empty_bed_count'], $emptyBedCount);
                        $totalDataArr['pmt_count']       = bcadd($totalDataArr['pmt_count'],       $pmtCount);
                        $totalDataArr['act_count']       = bcadd($totalDataArr['act_count'],       $actCount);
                        $totalDataArr['nyuin']           = bcadd($totalDataArr['nyuin'],           $dataVal['nyuin']);
                        $totalDataArr['taiin']           = bcadd($totalDataArr['taiin'],           $dataVal['taiin']);
                    }
                }
            }

            // データがない場合
            if (count($tdArr) != '2') {
                $nullArr     = array();
                $status      = $this->checkCellDataStatus('daily_report', array('year' => $year, 'month' => $month, 'day' => $day));
//                $status      = ($date < mktime(0, 0, 0, date('m'), date('d'), date('Y')))? 1 : 3;
                $totalStatus = ($totalStatus > $status)? $status : $totalStatus;
                for ($i=0; $i <= 8; $i++) { $nullArr[$i] = array('status' => $status, 'date' => '&nbsp;'); }
                $tdArr[] = $this->convertCellData($nullArr, $fontTag, array());
            }
            $rtnStr .= '<tr>'. implode('', $tdArr).'</tr>';
        }

        // 合計
        $totalArr[] = $this->convertCellData(array(0 => array('status' => 3, 'data' => '合計')), '%s', array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));

        $totalGokeiGa       = $totalDataArr['gokei_ga'];
        $totalPatientCount  = $totalDataArr['patient_count'];
        $totalPmtCount      = $this->getArrayValueByKeyForEmptyIsNull('pmt_count', $totalDataArr);
        $totalActCount      = $this->getArrayValueByKeyForEmptyIsNull('act_count', $totalDataArr);

        $totalEmptyBedCount = is_null($totalActCount)? '' : $this->getNumberOfEmptyFloors($totalPatientCount, $totalActCount);

        $total_movement_sickbed_utilization_rates = is_null($totalActCount)? null : $this->getUtilizationRates($totalPatientCount, $totalActCount, 1);
        if (is_null($total_movement_sickbed_utilization_rates)) {
            $total_movement_sickbed_utilization_rates_trigona = '&nbsp;';
            $total_movement_sickbed_utilization_rates         = '&nbsp;';
        }
        else {
            $total_movement_sickbed_utilization_rates_trigona = $this->getUtilizationRatesTrigona($total_movement_sickbed_utilization_rates, 90);
            $total_movement_sickbed_utilization_rates         = $total_movement_sickbed_utilization_rates.'%';
        }

        $total_permission_sickbed_utilization_rates = is_null($totalPmtCount)? null : $this->getUtilizationRates($totalPatientCount, $totalPmtCount, 1);
        if (is_null($total_permission_sickbed_utilization_rates)) {
            $total_permission_sickbed_utilization_rates_trigona = '&nbsp;';
            $total_permission_sickbed_utilization_rates         = '&nbsp;';
        }
        else {
            $total_permission_sickbed_utilization_rates_trigona = $this->getUtilizationRatesTrigona($total_permission_sickbed_utilization_rates, 90);
            $total_permission_sickbed_utilization_rates         = $total_permission_sickbed_utilization_rates.'%';
        }

        $totalArr[] = $this->convertCellData(
            array(0 => array('status' => $totalStatus, 'data' => $totalGokeiGa)
                , 1 => array('status' => $totalStatus, 'data' => $totalPatientCount)
                , 2 => array('status' => $totalStatus, 'data' => $totalEmptyBedCount)
                , 3 => array('status' => $totalStatus, 'data' => $total_movement_sickbed_utilization_rates_trigona, 'style' => array('textAlign' => $textAlign_center))
                , 4 => array('status' => $totalStatus, 'data' => $total_movement_sickbed_utilization_rates, 'disp_type' => 'float_percent')
                , 5 => array('status' => $totalStatus, 'data' => $total_permission_sickbed_utilization_rates_trigona, 'style' => array('textAlign' => $textAlign_center))
                , 6 => array('status' => $totalStatus, 'data' => $total_permission_sickbed_utilization_rates, 'disp_type' => 'float_percent')
                , 7 => array('status' => $totalStatus, 'data' => $totalDataArr['nyuin'])
                , 8 => array('status' => $totalStatus, 'data' => $totalDataArr['taiin'])
            )
          , $fontTag
          , array('textAlign' => $textAlign_right)
        );

        $rtnStr .= '<tr>'.implode('', $totalArr).'</tr></table>';
        return $rtnStr;
    }

    /**
     * No.62 [病院] 病院別患者日報のデータ出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function getDailyReportAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr       = '';
        $totalArr     = array();
        $totalDataArr = array();
        $totalStatus  = 3;

        $year  = $this->getArrayValueByKeyForEmptyIsNull('year',  $dateArr);
        $month = $this->getArrayValueByKeyForEmptyIsNull('month', $dateArr);
        $day   = $this->getArrayValueByKeyForEmptyIsNull('day',   $dateArr);
        if ($day > $this->getLastDay($year, $month)) { $day = 1; }
        $date  = date('Ymd', mktime(0, 0, 0, $month, $day, $year));

        $fontTag     = $this->getFontTag();
        $bgColorBlue = $this->getRgbColor('blue', 'background-color');

        # attr
        $rowspan_2         = array('name' => 'rowspan',          'value' =>  2);
        $rowspan_3         = array('name' => 'rowspan',          'value' =>  3);
        $colspan_2         = array('name' => 'colspan',          'value' =>  2);
        $colspan_3         = array('name' => 'colspan',          'value' =>  3);
        $colspan_6         = array('name' => 'colspan',          'value' =>  6);
        $colspan_9         = array('name' => 'colspan',          'value' =>  9);
        $colspan_24        = array('name' => 'colspan',          'value' => 24);
        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $header = '<table class="list" width="100%">'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '病院名',           'attr' => array($rowspan_3))
                                 , 1 => array('data' => '外来<br />患者数', 'attr' => array($rowspan_3))
                                 , 2 => array('data' => '入院(一般)',       'attr' => array($colspan_9))
                                 , 3 => array('data' => '入院(療養)',       'attr' => array($colspan_6))
                                 , 4 => array('data' => '入院(特定)',       'attr' => array($colspan_24))
                                 , 5 => array('data' => '入院合計',         'attr' => array($rowspan_2, $colspan_6))
                                 , 6 => array('data' => '入退院数',         'attr' => array($rowspan_2, $colspan_2)),
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '一般病棟',       'attr' => array($colspan_3))
                                 , 1 => array('data' => '障害者病棟',     'attr' => array($colspan_3))
                                 , 2 => array('data' => '精神一般病棟',   'attr' => array($colspan_3))
                                 , 3 => array('data' => '医療療養病棟',   'attr' => array($colspan_3))
                                 , 4 => array('data' => '介護療養病棟',   'attr' => array($colspan_3))
                                 , 5 => array('data' => '回復期リハ病棟', 'attr' => array($colspan_3))
                                 , 6 => array('data' => '亜急性期病床',   'attr' => array($colspan_3))
                                 , 7 => array('data' => '緩和ケア病棟',   'attr' => array($colspan_3))
                                 , 8 => array('data' => 'ICU及びハイケアユニット',      'attr' => array($colspan_3))
                                 , 9 => array('data' => '小児管理料',  'attr' => array($colspan_3))
                                , 10 => array('data' => '精神療養病棟',   'attr' => array($colspan_3))
                                , 11 => array('data' => '特殊疾患(2)',    'attr' => array($colspan_3))
                                , 12 => array('data' => '認知症病棟',  'attr' => array($colspan_3)),
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '患者数'),  1 => array('data' => '空床数'),  2 => array('data' => '稼働率')
                                 , 3 => array('data' => '患者数'),  4 => array('data' => '空床数'),  5 => array('data' => '稼働率')
                                 , 6 => array('data' => '患者数'),  7 => array('data' => '空床数'),  8 => array('data' => '稼働率')
                                 , 9 => array('data' => '患者数'), 10 => array('data' => '空床数'), 11 => array('data' => '稼働率')
                                , 12 => array('data' => '患者数'), 13 => array('data' => '空床数'), 14 => array('data' => '稼働率')
                                , 15 => array('data' => '患者数'), 16 => array('data' => '空床数'), 17 => array('data' => '稼働率')
                                , 18 => array('data' => '患者数'), 19 => array('data' => '空床数'), 20 => array('data' => '稼働率')
                                , 21 => array('data' => '患者数'), 22 => array('data' => '空床数'), 23 => array('data' => '稼働率')
                                , 24 => array('data' => '患者数'), 25 => array('data' => '空床数'), 26 => array('data' => '稼働率')
                                , 27 => array('data' => '患者数'), 28 => array('data' => '空床数'), 29 => array('data' => '稼働率')
                                , 30 => array('data' => '患者数'), 31 => array('data' => '空床数'), 32 => array('data' => '稼働率')
                                , 33 => array('data' => '患者数'), 34 => array('data' => '空床数'), 35 => array('data' => '稼働率')
                                , 36 => array('data' => '患者数'), 37 => array('data' => '空床数'), 38 => array('data' => '稼働率')
                                , 39 => array('data' => '患者数'), 40 => array('data' => '空床数')
                                , 41 => array('data' => '稼働病床<br />稼働率', 'attr' => array($colspan_2))
                                , 43 => array('data' => '許可病床<br />稼働率', 'attr' => array($colspan_2))
                                , 44 => array('data' => '入院'), 45 => array('data' => '退院')
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';

        $rtnStr .= $header;

        $categoryArr = array(0 => 'ip', 1 => 'shog', 2 => 'seip', 3 => 'ir', 4 => 'kaig', 5 => 'kaif', 6 => 'ak', 7 => 'kan', 8 => 'icu', 9 => 'shoni', 10 => 'seir', 11 => 'tok', 12 => 'nin');

        foreach ($plantArr as $plantVal) {
            $tdArr = array();
            $tdArr[] = sprintf('<td style="text-align: center; white-space: nowrap; %s" >%s</td>', $bgColorBlue, sprintf($fontTag, $this->getArrValueByKey('facility_name', $plantVal)));

            foreach ($dataArr as $dataVal) {
                /* add 20100409 */
                $dataVal['year']  = $year;
                $dataVal['month'] = $month;
                $dataVal['day']   = $day;
                /* add 20100409 */

                if ($this->getArrValueByKey('facility_id', $plantVal) == $this->getArrValueByKey('facility_id', $dataVal)) {
                    $status      = $this->checkCellDataStatus('daily_report', $dataVal);
                    $totalStatus = ($totalStatus > $status)? $status : $totalStatus;
                    if ($status == 1) {
                        $nullArr = array();
                        for ($i=0; $i <= 47; $i++) { $nullArr[$i] = array('status' => 1, 'date' => '&nbsp;'); }
                        $tdArr[] = $this->convertCellData($nullArr, '%s', array());
                    }
                    else{
                        $gokeiGa      = $dataVal['gokei_ga'];
                        $patientCount = $dataVal['patient_count'];
                        $actCount     = $dataVal['act_count'];
                        $pmtCount     = $dataVal['pmt_count'];

                        $paramArr = array();
                        foreach ($categoryArr as $val) {
                            $paramArr['gokei_'.$val] = $this->getArrValueByKey('gokei_'.$val, $dataVal);
                            $paramArr['gokei_'.$val] = is_null($paramArr['gokei_'.$val])? 0 : $paramArr['gokei_'.$val];
                            $paramArr[$val.'_act']   = $this->getArrValueByKey($val.'_act', $dataVal);
                            $paramArr[$val.'_act']   = is_null($paramArr[$val.'_act'])? 0 : $paramArr[$val.'_act'];
                            $paramArr[$val.'_permission_sickbed_count'] = ($paramArr[$val.'_act'] - $paramArr['gokei_'.$val]);
                            $paramArr[$val.'_rates'] = empty($paramArr[$val.'_act'])? 0 : $this->getUtilizationRates($paramArr['gokei_'.$val], $paramArr[$val.'_act'], 1).'%';
                        }

                        $emptyBedCount = ($actCount - $patientCount);
                        $movement_sickbed_utilization_rates   = $this->getUtilizationRates($patientCount, $actCount, 1);
                        $permission_sickbed_utilization_rates = $this->getUtilizationRates($patientCount, $pmtCount, 1);

                        $tdArr[] = $this->convertCellData(
                            array(
                                 # 外来患者数
                                 0 => array('status' => $status, 'data' => $gokeiGa),

                                 # 入院(一般) 一般病棟
                                 1 => array('status' => $status, 'data' => $paramArr['gokei_ip']),                                                 # 入院(一般) 一般病棟      患者数
                                 2 => array('status' => $status, 'data' => $paramArr['ip_permission_sickbed_count']),                              # 入院(一般) 一般病棟      空床数
                                 3 => array('status' => $status, 'data' => $paramArr['ip_rates'], 'disp_type' => 'float_percent'),                 # 入院(一般) 一般病棟      稼働率

                                 # 入院(一般) 障害者病棟
                                 4 => array('status' => $status, 'data' => $paramArr['gokei_shog']),                                               # 入院(一般) 障害者病棟    患者数
                                 5 => array('status' => $status, 'data' => $paramArr['shog_permission_sickbed_count']),                            # 入院(一般) 障害者病棟    空床数
                                 6 => array('status' => $status, 'data' => $paramArr['shog_rates'], 'disp_type' => 'float_percent'),               # 入院(一般) 障害者病棟    患者数

                                 # 入院(一般) 精神一般病棟
                                 7 => array('status' => $status, 'data' => $paramArr['gokei_seip']),                                               # 入院(一般) 精神一般病棟  患者数
                                 8 => array('status' => $status, 'data' => $paramArr['seip_permission_sickbed_count']),                            # 入院(一般) 精神一般病棟  空床数
                                 9 => array('status' => $status, 'data' => $paramArr['seip_rates'], 'disp_type' => 'float_percent'),               # 入院(一般) 精神一般病棟  稼働率

                                # 入院(療養) 医療療養病棟
                                10 => array('status' => $status, 'data' => $paramArr['gokei_ir']),                                                 # 入院(療養) 医療療養病棟  患者数
                                11 => array('status' => $status, 'data' => $paramArr['ir_permission_sickbed_count']),                              # 入院(療養) 医療療養病棟  空床数
                                12 => array('status' => $status, 'data' => $paramArr['ir_rates'], 'disp_type' => 'float_percent'),                 # 入院(療養) 医療療養病棟  稼働率

                                # 入院(療養) 介護療養病棟
                                13 => array('status' => $status, 'data' => $paramArr['gokei_kaig']),                                               # 入院(療養) 介護療養病棟  患者数
                                14 => array('status' => $status, 'data' => $paramArr['kaig_permission_sickbed_count']),                            # 入院(療養) 介護療養病棟  空床数
                                15 => array('status' => $status, 'data' => $paramArr['kaig_rates'], 'disp_type' => 'float_percent'),               # 入院(療養) 介護療養病棟  稼働率

                                # 入院(特定) 回復期ﾘﾊ病棟
                                16 => array('status' => $status, 'data' => $paramArr['gokei_kaif']),                                               # 入院(特定) 回復期ﾘﾊ病棟  患者数
                                17 => array('status' => $status, 'data' => $paramArr['kaif_permission_sickbed_count']),                            # 入院(特定) 回復期ﾘﾊ病棟  空床数
                                18 => array('status' => $status, 'data' => $paramArr['kaif_rates'], 'disp_type' => 'float_percent'),               # 入院(特定) 回復期ﾘﾊ病棟  稼働率

                                # 入院(特定) 亜急性期病床
                                19 => array('status' => $status, 'data' => $paramArr['gokei_ak']),                                                 # 入院(特定) 亜急性期病床  患者数
                                20 => array('status' => $status, 'data' => $paramArr['ak_permission_sickbed_count']),                              # 入院(特定) 亜急性期病床  空床数
                                21 => array('status' => $status, 'data' => $paramArr['ak_rates'], 'disp_type' => 'float_percent'),                 # 入院(特定) 亜急性期病床  稼働率

                                # 入院(特定) 緩和ｹｱ病棟
                                22 => array('status' => $status, 'data' => $paramArr['gokei_kan']),                                                # 入院(特定) 緩和ｹｱ病棟    患者数
                                23 => array('status' => $status, 'data' => $paramArr['kan_permission_sickbed_count']),                             # 入院(特定) 緩和ｹｱ病棟    空床数
                                24 => array('status' => $status, 'data' => $paramArr['kan_rates'], 'disp_type' => 'float_percent'),                # 入院(特定) 緩和ｹｱ病棟    稼働率

                                # 入院(特定) ICU及びハイケアユニット
                                25 => array('status' => $status, 'data' => $paramArr['gokei_icu']),                                                # 入院(特定) ICU及びハイケアユニット     患者数
                                26 => array('status' => $status, 'data' => $paramArr['icu_permission_sickbed_count']),                             # 入院(特定) ICU及びハイケアユニット     空床数
                                26 => array('status' => $status, 'data' => $paramArr['icu_permission_sickbed_count']),                             # 入院(特定) ICU及びハイケアユニット     空床数
                                27 => array('status' => $status, 'data' => $paramArr['icu_rates'], 'disp_type' => 'float_percent'),                # 入院(特定) ICU及びハイケアユニット     稼働率

                                # 入院(特定) 小児管理料
                                28 => array('status' => $status, 'data' => $paramArr['gokei_shoni']),                                              # 入院(特定) 小児管理料 患者数
                                29 => array('status' => $status, 'data' => $paramArr['shoni_permission_sickbed_count']),                           # 入院(特定) 小児管理料 空床数
                                30 => array('status' => $status, 'data' => $paramArr['shoni_rates'], 'disp_type' => 'float_percent'),              # 入院(特定) 小児管理料 稼働率

                                # 入院(特定) 精神療養病棟
                                31 => array('status' => $status, 'data' => $paramArr['gokei_seir']),                                               # 入院(特定) 精神療養病棟  患者数
                                32 => array('status' => $status, 'data' => $paramArr['seir_permission_sickbed_count']),                            # 入院(特定) 精神療養病棟  空床数
                                33 => array('status' => $status, 'data' => $paramArr['seir_rates'], 'disp_type' => 'float_percent'),               # 入院(特定) 精神療養病棟  稼働率

                                # 入院(特定) 特殊疾患(2)
                                34 => array('status' => $status, 'data' => $paramArr['gokei_tok']),                                                # 入院(特定) 特殊疾患(2)   患者数
                                35 => array('status' => $status, 'data' => $paramArr['tok_permission_sickbed_count']),                             # 入院(特定) 特殊疾患(2)   空床数
                                36 => array('status' => $status, 'data' => $paramArr['tok_rates'], 'disp_type' => 'float_percent'),                # 入院(特定) 特殊疾患(2)   稼働率

                                # 入院(特定) 認知症病棟
                                37 => array('status' => $status, 'data' => $paramArr['gokei_nin']),                                                # 入院(特定) 認知症病棟 患者数
                                38 => array('status' => $status, 'data' => $paramArr['nin_permission_sickbed_count']),                             # 入院(特定) 認知症病棟 空床数
                                39 => array('status' => $status, 'data' => $paramArr['nin_rates'], 'disp_type' => 'float_percent'),                # 入院(特定) 認知症病棟 稼働率

                                40 => array('status' => $status, 'data' => $patientCount),                                                         # 入院合計   患者数
                                41 => array('status' => $status, 'data' => $emptyBedCount),                                                        # 入院合計   空床数
                                42 => array(
                                    'status' => $status,
                                    'data' => ($movement_sickbed_utilization_rates < 90)? '▲' : '&nbsp;',                                         # 入院合計   稼働病床稼働率(▲)
                                    'style' => array('textAlign' => $textAlign_center)
                                ),
                                43 => array('status' => $status, 'data' => $movement_sickbed_utilization_rates, 'disp_type' => 'float_percent'),   # 入院合計   稼働病床稼働率
                                44 => array(
                                    'status' => $status,
                                    'data' => ($permission_sickbed_utilization_rates < 90)? '▲' : '&nbsp;',                                       # 入院合計   許可病床稼働率(▲)
                                    'style' => array('textAlign' => $textAlign_center)
                                ),
                                45 => array('status' => $status, 'data' => $permission_sickbed_utilization_rates, 'disp_type' => 'float_percent'), # 入院合計   許可病床稼働率
                                46 => array('status' => $status, 'data' => $dataVal['nyuin']),                                                     # 入退院数   入院
                                47 => array('status' => $status, 'data' => $dataVal['taiin']),                                                     # 入退院数   退院
                            ),
                            $fontTag,
                            array('textAlign' => $textAlign_right)
                        );

                        $totalDataArr['gokei_ga']        = bcadd($totalDataArr['gokei_ga'],        $gokeiGa,                 1);
                        $totalDataArr['act_count']       = bcadd($totalDataArr['act_count'],       $actCount,                1);
                        $totalDataArr['pmt_count']       = bcadd($totalDataArr['pmt_count'],       $pmtCount,                1);
                        foreach ($categoryArr as $val) {
                            $totalDataArr['gokei_'.$val] = bcadd($totalDataArr['gokei_'.$val],     $paramArr['gokei_'.$val], 1);
                            $totalDataArr[$val.'_act']   = bcadd($totalDataArr[$val.'_act'],       $paramArr[$val.'_act'],   1);
                        }
                        $totalDataArr['patient_count']   = bcadd($totalDataArr['patient_count'],   $patientCount,            1);
                        $totalDataArr['empty_bed_count'] = bcadd($totalDataArr['empty_bed_count'], $emptyBedCount,           1);
                        $totalDataArr['nyuin']           = bcadd($totalDataArr['nyuin'],           $dataVal['nyuin'],        1);
                        $totalDataArr['taiin']           = bcadd($totalDataArr['taiin'],           $dataVal['taiin'],        1);
                    }
                }
            }

            // データがない場合
            if (count($tdArr) != '2') {
                $nullArr     = array();
                $status      = $this->checkCellDataStatus('daily_report', array('year' => $year, 'month' => $month, 'day' => $day));
//                $status      = ($date <= date('Ymd', mktime(0, 0, 0, date('m'), date('d'), date('Y'))))? 1 : 3;
                $totalStatus = ($totalStatus > $status)? $status : $totalStatus;
                for ($i=0; $i <= 47; $i++) { $nullArr[$i] = array('status' => $status, 'date' => '&nbsp;'); }
                $tdArr[]     = $this->convertCellData($nullArr, '%s', array());
            }
            $rtnStr .= '<tr>'. implode('', $tdArr).'</tr>';
        }

        // 合計
        $totalArr[] = $this->convertCellData(array(0 => array('status' => 3, 'data' => '合計')), '%s', array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));

        $totalGokeiGa      = $totalDataArr['gokei_ga'];
        $totalPatientCount = $this->getArrayValueByKeyForEmptyIsNull('patient_count', $totalDataArr);
        $totalActCount     = $this->getArrayValueByKeyForEmptyIsNull('act_count',     $totalDataArr);
        $totalPmtCount     = $this->getArrayValueByKeyForEmptyIsNull('pmt_count',     $totalDataArr);

        // 空床数
        $totalEmptyBedCount = is_null($totalActCount)? null : $this->getNumberOfEmptyFloors($totalPatientCount, $totalActCount);

        $total_movement_sickbed_utilization_rates = is_null($totalActCount)? null : $this->getUtilizationRates($totalPatientCount, $totalActCount, 1);
        if (is_null($total_movement_sickbed_utilization_rates)) {
            $total_movement_sickbed_utilization_rates_trigona = '&nbsp;';
            $total_movement_sickbed_utilization_rates         = '&nbsp;';
        }
        else {
            $total_movement_sickbed_utilization_rates_trigona = $this->getUtilizationRatesTrigona($total_movement_sickbed_utilization_rates, 90);
            $total_movement_sickbed_utilization_rates        .= '%';
        }

        $total_permission_sickbed_utilization_rates = is_null($totalPmtCount)? null : $this->getUtilizationRates($totalPatientCount, $totalPmtCount, 1);
        if (is_null($total_permission_sickbed_utilization_rates)) {
            $total_permission_sickbed_utilization_rates_trigona = '&nbsp;';
            $total_permission_sickbed_utilization_rates         = '&nbsp;';
        }
        else {
            $total_permission_sickbed_utilization_rates_trigona = $this->getUtilizationRatesTrigona($total_permission_sickbed_utilization_rates, 90);
            $total_permission_sickbed_utilization_rates        .= '%';
        }

        $totalParamArr = array();
        foreach ($categoryArr as $val) {
            $totalParamArr['gokei_'.$val] = $this->getArrValueByKey('gokei_'.$val, $totalDataArr);
            $totalParamArr['gokei_'.$val] = empty($totalParamArr['gokei_'.$val])? null : $totalParamArr['gokei_'.$val];
            $totalParamArr[$val.'_act']   = $this->getArrValueByKey($val.'_act', $totalDataArr);
            $totalParamArr[$val.'_act']   = empty($totalParamArr[$val.'_act'])? null : $totalParamArr[$val.'_act'];
            $totalParamArr[$val.'_movement_sickbed_count']   = $totalParamArr['gokei_'.$val];
            $totalParamArr[$val.'_permission_sickbed_count'] = (!is_null($totalParamArr[$val.'_act']) && !is_null($totalParamArr['gokei_'.$val]))? ($totalParamArr[$val.'_act'] - $totalParamArr['gokei_'.$val]) : '&nbsp;';
            $totalParamArr[$val.'_rates']                    = empty($totalParamArr[$val.'_act'])? null : $this->getUtilizationRates($totalParamArr['gokei_'.$val], $totalParamArr[$val.'_act'], 1).'%';
        }

        $totalArr[] = $this->convertCellData(
            array(0 => array('status' => $totalStatus, 'data' => $totalGokeiGa, 'disp_type' => 'integer')

                , 1 => array('status' => $totalStatus, 'data' => $totalParamArr['ip_movement_sickbed_count'],         'disp_type' => 'integer')
                , 2 => array('status' => $totalStatus, 'data' => $totalParamArr['ip_permission_sickbed_count'],       'disp_type' => 'integer')
                , 3 => array('status' => $totalStatus, 'data' => $totalParamArr['ip_rates'],                          'disp_type' => 'float_percent')

                , 4 => array('status' => $totalStatus, 'data' => $totalParamArr['shog_movement_sickbed_count'],       'disp_type' => 'integer')
                , 5 => array('status' => $totalStatus, 'data' => $totalParamArr['shog_permission_sickbed_count'],     'disp_type' => 'integer')
                , 6 => array('status' => $totalStatus, 'data' => $totalParamArr['shog_rates'],                        'disp_type' => 'float_percent')

                , 7 => array('status' => $totalStatus, 'data' => $totalParamArr['seip_movement_sickbed_count'],       'disp_type' => 'integer')
                , 8 => array('status' => $totalStatus, 'data' => $totalParamArr['seip_permission_sickbed_count'],     'disp_type' => 'integer')
                , 9 => array('status' => $totalStatus, 'data' => $totalParamArr['seip_rates'],                        'disp_type' => 'float_percent')

               , 10 => array('status' => $totalStatus, 'data' => $totalParamArr['ir_movement_sickbed_count'],         'disp_type' => 'integer')
               , 11 => array('status' => $totalStatus, 'data' => $totalParamArr['ir_permission_sickbed_count'],       'disp_type' => 'integer')
               , 12 => array('status' => $totalStatus, 'data' => $totalParamArr['ir_rates'],                          'disp_type' => 'float_percent')

               , 13 => array('status' => $totalStatus, 'data' => $totalParamArr['kaig_movement_sickbed_count'],       'disp_type' => 'integer')
               , 14 => array('status' => $totalStatus, 'data' => $totalParamArr['kaig_permission_sickbed_count'],     'disp_type' => 'integer')
               , 15 => array('status' => $totalStatus, 'data' => $totalParamArr['kaig_rates'],                        'disp_type' => 'float_percent')

               , 16 => array('status' => $totalStatus, 'data' => $totalParamArr['kaif_movement_sickbed_count'],       'disp_type' => 'integer')
               , 17 => array('status' => $totalStatus, 'data' => $totalParamArr['kaif_permission_sickbed_count'],     'disp_type' => 'integer')
               , 18 => array('status' => $totalStatus, 'data' => $totalParamArr['kaif_rates'],                        'disp_type' => 'float_percent')

               , 19 => array('status' => $totalStatus, 'data' => $totalParamArr['ak_movement_sickbed_count'],         'disp_type' => 'integer')
               , 20 => array('status' => $totalStatus, 'data' => $totalParamArr['ak_permission_sickbed_count'],       'disp_type' => 'integer')
               , 21 => array('status' => $totalStatus, 'data' => $totalParamArr['ak_rates'],                          'disp_type' => 'float_percent')

               , 22 => array('status' => $totalStatus, 'data' => $totalParamArr['kan_movement_sickbed_count'],        'disp_type' => 'integer')
               , 23 => array('status' => $totalStatus, 'data' => $totalParamArr['kan_permission_sickbed_count'],      'disp_type' => 'integer')
               , 24 => array('status' => $totalStatus, 'data' => $totalParamArr['kan_rates'],                         'disp_type' => 'float_percent')

               , 25 => array('status' => $totalStatus, 'data' => $totalParamArr['icu_movement_sickbed_count'],        'disp_type' => 'integer')
               , 26 => array('status' => $totalStatus, 'data' => $totalParamArr['icu_permission_sickbed_count'],      'disp_type' => 'integer')
               , 27 => array('status' => $totalStatus, 'data' => $totalParamArr['icu_rates'],                         'disp_type' => 'float_percent')

               , 28 => array('status' => $totalStatus, 'data' => $totalParamArr['shoni_movement_sickbed_count'],      'disp_type' => 'integer')
               , 29 => array('status' => $totalStatus, 'data' => $totalParamArr['shoni_permission_sickbed_count'],    'disp_type' => 'integer')
               , 30 => array('status' => $totalStatus, 'data' => $totalParamArr['shoni_rates'],                       'disp_type' => 'float_percent')

               , 31 => array('status' => $totalStatus, 'data' => $totalParamArr['seir_movement_sickbed_count'],       'disp_type' => 'integer')
               , 32 => array('status' => $totalStatus, 'data' => $totalParamArr['seir_permission_sickbed_count'],     'disp_type' => 'integer')
               , 33 => array('status' => $totalStatus, 'data' => $totalParamArr['seir_rates'],                        'disp_type' => 'float_percent')

               , 34 => array('status' => $totalStatus, 'data' => $totalParamArr['tok_movement_sickbed_count'],        'disp_type' => 'integer')
               , 35 => array('status' => $totalStatus, 'data' => $totalParamArr['tok_permission_sickbed_count'],      'disp_type' => 'integer')
               , 36 => array('status' => $totalStatus, 'data' => $totalParamArr['tok_rates'],                         'disp_type' => 'float_percent')

               , 37 => array('status' => $totalStatus, 'data' => $totalParamArr['nin_movement_sickbed_count'],        'disp_type' => 'integer')
               , 38 => array('status' => $totalStatus, 'data' => $totalParamArr['nin_permission_sickbed_count'],      'disp_type' => 'integer')
               , 39 => array('status' => $totalStatus, 'data' => $totalParamArr['nin_rates'],                         'disp_type' => 'float_percent')

               , 40 => array('status' => $totalStatus, 'data' => $totalPatientCount,                                  'disp_type' => 'integer')
               , 41 => array('status' => $totalStatus, 'data' => $totalEmptyBedCount,                                 'disp_type' => 'integer')
               , 42 => array('status' => $totalStatus, 'data' => $total_movement_sickbed_utilization_rates_trigona,   'style'     => array('textAlign' => $textAlign_center))
               , 43 => array('status' => $totalStatus, 'data' => $total_movement_sickbed_utilization_rates,           'disp_type' => 'float_percent')
               , 44 => array('status' => $totalStatus, 'data' => $total_permission_sickbed_utilization_rates_trigona, 'style'     => array('textAlign' => $textAlign_center))
               , 45 => array('status' => $totalStatus, 'data' => $total_permission_sickbed_utilization_rates,         'disp_type' => 'float_percent')
               , 46 => array('status' => $totalStatus, 'data' => $totalDataArr['nyuin'],                              'disp_type' => 'integer')
               , 47 => array('status' => $totalStatus, 'data' => $totalDataArr['taiin'],                              'disp_type' => 'integer')
            )
          , $fontTag
          , array('textAlign' => $textAlign_right)
        );
        $rtnStr .= '<tr>'.implode('', $totalArr).'</tr></table>';
        return $rtnStr;
    }

    /**
     * 患者月報(日報・日毎)のデータ出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function getMonthlyReportAccordingToDay($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr = "";
        $fontTag = $this->getFontTag();

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_6  = array('name' => 'colspan', 'value' =>  6);
        $colspan_9  = array('name' => 'colspan', 'value' =>  9);
        $colspan_24 = array('name' => 'colspan', 'value' => 24);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $header = '<table class="list" width="100%">'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '日付',             'attr' => array($rowspan_3))
                                 , 1 => array('data' => '外来<br />患者数', 'attr' => array($rowspan_3))
                                 , 2 => array('data' => '入院(一般)',       'attr' => array($colspan_9))
                                 , 3 => array('data' => '入院(療養)',       'attr' => array($colspan_6))
                                 , 4 => array('data' => '入院(特定)',       'attr' => array($colspan_24))
                                 , 5 => array('data' => '入院合計',         'attr' => array($rowspan_2, $colspan_6))
                                 , 6 => array('data' => '退院合計',         'attr' => array($rowspan_2, $colspan_2))
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '一般病棟',       'attr' => array($colspan_3))
                                 , 1 => array('data' => '障害者病棟',     'attr' => array($colspan_3))
                                 , 2 => array('data' => '精神一般病棟',   'attr' => array($colspan_3))
                                 , 3 => array('data' => '医療療養病棟',   'attr' => array($colspan_3))
                                 , 4 => array('data' => '介護療養病棟',   'attr' => array($colspan_3))
                                 , 5 => array('data' => '回復期リハ病棟', 'attr' => array($colspan_3))
                                 , 6 => array('data' => '亜急性期病床',   'attr' => array($colspan_3))
                                 , 7 => array('data' => '緩和ケア病棟',   'attr' => array($colspan_3))
                                 , 8 => array('data' => 'ICU及びハイケアユニット',      'attr' => array($colspan_3))
                                 , 9 => array('data' => '小児管理料',  'attr' => array($colspan_3))
                                , 10 => array('data' => '精神療養病棟',   'attr' => array($colspan_3))
                                , 11 => array('data' => '特殊疾患(2)',    'attr' => array($colspan_3))
                                , 12 => array('data' => '認知症病棟',  'attr' => array($colspan_3))
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '患者数'),  1 => array('data' => '空床数'),  2 => array('data' => '稼働率')
                                 , 3 => array('data' => '患者数'),  4 => array('data' => '空床数'),  5 => array('data' => '稼働率')
                                 , 6 => array('data' => '患者数'),  7 => array('data' => '空床数'),  8 => array('data' => '稼働率')
                                 , 9 => array('data' => '患者数'), 10 => array('data' => '空床数'), 11 => array('data' => '稼働率')
                                , 12 => array('data' => '患者数'), 13 => array('data' => '空床数'), 14 => array('data' => '稼働率')
                                , 15 => array('data' => '患者数'), 16 => array('data' => '空床数'), 17 => array('data' => '稼働率')
                                , 18 => array('data' => '患者数'), 19 => array('data' => '空床数'), 20 => array('data' => '稼働率')
                                , 21 => array('data' => '患者数'), 22 => array('data' => '空床数'), 23 => array('data' => '稼働率')
                                , 24 => array('data' => '患者数'), 25 => array('data' => '空床数'), 26 => array('data' => '稼働率')
                                , 27 => array('data' => '患者数'), 28 => array('data' => '空床数'), 29 => array('data' => '稼働率')
                                , 30 => array('data' => '患者数'), 31 => array('data' => '空床数'), 32 => array('data' => '稼働率')
                                , 33 => array('data' => '患者数'), 34 => array('data' => '空床数'), 35 => array('data' => '稼働率')
                                , 36 => array('data' => '患者数'), 37 => array('data' => '空床数'), 38 => array('data' => '稼働率')
                                , 39 => array('data' => '患者数'), 40 => array('data' => '空床数')
                                , 41 => array('data' => '稼働病床<br />稼働率', 'attr' => array($colspan_2))
                                , 43 => array('data' => '許可病床<br />稼働率', 'attr' => array($colspan_2))
                                , 45 => array('data' => '入院'),   46 => array('data' => '退院')
                             ),
                             $fontTag,
                             array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue)
                         )
                     .'</tr>';
        $rtnStr .= $header;

        $year    = $this->getArrValueByKey('year',  $dateArr);
        $month   = $this->getArrValueByKey('month', $dateArr);
        $lastDay = $this->getLastDay($year, $month);

        $totalStatus = 3;

        $totalGa    = array();
        $totalIp    = array();
        $totalShog  = array();
        $totalSeip  = array();
        $totalIr    = array();
        $totalKaig  = array();
        $totalKaif  = array();
        $totalAk    = array();
        $totalKan   = array();
        $totalIcu   = array();
        $totalShoni = array();
        $totalSeir  = array();
        $totalTok   = array();
        $totalNin   = array();
        $nyuinTotal = array();

        for ($d=1; $d<=$lastDay; $d++) {
            $tdArr    = array();
            $day      = sprintf('%02d', $d);
            $checkFlg = false;

            foreach ($dataArr as $dataVal) {
                if ($day == $this->getArrValueByKey('day', $dataVal)) {
                    $status = $this->checkCellDataStatus('monthly_report', $dataVal);
                    $totalStatus = ($totalStatus > $status)? $status : $totalStatus;

                    $gokeiGa      = $dataVal['gokei_ga'];
                    $gokeiIp      = $dataVal['gokei_ip'];
                    $gokeiShog    = $dataVal['gokei_shog'];
                    $gokeiSeip    = $dataVal['gokei_seip'];
                    $gokeiIr      = $dataVal['gokei_ir'];
                    $gokeiKaig    = $dataVal['gokei_kaig'];
                    $gokeiKaif    = $dataVal['gokei_kaif'];
                    $gokeiAk      = $dataVal['gokei_ak'];
                    $gokeiKan     = $dataVal['gokei_kan'];
                    $gokeiIcu     = $dataVal['gokei_icu'];
                    $gokeiShoni   = $dataVal['gokei_shoni'];
                    $gokeiSeir    = $dataVal['gokei_seir'];
                    $gokeiTok     = $dataVal['gokei_tok'];
                    $gokeiNin     = $dataVal['gokei_nin'];
                    $ipAct        = $dataVal['ip_act'];
                    $shogAct      = $dataVal['shog_act'];
                    $seipAct      = $dataVal['seip_act'];
                    $irAct        = $dataVal['ir_act'];
                    $kaigAct      = $dataVal['kaig_act'];
                    $kaifAct      = $dataVal['kaif_act'];
                    $akAct        = $dataVal['ak_act'];
                    $kanAct       = $dataVal['kan_act'];
                    $icuAct       = $dataVal['icu_act'];
                    $shoniAct     = $dataVal['shoni_act'];
                    $seirAct      = $dataVal['seir_act'];
                    $tokAct       = $dataVal['tok_act'];
                    $ninAct       = $dataVal['nin_act'];
                    $patientCount = $dataVal['patient_count'];
                    $actCount     = $dataVal['act_count'];
                    $pmtCount     = $dataVal['pmt_count'];

                    $movementSickbedUtilizationRates   = $this->getUtilizationRates($patientCount, $actCount, 1);
                    $permissionSickbedUtilizationRates = $this->getUtilizationRates($patientCount, $pmtCount, 1);

                    $nyuin = $dataVal['nyuin'];
                    $taiin = $dataVal['taiin'];

                    $totalGa['gokei']            = $totalGa['gokei']            + $gokeiGa;
                    $totalIp['gokei']            = $totalIp['gokei']            + $gokeiIp;
                    $totalIp['act']              = $totalIp['act']              + $ipAct;
                    $totalShog['gokei']          = $totalShog['gokei']          + $gokeiShog;
                    $totalShog['act']            = $totalShog['act']            + $shogAct;
                    $totalSeip['gokei']          = $totalSeip['gokei']          + $gokeiSeip;
                    $totalSeip['act']            = $totalSeip['act']            + $seipAct;
                    $totalIr['gokei']            = $totalIr['gokei']            + $gokeiIr;
                    $totalIr['act']              = $totalIr['act']              + $irAct;
                    $totalKaig['gokei']          = $totalKaig['gokei']          + $gokeiKaig;
                    $totalKaig['act']            = $totalKaig['act']            + $kaigAct;
                    $totalKaif['gokei']          = $totalKaif['gokei']          + $gokeiKaif;
                    $totalKaif['act']            = $totalKaif['act']            + $kaifAct;
                    $totalAk['gokei']            = $totalAk['gokei']            + $gokeiAk;
                    $totalAk['act']              = $totalAk['act']              + $akAct;
                    $totalKan['gokei']           = $totalKan['gokei']           + $gokeiKan;
                    $totalKan['act']             = $totalKan['act']             + $kanAct;
                    $totalIcu['gokei']           = $totalIcu['gokei']           + $gokeiIcu;
                    $totalIcu['act']             = $totalIcu['act']             + $icuAct;
                    $totalShoni['gokei']         = $totalShoni['gokei']         + $gokeiShoni;
                    $totalShoni['act']           = $totalShoni['act']           + $shoniAct;
                    $totalSeir['gokei']          = $totalSeir['gokei']          + $gokeiSeir;
                    $totalSeir['act']            = $totalSeir['act']            + $seirAct;
                    $totalTok['gokei']           = $totalTok['gokei']           + $gokeiTok;
                    $totalTok['act']             = $totalTok['act']             + $tokAct;
                    $totalNin['gokei']           = $totalNin['gokei']           + $gokeiNin;
                    $totalNin['act']             = $totalNin['act']             + $ninAct;
                    $nyuinTotal['patient_count'] = $nyuinTotal['patient_count'] + $patientCount;
                    $nyuinTotal['act_count']     = $nyuinTotal['act_count']     + $actCount;
                    $nyuinTotal['pmt_count']     = $nyuinTotal['pmt_count']     + $pmtCount;
                    $nyuinTotal['nyuin']         = $nyuinTotal['nyuin']         + $nyuin;
                    $nyuinTotal['taiin']         = $nyuinTotal['taiin']         + $taiin;

                    $tdArr[] = $this->convertCellData(
                        array(0 => array('data' => sprintf('%02d月%02d日(%s)', $month, $day, $this->getDayOfTheWeekForDate($year, $month, $day)), 'style' => array('bgColor' => $bgColor_blue))
                            # 外来患者数
                            , 1 => array('status' => $status, 'data' => $gokeiGa, 'disp_type' => 'integer')

                            # 入院(一般) 一般病棟
                            , 2 => array('status' => $status, 'data' => $gokeiIp)
                            , 3 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiIp, $ipAct, 1))
                            , 4 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiIp, $ipAct, 1), 'disp_type' => 'float_percent')

                            # 入院(一般) 障害者病棟
                            , 5 => array('status' => $status, 'data' => $gokeiShog)
                            , 6 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiShog, $shogAct, 1))
                            , 7 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiShog, $shogAct, 1), 'disp_type' => 'float_percent')

                            # 入院(一般) 精神一般病棟
                            , 8 => array('status' => $status, 'data' => $gokeiSeip)
                            , 9 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiSeip, $seipAct, 1))
                           , 10 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiSeip, $seipAct, 1), 'disp_type' => 'float_percent')

                           # 入院(療養) 医療療養病棟
                           , 11 => array('status' => $status, 'data' => $gokeiIr)
                           , 12 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiIr, $irAct, 1))
                           , 13 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiIr, $irAct, 1), 'disp_type' => 'float_percent')

                           # 入院(療養) 介護療養病棟
                           , 14 => array('status' => $status, 'data' => $gokeiKaig)
                           , 15 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiKaig, $kaigAct, 1))
                           , 16 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiKaig, $kaigAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) 回復期ﾘﾊ病棟
                           , 17 => array('status' => $status, 'data' => $gokeiKaif)
                           , 18 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiKaif, $kaifAct, 1))
                           , 19 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiKaif, $kaifAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) 亜急性期病床
                           , 20 => array('status' => $status, 'data' => $gokeiAk)
                           , 21 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiAk, $akAct, 1))
                           , 22 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiAk, $akAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) 緩和ｹｱ病棟
                           , 23 => array('status' => $status, 'data' => $gokeiKan)
                           , 24 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiKan, $kanAct, 1))
                           , 25 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiKan, $kanAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) ICU及びハイケアユニット
                           , 26 => array('status' => $status, 'data' => $gokeiIcu)
                           , 27 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiIcu, $icuAct, 1))
                           , 28 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiIcu, $icuAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) 小児管理料
                           , 29 => array('status' => $status, 'data' => $gokeiShoni)
                           , 30 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiShoni, $shoniAct, 1))
                           , 31 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiShoni, $shoniAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) 精神療養病棟
                           , 32 => array('status' => $status, 'data' => $gokeiSeir)
                           , 33 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiSeir, $seirAct, 1))
                           , 34 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiSeir, $seirAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) 特殊疾患(2)
                           , 35 => array('status' => $status, 'data' => $gokeiTok)
                           , 36 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiTok, $tokAct, 1))
                           , 37 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiTok, $tokAct, 1), 'disp_type' => 'float_percent')

                           # 入院(特定) 認知症病棟
                           , 38 => array('status' => $status, 'data' => $gokeiNin)
                           , 39 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($gokeiNin, $ninAct, 1))
                           , 40 => array('status' => $status, 'data' => $this->getUtilizationRates($gokeiNin, $ninAct, 1), 'disp_type' => 'float_percent')

                           # 入院合計
                           , 41 => array('status' => $status, 'data' => $patientCount)
                           , 42 => array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($patientCount, $actCount, 1))
                           , 43 => array('status' => $status, 'data' => $this->getUtilizationRatesTrigona($movementSickbedUtilizationRates, 90), 'style' =>  array('textAlign'  => $textAlign_center))
                           , 44 => array('status' => $status, 'data' => $movementSickbedUtilizationRates, 'disp_type' => 'float_percent')
                           , 45 => array('status' => $status, 'data' => $this->getUtilizationRatesTrigona($permissionSickbedUtilizationRates, 90), 'style' =>  array('textAlign'  => $textAlign_center))
                           , 46 => array('status' => $status, 'data' => $permissionSickbedUtilizationRates, 'disp_type' => 'float_percent')
                           , 47 => array('status' => $status, 'data' => $nyuin)
                           , 48 => array('status' => $status, 'data' => $taiin)
                        )
                      , $fontTag
                      , array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)
                    );
                    $rtnStr .= sprintf('<tr>%s</tr>', implode('', $tdArr));
                    $checkFlg = true;
                }
            }

            // その日のデータがなかった場合
            if ($checkFlg!==true) {
                $status      = $this->checkCellDataStatus('monthly_report', array('year' => $year, 'month' => $month, 'day' => $day));
                $totalStatus = ($totalStatus > $status)? $status : $totalStatus;
                $tdArr[]     = $this->convertCellData(
                    array(0 => array(
                              'data'  => sprintf('%02d月%02d日(%s)', $month, $day, $this->getDayOfTheWeekForDate($year, $month, $day))
                            , 'style' => array('bgColor' => $bgColor_blue)
                          )
                        , 1 => array('status' => $status, 'data' => ''),  2 => array('status' => $status, 'data' => '')
                        , 3 => array('status' => $status, 'data' => ''),  4 => array('status' => $status, 'data' => '')
                        , 5 => array('status' => $status, 'data' => ''),  6 => array('status' => $status, 'data' => '')
                        , 7 => array('status' => $status, 'data' => ''),  8 => array('status' => $status, 'data' => '')
                        , 9 => array('status' => $status, 'data' => ''), 10 => array('status' => $status, 'data' => '')
                       , 11 => array('status' => $status, 'data' => ''), 12 => array('status' => $status, 'data' => '')
                       , 13 => array('status' => $status, 'data' => ''), 14 => array('status' => $status, 'data' => '')
                       , 15 => array('status' => $status, 'data' => ''), 16 => array('status' => $status, 'data' => '')
                       , 17 => array('status' => $status, 'data' => ''), 18 => array('status' => $status, 'data' => '')
                       , 19 => array('status' => $status, 'data' => ''), 20 => array('status' => $status, 'data' => '')
                       , 21 => array('status' => $status, 'data' => ''), 22 => array('status' => $status, 'data' => '')
                       , 23 => array('status' => $status, 'data' => ''), 24 => array('status' => $status, 'data' => '')
                       , 25 => array('status' => $status, 'data' => ''), 26 => array('status' => $status, 'data' => '')
                       , 27 => array('status' => $status, 'data' => ''), 28 => array('status' => $status, 'data' => '')
                       , 29 => array('status' => $status, 'data' => ''), 30 => array('status' => $status, 'data' => '')
                       , 31 => array('status' => $status, 'data' => ''), 32 => array('status' => $status, 'data' => '')
                       , 33 => array('status' => $status, 'data' => ''), 34 => array('status' => $status, 'data' => '')
                       , 35 => array('status' => $status, 'data' => ''), 36 => array('status' => $status, 'data' => '')
                       , 37 => array('status' => $status, 'data' => ''), 38 => array('status' => $status, 'data' => '')
                       , 39 => array('status' => $status, 'data' => ''), 40 => array('status' => $status, 'data' => '')
                       , 41 => array('status' => $status, 'data' => ''), 42 => array('status' => $status, 'data' => '')
                       , 43 => array('status' => $status, 'data' => '', 'style' =>  array('textAlign'  => $textAlign_center))
                       , 44 => array('status' => $status, 'data' => '')
                       , 45 => array('status' => $status, 'data' => '', 'style' =>  array('textAlign'  => $textAlign_center))
                       , 46 => array('status' => $status, 'data' => ''), 47 => array('status' => $status, 'data' => '')
                       , 48 => array('status' => $status, 'data' => '')
                    )
                  , $fontTag
                  , array('textAlign'  => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)
                );
                $rtnStr .= sprintf('<tr>%s</tr>', implode('', $tdArr));
            }
        }

        $totalMovementSickbedUtilizationRates   = $this->getUtilizationRates($nyuinTotal['patient_count'], $nyuinTotal['act_count'], 1);
        $totalPermissionSickbedUtilizationRates = $this->getUtilizationRates($nyuinTotal['patient_count'], $nyuinTotal['pmt_count'], 1);

        $tdTotalArr[] = $this->convertCellData(
            array(
                 0 => array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign'  => $textAlign_center))

                 # 外来患者数
               , 1 => array('status' => $totalStatus, 'data' => $totalGa['gokei'])

                 # 入院(一般) 一般病棟
               , 2 => array('status' => $totalStatus, 'data' => $totalIp['gokei'])
               , 3 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalIp['gokei'], $totalIp['act'], 1))
               , 4 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalIp['gokei'], $totalIp['act'], 1), 'disp_type' => 'float_percent')

                 # 入院(一般) 障害者病棟
               , 5 => array('status' => $totalStatus, 'data' => $totalShog['gokei'])
               , 6 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalShog['gokei'], $totalShog['act'], 1))
               , 7 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalShog['gokei'], $totalShog['act'], 1), 'disp_type' => 'float_percent')

                 # 入院(一般) 精神一般病棟
               , 8 => array('status' => $totalStatus, 'data' => $totalSeip['gokei'])
               , 9 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalSeip['gokei'], $totalSeip['act'], 1))
              , 10 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalSeip['gokei'], $totalSeip['act'], 1), 'disp_type' => 'float_percent')


                # 入院(療養) 医療療養病棟
              , 11 => array('status' => $totalStatus, 'data' => $totalIr['gokei'])
              , 12 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalIr['gokei'], $totalIr['act'], 1))
              , 13 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalIr['gokei'], $totalIr['act'], 1), 'disp_type' => 'float_percent')

                # 入院(療養) 介護療養病棟
              , 14 => array('status' => $totalStatus, 'data' => $totalKaig['gokei'])
              , 15 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalKaig['gokei'], $totalKaig['act'], 1))
              , 16 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalKaig['gokei'], $totalKaig['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) 回復期ﾘﾊ病棟
              , 17 => array('status' => $totalStatus, 'data' => $totalKaif['gokei'])
              , 18 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalKaif['gokei'], $totalKaif['act'], 1))
              , 19 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalKaif['gokei'], $totalKaif['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) 亜急性期病床
              , 20 => array('status' => $totalStatus, 'data' => $totalAk['gokei'])
              , 21 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalAk['gokei'], $totalAk['act'], 1))
              , 22 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalAk['gokei'], $totalAk['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) 緩和ｹｱ病棟
              , 23 => array('status' => $totalStatus, 'data' => $totalKan['gokei'])
              , 24 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalKan['gokei'], $totalKan['act'], 1))
              , 25 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalKan['gokei'], $totalKan['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) ICU及びハイケアユニット
              , 26 => array('status' => $totalStatus, 'data' => $totalIcu['gokei'])
              , 27 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalIcu['gokei'], $totalIcu['act'], 1))
              , 28 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalIcu['gokei'], $totalIcu['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) 小児管理料
              , 29 => array('status' => $totalStatus, 'data' => $totalShoni['gokei'])
              , 30 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalShoni['gokei'], $totalShoni['act'], 1))
              , 31 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalShoni['gokei'], $totalShoni['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) 精神療養病棟
              , 32 => array('status' => $totalStatus, 'data' => $totalSeir['gokei'])
              , 33 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalSeir['gokei'], $totalSeir['act'], 1))
              , 34 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalSeir['gokei'], $totalSeir['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) 特殊疾患(2)
              , 35 => array('status' => $totalStatus, 'data' => $totalTok['gokei'])
              , 36 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalTok['gokei'], $totalTok['act'], 1))
              , 37 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalTok['gokei'], $totalTok['act'], 1), 'disp_type' => 'float_percent')

                # 入院(特定) 認知症病棟
              , 38 => array('status' => $totalStatus, 'data' => $totalNin['gokei'])
              , 39 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($totalNin['gokei'], $totalNin['act'], 1))
              , 40 => array('status' => $totalStatus, 'data' => $this->getUtilizationRates($totalNin['gokei'], $totalNin['act'], 1), 'disp_type' => 'float_percent')

                # 入院合計
              , 41 => array('status' => $totalStatus, 'data' => $nyuinTotal['patient_count'])
              , 42 => array('status' => $totalStatus, 'data' => $this->getNumberOfEmptyFloors($nyuinTotal['patient_count'], $nyuinTotal['act_count'], 1))
              , 43 => array('status' => $totalStatus, 'data' => $this->getUtilizationRatesTrigona($totalMovementSickbedUtilizationRates, 90), 'style' =>  array('textAlign'  => $textAlign_center))
              , 44 => array('status' => $totalStatus, 'data' => $totalMovementSickbedUtilizationRates, 'disp_type' => 'float_percent')
              , 45 => array('status' => $totalStatus, 'data' => $this->getUtilizationRatesTrigona($totalPermissionSickbedUtilizationRates, 90), 'style' =>  array('textAlign'  => $textAlign_center))
              , 46 => array('status' => $totalStatus, 'data' => $totalPermissionSickbedUtilizationRates, 'disp_type' => 'float_percent')
              , 47 => array('status' => $totalStatus, 'data' => $nyuinTotal['nyuin'])
              , 48 => array('status' => $totalStatus, 'data' => $nyuinTotal['taiin'])
            ),
            $fontTag,
            array('textAlign'  => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)
        );
        $rtnStr .= sprintf('<tr>%s</tr>', implode('', $tdTotalArr));
        $rtnStr .= '</table>';

        return $rtnStr;
    }

    /**
     * No.65 [病院] 病院別患者月報のデータ出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function getMonthlyReportAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr  = '';
        $fontTag = $this->getFontTag();
        $year    = $dateArr['year'];
        $month   = $dateArr['month'];

        # new IndicatorData()
        $dataObj  = $this->getIndicatorData();

        $currentYear         = $this->getCurrentYear();
        $lastYear            = $this->getLastYear();
        $currentYearDayCount = $dataObj->getDayCountOfMonth($year, $month);
        $lastYearDayCount    = $dataObj->getDayCountOfMonth($year, $month);

        $rgbBlue  = $this->getRgbColor('blue');
        $rgbGreen = $this->getRgbColor('green');

        # attr
        $rowspan_2         = array('name' => 'rowspan',          'value' =>  2);
        $rowspan_3         = array('name' => 'rowspan',          'value' =>  3);
        $colspan_2         = array('name' => 'colspan',          'value' =>  2);
        $colspan_3         = array('name' => 'colspan',          'value' =>  3);
        $colspan_6         = array('name' => 'colspan',          'value' =>  6);
        $colspan_9         = array('name' => 'colspan',          'value' =>  9);
        $colspan_24        = array('name' => 'colspan',          'value' => 24);
        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $rgbBlue);
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $rtnStr .= '<table border="0" width="100%" >'
                      .'<tr>'
                          .'<td style="text-align: left;">'
                              .'<table class="list" width="25%" >'
                                  .'<tr>'
                                       .$this->convertCellData(
                                           array(0 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday))
                                               , 1 => array('data' => $currentYearDayCount['weekday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                               , 2 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday))
                                               , 3 => array('data' => $currentYearDayCount['saturday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                               , 4 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday))
                                               , 5 => array('data' => $currentYearDayCount['holiday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                           ),
                                           $fontTag,
                                           array('whiteSpace' => $whiteSpace_nowrap, 'textAlign'  => $textAlign_left)
                                       )
                                  .'</tr>'
                              .'</table>'
                          .'</td>'
                          .'<td style="text-align: right;">'.sprintf($fontTag, $year.'年'.$month.'月').'</td>'
                      .'</tr>'
                  .'</table>';

        $header = '<table class="list" width="100%">'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '病院名',           'attr' => array($rowspan_3))
                                 , 1 => array('data' => '外来<br />患者数', 'attr' => array($rowspan_3))
                                 , 2 => array('data' => '入院(一般)',       'attr' => array($colspan_9))
                                 , 3 => array('data' => '入院(療養)',       'attr' => array($colspan_6))
                                 , 4 => array('data' => '入院(特定)',       'attr' => array($colspan_24))
                                 , 5 => array('data' => '入院合計',         'attr' => array($colspan_6, $rowspan_2))
                                 , 6 => array('data' => '入退院数',         'attr' => array($colspan_2, $rowspan_2))
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '一般病棟',      'attr' => array($colspan_3))
                                 , 1 => array('data' => '障害者病棟',    'attr' => array($colspan_3))
                                 , 2 => array('data' => '精神一般病棟',  'attr' => array($colspan_3))
                                 , 3 => array('data' => '医療療養病棟',  'attr' => array($colspan_3))
                                 , 4 => array('data' => '介護療養病棟',  'attr' => array($colspan_3))
                                 , 5 => array('data' => '回復期ﾘﾊ病棟',    'attr' => array($colspan_3))
                                 , 6 => array('data' => '亜急性期病床',  'attr' => array($colspan_3))
                                 , 7 => array('data' => '緩和ｹｱ病棟',    'attr' => array($colspan_3))
                                 , 8 => array('data' => 'ICU及びハイケアユニット',     'attr' => array($colspan_3))
                                 , 9 => array('data' => '小児管理料', 'attr' => array($colspan_3))
                                , 10 => array('data' => '精神療養病棟',  'attr' => array($colspan_3))
                                , 11 => array('data' => '特殊疾患(2)',   'attr' => array($colspan_3))
                                , 12 => array('data' => '認知症病棟', 'attr' => array($colspan_3))
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '患者数'),  1 => array('data' => '空床数'),  2 => array('data' => '稼働率'),
                                   3 => array('data' => '患者数'),  4 => array('data' => '空床数'),  5 => array('data' => '稼働率'),
                                   6 => array('data' => '患者数'),  7 => array('data' => '空床数'),  8 => array('data' => '稼働率'),
                                   9 => array('data' => '患者数'), 10 => array('data' => '空床数'), 11 => array('data' => '稼働率'),
                                  12 => array('data' => '患者数'), 13 => array('data' => '空床数'), 14 => array('data' => '稼働率'),
                                  15 => array('data' => '患者数'), 16 => array('data' => '空床数'), 17 => array('data' => '稼働率'),
                                  18 => array('data' => '患者数'), 19 => array('data' => '空床数'), 20 => array('data' => '稼働率'),
                                  21 => array('data' => '患者数'), 22 => array('data' => '空床数'), 23 => array('data' => '稼働率'),
                                  24 => array('data' => '患者数'), 25 => array('data' => '空床数'), 26 => array('data' => '稼働率'),
                                  27 => array('data' => '患者数'), 28 => array('data' => '空床数'), 29 => array('data' => '稼働率'),
                                  30 => array('data' => '患者数'), 31 => array('data' => '空床数'), 32 => array('data' => '稼働率'),
                                  33 => array('data' => '患者数'), 34 => array('data' => '空床数'), 35 => array('data' => '稼働率'),
                                  36 => array('data' => '患者数'), 37 => array('data' => '空床数'), 38 => array('data' => '稼働率'),
                                  39 => array('data' => '患者数'), 40 => array('data' => '空床数'),
                                  41 => array('data' => '稼働病床<br />稼働率', 'attr' => array($colspan_2)),
                                  43 => array('data' => '許可病床<br />稼働率', 'attr' => array($colspan_2)),
                                  45 => array('data' => '入院'),   46 => array('data' => '退院'),
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';

        $rtnStr        .= $header;
        $departmentType = $this->getDepartmentType();
        $currentHeader  = '';
        $totalDataArr['current'] = array();

        foreach ($plantArr as $plantVal) {
            $plantId = $this->getArrValueByKey('facility_id', $plantVal);
            $currentHeader = '<tr>'.$this->convertCellData(
                array(0 => array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'style' => array('bgColor' => $bgColor_blue))),
                $fontTag,
                array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
            );

            $cuurentDataArr = array();
            $checkPlant     = false;

            foreach ($dataArr as $dataVal) {
                $facilityId = $this->getArrValueByKey('jnl_facility_id', $dataVal);
                if ($plantId == $facilityId) {
                    $yearType = $dataVal['year_type'];
                    $status   = $this->checkCellDataStatus('monthly_report_comp', $dataVal);
                    switch ($yearType) {
                        case 'current':
                            $cTotalAct        = 0;
                            $cTotalPmt        = 0;
                            $cTotalPatient    = 0;
                            $cTotalEmptyFloor = 0;
                            $cTotalActRate    = 0;
                            $cTotalPmtRate    = 0;

                            # 外来患者数
                            $cuurentDataArr[0]  = array('data' => $dataVal['gokei_ga'], 'status' => $status);
                            $cuurentDataArr[46] = array('data' => $dataVal['nyuin'],    'status' => $status);
                            $cuurentDataArr[47] = array('data' => $dataVal['taiin'],    'status' => $status);

                            $cTotalAct = $dataVal['act_count'];
                            $cTotalPmt = $dataVal['pmt_count'];

                            $totalDataArr['current'][0] = array(
                                'data'   => ($totalDataArr['current'][0]['data'] + $cuurentDataArr[0]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][0]['status'], $status))
                            );
                            $totalDataArr['current'][46] = array(
                                'data'   => ($totalDataArr['current'][46]['data'] + $cuurentDataArr[46]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][46]['status'], $status))
                            );
                            $totalDataArr['current'][47] = array(
                                'data'   => ($totalDataArr['current'][47]['data'] + $cuurentDataArr[47]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][47]['status'], $status))
                            );
                            break;
                    }

                    foreach ($departmentType as $dTypeKey => $dTypeVal) {
                        $columnNum     = $dTypeKey * 3 + 1;
                        $tmpAct        = $dataVal[$dTypeVal.'_act'];
                        $tmpPatient    = $dataVal['gokei_'.$dTypeVal];
                        $tmpEmptyFloor = $this->getNumberOfEmptyFloors($tmpPatient, $tmpAct, 1);
                        $tmpRates      = $this->getUtilizationRates($tmpPatient,    $tmpAct, 1);

                        switch ($yearType) {
                            case 'current':
                                $cuurentDataArr[$columnNum]   = array('data' => $tmpPatient,    'status' => $status);
                                $cuurentDataArr[$columnNum+1] = array('data' => $tmpEmptyFloor, 'status' => $status);
                                $cuurentDataArr[$columnNum+2] = array('data' => $tmpRates,      'status' => $status, 'disp_type' => 'float_percent');

                                $cTotalPatient = $cTotalPatient + $tmpPatient;

                                $totalDataArr['current'][$columnNum] = array(
                                    'data'   => ($totalDataArr['current'][$columnNum]['data'] + $cuurentDataArr[$columnNum]['data']),
                                    'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$columnNum]['status'], $status)),
                                    'act'    => ($totalDataArr['current'][$columnNum]['act'] + $tmpAct),
                                );
                                break;
                        }
                    }

                    switch ($yearType) {
                        case 'current':
                            $cTotalEmptyFloor = $this->getNumberOfEmptyFloors($cTotalPatient, $cTotalAct, 1);
                            $cTotalActRate    = $this->getUtilizationRates($cTotalPatient,    $cTotalAct, 1);
                            $cTotalPmtRate    = $this->getUtilizationRates($cTotalPatient,    $cTotalPmt, 1);

                            # 入院合計
                            $cuurentDataArr[40] = array('data' => $cTotalPatient,    'status' => $status);
                            $cuurentDataArr[41] = array('data' => $cTotalEmptyFloor, 'status' => $status);
                            $cuurentDataArr[42] = array('data' => $this->getUtilizationRatesTrigona($cTotalActRate, 90), 'status' => $status, 'style' => array('textAlign' => $textAlign_center));
                            $cuurentDataArr[43] = array('data' => $cTotalActRate,    'status' => $status, 'disp_type' => 'float_percent');
                            $cuurentDataArr[44] = array('data' => $this->getUtilizationRatesTrigona($cTotalPmtRate, 90), 'status' => $status, 'style' => array('textAlign' => $textAlign_center));
                            $cuurentDataArr[45] = array('data' => $cTotalPmtRate,    'status' => $status, 'disp_type' => 'float_percent');

                            $totalDataArr['current'][40] = array(
                                'data'   => ($totalDataArr['current'][40]['data'] + $cuurentDataArr[40]['data']),
                                'act'    => ($totalDataArr['current'][40]['act']  + $cTotalAct),
                                'pmt'    => ($totalDataArr['current'][40]['pmt']  + $cTotalPmt),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][40]['status'], $status))
                            );
                            break;
                    }
                    $checkPlant = true;
                }
            }
            if ($checkPlant == true) {
                # 入院合計 稼働病床稼働率
                $cuurentDataArr[43]['data'] = !empty($cuurentDataArr[43]['data'])? $cuurentDataArr[43]['data'].'%' : $cuurentDataArr[43]['data'];

                # 入院合計 許可病床稼働率
                $cuurentDataArr[45]['data'] = !empty($cuurentDataArr[45]['data'])? $cuurentDataArr[45]['data'].'%' : $cuurentDataArr[45]['data'];

                foreach ($departmentType as $dTypeKey => $dTypeVal) {
                    $columnNum = $dTypeKey * 3 + 1;
                    $cuurentDataArr[$columnNum+2]['data'] = !empty($cuurentDataArr[$columnNum+2]['data'])? $cuurentDataArr[$columnNum+2]['data'].'%' : $cuurentDataArr[$columnNum+2]['data'];
                }
            }

            for ($i=0; $i <= 47; $i++ ) {
                if (!$this->getArrValueByKey($i, $cuurentDataArr)) {
                    $cuurentDataArr[$i] = array('data' => '', 'status' => $this->checkCellDataStatus('monthly_report_comp', array('year' => $year, 'month' => $month)));
                }
            }
            $rtnStr .= $currentHeader.$this->convertCellData($cuurentDataArr, $fontTag, array('textAlign' => $textAlign_right)).'</tr>';
        }

        for ($i=0; $i <= 47; $i++) {
            if (!$this->getArrValueByKey($i, $totalDataArr['current'])) {
                switch ($i) {
                    case '2': case '5': case '8': case '11': case '14': case '17': case '20': case '23': case '26': case '29': case '32': case '35': case '38':
                        $totalDataArr['current'][$i] = array(
                            'data'   => (is_null($totalDataArr['current'][$i-1]['data']) && is_null($totalDataArr['current'][$i-1]['act']))? null : $this->getNumberOfEmptyFloors($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        $totalDataArr['current'][$i+1] = array(
                            'data'   => is_null($totalDataArr['current'][$i-1]['act'])? null : $this->getUtilizationRates($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+1]['status'], $totalDataArr['current'][$i-1]['status'])),
                            'disp_type' => 'float_percent'
                        );
                        break;

                    case '41':
                        $totalDataArr['current'][$i] = array(
                            'data'   => (is_null($totalDataArr['current'][$i-1]['data']) && is_null($totalDataArr['current'][$i-1]['act']))? null : $this->getNumberOfEmptyFloors($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        $totalDataArr['current'][$i+2] = array(
                            'data'   => is_null($totalDataArr['current'][$i-1]['act'])? null : $this->getUtilizationRates($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+2]['status'], $totalDataArr['current'][$i-1]['status'])),
                            'disp_type' => 'float_percent'
                        );
                        $totalDataArr['current'][$i+4] = array(
                            'data'   => is_null($totalDataArr['current'][$i-1]['pmt'])? null : $this->getUtilizationRates($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['pmt'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+4]['status'], $totalDataArr['current'][$i-1]['status'])),
                            'disp_type' => 'float_percent'
                        );
                        $totalDataArr['current'][$i+1] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['current'][$i+2]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+1]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        $totalDataArr['current'][$i+3] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['current'][$i+4]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+1]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        break;

                    default:
                        $totalDataArr['current'][$i]= array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['current'][$i-1]['status'])));
                        break;
                }
            }
        }

        $totalHeader = '<tr>'.$this->convertCellData(
            array(0 => array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue))),
            $fontTag,
            array('textAlign'  => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
        );

        $rtnStr .= $totalHeader.$this->convertCellData($totalDataArr['current'], $fontTag, array('textAlign' => $textAlign_right)).'</tr></table>';
        return $rtnStr;
    }

    /**
     * No.66 [病院] 病院別患者月報(前年比較)のデータ出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function getMonthlyReportCompDisplayInThePreviousYear($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr  = '';
        $fontTag = $this->getFontTag();
        $year    = $dateArr['year'];
        $month   = $dateArr['month'];

        /* @var $dataObj IndicatorData */
        $dataObj  = $this->getIndicatorData();

        $currentYear         = $this->getCurrentYear();
        $lastYear            = $this->getLastYear();
        $currentYearDayCount = $dataObj->getDayCountOfMonth($year,     $month);
        $lastYearDayCount    = $dataObj->getDayCountOfMonth($year - 1, $month);

        $rgbBlue  = $this->getRgbColor('blue');
        $rgbGreen = $this->getRgbColor('green');

        # attr
        $rowspan_2         = array('name' => 'rowspan',          'value' =>  2);
        $rowspan_3         = array('name' => 'rowspan',          'value' =>  3);
        $rowspan_4         = array('name' => 'rowspan',          'value' =>  4);
        $colspan_2         = array('name' => 'colspan',          'value' =>  2);
        $colspan_3         = array('name' => 'colspan',          'value' =>  3);
        $colspan_6         = array('name' => 'colspan',          'value' =>  6);
        $colspan_9         = array('name' => 'colspan',          'value' =>  9);
        $colspan_24        = array('name' => 'colspan',          'value' => 24);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_green     = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $rtnStr .= '<table border="0" width="100%" >'
                      .'<tr>'
                          .'<td style="text-align: left;">'
                              .'<table class="list" width="25%" >'
                                  .'<tr>'
                                      .$this->convertCellData(
                                          array(0 => array('data' => sprintf('%04d年度', $currentYear))
                                              , 1 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday))
                                              , 2 => array('data' => $currentYearDayCount['weekday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                              , 3 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday))
                                              , 4 => array('data' => $currentYearDayCount['saturday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                              , 5 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday))
                                              , 6 => array('data' => $currentYearDayCount['holiday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                           ),
                                           $fontTag,
                                           array('whiteSpace' => $whiteSpace_nowrap, 'textAlign'  => $textAlign_left)
                                       )
                                   .'</tr>'
                                   .'<tr>'
                                      .$this->convertCellData(
                                          array(0 => array('data' => sprintf('%04d年度', $lastYear))
                                              , 1 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday))
                                              , 2 => array('data' => $lastYearDayCount['weekday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                              , 3 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday))
                                              , 4 => array('data' => $lastYearDayCount['saturday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                              , 5 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday))
                                              , 6 => array('data' => $lastYearDayCount['holiday'].'日', 'style' => array('textAlign' => $textAlign_center))
                                           ),
                                           $fontTag,
                                           array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left)
                                       )
                                   .'</tr>'
                               .'</table>'
                          .'</td>'
                          .'<td style="text-align: right;">'. sprintf($fontTag, $year.'年'.$month.'月'). '</td>'
                      .'</tr>'
                  .'</table>';

        $header = '<table class="list" width="100%">'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '病院名',           'attr' => array($rowspan_3))
                                 , 1 => array('data' => '年',               'attr' => array($rowspan_3))
                                 , 2 => array('data' => '外来<br />患者数', 'attr' => array($rowspan_3))
                                 , 3 => array('data' => '入院(一般)',       'attr' => array($colspan_9))
                                 , 4 => array('data' => '入院(療養)',       'attr' => array($colspan_6))
                                 , 5 => array('data' => '入院(特定)',       'attr' => array($colspan_24))
                                 , 6 => array('data' => '入院合計',         'attr' => array($colspan_6, $rowspan_2))
                                 , 7 => array('data' => '入退院数',         'attr' => array($colspan_2, $rowspan_2))
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '一般病棟',      'attr' => array($colspan_3))
                                 , 1 => array('data' => '障害者病棟',    'attr' => array($colspan_3))
                                 , 2 => array('data' => '精神一般病棟',  'attr' => array($colspan_3))
                                 , 3 => array('data' => '医療療養病棟',  'attr' => array($colspan_3))
                                 , 4 => array('data' => '介護療養病棟',  'attr' => array($colspan_3))
                                 , 5 => array('data' => '回復期ﾘﾊ病棟',    'attr' => array($colspan_3))
                                 , 6 => array('data' => '亜急性期病床',  'attr' => array($colspan_3))
                                 , 7 => array('data' => '緩和ｹｱ病棟',    'attr' => array($colspan_3))
                                 , 8 => array('data' => 'ICU及びハイケアユニット',     'attr' => array($colspan_3))
                                 , 9 => array('data' => '小児管理料', 'attr' => array($colspan_3))
                                , 10 => array('data' => '精神療養病棟',  'attr' => array($colspan_3))
                                , 11 => array('data' => '特殊疾患(2)',   'attr' => array($colspan_3))
                                , 12 => array('data' => '認知症病棟', 'attr' => array($colspan_3))
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(
                                  0 => array('data' => '患者数'),  1 => array('data' => '空床数'),  2 => array('data' => '稼働率')
                                , 3 => array('data' => '患者数'),  4 => array('data' => '空床数'),  5 => array('data' => '稼働率')
                                , 6 => array('data' => '患者数'),  7 => array('data' => '空床数'),  8 => array('data' => '稼働率')
                                , 9 => array('data' => '患者数'), 10 => array('data' => '空床数'), 11 => array('data' => '稼働率')
                               , 12 => array('data' => '患者数'), 13 => array('data' => '空床数'), 14 => array('data' => '稼働率')
                               , 15 => array('data' => '患者数'), 16 => array('data' => '空床数'), 17 => array('data' => '稼働率')
                               , 18 => array('data' => '患者数'), 19 => array('data' => '空床数'), 20 => array('data' => '稼働率')
                               , 21 => array('data' => '患者数'), 22 => array('data' => '空床数'), 23 => array('data' => '稼働率')
                               , 24 => array('data' => '患者数'), 25 => array('data' => '空床数'), 26 => array('data' => '稼働率')
                               , 27 => array('data' => '患者数'), 28 => array('data' => '空床数'), 29 => array('data' => '稼働率')
                               , 30 => array('data' => '患者数'), 31 => array('data' => '空床数'), 32 => array('data' => '稼働率')
                               , 33 => array('data' => '患者数'), 34 => array('data' => '空床数'), 35 => array('data' => '稼働率')
                               , 36 => array('data' => '患者数'), 37 => array('data' => '空床数'), 38 => array('data' => '稼働率')
                               , 39 => array('data' => '患者数'), 40 => array('data' => '空床数')
                               , 41 => array('data' => '稼働病床<br />稼働率', 'attr' => array($colspan_2))
                               , 42 => array('data' => '許可病床<br />稼働率', 'attr' => array($colspan_2))
                               , 43 => array('data' => '入院'),   44 => array('data' => '退院')
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';

        $rtnStr        .= $header;
        $departmentType = $this->getDepartmentType();
        $currentHeader  = '';
        $lastHeader     = '';
        $compHeader     = '';
        $raitoHeader    = '';

        $totalDataArr['current'] = array();
        $totalDataArr['last']    = array();
        $totalDataArr['comp']    = array();
        $totalDataArr['raito']   = array();

        foreach ($plantArr as $plantVal) {
            $plantId = $this->getArrValueByKey('facility_id', $plantVal);
            $currentHeader = '<tr>'.$this->convertCellData(
                array(0 => array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue)),
                      1 => array('data' => $currentYear.'年度'),
                ),
                $fontTag,
                array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
            );
            $lastHeader  = '<tr>'.$this->convertCellData(array(0 => array('data' => $lastYear.'年度')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap));
            $compHeader  = '<tr>'.$this->convertCellData(array(0 => array('data' => '前年比')), $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap));
            $raitoHeader = '<tr>'.$this->convertCellData(array(0 => array('data' => '増減率')), $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap));

            $cuurentDataArr = array();
            $lastDataArr    = array();
            $compDataArr    = array();
            $raitoDataArr   = array();

            $checkPlant = false;
            foreach ($dataArr as $dataVal) {
                $facilityId = $this->getArrValueByKey('jnl_facility_id', $dataVal);
                if ($plantId == $facilityId) {
                    $yearType = $dataVal['year_type'];
                    $status   = $this->checkCellDataStatus('monthly_report_comp', $dataVal);
                    switch ($yearType) {
                        case 'current':
                            $cTotalAct        = 0;
                            $cTotalPmt        = 0;
                            $cTotalPatient    = 0;
                            $cTotalEmptyFloor = 0;
                            $cTotalActRate    = 0;
                            $cTotalPmtRate    = 0;

                            # 外来患者数
                            $cuurentDataArr[0]  = array('data' => $dataVal['gokei_ga'], 'status' => $status);
                            $cuurentDataArr[46] = array('data' => $dataVal['nyuin'],    'status' => $status);
                            $cuurentDataArr[47] = array('data' => $dataVal['taiin'],    'status' => $status);

                            $cTotalAct = $dataVal['act_count'];
                            $cTotalPmt = $dataVal['pmt_count'];

                            $totalDataArr['current'][0] = array(
                                'data'   => ($totalDataArr['current'][0]['data'] + $cuurentDataArr[0]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][0]['status'], $status))
                            );
                            $totalDataArr['current'][46] = array(
                                'data'   => ($totalDataArr['current'][46]['data'] + $cuurentDataArr[46]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][46]['status'], $status))
                            );
                            $totalDataArr['current'][47] = array(
                                'data'   => ($totalDataArr['current'][47]['data'] + $cuurentDataArr[47]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][47]['status'], $status))
                            );
                            break;

                        case 'last':
                            $lTotalAct        = 0;
                            $lTotalPmt        = 0;
                            $lTotalPatient    = 0;
                            $lTotalEmptyFloor = 0;
                            $lTotalActRate    = 0;
                            $lTotalPmtRate    = 0;

                            # 外来患者数
                            $lastDataArr[0]  = array('data' => $dataVal['gokei_ga'], 'status' => $status);
                            $lastDataArr[46] = array('data' => $dataVal['nyuin'],    'status' => $status);
                            $lastDataArr[47] = array('data' => $dataVal['taiin'],    'status' => $status);

                            $lTotalAct = $dataVal['act_count'];
                            $lTotalPmt = $dataVal['pmt_count'];

                            $totalDataArr['last'][0] = array(
                                'data'   => ($totalDataArr['last'][0]['data'] + $lastDataArr[0]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][0]['status'], $status))
                            );
                            $totalDataArr['last'][46] = array(
                                'data'   => ($totalDataArr['last'][46]['data'] + $lastDataArr[46]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][0]['status'], $status))
                            );
                            $totalDataArr['last'][47] = array(
                                'data'   => ($totalDataArr['last'][47]['data'] + $lastDataArr[47]['data']),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][47]['status'], $status))
                            );
                            break;
                    }

                    foreach ($departmentType as $dTypeKey => $dTypeVal) {
                        $columnNum     = $dTypeKey * 3 + 1;
                        $tmpAct        = $dataVal[$dTypeVal.'_act'];
                        $tmpPatient    = $dataVal['gokei_'.$dTypeVal];
                        $tmpEmptyFloor = $this->getNumberOfEmptyFloors($tmpPatient, $tmpAct, 1);
                        $tmpRates      = $this->getUtilizationRates($tmpPatient, $tmpAct, 1);

                        switch ($yearType) {
                            case 'current':
                                $cuurentDataArr[$columnNum]   = array('data' => $tmpPatient,    'status' => $status);
                                $cuurentDataArr[$columnNum+1] = array('data' => $tmpEmptyFloor, 'status' => $status);
                                $cuurentDataArr[$columnNum+2] = array('data' => $tmpRates,      'status' => $status, 'disp_type' => 'float_percent');

                                $cTotalPatient = $cTotalPatient + $tmpPatient;

                                $totalDataArr['current'][$columnNum] = array(
                                    'data'   => ($totalDataArr['current'][$columnNum]['data'] + $cuurentDataArr[$columnNum]['data']),
                                    'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$columnNum]['status'], $status)),
                                    'act'    => ($totalDataArr['current'][$columnNum]['act'] + $tmpAct),
                                );
                                break;

                            case 'last':
                                $lastDataArr[$columnNum]   = array('data' => $tmpPatient,    'status' => $status);
                                $lastDataArr[$columnNum+1] = array('data' => $tmpEmptyFloor, 'status' => $status);
                                $lastDataArr[$columnNum+2] = array('data' => $tmpRates,      'status' => $status, 'disp_type' => 'float_percent');

                                $lTotalPatient = $lTotalPatient + $tmpPatient;

                                $totalDataArr['last'][$columnNum] = array(
                                    'data' => ($totalDataArr['last'][$columnNum]['data'] + $lastDataArr[$columnNum]['data']),
                                    'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$columnNum]['status'], $status)),
                                    'act' => ($totalDataArr['last'][$columnNum]['act'] + $tmpAct)
                                );
                                break;
                        }
                    }

                    switch ($yearType) {
                        case 'current':
                            $cTotalEmptyFloor = $this->getNumberOfEmptyFloors($cTotalPatient, $cTotalAct, 1);
                            $cTotalActRate    = $this->getUtilizationRates($cTotalPatient, $cTotalAct, 1);
                            $cTotalPmtRate    = $this->getUtilizationRates($cTotalPatient, $cTotalPmt, 1);

                            # 入院合計
                            $cuurentDataArr[40] = array('data' => $cTotalPatient,    'status' => $status);
                            $cuurentDataArr[41] = array('data' => $cTotalEmptyFloor, 'status' => $status);
                            $cuurentDataArr[42] = array(
                                'data'   => $this->getUtilizationRatesTrigona($cTotalActRate, 90),
                                'status' => $status,
                                'style'  => array('textAlign' => $textAlign_center)
                            );
                            $cuurentDataArr[43] = array('data' => $cTotalActRate,    'status' => $status, 'disp_type' => 'float_percent');
                            $cuurentDataArr[44] = array(
                                'data'   => $this->getUtilizationRatesTrigona($cTotalPmtRate, 90),
                                'status' => $status,
                                'style'  => array('textAlign' => $textAlign_center)
                            );
                            $cuurentDataArr[45] = array('data' => $cTotalPmtRate,    'status' => $status, 'disp_type' => 'float_percent');

                            $totalDataArr['current'][40] = array(
                                'data'   => ($totalDataArr['current'][40]['data'] + $cuurentDataArr[40]['data']),
                                'act'    => ($totalDataArr['current'][40]['act'] + $cTotalAct),
                                'pmt'    => ($totalDataArr['current'][40]['pmt'] + $cTotalPmt),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][40]['status'], $status))
                            );
                            break;

                        case 'last':
                            $lTotalEmptyFloor = $this->getNumberOfEmptyFloors($lTotalPatient, $lTotalAct, 1);
                            $lTotalActRate    = $this->getUtilizationRates($lTotalPatient, $lTotalAct, 1);
                            $lTotalPmtRate    = $this->getUtilizationRates($lTotalPatient, $lTotalPmt, 1);

                            # 入院合計
                            $lastDataArr[40] = array('data' => $lTotalPatient,    'status' => $status);
                            $lastDataArr[41] = array('data' => $lTotalEmptyFloor, 'status' => $status);
                            $lastDataArr[42] = array(
                                'data'   => $this->getUtilizationRatesTrigona($lTotalActRate, 90),
                                'status' => $status,
                                'style'  => array('textAlign' => $textAlign_center)
                            );
                            $lastDataArr[43] = array('data' => $lTotalActRate,    'status' => $status, 'disp_type' => 'float_percent');
                            $lastDataArr[44] = array(
                                'data'   => $this->getUtilizationRatesTrigona($lTotalPmtRate, 90),
                                'status' => $status,
                                'style'  => array('textAlign' => $textAlign_center)
                            );
                            $lastDataArr[45] = array('data' => $lTotalPmtRate,    'status' => $status, 'disp_type' => 'float_percent');

                            $totalDataArr['last'][40] = array(
                                'data'   => ($totalDataArr['last'][40]['data'] + $lastDataArr[40]['data']),
                                'act'    => ($totalDataArr['last'][40]['act'] + $lTotalAct),
                                'pmt'    => ($totalDataArr['last'][40]['pmt'] + $lTotalPmt),
                                'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][40]['status'], $status)),
                            );
                            break;
                    }

                    $checkPlant = true;
                }
            }

            if ($checkPlant == true) {
                # 外来患者数
                $cGokeiGa = $this->getArrValueByKey(0, $cuurentDataArr)? $cuurentDataArr[0]['data'] : null;
                $lGokeiGa = $this->getArrValueByKey(0, $lastDataArr)?    $lastDataArr[0]['data']    : null;
                $gokeiga  = (is_null($lGokeiGa) && is_null($cGokeiGa))? null : $this->getNumberOfEmptyFloors($lGokeiGa, $cGokeiGa, 1);

                $compDataArr[0]  = array('data' => $gokeiga, 'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[0]['status'], $lastDataArr[0]['status'])));
                $raitoDataArr[0] = array(
                    'data'   => empty($lGokeiGa)? null : $this->getUtilizationRates($compDataArr[0]['data'], $lGokeiGa, 1),
                    'status' => $this->checkCellDataStatus('comp', array($compDataArr[0]['status'], $lastDataArr[0]['status'])),
                    'disp_type' => 'float_percent'
                );

                # 入院合計 患者数
                $cPatient = $this->getArrValueByKey(40, $cuurentDataArr)? $cuurentDataArr[40]['data'] : null;
                $lPatient = $this->getArrValueByKey(40, $lastDataArr)?    $lastDataArr[40]['data']    : null;
                $compDataArr[40] = array(
                    'data'   => (is_null($lPatient) && is_null($cPatient))? null : $this->getNumberOfEmptyFloors($lPatient, $cPatient, 1),
                    'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[40]['status'], $lastDataArr[40]['status']))
                );
                $raitoDataArr[40] = array(
                    'data'   => is_null($lPatient)? null : $this->getUtilizationRates($compDataArr[40]['data'], $lPatient, 1),
                    'status' => $this->checkCellDataStatus('comp', array($compDataArr[40]['status'], $lastDataArr[40]['status'])),
                    'disp_type' => 'float_percent'
                );

                # 入院合計 空床数
                $cEmptyFloor = $this->getArrValueByKey(41, $cuurentDataArr)? $cuurentDataArr[41]['data'] : 0;
                $lEmptyFloor = $this->getArrValueByKey(41, $lastDataArr)?    $lastDataArr[41]['data']    : 0;
                $compDataArr[41] = array(
                    'data'   => $this->getNumberOfEmptyFloors($lEmptyFloor, $cEmptyFloor, 1),
                    'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[41]['status'], $lastDataArr[41]['status']))
                );
                $raitoDataArr[41] = array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($compDataArr[41]['status'], $lastDataArr[41]['status'])));

                # 入院合計 稼働病床稼働率
                $cActRate = $this->getArrValueByKey(43, $cuurentDataArr)? $cuurentDataArr[43]['data'] : 0;
                $lActRate = $this->getArrValueByKey(43, $lastDataArr)?    $lastDataArr[43]['data']    : 0;
                $actRate  = $this->getNumberOfEmptyFloors($lActRate, $cActRate, 1);

                $compDataArr[42] = array(
                    'data'   => $this->getUtilizationRatesTrigona($actRate, 90),
                    'style'  => array('textAlign' => $textAlign_center),
                    'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[42]['status'], $lastDataArr[42]['status'])),
                );
                $compDataArr[43]  = array('data' => $actRate, 'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[42]['status'], $lastDataArr[42]['status'])), 'disp_type' => 'float_percent');
                $raitoDataArr[42] = array('data' => '', 'status' => $this->checkCellDataStatus('comp', array($compDataArr[42]['status'], $lastDataArr[42]['status'])));
                $raitoDataArr[43] = array('data' => '', 'status' => $this->checkCellDataStatus('comp', array($compDataArr[43]['status'], $lastDataArr[43]['status'])));

                # 入院合計 許可病床稼働率
                $cPmtRate = $this->getArrValueByKey(45, $cuurentDataArr)? $cuurentDataArr[45]['data'] : 0;
                $lPmtRate = $this->getArrValueByKey(45, $lastDataArr)?    $lastDataArr[45]['data']    : 0;
                $pmtRate = $this->getNumberOfEmptyFloors($lPmtRate, $cPmtRate, 1);

                $compDataArr[44] = array(
                     'data'   => $this->getUtilizationRatesTrigona($pmtRate, 90),
                     'style'  => array('textAlign' => $textAlign_center),
                     'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[44]['status'], $lastDataArr[44]['status']))
                );
                $compDataArr[45]  = array('data' => $pmtRate, 'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[45]['status'], $lastDataArr[45]['status'])), 'disp_type' => 'float_percent');
                $raitoDataArr[44] = array('data' => '', 'status' => $this->checkCellDataStatus('comp', array($compDataArr[44]['status'], $lastDataArr[44]['status'])));
                $raitoDataArr[45] = array('data' => '', 'status' => $this->checkCellDataStatus('comp', array($compDataArr[45]['status'], $lastDataArr[45]['status'])));

                # 入退院数 入院
                $cNyuin = $this->getArrValueByKey(46, $cuurentDataArr)? $cuurentDataArr[46]['data'] : 0;
                $lNyuin = $this->getArrValueByKey(46, $lastDataArr)?    $lastDataArr[46]['data']    : 0;
                $compDataArr[46] = array(
                    'data'   => $this->getNumberOfEmptyFloors($lNyuin, $cNyuin),
                    'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[46]['status'], $lastDataArr[46]['status']))
                );
                $raitoDataArr[46] = array(
                    'data'      => empty($lNyuin)? '' : $this->getUtilizationRates($compDataArr[46]['data'], $lNyuin, 1),
                    'status'    => $this->checkCellDataStatus('comp', array($compDataArr[46]['status'], $lastDataArr[46]['status'])),
                    'disp_type' => 'float_percent'
                );

                # 入退院数 退院
                $cTaiin = $this->getArrValueByKey(47, $cuurentDataArr)? $cuurentDataArr[47]['data'] : 0;
                $lTaiin = $this->getArrValueByKey(47, $lastDataArr)?    $lastDataArr[47]['data']    : 0;
                $compDataArr[47]  = array('data' => $this->getNumberOfEmptyFloors($lTaiin, $cTaiin), 'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[47]['status'], $lastDataArr[47]['status'])));
                $raitoDataArr[47] = array(
                    'data'      => empty($lTaiin)? '' :$this->getUtilizationRates($compDataArr[47]['data'], $lTaiin),
                    'status'    => $this->checkCellDataStatus('comp', array($compDataArr[47]['status'], $lastDataArr[47]['status'])),
                    'disp_type' => 'float_percent'
                );

                foreach ($departmentType as $dTypeKey => $dTypeVal) {
                    $columnNum = $dTypeKey * 3 + 1;
                    $tmpC = $this->getArrValueByKey($columnNum, $cuurentDataArr)? $cuurentDataArr[$columnNum]['data'] : null;
                    $tmpL = $this->getArrValueByKey($columnNum, $lastDataArr)?    $lastDataArr[$columnNum]['data']    : null;
                    $compDataArr[$columnNum]  = array(
                        'data'   => (is_null($tmpL) && is_null($tmpC))? null : $this->getNumberOfEmptyFloors($tmpL, $tmpC, 1),
                        'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[$columnNum]['status'], $lastDataArr[$columnNum]['status']))
                    );
                    $raitoDataArr[$columnNum] = array(
                        'data'      => is_null($tmpL)? null : $this->getUtilizationRates($compDataArr[$columnNum]['data'], $tmpL, 1),
                        'status'    => $this->checkCellDataStatus('comp', array($compDataArr[$columnNum]['status'], $lastDataArr[$columnNum]['status'])),
                        'disp_type' => 'float_percent'
                    );

                    $tmpC = $this->getArrValueByKey($columnNum+1, $cuurentDataArr)? $cuurentDataArr[$columnNum+1]['data'] : null;
                    $tmpL = $this->getArrValueByKey($columnNum+1, $lastDataArr)?    $lastDataArr[$columnNum+1]['data']    : null;
                    $compDataArr[$columnNum+1] = array(
                        'data'   => (is_null($tmpL) && is_null($tmpC))? null : $this->getNumberOfEmptyFloors($tmpL, $tmpC, 1),
                        'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[$columnNum+1]['status'], $lastDataArr[$columnNum+1]['status']))
                    );
                    $raitoDataArr[$columnNum+1] = array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($compDataArr[$columnNum+1]['status'], $lastDataArr[$columnNum+1]['status'])));

                    $tmpC = $this->getArrValueByKey($columnNum+2, $cuurentDataArr)? $cuurentDataArr[$columnNum+2]['data'] : null;
                    $tmpL = $this->getArrValueByKey($columnNum+2, $lastDataArr)?    $lastDataArr[$columnNum+2]['data']    : null;
                    $compDataArr[$columnNum+2] = array(
                        'data'   => (is_null($tmpL) && is_null($tmpC))? null : $this->getNumberOfEmptyFloors($tmpL, $tmpC, 1).'%',
                        'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[$columnNum+2]['status'], $lastDataArr[$columnNum+2]['status']))
                    );
                    $raitoDataArr[$columnNum+2] = array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($compDataArr[$columnNum+2]['status'], $lastDataArr[$columnNum+2]['status'])));
                }
            }

            for ($i=0; $i <= 47; $i++ ) {
                if (!$this->getArrValueByKey($i, $cuurentDataArr)) { $cuurentDataArr[$i] = array('data' => null, 'status' => $this->checkCellDataStatus('monthly_report_comp', array('year' => $year, 'month' => $month))); }
                if (!$this->getArrValueByKey($i, $lastDataArr))    { $lastDataArr[$i]    = array('data' => null, 'status' => $this->checkCellDataStatus('monthly_report_comp', array('year' => ($year-1), 'month' => $month))); }
                if (!$this->getArrValueByKey($i, $compDataArr))    { $compDataArr[$i]    = array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($cuurentDataArr[$i]['status'], $lastDataArr[$i]['status']))); }
                if (!$this->getArrValueByKey($i, $raitoDataArr))   { $raitoDataArr[$i]   = array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($compDataArr[$i]['status'], $lastDataArr[$i]['status']))); }
            }

            $rtnStr .= $currentHeader.$this->convertCellData($cuurentDataArr, $fontTag, array('textAlign' => $textAlign_right)).'</tr>'
                      .$lastHeader.$this->convertCellData($lastDataArr, $fontTag, array('textAlign' => $textAlign_right)).'</tr>'
                      .$compHeader.$this->convertCellData($compDataArr, $fontTag, array('textAlign' => $textAlign_right, 'bgColor' => $bgColor_green)).'</tr>'
                      .$raitoHeader.$this->convertCellData($raitoDataArr, $fontTag, array('textAlign' => $textAlign_right, 'bgColor' => $bgColor_green)).'</tr>';
        }

        for ($i=0; $i <= 47; $i++) {
            if (!$this->getArrValueByKey($i, $totalDataArr['current'])) {
                switch ($i) {
                    case '2': case '5': case '8': case '11': case '14': case '17': case '20': case '23': case '26': case '29': case '32': case '35': case '38':
                        $totalDataArr['current'][$i] = array(
                            'data'   => (is_null($totalDataArr['current'][$i-1]['data']) && is_null($totalDataArr['current'][$i-1]['act']))? null : $this->getNumberOfEmptyFloors($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        $totalDataArr['current'][$i+1] = array(
                            'data'   => is_null($totalDataArr['current'][$i-1]['act'])? null : $this->getUtilizationRates($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+1]['status'], $totalDataArr['current'][$i-1]['status'])),
                            'disp_type' => 'float_percent'
                        );
                        break;

                    case '41':
                        $totalDataArr['current'][$i] = array(
                            'data'   => (is_null($totalDataArr['current'][$i-1]['data']) && is_null($totalDataArr['current'][$i-1]['act']))? null : $this->getNumberOfEmptyFloors($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        $totalDataArr['current'][$i+2] = array(
                            'data'   => is_null($totalDataArr['current'][$i-1]['act'])? null : $this->getUtilizationRates($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+2]['status'], $totalDataArr['current'][$i-1]['status'])), 'disp_type' => 'float_percent'
                        );
                        $totalDataArr['current'][$i+4] = array(
                            'data'   => is_null($totalDataArr['current'][$i-1]['pmt'])? null : $this->getUtilizationRates($totalDataArr['current'][$i-1]['data'], $totalDataArr['current'][$i-1]['pmt'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+4]['status'], $totalDataArr['current'][$i-1]['status'])), 'disp_type' => 'float_percent'
                        );
                        $totalDataArr['current'][$i+1] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['current'][$i+2]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+1]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        $totalDataArr['current'][$i+3] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['current'][$i+4]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+1]['status'], $totalDataArr['current'][$i-1]['status'])),
                        );
                        break;

                    default:
                        $totalDataArr['current'][$i]= array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['current'][$i-1]['status'])));
                        break;
                }
            }
            if (!$this->getArrValueByKey($i, $totalDataArr['last'])) {
                switch ($i) {
                    case '2': case '5': case '8': case '11': case '14': case '17': case '20': case '23': case '26': case '29': case '32': case '35': case '38':
                        $totalDataArr['last'][$i] = array(
                            'data'   => (is_null($totalDataArr['last'][$i-1]['data']) && is_null($totalDataArr['last'][$i-1]['act']))? null : $this->getNumberOfEmptyFloors($totalDataArr['last'][$i-1]['data'], $totalDataArr['last'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i]['status'], $totalDataArr['last'][$i-1]['status']))
                        );
                        $totalDataArr['last'][$i+1]= array(
                            'data'   => is_null($totalDataArr['last'][$i-1]['act'])? null : $this->getUtilizationRates($totalDataArr['last'][$i-1]['data'], $totalDataArr['last'][$i-1]['act'], 1).'%',
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i+1]['status'], $totalDataArr['last'][$i-1]['status'])), 'disp_type' => 'float_percent'
                        );
                        break;

                    case '41':
                        $totalDataArr['last'][$i] = array(
                            'data'   => (is_null($totalDataArr['last'][$i-1]['data']) && is_null($totalDataArr['last'][$i-1]['act']))? null : $this->getNumberOfEmptyFloors($totalDataArr['last'][$i-1]['data'], $totalDataArr['last'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i]['status'], $totalDataArr['last'][$i-1]['status']))
                        );
                        $totalDataArr['last'][$i+2] = array(
                            'data'   => is_null($totalDataArr['last'][$i-1]['act'])? null : $this->getUtilizationRates($totalDataArr['last'][$i-1]['data'], $totalDataArr['last'][$i-1]['act'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i+2]['status'], $totalDataArr['last'][$i-1]['status'])), 'disp_type' => 'float_percent'
                        );
                        $totalDataArr['last'][$i+4] = array(
                            'data'   => is_null($totalDataArr['last'][$i-1]['pmt'])? null : $this->getUtilizationRates($totalDataArr['last'][$i-1]['data'], $totalDataArr['last'][$i-1]['pmt'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i+4]['status'], $totalDataArr['last'][$i-1]['status'])), 'disp_type' => 'float_percent'
                        );
                        $totalDataArr['last'][$i+1] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['last'][$i+2]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i+1]['status'], $totalDataArr['last'][$i-1]['status']))
                        );
                        $totalDataArr['last'][$i+3] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['last'][$i+4]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i+3]['status'], $totalDataArr['last'][$i-1]['status']))
                        );
                        break;

                    default:
                        $totalDataArr['last'][$i] = array('data' => null, 'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i]['status'], $totalDataArr['last'][$i-1]['status'])));
                        break;
                }
            }
            if (!$this->getArrValueByKey($i, $totalDataArr['comp'])) {
                switch ($i) {
                    case '3': case '6': case '9': case '12': case '15': case '18': case '21': case '24': case '27': case '30': case '33': case '36': case '39':
                        $totalDataArr['comp'][$i] = array(
                            'data'   => (is_null($totalDataArr['last'][$i]['data']) && is_null($totalDataArr['current'][$i]['data']))? null : $this->getNumberOfEmptyFloors($totalDataArr['last'][$i]['data'], $totalDataArr['current'][$i]['data'], 1).'%',
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['last'][$i]['status'])), 'disp_type' => 'float_percent'
                        );
                        break;

                    case '42':
                        $totalDataArr['comp'][$i+1] = array(
                            'data'   => (is_null($totalDataArr['last'][$i+1]['data']) && is_null($totalDataArr['current'][$i+1]['data']))? null : $this->getNumberOfEmptyFloors($totalDataArr['last'][$i+1]['data'], $totalDataArr['current'][$i+1]['data'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+1]['status'], $totalDataArr['last'][$i+1]['status'])), 'disp_type' => 'float_percent'
                        );
                        $totalDataArr['comp'][$i+3] = array(
                            'data'   => (is_null($totalDataArr['last'][$i+3]['data']) && is_null($totalDataArr['current'][$i+3]['data']))? null : $this->getNumberOfEmptyFloors($totalDataArr['last'][$i+3]['data'], $totalDataArr['current'][$i+3]['data'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+3]['status'], $totalDataArr['last'][$i+3]['status'])), 'disp_type' => 'float_percent'
                        );
                        $totalDataArr['comp'][$i] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['comp'][$i+1]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['last'][$i]['status']))
                        );
                        $totalDataArr['comp'][$i+2] = array(
                            'data'   => $this->getUtilizationRatesTrigona($totalDataArr['comp'][$i+3]['data'], 90), 'style' => array('textAlign' => $textAlign_center),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i+2]['status'], $totalDataArr['last'][$i+2]['status']))
                        );
                        break;

                    default:
                        $totalDataArr['comp'][$i] = array(
                            'data'   => (is_null($totalDataArr['last'][$i]['data']) && is_null($totalDataArr['current'][$i]['data']))? null : $this->getNumberOfEmptyFloors($totalDataArr['last'][$i]['data'], $totalDataArr['current'][$i]['data'], 1),
                            'status' => $this->checkCellDataStatus('comp', array($totalDataArr['current'][$i]['status'], $totalDataArr['last'][$i]['status']))
                        );
                        break;
                }


            }
            if (!$this->getArrValueByKey($i, $totalDataArr['raito'])) {
                switch ($i) {
                    case  '0': case  '1': case  '4': case  '7': case '10': case '13': case '16': case '19': case '22':
                    case '25': case '28': case '31': case '34': case '37': case '40': case '46': case '47':
                        $totalDataArr['raito'][$i] = array(
                            'data'      => (empty($totalDataArr['last'][$i]['data'])? '' : $this->getUtilizationRates($totalDataArr['comp'][$i]['data'], $totalDataArr['last'][$i]['data'], 1)),
                            'status'    => $this->checkCellDataStatus('comp', array($totalDataArr['comp'][$i]['status'], $totalDataArr['last'][$i]['status'])),
                            'disp_type' => 'float_percent'
                        );
                        break;

                    default:
                        $totalDataArr['raito'][$i] = array('data' => '', 'status' => $this->checkCellDataStatus('comp', array($totalDataArr['comp'][$i]['status'], $totalDataArr['last'][$i]['status'])));
                        break;
                }
            }
        }

        $totalHeader = '<tr>'.$this->convertCellData(
            array(0 => array('data' => '合計', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue)),
                  1 => array('data' => $currentYear.'年度'),
            ),
            $fontTag,
            array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
        );

        $rtnStr .= $totalHeader.$this->convertCellData($totalDataArr['current'], $fontTag, array('textAlign' => $textAlign_right)).'</tr>'
                  .$lastHeader.$this->convertCellData($totalDataArr['last'], $fontTag, array('textAlign' => $textAlign_right)).'</tr>'
                  .$compHeader.$this->convertCellData($totalDataArr['comp'], $fontTag, array('textAlign' => $textAlign_right, 'bgColor' => $bgColor_green)).'</tr>'
                  .$raitoHeader.$this->convertCellData($totalDataArr['raito'], $fontTag, array('textAlign' => $textAlign_right, 'bgColor' => $bgColor_green)).'</tr>';

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * 病院別患者日報(累積統計表)のデータ出力処理
     * @param  array $dataArr
     * @param  array $plantArr
     * @param  array $arrx
     * @param  array $dateArr
     * @return string
     */
    function getDailyReportAccumulationStatistics($dataArr, $plantArr, $arr, $dateArr)
    {
        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();

        $rtnStr  = '';
        $fontTag = $this->getFontTag();

        $targetYear    = $dateArr['year'];
        $targetMonth   = $dateArr['month'];
        $targetDay     = $dateArr['day'];
        $rgbColor_blue = $this->getRgbColor('blue');

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_11 = array('name' => 'colspan', 'value' => 11);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $mmdd = sprintf('%02d月%02d日', $targetMonth, $targetDay);

        $header = '<table border="0" width="100%" >'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => sprintf('%04d年%02d月%02d日', $targetYear, $targetMonth, $targetDay), 'style' => array('textAlign' => $textAlign_right)))
                           , $fontTag
                           , array()
                         )
                     .'</tr>'
                 .'</table>'
                 .'<table class="list this_table" width="100%" >'
                     .'<tr>'
                         .$this->convertCellData(
                             array(
                                 0 => array('data' => '病院名',     'attr' => array($rowspan_3))
                               , 1 => array('data' => '外来患者数', 'attr' => array($rowspan_2, $colspan_2))
                               , 2 => array('data' => '入院合計',   'attr' => array($colspan_11))
                               , 3 => array('data' => '入退院数',   'attr' => array($colspan_4)),
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(
                                 0 => array('data' => '患者数',         'attr' => array($colspan_2))
                               , 1 => array('data' => '空床数',         'attr' => array($rowspan_2))
                               , 2 => array('data' => '稼働病床稼働率', 'attr' => array($colspan_4))
                               , 3 => array('data' => '許可病床稼働率', 'attr' => array($colspan_4))
                               , 4 => array('data' => '入院',           'attr' => array($colspan_2))
                               , 5 => array('data' => '退院',           'attr' => array($colspan_2))
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(
                                  0 => array('data' => $mmdd),  1 => array('data' => '(月平均)')
                                , 2 => array('data' => $mmdd),  3 => array('data' => '(月平均)')
                                , 4 => array('data' => $mmdd, 'attr' => array($colspan_2)), 5 => array('data' => '(月平均)', 'attr' => array($colspan_2))
                                , 6 => array('data' => $mmdd, 'attr' => array($colspan_2)), 7 => array('data' => '(月平均)', 'attr' => array($colspan_2))
                                , 8 => array('data' => $mmdd),  9 => array('data' => '(月累積)')
                               , 10 => array('data' => $mmdd), 11 => array('data' => '(月累積)')
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';

        $rtnStr .= $header;

        $totalGairai          = null;
        $totalGairaiAvg       = null;
        $totalPatient         = null;
        $totalPatientAvg      = null;
        $totalORateTrigona    = null;
        $totalORateAvgTrigona = null;
        $totalORateAvg        = null;
        $totalPRateTrigona    = null;
        $totalPRate           = null;
        $totalPRateAvgTrigona = null;
        $totalPRateAvg        = null;
        $totalNyuin           = null;
        $totalNyuinSum        = null;
        $totalTaiin           = null;
        $totalTaiinSum        = null;

        $totalStatus = 3;

        $totalGokeiGa       = null;
        $totalGokeiCount    = null;
        $totalGokeiCountSum = null;
        $totalActCount      = null;
        $totalActCountSum   = null;
        $totalPmtCount      = null;
        $totalPmtCountSum   = null;

		$week_day_chk_flg = 0;
		

        foreach ($plantArr as $plantVal) {
            $plantId = $this->getArrValueByKey('facility_id', $plantVal);

            $gairai          = null;
            $gairaiAvg       = null;
            $patient         = null;
            $patientAvg      = null;
            $emptyfloors     = null;
            $oRateTrigona    = null;
            $oRate           = null;
            $oRateAvgTrigona = null;
            $oRateAvg        = null;
            $pRateTrigona    = null;
            $pRate           = null;
            $pRateAvgTrigona = null;
            $pRateAvg        = null;
            $nyuin           = null;
            $nyuinSum        = null;
            $taiin           = null;
            $taiinSum        = null;

            $status = $this->checkCellDataStatus('daily_report_statistics', array('year' => $targetYear, 'month' => $targetMonth, 'target_day' => $targetDay));

            foreach ($dataArr as $dataVal) {
                $dataVal['target_day'] = $targetDay;
                $facilityId = $this->getArrValueByKey('jnl_facility_id', $dataVal);
                if ($plantId == $facilityId) {
                    $status = $this->checkCellDataStatus('daily_report_statistics', $dataVal);

                    $gokeiGa           = $dataVal['gokei_ga'];
                    $gokeiCount        = $dataVal['gokei_count'];
                    $actCount          = $dataVal['act_count'];
                    $pmtCount          = $dataVal['pmt_count'];
                    $dayCount          = $dataVal['day_count'];
                    $dayCountNoHoliday = $dataVal['day_count_no_holiday'];
                    $gokeiGaSum        = $dataVal['sum_gokei_ga'];
                    $gokeiCountSum     = $dataVal['sum_gokei_count'];
                    $actCountSum       = $dataVal['sum_act_count'];
                    $pmtCountSum       = $dataVal['sum_pmt_count'];
					
					//////////////////////
					//////// 対象月の1日が休日であれば固定で分母に１をいれているのでweekdayになるとその1日分が分母が大きくなるので
					//////// その分を減算する
					/////////////////////////
					//月初が休日かどうか判定する
					if ($indicatorData->is_holiday($targetYear, $targetMonth, "01")) 
					{
						//休日である
						
						//対象月の2日目のweekdayの日付を取得する
						for ($checkm=2; $checkm < 32; $checkm++) 
						{
					       $check_day = sprintf('%02d', $checkm);

							//休日かどうか判定する
							if (!$indicatorData->is_holiday($targetYear, $targetMonth, $check_day)) 
							{
								if($check_day <= $targetDay)
								{
									//表示日付が月初が休日で、既にweekdayなので分母から減算する
									$dayCountNoHoliday--;
									break; 
								}
							}
						}
					}
					

                    $gairai          = $gokeiGa;
                    $gairaiAvg       = round($gokeiGaSum / $dayCountNoHoliday, 1);
                    $patient         = $gokeiCount;
                    $patientAvg      = round($gokeiCountSum / $dayCount, 1);
                    $emptyfloors     = (is_null($gokeiCount) && is_null($actCount))? null : $this->getNumberOfEmptyFloors($gokeiCount, $actCount, 1);
                    $oRate           = is_null($actCount)? null : $this->getUtilizationRates($gokeiCount, $actCount, 1);
                    $oRateTrigona    = $this->getUtilizationRatesTrigona($oRate);
                    $oRateAvg        = is_null($actCountSum)? null : $this->getUtilizationRates($gokeiCountSum, $actCountSum, 1);
                    $oRateAvgTrigona = $this->getUtilizationRatesTrigona($oRateAvg);
                    $pRate           = is_null($pmtCount)? null : $this->getUtilizationRates($gokeiCount, $pmtCount, 1);
                    $pRateTrigona    = $this->getUtilizationRatesTrigona($pRate);
                    $pRateAvg        = $this->getUtilizationRates($gokeiCountSum, $pmtCountSum, 1);
                    $pRateAvgTrigona = $this->getUtilizationRatesTrigona($pRateAvg);
                    $nyuin           = $dataVal['nyuin'];
                    $nyuinSum        = $dataVal['sum_nyuin'];
                    $taiin           = $dataVal['taiin'];
                    $taiinSum        = $dataVal['sum_taiin'];

                    $totalGokeiGa           = $totalGokeiGa           + $gokeiGa;
                    
					//全施設の平均を表示するのをやめて、全施設の合計を表示するように仕様変更しました
					//$totalGokeiGaSum        = $totalGokeiGaSum        + $gokeiGaSum;
					//$totalGokeiCountSum     = $totalGokeiCountSum     + $gokeiCountSum;
					$totalGokeiGaSum        = $totalGokeiGaSum        + $gairaiAvg;
					$totalGokeiCountSum     = $totalGokeiCountSum     + $patientAvg;
					
					
                    $totalGokeiCount        = $totalGokeiCount        + $gokeiCount;
                    $totalDayCount          = $totalDayCount          + $dayCount;
                    $totalDayCountNoHoliday = $totalDayCountNoHoliday + $dayCountNoHoliday;
                    $totalActCount          = $totalActCount          + $actCount;
                    $totalActCountSum       = $totalActCountSum       + $actCountSum;
                    $totalPmtCount          = $totalPmtCount          + $pmtCount;
                    $totalPmtCountSum       = $totalPmtCountSum       + $pmtCountSum;
                    $totalGairai            = $totalGairai            + $gairai;
                    $totalPatient           = $totalPatient           + $patient;
                    $totalNyuin             = $totalNyuin             + $nyuin;
                    $totalNyuinSum          = $totalNyuinSum          + $nyuinSum;
                    $totalTaiin             = $totalTaiin             + $taiin;
                    $totalTaiinSum          = $totalTaiinSum          + $taiinSum;
                }
            }

            $totalStatus = $this->checkCellDataStatus('comp', array($totalStatus, $status));

            $tdArr = array(
                 0 => array(
                     'data'  => $this->getArrValueByKey('facility_name', $plantVal)
                   , 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap),
                 )
               , 1 => array('data' => $gairai,          'status' => $status)
               , 2 => array('data' => $gairaiAvg,       'status' => $status, 'disp_type' => 'float_parenthesis')
               , 3 => array('data' => $patient,         'status' => $status)
               , 4 => array('data' => $patientAvg,      'status' => $status, 'disp_type' => 'float_parenthesis')
               , 5 => array('data' => $emptyfloors,     'status' => $status)
               , 6 => array('data' => $oRateTrigona,    'status' => $status)
               , 7 => array('data' => $oRate,           'status' => $status, 'disp_type' => 'float_percent')
               , 8 => array('data' => $oRateAvgTrigona, 'status' => $status)
               , 9 => array('data' => $oRateAvg,        'status' => $status, 'disp_type' => 'float_percent_parenthesis')
              , 10 => array('data' => $pRateTrigona,    'status' => $status)
              , 11 => array('data' => $pRate,           'status' => $status, 'disp_type' => 'float_percent')
              , 12 => array('data' => $pRateAvgTrigona, 'status' => $status)
              , 13 => array('data' => $pRateAvg,        'status' => $status, 'disp_type' => 'float_percent_parenthesis')
              , 14 => array('data' => $nyuin,           'status' => $status)
              , 15 => array('data' => $nyuinSum,        'status' => $status, 'disp_type' => 'parenthesis')
              , 16 => array('data' => $taiin,           'status' => $status)
              , 17 => array('data' => $taiinSum,        'status' => $status, 'disp_type' => 'parenthesis')
            );

            $rtnStr .= '<tr>'. $this->convertCellData($tdArr, $fontTag, array('textAlign' => $textAlign_right)). '</tr>';
        }


//        $totalGairaiAvg       = empty($totalDayCount)? null : round($totalGokeiGaSum / $totalDayCountNoHoliday, 1);
//        $totalPatientAvg      = empty($totalDayCount)? null : round($totalGokeiCountSum / $totalDayCount, 1);
//全施設の平均を表示するのをやめて、全施設の合計を表示するように仕様変更しました
		$totalGairaiAvg       = empty($totalDayCount)? null : $totalGokeiGaSum;
		$totalPatientAvg      = empty($totalDayCount)? null :$totalGokeiCountSum;
		

        $totalEmptyfloors     = $this->getNumberOfEmptyFloors($totalGokeiCount, $totalActCount, 1);
        $totalORate           = empty($totalActCount)? null : $this->getUtilizationRates($totalGokeiCount, $totalActCount, 1);
        $totalORateTrigona    = $this->getUtilizationRatesTrigona($totalORate);
        $totalORateAvg        = empty($totalActCountSum)? null : $this->getUtilizationRates($totalGokeiCountSum, $totalActCountSum, 1);
        $totalORateAvgTrigona = $this->getUtilizationRatesTrigona($totalORateAvg);
        $totalPRate           = empty($totalPmtCount)? null : $this->getUtilizationRates($totalGokeiCount, $totalPmtCount, 1);
        $totalPRateTrigona    = $this->getUtilizationRatesTrigona($totalPRate);
        $totalPRateAvg        = empty($totalPmtCountSum)? null : $this->getUtilizationRates($totalGokeiCountSum, $totalPmtCountSum, 1);
        $totalPRateAvgTrigona = $this->getUtilizationRatesTrigona($totalPRateAvg);

        $totalTdArr = array(0 => array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap) )
                          , 1 => array('data' => $totalGairai,          'status' => $totalStatus)
                          , 2 => array('data' => $totalGairaiAvg,       'status' => $totalStatus, 'disp_type' => 'float_parenthesis')
                          , 3 => array('data' => $totalPatient,         'status' => $totalStatus)
                          , 4 => array('data' => $totalPatientAvg,      'status' => $totalStatus, 'disp_type' => 'float_parenthesis')
                          , 5 => array('data' => empty($totalEmptyfloors)? null : $totalEmptyfloors, 'status' => $totalStatus)
                          , 6 => array('data' => $totalORateTrigona,    'status' => $totalStatus)
                          , 7 => array('data' => $totalORate,           'status' => $totalStatus, 'disp_type' => 'float_percent')
                          , 8 => array('data' => $totalORateAvgTrigona, 'status' => $totalStatus)
                          , 9 => array('data' => $totalORateAvg,        'status' => $totalStatus, 'disp_type' => 'float_percent_parenthesis')
                         , 10 => array('data' => $totalPRateTrigona,    'status' => $totalStatus)
                         , 11 => array('data' => $totalPRate,           'status' => $totalStatus, 'disp_type' => 'float_percent')
                         , 12 => array('data' => $totalPRateAvgTrigona, 'status' => $totalStatus)
                         , 13 => array('data' => $totalPRateAvg,        'status' => $totalStatus, 'disp_type' => 'float_percent_parenthesis')
                         , 14 => array('data' => $totalNyuin,           'status' => $totalStatus)
                         , 15 => array('data' => $totalNyuinSum,        'status' => $totalStatus, 'disp_type' => 'parenthesis')
                         , 16 => array('data' => $totalTaiin,           'status' => $totalStatus)
                         , 17 => array('data' => $totalTaiinSum,        'status' => $totalStatus, 'disp_type' => 'parenthesis')
        );
        $rtnStr .= '<tr>' .$this->convertCellData($totalTdArr, $fontTag, array('textAlign' => $textAlign_right)). '</tr></table>';
        return $rtnStr;
    }

    /**
     * 月報データの前年度比各画面表示出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
	 * @param  array  $optionArr
     * @return string
     */
	function getMonthlyReportOfComparisonInThePreviousYear($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = '';
        $fontTag = $this->getFontTag();

        $totalNameStr   = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr);
        $averageNameStr = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr);
        $amgTotalFlg    = $this->getArrayValueByKeyForEmptyIsNull('amg_total',   $arr);
        $amgAverageFlg  = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr);

        # attr
        $colspan_2         = array('name' => 'colspan',          'value' => '2');
        $rowspan_3         = array('name' => 'rowspan',          'value' => '3');
        $rowspan_4         = array('name' => 'rowspan',          'value' => '4');

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_green     = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

/*
        $startMonth  = $this->getFiscalYearStartMonth();
        $currentYear = $this->getCurrentYear();
        $lastYear    = $this->getLastYear();
*/

		if($optionArr == null)
			{$currentYear = $this->getCurrentYear();}
		else
			{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
		$lastYear		= $currentYear - 1;
		$startMonth	= $this->getFiscalYearStartMonth();
		

        $monthArr = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)));
        for ($m=0; $m < 12; $m++) {
            $month = (($startMonth + $m) <= 12)? ($startMonth + $m) : ($startMonth + $m) - 12;
            $monthArr[] = array('data' => $month.'月');
        }

        if (!is_null($totalNameStr))   { $monthArr[] = array('data' => '病院合計'); }
        if (!is_null($averageNameStr)) { $monthArr[] = array('data' => '病院平均'); }

        $header = '<table class="list this_table" width="100%" ><tr>'.$this->convertCellData($monthArr, $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
        $rtnStr .= $header;

        $amgTotalDataArr        = array();
        $amgAverageDataArr      = array();
        $currentTotalMonthCount = 0;
        $lastTotalMonthCount    = 0;
        $currentPlantCount      = array();
        $lastPlantCount         = array();

        foreach ($plantArr as $plantVal) {
            $plantId = $this->getArrValueByKey('facility_id', $plantVal);
            $currentDataArr = array();
            $lastDataArr    = array();
            $compDataArr    = array();
            $raitoDataArr   = array();

            $currentDataArr = array(
                0 => array(
                    'data'  => $this->getArrValueByKey('facility_name', $plantVal)
                  , 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)
                  , 'attr'  => array($rowspan_4)
                ),
                1 => array('data' => $currentYear .'年度', 'style' => array('textAlign' => $textAlign_center))
            );

            $lastDataArr  = array(0 => array('data' => $lastYear.'年度', 'style' => array('textAlign' => $textAlign_center)));
            $compDataArr  = array(0 => array('data' => '増減',           'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_green)));
            $raitoDataArr = array(0 => array('data' => '増減率',         'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_green)));

            $totalData     = array();
            $averageData   = array();

            $currentStatus = 3;
            $lastStatus    = 3;
            $compStatus    = 3;
            $raitoStatus   = 3;

            $currentYearMonthCount = 0;
            $lastYearMonthCount    = 0;

            for ($m=0; $m < 12; $m++) {
                $targetMonth       = $m + $startMonth;
                $targetCurrentYear = ($targetMonth <= 12)? $currentYear : $currentYear + 1;
                $targetLastYear    = ($targetMonth <= 12)? $lastYear    : $lastYear    + 1;
                $targetMonth       = ($targetMonth <= 12)? $targetMonth : $targetMonth - 12;

                $currentData = 0;
                $lastData    = 0;
                $compData    = 0;
                $raitoData   = 0;

                $currentStatus = 3;
                $lastStatus    = 3;
                $compStatus    = 3;
                $raitoStatus   = 3;

                $currentStatus = $this->checkCellDataStatus('monthly_report_of_comparison_in_the_previous_year', array('year' => $targetCurrentYear, 'main_data' => array('month' => $targetMonth)));
                $lastStatus    = $this->checkCellDataStatus('monthly_report_of_comparison_in_the_previous_year', array('year' => $targetLastYear,    'main_data' => array('month' => $targetMonth)));

                foreach ($dataArr as $dataArrVal) {
                    if (sprintf('%02d', $targetMonth)==$this->getArrValueByKey('month', $dataArrVal) && $plantId == $this->getArrValueByKey('facility_id', $dataArrVal)) {
                        if (!is_null($this->getArrValueByKey($targetCurrentYear, $dataArrVal))) {
                            $currentData            = $currentData + $this->getArrValueByKey($targetCurrentYear, $dataArrVal);
                            $currentStatus          = $this->checkCellDataStatus('monthly_report_of_comparison_in_the_previous_year', array('year' => $targetCurrentYear, 'main_data' => $dataArrVal));
                            $currentYearMonthCount  = $currentYearMonthCount  + 1;
                            $currentPlantCount[$m]  = $currentPlantCount[$m]  + 1;
                        }
                        if (!is_null($this->getArrValueByKey($targetLastYear, $dataArrVal))) {
                            $lastData            = $lastData + $this->getArrValueByKey($targetLastYear, $dataArrVal);
                            $lastStatus          = $this->checkCellDataStatus('monthly_report_of_comparison_in_the_previous_year', array('year' => $targetLastYear, 'main_data' => $dataArrVal));
                            $lastYearMonthCount  = $lastYearMonthCount  + 1;
                            $lastPlantCount[$m]  = $lastPlantCount[$m]  + 1;
                        }
                    }
                }

                $compStatus  = $this->checkCellDataStatus('comp', array($currentStatus, $lastStatus));
                $raitoStatus = $this->checkCellDataStatus('comp', array($compStatus,    $lastStatus));
                $compData    = $this->getNumberOfEmptyFloors($lastData, $currentData, 1);
                $raitoData   = $this->getUtilizationRates($compData, $lastData, 1);

                # 病院合計
				if (($arr["name"] == "一般病棟平均在院日数") 
						|| ($arr["name"] == "救急受入率")
						|| ($arr["name"] == "救急入院率")
						|| ($arr["name"] == "診療時間外入院率")
						|| ($arr["name"] == "紹介率"))
				{
					$totalData['current'] = array('data' => $totalData['current']['data'] + $currentData, 'status' => $this->checkCellDataStatus('comp', array($totalData['current']['status'], $currentStatus)), 'disp_type' => 'float');
					$totalData['last']    = array('data' => $totalData['last']['data'] + $lastData, 'status' => $this->checkCellDataStatus('comp', array($totalData['last']['status'], $lastStatus)), 'disp_type' => 'float');
					$totalData['comp']    = array('data' => $this->getNumberOfEmptyFloors($totalData['last']['data'], $totalData['current']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($totalData['current']['status'], $totalData['last']['status'])), 'disp_type' => 'float');
					$totalData['raito']   = array('data' => $this->getUtilizationRates($totalData['comp']['data'], $totalData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($totalData['comp']['status'], $totalData['last']['status'])), 'disp_type' => 'float_percent');
				}else{
					$totalData['current'] = array('data' => $totalData['current']['data'] + $currentData, 'status' => $this->checkCellDataStatus('comp', array($totalData['current']['status'], $currentStatus)));
					$totalData['last']    = array('data' => $totalData['last']['data'] + $lastData, 'status' => $this->checkCellDataStatus('comp', array($totalData['last']['status'], $lastStatus)));
					$totalData['comp']    = array('data' => $this->getNumberOfEmptyFloors($totalData['last']['data'], $totalData['current']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($totalData['current']['status'], $totalData['last']['status'])));
					$totalData['raito']   = array('data' => $this->getUtilizationRates($totalData['comp']['data'], $totalData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($totalData['comp']['status'], $totalData['last']['status'])), 'disp_type' => 'float_percent');
				}

                # 病院平均
                $averageData['current'] = array('data' => round(($totalData['current']['data']) / $currentYearMonthCount, 1), 'status' => $this->checkCellDataStatus('comp', array($averageData['current']['status'], $currentStatus)), 'disp_type' => 'float');
                $averageData['last']    = array('data' => round(($totalData['last']['data']) / $lastYearMonthCount, 1), 'status' => $this->checkCellDataStatus('comp', array($averageData['last']['status'], $lastStatus)), 'disp_type' => 'float');
                $averageData['comp']    = array('data' => $this->getNumberOfEmptyFloors($averageData['last']['data'], $averageData['current']['data'] ,1), 'status' => $this->checkCellDataStatus('comp', array($averageData['current']['status'], $averageData['last']['status'])), 'disp_type' => 'float');
                $averageData['raito']   = array('data' => $this->getUtilizationRates($averageData['comp']['data'], $averageData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($averageData['comp']['status'], $averageData['last']['status'])), 'disp_type' => 'float_percent');

                # AMG合計
                $amgTotalDataArr['current'][$m] = array('data' => $amgTotalDataArr['current'][$m]['data'] + $currentData, 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['current'][$m]['status'], $currentStatus)));
                $amgTotalDataArr['last'][$m]    = array('data' => $amgTotalDataArr['last'][$m]['data'] + $lastData, 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['last'][$m]['status'], $lastStatus)));
                $amgTotalDataArr['comp'][$m]    = array('data' => $this->getNumberOfEmptyFloors($amgTotalDataArr['last'][$m]['data'], $amgTotalDataArr['current'][$m]['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['current'][$m]['status'], $amgTotalDataArr['last'][$m]['status'])));
                $amgTotalDataArr['raito'][$m]   = array('data' => $this->getUtilizationRates($amgTotalDataArr['comp'][$m]['data'], $amgTotalDataArr['last'][$m]['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['comp'][$m]['status'], $amgTotalDataArr['last'][$m]['status'])), 'disp_type' => 'float_percent');

                # AMG平均
				if (($arr["name"] == "一般病棟平均在院日数") 
						|| ($arr["name"] == "救急受入率")
						|| ($arr["name"] == "救急入院率")
						|| ($arr["name"] == "診療時間外入院率")
						|| ($arr["name"] == "紹介率"))
				{
					$amgAverageDataArr['current'][$m] = array('data' => round(($amgTotalDataArr['current'][$m]['data'] / $currentPlantCount[$m]), 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['current'][$m]['status'], $currentStatus)), 'disp_type' => 'float');
					$amgAverageDataArr['last'][$m]    = array('data' => round(($amgTotalDataArr['last'][$m]['data'] / $lastPlantCount[$m]), 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['last'][$m]['status'], $lastStatus)), 'disp_type' => 'float');
					$amgAverageDataArr['comp'][$m]    = array('data' => $this->getNumberOfEmptyFloors($amgAverageDataArr['last'][$m]['data'], $amgAverageDataArr['current'][$m]['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['current'][$m]['status'], $amgTotalDataArr['last'][$m]['status'])), 'disp_type' => 'float');
					$amgAverageDataArr['raito'][$m]   = array('data' => $this->getUtilizationRates($amgAverageDataArr['comp'][$m]['data'], $amgAverageDataArr['last'][$m]['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['comp'][$m]['status'], $amgTotalDataArr['last'][$m]['status'])), 'disp_type' => 'float_percent');
				}else
				{
					$amgAverageDataArr['current'][$m] = array('data' => round(($amgTotalDataArr['current'][$m]['data'] / $currentPlantCount[$m]), 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['current'][$m]['status'], $currentStatus)));
					$amgAverageDataArr['last'][$m]    = array('data' => round(($amgTotalDataArr['last'][$m]['data'] / $lastPlantCount[$m]), 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['last'][$m]['status'], $lastStatus)));
					$amgAverageDataArr['comp'][$m]    = array('data' => $this->getNumberOfEmptyFloors($amgAverageDataArr['last'][$m]['data'], $amgAverageDataArr['current'][$m]['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['current'][$m]['status'], $amgTotalDataArr['last'][$m]['status'])));
					$amgAverageDataArr['raito'][$m]   = array('data' => $this->getUtilizationRates($amgAverageDataArr['comp'][$m]['data'], $amgAverageDataArr['last'][$m]['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalDataArr['comp'][$m]['status'], $amgTotalDataArr['last'][$m]['status'])), 'disp_type' => 'float_percent');
				}


                # AMG合計 病院合計
                $amgTotalSideTotalData['current'] = array('data' => $amgTotalSideTotalData['current']['data'] + $currentData, 'status' => $this->checkCellDataStatus('comp', array($amgTotalSideTotalData['current']['status'], $currentStatus)));
                $amgTotalSideTotalData['last']    = array('data' => $amgTotalSideTotalData['last']['data'] + $lastData, 'status' => $this->checkCellDataStatus('comp', array($amgTotalSideTotalData['last']['status'], $lastStatus)));
                $amgTotalSideTotalData['comp']    = array('data' => $this->getNumberOfEmptyFloors($amgTotalSideTotalData['last']['data'], $amgTotalSideTotalData['current']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalSideTotalData['comp']['status'], $compStatus)));
                $amgTotalSideTotalData['raito']   = array('data' => $this->getUtilizationRates($amgTotalSideTotalData['comp']['data'], $amgTotalSideTotalData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgTotalSideTotalData['raito']['status'], $raitoStatus)), 'disp_type' => 'float_percent');

                # AMG合計 病院平均
                $amgTotalSideAverageData['current'] = array('data' => round(($amgTotalSideTotalData['current']['data'] / $this->getDenominatorOfAverageValueOfAverageValue($currentPlantCount)), 1), 'status' => $this->checkCellDataStatus('comp',array($amgTotalSideAverageData['current']['status'], $currentStatus)), 'disp_type' => 'float');
                $amgTotalSideAverageData['last']    = array('data' => round(($amgTotalSideTotalData['last']['data'] / $this->getDenominatorOfAverageValueOfAverageValue($lastPlantCount)), 1), 'status' => $this->checkCellDataStatus('comp',array($amgTotalSideAverageData['last']['status'], $lastStatus)), 'disp_type' => 'float');
                $amgTotalSideAverageData['comp']    = array('data' => $this->getNumberOfEmptyFloors($amgTotalSideAverageData['last']['data'], $amgTotalSideAverageData['current']['data'], 1), 'status' => $this->checkCellDataStatus('comp',array($amgTotalSideAverageData['comp']['status'], $compStatus)), 'disp_type' => 'float');
                $amgTotalSideAverageData['raito']   = array('data' => $this->getUtilizationRates($amgTotalSideAverageData['comp']['data'], $amgTotalSideAverageData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp',array($amgTotalSideAverageData['raito']['status'], $raitoStatus)), 'disp_type' => 'float_percent');

                $amgAverageSideTotalData_current = 0;
                $amgAverageSideTotalData_last    = 0;
                for ($i = 0; $i < 12; $i++) {
                    if ($this->getArrValueByKey($i, $amgAverageDataArr['current'])) { $amgAverageSideTotalData_current = $amgAverageSideTotalData_current + $amgAverageDataArr['current'][$i]['data']; }
                    if ($this->getArrValueByKey($i, $amgAverageDataArr['last']))    { $amgAverageSideTotalData_last    = $amgAverageSideTotalData_last + $amgAverageDataArr['last'][$i]['data']; }
                }

                # AMG平均 病院合計
				if (($arr["name"] == "一般病棟平均在院日数") 
						|| ($arr["name"] == "救急受入率")
						|| ($arr["name"] == "救急入院率")
						|| ($arr["name"] == "診療時間外入院率")
						|| ($arr["name"] == "紹介率"))
				{
					$amgAverageSideTotalData['current'] = array('data' => $amgAverageSideTotalData_current, 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['current']['status'], $currentStatus)), 'disp_type' => 'float');
					$amgAverageSideTotalData['last']    = array('data' => $amgAverageSideTotalData_last, 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['last']['status'], $lastStatus)), 'disp_type' => 'float');
					$amgAverageSideTotalData['comp']    = array('data' => $this->getNumberOfEmptyFloors($amgAverageSideTotalData['last']['data'], $amgAverageSideTotalData['current']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['comp']['status'], $compStatus)), 'disp_type' => 'float');
					$amgAverageSideTotalData['raito']   = array('data' => $this->getUtilizationRates($amgAverageSideTotalData['comp']['data'], $amgAverageSideTotalData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['raito']['status'], $raitoStatus)), 'disp_type' => 'float_percent');
				}
				else
				{
					$amgAverageSideTotalData['current'] = array('data' => $amgAverageSideTotalData_current, 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['current']['status'], $currentStatus)));
					$amgAverageSideTotalData['last']    = array('data' => $amgAverageSideTotalData_last, 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['last']['status'], $lastStatus)));
					$amgAverageSideTotalData['comp']    = array('data' => $this->getNumberOfEmptyFloors($amgAverageSideTotalData['last']['data'], $amgAverageSideTotalData['current']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['comp']['status'], $compStatus)));
					$amgAverageSideTotalData['raito']   = array('data' => $this->getUtilizationRates($amgAverageSideTotalData['comp']['data'], $amgAverageSideTotalData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp', array($amgAverageSideTotalData['raito']['status'], $raitoStatus)), 'disp_type' => 'float_percent');
				}

                # AMG平均 病院平均
                $amgAverageSideAverageData['current'] = array('data' => round(($amgAverageSideTotalData['current']['data'] / $this->getDenominatorOfAverageValueOfAverageValue($currentPlantCount)), 1), 'status' => $this->checkCellDataStatus('comp',array($amgAverageSideAverageData['current']['status'], $currentStatus)), 'disp_type' => 'float');
                $amgAverageSideAverageData['last']    = array('data' => round(($amgAverageSideTotalData['last']['data'] / $this->getDenominatorOfAverageValueOfAverageValue($lastPlantCount)), 1), 'status' => $this->checkCellDataStatus('comp',array($amgAverageSideAverageData['last']['status'], $lastStatus)), 'disp_type' => 'float');
                $amgAverageSideAverageData['comp']    = array('data' => $this->getNumberOfEmptyFloors($amgAverageSideAverageData['last']['data'], $amgAverageSideAverageData['current']['data'], 1), 'status' => $this->checkCellDataStatus('comp',array($amgAverageSideAverageData['comp']['status'], $compStatus)), 'disp_type' => 'float');
                $amgAverageSideAverageData['raito']   = array('data' => $this->getUtilizationRates($amgAverageSideAverageData['comp']['data'], $amgAverageSideAverageData['last']['data'], 1), 'status' => $this->checkCellDataStatus('comp',array($amgAverageSideAverageData['raito']['status'], $raitoStatus)), 'disp_type' => 'float_percent');

				if (($arr["name"] == "一般病棟平均在院日数") 
						|| ($arr["name"] == "救急受入率")
						|| ($arr["name"] == "救急入院率")
						|| ($arr["name"] == "診療時間外入院率")
						|| ($arr["name"] == "紹介率"))
				{
					$currentDataArr[] = array('data' => $currentData, 'status' => $currentStatus, 'disp_type' => 'float');
					$lastDataArr[]    = array('data' => $lastData,    'status' => $lastStatus, 'disp_type' => 'float');
					$compDataArr[]    = array('data' => $compData,    'status' => $compStatus,  'style' => array('bgColor' => $bgColor_green), 'disp_type' => 'float');
					$raitoDataArr[]   = array('data' => $raitoData,   'status' => $raitoStatus, 'style' => array('bgColor' => $bgColor_green), 'disp_type' => 'float_percent');
					
				}else{
					
					$currentDataArr[] = array('data' => $currentData, 'status' => $currentStatus);
					$lastDataArr[]    = array('data' => $lastData,    'status' => $lastStatus);
					$compDataArr[]    = array('data' => $compData,    'status' => $compStatus,  'style' => array('bgColor' => $bgColor_green));
					$raitoDataArr[]   = array('data' => $raitoData,   'status' => $raitoStatus, 'style' => array('bgColor' => $bgColor_green), 'disp_type' => 'float_percent');
				}
            }

            # 病院合計
            if (!is_null($totalNameStr)) {
                $currentDataArr[] = $totalData['current'];
                $lastDataArr[]    = $totalData['last'];
                $compDataArr[]    = $totalData['comp'];
                $raitoDataArr[]   = $totalData['raito'];
            }
            # 病院平均
            if (!is_null($averageNameStr)) {
                $currentDataArr[] = $averageData['current'];
                $lastDataArr[]    = $averageData['last'];
                $compDataArr[]    = $averageData['comp'];
                $raitoDataArr[]   = $averageData['raito'];
            }

            # AMG合計
            if ($amgTotalFlg) {
                # 病院合計
                if (!is_null($totalNameStr)) {
                    $amgTotalDataArr['current'][12] = $amgTotalSideTotalData['current'];
                    $amgTotalDataArr['last'][12]    = $amgTotalSideTotalData['last'];
                    $amgTotalDataArr['comp'][12]    = $amgTotalSideTotalData['comp'];
                    $amgTotalDataArr['raito'][12]   = $amgTotalSideTotalData['raito'];
                }
                # 病院平均
                if (!is_null($averageNameStr)) {
                    $amgTotalDataArr['current'][13] = $amgTotalSideAverageData['current'];
                    $amgTotalDataArr['last'][13]    = $amgTotalSideAverageData['last'];
                    $amgTotalDataArr['comp'][13]    = $amgTotalSideAverageData['comp'];
                    $amgTotalDataArr['raito'][13]   = $amgTotalSideAverageData['raito'];
                }
            }

            # AMG平均
            if ($amgAverageFlg) {
                # 病院合計
                if (!is_null($totalNameStr)) {
                    $amgAverageDataArr['current'][12] = $amgAverageSideTotalData['current'];
                    $amgAverageDataArr['last'][12]    = $amgAverageSideTotalData['last'];
                    $amgAverageDataArr['comp'][12]    = $amgAverageSideTotalData['comp'];
                    $amgAverageDataArr['raito'][12]   = $amgAverageSideTotalData['raito'];
                }
                # 病院平均
                if (!is_null($averageNameStr)) {
                    $amgAverageDataArr['current'][13] = $amgAverageSideAverageData['current'];
                    $amgAverageDataArr['last'][13]    = $amgAverageSideAverageData['last'];
                    $amgAverageDataArr['comp'][13]    = $amgAverageSideAverageData['comp'];
                    $amgAverageDataArr['raito'][13]   = $amgAverageSideAverageData['raito'];
                }
            }

            $plantHtml = '<tr>'.$this->convertCellData($currentDataArr, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                        .'<tr>'.$this->convertCellData($lastDataArr,    $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                        .'<tr>'.$this->convertCellData($compDataArr,    $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>'
                        .'<tr>'.$this->convertCellData($raitoDataArr,   $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>';

            $rtnStr .= $plantHtml;
        }

        # AMG合計
        if ($amgTotalFlg) {
            $amgTotalHtml = '<tr>'.$this->convertCellData(array(0 => array('data' => 'AMG合計', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue)), 1 => array('data' => $currentYear.'年度')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                               .$this->convertCellData($amgTotalDataArr['current'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                           .'<tr>'.$this->convertCellData(array(0 => array('data' => $lastYear.'年度')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                               .$this->convertCellData($amgTotalDataArr['last'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                           .'<tr>'.$this->convertCellData(array(0 => array('data' => '増減')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green))
                               .$this->convertCellData($amgTotalDataArr['comp'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>'
                           .'<tr>'.$this->convertCellData(array(0 => array('data' => '増減率')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green))
                               .$this->convertCellData($amgTotalDataArr['raito'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>';
            $rtnStr .= $amgTotalHtml;
        }

        # AMG平均
        if ($amgAverageFlg) {
            $amgAverageHtml = '<tr>'.$this->convertCellData(array(0 => array('data' => 'AMG平均', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue)), 1 => array('data' => $currentYear.'年度')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                                 .$this->convertCellData($amgAverageDataArr['current'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                             .'<tr>'.$this->convertCellData(array(0 => array('data' => $lastYear.'年度')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                                 .$this->convertCellData($amgAverageDataArr['last'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                             .'<tr>'.$this->convertCellData(array(0 => array('data' => '増減')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green))
                                 .$this->convertCellData($amgAverageDataArr['comp'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>'
                             .'<tr>'.$this->convertCellData(array(0 => array('data' => '増減率')), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green))
                                 .$this->convertCellData($amgAverageDataArr['raito'], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>';
            $rtnStr .= $amgAverageHtml;
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * 病院月間報告書患者数総括のデータ出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function getSummaryOfNumberOfHospitalMonthlyReportPatients($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr = '';
        $fontTag = $this->getFontTag();

        $displayMonth = $this->getDisplayMonth();
        $startMonth   = $this->getFiscalYearStartMonth();
        $cYear        = $this->getCurrentYear();
        $cYear        = ($displayMonth < $startMonth)? $cYear + 1 : $cYear;

        $indicatorData = $this->getIndicatorData();
        $cDayCount     = $indicatorData->getDayCountOfMonth($cYear, $displayMonth);

		//表示月が現在月より過去の場合はMAXの日曜・祝日日数を設定する
		//表示月と現在月が同じ場合は現在日までの日曜・祝日日数を設定する
		$holi_cnt = $indicatorData->getHolidayCount($cYear,$displayMonth);
		

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_8  = array('name' => 'rowspan', 'value' =>  8);
        $rowspan_9  = array('name' => 'rowspan', 'value' =>  9);
        $rowspan_10 = array('name' => 'rowspan', 'value' => 10);
        $rowspan_13 = array('name' => 'rowspan', 'value' => 13);
        $rowspan_14 = array('name' => 'rowspan', 'value' => 14);
        $rowspan_19 = array('name' => 'rowspan', 'value' => 19);
        $rowspan_28 = array('name' => 'rowspan', 'value' => 28);
        $rowspan_65 = array('name' => 'rowspan', 'value' => 65);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_5  = array('name' => 'colspan', 'value' =>  5);

        # style
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $header = '<table width="100%" border="0" >'
                     .'<tr>'
                         .'<td style="text-align: left;" >'
                             .'<table class="list this_table" width="40%" >'
                                 .'<tr>'
                                     .$this->convertCellData(
                                         array(
                                             0 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday))
                                           , 1 => array('data' => ' '.$this->getArrValueByKey('weekday', $cDayCount).'日')
                                           , 2 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday))
                                           , 3 => array('data' => ' '.$this->getArrValueByKey('saturday', $cDayCount).'日')
                                           , 4 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday))
                                           , 5 => array('data' => $this->getArrValueByKey('holiday', $cDayCount).'日')
                                         )
                                       , $fontTag
                                       , array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                                     )
                                 .'</tr>'
                             .'</table>'
                         .'</td>'
                         .'<td style="text-align: right;" >' .sprintf($fontTag, sprintf('%04d年%02d月', $cYear, $displayMonth)). '</td>'
                     .'</tr>'
                 .'</table>';

        $rtnStr .= $header;
        $rtnStr .= '<table class="list this_table" width="100%" ><tr>';

        $plantHeaderArr[] = array('data' => '&nbsp;', 'attr' => array($colspan_5));
        foreach ($plantArr as $plantVal) { $plantHeaderArr[] = array('data' => $this->getArrValueByKey('facility_name', $plantVal)); }
        $plantHeaderArr[] = array('data' => '合計');
        $rtnStr .= $this->convertCellData($plantHeaderArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)).'</tr>';

        # 外来
        $leftSide[1] = array(0 => array('data' => '外来',     'attr' => array($rowspan_2))
                           , 1 => array('data' => '外来合計', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[2] = array(0 => array('data' => '1日平均',  'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));

        # 入院
        $leftSide[3] = array(0 => array('data' => '入院',                            'attr' => array($rowspan_65))
        # 入院 全病棟
                           , 1 => array('data' => '全病棟',                          'attr' => array($rowspan_6))
                           , 2 => array('data' => '入院合計',                        'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[4] = array(0 => array('data' => '1日平均',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[5] = array(0 => array('data' => '定床数',                          'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[6] = array(0 => array('data' => '空床数',                          'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[7] = array(0 => array('data' => '稼働病床稼働率',                  'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[8] = array(0 => array('data' => '許可病床稼働率',                  'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        # 入院 一般病棟
        $leftSide[9]  = array(0 => array('data' => '一般病棟',                       'attr' => array($rowspan_13))
                            , 1 => array('data' => '延べ患者数',                     'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[10] = array(0 => array('data' => '定床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[11] = array(0 => array('data' => '空床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[12] = array(0 => array('data' => '稼働率',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[13] = array(0 => array('data' => '平均在院日数(直近3ヶ月)',        'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[14] = array(0 => array('data' => '新入院患者数',                   'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[15] = array(0 => array('data' => 'DPC対象延べ患者数',              'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[16] = array(0 => array('data' => '重症度・看護必要度割合',         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[17] = array(0 => array('data' => '1日〜14日延べ患者数',            'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[18] = array(0 => array('data' => '15日〜30日延べ患者数',           'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[19] = array(0 => array('data' => '救急医療管理加算算定延べ数',     'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[20] = array(0 => array('data' => '90日超延べ患者数',               'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[21] = array(0 => array('data' => '上記の内特定入院対象延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 障害者病棟
        $leftSide[22] = array(0 => array('data' => '障害者病棟',                     'attr' => array($rowspan_8))
                            , 1 => array('data' => '延べ患者数',                     'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[23] = array(0 => array('data' => '定床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[24] = array(0 => array('data' => '空床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[25] = array(0 => array('data' => '稼働率',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[26] = array(0 => array('data' => '1日〜14日延べ患者数',            'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[27] = array(0 => array('data' => '15日〜30日延べ患者数',           'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[28] = array(0 => array('data' => '90日超延べ患者数',               'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[29] = array(0 => array('data' => '上記の内特定入院対象延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 精神一般病
        $leftSide[30] = array(0 => array('data' => '精神一般病棟', 'attr' => array($rowspan_4))
                            , 1 => array('data' => '延べ患者数',   'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[31] = array(0 => array('data' => '定床数',       'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[32] = array(0 => array('data' => '空床数',       'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[33] = array(0 => array('data' => '稼働率',       'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟
        $leftSide[34] = array(0 => array('data' => '療養病棟',   'attr' => array($rowspan_28))
        # 入院 療養病棟 医療
                            , 1 => array('data' => '医療',       'attr' => array($rowspan_14))
                            , 2 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[35] = array(0 => array('data' => '定床数',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[36] = array(0 => array('data' => '1日平均',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[37] = array(0 => array('data' => '稼働率',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        # 入院 療養病棟 医療 入院基本料
        $leftSide[38] = array(0 => array('data' => '入院基本料', 'attr' => array($rowspan_9))
                            , 1 => array('data' => 'A', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[39] = array(0 => array('data' => 'B', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[40] = array(0 => array('data' => 'C', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[41] = array(0 => array('data' => 'D', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[42] = array(0 => array('data' => 'E', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[43] = array(0 => array('data' => 'F', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[44] = array(0 => array('data' => 'G', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[45] = array(0 => array('data' => 'H', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[46] = array(0 => array('data' => 'I', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[47] = array(0 => array('data' => '1日〜14日延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟 介護
        $leftSide[48] = array(0 => array('data' => '介護', 'attr' => array($rowspan_10))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[49] = array(0 => array('data' => '定床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[50] = array(0 => array('data' => '1日平均', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[51] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[52] = array(0 => array('data' => '要介護5', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[53] = array(0 => array('data' => '要介護4', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[54] = array(0 => array('data' => '要介護3', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[55] = array(0 => array('data' => '要介護2', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[56] = array(0 => array('data' => '要介護1', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[57] = array(0 => array('data' => '平均介護度', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟 精神療法
        $leftSide[58] = array(0 => array('data' => '精神療養', 'attr' => array($rowspan_4))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[59] = array(0 => array('data' => '定床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[60] = array(0 => array('data' => '空床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[61] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 特定病棟
        $leftSide[62] = array(0 => array('data' => '特定病棟', 'attr' => array($rowspan_6))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[63] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[64] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[65] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[66] = array(0 => array('data' => '回復期リハ', 'attr' => array($rowspan_2), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '重症患者の割合', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[67] = array(0 => array('data' => '在宅復帰率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[68] = array(0 => array('data' => '重点項目', 'attr' => array($rowspan_19), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(1),  'style' => array('textAlign' => $textAlign_center))
                            , 2 => array('data' => '新患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[69] = array(0 => array('data' => $indicatorData->sc(2),    'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '初診患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[70] = array(0 => array('data' => $indicatorData->sc(3),    'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '紹介患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[71] = array(0 => array('data' => $indicatorData->sc(4),      'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '救急依頼件数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[72] = array(0 => array('data' => $indicatorData->sc(5), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(4).'の内、救急受入れ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[73] = array(0 => array('data' => $indicatorData->sc(6), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(5).'の内、救急入院患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[74] = array(0 => array('data' => $indicatorData->sc(7), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '診療時間外患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[75] = array(0 => array('data' => $indicatorData->sc(8), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(7).'の内、時間外入院患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[76] = array(0 => array('data' => '紹介率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[77] = array(0 => array('data' => '救急受入率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[78] = array(0 => array('data' => '救急入院率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[79] = array(0 => array('data' => '時間外入院率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[80] = array(0 => array('data' => $indicatorData->sc(9), 'style' => array('textAlign' => $textAlign_center)),
                              1 => array('data' => 'リハビリ平均提供単位数(一般)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[81] = array(0 => array('data' => $indicatorData->sc(10), 'style' => array('textAlign' => $textAlign_center)),
                              1 => array('data' => 'リハビリ平均提供単位数(療養・精神)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[82] = array(0 => array('data' => $indicatorData->sc(11), 'style' => array('textAlign' => $textAlign_center)),
                              1 => array('data' => 'リハビリ平均提供単位(回復期リハ)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[83] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[84] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[85] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[86] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $RowCnt = 86;

        $mainData    = array();
        $totalData   = array();
        $monthlyData = $this->getArrValueByKey('monthly_data', $dataArr);
        $dailyData   = $this->getArrValueByKey('daily_data',   $dataArr);

		//「入院 一般病棟 平均在院日数(直近3ヶ月)」用の一般病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt1 = 0;
		
		//「入院 一般病棟 重症度・看護必要度割合」用の一般病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt2 = 0;

		//「入院 特殊病棟 重症患者の割合」用の特殊病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt3 = 0;
		
		//「入院 特殊病棟 在宅復帰率」用の特殊病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt4 = 0;
		
        foreach ($plantArr as $plantKey => $plantVal) {
            $facilityId = $this->getArrValueByKey('facility_id', $plantVal);
            for ($m=1; $m <= $RowCnt; $m++) {
                $mainData[$m][$plantKey] = array(
                     'data'   => '&nbsp;'
                   , 'status' => $this->checkCellDataStatus('summary_of_number_of_hospital_monthly_report_patients', array('year' => $cYear, 'month' => $displayMonth))
                );
            }

            if (is_array($dailyData)) {
                foreach ($dailyData as $dailyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dailyVal) && 'current' == $this->getArrValueByKey('year_type', $dailyVal)) {
                        $status       = $this->checkCellDataStatus('summary_of_number_of_hospital_monthly_report_patients', $dailyVal);
                        $gokeiGa      = $dailyVal['gokei_ga'];
                        $dayCount     = $dailyVal['day_count'];
                        $gokeiIp      = $dailyVal['gokei_ip'];
                        $gokeiShog    = $dailyVal['gokei_shog'];
                        $gokeiSeip    = $dailyVal['gokei_seip'];
                        $gokeiIr      = $dailyVal['gokei_ir'];
                        $gokeiKaig    = $dailyVal['gokei_kaig'];
                        $gokeiKaif    = $dailyVal['gokei_kaif'];
                        $gokeiAk      = $dailyVal['gokei_ak'];
                        $gokeiKan     = $dailyVal['gokei_kan'];
                        $gokeiIcu     = $dailyVal['gokei_icu'];
                        $gokeiShoni   = $dailyVal['gokei_shoni'];
                        $gokeiSeir    = $dailyVal['gokei_seir']; # 精神療養
                        $gokeiTok     = $dailyVal['gokei_tok'];
                        $gokeiNin     = $dailyVal['gokei_nin'];
                        $actCount     = $dailyVal['act_count'];
                        $pmtCount     = $dailyVal['pmt_count'];
                        $ipAct        = $dailyVal['ip_act'];
                        $shogAct      = $dailyVal['shog_act'];
                        $seipAct      = $dailyVal['seip_act'];
                        $irAct        = $dailyVal['ir_act'];
                        $kaigAct      = $dailyVal['kaig_act'];
                        $kaifAct      = $dailyVal['kaif_act'];
                        $akAct        = $dailyVal['ak_act'];
                        $kanAct       = $dailyVal['kan_act'];
                        $icuAct       = $dailyVal['icu_act'];
                        $shoniAct     = $dailyVal['shoni_act'];
                        $seirAct      = $dailyVal['seir_act']; # 精神療養
                        $tokAct       = $dailyVal['tok_act'];
                        $ninAct       = $dailyVal['nin_act'];
                        $sumGokei     = $gokeiIp + $gokeiShog + $gokeiSeip + $gokeiIr + $gokeiKaig + $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiSeir + $gokeiTok + $gokeiNin;
                        $gokeiTokutei = $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiTok + $gokeiNin;
                        $tokuteiAct   = $kaifAct + $akAct + $kanAct + $icuAct + $shoniAct + $tokAct + $ninAct;

                        # 外来
                        $mainData[1][$plantKey] = array('data' => $gokeiGa, 'status' => $status);
						
						//日・祝日は分母からはずす
						$mainData[2][$plantKey] = array('data' => round($gokeiGa / ($dayCount - $holi_cnt), 1), 'gokei_ga' => $gokeiGa, 'day_count' => $dayCount, 'status' => $status, 'disp_type' => 'float');

                        # 入院 全病棟
                        $mainData[3][$plantKey] = array('data' => $sumGokei, 'status' => $status);
                        $mainData[4][$plantKey] = array('data' => round($sumGokei / $dayCount, 1), 'sum_gokei' => $sumGokei, 'day_count' => $dayCount, 'status' => $status, 'disp_type' => 'float');
                        $mainData[5][$plantKey] = array('data' => $actCount, 'status' => $status);
                        $mainData[6][$plantKey] = array('data' => round($actCount - $sumGokei, 1), 'sum_gokei' => $sumGokei, 'act_count' => $actCount, 'status' => $status);
                        $mainData[7][$plantKey] = array('data' => $this->getUtilizationRates($sumGokei, $actCount, 1), 'act_count' => $actCount, 'sum_gokei' => $sumGokei, 'status' => $status, 'disp_type' => 'float_percent');
                        $mainData[8][$plantKey] = array('data' => $this->getUtilizationRates($sumGokei, $pmtCount, 1), 'pmt_count' => $pmtCount, 'sum_gokei' => $sumGokei, 'status' => $status, 'disp_type' => 'float_percent');

                        # 入院 一般病棟
                        $mainData[9][$plantKey]  = array('data' => $gokeiIp, 'status' => $status);
                        $mainData[10][$plantKey] = array('data' => $ipAct, 'status' => $status);
                        $mainData[11][$plantKey] = array('data' => round($ipAct - $gokeiIp, 1), 'ip_act' => $ipAct, 'gokei_ip' => $gokeiIp, 'status' => $status);
                        $mainData[12][$plantKey] = array('data' => $this->getUtilizationRates($gokeiIp, $ipAct, 1), 'ip_act' => $ipAct, 'gokei_ip' => $gokeiIp, 'status' => $status, 'disp_type' => 'float_percent');

                        # 入院 障害者病棟
                        $mainData[22][$plantKey] = array('data' => $gokeiShog, 'status' => $status);
                        $mainData[23][$plantKey] = array('data' => $shogAct, 'status' => $status);
                        $mainData[24][$plantKey] = array('data' => round($shogAct - $gokeiShog, 1), 'shog_act' => $shogAct, 'gokei_shog' => $gokeiShog, 'status' => $status);
                        $mainData[25][$plantKey] = array('data' => round($gokeiShog / $shogAct * 100, 1), 'shog_act' => $shogAct, 'gokei_shog' => $gokeiShog, 'status' => $status, 'disp_type' => 'float_percent');

                        # 入院 精神一般
                        $mainData[30][$plantKey] = array('data' => $gokeiSeip, 'status' => $status);
                        $mainData[31][$plantKey] = array('data' => $seipAct, 'status' => $status);
                        $mainData[32][$plantKey] = array('data' => round($seipAct - $gokeiSeip, 1), 'seip_act' => $seipAct, 'gokei_seip' => $gokeiSeip, 'status' => $status);
                        $mainData[33][$plantKey] = array('data' => round($gokeiSeip / $seipAct * 100, 1), 'seip_act' => $seipAct, 'gokei_seip' => $gokeiSeip, 'status' => $status, 'disp_type' => 'float_percent');

                        # 入院 療養病棟 医療適用
                        $mainData[34][$plantKey] = array('data' => $gokeiIr, 'status' => $status);
                        $mainData[35][$plantKey] = array('data' => $irAct, 'status' => $status);
                        $mainData[36][$plantKey] = array('data' => round($gokeiIr / $dayCount, 1), 'day_count' => $dayCount, 'gokei_ir' => $gokeiIr, 'status' => $status, 'disp_type' => 'float');
                        $mainData[37][$plantKey] = array('data' => round($gokeiIr / $irAct * 100, 1), 'ir_act' => $irAct,    'gokei_ir' => $gokeiIr, 'status' => $status, 'disp_type' => 'float_percent');

                        # 入院 療養病棟 介護適用
                        $mainData[48][$plantKey] = array('data' => $gokeiKaig, 'status' => $status);
                        $mainData[49][$plantKey] = array('data' => $kaigAct, 'status' => $status);
                        $mainData[50][$plantKey] = array('data' => round($gokeiKaig / $dayCount,  1), 'day_count' => $dayCount,   'gokei_kaig' => $gokeiKaig, 'status' => $status, 'disp_type' => 'float');
                        $mainData[51][$plantKey] = array('data' => round($gokeiKaig / $kaigAct * 100, 1), 'kaig_act' => $kaigAct, 'gokei_kaig' => $gokeiKaig, 'status' => $status, 'disp_type' => 'float_percent');

                        # 入院 療養病棟 精神療養
                        $mainData[58][$plantKey] = array('data' => $gokeiSeir, 'status' => $status);
                        $mainData[59][$plantKey] = array('data' => $seirAct, 'status' => $status);
                        $mainData[60][$plantKey] = array('data' => $this->getNumberOfEmptyFloors($gokeiSeir, $seirAct, 1), 'gokei_seir' => $gokeiSeir, 'seir_act' => $seirAct, 'status' => $status);
                        $mainData[61][$plantKey] = array('data' => $this->getUtilizationRates($gokeiSeir, $seirAct, 1), 'gokei_seir' => $gokeiSeir, 'seir_act' => $seirAct, 'status' => $status, 'disp_type' => 'float_percent');

                        # 入院 特定病棟
                        $mainData[62][$plantKey] = array('data' => $gokeiTokutei, 'status' => $status);
                        $mainData[63][$plantKey] = array('data' => $tokuteiAct, 'status' => $status);
                        $mainData[64][$plantKey] = array('data' => round($tokuteiAct - $gokeiTokutei, 1), 'tokutei_act' => $tokuteiAct, 'gokei_tokutei' => $gokeiTokutei, 'status' => $status);
                        $mainData[65][$plantKey] = array('data' => round($gokeiTokutei / $tokuteiAct * 100, 1), 'tokutei_act' => $tokuteiAct, 'gokei_tokutei' => $gokeiTokutei, 'status' => $status, 'disp_type' => 'float_percent');
                    }
                }
            }

            if (is_array($monthlyData)) {
                foreach ($monthlyData as $monthlyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $monthlyVal)) {
                        $status     = $this->checkCellDataStatus('summary_of_number_of_hospital_monthly_report_patients', array('main_data' => $monthlyVal, 'year' => $cYear, 'month' => $displayMonth));
                        $dispNum322 = (float)$monthlyVal['dispnum_322'];
                        $dispNum323 = (float)$monthlyVal['dispnum_323'];
                        $dispNum324 = (float)$monthlyVal['dispnum_324'];
                        $dispNum325 = (float)$monthlyVal['dispnum_325'];
                        $dispNum326 = (float)$monthlyVal['dispnum_326'];

                        $dispNum328 = (float)$monthlyVal['dispnum_328'];
                        $dispNum329 = (float)$monthlyVal['dispnum_329'];
                        $dispNum330 = (float)$monthlyVal['dispnum_330'];
                        $dispNum331 = (float)$monthlyVal['dispnum_331'];
                        $dispNum332 = (float)$monthlyVal['dispnum_332'];
						$dispNum333 = (float)$monthlyVal['dispnum_333'];
						$dispNum334 = (float)$monthlyVal['dispnum_334'];

                        # 入院 一般病棟
                        $mainData[13][$plantKey] = array('data' => $monthlyVal['dispnum_297'], 'status' => $status, 'disp_type' => 'float');
                        $mainData[14][$plantKey] = array('data' => $monthlyVal['dispnum_298'], 'status' => $status);
                        $mainData[15][$plantKey] = array('data' => $monthlyVal['dispnum_299'], 'status' => $status);
                        $mainData[16][$plantKey] = array('data' => $monthlyVal['dispnum_300'], 'status' => $status, 'disp_type' => 'float_percent');
                        $mainData[17][$plantKey] = array('data' => $monthlyVal['dispnum_301'], 'status' => $status);
                        $mainData[18][$plantKey] = array('data' => $monthlyVal['dispnum_302'], 'status' => $status);
                        $mainData[19][$plantKey] = array('data' => $monthlyVal['dispnum_303'], 'status' => $status);
                        $mainData[20][$plantKey] = array('data' => $monthlyVal['dispnum_304'], 'status' => $status);
                        $mainData[21][$plantKey] = array('data' => $monthlyVal['dispnum_305'], 'status' => $status);

                        # 入院 障害者病棟
                        $mainData[26][$plantKey] = array('data' => $monthlyVal['dispnum_306'], 'status' => $status);
                        $mainData[27][$plantKey] = array('data' => $monthlyVal['dispnum_307'], 'status' => $status);
                        $mainData[28][$plantKey] = array('data' => $monthlyVal['dispnum_308'], 'status' => $status);
                        $mainData[29][$plantKey] = array('data' => $monthlyVal['dispnum_309'], 'status' => $status);

                        # 入院 療養病棟 医療
                        $mainData[38][$plantKey] = array('data' => $monthlyVal['dispnum_312'], 'status' => $status);
                        $mainData[39][$plantKey] = array('data' => $monthlyVal['dispnum_313'], 'status' => $status);
                        $mainData[40][$plantKey] = array('data' => $monthlyVal['dispnum_314'], 'status' => $status);
                        $mainData[41][$plantKey] = array('data' => $monthlyVal['dispnum_315'], 'status' => $status);
                        $mainData[42][$plantKey] = array('data' => $monthlyVal['dispnum_316'], 'status' => $status);
                        $mainData[43][$plantKey] = array('data' => $monthlyVal['dispnum_317'], 'status' => $status);
                        $mainData[44][$plantKey] = array('data' => $monthlyVal['dispnum_318'], 'status' => $status);
                        $mainData[45][$plantKey] = array('data' => $monthlyVal['dispnum_319'], 'status' => $status);
                        $mainData[46][$plantKey] = array('data' => $monthlyVal['dispnum_320'], 'status' => $status);
                        $mainData[47][$plantKey] = array('data' => $monthlyVal['dispnum_321'], 'status' => $status);

                        # 入院 療養病棟 介護
                        $mainData[52][$plantKey] = array('data' => $dispNum322, 'status' => $status);
                        $mainData[53][$plantKey] = array('data' => $dispNum323, 'status' => $status);
                        $mainData[54][$plantKey] = array('data' => $dispNum324, 'status' => $status);
                        $mainData[55][$plantKey] = array('data' => $dispNum325, 'status' => $status);
                        $mainData[56][$plantKey] = array('data' => $dispNum326, 'status' => $status);

                        $mainData57 = round(($dispNum322*5+$dispNum323*4+$dispNum324*3+$dispNum325*2+$dispNum326*1)/($dispNum322+$dispNum323+$dispNum324+$dispNum325+$dispNum326), 1);
						$mainData[57][$plantKey] = array('data' => $mainData57, 'dn322' => $dispNum322, 'dn323' => $dispNum323, 'dn324' => $dispNum324, 'dn325' => $dispNum325, 'dn326' => $dispNum326, 'status' => $status, 'disp_type' => 'float');

                        # 入院 特定病棟 回復期リハ
                        $mainData[66][$plantKey] = array('data' => $monthlyVal['dispnum_310'], 'status' => $status, 'disp_type' => 'float_percent');
                        $mainData[67][$plantKey] = array('data' => $monthlyVal['dispnum_311'], 'status' => $status, 'disp_type' => 'float_percent');

                        # 重点項目
                        $mainData[68][$plantKey] = array('data' => $monthlyVal['dispnum_327'], 'status' => $status);
                        $mainData[69][$plantKey] = array('data' => $dispNum328,                'status' => $status);
                        $mainData[70][$plantKey] = array('data' => $dispNum329,                'status' => $status);
                        $mainData[71][$plantKey] = array('data' => $dispNum330,                'status' => $status);
                        $mainData[72][$plantKey] = array('data' => $dispNum331,                'status' => $status);
                        $mainData[73][$plantKey] = array('data' => $dispNum332,                'status' => $status);
                        $mainData[74][$plantKey] = array('data' => $monthlyVal['dispnum_333'], 'status' => $status);
                        $mainData[75][$plantKey] = array('data' => $dispNum334,                'status' => $status);

                        # 重点項目 紹介率
                        $mainData[76][$plantKey] = array('data' => round((($dispNum329+$dispNum331)/$dispNum328)*100, 1), 'dn328' => $dispNum328, 'dn329' => $dispNum329, 'dn331' => $dispNum331, 'status' => $status, 'disp_type' => 'float_percent');
                        $mainData[77][$plantKey] = array('data' => round(($dispNum331/$dispNum330)*100, 1), 'dn330' => $dispNum330, 'dn331' => $dispNum331, 'status' => $status, 'disp_type' => 'float_percent');
                        $mainData[78][$plantKey] = array('data' => round(($dispNum332/$dispNum331)*100, 1), 'dn332' => $dispNum332, 'dn331' => $dispNum331, 'status' => $status, 'disp_type' => 'float_percent');
                        $mainData[79][$plantKey] = array('data' => round(($dispNum334/$dispNum333)*100, 1), 'dn333' => $dispNum333, 'dn334' => $dispNum334, 'status' => $status, 'disp_type' => 'float_percent');

                        $mainData[80][$plantKey] = array('data' => $monthlyVal['dispnum_335'], 'status' => $status, 'disp_type' => 'float_unit');
                        $mainData[81][$plantKey] = array('data' => $monthlyVal['dispnum_336'], 'status' => $status, 'disp_type' => 'float_unit');
                        $mainData[82][$plantKey] = array('data' => $monthlyVal['dispnum_337'], 'status' => $status, 'disp_type' => 'float_unit');
                        $mainData[83][$plantKey] = array('data' => $monthlyVal['dispnum_338'], 'status' => $status);
                        $mainData[84][$plantKey] = array('data' => $monthlyVal['dispnum_339'], 'status' => $status);
                        $mainData[85][$plantKey] = array('data' => $monthlyVal['dispnum_340'], 'status' => $status);
                        $mainData[86][$plantKey] = array('data' => $monthlyVal['dispnum_341'], 'status' => $status);
                    }
                }
            }

			
            for ($t=1; $t <= $RowCnt; $t++) {
                $totalData[$t][0]['status']  = $this->checkCellDataStatus('comp', array($totalData[$t][0]['status'], $mainData[$t][$plantKey]['status']));
                switch ($t) {
                    case '2': # 外来 1日平均
//                        $totalData[$t][0]['gokei_ga']  = $totalData[$t][0]['gokei_ga']  + $mainData[$t][$plantKey]['gokei_ga'];
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
//                        $totalData[$t][0]['data']      = round($totalData[$t][0]['gokei_ga'] / $totalData[$t][0]['day_count'], 1);
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
						break;

                    case '4': # 入院 全病棟 1日平均
//                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$plantKey]['sum_gokei'];
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
//                        $totalData[$t][0]['data']      = round($totalData[$t][0]['sum_gokei'] / $totalData[$t][0]['day_count'], 1);
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
                        break;

                    case '6': # 入院 全病棟 空床数
                        $totalData[$t][0]['act_count'] = $totalData[$t][0]['act_count'] + $mainData[$t][$plantKey]['act_count'];
                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$plantKey]['sum_gokei'];
                        $totalData[$t][0]['data']      = $this->getNumberOfEmptyFloors($totalData[$t][0]['sum_gokei'], $totalData[$t][0]['act_count'], 1);
                        break;

                    case '7': # 入院 全病棟 稼働病床稼働率
                        $totalData[$t][0]['act_count'] = $totalData[$t][0]['act_count'] + $mainData[$t][$plantKey]['act_count'];
                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$plantKey]['sum_gokei'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['sum_gokei'], $totalData[$t][0]['act_count'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '8': # 入院 全病棟 許可病床稼働率
                        $totalData[$t][0]['pmt_count'] = $totalData[$t][0]['pmt_count'] + $mainData[$t][$plantKey]['pmt_count'];
                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$plantKey]['sum_gokei'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['sum_gokei'], $totalData[$t][0]['pmt_count'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '11': # 入院 一般病棟 空床数
                        $totalData[$t][0]['ip_act']   = $totalData[$t][0]['ip_act']   + $mainData[$t][$plantKey]['ip_act'];
                        $totalData[$t][0]['gokei_ip'] = $totalData[$t][0]['gokei_ip'] + $mainData[$t][$plantKey]['gokei_ip'];
                        $totalData[$t][0]['data']     = round($totalData[$t][0]['ip_act'] - $totalData[$t][0]['gokei_ip'], 1);
                        break;

                    case '12': # 入院 一般病棟 稼働率
                        $totalData[$t][0]['ip_act']   = $totalData[$t][0]['ip_act']   + $mainData[$t][$plantKey]['ip_act'];
                        $totalData[$t][0]['gokei_ip'] = $totalData[$t][0]['gokei_ip'] + $mainData[$t][$plantKey]['gokei_ip'];
                        $totalData[$t][0]['data']     = round($totalData[$t][0]['gokei_ip'] / $totalData[$t][0]['ip_act'] * 100, 1);
						$totalData[$t][0]['disp_type']  = 'float_percent';
						break;

                    case '13': # 入院 一般病棟 平均在院日数(直近3ヶ月)
						if(($mainData[$t][$plantKey]['data'] !=null) && ($mainData[$t][$plantKey]['data'] !=0))
						{
							//一般病棟がある施設です
							$denominator_cnt1++;
							
							$totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
							$totalData[$t][0]['disp_type'] = 'float';
						}
                        break;


/*
					case '13': # 平均在院日数(直近3ヶ月)
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] +  $mainData[$t][$plantKey]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
						break;
					


                    case '16': # 入院 一般病棟 重症度・看護必要度割合
                        $totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;
*/

					case '16': # 入院 一般病棟 重症度・看護必要度割合
						if(($mainData[$t][$plantKey]['data'] !=null) && ($mainData[$t][$plantKey]['data'] !=0))
						{
							//一般病棟がある施設です
							$denominator_cnt2++;

							$totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
							$totalData[$t][0]['disp_type'] = 'float_percent';
						}
						break;


                    case '24': # 入院 障害者病棟 空床数
                        $totalData[$t][0]['shog_act']   = $totalData[$t][0]['shog_act']   + $mainData[$t][$plantKey]['shog_act'];
                        $totalData[$t][0]['gokei_shog'] = $totalData[$t][0]['gokei_shog'] + $mainData[$t][$plantKey]['gokei_shog'];
                        $totalData[$t][0]['data']       = round($totalData[$t][0]['shog_act'] - $totalData[$t][0]['gokei_shog'], 1);
                        break;

                    case '25': # 入院 障害者病棟 稼働率
                        $totalData[$t][0]['shog_act']   = $totalData[$t][0]['shog_act']   + $mainData[$t][$plantKey]['shog_act'];
                        $totalData[$t][0]['gokei_shog'] = $totalData[$t][0]['gokei_shog'] + $mainData[$t][$plantKey]['gokei_shog'];
                        $totalData[$t][0]['data']       = round($totalData[$t][0]['gokei_shog'] / $totalData[$t][0]['shog_act'] * 100, 1);
                        $totalData[$t][0]['disp_type']  = 'float_percent';
                        break;

                    case '32': # 入院 精神一般病棟 空床数
                        $totalData[$t][0]['seip_act']   = $totalData[$t][0]['seip_act']   + $mainData[$t][$plantKey]['seip_act'];
                        $totalData[$t][0]['gokei_seip'] = $totalData[$t][0]['gokei_seip'] + $mainData[$t][$plantKey]['gokei_seip'];
                        $totalData[$t][0]['data']       = round($totalData[$t][0]['seip_act'] - $totalData[$t][0]['gokei_seip'] , 1);
                        break;

                    case '33': # 入院 精神一般病棟 稼働率
                        $totalData[$t][0]['seip_act']   = $totalData[$t][0]['seip_act']   + $mainData[$t][$plantKey]['seip_act'];
                        $totalData[$t][0]['gokei_seip'] = $totalData[$t][0]['gokei_seip'] + $mainData[$t][$plantKey]['gokei_seip'];
                        $totalData[$t][0]['data']       = round($totalData[$t][0]['gokei_seip'] / $totalData[$t][0]['seip_act'] * 100, 1);
                        $totalData[$t][0]['disp_type']  = 'float_percent';
                        break;

                    case '36': # 入院 療養病棟 医療適用 1日平均
                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
                        $totalData[$t][0]['gokei_ir']  = $totalData[$t][0]['gokei_ir']  + $mainData[$t][$plantKey]['gokei_ir'];
                        $totalData[$t][0]['data']      = round($totalData[$t][0]['gokei_ir'] / $totalData[$t][0]['day_count'], 1);
                        $totalData[$t][0]['disp_type'] = 'float';
                        break;

                    case '37': # 入院 療養病棟 医療適用 稼働率
                        $totalData[$t][0]['ir_act']    = $totalData[$t][0]['ir_act']   + $mainData[$t][$plantKey]['ir_act'];
                        $totalData[$t][0]['gokei_ir']  = $totalData[$t][0]['gokei_ir'] + $mainData[$t][$plantKey]['gokei_ir'];
                        $totalData[$t][0]['data']      = round($totalData[$t][0]['gokei_ir'] / $totalData[$t][0]['ir_act'] * 100, 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '50': # 入院 療養病棟 介護適用 1日平均
                        $totalData[$t][0]['day_count']  = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
                        $totalData[$t][0]['gokei_kaig'] = $totalData[$t][0]['gokei_kaig'] + $mainData[$t][$plantKey]['gokei_kaig'];
                        $totalData[$t][0]['data']       = round($totalData[$t][0]['gokei_kaig'] / $totalData[$t][0]['day_count'], 1);
                        $totalData[$t][0]['disp_type']  = 'float';
                        break;

                    case '51': # 入院 療養病棟 介護適用 稼働率
                        $totalData[$t][0]['kaig_act']   = $totalData[$t][0]['kaig_act']   + $mainData[$t][$plantKey]['kaig_act'];
                        $totalData[$t][0]['gokei_kaig'] = $totalData[$t][0]['gokei_kaig'] + $mainData[$t][$plantKey]['gokei_kaig'];
                        $totalData[$t][0]['data']       = round($totalData[$t][0]['gokei_kaig'] / $totalData[$t][0]['kaig_act'] * 100, 1);
                        $totalData[$t][0]['disp_type']  = 'float_percent';
                        break;

                    case '57': # 入院 療養病棟 介護適用 平均介護度
                        $totalData[$t][0]['dn322'] = $totalData[$t][0]['dn322'] + $mainData[$t][$plantKey]['dn322'];
                        $totalData[$t][0]['dn323'] = $totalData[$t][0]['dn323'] + $mainData[$t][$plantKey]['dn323'];
                        $totalData[$t][0]['dn324'] = $totalData[$t][0]['dn324'] + $mainData[$t][$plantKey]['dn324'];
                        $totalData[$t][0]['dn325'] = $totalData[$t][0]['dn325'] + $mainData[$t][$plantKey]['dn325'];
                        $totalData[$t][0]['dn326'] = $totalData[$t][0]['dn326'] + $mainData[$t][$plantKey]['dn326'];
                        $totalData[$t][0]['data']  = round((($totalData[$t][0]['dn322']*5 + $totalData[$t][0]['dn323']*4 + $totalData[$t][0]['dn324']*3 + $totalData[$t][0]['dn325']*2 + $totalData[$t][0]['dn326']*1) / ($totalData[$t][0]['dn322'] + $totalData[$t][0]['dn323'] + $totalData[$t][0]['dn324'] + $totalData[$t][0]['dn325'] + $totalData[$t][0]['dn326'])), 1);
						$totalData[$t][0]['disp_type']  = 'float';
						break;

                    case '60': # 入院 療養病棟 精神療養 空床数
                        $totalData[$t][0]['seir_act']   = $totalData[$t][0]['seir_act']   + $mainData[$t][$plantKey]['seir_act'];
                        $totalData[$t][0]['gokei_seir'] = $totalData[$t][0]['gokei_seir'] + $mainData[$t][$plantKey]['gokei_seir'];
                        $totalData[$t][0]['data']       = $this->getNumberOfEmptyFloors($totalData[$t][0]['gokei_seir'], $totalData[$t][0]['seir_act'], 1);
                        break;

                    case '61': # 入院 療養病棟 精神療養 稼働率
                        $totalData[$t][0]['seir_act']    = $totalData[$t][0]['seir_act']   + $mainData[$t][$plantKey]['seir_act'];
                        $totalData[$t][0]['gokei_seir']  = $totalData[$t][0]['gokei_seir'] + $mainData[$t][$plantKey]['gokei_seir'];
                        $totalData[$t][0]['data']        = $this->getUtilizationRates($totalData[$t][0]['gokei_seir'], $totalData[$t][0]['seir_act'], 1);
                        $mainData[$t][$plantKey]['data'] = ($mainData[$t][$plantKey]['data']=='&nbsp;')? '' : $mainData[$t][$plantKey]['data'];
                        $totalData[$t][0]['disp_type']   = 'float_percent';
                        break;

                    case '64': # 入院 特定病棟 空床数
                        $totalData[$t][0]['tokutei_act']   = $totalData[$t][0]['tokutei_act']   + $mainData[$t][$plantKey]['tokutei_act'];
                        $totalData[$t][0]['gokei_tokutei'] = $totalData[$t][0]['gokei_tokutei'] + $mainData[$t][$plantKey]['gokei_tokutei'];
                        $totalData[$t][0]['data']          = round($totalData[$t][0]['tokutei_act'] - $totalData[$t][0]['gokei_tokutei'] , 1);
                        break;

                    case '65': # 入院 特定病棟 稼働率
                        $totalData[$t][0]['tokutei_act']   = $totalData[$t][0]['tokutei_act']   + $mainData[$t][$plantKey]['tokutei_act'];
                        $totalData[$t][0]['gokei_tokutei'] = $totalData[$t][0]['gokei_tokutei'] + $mainData[$t][$plantKey]['gokei_tokutei'];
                        $totalData[$t][0]['data']          = round($totalData[$t][0]['gokei_tokutei'] / $totalData[$t][0]['tokutei_act'] * 100, 1);
                        $totalData[$t][0]['disp_type']     = 'float_percent';
                        break;

                    case '66': 
						if(($mainData[$t][$plantKey]['data'] !=null) && ($mainData[$t][$plantKey]['data'] !=0))
						{
							//特殊病棟がある施設です
							$denominator_cnt3++;
							
							$totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
							$totalData[$t][0]['disp_type'] = 'float_percent';
						}
                        break;

					case '67':
						if(($mainData[$t][$plantKey]['data'] !=null) && ($mainData[$t][$plantKey]['data'] !=0))
						{
							//特殊病棟がある施設です
							$denominator_cnt4++;
							
							$totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
							$totalData[$t][0]['disp_type'] = 'float_percent';
						}
						break;
					
					case '76': # 重点事項 紹介率
                        $totalData[$t][0]['dn329']     = $totalData[$t][0]['dn329'] + $mainData[$t][$plantKey]['dn329'];
                        $totalData[$t][0]['dn331']     = $totalData[$t][0]['dn331'] + $mainData[$t][$plantKey]['dn331'];
                        $totalData[$t][0]['dn328']     = $totalData[$t][0]['dn328'] + $mainData[$t][$plantKey]['dn328'];
                        $totalData[$t][0]['data']      = round(((($totalData[$t][0]['dn329']+$totalData[$t][0]['dn331'])/$totalData[$t][0]['dn328'])*100), 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '77': # その他 救急受入率
                        $totalData[$t][0]['dn330']     = $totalData[$t][0]['dn330'] + $mainData[$t][$plantKey]['dn330'];
                        $totalData[$t][0]['dn331']     = $totalData[$t][0]['dn331'] + $mainData[$t][$plantKey]['dn331'];
                        $totalData[$t][0]['data']      = round((($totalData[$t][0]['dn331']/$totalData[$t][0]['dn330'])*100), 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '78': # その他 救急入院率
                        $totalData[$t][0]['dn331']     = $totalData[$t][0]['dn331'] + $mainData[$t][$plantKey]['dn331'];
                        $totalData[$t][0]['dn332']     = $totalData[$t][0]['dn332'] + $mainData[$t][$plantKey]['dn332'];
                        $totalData[$t][0]['data']      = round(($totalData[$t][0]['dn332'] / $totalData[$t][0]['dn331'] * 100), 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '79': # その他 時間外入院率
                        $totalData[$t][0]['dn333']     = $totalData[$t][0]['dn333'] + $mainData[$t][$plantKey]['dn333'];
                        $totalData[$t][0]['dn334']     = $totalData[$t][0]['dn334'] + $mainData[$t][$plantKey]['dn334'];
                        $totalData[$t][0]['data']      = round(($totalData[$t][0]['dn334'] / $totalData[$t][0]['dn333'] * 100), 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '80': case '81': case '82':
                        $totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
                        $totalData[$t][0]['disp_type'] = 'float_unit';
                        break;

                    default:
                        $totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$plantKey]['data'];
                        break;
                }
            }
        }


		//月数分の平均値を計算
		//「平均在院日数（直近3カ月）」、「重症度、看護必要度割合」、「平均介護度」、「重症患者の割合」、「在宅復帰率」
		//「ﾘﾊﾋﾞﾘ平均提供単位数（一般）」、「ﾘﾊﾋﾞﾘ平均提供単位数（療養、精神）」、「ﾘﾊﾋﾞﾘ平均提供単位数（回復期リハ）」
		$totalData["13"][0]['data'] =  ($totalData["13"][0]['data'] / $denominator_cnt1);
		$totalData["16"][0]['data'] =  ($totalData["16"][0]['data'] / $denominator_cnt2);
		$totalData["66"][0]['data'] =  ($totalData["66"][0]['data'] / $denominator_cnt3);
		$totalData["67"][0]['data'] =  ($totalData["67"][0]['data'] / $denominator_cnt4);
		$totalData["80"][0]['data'] =  ($totalData["80"][0]['data'] / count($monthlyData));
		$totalData["81"][0]['data'] =  ($totalData["81"][0]['data'] / count($monthlyData));
		$totalData["82"][0]['data'] =  ($totalData["82"][0]['data'] / count($monthlyData));
		

        for ($i=1; $i <= $RowCnt; $i++) {
            switch ($i) {
                default:
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .= '</table>';

        return $rtnStr;
    }

    /**
     * 病院月間報告書(行為件数)のデータ出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function getHospitalMonthlyReport($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr  = '';
        $fontTag = $this->getFontTag();

        $displayMonth = $this->getDisplayMonth();
        $startMonth   = $this->getFiscalYearStartMonth();

        $cYear = $this->getArrValueByKey('year', $dateArr);

        $indicatorData = $this->getIndicatorData();
        $cDayCount     = $indicatorData->getDayCountOfMonth($cYear, $displayMonth);

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_9  = array('name' => 'rowspan', 'value' =>  9);
        $rowspan_17 = array('name' => 'rowspan', 'value' => 17);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);

        # style
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $dayCountHeader = '<table width="100%" >'
                             .'<tr>'
                                 .'<td>'
                                     .'<table class="list" width="50%" >'
                                         .'<tr>'
                                             .$this->convertCellData(
                                                 array(0 => array('data' => '平日', 'style' => array($bgColor_weekday))
                                                     , 1 => array('data' => $this->getArrValueByKey('weekday', $cDayCount).'日', 'style' => array($textAlign_center))
                                                     , 2 => array('data' => '土曜', 'style' => array($bgColor_saturday))
                                                     , 3 => array('data' => $this->getArrValueByKey('saturday', $cDayCount).'日', 'style' => array($textAlign_center))
                                                     , 4 => array('data' => '休日', 'style' => array($bgColor_holiday))
                                                     , 5 => array('data' => $this->getArrValueByKey('holiday', $cDayCount).'日', 'style' => array($textAlign_center))
                                                 ),
                                                 $fontTag,
                                                 array()
                                             )
                                         .'</tr>'
                                     .'</table>'
                                 .'</td>'
                                 .'<td style="text-align: right;" >'.sprintf($fontTag,sprintf('%04d年%02d月', $cYear, $displayMonth)).'</td>'
                             .'</tr>'
                         .'</table>';

        $rtnStr .= $dayCountHeader;
        $rtnStr .= '<table class="list" width="100%" >';

        $leftSide  = array();
        $mainData  = array();
        $totalData = array();

        $leftSide[0]  = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)));
		//$leftSide[0]  = array(0 => array('data' => '&nbsp;', 'attr' => array($rowspan_2, $colspan_2)));
		$leftSide[2]  = array(0 => array('data' => '手術件数', 'attr' => array($rowspan_5))
                            , 1 => array('data' => '全身麻酔による手術')
        );
        $leftSide[3]  = array(0 => array('data' => '硬膜外・脊椎麻酔による手術'));
        $leftSide[4]  = array(0 => array('data' => '上・下肢伝達麻酔による手術'));
        $leftSide[5]  = array(0 => array('data' => '内視鏡的手術'));
        $leftSide[6]  = array(0 => array('data' => '白内障手術'));
        $leftSide[7]  = array(0 => array('data' => '内視鏡', 'attr' => array($rowspan_3))
                            , 1 => array('data' => '食道・胃・十二指腸ファイバー')
        );
        $leftSide[8]  = array(0 => array('data' => '大腸・直腸ファイバー'));
        $leftSide[9]  = array(0 => array('data' => 'その他'));
        $leftSide[10] = array(0 => array('data' => '検査', 'attr' => array($rowspan_9))
                            , 1 => array('data' => '心電図')
        );
        $leftSide[11] = array(0 => array('data' => 'ホルター型心電図'));
        $leftSide[12] = array(0 => array('data' => 'トレッドミル'));
        $leftSide[13] = array(0 => array('data' => '心臓超音波'));
        $leftSide[14] = array(0 => array('data' => '胸腹部超音波'));
        $leftSide[15] = array(0 => array('data' => 'その他超音波(頭頸部・四肢等)'));
        $leftSide[16] = array(0 => array('data' => '肺機能'));
        $leftSide[17] = array(0 => array('data' => '骨塩定量'));
        $leftSide[18] = array(0 => array('data' => '血液ガス分析'));
        $leftSide[19] = array(0 => array('data' => '放射線', 'attr' => array($rowspan_17))
                            , 1 => array('data' => '一般撮影')
        );
        $leftSide[20] = array(0 => array('data' => 'マンモグラフィー'));
        $leftSide[21] = array(0 => array('data' => 'MDL(UGI)'));
        $leftSide[22] = array(0 => array('data' => 'BEM'));
        $leftSide[23] = array(0 => array('data' => 'DIP'));
        $leftSide[24] = array(0 => array('data' => 'DIC'));
        $leftSide[25] = array(0 => array('data' => '特殊'));
        $leftSide[26] = array(0 => array('data' => 'その他'));
        $leftSide[27] = array(0 => array('data' => 'CT(BRAIN)'));
        $leftSide[28] = array(0 => array('data' => 'CT(BODY)'));
        $leftSide[29] = array(0 => array('data' => 'CT(その他)'));
        $leftSide[30] = array(0 => array('data' => 'CT造影剤加算'));
        $leftSide[31] = array(0 => array('data' => 'MRI(BRAIN)'));
        $leftSide[32] = array(0 => array('data' => 'MRI(BODY)'));
        $leftSide[33] = array(0 => array('data' => 'MRI(その他)'));
        $leftSide[34] = array(0 => array('data' => 'MRI造影剤加算'));
        $leftSide[35] = array(0 => array('data' => 'Angio'));
        $leftSide[36] = array(0 => array('data' => '重点項目', 'attr' => array($rowspan_17))
                            , 1 => array('data' => '超重症児(者)入院診療加算(イ・ロ)')
        );
        $leftSide[37] = array(0 => array('data' => '栄養サポートチーム加算'));
        $leftSide[38] = array(0 => array('data' => '呼吸ケアチーム加算'));
        $leftSide[39] = array(0 => array('data' => '医療安全対策加算(1・2)'));
        $leftSide[40] = array(0 => array('data' => '感染防止対策加算'));
        $leftSide[41] = array(0 => array('data' => '医療機器安全管理料(1・2)'));
        $leftSide[42] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_1').')'));
        $leftSide[43] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_2').')'));
        $leftSide[44] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_3').')'));
        $leftSide[45] = array(0 => array('data' => '医薬品安全性情報等管理体制加算'));
        $leftSide[46] = array(0 => array('data' => '&nbsp;'));
        $leftSide[47] = array(0 => array('data' => '&nbsp;'));
        $leftSide[48] = array(0 => array('data' => '&nbsp;'));
        $leftSide[49] = array(0 => array('data' => '&nbsp;'));
        $leftSide[50] = array(0 => array('data' => '&nbsp;'));
        $leftSide[51] = array(0 => array('data' => '&nbsp;'));
        $leftSide[52] = array(0 => array('data' => '&nbsp;'));

        $rowCnt = count($leftSide);

        $plantNameArr = array();
        if (is_array($plantArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $facilityId = $this->getArrValueByKey('facility_id', $plantVal);

                $plantNameArr[$plantKey] = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'style' => array('bgColor' => $bgColor_blue));
//                $plantNameAnderDisplay[$plantKey] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));

                $dispNum = array();
                for ($i=342; $i <= 392; $i++) { $dispNum[$i] = null; }

                $status = $this->checkCellDataStatus('get_hospital_monthly_report', array('year' => $cYear, 'month' => $displayMonth));

                foreach ($dataArr as $dataKey => $dataVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal)) {
                        $status = $this->checkCellDataStatus('get_hospital_monthly_report', $dataVal);

                        for ($i=342; $i <= 392; $i++) { $dispNum[$i] = $this->getArrValueByKey('dispnum_'.$i, $dataVal); }
                    }
                }

                $startNum = 2;
                for ($i=342; $i <= 392; $i++) {
                    $mainData[$startNum][$plantKey] = array('data' => $dispNum[$i], 'status' => $status);
                    $startNum++;
                }

                for ($i = 2; $i <= $rowCnt; $i++) {
                    $totalData[$i][0] = array(
                        'data' => $totalData[$i][0]['data'] + $mainData[$i][$plantKey]['data'],
                        'status' => $this->checkCellDataStatus('comp', array($totalData[$i][0]['status'], $mainData[$i][$plantKey]['status']))
                    );
                }
            }
        }

        $plantNameArr[]          = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue));
//        $plantNameAnderDisplay[] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));

        $mainData[0] = $plantNameArr;
//        $mainData[1] = $plantNameAnderDisplay;

        $rtnStr .= '<tr>'.$this->convertCellData($leftSide[0], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue)).$this->convertCellData($mainData[0], $fontTag, array('textAlign' => $textAlign_center)).'</tr>';
//		$rtnStr .= '<tr>'.$this->convertCellData($leftSide[0], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue)).$this->convertCellData($mainData[0], $fontTag, array('textAlign' => $textAlign_center)).'</tr>'
//                  .'<tr>'.$this->convertCellData($mainData[1], $fontTag, array('textAlign' => $textAlign_center)).'</tr>';
//        for ($i = 2; $i <= $rowCnt; $i++) {
        for ($i = 1; $i <= $rowCnt; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSide[$i], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($mainData[$i], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_center))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * 取得したデータをテーブルの <tr> 内データに置き換える。
     * @param  array  $targetData
     * @param  string $fontTag
     * @param  array  $styleArr
     * @param  bool   $percent
     * @return string
     */
    function convertCellData($targetData, $fontTag, $styleArr, $percent=null)
    {
        $rtnStr     = '';
        $percentStr = is_null($percent)? '' : '%' ;
        $red        = $this->getRgbColor('red',    'background-color'); # 日報未入力
        $yellow     = $this->getRgbColor('yellow', 'background-color'); # 申請中
        $startMonth = $this->getFiscalYearStartMonth();
        if (is_array($targetData)) {
            ksort($targetData, SORT_NUMERIC);
            foreach ($targetData as $key => $val) {
                $month    = (($startMonth + $key) <= 12)? ($startMonth + $key) : ($startMonth + $key) - 12;
                $styleVal = null;
                if (is_array($this->getArrayValueByKeyForEmptyIsNull('style', $val))) { $styleVal = array_merge($styleArr, $val['style']); }
                else { $styleVal = $styleArr; }
                $style = null;
                if ($val['status'] == 1) {
                    # style 指定がある場合
                    if (is_array($styleVal)) {
                        if ($this->getArrValueByKey('bgColor', $styleVal)) { foreach ($styleVal as $sKey => $sVal) { $style .= ($sKey == 'bgColor')? $red : ' '.$sVal['name'].':'.$sVal['value'].';'; } }
                        else {
                           foreach ($styleVal as $sKey => $sVal) { $style .= ' '.$sVal['name'].':'.$sVal['value'].';'; }
                           $style .= $red;
                        }
                    }
                    # style 指定がない場合
                    else { $style .= $red; }
                }
                else if($val['status'] == 2) {
                    if (is_array($styleVal)) { # style 指定がある場合
                        if ($this->getArrValueByKey('bgColor', $styleVal)) { foreach ($styleVal as $sKey => $sVal) { $style .= ($sKey == 'bgColor')? $yellow : ' '.$sVal['name'].':'.$sVal['value'].';'; } }
                        else {
                           foreach ($styleVal as $sKey => $sVal) { $style .= ' '.$sVal['name'].':'.$sVal['value'].';'; }
                           $style .= $yellow;
                        }
                    }
                    # style 指定がない場合
                    else { $style .= $yellow; }
                }
                else { if (is_array($styleVal)) { foreach ($styleVal as $sKey => $sVal) { $style .= ' '.$sVal['name'].':'.$sVal['value'].';'; } } }
                $attr = ' ';
                $attrArr = $this->getArrValueByKey('attr', $val);
                if (is_array($attrArr)) { foreach ($attrArr as $attrVal) { $attr .= $attrVal['name'].'="'.$attrVal['value'].'" '; } }

                # 表示対応(桁数・単位etc)
                if ($dispType = $this->getArrValueByKey('disp_type', $val)) {
//                    if (is_numeric($val['data'])) { $val['data'] = number_format($val['data'], 0); }
                    switch ($dispType) {
                        # 整数
                        case 'int': case 'integer':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', number_format($val['data'])).$percentStr));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%d', $val['data']).$percentStr));
                            }
                            break;

                        # 少数表示対応(少数第一位)
                        case 'float':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', number_format($val['data'], 1)).$percentStr));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data']).$percentStr));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%.1f', $val['data']).$percentStr));
                            }
                            break;

                        # パーセント表示対応(少数第一位)
                        case 'float_percent':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', number_format($val['data'], 1)).'%'));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%.1f', $val['data']).'%'));
                            }
                            break;

                        # 括弧付き少数表示(少数第一位)
                        case 'float_parenthesis':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, '('.sprintf('%s', number_format($val['data'], 1)).$percentStr.')'));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, '('.sprintf('%.1f', $val['data']).$percentStr.')'));
                            }
                            break;

                        # 「○○単位」表示対応(少数第一位)
                        case 'float_unit':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', number_format($val['data'], 1)).'単位'));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%.1f', $val['data']).'単位'));
                            }
                            break;

                        # 括弧付きパーセント表示対応(少数第一位)
                        case 'float_percent_parenthesis':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, '('.sprintf('%s', number_format($val['data'], 1)).'%)'));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, '('.sprintf('%.1f', $val['data']).'%)'));
                            }
                            break;

                        # 括弧付き表示対応
                        case 'parenthesis':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('(%s)', number_format($val['data'])).$percentStr));
//                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('(%s)', $val['data']).$percentStr));
                            }
                            break;

                        # 例外(ココはカンマ表示対応しない)
                        case 'exception':
                            if (is_null($val['data']) || $val['data']==='' || $val['data']==='&nbsp;') {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', $val['data'])));
                            }
                            else {
                                $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, sprintf('%s', ($val['data'])).$percentStr));
                            }
                            break;

                    }
                }
                else {
                    if (is_null($val['data'])) { $val['data'] = '&nbsp;'; }
                    if (is_numeric($val['data'])) { $val['data'] = number_format($val['data'], 0); }
                    $rtnStr .= sprintf('<td%s style="%s" >%s</td>', $attr, $style, sprintf($fontTag, $val['data']).$percentStr);
                }
            }
        }
        return $rtnStr;
    }

    /**
     * セルのデータの状態を取得します。
     * @param  string  $type
     * @param  array   $arr
     * @return integer
     */
    function checkCellDataStatus($type, $arr)
    {
        $rtn        = 3;
        $startMonth = $this->getFiscalYearStartMonth();
        $thisYear   = date('Y');
        $lastYear   = date('Y') - 1;
        $thisMonth  = date('m');
        $today      = date('d');

        switch ($type) {
            case 'comp': case 'raito': case 'total': case 'average': case 'amg':
                foreach ($arr as $val) { if (!empty($val) && $rtn > $val) { $rtn = $val; } }
                break;

            case 'last': case 'current':
                $year     = $this->getArrValueByKey('year',          $arr);
                $mainData = $this->getArrValueByKey('main_data',     $arr);
                $month    = $this->getArrValueByKey('month',         $mainData);
                $valid    = $this->getArrValueByKey($year.'_valid',  $mainData);
                $status   = $this->getArrValueByKey($year.'_status', $mainData);
                $count    = $this->getArrValueByKey($year.'_count',  $mainData);

                if ($valid === '0' || (is_null($valid) && is_null($status))) { $rtn = 1; }
                else if ($status === '0') { $rtn = 2; }
                if ($year == $thisYear && $month == $thisMonth) {
                    # 当月のデータなら、当月分のデータ数と日付の値が同じでなければ、日報未登録があると見なす。
                    if ($count < date('d', mktime(0, 0, 0, $thisMonth, $today - 1, $thisYear))) { $rtn = 1; }
                }
                else if (date('Ym') > $year.$month) {
                    # 当月以前のデータで、月の日数と日報の投稿数が合わなければ、日報未登録があると見なす。
                    if ($count < $this->getLastDay($year, $month)) { $rtn = 1; }
                }
                # 当月以降のデータ入力状況を参照しない処理
                if ($this->getDateForMonthStart($year, $month) > $this->getDateForMonthStart($thisYear, $thisMonth)) { $rtn = 3; }
                # 運用開始年以前のデータ入力状況を参照しない処理
                else if ($this->getDateForMonthStart($year, $month) < $this->getDateForMonthStart(date('Y', mktime(0, 0, 0, $thisMonth, $today, $thisYear)), $startMonth)) { $rtn = 3; }
                break;

            # No.62 [病院] 病院別患者日報
            # No.63 [病院] 病院別患者日報(シンプル版)
            case 'daily_report':
                $valid  = $this->getArrValueByKey('valid',  $arr);
                $status = $this->getArrValueByKey('status', $arr);
                $year   = $this->getArrValueByKey('year',   $arr);
                $month  = $this->getArrValueByKey('month',  $arr);
                $day    = $this->getArrValueByKey('day',    $arr);

                if ($valid === '0' || (is_null($valid) && is_null($status))) { $rtn = 1; }
                else if ($status === '0') { $rtn = 2; }

                if ($year < $thisYear) { # 閲覧年以前の場合
                    if ($thisMonth < $startMonth) { if ($month < $startMonth) { $rtn = 3; } }
                    else { $rtn = 3; }
                    if ($year < $lastYear) { $rtn = 3; }
                }
                if ($year == $thisYear) { if ($thisMonth >= $startMonth) { if ($month < $startMonth) { $rtn = 3; } } } # 閲覧年の場合
                if ( ($year == $thisYear && $month >= $thisMonth) || $year > $thisYear) { # 翌月以降の場合
                    if ($month == $thisMonth && $day >= $today &&  $rtn == 1) { $rtn = 3; }
                    else if ($month > $thisMonth || $year > $thisYear) { $rtn = 3; }
                }
                break;

            case 'monthly_report':
                $valid    = $this->getArrValueByKey('valid',     $arr);
                $status   = $this->getArrValueByKey('status',    $arr);
                $year     = $this->getArrValueByKey('year',      $arr);
                $month    = $this->getArrValueByKey('month',     $arr);
                $day      = $this->getArrValueByKey('day',       $arr);
                $dayCount = $this->getArrValueByKey('day_count', $arr);

                if (!is_null($dayCount) && ($year == $thisYear && $month == $thisMonth)) {
                    if ($dayCount < $this->getLastDay($year, $month)) { $rtn = 1; }
                    if ($dayCount < date('d', mktime(0, 0, 0, $thisMonth, $today - 1, $thisYear)) ) { $rtn = 3; }
                }

                if ($valid == '0' || (is_null($valid) && is_null($status))) { $rtn = 1; }
                else if ($status == '0' && $rtn > 2) { $rtn = 2; }
                # 参照日以降のデータ入力状況を参照しない処理
                if (empty($valid) && mktime(0, 0, 0, $month, $day, $year) >= mktime(0, 0, 0, $thisMonth, $today, $thisYear)) { $rtn = 3; }
                if (empty($day) && mktime(0, 0, 0, $month, 1, $year) > mktime(0, 0, 0, $thisMonth, 1, $thisYear)) { $rtn = 3; }
                # 運用開始年以前のデータ入力状況を参照しない処理
                else if ($this->getDateForMonthStart($year, $month) < $this->getDateForMonthStart(date('Y', mktime(0, 0, 0, $thisMonth, $today, $thisYear)), $startMonth)) { $rtn = 3; }
                break;

            # No.65 [病院] 病院別患者月報
            # No.66 [病院] 病院別患者月報(前年比較)
            case 'monthly_report_comp':
                $valid     = $this->getArrValueByKey('valid',     $arr);
                $status    = $this->getArrValueByKey('status',    $arr);
                $year      = $this->getArrValueByKey('year',      $arr);
                $month     = $this->getArrValueByKey('month',     $arr);
                $dayCount  = $this->getArrValueByKey('day_count', $arr);
                $lastDay   = $this->getLastDay($year, $month);

                $compDay;
                if ($year == $thisYear && $month == $thisMonth) { $compDay = $today - 1; }
                else { $compDay = $lastDay; }

                if ($dayCount >= $compDay) {
                    if (!is_null($status) && $status === '0') { $rtn = 2; }
                    else if ($valid == '0' || is_null($status)) { $rtn = 1; }
                }
                else { $rtn = 1; }

                if ($year < $thisYear) { # 閲覧年以前の場合
                    if ($thisMonth < $startMonth) { if ($month < $startMonth) { $rtn = 3; } }
                    else { $rtn = 3; }
                    if ($year < $lastYear) { $rtn = 3; }
                }

                if ($year == $thisYear) { if ($thisMonth >= $startMonth) { if ($month < $startMonth) { $rtn = 3; } } } # 閲覧年の場合

                if ( ($year == $thisYear && $month >= $thisMonth) || $year > $thisYear) { # 翌月以降の場合
                    if ($month == $thisMonth && $dayCount >= $compDay && $rtn == 1) { $rtn = 3; } else
                    if ($month > $thisMonth || $year > $thisYear) { $rtn = 3; }
                }
                break;

            case 'daily_report_statistics':
                $valid     = $this->getArrValueByKey('valid',      $arr);
                $status    = $this->getArrValueByKey('status',     $arr);
                $year      = $this->getArrValueByKey('year',       $arr);
                $month     = $this->getArrValueByKey('month',      $arr);
                $day       = $this->getArrValueByKey('day',        $arr);
                $targetDay = $this->getArrValueByKey('target_day', $arr);
                $dayCount  = $this->getArrValueByKey('day_count',  $arr);

                if ($year == $thisYear) { # 表示年が当年なら
                    if ($month == $thisMonth) { # 表示月が当月なら
                        if ($targetDay == $today) { # 表示日が当日なら
                            if ($day >= $targetDay - 1) {
                                $rtn = 3;
                                if ($valid == '0') { $rtn = 1; }
                                else if ($status == '0') { $rtn = 2; }
                            }
                            else { $rtn = 1; }
                        }
                        else if ($targetDay > date('d')) { $rtn = 3; } # 表示日が未来なら
                        else if ($targetDay < date('d') || empty($targetDay)) { # 表示日が過去なら
                            if ($day >= $targetDay - 1) {
                                $rtn = 3;
                                if ($valid == '0') { $rtn = 1; }
                                else if ($status == '0') { $rtn = 2; }
                            }
                            else { $rtn = 1; }
                        }
                    }
                    else if ($month < $thisMonth) { # 表示月が過去の場合
                        if ($day >= $targetDay) {
                            $rtn = 3;
                            if ($valid == '0') { $rtn = 1; }
                            else if ($status == '0') { $rtn = 2; }
                        }
                        else { $rtn = 1; }
                    }
                    else if ($month > $thisMonth) { $rtn = 3; } # 表示月が未来の場合
                }
                else if ($year < $thisYear) { # 表示年が過去の場合
                    if ($dayCount == $this->getLastDay($year, $month)) { $rtn = 3; }
                    else { $rtn = 1; }
                }
                else if ($year > $thisYear) { $rtn = 3; } # 表示年が未来の場合
                if ($this->getDateForMonthStart($year, $month) < $this->getDateForMonthStart(date('Y', mktime(0, 0, 0, $thisMonth, $today, $thisYear)), $startMonth)) { $rtn = 3; } # 運用開始年以前のデータ入力状況を参照しない処理
                break;

            # 月報データの前年度比
            case 'monthly_report_of_comparison_in_the_previous_year':
                $year       = $this->getArrValueByKey('year',          $arr);
                $mainData   = $this->getArrValueByKey('main_data',     $arr);
                $month      = $this->getArrValueByKey('month',         $mainData);
                $valid      = $this->getArrValueByKey($year.'_valid',  $mainData);
                $status     = $this->getArrValueByKey($year.'_status', $mainData);

                if ($valid === '0' || (is_null($valid) && is_null($status))) { $rtn = 1; }
                else if ($status === '0') { $rtn = 2; }
                if ($year == $thisYear && $month == $thisMonth && $rtn == 1) { $rtn = 3; }
                # 当月以降のデータ入力状況を参照しない処理
                if ($this->getDateForMonthStart($year, $month) > $this->getDateForMonthStart($thisYear, $thisMonth)) { $rtn = 3; }
                # 運用開始年以前のデータ入力状況を参照しない処理
                else if ($this->getDateForMonthStart($year, $month) < $this->getDateForMonthStart(date('Y', mktime(0, 0, 0, $thisMonth, $today, $thisYear)), $startMonth)) { $rtn = 3; }
                break;

            # 病院月間報告書患者数総括
            case 'summary_of_number_of_hospital_monthly_report_patients':
                $year     = $this->getArrValueByKey('year',      $arr);
                $month    = $this->getArrValueByKey('month',     $arr);
                $dayCount = $this->getArrValueByKey('day_count', $arr);
                $valid    = $this->getArrValueByKey('valid',     $arr);
                $status   = $this->getArrValueByKey('status',    $arr);
                $lastDay  = $this->getLastDay($year, $month);
                $mainData = null;

                if (is_null($dayCount)) { # 月報またはデータがない場合
                    $mainData = $this->getArrValueByKey('main_data', $arr);
                    $status   = $this->getArrValueByKey('status',    $mainData);

                    if (is_null($status)) { $rtn = 1; } # データがない場合
                    else { if($status === '0') { $rtn = 2; } } # 月報の場合
                    if ($thisYear < $year) { $rtn = 3; }
                    else if ($thisYear == $year) {
                        if ($thisMonth <= $month) { if ($rtn == 1) { $rtn = 3; } }
                        else if ($thisMonth > $month) { if (is_null($mainData)) { $rtn = 1; } }
                    }
                    else if ($thisYear > $year){
                        if($status === '0') { $rtn = 2; }
                        else { $rtn = 1; }
                    }
                }
                else { # 日報の1月分のデータ
                    if ($valid === '0' || (is_null($valid) && is_null($status))) { $rtn = 1; }
                    else if ($status === '0') { $rtn = 2; }
                    if ($thisYear > $year) { if ($dayCount < $lastDay) { $rtn= 1; } }
                    else if($thisYear == $year) {
                        if ($thisMonth > $month) { if ($lastDay > $dayCount) { $rtn= 1; } }
                        else if ($thisMonth == $month) { if (date('d') - 1 > $dayCount) { $rtn = 1; } }
                        else if ($thisMonth < $month) { $rtn = 3; }
                    }
                    else if ($thisYear < $year) { $rtn = 3; }
                }
                break;

            # 病院月刊報告書(行為件数)
            case 'get_hospital_monthly_report':
                $year     = $this->getArrValueByKey('year',   $arr);
                $month    = $this->getArrValueByKey('month',  $arr);
                $status   = $this->getArrValueByKey('status', $arr);
                if (is_null($status)) {
                    $rtn = 1;
                    if ($month==$thisMonth) { if ($rtn == 1) { $rtn = 3; } } # データがない場合
                }
                if($status === '0') { $rtn = 2; } # 月報の場合
                if ($thisYear < $year) { $rtn = 3; }
                else if ($thisYear > $year) { if (is_null($status)) { $rtn = 1; } }
                else if ($thisYear == $year) { if ($thisMonth <= $month) { if ($rtn == 1) { $rtn = 3; } } }
                break;
        }

        return $rtn;
    }

    /**
     * メニューリスト出力(閲覧権限対応)
     * @param  array  $arr
     * @param  array  $font
     * @return string $rtnStr
     */
    function getMenuList($arr, $font=null, $title=null, $empId=null)
    {
        $rtnStr = '';
        if (is_null($title)) { $title='管理指標'; }
        /* @var $indicatorAjax IndicatorAjax */
        $indicatorAjax = $this->getIndicatorAjax();

        $cNum  = $this->getArrayValueByKeyForEmptyIsNull('category',     $arr);
        $scNum = $this->getArrayValueByKeyForEmptyIsNull('sub_category', $arr);
        $no    = $this->getArrayValueByKeyForEmptyIsNull('no',           $arr);
        $plant = $this->getArrayValueByKeyForEmptyIsNull('plant',        $arr);
        $type  = $this->getArrayValueByKeyForEmptyIsNull('type',         $arr);
        $noArr = $this->getNoForNo($no);

        if (is_null($noArr)) {
            /* @var $indicatorFacility IndicatorFacility */
            $indicatorFacility  = $this->getIndicatorFacility();
            $noArr              = $indicatorFacility->getNoForNo($no);
            if (is_null($noArr)) {
                /* @var $indicatorNurseHome IndicatorNurseHome */
                $indicatorNurseHome = $this->getIndicatorNurseHome();
                $noArr              = $indicatorNurseHome->getNoForNo($no);
                if (is_null($noArr)) {
                    /* @var $indicatorReturn IndicatorReturn */
                    $indicatorReturn = $this->getIndicatorReturn();
                    $noArr           = $indicatorReturn->getNoForNo($no);
                }
            }
        }

        if (isset($no) && is_null($cNum) && is_null($scNum)) {
            $cNum  = $this->getArrValueByKey('category',     $noArr);
            $scNum = $this->getArrValueByKey('sub_category', $noArr);
        }
        if (is_array($noArr)) { $title = $this->getArrValueByKey('name', $noArr); }
        if ($cNum != 0) { $plant = null; }
        if (is_null($font)) { $font = $this->getFont(); }

        $reportNo = null;

        # default
        $rtnStr .= $this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_first')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_second')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_third')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_fourth')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'ctg'),     array('name' => 'name', 'value' => 'ctg'),     array('name' => 'value', 'value' => $cNum)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'sub_ctg'), array('name' => 'name', 'value' => 'sub_ctg'), array('name' => 'value', 'value' => $scNum)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => '_no'),     array('name' => 'name', 'value' => '_no'),     array('name' => 'value', 'value' => $no)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'last_no'), array('name' => 'name', 'value' => 'last_no'), array('name' => 'value', 'value' => $no)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => '_plant'),  array('name' => 'name', 'value' => '_plant'),  array('name' => 'value', 'value' => $plant)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => '_emp_id'), array('name' => 'name', 'value' => '_emp_id'), array('name' => 'value', 'value' => $empId)))
                  .'<table><tr><td>';

        $cArr = $indicatorAjax->getCategory($empId);
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'name', 'value' => 'first'), array('name' => 'id', 'value' => 'first'), array('name' => 'onchange', 'value' => "chg1st(this, '".$empId."')")), $cArr, $cNum);

        $secondArr = array();
        if (!is_null($cNum)) {
            switch ($cNum) {
                case '0':
                    $secondArr = $indicatorAjax->getSubCategory($empId, $cNum);
                    break;

                case '1': case '2': case '3':
                    $secondArr = $indicatorAjax->getSubCategory($empId, $cNum);
                    break;
            }

            $rtnStr .= '</td><td><div id="second_div" style="display: ;">';
        }
        else {
            $rtnStr .= '</td><td><div id="second_div" style="display: none;">';
        }

        $rtnStr .= $this->getHtmlSelect(
            array(array('name' => 'name', 'value' => 'second'), array('name' => 'id', 'value' => 'second'), array('name' => 'onchange', 'value' => "chg2nd(this, '".$empId."');")),
            $secondArr,
            $scNum
        );

        $thirdArr = array();
        $thirdNum = null;

        if ($scNum) {
            switch ($cNum) {
                case '0':
                    $thirdNum = $plant;
                    $thirdArr = $indicatorAjax->getFacility($empId, $cNum, $scNum);
                    break;

                case '1': case '2': case '3':
                    $reportNo = $no;
                    $thirdNum = $no;
                    $thirdArr = $indicatorAjax->getDisplayNoNameArr($cNum, $scNum, null, $empId);
                    break;
            }
            $rtnStr .= '</div></td><td><div id="third_div" style="display: ;">';
        }
        else {
            $rtnStr .= '</div></td><td><div id="third_div" style="display: none;">';
        }

        $rtnStr .= $this->getHtmlSelect(
            array(array('name' => 'name', 'value' => 'third'), array('name' => 'id', 'value' => 'third'), array('name' => 'onchange', 'value' => "chg3rd(this, '".$empId."');")),
            $thirdArr,
            $thirdNum
        );

        $fourthNum = null;
        $fourthArr = array();

        if ($cNum==0 && !is_null($thirdNum)) {
            $reportNo = $no;
            $fourthNum = $no;
            $fourthArr = $indicatorAjax->getDisplayNoNameArr($cNum, $scNum, null, $empId, $plant);
            $rtnStr .= '</div></td><td><div id="fourth_div" style="display: ;">';
        }
        else {
            $rtnStr .= '</div></td><td><div id="fourth_div" style="display: none;">';
        }
        $rtnStr .= $this->getHtmlSelect(
            array(array('name' => 'name', 'value' => 'fourth'), array('name' => 'id', 'value' => 'fourth'), array('name' => 'onchange', 'value' => "chg4th(this, '".$empId."');")),
            $fourthArr,
            $fourthNum
        );
        $rtnStr .= '</div></td></tr></table><div style="display: none;">';

        # target file
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_0_1')), $indicatorAjax->getDisplayNoFileNameArr(0, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_0_2')), $indicatorAjax->getDisplayNoFileNameArr(0, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_1')), $indicatorAjax->getDisplayNoFileNameArr(1, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_2')), $indicatorAjax->getDisplayNoFileNameArr(1, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_3')), $indicatorAjax->getDisplayNoFileNameArr(1, 3));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_4')), $indicatorAjax->getDisplayNoFileNameArr(1, 4));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_5')), $indicatorAjax->getDisplayNoFileNameArr(1, 5));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_1')), $indicatorAjax->getDisplayNoFileNameArr(2, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_2')), $indicatorAjax->getDisplayNoFileNameArr(2, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_3')), $indicatorAjax->getDisplayNoFileNameArr(2, 3));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_4')), $indicatorAjax->getDisplayNoFileNameArr(2, 4));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_5')), $indicatorAjax->getDisplayNoFileNameArr(2, 5));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_3_1')), $indicatorAjax->getDisplayNoFileNameArr(3, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_3_2')), $indicatorAjax->getDisplayNoFileNameArr(3, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_3_3')), $indicatorAjax->getDisplayNoFileNameArr(3, 3));

        $rtnStr .= '</div>';
        $titleHeader = '<div style="text-align: left;"><span style="margin-left: auto; margin-right: auto"><h3 style="background-color: #ffdddd;">'.$title.'</h3></span></div>';
        $rtnStr .= $titleHeader;
//        $targetFileNameArr = $indicatorAjax->getDisplayNoFileNameArr($cNum, $scNum, $reportNo);
        return $rtnStr;
    }

    /**
     * メニューリスト出力
     * @param  array  $arr
     * @param  array  $font
     * @return string $rtnStr
     */
    function getRegisterMenuList($arr, $font=null, $title='管理指標')
    {
        $indicatorData      = $this->getIndicatorData();      # new IndicatorData()
        $indicatorFacility  = $this->getIndicatorFacility();  # new IndicatorFacility()
        $indicatorNurseHome = $this->getIndicatorNurseHome(); # new IndicatorNurseHome()
        $indicatorReturn    = $this->getIndicatorReturn();    # new IndicatorReturn()

        $rtnStr = '';

        $cNum  = $this->getArrayValueByKeyForEmptyIsNull('category',     $arr);
        $scNum = $this->getArrayValueByKeyForEmptyIsNull('sub_category', $arr);
        $no    = $this->getArrayValueByKeyForEmptyIsNull('no',           $arr);
        $plant = $this->getArrayValueByKeyForEmptyIsNull('plant',        $arr);
        $type  = $this->getArrayValueByKeyForEmptyIsNull('type',         $arr);
        $noArr = $this->getNoForNo($no);

        if (is_null($noArr)) {
            $noArr = $indicatorFacility->getNoForNo($no);
            if (is_null($noArr)) {
                $noArr = $indicatorNurseHome->getNoForNo($no);
                if (is_null($noArr)) { $noArr = $indicatorReturn->getNoForNo($no); }
            }
        }

        if (isset($no) && is_null($cNum) && is_null($scNum)) {
            $cNum  = $this->getArrValueByKey('category',     $noArr);
            $scNum = $this->getArrValueByKey('sub_category', $noArr);
        }
        if (is_array($noArr)) { $title = $this->getArrValueByKey('name', $noArr); }
        if ($cNum != 0) { $plant = null; }
        if (is_null($font)) { $font = $this->getFont(); }

        # default
        $rtnStr .= $this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_first')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_second')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_third')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'hidden_fourth')))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'ctg'), array('name' => 'name', 'value' => 'ctg'), array('name' => 'value', 'value' => $cNum)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => 'sub_ctg'), array('name' => 'name', 'value' => 'sub_ctg'), array('name' => 'value', 'value' => $scNum)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => '_no'), array('name' => 'name', 'value' => '_no'), array('name' => 'value', 'value' => $no)))
                  .$this->getHtmlInput(array(array('name' => 'type', 'value' => 'hidden'), array('name' => 'id', 'value' => '_plant'), array('name' => 'name', 'value' => '_plant'), array('name' => 'value', 'value' => $plant)))
                  .'<table><tr><td>';
        $cArr = $this->getCategory();
        $rtnStr .= $this->getHtmlSelect(
            array(array('name' => 'name', 'value' => 'first'), array('name' => 'id', 'value' => 'first'), array('name' => 'onchange', 'value' => "changeFirst(this);")),
            $cArr,
            $cNum
        );

        $secondArr = array();
        if (!is_null($cNum)) {
            switch ($cNum) {
                case '0':
                    $secondArr = $indicatorFacility->getSubCategory($cNum);
                    break;

                case '1':
                    $secondArr = $this->getSubCategory($cNum);
                    break;

                case '2':
                    $secondArr = $indicatorNurseHome->getSubCategory($cNum);
                    break;

                case '3':
                    $secondArr = $indicatorReturn->getSubCategory($cNum);
                    break;
            }

            $rtnStr .= '</td><td><div id="second_div" style="display: ;">';
        }
        else {
            $rtnStr .= '</td><td><div id="second_div" style="display: none;">';
        }
        $rtnStr .= $this->getHtmlSelect(
            array(array('name' => 'name', 'value' => 'second'), array('name' => 'id', 'value' => 'second'), array('name' => 'onchange', 'value' => "changeSecond(this);")),
            $secondArr,
            $scNum
        );

        $thirdArr = array();
        $thirdNum = null;

        if ($scNum) {
            switch ($cNum) {
                case '0':
                    $thirdNum = $plant;
                    $thirdArr = $indicatorData->getPlantList($scNum);
                    break;

                case '1':
                    $thirdNum = $no;
                    $thirdArr = $this->getDisplayNoNameArr($cNum, $scNum);
                    break;

                case '2':
                    $thirdNum = $no;
                    $thirdArr = $indicatorNurseHome->getDisplayNoNameArr($cNum, $scNum);
                    break;

                case '3':
                    $thirdNum = $no;
                    $thirdArr = $indicatorReturn->getDisplayNoNameArr($cNum, $scNum);
                    break;
            }
            $rtnStr .= '</div></td><td><div id="third_div" style="display: ;">';
        }
        else {
            $rtnStr .= '</div></td><td><div id="third_div" style="display: none;">';
        }

        $rtnStr .= $this->getHtmlSelect(
            array(array('name' => 'name', 'value' => 'third'), array('name' => 'id', 'value' => 'third'), array('name' => 'onchange', 'value' => "changeThird(this);")),
            $thirdArr,
            $thirdNum
        );

        $fourthNum = null;
        $fourthArr = array();

        if ($cNum==0 && !is_null($thirdNum)) {
            $fourthNum = $no;
            $fourthArr = $indicatorFacility->getDisplayNoNameArr($cNum, $scNum);
            $rtnStr .= '</div></td><td><div id="fourth_div" style="display: ;">';
        }
        else {
            $rtnStr .= '</div></td><td><div id="fourth_div" style="display: none;">';
        }
        $rtnStr .= $this->getHtmlSelect(
            array(array('name' => 'name', 'value' => 'fourth'), array('name' => 'id', 'value' => 'fourth'), array('name' => 'onchange', 'value' => "changeFourth(this);")),
            $fourthArr,
            $fourthNum
        );
        $rtnStr .= '</div></td></tr></table><div style="display: none;">';

        # second
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'second_0')), $indicatorFacility->getSubCategory(0));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'second_1')), $this->getSubCategory(1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'second_2')), $indicatorNurseHome->getSubCategory(2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'second_3')), $indicatorReturn->getSubCategory(3));

        # third
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_0_1')), $indicatorData->getPlantList(1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_0_2')), $indicatorData->getPlantList(2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_1_1')), $this->getDisplayNoNameArr(1, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_1_2')), $this->getDisplayNoNameArr(1, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_1_3')), $this->getDisplayNoNameArr(1, 3));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_1_4')), $this->getDisplayNoNameArr(1, 4));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_1_5')), $this->getDisplayNoNameArr(1, 5));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_2_1')), $indicatorNurseHome->getDisplayNoNameArr(2, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_2_2')), $indicatorNurseHome->getDisplayNoNameArr(2, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_2_3')), $indicatorNurseHome->getDisplayNoNameArr(2, 3));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_2_4')), $indicatorNurseHome->getDisplayNoNameArr(2, 4));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_2_5')), $indicatorNurseHome->getDisplayNoNameArr(2, 5));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_3_1')), $indicatorReturn->getDisplayNoNameArr(3, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_3_2')), $indicatorReturn->getDisplayNoNameArr(3, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'third_3_3')), $indicatorReturn->getDisplayNoNameArr(3, 3));

        # fourth
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'fourth_0_1')), $indicatorFacility->getDisplayNoNameArr(0, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'fourth_0_2')), $indicatorFacility->getDisplayNoNameArr(0, 2));

        # target file
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_0_1')), $indicatorFacility->getDisplayNoFileNameArr(0, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_0_2')), $indicatorFacility->getDisplayNoFileNameArr(0, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_1')), $this->getDisplayNoFileNameArr(1, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_2')), $this->getDisplayNoFileNameArr(1, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_3')), $this->getDisplayNoFileNameArr(1, 3));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_4')), $this->getDisplayNoFileNameArr(1, 4));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_1_5')), $this->getDisplayNoFileNameArr(1, 5));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_1')), $indicatorNurseHome->getDisplayNoFileNameArr(2, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_2')), $indicatorNurseHome->getDisplayNoFileNameArr(2, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_3')), $indicatorNurseHome->getDisplayNoFileNameArr(2, 3));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_4')), $indicatorNurseHome->getDisplayNoFileNameArr(2, 4));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_2_5')), $indicatorNurseHome->getDisplayNoFileNameArr(2, 5));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_3_1')), $indicatorReturn->getDisplayNoFileNameArr(3, 1));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_3_2')), $indicatorReturn->getDisplayNoFileNameArr(3, 2));
        $rtnStr .= $this->getHtmlSelect(array(array('name' => 'id', 'value' => 'target_file_3_3')), $indicatorReturn->getDisplayNoFileNameArr(3, 3));

        $rtnStr .= '</div>';
        $titleHeader = '<div style="text-align: left;"><span style="margin-left: auto; margin-right: auto"><h3 style="background-color: #ffdddd;">'.$title.'</h3></span></div>';
        $rtnStr .= $titleHeader;
        return $rtnStr;
    }

    function getIndicatorData() {
        if (!is_object($this->indicatorData)) { $this->setIndicatorData(); }
        return $this->indicatorData;
    }

    function setIndicatorData() {
        $this->indicatorData = new IndicatorData(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @var IndicatorAjax
     */
    var $indicatorAjax;

    /**
     * @return IndicatorAjax
     */
    function getIndicatorAjax() {
        if (!is_object($this->indicatorAjax)) { $this->setIndicatorAjax(); }
        return $this->indicatorAjax;
    }

    /*
     * @return void
     */
    function setIndicatorAjax() {
        $this->indicatorAjax = new IndicatorAjax(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorFacility
     */
    function getIndicatorFacility() {
        if (!is_object($this->indicatorFacility)) { $this->setIndicatorFacility(); }
        return $this->indicatorFacility;
    }

    /**
     * @return void
     */
    function setIndicatorFacility() {
        $this->indicatorFacility = new IndicatorFacility(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorNurseHome
     */
    function getIndicatorNurseHome() {
        if (!is_object($this->indicatorNurseHome)) { $this->setIndicatorNurseHome(); }
        return $this->indicatorNurseHome;
    }

    /**
     * @return void
     */
    function setIndicatorNurseHome() {
        $this->indicatorNurseHome = new IndicatorNurseHome(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorReturn
     */
    function getIndicatorReturn() {
        if (!is_object($this->indicatorReturn)) { $this->setIndicatorReturn(); }
        return $this->indicatorReturn;
    }

    /**
     * @return void
     */
    function setIndicatorReturn() {
        $this->indicatorReturn = new IndicatorReturn(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * 画面に表示対象の年指定用のセレクトボックスを表示する。
     * @param  array  $arr
     * @return string [html select tab]
     */
    function getSelectDislpayYear($arr)
    {
        $year        = $this->getArrValueByKey('display_year',  $arr);
        $category    = $this->getArrValueByKey('category',      $arr);
        $subCategory = $this->getArrValueByKey('sub_category',  $arr);
        $no          = $this->getArrValueByKey('no',            $arr);
        $type        = $this->getArrValueByKey('type',          $arr);
        $plant       = $this->getArrValueByKey('plant ',        $arr);

        if (is_null($year)) { $year = date('Y'); }

		$selectData[] = array('year' => date('Y') - 4);
		$selectData[] = array('year' => date('Y') - 3);
		$selectData[] = array('year' => date('Y') - 2);
        $selectData[] = array('year' => date('Y') - 1);
        $selectData[] = array('year' => date('Y'));

        $rtnStr = sprintf('<select id="display_year" name="display_year" onchange="selectYear(this, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" >', $category, $subCategory, $no, $type, $plant);
        foreach ($selectData as $val) {
            if ($year == $val['year']) { $rtnStr .= sprintf('<option value="%s" selected="selected" >%s</option>', $val['year'], $val['year']); }
            else { $rtnStr .= sprintf('<option value="%s" >%s</option>', $val['year'], $val['year']); }
        }
        $rtnStr .= '</select>';
        return $rtnStr;
    }

    /**
     * 年度表示用の getSelectDislpayYear
     * @param  array  $arr
     * @return string [html select tag]
     */
    function getSelectFiscalDislpayYear($arr)
    {
        $fiscalYear  = $this->getArrValueByKey('fiscal_year',   $arr);
        $fiscalYear  = is_null($fiscalYear)? $this->getCurrentYear() : $fiscalYear;

        $category    = $this->getArrValueByKey('category',      $arr);
        $subCategory = $this->getArrValueByKey('sub_category',  $arr);
        $no          = $this->getArrValueByKey('no',            $arr);
        $type        = $this->getArrValueByKey('type',          $arr);
        $plant       = $this->getArrValueByKey('plant ',        $arr);

        if (is_null($fiscalYear)) { $fiscalYear = date('Y'); }

		$selectData[] = array('year' => date('Y') - 4);
		$selectData[] = array('year' => date('Y') - 3);
		$selectData[] = array('year' => date('Y') - 2);
        $selectData[] = array('year' => date('Y') - 1);
        $selectData[] = array('year' => date('Y'));

        $rtnStr = sprintf('<select id="fiscal_year" name="fiscal_year" onchange="selectFiscalYear(this, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" >', $category, $subCategory, $no, $type, $plant);
        foreach ($selectData as $val) {
            if ($fiscalYear == $val['year']) { $rtnStr .= sprintf('<option value="%s" selected="selected" >%s</option>', $val['year'], $val['year']); }
            else { $rtnStr .= sprintf('<option value="%s" >%s</option>', $val['year'], $val['year']); }
        }
        $rtnStr .= '</select>';

        return $rtnStr;
    }

    /**
     * 画面に表示対象の月指定用のセレクトボックスを表示する。
     * @param  array  $arr
     * @return string [html select tag]
     */
    function getSelectDisplayMonth($arr)
    {
        $year        = $this->getArrValueByKey('display_year',  $arr);
        $year        = empty($year)? date('Y') : $year;
        $month       = $this->getArrValueByKey('display_month', $arr);
        $month       = empty($month)? date('m') : $month;
        $category    = $this->getArrValueByKey('category',      $arr);
        $subCategory = $this->getArrValueByKey('sub_category',  $arr);
        $no          = $this->getArrValueByKey('no',            $arr);
        $type        = $this->getArrValueByKey('type',          $arr);
        $plant       = $this->getArrValueByKey('plant ',        $arr);

        $rtnStr     = '';
        $data       = $this->getIndicatorData();
        $selectData = $data->getSelectMonth();
        $rtnStr    .= sprintf('<select id="display_month" name="display_month" onchange="selectMonth(this, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" >', $category, $subCategory, $no, $type, $plant);
        foreach ($selectData as $val) {
            if ($month == $val['month']) { $rtnStr .= sprintf('<option value="%s" selected="selected" >%s</option>', $val['month'], $val['month']); }
            else { $rtnStr .= sprintf('<option value="%s" >%s</option>', $val['month'], $val['month']); }
        }
        $rtnStr .= '</select>';

        return $rtnStr;
    }

    /**
     * 画面に表示対象の日指定用のセレクトボックスを表示する。
     * @param  array  $arr
     * @return string [html select tag]
     */
    function getSelectDisplayDay($arr)
    {
        $year        = $this->getArrValueByKey('display_year',  $arr);
        $year        = empty($year)? date('Y') : $year;
        $month       = $this->getArrValueByKey('display_month', $arr);
        $month       = empty($month)? date('m') : $month;
        $day         = $this->getArrValueByKey('display_day',   $arr);
        $day         = empty($day)? date('d') : $day;
        $category    = $this->getArrValueByKey('category',      $arr);
        $subCategory = $this->getArrValueByKey('sub_category',  $arr);
        $no          = $this->getArrValueByKey('no',            $arr);
        $type        = $this->getArrValueByKey('type',          $arr);
        $plant       = $this->getArrValueByKey('plant ',        $arr);

        $rtnStr     = '';
        $data       = $this->getIndicatorData();
        $selectDate = $data->getSelectDay($data->getLastDay($year, $month));
        $rtnStr    .= sprintf('<select id="display_day" name="display_day" onchange="selectDay(this, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" >', $category, $subCategory, $no, $type, $plant);
        foreach ($selectDate as $val) {
            if ($day == $val['day']) { $rtnStr .= sprintf('<option value="%s" selected="selected" >%s</option>', $val['day'], $val['day']); }
            else { $rtnStr .= sprintf('<option value="%s" >%s</option>', $val['day'], $val['day']); }
        }
        $rtnStr .= '</select>';

        return $rtnStr;
    }

    /**
     * @param  string $type : 'percent', 'decimal', 'comma', 'unit', 'comma_parenthesis', 'decimal_parenthesis', 'percent_parenthesis'
     * @return array
     */
    function getMsoNumberFormat($type)
    {
        $msoNumberFormatArr = $this->msoNumberFormatArr;
        return $this->getArrValueByKey($type, $this->msoNumberFormatArr);
    }

    /**
     * 各画面に応じたエクセルファイルを出力する
     * @access public
     * @param  integer $no
     * @param  array   $dataArr
     * @param  array   $plantArr
     * @param  array   $arr
     * @param  array   $optionArr
     * @return string
     */
    function outputExcelData($no, $dataArr, $plantArr, $arr, $optionArr=null)
    {
        $excelData = null;
        switch ($no) {
            case '1': # 外来延患者数
				$excelData = $this->outputExcelComparisonDisplayInThePreviousYear($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '2': # 外来平均患者数
				$excelData = $this->outputExcelComparisonDisplayInThePreviousYearAverage($dataArr, $plantArr, $arr, $optionArr);
                break;

            case  '3': # 全病棟延入院患者数
            case  '4': # 一般病棟延入院患者数
            case  '5': # 障害患者病棟延入院患者数
            case  '6': # 精神一般病棟延入院患者数
            case  '7': # 医療療養病棟延入院患者数
            case  '8': # 介護療養病棟延入院患者数
            case  '9': # 回復期リハ病棟延入院患者数
            case '10': # 亜急性期病床延入院患者数
            case '11': # 緩和ケア病棟延入院患者数
            case '12': # ICU及びハイケアユニット延入院患者数
            case '13': # 小児管理料延入院患者数
            case '14': # 精神療養病棟延入院患者数
            case '15': # 特殊疾患(2)延入院患者数
            case '16': # 認知症病棟延入院患者数
				$excelData = $this->outputExcelComparisonDisplayInThePreviousYear($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '17': # 全病棟平均入院患者数
            case '18': # 一般病棟平均入院患者数
            case '19': # 障害者病棟平均入院患者数
            case '20': # 精神一般病棟平均入院患者数
            case '21': # 医療療養病棟平均入院患者数
            case '22': # 介護療養病棟平均入院患者数
            case '23': # 回復期リハ病棟平均入院患者数
            case '24': # 亜急性期病床平均入院患者数
            case '25': # 緩和ケア病棟平均入院患者数
            case '26': # ICU及びハイケアユニット平均入院患者数
            case '27': # 小児管理料平均入院患者数
            case '28': # 精神療養病棟平均入院患者数
            case '29': # 特殊疾患(2)平均入院患者数
            case '30': # 認知症病棟平均入院患者数
				$excelData = $this->outputExcelComparisonDisplayInThePreviousYearAverage($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '31': # 全病棟稼働率(稼働病床)
            case '32': # 全病棟稼働率(稼働病床)
            case '33': # 一般病棟稼働率
            case '34': # 障害者病棟稼働率
            case '35': # 精神一般病棟稼働率
            case '36': # 医療療養病棟稼働率
            case '37': # 介護療養病棟稼働率
            case '38': # 回復期リハ病棟稼働率
            case '39': # 亜急性期病床稼働率
            case '40': # 緩和ケア病棟稼働率
            case '41': # ICU及びハイケアユニット稼働率
            case '42': # 小児管理料稼働率
            case '43': # 精神療養病棟稼働率
            case '44': # 特殊疾患(2)稼働率
            case '45': # 認知症病棟稼働率
				$excelData = $this->outputExcelUtilizationRatesDisplayData($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '46': # 全病棟入院患者件数
            case '47': # 全病棟退院患者件数
				$excelData = $this->outputExcelComparisonDisplayInThePreviousYear($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '48': # 一般病棟平均在院日数
            case '49': # 新患者数
            case '50': # 紹介患者数
            case '51': # 救急依頼件数
            case '52': # 救急受入率
            case '53': # 救急入院患者数
            case '54': # 救急入院率
            case '55': # 診療時間外患者数
            case '56': # 時間外入院患者数
            case '57': # 診療時間外入院率
            case '58': # 紹介率
            case '59': # 手術件数(全麻)
            case '60': # 手術件数(腰麻)
            case '61': # 手術件数(伝達・局麻)
                $excelData = $this->outputExcelMonthlyReportOfComparisonInThePreviousYear($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '62': # 病院別患者日報
                $excelData = $this->outputExcelDailyReportAccordingToHospital($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '63': # 病院別患者日報(シンプル版)
                $excelData = $this->outputExcelDailySimpleReportAccordingToHospital($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '64': # [施設別] 患者月報(日報・日毎)
                $excelData = $this->outputExcelMonthlyReportAccordingToDay($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '65': # 病院別患者月報
                $excelData = $this->outputExcelMonthlyReportAccordingToHospital($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '66': # 病院別患者月報(前年比較)
                $excelData = $this->outputExcelMonthlyReportCompDisplayInThePreviousYear($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '71': # 病院月間報告書患者数総括
                $excelData = $this->outputExcelSummaryOfNumberOfHospitalMonthlyReportPatients($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '72': # 病院月間報告書(行為件数)
                $excelData = $this->outputExcelHospitalMonthlyReport($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '73': # 病院別患者日報(累積統計表)
                $excelData = $this->outputExcelDailyReportAccumulationStatistics($dataArr, $plantArr, $arr, $optionArr);
                break;
        }

        return $excelData;
    }

    /**
     * エクセル表示用の getComparisonDisplayInThePreviousYear
     * No. 3 全病棟延入院患者数
     * No. 4 一般病棟延入院患者数
     * No. 5 障害患者病棟延入院患者数
     * No. 6 精神一般病棟延入院患者数
     * No. 7 医療療養病棟延入院患者数
     * No. 8 介護療養病棟延入院患者数
     * No. 9 回復期リハ病棟延入院患者数
     * No.10 亜急性期病床延入院患者数
     * No.11 緩和ケア病棟延入院患者数
     * No.12 ICU及びハイケアユニット延入院患者数
     * No.13 小児管理料延入院患者数
     * No.14 精神療養病棟延入院患者数
     * No.15 特殊疾患(2)延入院患者数
     * No.16 認知症病棟延入院患者数
     * No.46 全病棟入院患者件数
     * No.47 全病棟退院患者件数
     * @access protected
     * @param  array     $dataArr  表示対象データ
     * @param  array     $plantArr 病院リスト
     * @param  array     $arr      表示オプション
	 * @param  array	  $optionArr      表示年度
	 * @return mixed               表示データ(html table)
     */
	function outputExcelComparisonDisplayInThePreviousYear($dataArr, $plantArr, $arr, $optionArr)//20110419
    {
        # 各表示フラグ
        $total      = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr); # 施設合計
        $average    = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr); # 施設平均
        $raito      = $this->getArrayValueByKeyForEmptyIsNull('raito',       $arr); # 増減率
        $amgTotal   = $this->getArrayValueByKeyForEmptyIsNull('amg_total',   $arr); # AMG合計
        $amgAverage = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr); # AMG平均

		//20110419
/*
        $lastYear    = $this->getLastYear();
        $currentYear = $this->getCurrentYear();
        $startMonth  = $this->getFiscalYearStartMonth();
*/
		if($optionArr == null)
		{
			$currentYear = $this->getCurrentYear();
		}
		else
		{
			$currentYear	= $this->getArrValueByKey('year', $optionArr);
		}
		$lastYear		= $currentYear - 1;
		$startMonth	= $this->getFiscalYearStartMonth();
		//20110419
		
        // 増減率がある場合は 1 施設 4 行になる
        $rowspan = (!is_null($raito))? 4 : 3;

        $thColorArr              = array('name' => 'background-color',  'value' => $this->getRgbColor('blue')); # 青
        $compColorArr            = array('name' => 'background-color',  'value' => $this->getRgbColor('green')); # 緑
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');

        $thColor   = $this->getArrValueByKey('name', $thColorArr).   ':'. $this->getArrValueByKey('value', $thColorArr);
        $compColor = $this->getArrValueByKey('name', $compColorArr). ':'. $this->getArrValueByKey('value', $compColorArr);

        $font = $this->getFont();
        $font['color'] = null;
        if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
        $dateTable = $this->getExcelMeta();
        $dateTable .= '<table border="1" class="list this_table" width="100%"><tbody>';

        $fontTag = $this->getFontTag($font);
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

        // header
        $tableHeader = '<tr><td style="'.$thColor.'" colspan="2" />';

        for ($i=0; $i < 12; $i++) {
            $dispMonth = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
            $tableHeader .= '<td style="text-align: center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $dispMonth. '月'). '</td>';
        }

        $tableHeader .= (!is_null($total))?   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['total']). '</td>'   : '';
        $tableHeader .= (!is_null($average))? '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['average']). '</td>' : '';
        $tableHeader .= '</tr>';

        $dateTable   .= $tableHeader;

        $plantCount    = count($plantArr);
        $amgTotalRow   = array();
        $amgAverageRow = array();

        $totalColumnNum = is_null($total)? 0 : 12;
        $averageColumnNum = is_null($average)? 0 : is_null($total)? 12 : 13;

        $startRowNum        = 2;
        $startColumnNum     = 3;

        $columnAlphaNameArr = array(
             2 => 'B',   3 => 'C',   4 => 'D',   5 => 'E',   6 => 'F',   7 => 'G',   8 => 'H',   9 => 'I',  10 => 'J',  11 => 'K',  12 => 'L',  13 => 'M',  14 => 'N',  15 => 'O',  16 => 'P',  17 => 'Q',  18 => 'R',
            19 => 'S',  20 => 'T',  21 => 'U',  22 => 'V',  23 => 'W',  24 => 'X',  25 => 'Y',  26 => 'Z',  27 => 'AA', 28 => 'AB', 29 => 'AC', 30 => 'AD', 31 => 'AE', 32 => 'AF', 33 => 'AG', 34 => 'AH', 35 => 'AI',
            36 => 'AJ', 37 => 'AK', 38 => 'AL', 39 => 'AM', 40 => 'AN', 41 => 'AO', 42 => 'AP', 43 => 'AQ', 44 => 'AR', 45 => 'AS', 46 => 'AT', 47 => 'AU', 48 => 'AV', 49 => 'AW', 50 => 'AX',
        );

        $amgTotalRowNum   = !is_null($amgTotal)? $startRowNum + $plantCount * 4 : null;
        $amgAverageRowNum = !is_null($amgTotalRowNum)? $startRowNum + 4 + $plantCount * 4 : null;

        // data
        if (is_array($dataArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $rowNum = $startRowNum + $plantKey * 4;

                $facilityId  = $this->getArrValueByKey('facility_id', $plantVal);

				//20110419
				//$lastYear    = $this->getLastYear();
                //$currentYear = $this->getCurrentYear();
				if($optionArr == null)
				{
					$currentYear = $this->getCurrentYear();
				}
				else
				{
					$currentYear	= $this->getArrValueByKey('year', $optionArr);
				}
				$lastYear		= $currentYear - 1;
				//20110419
				
				
                $lastRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center;">'.sprintf($fontTag, $lastYear.'年度').'</td>';

                $currentRowHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, $plantVal['facility_name']).'</td>'
                                       .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $compRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';

                $raitoRowHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減率').'</td>';

                $amgTotalHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG合計').'</td>'
                                     .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $amgAverageHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG平均').'</td>'
                                       .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $lastRow    = array();
                $currentRow = array();
                $compRow    = array();
                $raitoRow   = array();

                $lastTotal    = 0;
                $currentTotal = 0;
                $compTotal    = 0;
                $raitoToral   = 0;

                $currentYearMonthCount = 0;
                $lastYearMonthCount    = 0;

                for ($i=0; $i < 12; $i++) {
                    $columnNum = $startColumnNum + $i;

					//20110419
					//$currentYear = $this->getCurrentYear();
                    //$lastYear    = $this->getLastYear();
					if($optionArr == null)
					{
						$currentYear = $this->getCurrentYear();
					}
					else
					{
						$currentYear	= $this->getArrValueByKey('year', $optionArr);
					}
					$lastYear		= $currentYear - 1;
					//20110419
					
                    $month       = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth  + $i - 12;
                    $currentYear = (($startMonth + $i) <= 12)? $currentYear     : $currentYear + 1;
                    $lastYear    = (($startMonth + $i) <= 12)? $lastYear        : $lastYear    + 1;

                    $currentData = 0;
                    $lastData    = 0;
                    $compData    = 0;
                    $raitoData   = 0;

                    foreach ($dataArr as $dataVal) {
                        if ($facilityId == $this->getArrValueByKey('facility_id', $dataVal) && $month == $this->getArrValueByKey('month', $dataVal)) {
                        if (!is_null($this->getArrValueByKey($currentYear, $dataVal))) {
                            $currentData = $currentData + $this->getArrValueByKey($currentYear, $dataVal);
                            $currentYearMonthCount = $currentYearMonthCount + 1;
                            $currentPlantMonthCount[$i] = $currentPlantMonthCount[$i] + 1;
                        }
                        if (!is_null($this->getArrValueByKey($lastYear, $dataVal))) {
                            $lastData    = $lastData + $this->getArrValueByKey($lastYear, $dataVal);
                            $lastYearMonthCount = $lastYearMonthCount + 1;
                            $lastPlantMonthCount[$i] = $lastPlantMonthCount[$i] + 1;
                        }
                        }
                    }

                    $currentCoordinate = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum);
                    $lastCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum+1);
                    $compCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum+2);

                    $compData  = $this->getExcelSubData($currentCoordinate, $lastCoordinate, 1);
                    $raitoData = $this->getExcelDivData($compCoordinate, $lastCoordinate, 3);

                    $currentRow[$i]['data'] = $currentData;
                    $lastRow[$i]['data']    = $lastData;
                    $compRow[$i]['data']    = $compData;
                    $raitoRow[$i]['data']   = $raitoData;

                    $currentTotal = $currentTotal + $currentData;
                    $lastTotal    = $lastTotal    + $lastData;

                    $amgTotalCurrentCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgTotalRowNum);
                    $amgTotalLastCoordinate      = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgTotalRowNum + 1);
                    $amgTotalCompCoordinate      = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgTotalRowNum + 2);
                    $amgAverageCurrentCoordinate = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgAverageRowNum);
                    $amgAverageLastCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgAverageRowNum + 1);
                    $amgAverageCompCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgAverageRowNum + 2);

                    $amgTotalCurrentDataArr = array();
                    $amgTotalLastDataArr = array();
                    for ($l=0; $l < $plantCount; $l++) {
                        $amgTotalCurrentDataArr[] = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $startRowNum + ($l * 4));
                        $amgTotalLastDataArr[]    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $startRowNum + ($l * 4) + 1);
                    }

                    $amgTotalRow['current'][$i]['data'] = $this->getExcelSumDataByArray($amgTotalCurrentDataArr);
                    $amgTotalRow['last'][$i]['data']    = $this->getExcelSumDataByArray($amgTotalLastDataArr);
                    $amgTotalRow['comp'][$i]['data']    = $this->getExcelSubData($amgTotalCurrentCoordinate, $amgTotalLastCoordinate, 1);
                    $amgTotalRow['raito'][$i]['data']   = $this->getExcelDivData($amgTotalCompCoordinate,    $amgTotalLastCoordinate, 3);

                    $amgAverageRow['current'][$i]['data'] = $this->getExcelDivData(sprintf('SUM(%s)', implode(', ', $amgTotalCurrentDataArr)), $currentPlantMonthCount[$i], 1);
                    $amgAverageRow['last'][$i]['data']    = $this->getExcelDivData(sprintf('SUM(%s)', implode(', ', $amgTotalLastDataArr)),    $lastPlantMonthCount[$i],    1);
                    $amgAverageRow['comp'][$i]['data']    = $this->getExcelSubData($amgAverageCurrentCoordinate, $amgAverageLastCoordinate, 1);
                    $amgAverageRow['raito'][$i]['data']   = $this->getExcelDivData($amgAverageCompCoordinate, $amgAverageLastCoordinate, 3);
                }

                $compTotal  = $this->getNumberOfEmptyFloors($lastTotal, $currentTotal, 1);
                $raitoToral = $this->getUtilizationRates($compTotal, $lastTotal, 1);

                # 施設合計表示の場合
                if (!is_null($total)) {
                    $currentSideTotalCoordinate = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $rowNum);
                    $lastSideTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $rowNum + 1);
                    $compSideTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $rowNum + 2);

                    $currentRow[$totalColumnNum]['data'] = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum);
                    $lastRow[$totalColumnNum]['data']    = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum + 1);
                    $compRow[$totalColumnNum]['data']    = $this->getExcelSubData($currentSideTotalCoordinate, $lastSideTotalCoordinate, 1);
                    $raitoRow[$totalColumnNum]['data']   = $this->getExcelDivData($compSideTotalCoordinate, $lastSideTotalCoordinate, 3);
                }

                // 施設平均表示の場合
                if (!is_null($average)) {
                    $currentSideAverageCoordinate = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $rowNum);
                    $lastSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $rowNum + 1);
                    $compSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $rowNum + 2);

                    $currentRow[$averageColumnNum]['data'] = $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum, $currentYearMonthCount, 1);
                    $lastRow[$averageColumnNum]['data']    = $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum + 1, $lastYearMonthCount, 1);
                    $compRow[$averageColumnNum]['data']    = $this->getExcelSubData($currentSideAverageCoordinate, $lastSideAverageCoordinate, 1);
                    $raitoRow[$averageColumnNum]['data']   = $this->getExcelDivData($compSideAverageCoordinate, $lastSideAverageCoordinate, 3);

                    $currentRow[$averageColumnNum]['style']  = array('msoNumberFormat' => $msoNumberFormat_0_0);
                    $lastRow[$averageColumnNum]['style']     = array('msoNumberFormat' => $msoNumberFormat_0_0);
                    $compRow[$averageColumnNum]['style']     = array('msoNumberFormat' => $msoNumberFormat_0_0);
                    $raitoRow[$averageColumnNum]['style']    = array('msoNumberFormat' => $msoNumberFormat_percent);
                }

                $dateTable .= $currentRowHeader.$this->convertCellData($currentRow, $fontTag, array('msoNumberFormat' => $msoNumberFormat_comma)).'</tr>'
                             .$lastRowHeader.$this->convertCellData($lastRow, $fontTag, array('msoNumberFormat' => $msoNumberFormat_comma)).'</tr>'
                             .$compRowHeader.$this->convertCellData($compRow, $fontTag, array($compColorArr)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) {
                    $dateTable .= $raitoRowHeader.$this->convertCellData($raitoRow, $fontTag, array('bgColor' => $compColorArr, 'msoNumberFormat' => $msoNumberFormat_percent)).'</tr>';
                }
            }
        }

        $amgTotalSideTotal_lastData    = 0;
        $amgTotalSideTotal_currentData = 0;
        $amgTotalSideTotal_compData    = 0;
        $amgTotalSideTotal_raitoData   = 0;

        $amgTotalSideAverage_lastData    = 0;
        $amgTotalSideAverage_currentData = 0;
        $amgTotalSideAverage_compData    = 0;
        $amgTotalSideAverage_raitoData   = 0;

        $amgAverageSideTotal_lastData    = 0;
        $amgAverageSideTotal_currentData = 0;
        $amgAverageSideTotal_compData    = 0;
        $amgAverageSideTotal_raitoData   = 0;

        $amgAverageSideAverage_currentData = 0;
        $amgAverageSideAverage_lastData    = 0;
        $amgAverageSideAverage_compData    = 0;
        $amgAverageSideAverage_raitoData   = 0;

        foreach ($amgTotalRow['current'] as $rowKey => $rowData) {
            $amgAverage_currentData          = round($rowData['data'] / $currentPlantMonthCount[$rowKey], 1);
            $amgTotalSideTotal_currentData   = $amgTotalSideTotal_currentData + $rowData['data'];
            $amgAverageSideTotal_currentData = $amgAverageSideTotal_currentData + $amgAverage_currentData;
        }

        foreach ($amgTotalRow['last'] as $rowKey => $rowData) {
            $amgAverage_lastData          = round($rowData['data'] / $lastPlantMonthCount[$rowKey], 1);
            $amgTotalSideTotal_lastData   = $amgTotalSideTotal_lastData + $rowData['data'];
            $amgAverageSideTotal_lastData = $amgAverageSideTotal_lastData + $amgAverage_lastData;
        }


        // 施設合計表示の場合
        if (!is_null($total)) {
            $amgTotalSideTotal_compData  = $this->getNumberOfEmptyFloors($amgTotalSideTotal_lastData, $amgTotalSideTotal_currentData, 1);
            $amgTotalSideTotal_raitoData = $this->getUtilizationRates($amgTotalSideTotal_compData, $amgTotalSideTotal_lastData, 1);

            $amgAverageSideTotal_compData  = $this->getNumberOfEmptyFloors($amgAverageSideTotal_lastData, $amgAverageSideTotal_currentData, 1);
            $amgAverageSideTotal_raitoData = $this->getUtilizationRates($amgAverageSideTotal_compData, $amgAverageSideTotal_lastData, 1);

            $amgTotalCurrentSideTotalCoordinate =  sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $amgTotalRowNum);
            $amgTotalLastSideTotalCoordinate    =  sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $amgTotalRowNum + 1);
            $amgTotalCompSideTotalCoordinate    =  sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $amgTotalRowNum + 2);

            $amgAverageCurrentSideTotalCoordinate =  sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $amgAverageRowNum);
            $amgAverageLastSideTotalCoordinate    =  sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $amgAverageRowNum + 1);
            $amgAverageCompSideTotalCoordinate    =  sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$totalColumnNum], $amgAverageRowNum + 2);

            // AMG合計
            $amgTotalRow['current'][$totalColumnNum]['data'] = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgTotalRowNum);
            $amgTotalRow['last'][$totalColumnNum]['data']    = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgTotalRowNum + 1);
            $amgTotalRow['comp'][$totalColumnNum]['data']    = $this->getExcelSubData($amgTotalCurrentSideTotalCoordinate, $amgTotalLastSideTotalCoordinate, 1);
            $amgTotalRow['raito'][$totalColumnNum]['data']   = $this->getExcelDivData($amgTotalCompSideTotalCoordinate, $amgTotalLastSideTotalCoordinate, 3);

            // AMG平均
            $amgAverageRow['current'][$totalColumnNum]['data'] = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgAverageRowNum);
            $amgAverageRow['last'][$totalColumnNum]['data']    = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgAverageRowNum + 1);
            $amgAverageRow['comp'][$totalColumnNum]['data']    = $this->getExcelSubData($amgAverageCurrentSideTotalCoordinate, $amgAverageLastSideTotalCoordinate, 1);
            $amgAverageRow['raito'][$totalColumnNum]['data']   = $this->getExcelDivData($amgAverageCompSideTotalCoordinate, $amgAverageLastSideTotalCoordinate, 3);
        }

        // 施設平均表示の場合
        if (!is_null($average)) {
            $amgTotalSideAverage_currentData = round($amgTotalSideTotal_currentData / $this->getDenominatorOfAverageValueOfAverageValue($currentPlantMonthCount), 1);
            $amgTotalSideAverage_lastData    = round($amgTotalSideTotal_lastData / $this->getDenominatorOfAverageValueOfAverageValue($lastPlantMonthCount), 1);
            $amgTotalSideAverage_compData    = $this->getNumberOfEmptyFloors($amgTotalSideAverage_lastData, $amgTotalSideAverage_currentData, 1);
            $amgTotalSideAverage_raitoData   = $this->getUtilizationRates($amgTotalSideAverage_compData, $amgTotalSideAverage_lastData, 1);

            $amgAverageSideAverage_currentData = round($amgAverageSideTotal_currentData / $this->getDenominatorOfAverageValueOfAverageValue($currentPlantMonthCount), 1);
            $amgAverageSideAverage_lastData    = round($amgAverageSideTotal_lastData / $this->getDenominatorOfAverageValueOfAverageValue($lastPlantMonthCount), 1);
            $amgAverageSideAverage_compData    = $this->getNumberOfEmptyFloors($amgAverageSideAverage_lastData, $amgAverageSideAverage_currentData, 1);
            $amgAverageSideAverage_raitoData   = $this->getUtilizationRates($amgAverageSideAverage_compData, $amgAverageSideAverage_lastData, 1);

            $amgTotalCurrentSideAverageCoordinate = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $amgTotalRowNum);
            $amgTotalLastSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $amgTotalRowNum + 1);
            $amgTotalCompSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $amgTotalRowNum + 2);

            $amgAverageCurrentSideAverageCoordinate = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $amgAverageRowNum);
            $amgAverageLastSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $amgAverageRowNum + 1);
            $amgAverageCompSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$startColumnNum+$averageColumnNum], $amgAverageRowNum + 2);

            // AMG合計
            $amgTotalRow['current'][$averageColumnNum]['data'] = $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgTotalRowNum, $this->getDenominatorOfAverageValueOfAverageValue($currentPlantMonthCount), 1);
            $amgTotalRow['last'][$averageColumnNum]['data']    = $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgTotalRowNum + 1, $this->getDenominatorOfAverageValueOfAverageValue($lastPlantMonthCount), 1);
            $amgTotalRow['comp'][$averageColumnNum]['data']    = $this->getExcelSubData($amgTotalCurrentSideAverageCoordinate, $amgTotalLastSideAverageCoordinate, 1);
            $amgTotalRow['raito'][$averageColumnNum]['data']   = $this->getExcelDivData($amgTotalCompSideAverageCoordinate, $amgTotalLastSideAverageCoordinate, 3);

            $amgTotalRow['current'][$averageColumnNum]['style'] = array('msoNumberFormat' => $msoNumberFormat_0_0);
            $amgTotalRow['last'][$averageColumnNum]['style']    = array('msoNumberFormat' => $msoNumberFormat_0_0);
            $amgTotalRow['comp'][$averageColumnNum]['style']    = array('msoNumberFormat' => $msoNumberFormat_0_0);

            // AMG平均
            $amgAverageRow['current'][$averageColumnNum]['data'] = $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgAverageRowNum, $this->getDenominatorOfAverageValueOfAverageValue($currentPlantMonthCount), 1);
            $amgAverageRow['last'][$averageColumnNum]['data']    = $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$startColumnNum + 11], $amgAverageRowNum + 1, $this->getDenominatorOfAverageValueOfAverageValue($lastPlantMonthCount), 1);
            $amgAverageRow['comp'][$averageColumnNum]['data']    = $this->getExcelSubData($amgAverageCurrentSideAverageCoordinate, $amgAverageLastSideAverageCoordinate, 1);
            $amgAverageRow['raito'][$averageColumnNum]['data']   = $this->getExcelDivData($amgAverageCompSideAverageCoordinate, $amgAverageLastSideAverageCoordinate, 3);

            $amgAverageRow['current'][$averageColumnNum]['style'] = array('msoNumberFormat' => $msoNumberFormat_0_0);
            $amgAverageRow['last'][$averageColumnNum]['style']    = array('msoNumberFormat' => $msoNumberFormat_0_0);
            $amgAverageRow['comp'][$averageColumnNum]['style']    = array('msoNumberFormat' => $msoNumberFormat_0_0);
        }

        if ($plantCount!=1) {
            // AMG合計表示の場合
            if (!is_null($amgTotal)) {
                $dateTable .= $amgTotalHeader.$this->convertCellData($amgTotalRow['current'], $fontTag).'</tr>'
                              .$lastRowHeader.$this->convertCellData($amgTotalRow['last'], $fontTag).'</tr>'
                              .$compRowHeader.$this->convertCellData($amgTotalRow['comp'], $fontTag, array($compColorArr)).'</tr>';
                if (!is_null($raito)) {
                    $dateTable .= $raitoRowHeader
                                  .$this->convertCellData($amgTotalRow['raito'], $fontTag, array($compColorArr, 'msoNumberFormat' => $msoNumberFormat_percent)).'</tr>';
                }
            }
            // AMG平均表示の場合
            if (!is_null($amgAverage)) {
                $dateTable .= $amgAverageHeader.$this->convertCellData($amgAverageRow['current'], $fontTag).'</tr>'
                              .$lastRowHeader.$this->convertCellData($amgAverageRow['last'], $fontTag).'</tr>'
                              .$compRowHeader.$this->convertCellData($amgAverageRow['comp'], $fontTag, array('bgColor' => $compColorArr)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) {
                    $dateTable .= $raitoRowHeader.$this->convertCellData($amgAverageRow['raito'], $fontTag, array('bgColor' => $compColorArr, 'msoNumberFormat' => $msoNumberFormat_percent)).'</tr>';
                }
            }
        }

        $dateTable .= '</tbody></table>';
        return $dateTable;
    }

    /**
     * エクセル表示用の getComparisonDisplayInThePreviousYearAverage
     * No.17 全病棟平均入院患者数
     * No.18 一般病棟平均入院患者数
     * No.19 障害者病棟平均入院患者数
     * No.20 精神一般病棟平均入院患者数
     * No.21 医療療養病棟平均入院患者数
     * No.22 介護療養病棟平均入院患者数
     * No.23 回復期リハ病棟平均入院患者数
     * No.24 亜急性期病床平均入院患者数
     * No.25 緩和ケア病棟平均入院患者数
     * No.26 ICU及びハイケアユニット平均入院患者数
     * No.27 小児管理料平均入院患者数
     * No.28 精神療養病棟平均入院患者数
     * No.29 特殊疾患(2)平均入院患者数
     * No.30 認知症病棟平均入院患者数
     * @access protected
     * @param  array $dataArr  表示対象データ
     * @param  array $plantArr 病院リスト
     * @param  array $arr      表示オプション
	 * @param  array $optionArr      表示年度
	 * @return mixed           表示データ(html table)
     */
	function outputExcelComparisonDisplayInThePreviousYearAverage($dataArr, $plantArr, $arr, $optionArr)
    {
        $fontTag = $this->getFontTag();

        // 各表示フラグ
        $total      = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr); // 施設合計
        $average    = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr); // 施設平均
        $raito      = $this->getArrayValueByKeyForEmptyIsNull('raito',       $arr); // 増減率
        $amgTotal   = $this->getArrayValueByKeyForEmptyIsNull('amg_total',   $arr); // AMG合計
        $amgAverage = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr); // AMG平均

        //$lastYear    = $this->getLastYear();
        //$currentYear = $this->getCurrentYear();
		if($optionArr == null)
			{$currentYear = $this->getCurrentYear();}
		else
			{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
		$lastYear		= $currentYear - 1;
		$startMonth  = $this->getFiscalYearStartMonth();

        // 増減率がある場合は 1 施設 4 行になる
        $rowspan = (!is_null($raito))? 4 : 3;

        $msoNumberFormat_percent  = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0      = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma    = $this->getMsoNumberFormat('comma');
//        $msoNumberFormat_percent  = array('name' => 'mso-number-format', 'value' => '0.0%');
//        $msoNumberFormat_0_0      = array('name' => 'mso-number-format', 'value' => '0.0');
        $thColorArr               = array('name' => 'background-color',  'value' => $this->getRgbColor('blue')); // 青
        $compColorArr             = array('name' => 'background-color',  'value' => $this->getRgbColor('green')); // 緑

        $thColor   = $this->getRgbColor('blue',  'background-color');  // 青
        $compColor = $this->getRgbColor('green', 'background-color'); // 緑

        $dateTable = $this->getExcelMeta();
        $dateTable .= '<table border="1" class="list this_table" width="100%"><tbody>';

        // header
        $tableHeader = '<tr><td style="'.$thColor.'" colspan="2" />';
        for ($i=0; $i < 12; $i++) {
            $dispMonth = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
            $tableHeader .= '<td style="text-align: center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $dispMonth. '月'). '</td>';
        }

        $tableHeader .= (!is_null($total))?   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['total']). '</td>'   : '';
        $tableHeader .= (!is_null($average))? '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $arr['average']). '</td>' : '';
        $tableHeader .= '</tr>';

        $dateTable   .= $tableHeader;

        $amgTotalRow    = array();
        $amgAverageRow  = array();
        $targetMonthNum = 0;
        $totalRowNum    = 12;
        $averageRowNum  = is_null($total)? 12 : 13 ;

        $cPlantCnt[] = array();
        $lPlantCnt[] = array();

        # 座標計算対応
        $plantCnt      = count($plantArr);
        $startRowNum   = 2;
        $startColNum   = 3;
        $totalColNum   = $startColNum + 12;
        $averageColNum = is_null($total)? $totalColNum : $totalColNum + 1;
        $colAlphaName  = $this->columnAlphaNameArr;

        $amgTotalCurrentRowNum = $startRowNum + ($plantCnt * $rowspan);
        $amgTotalLastRowNum    = $amgTotalCurrentRowNum + 1;
        $amgTotalCompRowNum    = $amgTotalCurrentRowNum + 2;
        $amgTotalRaitoRowNum   = $amgTotalCurrentRowNum + 3;

        $amgAverageCurrentRowNum = is_null($amgTotal)? $amgTotalRowNum : $startRowNum + ( ($plantCnt+1) * $rowspan);
        $amgAverageLastRowNum    = $amgAverageCurrentRowNum + 1;
        $amgAverageCompRowNum    = $amgAverageCurrentRowNum + 2;
        $amgAverageRaitoRowNum   = $amgAverageCurrentRowNum + 3;

        $amgTotalCurrentRowTotalColCoord = $this->coord($colAlphaName[$totalColNum], $amgTotalCurrentRowNum);
        $amgTotalLastRowTotalColCoord    = $this->coord($colAlphaName[$totalColNum], $amgTotalLastRowNum);
        $amgTotalCompRowTotalColCoord    = $this->coord($colAlphaName[$totalColNum], $amgTotalCompRowNum);

        $amgTotalCurrentRowAverageColCoord = $this->coord($colAlphaName[$averageColNum], $amgTotalCurrentRowNum);
        $amgTotalLastRowAverageColCoord    = $this->coord($colAlphaName[$averageColNum], $amgTotalLastRowNum);
        $amgTotalCompRowAverageColCoord    = $this->coord($colAlphaName[$averageColNum], $amgTotalCompRowNum);

        $amgAverageCurrentRowTotalColCoord = $this->coord($colAlphaName[$totalColNum], $amgAverageCurrentRowNum);
        $amgAverageLastRowTotalColCoord    = $this->coord($colAlphaName[$totalColNum], $amgAverageLastRowNum);
        $amgAverageCompRowTotalColCoord    = $this->coord($colAlphaName[$totalColNum], $amgAverageCompRowNum);

        $amgAverageCurrentRowAverageColCoord = $this->coord($colAlphaName[$averageColNum], $amgAverageCurrentRowNum);
        $amgAverageLastRowAverageColCoord    = $this->coord($colAlphaName[$averageColNum], $amgAverageLastRowNum);
        $amgAverageCompRowAverageColCoord    = $this->coord($colAlphaName[$averageColNum], $amgAverageCompRowNum);

        // data
        if (is_array($dataArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $currentRowNum = $startRowNum + ($plantKey * $rowspan);
                $lastRowNum    = $currentRowNum + 1;
                $compRowNum    = $currentRowNum + 2;
                $raitoRowNum   = $currentRowNum + 3;

                $facilityId  = $plantVal['facility_id'];
                //$lastYear    = $this->getLastYear();
                //$currentYear = $this->getCurrentYear();
				if($optionArr == null)
					{$currentYear = $this->getCurrentYear();}
				else
					{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
				$lastYear		= $currentYear - 1;
				
                $lastRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center;">'.sprintf($fontTag, $lastYear.'年度').'</td>';

                $currentRowHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, $plantVal['facility_name']).'</td>'
                                       .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $compRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';

                $raitoRowHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減率').'</td>';

                $amgTotalHeader = '<tr style="text-align : right;">'
                                     .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG合計').'</td>'
                                     .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $amgAverageHeader = '<tr style="text-align : right;">'
                                       .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="'. $rowspan. '">'.sprintf($fontTag, 'AMG平均').'</td>'
                                       .'<td style="text-align : center;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $lastRow    = array();
                $currentRow = array();
                $compRow    = array();
                $raitoRow   = array();

                $cPlantCnt[$plantKey] = null;
                $lPlantCnt[$plantKey] = null;

                for ($m=0; $m < 12; $m++) {
                    $colNum = $startColNum + $m;

                    //$lastYear    = $this->getLastYear();
                    //$currentYear = $this->getCurrentYear();
					if($optionArr == null)
						{$currentYear = $this->getCurrentYear();}
					else
						{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
					$lastYear		= $currentYear - 1;
					
                    $month       = (($startMonth + $m) <= 12)? $startMonth + $m : $startMonth + $m - 12;
                    $lastYear    = (($startMonth + $m) <= 12)? $lastYear        : $lastYear + 1;
                    $currentYear = (($startMonth + $m) <= 12)? $currentYear     : $currentYear + 1;

                    $currentData = 0;
                    $lastData    = 0;

                    foreach ($dataArr as $mainData) {
                        if ($facilityId == $this->getArrValueByKey('facility_id', $mainData)
                         && $month == $this->getArrValueByKey('month', $mainData)
                         && ( ($currentYear == date('Y') && $month <= date('m')) || $currentYear < date('Y') )
                        ) {
                            $current = $this->getArrValueByKey($currentYear, $mainData);
                            $last    = $this->getArrValueByKey($lastYear, $mainData);
                            if (!is_null($current)) {
                                $currentData = round($current / $mainData[$currentYear.'_count'], 3);
//                                $currentData = round($current / $mainData[$currentYear.'_count'], 1);
                                $cPlantCnt[$plantKey] = $cPlantCnt[$plantKey] + 1;
                                $cMonthCnt[$m] = empty($cMonthCnt[$m])? 1 : ($cMonthCnt[$m] + 1);

                            }
                            if (!is_null($last)) {
								
								/////////////////////////////////////////////////////////////////////////////////////////////////////
								//////	ここから↓は2009年度の日報データが過去データとして1日付けに一括で入力されているので
								//////	平均数値が出ません。なので固定でその月の日数を分母に入れることで暫定的に対処しています。
								//////	2011年度になったら必要なくなるので↓の処理を消してください。そして1行コメント部分をコメントをはずしてください
								/////////////////////////////////////////////////////////////////////////////////////////////////////
								
								
								if(($lastYear = "2009") && (
											($mainData['month'] == "04") ||
											($mainData['month'] == "06") ||
											($mainData['month'] == "09") ||
											($mainData['month'] == "11") 
											))
								{
									$lastData = round($last / 30, 1);
									
								}
								elseif(($lastYear = "2009") && (
											($mainData['month'] == "05") ||
											($mainData['month'] == "07") ||
											($mainData['month'] == "08") ||
											($mainData['month'] == "10") ||
											($mainData['month'] == "12") 
											))
								{
									$lastData = round($last / 31, 1);
									
									
								}
								elseif(($lastYear = "2010") && (
											($mainData['month'] == "01") ||
											($mainData['month'] == "03") 
											))
								{
									
									$lastData = round($last / 31, 1);
									
								}
								elseif(($lastYear = "2010") && ($mainData['month'] == "02"))
								{
									
									$lastData = round($last / 28, 1);
									
								}
								else
								{
									$lastData = round($last / $mainData[$lastYear.'_count'], 1);
								}
								
								//↓のコメントをはずして
								//$lastData = round($last / $mainData[$lastYear.'_count'], 1);
								
								/////////////////////////////////////////////////////////////////////////////////////////////////////
								//////	ここまで↑
								/////////////////////////////////////////////////////////////////////////////////////////////////////
								
								
								
								
								
                                $lPlantCnt[$plantKey] = $lPlantCnt[$plantKey] + 1;
                                $lMonthCnt[$m] = empty($lMonthCnt[$m])? 1 : ($lMonthCnt[$m] + 1);
                            }
                        }
                    }

                    $currentRow[$m]['data'] = $currentData;
                    $lastRow[$m]['data']    = $lastData;
                    $compRow[$m]['data']    = $this->getExcelSubData($this->coord($colAlphaName[$colNum], $currentRowNum), $this->coord($colAlphaName[$colNum], $lastRowNum), 1);
                    $raitoRow[$m]['data']   = $this->getExcelDivData($this->coord($colAlphaName[$colNum], $compRowNum), $this->coord($colAlphaName[$colNum], $lastRowNum), 3);
                    $lastRow[$m]['status']  = $lastStatus;

                    $currentRow[$m]['disp_type'] = 'float';
                    $lastRow[$m]['disp_type']    = 'float';

                    $amgTotalRow['last'][$m]['data']    = round($amgTotalRow['last'][$m]['data'] + $lastRow[$m]['data'], 1);
                    $amgTotalRow['current'][$m]['data'] = round($amgTotalRow['current'][$m]['data'] + $currentRow[$m]['data'], 1);
                    $amgTotalRow['comp'][$m]['data']    = empty($amgTotalRow['comp'][$m]['data'])? 0 : $amgTotalRow['comp'][$m]['data'];
                    $amgTotalRow['raito'][$m]['data']   = $this->getUtilizationRates($amgTotalRow['comp'][$m]['data'], $amgTotalRow['last'][$m]['data'], 1);
                }

                $lastTotal    = 0;
                $currentTotal = 0;
                $compTotal    = 0;
                $raitoToral   = 0;

                foreach ($lastRow as $rowData) { $lastTotal = round($lastTotal + $rowData['data'], 1); }

                foreach ($currentRow as $rowData) { $currentTotal = round($currentTotal + $rowData['data'], 1); }

                $compTotal  = $this->getNumberOfEmptyFloors($lastTotal, $currentTotal, 1);
                $raitoToral = $this->getUtilizationRates($compTotal, $lastTotal, 1);

                // 施設合計表示の場合
                if (!is_null($total)) {
                    $currentRowCoord = $this->coord($colAlphaName[$totalColNum], $currentRowNum);
                    $lastRowCoord    = $this->coord($colAlphaName[$totalColNum], $lastRowNum);
                    $compRowCoord    = $this->coord($colAlphaName[$totalColNum], $compRowNum);

                    $currentRow[$totalRowNum]['data'] = $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum-1], $currentRowNum, 1);
                    $lastRow[$totalRowNum]['data']    = $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum-1], $lastRowNum, 1);
                    $compRow[$totalRowNum]['data']    = $this->getExcelSubData($currentRowCoord, $lastRowCoord, 1);
                    $raitoRow[$totalRowNum]['data']   = $this->getExcelDivData($compRowCoord, $lastRowCoord, 3);
                }

                // 施設平均表示の場合
                if (!is_null($average)) {
                    $currentRowCoord = $this->coord($colAlphaName[$averageColNum], $currentRowNum);
                    $lastRowCoord    = $this->coord($colAlphaName[$averageColNum], $lastRowNum);
                    $compRowCoord    = $this->coord($colAlphaName[$averageColNum], $compRowNum);

                    $currentRow[$averageRowNum]['data'] = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$totalColNum-1], $currentRowNum, $cPlantCnt[$plantKey], 3);
                    $lastRow[$averageRowNum]['data']    = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$totalColNum-1], $lastRowNum,    $lPlantCnt[$plantKey], 3);
                    $compRow[$averageRowNum]['data']    = $this->getExcelSubData($currentRowCoord, $lastRowCoord, 1);
                    $raitoRow[$averageRowNum]['data']   = $this->getExcelDivData($compRowCoord, $lastRowCoord, 3);
                }

                $dateTable .= $currentRowHeader.$this->convertCellData($currentRow, $fontTag, array($msoNumberFormat_0_0)).'</tr>'
                             .$lastRowHeader.$this->convertCellData($lastRow, $fontTag, array($msoNumberFormat_0_0)).'</tr>'
                             .$compRowHeader.$this->convertCellData($compRow, $fontTag, array($compColorArr, $msoNumberFormat_0_0)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) { $dateTable .= $raitoRowHeader.$this->convertCellData($raitoRow, $fontTag, array('bgColor' => $compColorArr, 'msoNumberFormat' => $msoNumberFormat_percent)).'</tr>'; }
            }
        }

        $lastAmgTotal    = 0;
        $currentAmgTotal = 0;
        $compAmgTotal    = 0;
        $raiotAmgTotal   = 0;

        $lastAmgAverage    = 0;
        $currentAmgAverage = 0;
        $compAmgAverage    = 0;
        $raiotAmgAverage   = 0;

//        $lastAmgStatus    = 3;
//        $currentAmgStatus = 3;
//        $compAmgStatus    = 3;
//        $raiotAmgStatus   = 3;

        foreach ($amgTotalRow['last'] as $rowKey => $rowData) {
            $colNum = $startColNum + $rowKey;
            $amgTotalRow['last'][$rowKey]['data'] = $this->getAmgTotalCellData($colAlphaName[$colNum], $plantCnt-1, $rowspan, 1, $startRowNum, 3);
            $amgAverageRow['last'][$rowKey]['data'] = $this->getAmgAverageCellData($colAlphaName[$colNum], $plantCnt-1, $rowspan, 1, $startRowNum, $lMonthCnt[$rowKey], 3);
            $lastAmgAverage = round($lastAmgAverage + $amgAverageRow['last'][$rowKey]['data'], 1);
            $lastAmgTotal   = round($lastAmgTotal + $rowData['data'], 1);
        }

        foreach ($amgTotalRow['current'] as $rowKey => $rowData) {
            $colNum = $startColNum + $rowKey;
            $amgTotalRow['current'][$rowKey]['data'] = $this->getAmgTotalCellData($colAlphaName[$colNum], $plantCnt-1, $rowspan, null, $startRowNum, 3);
            $amgAverageRow['current'][$rowKey]['data'] = $this->getAmgAverageCellData($colAlphaName[$colNum], $plantCnt-1, $rowspan, null, $startRowNum, $cMonthCnt[$rowKey], 3);
            $currentAmgAverage = ($amgAverageRow['current'][$rowKey]['data']!=0)? bcadd($currentAmgAverage, $amgAverageRow['current'][$rowKey]['data'], 1) : $currentAmgAverage;
            $currentAmgTotal   = round($currentAmgTotal + $rowData['data'], 1);
        }

        foreach ($amgTotalRow['comp'] as $rowKey => $rowData) {
            $colNum = $startColNum + $rowKey;
            $amgTotalRow['comp'][$rowKey]['data']   = $this->getExcelSubData($this->coord($colAlphaName[$colNum], $amgTotalCurrentRowNum), $this->coord($colAlphaName[$colNum], $amgTotalLastRowNum), 1);
            $amgAverageRow['comp'][$rowKey]['data'] = $this->getExcelSubData($this->coord($colAlphaName[$colNum], $amgAverageCurrentRowNum), $this->coord($colAlphaName[$colNum], $amgAverageLastRowNum), 1);
        }

        foreach ($amgTotalRow['raito'] as $rowKey => $rowData) {
            $colNum = $startColNum + $rowKey;
            $amgTotalRow['raito'][$rowKey]['data']   = $this->getExcelDivData($this->coord($colAlphaName[$colNum], $amgTotalCompRowNum), $this->coord($colAlphaName[$colNum], $amgTotalLastRowNum), 3);
            $amgAverageRow['raito'][$rowKey]['data'] = $this->getExcelDivData($this->coord($colAlphaName[$colNum], $amgAverageCompRowNum), $this->coord($colAlphaName[$colNum], $amgAverageLastRowNum), 3);
        }

        // 施設合計表示の場合
        if (!is_null($total)) {
            // AMG合計
            $amgTotalRow['current'][$totalRowNum]['data'] = $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgTotalCurrentRowNum, 3);
            $amgTotalRow['last'][$totalRowNum]['data']    = $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgTotalLastRowNum, 3);
            $amgTotalRow['comp'][$totalRowNum]['data']    = $this->getExcelSubData($amgTotalCurrentRowTotalColCoord, $amgTotalLastRowTotalColCoord, 1);
            $amgTotalRow['raito'][$totalRowNum]['data']   = $this->getExcelDivData($amgTotalCompRowTotalColCoord, $amgTotalLastRowTotalColCoord, 3);

            // AMG平均
            $amgAverageRow['current'][$totalRowNum]['data'] = $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgAverageCurrentRowNum, 3);
            $amgAverageRow['last'][$totalRowNum]['data']    = $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgAverageLastRowNum, 3);
            $amgAverageRow['comp'][$totalRowNum]['data']    = $this->getExcelSubData($amgAverageCurrentRowTotalColCoord, $amgAverageLastRowTotalColCoord, 1);
            $amgAverageRow['raito'][$totalRowNum]['data']   = $this->getExcelDivData($amgAverageCompRowTotalColCoord, $amgAverageLastRowTotalColCoord, 3);
        }

        // 施設平均表示の場合
        if (!is_null($average)) {
            // AMG合計
            $amgTotalRow['current'][$averageRowNum]['data'] = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgTotalCurrentRowNum, $this->getDenominatorOfAverageValueOfAverageValue($cMonthCnt), 1);
            $amgTotalRow['last'][$averageRowNum]['data']    = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgTotalLastRowNum,    $this->getDenominatorOfAverageValueOfAverageValue($lMonthCnt), 1);
            $amgTotalRow['comp'][$averageRowNum]['data']    = $this->getExcelSubData($amgTotalCurrentRowAverageColCoord, $amgTotalLastRowAverageColCoord, 1);
            $amgTotalRow['raito'][$averageRowNum]['data']   = $this->getExcelDivData($amgTotalCompRowAverageColCoord, $amgTotalLastRowAverageColCoord, 3);

            // AMG平均
            $amgAverageRow['current'][$averageRowNum]['data'] = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgAverageCurrentRowNum, $this->getDenominatorOfAverageValueOfAverageValue($cMonthCnt), 1);
            $amgAverageRow['last'][$averageRowNum]['data']    = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$totalColNum - 1], $amgAverageLastRowNum,    $this->getDenominatorOfAverageValueOfAverageValue($lMonthCnt), 1);
            $amgAverageRow['comp'][$averageRowNum]['data']    = $this->getExcelSubData($amgAverageCurrentRowAverageColCoord, $amgAverageLastRowAverageColCoord, 1);
            $amgAverageRow['raito'][$averageRowNum]['data']   = $this->getExcelDivData($amgAverageCompRowAverageColCoord, $amgAverageLastRowAverageColCoord, 3);
        }

        if (count($plantArr)!=1) {
            // AMG合計表示の場合
            if (!is_null($amgTotal)) {
                $dateTable .= $amgTotalHeader.$this->convertCellData($amgTotalRow['current'], $fontTag, array($msoNumberFormat_0_0)).'</tr>'
                              .$lastRowHeader.$this->convertCellData($amgTotalRow['last'], $fontTag, array($msoNumberFormat_0_0)).'</tr>'
                              .$compRowHeader.$this->convertCellData($amgTotalRow['comp'], $fontTag, array($compColorArr, $msoNumberFormat_0_0)).'</tr>';
                if (!is_null($raito)) {
                    $dateTable .= $raitoRowHeader
                                 .$this->convertCellData($amgTotalRow['raito'], $fontTag, array($compColorArr, $msoNumberFormat_percent)).'</tr>';
                }
            }
            // AMG平均表示の場合
            if (!is_null($amgAverage)) {

                $dateTable .= $amgAverageHeader.$this->convertCellData($amgAverageRow['current'], $fontTag, array($msoNumberFormat_0_0)).'</tr>'
                                 .$lastRowHeader.$this->convertCellData($amgAverageRow['last'], $fontTag, array($msoNumberFormat_0_0)).'</tr>'
                                 .$compRowHeader.$this->convertCellData($amgAverageRow['comp'], $fontTag, array($compColorArr, $msoNumberFormat_0_0)).'</tr>';

                // 増減率表示の場合
                if (!is_null($raito)) { $dateTable .= $raitoRowHeader.$this->convertCellData($amgAverageRow['raito'], $fontTag, array($compColorArr, $msoNumberFormat_percent)).'</tr>'; }
            }
        }

        $dateTable .= '</tbody></table>';
        return $dateTable;
    }

    /**
     * エクセル表示用の getUtilizationRatesDisplayData(各稼働率)
     * No.31 全病棟稼働率(稼働病床)
     * No.32 全病棟稼働率(稼働病床)
     * No.33 一般病棟稼働率
     * No.34 障害者病棟稼働率
     * No.35 精神一般病棟稼働率
     * No.36 医療療養病棟稼働率
     * No.37 介護療養病棟稼働率
     * No.38 回復期リハ病棟稼働率
     * No.39 亜急性期病床稼働率
     * No.40 緩和ケア病棟稼働率
     * No.41 ICU及びハイケアユニット稼働率
     * No.42 小児管理料稼働率
     * No.43 精神療養病棟稼働率
     * No.44 特殊疾患(2)稼働率
     * No.45 認知症病棟稼働率
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
	 * @param  array $optionArr      表示年度
	 * @return string
     */
	function outputExcelUtilizationRatesDisplayData($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr      = $this->getExcelMeta();
        $fontTag     = $this->getFontTag();
        $total       = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr); // 施設合計
        $average     = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr); // 施設平均
        $amgAverage  = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr); // AMG平均
        
		//$lastYear    = $this->getLastYear();
        //$currentYear = $this->getCurrentYear();
		//$startMonth  = $this->getFiscalYearStartMonth();
		if($optionArr == null)
			{$currentYear = $this->getCurrentYear();}
		else
			{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
		$lastYear		= $currentYear - 1;
        $startMonth  = $this->getFiscalYearStartMonth();

        # attr
        $colspan_2        = array('name' => 'colspan',           'value' => 2);
        $rowspan_3        = array('name' => 'rowspan',           'value' => 3);
        # style
        $bgColor_blue     = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_green    = array('name' => 'background-color',  'value' => $this->getRgbColor('green'));
        $textAlign_center = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right  = array('name' => 'text-align',        'value' => 'right');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
//        $msoNumberFormat  = array('name' => 'mso-number-format', 'value' => '0.0%');

        $topHeaderArr = array();
        $mainDataArr  = array();
        $leftSideArr  = array();
        $cCoordArr    = array();
        $lCoordArr    = array();

        $topHeaderArr[] = array('data' => '&nbsp;', 'attr' => array($colspan_2));

        for ($m=0; $m<12; $m++) {
            $month          = (($startMonth+$m) > 12)? ($startMonth + $m) - 12 : ($startMonth + $m);
            $monthName      = sprintf('%d月', $month);
            $topHeaderArr[] = array('data' => $monthName);
            $cCoordArr[$m]  = array();
        }

        $cDataCount = array();
        $lDataCount = array();

        $startRowNum       = 2;
        $startColNum       = 3;
        $cAmgAvrgRowNum    = $startRowNum + ( count($plantArr) * 3 );
        $lAmgAvrgRowNum    = $cAmgAvrgRowNum + 1;
        $compAmgAvrgRowNum = $cAmgAvrgRowNum + 2;
        $avrgColNum        = $startColNum + 12;
        $colAlphaName      = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {
            $cRowNum    = $startRowNum + ( $plantKey * 3 );
            $lRowNum    = $startRowNum + ( $plantKey * 3 ) + 1;
            $compRowNum = $startRowNum + ( $plantKey * 3 ) + 2;

            $facilityId = $plantVal['facility_id'];
            $leftSideArr[$plantKey]['current'] = array(
                0 =>array('data' => $plantVal['facility_name'], 'attr' => array($rowspan_3), 'style' => array('bgColor' => $bgColor_blue)),
                1 =>array('data' => $currentYear.'年度')
            );
            $leftSideArr['amg_average']['current'] = array(
                0 =>array('data' => 'AMG平均', 'attr' => array($rowspan_3), 'style' => array('bgColor' => $bgColor_blue)),
                1 =>array('data' => $currentYear.'年度')
            );
            $leftSideArr[$plantKey]['last'][] = array('data' => $lastYear.'年度');
            $leftSideArr[$plantKey]['comp'][] = array('data' => '増減');

            $cSideAverage = array();
            $lSideAverage = array();

            for ($m=0; $m<12; $m++) {
                $colNum = $startColNum + $m;
                $month = (($startMonth + $m) > 12)? ($startMonth + $m) - 12 : ($startMonth + $m);
                $cYear = (($startMonth + $m) > 12)? $currentYear + 1 : $currentYear;
                $lYear = (($startMonth + $m) > 12)? $lastYear + 1 : $lastYear;

                $mainDataArr[$plantKey]['current'][$m]['data'] = 0;
                $mainDataArr[$plantKey]['last'][$m]['data']    = 0;
                $mainDataArr[$plantKey]['comp'][$m]['data']    = 0;

                foreach ($dataArr as $dataVal) {
                    if ($facilityId == $dataVal['facility_id'] && $month == $dataVal['month']) {
                        if ($this->getArrValueByKey($cYear.'_gokei', $dataVal)) {
                            $mainDataArr[$plantKey]['current'][$m]['data'] = $this->getExcelDivData($dataVal[$cYear.'_gokei'], $dataVal[$cYear.'_act'], 3);
                            $cSideAverage['data'][] = $mainDataArr[$plantKey]['current'][$m]['data'];
                            $cDataCount[$m] = $cDataCount[$m] + 1;
                        }
                        if ($this->getArrValueByKey($lYear.'_gokei', $dataVal)) {
                            $mainDataArr[$plantKey]['last'][$m]['data'] = $this->getExcelDivData($dataVal[$lYear.'_gokei'], $dataVal[$lYear.'_act'], 3);
                            $lSideAverage['data'][] = $mainDataArr[$plantKey]['last'][$m]['data'];
                            $lDataCount[$m] = $lDataCount[$m] + 1;
                        }
                        $mainDataArr[$plantKey]['comp'][$m]['data'] = $this->getExcelSubData($this->coord($colAlphaName[$colNum], $cRowNum), $this->coord($colAlphaName[$colNum], $lRowNum), 3);
                    }
                }

                $cCoordArr[$m][] = $this->coord($colAlphaName[$colNum], $cRowNum);
                $lCoordArr[$m][] = $this->coord($colAlphaName[$colNum], $lRowNum);
            }

            $mainDataArr[$plantKey]['current'][12]['data'] = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$avrgColNum-1], $cRowNum, count($cSideAverage['data']), 3);
            $mainDataArr[$plantKey]['last'][12]['data']    = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$avrgColNum-1], $lRowNum, count($lSideAverage['data']), 3);
            $mainDataArr[$plantKey]['comp'][12]['data']    = $this->getExcelSubData($this->coord($colAlphaName[$avrgColNum], $cRowNum), $this->coord($colAlphaName[$avrgColNum], $lRowNum), 3);
        }

        if (!is_null($total))   { $topHeaderArr[] = array('data' => $total); }
        if (!is_null($average)) { $topHeaderArr[] = array('data' => $average); }

        $rtnStr .= '<table class="list this_table" width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData($topHeaderArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center))
                      .'</tr>';

        for ($r=0; $r < count($plantArr); $r++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSideArr[$r]['current'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($mainDataArr[$r]['current'], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[$r]['last'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($mainDataArr[$r]['last'], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[$r]['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center))
                          .$this->convertCellData($mainDataArr[$r]['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                      .'</tr>';
        }


        if (!is_null($amgAverage)) {
            $amgAvrgArr = array();
            foreach ($mainDataArr as $mainDataVal) {
                for ($m=0; $m<12; $m++) {
                    $amgAvrgArr['current'][$m]['data'] = $amgAvrgArr['current'][$m]['data'] + $mainDataVal['current'][$m]['data'];
                    $amgAvrgArr['last'][$m]['data']    = $amgAvrgArr['last'][$m]['data']    + $mainDataVal['last'][$m]['data'];
                }
            }

            $cAvrgTotal     = array();
            $cAvrgDataCount = array();
            $lAvrgTotal     = array();
            $lAvrgDataCount = array();

            for ($m=0; $m<12; $m++) {
                $colNum = $startColNum + $m;
                $amgAvrgArr['current'][$m]['data'] = $this->getExcelAverageDataByArray($cCoordArr[$m], $cDataCount[$m], 3);
                $amgAvrgArr['last'][$m]['data']    = $this->getExcelAverageDataByArray($lCoordArr[$m], $lDataCount[$m], 3);
                $amgAvrgArr['comp'][$m]['data']    = $this->getExcelSubData($this->coord($colAlphaName[$colNum], $cAmgAvrgRowNum), $this->coord($colAlphaName[$colNum], $lAmgAvrgRowNum), 3);
                $cAvrgTotal[$m] = $amgAvrgArr['current'][$m]['data'];
                $lAvrgTotal[$m] = $amgAvrgArr['last'][$m]['data'];
                if ($cDataCount[$m] > 0) { $cAvrgDataCount[$m] = $cAvrgDataCount[$m] + 1; }
                if ($lDataCount[$m] > 0) { $lAvrgDataCount[$m] = $lAvrgDataCount[$m] + 1; }
            }

            $amgAvrgArr['current'][12]['data'] = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$avrgColNum-1], $cAmgAvrgRowNum, count($cAvrgDataCount), 3);
            $amgAvrgArr['last'][12]['data']    = $this->getExcelSideAverageData($colAlphaName[$startColNum], $colAlphaName[$avrgColNum-1], $lAmgAvrgRowNum, count($lAvrgDataCount), 3);
            $amgAvrgArr['comp'][12]['data']    = $this->getExcelSubData($this->coord($colAlphaName[$avrgColNum], $cAmgAvrgRowNum), $this->coord($colAlphaName[$avrgColNum], $lAmgAvrgRowNum), 3);

            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSideArr['amg_average']['current'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($amgAvrgArr['current'], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[0]['last'], $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($amgAvrgArr['last'], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($leftSideArr[0]['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center))
                          .$this->convertCellData($amgAvrgArr['comp'], $fontTag, array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.63 病院別患者日報(シンプル版)のエクセル出力処理
     * @access protected
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function outputExcelDailySimpleReportAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr       = '';
        $totalArr     = array();
        $totalDataArr = array();
        $totalStatus  = 3;

        $year  = $this->getArrayValueByKeyForEmptyIsNull('year',  $dateArr);
        $month = $this->getArrayValueByKeyForEmptyIsNull('month', $dateArr);
        $day   = $this->getArrayValueByKeyForEmptyIsNull('day',   $dateArr);

        # timestamp
        $date  = mktime(0, 0, 0, $month, $day, $year);

        $font = $this->getFont();
        $font['color'] = '';
        $fontTag = $this->getFontTag($font);
        $bgColorBlue = $this->getRgbColor('blue', 'background-color');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');
        $textAlign_center = array('name' => 'text-align', 'value' => 'center');
        $textAlign_right  = array('name' => 'text-align', 'value' => 'right');

        $rtnStr .= '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';
        $header = '<table border="1" border="1" width="100%"><tr><td style="text-align: center;" >'
                  .sprintf('%04d年%02d月%02d日(%s)', $year, $month, $day, $this->getDayOfTheWeekForDate($year, $month, $day))
                  .'</td></tr></table>'
                  .'<table border="1" class="list this_table" width="100%">'
                     .'<tr>'
                         .sprintf('<td rowspan="2" style="text-align: center; %s" >', $bgColorBlue).sprintf($fontTag, '病院名').'</td>'
                         .'<td rowspan="2" style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '外来<br />患者数').'</td>'
                         .'<td colspan="6" style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '入院合計').'</td>'
                         .'<td colspan="2" style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '入退院数').'</td>'
                     .'</tr>'
                     .'<tr>'
                         .'<td style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td colspan="2" style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働病床<br />稼働率').'</td>'
                         .'<td colspan="2" style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '許可病床<br />稼働率').'</td>'
                         .'<td style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '入院').'</td>'
                        . '<td style="text-align: center; '.$bgColorBlue.'" >'.sprintf($fontTag, '退院').'</td>'
                     .'</tr>';

        $rtnStr       .= $header;
        $plantCount    = count($plantArr);
        $defaultRowNum = 5;
        $excelRowNum   = $defaultRowNum;

        foreach ($plantArr as $plantVal) {
            $tdArr = array();
            $tdArr[] = sprintf('<td style="text-align: center; %s" >%s</td>', $bgColorBlue, sprintf($fontTag, $this->getArrValueByKey('facility_name', $plantVal)));

            $movement_sickbed_utilization_rates_trigona   = $this->getExcelUtilizationRatesTrigona('F', $excelRowNum, '90');
            $permission_sickbed_utilization_rates_trigona = $this->getExcelUtilizationRatesTrigona('H', $excelRowNum, '90');

            foreach ($dataArr as $dataVal) {
                if ($this->getArrValueByKey('facility_id', $plantVal) == $this->getArrValueByKey('facility_id', $dataVal)) {
                    $status        = 3;
                    $totalStatus   = ($totalStatus > $status)? $status : $totalStatus;
                    $gokeiGa       = $this->getArrValueByKey('gokei_ga',  $dataVal);
                    $patientCount  = is_null($this->getArrayValueByKeyForEmptyIsNull('patient_count', $dataVal))? 0 : $this->getArrayValueByKeyForEmptyIsNull('patient_count', $dataVal);
                    $actCount      = $this->getArrValueByKey('act_count', $dataVal);
                    $pmtCount      = $this->getArrValueByKey('pmt_count', $dataVal);
                    $nyuin         = $this->getArrValueByKey('nyuin',     $dataVal);
                    $taiin         = $this->getArrValueByKey('taiin',     $dataVal);
                    $emptyBedCount = $this->getExcelSubData($actCount, $patientCount, 3);

                    $movement_sickbed_utilization_rates   = $this->getExcelDivData($patientCount, $actCount, 3);
                    $permission_sickbed_utilization_rates = $this->getExcelDivData($patientCount, $pmtCount, 3);

                    $tdArr[] = $this->convertCellData(
                        array(
                            # 外来患者数
                            0 => array('status' => $status, 'data' => $gokeiGa),

                            # 入院合計：患者数, 空床数, 稼働病床稼働率(▲), 稼働病床稼働率, 許可病床稼働率(▲), 許可病床稼働率
                            1 => array('status' => $status, 'data' => $patientCount),
                            2 => array('status' => $status, 'data' => $emptyBedCount),
                            3 => array('status' => $status, 'data' => $movement_sickbed_utilization_rates_trigona),
                            4 => array('status' => $status, 'data' => $movement_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                            5 => array('status' => $status, 'data' => $permission_sickbed_utilization_rates_trigona),
                            6 => array('status' => $status, 'data' => $permission_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入退院数：入院, 退院
                            7 => array('status' => $status, 'data' => $nyuin),
                            8 => array('status' => $status, 'data' => $taiin),
                        ),
                        $fontTag,
                        array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma)
                    );
                    $totalDataArr['gokei_ga']        = bcadd($this->getArrValueByKey('gokei_ga',        $totalDataArr), $gokeiGa);
                    $totalDataArr['patient_count']   = bcadd($this->getArrValueByKey('patient_count',   $totalDataArr), $patientCount);
                    $totalDataArr['empty_bed_count'] = bcadd($this->getArrValueByKey('empty_bed_count', $totalDataArr), $emptyBedCount);
                    $totalDataArr['pmt_count']       = bcadd($this->getArrValueByKey('pmt_count',       $totalDataArr), $pmtCount);
                    $totalDataArr['act_count']       = bcadd($this->getArrValueByKey('act_count',       $totalDataArr), $actCount);
                    $totalDataArr['nyuin']           = bcadd($this->getArrValueByKey('nyuin',           $totalDataArr), $nyuin);
                    $totalDataArr['taiin']           = bcadd($this->getArrValueByKey('taiin',           $totalDataArr), $taiin);
                }
            }

            // データがない場合
            if (count($tdArr) != '2') {
                $tdArr[] = $this->convertCellData(
                    array(
                        # 外来患者数,
                        0 => array('status' => $status, 'data' => ''),

                        # 入院合計：患者数, 空床数, 稼働病床稼働率(▲), 稼働病床稼働率, 許可病床稼働率(▲), 許可病床稼働率
                        1 => array('status' => $status, 'data' => ''),
                        2 => array('status' => $status, 'data' => ''),
                        3 => array('status' => $status, 'data' => $movement_sickbed_utilization_rates_trigona),
                        4 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                        5 => array('status' => $status, 'data' => $permission_sickbed_utilization_rates_trigona),
                        6 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入退院数：入院, 退院
                        7 => array('status' => $status, 'data' => ''),
                        8 => array('status' => $status, 'data' => ''),
                    ),
                    $fontTag,
                    array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma)
                );
            }
            $rtnStr .= '<tr>'. implode('', $tdArr).'</tr>';
            $excelRowNum++;
        }

        // 合計
        $totalArr[] = $this->convertCellData(
            array(0 => array('status' => 3, 'data' => '合計')),
            $fontTag,
            array(
                'textAlign' => $textAlign_center
              , 'fontSize'  => array('name' => 'font-size',        'value' => '120%')
              , 'bgColor'   => array('name' => 'background-color', 'value' => $this->getRgbColor('blue'))
              , 'msoNumberFormat' => $msoNumberFormat_comma
            )
        );

        $totalPatientCount = $this->getArrValueByKey('patient_count',  $totalDataArr);
        $totalPmtCount     = $this->getArrayValueByKeyForEmptyIsNull('pmt_count', $totalDataArr);
        $totalActCount     = $this->getArrayValueByKeyForEmptyIsNull('act_count', $totalDataArr);

        $total_movement_sickbed_utilization_rates   = is_null($totalActCount)? '' : $this->getExcelDivData($totalPatientCount, $totalActCount, 3);
        $total_permission_sickbed_utilization_rates = is_null($totalPmtCount)? '' : $this->getExcelDivData($totalPatientCount, $totalPmtCount, 3);

        $totalArr[] = $this->convertCellData(
            array(
                # 外来患者数
                0 => array('status' => $totalStatus, 'data' => $this->getColmunTotalData('B', $defaultRowNum, $defaultRowNum+$plantCount)),

                # 入院合計：患者数, 空床数, 稼働病床稼働率(▲), 稼働病床稼働率, 許可病床稼働率(▲), 許可病床稼働率
                1 => array('status' => $totalStatus, 'data' => $this->getColmunTotalData('C', $defaultRowNum, $defaultRowNum+$plantCount)),
                2 => array('status' => $totalStatus, 'data' => $this->getColmunTotalData('D', $defaultRowNum, $defaultRowNum+$plantCount)),
                3 => array('status' => $totalStatus, 'data' => $this->getExcelUtilizationRatesTrigona('F', $defaultRowNum+$plantCount, '90')),
                4 => array('status' => $totalStatus, 'data' => $total_movement_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                5 => array('status' => $totalStatus, 'data' => $this->getExcelUtilizationRatesTrigona('H', $defaultRowNum+$plantCount, '90')),
                6 => array('status' => $totalStatus, 'data' => $total_permission_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                # 入退院数：入院, 退院
                7 => array('status' => $totalStatus, 'data' => $this->getColmunTotalData('I', $defaultRowNum, $defaultRowNum+$plantCount)),
                8 => array('status' => $totalStatus, 'data' => $this->getColmunTotalData('J', $defaultRowNum, $defaultRowNum+$plantCount)),
            ),
            $fontTag,
            array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma)
        );

        $rtnStr .= '<tr>'.implode('', $totalArr).'</tr></table>';
        return $rtnStr;
    }

    /**
     * No.62 病院別患者日報のエクセル出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function outputExcelDailyReportAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr       = $this->getExcelMeta();
        $totalArr     = array();
        $totalDataArr = array();
        $totalStatus  = 3;

        $year  = $this->getArrayValueByKeyForEmptyIsNull('year',  $dateArr);
        $month = $this->getArrayValueByKeyForEmptyIsNull('month', $dateArr);
        $day   = $this->getArrayValueByKeyForEmptyIsNull('day',   $dateArr);
        $date  = mktime(0, 0, 0, $month, $day, $year);

        $font = $this->getFont();
        $font['color'] = '';
        $fontTag = $this->getFontTag($font);
        $bgColorBlue = $this->getRgbColor('blue', 'background-color');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_decimal = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');
        $textAlign_center = array('name' => 'text-align', 'value' => 'center');
        $textAlign_right  = array('name' => 'text-align', 'value' => 'right');

        $header = '<table border="1" border="1" width="100%"><tr><td style="text-align: center;" >'
                  .sprintf('%04d年%02d月%02d日(%s)', $year, $month, $day, $this->getDayOfTheWeekForDate($year, $month, $day))
                  .'</td></tr></table>'
                  .'<table border="1" class="list this_table" width="100%">'
                     .'<tr>'
                         .'<td rowspan="3" style="text-align: center; white-space: nowrap;'.$bgColorBlue.'" >'.sprintf($fontTag, '病院名').'</td>'
                         .'<td rowspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '外来<br />患者数').'</td>'
                         .'<td colspan="9" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '入院(一般)').'</td>'
                         .'<td colspan="6" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '入院(療養)').'</td>'
                         .'<td colspan="24" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '入院(特定)').'</td>'
                         .'<td rowspan="2" colspan="6" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '入院合計').'</td>'
                         .'<td rowspan="2" colspan="2" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '入退院数').'</td>'
                     .'</tr>'
                     .'<tr>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '一般病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '障害者病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '精神一般病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '医療療養病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '介護療養病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '回復期ﾘﾊ病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '亜急性期病床').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '緩和ｹｱ病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, 'ICU及びハイケアユニット').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '小児管理料').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '精神療養病棟').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '特殊疾患(2)').'</td>'
                         .'<td colspan="3" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '認知症病棟').'</td>'
                     .'</tr>'
                     .'<tr>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '患者数').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '空床数').'</td>'
                         .'<td colspan="2" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '稼働病床<br />稼働率').'</td>'
                         .'<td colspan="2" style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '許可病床<br />稼働率').'</td>'
                         .'<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '入院').'</td>'
                        . '<td style="text-align: center; white-space: nowrap; '.$bgColorBlue.'" >'.sprintf($fontTag, '退院').'</td>'
                     .'</tr>';

        $rtnStr .= $header;

        $categoryArr   = array(0 => 'ip', 1 => 'shog', 2 => 'seip', 3 => 'ir', 4 => 'kaig', 5 => 'kaif', 6 => 'ak', 7 => 'kan', 8 => 'icu', 9 => 'shoni', 10 => 'seir', 11 => 'tok', 12 => 'nin');
        $defaultRowNum = 6;
        $rowNum        = $defaultRowNum;
        $plantCount    = count($plantArr);

        foreach ($plantArr as $plantVal) {
            $tdArr = array();
            $tdArr[] = sprintf('<td style="text-align: center; white-space: nowrap; %s" >%s</td>', $bgColorBlue, sprintf($fontTag, $this->getArrValueByKey('facility_name', $plantVal)));

            foreach ($dataArr as $dataVal) {
                if ($this->getArrValueByKey('facility_id', $plantVal) == $this->getArrValueByKey('facility_id', $dataVal)) {
                    $status       = 3;
                    $totalStatus  = ($totalStatus > $status)? $status : $totalStatus;
                    $gokeiGa      = $this->getArrValueByKey('gokei_ga',      $dataVal);
                    $patientCount = $this->getArrValueByKey('patient_count', $dataVal);
                    $actCount     = $this->getArrValueByKey('act_count',     $dataVal);
                    $pmtCount     = $this->getArrValueByKey('pmt_count',     $dataVal);
                    $nyuin        = $this->getArrValueByKey('nyuin',         $dataVal);
                    $taiin        = $this->getArrValueByKey('taiin',         $dataVal);
                    $paramArr     = array();

                    foreach ($categoryArr as $val) {
                        $paramArr['gokei_'.$val]     = $this->getArrayValueByKeyForEmptyIsNull('gokei_'.$val, $dataVal);
                        $paramArr['gokei_'.$val]     = is_null($paramArr['gokei_'.$val])? '0' : $paramArr['gokei_'.$val];
                        $paramArr[$val.'_act']       = $this->getArrayValueByKeyForEmptyIsNull($val.'_act', $dataVal);
                        $paramArr[$val.'_act']       = is_null($paramArr[$val.'_act'])?   '0' : $paramArr[$val.'_act'];
                        $paramArr[$val.'_rates']     = $this->getExcelDivData($paramArr['gokei_'.$val], $paramArr[$val.'_act']);
                        $totalDataArr['gokei_'.$val] = bcadd($this->getArrValueByKey('gokei_'.$val, $totalDataArr), $paramArr['gokei_'.$val]);
                        $totalDataArr[$val.'_act']   = bcadd($this->getArrValueByKey($val.'_act',   $totalDataArr), $paramArr[$val.'_act']);
                    }

                    $emptyBedCount                        = $this->getExcelSubData($actCount, $patientCount, 3);
                    $movement_sickbed_utilization_rates   = $this->getExcelDivData($patientCount, $actCount, 3);
                    $permission_sickbed_utilization_rates = $this->getExcelDivData($patientCount, $pmtCount, 3);

                    $tdArr[] = $this->convertCellData(
                        array(
                             # 外来患者数
                             0 => array('status' => $status, 'data' => $gokeiGa),

                             # 入院(一般) 一般病棟 患者数, 空床数, 稼働率
                             1 => array('status' => $status, 'data' => $paramArr['gokei_ip']),
                             2 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['ip_act'], $paramArr['gokei_ip'], 3)),
                             3 => array('status' => $status, 'data' => $paramArr['ip_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                             # 入院(一般) 障害者病棟 患者数, 空床数, 患者数
                             4 => array('status' => $status, 'data' => $paramArr['gokei_shog']),
                             5 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['shog_act'], $paramArr['gokei_shog'], 3)),
                             6 => array('status' => $status, 'data' => $paramArr['shog_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                             # 入院(一般) 精神一般病棟 患者数, 空床数, 稼働率
                             7 => array('status' => $status, 'data' => $paramArr['gokei_seip']),
                             8 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['seip_act'], $paramArr['gokei_seip'], 3)),
                             9 => array('status' => $status, 'data' => $paramArr['seip_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(療養) 医療療養病棟 患者数, 空床数, 稼働率
                            10 => array('status' => $status, 'data' => $paramArr['gokei_ir']),
                            11 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['ir_act'], $paramArr['gokei_ir'], 3)),
                            12 => array('status' => $status, 'data' => $paramArr['ir_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(療養) 介護療養病棟 患者数, 空床数, 稼働率
                            13 => array('status' => $status, 'data' => $paramArr['gokei_kaig']),
                            14 => array('status' => $status, 'data' =>  $this->getExcelSubData($paramArr['kaig_act'], $paramArr['gokei_kaig'], 3)),
                            15 => array('status' => $status, 'data' => $paramArr['kaig_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) 回復期ﾘﾊ病棟  患者数, 空床数, 稼働率
                            16 => array('status' => $status, 'data' => $paramArr['gokei_kaif']),
                            17 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['kaif_act'], $paramArr['gokei_kaif'], 3)),
                            18 => array('status' => $status, 'data' => $paramArr['kaif_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) 亜急性期病床 患者数, 空床数, 稼働率
                            19 => array('status' => $status, 'data' => $paramArr['gokei_ak']),
                            20 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['ak_act'], $paramArr['gokei_ak'], 3)),
                            21 => array('status' => $status, 'data' => $paramArr['ak_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) 緩和ｹｱ病棟 患者数, 空床数, 稼働率
                            22 => array('status' => $status, 'data' => $paramArr['gokei_kan']),
                            23 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['kan_act'], $paramArr['gokei_kan'], 3)),
                            24 => array('status' => $status, 'data' => $paramArr['kan_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) ICU及びハイケアユニット 患者数, 空床数, 稼働率
                            25 => array('status' => $status, 'data' => $paramArr['gokei_icu']),
                            26 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['icu_act'], $paramArr['gokei_icu'], 3)),
                            27 => array('status' => $status, 'data' => $paramArr['icu_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) 小児管理料 患者数, 空床数, 稼働率
                            28 => array('status' => $status, 'data' => $paramArr['gokei_shoni']),
                            29 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['shoni_act'], $paramArr['gokei_shoni'], 3)),
                            30 => array('status' => $status, 'data' => $paramArr['shoni_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) 精神療養病棟 患者数, 空床数, 稼働率
                            31 => array('status' => $status, 'data' => $paramArr['gokei_seir']),
                            32 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['seir_act'], $paramArr['gokei_seir'], 3)),
                            33 => array('status' => $status, 'data' => $paramArr['seir_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) 特殊疾患(2) 患者数, 空床数, 稼働率
                            34 => array('status' => $status, 'data' => $paramArr['gokei_tok']),
                            35 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['tok_act'], $paramArr['gokei_tok'], 3)),
                            36 => array('status' => $status, 'data' => $paramArr['tok_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院(特定) 認知症病棟 患者数, 空床数, 稼働率
                            37 => array('status' => $status, 'data' => $paramArr['gokei_nin']),
                            38 => array('status' => $status, 'data' => $this->getExcelSubData($paramArr['nin_act'], $paramArr['gokei_nin'], 3)),
                            39 => array('status' => $status, 'data' => $paramArr['nin_rates'], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                            # 入院合計 患者数, 空床数, 稼働病床稼働率(▲), 稼働病床稼働率, 許可病床稼働率(▲), 許可病床稼働率, 入院, 退院
                            40 => array('status' => $status, 'data' => $patientCount),
                            41 => array('status' => $status, 'data' => $emptyBedCount),
                            42 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AS', $rowNum, '90')),
                            43 => array('status' => $status, 'data' => $movement_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                            44 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AU', $rowNum, '90')),
                            45 => array('status' => $status, 'data' => $permission_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                            46 => array('status' => $status, 'data' => $nyuin),
                            47 => array('status' => $status, 'data' => $taiin),
                        ),
                        $fontTag,
                        array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma)
                    );

                    $totalDataArr['gokei_ga']  = bcadd($this->getArrValueByKey('gokei_ga',  $totalDataArr), $gokeiGa);
                    $totalDataArr['act_count'] = bcadd($this->getArrValueByKey('act_count', $totalDataArr), $actCount);
                    $totalDataArr['pmt_count'] = bcadd($this->getArrValueByKey('pmt_count', $totalDataArr), $pmtCount);

                    $totalDataArr['patient_count']   = bcadd($this->getArrValueByKey('patient_count',   $totalDataArr), $patientCount);
                    $totalDataArr['empty_bed_count'] = bcadd($this->getArrValueByKey('empty_bed_count', $totalDataArr), $emptyBedCount);
                    $totalDataArr['nyuin']           = bcadd($this->getArrValueByKey('nyuin',           $totalDataArr), $nyuin);
                    $totalDataArr['taiin']           = bcadd($this->getArrValueByKey('taiin',           $totalDataArr), $taiin);
                }
            }

            // データがない場合
            if (count($tdArr) != '2') {
                $tdArr[] = $this->convertCellData(
                    array(
                         # 外来患者数
                         0 => array('status' => $status, 'data' => ''),

                         # 入院(一般) 一般病棟 患者数, 空床数, 稼働率
                         1 => array('status' => $status, 'data' => ''),
                         2 => array('status' => $status, 'data' => ''),
                         3 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                         # 入院(一般) 障害者病棟 患者数, 空床数, 患者数
                         4 => array('status' => $status, 'data' => ''),
                         5 => array('status' => $status, 'data' => ''),
                         6 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                         # 入院(一般) 精神一般病棟 患者数, 空床数, 稼働率
                         7 => array('status' => $status, 'data' => ''),
                         8 => array('status' => $status, 'data' => ''),
                         9 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(療養) 医療療養病棟 患者数, 空床数, 稼働率
                        10 => array('status' => $status, 'data' => ''),
                        11 => array('status' => $status, 'data' => ''),
                        12 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(療養) 介護療養病棟 患者数, 空床数, 稼働率
                        13 => array('status' => $status, 'data' => ''),
                        14 => array('status' => $status, 'data' => ''),
                        15 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) 回復期ﾘﾊ病棟  患者数, 空床数, 稼働率
                        16 => array('status' => $status, 'data' => ''),
                        17 => array('status' => $status, 'data' => ''),
                        18 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) 亜急性期病床 患者数, 空床数, 稼働率
                        19 => array('status' => $status, 'data' => ''),
                        20 => array('status' => $status, 'data' => ''),
                        21 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) 緩和ｹｱ病棟 患者数, 空床数, 稼働率
                        22 => array('status' => $status, 'data' => ''),
                        23 => array('status' => $status, 'data' => ''),
                        24 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) ICU及びハイケアユニット 患者数, 空床数, 稼働率
                        25 => array('status' => $status, 'data' => ''),
                        26 => array('status' => $status, 'data' => ''),
                        27 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) 小児管理料 患者数, 空床数, 稼働率
                        28 => array('status' => $status, 'data' => ''),
                        29 => array('status' => $status, 'data' => ''),
                        30 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) 精神療養病棟 患者数, 空床数, 稼働率
                        31 => array('status' => $status, 'data' => ''),
                        32 => array('status' => $status, 'data' => ''),
                        33 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) 特殊疾患(2) 患者数, 空床数, 稼働率
                        34 => array('status' => $status, 'data' => ''),
                        35 => array('status' => $status, 'data' => ''),
                        36 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院(特定) 認知症病棟 患者数, 空床数, 稼働率
                        37 => array('status' => $status, 'data' => ''),
                        38 => array('status' => $status, 'data' => ''),
                        39 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                        # 入院合計 患者数, 空床数, 稼働病床稼働率(▲), 稼働病床稼働率, 許可病床稼働率(▲), 許可病床稼働率, 入院, 退院
                        40 => array('status' => $status, 'data' => ''),
                        41 => array('status' => $status, 'data' => ''),
                        42 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AS', $rowNum, '90')),
                        43 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                        44 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AU', $rowNum, '90')),
                        45 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                        46 => array('status' => $status, 'data' => ''),
                        47 => array('status' => $status, 'data' => ''),
                    ),
                    $fontTag,
                    array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma)
                );
            }
            $rtnStr .= '<tr>'. implode('', $tdArr).'</tr>';
            $rowNum++;
        }

        // 合計
        $totalArr[] = $this->convertCellData(
            array(0 => array('status' => 3, 'data' => '合計')),
            '%s',
            array(
                'textAlign' => $textAlign_center,
                'fontSize'  => array('name' => 'font-size',        'value' => '120%'),
                'bgColor'   => array('name' => 'background-color', 'value' => $this->getRgbColor('blue')),
            )
        );

        $gokeiGa = $this->getArrValueByKey('gokei_ga', $totalDataArr);

        $lastRow = $defaultRowNum+$plantCount - 1;

        $totalGokeiGa       = $this->getExcelLengthSumData('B', $defaultRowNum, $lastRow);
        $totalPatientCount  = $this->getArrayValueByKeyForEmptyIsNull('patient_count', $totalDataArr);
        $totalActCount      = $this->getArrayValueByKeyForEmptyIsNull('act_count',     $totalDataArr);
        $totalPmtCount      = $this->getArrayValueByKeyForEmptyIsNull('pmt_count',     $totalDataArr);

        $total_movement_sickbed_utilization_rates   = is_null($totalActCount)? '' : $this->getExcelDivData($totalPatientCount, $totalActCount);
        $total_permission_sickbed_utilization_rates = is_null($totalPmtCount)? '' : $this->getExcelDivData($totalPatientCount, $totalPmtCount);

        $totalParamArr = array();
        foreach ($categoryArr as $key => $val) {
            $totalParamArr['gokei_'.$val] = $this->getArrayValueByKeyForEmptyIsNull('gokei_'.$val, $totalDataArr);
            $totalParamArr['gokei_'.$val] = is_null($totalParamArr['gokei_'.$val])? '': $totalParamArr['gokei_'.$val];
            $totalParamArr[$val.'_act']   = $this->getArrayValueByKeyForEmptyIsNull($val.'_act', $totalDataArr);
            $totalParamArr[$val.'_act']   = is_null($totalParamArr[$val.'_act'])? '': $totalParamArr[$val.'_act'];
            $totalParamArr[$val.'_rates'] = empty($totalParamArr[$val.'_act'])? '' : $this->getExcelDivData($totalParamArr['gokei_'.$val], $totalParamArr[$val.'_act']);
        }

        $totalArr[] = $this->convertCellData(
            array(
                 0 => array('status' => $totalStatus, 'data' => $totalGokeiGa),

                 1 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('C', $defaultRowNum, $lastRow)),
                 2 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('D', $defaultRowNum, $lastRow)),
                 3 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('ip_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                 4 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('F', $defaultRowNum, $lastRow)),
                 5 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('G', $defaultRowNum, $lastRow)),
                 6 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('shog_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                 7 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('I', $defaultRowNum, $lastRow)),
                 8 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('J', $defaultRowNum, $lastRow)),
                 9 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('seip_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                10 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('L', $defaultRowNum, $lastRow)),
                11 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('M', $defaultRowNum, $lastRow)),
                12 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('ir_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                13 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('O', $defaultRowNum, $lastRow)),
                14 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('P', $defaultRowNum, $lastRow)),
                15 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('kaig_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                16 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('R', $defaultRowNum, $lastRow)),
                17 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('S', $defaultRowNum, $lastRow)),
                18 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('kaif_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                19 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('U', $defaultRowNum, $lastRow)),
                20 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('V', $defaultRowNum, $lastRow)),
                21 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('ak_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                22 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('X', $defaultRowNum, $lastRow)),
                23 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('Y', $defaultRowNum, $lastRow)),
                24 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('kan_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                25 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AA', $defaultRowNum, $lastRow)),
                26 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AB', $defaultRowNum, $lastRow)),
                27 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('icu_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                28 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AD', $defaultRowNum, $lastRow)),
                29 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AE', $defaultRowNum, $lastRow)),
                30 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('shoni_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                31 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AG', $defaultRowNum, $lastRow)),
                32 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AH', $defaultRowNum, $lastRow)),
                33 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('seir_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                34 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AJ', $defaultRowNum, $lastRow)),
                35 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AK', $defaultRowNum, $lastRow)),
                36 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('tok_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                37 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AM', $defaultRowNum, $lastRow)),
                38 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AN', $defaultRowNum, $lastRow)),
                39 => array('status' => $totalStatus, 'data' => $this->getArrValueByKey('nin_rates', $totalParamArr), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),

                40 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AP', $defaultRowNum, $lastRow)),
                41 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AQ', $defaultRowNum, $lastRow)),
                42 => array('status' => $totalStatus, 'data' => $this->getExcelUtilizationRatesTrigona('AS' , $lastRow+1, '90')),
                43 => array('status' => $totalStatus, 'data' => $total_movement_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                44 => array('status' => $totalStatus, 'data' => $this->getExcelUtilizationRatesTrigona('AU' , $lastRow+1, '90')),
                45 => array('status' => $totalStatus, 'data' => $total_permission_sickbed_utilization_rates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)),
                46 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AV', $defaultRowNum, $lastRow)),
                47 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AW', $defaultRowNum, $lastRow)),
            ),
            $fontTag,
            array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma)
        );
        $rtnStr .= '<tr>'.implode('', $totalArr).'</tr></table>';
        return $rtnStr;
    }

    /**
     * No.64 [施設別] 患者月報(日報・日毎)のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string
     */
    function outputExcelMonthlyReportAccordingToDay($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr = $this->getExcelMeta();
        $font = $this->getFont();
        $font['color'] = null;
        $fontTag = $this->getFontTag($font);
        $bgColorBlue = $this->getRgbColor('blue', 'background-color');
        $msoNumberFormat = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma = $this->getMsoNumberFormat('comma');
//        $msoNumberFormat = array('name' => 'mso-number-format', 'value' => '0.0%');
        $textAlign_center = array('name' => 'text-align', 'value' => 'center');

        $header = '<table border="1" class="list this_table" width="100%">'
                     .'<tr>'
                         .sprintf('<td rowspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '日付').'</td>'
                         .sprintf('<td rowspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '外来<br />患者数').'</td>'
                         .sprintf('<td colspan="9" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '入院(一般)').'</td>'
                         .sprintf('<td colspan="6" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '入院(療養)').'</td>'
                         .sprintf('<td colspan="24" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '入院(特定)').'</td>'
                         .sprintf('<td rowspan="2" colspan="6" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '入院合計').'</td>'
                         .sprintf('<td rowspan="2" colspan="2" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '退院合計').'</td>'
                     .'</tr>'
                     .'<tr>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '一般病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '障害者病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '精神一般病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '医療療養病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '介護療養病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '回復期ﾘﾊ病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '亜急性期病床').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '緩和ｹｱ病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, 'ICU及びハイケアユニット').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '小児管理料').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '精神療養病棟').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '特殊疾患(2)').'</td>'
                         .sprintf('<td colspan="3" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '認知症病棟').'</td>'
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(
                                  0 => array('data' => '患者数'),
                                  1 => array('data' => '空床数'),
                                  2 => array('data' => '稼働率'),
                                  3 => array('data' => '患者数'),
                                  4 => array('data' => '空床数'),
                                  5 => array('data' => '稼働率'),
                                  6 => array('data' => '患者数'),
                                  7 => array('data' => '空床数'),
                                  8 => array('data' => '稼働率'),
                                  9 => array('data' => '患者数'),
                                 10 => array('data' => '空床数'),
                                 11 => array('data' => '稼働率'),
                                 12 => array('data' => '患者数'),
                                 13 => array('data' => '空床数'),
                                 14 => array('data' => '稼働率'),
                                 15 => array('data' => '患者数'),
                                 16 => array('data' => '空床数'),
                                 17 => array('data' => '稼働率'),
                                 18 => array('data' => '患者数'),
                                 19 => array('data' => '空床数'),
                                 20 => array('data' => '稼働率'),
                                 21 => array('data' => '患者数'),
                                 22 => array('data' => '空床数'),
                                 23 => array('data' => '稼働率'),
                                 24 => array('data' => '患者数'),
                                 25 => array('data' => '空床数'),
                                 26 => array('data' => '稼働率'),
                                 27 => array('data' => '患者数'),
                                 28 => array('data' => '空床数'),
                                 29 => array('data' => '稼働率'),
                                 30 => array('data' => '患者数'),
                                 31 => array('data' => '空床数'),
                                 32 => array('data' => '稼働率'),
                                 33 => array('data' => '患者数'),
                                 34 => array('data' => '空床数'),
                                 35 => array('data' => '稼働率'),
                                 36 => array('data' => '患者数'),
                                 37 => array('data' => '空床数'),
                                 38 => array('data' => '稼働率'),
                                 39 => array('data' => '患者数'),
                                 40 => array('data' => '空床数'),
                             ),
                             $fontTag,
                             array(
                                 'textAlign'  => array('name' => 'text-align',       'value' => 'center'),
                                 'whiteSpace' => array('name' => 'white-space',      'value' => 'nowrap'),
                                 'bgColor'    => array('name' => 'background-color', 'value' => $this->getRgbColor('blue')),
                             )
                         )
                         .sprintf('<td colspan="2" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '稼働病床<br />稼働率').'</td>'
                         .sprintf('<td colspan="2" style="text-align: center; white-space: nowrap;%s">', $bgColorBlue).sprintf($fontTag, '許可病床<br />稼働率').'</td>'
                         .$this->convertCellData(
                             array(
                                  0 => array('data' => '入院'),
                                  1 => array('data' => '退院'),
                             ),
                             $fontTag,
                             array(
                                 'textAlign'  => array('name' => 'text-align',       'value' => 'center'),
                                 'whiteSpace' => array('name' => 'white-space',      'value' => 'nowrap'),
                                 'bgColor'    => array('name' => 'background-color', 'value' => $this->getRgbColor('blue')),
                             )
                         )
                     .'</tr>';
        $rtnStr .= $header;

        $year    = $this->getArrValueByKey('year',  $dateArr);
        $month   = $this->getArrValueByKey('month', $dateArr);
        $lastDay = $this->getLastDay($year, $month);

        $totalStatus = 3;

        $totalGa    = array();
        $totalIp    = array();
        $totalShog  = array();
        $totalSeip  = array();
        $totalIr    = array();
        $totalKaig  = array();
        $totalKaif  = array();
        $totalAk    = array();
        $totalKan   = array();
        $totalIcu   = array();
        $totalShoni = array();
        $totalSeir  = array();
        $totalTok   = array();
        $totalNin   = array();

        $nyuinTotal = array();

        $defaultExcelRowNum = 4;

        for ($d=1; $d<=$lastDay; $d++) {
            $tdArr    = array();
            $day      = sprintf('%02d', $d);
            $checkFlg = false;

            $excelRowNum = $defaultExcelRowNum + $d;

            foreach ($dataArr as $dataVal) {
                if ($day == $this->getArrValueByKey('day', $dataVal)) {
                    $status = 3;
                    $totalStatus = ($totalStatus > $status)? $status : $totalStatus;

                    $gokeiGa      = $this->getArrValueByKey('gokei_ga',      $dataVal);
                    $gokeiIp      = $this->getArrValueByKey('gokei_ip',      $dataVal);
                    $gokeiShog    = $this->getArrValueByKey('gokei_shog',    $dataVal);
                    $gokeiSeip    = $this->getArrValueByKey('gokei_seip',    $dataVal);
                    $gokeiIr      = $this->getArrValueByKey('gokei_ir',      $dataVal);
                    $gokeiKaig    = $this->getArrValueByKey('gokei_kaig',    $dataVal);
                    $gokeiKaif    = $this->getArrValueByKey('gokei_kaif',    $dataVal);
                    $gokeiAk      = $this->getArrValueByKey('gokei_ak',      $dataVal);
                    $gokeiKan     = $this->getArrValueByKey('gokei_kan',     $dataVal);
                    $gokeiIcu     = $this->getArrValueByKey('gokei_icu',     $dataVal);
                    $gokeiShoni   = $this->getArrValueByKey('gokei_shoni',   $dataVal);
                    $gokeiSeir    = $this->getArrValueByKey('gokei_seir',    $dataVal);
                    $gokeiTok     = $this->getArrValueByKey('gokei_tok',     $dataVal);
                    $gokeiNin     = $this->getArrValueByKey('gokei_nin',     $dataVal);
                    $ipAct        = $this->getArrValueByKey('ip_act',        $dataVal);
                    $shogAct      = $this->getArrValueByKey('shog_act',      $dataVal);
                    $seipAct      = $this->getArrValueByKey('seip_act',      $dataVal);
                    $irAct        = $this->getArrValueByKey('ir_act',        $dataVal);
                    $kaigAct      = $this->getArrValueByKey('kaig_act',      $dataVal);
                    $kaifAct      = $this->getArrValueByKey('kaif_act',      $dataVal);
                    $akAct        = $this->getArrValueByKey('ak_act',        $dataVal);
                    $kanAct       = $this->getArrValueByKey('kan_act',       $dataVal);
                    $icuAct       = $this->getArrValueByKey('icu_act',       $dataVal);
                    $shoniAct     = $this->getArrValueByKey('shoni_act',     $dataVal);
                    $seirAct      = $this->getArrValueByKey('seir_act',      $dataVal);
                    $tokAct       = $this->getArrValueByKey('tok_act',       $dataVal);
                    $ninAct       = $this->getArrValueByKey('nin_act',       $dataVal);
                    $patientCount = $this->getArrValueByKey('patient_count', $dataVal);
                    $actCount     = $this->getArrValueByKey('act_count',     $dataVal);
                    $pmtCount     = $this->getArrValueByKey('pmt_count',     $dataVal);

                    $movementSickbedUtilizationRates   = $this->getExcelDivData($patientCount, $actCount, 3);
                    $permissionSickbedUtilizationRates = $this->getExcelDivData($patientCount, $pmtCount, 3);

                    $nyuin = $this->getArrValueByKey('nyuin', $dataVal);
                    $taiin = $this->getArrValueByKey('taiin', $dataVal);

                    $totalGa['gokei']            = $totalGa['gokei']            + $gokeiGa;
                    $totalIp['gokei']            = $totalIp['gokei']            + $gokeiIp;
                    $totalIp['act']              = $totalIp['act']              + $ipAct;
                    $totalShog['gokei']          = $totalShog['gokei']          + $gokeiShog;
                    $totalShog['act']            = $totalShog['act']            + $shogAct;
                    $totalSeip['gokei']          = $totalSeip['gokei']          + $gokeiSeip;
                    $totalSeip['act']            = $totalSeip['act']            + $seipAct;
                    $totalIr['gokei']            = $totalIr['gokei']            + $gokeiIr;
                    $totalIr['act']              = $totalIr['act']              + $irAct;
                    $totalKaig['gokei']          = $totalKaig['gokei']          + $gokeiKaig;
                    $totalKaig['act']            = $totalKaig['act']            + $kaigAct;
                    $totalKaif['gokei']          = $totalKaif['gokei']          + $gokeiKaif;
                    $totalKaif['act']            = $totalKaif['act']            + $kaifAct;
                    $totalAk['gokei']            = $totalAk['gokei']            + $gokeiAk;
                    $totalAk['act']              = $totalAk['act']              + $akAct;
                    $totalKan['gokei']           = $totalKan['gokei']           + $gokeiKan;
                    $totalKan['act']             = $totalKan['act']             + $kanAct;
                    $totalIcu['gokei']           = $totalIcu['gokei']           + $gokeiIcu;
                    $totalIcu['act']             = $totalIcu['act']             + $icuAct;
                    $totalShoni['gokei']         = $totalShoni['gokei']         + $gokeiShoni;
                    $totalShoni['act']           = $totalShoni['act']           + $shoniAct;
                    $totalSeir['gokei']          = $totalSeir['gokei']          + $gokeiSeir;
                    $totalSeir['act']            = $totalSeir['act']            + $seirAct;
                    $totalTok['gokei']           = $totalTok['gokei']           + $gokeiTok;
                    $totalTok['act']             = $totalTok['act']             + $tokAct;
                    $totalNin['gokei']           = $totalNin['gokei']           + $gokeiNin;
                    $totalNin['act']             = $totalNin['act']             + $ninAct;
                    $nyuinTotal['patient_count'] = $nyuinTotal['patient_count'] + $patientCount;
                    $nyuinTotal['act_count']     = $nyuinTotal['act_count']     + $actCount;
                    $nyuinTotal['pmt_count']     = $nyuinTotal['pmt_count']     + $pmtCount;
                    $nyuinTotal['nyuin']         = $nyuinTotal['nyuin']         + $nyuin;
                    $nyuinTotal['taiin']         = $nyuinTotal['taiin']         + $taiin;

                    $tdArr[] = $this->convertCellData(
                        array(
                            0 => array(
                                'data'   => sprintf('%02d月%02d日(%s)', $month, $day, $this->getDayOfTheWeekForDate($year, $month, $day)),
                                'style'  => array('bgColor' => array('name' => 'background-color', 'value' => $this->getRgbColor('blue')))
                            ),

                            # 外来患者数
                            1 => array('status' => $status, 'data' => $gokeiGa),

                            # 入院(一般) 一般病棟
                            2 => array('status' => $status, 'data' => $gokeiIp),
                            3 => array('status' => $status, 'data' => $this->getExcelSubData($ipAct, $gokeiIp, 3)),
                            4 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiIp, $ipAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(一般) 障害者病棟
                            5 => array('status' => $status, 'data' => $gokeiShog),
                            6 => array('status' => $status, 'data' => $this->getExcelSubData($shogAct, $gokeiShog, 3)),
                            7 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiShog, $shogAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(一般) 精神一般病棟
                            8 => array('status'  => $status, 'data' => $gokeiSeip),
                            9 => array('status'  => $status, 'data' => $this->getExcelSubData($seipAct, $gokeiSeip, 3)),
                            10 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiSeip, $seipAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(療養) 医療療養病棟
                            11 => array('status' => $status, 'data' => $gokeiIr),
                            12 => array('status' => $status, 'data' => $this->getExcelSubData($irAct, $gokeiIr, 3)),
                            13 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiIr, $irAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(療養) 介護療養病棟
                            14 => array('status' => $status, 'data' => $gokeiKaig),
                            15 => array('status' => $status, 'data' => $this->getExcelSubData($kaigAct, $gokeiKaig, 3)),
                            16 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiKaig, $kaigAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) 回復期ﾘﾊ病棟
                            17 => array('status' => $status, 'data' => $gokeiKaif),
                            18 => array('status' => $status, 'data' => $this->getExcelSubData($kaifAct, $gokeiKaif, 3)),
                            19 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiKaif, $kaifAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) 亜急性期病床
                            20 => array('status' => $status, 'data' => $gokeiAk),
                            21 => array('status' => $status, 'data' => $this->getExcelSubData($akAct, $gokeiAk, 3)),
                            22 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiAk, $akAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) 緩和ｹｱ病棟
                            23 => array('status' => $status, 'data' => $gokeiKan),
                            24 => array('status' => $status, 'data' => $this->getExcelSubData($kanAct, $gokeiKan, 3)),
                            25 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiKan, $kanAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) ICU及びハイケアユニット
                            26 => array('status' => $status, 'data' => $gokeiIcu),
                            27 => array('status' => $status, 'data' => $this->getExcelSubData($icuAct, $gokeiIcu, 3)),
                            28 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiIcu, $icuAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) 小児管理料
                            29 => array('status' => $status, 'data' => $gokeiShoni),
                            30 => array('status' => $status, 'data' => $this->getExcelSubData($shoniAct, $gokeiShoni, 3)),
                            31 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiShoni, $shoniAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) 精神療養病棟
                            32 => array('status' => $status, 'data' => $gokeiSeir),
                            33 => array('status' => $status, 'data' => $this->getExcelSubData($seirAct, $gokeiSeir, 3)),
                            34 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiSeir, $seirAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) 特殊疾患(2)
                            35 => array('status' => $status, 'data' => $gokeiTok),
                            36 => array('status' => $status, 'data' => $this->getExcelSubData($tokAct, $gokeiTok, 3)),
                            37 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiTok, $tokAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院(特定) 認知症病棟
                            38 => array('status' => $status, 'data' => $gokeiNin),
                            39 => array('status' => $status, 'data' => $this->getExcelSubData($ninAct, $gokeiNin, 3)),
                            40 => array('status' => $status, 'data' => $this->getExcelDivData($gokeiNin, $ninAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),

                            # 入院合計
                            41 => array('status' => $status, 'data' => $patientCount),
                            42 => array('status' => $status, 'data' => $this->getExcelSubData($actCount, $patientCount, 3)),
                            43 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AS', $excelRowNum)),
                            44 => array('status' => $status, 'data' => $movementSickbedUtilizationRates, 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                            45 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AU', $excelRowNum)),
                            46 => array('status' => $status, 'data' => $permissionSickbedUtilizationRates, 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                            47 => array('status' => $status, 'data' => $nyuin),
                            48 => array('status' => $status, 'data' => $taiin),
                        ),
                        $fontTag,
                        array(
                            'textAlign' => array('name' => 'text-align', 'value' => 'center')
                          , 'whiteSpace' => array('name' => 'white-space', 'value' => 'nowrap')
                          , 'msoNumberFormat' => $msoNumberFormat_comma
                        )
                    );
                    $rtnStr .= sprintf('<tr>%s</tr>', implode('', $tdArr));
                    $checkFlg = true;
                }
            }

            // その日のデータがなかった場合
            if ($checkFlg!==true) {
                $status      = 3;
                $totalStatus = ($totalStatus > $status)? $status : $totalStatus;
                $tdArr[]     = $this->convertCellData(
                    array(
                         0 => array(
                             'data'  => sprintf('%02d月%02d日(%s)', $month, $day, $this->getDayOfTheWeekForDate($year, $month, $day)),
                             'style' => array('bgColor' => array('name' => 'background-color', 'value' => $this->getRgbColor('blue')))
                         ),
                         1 => array('status' => $status, 'data' => ''),
                         2 => array('status' => $status, 'data' => ''),
                         3 => array('status' => $status, 'data' => ''),
                         4 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                         5 => array('status' => $status, 'data' => ''),
                         6 => array('status' => $status, 'data' => ''),
                         7 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                         8 => array('status' => $status, 'data' => ''),
                         9 => array('status' => $status, 'data' => ''),
                        10 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        11 => array('status' => $status, 'data' => ''),
                        12 => array('status' => $status, 'data' => ''),
                        13 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        14 => array('status' => $status, 'data' => ''),
                        15 => array('status' => $status, 'data' => ''),
                        16 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        17 => array('status' => $status, 'data' => ''),
                        18 => array('status' => $status, 'data' => ''),
                        19 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        20 => array('status' => $status, 'data' => ''),
                        21 => array('status' => $status, 'data' => ''),
                        22 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        23 => array('status' => $status, 'data' => ''),
                        24 => array('status' => $status, 'data' => ''),
                        25 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        26 => array('status' => $status, 'data' => ''),
                        27 => array('status' => $status, 'data' => ''),
                        28 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        29 => array('status' => $status, 'data' => ''),
                        30 => array('status' => $status, 'data' => ''),
                        31 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        32 => array('status' => $status, 'data' => ''),
                        33 => array('status' => $status, 'data' => ''),
                        34 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        35 => array('status' => $status, 'data' => ''),
                        36 => array('status' => $status, 'data' => ''),
                        37 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        38 => array('status' => $status, 'data' => ''),
                        39 => array('status' => $status, 'data' => ''),
                        40 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        41 => array('status' => $status, 'data' => ''),
                        42 => array('status' => $status, 'data' => ''),
                        43 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AS', $excelRowNum)),
                        44 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        45 => array('status' => $status, 'data' => $this->getExcelUtilizationRatesTrigona('AU', $excelRowNum)),
                        46 => array('status' => $status, 'data' => '', 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                        47 => array('status' => $status, 'data' => ''),
                        48 => array('status' => $status, 'data' => ''),
                    ),
                    $fontTag,
                    array(
                        'textAlign'  => array('name' => 'text-align',  'value' => 'center')
                      , 'whiteSpace' => array('name' => 'white-space', 'value' => 'nowrap')
                      , 'msoNumberFormat' => $msoNumberFormat_comma
                    )
                );
                $rtnStr .= sprintf('<tr>%s</tr>', implode('', $tdArr));
            }

        }

        $totalExcelRowNum = $defaultExcelRowNum + $lastDay + 1;

        $totalMovementSickbedUtilizationRates = $this->getExcelDivData($nyuinTotal['patient_count'], $nyuinTotal['act_count'], 3);
        $totalPermissionSickbedUtilizationRates = $this->getExcelDivData($nyuinTotal['patient_count'], $nyuinTotal['pmt_count'], 3);

        $tdTotalArr[] = $this->convertCellData(
            array(
                 0 => array('data' => '合計', 'style' => array('bgColor'    => array('name' => 'background-color', 'value' => $this->getRgbColor('blue')))),
                 1 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('B', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                 2 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('C', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                 3 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('D', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                 4 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalIp['gokei'], $totalIp['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                 5 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('F', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                 6 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('G', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                 7 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalShog['gokei'], $totalShog['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                 8 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('I', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                 9 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('J', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                10 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalSeip['gokei'], $totalSeip['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                11 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('L', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                12 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('M', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                13 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalIr['gokei'], $totalIr['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                14 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('O', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                15 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('P', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                16 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalKaig['gokei'], $totalKaig['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                17 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('R', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                18 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('S', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                19 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalKaif['gokei'], $totalKaif['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                20 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('U', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                21 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('V', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                22 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalAk['gokei'], $totalAk['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                23 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('X', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                24 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('Y', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                25 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalKan['gokei'], $totalKan['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                26 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AA', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                27 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AB', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                28 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalIcu['gokei'], $totalIcu['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                29 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AD', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                30 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AE', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                31 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalShoni['gokei'], $totalShoni['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat))  ,
                32 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AG', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                33 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AH', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                34 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalSeir['gokei'], $totalSeir['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                35 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AJ', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                36 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AK', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                37 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalNin['gokei'], $totalNin['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                38 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AM', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                39 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AN', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                40 => array('status' => $totalStatus, 'data' => $this->getExcelDivData($totalTok['gokei'], $totalTok['act'], 3), 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                41 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AP', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                42 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AQ', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                43 => array('status' => $totalStatus, 'data' => $this->getExcelUtilizationRatesTrigona('AS', $totalExcelRowNum)),
                44 => array('status' => $totalStatus, 'data' => $totalMovementSickbedUtilizationRates, 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                45 => array('status' => $totalStatus, 'data' => $this->getExcelUtilizationRatesTrigona('AU', $totalExcelRowNum)),
                46 => array('status' => $totalStatus, 'data' => $totalPermissionSickbedUtilizationRates, 'style' => array('msoNumberFormat' => $msoNumberFormat)),
                47 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AV', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
                48 => array('status' => $totalStatus, 'data' => $this->getExcelLengthSumData('AW', $defaultExcelRowNum+1, $defaultExcelRowNum+$lastDay)),
            ),
            $fontTag,
            array(
                'textAlign'  => array('name' => 'text-align', 'value' => 'center')
              , 'whiteSpace' => array('name' => 'white-space', 'value' => 'nowrap')
              , 'msoNumberFormat' => $msoNumberFormat_comma
            )
        );
        $rtnStr .= sprintf('<tr>%s</tr>', implode('', $tdTotalArr));
        $rtnStr .= '</table>';

        return $rtnStr;
    }

    /**
     * No.65 病院別患者月報のエクセル出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelMonthlyReportAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();
        $year    = $this->getArrValueByKey('year',  $dateArr);
        $month   = $this->getArrValueByKey('month', $dateArr);

        # new IndicatorData()
        $dataObj = $this->getIndicatorData();

        $currentYear         = $this->getCurrentYear();
        $lastYear            = $this->getLastYear();
        $currentYearDayCount = $dataObj->getDayCountOfMonth($year, $month);
        $lastYearDayCount    = $dataObj->getDayCountOfMonth($year, $month);

        $rgbBlue  = $this->getRgbColor('blue');
        $rgbGreen = $this->getRgbColor('green');

        # style
        $bgColor_blue            = array('name' => 'background-color', 'value' => $rgbBlue);
        $bgColor_weekday         = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday        = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday         = array('name' => 'background-color', 'value' => $this->getRgbColor('lightpink'));
        $textAlignArr_center     = array('name' => 'text-align',  'value' => 'center');
        $textAlignArr_right      = array('name' => 'text-align',  'value' => 'right');
        $textAlignArr_left       = array('name' => 'text-align',  'value' => 'left');
        $whiteSpace_nowrap       = array('name' => 'white-space', 'value' => 'nowrap');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_6  = array('name' => 'colspan', 'value' =>  6);
        $colspan_9  = array('name' => 'colspan', 'value' =>  9);
        $colspan_24 = array('name' => 'colspan', 'value' => 24);

        $rtnStr .= '<table border="1" width="100%" >'
                      .'<tr>'
                          .'<td style="text-align: left;">'
                              .'<table class="list this_table" width="25%" border="1" >'
                                  .'<tr>'
                                      .$this->convertCellData(
                                          array(0 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday)),
                                                1 => array('data' => $currentYearDayCount['weekday'].'日', 'style' =>  array('textAlign'  => $textAlignArr_center)),
                                                2 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday)),
                                                3 => array('data' => $currentYearDayCount['saturday'].'日', 'style' =>  array('textAlign'  => $textAlignArr_center)),
                                                4 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday)),
                                                5 => array('data' => $currentYearDayCount['holiday'].'日', 'style' =>  array('textAlign'  => $textAlignArr_center)),
                                           ),
                                           $fontTag,
                                           array('whiteSpace' => $whiteSpace_nowrap, 'textAlign'  => $textAlignArr_left)
                                       )
                                   .'</tr>'
                               .'</table>'
                          .'</td>'
                          .'<td style="text-align: right;"><table border="1"><tr><td /><td>'.sprintf($fontTag, $year.'年'.$month.'月').'</td></tr></table></td>'
                      .'</tr>'
                  .'</table>';

        $header = '<table class="list thie_table" width="100%" border="1" >'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '病院名',           'attr' => array($rowspan_3)),
                                   1 => array('data' => '外来<br />患者数', 'attr' => array($rowspan_3)),
                                   2 => array('data' => '入院(一般)',       'attr' => array($colspan_9)),
                                   3 => array('data' => '入院(療養)',       'attr' => array($colspan_6)),
                                   4 => array('data' => '入院(特定)',       'attr' => array($colspan_24)),
                                   5 => array('data' => '入院合計',         'attr' => array($colspan_6, $rowspan_2)),
                                   6 => array('data' => '入退院数',         'attr' => array($colspan_2, $rowspan_2)),
                             ),
                             $fontTag,
                             array('bgColor' => array('name' => 'background-color', 'value' => $rgbBlue), 'textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '一般病棟',      'attr' => array($colspan_3)),
                                   1 => array('data' => '障害者病棟',    'attr' => array($colspan_3)),
                                   2 => array('data' => '精神一般病棟',  'attr' => array($colspan_3)),
                                   3 => array('data' => '医療療養病棟',  'attr' => array($colspan_3)),
                                   4 => array('data' => '介護療養病棟',  'attr' => array($colspan_3)),
                                   5 => array('data' => '回復期ﾘﾊ病棟',    'attr' => array($colspan_3)),
                                   6 => array('data' => '亜急性期病床',  'attr' => array($colspan_3)),
                                   7 => array('data' => '緩和ｹｱ病棟',    'attr' => array($colspan_3)),
                                   8 => array('data' => 'ICU及びハイケアユニット',     'attr' => array($colspan_3)),
                                   9 => array('data' => '小児管理料', 'attr' => array($colspan_3)),
                                  10 => array('data' => '精神療養病棟',  'attr' => array($colspan_3)),
                                  11 => array('data' => '特殊疾患(2)',   'attr' => array($colspan_3)),
                                  12 => array('data' => '認知症病棟', 'attr' => array($colspan_3)),
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '患者数'),
                                   1 => array('data' => '空床数'),
                                   2 => array('data' => '稼働率'),
                                   3 => array('data' => '患者数'),
                                   4 => array('data' => '空床数'),
                                   5 => array('data' => '稼働率'),
                                   6 => array('data' => '患者数'),
                                   7 => array('data' => '空床数'),
                                   8 => array('data' => '稼働率'),
                                   9 => array('data' => '患者数'),
                                  10 => array('data' => '空床数'),
                                  11 => array('data' => '稼働率'),
                                  12 => array('data' => '患者数'),
                                  13 => array('data' => '空床数'),
                                  14 => array('data' => '稼働率'),
                                  15 => array('data' => '患者数'),
                                  16 => array('data' => '空床数'),
                                  17 => array('data' => '稼働率'),
                                  18 => array('data' => '患者数'),
                                  19 => array('data' => '空床数'),
                                  20 => array('data' => '稼働率'),
                                  21 => array('data' => '患者数'),
                                  22 => array('data' => '空床数'),
                                  23 => array('data' => '稼働率'),
                                  24 => array('data' => '患者数'),
                                  25 => array('data' => '空床数'),
                                  26 => array('data' => '稼働率'),
                                  27 => array('data' => '患者数'),
                                  28 => array('data' => '空床数'),
                                  29 => array('data' => '稼働率'),
                                  30 => array('data' => '患者数'),
                                  31 => array('data' => '空床数'),
                                  32 => array('data' => '稼働率'),
                                  33 => array('data' => '患者数'),
                                  34 => array('data' => '空床数'),
                                  35 => array('data' => '稼働率'),
                                  36 => array('data' => '患者数'),
                                  37 => array('data' => '空床数'),
                                  38 => array('data' => '稼働率'),
                                  39 => array('data' => '患者数'),
                                  40 => array('data' => '空床数'),
                                  41 => array('data' => '稼働病床<br />稼働率', 'attr' => array($colspan_2)),
                                  42 => array('data' => '許可病床<br />稼働率', 'attr' => array($colspan_2)),
                                  43 => array('data' => '入院'),
                                  44 => array('data' => '退院'),
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';

        $rtnStr        .= $header;
        $departmentType = $this->getDepartmentType();
        $currentHeader  = '';
        $totalDataArr['current'] = array();

        $plantCount         = count($plantArr);
        $startRowNum        = 6;
        $startColumnNum     = 2;
        $columnAlphaNameArr = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {
            $plantId = $plantVal['facility_id'];
            $rowNum  = $startRowNum + $plantKey;
            $colNum  = $startColumnNum;

            $currentHeader = '<tr>'.$this->convertCellData(
                array(0 => array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'style' => array('bgColor' => $bgColor_blue))),
                $fontTag,
                array('textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
            );

            $cuurentDataArr = array();
            $checkPlant     = false;

            foreach ($dataArr as $dataVal) {
                $facilityId = $this->getArrValueByKey('jnl_facility_id', $dataVal);
                if ($plantId == $facilityId) {
                    $yearType = $dataVal['year_type'];
                    $status = $this->checkCellDataStatus('monthly_report_comp', $dataVal);
                    switch ($yearType) {
                        case 'current':
                            $cTotalAct        = 0;
                            $cTotalPmt        = 0;
                            $cTotalPatient    = 0;
                            $cTotalEmptyFloor = 0;
                            $cTotalActRate    = 0;
                            $cTotalPmtRate    = 0;

                            # 外来患者数
                            $cuurentDataArr[0]  = array('data' => $dataVal['gokei_ga']);
                            $cuurentDataArr[46] = array('data' => $dataVal['nyuin']);
                            $cuurentDataArr[47] = array('data' => $dataVal['taiin']);

                            $cTotalAct = $dataVal['act_count'];
                            $cTotalPmt = $dataVal['pmt_count'];
                            break;
                    }

                    foreach ($departmentType as $dTypeKey => $dTypeVal) {
                        $columnNum     = $dTypeKey * 3 + 1;
                        $tmpAct        = $dataVal[$dTypeVal.'_act'];
                        $tmpPatient    = $dataVal['gokei_'.$dTypeVal];
                        $tmpEmptyFloor = $this->getNumberOfEmptyFloors($tmpPatient, $tmpAct, 1);
                        $tmpRates      = $this->getUtilizationRates($tmpPatient,    $tmpAct, 1);

                        switch ($yearType) {
                            case 'current':
                                $cuurentDataArr[$columnNum]   = array('data' => $tmpPatient);
                                $cuurentDataArr[$columnNum+1] = array('data' => $tmpEmptyFloor);
                                $cuurentDataArr[$columnNum+2] = array('data' => $tmpRates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                                $cTotalPatient = $cTotalPatient + $tmpPatient;
                                $totalDataArr['current'][$columnNum] = array('act' => ($totalDataArr['current'][$columnNum]['act'] + $tmpAct));
                                break;
                        }
                    }

                    switch ($yearType) {
                        case 'current':
                            $tmpPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum+40], $rowNum);

                            $cTotalPatientArr = array();
                            foreach ($departmentType as $dTypeKey => $dTypeVal) {
                                $columnNum = $colNum + $dTypeKey * 3 + 1;
                                $cTotalPatientArr[] = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum);
                            }

                            # 入院合計
                            $cuurentDataArr[40] = array('data' => $this->getExcelSumDataByArray($cTotalPatientArr));
                            $cuurentDataArr[41] = array('data' => $this->getExcelSubData($cTotalAct, $tmpPatient, 3));
                            $cuurentDataArr[42] = array('data' => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum+43], $rowNum), 'style' => array('textAlign' => $textAlignArr_center));
                            $cuurentDataArr[43] = array('data' => $this->getExcelDivData($tmpPatient, $cTotalAct, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                            $cuurentDataArr[44] = array('data' => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum+45], $rowNum), 'style' => array('textAlign' => $textAlignArr_center));
                            $cuurentDataArr[45] = array('data' => $this->getExcelDivData($tmpPatient, $cTotalPmt, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                            $totalDataArr['current'][40] = array(
                                'act'    => ($totalDataArr['current'][40]['act']  + $cTotalAct),
                                'pmt'    => ($totalDataArr['current'][40]['pmt']  + $cTotalPmt),
                            );
                            break;
                    }

                    $checkPlant = true;
                }
            }
            if ($checkPlant == true) {
                foreach ($departmentType as $dTypeKey => $dTypeVal) {
                    $columnNum = $dTypeKey * 3 + 1;
                    $cuurentDataArr[$columnNum+2]['data'] = !empty($cuurentDataArr[$columnNum+2]['data'])? $cuurentDataArr[$columnNum+2]['data'].'%' : $cuurentDataArr[$columnNum+2]['data'];
                }
            }

            for ($i=0; $i <= 47; $i++ ) { if (!$this->getArrValueByKey($i, $cuurentDataArr)) { $cuurentDataArr[$i] = array('data' => ''); } }
            $rtnStr .= $currentHeader.$this->convertCellData($cuurentDataArr, $fontTag, array('textAlign' => $textAlignArr_right, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
        }

        for ($i=0; $i <= 47; $i++) {
            $rowNum = $startRowNum + $plantCount;
            $colNum = $startColumnNum + $i;
            switch ($i) {
                case  '0': case  '1': case  '2': case  '4': case  '5': case  '7': case  '8': case '10': case '11': case '13':
                case '14': case '16': case '17': case '19': case '20': case '22': case '23': case '25': case '26': case '28':
                case '29': case '31': case '32': case '34': case '35': case '37': case '38': case '40': case '41': case '46':
                case '47':
                    $rowArr = array();
                    for ($l=0; $l < $plantCount; $l++) {
                        $rowArrNum = $startRowNum + $l;
                        $rowArr[] = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowArrNum);
                    }
                    $totalDataArr['current'][$i]['data'] = $this->getExcelSumDataByArray($rowArr);
                    break;

                case  '3': case  '6': case  '9': case '12': case '15': case '18': case '21': case '24': case '27': case '30':
                case '33': case '36': case '39':
                    $tmpPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum-2], $rowNum);
                    $totalDataArr['current'][$i] = array(
                        'data'   => $this->getExcelDivData($tmpPatient, $totalDataArr['current'][$i-2]['act'], 3),
                        'style' => array('msoNumberFormat' => $msoNumberFormat_percent)
                    );
                    break;

                case '43':
                    $tmpPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum-3], $rowNum);
                    $totalDataArr['current'][$i] = array(
                        'data' => $this->getExcelDivData($tmpPatient, $totalDataArr['current'][$i-3]['act'], 3),
                        'style' => array('msoNumberFormat' => $msoNumberFormat_percent)
                    );
                    $totalDataArr['current'][$i+2] = array(
                        'data' => $this->getExcelDivData($tmpPatient, $totalDataArr['current'][$i-3]['pmt'], 3),
                        'style' => array('msoNumberFormat' => $msoNumberFormat_percent)
                    );
                    $totalDataArr['current'][$i-1] = array(
                        'data' => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum], $rowNum),
                        'style' => array('textAlign' => $textAlignArr_center)
                    );
                    $totalDataArr['current'][$i+1] = array(
                        'data' => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum+2], $rowNum),
                        'style' => array('textAlign' => $textAlignArr_center)
                    );
                    break;

                default:
                    break;
            }
        }

        $totalHeader = '<tr>'.$this->convertCellData(
            array(0 => array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue))),
            $fontTag,
            array('textAlign'  => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
        );

        $rtnStr .= $totalHeader.$this->convertCellData($totalDataArr['current'], $fontTag, array('textAlign' => $textAlignArr_right, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr></table>';
        return $rtnStr;
    }

    /**
     * No.66 病院別患者月報(前年比較)のエクセル出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelMonthlyReportCompDisplayInThePreviousYear($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr  = '';
        $fontTag = $this->getFontTag();
        $year    = $this->getArrValueByKey('year', $dateArr);
        $month   = $this->getArrValueByKey('month', $dateArr);

        # new IndicatorData()
        $dataObj  = $this->getIndicatorData();

        $currentYear         = $this->getCurrentYear();
        $lastYear            = $this->getLastYear();
        $currentYearDayCount = $dataObj->getDayCountOfMonth($year, $month);
        $lastYearDayCount    = $dataObj->getDayCountOfMonth($year, $month);

        $rgbBlue  = $this->getRgbColor('blue');
        $rgbGreen = $this->getRgbColor('green');

        # style
        $textAlignArr_center     = array('name' => 'text-align',        'value' => 'center');
        $textAlignArr_right      = array('name' => 'text-align',        'value' => 'right');
        $textAlignArr_left       = array('name' => 'text-align',        'value' => 'left');
        $whiteSpace_nowrap       = array('name' => 'white-space',       'value' => 'nowrap');
        $bgColor_blue            = array('name' => 'background-color',   'value' => $rgbBlue);
        $bgColor_green           = array('name' => 'background-color',   'value' => $rgbGreen);
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_6  = array('name' => 'colspan', 'value' =>  6);
        $colspan_9  = array('name' => 'colspan', 'value' =>  9);
        $colspan_24 = array('name' => 'colspan', 'value' => 24);

        $rtnStr .= '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';
        $rtnStr .= '<table border="1" width="100%" >'
                      .'<tr>'
                          .'<td style="text-align: left;">'
                              .'<table class="list this_table" width="25%" border="1" >'
                                  .'<tr>'
                                      .$this->convertCellData(
                                          array(0 => array('data' => sprintf('%04d年', $currentYear))
                                              , 1 => array('data' => '平日', 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffe9c8')))
                                              , 2 => array('data' => $this->getArrValueByKey('weekday', $currentYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlignArr_center))
                                              , 3 => array('data' => '土曜', 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#c8fcff')))
                                              , 4 => array('data' => $this->getArrValueByKey('saturday', $currentYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlignArr_center))
                                              , 5 => array('data' => '休日', 'style' => array('bgColor' => array('name' => 'background-color', 'value' => 'lightpink')))
                                              , 6 => array('data' => $this->getArrValueByKey('holiday', $currentYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlignArr_center))
                                           ),
                                           $fontTag,
                                           array('whiteSpace' => $whiteSpace_nowrap, 'textAlign'  => $textAlignArr_left)
                                       )
                                   .'</tr>'
                                   .'<tr>'
                                      .$this->convertCellData(
                                          array(0 => array('data' => sprintf('%04d年', $lastYear))
                                              , 1 => array('data' => '平日', 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffe9c8')))
                                              , 2 => array('data' => $this->getArrValueByKey('weekday', $lastYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlignArr_center))
                                              , 3 => array('data' => '土曜', 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#c8fcff')))
                                              , 4 => array('data' => $this->getArrValueByKey('saturday', $lastYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlignArr_center))
                                              , 5 => array('data' => '休日', 'style' => array('bgColor' => array('name' => 'background-color', 'value' => 'lightpink')))
                                              , 6 => array('data' => $this->getArrValueByKey('holiday', $lastYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlignArr_center))
                                           ),
                                           $fontTag,
                                           array('whiteSpace' => $whiteSpace_nowrap, 'textAlign'  => $textAlignArr_left)
                                       )
                                   .'</tr>'
                               .'</table>'
                          .'</td>'
                          .'<td style="text-align: right;"><table border="1"><tr><td rowspan="2" /><td rowspan="2" >'.sprintf($fontTag, $year.'年'.$month.'月').'</td></tr></table></td>'
                      .'</tr>'
                  .'</table>';

        # ヘッダー
        $header = '<table class="list thie_table" width="100%" border="1" >'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '病院名',           'attr' => array($rowspan_3))
                                 , 1 => array('data' => '年',               'attr' => array($rowspan_3))
                                 , 2 => array('data' => '外来<br />患者数', 'attr' => array($rowspan_3))
                                 , 3 => array('data' => '入院(一般)',       'attr' => array($colspan_9))
                                 , 4 => array('data' => '入院(療養)',       'attr' => array($colspan_6))
                                 , 5 => array('data' => '入院(特定)',       'attr' => array($colspan_24))
                                 , 6 => array('data' => '入院合計',         'attr' => array($colspan_6, $rowspan_2))
                                 , 7 => array('data' => '入退院数',         'attr' => array($colspan_2, $rowspan_2))
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '一般病棟',      'attr' => array($colspan_3))
                                 , 1 => array('data' => '障害者病棟',    'attr' => array($colspan_3))
                                 , 2 => array('data' => '精神一般病棟',  'attr' => array($colspan_3))
                                 , 3 => array('data' => '医療療養病棟',  'attr' => array($colspan_3))
                                 , 4 => array('data' => '介護療養病棟',  'attr' => array($colspan_3))
                                 , 5 => array('data' => '回復期ﾘﾊ病棟',    'attr' => array($colspan_3))
                                 , 6 => array('data' => '亜急性期病床',  'attr' => array($colspan_3))
                                 , 7 => array('data' => '緩和ｹｱ病棟',    'attr' => array($colspan_3))
                                 , 8 => array('data' => 'ICU及びハイケアユニット',     'attr' => array($colspan_3))
                                 , 9 => array('data' => '小児管理料', 'attr' => array($colspan_3))
                                , 10 => array('data' => '精神療養病棟',  'attr' => array($colspan_3))
                                , 11 => array('data' => '特殊疾患(2)',   'attr' => array($colspan_3))
                                , 12 => array('data' => '認知症病棟', 'attr' => array($colspan_3))
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign'  => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '患者数'),  1 => array('data' => '空床数'),  2 => array('data' => '稼働率'),  3 => array('data' => '患者数')
                                 , 4 => array('data' => '空床数'),  5 => array('data' => '稼働率'),  6 => array('data' => '患者数'),  7 => array('data' => '空床数')
                                 , 8 => array('data' => '稼働率'),  9 => array('data' => '患者数'), 10 => array('data' => '空床数'), 11 => array('data' => '稼働率')
                                , 12 => array('data' => '患者数'), 13 => array('data' => '空床数'), 14 => array('data' => '稼働率'), 15 => array('data' => '患者数')
                                , 16 => array('data' => '空床数'), 17 => array('data' => '稼働率'), 18 => array('data' => '患者数'), 19 => array('data' => '空床数')
                                , 20 => array('data' => '稼働率'), 21 => array('data' => '患者数'), 22 => array('data' => '空床数'), 23 => array('data' => '稼働率')
                                , 24 => array('data' => '患者数'), 25 => array('data' => '空床数'), 26 => array('data' => '稼働率'), 27 => array('data' => '患者数')
                                , 28 => array('data' => '空床数'), 29 => array('data' => '稼働率'), 30 => array('data' => '患者数'), 31 => array('data' => '空床数')
                                , 32 => array('data' => '稼働率'), 33 => array('data' => '患者数'), 34 => array('data' => '空床数'), 35 => array('data' => '稼働率')
                                , 36 => array('data' => '患者数'), 37 => array('data' => '空床数'), 38 => array('data' => '稼働率'), 39 => array('data' => '患者数')
                                , 40 => array('data' => '空床数'), 41 => array('data' => '稼働病床<br />稼働率', 'attr' => array($colspan_2))
                                , 42 => array('data' => '許可病床<br />稼働率', 'attr' => array($colspan_2))
                                , 43 => array('data' => '入院'), 44 => array('data' => '退院'),
                             ),
                             $fontTag,
                             array('bgColor' => $bgColor_blue, 'textAlign'  => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';

        $rtnStr        .= $header;
        $departmentType = $this->getDepartmentType();
        $currentHeader  = '';
        $lastHeader     = '';
        $compHeader     = '';
        $raitoHeader    = '';

        $totalDataArr['current'] = array();
        $totalDataArr['last']    = array();
        $totalDataArr['comp']    = array();
        $totalDataArr['raito']   = array();


        $plantCount = count($plantArr);
        $startRowNum        = 7;
        $startColumnNum     = 3;
        $columnAlphaNameArr = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {
            $plantId = $this->getArrValueByKey('facility_id', $plantVal);
            $rowNum  = $startRowNum + ($plantKey * 4);
            $colNum  = $startColumnNum;

            $currentHeader = '<tr>'.$this->convertCellData(
                array(
                    0 => array('data'  => $this->getArrValueByKey('facility_name', $plantVal), 'attr'  => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue))
                  , 1 => array('data' => $currentYear.'年度')
                ),
                $fontTag,
                array('textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
            );
            $lastHeader = '<tr>'.$this->convertCellData(
                array(0 => array('data' => $lastYear.'年度'))
              , $fontTag
              , array('textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
            );
            $compHeader = '<tr>'.$this->convertCellData(
                array(0 => array('data' => '前年比'))
              , $fontTag
              , array('bgColor' => $bgColor_green, 'textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
            );
            $raitoHeader = '<tr>'.$this->convertCellData(
                array(0 => array('data' => '増減率'))
              , $fontTag
              , array('bgColor' => $bgColor_green, 'textAlign' => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
            );

            $cuurentDataArr = array();
            $lastDataArr    = array();
            $compDataArr    = array();
            $raitoDataArr   = array();

            $checkPlant = false;
            foreach ($dataArr as $dataVal) {
                $facilityId = $this->getArrValueByKey('jnl_facility_id', $dataVal);
                if ($plantId == $facilityId) {
                    $yearType = $dataVal['year_type'];
                    switch ($yearType) {
                        case 'current':
                            $cTotalAct        = 0;
                            $cTotalPmt        = 0;
                            $cTotalPatient    = 0;
                            $cTotalEmptyFloor = 0;
                            $cTotalActRate    = 0;
                            $cTotalPmtRate    = 0;
                            $cTotalPatientArr = array();

                            # 外来患者数
                            $cuurentDataArr[0]  = array('data' => $dataVal['gokei_ga']);
                            $cuurentDataArr[46] = array('data' => $dataVal['nyuin']);
                            $cuurentDataArr[47] = array('data' => $dataVal['taiin']);

                            $cTotalAct = $dataVal['act_count'];
                            $cTotalPmt = $dataVal['pmt_count'];
                            break;

                        case 'last':
                            $lTotalAct        = 0;
                            $lTotalPmt        = 0;
                            $lTotalPatient    = 0;
                            $lTotalEmptyFloor = 0;
                            $lTotalActRate    = 0;
                            $lTotalPmtRate    = 0;
                            $lTotalPatientArr = array();

                            # 外来患者数
                            $lastDataArr[0]  = array('data' => $dataVal['gokei_ga']);
                            $lastDataArr[46] = array('data' => $dataVal['nyuin']);
                            $lastDataArr[47] = array('data' => $dataVal['taiin']);

                            $lTotalAct = $dataVal['act_count'];
                            $lTotalPmt = $dataVal['pmt_count'];
                            break;
                    }

                    foreach ($departmentType as $dTypeKey => $dTypeVal) {
                        $columnNum = $dTypeKey * 3 + 1;
                        $tmpAct    = $dataVal[$dTypeVal.'_act'];
                        $tmpGokei  = $dataVal['gokei_'.$dTypeVal];

                        switch ($yearType) {
                            case 'current':
                                $tmpPatient    = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum], $rowNum);
                                $tmpEmptyFloor = $this->getExcelSubData($tmpAct, $tmpPatient);
                                $tmpRates      = $this->getExcelDivData($tmpPatient, $tmpAct);
                                $cuurentDataArr[$columnNum]   = array('data' => $tmpGokei);
                                $cuurentDataArr[$columnNum+1] = array('data' => $tmpEmptyFloor);
                                $cuurentDataArr[$columnNum+2] = array('data' => $tmpRates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                                $totalDataArr['current'][$columnNum]['arr'][] = $tmpPatient;
                                $totalDataArr['current'][$columnNum]['act'] = $totalDataArr['current'][$columnNum]['act'] + $tmpAct;

                                break;

                            case 'last':
                                $tmpPatient    = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum], $rowNum+1);
                                $tmpEmptyFloor = $this->getExcelSubData($tmpAct, $tmpPatient);
                                $tmpRates      = $this->getExcelDivData($tmpPatient, $tmpAct);
                                $lastDataArr[$columnNum]   = array('data' => $tmpGokei);
                                $lastDataArr[$columnNum+1] = array('data' => $tmpEmptyFloor);
                                $lastDataArr[$columnNum+2] = array('data' => $tmpRates, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                                $totalDataArr['last'][$columnNum]['arr'][] = $tmpPatient;
                                $totalDataArr['last'][$columnNum]['act'] = $totalDataArr['last'][$columnNum]['act'] + $tmpAct;
                                break;
                        }
                    }

                    switch ($yearType) {
                        case 'current':
                            $cTotalPatientArr = array();
                            foreach ($departmentType as $dTypeKey => $dTypeVal) {
                                $columnNum = $dTypeKey * 3 + 1;
                                $cTotalPatientArr[] = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum], $rowNum);
                            }

                            $cPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum+40], $rowNum);

                            # 入院合計
                            $cuurentDataArr[40] = array('data' => $this->getExcelSumDataByArray($cTotalPatientArr));
                            $cuurentDataArr[41] = array('data' => $this->getExcelSubData($cTotalAct, $cPatient));
                            $cuurentDataArr[42] = array('data' => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+43]), $rowNum), 'style' => array('textAlign' => $textAlignArr_center));
                            $cuurentDataArr[43] = array('data' => $this->getExcelDivData($cPatient, $cTotalAct), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                            $cuurentDataArr[44] = array('data' => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+45]), $rowNum), 'style' => array('textAlign' => $textAlignArr_center));
                            $cuurentDataArr[45] = array('data' => $this->getExcelDivData($cPatient, $cTotalPmt), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                            $totalDataArr['current'][40] = array('act' => ($totalDataArr['current'][40]['act'] + $cTotalAct), 'pmt' => ($totalDataArr['current'][40]['pmt'] + $cTotalPmt));
                            break;

                        case 'last':
                            $lTotalPatientArr = array();
                            foreach ($departmentType as $dTypeKey => $dTypeVal) {
                                $columnNum = $dTypeKey * 3 + 1;
                                $lTotalPatientArr[] = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum], $rowNum+1);
                            }

                            $lPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum+40], $rowNum+1);

                            # 入院合計
                            $lastDataArr[40] = array('data' => $this->getExcelSumDataByArray($lTotalPatientArr));
                            $lastDataArr[41] = array('data' => $this->getExcelSubData($lTotalAct, $lPatient));
                            $lastDataArr[42] = array('data' => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+43]), $rowNum+1),'style' => array('textAlign' => $textAlignArr_center));
                            $lastDataArr[43] = array('data' => $this->getExcelDivData($lPatient, $lTotalAct), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                            $lastDataArr[44] = array('data' => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+45]), $rowNum+1), 'style' => array('textAlign' => $textAlignArr_center));
                            $lastDataArr[45] = array('data' => $this->getExcelDivData($lPatient, $lTotalPmt), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                            $totalDataArr['last'][40] = array('act' => ($totalDataArr['last'][40]['act'] + $lTotalAct), 'pmt' => ($totalDataArr['last'][40]['pmt']  + $lTotalPmt));
                            break;
                    }

                    $checkPlant = true;
                }
            }
            if ($checkPlant == true) {
                # 外来患者数
                $cGokeiGa = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum);
                $lGokeiGa = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+1);
                $compDataArr[0]  = array('data' => $this->getExcelSubData($cGokeiGa, $lGokeiGa, 2));
                $raitoDataArr[0] = array('data' => $this->getExcelDivData(sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+2), $lGokeiGa),'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                # 入院合計 患者数
                $cPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum+40], $rowNum);
                $lPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum+40], $rowNum+1);
                $compDataArr[40]  = array('data' => $this->getExcelSubData($cPatient, $lPatient, 2));
                $raitoDataArr[40] = array('data' => $this->getExcelDivData(sprintf('%s%d', $columnAlphaNameArr[$colNum+40], $rowNum+2), $lPatient), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                # 入院合計 空床数
                $cEmptyFloor = sprintf('%s%d', $columnAlphaNameArr[$colNum+41], $rowNum);
                $lEmptyFloor = sprintf('%s%d', $columnAlphaNameArr[$colNum+41], $rowNum+1);
                $compDataArr[41]  = array('data' => $this->getExcelSubData($cEmptyFloor, $lEmptyFloor, 2));
                $raitoDataArr[41] = array('data' => '');

                # 入院合計 稼働病床稼働率
                $cActRate = sprintf('%s%d', $columnAlphaNameArr[$colNum+43], $rowNum);
                $lActRate = sprintf('%s%d', $columnAlphaNameArr[$colNum+43], $rowNum+1);
                $compDataArr[42]  = array('data' => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum+43], $rowNum+2), 'style' => array('textAlign' => $textAlignArr_center));
                $compDataArr[43]  = array('data' => $this->getExcelSubData($cActRate, $lActRate, 2));
                $raitoDataArr[42] = array('data' => '');
                $raitoDataArr[43] = array('data' => '');

                # 入院合計 許可病床稼働率
                $cPmtRate = sprintf('%s%d', $columnAlphaNameArr[$colNum+45], $rowNum);
                $lPmtRate = sprintf('%s%d', $columnAlphaNameArr[$colNum+45], $rowNum+1);
                $compDataArr[44]  = array('data' => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum+45], $rowNum+2), 'style' => array('textAlign' => array('name' => 'text-align', 'value' => 'center')));
                $compDataArr[45]  = array('data' => $this->getExcelSubData($cPmtRate, $lPmtRate, 2));
                $raitoDataArr[44] = array('data' => '');
                $raitoDataArr[45] = array('data' => '');

                # 入退院数 入院
                $cNyuin = sprintf('%s%d', $columnAlphaNameArr[$colNum+46], $rowNum);
                $lNyuin = sprintf('%s%d', $columnAlphaNameArr[$colNum+46], $rowNum+1);
                $compDataArr[46]  = array('data' => $this->getExcelSubData($cNyuin, $lNyuin, 2));
                $raitoDataArr[46] = array('data' => $this->getExcelDivData(sprintf('%s%d', $columnAlphaNameArr[$colNum+46], $rowNum+2), $lNyuin), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                # 入退院数 退院
                $cTaiin = sprintf('%s%d', $columnAlphaNameArr[$colNum+47], $rowNum);
                $lTaiin = sprintf('%s%d', $columnAlphaNameArr[$colNum+47], $rowNum+1);
                $compDataArr[47]  = array('data' => $this->getExcelSubData($cTaiin, $lTaiin, 2));
                $raitoDataArr[47] = array('data' => $this->getExcelDivData(sprintf('%s%d', $columnAlphaNameArr[$colNum+47], $rowNum+2), $lTaiin), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));

                foreach ($departmentType as $dTypeKey => $dTypeVal) {
                    $columnNum = $dTypeKey * 3 + 1;
                    $tmpC = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum], $rowNum);
                    $tmpL = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum], $rowNum+1);
                    $compDataArr[$columnNum]  = array('data' => $this->getExcelSubData($tmpC, $tmpL));
                    $raitoDataArr[$columnNum] = array(
                        'data'  => $this->getExcelDivData(sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum], $rowNum+2), $tmpL)
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)
                    );

                    $tmpC = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum+1], $rowNum);
                    $tmpL = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum+1], $rowNum+1);
                    $compDataArr[$columnNum+1]  = array('data' => $this->getExcelSubData($tmpC, $tmpL));
                    $raitoDataArr[$columnNum+1] = array('data' => '');

                    $tmpC = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum+2], $rowNum);
                    $tmpL = sprintf('%s%d', $columnAlphaNameArr[$colNum+$columnNum+2], $rowNum+1);
                    $compDataArr[$columnNum+2]  = array('data' => $this->getExcelSubData($tmpC, $tmpL));
                    $raitoDataArr[$columnNum+2] = array('data' => '');
                }
            }

            for ($i=0; $i <= 47; $i++ ) {
                if (!$this->getArrValueByKey($i, $cuurentDataArr)) { $cuurentDataArr[$i] = array('data' => ''); }
                if (!$this->getArrValueByKey($i, $lastDataArr))    { $lastDataArr[$i]    = array('data' => ''); }

                if (!$this->getArrValueByKey($i, $compDataArr)) {
                    $colNum = $startColumnNum + $i;
                    switch ($i) {
                        default:
                            $cTmpComp = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum);
                            $lTmpComp = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+1);
                            $compDataArr[$i] = array('data' => $this->getExcelSubData($cTmpComp, $lTmpComp, 2));
                            break;

                        case '42': case '44':
                            $compDataArr[$i] = array(
                                'data'  => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum+1], $rowNum+2)
                              , 'style' => array('textAlign' => $textAlignArr_center, 'msoNumberFormat' => $msoNumberFormat_percent)
                            );
                            break;
                    }
                }
                if (!$this->getArrValueByKey($i, $raitoDataArr)) {
                    $colNum = $startColumnNum + $i;
                    switch ($i) {
                        case  '0': case  '1': case  '4': case  '7': case '10': case '13': case '16': case '19': case '22': case '25':
                        case '28': case '31': case '34': case '37': case '40': case '46': case '47':
                            $tmpComp = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+2);
                            $tmpLast = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+1);
                            $raitoDataArr[$i] = array('data' => $this->getExcelDivData($tmpComp, $tmpLast), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                            break;

                        default:
                            if (!$this->getArrValueByKey($i, $totalDataArr['raito'])) { $raitoDataArr[$i] = array('data' => ''); }
                            break;
                    }
                }
            }

            $rtnStr .= $currentHeader. $this->convertCellData($cuurentDataArr, $fontTag, array('textAlign' => $textAlignArr_right, 'msoNumberFormat' => $msoNumberFormat_comma)). '</tr>'
                      .$lastHeader. $this->convertCellData($lastDataArr, $fontTag, array('textAlign' => $textAlignArr_right, 'msoNumberFormat' => $msoNumberFormat_comma)). '</tr>'
                      .$compHeader. $this->convertCellData($compDataArr, $fontTag, array('textAlign' => $textAlignArr_right, 'bgColor' => $bgColor_green, 'msoNumberFormat' => $msoNumberFormat_comma)). '</tr>'
                      .$raitoHeader. $this->convertCellData($raitoDataArr, $fontTag, array('textAlign' => $textAlignArr_right,'bgColor' => $bgColor_green, 'msoNumberFormat' => $msoNumberFormat_comma)). '</tr>';
        }
        for ($i=0; $i <= 47; $i++) {
            $rowNum = $startRowNum + $plantCount * 4;
            $colNum = $startColumnNum + $i;
            $rowArr = array();
            switch ($i) {
                case  '0': case  '1': case  '4': case  '7': case '10': case '13': case '16': case '19': case '22': case '25':
                case '28': case '31': case '34': case '37': case '40': case '46': case '47':
                    for ($l=0; $l<$plantCount; $l++) {
                        $rowArrNum = $startRowNum + $l * 4;
                        $rowArr[] = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowArrNum);
                    }
                    $totalDataArr['current'][$i]['data'] = $this->getExcelSumDataByArray($rowArr);
                    $rowArr = array();
                    for ($l=0; $l<$plantCount; $l++) {
                        $rowArrNum = $startRowNum + $l * 4;
                        $rowArr[] = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowArrNum+1);
                    }
                    $totalDataArr['last'][$i]['data'] = $this->getExcelSumDataByArray($rowArr);
                    break;

                case  '2': case  '5': case  '8': case '11': case '14': case '17': case '20': case '23': case '26': case '29':
                case '32': case '35': case '38':
                    $tmpPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum-1], $rowNum);
                    $totalDataArr['current'][$i]   = array('data' => $this->getExcelSubData($totalDataArr['current'][$i-1]['act'], $tmpPatient));
                    $totalDataArr['current'][$i+1] = array('data' => $this->getExcelDivData($tmpPatient, $totalDataArr['current'][$i-1]['act']),'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                    $tmpPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum-1], $rowNum+1);
                    $totalDataArr['last'][$i]      = array('data' => $this->getExcelSubData($totalDataArr['last'][$i-1]['act'], $tmpPatient));
                    $totalDataArr['last'][$i+1]    = array('data' => $this->getExcelDivData($tmpPatient, $totalDataArr['last'][$i-1]['act']),'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                    break;

                case '41':
                    $tmpPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum-1], $rowNum);
                    $totalDataArr['current'][$i] = array('data' => $this->getExcelSubData($totalDataArr['current'][$i-1]['act'], $tmpPatient));
                    $totalDataArr['current'][$i+2] = array(
                        'data'  => $this->getExcelDivData($tmpPatient, $totalDataArr['current'][$i-1]['act'])
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlignArr_right)
                    );
                    $totalDataArr['current'][$i+4] = array(
                        'data'  => $this->getExcelDivData($tmpPatient, $totalDataArr['current'][$i-1]['pmt'])
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlignArr_right)
                    );
                    $totalDataArr['current'][$i+1] = array(
                        'data'  => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+2]), $rowNum)
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlignArr_right)
                    );
                    $totalDataArr['current'][$i+3] = array(
                        'data'  => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+4]), $rowNum)
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlignArr_right)
                    );
                    $tmpPatient = sprintf('%s%d', $columnAlphaNameArr[$colNum-1], $rowNum+1);
                    $totalDataArr['last'][$i] = array('data' => $this->getExcelSubData($totalDataArr['last'][$i-1]['act'], $tmpPatient));
                    $totalDataArr['last'][$i+2] = array(
                        'data'  => $this->getExcelDivData($tmpPatient, $totalDataArr['last'][$i-1]['act'])
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlignArr_right)
                    );
                    $totalDataArr['last'][$i+4] = array(
                        'data'  => $this->getExcelDivData($tmpPatient, $totalDataArr['last'][$i-1]['pmt'])
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlignArr_right)
                    );
                    $totalDataArr['last'][$i+1] = array('data' => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+2]), $rowNum+1));
                    $totalDataArr['last'][$i+3] = array('data' => $this->getExcelUtilizationRatesTrigona(sprintf('%s', $columnAlphaNameArr[$colNum+4]), $rowNum+1));
                    break;

                case '42': case '44':
                    break;

                default:
                    if (!$this->getArrValueByKey('data', $totalDataArr['current'][$i])) { $totalDataArr['current'][$i]= array('data' => ''); }
                    if (!$this->getArrValueByKey($i, $totalDataArr['last'])) {
                        $totalDataArr['last'][$i] = array('data' => '', 'status' => $this->checkCellDataStatus('comp', array($totalDataArr['last'][$i]['status'], $totalDataArr['last'][$i-1]['status'])));
                    }
                    break;
            }
            $rowArr = array();
            switch ($i) {
                default:
                    $cTmpComp = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum);
                    $lTmpComp = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+1);
                    $totalDataArr['comp'][$i] = array('data' => $this->getExcelSubData($cTmpComp, $lTmpComp, 3));
                    break;

                case '42': case '44':
                    $totalDataArr['comp'][$i] = array(
                        'data'  => $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[$colNum+1], $rowNum+2)
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlignArr_center),
                    );
                    break;
            }
            switch ($i) {
                case  '0': case  '1': case  '4': case  '7': case '10': case '13': case '16': case '19': case '22': case '25':
                case '28': case '31': case '34': case '37': case '40': case '46': case '47':
                    $tmpComp = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+2);
                    $tmpLast = sprintf('%s%d', $columnAlphaNameArr[$colNum], $rowNum+1);
                    $totalDataArr['raito'][$i] = array('data' => $this->getExcelDivData($tmpComp, $tmpLast), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                    break;

                default:
                    if (!$this->getArrValueByKey($i, $totalDataArr['raito'])) {
                        $totalDataArr['raito'][$i] = array('data' => '', 'status' => $this->checkCellDataStatus('comp', array($totalDataArr['comp'][$i]['status'], $totalDataArr['last'][$i]['status'])));
                    }
                    break;
            }
        }

        $totalHeader = '<tr>'.$this->convertCellData(
            array(
                0 => array('data' => '合計', 'attr' => array(array('name' => 'rowspan', 'value' => '4')), 'style' => array('bgColor' => $bgColor_blue))
              , 1 => array('data' => $currentYear.'年度')
            ),
            $fontTag,
            array('textAlign'  => $textAlignArr_center, 'whiteSpace' => $whiteSpace_nowrap)
        );

        $rtnStr .= $totalHeader.$this->convertCellData(
                      $totalDataArr['current']
                    , $fontTag
                    , array('textAlign' => $textAlignArr_right, 'msoNumberFormat' => $msoNumberFormat_comma, 'whiteSpace' => $whiteSpace_nowrap)
                  ).'</tr>'
                  .$lastHeader.$this->convertCellData(
                      $totalDataArr['last']
                    , $fontTag
                    , array('textAlign' => $textAlignArr_right, 'msoNumberFormat' => $msoNumberFormat_comma, 'whiteSpace' => $whiteSpace_nowrap)
                  ).'</tr>'
                  .$compHeader.$this->convertCellData(
                      $totalDataArr['comp']
                    , $fontTag
                    , array('textAlign' => $textAlignArr_right, 'bgColor' => $bgColor_green, 'msoNumberFormat' => $msoNumberFormat_comma, 'whiteSpace' => $whiteSpace_nowrap)
                  ).'</tr>'
                  .$raitoHeader.$this->convertCellData(
                      $totalDataArr['raito']
                    , $fontTag
                    , array('textAlign' => $textAlignArr_right, 'bgColor' => $bgColor_green, 'msoNumberFormat' => $msoNumberFormat_comma, 'whiteSpace' => $whiteSpace_nowrap)
                  ).'</tr></table>';
        return $rtnStr;
    }

    /**
     * 病院別患者日報(累積統計表)のエクセル出力処理
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arrx
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelDailyReportAccumulationStatistics($dataArr, $plantArr, $arr, $dateArr)
    {
        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();
        $rtnStr  = '';
        $fontTag = $this->getFontTag();

        $targetYear  = $this->getArrValueByKey('year',  $dateArr);
        $targetMonth = $this->getArrValueByKey('month', $dateArr);
        $targetDay   = $this->getArrValueByKey('day',   $dateArr);

        $rgbColor_blue = $this->getRgbColor('blue');

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $bgColor_blue      = array('name' => 'background-color', 'value' => $rgbColor_blue);

        $msoNumberFormat_percent             = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0                 = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma               = $this->getMsoNumberFormat('comma');
        $msoNumberFormat_parenthesis         = $this->getMsoNumberFormat('comma_parenthesis');
        $msoNumberFormat_0_0_parenthesis     = $this->getMsoNumberFormat('decimal_parenthesis');
        $msoNumberFormat_percent_parenthesis = $this->getMsoNumberFormat('percent_parenthesis');

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_11 = array('name' => 'colspan', 'value' => 11);

        $mmdd = sprintf('%02d月%02d日', $targetMonth, $targetDay);

        $rtnStr .= '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';
        $header = '<table border="1" width="100%" >'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => sprintf('%04d年%02d月%02d日(%s)', $targetYear, $targetMonth, $targetDay, $this->getDayOfTheWeekForDate($targetYear, $targetMonth, $targetDay)), 'style' => array('textAlign' => $textAlign_right)))
                           , $fontTag
                           , array()
                         )
                     .'</tr>'
                 .'</table>'
                 .'<table class="list this_table" border="1" width="100%" >'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '病院名',     'attr' => array($rowspan_3))
                                 , 1 => array('data' => '外来患者数', 'attr' => array($rowspan_2, $colspan_2))
                                 , 2 => array('data' => '入院合計',   'attr' => array($colspan_11))
                                 , 3 => array('data' => '入退院数',   'attr' => array($colspan_4))
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => '患者数',         'attr' => array($colspan_2))
                                 , 1 => array('data' => '空床数',         'attr' => array($rowspan_2))
                                 , 2 => array('data' => '稼働病床稼働率', 'attr' => array($colspan_4))
                                 , 3 => array('data' => '許可病床稼働率', 'attr' => array($colspan_4))
                                 , 4 => array('data' => '入院',           'attr' => array($colspan_2))
                                 , 5 => array('data' => '退院',           'attr' => array($colspan_2))
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                             array(0 => array('data' => $mmdd)
                                 , 1 => array('data' => '(月平均)')
                                 , 2 => array('data' => $mmdd)
                                 , 3 => array('data' => '(月平均)')
                                 , 4 => array('data' => $mmdd,      'attr' => array($colspan_2))
                                 , 5 => array('data' => '(月平均)', 'attr' => array($colspan_2))
                                 , 6 => array('data' => $mmdd,      'attr' => array($colspan_2))
                                 , 7 => array('data' => '(月平均)', 'attr' => array($colspan_2))
                                 , 8 => array('data' => $mmdd)
                                 , 9 => array('data' => '(月累積)')
                                , 10 => array('data' => $mmdd)
                                , 11 => array('data' => '(月累積)')
                             )
                           , $fontTag
                           , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';

        $rtnStr .= $header;

        $totalGairai          = null;
        $totalGairaiAvg       = null;
        $totalPatient         = null;
        $totalPatientAvg      = null;
        $totalORateTrigona    = null;
        $totalORateAvgTrigona = null;
        $totalORateAvg        = null;
        $totalPRateTrigona    = null;
        $totalPRate           = null;
        $totalPRateAvgTrigona = null;
        $totalPRateAvg        = null;
        $totalNyuin           = null;
        $totalNyuinSum        = null;
        $totalTaiin           = null;
        $totalTaiinSum        = null;

        $totalGokeiGa       = null;
        $totalGokeiCount    = null;
        $totalGokeiCountSum = null;
        $totalActCount      = null;
        $totalActCountSum   = null;
        $totalPmtCount      = null;
        $totalPmtCountSum   = null;

        $plantCount         = count($plantArr);
        $startRowNum        = 5;
        $startColumnNum     = 2;
        $columnAlphaNameArr = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {
            $plantId = $this->getArrValueByKey('facility_id', $plantVal);
            $rowNum  = $startRowNum + $plantKey;

            $gokeiGa           = '';
            $gokeiCount        = '';
            $actCount          = 0;
            $pmtCount          = 0;
            $dayCount          = 0;
            $dayCountNoHoliday = 0;
            $gokeiGaSum        = 0;
            $gokeiCountSum     = 0;
            $actCountSum       = 0;
            $pmtCountSum       = 0;

            $gairai          = null;
            $gairaiAvg       = null;
            $patient         = null;
            $patientAvg      = null;
            $emptyfloors     = null;
            $oRateTrigona    = null;
            $oRate           = null;
            $oRateAvgTrigona = null;
            $oRateAvg        = null;
            $pRateTrigona    = null;
            $pRate           = null;
            $pRateAvgTrigona = null;
            $pRateAvg        = null;
            $nyuin           = null;
            $nyuinSum        = null;
            $taiin           = null;
            $taiinSum        = null;

            foreach ($dataArr as $dataVal) {
                $dataVal['target_day'] = $targetDay;
                $facilityId = $this->getArrValueByKey('jnl_facility_id', $dataVal);
                if ($plantId == $facilityId) {
                    $gokeiGa           = $this->getArrValueByKey('gokei_ga',             $dataVal);
                    $gokeiCount        = $this->getArrValueByKey('gokei_count',          $dataVal);
                    $actCount          = $this->getArrValueByKey('act_count',            $dataVal);
                    $pmtCount          = $this->getArrValueByKey('pmt_count',            $dataVal);
                    $dayCount          = $this->getArrValueByKey('day_count',            $dataVal);
                    $dayCountNoHoliday = $this->getArrValueByKey('day_count_no_holiday', $dataVal);
                    $gokeiGaSum        = $this->getArrValueByKey('sum_gokei_ga',         $dataVal);
                    $gokeiCountSum     = $this->getArrValueByKey('sum_gokei_count',      $dataVal);
                    $actCountSum       = $this->getArrValueByKey('sum_act_count',        $dataVal);
                    $pmtCountSum       = $this->getArrValueByKey('sum_pmt_count',        $dataVal);

//                    $patientCoordinate = sprintf('%s%d', $columnAlphaNameArr[4], $rowNum);
//
//                    $gairai          = $gokeiGa;
//                    $gairaiAvg       = $this->getExcelDivData($gokeiGaSum, $dayCountNoHoliday, 1);
//                    $patient         = $gokeiCount;
//                    $patientAvg      = $this->getExcelDivData($gokeiCountSum, $dayCount, 1);
//                    $emptyfloors     = $this->getExcelSubData($actCount, $patientCoordinate);
//                    $oRate           = $this->getExcelDivData($patientCoordinate, $actCount);
//                    $oRateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[8], $rowNum);
//                    $oRateAvg        = $this->getExcelDivData($gokeiCountSum, $actCountSum);
//                    $oRateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[10], $rowNum);
//                    $pRate           = $this->getExcelDivData($patientCoordinate, $pmtCount);
//                    $pRateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[12], $rowNum);
//                    $pRateAvg        = $this->getExcelDivData($gokeiCountSum, $pmtCountSum);
//                    $pRateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[14], $rowNum);
//                    $nyuin           = $this->getArrValueByKey('nyuin',     $dataVal);
//                    $nyuinSum        = $this->getArrValueByKey('sum_nyuin', $dataVal);
//                    $taiin           = $this->getArrValueByKey('taiin',     $dataVal);
//                    $taiinSum        = $this->getArrValueByKey('sum_taiin', $dataVal);
//
//                    $totalGokeiGa           = $totalGokeiGa           + $gokeiGa;
//                    $totalGokeiGaSum        = $totalGokeiGaSum        + $gokeiGaSum;
//                    $totalGokeiCount        = $totalGokeiCount        + $gokeiCount;
//                    $totalGokeiCountSum     = $totalGokeiCountSum     + $gokeiCountSum;
//                    $totalDayCount          = $totalDayCount          + $dayCount;
//                    $totalDayCountNoHoliday = $totalDayCountNoHoliday + $dayCountNoHoliday;
//                    $totalActCount          = $totalActCount          + $actCount;
//                    $totalActCountSum       = $totalActCountSum       + $actCountSum;
//                    $totalPmtCount          = $totalPmtCount          + $pmtCount;
//                    $totalPmtCountSum       = $totalPmtCountSum       + $pmtCountSum;
//                    $totalGairai            = $totalGairai            + $gairai;
//                    $totalPatient           = $totalPatient           + $patient;
//                    $totalNyuin             = $totalNyuin             + $nyuin;
//                    $totalNyuinSum          = $totalNyuinSum          + $nyuinSum;
//                    $totalTaiin             = $totalTaiin             + $taiin;
//                    $totalTaiinSum          = $totalTaiinSum          + $taiinSum;



					//////////////////////
					//////// 対象月の1日が休日であれば固定で分母に１をいれているのでweekdayになるとその1日分が分母が大きくなるので
					//////// その分を減算する
					/////////////////////////
					//月初が休日かどうか判定する
					if ($indicatorData->is_holiday($targetYear, $targetMonth, "01")) 
					{
						//休日である
						
						//対象月の2日目のweekdayの日付を取得する
						for ($checkm=2; $checkm < 32; $checkm++) 
						{
							$check_day = sprintf('%02d', $checkm);
							
							//休日かどうか判定する
							if (!$indicatorData->is_holiday($targetYear, $targetMonth, $check_day)) 
							{
								if($check_day <= $targetDay)
								{
									//表示日付が月初が休日で、既にweekdayなので分母から減算する
									$dayCountNoHoliday--;
									break; 
								}
							}
						}
					}
					

					$patientCoordinate = sprintf('%s%d', $columnAlphaNameArr[4], $rowNum);
					
					$gairai          = $gokeiGa;
					$gairaiAvg       = $this->getExcelDivData($gokeiGaSum, $dayCountNoHoliday, 1);
					$patient         = $gokeiCount;
					$patientAvg      = $this->getExcelDivData($gokeiCountSum, $dayCount, 1);
					$emptyfloors     = $this->getExcelSubData($actCount, $patientCoordinate);
					$oRate           = $this->getExcelDivData($patientCoordinate, $actCount);
					$oRateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[8], $rowNum);
					$oRateAvg        = $this->getExcelDivData($gokeiCountSum, $actCountSum);
					$oRateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[10], $rowNum);
					$pRate           = $this->getExcelDivData($patientCoordinate, $pmtCount);
					$pRateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[12], $rowNum);
					$pRateAvg        = $this->getExcelDivData($gokeiCountSum, $pmtCountSum);
					$pRateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[14], $rowNum);
					$nyuin           = $this->getArrValueByKey('nyuin',     $dataVal);
					$nyuinSum        = $this->getArrValueByKey('sum_nyuin', $dataVal);
					$taiin           = $this->getArrValueByKey('taiin',     $dataVal);
					$taiinSum        = $this->getArrValueByKey('sum_taiin', $dataVal);
					
					$totalGokeiGa           = $totalGokeiGa           + $gokeiGa;
					$totalGokeiGaSum        = $totalGokeiGaSum        + $gokeiGaSum;
					$totalGokeiCount        = $totalGokeiCount        + $gokeiCount;
					$totalGokeiCountSum     = $totalGokeiCountSum     + $gokeiCountSum;
					$totalDayCount          = $totalDayCount          + $dayCount;
					$totalDayCountNoHoliday = $totalDayCountNoHoliday + $dayCountNoHoliday;
					$totalActCount          = $totalActCount          + $actCount;
					$totalActCountSum       = $totalActCountSum       + $actCountSum;
					$totalPmtCount          = $totalPmtCount          + $pmtCount;
					$totalPmtCountSum       = $totalPmtCountSum       + $pmtCountSum;
					$totalGairai            = $totalGairai            + $gairai;
					$totalPatient           = $totalPatient           + $patient;
					$totalNyuin             = $totalNyuin             + $nyuin;
					$totalNyuinSum          = $totalNyuinSum          + $nyuinSum;
					$totalTaiin             = $totalTaiin             + $taiin;
					$totalTaiinSum          = $totalTaiinSum          + $taiinSum;
					



                }
            }

//            $patientCoordinate = sprintf('%s%d', $columnAlphaNameArr[4], $rowNum);

//            $gairai          = $gokeiGa;
//            $gairaiAvg       = $this->getExcelDivData($gokeiGaSum, $dayCountNoHoliday, 1);
//            $patient         = $gokeiCount;
//            $patientAvg      = $this->getExcelDivData($gokeiCountSum, $dayCount, 1);
//            $emptyfloors     = $this->getExcelSubData($actCount, $patientCoordinate);
//            $oRate           = $this->getExcelDivData($patientCoordinate, $actCount);
//            $oRateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[8], $rowNum);
//            $oRateAvg        = $this->getExcelDivData($gokeiCountSum, $actCountSum);
//            $oRateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[10], $rowNum);
//            $pRate           = $this->getExcelDivData($patientCoordinate, $pmtCount);
//            $pRateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[12], $rowNum);
//            $pRateAvg        = $this->getExcelDivData($gokeiCountSum, $pmtCountSum);
//            $pRateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[14], $rowNum);
//            $nyuin           = $this->getArrValueByKey('nyuin',     $dataVal);
//            $nyuinSum        = $this->getArrValueByKey('sum_nyuin', $dataVal);
//            $taiin           = $this->getArrValueByKey('taiin',     $dataVal);
//            $taiinSum        = $this->getArrValueByKey('sum_taiin', $dataVal);

//            $totalGokeiGa           = $totalGokeiGa           + $gokeiGa;
//            $totalGokeiGaSum        = $totalGokeiGaSum        + $gokeiGaSum;
//            $totalGokeiCount        = $totalGokeiCount        + $gokeiCount;
//            $totalGokeiCountSum     = $totalGokeiCountSum     + $gokeiCountSum;
//            $totalDayCount          = $totalDayCount          + $dayCount;
//            $totalDayCountNoHoliday = $totalDayCountNoHoliday + $dayCountNoHoliday;
//            $totalActCount          = $totalActCount          + $actCount;
//            $totalActCountSum       = $totalActCountSum       + $actCountSum;
//            $totalPmtCount          = $totalPmtCount          + $pmtCount;
//            $totalPmtCountSum       = $totalPmtCountSum       + $pmtCountSum;
//            $totalGairai            = $totalGairai            + $gairai;
//            $totalPatient           = $totalPatient           + $patient;
//            $totalNyuin             = $totalNyuin             + $nyuin;
//            $totalNyuinSum          = $totalNyuinSum          + $nyuinSum;
//            $totalTaiin             = $totalTaiin             + $taiin;
//            $totalTaiinSum          = $totalTaiinSum          + $taiinSum;

            $totalStatus = $this->checkCellDataStatus('comp', array($totalStatus, $status));
            $tdArr = array(0 => array('data' => $plantVal['facility_name'], 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                         , 1 => array('data' => $gairai,         'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                         , 2 => array('data' => $gairaiAvg,       'style' => array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis))
                         , 3 => array('data' => $patient,         'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                         , 4 => array('data' => $patientAvg,      'style' => array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis))
                         , 5 => array('data' => $emptyfloors,     'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                         , 6 => array('data' => $oRateTrigona,    'style' => array('textAlign' => $textAlign_center))
                         , 7 => array('data' => $oRate,           'style' => array('msoNumberFormat' => $msoNumberFormat_percent))
                         , 8 => array('data' => $oRateAvgTrigona, 'style' => array('textAlign' => $textAlign_center))
                         , 9 => array('data' => $oRateAvg,        'style' => array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis))
                        , 10 => array('data' => $pRateTrigona,    'style' => array('textAlign' => $textAlign_center))
                        , 11 => array('data' => $pRate,           'style' => array('msoNumberFormat' => $msoNumberFormat_percent))
                        , 12 => array('data' => $pRateAvgTrigona, 'style' => array('textAlign' => $textAlign_center))
                        , 13 => array('data' => $pRateAvg,        'style' => array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis))
                        , 14 => array('data' => $nyuin,           'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                        , 15 => array('data' => $nyuinSum,        'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis))
                        , 16 => array('data' => $taiin,           'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                        , 17 => array('data' => $taiinSum,        'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis))
            );

            $rtnStr .= '<tr>'.$this->convertCellData($tdArr, $fontTag, array('textAlign' => $textAlign_right)).'</tr>';
        }

        $totalRowNum = $startRowNum + $plantCount;

        $totalPatientCoordinate = sprintf('%s%d', $columnAlphaNameArr[4], $totalRowNum);

        $totalGairai          = $this->getExcelLengthSumData($columnAlphaNameArr[2], $startRowNum, $totalRowNum-1);
		
		//全施設の平均を表示するのをやめて、全施設の合計を表示するように仕様変更しました
//        $totalGairaiAvg       = $this->getExcelDivData($totalGokeiGaSum, $totalDayCountNoHoliday, 1);
		$totalGairaiAvg       = $this->getExcelLengthSumData($columnAlphaNameArr[3], $startRowNum, $totalRowNum-1);
		
		$totalPatient         = $this->getExcelLengthSumData($columnAlphaNameArr[4], $startRowNum, $totalRowNum-1);

		//全施設の平均を表示するのをやめて、全施設の合計を表示するように仕様変更しました
        //$totalPatientAvg      = $this->getExcelDivData($totalGokeiCountSum, $totalDayCount, 1);
		$totalPatientAvg      = $this->getExcelLengthSumData($columnAlphaNameArr[5], $startRowNum, $totalRowNum-1);
		
		
		
        $totalEmptyfloors     = $this->getExcelSubData($totalActCount, $totalPatientCoordinate);
        $totalORate           = $this->getExcelDivData($totalPatientCoordinate, $totalActCount);
        $totalORateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[8], $totalRowNum);
        $totalORateAvg        = $this->getExcelDivData($totalGokeiCountSum, $totalActCountSum);
        $totalORateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[10], $totalRowNum);
        $totalPRate           = $this->getExcelDivData($totalPatientCoordinate, $totalPmtCount);
        $totalPRateTrigona    = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[12], $totalRowNum);
        $totalPRateAvg        = $this->getExcelDivData($totalGokeiCountSum, $totalPmtCountSum);
        $totalPRateAvgTrigona = $this->getExcelUtilizationRatesTrigona($columnAlphaNameArr[14], $totalRowNum);
        $totalNyuin           = $this->getExcelLengthSumData($columnAlphaNameArr[15], $startRowNum, $totalRowNum-1);
        $totalNyuinSum        = $this->getExcelLengthSumData($columnAlphaNameArr[16], $startRowNum, $totalRowNum-1);
        $totalTaiin           = $this->getExcelLengthSumData($columnAlphaNameArr[17], $startRowNum, $totalRowNum-1);
        $totalTaiinSum        = $this->getExcelLengthSumData($columnAlphaNameArr[18], $startRowNum, $totalRowNum-1);

        $totalTdArr = array(0 => array('data'  => '合計',               'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          , 1 => array('data' => $totalGairai,          'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                          , 2 => array('data' => $totalGairaiAvg,       'style' => array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis))
                          , 3 => array('data' => $totalPatient,         'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                          , 4 => array('data' => $totalPatientAvg,      'style' => array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis))
                          , 5 => array('data' => $totalEmptyfloors,     'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                          , 6 => array('data' => $totalORateTrigona,    'style' => array('textAlign' => $textAlign_center))
                          , 7 => array('data' => $totalORate,           'style' => array('msoNumberFormat' => $msoNumberFormat_percent))
                          , 8 => array('data' => $totalORateAvgTrigona, 'style' => array('textAlign' => $textAlign_center))
                          , 9 => array('data' => $totalORateAvg,        'style' => array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis))
                         , 10 => array('data' => $totalPRateTrigona,    'style' => array('textAlign' => $textAlign_center))
                         , 11 => array('data' => $totalPRate,           'style' => array('msoNumberFormat' => $msoNumberFormat_percent))
                         , 12 => array('data' => $totalPRateAvgTrigona, 'style' => array('textAlign' => $textAlign_center))
                         , 13 => array('data' => $totalPRateAvg,        'style' => array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis))
                         , 14 => array('data' => $totalNyuin,           'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                         , 15 => array('data' => $totalNyuinSum,        'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis))
                         , 16 => array('data' => $totalTaiin,           'style' => array('msoNumberFormat' => $msoNumberFormat_comma))
                         , 17 => array('data' => $totalTaiinSum,        'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis))
        );
        $rtnStr .= '<tr>'.$this->convertCellData($totalTdArr, $fontTag, array('textAlign' => $textAlign_right)).'</tr></table>';
        return $rtnStr;
    }

    /**
     * 月報データの前年度比較のエクセル出力
     * No.48 一般病棟平均在院日数
     * No.49 新患者数
     * No.50 紹介患者数
     * No.51 救急依頼件数
     * No.52 救急受入率
     * No.53 救急入院患者数
     * No.54 救急入院率
     * No.55 診療時間外患者数
     * No.56 時間外入院患者数
     * No.57 診療時間外入院率
     * No.58 紹介率
     * No.59 手術件数(全麻)
     * No.60 手術件数(腰麻)
     * No.61 手術件数(伝達・局麻)
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
	 * @param  array  $optionArr
     * @return string
     */
	function outputExcelMonthlyReportOfComparisonInThePreviousYear($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr     = $this->getExcelMeta();
        $startMonth = $this->getFiscalYearStartMonth();
        $fontTag    = $this->getFontTag();

        $totalNameStr   = $this->getArrayValueByKeyForEmptyIsNull('total',       $arr);
        $averageNameStr = $this->getArrayValueByKeyForEmptyIsNull('average',     $arr);
        $amgTotalFlg    = $this->getArrayValueByKeyForEmptyIsNull('amg_total',   $arr);
        $amgAverageFlg  = $this->getArrayValueByKeyForEmptyIsNull('amg_average', $arr);

        # attr
        $colspan_2 = array('name' => 'colspan', 'value' => '2');
        $rowspan_3 = array('name' => 'rowspan', 'value' => '3');
        $rowspan_4 = array('name' => 'rowspan', 'value' => '4');

        # style
        $textAlign_center        = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right         = array('name' => 'text-align',        'value' => 'right');
        $bgColor_blue            = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_green           = array('name' => 'background-color',  'value' => $this->getRgbColor('green'));
        $whiteSpace_nowrap       = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
//        $msoNumberFormat_percent = array('name' => 'mso-number-format', 'value' => '0.0%');
//        $msoNumberFormat_0_0     = array('name' => 'mso-number-format', 'value' => '0.0');

        //$currentYear = $this->getCurrentYear();
        //$lastYear    = $this->getLastYear();

		if($optionArr == null)
			{$currentYear = $this->getCurrentYear();}
		else
			{$currentYear	= $this->getArrValueByKey('year', $optionArr);}
		$lastYear		= $currentYear - 1;
		//$startMonth	= $this->getFiscalYearStartMonth();
		

        $monthArr = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)));
        for ($m=0; $m < 12; $m++) {
            $month = $startMonth + $m;
            $month = ($month <= 12)? $month : $month - 12;
            $monthArr[] = array('data' => $month.'月');
        }

        if (!is_null($totalNameStr))   { $monthArr[] = array('data' => '病院合計'); }
        if (!is_null($averageNameStr)) { $monthArr[] = array('data' => '病院平均'); }

        $header = '<table class="list this_table" border="1" width="100%" >'
                     .'<tr>'
                     .$this->convertCellData(
                         $monthArr,
                         $fontTag,
                         array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap)
                     )
                     .'</tr>';

        $rtnStr .= $header;

        $amgTotalDataArr        = array();
        $amgAverageDataArr      = array();
        $currentTotalMonthCount = 0;
        $lastTotalMonthCount    = 0;
        $currentPlantCount      = array();
        $lastPlantCount         = array();

        $plantCount = count($plantArr);

        $startRowNum        = 2;
        $startColumnNum     = 3;
        $columnAlphaNameArr = $this->columnAlphaNameArr;


        $sideTotalcolumnNum   = !is_null($totalNameStr)? $startColumnNum + 12 : null;
        $sideAverageColumnNum = !is_null($averageNameStr)? !is_null($totalNameStr)? $startColumnNum + 13 : $startColumnNum + 12 : null;
        $amgTotalRowNum       = ($amgTotalFlg)? $startRowNum + count($plantArr) * 4 : null;
        $amgAverageRowNum     = ($amgAverageFlg)? ($amgTotalFlg)? $startRowNum + (count($plantArr) + 1) * 4: $startRowNum + count($plantArr) * 4 : null;

        foreach ($plantArr as $plantKey => $plantVal) {
            $plantId = $this->getArrValueByKey('facility_id', $plantVal);
            $rowNum  = $startRowNum + $plantKey * 4;

            $currentDataArr = array();
            $lastDataArr    = array();
            $compDataArr    = array();
            $raitoDataArr   = array();

            $currentDataArr = array(
                0 => array(
                    'data'  => $this->getArrValueByKey('facility_name', $plantVal),
                    'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center),
                    'attr'  => array($rowspan_4)
                ),
                1 => array('data' => $currentYear .'年度', 'style' => array('textAlign' => $textAlign_center))
            );

            $lastDataArr  = array(0 => array('data' => $lastYear.'年度', 'style' => array('textAlign' => $textAlign_center)));
            $compDataArr  = array(0 => array('data' => '増減',           'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_green)));
            $raitoDataArr = array(0 => array('data' => '増減率',         'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_green)));

            $totalData     = array();
            $averageData   = array();

            $currentYearMonthCount = 0;
            $lastYearMonthCount    = 0;
            $checkData = false;

            for ($m=0; $m < 12; $m++) {

                $columnNum = $startColumnNum + $m;

                $currentCoordinate = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum);
                $lastCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum + 1);
                $compCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum + 2);
                $raitoCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $rowNum + 3);

                # 病院合計
                $currentTotalCoordinate = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $rowNum);
                $lastTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $rowNum + 1);
                $compTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $rowNum + 2);
                $raitoTotalCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $rowNum + 3);

                # 病院平均
                $currentAverageCoordinate = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $rowNum);
                $lastAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $rowNum + 1);
                $compAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $rowNum + 2);
                $raitoAverageCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $rowNum + 3);

                # AMG合計
                $amgTotalCurrentCoordinate = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgTotalRowNum);
                $amgTotalLastCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgTotalRowNum + 1);
                $amgTotalCompCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgTotalRowNum + 2);
                $amgTotalRaitoCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgTotalRowNum + 3);

                # AMG合計 病院合計
                $amgTotalCurrentSideTotalCoordinate = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgTotalRowNum);
                $amgTotalLastSideTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgTotalRowNum + 1);
                $amgTotalCompSideTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgTotalRowNum + 2);
                $amgTotalRaitoSideTotalCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgTotalRowNum + 3);

                # AMG合計 病院平均
                $amgTotalCurrentSideAverageCoordinate = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgTotalRowNum);
                $amgTotalLastSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgTotalRowNum + 1);
                $amgTotalCompSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgTotalRowNum + 2);
                $amgTotalRaitoSideAverageCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgTotalRowNum + 3);

                # AMG平均
                $amgAverageCurrentCoordinate = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgAverageRowNum);
                $amgAverageLastCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgAverageRowNum + 1);
                $amgAverageCompCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgAverageRowNum + 2);
                $amgAverageRaitoCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $amgAverageRowNum + 3);

                # AMG平均 病院合計
                $amgAverageCurrentSideTotalCoordinate = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgAverageRowNum);
                $amgAverageLastSideTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgAverageRowNum + 1);
                $amgAverageCompSideTotalCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgAverageRowNum + 2);
                $amgAverageRaitoSideTotalCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$sideTotalcolumnNum], $amgAverageRowNum + 3);

                # AMG平均 病院平均
                $amgAverageCurrentSideAverageCoordinate = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgAverageRowNum);
                $amgAverageLastSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgAverageRowNum + 1);
                $amgAverageCompSideAverageCoordinate    = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgAverageRowNum + 2);
                $amgAverageRaitoSideAverageCoordinate   = sprintf('%s%d', $columnAlphaNameArr[$sideAverageColumnNum], $amgAverageRowNum + 3);

                $targetMonth       = $m + $startMonth;
                $targetCurrentYear = ($targetMonth <= 12)? $currentYear : $currentYear + 1;
                $targetLastYear    = ($targetMonth <= 12)? $lastYear    : $lastYear    + 1;
                $targetMonth       = ($targetMonth <= 12)? $targetMonth : $targetMonth - 12;

                $currentData = 0;
                $lastData    = 0;
                $compData    = 0;
                $raitoData   = 0;

                foreach ($dataArr as $dataArrVal) {
                    if (sprintf('%02d', $targetMonth)==$this->getArrValueByKey('month', $dataArrVal) && $plantId == $this->getArrValueByKey('facility_id', $dataArrVal)) {
                        if (!is_null($this->getArrValueByKey($targetCurrentYear, $dataArrVal))) {
                            $currentData            = $currentData + $this->getArrValueByKey($targetCurrentYear, $dataArrVal);
                            $currentYearMonthCount  = $currentYearMonthCount  + 1;
                            $currentTotalMonthCount = $currentTotalMonthCount + 1;
                            $currentPlantCount[$m]  = $currentPlantCount[$m]  + 1;
                        }
                        if (!is_null($this->getArrValueByKey($targetLastYear, $dataArrVal))) {
                            $lastData            = $lastData + $this->getArrValueByKey($targetLastYear, $dataArrVal);
                            $lastYearMonthCount  = $lastYearMonthCount  + 1;
                            $lastTotalMonthCount = $lastTotalMonthCount + 1;
                            $lastPlantCount[$m]  = $lastPlantCount[$m]  + 1;
                        }
                    }
                }
                $compData  = $this->getExcelSubData($currentCoordinate, $lastCoordinate, 3);
                $raitoData = $this->getExcelDivData($compCoordinate, $lastCoordinate, 3);

                # 病院合計
                $totalData['current'] = array('data' => $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum));
                $totalData['last']    = array('data' => $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum + 1));
                $totalData['comp']    = array('data' => $this->getExcelSubData($currentTotalCoordinate, $lastTotalCoordinate, 3));
                $totalData['raito']   = array('data' => $this->getExcelDivData($compTotalCoordinate, $lastTotalCoordinate, 3));

                # 病院平均
                $averageData['current'] = array('data' => $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum, $currentYearMonthCount, 1));
                $averageData['last']    = array('data' => $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $rowNum + 1, $lastYearMonthCount, 1));
                $averageData['comp']    = array('data' => $this->getExcelSubData($currentAverageCoordinate, $lastAverageCoordinate, 3));
                $averageData['raito']   = array('data' => $this->getExcelDivData($compAverageCoordinate, $lastAverageCoordinate, 3));

                $currentAllArr = array();
                $lastAllArr = array();
                for ($i = 0; $i < $plantCount; $i++) {

					//AMG平均用の文字列を作成する
					if($i == 0)
					{
						$currentAllAve = sprintf('(SUM(%s%d)', $columnAlphaNameArr[$columnNum], $startRowNum + $i * 4);
						$lastAllAve    = sprintf('(SUM(%s%d)', $columnAlphaNameArr[$columnNum], $startRowNum + ($i * 4) + 1);
					}
					else
					{
						$currentAllAve .= sprintf(' + SUM(%s%d)', $columnAlphaNameArr[$columnNum], $startRowNum + $i * 4);
						$lastAllAve    .= sprintf(' + SUM(%s%d)', $columnAlphaNameArr[$columnNum], $startRowNum + ($i * 4) + 1);
						
					}
					
					$currentAllArr[] = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $startRowNum + $i * 4);
					$lastAllArr []   = sprintf('%s%d', $columnAlphaNameArr[$columnNum], $startRowNum + ($i * 4) + 1);
                }
				$currentAllAve .= ')';
				$lastAllAve    .=  ')';



                # AMG合計
                $amgTotalDataArr['current'][$m] = array('data' => $this->getExcelSumDataByArray($currentAllArr));
                $amgTotalDataArr['last'][$m]    = array('data' => $this->getExcelSumDataByArray($lastAllArr));
                $amgTotalDataArr['comp'][$m]    = array('data' => $this->getExcelSubData($amgTotalCurrentCoordinate, $amgTotalLastCoordinate, 3));
                $amgTotalDataArr['raito'][$m]   = array('data' => $this->getExcelDivData($amgTotalCompCoordinate, $amgTotalLastCoordinate, 3));

                # AMG平均
//                $amgAverageDataArr['current'][$m] = array('data' => $this->getExcelDivData(sprintf('SUM(%s)', implode(', ', $currentAllArr)), $currentPlantCount[$m], 1));
//                $amgAverageDataArr['last'][$m]    = array('data' => $this->getExcelDivData(sprintf('SUM(%s)', implode(', ', $lastAllArr)), $lastPlantCount[$m], 1));
				$amgAverageDataArr['current'][$m] = array('data' => $this->getExcelDivData($currentAllAve, $currentPlantCount[$m], 1));
				$amgAverageDataArr['last'][$m]    = array('data' => $this->getExcelDivData($lastAllAve, $lastPlantCount[$m], 1));
				
				$amgAverageDataArr['comp'][$m]    = array('data' => $this->getExcelSubData($amgAverageCurrentCoordinate, $amgAverageLastCoordinate, 3));
                $amgAverageDataArr['raito'][$m]   = array('data' => $this->getExcelDivData($amgAverageCompCoordinate, $amgAverageLastCoordinate, 3));

                # AMG合計 病院合計
                $amgTotalSideTotalData['current'] = array('data' => $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgTotalRowNum));
                $amgTotalSideTotalData['last']    = array('data' => $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgTotalRowNum+1));
                $amgTotalSideTotalData['comp']    = array('data' => $this->getExcelSubData($amgTotalCurrentSideTotalCoordinate, $amgTotalLastSideTotalCoordinate, 3));
                $amgTotalSideTotalData['raito']   = array('data' => $this->getExcelDivData($amgTotalCompSideTotalCoordinate, $amgTotalLastSideTotalCoordinate, 3));

                # AMG合計 病院平均
                $amgTotalSideAverageData['current'] = array('data' => $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgTotalRowNum, $this->getDenominatorOfAverageValueOfAverageValue($currentPlantCount), 1));
                $amgTotalSideAverageData['last']    = array('data' => $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgTotalRowNum + 1, $this->getDenominatorOfAverageValueOfAverageValue($lastPlantCount), 1));
                $amgTotalSideAverageData['comp']    = array('data' => $this->getExcelSubData($amgTotalCurrentSideAverageCoordinate, $amgTotalLastSideAverageCoordinate, 3));
                $amgTotalSideAverageData['raito']   = array('data' => $this->getExcelDivData($amgTotalCompSideAverageCoordinate, $amgTotalLastSideAverageCoordinate, 3));

                # AMG平均 病院合計
                $amgAverageSideTotalData_current = 0;
                $amgAverageSideTotalData_last    = 0;
                for ($i = 0; $i < 12; $i++) {
                    if ($this->getArrValueByKey($i, $amgAverageDataArr['current'])) { $amgAverageSideTotalData_current = bcadd($amgAverageSideTotalData_current, $amgAverageDataArr['current'][$i]['data']); }
                    if ($this->getArrValueByKey($i, $amgAverageDataArr['last']))    { $amgAverageSideTotalData_last    = bcadd($amgAverageSideTotalData_last,    $amgAverageDataArr['last'][$i]['data']);    }
                }
                $amgAverageSideTotalData['current'] = array('data' => $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgAverageRowNum));
                $amgAverageSideTotalData['last']    = array('data' => $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgAverageRowNum + 1));
                $amgAverageSideTotalData['comp']    = array('data' => $this->getExcelSubData($amgAverageCurrentSideTotalCoordinate, $amgAverageLastSideTotalCoordinate, 3));
                $amgAverageSideTotalData['raito']   = array('data' => $this->getExcelDivData($amgAverageCompSideTotalCoordinate, $amgAverageLastSideTotalCoordinate, 3));

                # AMG平均 病院平均
                $amgAverageSideAverageData['current'] = array('data' => $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgAverageRowNum, $this->getDenominatorOfAverageValueOfAverageValue($currentPlantCount), 1));
                $amgAverageSideAverageData['last']    = array('data' => $this->getExcelSideAverageData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$columnNum], $amgAverageRowNum + 1, $this->getDenominatorOfAverageValueOfAverageValue($lastPlantCount), 1));
                $amgAverageSideAverageData['comp']    = array('data' => $this->getExcelSubData($amgAverageCurrentSideAverageCoordinate, $amgAverageLastSideAverageCoordinate, 3));
                $amgAverageSideAverageData['raito']   = array('data' => $this->getExcelDivData($amgAverageCompSideAverageCoordinate, $amgAverageLastSideAverageCoordinate, 3));

				if (($arr["name"] == "一般病棟平均在院日数") || ($arr["name"] == "救急受入率") || ($arr["name"] == "救急入院率") || ($arr["name"] == "診療時間外入院率") || ($arr["name"] == "紹介率"))
				{
					$currentDataArr[] = array('data' => $currentData, 'disp_type' => 'float');
					$lastDataArr[]    = array('data' => $lastData, 'disp_type' => 'float');
					$compDataArr[]    = array('data' => $compData, 'style' => array('bgColor' => $bgColor_green));
					$raitoDataArr[]   = array('data' => empty($raitoData)? $raitoData : $raitoData, 'style' => array('bgColor' => $bgColor_green));
				}
				else
				{
					$currentDataArr[] = array('data' => $currentData);
					$lastDataArr[]    = array('data' => $lastData);
					$compDataArr[]    = array('data' => $compData, 'style' => array('bgColor' => $bgColor_green));
					$raitoDataArr[]   = array('data' => empty($raitoData)? $raitoData : $raitoData, 'style' => array('bgColor' => $bgColor_green));
				}

            }

            # 病院合計
            if (!is_null($totalNameStr)) {
                $currentDataArr[] = $totalData['current'];
                $lastDataArr[]    = $totalData['last'];
                $compDataArr[]    = $totalData['comp'];
                $raitoDataArr[]   = $totalData['raito'];
            }
            # 病院平均
            if (!is_null($averageNameStr)) {
                $currentDataArr[] = $averageData['current'];
                $lastDataArr[]    = $averageData['last'];
                $compDataArr[]    = $averageData['comp'];
                $raitoDataArr[]   = $averageData['raito'];
            }

            # AMG合計
            if ($amgTotalFlg) {
                # 病院合計
                if (!is_null($totalNameStr)) {
                    $amgTotalDataArr['current'][12] = $amgTotalSideTotalData['current'];
                    $amgTotalDataArr['last'][12]    = $amgTotalSideTotalData['last'];
                    $amgTotalDataArr['comp'][12]    = $amgTotalSideTotalData['comp'];
                    $amgTotalDataArr['raito'][12]   = $amgTotalSideTotalData['raito'];
                }
                # 病院平均
                if (!is_null($averageNameStr)) {
                    $amgTotalDataArr['current'][13] = $amgTotalSideAverageData['current'];
                    $amgTotalDataArr['last'][13]    = $amgTotalSideAverageData['last'];
                    $amgTotalDataArr['comp'][13]    = $amgTotalSideAverageData['comp'];
                    $amgTotalDataArr['raito'][13]   = $amgTotalSideAverageData['raito'];
                }
            }

            # AMG平均
            if ($amgAverageFlg) {
                # 病院合計
                if (!is_null($totalNameStr)) {
                    $amgAverageDataArr['current'][12] = $amgAverageSideTotalData['current'];
                    $amgAverageDataArr['last'][12]    = $amgAverageSideTotalData['last'];
                    $amgAverageDataArr['comp'][12]    = $amgAverageSideTotalData['comp'];
                    $amgAverageDataArr['raito'][12]   = $amgAverageSideTotalData['raito'];
                }
                # 病院平均
                if (!is_null($averageNameStr)) {
                    $amgAverageDataArr['current'][13] = $amgAverageSideAverageData['current'];
                    $amgAverageDataArr['last'][13]    = $amgAverageSideAverageData['last'];
                    $amgAverageDataArr['comp'][13]    = $amgAverageSideAverageData['comp'];
                    $amgAverageDataArr['raito'][13]   = $amgAverageSideAverageData['raito'];
                }
            }

            $plantHtml = '<tr>'.$this->convertCellData($currentDataArr, $fontTag, array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                        .'<tr>'.$this->convertCellData($lastDataArr,    $fontTag, array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                        .'<tr>'.$this->convertCellData($compDataArr,    $fontTag, array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>'
                        .'<tr>'.$this->convertCellData($raitoDataArr,   $fontTag, array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)).'</tr>';

            $rtnStr .= $plantHtml;
        }

        # AMG合計
        if ($amgTotalFlg) {
            $amgTotalHtml = '<tr>'
                               .$this->convertCellData(
                                   array(0 => array('data' => 'AMG合計', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue))
                                       , 1 => array('data' => $currentYear.'年度')
                                   ),
                                   $fontTag,
                                   array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                               )
                               .$this->convertCellData(
                                   $amgTotalDataArr['current'], $fontTag, array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                           .'</tr>'
                           .'<tr>'
                               .$this->convertCellData(
                                   array(0 => array('data' => $lastYear.'年度')),
                                   $fontTag,
                                   array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                               )
                               .$this->convertCellData(
                                   $amgTotalDataArr['last'],
                                   $fontTag,
                                   array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)
                               )
                           .'</tr>'
                           .'<tr>'
                               .$this->convertCellData(
                                   array(0 => array('data' => '増減')),
                                   $fontTag,
                                   array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                               )
                               .$this->convertCellData(
                                   $amgTotalDataArr['comp'],
                                   $fontTag,
                                   array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                               )
                            .'</tr>'
                           .'<tr>'
                               .$this->convertCellData(
                                   array(0 => array('data' => '増減率')),
                                   $fontTag,
                                   array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                               )
                               .$this->convertCellData(
                                   $amgTotalDataArr['raito'],
                                   $fontTag,
                                   array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                               )
                           .'</tr>';
            $rtnStr .= $amgTotalHtml;
        }

        # AMG平均
        if ($amgAverageFlg) {
            $amgAverageHtml = '<tr>'
                                 .$this->convertCellData(
                                     array(0 => array('data' => 'AMG平均', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue))
                                         , 1 => array('data' => $currentYear.'年度')
                                     ),
                                     $fontTag,
                                     array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                                 )
                                 .$this->convertCellData(
                                     $amgAverageDataArr['current'],
                                     $fontTag,
                                     array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)
                                 )
                             .'</tr>'
                             .'<tr>'
                                 .$this->convertCellData(
                                     array(0 => array('data' => $lastYear.'年度')),
                                     $fontTag,
                                     array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                                 )
                                 .$this->convertCellData(
                                     $amgAverageDataArr['last'],
                                     $fontTag,
                                     array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)
                                 )
                             .'</tr>'
                             .'<tr>'
                                 .$this->convertCellData(
                                     array(0 => array('data' => '増減')),
                                     $fontTag,
                                     array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                                 )
                                 .$this->convertCellData(
                                     $amgAverageDataArr['comp'],
                                     $fontTag,
                                     array('msoNumberFormat' => $msoNumberFormat_0_0, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                                 )
                             .'</tr>'
                             .'<tr>'
                                 .$this->convertCellData(
                                     array(0 => array('data' => '増減率')),
                                     $fontTag,
                                     array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                                 )
                                 .$this->convertCellData(
                                     $amgAverageDataArr['raito'],
                                     $fontTag,
                                     array('msoNumberFormat' => $msoNumberFormat_percent, 'textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_green)
                                 )
                             .'</tr>';
            $rtnStr .= $amgAverageHtml;
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.71 病院月間報告書患者数総括のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelSummaryOfNumberOfHospitalMonthlyReportPatients($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $displayMonth = $this->getDisplayMonth();
        $startMonth   = $this->getFiscalYearStartMonth();
        $cYear        = $this->getCurrentYear();
        $cYear        = ($displayMonth < $startMonth)? $cYear + 1 : $cYear;

        $indicatorData = $this->getIndicatorData();
        $cDayCount     = $indicatorData->getDayCountOfMonth($cYear, $displayMonth);

		//表示月が現在月より過去の場合はMAXの日曜・祝日日数を設定する
		//表示月と現在月が同じ場合は現在日までの日曜・祝日日数を設定する
		$holi_cnt = $indicatorData->getHolidayCount($cYear,$displayMonth);
		
        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_8  = array('name' => 'rowspan', 'value' =>  8);
        $rowspan_9  = array('name' => 'rowspan', 'value' =>  9);
        $rowspan_10 = array('name' => 'rowspan', 'value' => 10);
        $rowspan_13 = array('name' => 'rowspan', 'value' => 13);
        $rowspan_14 = array('name' => 'rowspan', 'value' => 14);
        $rowspan_19 = array('name' => 'rowspan', 'value' => 19);
        $rowspan_28 = array('name' => 'rowspan', 'value' => 28);
        $rowspan_65 = array('name' => 'rowspan', 'value' => 65);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_5  = array('name' => 'colspan', 'value' =>  5);

        # style
        $bgColor_weekday         = array('name' => 'background-color',  'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday        = array('name' => 'background-color',  'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday         = array('name' => 'background-color',  'value' => $this->getRgbColor('lightpink'));
        $bgColor_blue            = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $textAlign_center        = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right         = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap       = array('name' => 'white-space',       'value' => 'nowrap');

        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');
        $msoNumberFormat_unit    = $this->getMsoNumberFormat('unit');

        $header = '<table width="100%" border="0" >'
                     .'<tr>'
                         .'<td style="text-align: left;" >'
                             .'<table class="list" width="25%" border="1" >'
                                 .'<tr>'
                                     .$this->convertCellData(
                                         array(0 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday))
                                             , 1 => array('data' => $cDayCount['weekday'].'日')
                                             , 2 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday))
                                             , 3 => array('data' => $cDayCount['saturday'].'日')
                                             , 4 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday))
                                             , 5 => array('data' => $cDayCount['holiday'].'日')
                                         ),
                                         $fontTag,
                                         array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                                     )
                                 .'</tr>'
                             .'</table>'
                         .'</td>'
                         .'<td style="text-align: right;" >' .sprintf($fontTag, sprintf('%04d年%02d月', $cYear, $displayMonth)). '</td>'
                     .'</tr>'
                 .'</table>';

        $rtnStr .= $header;
        $rtnStr .= '<table class="list" width="100%" border="1" ><tr>';

        $plantHeaderArr[] = array('data' => '&nbsp;', 'attr' => array($colspan_5));
        foreach ($plantArr as $plantVal) { $plantHeaderArr[] = array('data' => $this->getArrValueByKey('facility_name', $plantVal)); }
        $plantHeaderArr[] = array('data' => '合計');
        $rtnStr .= $this->convertCellData($plantHeaderArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)).'</tr>';

        # 外来
        $leftSide[1] = array(0 => array('data' => '外来',     'attr' => array($rowspan_2))
                           , 1 => array('data' => '外来合計', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[2] = array(0 => array('data' => '1日平均',  'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));

        # 入院
        $leftSide[3] = array(0 => array('data' => '入院',     'attr' => array($rowspan_65))
        # 入院 全病棟
                           , 1 => array('data' => '全病棟',   'attr' => array($rowspan_6))
                           , 2 => array('data' => '入院合計', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[4] = array(0 => array('data' => '1日平均',        'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[5] = array(0 => array('data' => '定床数',         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[6] = array(0 => array('data' => '空床数',         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[7] = array(0 => array('data' => '稼働病床稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[8] = array(0 => array('data' => '許可病床稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 一般病棟
        $leftSide[9] = array(0 => array('data' => '一般病棟',       'attr' => array($rowspan_13))
                           , 1 => array('data' => '延べ患者数',     'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[10] = array(0 => array('data' => '定床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[11] = array(0 => array('data' => '空床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[12] = array(0 => array('data' => '稼働率',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[13] = array(0 => array('data' => '平均在院日数(直近3ヶ月)',        'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[14] = array(0 => array('data' => '新入院患者数',                   'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[15] = array(0 => array('data' => 'DPC対象延べ患者数',              'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[16] = array(0 => array('data' => '重症度・看護必要度割合',         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[17] = array(0 => array('data' => '1日〜14日延べ患者数',            'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[18] = array(0 => array('data' => '15日〜30日延べ患者数',           'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[19] = array(0 => array('data' => '救急医療管理加算算定延べ数',     'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[20] = array(0 => array('data' => '90日超延べ患者数',               'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[21] = array(0 => array('data' => '上記の内特定入院対象延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 障害者病棟
        $leftSide[22] = array(0 => array('data' => '障害者病棟',                     'attr' => array($rowspan_8))
                            , 1 => array('data' => '延べ患者数',                     'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[23] = array(0 => array('data' => '定床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[24] = array(0 => array('data' => '空床数',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[25] = array(0 => array('data' => '稼働率',                         'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[26] = array(0 => array('data' => '1日〜14日延べ患者数',            'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[27] = array(0 => array('data' => '15日〜30日延べ患者数',           'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[28] = array(0 => array('data' => '90日超延べ患者数',               'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[29] = array(0 => array('data' => '上記の内特定入院対象延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        # 入院 精神一般病
        $leftSide[30] = array(0 => array('data' => '精神一般病棟', 'attr' => array($rowspan_4))
                            , 1 => array('data' => '延べ患者数',   'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[31] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[32] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[33] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟
        $leftSide[34] = array(0 => array('data' => '療養病棟',   'attr' => array($rowspan_28))
        # 入院 療養病棟 医療
                            , 1 => array('data' => '医療',       'attr' => array($rowspan_14))
                            , 2 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[35] = array(0 => array('data' => '定床数',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[36] = array(0 => array('data' => '1日平均',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[37] = array(0 => array('data' => '稼働率',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟 医療 入院基本料
        $leftSide[38] = array(0 => array('data' => '入院基本料', 'attr' => array($rowspan_9))
                            , 1 => array('data' => 'A', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[39] = array(0 => array('data' => 'B', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[40] = array(0 => array('data' => 'C', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[41] = array(0 => array('data' => 'D', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[42] = array(0 => array('data' => 'E', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[43] = array(0 => array('data' => 'F', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[44] = array(0 => array('data' => 'G', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[45] = array(0 => array('data' => 'H', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[46] = array(0 => array('data' => 'I', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[47] = array(0 => array('data' => '1日〜14日延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟 介護
        $leftSide[48] = array(0 => array('data' => '介護', 'attr' => array($rowspan_10))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[49] = array(0 => array('data' => '定床数',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[50] = array(0 => array('data' => '1日平均',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[51] = array(0 => array('data' => '稼働率',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[52] = array(0 => array('data' => '要介護5',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[53] = array(0 => array('data' => '要介護4',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[54] = array(0 => array('data' => '要介護3',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[55] = array(0 => array('data' => '要介護2',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[56] = array(0 => array('data' => '要介護1',    'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[57] = array(0 => array('data' => '平均介護度', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟 精神療養
        $leftSide[58] = array(0 => array('data' => '精神療養',   'attr' => array($rowspan_4))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[59] = array(0 => array('data' => '定床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[60] = array(0 => array('data' => '空床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[61] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 特定病棟
        $leftSide[62] = array(0 => array('data' => '特定病棟',   'attr' => array($rowspan_6))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[63] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[64] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[65] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[66] = array(0 => array('data' => '回復期リハ', 'attr' => array($rowspan_2), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '重症患者の割合', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[67] = array(0 => array('data' => '在宅復帰率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[68] = array(0 => array('data' => '重点項目', 'attr' => array($rowspan_19), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(1),  'style' => array('textAlign' => $textAlign_center))
                            , 2 => array('data' => '新患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[69] = array(
            0 => array('data' => $indicatorData->sc(2), 'style' => array('textAlign' => $textAlign_center)),
            1 => array('data' => '初診患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[70] = array(
            0 => array('data' => $indicatorData->sc(3), 'style' => array('textAlign' => $textAlign_center)),
            1 => array('data' => '紹介患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[71] = array(
            0 => array('data' => $indicatorData->sc(4), 'style' => array('textAlign' => $textAlign_center)),
            1 => array('data' => '救急依頼件数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[72] = array(
            0 => array('data' => $indicatorData->sc(5), 'style' => array('textAlign' => $textAlign_center)),
            1 => array('data' => $indicatorData->sc(4).'の内、救急受入れ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[73] = array(
            0 => array('data' => $indicatorData->sc(6), 'style' => array('textAlign' => $textAlign_center)),
            1 => array('data' => $indicatorData->sc(5).'の内、救急入院患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[74] = array(
            0 => array('data' => $indicatorData->sc(7), 'style' => array('textAlign' => $textAlign_center)),
            1 => array('data' => '診療時間外患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[75] = array(
            0 => array('data' => $indicatorData->sc(8), 'style' => array('textAlign' => $textAlign_center)),
            1 => array('data' => $indicatorData->sc(7).'の内、時間外入院患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[76] = array(0 => array('data' => '紹介率',       'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[77] = array(0 => array('data' => '救急受入率',   'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[78] = array(0 => array('data' => '救急入院率',   'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[79] = array(0 => array('data' => '時間外入院率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[80] = array(0 => array('data' => $indicatorData->sc(9), 'style' => array('textAlign' => $textAlign_center)),
                              1 => array('data' => 'リハビリ平均提供単位数(一般)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[81] = array(0 => array('data' => $indicatorData->sc(10), 'style' => array('textAlign' => $textAlign_center)),
                              1 => array('data' => 'リハビリ平均提供単位数(療養・精神)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[82] = array(0 => array('data' => $indicatorData->sc(11), 'style' => array('textAlign' => $textAlign_center)),
                              1 => array('data' => 'リハビリ平均提供単位(回復期リハ)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[83] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[84] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[85] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[86] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));

        $RowCnt = 86;

        $mainData    = array();
        $totalData   = array();
        $monthlyData = $this->getArrValueByKey('monthly_data', $dataArr);
        $dailyData   = $this->getArrValueByKey('daily_data',   $dataArr);

        $startRowNum = 3;
        $startColumnNum = 6;
        $totalColumnNum = $startColumnNum + count($plantArr);
        $columnAlphaNameArr = $this->columnAlphaNameArr;
        $rowNum = $startRowNum-1;

		//「入院 一般病棟 平均在院日数(直近3ヶ月)」用の一般病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt1 = 0;
		
		//「入院 一般病棟 重症度・看護必要度割合」用の一般病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt2 = 0;

		//「入院 特殊病棟 重症度の割合」用の特殊病棟病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt3 = 0;
		
		//「入院 特殊病棟 在宅復帰率」用の特殊病棟病棟がある施設数を設定（平均値の分母にします）
		$denominator_cnt4 = 0;
		

        foreach ($plantArr as $plantKey => $plantVal) {
            $facilityId = $this->getArrValueByKey('facility_id', $plantVal);
            $colNum = $startColumnNum + $plantKey;


            for ($m=1; $m <= $RowCnt; $m++) { $mainData[$m][$plantKey] = array('data' => '&nbsp;'); }

            if (is_array($dailyData)) {
                foreach ($dailyData as $dailyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dailyVal) && 'current' == $this->getArrValueByKey('year_type', $dailyVal)) {
                        $gokeiGa      = $dailyVal['gokei_ga'];
                        $dayCount     = $dailyVal['day_count'];
                        $gokeiIp      = $dailyVal['gokei_ip'];
                        $gokeiShog    = $dailyVal['gokei_shog'];
                        $gokeiSeip    = $dailyVal['gokei_seip'];
                        $gokeiIr      = $dailyVal['gokei_ir'];
                        $gokeiKaig    = $dailyVal['gokei_kaig'];
                        $gokeiKaif    = $dailyVal['gokei_kaif'];
                        $gokeiAk      = $dailyVal['gokei_ak'];
                        $gokeiKan     = $dailyVal['gokei_kan'];
                        $gokeiIcu     = $dailyVal['gokei_icu'];
                        $gokeiShoni   = $dailyVal['gokei_shoni'];
                        $gokeiSeir    = $dailyVal['gokei_seir'];
                        $gokeiTok     = $dailyVal['gokei_tok'];
                        $gokeiNin     = $dailyVal['gokei_nin'];
                        $actCount     = $dailyVal['act_count'];
                        $pmtCount     = $dailyVal['pmt_count'];
                        $ipAct        = $dailyVal['ip_act'];
                        $shogAct      = $dailyVal['shog_act'];
                        $seipAct      = $dailyVal['seip_act'];
                        $irAct        = $dailyVal['ir_act'];
                        $kaigAct      = $dailyVal['kaig_act'];
                        $seirAct      = $dailyVal['seir_act'];
                        $sumGokei     = $gokeiIp + $gokeiShog + $gokeiSeip + $gokeiIr + $gokeiKaig + $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiSeir + $gokeiTok + $gokeiNin;
                        $gokeiTokutei = $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiTok + $gokeiNin;
                        $tokuteiAct   = $dailyVal['kaif_act'] + $dailyVal['ak_act'] + $dailyVal['kan_act'] + $dailyVal['icu_act'] + $dailyVal['shoni_act'] + $dailyVal['tok_act'] + $dailyVal['nin_act'];

                        # 外来
                        $mainData[1][$plantKey] = array('data' => $gokeiGa);
                        $mainData[2][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+1), ($dayCount - $holi_cnt), 3), 'day_count' => $dayCount);

                        # 入院 全病棟
                        $mainData[3][$plantKey] = array('data' => $sumGokei);
                        $mainData[4][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+3), $dayCount, 3), 'day_count' => $dayCount);
                        $mainData[5][$plantKey] = array('data' => $actCount);
                        $mainData[6][$plantKey] = array('data' => $this->getExcelSubData($this->coord($columnAlphaNameArr[$colNum], $rowNum+5), $this->coord($columnAlphaNameArr[$colNum], $rowNum+3), 1));
                        $mainData[7][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+3), $this->coord($columnAlphaNameArr[$colNum], $rowNum+5), 3));
                        $mainData[8][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+3), $pmtCount, 3), 'pmt_count' => $pmtCount);

                        # 入院 一般病棟
                        $mainData[9][$plantKey]  = array('data' => $gokeiIp);
                        $mainData[10][$plantKey] = array('data' => $ipAct);
                        $mainData[11][$plantKey] = array('data' => $this->getExcelSubData($this->coord($columnAlphaNameArr[$colNum], $rowNum+10), $this->coord($columnAlphaNameArr[$colNum], $rowNum+9), 1));
                        $mainData[12][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+9), $this->coord($columnAlphaNameArr[$colNum], $rowNum+10), 3));

                        # 入院 障害者病棟
                        $mainData[22][$plantKey] = array('data' => $gokeiShog);
                        $mainData[23][$plantKey] = array('data' => $shogAct);
                        $mainData[24][$plantKey] = array('data' => $this->getExcelSubData($this->coord($columnAlphaNameArr[$colNum], $rowNum+23), $this->coord($columnAlphaNameArr[$colNum], $rowNum+22), 1), 'shog_act' => $shogAct, 'gokei_shog' => $gokeiShog);
                        $mainData[25][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+22), $this->coord($columnAlphaNameArr[$colNum], $rowNum+23), 3), 'shog_act' => $shogAct, 'gokei_shog' => $gokeiShog);

                        # 入院 精神一般
                        $mainData[30][$plantKey] = array('data' => $gokeiSeip);
                        $mainData[31][$plantKey] = array('data' => $seipAct);
                        $mainData[32][$plantKey] = array('data' => $this->getExcelSubData($this->coord($columnAlphaNameArr[$colNum], $rowNum+31), $this->coord($columnAlphaNameArr[$colNum], $rowNum+30), 1), 'seip_act' => $seipAct, 'gokei_seip' => $gokeiSeip);
                        $mainData[33][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+30), $this->coord($columnAlphaNameArr[$colNum], $rowNum+31), 3), 'seip_act' => $seipAct, 'gokei_seip' => $gokeiSeip);

                        # 入院 療養病棟 医療適用
                        $mainData[34][$plantKey] = array('data' => $gokeiIr);
                        $mainData[35][$plantKey] = array('data' => $irAct);
                        $mainData[36][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+34), $dayCount, 1), 'day_count' => $dayCount, 'gokei_ir' => $gokeiIr);
                        $mainData[37][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+34), $this->coord($columnAlphaNameArr[$colNum], $rowNum+35), 3), 'ir_act' => $irAct,    'gokei_ir' => $gokeiIr);

                        # 入院 療養病棟 介護適用
                        $mainData[48][$plantKey] = array('data' => $gokeiKaig);
                        $mainData[49][$plantKey] = array('data' => $kaigAct);
                        $mainData[50][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+48), $dayCount, 3), 'day_count' => $dayCount,   'gokei_kaig' => $gokeiKaig);
                        $mainData[51][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+48), $this->coord($columnAlphaNameArr[$colNum], $rowNum+49), 3), 'kaig_act' => $kaigAct, 'gokei_kaig' => $gokeiKaig);

                        # 入院 療養病棟 精神療養
                        $mainData[58][$plantKey] = array('data' => $gokeiSeir);
                        $mainData[59][$plantKey] = array('data' => $seirAct);
                        $mainData[60][$plantKey] = array('data' => $this->getExcelSubData($this->coord($columnAlphaNameArr[$colNum], $rowNum+59), $this->coord($columnAlphaNameArr[$colNum], $rowNum+58), 1), 'seir_act' => $seirAct, 'gokei_seir' => $gokeiSeir);
                        $mainData[61][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+58), $this->coord($columnAlphaNameArr[$colNum], $rowNum+59), 3), 'seir_act' => $seirAct, 'gokei_seir' => $gokeiSeir);

                        # 入院 特定病棟
                        $mainData[62][$plantKey] = array('data' => $gokeiTokutei);
                        $mainData[63][$plantKey] = array('data' => $tokuteiAct);
                        $mainData[64][$plantKey] = array('data' => $this->getExcelSubData($this->coord($columnAlphaNameArr[$colNum], $rowNum+63), $this->coord($columnAlphaNameArr[$colNum], $rowNum+62), 1), 'tokutei_act' => $tokuteiAct, 'gokei_tokutei' => $gokeiTokutei);
                        $mainData[65][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+62), $this->coord($columnAlphaNameArr[$colNum], $rowNum+63), 3), 'tokutei_act' => $tokuteiAct, 'gokei_tokutei' => $gokeiTokutei);
                    }
                }
            }

            if (is_array($monthlyData)) {
                foreach ($monthlyData as $monthlyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $monthlyVal)) {
                        $dispNum322 = (float)$monthlyVal['dispnum_322'];
                        $dispNum323 = (float)$monthlyVal['dispnum_323'];
                        $dispNum324 = (float)$monthlyVal['dispnum_324'];
                        $dispNum325 = (float)$monthlyVal['dispnum_325'];
                        $dispNum326 = (float)$monthlyVal['dispnum_326'];

                        $dispNum328 = (float)$monthlyVal['dispnum_328'];
                        $dispNum329 = (float)$monthlyVal['dispnum_329'];
                        $dispNum330 = (float)$monthlyVal['dispnum_330'];
                        $dispNum331 = (float)$monthlyVal['dispnum_331'];
                        $dispNum332 = (float)$monthlyVal['dispnum_332'];
						$dispNum333 = (float)$monthlyVal['dispnum_333'];
						$dispNum334 = (float)$monthlyVal['dispnum_334'];

						# 入院 一般病棟 
                        //$mainData[13][$plantKey] = array('data' => $monthlyVal['dispnum_297']);
						
						if(($monthlyVal['dispnum_297'] !=null) && ($monthlyVal['dispnum_297'] !=0))
						{
							//一般病棟がある施設です
							$denominator_cnt1++;
							
							$mainData[13][$plantKey] = array('data' => $this->getExcelDivData($monthlyVal['dispnum_297'] * 1 , 1));
						}
						
						if(($monthlyVal['dispnum_300'] !=null) && ($monthlyVal['dispnum_300'] !=0))
						{
							//一般病棟がある施設です
							$denominator_cnt2++;
							
							$mainData[16][$plantKey] = array('data' => $this->getExcelDivData(($monthlyVal['dispnum_300'] / 100) , 1));
						}
						
						$mainData[14][$plantKey] = array('data' => $monthlyVal['dispnum_298']);
                        $mainData[15][$plantKey] = array('data' => $monthlyVal['dispnum_299']);

						$mainData[17][$plantKey] = array('data' => $monthlyVal['dispnum_301']);
                        $mainData[18][$plantKey] = array('data' => $monthlyVal['dispnum_302']);
                        $mainData[19][$plantKey] = array('data' => $monthlyVal['dispnum_303']);
                        $mainData[20][$plantKey] = array('data' => $monthlyVal['dispnum_304']);
                        $mainData[21][$plantKey] = array('data' => $monthlyVal['dispnum_305']);

                        # 入院 障害者病棟
                        $mainData[26][$plantKey] = array('data' => $monthlyVal['dispnum_306']);
                        $mainData[27][$plantKey] = array('data' => $monthlyVal['dispnum_307']);
                        $mainData[28][$plantKey] = array('data' => $monthlyVal['dispnum_308']);
                        $mainData[29][$plantKey] = array('data' => $monthlyVal['dispnum_309']);

                        # 入院 療養病棟 医療
                        $mainData[38][$plantKey] = array('data' => $monthlyVal['dispnum_312']);
                        $mainData[39][$plantKey] = array('data' => $monthlyVal['dispnum_313']);
                        $mainData[40][$plantKey] = array('data' => $monthlyVal['dispnum_314']);
                        $mainData[41][$plantKey] = array('data' => $monthlyVal['dispnum_315']);
                        $mainData[42][$plantKey] = array('data' => $monthlyVal['dispnum_316']);
                        $mainData[43][$plantKey] = array('data' => $monthlyVal['dispnum_317']);
                        $mainData[44][$plantKey] = array('data' => $monthlyVal['dispnum_318']);
                        $mainData[45][$plantKey] = array('data' => $monthlyVal['dispnum_319']);
                        $mainData[46][$plantKey] = array('data' => $monthlyVal['dispnum_320']);
                        $mainData[47][$plantKey] = array('data' => $monthlyVal['dispnum_321']);

                        # 入院 療養病棟 介護
                        $mainData[52][$plantKey] = array('data' => $dispNum322);
                        $mainData[53][$plantKey] = array('data' => $dispNum323);
                        $mainData[54][$plantKey] = array('data' => $dispNum324);
                        $mainData[55][$plantKey] = array('data' => $dispNum325);
                        $mainData[56][$plantKey] = array('data' => $dispNum326);

                        $mainData57 = $this->getExcelDivData(
                            sprintf(
                                '(%s*5+%s*4+%s*3+%s*2+%s*1)', $this->coord($columnAlphaNameArr[$colNum], $rowNum+52), $this->coord($columnAlphaNameArr[$colNum], $rowNum+53),
                                $this->coord($columnAlphaNameArr[$colNum], $rowNum+54), $this->coord($columnAlphaNameArr[$colNum], $rowNum+55), $this->coord($columnAlphaNameArr[$colNum], $rowNum+56)
                            ),
                            sprintf('(%s+%s+%s+%s+%s)', $this->coord($columnAlphaNameArr[$colNum], $rowNum+52), $this->coord($columnAlphaNameArr[$colNum], $rowNum+53),
                                $this->coord($columnAlphaNameArr[$colNum], $rowNum+54), $this->coord($columnAlphaNameArr[$colNum], $rowNum+55), $this->coord($columnAlphaNameArr[$colNum], $rowNum+56)
                            ),
                            3
                        );

                        $mainData[57][$plantKey] = array('data' => $mainData57, 'dn322' => $dispNum322, 'dn323' => $dispNum323, 'dn324' => $dispNum324, 'dn325' => $dispNum325, 'dn326' => $dispNum326, 'status' => $status);

						if(($monthlyVal['dispnum_310'] !=null) && ($monthlyVal['dispnum_310'] !=0))
						{
							//特殊病棟がある施設です
							$denominator_cnt3++;
							
							# 入院 特定病棟 回復期リハ
							//$mainData[66][$plantKey] = array('data' => $monthlyVal['dispnum_310']/100);
							$mainData[66][$plantKey] = array('data' => $this->getExcelDivData(($monthlyVal['dispnum_310'] / 100) , 1));
						}
						
						if(($monthlyVal['dispnum_311'] !=null) && ($monthlyVal['dispnum_311'] !=0))
						{
							//特殊病棟がある施設です
							$denominator_cnt4++;
							
							# 入院 特定病棟 回復期リハ
							//$mainData[67][$plantKey] = array('data' => $monthlyVal['dispnum_311']/100);
							$mainData[67][$plantKey] = array('data' => $this->getExcelDivData(($monthlyVal['dispnum_311'] / 100) , 1));
							//$mainData[16][$plantKey] = array('data' => $this->getExcelDivData(($monthlyVal['dispnum_300'] / 100) , 1));
						}
						
						
                        # 重点項目
                        $mainData[68][$plantKey] = array('data' => $monthlyVal['dispnum_327']);
                        $mainData[69][$plantKey] = array('data' => $dispNum328);
                        $mainData[70][$plantKey] = array('data' => $dispNum329);
                        $mainData[71][$plantKey] = array('data' => $dispNum330);
                        $mainData[72][$plantKey] = array('data' => $dispNum331);
                        $mainData[73][$plantKey] = array('data' => $dispNum332);
                        $mainData[74][$plantKey] = array('data' => $monthlyVal['dispnum_333']);
                        $mainData[75][$plantKey] = array('data' => $dispNum334);

                        # 重点項目 紹介率
                        $mainData[76][$plantKey] = array(
                            'data' => $this->getExcelDivData(sprintf('(%s+%s)', $this->coord($columnAlphaNameArr[$colNum], $rowNum+70), $this->coord($columnAlphaNameArr[$colNum], $rowNum+72)), $this->coord($columnAlphaNameArr[$colNum], $rowNum+69), 3),
                            'dn328' => $dispNum328, 'dn329' => $dispNum329, 'dn331' => $dispNum331
                        );

                        $mainData[77][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+72), $this->coord($columnAlphaNameArr[$colNum], $rowNum+71), 3), 'dn330' => $dispNum330, 'dn331' => $dispNum331);
                        $mainData[78][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+73), $this->coord($columnAlphaNameArr[$colNum], $rowNum+72), 3), 'dn332' => $dispNum332, 'dn331' => $dispNum331);
                        $mainData[79][$plantKey] = array('data' => $this->getExcelDivData($this->coord($columnAlphaNameArr[$colNum], $rowNum+75), $this->coord($columnAlphaNameArr[$colNum], $rowNum+74), 3), 'dn333' => $dispNum333, 'dn334' => $dispNum334);

						$mainData[80][$plantKey] = array('data' => $monthlyVal['dispnum_335'], 'disp_type' => 'float');//�� リハビリ平均提供単位数(一般) 
						$mainData[81][$plantKey] = array('data' => $monthlyVal['dispnum_336'], 'disp_type' => 'float');//�� リハビリ平均提供単位数(療養・精神) 
						$mainData[82][$plantKey] = array('data' => $monthlyVal['dispnum_337'], 'disp_type' => 'float');//�� リハビリ平均提供単位(回復期リハ) 
                        $mainData[83][$plantKey] = array('data' => $monthlyVal['dispnum_338']);
                        $mainData[84][$plantKey] = array('data' => $monthlyVal['dispnum_339']);
                        $mainData[85][$plantKey] = array('data' => $monthlyVal['dispnum_340']);
                        $mainData[86][$plantKey] = array('data' => $monthlyVal['dispnum_341']);
						//$mainData[86][$plantKey] = array('data' => $this->getExcelDivData(($monthlyVal['dispnum_341'] / 100) , 1));
						
                    }
                }
            }

            for ($t=1; $t <= $RowCnt; $t++) {
                switch ($t) {
                    case '2': # 外来 1日平均
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
//                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+1), $totalData[$t][0]['day_count'], 3);
						$totalData[$t][0]['data'] = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$totalColumnNum-1], $rowNum+$t);
						break;

                    case '4': # 入院 全病棟 1日平均
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
//                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+3), $totalData[$t][0]['day_count'], 3);
						$totalData[$t][0]['data'] = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$totalColumnNum-1], $rowNum+$t);
						break;

                    case '6': # 入院 全病棟 空床数
                        $totalData[$t][0]['data']      = $this->getExcelSubData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+5), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+3), 1);
                        break;

                    case '7': # 入院 全病棟 稼働病床稼働率
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+3), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+5), 3);
                        break;

                    case '8': # 入院 全病棟 許可病床稼働率
                        $totalData[$t][0]['pmt_count'] = $totalData[$t][0]['pmt_count'] + $mainData[$t][$plantKey]['pmt_count'];
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+3), $totalData[$t][0]['pmt_count'], 3);
                        break;

                    case '11': # 入院 一般病棟 空床数
                        $totalData[$t][0]['data']      = $this->getExcelSubData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+10), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+9), 1);
                        break;

                    case '12': # 入院 一般病棟 稼働率
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+9), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+10), 3);
                        break;

					case '13': # 入院 一般病棟 平均在院日数（直近3カ月）
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaNameArr[$startColumnNum], $rowNum+13,$columnAlphaNameArr[$totalColumnNum-1],$rowNum+13,
								$denominator_cnt1,
								3
								);
						break;
					
					case '16': # 入院 一般病棟 「重症度、看護必要度割合」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaNameArr[$startColumnNum], $rowNum+16,$columnAlphaNameArr[$totalColumnNum-1],$rowNum+16,
								$denominator_cnt2,
								3
								);
						break;

                    case '24': # 入院 障害者病棟 空床数
                        $totalData[$t][0]['data']      = $this->getExcelSubData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+23), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+22), 1);
                        break;

                    case '25': # 入院 障害者病棟 稼働率
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+22), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+23), 3);
                        break;

                    case '32': # 入院 精神一般病棟 空床数
                        $totalData[$t][0]['data']      = $this->getExcelSubData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+31), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+30), 1);
                        break;

                    case '33': # 入院 精神一般病棟 稼働率
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+30), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+31), 3);
                        break;

                    case '36': # 入院 療養病棟 医療適用 1日平均
                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+34), $totalData[$t][0]['day_count'], 3);
                        break;

                    case '37': # 入院 療養病棟 医療適用 稼働率
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+34), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+35), 3);
                        break;

                    case '50': # 入院 療養病棟 介護適用 1日平均
                        $totalData[$t][0]['day_count']  = $totalData[$t][0]['day_count'] + $mainData[$t][$plantKey]['day_count'];
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+48), $totalData[$t][0]['day_count'], 3);
                        break;

                    case '51': # 入院 療養病棟 介護適用 稼働率
                        $totalData[$t][0]['data']      = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+48), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+49), 3);
                        break;

                    case '57': # 入院 療養病棟 介護適用 平均介護度
                        $totalData[$t][0]['dn322'] = $totalData[$t][0]['dn322'] + $mainData[$t][$plantKey]['dn322'];
                        $totalData[$t][0]['dn323'] = $totalData[$t][0]['dn323'] + $mainData[$t][$plantKey]['dn323'];
                        $totalData[$t][0]['dn324'] = $totalData[$t][0]['dn324'] + $mainData[$t][$plantKey]['dn324'];
                        $totalData[$t][0]['dn325'] = $totalData[$t][0]['dn325'] + $mainData[$t][$plantKey]['dn325'];
                        $totalData[$t][0]['dn326'] = $totalData[$t][0]['dn326'] + $mainData[$t][$plantKey]['dn326'];

                        $totalData[$t][0]['data'] = $this->getExcelDivData(
                            sprintf(
                                '(%s*5+%s*4+%s*3+%s*2+%s*1)', $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+52), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+53),
                                $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+54), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+55), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+56)
                            ),
                            sprintf('(%s+%s+%s+%s+%s)', $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+52), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+53),
                                $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+54), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+55), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+56)
                            ),
                            3
                        );
                        break;

                    case '60': # 入院 療養病棟 精神療養 空床数
                        $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+59), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+58), 1);
                        break;

                    case '61': # 入院 療養病棟 精神療養 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+58), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+59), 3);
                        break;

                    case '64': # 入院 特定病棟 空床数
                        $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+63), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+62), 1);
                        break;

                    case '65': # 入院 特定病棟 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+62), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+63), 3);
                        break;

					case '66': # 入院 特定病棟 「重症患者の割合」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaNameArr[$startColumnNum], $rowNum+66,$columnAlphaNameArr[$totalColumnNum-1],$rowNum+66,
								$denominator_cnt3,
								3
								);
						break;
					
					case '67': # 入院 特定病棟 「在宅復帰率」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaNameArr[$startColumnNum], $rowNum+67,$columnAlphaNameArr[$totalColumnNum-1],$rowNum+67,
								$denominator_cnt4,
								3
								);
						break;
					
					case '76': # 重点事項 紹介率
                        $totalData[$t][0]['data'] = $this->getExcelDivData(sprintf('(%s+%s)', $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+70), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+72)), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+69), 3);
                        break;

                    case '77': # その他 救急受入率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+72), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+71), 3);
                        break;

                    case '78': # その他 救急入院率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+73), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+72), 3);
                        break;

                    case '79': # その他 時間外入院率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+75), $this->coord($columnAlphaNameArr[$totalColumnNum], $rowNum+74), 3);
                        break;

					case '80': # その他　「ﾘﾊﾋﾞﾘ平均提供単位数（一般）」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaNameArr[$startColumnNum], $rowNum+80,$columnAlphaNameArr[$totalColumnNum-1],$rowNum+80,
								count($monthlyData),
								3
								);
						break;
					
					case '81': # その他　「ﾘﾊﾋﾞﾘ平均提供単位数（療養、精神）」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaNameArr[$startColumnNum], $rowNum+81,$columnAlphaNameArr[$totalColumnNum-1],$rowNum+81,
								count($monthlyData),
								3
								);
						break;
					
					case '82': # その他　「ﾘﾊﾋﾞﾘ平均提供単位数（回復期リハ）」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaNameArr[$startColumnNum], $rowNum+82,$columnAlphaNameArr[$totalColumnNum-1],$rowNum+82,
								count($monthlyData),
								3
								);
						break;
					
					default:
                        $totalData[$t][0]['data'] = $this->getExcelSideSumData($columnAlphaNameArr[$startColumnNum], $columnAlphaNameArr[$totalColumnNum-1], $rowNum+$t);
                        break;
                }
            }
        }

        for ($i=1; $i <= $RowCnt; $i++) {
            switch ($i) {
                default:
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma))
                              .'</tr>';
                    break;

                case '2': case '4': case '13': case '36': case '50': case '57':
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_0_0))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_0_0))
                              .'</tr>';
                    break;

                case '7': case '8': case '12': case '16': case '25': case '33': case '37': case '51': case '61': case '65': case '66': case '67': case '76': case '77': case '78': case '79':
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_percent))
                              .'</tr>';
                    break;

                case '80': case '81': case '82':
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_unit))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_unit))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.72 病院月間報告書(行為件数)のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelHospitalMonthlyReport($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr       = $this->getExcelMeta();
        $fontTag      = $this->getFontTag();
        $displayMonth = $this->getDisplayMonth();
        $displayYear  = $this->getDisplayYear();
        $cYear        = $this->getCurrentYear();

        $indicatorData = $this->getIndicatorData();
        $cDayCount     = $indicatorData->getDayCountOfMonth($displayYear, $displayMonth);

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_9  = array('name' => 'rowspan', 'value' =>  9);
        $rowspan_17 = array('name' => 'rowspan', 'value' => 17);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);

        # style
        $bgColor_weekday       = array('name' => 'background-color',  'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday      = array('name' => 'background-color',  'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday       = array('name' => 'background-color',  'value' => $this->getRgbColor('lightpink'));
        $bgColor_blue          = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $textAlign_center      = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right       = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap     = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_comma = $this->getMsoNumberFormat('comma');

        $dayCountHeader = '<table width="100%" border="1" >'
                             .'<tr>'
                                 .'<td>'
                                     .'<table class="list" width="50%" border="1" >'
                                         .'<tr>'
                                             .$this->convertCellData(
                                                 array(0 => array('data' => '平日', 'style' => array($bgColor_weekday))
                                                     , 1 => array('data' => $cDayCount['weekday'].'日', 'style' => array($textAlign_center))
                                                     , 2 => array('data' => '土曜', 'style' => array($bgColor_saturday))
                                                     , 3 => array('data' => $cDayCount['saturday'].'日', 'style' => array($textAlign_center))
                                                     , 4 => array('data' => '休日', 'style' => array($bgColor_holiday))
                                                     , 5 => array('data' => $cDayCount['holiday'].'日', 'style' => array($textAlign_center))
                                                 )
                                               , $fontTag
                                               , array()
                                             )
                                         .'</tr>'
                                     .'</table>'
                                 .'</td>'
                                 .'<td style="text-align: right;" >'.sprintf($fontTag,sprintf('%04d年%02d月', $displayYear, $displayMonth)).'</td>'
                             .'</tr>'
                         .'</table>';

        $rtnStr .= $dayCountHeader;
        $rtnStr .= '<table class="list" width="100%" border="1" >';

        $leftSide  = array();
        $mainData  = array();
        $totalData = array();

//        $leftSide[0]  = array(0 => array('data' => '&nbsp;', 'attr' => array($rowspan_2, $colspan_2)));
		$leftSide[0]  = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)));
		$leftSide[2]  = array(0 => array('data' => '手術件数', 'attr' => array($rowspan_5))
                            , 1 => array('data' => '全身麻酔による手術')
        );
        $leftSide[3]  = array(0 => array('data' => '硬膜外・脊椎麻酔による手術'));
        $leftSide[4]  = array(0 => array('data' => '上・下肢伝達麻酔による手術'));
        $leftSide[5]  = array(0 => array('data' => '内視鏡的手術'));
        $leftSide[6]  = array(0 => array('data' => '白内障手術'));
        $leftSide[7]  = array(0 => array('data' => '内視鏡', 'attr' => array($rowspan_3))
                            , 1 => array('data' => '食道・胃・十二指腸ファイバー')
        );
        $leftSide[8]  = array(0 => array('data' => '大腸・直腸ファイバー'));
        $leftSide[9]  = array(0 => array('data' => 'その他'));
        $leftSide[10] = array(0 => array('data' => '検査', 'attr' => array($rowspan_9))
                            , 1 => array('data' => '心電図')
        );
        $leftSide[11] = array(0 => array('data' => 'ホルター型心電図'));
        $leftSide[12] = array(0 => array('data' => 'トレッドミル'));
        $leftSide[13] = array(0 => array('data' => '心臓超音波'));
        $leftSide[14] = array(0 => array('data' => '胸腹部超音波'));
        $leftSide[15] = array(0 => array('data' => 'その他超音波(頭頸部・四肢等)'));
        $leftSide[16] = array(0 => array('data' => '肺機能'));
        $leftSide[17] = array(0 => array('data' => '骨塩定量'));
        $leftSide[18] = array(0 => array('data' => '血液ガス分析'));
        $leftSide[19] = array(0 => array('data' => '放射線', 'attr' => array($rowspan_17))
                            , 1 => array('data' => '一般撮影')
        );
        $leftSide[20] = array(0 => array('data' => 'マンモグラフィー'));
        $leftSide[21] = array(0 => array('data' => 'MDL(UGI)'));
        $leftSide[22] = array(0 => array('data' => 'BEM'));
        $leftSide[23] = array(0 => array('data' => 'DIP'));
        $leftSide[24] = array(0 => array('data' => 'DIC'));
        $leftSide[25] = array(0 => array('data' => '特殊'));
        $leftSide[26] = array(0 => array('data' => 'その他'));
        $leftSide[27] = array(0 => array('data' => 'CT(BRAIN)'));
        $leftSide[28] = array(0 => array('data' => 'CT(BODY)'));
        $leftSide[29] = array(0 => array('data' => 'CT(その他)'));
        $leftSide[30] = array(0 => array('data' => 'CT造影剤加算'));
        $leftSide[31] = array(0 => array('data' => 'MRI(BRAIN)'));
        $leftSide[32] = array(0 => array('data' => 'MRI(BODY)'));
        $leftSide[33] = array(0 => array('data' => 'MRI(その他)'));
        $leftSide[34] = array(0 => array('data' => 'MRI造影剤加算'));
        $leftSide[35] = array(0 => array('data' => 'Angio'));
        $leftSide[36] = array(0 => array('data' => '重点項目', 'attr' => array($rowspan_17))
                            , 1 => array('data' => '超重症児(者)入院診療加算(イ・ロ)')
        );
        $leftSide[37] = array(0 => array('data' => '栄養サポートチーム加算'));
        $leftSide[38] = array(0 => array('data' => '呼吸ケアチーム加算'));
        $leftSide[39] = array(0 => array('data' => '医療安全対策加算(1・2)'));
        $leftSide[40] = array(0 => array('data' => '感染防止対策加算'));
        $leftSide[41] = array(0 => array('data' => '医療機器安全管理料(1・2)'));
        $leftSide[42] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_1').')'));
        $leftSide[43] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_2').')'));
        $leftSide[44] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_3').')'));
        $leftSide[45] = array(0 => array('data' => '医薬品安全性情報等管理体制加算'));
        $leftSide[46] = array(0 => array('data' => '&nbsp;'));
        $leftSide[47] = array(0 => array('data' => '&nbsp;'));
        $leftSide[48] = array(0 => array('data' => '&nbsp;'));
        $leftSide[49] = array(0 => array('data' => '&nbsp;'));
        $leftSide[50] = array(0 => array('data' => '&nbsp;'));
        $leftSide[51] = array(0 => array('data' => '&nbsp;'));
        $leftSide[52] = array(0 => array('data' => '&nbsp;'));

        $rowCnt   = count($leftSide);
        $plantCnt = count($plantArr);

//        $startRowNum  = 4;
		$startRowNum  = 3;
		$startColNum  = 3;
        $colAlphaName = $this->columnAlphaNameArr;
        $totalColNum  = $startColNum + $plantCnt;

        $plantNameArr = array();
        if (is_array($plantArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $facilityId = $this->getArrValueByKey('facility_id', $plantVal);
                $plantNameArr[$plantKey]          = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'style' => array('bgColor' => $bgColor_blue));
//                $plantNameAnderDisplay[$plantKey] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));
                $dispNum = array();
                for ($i=342; $i <= 392; $i++) { $dispNum[$i] = null; }

                foreach ($dataArr as $dataKey => $dataVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal)) {
                        for ($i=342; $i <= 392; $i++) { $dispNum[$i] = $this->getArrValueByKey('dispnum_'.$i, $dataVal); }
                    }
                }

                $startNum = 2;
                for ($i=342; $i <= 392; $i++) {
                    $mainData[$startNum][$plantKey] = array('data' => $dispNum[$i]);
                    $startNum++;
                }

                for ($i = 2; $i <= $rowCnt; $i++) {
                    $rowNum = $startRowNum - 2 + $i;
                    $totalData[$i][0] = array('data' => $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum-1], $rowNum));
                }
            }
        }

        $plantNameArr[]          = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue));
//        $plantNameAnderDisplay[] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));

        $mainData[0] = $plantNameArr;
//        $mainData[1] = $plantNameAnderDisplay;

        $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSide[0], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
                      .$this->convertCellData($mainData[0], $fontTag, array('textAlign' => $textAlign_center, 'msoNumberFormat' => $msoNumberFormat_comma))
                  .'</tr>';
//                  .'<tr>'
//                      .$this->convertCellData($mainData[1], $fontTag, array('textAlign' => $textAlign_center, 'msoNumberFormat' => $msoNumberFormat_comma))
//                  .'</tr>';

        for ($i = 2; $i <= $rowCnt; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSide[$i], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($mainData[$i], $fontTag, array('textAlign' => $textAlign_center, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_center, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * エクセルファイル上での座標(カラムのアルファベット)を取得する
     * ただし、月初めのデータ以降のみ
     * @access protected
     */
    function getExcelColumnAlphabet($i)
    {
        $colArr = $this->monthCellArr;
        return $colArr[$i];
    }


    function coord($colAlphaName, $row)
    {
        return sprintf('%s%d', $colAlphaName, $row);
    }




    /**
     * エクセルファイルを出力する際のヘッダーを返す
     * @return string
     */
    function getExcelMeta()
    {
        return $this->excelMeta;
    }

    /**
     * エクセルファイル上の座標(行番号)を取得する
     * ただし、月初めのデータ以降のみ
     * @param $i       integer 施設の順序
     * @param $rowspan integer １施設の表示行数
     * @return unknown_type
     */
    function getExcelRowCount($i, $rowspan, $base_row = 2)
    {
        return $base_row + ($rowspan * $i);
    }

    /**
     * エクセルファイル上の増減に当たるデータを取得する
     */
    function getCompCellData($column, $currentRow, $lastRow, $precision=1)
    {
        return $this->getExcelSubData($this->coord($column, $currentRow), $this->coord($column, $lastRow), $precision);
    }

    /**
     * エクセルファイル上の増減率に当たるデータを取得する
     */
    function getRaitoCellData($column, $compRow, $lastRow, $precision=3)
    {
        return $this->getExcelDivData($this->coord($column, $compRow), $this->coord($column, $lastRow), $precision);
    }

    /**
     * エクセルファイル上の施設合計に当たるセル内のデータを取得する。
     */
    function getTotalCellData($rowNum)
    {
        $arr = $this->monthCellArr;
        return $this->getExcelSideSumData($arr[0], $arr[11], $rowNum);
    }

    /**
     * エクセルファイル上の施設平均に当たるセル内のデータを取得する。
     * ただし、前年比較画面限定
     * @param $rowNum
     * @return string                  エクセルの関数(SUM(X,Y,Z))
     */
    function getAverageCellData($rowNum, $precision=0)
    {
        $arr = $this->monthCellArr;
        if ($precision==0) {
            return '=AVERAGE('.$arr[0].$rowNum.':'.$arr[11].$rowNum.')';
        }
        elseif($precision > 0) {
            return '=ROUND(AVERAGE('.$arr[0].$rowNum.':'.$arr[11].$rowNum.'), '.$precision.')';
        }
    }

    /**
     * エクセルファイル上のAMG合計に当たるセル内のデータを取得する。
     * 施設合計や、施設平均を取得する際には、getTotalCellData または getAverageCellData を使用すること
     *
     * @param $column                  対象データ(取得対象の月)のアルファベット
     * @param $lastRowNum              対象データの施設数-1
     * @param $rowspan                 一施設辺りの表示行数
     * @param $rowNum     DEFAULT NULL 対象データ(表示年度、増減、増減率)のデータ取得時に使用
     * @return string                  エクセルの関数(SUM(X,Y,Z))
     */
    function getAmgTotalCellData($column, $lastRowNum, $rowspan, $rowNum=null, $baseRowNum=2, $precision=null)
    {
        $alphabetArr = array();
        for ($i=0; $i <= $lastRowNum; $i++) {
            if (bcmod($this->getExcelRowCount($i, $rowspan, $baseRowNum), $rowspan)==$baseRowNum) {
                $alphabetArr[] = $column.($this->getExcelRowCount($i, $rowspan) + $rowNum);
            }
        }
        return $this->getExcelSumDataByArray($alphabetArr, $precision);
    }

    /**
     * エクセルファイル上のAMG平均に当たるセル内のデータを取得する。
     * 施設合計や施設平均を取得する際には、getTotalCellData または getAverageCellData を使用すること
     *
     * @param $column                  対象データ(取得対象の月)のアルファベット
     * @param $lastRowNum              対象データの施設数-1
     * @param $rowspan                 一施設辺りの表示行数
     * @param $rowNum     DEFAULT NULL 対象データ(前年度、増減、増減率)のデータ取得時に使用
     * @return string                  エクセルの関数(AVERAGE(X,Y,Z))
     */
    function getAmgAverageCellData($column, $lastRowNum, $rowspan, $rowNum=null, $baseRowNum=2, $denominator, $precision=null)
    {
        $alphabetArr = array();
        for ($i=0; $i <= $lastRowNum; $i++) {
            if (bcmod($this->getExcelRowCount($i, $rowspan, $baseRowNum), $rowspan)==$baseRowNum) {
                $alphabetArr[] = $column.($this->getExcelRowCount($i, $rowspan) + $rowNum);
            }
        }
        return $this->getExcelAverageDataByArray($alphabetArr, $denominator, $precision);
    }

    function getColmunTotalData($column, $startNum, $lastNum)
    {
        $rtnArr = array();
        for ($i=$startNum; $i < $lastNum; $i++) { $rtnArr[] = $column.$i; }
        return $this->getExcelSumDataByArray($rtnArr);
    }

    function getColmunAverageData($column, $startNum, $lastNum)
    {
        $rtnArr = array();
        for ($i=$startNum; $i < $lastNum; $i++) { $rtnArr[] = $column.$i; }
        return $this->getExcelAverageData($rtnArr);
    }

    function getExcelSubData($left, $right, $precision=null) {
        if (is_null($precision)) {
            return sprintf('=%s-%s', $left, $right);
        }
        else {
            return sprintf('=ROUND(ROUND(%s, %d)-ROUND(%s, %d), %d)', $left, $precision, $right, $precision, $precision);
        }
    }

    function getExcelDivData($left, $right, $precision=null) {
        if (is_null($precision)) {
            $right = empty($right)? 0 : $right;
            return sprintf('=IF(%s="", "", IF(%s=0, 0, (%s/%s)))', $left, $right, $left, $right);
        }
        else {
            $right = empty($right)? 0 : $right;
            return sprintf('=IF(%s="", "", IF(%s=0, 0, ROUND(%s/%s, %d)))', $left, $right, $left, $right, $precision);
        }
    }

    function getExcelSumData($startColumn, $startNum, $endColumn, $endNum, $precision=null)
    {
        if (!is_null($precision)) { return sprintf('=ROUND(SUM(%s%d:%s%d), %d)',$startColumn, $startNum, $endColumn, $endNum, $precision); }
        else { return sprintf('=SUM(%s%d:%s%d)',$startColumn, $startNum, $endColumn, $endNum); }
    }

    /**
     *
     * @param $column
     * @param $startNum
     * @param $endNum
     * @return unknown_type
     */
    function getExcelLengthSumData($column, $startNum, $endNum)
    {
        return $this->getExcelSumData($column, $startNum, $column, $endNum);
    }

    /**
     * エクセル上の施設合計の取得を行う
     * @param $startColumn
     * @param $endColumn
     * @param $number
     * @return string
     */
    function getExcelSideSumData($startColumn, $endColumn, $number, $precision=null)
    {
        return $this->getExcelSumData($startColumn, $number, $endColumn, $number, $precision);
    }

    function getExcelSideAverageData($startColumn, $endColumn, $number, $denominator, $precision=null)
    {
        $numerator = sprintf('SUM(%s:%s)',$this->coord($startColumn, $number), $this->coord($endColumn, $number));
        return $this->getExcelDivData($numerator, $denominator, $precision);
    }

    /**
     *
     * @param $arr
     * @return unknown_type
     */
    function getExcelSumDataByArray($arr, $precision=null)
    {
        if (is_null($precision)) {
            return '=SUM('.implode(',', $arr).')';
        }
        else {
            return '=ROUND(SUM('.implode(',', $arr).'), '.$precision.')';
        }
    }

    function getExcelAverageDataByArray($arr, $denominator, $precision=null)
    {
        return $this->getExcelDivData('SUM('.implode(',', $arr).')', $denominator, $precision);
    }

    function getExcelAverageData($arr)
    {
        return '=AVERAGE('.implode(',', $arr).')';
    }

    function getDenominatorOfAverageValueOfAverageValue($arr)
    {
        $rtnCnt = 0;
        if (!is_array($arr)) { return false; }
        foreach ($arr as $Key => $val) { if ($val > 0) { $rtnCnt++; } }
        return $rtnCnt;
    }

    /**
     * 稼働率　又は 増減率の計算用
     * @param $gokei
     * @param $act
     * @return unknown_type
     */
    function getUtilizationRates($left, $right, $precision=0) {
        return empty($right)? 0 : round(($left / $right * 100), $precision);
    }

    /**
     * 引き算用
     * @param $gokei
     * @param $act
     * @return unknown_type
     */
    function getNumberOfEmptyFloors($right, $left, $precision=0) {
        return round($left - $right, $precision);
    }

    /**
     * ▲マーク表示
     * @param $column
     * @param $rowNum
     * @param $parsent
     * @return unknown_type
     */
    function getUtilizationRatesTrigona($target, $parsent='90') {
        return ($target < $parsent)? is_null($target)? '' : '▲' : null;
    }

    /**
     * ▲マーク表示
     * @param $column
     * @param $rowNum
     * @param $parsent
     * @return unknown_type
     */
    function getExcelUtilizationRatesTrigona($column, $rowNum, $parsent='90')
    {
        return sprintf('=IF(%s="", "", IF(ROUND(%s*100/1,1) < %d, "▲", ""))', $this->coord($column, $rowNum), $this->coord($column, $rowNum), $parsent);
    }

    function getRgbColor($name, $style=null) {
        $colorArr = $this->colorArr;
        foreach ($colorArr as $colorVal) {
            if ($this->getArrValueByKey('name', $colorVal) == $name) {
                return ($style == 'background-color')? 'background-color: '.$colorVal['value'].';' : $colorVal['value'];
            }
        }
    }

    /**
     *
     * @param $arr
     * @return unknown_type
     */
    function getAddArrayValue($arr) {
        $rtn = null;
        foreach ($arr as $key => $val) { $rtn = $rtn + $val; }
        return $rtn;
    }

    function getAddArrayValueForStatus($arr) {
        $rtn = null;
        foreach ($arr as $key => $val) { $rtn = $this->checkCellDataStatus('comp', array($rtn, $val)); }
        return $rtn;
    }

    function addParentheses($str) {
        return sprintf('(%s)', $str);
    }
    /* */

    function getDeptIPDOPDDisplay($resultArr,$dateMonth,$dateYear)
    {
    $currentMonth = date('m');
    $currentYear = date('Y');
    //If current month and year is same as selected then
    //get the total days in months as till yesterday

    if($currentMonth===$dateMonth && $currentYear===$dateYear){
        $totalDaysInMonth=date('d')-1;
    }else if(($currentMonth>$dateMonth && $currentYear===$dateYear) || ($currentYear>$dateYear)){
        $totalDaysInMonth=date("j",mktime(0,0,0,$dateMonth+1,0, $dateYear));
    }else {
        $totalDaysInMonth = 0;
    }

    $thColorArr   = array('name' => 'background-color', 'value' => '#d2f4fa;'); # 青
    $compColorArr = array('name' => 'background-color', 'value' => '#FFC8ED;'); # 緑
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');
    $statusColorWhiteArr = array('name' => 'background-color', 'value' => '#FFF;');
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');
    $thColor   = $this->getArrValueByKey('name', $thColorArr).   ':'. $this->getArrValueByKey('value', $thColorArr);
    $compColor = $this->getArrValueByKey('name', $compColorArr). ':'. $this->getArrValueByKey('value', $compColorArr);
    $statusRedColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusWhiteColor = $this->getArrValueByKey('name', $statusColorWhiteArr). ':'. $this->getArrValueByKey('value', $statusColorWhiteArr);
    $statusYellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);

    $font = $this->getFont();
    $font['color'] = null;
    if (!is_null($fontStyle) && is_array($fontStyle)) {
        $font = $fontStyle;
    }
    $th_header="";
    $td_naika_ga="";
    $td_sinkei_ga="";
    $td_kokyu_ga="";
    $td_shoka_ga="";
    $td_junkanki_ga="";
    $td_shouni_ga="";
    $td_geka_ga="";
    $td_seikei_ga="";
    $td_keiseibi_ga="";
    $td_nou_ga="";
    $td_hifu_ga="";
    $td_hinyo_ga="";
    $td_sanfu_ga="";
    $td_ganka_ga="";
    $td_jibi_ga="";
    $td_touseki_ga="";
    $td_seisin_ga="";
    $td_sika_ga="";
    $td_hoshasen_ga="";
    $td_masui_ga="";
    //Total variables
    $total_naika_ga=0;
    $total_sinkei_ga=0;
    $total_kokyu_ga=0;
    $total_shoka_ga=0;
    $total_junkanki_ga=0;
    $total_shouni_ga=0;
    $total_geka_ga=0;
    $total_seikei_ga=0;
    $total_keiseibi_ga=0;
    $total_nou_ga=0;
    $total_hifu_ga=0;
    $total_hinyo_ga=0;
    $total_sanfu_ga=0;
    $total_ganka_ga=0;
    $total_jibi_ga=0;
    $total_touseki_ga=0;
    $total_seisin_ga=0;
    $total_sika_ga=0;
    $total_hoshasen_ga=0;
    $total_masui_ga=0;

    $total_count=0;

    $td_total="";
    $td_avg="";
    $fontTag = $this->getFontTag($font);
    list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
    /*
    * Color flag for count and jnl_status for total fields
    *  0 - Normal
    *  1 - jnl_status is 0
    *  2 -  count is less than no. of days
    */
    $colorFlag=0;
    //Iterate through resultset to transponse the records
    for($i=0;$i<count($resultArr);$i++){
        //Create header row
        $thArr.="<td style='".$thColor." text-align : center; '>".$fontTagLeft.$resultArr[$i]['jnl_facility_name'].$fontTagRight."</td>";
        //Check the jnl status and set the background color for cell
        if($resultArr[$i]['jnl_status']==0){
            if($colorFlag==0){
                $colorFlag=1;
            }
            $statusColor=$statusYellowColor;
            if($resultArr[$i]['countstatus']<$totalDaysInMonth)
            {
                $colorFlag=2;
                $statusColor=$statusRedColor;
            }
        }else if($resultArr[$i]['countstatus']<$totalDaysInMonth)
        {
            $colorFlag=2;
            $statusColor=$statusRedColor;
        }else{
            $statusColor=$statusWhiteColor;
        }
        //create row for naika ga
        $td_naika_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['naika_ga'].$fontTagRight."</td>";
        //create row for sinkei ga
        $td_sinkei_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['sinkei_ga'].$fontTagRight."</td>";
        //create row for kokyu ga
        $td_kokyu_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['kokyu_ga'].$fontTagRight."</td>";
        //create row for shoka ga
        $td_shoka_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['shoka_ga'].$fontTagRight."</td>";
        //create row for junkanki ga
        $td_junkanki_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['junkanki_ga'].$fontTagRight."</td>";
        //create row for shouni ga
        $td_shouni_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['shouni_ga'].$fontTagRight."</td>";
        //create row for geka ga
        $td_geka_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['geka_ga'].$fontTagRight."</td>";
        //create row for seikei ga
        $td_seikei_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['seikei_ga'].$fontTagRight."</td>";
        //create row for keiseibi ga
        $td_keiseibi_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['keiseibi_ga'].$fontTagRight."</td>";
        //create row for nou ga
        $td_nou_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['nou_ga'].$fontTagRight."</td>";
        //create row for hifu ga
        $td_hifu_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['hifu_ga'].$fontTagRight."</td>";
        //create row for hinyo ga
        $td_hinyo_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['hinyo_ga'].$fontTagRight."</td>";
        //create row for sanfu ga
        $td_sanfu_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['sanfu_ga'].$fontTagRight."</td>";
        //create row for ganka ga
        $td_ganka_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['ganka_ga'].$fontTagRight."</td>";
        //create row for jibi ga
        $td_jibi_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['jibi_ga'].$fontTagRight."</td>";
        //create row for touseki ga
        $td_touseki_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['touseki_ga'].$fontTagRight."</td>";
        //create row for seisin ga
        $td_seisin_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['seisin_ga'].$fontTagRight."</td>";
        //create row for sika ga
        $td_sika_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['sika_ga'].$fontTagRight."</td>";
        //create row for hoshasen ga
        $td_hoshasen_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['hoshasen_ga'].$fontTagRight."</td>";
        //create row for masui ga
        $td_masui_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['masui_ga'].$fontTagRight."</td>";
        //create row for total
        $td_total.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['total'].$fontTagRight."</td>";
        //create row for average using (total/no. of days in month)
        $td_avg.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.sprintf('%.1f', round(($resultArr[$i]['total']/$resultArr[$i]['countstatus']),1)).$fontTagRight."</td>";
        //Calculate the total as per dept
        $total_naika_ga+=$resultArr[$i]['naika_ga'];
        $total_sinkei_ga+=$resultArr[$i]['sinkei_ga'];
        $total_kokyu_ga+=$resultArr[$i]['kokyu_ga'];
        $total_shoka_ga+=$resultArr[$i]['shoka_ga'];
        $total_junkanki_ga+=$resultArr[$i]['junkanki_ga'];
        $total_shouni_ga+=$resultArr[$i]['shouni_ga'];
        $total_geka_ga+=$resultArr[$i]['geka_ga'];
        $total_seikei_ga+=$resultArr[$i]['seikei_ga'];
        $total_keiseibi_ga+=$resultArr[$i]['keiseibi_ga'];
        $total_nou_ga+=$resultArr[$i]['nou_ga'];
        $total_hifu_ga+=$resultArr[$i]['hifu_ga'];
        $total_hinyo_ga+=$resultArr[$i]['hinyo_ga'];
        $total_sanfu_ga+=$resultArr[$i]['sanfu_ga'];
        $total_ganka_ga+=$resultArr[$i]['ganka_ga'];
        $total_jibi_ga+=$resultArr[$i]['jibi_ga'];
        $total_touseki_ga+=$resultArr[$i]['touseki_ga'];
        $total_seisin_ga+=$resultArr[$i]['seisin_ga'];
        $total_sika_ga+=$resultArr[$i]['sika_ga'];
        $total_hoshasen_ga+=$resultArr[$i]['hoshasen_ga'];
        $total_masui_ga+=$resultArr[$i]['masui_ga'];
        $total_count+=$resultArr[$i]['countstatus'];
    }

    //Set color based on color flag
    switch($colorFlag){
        case 0:
            $totalCellColor=$statusWhiteColor;
            break;
        case 1:
            $totalCellColor=$statusYellowColor;
            break;
        case 2:
            $totalCellColor=$statusRedColor;
            break;

    }

    //Assemble the rows
    $table='<table class="list" width="100%">';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.sprintf($fontTag, '診療科').'</td>'.$thArr.'<td style="'.$thColor.'text-align : center; ">'.sprintf($fontTag, '合計').'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'内科'.$fontTagRight.'</td>'.$td_naika_ga.'<td style="text-align : right; '.$totalCellColor.' ">'.$fontTagLeft.$total_naika_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'神経内科'.$fontTagRight.'</td>'.$td_sinkei_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_sinkei_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'呼吸器科'.$fontTagRight.'</td>'.$td_kokyu_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_kokyu_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'消化器科'.$fontTagRight.'</td>'.$td_shoka_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_shoka_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'循環器科'.$fontTagRight.'</td>'.$td_junkanki_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_junkanki_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'小児科'.$fontTagRight.'</td>'.$td_shouni_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_shouni_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'外科'.$fontTagRight.'</td>'.$td_geka_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_geka_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'整形外科'.$fontTagRight.'</td>'.$td_seikei_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_seikei_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'形成（美）外科'.$fontTagRight.'</td>'.$td_keiseibi_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_keiseibi_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'脳神経外科'.$fontTagRight.'</td>'.$td_nou_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_nou_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'皮膚科'.$fontTagRight.'</td>'.$td_hifu_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_hifu_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'泌尿器科'.$fontTagRight.'</td>'.$td_hinyo_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_hinyo_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'産婦人科'.$fontTagRight.'</td>'.$td_sanfu_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_sanfu_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'眼科'.$fontTagRight.'</td>'.$td_ganka_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_ganka_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'耳鼻咽喉科'.$fontTagRight.'</td>'.$td_jibi_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_jibi_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'透析科'.$fontTagRight.'</td>'.$td_touseki_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_touseki_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'精神科'.$fontTagRight.'</td>'.$td_seisin_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_seisin_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'歯科'.$fontTagRight.'</td>'.$td_sika_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_sika_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'放射線科'.$fontTagRight.'</td>'.$td_hoshasen_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_hoshasen_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'麻酔科'.$fontTagRight.'</td>'.$td_masui_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$total_masui_ga.$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'合計'.$fontTagRight.'</td>'.$td_total.'<td  style="'.$totalCellColor.'text-align : right;">'.$fontTagLeft.($total_naika_ga+$total_sinkei_ga+    $total_kokyu_ga+$total_shoka_ga+$total_junkanki_ga+    $total_shouni_ga+$total_geka_ga+    $total_seikei_ga+$total_keiseibi_ga+$total_nou_ga+$total_hifu_ga+$total_hinyo_ga+$total_sanfu_ga+$total_ganka_ga+$total_jibi_ga+$total_touseki_ga+$total_seisin_ga+$total_sika_ga+$total_hoshasen_ga+$total_masui_ga).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'一日平均'.$fontTagRight.'</td>'.$td_avg.'<td style="'.$totalCellColor.'text-align : right;">'.$fontTagLeft.sprintf('%.1f', round((($total_naika_ga+$total_sinkei_ga+    $total_kokyu_ga+$total_shoka_ga+$total_junkanki_ga+    $total_shouni_ga+$total_geka_ga+    $total_seikei_ga+$total_keiseibi_ga+$total_nou_ga+$total_hifu_ga+$total_hinyo_ga+$total_sanfu_ga+$total_ganka_ga+$total_jibi_ga+$total_touseki_ga+$total_seisin_ga+$total_sika_ga+$total_hoshasen_ga+$total_masui_ga)/$total_count),1)).$fontTagRight.'</td></tr>';
    $table.='</table>';
    return $table;


    }
    /*
    Excel output function for IPD OPD monthly report
    */
    function outputExcelIPDOPDReport($resultArr,$dateMonth,$dateYear)
     {
    //Constants for depts
    define('NAIKA_GA',4);
    define('SINKEI_GA',5);
    define('KOKYU_GA',6);
    define('SHOKA_GA',7);
    define('JUNKANKI_GA',8);
    define('SHOUNI_GA',9);
    define('GEKA_GA',10);
    define('SEIKEI_GA',11);
    define('KEISEIBI_GA',12);
    define('NOU_GA',13);
    define('HIFU_GA',14);
    define('HINYO_GA',15);
    define('SANFU_GA',16);
    define('GANKA_GA',17);
    define('JIBI_GA',18);
    define('TOUSEKI_GA',19);
    define('SEISIN_GA',20);
    define('SIKA_GA',21);
    define('HOSHASEN_GA',22);
    define('MASUI_GA',23);
    define('TOTAL',24);
    //define('AVERAGE',25);
    //get the excel columns names in array
    $excelColumns=$this->getExcelColumnNames();
    $table= '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';
    $sundayCount=0;
    $saturdayCount=0;
    //Get the total number of days in selected month
    $totalDays=date("j",mktime(0,0,0,$dateMonth+1,0, $dateYear));
    //Iterate through all the days to count the saturday's and sunday's
    for($i=1;$i<=$totalDays;$i++){

        if((int)$dateMonth==12 && $i==30){
            $saturdayCount++;
        }else if((int)$dateMonth==12 && $i==31){
            $sundayCount++;
        }else if((int)$dateMonth==1 && $i==1){
            $sundayCount++;
        }else if((int)$dateMonth==1 && $i==2){
            $sundayCount++;
        }else if((int)$dateMonth==1 && $i==3){
            $sundayCount++;
        }else{
            switch(date("l",mktime(0,0,0,$dateMonth,$i, $dateYear))){
                case "Saturday":
                    $saturdayCount++;
                    break;
                case "Sunday":
                    $sundayCount++;
                    break;
                default:
                    break;
            }
        }
    }
    $totalDays=$totalDays-($saturdayCount+    $sundayCount);

    $fontTag = $this->getFontTag();
    list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);


    $indicatorData = $this->getIndicatorData();
    $dayCount = $indicatorData->getDayCountOfMonth($dateYear, $dateMonth);

    $saturdayColor = $this->getRgbColor('saturday');
    $holidayColor = $this->getRgbColor('lightpink');

    $table.='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    $table.='<tbody><tr><td width="80%">';
    for($i=0;$i<count($resultArr)-1;$i++){
        $table.='<td></td>';
    }
    $table.='<td style="text-align: right; border:1px solid black;" align="right">'.$dateYear.'</td>';
    $table.='<td style="text-align: right; border:1px solid black;">'.$dateMonth.'</td></tr></tbody></table>';
    $table.='<table width="30%" class="list" cellspacing="0" cellpadding="1" border="1">';
    $table.='<tr><td style="text-align: left; background-color:#ffe9c8;">'.$fontTagLeft.'平日'.$fontTagRight.'</td>';
    $table.='<td style="text-align:center;padding:0 2px 0 2px;">';

    $table.=''.$fontTagLeft.$dayCount['weekday'].'日'.$fontTagRight.'</td>';
//    $table.=''.$fontTagLeft.$totalDays.$fontTagRight.'</td>';
    $table.='<td style="text-align: left; background-color: '.$saturdayColor.'";>'.$fontTagLeft.'土曜'.$fontTagRight.'</td>';
    $table.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$dayCount['saturday'].'日'.$fontTagRight.'</td>';
    $table.='<td  style="text-align: left; background-color: '.$holidayColor.'";>'.sprintf($fontTag,'休日').'</td>';
    $table.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$dayCount['holiday'].'日'.$fontTagRight.'</td></tr>    </table>';
    $currentMonth = date('m');
    $currentYear = date('Y');
    //If current month and year is same as selected then
    //get the total days in months as till yesterday
    if($currentMonth===$dateMonth && $currentYear===$dateYear){
        $totalDaysInMonth=date('d')-1;
    }else if (($currentMonth>$dateMonth && $currentYear===$dateYear) || ($currentYear>$dateYear)){
        $totalDaysInMonth=date("j",mktime(0,0,0,$dateMonth+1,0, $dateYear));
    }
    else {
        $totalDaysInMonth=0;
    }

    $thColorArr   = array('name' => 'background-color', 'value' => '#d2f4fa;'); # 青
    $compColorArr = array('name' => 'background-color', 'value' => '#FFC8ED;'); # 緑
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');
    $statusColorWhiteArr = array('name' => 'background-color', 'value' => '#FFF;');
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');
    $thColor   = $this->getArrValueByKey('name', $thColorArr).   ':'. $this->getArrValueByKey('value', $thColorArr);
    $compColor = $this->getArrValueByKey('name', $compColorArr). ':'. $this->getArrValueByKey('value', $compColorArr);
    $statusRedColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusWhiteColor = $this->getArrValueByKey('name', $statusColorWhiteArr). ':'. $this->getArrValueByKey('value', $statusColorWhiteArr);
    $statusYellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
    $msoNumberFormat_arr = array('name' => 'mso-number-format', 'value' => '0.0');
    $msoNumberFormat = $msoNumberFormat_arr['name'].':'.$msoNumberFormat_arr['value'].';';

    $font = $this->getFont();
    $font['color'] = null;
    if (!is_null($fontStyle) && is_array($fontStyle)) {
        $font = $fontStyle;
    }
    $th_header="";
    $td_naika_ga="";
    $td_sinkei_ga="";
    $td_kokyu_ga="";
    $td_shoka_ga="";
    $td_junkanki_ga="";
    $td_shouni_ga="";
    $td_geka_ga="";
    $td_seikei_ga="";
    $td_keiseibi_ga="";
    $td_nou_ga="";
    $td_hifu_ga="";
    $td_hinyo_ga="";
    $td_sanfu_ga="";
    $td_ganka_ga="";
    $td_jibi_ga="";
    $td_touseki_ga="";
    $td_seisin_ga="";
    $td_sika_ga="";
    $td_hoshasen_ga="";
    $td_masui_ga="";
    /*
    //Total variables
    $total_naika_ga=0;
    $total_sinkei_ga=0;
    $total_kokyu_ga=0;
    $total_shoka_ga=0;
    $total_junkanki_ga=0;
    $total_shouni_ga=0;
    $total_geka_ga=0;
    $total_seikei_ga=0;
    $total_keiseibi_ga=0;
    $total_nou_ga=0;
    $total_hifu_ga=0;
    $total_hinyo_ga=0;
    $total_sanfu_ga=0;
    $total_ganka_ga=0;
    $total_jibi_ga=0;
    $total_touseki_ga=0;
    $total_seisin_ga=0;
    $total_sika_ga=0;
    $total_hoshasen_ga=0;
    $total_masui_ga=0;
    */
    $total_count=0;
    $td_total="";
    $td_avg="";
    $fontTag = $this->getFontTag($font);
    list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
    /*
    * Color flag for count and jnl_status for total fields
    *  0 - Normal
    *  1 - jnl_status is 0
    *  2 -  count is less than no. of days
    */
    $colorFlag=0;
    //Iterate through resultset to transponse the records
    for($i=0;$i<count($resultArr);$i++){
        //Create header row
        $thArr.="<td style='".$thColor." text-align : center; '>".$fontTagLeft.$resultArr[$i]['jnl_facility_name'].$fontTagRight."</td>";
        //Check the jnl status and set the background color for cell
        if($resultArr[$i]['jnl_status']==0){
            if($colorFlag==0){
                $colorFlag=1;
            }
            $statusColor=$statusYellowColor;
            if($resultArr[$i]['countstatus']<$totalDaysInMonth)
            {
                $colorFlag=2;
                $statusColor=$statusRedColor;
            }
        }else if($resultArr[$i]['countstatus']<$totalDaysInMonth)
        {
            $colorFlag=2;
            $statusColor=$statusRedColor;
        }else{
            $statusColor=$statusWhiteColor;
        }
        //create row for naika ga
        $td_naika_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['naika_ga'].$fontTagRight."</td>";
        //create row for sinkei ga
        $td_sinkei_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['sinkei_ga'].$fontTagRight."</td>";
        //create row for kokyu ga
        $td_kokyu_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['kokyu_ga'].$fontTagRight."</td>";
        //create row for shoka ga
        $td_shoka_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['shoka_ga'].$fontTagRight."</td>";
        //create row for junkanki ga
        $td_junkanki_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['junkanki_ga'].$fontTagRight."</td>";
        //create row for shouni ga
        $td_shouni_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['shouni_ga'].$fontTagRight."</td>";
        //create row for geka ga
        $td_geka_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['geka_ga'].$fontTagRight."</td>";
        //create row for seikei ga
        $td_seikei_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['seikei_ga'].$fontTagRight."</td>";
        //create row for keiseibi ga
        $td_keiseibi_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['keiseibi_ga'].$fontTagRight."</td>";
        //create row for nou ga
        $td_nou_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['nou_ga'].$fontTagRight."</td>";
        //create row for hifu ga
        $td_hifu_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['hifu_ga'].$fontTagRight."</td>";
        //create row for hinyo ga
        $td_hinyo_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['hinyo_ga'].$fontTagRight."</td>";
        //create row for sanfu ga
        $td_sanfu_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['sanfu_ga'].$fontTagRight."</td>";
        //create row for ganka ga
        $td_ganka_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['ganka_ga'].$fontTagRight."</td>";
        //create row for jibi ga
        $td_jibi_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['jibi_ga'].$fontTagRight."</td>";
        //create row for touseki ga
        $td_touseki_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['touseki_ga'].$fontTagRight."</td>";
        //create row for seisin ga
        $td_seisin_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['seisin_ga'].$fontTagRight."</td>";
        //create row for sika ga
        $td_sika_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['sika_ga'].$fontTagRight."</td>";
        //create row for hoshasen ga
        $td_hoshasen_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['hoshasen_ga'].$fontTagRight."</td>";
        //create row for masui ga
        $td_masui_ga.="<td style='".$statusColor."text-align : right;'>".$fontTagLeft.$resultArr[$i]['masui_ga'].$fontTagRight."</td>";
        //create row for total
        $td_total.="<td style='".$statusColor."text-align : right;'>=sum(".$excelColumns[$i+1].NAIKA_GA.":".$excelColumns[$i+1].MASUI_GA.")</td>";
        //create row for average using (total/no. of days in month)
        $td_avg.="<td style='".$statusColor."text-align : right; ".$msoNumberFormat."'>".$fontTagLeft."=ROUND(".$excelColumns[$i+1].TOTAL."/".($resultArr[$i]['countstatus']==0?1:$resultArr[$i]['countstatus']).",1)".$fontTagRight."</td>";
        //Calculate the total as per dept
        $total_naika_ga+=$resultArr[$i]['naika_ga'];
        $total_sinkei_ga+=$resultArr[$i]['sinkei_ga'];
        $total_kokyu_ga+=$resultArr[$i]['kokyu_ga'];
        $total_shoka_ga+=$resultArr[$i]['shoka_ga'];
        $total_junkanki_ga+=$resultArr[$i]['junkanki_ga'];
        $total_shouni_ga+=$resultArr[$i]['shouni_ga'];
        $total_geka_ga+=$resultArr[$i]['geka_ga'];
        $total_seikei_ga+=$resultArr[$i]['seikei_ga'];
        $total_keiseibi_ga+=$resultArr[$i]['keiseibi_ga'];
        $total_nou_ga+=$resultArr[$i]['nou_ga'];
        $total_hifu_ga+=$resultArr[$i]['hifu_ga'];
        $total_hinyo_ga+=$resultArr[$i]['hinyo_ga'];
        $total_sanfu_ga+=$resultArr[$i]['sanfu_ga'];
        $total_ganka_ga+=$resultArr[$i]['ganka_ga'];
        $total_jibi_ga+=$resultArr[$i]['jibi_ga'];
        $total_touseki_ga+=$resultArr[$i]['touseki_ga'];
        $total_seisin_ga+=$resultArr[$i]['seisin_ga'];
        $total_sika_ga+=$resultArr[$i]['sika_ga'];
        $total_hoshasen_ga+=$resultArr[$i]['hoshasen_ga'];
        $total_masui_ga+=$resultArr[$i]['masui_ga'];

        $total_count+=$resultArr[$i]['countstatus'];
    }

    //Set color based on color flag
    switch($colorFlag){
        case 0:
            $totalCellColor=$statusWhiteColor;
            break;
        case 1:
            $totalCellColor=$statusYellowColor;
            break;
        case 2:
            $totalCellColor=$statusRedColor;
            break;

    }
    //Assemble the rows
    $table.='<table border="1" class="list" width="100%">';
    $table.='<tr><td style="'.$thColor.'text-align : center; "></td>'.$thArr.'<td style="'.$thColor.'text-align : center; ">合計</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'内科'.$fontTagRight.'</td>'.$td_naika_ga.'<td style="text-align : right; '.$totalCellColor.' ">'.$fontTagLeft.$this->getSumFormula($excelColumns,NAIKA_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'神経内科'.$fontTagRight.'</td>'.$td_sinkei_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,SINKEI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'呼吸器科'.$fontTagRight.'</td>'.$td_kokyu_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,KOKYU_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'消化器科'.$fontTagRight.'</td>'.$td_shoka_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,SHOKA_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'循環器科'.$fontTagRight.'</td>'.$td_junkanki_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,JUNKANKI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'小児科'.$fontTagRight.'</td>'.$td_shouni_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,SHOUNI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'外科'.$fontTagRight.'</td>'.$td_geka_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,GEKA_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'整形外科'.$fontTagRight.'</td>'.$td_seikei_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,SEIKEI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'形成（美）外科'.$fontTagRight.'</td>'.$td_keiseibi_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,KEISEIBI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'脳神経外科'.$fontTagRight.'</td>'.$td_nou_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,NOU_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'皮膚科'.$fontTagRight.'</td>'.$td_hifu_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,HIFU_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'泌尿器科'.$fontTagRight.'</td>'.$td_hinyo_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,HINYO_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'産婦人科'.$fontTagRight.'</td>'.$td_sanfu_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,SANFU_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'眼科'.$fontTagRight.'</td>'.$td_ganka_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,GANKA_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'耳鼻咽喉科'.$fontTagRight.'</td>'.$td_jibi_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,JIBI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'透析科'.$fontTagRight.'</td>'.$td_touseki_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,TOUSEKI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'精神科'.$fontTagRight.'</td>'.$td_seisin_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,SEISIN_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'歯科'.$fontTagRight.'</td>'.$td_sika_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,SIKA_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'放射線科'.$fontTagRight.'</td>'.$td_hoshasen_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,HOSHASEN_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'麻酔科'.$fontTagRight.'</td>'.$td_masui_ga.'<td style="text-align : right; '.$totalCellColor.'">'.$fontTagLeft.$this->getSumFormula($excelColumns,MASUI_GA,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'合計'.$fontTagRight.'</td>'.$td_total.'<td  style="'.$totalCellColor.'text-align : right;">'.$fontTagLeft.$this->getSumFormula($excelColumns,TOTAL,1,count($resultArr)).$fontTagRight.'</td></tr>';
    $table.='<tr><td style="'.$thColor.'text-align : center; ">'.$fontTagLeft.'一日平均'.$fontTagRight.'</td>'.$td_avg.'<td style="'.$totalCellColor.'text-align : right; '.$msoNumberFormat.'">'.$fontTagLeft."=ROUND((".$excelColumns[count($resultArr)+1].TOTAL."/".$total_count."),1)".$fontTagRight.'</td></tr>';
    $table.='</table>';
    return $table;



     }


    /**
     * 外来診療科別患者月報
     * This method get the display for months report
     * @param $resultArr
     * @param $dateMonth
     * @param $dateYear
     * @return unknown_type
     */
    function getDeptOPDMonthsReportDisplay($resultArr,$dateMonth,$dateYear)
    {
        $table=$this->getDeptIPDOPDDisplay($resultArr,$dateMonth,$dateYear);
        return $table;
    }

     /**
     * Select for month is created
     */
    function getSelectDislpayMonth($arr)
     {
        $displayMonth = $this->getArrValueByKey('display_month', $arr);
        $category     = $this->getArrValueByKey('category',      $arr);
        $subCategory  = $this->getArrValueByKey('sub_category',  $arr);
        $no           = $this->getArrValueByKey('no',            $arr);
        $type         = $this->getArrValueByKey('type',          $arr);
        $plant        = $this->getArrValueByKey('plant ',        $arr);

        $data = $this->getIndicatorData();
        $selectData = $data->getSelectMonth();
        $rtnStr = sprintf('<select id="display_month" name="display_month" onchange="selectMonth(this, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" >', $category, $subCategory, $no, $type, $plant);
        foreach ($selectData as $val) {
            $strMonth=$val['month']<10?"0".$val['month']:$val['month'];
            if ($displayMonth == $val['month']) {
                $rtnStr .= sprintf('<option value="%s" selected="selected" >%s</option>', $strMonth, $val['month']);
            }
            else {
                $rtnStr .= sprintf('<option value="%s" >%s</option>', $strMonth, $val['month']);
            }
        }
        $rtnStr .= '</select>';

        return $rtnStr;
    }
   function outputExcelDeptOPDMonthReport($resultArr,$dateMonth,$dateYear,$arr){
    return $this->outputExcelIPDOPDReport($resultArr,$dateMonth,$dateYear);

    }




  /**
     *
     * This method get the display for dept IPD months report
     *
     * @param $resultArr
     * @return string
     */
     function getDeptIPDMonthsReportDisplay($resultArr,$dateMonth,$dateYear){
        return $this->getDeptIPDOPDDisplay($resultArr,$dateMonth,$dateYear);
      }
    function outputExcelDeptIPDMonthReport($resultArr,$dateMonth,$dateYear){
    return $this->outputExcelIPDOPDReport($resultArr,$dateMonth,$dateYear);
     }


    /**
     * 入院診療科別患者月報
     * @param $resultArr
     * @param $dateMonth
     * @param $dateYear
     * @return unknown_type
     */
    function getDiseasewiseOPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear)
     {
    return $this-> getDiseasewiseOPDIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear);

     }
    function getDiseasewiseIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear)
    {
        return $this->getDiseasewiseOPDIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear);
    }


    function getDiseasewiseOPDIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear)
    {
    $indicatorData = $this->getIndicatorData();
    $i=intval($dateYear);
    $prevYear = $i;
    $prepprevYear = $i-1;

    $currentMonth = date('m');
    $currentYear = date('Y');
    //If current month and year is same as selected then
    //get the total days in months as till yesterday
    if($currentMonth===$dateMonth && $currentYear===$dateYear){
        $totalDaysInMonth=date('d')-1;
    }else if(($currentMonth>$dateMonth && $currentYear===$dateYear) || ($currentYear>$dateYear)){
        $totalDaysInMonth=date("j",mktime(0,0,0,$dateMonth+1,0, $dateYear));
    }else{
        $totalDaysInMonth=0;
    }

    $thColorArr   = array('name' => 'background-color', 'value' => '#d2f4fa;'); # 青
    $compColorArr = array('name' => 'background-color', 'value' => '#FFC8ED;'); # 緑
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');
    $statusColorWhiteArr = array('name' => 'background-color', 'value' => '#FFF;');
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');
    $statusColorGreenArr = array('name' => 'background-color', 'value' => '#CFFFCF;');
    $thColor  = $this->getArrValueByKey('name', $thColorArr).   ':'. $this->getArrValueByKey('value', $thColorArr);
    $compColor = $this->getArrValueByKey('name', $compColorArr). ':'. $this->getArrValueByKey('value', $compColorArr);
    $statusRedColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusWhiteColor = $this->getArrValueByKey('name', $statusColorWhiteArr). ':'. $this->getArrValueByKey('value', $statusColorWhiteArr);
    $statusYellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
    $statusGreenColor = $this->getArrValueByKey('name', $statusColorGreenArr). ':'. $this->getArrValueByKey('value', $statusColorGreenArr);

        $dateTable = '<table class="list" width="100%"><tbody>';
        $fontTag = $this->getFontTag();
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

    $statusColor = $statusWhiteColor;
    $statusColorPrev = $statusWhiteColor;
    //$statusColorCurr = $statusWhiteColor;



        $tableHeader = "<td style='".$thColor." text-align: center;'>年</td>";
// Naika ga
      $naikaga="";
    $naikaCurr="";
    $naikaPrev="";
    $naikaDiff="";
    $naikaRatio="";
// Sinkei ga
    $sinkei_ga="";
    $sinkeiCurr="";
    $sinkeiPrev="";
    $sinkeiDiff="";
    $sinkeiRatio="";
// Kokyu ga
    $kokyu_ga="";
    $kokyuCurr="";
    $kokyuPrev="";
    $kokyuDiff="";
    $kokyuRatio="";

// Shoka ga
    $shoka_ga="";
    $shokaCurr="";
    $shokaPrev="";
    $shokaDiff="";
    $shokaRatio="";

// Junkanki ga
    $junkanki_ga="";
    $junkankiCurr="";
    $junkankiPrev="";
    $junkankiDiff="";
    $junkankiRatio="";

// Shouni Ga
    $shouni_ga="";
    $shouniCurr="";
    $shouniPrev="";
    $shouniDiff="";
    $shouniRatio="";

// Geka Ga
    $geka_ga="";
    $gekaCurr="";
    $gekaPrev="";
    $gekaDiff="";
    $gekaRatio="";

// Seikei Ga
    $seikei_ga="";
    $seikeiCurr="";
    $seikeiPrev="";
    $seikeiDiff="";
    $seikeiRatio="";

// Keiseibi Ga
    $keiseibi_ga="";
    $keiseibiCurr="";
    $keiseibiPrev="";
    $keiseibiDiff="";
    $keiseibiRatio="";

// Nou Ga
    $nou_ga="";
    $nouCurr="";
    $nouPrev="";
    $nouDiff="";
    $nouRatio="";

// Hifu Ga
    $hifu_ga="";
    $hifuCurr="";
    $hifuPrev="";
    $hifuDiff="";
    $hifuRatio="";

// Hinyo Ga
    $hinyo_ga="";
    $hinyoCurr="";
    $hinyoPrev="";
    $hinyoDiff="";
    $hinyoRatio="";

// Sanfu Ga
    $sanfu_ga="";
    $sanfuCurr="";
    $sanfuPrev="";
    $sanfuDiff="";
    $sanfuRatio="";

// Ganka Ga
    $ganka_ga="";
    $gankaCurr="";
    $gankaPrev="";
    $gankaDiff="";
    $gankaRatio="";

// Jibi Ga
    $jibi_ga="";
    $jibiCurr="";
    $jibiPrev="";
    $jibiDiff="";
    $jibiRatio="";

// Touseki Ga
    $touseki_ga="";
    $tousekiCurr="";
    $tousekiPrev="";
    $tousekiDiff="";
    $tousekiRatio="";

// Seisin Ga
    $seisin_ga="";
    $seisinCurr="";
    $seisinPrev="";
    $seisinDiff="";
    $seisinRatio="";

// Sika Ga
    $sika_ga="";
    $sikaCurr="";
    $sikaPrev="";
    $sikaDiff="";
    $sikaRatio="";

// Hoshasen Ga
    $hoshasen_ga="";
    $hoshasenCurr="";
    $hoshasenPrev="";
    $hoshasenDiff="";
    $hoshasenRatio="";

// Masui Ga
    $masui_ga="";
    $masuiCurr="";
    $masuiPrev="";
    $masuiDiff="";
    $masuiRatio="";

// Td total
    $total_td_curr="";
    $total_td_prev="";
    $total_td_diff="";
    $total_td_ratio="";

// Total Prev Year
    $total_naika_ga_curr=0;
    $total_sinkei_ga_curr=0;
    $total_kokyu_ga_curr=0;
    $total_shoka_ga_curr=0;
    $total_junkanki_ga_curr=0;
    $total_shouni_ga_curr=0;
    $total_geka_ga_curr=0;
    $total_seikei_ga_curr=0;
    $total_keiseibi_ga_curr=0;
    $total_nou_ga_curr=0;
    $total_hifu_ga_curr=0;
    $total_hinyo_ga_curr=0;
    $total_sanfu_ga_curr=0;
    $total_ganka_ga_curr=0;
    $total_jibi_ga_curr=0;
    $total_touseki_ga_curr=0;
    $total_seisin_ga_curr=0;
    $total_sika_ga_curr=0;
    $total_hoshasen_ga_curr=0;
    $total_masui_ga_curr=0;


// Total prev prev year

    $total_naika_ga_prev=0;
    $total_sinkei_ga_prev=0;
    $total_kokyu_ga_prev=0;
    $total_shoka_ga_prev=0;
    $total_junkanki_ga_prev=0;
    $total_shouni_ga_prev=0;
    $total_geka_ga_prev=0;
    $total_seikei_ga_prev=0;
    $total_keiseibi_ga_prev=0;
    $total_nou_ga_prev=0;
    $total_hifu_ga_prev=0;
    $total_hinyo_ga_prev=0;
    $total_sanfu_ga_prev=0;
    $total_ganka_ga_prev=0;
    $total_jibi_ga_prev=0;
    $total_touseki_ga_prev=0;
    $total_seisin_ga_prev=0;
    $total_sika_ga_prev=0;
    $total_hoshasen_ga_prev=0;
    $total_masui_ga_prev=0;

 $final_total_curr=0;
 $final_total_prev=0;
 $final_total_diff=0;
 $final_total_ratio=0;

    $colorFlagCurr=0;
    $colorFlagPrev=0;
    $totalCellColorCurr="";
    $totalCellColorPrev="";
    $totalRowColorCurr="";
    $totalRowColorPrev="";

    for($i=0;$i<count($resultArr);$i++){



      $tableHeader .= "<td style='".$thColor." text-align: center;'>".$fontTagLeft.$resultArr[$i]['jnl_facility_name'].$fontTagRight."</td>";
    }
$tableHeader .= "<td style='".$thColor." text-align: center;'>".$fontTagLeft."合計".$fontTagRight."</td>";
$colorTotalCurr=0;
$colorTotalPrev=0;
$colorFlagTotalCurr = 0;
$colorFlagTotalPrev = 0;
    for($i=0;$i<count($resultArr);$i++){
    $colorTotalCurr = $colorTotalCurr +     $resultArr[$i]['naika_ga_c'] +
    $resultArr[$i]['sinkei_ga_c'] +    $resultArr[$i]['kokyu_ga_c'] +
    $resultArr[$i]['shoka_ga_c'] +    $resultArr[$i]['junkanki_ga_c'] +
    $resultArr[$i]['shouni_ga_c'] +    $resultArr[$i]['geka_ga_c'] +
    $resultArr[$i]['seikei_ga_c'] +    $resultArr[$i]['keiseibi_ga_c'] +
    $resultArr[$i]['nou_ga_c'] +    $resultArr[$i]['hifu_ga_c'] +
    $resultArr[$i]['hinyo_ga_c'] +    $resultArr[$i]['sanfu_ga_c'] +
    $resultArr[$i]['ganka_ga_c'] +    $resultArr[$i]['jibi_ga_c'] +
    $resultArr[$i]['touseki_ga_c'] + $resultArr[$i]['seisin_ga_c'] +
    $resultArr[$i]['sika_ga_c'] +    $resultArr[$i]['hoshasen_ga_c'] +
    $resultArr[$i]['masui_ga_c'] ;
    $colorTotalPrev = $colorTotalPrev +     $resultArr[$i]['naika_ga_p'] +
    $resultArr[$i]['sinkei_ga_p'] +    $resultArr[$i]['kokyu_ga_p'] +
    $resultArr[$i]['shoka_ga_p'] +    $resultArr[$i]['junkanki_ga_p'] +
    $resultArr[$i]['shouni_ga_p'] +    $resultArr[$i]['geka_ga_p'] +
    $resultArr[$i]['seikei_ga_p'] +    $resultArr[$i]['keiseibi_ga_p'] +
    $resultArr[$i]['nou_ga_p'] +    $resultArr[$i]['hifu_ga_p'] +
    $resultArr[$i]['hinyo_ga_p'] +    $resultArr[$i]['sanfu_ga_p'] +
    $resultArr[$i]['ganka_ga_p'] +    $resultArr[$i]['jibi_ga_p'] +
    $resultArr[$i]['touseki_ga_p'] + $resultArr[$i]['seisin_ga_p'] +
    $resultArr[$i]['sika_ga_p'] +    $resultArr[$i]['hoshasen_ga_p'] +
    $resultArr[$i]['masui_ga_p'] ;
    }


     for($i=0;$i<count($resultArr);$i++){

    $statusColor = $statusWhiteColor;
    $statusColorPrev = $statusWhiteColor;
    //$statusColorCurr = $statusWhiteColor;

    $countResultPrev = $indicatorData->getTotalForMonthInYear($resultArr[$i]['jnl_facility_id'], $dateMonth, $prevYear);

    $deptCountPrev = $countResultPrev[0]['deptcount'];

    $countResultPrevPrev = $indicatorData->getTotalForMonthInYear($resultArr[$i]    ['jnl_facility_id'],$dateMonth, $prepprevYear);
    $deptCountPrevPrev = $countResultPrevPrev[0]['deptcount'];

$td_total_curr = 0;
$td_total_prev = 0;
$colorFlagCurr = 0;
$colorFlagPrev = 0;

        //Check the jnl status and set the background color for cell
        if($resultArr[$i]['jnl_status_c']==0){
            $statusColor=$statusYellowColor;
            $statusColorCurr = $statusYellowColor;
            if($colorFlagCurr==0)
            {
                $colorFlagCurr=1;
                $colorFlagTotalCurr = 1;
            }
            if( $deptCountPrev < $totalDaysInMonth )
            {
                $colorFlagCurr=2;
                $colorFlagTotalCurr = 2;
                $statusColorCurr=$statusRedColor;
                $statusColor=$statusRedColor;

            }

        }else if( $deptCountPrev < $totalDaysInMonth ){
            $colorFlagCurr=2;
            $colorFlagTotalCurr = 2;
            $statusColorCurr=$statusRedColor;
            $statusColor=$statusRedColor;

        }
        else {
            $statusColorCurr=$statusWhiteColor;
            $statusColor=$statusWhiteColor;
        }



    switch($colorFlagTotalCurr){
        case 0:
            $totalCellColorCurr=$statusWhiteColor;
            break;
        case 1:
            $totalCellColorCurr=$statusYellowColor;
            break;
        case 2:
            $totalCellColorCurr=$statusRedColor;
            break;

    }
    switch($colorFlagPrev){
        case 0:
            $totalCellColorPrev=$statusWhiteColor;
            break;
        case 1:
            $totalCellColorPrev=$statusYellowColor;
            break;
        case 2:
            $totalCellColorPrev=$statusRedColor;
            break;

    }
    switch($colorFlagTotalCurr){
        case 0:
            $totalRowColorCurr=$statusWhiteColor;
            break;
        case 1:
            $totalRowColorCurr=$statusYellowColor;
            break;
        case 2:
            $totalRowColorCurr=$statusRedColor;
            break;

    }
    switch($colorFlagTotalPrev){
        case 0:
            $totalRowColorPrev=$statusWhiteColor;
            break;
        case 1:
            $totalRowColorPrev=$statusYellowColor;
            break;
        case 2:
            $totalRowColorPrev=$statusRedColor;
            break;

    }


//    if($colorTotalCurr==0){
//    $statusColorCurr = $statusRedColor;
//    $totalCellColorCurr = $statusRedColor;
//    $totalRowColorCurr=$statusRedColor;
//
//    }

    if($colorTotalPrev==0){
    $statusColorPrev = $statusRedColor;
    $totalCellColorPrev = $statusRedColor;
    $totalRowColorPrev=$statusRedColor;

    }
    //Cell bg color
    //$statusColorCurr= $statusWhiteColor;
    $statusColorPrev= $statusWhiteColor;
    //Total Cell bg color in row
    //$totalRowColorCurr=$statusWhiteColor;
    $totalRowColorPrev=$statusWhiteColor;
    //Total Cell bg color in column
    //$totalCellColorCurr=$statusWhiteColor;
    $totalCellColorPrev=$statusWhiteColor;

    // Naika ga
    $naikaCurr.="<td style='".$statusColorCurr."' align='right'>".$fontTagLeft.$resultArr[$i]['naika_ga_c'].$fontTagRight."</td>";
    $naikaPrev.="<td style='  text-align : right;".$statusColorPrev."'>".$fontTagLeft.$resultArr[$i]['naika_ga_p'].$fontTagRight."</td>";
    $naikaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['naika_ga_c']-$resultArr[$i]['naika_ga_p']).$fontTagRight."</td>";
    $naikaRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['naika_ga_c']-$resultArr[$i]['naika_ga_p'])/$resultArr[$i]['naika_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_naika_ga_curr = $total_naika_ga_curr + $resultArr[$i]['naika_ga_c'];
$total_naika_ga_prev = $total_naika_ga_prev + $resultArr[$i]['naika_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['naika_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['naika_ga_p'];


    // Sinkei Ga
    $sinkeiCurr.="<td style='text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['sinkei_ga_c'].$fontTagRight."</td>";
    $sinkeiPrev.="<td style=' text-align : right; ".$statusColorPrev."'>".$fontTagLeft.$resultArr[$i]['sinkei_ga_p'].$fontTagRight."</td>";
    $sinkeiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['sinkei_ga_c']-$resultArr[$i]['sinkei_ga_p']).$fontTagRight."</td>";
    $sinkeiRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['sinkei_ga_c']-$resultArr[$i]['sinkei_ga_p'])/$resultArr[$i]['sinkei_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_sinkei_ga_curr = $total_sinkei_ga_curr + $resultArr[$i]['sinkei_ga_c'];
$total_sinkei_ga_prev = $total_sinkei_ga_prev + $resultArr[$i]['sinkei_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['sinkei_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['sinkei_ga_p'];


    // Kokyu Ga
    $kokyuCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['kokyu_ga_c'].$fontTagRight."</td>";
    $kokyuPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['kokyu_ga_p'].$fontTagRight."</td>";
    $kokyuDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['kokyu_ga_c']-$resultArr[$i]['kokyu_ga_p']).$fontTagRight."</td>";
    $kokyuRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['kokyu_ga_c']-$resultArr[$i]['kokyu_ga_p'])/$resultArr[$i]['kokyu_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_kokyu_ga_curr = $total_kokyu_ga_curr + $resultArr[$i]['kokyu_ga_c'];
$total_kokyu_ga_prev = $total_kokyu_ga_prev + $resultArr[$i]['kokyu_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['kokyu_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['kokyu_ga_p'];

    // Shoka Ga
    $shokaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['shoka_ga_c'].$fontTagRight."</td>";
    $shokaPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['shoka_ga_p'].$fontTagRight."</td>";
    $shokaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['shoka_ga_c']-$resultArr[$i]['shoka_ga_p']).$fontTagRight."</td>";
    $shokaRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['shoka_ga_c']-$resultArr[$i]['shoka_ga_p'])/$resultArr[$i]['shoka_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_shoka_ga_curr = $total_shoka_ga_curr + $resultArr[$i]['shoka_ga_c'];
$total_shoka_ga_prev = $total_shoka_ga_prev + $resultArr[$i]['shoka_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['shoka_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['shoka_ga_p'];

    // Junkanki Ga
    $junkankiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['junkanki_ga_c'].$fontTagRight."</td>";
    $junkankiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['junkanki_ga_p'].$fontTagRight."</td>";
    $junkankiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['junkanki_ga_c']-$resultArr[$i]['junkanki_ga_p']).$fontTagRight."</td>";
    $junkankiRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['junkanki_ga_c']-$resultArr[$i]['junkanki_ga_p'])/$resultArr[$i]['junkanki_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_junkanki_ga_curr = $total_junkanki_ga_curr + $resultArr[$i]['junkanki_ga_c'];
$total_junkanki_ga_prev = $total_junkanki_ga_prev + $resultArr[$i]['junkanki_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['junkanki_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['junkanki_ga_p'];

    // Shouni Ga
        $shouniCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['shouni_ga_c'].$fontTagRight."</td>";
        $shouniPrev.="<td style='text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['shouni_ga_p'].$fontTagRight."</td>";
        $shouniDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['shouni_ga_c']-$resultArr[$i]['shouni_ga_p']).$fontTagRight."</td>";
        $shouniRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['shouni_ga_c']-$resultArr[$i]['shouni_ga_p'])/$resultArr[$i]['shouni_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_shouni_ga_curr = $total_shouni_ga_curr + $resultArr[$i]['shouni_ga_c'];
$total_shouni_ga_prev = $total_shouni_ga_prev + $resultArr[$i]['shouni_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['shouni_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['shouni_ga_p'];

    // Geka Ga
        $gekaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['geka_ga_c'].$fontTagRight."</td>";
        $gekaPrev.="<td style=' text-align : right; ".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['geka_ga_p'].$fontTagRight."</td>";
        $gekaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['geka_ga_c']-$resultArr[$i]['geka_ga_p']).$fontTagRight."</td>";
        $gekaRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['geka_ga_c']-$resultArr[$i]['geka_ga_p'])/$resultArr[$i]['geka_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_geka_ga_curr = $total_geka_ga_curr + $resultArr[$i]['geka_ga_c'];
$total_geka_ga_prev = $total_geka_ga_prev + $resultArr[$i]['geka_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['geka_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['geka_ga_p'];

    // Seikei Ga
        $seikeiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['seikei_ga_c'].$fontTagRight."</td>";
        $seikeiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['seikei_ga_p'].$fontTagRight."</td>";
        $seikeiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['seikei_ga_c']-$resultArr[$i]['seikei_ga_p']).$fontTagRight."</td>";
        $seikeiRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['seikei_ga_c']-$resultArr[$i]['seikei_ga_p'])/$resultArr[$i]['seikei_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_seikei_ga_curr = $total_seikei_ga_curr + $resultArr[$i]['seikei_ga_c'];
$total_seikei_ga_prev = $total_seikei_ga_prev + $resultArr[$i]['seikei_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['seikei_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['seikei_ga_p'];


    // Keiseibi Ga
        $keiseibiCurr.="<td style=' text-align : right; ".$statusColorCurr."'>".$fontTagLeft.$resultArr[$i]['keiseibi_ga_c'].$fontTagRight."</td>";
        $keiseibiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['keiseibi_ga_p'].$fontTagRight."</td>";
        $keiseibiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['keiseibi_ga_c']-$resultArr[$i]['keiseibi_ga_p']).$fontTagRight."</td>";
        $keiseibiRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['keiseibi_ga_c']-$resultArr[$i]['keiseibi_ga_p'])/$resultArr[$i]['keiseibi_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_keiseibi_ga_curr = $total_keiseibi_ga_curr + $resultArr[$i]['keiseibi_ga_c'];
$total_keiseibi_ga_prev = $total_keiseibi_ga_prev + $resultArr[$i]['keiseibi_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['keiseibi_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['keiseibi_ga_p'];


    // Nou Ga
        $nouCurr.="<td style=' text-align : right; ".$statusColorCurr."'>".$fontTagLeft.$resultArr[$i]['nou_ga_c'].$fontTagRight."</td>";
        $nouPrev.="<td style=' text-align : right; ".$statusColorPrev."'>".$fontTagLeft.$resultArr[$i]['nou_ga_p'].$fontTagRight."</td>";
        $nouDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['nou_ga_c']-$resultArr[$i]['nou_ga_p']).$fontTagRight."</td>";
        $nouRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['nou_ga_c']-$resultArr[$i]['nou_ga_p'])/$resultArr[$i]['nou_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_nou_ga_curr = $total_nou_ga_curr + $resultArr[$i]['nou_ga_c'];
$total_nou_ga_prev = $total_nou_ga_prev + $resultArr[$i]['nou_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['nou_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['nou_ga_p'];

    // Hifu Ga
        $hifuCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['hifu_ga_c'].$fontTagRight."</td>";
        $hifuPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['hifu_ga_p'].$fontTagRight."</td>";
        $hifuDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['hifu_ga_c']-$resultArr[$i]['hifu_ga_p']).$fontTagRight."</td>";
        $hifuRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['hifu_ga_c']-$resultArr[$i]['hifu_ga_p'])/$resultArr[$i]['hifu_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_hifu_ga_curr = $total_hifu_ga_curr + $resultArr[$i]['hifu_ga_c'];
$total_hifu_ga_prev = $total_hifu_ga_prev + $resultArr[$i]['hifu_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['hifu_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['hifu_ga_p'];

    // Hinyo Ga
        $hinyoCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['hinyo_ga_c'].$fontTagRight."</td>";
        $hinyoPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['hinyo_ga_p'].$fontTagRight."</td>";
        $hinyoDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['hinyo_ga_c']-$resultArr[$i]['hinyo_ga_p']).$fontTagRight."</td>";
        $hinyoRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['hinyo_ga_c']-$resultArr[$i]['hinyo_ga_p'])/$resultArr[$i]['hinyo_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_hinyo_ga_curr = $total_hinyo_ga_curr + $resultArr[$i]['hinyo_ga_c'];
$total_hinyo_ga_prev = $total_hinyo_ga_prev + $resultArr[$i]['hinyo_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['hinyo_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['hinyo_ga_p'];

    // Sanfu Ga
        $sanfuCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['sanfu_ga_c'].$fontTagRight."</td>";
        $sanfuPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['sanfu_ga_p'].$fontTagRight."</td>";
        $sanfuDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['sanfu_ga_c']-$resultArr[$i]['sanfu_ga_p']).$fontTagRight."</td>";
        $sanfuRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['sanfu_ga_c']-$resultArr[$i]['sanfu_ga_p'])/$resultArr[$i]['sanfu_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_sanfu_ga_curr = $total_sanfu_ga_curr + $resultArr[$i]['sanfu_ga_c'];
$total_sanfu_ga_prev = $total_sanfu_ga_prev + $resultArr[$i]['sanfu_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['sanfu_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['sanfu_ga_p'];

    // Ganka Ga
        $gankaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['ganka_ga_c'].$fontTagRight."</td>";
        $gankaPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['ganka_ga_p'].$fontTagRight."</td>";
        $gankaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['ganka_ga_c']-$resultArr[$i]['ganka_ga_p']).$fontTagRight."</td>";
        $gankaRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['ganka_ga_c']-$resultArr[$i]['ganka_ga_p'])/$resultArr[$i]['ganka_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_ganka_ga_curr = $total_ganka_ga_curr + $resultArr[$i]['ganka_ga_c'];
$total_ganka_ga_prev = $total_ganka_ga_prev + $resultArr[$i]['ganka_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['ganka_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['ganka_ga_p'];


    // Jibi Ga
        $jibiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['jibi_ga_c'].$fontTagRight."</td>";
        $jibiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['jibi_ga_p'].$fontTagRight."</td>";
        $jibiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['jibi_ga_c']-$resultArr[$i]['jibi_ga_p']).$fontTagRight."</td>";
        $jibiRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['jibi_ga_c']-$resultArr[$i]['jibi_ga_p'])/$resultArr[$i]['jibi_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_jibi_ga_curr = $total_jibi_ga_curr + $resultArr[$i]['jibi_ga_c'];
$total_jibi_ga_prev = $total_jibi_ga_prev + $resultArr[$i]['jibi_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['jibi_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['jibi_ga_p'];

    // Touseki Ga
        $tousekiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['touseki_ga_c'].$fontTagRight."</td>";
        $tousekiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['touseki_ga_p'].$fontTagRight."</td>";
        $tousekiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['touseki_ga_c']-$resultArr[$i]['touseki_ga_p']).$fontTagRight."</td>";
        $tousekiRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['touseki_ga_c']-$resultArr[$i]['touseki_ga_p'])/$resultArr[$i]['touseki_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_touseki_ga_curr = $total_touseki_ga_curr + $resultArr[$i]['touseki_ga_c'];
$total_touseki_ga_prev = $total_touseki_ga_prev + $resultArr[$i]['touseki_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['touseki_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['touseki_ga_p'];

    // Seisin Ga
        $seisinCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['seisin_ga_c'].$fontTagRight."</td>";
        $seisinPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['seisin_ga_p'].$fontTagRight."</td>";
        $seisinDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['seisin_ga_c']-$resultArr[$i]['seisin_ga_p']).$fontTagRight."</td>";
        $seisinRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['seisin_ga_c']-$resultArr[$i]['seisin_ga_p'])/$resultArr[$i]['seisin_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_seisin_ga_curr = $total_seisin_ga_curr + $resultArr[$i]['seisin_ga_c'];
$total_seisin_ga_prev = $total_seisin_ga_prev + $resultArr[$i]['seisin_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['seisin_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['seisin_ga_p'];

    // Sika Ga
        $sikaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['sika_ga_c'].$fontTagRight."</td>";
        $sikaPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['sika_ga_p'].$fontTagRight."</td>";
        $sikaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['sika_ga_c']-$resultArr[$i]['sika_ga_p']).$fontTagRight."</td>";
        $sikaRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['sika_ga_c']-$resultArr[$i]['sika_ga_p'])/$resultArr[$i]['sika_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_sika_ga_curr = $total_sika_ga_curr + $resultArr[$i]['sika_ga_c'];
$total_sika_ga_prev = $total_sika_ga_prev + $resultArr[$i]['sika_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['sika_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['sika_ga_p'];

    // Hoshasen Ga
        $hoshasenCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['hoshasen_ga_c'].$fontTagRight."</td>";
        $hoshasenPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['hoshasen_ga_p'].$fontTagRight."</td>";
        $hoshasenDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['hoshasen_ga_c']-$resultArr[$i]['hoshasen_ga_p']).$fontTagRight."</td>";
        $hoshasenRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['hoshasen_ga_c']-$resultArr[$i]['hoshasen_ga_p'])/$resultArr[$i]['hoshasen_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_hoshasen_ga_curr = $total_hoshasen_ga_curr + $resultArr[$i]['hoshasen_ga_c'];
$total_hoshasen_ga_prev = $total_hoshasen_ga_prev + $resultArr[$i]['hoshasen_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['hoshasen_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['hoshasen_ga_p'];

    // Masui Ga
        $masuiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['masui_ga_c'].$fontTagRight."</td>";
        $masuiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['masui_ga_p'].$fontTagRight."</td>";
        $masuiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($resultArr[$i]['masui_ga_c']-$resultArr[$i]['masui_ga_p']).$fontTagRight."</td>";
        $masuiRatio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($resultArr[$i]['masui_ga_c']-$resultArr[$i]['masui_ga_p'])/$resultArr[$i]['masui_ga_p'])*100),1))."%".$fontTagRight."</td>";
$total_masui_ga_curr = $total_masui_ga_curr + $resultArr[$i]['masui_ga_c'];
$total_masui_ga_prev = $total_masui_ga_prev + $resultArr[$i]['masui_ga_p'];
$td_total_curr = $td_total_curr + $resultArr[$i]['masui_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['masui_ga_p'];


    $total_td_curr.="<td style=' text-align : right;".$totalCellColorCurr." '>".$fontTagLeft.$td_total_curr.$fontTagRight."</td>";
    $total_td_prev.="<td style=' text-align : right;".$totalCellColorPrev." '>".$fontTagLeft.$td_total_prev.$fontTagRight."</td>";
$total_td_diff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($td_total_curr-$td_total_prev).$fontTagRight."</td>";
        $total_td_ratio.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($td_total_curr-$td_total_prev)/$td_total_prev)*100),1))."%".$fontTagRight."</td>";


$final_total_curr=$final_total_curr+ $td_total_curr;
$final_total_prev=$final_total_prev+ $td_total_prev;



}

// Naika Ga

$naikaga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."内科".$fontTagRight."</td><td style='
text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$naikaga.= $naikaCurr;
$naikaga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_naika_ga_curr.$fontTagRight."</td>";
$naikaga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$naikaga.= $naikaPrev;
$naikaga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_naika_ga_prev.$fontTagRight."</td>";
$naikaga.="</tr><tr><td style='".$statusGreenColor." text-align : center; white-space: nowrap; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$naikaga.= $naikaDiff;
$naikaga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_naika_ga_curr-$total_naika_ga_prev).$fontTagRight."</td>";
$naikaga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$naikaga.= $naikaRatio;
$naikaga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_naika_ga_curr-$total_naika_ga_prev)/$total_naika_ga_prev)*100),1))."%".$fontTagRight."</td>";
$naikaga.= "</tr>";


// Sinkei Ga
$sinkei_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."神経内科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$sinkei_ga.= $sinkeiCurr;
$sinkei_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_sinkei_ga_curr.$fontTagRight."</td>";
$sinkei_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$sinkei_ga.= $sinkeiPrev;
$sinkei_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_sinkei_ga_prev.$fontTagRight."</td>";
$sinkei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$sinkei_ga.= $sinkeiDiff;
$sinkei_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_sinkei_ga_curr-$total_sinkei_ga_prev).$fontTagRight."</td>";
$sinkei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$sinkei_ga.= $sinkeiRatio;
$sinkei_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_sinkei_ga_curr-$total_sinkei_ga_prev)/$total_sinkei_ga_prev)*100),1))."%".$fontTagRight."</td>";
$sinkei_ga.= "</tr>";

// Kokyu Ga
$kokyu_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."呼吸器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$kokyu_ga.= $kokyuCurr;
$kokyu_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_kokyu_ga_curr.$fontTagRight."</td>";
$kokyu_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$kokyu_ga.= $kokyuPrev;
$kokyu_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_kokyu_ga_prev.$fontTagRight."</td>";
$kokyu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$kokyu_ga.= $kokyuDiff;
$kokyu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_kokyu_ga_curr-$total_kokyu_ga_prev).$fontTagRight."</td>";
$kokyu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$kokyu_ga.= $kokyuRatio;
$kokyu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_kokyu_ga_curr-$total_kokyu_ga_prev)/$total_kokyu_ga_prev)*100),1))."%".$fontTagRight."</td>";
$kokyu_ga.= "</tr>";


// Shoka Ga
$shoka_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."消化器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$shoka_ga.= $shokaCurr;
$shoka_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_shoka_ga_curr.$fontTagRight."</td>";
$shoka_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$shoka_ga.= $shokaPrev;
$shoka_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_shoka_ga_prev.$fontTagRight."</td>";
$shoka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$shoka_ga.= $shokaDiff;
$shoka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_shoka_ga_curr-$total_shoka_ga_prev).$fontTagRight."</td>";
$shoka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$shoka_ga.= $shokaRatio;
$shoka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_shoka_ga_curr-$total_shoka_ga_prev)/$total_shoka_ga_prev)*100),1))."%".$fontTagRight."</td>";
$shoka_ga.= "</tr>";

// Junkanki Ga
$junkanki_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."循環器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$junkanki_ga.= $junkankiCurr;
$junkanki_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_junkanki_ga_curr.$fontTagRight."</td>";
$junkanki_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$junkanki_ga.= $junkankiPrev;
$junkanki_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_junkanki_ga_prev.$fontTagRight."</td>";
$junkanki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$junkanki_ga.= $junkankiDiff;
$junkanki_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_junkanki_ga_curr-$total_junkanki_ga_prev).$fontTagRight."</td>";
$junkanki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$junkanki_ga.= $junkankiRatio;
$junkanki_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_junkanki_ga_curr-$total_junkanki_ga_prev)/$total_junkanki_ga_prev)*100),1))."%".$fontTagRight."</td>";
$junkanki_ga.= "</tr>";

// Shouni Ga
$shouni_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."小児科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$shouni_ga.= $shouniCurr;
$shouni_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_shouni_ga_curr.$fontTagRight."</td>";
$shouni_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$shouni_ga.= $shouniPrev;
$shouni_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_shouni_ga_prev.$fontTagRight."</td>";
$shouni_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$shouni_ga.= $shouniDiff;
$shouni_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_shouni_ga_curr-$total_shouni_ga_prev).$fontTagRight."</td>";
$shouni_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$shouni_ga.= $shouniRatio;
$shouni_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_shouni_ga_curr-$total_shouni_ga_prev)/$total_shouni_ga_prev)*100),1))."%".$fontTagRight."</td>";
$shouni_ga.= "</tr>";

// Geka Ga
$geka_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."外科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$geka_ga.= $gekaCurr;
$geka_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_geka_ga_curr.$fontTagRight."</td>";
$geka_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$geka_ga.= $gekaPrev;
$geka_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_geka_ga_prev.$fontTagRight."</td>";
$geka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$geka_ga.= $gekaDiff;
$geka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_geka_ga_curr-$total_geka_ga_prev).$fontTagRight."</td>";
$geka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$geka_ga.= $gekaRatio;
$geka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_geka_ga_curr-$total_geka_ga_prev)/$total_geka_ga_prev)*100),1))."%".$fontTagRight."</td>";
$geka_ga.= "</tr>";

// Seikei Ga
$seikei_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."整形外科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$seikei_ga.= $seikeiCurr;
$seikei_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_seikei_ga_curr.$fontTagRight."</td>";
$seikei_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$seikei_ga.= $seikeiPrev;
$seikei_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_seikei_ga_prev.$fontTagRight."</td>";
$seikei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$seikei_ga.= $seikeiDiff;
$seikei_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_seikei_ga_curr-$total_seikei_ga_prev).$fontTagRight."</td>";
$seikei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$seikei_ga.= $seikeiRatio;
$seikei_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_seikei_ga_curr-$total_seikei_ga_prev)/$total_seikei_ga_prev)*100),1))."%".$fontTagRight."</td>";
$seikei_ga.= "</tr>";

// Keiseibi Ga
$keiseibi_ga.="<tr><td rowspan='4'style='".$thColor." text-align : center; ' >".$fontTagLeft."形成（美）外科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$keiseibi_ga.= $keiseibiCurr;
$keiseibi_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_keiseibi_ga_curr.$fontTagRight."</td>";
$keiseibi_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$keiseibi_ga.= $keiseibiPrev;
$keiseibi_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_keiseibi_ga_prev.$fontTagRight."</td>";
$keiseibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$keiseibi_ga.= $keiseibiDiff;
$keiseibi_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_keiseibi_ga_curr-$total_keiseibi_ga_prev).$fontTagRight."</td>";
$keiseibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$keiseibi_ga.= $keiseibiRatio;
$keiseibi_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_keiseibi_ga_curr-$total_keiseibi_ga_prev)/$total_keiseibi_ga_prev)*100),1))."%".$fontTagRight."</td>";
$keiseibi_ga.= "</tr>";

// Nou Ga
$nou_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."脳神経外科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$nou_ga.= $nouCurr;
$nou_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_nou_ga_curr.$fontTagRight."</td>";
$nou_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$nou_ga.= $nouPrev;
$nou_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_nou_ga_prev.$fontTagRight."</td>";
$nou_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$nou_ga.= $nouDiff;
$nou_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_nou_ga_curr-$total_nou_ga_prev).$fontTagRight."</td>";
$nou_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$nou_ga.= $nouRatio;
$nou_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_nou_ga_curr-$total_nou_ga_prev)/$total_nou_ga_prev)*100),1))."%".$fontTagRight."</td>";
$nou_ga.= "</tr>";

// Hifu Ga
$hifu_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."皮膚科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$hifu_ga.= $hifuCurr;
$hifu_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_hifu_ga_curr.$fontTagRight."</td>";
$hifu_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$hifu_ga.= $hifuPrev;
$hifu_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_hifu_ga_prev.$fontTagRight."</td>";
$hifu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$hifu_ga.= $hifuDiff;
$hifu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_hifu_ga_curr-$total_hifu_ga_prev).$fontTagRight."</td>";
$hifu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$hifu_ga.= $hifuRatio;
$hifu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_hifu_ga_curr-$total_hifu_ga_prev)/$total_hifu_ga_prev)*100),1))."%".$fontTagRight."</td>";
$hifu_ga.= "</tr>";


// Hinyo Ga
$hinyo_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."泌尿器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$hinyo_ga.= $hinyoCurr;
$hinyo_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_hinyo_ga_curr.$fontTagRight."</td>";
$hinyo_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$hinyo_ga.= $hinyoPrev;
$hinyo_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_hinyo_ga_prev.$fontTagRight."</td>";
$hinyo_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$hinyo_ga.= $hinyoDiff;
$hinyo_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_hinyo_ga_curr-$total_hinyo_ga_prev).$fontTagRight."</td>";
$hinyo_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$hinyo_ga.= $hinyoRatio;
$hinyo_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_hinyo_ga_curr-$total_hinyo_ga_prev)/$total_hinyo_ga_prev)*100),1))."%".$fontTagRight."</td>";
$hinyo_ga.= "</tr>";

// Sanfu Ga
$sanfu_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."産婦人科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$sanfu_ga.= $sanfuCurr;
$sanfu_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_sanfu_ga_curr.$fontTagRight."</td>";
$sanfu_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$sanfu_ga.= $sanfuPrev;
$sanfu_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_sanfu_ga_prev.$fontTagRight."</td>";
$sanfu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$sanfu_ga.= $sanfuDiff;
$sanfu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_sanfu_ga_curr-$total_sanfu_ga_prev).$fontTagRight."</td>";
$sanfu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$sanfu_ga.= $sanfuRatio;
$sanfu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_sanfu_ga_curr-$total_sanfu_ga_prev)/$total_sanfu_ga_prev)*100),1))."%".$fontTagRight."</td>";
$sanfu_ga.= "</tr>";

// Ganka Ga
$ganka_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."眼科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$ganka_ga.= $gankaCurr;
$ganka_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_ganka_ga_curr.$fontTagRight."</td>";
$ganka_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$ganka_ga.= $gankaPrev;
$ganka_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_ganka_ga_prev.$fontTagRight."</td>";
$ganka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$ganka_ga.= $gankaDiff;
$ganka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_ganka_ga_curr-$total_ganka_ga_prev).$fontTagRight."</td>";
$ganka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$ganka_ga.= $gankaRatio;
$ganka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_ganka_ga_curr-$total_ganka_ga_prev)/$total_ganka_ga_prev)*100),1))."%".$fontTagRight."</td>";
$ganka_ga.= "</tr>";

// Jibi Ga
$jibi_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."耳鼻咽喉科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$jibi_ga.= $jibiCurr;
$jibi_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_jibi_ga_curr.$fontTagRight."</td>";
$jibi_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$jibi_ga.= $jibiPrev;
$jibi_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_jibi_ga_prev.$fontTagRight."</td>";
$jibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$jibi_ga.= $jibiDiff;
$jibi_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_jibi_ga_curr-$total_jibi_ga_prev).$fontTagRight."</td>";
$jibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$jibi_ga.= $jibiRatio;
$jibi_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_jibi_ga_curr-$total_jibi_ga_prev)/$total_jibi_ga_prev)*100),1))."%".$fontTagRight."</td>";
$jibi_ga.= "</tr>";

// Touseki Ga
$touseki_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."透析科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$touseki_ga.= $tousekiCurr;
$touseki_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_touseki_ga_curr.$fontTagRight."</td>";
$touseki_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$touseki_ga.= $tousekiPrev;
$touseki_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_touseki_ga_prev.$fontTagRight."</td>";
$touseki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$touseki_ga.= $tousekiDiff;
$touseki_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_touseki_ga_curr-$total_touseki_ga_prev).$fontTagRight."</td>";
$touseki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$touseki_ga.= $tousekiRatio;
$touseki_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_touseki_ga_curr-$total_touseki_ga_prev)/$total_touseki_ga_prev)*100),1))."%".$fontTagRight."</td>";
$touseki_ga.= "</tr>";

// Seisin Ga
$seisin_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."精神科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$seisin_ga.= $seisinCurr;
$seisin_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_seisin_ga_curr.$fontTagRight."</td>";
$seisin_ga.="</tr><tr><td style=' text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$seisin_ga.= $seisinPrev;
$seisin_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_seisin_ga_prev.$fontTagRight."</td>";
$seisin_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$seisin_ga.= $seisinDiff;
$seisin_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_seisin_ga_curr-$total_seisin_ga_prev).$fontTagRight."</td>";
$seisin_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$seisin_ga.= $seisinRatio;
$seisin_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_seisin_ga_curr-$total_seisin_ga_prev)/$total_seisin_ga_prev)*100),1))."%".$fontTagRight."</td>";
$seisin_ga.= "</tr>";

// Sika Ga
$sika_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."歯科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$sika_ga.= $sikaCurr;
$sika_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_sika_ga_curr.$fontTagRight."</td>";
$sika_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$sika_ga.= $sikaPrev;
$sika_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_sika_ga_prev.$fontTagRight."</td>";
$sika_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$sika_ga.= $sikaDiff;
$sika_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_sika_ga_curr-$total_sika_ga_prev).$fontTagRight."</td>";
$sika_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$sika_ga.= $sikaRatio;
$sika_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_sika_ga_curr-$total_sika_ga_prev)/$total_sika_ga_prev)*100),1))."%".$fontTagRight."</td>";
$sika_ga.= "</tr>";

// Hoshasen Ga
$hoshasen_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."放射線科".$fontTagLeft."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$hoshasen_ga.= $hoshasenCurr;
$hoshasen_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_hoshasen_ga_curr.$fontTagRight."</td>";
$hoshasen_ga.="</tr><tr><td style=' text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$hoshasen_ga.= $hoshasenPrev;
$hoshasen_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_hoshasen_ga_prev.$fontTagRight."</td>";
$hoshasen_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$hoshasen_ga.= $hoshasenDiff;
$hoshasen_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_hoshasen_ga_curr-$total_hoshasen_ga_prev).$fontTagRight."</td>";
$hoshasen_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$hoshasen_ga.= $hoshasenRatio;
$hoshasen_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_hoshasen_ga_curr-$total_hoshasen_ga_prev)/$total_hoshasen_ga_prev)*100),1))."%".$fontTagRight."</td>";
$hoshasen_ga.= "</tr>";

// Masui Ga
$masui_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."麻酔科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$masui_ga.= $masuiCurr;
$masui_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$total_masui_ga_curr.$fontTagRight."</td>";
$masui_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$masui_ga.= $masuiPrev;
$masui_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$total_masui_ga_prev.$fontTagRight."</td>";
$masui_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$masui_ga.= $masuiDiff;
$masui_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($total_masui_ga_curr-$total_masui_ga_prev).$fontTagRight."</td>";
$masui_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$masui_ga.= $masuiRatio;
$masui_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($total_masui_ga_curr-$total_masui_ga_prev)/$total_masui_ga_prev)*100),1))."%".$fontTagRight."</td>";
$masui_ga.= "</tr>";

// Total Row

$totalRow.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."合計".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
$totalRow.= $total_td_curr;
$totalRow.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$final_total_curr.$fontTagRight."</td>";
$totalRow.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
$totalRow.= $total_td_prev;
$totalRow.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$final_total_prev.$fontTagRight."</td>";
$totalRow.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
$totalRow.= $total_td_diff;
$totalRow.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.($final_total_curr-$final_total_prev).$fontTagRight."</td>";
$totalRow.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
$totalRow.= $total_td_ratio;
$totalRow.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.sprintf('%.1f', round(((($final_total_curr-$final_total_prev)/$final_total_prev)*100),1))."%".$fontTagRight."</td>";
$totalRow.= "</tr>";

    $dateTable.="<tr><td style='".$thColor." text-align : center; '>".$fontTagLeft."診療科".$fontTagRight."</td>".$tableHeader."</tr>";
    $dateTable.=$naikaga;
    $dateTable.=$sinkei_ga;
    $dateTable.=$kokyu_ga;
    $dateTable.=$shoka_ga;
    $dateTable.=$junkanki_ga;
    $dateTable.=$shouni_ga;
    $dateTable.=$geka_ga;
    $dateTable.=$seikei_ga;
    $dateTable.=$keiseibi_ga;
    $dateTable.=$nou_ga;
    $dateTable.=$hifu_ga;
    $dateTable.=$hinyo_ga;
    $dateTable.=$sanfu_ga;
    $dateTable.=$ganka_ga;
    $dateTable.=$jibi_ga;
    $dateTable.=$touseki_ga;
    $dateTable.=$seisin_ga;
    $dateTable.=$sika_ga;
    $dateTable.=$hoshasen_ga;
    $dateTable.=$masui_ga;
    $dateTable.=$totalRow;



    $dateTable.="</tbody></table>";
    return $dateTable  ;

    }

function outputExcelDiseasewiseOPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear){
return $this-> outputExcelDiseasewiseOPDIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear);

}
function outputExcelDiseasewiseIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear){
return $this-> outputExcelDiseasewiseOPDIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear);

}

    /*
    *
    * This method get the excel output for diseasewise OPD IPD  compare report
    *
    * @param $resultArr (SQL query o/p array)
    * @param $dateMonth (selected month integer)
    * @param $dateYear (selected year integer)
    * @return string  (HTML string)
    */
    function outputExcelDiseasewiseOPDIPDCompPrevYearDisplay($resultArr,$dateMonth,$dateYear)
    {
        //Constants for dept indexes in excel sheet
        define('NAIKA_GA',5);
        define('SINKEI_GA',9);
        define('KOKYU_GA',13);
        define('SHOKA_GA',17);
        define('JUNKANKI_GA',21);
        define('SHOUNI_GA',25);
        define('GEKA_GA',29);
        define('SEIKEI_GA',33);
        define('KEISEIBI_GA',37);
        define('NOU_GA',41);
        define('HIFU_GA',45);
        define('HINYO_GA',49);
        define('SANFU_GA',53);
        define('GANKA_GA',57);
        define('JIBI_GA',61);
        define('TOUSEKI_GA',65);
        define('SEISIN_GA',69);
        define('SIKA_GA',73);
        define('HOSHASEN_GA',77);
        define('MASUI_GA',81);
        define('TOTAL',85);
        //Create dept index array
        $depts= array();
        $depts[]=NAIKA_GA;
        $depts[]=SINKEI_GA;
        $depts[]=KOKYU_GA;
        $depts[]=SHOKA_GA;
        $depts[]=JUNKANKI_GA;
        $depts[]=SHOUNI_GA;
        $depts[]=GEKA_GA;
        $depts[]=SEIKEI_GA;
        $depts[]=KEISEIBI_GA;
        $depts[]=NOU_GA;
        $depts[]=HIFU_GA;
        $depts[]=HINYO_GA;
        $depts[]=SANFU_GA;
        $depts[]=GANKA_GA;
        $depts[]=JIBI_GA;
        $depts[]=TOUSEKI_GA;
        $depts[]=SEISIN_GA;
        $depts[]=SIKA_GA;
        $depts[]=HOSHASEN_GA;
        $depts[]=MASUI_GA;
        //get the excel columns in array
        $excelColumns=$this->getExcelColumnNames();

        $dateTable= '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';


        $indicatorData = $this->getIndicatorData();
        $i=intval($dateYear);
        $prevYear = $i;
        $prepprevYear = $i-1;

        $sundayCountPrev=0;
        $saturdayCountPrev=0;
        //Get the total number of days in selected month
        $totalDaysPrev=date("j",mktime(0,0,0,$dateMonth+1,0, $prevYear));
        //Iterate through all the days to count the saturday's and sunday's
        for($i=1;$i<=$totalDaysPrev;$i++){
            if((int)$dateMonth==12 && $i==30){
                $saturdayCountPrev++;
            }else if((int)$dateMonth==12 && $i==31){
                $sundayCountPrev++;
            }else if((int)$dateMonth==1 && $i==1){
                $sundayCountPrev++;
            }else if((int)$dateMonth==1 && $i==2){
                $sundayCountPrev++;
            }else if((int)$dateMonth==1 && $i==3){
                $sundayCountPrev++;
            }else{
                switch(date("l",mktime(0,0,0,$dateMonth,$i, $prevYear))){
                    case "Saturday":
                        $saturdayCountPrev++;
                        break;
                    case "Sunday":
                        $sundayCountPrev++;
                        break;
                    default:
                        break;
                }
            }
        }
        $totalDaysPrev=$totalDaysPrev-($saturdayCountPrev+    $sundayCountPrev);

        // prev prev year

        $sundayCountPrevPrev=0;
        $saturdayCountPrevPrev=0;
        //Get the total number of days in selected month
        $totalDaysPrevPrev=date("j",mktime(0,0,0,$dateMonth+1,0, $prepprevYear));
        //Iterate through all the days to count the saturday's and sunday's
        for($i=1;$i<=$totalDaysPrevPrev;$i++){
            if((int)$dateMonth==12 && $i==30){
                $saturdayCountPrevPrev++;
            }else if((int)$dateMonth==12 && $i==31){
                $sundayCountPrevPrev++;
            }else if((int)$dateMonth==1 && $i==1){
                $sundayCountPrevPrev++;
            }else if((int)$dateMonth==1 && $i==2){
                $sundayCountPrevPrev++;
            }else if((int)$dateMonth==1 && $i==3){
                $sundayCountPrevPrev++;
            }else{
                switch(date("l",mktime(0,0,0,$dateMonth,$i, $prepprevYear))){
                    case "Saturday":
                        $saturdayCountPrevPrev++;
                        break;
                    case "Sunday":
                        $sundayCountPrevPrev++;
                        break;
                    default:
                        break;
                }
            }
        }
        $totalDaysPrevPrev=$totalDaysPrevPrev-($saturdayCountPrevPrev+    $sundayCountPrevPrev);

        $saturdayColor = $this->getRgbColor('saturday');
        $holidayColor  = $this->getRgbColor('lightpink');

        $currentYear = $dateYear;
        $lastYear = $dateYear - 1;

        $currentYearDayCountArr = $indicatorData->getDayCountOfMonth($currentYear, $dateMonth);
        $lastYearDayCountArr = $indicatorData->getDayCountOfMonth($lastYear, $dateMonth);

        $fontTag = $this->getFontTag();
        list($fontTagLeft, $fontTagRight) = implode('%s', $fontTag);

        $dateTable.='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        $dateTable.='<tbody><tr><td width="80%">';
        for($i=0;$i<count($resultArr)-1;$i++){
            $dateTable.='<td></td>';
        }
        $dateTable.='<td style="text-align: right; border:1px solid black;" align="right">'.$dateYear.'</td>';
        $dateTable.='<td style="text-align: right; border:1px solid black;">'.$dateMonth.'</td></tr></tbody></table>';
        $dateTable.='<table width="30%" class="list" cellspacing="0" cellpadding="1" border="1">';
        $dateTable.='<tr><td>'.sprintf($fontTag, $currentYear.'年').'</td>';
        $dateTable.='<td style="text-align: left; background-color:#ffe9c8;">'.$fontTagLeft.'平日'.$fontTagRight.'</td>';
        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">';

        $dateTable.=''.$fontTagLeft.$currentYearDayCountArr['weekday'].'日'.$fontTagRight.'</td>';
//        $dateTable.=''.$fontTagLeft.$totalDaysPrev.'日'.$fontTagRight.'</td>';
        $dateTable.='<td style="text-align: left; background-color:'.$saturdayColor.'">'.$fontTagLeft.'土曜'.$fontTagRight.'</td>';
        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$currentYearDayCountArr['saturday'].'日'.$fontTagRight.'</td>';
//        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$saturdayCountPrev.'日'.$fontTagRight.'</td>';
        $dateTable.='<td style="text-align: left; background-color: '.$holidayColor.';">休日</td>';
        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$currentYearDayCountArr['holiday'].'日'.$fontTagRight.'</td></tr>    ';
//        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$sundayCountPrev.'日'.$fontTagRight.'</td></tr>    ';
        $dateTable.='<tr><td>'.sprintf($fontTag, $lastYear.'年').'</td>';
        $dateTable.='<td style="text-align: left; background-color:#ffe9c8;">'.$fontTagLeft.'平日'.$fontTagRight.'</td>';
        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">';

        $dateTable.=''.$fontTagLeft.$lastYearDayCountArr['weekday'].'日'.$fontTagRight.'</td>';
//        $dateTable.=''.$fontTagLeft.$totalDaysPrevPrev.'日'.$fontTagRight.'</td>';
        $dateTable.='<td style="text-align: left; background-color:'.$saturdayColor.'">'.$fontTagLeft.'土曜'.$fontTagRight.'</td>';

        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$lastYearDayCountArr['saturday'].'日'.$fontTagRight.'</td>';
//        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$saturdayCountPrevPrev.'日'.$fontTagRight.'</td>';
        $dateTable.='<td style="text-align: left; background-color: '.$holidayColor.';">休日</td>';
        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$lastYearDayCountArr['holiday'].'日'.$fontTagRight.'</td></tr>    ';
//        $dateTable.='<td style="text-align:center;padding:0 2px 0 2px;">'.$fontTagLeft.$sundayCountPrevPrev.'日'.$fontTagRight.'</td></tr>    ';
    $dateTable.='</table>';

        //If current month and year is same as selected then
        //get the total days in months as till yesterday
        if($currentMonth===$dateMonth && $currentYear===$dateYear){
            $totalDaysInMonth=date('d')-1;
        }elseif (($currentMonth>$dateMonth && $currentYear===$dateYear) || ($currentYear>$dateYear)){
            $totalDaysInMonth=date("j",mktime(0,0,0,$dateMonth+1,0, $dateYear));
        }else{
            $totalDaysInMonth=0;
        }

        $thColorArr   = array('name' => 'background-color', 'value' => '#d2f4fa;'); # 青
        $compColorArr = array('name' => 'background-color', 'value' => '#FFC8ED;'); # 緑
        $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');
        $statusColorWhiteArr = array('name' => 'background-color', 'value' => '#FFF;');
        $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');
        $statusColorGreenArr = array('name' => 'background-color', 'value' => '#CFFFCF;');
        $thColor  = $this->getArrValueByKey('name', $thColorArr).   ':'. $this->getArrValueByKey('value', $thColorArr);
        $compColor = $this->getArrValueByKey('name', $compColorArr). ':'. $this->getArrValueByKey('value', $compColorArr);
        $statusRedColor = '';
//        $statusRedColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
        $statusWhiteColor = '';
//        $statusWhiteColor = $this->getArrValueByKey('name', $statusColorWhiteArr). ':'. $this->getArrValueByKey('value', $statusColorWhiteArr);
        $statusYellowColor = '';
//        $statusYellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
        $statusGreenColor = $this->getArrValueByKey('name', $statusColorGreenArr). ':'. $this->getArrValueByKey('value', $statusColorGreenArr);
        $msoNumberFormat_percent_arr = array('name' => 'mso-number-format', 'value' => '0.0%');
        $msoNumberFormat_percent = $msoNumberFormat_percent_arr['name'].':'.$msoNumberFormat_percent_arr['value'].';';


//        $font = $this->getFont();
//        $font['color'] = null;
//        if (!is_null($fontStyle) && is_array($fontStyle)) {
//            $font = $fontStyle;
//        }
        $dateTable .= '<table border="1" class="list" width="100%"><tbody>';
        $fontTag = $this->getFontTag();
//        $fontTag = $this->getFontTag($font);
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

        $statusColor = $statusWhiteColor;
        $statusColorPrev = $statusWhiteColor;
        $statusColorCurr = $statusWhiteColor;


        $tableHeader = "<td style='".$thColor." text-align: center;'>".sprintf($fontTag, "年")."</td>";
        // Naika ga
        $naikaga="";
        $naikaCurr="";
        $naikaPrev="";
        $naikaDiff="";
        $naikaRatio="";
        // Sinkei ga
        $sinkei_ga="";
        $sinkeiCurr="";
        $sinkeiPrev="";
        $sinkeiDiff="";
        $sinkeiRatio="";
        // Kokyu ga
        $kokyu_ga="";
        $kokyuCurr="";
        $kokyuPrev="";
        $kokyuDiff="";
        $kokyuRatio="";

        // Shoka ga
        $shoka_ga="";
        $shokaCurr="";
        $shokaPrev="";
        $shokaDiff="";
        $shokaRatio="";

        // Junkanki ga
        $junkanki_ga="";
        $junkankiCurr="";
        $junkankiPrev="";
        $junkankiDiff="";
        $junkankiRatio="";

        // Shouni Ga
        $shouni_ga="";
        $shouniCurr="";
        $shouniPrev="";
        $shouniDiff="";
        $shouniRatio="";

        // Geka Ga
        $geka_ga="";
        $gekaCurr="";
        $gekaPrev="";
        $gekaDiff="";
        $gekaRatio="";

        // Seikei Ga
        $seikei_ga="";
        $seikeiCurr="";
        $seikeiPrev="";
        $seikeiDiff="";
        $seikeiRatio="";

        // Keiseibi Ga
        $keiseibi_ga="";
        $keiseibiCurr="";
        $keiseibiPrev="";
        $keiseibiDiff="";
        $keiseibiRatio="";

        // Nou Ga
        $nou_ga="";
        $nouCurr="";
        $nouPrev="";
        $nouDiff="";
        $nouRatio="";

        // Hifu Ga
        $hifu_ga="";
        $hifuCurr="";
        $hifuPrev="";
        $hifuDiff="";
        $hifuRatio="";

        // Hinyo Ga
        $hinyo_ga="";
        $hinyoCurr="";
        $hinyoPrev="";
        $hinyoDiff="";
        $hinyoRatio="";

        // Sanfu Ga
        $sanfu_ga="";
        $sanfuCurr="";
        $sanfuPrev="";
        $sanfuDiff="";
        $sanfuRatio="";

        // Ganka Ga
        $ganka_ga="";
        $gankaCurr="";
        $gankaPrev="";
        $gankaDiff="";
        $gankaRatio="";

        // Jibi Ga
        $jibi_ga="";
        $jibiCurr="";
        $jibiPrev="";
        $jibiDiff="";
        $jibiRatio="";

        // Touseki Ga
        $touseki_ga="";
        $tousekiCurr="";
        $tousekiPrev="";
        $tousekiDiff="";
        $tousekiRatio="";

        // Seisin Ga
        $seisin_ga="";
        $seisinCurr="";
        $seisinPrev="";
        $seisinDiff="";
        $seisinRatio="";

        // Sika Ga
        $sika_ga="";
        $sikaCurr="";
        $sikaPrev="";
        $sikaDiff="";
        $sikaRatio="";

        // Hoshasen Ga
        $hoshasen_ga="";
        $hoshasenCurr="";
        $hoshasenPrev="";
        $hoshasenDiff="";
        $hoshasenRatio="";

        // Masui Ga
        $masui_ga="";
        $masuiCurr="";
        $masuiPrev="";
        $masuiDiff="";
        $masuiRatio="";

        // Td total
        $total_td_curr="";
        $total_td_prev="";
        $total_td_diff="";
        $total_td_ratio="";

        // Total Prev Year
        $total_naika_ga_curr=0;
        $total_sinkei_ga_curr=0;
        $total_kokyu_ga_curr=0;
        $total_shoka_ga_curr=0;
        $total_junkanki_ga_curr=0;
        $total_shouni_ga_curr=0;
        $total_geka_ga_curr=0;
        $total_seikei_ga_curr=0;
        $total_keiseibi_ga_curr=0;
        $total_nou_ga_curr=0;
        $total_hifu_ga_curr=0;
        $total_hinyo_ga_curr=0;
        $total_sanfu_ga_curr=0;
        $total_ganka_ga_curr=0;
        $total_jibi_ga_curr=0;
        $total_touseki_ga_curr=0;
        $total_seisin_ga_curr=0;
        $total_sika_ga_curr=0;
        $total_hoshasen_ga_curr=0;
        $total_masui_ga_curr=0;


        // Total prev prev year

        $total_naika_ga_prev=0;
        $total_sinkei_ga_prev=0;
        $total_kokyu_ga_prev=0;
        $total_shoka_ga_prev=0;
        $total_junkanki_ga_prev=0;
        $total_shouni_ga_prev=0;
        $total_geka_ga_prev=0;
        $total_seikei_ga_prev=0;
        $total_keiseibi_ga_prev=0;
        $total_nou_ga_prev=0;
        $total_hifu_ga_prev=0;
        $total_hinyo_ga_prev=0;
        $total_sanfu_ga_prev=0;
        $total_ganka_ga_prev=0;
        $total_jibi_ga_prev=0;
        $total_touseki_ga_prev=0;
        $total_seisin_ga_prev=0;
        $total_sika_ga_prev=0;
        $total_hoshasen_ga_prev=0;
        $total_masui_ga_prev=0;

        $final_total_curr=0;
        $final_total_prev=0;
        $final_total_diff=0;
        $final_total_ratio=0;

        $colorFlagCurr=0;
        $colorFlagPrev=0;
        $totalCellColorCurr="";
        $totalCellColorPrev="";
        $totalRowColorCurr="";
        $totalRowColorPrev="";

        for($i=0;$i<count($resultArr);$i++){
              $tableHeader .= "<td style='".$thColor." text-align: center;'>".$fontTagLeft.$resultArr[$i]['jnl_facility_name'].$fontTagRight."</td>";
        }
        $tableHeader .= "<td style='".$thColor." text-align: center;'>".$fontTagLeft."合計".$fontTagRight."</td>";
        $colorTotalCurr=0;
        $colorTotalPrev=0;
        $colorFlagTotalCurr = 0;
        $colorFlagTotalPrev = 0;
        for($i=0;$i<count($resultArr);$i++){
            $colorTotalCurr = $colorTotalCurr +     $resultArr[$i]['naika_ga_c'] +
                $resultArr[$i]['sinkei_ga_c'] +    $resultArr[$i]['kokyu_ga_c'] +
                $resultArr[$i]['shoka_ga_c'] +    $resultArr[$i]['junkanki_ga_c'] +
                $resultArr[$i]['shouni_ga_c'] +    $resultArr[$i]['geka_ga_c'] +
                $resultArr[$i]['seikei_ga_c'] +    $resultArr[$i]['keiseibi_ga_c'] +
                $resultArr[$i]['nou_ga_c'] +    $resultArr[$i]['hifu_ga_c'] +
                $resultArr[$i]['hinyo_ga_c'] +    $resultArr[$i]['sanfu_ga_c'] +
                $resultArr[$i]['ganka_ga_c'] +    $resultArr[$i]['jibi_ga_c'] +
                $resultArr[$i]['touseki_ga_c'] + $resultArr[$i]['seisin_ga_c'] +
                $resultArr[$i]['sika_ga_c'] +    $resultArr[$i]['hoshasen_ga_c'] +
                $resultArr[$i]['masui_ga_c'] ;
            $colorTotalPrev = $colorTotalPrev +     $resultArr[$i]['naika_ga_p'] +
                $resultArr[$i]['sinkei_ga_p'] +    $resultArr[$i]['kokyu_ga_p'] +
                $resultArr[$i]['shoka_ga_p'] +    $resultArr[$i]['junkanki_ga_p'] +
                $resultArr[$i]['shouni_ga_p'] +    $resultArr[$i]['geka_ga_p'] +
                $resultArr[$i]['seikei_ga_p'] +    $resultArr[$i]['keiseibi_ga_p'] +
                $resultArr[$i]['nou_ga_p'] +    $resultArr[$i]['hifu_ga_p'] +
                $resultArr[$i]['hinyo_ga_p'] +    $resultArr[$i]['sanfu_ga_p'] +
                $resultArr[$i]['ganka_ga_p'] +    $resultArr[$i]['jibi_ga_p'] +
                $resultArr[$i]['touseki_ga_p'] + $resultArr[$i]['seisin_ga_p'] +
                $resultArr[$i]['sika_ga_p'] +    $resultArr[$i]['hoshasen_ga_p'] +
                $resultArr[$i]['masui_ga_p'] ;
        }

        for($i=0;$i<count($resultArr);$i++){
            $statusColor = $statusWhiteColor;
            $statusColorPrev = $statusWhiteColor;
            $statusColorCurr = $statusWhiteColor;
            $countResultPrev = $indicatorData->getTotalForMonthInYear($resultArr[$i]['jnl_facility_id'],    $dateMonth, $prevYear);
            $deptCountPrev = $countResultPrev[0]['deptcount'];

            $countResultPrevPrev = $indicatorData->getTotalForMonthInYear($resultArr[$i]    ['jnl_facility_id'],$dateMonth, $prepprevYear);
            $deptCountPrevPrev = $countResultPrevPrev[0]['deptcount'];

            $td_total_curr = 0;
            $td_total_prev = 0;
            $colorFlagCurr = 0;
            $colorFlagPrev = 0;

            //Check the jnl status and set the background color for cell

        if($resultArr[$i]['jnl_status_c']==0){
            $statusColor=$statusYellowColor;
            $statusColorCurr = $statusYellowColor;
            if($colorFlagCurr==0)
            {
                $colorFlagCurr=1;
                $colorFlagTotalCurr = 1;
            }
            if( $deptCountPrev < $totalDaysInMonth )
            {
                $colorFlagCurr=2;
                $colorFlagTotalCurr = 2;
                $statusColorCurr=$statusRedColor;
                $statusColor=$statusRedColor;

            }

        }else if( $deptCountPrev < $totalDaysInMonth ){
            $colorFlagCurr=2;
            $colorFlagTotalCurr = 2;
            $statusColorCurr=$statusRedColor;
            $statusColor=$statusRedColor;

        }
        else {
            $statusColor=$statusWhiteColor;
        }





    switch($colorFlagTotalCurr){
        case 0:
            $totalCellColorCurr=$statusWhiteColor;
            break;
        case 1:
            $totalCellColorCurr=$statusYellowColor;
            break;
        case 2:
            $totalCellColorCurr=$statusRedColor;
            break;

    }
    switch($colorFlagPrev){
        case 0:
            $totalCellColorPrev=$statusWhiteColor;
            break;
        case 1:
            $totalCellColorPrev=$statusYellowColor;
            break;
        case 2:
            $totalCellColorPrev=$statusRedColor;
            break;

    }
    switch($colorFlagTotalCurr){
        case 0:
            $totalRowColorCurr=$statusWhiteColor;
            break;
        case 1:
            $totalRowColorCurr=$statusYellowColor;
            break;
        case 2:
            $totalRowColorCurr=$statusRedColor;
            break;

    }
    switch($colorFlagTotalPrev){
        case 0:
            $totalRowColorPrev=$statusWhiteColor;
            break;
        case 1:
            $totalRowColorPrev=$statusYellowColor;
            break;
        case 2:
            $totalRowColorPrev=$statusRedColor;
            break;

    }


    if($colorTotalCurr==0){
    $statusColorCurr = $statusRedColor;
    $totalCellColorCurr = $statusRedColor;
    $totalRowColorCurr=$statusRedColor;

    }
    if($colorTotalPrev==0){
    $statusColorPrev = $statusRedColor;
    $totalCellColorPrev = $statusRedColor;
    $totalRowColorPrev=$statusRedColor;

    }
    //Cell bg color
    //$statusColorCurr= $statusWhiteColor;
    $statusColorPrev= $statusWhiteColor;
    //Total Cell bg color in row
    //$totalRowColorCurr=$statusWhiteColor;
    $totalRowColorPrev=$statusWhiteColor;
    //Total Cell bg color in column
    //$totalCellColorCurr=$statusWhiteColor;
    $totalCellColorPrev=$statusWhiteColor;

                // Naika ga
                $naikaCurr.="<td style='text-align : right; ".$statusColorCurr."' >".$fontTagLeft.$resultArr[$i]['naika_ga_c'].$fontTagRight."</td>";
                $naikaPrev.="<td style='  text-align : right;".$statusColorPrev."'>".$fontTagLeft.$resultArr[$i]['naika_ga_p'].$fontTagRight."</td>";
                $naikaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, NAIKA_GA, $i).$fontTagRight."</td>";
                $naikaRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, NAIKA_GA, $i).$fontTagRight."</td>";
                $total_naika_ga_curr = $total_naika_ga_curr + $resultArr[$i]['naika_ga_c'];
                $total_naika_ga_prev = $total_naika_ga_prev + $resultArr[$i]['naika_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['naika_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['naika_ga_p'];


                // Sinkei Ga
                $sinkeiCurr.="<td style='text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['sinkei_ga_c'].$fontTagRight."</td>";
                $sinkeiPrev.="<td style=' text-align : right; ".$statusColorPrev."'>".$fontTagLeft.$resultArr[$i]['sinkei_ga_p'].$fontTagRight."</td>";
                $sinkeiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SINKEI_GA, $i).$fontTagRight."</td>";
                $sinkeiRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$fontTagLeft.$this->getRatioCell($excelColumns, SINKEI_GA, $i).$fontTagRight."</td>";
                $total_sinkei_ga_curr = $total_sinkei_ga_curr + $resultArr[$i]['sinkei_ga_c'];
                $total_sinkei_ga_prev = $total_sinkei_ga_prev + $resultArr[$i]['sinkei_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['sinkei_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['sinkei_ga_p'];


                // Kokyu Ga
                $kokyuCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['kokyu_ga_c'].$fontTagRight."</td>";
                $kokyuPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['kokyu_ga_p'].$fontTagRight."</td>";
                $kokyuDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, KOKYU_GA, $i).$fontTagRight."</td>";
                $kokyuRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, KOKYU_GA, $i).$fontTagRight."</td>";
                $total_kokyu_ga_curr = $total_kokyu_ga_curr + $resultArr[$i]['kokyu_ga_c'];
                $total_kokyu_ga_prev = $total_kokyu_ga_prev + $resultArr[$i]['kokyu_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['kokyu_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['kokyu_ga_p'];

                // Shoka Ga
                $shokaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['shoka_ga_c'].$fontTagRight."</td>";
                $shokaPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['shoka_ga_p'].$fontTagRight."</td>";
                $shokaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SHOKA_GA, $i).$fontTagRight."</td>";
                $shokaRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SHOKA_GA, $i).$fontTagRight."</td>";
                $total_shoka_ga_curr = $total_shoka_ga_curr + $resultArr[$i]['shoka_ga_c'];
                $total_shoka_ga_prev = $total_shoka_ga_prev + $resultArr[$i]['shoka_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['shoka_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['shoka_ga_p'];

                // Junkanki Ga
                $junkankiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['junkanki_ga_c'].$fontTagRight."</td>";
                $junkankiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['junkanki_ga_p'].$fontTagRight."</td>";
                $junkankiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, JUNKANKI_GA, $i).$fontTagRight."</td>";
                $junkankiRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, JUNKANKI_GA, $i).$fontTagRight."</td>";
                $total_junkanki_ga_curr = $total_junkanki_ga_curr + $resultArr[$i]['junkanki_ga_c'];
                $total_junkanki_ga_prev = $total_junkanki_ga_prev + $resultArr[$i]['junkanki_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['junkanki_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['junkanki_ga_p'];

                // Shouni Ga
                $shouniCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['shouni_ga_c'].$fontTagRight."</td>";
                $shouniPrev.="<td style='text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['shouni_ga_p'].$fontTagRight."</td>";
                $shouniDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SHOUNI_GA, $i).$fontTagRight."</td>";
                $shouniRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SHOUNI_GA, $i).$fontTagRight."</td>";
                $total_shouni_ga_curr = $total_shouni_ga_curr + $resultArr[$i]['shouni_ga_c'];
                $total_shouni_ga_prev = $total_shouni_ga_prev + $resultArr[$i]['shouni_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['shouni_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['shouni_ga_p'];

                // Geka Ga
                $gekaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['geka_ga_c'].$fontTagRight."</td>";
                $gekaPrev.="<td style=' text-align : right; ".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['geka_ga_p'].$fontTagRight."</td>";
                $gekaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, GEKA_GA, $i).$fontTagRight."</td>";
                $gekaRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, GEKA_GA, $i).$fontTagRight."</td>";
                $total_geka_ga_curr = $total_geka_ga_curr + $resultArr[$i]['geka_ga_c'];
                $total_geka_ga_prev = $total_geka_ga_prev + $resultArr[$i]['geka_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['geka_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['geka_ga_p'];

                // Seikei Ga
                $seikeiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['seikei_ga_c'].$fontTagRight."</td>";
                $seikeiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['seikei_ga_p'].$fontTagRight."</td>";
                $seikeiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SEIKEI_GA, $i).$fontTagRight."</td>";
                $seikeiRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SEIKEI_GA, $i).$fontTagRight."</td>";
                $total_seikei_ga_curr = $total_seikei_ga_curr + $resultArr[$i]['seikei_ga_c'];
                $total_seikei_ga_prev = $total_seikei_ga_prev + $resultArr[$i]['seikei_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['seikei_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['seikei_ga_p'];


                // Keiseibi Ga
                $keiseibiCurr.="<td style=' text-align : right; ".$statusColorCurr."'>".$fontTagLeft.$resultArr[$i]['keiseibi_ga_c'].$fontTagRight."</td>";
                $keiseibiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['keiseibi_ga_p'].$fontTagRight."</td>";
                $keiseibiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, KEISEIBI_GA, $i).$fontTagRight."</td>";
                $keiseibiRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, KEISEIBI_GA, $i).$fontTagRight."</td>";
                $total_keiseibi_ga_curr = $total_keiseibi_ga_curr + $resultArr[$i]['keiseibi_ga_c'];
                $total_keiseibi_ga_prev = $total_keiseibi_ga_prev + $resultArr[$i]['keiseibi_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['keiseibi_ga_c'];
$td_total_prev = $td_total_prev + $resultArr[$i]['keiseibi_ga_p'];


                // Nou Ga
                $nouCurr.="<td style=' text-align : right; ".$statusColorCurr."'>".$fontTagLeft.$resultArr[$i]['nou_ga_c'].$fontTagRight."</td>";
                $nouPrev.="<td style=' text-align : right; ".$statusColorPrev."'>".$fontTagLeft.$resultArr[$i]['nou_ga_p'].$fontTagRight."</td>";
                $nouDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, NOU_GA, $i).$fontTagRight."</td>";
                $nouRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, NOU_GA, $i).$fontTagRight."</td>";
                $total_nou_ga_curr = $total_nou_ga_curr + $resultArr[$i]['nou_ga_c'];
                $total_nou_ga_prev = $total_nou_ga_prev + $resultArr[$i]['nou_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['nou_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['nou_ga_p'];

                // Hifu Ga
                $hifuCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['hifu_ga_c'].$fontTagRight."</td>";
                $hifuPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['hifu_ga_p'].$fontTagRight."</td>";
                $hifuDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, HIFU_GA, $i).$fontTagRight."</td>";
                $hifuRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, HIFU_GA, $i).$fontTagRight."</td>";
                $total_hifu_ga_curr = $total_hifu_ga_curr + $resultArr[$i]['hifu_ga_c'];
                $total_hifu_ga_prev = $total_hifu_ga_prev + $resultArr[$i]['hifu_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['hifu_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['hifu_ga_p'];

                // Hinyo Ga
                $hinyoCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['hinyo_ga_c'].$fontTagRight."</td>";
                $hinyoPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['hinyo_ga_p'].$fontTagRight."</td>";
                $hinyoDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, HINYO_GA, $i).$fontTagRight."</td>";
                $hinyoRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, HINYO_GA, $i).$fontTagRight."</td>";
                $total_hinyo_ga_curr = $total_hinyo_ga_curr + $resultArr[$i]['hinyo_ga_c'];
                $total_hinyo_ga_prev = $total_hinyo_ga_prev + $resultArr[$i]['hinyo_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['hinyo_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['hinyo_ga_p'];

                // Sanfu Ga
                $sanfuCurr.="<td style=' text-align : right;".$statusColorCurr." '>".    $fontTagLeft.$resultArr[$i]['sanfu_ga_c'].$fontTagRight."</td>";
                $sanfuPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['sanfu_ga_p'].$fontTagRight."</td>";
                $sanfuDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SANFU_GA, $i).$fontTagRight."</td>";
                $sanfuRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SANFU_GA, $i).$fontTagRight."</td>";
                $total_sanfu_ga_curr = $total_sanfu_ga_curr + $resultArr[$i]['sanfu_ga_c'];
                $total_sanfu_ga_prev = $total_sanfu_ga_prev + $resultArr[$i]['sanfu_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['sanfu_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['sanfu_ga_p'];

                // Ganka Ga
                $gankaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['ganka_ga_c'].$fontTagRight."</td>";
                $gankaPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['ganka_ga_p'].$fontTagRight."</td>";
                $gankaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, GANKA_GA, $i).$fontTagRight."</td>";
                $gankaRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, GANKA_GA, $i).$fontTagRight."</td>";
                $total_ganka_ga_curr = $total_ganka_ga_curr + $resultArr[$i]['ganka_ga_c'];
                $total_ganka_ga_prev = $total_ganka_ga_prev + $resultArr[$i]['ganka_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['ganka_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['ganka_ga_p'];


                // Jibi Ga
                $jibiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['jibi_ga_c'].$fontTagRight."</td>";
                $jibiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['jibi_ga_p'].$fontTagRight."</td>";
                $jibiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, JIBI_GA, $i).$fontTagRight."</td>";
                $jibiRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, JIBI_GA, $i).$fontTagRight."</td>";
                $total_jibi_ga_curr = $total_jibi_ga_curr + $resultArr[$i]['jibi_ga_c'];
                $total_jibi_ga_prev = $total_jibi_ga_prev + $resultArr[$i]['jibi_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['jibi_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['jibi_ga_p'];

                // Touseki Ga
                $tousekiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['touseki_ga_c'].$fontTagRight."</td>";
                $tousekiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['touseki_ga_p'].$fontTagRight."</td>";
                $tousekiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, TOUSEKI_GA, $i).$fontTagRight."</td>";
                $tousekiRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, TOUSEKI_GA, $i).$fontTagRight."</td>";
                $total_touseki_ga_curr = $total_touseki_ga_curr + $resultArr[$i]['touseki_ga_c'];
                $total_touseki_ga_prev = $total_touseki_ga_prev + $resultArr[$i]['touseki_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['touseki_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['touseki_ga_p'];

                // Seisin Ga
                $seisinCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['seisin_ga_c'].$fontTagRight."</td>";
                $seisinPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['seisin_ga_p'].$fontTagRight."</td>";
                $seisinDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SEISIN_GA, $i).$fontTagRight."</td>";
                $seisinRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SEISIN_GA, $i).$fontTagRight."</td>";
                $total_seisin_ga_curr = $total_seisin_ga_curr + $resultArr[$i]['seisin_ga_c'];
                $total_seisin_ga_prev = $total_seisin_ga_prev + $resultArr[$i]['seisin_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['seisin_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['seisin_ga_p'];

                // Sika Ga
                $sikaCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['sika_ga_c'].$fontTagRight."</td>";
                $sikaPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['sika_ga_p'].$fontTagRight."</td>";
                $sikaDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SIKA_GA, $i).$fontTagRight."</td>";
                $sikaRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SIKA_GA, $i).$fontTagRight."</td>";
                $total_sika_ga_curr = $total_sika_ga_curr + $resultArr[$i]['sika_ga_c'];
                $total_sika_ga_prev = $total_sika_ga_prev + $resultArr[$i]['sika_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['sika_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['sika_ga_p'];

                // Hoshasen Ga
                $hoshasenCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['hoshasen_ga_c'].$fontTagRight."</td>";
                $hoshasenPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['hoshasen_ga_p'].$fontTagRight."</td>";
                $hoshasenDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, HOSHASEN_GA, $i).$fontTagRight."</td>";
                $hoshasenRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, HOSHASEN_GA, $i).$fontTagRight."</td>";
                $total_hoshasen_ga_curr = $total_hoshasen_ga_curr + $resultArr[$i]['hoshasen_ga_c'];
                $total_hoshasen_ga_prev = $total_hoshasen_ga_prev + $resultArr[$i]['hoshasen_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['hoshasen_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['hoshasen_ga_p'];

                // Masui Ga
                $masuiCurr.="<td style=' text-align : right;".$statusColorCurr." '>".$fontTagLeft.$resultArr[$i]['masui_ga_c'].$fontTagRight."</td>";
                $masuiPrev.="<td style=' text-align : right;".$statusColorPrev." '>".$fontTagLeft.$resultArr[$i]['masui_ga_p'].$fontTagRight."</td>";
                $masuiDiff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, MASUI_GA, $i).$fontTagRight."</td>";
                $masuiRatio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, MASUI_GA, $i).$fontTagRight."</td>";
                $total_masui_ga_curr = $total_masui_ga_curr + $resultArr[$i]['masui_ga_c'];
                $total_masui_ga_prev = $total_masui_ga_prev + $resultArr[$i]['masui_ga_p'];
                $td_total_curr = $td_total_curr + $resultArr[$i]['masui_ga_c'];
                $td_total_prev = $td_total_prev + $resultArr[$i]['masui_ga_p'];


                $total_td_curr.="<td style=' text-align : right;".$totalCellColorCurr." '>".$fontTagLeft.$this->getTotalCell($excelColumns,$depts,true, $i+2).$fontTagRight."</td>";
                $total_td_prev.="<td style=' text-align : right;".$totalCellColorPrev." '>".$fontTagLeft.$this->getTotalCell($excelColumns,$depts,false, $i+2).$fontTagRight."</td>";
                $total_td_diff.="<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, TOTAL, $i).$fontTagRight."</td>";
                $total_td_ratio.="<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, TOTAL, $i).$fontTagRight."</td>";


                $final_total_curr=$final_total_curr+ $td_total_curr;
                $final_total_prev=$final_total_prev+ $td_total_prev;



            }
            //total number of columns
            $totalColumns=count($resultArr)+1;
            //start index for formula
            $startIndex=2;

            // Naika Ga
            $naikaga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".    $fontTagLeft."内科".$fontTagRight."</td><td style='text-align: center;'>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $naikaga.= $naikaCurr;
            $naikaga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, NAIKA_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $naikaga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $naikaga.= $naikaPrev;
            $naikaga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, NAIKA_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $naikaga.="</tr><tr><td style='".$statusGreenColor." text-align : center; white-space: nowrap; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $naikaga.= $naikaDiff;
            $naikaga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, NAIKA_GA, count($resultArr)).$fontTagRight."</td>";
            $naikaga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $naikaga.= $naikaRatio;
            $naikaga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, NAIKA_GA, count($resultArr)).$fontTagRight."</td>";
            $naikaga.= "</tr>";


            // Sinkei Ga
            $sinkei_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."神経内科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $sinkei_ga.= $sinkeiCurr;
            $sinkei_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SINKEI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $sinkei_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $sinkei_ga.= $sinkeiPrev;
            $sinkei_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SINKEI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $sinkei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $sinkei_ga.= $sinkeiDiff;
            $sinkei_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SINKEI_GA, count($resultArr)).$fontTagRight."</td>";
            $sinkei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $sinkei_ga.= $sinkeiRatio;
            $sinkei_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SINKEI_GA, count($resultArr)).$fontTagRight."</td>";
            $sinkei_ga.= "</tr>";

            // Kokyu Ga
            $kokyu_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."呼吸器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $kokyu_ga.= $kokyuCurr;
            $kokyu_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, KOKYU_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $kokyu_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $kokyu_ga.= $kokyuPrev;
            $kokyu_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, KOKYU_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $kokyu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $kokyu_ga.= $kokyuDiff;
            $kokyu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, KOKYU_GA, count($resultArr)).$fontTagRight."</td>";
            $kokyu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $kokyu_ga.= $kokyuRatio;
            $kokyu_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, KOKYU_GA, count($resultArr)).$fontTagRight."</td>";
            $kokyu_ga.= "</tr>";


            // Shoka Ga
            $shoka_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."消化器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $shoka_ga.= $shokaCurr;
            $shoka_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SHOKA_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $shoka_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $shoka_ga.= $shokaPrev;
            $shoka_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$fontTagLeft.$this->getSumFormula($excelColumns, SHOKA_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $shoka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $shoka_ga.= $shokaDiff;
            $shoka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SHOKA_GA, count($resultArr)).$fontTagRight."</td>";
            $shoka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $shoka_ga.= $shokaRatio;
            $shoka_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SHOKA_GA, count($resultArr)).$fontTagRight."</td>";
            $shoka_ga.= "</tr>";

            // Junkanki Ga
            $junkanki_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."循環器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $junkanki_ga.= $junkankiCurr;
            $junkanki_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$fontTagLeft.$this->getSumFormula($excelColumns, JUNKANKI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $junkanki_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $junkanki_ga.= $junkankiPrev;
            $junkanki_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, JUNKANKI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $junkanki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $junkanki_ga.= $junkankiDiff;
            $junkanki_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, JUNKANKI_GA, count($resultArr)).$fontTagRight."</td>";
            $junkanki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $junkanki_ga.= $junkankiRatio;
            $junkanki_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, JUNKANKI_GA, count($resultArr)).$fontTagRight."</td>";
            $junkanki_ga.= "</tr>";

            // Shouni Ga
            $shouni_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."小児科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $shouni_ga.= $shouniCurr;
            $shouni_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SHOUNI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $shouni_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $shouni_ga.= $shouniPrev;
            $shouni_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SHOUNI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $shouni_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $shouni_ga.= $shouniDiff;
            $shouni_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SHOUNI_GA, count($resultArr)).$fontTagRight."</td>";
            $shouni_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $shouni_ga.= $shouniRatio;
            $shouni_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SHOUNI_GA, count($resultArr)).$fontTagRight."</td>";
            $shouni_ga.= "</tr>";

            // Geka Ga
            $geka_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."外科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $geka_ga.= $gekaCurr;
            $geka_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, GEKA_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $geka_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $geka_ga.= $gekaPrev;
            $geka_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, GEKA_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $geka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $geka_ga.= $gekaDiff;
            $geka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, GEKA_GA, count($resultArr)).$fontTagRight."</td>";
            $geka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $geka_ga.= $gekaRatio;
            $geka_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, GEKA_GA, count($resultArr)).$fontTagRight."</td>";
            $geka_ga.= "</tr>";

            // Seikei Ga
            $seikei_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."整形外科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $seikei_ga.= $seikeiCurr;
            $seikei_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SEIKEI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $seikei_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $seikei_ga.= $seikeiPrev;
            $seikei_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SEIKEI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $seikei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $seikei_ga.= $seikeiDiff;
            $seikei_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SEIKEI_GA, count($resultArr)).$fontTagRight."</td>";
            $seikei_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $seikei_ga.= $seikeiRatio;
            $seikei_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SEIKEI_GA, count($resultArr)).$fontTagRight."</td>";
            $seikei_ga.= "</tr>";

            // Keiseibi Ga
            $keiseibi_ga.="<tr><td rowspan='4'style='".$thColor." text-align : center; ' >".$fontTagLeft."形成（美）外科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.    $prevYear.$fontTagRight."</td>";
            $keiseibi_ga.= $keiseibiCurr;
            $keiseibi_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, KEISEIBI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $keiseibi_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $keiseibi_ga.= $keiseibiPrev;
            $keiseibi_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, KEISEIBI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $keiseibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $keiseibi_ga.= $keiseibiDiff;
            $keiseibi_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, KEISEIBI_GA, count($resultArr)).$fontTagRight."</td>";
            $keiseibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $keiseibi_ga.= $keiseibiRatio;
            $keiseibi_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, KEISEIBI_GA, count($resultArr)).$fontTagRight."</td>";
            $keiseibi_ga.= "</tr>";

            // Nou Ga
            $nou_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."脳神経外科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $nou_ga.= $nouCurr;
            $nou_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, NOU_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $nou_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $nou_ga.= $nouPrev;
            $nou_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, NOU_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $nou_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $nou_ga.= $nouDiff;
            $nou_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, NOU_GA, count($resultArr)).$fontTagRight."</td>";
            $nou_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $nou_ga.= $nouRatio;
            $nou_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, NOU_GA, count($resultArr)).$fontTagRight."</td>";
            $nou_ga.= "</tr>";

            // Hifu Ga
            $hifu_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."皮膚科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $hifu_ga.= $hifuCurr;
            $hifu_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, HIFU_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $hifu_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $hifu_ga.= $hifuPrev;
            $hifu_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, HIFU_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $hifu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $hifu_ga.= $hifuDiff;
            $hifu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, HIFU_GA, count($resultArr)).$fontTagRight."</td>";
            $hifu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $hifu_ga.= $hifuRatio;
            $hifu_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, HIFU_GA, count($resultArr)).$fontTagRight."</td>";
            $hifu_ga.= "</tr>";


            // Hinyo Ga
            $hinyo_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."泌尿器科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $hinyo_ga.= $hinyoCurr;
            $hinyo_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, HINYO_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $hinyo_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $hinyo_ga.= $hinyoPrev;
            $hinyo_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, HINYO_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $hinyo_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $hinyo_ga.= $hinyoDiff;
            $hinyo_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, HINYO_GA, count($resultArr)).$fontTagRight."</td>";
            $hinyo_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $hinyo_ga.= $hinyoRatio;
            $hinyo_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$fontTagLeft.$this->getRatioCell($excelColumns, HINYO_GA, count($resultArr)).    $fontTagRight."</td>";
            $hinyo_ga.= "</tr>";

            // Sanfu Ga
            $sanfu_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."産婦人科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $sanfu_ga.= $sanfuCurr;
            $sanfu_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SANFU_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $sanfu_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.    $prepprevYear.$fontTagRight."</td>";
            $sanfu_ga.= $sanfuPrev;
            $sanfu_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SANFU_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $sanfu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $sanfu_ga.= $sanfuDiff;
            $sanfu_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$fontTagLeft.$this->getDiffCell($excelColumns, SANFU_GA, count($resultArr)).$fontTagRight."</td>";
            $sanfu_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $sanfu_ga.= $sanfuRatio;
            $sanfu_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SANFU_GA, count($resultArr)).$fontTagRight."</td>";
            $sanfu_ga.= "</tr>";

            // Ganka Ga
            $ganka_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."眼科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $ganka_ga.= $gankaCurr;
            $ganka_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, GANKA_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $ganka_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $ganka_ga.= $gankaPrev;
            $ganka_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, GANKA_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $ganka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $ganka_ga.= $gankaDiff;
            $ganka_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, GANKA_GA, count($resultArr)).$fontTagRight."</td>";
            $ganka_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $ganka_ga.= $gankaRatio;
            $ganka_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, GANKA_GA, count($resultArr)).$fontTagRight."</td>";
            $ganka_ga.= "</tr>";

            // Jibi Ga
            $jibi_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; ' >".$fontTagLeft."耳鼻咽喉科".$fontTagRight."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $jibi_ga.= $jibiCurr;
            $jibi_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, JIBI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $jibi_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $jibi_ga.= $jibiPrev;
            $jibi_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, JIBI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $jibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $jibi_ga.= $jibiDiff;
            $jibi_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, JIBI_GA, count($resultArr)).$fontTagRight."</td>";
            $jibi_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $jibi_ga.= $jibiRatio;
            $jibi_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, JIBI_GA, count($resultArr)).$fontTagRight."</td>";
            $jibi_ga.= "</tr>";

            // Touseki Ga
            $touseki_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."透析科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $touseki_ga.= $tousekiCurr;
            $touseki_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, TOUSEKI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $touseki_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $touseki_ga.= $tousekiPrev;
            $touseki_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, TOUSEKI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $touseki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $touseki_ga.= $tousekiDiff;
            $touseki_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, TOUSEKI_GA, count($resultArr)).$fontTagRight."</td>";
            $touseki_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $touseki_ga.= $tousekiRatio;
            $touseki_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, TOUSEKI_GA, count($resultArr)).$fontTagRight."</td>";
            $touseki_ga.= "</tr>";

            // Seisin Ga
            $seisin_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."精神科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $seisin_ga.= $seisinCurr;
            $seisin_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SEISIN_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $seisin_ga.="</tr><tr><td style=' text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $seisin_ga.= $seisinPrev;
            $seisin_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SEISIN_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $seisin_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $seisin_ga.= $seisinDiff;
            $seisin_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SEISIN_GA, count($resultArr)).$fontTagRight."</td>";
            $seisin_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $seisin_ga.= $seisinRatio;
            $seisin_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SEISIN_GA, count($resultArr)).$fontTagRight."</td>";
            $seisin_ga.= "</tr>";

            // Sika Ga
            $sika_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."歯科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $sika_ga.= $sikaCurr;
            $sika_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SIKA_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $sika_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $sika_ga.= $sikaPrev;
            $sika_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, SIKA_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $sika_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $sika_ga.= $sikaDiff;
            $sika_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, SIKA_GA, count($resultArr)).$fontTagRight."</td>";
            $sika_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $sika_ga.= $sikaRatio;
            $sika_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, SIKA_GA, count($resultArr)).$fontTagRight."</td>";
            $sika_ga.= "</tr>";

            // Hoshasen Ga
            $hoshasen_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."放射線科".$fontTagLeft."</td><td style='text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $hoshasen_ga.= $hoshasenCurr;
            $hoshasen_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, HOSHASEN_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $hoshasen_ga.="</tr><tr><td style=' text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $hoshasen_ga.= $hoshasenPrev;
            $hoshasen_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, HOSHASEN_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $hoshasen_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $hoshasen_ga.= $hoshasenDiff;
            $hoshasen_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, HOSHASEN_GA, count($resultArr)).$fontTagRight."</td>";
            $hoshasen_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $hoshasen_ga.= $hoshasenRatio;
            $hoshasen_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, HOSHASEN_GA, count($resultArr)).$fontTagRight."</td>";
            $hoshasen_ga.= "</tr>";

            // Masui Ga
            $masui_ga.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."麻酔科".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $masui_ga.= $masuiCurr;
            $masui_ga.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getSumFormula($excelColumns, MASUI_GA,$startIndex,$totalColumns).$fontTagRight."</td>";
            $masui_ga.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.    $prepprevYear.$fontTagRight."</td>";
            $masui_ga.= $masuiPrev;
            $masui_ga.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getSumFormula($excelColumns, MASUI_GA+1,$startIndex,$totalColumns).$fontTagRight."</td>";
            $masui_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $masui_ga.= $masuiDiff;
            $masui_ga.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, MASUI_GA, count($resultArr)).$fontTagRight."</td>";
            $masui_ga.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $masui_ga.= $masuiRatio;
            $masui_ga.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, MASUI_GA, count($resultArr)).$fontTagRight."</td>";
            $masui_ga.= "</tr>";

            // Total Row

            $totalRow.="<tr><td rowspan='4' style='".$thColor." text-align : center; '>".$fontTagLeft."合計".$fontTagRight."</td><td style=' text-align : center; '>".$fontTagLeft.$prevYear.$fontTagRight."</td>";
            $totalRow.= $total_td_curr;
            $totalRow.="<td style='  text-align : right;".$totalRowColorCurr."'>".$fontTagLeft.$this->getTotalCell($excelColumns,$depts,true, count($resultArr)+2).$fontTagRight."</td>";
            $totalRow.="</tr><tr><td style='text-align : center; '>".$fontTagLeft.$prepprevYear.$fontTagRight."</td>";
            $totalRow.= $total_td_prev;
            $totalRow.="<td style='  text-align : right;".$totalRowColorPrev."'>".$fontTagLeft.$this->getTotalCell($excelColumns,$depts,false, count($resultArr)+2).$fontTagRight."</td>";
            $totalRow.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'前年比'.$fontTagRight."</td>";
            $totalRow.= $total_td_diff;
            $totalRow.= "<td style='".$statusGreenColor." text-align : right; '>".$fontTagLeft.$this->getDiffCell($excelColumns, TOTAL, count($resultArr)).$fontTagRight."</td>";
            $totalRow.="</tr><tr><td style='".$statusGreenColor." text-align : center; '>".$fontTagLeft.'増減率'.$fontTagRight."</td>";
            $totalRow.= $total_td_ratio;
            $totalRow.= "<td style='".$statusGreenColor." text-align : right; ".$msoNumberFormat_percent."'>".$fontTagLeft.$this->getRatioCell($excelColumns, TOTAL, count($resultArr)).$fontTagRight."</td>";
            $totalRow.= "</tr>";

            $dateTable.="<tr><td style='".$thColor." text-align : center; '>".$fontTagLeft."診療科".$fontTagRight."</td>".$tableHeader."</tr>";
            $dateTable.=$naikaga;
            $dateTable.=$sinkei_ga;
            $dateTable.=$kokyu_ga;
            $dateTable.=$shoka_ga;
            $dateTable.=$junkanki_ga;
            $dateTable.=$shouni_ga;
            $dateTable.=$geka_ga;
            $dateTable.=$seikei_ga;
            $dateTable.=$keiseibi_ga;
            $dateTable.=$nou_ga;
            $dateTable.=$hifu_ga;
            $dateTable.=$hinyo_ga;
            $dateTable.=$sanfu_ga;
            $dateTable.=$ganka_ga;
            $dateTable.=$jibi_ga;
            $dateTable.=$touseki_ga;
            $dateTable.=$seisin_ga;
            $dateTable.=$sika_ga;
            $dateTable.=$hoshasen_ga;
            $dateTable.=$masui_ga;
            $dateTable.=$totalRow;

            $dateTable.="</tbody></table>";
            return $dateTable  ;

}

    /*
    *
    * This method gets excel column names
    *
    * @return string array  (excel column name string array)
    */
     function getExcelColumnNames(){
        $columns= array();
        // get the column names in array using ascii value
        for($i=0;$i<52;$i++){
            if((65+$i)<91){
                $columns[$i]=chr(65+$i);
            }else{
                $columns[$i]=chr(65).chr(65+($i-26));
            }
        }
        return $columns;
        }

    /*
    *
    * This method get the Diff of current and prev year formula for excelsheet
    *
    * @param $excelColumns (excel column name array)
    * @param $rowIndex (row index integer)
    * @param $columnIndex (column index integer)
    * @return string  (excel formula as string)
    */
    function getDiffCell($excelColumns, $rowIndex, $columnIndex){

        $strFormula ="=(".$excelColumns[$columnIndex+2].$rowIndex."-".$excelColumns[$columnIndex+2].($rowIndex+1).")";
        return $strFormula;
    }

    /*
    *
    * This method get the ratio of current and prev year formula for excelsheet
    *
    * @param $excelColumns (excel column name array)
    * @param $rowIndex (row index integer)
    * @param $columnIndex (column index integer)
    * @return string  (excel formula as string)
    */
    function getRatioCell($excelColumns, $rowIndex, $columnIndex){
        $strFormula ='=IF('.$excelColumns[$columnIndex+2].($rowIndex+1).'=0,0,ROUND((('.$excelColumns[$columnIndex+2].($rowIndex+2).'/'.$excelColumns[$columnIndex+2].($rowIndex+1).')),3))';
//        $strFormula ='=IF('.$excelColumns[$columnIndex+2].($rowIndex+1).'=0,0,ROUND((('.$excelColumns[$columnIndex+2].($rowIndex+2).'/'.$excelColumns[$columnIndex+2].($rowIndex+1).')*100),1))&"%"';
        return $strFormula;
    }

    /*
    *
    * This method get the total cell formula for excelsheet
    *
    * @param $excelColumns (excel column name array)
    * @param $depts (department index array)
    * @param $isCurrentYear (boolean value for year)
    * @param $nColumnIndex (column index integer)
    * @return string  (excel formula as string)
    */
    function getTotalCell($excelColumns,$depts,$isCurrentYear, $nColumnIndex){
        $strFormula="=(";
        $nCellIndex=0;
        //Check if the calculation for current/prev year
        if(!$isCurrentYear){
            $nCellIndex=1;
        }
        //Iterate through depts to create formula for addition
        for($i=0;$i<count($depts);$i++){
            $nRowIndex=$depts[$i]+$nCellIndex;
            if($i>0){
                $strFormula.="+";
            }
            $strFormula.=$excelColumns[$nColumnIndex].$nRowIndex;
        }
        $strFormula.=")";
        return $strFormula;
    }
    /*
    *
    * This method get the sum formula for excelsheet
    *
    * @param $excelColumns (excel column name array)
    * @param $currentRow (current row index integer)
    * @param $startIndex (starting index integer)
    * @param $totalColumns (column count integer)
    * @return stringax  (excel formula as string)
    */
    function getSumFormula($excelColumns, $currentRow,$startIndex,$totalColumns){
        $strFormula="=sum(".$excelColumns[$startIndex].$currentRow.":".$excelColumns[$totalColumns].$currentRow.")";
        /*
        for($i=$startIndex;$i<=$totalColumns;$i++){
            if($strFormula==""){
                $strFormula.="=(".$excelColumns[$i].$currentRow;
            }else{
                $strFormula.="+".$excelColumns[$i].$currentRow;
            }
        }*/
        return $strFormula;
      }
}
?>