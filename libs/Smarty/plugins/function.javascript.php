<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * smarty_function_javascript
 * @param type $params
 * @param type $smarty
 */
function smarty_function_javascript($params, &$smarty) {

    if (empty($params['src'])) {
        $smarty->trigger_error("javascript: missing 'src' parameter");
        return;
    }
    else {
        $src = $params['src'];
    }

    $time = filemtime($src);
    return sprintf('<script type="text/javascript" src="%s?%s"></script>', $src, $time);
}
