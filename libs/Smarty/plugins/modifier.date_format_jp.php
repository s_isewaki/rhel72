<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
/**
 * Include the {@link modifier.date_format.php} plugin
 */
require_once $smarty->_get_plugin_filepath('modifier', 'date_format');

/**
 * Smarty date_format_jp modifier plugin
 */
function smarty_modifier_date_format_jp($string, $format = '%b %e, %Y', $default_date = '') {
    $date = smarty_modifier_date_format($string, $format, $default_date);

    $date = str_replace('Sunday', '&#26085;&#26332;', $date);
    $date = str_replace('Monday', '&#26376;&#26332;', $date);
    $date = str_replace('Tuesday', '&#28779;&#26332;', $date);
    $date = str_replace('Wednesday', '&#27700;&#26332;', $date);
    $date = str_replace('Thursday', '&#26408;&#26332;', $date);
    $date = str_replace('Friday', '&#37329;&#26332;', $date);
    $date = str_replace('Saturday', '&#22303;&#26332;', $date);

    $date = str_replace('Sun', '&#26085;', $date);
    $date = str_replace('Mon', '&#26376;', $date);
    $date = str_replace('Tue', '&#28779;', $date);
    $date = str_replace('Wed', '&#27700;', $date);
    $date = str_replace('Thu', '&#26408;', $date);
    $date = str_replace('Fri', '&#37329;', $date);
    $date = str_replace('Sat', '&#22303;', $date);

    return $date;
}
