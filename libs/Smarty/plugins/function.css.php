<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * smarty_function_css
 * @param type $params
 * @param type $smarty
 */
function smarty_function_css($params, &$smarty) {

    if (empty($params['href'])) {
        $smarty->trigger_error("css: missing 'href' parameter");
        return;
    }
    else {
        $href = $params['href'];
    }

    $option = '';
    foreach ($params as $key => $value) {
        if ($key === 'href') {
            continue;
        }
        $option = sprintf('%s="%s" ', $key, $value);
    }

    $time = filemtime($href);
    return sprintf('<link rel="stylesheet" type="text/css" href="%s?%s" %s/>', $href, $time, $option);
}
