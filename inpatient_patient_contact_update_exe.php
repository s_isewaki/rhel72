<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($post1) > 3) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の郵便番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($post2) > 4) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の郵便番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($address1) > 100) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の住所1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($address2) > 100) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の住所2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel1) > 6) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の電話番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel2) > 6) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の電話番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel3) > 6) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の電話番号3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($email) > 120) {
	echo("<script type=\"text/javascript\">alert('連絡先情報のE-Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mobile1) > 6) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の携帯端末1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mobile2) > 6) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の携帯端末2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mobile3) > 6) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の携帯端末3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($m_mail) > 120) {
	echo("<script type=\"text/javascript\">alert('連絡先情報の携帯Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_name) > 100) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の勤務先名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_post1) > 3) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の郵便番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_post2) > 4) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の郵便番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_address1) > 100) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の住所1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_address2) > 100) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の住所2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_tel1) > 6) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の電話番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_tel2) > 6) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の電話番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_tel3) > 6) {
	echo("<script type=\"text/javascript\">alert('勤務先情報の電話番号3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($cmp_email) > 120) {
	echo("<script type=\"text/javascript\">alert('勤務先情報のE-Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_nm) > 100) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの氏名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_zip1) > 3) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの郵便番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_zip2) > 4) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの郵便番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_addr1) > 100) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの住所1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_addr2) > 100) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの住所2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_tel1) > 6) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの電話番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_tel2) > 6) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの電話番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_tel3) > 6) {
	echo("<script type=\"text/javascript\">alert('キーパーソンの電話番号3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_key_email) > 120) {
	echo("<script type=\"text/javascript\">alert('キーパーソンのE-Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_nm) > 100) {
	echo("<script type=\"text/javascript\">alert('身元引受人の氏名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_zip1) > 3) {
	echo("<script type=\"text/javascript\">alert('身元引受人の郵便番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_zip2) > 4) {
	echo("<script type=\"text/javascript\">alert('身元引受人の郵便番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_addr1) > 100) {
	echo("<script type=\"text/javascript\">alert('身元引受人の住所1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_addr2) > 100) {
	echo("<script type=\"text/javascript\">alert('身元引受人の住所2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_tel1) > 6) {
	echo("<script type=\"text/javascript\">alert('身元引受人の電話番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_tel2) > 6) {
	echo("<script type=\"text/javascript\">alert('身元引受人の電話番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_tel3) > 6) {
	echo("<script type=\"text/javascript\">alert('身元引受人の電話番号3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_spn_email) > 120) {
	echo("<script type=\"text/javascript\">alert('身元引受人のE-Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_nm) > 100) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの氏名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_biz_nm) > 100) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの事業所名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_zip1) > 3) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの郵便番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_zip2) > 4) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの郵便番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_addr1) > 100) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの住所1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_addr2) > 100) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの住所2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_tel1) > 6) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの電話番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_tel2) > 6) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの電話番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_tel3) > 6) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーの電話番号3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ptif_cm_email) > 120) {
	echo("<script type=\"text/javascript\">alert('ケアマネージャーのE-Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 患者情報を更新
$sql = "update ptifmst set";
$set = array("ptif_zip1", "ptif_zip2", "ptif_prv", "ptif_addr1", "ptif_addr2", "ptif_tel1", "ptif_tel2", "ptif_tel3", "ptif_mobile1", "ptif_mobile2", "ptif_mobile3", "ptif_email", "ptif_m_email", "ptif_com_nm", "ptif_com_zip1", "ptif_com_zip2", "ptif_com_prv", "ptif_com_addr1", "ptif_com_addr2", "ptif_bus_tel1", "ptif_bus_tel2", "ptif_bus_tel3", "ptif_bus_email", "ptif_key_nm", "ptif_key_zip1", "ptif_key_zip2", "ptif_key_prv", "ptif_key_addr1", "ptif_key_addr2", "ptif_key_tel1", "ptif_key_tel2", "ptif_key_tel3", "ptif_key_email", "ptif_spn_nm", "ptif_spn_zip1", "ptif_spn_zip2", "ptif_spn_prv", "ptif_spn_addr1", "ptif_spn_addr2", "ptif_spn_tel1", "ptif_spn_tel2", "ptif_spn_tel3", "ptif_spn_email", "ptif_cm_nm", "ptif_cm_biz_nm", "ptif_cm_zip1", "ptif_cm_zip2", "ptif_cm_prv", "ptif_cm_addr1", "ptif_cm_addr2", "ptif_cm_tel1", "ptif_cm_tel2", "ptif_cm_tel3", "ptif_cm_email");
$setvalue = array($post1, $post2, $province, $address1, $address2, $tel1, $tel2, $tel3, $mobile1, $mobile2, $mobile3, $email, $m_mail, $cmp_name, $cmp_post1, $cmp_post2, $cmp_province, $cmp_address1, $cmp_address2, $cmp_tel1, $cmp_tel2, $cmp_tel3, $cmp_email, $ptif_key_nm, $ptif_key_zip1, $ptif_key_zip2, $ptif_key_prv, $ptif_key_addr1, $ptif_key_addr2, $ptif_key_tel1, $ptif_key_tel2, $ptif_key_tel3, $ptif_key_email, $ptif_spn_nm, $ptif_spn_zip1, $ptif_spn_zip2, $ptif_spn_prv, $ptif_spn_addr1, $ptif_spn_addr2, $ptif_spn_tel1, $ptif_spn_tel2, $ptif_spn_tel3, $ptif_spn_email, $ptif_cm_nm, $ptif_cm_biz_nm, $ptif_cm_zip1, $ptif_cm_zip2, $ptif_cm_prv, $ptif_cm_addr1, $ptif_cm_addr2, $ptif_cm_tel1, $ptif_cm_tel2, $ptif_cm_tel3, $ptif_cm_email);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
if ($path == "A" || $path == "B") {
	$url_pt_nm = urlencode($pt_nm);
	echo("<script type=\"text/javascript\">location.href = 'bed_menu_inpatient_contact.php?session=$session&pt_id=$pt_id&path=$path&pt_nm=$url_pt_nm&search_sect=$search_sect&search_doc=$search_doc';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'inpatient_patient_contact.php?session=$session&pt_id=$pt_id&key1=$url_key1&key2=$url_key2&path=$path&ward=$ward&ptrm=$ptrm&bed=$bed&bedundec1=$bedundec1&bedundec2=$bedundec2&bedundec3=$bedundec3&in_dt=$in_dt&in_tm=$in_tm&ccl_id=$ccl_id&ptwait_id=$ptwait_id'</script>");
}
?>
