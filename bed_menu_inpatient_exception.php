<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | 平均在院日数除外指定</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("show_in_search.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 患者氏名を取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

$sql = "select inpt_except_flg, inpt_except_from_date1, inpt_except_to_date1, inpt_except_from_date2, inpt_except_to_date2, inpt_except_from_date3, inpt_except_to_date3, inpt_except_from_date4, inpt_except_to_date4, inpt_except_from_date5, inpt_except_to_date5, inpt_except_reason_id1, inpt_except_reason_id2, inpt_except_reason_id3, inpt_except_reason_id4, inpt_except_reason_id5 from inptmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$except_flg = pg_fetch_result($sel, 0, "inpt_except_flg");
for ($i = 1; $i <= 5; $i++) {
	$tmp_from_date_var = "from_date$i";
	$tmp_to_date_var = "to_date$i";
	$$tmp_from_date_var = pg_fetch_result($sel, 0, "inpt_except_from_date$i");
	$$tmp_to_date_var = pg_fetch_result($sel, 0, "inpt_except_to_date$i");

	$tmp_from_year_var = "from_year$i";
	$tmp_from_month_var = "from_month$i";
	$tmp_from_day_var = "from_day$i";
	$tmp_to_year_var = "to_year$i";
	$tmp_to_month_var = "to_month$i";
	$tmp_to_day_var = "to_day$i";
	$$tmp_from_year_var = substr($$tmp_from_date_var, 0, 4);
	$$tmp_from_month_var = substr($$tmp_from_date_var, 4, 2);
	$$tmp_from_day_var = substr($$tmp_from_date_var, 6, 2);
	$$tmp_to_year_var = substr($$tmp_to_date_var, 0, 4);
	$$tmp_to_month_var = substr($$tmp_to_date_var, 4, 2);
	$$tmp_to_day_var = substr($$tmp_to_date_var, 6, 2);

	$tmp_reason_id_var = "reason_id$i";
	$$tmp_reason_id_var = pg_fetch_result($sel, 0, "inpt_except_reason_id$i");
}

$current_year = date("Y");

// 計算除外理由情報を取得
$sql = "select * from bedexprsn";
$cond = "where display = 't' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$reason_info = array();
while ($row = pg_fetch_array($sel)) {
	$reason_info[$row["reason_id"]] = $row["reason"];
}

$url_pt_nm = urlencode($pt_nm);

// 診療科情報、主治医情報を配列に格納
list($arr_sect, $arr_sectdr) = get_sectdr($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	sectOnChange2('<? echo($search_doc); ?>');
}

function sectOnChange2(doc) {
	var sect_id = document.search.search_sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.search.search_doc);

	// 主治医セレクトボックスのオプションを作成
	addOption(document.search.search_doc, '0', 'すべて', doc);
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.search.search_doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院患者検索</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? write_search_form($pt_nm, $arr_sect, $search_sect, $session); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- タブ2 -->
<? if ($path == "A") {  // 入院患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>平均在院日数除外指定</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "B") {  // 退院予定患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>平均在院日数除外指定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_out_reserve.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定情報</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="inpt" action="inpatient_exception_confirm.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="except_flg" value="t"<? if ($except_flg == "t") {echo(" checked");} ?>>平均在院日数計算除外患者として指定する</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">除外開始日</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">除外終了日</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">除外理由</font></td>
</tr>
<tr height="22">
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="from_year1"><option value="-"><? show_select_years_span(1960, $current_year + 1, $from_year1); ?></select>年<select name="from_month1"><? show_select_months($from_month1, true); ?></select>月<select name="from_day1"><? show_select_days($from_day1, true); ?></select>日</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="to_year1"><option value="-"><? show_select_years_span(1960, $current_year + 1, $to_year1); ?></select>年<select name="to_month1"><? show_select_months($to_month1, true); ?></select>月<select name="to_day1"><? show_select_days($to_day1, true); ?></select>日</font></td>
<td><select name="reason_id1">
<option value="">
<? foreach ($reason_info as $tmp_reason_id => $tmp_reason) { ?>
<option value="<? echo($tmp_reason_id); ?>"<? if ($tmp_reason_id == $reason_id1) {echo(" selected");} ?>><? echo($tmp_reason); ?>
<? } ?>
</select></td>
</tr>
<tr height="22">
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="from_year2"><option value="-"><? show_select_years_span(1960, $current_year + 1, $from_year2); ?></select>年<select name="from_month2"><? show_select_months($from_month2, true); ?></select>月<select name="from_day2"><? show_select_days($from_day2, true); ?></select>日</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="to_year2"><option value="-"><? show_select_years_span(1960, $current_year + 1, $to_year2); ?></select>年<select name="to_month2"><? show_select_months($to_month2, true); ?></select>月<select name="to_day2"><? show_select_days($to_day2, true); ?></select>日</font></td>
<td><select name="reason_id2">
<option value="">
<? foreach ($reason_info as $tmp_reason_id => $tmp_reason) { ?>
<option value="<? echo($tmp_reason_id); ?>"<? if ($tmp_reason_id == $reason_id2) {echo(" selected");} ?>><? echo($tmp_reason); ?>
<? } ?>
</select></td>
</tr>
<tr height="22">
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="from_year3"><option value="-"><? show_select_years_span(1960, $current_year + 1, $from_year3); ?></select>年<select name="from_month3"><? show_select_months($from_month3, true); ?></select>月<select name="from_day3"><? show_select_days($from_day3, true); ?></select>日</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="to_year3"><option value="-"><? show_select_years_span(1960, $current_year + 1, $to_year3); ?></select>年<select name="to_month3"><? show_select_months($to_month3, true); ?></select>月<select name="to_day3"><? show_select_days($to_day3, true); ?></select>日</font></td>
<td><select name="reason_id3">
<option value="">
<? foreach ($reason_info as $tmp_reason_id => $tmp_reason) { ?>
<option value="<? echo($tmp_reason_id); ?>"<? if ($tmp_reason_id == $reason_id3) {echo(" selected");} ?>><? echo($tmp_reason); ?>
<? } ?>
</select></td>
</tr>
<tr height="22">
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="from_year4"><option value="-"><? show_select_years_span(1960, $current_year + 1, $from_year4); ?></select>年<select name="from_month4"><? show_select_months($from_month4, true); ?></select>月<select name="from_day4"><? show_select_days($from_day4, true); ?></select>日</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="to_year4"><option value="-"><? show_select_years_span(1960, $current_year + 1, $to_year4); ?></select>年<select name="to_month4"><? show_select_months($to_month4, true); ?></select>月<select name="to_day4"><? show_select_days($to_day4, true); ?></select>日</font></td>
<td><select name="reason_id4">
<option value="">
<? foreach ($reason_info as $tmp_reason_id => $tmp_reason) { ?>
<option value="<? echo($tmp_reason_id); ?>"<? if ($tmp_reason_id == $reason_id4) {echo(" selected");} ?>><? echo($tmp_reason); ?>
<? } ?>
</select></td>
</tr>
<tr height="22">
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="from_year5"><option value="-"><? show_select_years_span(1960, $current_year + 1, $from_year5); ?></select>年<select name="from_month5"><? show_select_months($from_month5, true); ?></select>月<select name="from_day5"><? show_select_days($from_day5, true); ?></select>日</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="to_year5"><option value="-"><? show_select_years_span(1960, $current_year + 1, $to_year5); ?></select>年<select name="to_month5"><? show_select_months($to_month5, true); ?></select>月<select name="to_day5"><? show_select_days($to_day5, true); ?></select>日</font></td>
<td><select name="reason_id5">
<option value="">
<? foreach ($reason_info as $tmp_reason_id => $tmp_reason) { ?>
<option value="<? echo($tmp_reason_id); ?>"<? if ($tmp_reason_id == $reason_id5) {echo(" selected");} ?>><? echo($tmp_reason); ?>
<? } ?>
</select></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="pt_nm" value="<? echo($pt_nm); ?>">
<input type="hidden" name="search_sect" value="<? echo($search_sect); ?>">
<input type="hidden" name="search_doc" value="<? echo($search_doc); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
