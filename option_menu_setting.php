<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix オプション設定 | 表示設定</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("label_by_profile_type.ini");
require_once("show_menu_setting_common.ini");
require_once("webmail_alias_functions.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限の取得
$authorities = get_authority($session, $fname);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// オプション設定値を取得
$sql = "select sidemenu_show_flg, sidemenu_position from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sidemenu_show_flg = pg_fetch_result($sel, 0, "sidemenu_show_flg");
$sidemenu_position = pg_fetch_result($sel, 0, "sidemenu_position");
if ($sidemenu_show_flg == "") {$sidemenu_show_flg = "t";}
if ($sidemenu_position == "") {$sidemenu_position = "1";}

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
	$lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="option_register.php?session=<? echo($session); ?>"><img src="img/icon/b15.gif" width="32" height="32" border="0" alt="オプション設定"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="option_register.php?session=<? echo($session); ?>"><b>オプション設定</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#bdd1e7"><a href="option_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="option_menu_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>メニュー設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="option_mygroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="option_alias.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<!--<img src="img/spacer.gif" alt="" width="1" height="2"><br>-->
<form name="mainform" action="option_menu_setting_update.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ヘッダメニュー</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<? show_header_menu_list($con, $emp_id, $authorities, $lcs_func, $fname); ?>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>サイドメニュー</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイドメニューを表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sidemenu_show_flg" value="t"<? if ($sidemenu_show_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="sidemenu_show_flg" value="f"<? if ($sidemenu_show_flg == "f") {echo(" checked");} ?>>しない
</font></td>
<td width="100" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示サイド</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sidemenu_position" value="1"<? if ($sidemenu_position == "1") {echo(" checked");} ?>>右
<input type="radio" name="sidemenu_position" value="2"<? if ($sidemenu_position == "2") {echo(" checked");} ?>>左
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
