<?php
require_once 'jnl_indicator_data.php';
require_once 'jnl_indicator_return.php';
require_once 'jnl_indicator_facility_data.php';
class IndicatorReturnData extends IndicatorData
{
    var $indicatorReturn;

    var $indicatorFacilityData;

    /**
     * コンストラクタ
     * @param $arr
     */
    function IndicatorReturnData($arr)
    {
        parent::IndicatorData($arr);
    }

    function getNoForNo($no)
    {
        /* @var $indicatorReturn IndicatorReturn */
        $indicatorReturn = $this->getIndicatorReturn();
        $noArr = $indicatorReturn->noArr;
        $data  = $this->getArrValueByKey($no, $noArr);
        if (is_null($data)) {
            $indicatorFacilityData = $this->getIndicatorFacilityData();
            $data = $indicatorFacilityData->getNoForNo($no);
        }
        return $data;
    }

    function getIndicatorReturn()
    {
        if (!is_object($this->indicatorReturn)) { $this->setIndicatorReturn(); }
        return $this->indicatorReturn;
    }

    function setIndicatorReturn()
    {
        $this->indicatorReturn = new IndicatorReturn(array('fname' => $this->fname, 'con' => $this->con));
    }

    function getIndicatorFacilityData()
    {
        if (!is_object($this->indicatorFacilityData)) { $this->setIndicatorFacilityData(); }
        return $this->indicatorFacilityData;
    }

    function setIndicatorFacilityData()
    {
        $this->indicatorFacilityData = new IndicatorFacilityData(array('fname' => $this->fname, 'con' => $this->con));
    }


// [#1] added function for return screens to fetch data for links
 function getReturnDisplayDataList($no, $lastYearMonth, $currentYearMonth, $dateArr)
    {
        $data = false;

        switch ($no) {
// [#2] added case to call method for fetching data


      case '300':
                $data = $this->getjnlIndicatorReturnFactorPoint($lastYearMonth, $currentYearMonth);
                break;
// [@2]


 case '301':
                $data = $this->getjnlIndicatorReturnFactorAccount($lastYearMonth, $currentYearMonth);
                break;


case '302':
                $data = $this->getjnlIndicatorReturnAssessmentPoint($lastYearMonth, $currentYearMonth);
                break;

case '303':
                $data = $this->getjnlIndicatorReturnAssesmentAccount($lastYearMonth, $currentYearMonth);
                break;
case '304':
                $data = $this->getjnlIndicatorReturnRecharge($lastYearMonth, $currentYearMonth);
                break;
case '305':
                $data = $this->getjnlIndicatorReturnList($dateArr);
                break;



   default:
                break;
        }

        return $data;
    }

function deleteTableData($sql)
{
       $this->setSql($sql);
        $pgResult = delete_from_table($this->con, $sql, '', $this->fname);
        if ($pgResult===0) {
            return false;
        }
        return true;
}

function insertTableData($sql)
{

       $this->setSql($sql);
        $pgResult = insert_into_table($this->con, $sql, '', $this->fname);
        if ($pgResult===0) {
            return false;
        }
        return $pgResult;
}

// [@1] arr
// [#3] implementation of method to fetch data
function getjnlIndicatorReturnFactorPoint($lastYearMonth, $currentYearMonth)
    {




$sql = "SELECT year , month , facility_id , CASE WHEN total_point > 0 THEN round(((CAST(Col108 AS numeric)+CAST(Col267 AS numeric))/CAST(total_point AS numeric)),3)*100 ELSE 0 END AS GOKEI , status , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(point_si,0) + COALESCE(reject_point_si,0) ) as Col108,
            sum(COALESCE(point_ni,0) + COALESCE(reject_point_ni,0)) as Col267,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_factor RF , jnl_return_total as RT
where
RM.newest_apply_id=RF.newest_apply_id
and RT.newest_apply_id=RF.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) >= '".$lastYearMonth."'
AND SUBSTR(RM.regist_date, 0, 7) <= '".$currentYearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)
or (kind_factor=14 and kind_consult=1) or (kind_factor=14 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) AS Q1
order by facility_id, year,month";
     $tmpData  = $this->getDateList($sql);
 //var_dump($tmpData);
 return $tmpData;
    }

// [@3]


function getjnlIndicatorReturnFactorAccount($lastYearMonth, $currentYearMonth)
   {

$sql = "SELECT year
                  , month
                  , facility_id
                  ,  CASE WHEN total_account > 0 THEN round(((CAST(Col55 AS numeric)+CAST(Col214 AS numeric))/CAST(total_account AS numeric)),3)*100 ELSE 0 END AS GOKEI
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(account_si,0) + COALESCE(reject_account_si,0)) as Col55,
            sum(COALESCE(account_ni,0) + COALESCE(reject_account_ni,0)) as Col214,
            COALESCE(total_account,0) as total_account,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_factor RF , jnl_return_total as RT
where
RM.newest_apply_id=RF.newest_apply_id
and RT.newest_apply_id=RF.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) >= '".$lastYearMonth."'
AND SUBSTR(RM.regist_date, 0, 7) <= '".$currentYearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)
or (kind_factor=14 and kind_consult=1) or (kind_factor=14 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_account,rm.jnl_status
) AS Q1
order by facility_id, year,month";


     $tmpData  = $this->getDateList($sql);

 return $tmpData;
    }

function getjnlIndicatorReturnAssessmentPoint( $lastYearMonth, $currentYearMonth)
      {

$sql = "SELECT year
                  , month
                  , facility_id
                  , CASE WHEN total_point > 0 THEN round(((CAST(Col412 AS numeric)+CAST(Col547 AS numeric))/CAST(total_point AS numeric)),3)*100 ELSE 0 END AS GOKEI
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(point_si,0) + COALESCE(reject_point_si,0)) as Col412,
            sum(COALESCE(point_ni,0) + COALESCE(reject_point_ni,0)) as Col547,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_assessment RA , jnl_return_total as RT
where
RM.newest_apply_id=RA.newest_apply_id
and RT.newest_apply_id=RA.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) >= '".$lastYearMonth."'
AND SUBSTR(RM.regist_date, 0, 7) <= '".$currentYearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) AS Q1
order by facility_id, year,month";
     $tmpData  = $this->getDateList($sql);
 return $tmpData;
    }

function getjnlIndicatorReturnAssesmentAccount($lastYearMonth, $currentYearMonth)
     {

$sql = "SELECT year
                  , month
                  , facility_id
                  , CASE WHEN total_account > 0 THEN round(((CAST(Col367 AS numeric)+CAST(Col502 AS numeric))/CAST(total_account AS numeric)),3)*100 ELSE 0 END AS GOKEI
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(account_si,0) + COALESCE(reject_account_si,0)) as Col367,
            sum(COALESCE(account_ni,0) + COALESCE(reject_account_ni,0)) as Col502,
            COALESCE(total_account,0) as total_account,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_assessment RA , jnl_return_total as RT
where
RM.newest_apply_id=RA.newest_apply_id
and RT.newest_apply_id=RA.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) >= '".$lastYearMonth."'
AND SUBSTR(RM.regist_date, 0, 7) <= '".$currentYearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)
or (kind_factor=14 and kind_consult=1) or (kind_factor=14 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_account,rm.jnl_status
) AS Q1
order by facility_id, year,month";


     $tmpData  = $this->getDateList($sql);
 return $tmpData;
    }




function getjnlIndicatorReturnRecharge($lastYearMonth, $currentYearMonth)
     {
		
//2011/05/16 Line:351から 「 +COALESCE(point_di,0)」を削除しました。（返戻ーその他ー再請求率の計算が間違っているので、返戻・査定・再請求一覧の分子に合わせました）
//2011/05/16 Line:366に 「  or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)」を追加しました。（返戻ーその他ー再請求率の計算が間違っているので、返戻・査定・再請求一覧の分子に合わせました）
		

$sql = "SELECT year , month , facility_id , CASE WHEN Denominator > 0 THEN round((CAST(numerator as numeric)*100/CAST(Denominator as numeric)),3) ELSE 0 END AS GOKEI, status , valid FROM (SELECT Q1.year , Q1.month , Q1.facility_id , Col636 as numerator     , (col108+col267) as denominator  , Q1.status , Q1.valid FROM (SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id,
            sum(COALESCE(point_si,0)+COALESCE(point_ni,0)) as col636,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_re_charge RR , jnl_return_total as RT
where
RM.newest_apply_id=RR.newest_apply_id
and RT.newest_apply_id=RR.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) >= '".$lastYearMonth."'
AND SUBSTR(RM.regist_date, 0, 7) <= '".$currentYearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2) or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) as Q1,
(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(point_si,0) + COALESCE(reject_point_si,0)) as Col108,
            sum(COALESCE(point_ni,0) + COALESCE(reject_point_ni,0)) as Col267,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_factor RF , jnl_return_total as RT
where
RM.newest_apply_id=RF.newest_apply_id
and RT.newest_apply_id=RF.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) >= '".$lastYearMonth."'
AND SUBSTR(RM.regist_date, 0, 7) <= '".$currentYearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)
or (kind_factor=14 and kind_consult=1) or (kind_factor=14 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) AS Q2
where Q1.year=Q2.year AND
q1.MONTH=q2.MONTH AND
q1.FACILITY_ID=q2.FACILITY_ID
order by facility_id, year,month) AS q3";

     $tmpData  = $this->getDateList($sql);
 return $tmpData;
    }


function getjnlIndicatorReturnList($dateArr)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
    $deleteTabledatasql ="
        delete from jnl_return_temp1 ;
        delete from jnl_return_temp3 ;
        delete from jnl_return_temp2 ;
        delete from jnl_return_temp4 ;
        delete from jnl_return_temp7 ;
        delete from jnl_return_temp8 ;";
    $deleteStatusData = $this->deleteTableData($deleteTabledatasql);
    $inserttemp1 = "insert into jnl_return_temp1
(
regyear ,
regmonth ,
facility_id ,
point_factor_ratio,
status ,
valid
)
(
SELECT year
                  , month
                  , facility_id
                  ,
CASE WHEN total_point > 0 THEN ROUND((CAST((Col108+Col267) as Numeric)/CAST(total_point as Numeric))* 100,1) ELSE 0 END as point_factor_ratio
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(point_si,0) + COALESCE(reject_point_si ,0)) as Col108,
            sum(COALESCE(point_ni,0) + COALESCE(reject_point_ni,0)) as Col267,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_factor RF , jnl_return_total as RT
where
RM.newest_apply_id=RF.newest_apply_id
and RT.newest_apply_id=RF.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2
and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)

)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) AS Q1
";


    $inserttemp3="insert into jnl_return_temp3
(
regyear ,
regmonth ,
facility_id ,
point_assess_ratio ,
status ,
valid
)
(
SELECT year
                  , month
                  , facility_id
                  , CASE WHEN total_point > 0 THEN ROUND((CAST((Col412+Col547) as numeric)/CAST(total_point as numeric))*100,1) ELSE 0 END as point_assess_ratio
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(point_si,0) + COALESCE(reject_point_si,0)) as Col412,
            sum(COALESCE(point_ni,0) + COALESCE(reject_point_ni,0)) as Col547,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_assessment RA , jnl_return_total as RT
where
RM.newest_apply_id=RA.newest_apply_id
and RT.newest_apply_id=RA.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) AS Q1";


    $inserttemp2="insert into jnl_return_temp2
(
regyear ,
regmonth ,
facility_id ,
account_factor_ratio,
status ,
valid
)
(
SELECT year
                  , month
                  , facility_id
                  , CASE WHEN total_account > 0 THEN ROUND((CAST((Col55+Col214) as numeric)/CAST(total_account as numeric))*100,1) ELSE 0 END as account_factor_ratio
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(account_si,0) + COALESCE(reject_account_si,0)) as Col55,
            sum(COALESCE(account_ni,0) + COALESCE(reject_account_ni,0)) as Col214,
            COALESCE(total_account,0) as total_account,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_factor RF , jnl_return_total as RT
where
RM.newest_apply_id=RF.newest_apply_id
and RT.newest_apply_id=RF.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7)  = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2
and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)

)
group by FM.jnl_facility_id, regist_date,total_account,rm.jnl_status
) AS Q1";

$inserttemp4="insert into jnl_return_temp4
(
regyear ,
regmonth ,
facility_id ,
account_assess_ratio,
status ,
valid
)
(
SELECT year
                  , month
                  , facility_id
                  , CASE WHEN total_account > 0 THEN ROUND((CAST((Col367+Col502) as numeric)/CAST(total_account as numeric))*100,1) ELSE 0 END as account_assess_ratio
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(account_si,0) + COALESCE(reject_account_si,0)) as Col367,
            sum(COALESCE(account_ni,0) + COALESCE(reject_account_ni,0)) as Col502,
            COALESCE(total_account,0) as total_account,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_assessment RA , jnl_return_total as RT
where
RM.newest_apply_id=RA.newest_apply_id
and RT.newest_apply_id=RA.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7)  = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2

and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)
or (kind_factor=14 and kind_consult=1) or (kind_factor=14 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_account,rm.jnl_status
) AS Q1";

    $inserttemp7="insert into jnl_return_temp7
(
regyear ,
regmonth ,
facility_id ,
point_factor,
status ,
valid
)
(
SELECT year
                  , month
                  , facility_id
                  , CAST((Col108+Col267) as numeric) as point_factor
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
            sum(COALESCE(point_si,0) + COALESCE(reject_point_si,0)) as Col108,
            sum(COALESCE(point_ni,0) + COALESCE(reject_point_ni,0)) as Col267,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_factor RF , jnl_return_total as RT
where
RM.newest_apply_id=RF.newest_apply_id
and RT.newest_apply_id=RF.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2
and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2)
or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
or (kind_factor=3 and kind_consult=1) or (kind_factor=3 and kind_consult=2)
or (kind_factor=4 and kind_consult=1) or (kind_factor=4 and kind_consult=2)
or (kind_factor=5 and kind_consult=1) or (kind_factor=5 and kind_consult=2)
or (kind_factor=6 and kind_consult=1) or (kind_factor=6 and kind_consult=2)
or (kind_factor=7 and kind_consult=1) or (kind_factor=7 and kind_consult=2)
or (kind_factor=8 and kind_consult=1) or (kind_factor=8 and kind_consult=2)
or (kind_factor=9 and kind_consult=1) or (kind_factor=9 and kind_consult=2)
or (kind_factor=10 and kind_consult=1) or (kind_factor=10 and kind_consult=2)
or (kind_factor=11 and kind_consult=1) or (kind_factor=11 and kind_consult=2)
or (kind_factor=12 and kind_consult=1) or (kind_factor=12 and kind_consult=2)
or (kind_factor=13 and kind_consult=1) or (kind_factor=13 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) AS Q1";

    $inserttemp8="insert into jnl_return_temp8
(
regyear ,
regmonth ,
facility_id ,
point_recharge,
point_insurance,
status ,
valid
)
(

SELECT year
                  , month
                  , facility_id
				, Col625630 as point_recharge
          , Col635 as point_insurance
                  , status
                  , valid
              FROM

(SELECT SUBSTR(regist_date, 0, 5) AS year
                          , SUBSTR(regist_date, 5, 2) AS month
                          , FM.jnl_facility_id AS facility_id
                          ,
				sum(COALESCE(point_si,0)+COALESCE(point_ni,0)) as Col625630,
            sum( COALESCE(point_di,0)) as Col635,
            COALESCE(total_point,0) as total_point,
            rm.jnl_status as status,
            CASE WHEN RM.jnl_status = 2 THEN 0 WHEN RM.jnl_status = 3 THEN 0 ELSE 1 END AS valid
                        from
            jnl_facility_master FM
inner join jnl_return_main RM ON RM.jnl_facility_id = FM.jnl_facility_id,
jnl_return_re_charge RF , jnl_return_total as RT
where
RM.newest_apply_id=RF.newest_apply_id
and RT.newest_apply_id=RF.newest_apply_id
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2
				and ((kind_factor=1 and kind_consult=1) or (kind_factor=1 and kind_consult=2) or (kind_factor=2 and kind_consult=1) or (kind_factor=2 and kind_consult=2)
)
group by FM.jnl_facility_id, regist_date,total_point,rm.jnl_status
) AS Q1";
    $resultInsert1 = $this->insertTableData($inserttemp1);
    $resultInsert3 = $this->insertTableData($inserttemp3);
    $resultInsert2 = $this->insertTableData($inserttemp2);
    $resultInsert4 = $this->insertTableData($inserttemp4);
    $resultInsert7 = $this->insertTableData($inserttemp7);
    $resultInsert8 = $this->insertTableData($inserttemp8);


        $sql = "select  FM.jnl_facility_id as facility_id,
        FM.jnl_facility_name as facility_name,
        jnl_return_temp1.point_factor_ratio as point_factor_ratio,
    jnl_return_temp3.point_assess_ratio as point_assess_ratio,
    (jnl_return_temp1.point_factor_ratio+jnl_return_temp3.point_assess_ratio) as point_sum_ratio,
    jnl_return_temp2.account_factor_ratio,
    jnl_return_temp4.account_assess_ratio as account_assess_ratio,
    (jnl_return_temp2.account_factor_ratio+jnl_return_temp4.account_assess_ratio) as account_sum_ratio,
    jnl_return_temp7.point_factor as point_factor,
        jnl_return_temp8.point_recharge as point_recharge,
        jnl_return_temp8.point_insurance as point_insurance,
    CASE WHEN jnl_return_temp7.point_factor > 0 THEN round((((cast(jnl_return_temp8.point_recharge as numeric))*100)/cast(jnl_return_temp7.point_factor as numeric)),1)   ELSE NULL END as recharge_ratio
    from  jnl_facility_master FM
left outer join jnl_return_temp1  on jnl_return_temp1.facility_id = FM.jnl_facility_id
left outer join jnl_return_temp2 on jnl_return_temp2.facility_id = FM.jnl_facility_id
left outer join jnl_return_temp3 on jnl_return_temp3.facility_id = FM.jnl_facility_id
left outer join jnl_return_temp4 on jnl_return_temp4.facility_id = FM.jnl_facility_id
left outer join jnl_return_temp7 on jnl_return_temp7.facility_id = FM.jnl_facility_id
left outer join jnl_return_temp8 on jnl_return_temp8.facility_id = FM.jnl_facility_id
where jnl_facility_type='1' order by FM.jnl_facility_id
";

       $tmpData = $this->getDateList($sql);

//var_dump($tmpData);


       return $tmpData;
    }



function getjnlIndicatorReturnAnalysisTable1($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
    $deleteTabledatasql ="delete from  jnl_Screen7_temp1;";
    $deleteStatusData = $this->deleteTableData($deleteTabledatasql);
    // InsertRow1
          $insertRow1="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT ROWID, min(status12) as status12 , min(status34) as status34, SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  1 AS ROWID, min(jnl_status) as status12 , null as status34
                          , sum(COALESCE(RF.account_si,0)) as Col1
              , sum(COALESCE(RF.point_si,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=1
union all
(
                     SELECT      1 AS ROWID , null as status12, max(jnl_status) as status34 , 0 AS COL1, 0 AS COL2,
            sum(COALESCE(RA.account_si,0))  as Col3
                          ,sum(COALESCE(RA.point_si,0))  as Col4
                       from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=1
)
) AS Q1
GROUP BY ROWID
";
 //echo "xdc".$insertRow1;
    $resultInsertRow1 = $this->insertTableData($insertRow1);

    $insertRow2="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4

)
(
SELECT ROWID, min(status12) as status12 , min(status34) as status34, SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  2 AS ROWID , min(jnl_status) as status12 , null as status34,
                           sum(COALESCE(RF.reject_account_si,0)) as Col1
              , sum(COALESCE(RF.reject_point_si,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=1
union all
(
                     SELECT      2 AS ROWID, null as status12, max(jnl_status) as status34, 0 AS COL1, 0 AS COL2,
            sum(COALESCE(RA.reject_account_si,0))  as Col3
                          ,sum(COALESCE(RA.reject_point_si,0))  as Col4


                        from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id

and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=1
)
) AS Q1
GROUP BY ROWID
";
    $resultInsertRow2 = $this->insertTableData($insertRow2);

    $insertRow3="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT ROWID,min(status12) as status12, min(status34) as status34,SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  3 AS ROWID, min(jnl_status) as status12 , null as status34
                          , sum(COALESCE(RF.account_si,0)) as Col1
              , sum(COALESCE(RF.point_si,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=2
union all
(
                     SELECT      3 AS ROWID,  null as status12, max(jnl_status) as status34, 0 AS COL1, 0 AS COL2,
            sum(COALESCE(RA.account_si,0))  as Col3
                          ,sum(COALESCE(RA.point_si,0))  as Col4


                        from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id

and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=2
)
) AS Q1
GROUP BY ROWID
";
    $resultInsertRow3 = $this->insertTableData($insertRow3);

    $insertRow4="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT ROWID,min(status12) as status12, min(status34) as status34, SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  4 AS ROWID, min(jnl_status) as status12 , null as status34
                          , sum(COALESCE(RF.reject_account_si,0)) as Col1
              , sum(COALESCE(RF.reject_point_si,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=2
union all
(
                     SELECT      4 AS ROWID, null as status12, max(jnl_status) as status34, 0 AS COL1, 0 AS COL2,
            sum(COALESCE(RA.reject_account_si,0))  as Col3
                          ,sum(COALESCE(RA.reject_point_si,0))  as Col4


                        from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id

and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=2
)
) AS Q1
GROUP BY ROWID
";
    $resultInsertRow4 = $this->insertTableData($insertRow4);


$insertRow5="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT  5 ,min(status12) as status12, min(status34) as status34
                          , sum(Col1)  as Col1
              , sum(Col2)  as Col2
                          , sum(Col3)   as Col3
                          ,sum(Col4)  as Col4
                        from  jnl_Screen7_temp1
where rowID between 1 and 4";
    $resultInsertRow5 = $this->insertTableData($insertRow5);
$insertRow6="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT ROWID, min(status12) as status12, min(status34) as status34, SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  6 AS ROWID, min(jnl_status) as status12 , null as status34
                          , sum(COALESCE(RF.account_ni,0)) as Col1
              , sum(COALESCE(RF.point_ni,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=1
union all
(
                     SELECT      6 AS ROWID, null as status12, max(jnl_status) as status34,0 AS COL1, 0 AS COL2,
            sum(COALESCE(RA.account_ni,0))  as Col3
                          ,sum(COALESCE(RA.point_ni,0))  as Col4


                        from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id

and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=1
)
) AS Q1
GROUP BY ROWID
";
    $resultInsertRow6 = $this->insertTableData($insertRow6);
    $insertRow7="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT ROWID, min(status12) as status12, min(status34) as status34,SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  7 AS ROWID, min(jnl_status) as status12 , null as status34
                          , sum(COALESCE(RF.reject_account_ni,0)) as Col1
              , sum(COALESCE(RF.reject_point_ni,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=1
union all
(
                     SELECT      7 AS ROWID, null as status12, max(jnl_status) as status34,0 AS COL1, 0 AS COL2,
            sum(RA.reject_account_ni)  as Col3
                          ,sum(RA.reject_point_ni)  as Col4


                        from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id

and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=1
)
) AS Q1
GROUP BY ROWID
";
    $resultInsertRow7 = $this->insertTableData($insertRow7);

    $insertRow8="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT ROWID, min(status12) as status12, min(status34) as status34,SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  8 AS ROWID, min(jnl_status) as status12 , null as status34
                          , sum(COALESCE(RF.account_ni,0)) as Col1
              , sum(COALESCE(RF.point_ni,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=2
union all
(
                     SELECT      8 AS ROWID, null as status12, max(jnl_status) as status34,0 AS COL1, 0 AS COL2,
            sum(COALESCE(RA.account_ni,0))  as Col3
                          ,sum(COALESCE(RA.point_ni,0))  as Col4


                        from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id

and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=2
)
) AS Q1
GROUP BY ROWID
";
    $resultInsertRow8 = $this->insertTableData($insertRow8);

    $insertRow9="insert into jnl_Screen7_temp1
(
rowID,
status12, status34 ,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT ROWID, min(status12) as status12, min(status34) as status34,SUM(COL1) AS COL1, SUM(COL2) AS COL2, SUM(COL3) AS COL3, SUM(COL4) AS COL4
FROM (
SELECT  9 AS ROWID, min(jnl_status) as status12 , null as status34
                          , sum(COALESCE(RF.reject_account_ni,0)) as Col1
              , sum(COALESCE(RF.reject_point_ni,0))  as Col2,
            0 AS COL3,
            0 AS COL4
                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where
RM.jnl_facility_id = FM.jnl_facility_id
AND RM.newest_apply_id=RF.newest_apply_id
and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and RF.kind_consult=2
union all
(
                     SELECT      9 AS ROWID, null as status12, max(jnl_status) as status34,0 AS COL1, 0 AS COL2,
            sum(COALESCE(RA.reject_account_ni,0))  as Col3
                          ,sum(COALESCE(RA.reject_point_ni,0))  as Col4


                        from
            jnl_facility_master FM,
 jnl_return_main RM ,jnl_return_assessment RA

where
RM.jnl_facility_id = FM.jnl_facility_id and
rm.newest_apply_id=RA.newest_apply_id

and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
 AND RM.del_flg = 'f' AND jnl_status < 2
and  RA.kind_consult=2
)
) AS Q1
GROUP BY ROWID
";
    $resultInsertRow9 = $this->insertTableData($insertRow9);

$insertRow10="insert into jnl_Screen7_temp1
(
rowID,
status12, status34,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT  10, min(status12) as status12, min(status34) as status34
                          , sum(Col1)  as Col1
              , sum(Col2)  as Col2
                          , sum(Col3)   as Col3
                          ,sum(Col4)  as Col4
                        from  jnl_Screen7_temp1
where rowID between 6 and 9";
    $resultInsertRow10 = $this->insertTableData($insertRow10);

$insertRow11="insert into jnl_Screen7_temp1
(
rowID,
status12, status34,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT  11, min(status12) as status12, min(status34) as status34
                          , sum(Col1)  as Col1
              , sum(Col2)  as Col2
                          , sum(Col3)   as Col3
                          ,sum(Col4)  as Col4
                        from  jnl_Screen7_temp1
where rowID in (5,10)";
    $resultInsertRow11 = $this->insertTableData($insertRow11);

$sql="select COALESCE(Col1, 0) as Col1, COALESCE(Col2, 0) as Col2, COALESCE(Col3, 0) as Col3, COALESCE(Col4, 0) as Col4, (COALESCE(Col1, 0)+ COALESCE(Col3, 0)) as Col5, ( COALESCE(Col2, 0)+ COALESCE(Col4, 0)) as Col6 , status12, status34  from  jnl_Screen7_temp1";





       $tmpData = $this->getDateList($sql);
//var_dump($tmpData);
       return $tmpData;
    }
function getjnlIndicatorReturnAnalysisTable1LastRowCol1($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
	$sql="select min(jnl_status) as status,COALESCE((sum(COALESCE(money_si,0)+ COALESCE(reject_money_si,0)+ COALESCE(money_ni,0)+ COALESCE(reject_money_ni,0))),0) as sum1
from jnl_facility_master FM, jnl_return_main RM , jnl_return_factor RF
where
RM.jnl_facility_id = FM.jnl_facility_id and
RM.newest_apply_id=RF.newest_apply_id and
FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2 ";
    $tmpData = $this->getDateList($sql);
//    var_dump($tmpData);
    return $tmpData;


}
function getjnlIndicatorReturnAnalysisTable1LastRowCol2($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
	$sql="select min(jnl_status) as status, COALESCE((sum(COALESCE(money_si,0)+ COALESCE(reject_money_si,0)+ COALESCE(money_ni,0)+ COALESCE(reject_money_ni,0))),0) as sum2
from jnl_facility_master FM, jnl_return_main RM , jnl_return_assessment RA
where
RM.jnl_facility_id = FM.jnl_facility_id and
RM.newest_apply_id=RA.newest_apply_id and
FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'
AND RM.del_flg = 'f' AND jnl_status < 2 ";
    $tmpData = $this->getDateList($sql);
    //var_dump($tmpData);
    return $tmpData;


}


function getjnlIndicatorReturnAnalysisTable2($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
    $deleteTabledatasql ="delete from  jnl_Screen7_temp2";
    $deleteStatusData = $this->deleteTableData($deleteTabledatasql);
    // InsertRow1
          $insertRow1="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  1,min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))  as Col1
                    , sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))  as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=1) and ( RF.kind_consult=1 or   RF.kind_consult=2))";



$insertRow2="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  2,
                          min(jnl_status) as status,  sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))  as Col1
                    , sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))    as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=2) and ( RF.kind_consult=1 or   RF.kind_consult=2))";



$insertRow3="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  3 , min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))   as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))    as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=3) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;



$insertRow4="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  4 , min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))   as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))     as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=4) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;




$insertRow5="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  5, min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))   as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))      as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=5) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;




$insertRow6="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  6, min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))     as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))       as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=6) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;




$insertRow7="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  7, min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))    as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))        as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=7) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;





$insertRow8="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  8 ,min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))    as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))         as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=8) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;




$insertRow9="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  9 , min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))    as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))           as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=9) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;

$insertRow10="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  10, min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))  as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))    as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=10) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;



$insertRow11="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  11, min(jnl_status) as status
                          , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0)) as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=11) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;



$insertRow12="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  12, min(jnl_status) as status
                           , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))  as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))       as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=12) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;


$insertRow13="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  13, min(jnl_status) as status
                           , sum(COALESCE(account_si,0))+sum(COALESCE(reject_account_si,0))+ sum(COALESCE(account_ni,0))+sum(COALESCE(reject_account_ni,0))   as Col1
                    ,sum(COALESCE(point_si,0))+sum(COALESCE(reject_point_si,0))+ sum(COALESCE(point_ni,0))+sum(COALESCE(reject_point_ni,0))  as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_factor RF

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RF.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RF.kind_factor=13) and ( RF.kind_consult=1 or   RF.kind_consult=2))" ;


$insertRow14="insert into jnl_Screen7_temp2
(
rowID,
status,
col1 ,
col3
)
(
SELECT  14, min(status) as status
                           , sum(Col1)  as Col1
                    ,sum(Col3)  as Col3


                        from jnl_Screen7_temp2" ;

    $resultInsertRow1 = $this->insertTableData($insertRow1);
    $resultInsertRow2= $this->insertTableData($insertRow2);
    $resultInsertRow3 = $this->insertTableData($insertRow3);
    $resultInsertRow4 = $this->insertTableData($insertRow4);
    $resultInsertRow5 = $this->insertTableData($insertRow5);
    $resultInsertRow6 = $this->insertTableData($insertRow6);
    $resultInsertRow7 = $this->insertTableData($insertRow7);
    $resultInsertRow8 = $this->insertTableData($insertRow8);
    $resultInsertRow9 = $this->insertTableData($insertRow9);
    $resultInsertRow10 = $this->insertTableData($insertRow10);
    $resultInsertRow11 = $this->insertTableData($insertRow11);
    $resultInsertRow12 = $this->insertTableData($insertRow12);
    $resultInsertRow13 = $this->insertTableData($insertRow13);
    $resultInsertRow14 = $this->insertTableData($insertRow14);



$sql="select  COALESCE(Col1, 0) as Col1, COALESCE(Col3, 0) as Col3 from jnl_Screen7_temp2";





       $tmpData = $this->getDateList($sql);

       return $tmpData;



}

function getjnlIndicatorReturnAnalysisTable3($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
    $deleteTabledatasql ="delete from  jnl_Screen7_temp3";
    $deleteStatusData = $this->deleteTableData($deleteTabledatasql);
    // InsertRow1
          $insertRow1="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)
(
SELECT  1
                          , sum(COALESCE(account_si,0) ) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0) ) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si ,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=1) and ( RA.kind_consult=1 or   RA.kind_consult=2))";


$insertRow2="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)
(
SELECT  2
                          , sum(COALESCE(account_si ,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0) ) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0) ) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0) ) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=2) and ( RA.kind_consult=1 or   RA.kind_consult=2))";



$insertRow3="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)
(
SELECT 3
                          , sum(COALESCE(account_si ,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0) ) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si ,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni ,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=3) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;



$insertRow4="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 4
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=4) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;




$insertRow5="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 5
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=5) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;




$insertRow6="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 6
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=6) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;




$insertRow7="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 7
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=7) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;





$insertRow8="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 8
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=8) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;




$insertRow9="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 9
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=9) and ( RA.kind_consult=1 or   RA.kind_consult=2)) " ;

$insertRow10="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 10
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=10) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;



$insertRow11="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 11
                          , sum(COALESCE(account_si,0)) +sum(COALESCE(reject_account_si,0)) +sum(COALESCE(account_ni,0)) +sum(COALESCE(reject_account_ni,0))   as Col1
                    , sum(COALESCE(point_si,0)) +sum(COALESCE(reject_point_si,0)) + sum(COALESCE(point_ni,0)) +sum(COALESCE(reject_point_ni,0))   as Col3


                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_assessment  RA

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RA.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RA.kind_factor=11) and ( RA.kind_consult=1 or   RA.kind_consult=2))" ;









$insertRow12="insert into jnl_Screen7_temp3
(
rowID,
col1 ,
col3
)(
SELECT 12
                          , sum(Col1)   as Col1
                    , sum(Col3)   as Col3


                        from
            jnl_Screen7_temp3" ;

    $resultInsertRow1 = $this->insertTableData($insertRow1);
    $resultInsertRow2= $this->insertTableData($insertRow2);
    $resultInsertRow3 = $this->insertTableData($insertRow3);
    $resultInsertRow4 = $this->insertTableData($insertRow4);
    $resultInsertRow5 = $this->insertTableData($insertRow5);
    $resultInsertRow6 = $this->insertTableData($insertRow6);
    $resultInsertRow7 = $this->insertTableData($insertRow7);
    $resultInsertRow8 = $this->insertTableData($insertRow8);
    $resultInsertRow9 = $this->insertTableData($insertRow9);
    $resultInsertRow10 = $this->insertTableData($insertRow10);
    $resultInsertRow11 = $this->insertTableData($insertRow11);
    $resultInsertRow12 = $this->insertTableData($insertRow12);




$sql="select  COALESCE(Col1, 0) as Col1, COALESCE(Col3, 0) as Col3 from jnl_Screen7_temp3";





       $tmpData = $this->getDateList($sql);
//var_dump($tmpData);
       return $tmpData;



}


function getjnlIndicatorReturnAnalysisTable4($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
    $deleteTabledatasql ="delete from  jnl_Screen7_temp4";
    $deleteStatusData = $this->deleteTableData($deleteTabledatasql);

/*
    // InsertRow1
          $insertRow1="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT  1
                , sum(COALESCE(account_si,0))   as Col1
                    , sum(COALESCE(day_si,0))   as Col2
                , sum(COALESCE(point_si,0))   as Col3
                , sum(COALESCE(money_si,0))   as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1 or  RR.kind_factor=2) and (RR.kind_consult=1) )";


$insertRow2="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 2
                , sum(COALESCE(account_si,0))   as Col1
                    , sum(COALESCE(day_si,0))   as Col2
                , sum(COALESCE(point_si,0))   as Col3
                , sum(COALESCE(money_si,0))   as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1 or  RR.kind_factor=2) and (RR.kind_consult=2) )";



$insertRow3="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 3
                , sum(COALESCE(account_si,0))   as Col1
                    , sum(COALESCE(day_si,0))   as Col2
                , sum(COALESCE(point_si,0))   as Col3
                , sum(COALESCE(money_si,0))   as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1 or  RR.kind_factor=2) and (RR.kind_consult=1 or RR.kind_consult=2) )" ;



$insertRow4="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 4
                , sum(COALESCE(account_si,0))   as Col1
                    , 0  as Col2
                , sum(COALESCE(point_si,0))   as Col3
                , 0  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1 ) and (RR.kind_consult=1 or RR.kind_consult=2) )" ;




$insertRow5="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 5
                , sum(COALESCE(account_si,0))   as Col1
                    , 0  as Col2
                , sum(COALESCE(point_si,0))   as Col3
                , 0  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=2 ) and (RR.kind_consult=1 or RR.kind_consult=2) )" ;




$insertRow6="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 6
                , sum(COALESCE(account_ni,0))   as Col1
                    , sum(COALESCE(day_ni,0))   as Col2
                , sum(COALESCE(point_ni,0))   as Col3
                , sum(COALESCE(money_ni,0))  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1 or RR.kind_factor=2 ) and (RR.kind_consult=1))" ;




$insertRow7="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 7
                , sum(COALESCE(account_ni,0))   as Col1
                    , sum(COALESCE(day_ni,0))   as Col2
                , sum(COALESCE(point_ni,0))   as Col3
                , sum(COALESCE(money_ni,0))  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1 or RR.kind_factor=2 ) and (RR.kind_consult=2))" ;





$insertRow8="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 8
                , sum(COALESCE(account_ni,0))   as Col1
                    , sum(COALESCE(day_ni,0))   as Col2
                , sum(COALESCE(point_ni,0))   as Col3
                , sum(COALESCE(money_ni,0))  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1 or RR.kind_factor=2 ) and ( RR.kind_consult=1 or RR.kind_consult=2))" ;




$insertRow9="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 9
                , sum(COALESCE(account_ni,0))   as Col1
                    , 0   as Col2
                , sum(COALESCE(point_ni,0))   as Col3
                , 0  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=1  ) and ( RR.kind_consult=1 or RR.kind_consult=2))" ;


$insertRow10="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 10
                , sum(COALESCE(account_ni,0))   as Col1
                    , 0   as Col2
                , sum(COALESCE(point_ni,0))   as Col3
                , 0  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((RR.kind_factor=2  ) and ( RR.kind_consult=1 or RR.kind_consult=2))" ;



$insertRow11="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 11
                , sum(COALESCE(account_di,0))   as Col1
                    , sum(COALESCE(day_di,0))   as Col2
                , sum(COALESCE(point_di,0))   as Col3
                , sum(COALESCE(money_di,0))  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((  RR.kind_factor=1  or RR.kind_factor=2  ) and ( RR.kind_consult=1 ))" ;


$insertRow12="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 12
                , sum(COALESCE(account_di,0))   as Col1
                    , sum(COALESCE(day_di,0))   as Col2
                , sum(COALESCE(point_di,0))   as Col3
                , sum(COALESCE(money_di,0))  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((  RR.kind_factor=1  or RR.kind_factor=2  ) and ( RR.kind_consult=2))" ;


$insertRow13="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 13
                , sum(COALESCE(account_di,0))   as Col1
                    , sum(COALESCE(day_di,0))   as Col2
                , sum(COALESCE(point_di,0))   as Col3
                , sum(COALESCE(money_di,0))  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((  RR.kind_factor=1  or RR.kind_factor=2  ) and ( RR.kind_consult=1 or RR.kind_consult=2))" ;

$insertRow14="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 14
                , sum(COALESCE(account_di,0))   as Col1
                    , 0   as Col2
                , sum(COALESCE(point_di,0))   as Col3
                , 0  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((  RR.kind_factor=1 ) and ( RR.kind_consult=1 or RR.kind_consult=2))" ;

$insertRow15="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 15
                , sum(COALESCE(account_di,0))   as Col1
                    , 0   as Col2
                , sum(COALESCE(point_di,0))   as Col3
                , 0  as Col4

                        from
            jnl_facility_master FM,
 jnl_return_main RM , jnl_return_re_charge  RR

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RR.newest_apply_id and  FM.jnl_facility_id=".$facilityId."
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2
and ((  RR.kind_factor=2) and ( RR.kind_consult=1 or RR.kind_consult=2))" ;

$insertRow16="insert into jnl_Screen7_temp4
(
rowID,
col1 ,
col2 ,
col3 ,
col4
)
(
SELECT 16
                , sum(Col1)   as Col1
                    , sum(Col2)  as Col2
                , sum(col3)   as Col3
                , sum(Col4)  as Col4

                        from jnl_Screen7_temp4
where
rowID in  (3,8,13)" ;


    $resultInsertRow1 = $this->insertTableData($insertRow1);
    $resultInsertRow2= $this->insertTableData($insertRow2);
    $resultInsertRow3 = $this->insertTableData($insertRow3);
    $resultInsertRow4 = $this->insertTableData($insertRow4);
    $resultInsertRow5 = $this->insertTableData($insertRow5);
    $resultInsertRow6 = $this->insertTableData($insertRow6);
    $resultInsertRow7 = $this->insertTableData($insertRow7);
    $resultInsertRow8 = $this->insertTableData($insertRow8);
    $resultInsertRow9 = $this->insertTableData($insertRow9);
    $resultInsertRow10 = $this->insertTableData($insertRow10);
    $resultInsertRow11 = $this->insertTableData($insertRow11);
    $resultInsertRow12 = $this->insertTableData($insertRow12);
    $resultInsertRow13 = $this->insertTableData($insertRow13);
    $resultInsertRow14 = $this->insertTableData($insertRow14);
    $resultInsertRow15 = $this->insertTableData($insertRow15);
    $resultInsertRow16 = $this->insertTableData($insertRow16);

*/


$sql="select  COALESCE(Col1, 0) as Col1, COALESCE(Col2, 0) as Col2, COALESCE(Col3, 0) as Col3, COALESCE(Col4, 0) as Col4 from jnl_Screen7_temp4";

		$sql1="select COALESCE(JR.account_si, 0) as account_si,
				COALESCE(JR.day_si, 0) as day_si,
				COALESCE(JR.point_si, 0) as point_si,
				COALESCE(JR.money_si, 0) as money_si,
				COALESCE(JR.account_ni, 0) as account_ni,
				COALESCE(JR.day_ni, 0) as day_ni,
				COALESCE(JR.point_ni, 0) as point_ni,
				COALESCE(JR.money_ni, 0) as money_ni,
				COALESCE(JR.account_di, 0) as account_di,
				COALESCE(JR.day_di, 0) as day_di,
				COALESCE(JR.point_di, 0) as point_di,
				COALESCE(JR.money_di, 0) as money_di
				from jnl_return_re_charge JR, jnl_return_main JM where JM.newest_apply_id = JR.newest_apply_id AND  JM.jnl_facility_id=".$facilityId." AND SUBSTR(JM.regist_date, 0, 7) = '".$yearMonth."' AND JM.del_flg = 'f' AND JM.jnl_status < 2 order by JR.kind_factor , JR.kind_consult";
		$getAllData = $this->getDateList($sql1);
		
			
			
		$tmpData[0]["col1"] = $getAllData[0]["account_si"] + $getAllData[1]["account_si"];
		$tmpData[0]["col2"] = $getAllData[0]["day_si"];
		$tmpData[0]["col3"] = $getAllData[0]["point_si"] + $getAllData[1]["point_si"];
		$tmpData[0]["col4"] = $getAllData[0]["money_si"];
		
		$tmpData[1]["col1"] = $getAllData[2]["account_si"] + $getAllData[3]["account_si"];
		$tmpData[1]["col2"] = $getAllData[2]["day_si"];
		$tmpData[1]["col3"] = $getAllData[2]["point_si"] + $getAllData[3]["point_si"];
		$tmpData[1]["col4"] = $getAllData[2]["money_si"];

		$tmpData[2]["col1"] = $getAllData[0]["account_si"] + $getAllData[1]["account_si"] + $getAllData[2]["account_si"] + $getAllData[3]["account_si"];
		$tmpData[2]["col2"] = $getAllData[0]["day_si"] + $getAllData[2]["day_si"];
		$tmpData[2]["col3"] = $getAllData[0]["point_si"] + $getAllData[1]["point_si"] + $getAllData[2]["point_si"] + $getAllData[3]["point_si"];
		$tmpData[2]["col4"] = $getAllData[0]["money_si"] + $getAllData[2]["money_si"];
		
		$tmpData[3]["col1"] = $getAllData[0]["account_si"] + $getAllData[2]["account_si"];
		$tmpData[3]["col2"] = null;
		$tmpData[3]["col3"] = $getAllData[0]["point_si"] + $getAllData[2]["point_si"];
		$tmpData[3]["col4"] = ($getAllData[0]["point_si"] + $getAllData[2]["point_si"]) / ($getAllData[0]["point_si"] + $getAllData[1]["point_si"] + $getAllData[2]["point_si"] + $getAllData[3]["point_si"]) * 100;
		
		$tmpData[4]["col1"] = $getAllData[1]["account_si"] + $getAllData[3]["account_si"];
		$tmpData[4]["col2"] = null;
		$tmpData[4]["col3"] = $getAllData[1]["point_si"] + $getAllData[3]["point_si"];
		$tmpData[4]["col4"] = ($getAllData[1]["point_si"] + $getAllData[3]["point_si"]) / ($getAllData[0]["point_si"] + $getAllData[1]["point_si"] + $getAllData[2]["point_si"] + $getAllData[3]["point_si"]) * 100;
		


		$tmpData[5]["col1"] = $getAllData[0]["account_ni"] + $getAllData[1]["account_ni"];
		$tmpData[5]["col2"] = $getAllData[0]["day_ni"];
		$tmpData[5]["col3"] = $getAllData[0]["point_ni"] + $getAllData[1]["point_ni"];
		$tmpData[5]["col4"] = $getAllData[0]["money_ni"];
		
		$tmpData[6]["col1"] = $getAllData[2]["account_ni"] + $getAllData[3]["account_ni"];
		$tmpData[6]["col2"] = $getAllData[2]["day_ni"];
		$tmpData[6]["col3"] = $getAllData[2]["point_ni"] + $getAllData[3]["point_ni"];
		$tmpData[6]["col4"] = $getAllData[2]["money_ni"];
		
		$tmpData[7]["col1"] = $getAllData[0]["account_ni"] + $getAllData[1]["account_ni"] + $getAllData[2]["account_ni"] + $getAllData[3]["account_ni"];
		$tmpData[7]["col2"] = $getAllData[0]["day_ni"] + $getAllData[2]["day_ni"];
		$tmpData[7]["col3"] = $getAllData[0]["point_ni"] + $getAllData[1]["point_ni"] + $getAllData[2]["point_ni"] + $getAllData[3]["point_ni"];
		$tmpData[7]["col4"] = $getAllData[0]["money_ni"] + $getAllData[2]["money_ni"];
		
		$tmpData[8]["col1"] = $getAllData[0]["account_ni"] + $getAllData[2]["account_ni"];
		$tmpData[8]["col2"] = null;
		$tmpData[8]["col3"] = $getAllData[0]["point_ni"] + $getAllData[2]["point_ni"];
		$tmpData[8]["col4"] = ($getAllData[0]["point_ni"] + $getAllData[2]["point_ni"]) / ($getAllData[0]["point_ni"] + $getAllData[1]["point_ni"] + $getAllData[2]["point_ni"] + $getAllData[3]["point_ni"]) * 100;
		
		$tmpData[9]["col1"] = $getAllData[1]["account_ni"] + $getAllData[3]["account_ni"];
		$tmpData[9]["col2"] = null;
		$tmpData[9]["col3"] = $getAllData[1]["point_ni"] + $getAllData[3]["point_ni"];
		$tmpData[9]["col4"] = ($getAllData[1]["point_ni"] + $getAllData[3]["point_ni"]) / ($getAllData[0]["point_ni"] + $getAllData[1]["point_ni"] + $getAllData[2]["point_ni"] + $getAllData[3]["point_ni"]) * 100;
		

		$tmpData[10]["col1"] = $getAllData[0]["account_di"] + $getAllData[1]["account_di"];
		$tmpData[10]["col2"] = $getAllData[0]["day_di"];
		$tmpData[10]["col3"] = $getAllData[0]["point_di"] + $getAllData[1]["point_di"];
		$tmpData[10]["col4"] = $getAllData[0]["money_di"];
		
		$tmpData[11]["col1"] = $getAllData[2]["account_di"] + $getAllData[3]["account_di"];
		$tmpData[11]["col2"] = $getAllData[2]["day_di"];
		$tmpData[11]["col3"] = $getAllData[2]["point_di"] + $getAllData[3]["point_di"];
		$tmpData[11]["col4"] = $getAllData[2]["money_di"];
		
		$tmpData[12]["col1"] = $getAllData[0]["account_di"] + $getAllData[1]["account_di"] + $getAllData[2]["account_di"] + $getAllData[3]["account_di"];
		$tmpData[12]["col2"] = $getAllData[0]["day_di"] + $getAllData[2]["day_di"];
		$tmpData[12]["col3"] = $getAllData[0]["point_di"] + $getAllData[1]["point_di"] + $getAllData[2]["point_di"] + $getAllData[3]["point_di"];
		$tmpData[12]["col4"] = $getAllData[0]["money_di"] + $getAllData[2]["money_di"];
		
		$tmpData[13]["col1"] = $getAllData[0]["account_di"] + $getAllData[2]["account_di"];
		$tmpData[13]["col2"] = null;
		$tmpData[13]["col3"] = $getAllData[0]["point_di"] + $getAllData[2]["point_di"];
		$tmpData[13]["col4"] = ($getAllData[0]["point_di"] + $getAllData[2]["point_di"]) / ($getAllData[0]["point_di"] + $getAllData[1]["point_di"] + $getAllData[2]["point_di"] + $getAllData[3]["point_di"]) * 100;
		
		$tmpData[14]["col1"] = $getAllData[1]["account_di"] + $getAllData[3]["account_di"];
		$tmpData[14]["col2"] = null;
		$tmpData[14]["col3"] = $getAllData[1]["point_di"] + $getAllData[3]["point_di"];
		$tmpData[14]["col4"] = ($getAllData[1]["point_di"] + $getAllData[3]["point_di"]) / ($getAllData[0]["point_di"] + $getAllData[1]["point_di"] + $getAllData[2]["point_di"] + $getAllData[3]["point_di"]) * 100;
		
		$tmpData[15]["col1"] = $getAllData[0]["account_si"] + $getAllData[1]["account_si"] + $getAllData[2]["account_si"] + $getAllData[3]["account_si"] +
		                         $getAllData[0]["account_ni"] + $getAllData[1]["account_ni"] + $getAllData[2]["account_ni"] + $getAllData[3]["account_ni"] +
								 $getAllData[0]["account_di"] + $getAllData[1]["account_di"] + $getAllData[2]["account_di"] + $getAllData[3]["account_di"];
		
		$tmpData[15]["col2"] = $getAllData[0]["day_si"] + $getAllData[2]["day_si"] + 
								$getAllData[0]["day_ni"] + $getAllData[2]["day_ni"] + 
								$getAllData[0]["day_di"] + $getAllData[2]["day_di"]; 
			
		$tmpData[15]["col3"] = $getAllData[0]["point_si"] + $getAllData[1]["point_si"] + $getAllData[2]["point_si"] + $getAllData[3]["point_si"] +
								$getAllData[0]["point_ni"] + $getAllData[1]["point_ni"] + $getAllData[2]["point_ni"] + $getAllData[3]["point_ni"] +
								$getAllData[0]["point_di"] + $getAllData[1]["point_di"] + $getAllData[2]["point_di"] + $getAllData[3]["point_di"];
		
		$tmpData[15]["col4"] = $getAllData[0]["money_si"] + $getAllData[2]["money_si"] + 
								$getAllData[0]["money_ni"] + $getAllData[2]["money_ni"] + 
								$getAllData[0]["money_di"] + $getAllData[2]["money_di"]; 
		
		



		

       //$tmpData = $this->getDateList($sql);
    //var_dump($tmpData);
       return $tmpData;



}

function getjnlIndicatorReturnAnalysisTable5A($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
    $sqlA="SELECT    comment_return_si as txtcommentSi, comment_return_ni  as txtcommentNi
          from   jnl_facility_master FM, jnl_return_main RM , jnl_return_comment  RC

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RC.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2";
    $tmpData = $this->getDateList($sqlA);
//    var_dump($tmpData);
    return $tmpData;
}
function getjnlIndicatorReturnAnalysisTable5B($dateArr,$facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;

    $sqlB="SELECT    comment_judge_si as txtcommentSi, comment_judge_ni  as txtcommentNi
          from   jnl_facility_master FM, jnl_return_main RM , jnl_return_comment  RC

where RM.jnl_facility_id = FM.jnl_facility_id
and  RM.newest_apply_id=RC.newest_apply_id and  FM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."' AND RM.del_flg = 'f' AND jnl_status < 2";
    $tmpData = $this->getDateList($sqlB);
    return $tmpData;
}


function getjnlIndicatorAnalysisPointAccount($dateArr, $facilityId)
{
    $year  = $this->getArrValueByKey('year', $dateArr);
    $month = $this->getArrValueByKey('month', $dateArr);
    $yearMonth=$year.$month;
    $sql = "SELECT COALESCE(total_point,0) as total_point,COALESCE(total_account,0) as total_account FROM
          jnl_return_main RM, jnl_return_total RT
where  RM.newest_apply_id= RT.newest_apply_id
and RM.jnl_facility_id='".$facilityId."'
and SUBSTR(RM.regist_date, 0, 7) = '".$yearMonth."'

AND RM.del_flg = 'f' AND jnl_status < 2";
     $tmpData  = $this->getDateList($sql);
 //var_dump($tmpData);
 return $tmpData;
    }

















}
