<?
///*****************************************************************************
// 勤務シフト作成 | シフト作成 PHP5.1.6 ネイティブExcel対応
///*****************************************************************************
ini_set("max_execution_time", 0);
require_once("about_comedix.php");
ob_start();
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");
require_once("duty_shift_menu_excel_5_workshop.php");
require_once("show_class_name.ini");	// ユニット（３階層名）表示のため追加
//繰越公休残日数 start
require_once("duty_shift_hol_remain_common_class.php");
//繰越公休残日数 end
require_once("date_utils.php");

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///---------------------------------------------------- -------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_duty = new duty_shift_duty_common_class($con, $fname);
///-----------------------------------------------------------------------------
// Excelオブジェクト生成
///-----------------------------------------------------------------------------
$excelObj = new ExcelWorkShop();

///-----------------------------------------------------------------------------
// Excel列名
///-----------------------------------------------------------------------------
$excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN');
///-----------------------------------------------------------------------------
//処理
///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
$excel_flg = "1";			//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
$download_data = "";		//ＥＸＣＥＬデータ
///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];

///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
if ($duty_mm == "") { $duty_mm = $now_mm; }
///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
if ($excel_yearmonth != ""){						//当月末まで出力する 20130219
	$year_month = explode("-",$excel_yearmonth);
	$duty_yyyy = $year_month[0];
	$duty_mm = ltrim($year_month[1],"0");
	$data_cnt = "";
}
if ($extra_day == "") { $extra_day = 0; }
//4週対応 20140507
if ($excel_week4start != "") {
    $duty_yyyy = substr($excel_week4start, 0, 4);
    $duty_mm = substr($excel_week4start, 4, 2);
}
//filename,sheetname用
//$org_duty_yyyy = $duty_yyyy;
//$org_duty_mm = $duty_mm;
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
$start_day = 1;
$end_day = $day_cnt;

///-----------------------------------------------------------------------------
// system config
///-----------------------------------------------------------------------------
$conf = new Cmx_SystemConfig();

///-----------------------------------------------------------------------------
//各変数値の初期値設定
///-----------------------------------------------------------------------------

$individual_flg = "";
$hope_get_flg = "";

if ($data_cnt == "") { $data_cnt = 0; }
//入力形式で表示する日（開始／終了）
$edit_start_day = 0;
$edit_end_day = 0;

$group_id = $cause_group_id;
///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
///-----------------------------------------------------------------------------
//ＤＢ(stmst)より役職情報取得
//ＤＢ(jobmst)より職種情報取得
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------
$data_st = $obj->get_stmst_array();
$data_job = $obj->get_jobmst_array();
// 有効なグループの職員を取得
//$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm); //20140210
$data_wktmgrp = $obj->get_wktmgrp_array();


///-----------------------------------------------------------------------------
// 指定グループの病棟名を取得
///-----------------------------------------------------------------------------
$wk_group = $obj->get_duty_shift_group_array($group_id, "", $data_wktmgrp);
if (count($wk_group) > 0) {
    $group_name = $wk_group[0]["group_name"];
    $pattern_id = $wk_group[0]["pattern_id"];
    $start_month_flg1 = $wk_group[0]["start_month_flg1"];
    $start_day1 = $wk_group[0]["start_day1"];
    $month_flg1 = $wk_group[0]["month_flg1"];
    $end_day1 = $wk_group[0]["end_day1"];
    $start_day2 = $wk_group[0]["start_day2"];
    $month_flg2 = $wk_group[0]["month_flg2"];
    $end_day2 = $wk_group[0]["end_day2"];
    $print_title = $wk_group[0]["print_title"];
    $reason_setting_flg = $wk_group[0]["reason_setting_flg"];
    $paid_hol_disp_flg = $wk_group[0]["paid_hol_disp_flg"];    //有給休暇残日数表示フラグ
}
if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}
if ($start_day2 == "" || $start_day2 == "0") {	$start_day2 = "0";}
if ($month_flg2 == "" || $end_day2 == "") {	$month_flg2 = "1";}
if ($end_day2 == "" || $end_day2 == "0") {	$end_day2 = "99";}

$month_flg = "1"; //月またがりなし
if ($excel_yearmonth != "") {
    if ($start_month_flg1 == "0") {
        $month_flg = "0";//前月から当月
    }
    else if ($month_flg1 == "1") {
        $month_flg = "2"; //当月から翌月
    }
}
//4週対応 20140507
if ($excel_week4start != "") {
    $start_date = $excel_week4start;
    $end_date = substr(date_utils::add_day_ymdhi($start_date."0000", 27), 0, 8);
    //月またがりか確認
    $arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    //開始日が範囲より前の場合は、前月とする
    if ($start_date < $arr_date[0]) {
        $duty_mm--;
        if ($duty_mm < 1) {
            $duty_yyyy--;
            $duty_mm = 12;
        }
        $arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    } else if ($start_date > $arr_date[1]) {
        //当日が範囲より後の場合は、翌月とする
        $duty_mm++;
        if ($duty_mm > 12) {
            $duty_yyyy++;
            $duty_mm = 1;
        }
        $arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    }
//    $end_mm = substr($end_date, 4, 2);
//    if ($end_mm != $duty_mm) {
    if ($end_date > $arr_date[1]) {
        $month_flg = "0";
    }
    //$duty_mm = $end_mm; //debug 20140509
    //debug 20140509
    //$month_flg = "0";
}
// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm, $month_flg);
// 追加がある場合、取得
$add_staff_id_all_array = explode(",", $add_staff_id_all);
$add_staff_id_all_cnt = count($add_staff_id_all_array);
if ($add_staff_cnt > 0 || $add_staff_id_all_cnt > 0) {
    $wk_emp_id_array = explode(",", $add_staff_id);
    $wk_array = array_merge($add_staff_id_all_array, $wk_emp_id_array);
    $wk_cnt = count($data_emp);
    $add_staff_id_all = "";
    foreach ($wk_array as $wk_add_staff_id) {
        if ($wk_add_staff_id == "") {
            continue;
        }
        $wk_data_emp = $obj->get_empmst_array($wk_add_staff_id);
        $data_emp[$wk_cnt]["id"] 	= $wk_data_emp[0]["id"];
        $data_emp[$wk_cnt]["name"] 	= $wk_data_emp[0]["name"];
        $data_emp[$wk_cnt]["job_id"] 	= $wk_data_emp[0]["job_id"];
        $data_emp[$wk_cnt]["st_id"] 	= $wk_data_emp[0]["st_id"];
        $data_emp[$wk_cnt]["join"] 	= $wk_data_emp[0]["join"];
        $wk_cnt++;
        
        if ($add_staff_id_all != "") {
            $add_staff_id_all .= ",";
        }
        $add_staff_id_all .= $wk_add_staff_id;
    }
}

///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
if ($excel_yearmonth != "")
{
    $start_day1 = $start_day;	//当月のみ 20130219
    $end_day1 = $end_day;
    $start_month_flg1 = "1";
    $month_flg1 = $start_month_flg1;
    $tmp_group = $obj->get_group_info_for_report("",$group_id);	//月を跨ぐ対応 20130529
    $group_array = $tmp_group[0];
}
//4週対応 20140507
if ($excel_week4start == "") {
    $arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
    $start_date = $arr_date[0];
    $end_date = $arr_date[1];
}
else {
    //終了日が範囲外の場合、月またがり設定 debug 20140509
    if ($end_date > $arr_date[1]) {
        $group_array["start_month_flg1"] = "0";
        $group_array["month_flg1"] = "1";
    }
}
$calendar_array = $obj->get_calendar_array($start_date, $end_date);
$day_cnt=count($calendar_array);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
$tmp_date = $start_date;
for ($k=1; $k<=$day_cnt; $k++) {
    $tmp_date = strtotime($tmp_date);
    $week_array[$k]["name"] = $obj->get_weekday($tmp_date);
    $tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
}
///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
//20140220 start
$data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
//応援追加されている出勤パターングループ確認 
$arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, $wk_array, $pattern_id);
$pattern_id_cnt = count($arr_pattern_id);
if ($pattern_id_cnt > 0) {
    $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
    $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
    $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
    $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
}
//応援なしの場合、同じパターングループのデータを設定
else {
    $data_atdptn_all = $data_atdptn;
    $data_pattern_all = $data_pattern;
}
//20140220 end
///-----------------------------------------------------------------------------
//遷移元のデータに変更
///-----------------------------------------------------------------------------
if ( $excel_row_count == "2" ){
    switch ( $excel_row_type ){
        case "0": $show_data_flg = "0"; break;	// 空行
        case "1": $show_data_flg = "1"; break;	// 勤務実績
        case "2": $show_data_flg = "2"; break;	// 勤務希望
        case "3": $show_data_flg = "3"; break;	// 当直
        case "4": $show_data_flg = "4"; break;	// コメント
        default : $show_data_flg = "";
    }
}else{
    $show_data_flg = "";
}
$set_data = array();
$plan_array = array();
if (/*$excel_yearmonth == "" && */ $excel_week4start == "") { //4週対応 20140507
    for($i=0;$i<$data_cnt;$i++) {
        //応援追加情報
        $assist_str = "assist_group_$i";
        $arr_assist_group = explode(",", $$assist_str);
        //希望分
        if ($show_data_flg == "2") {
            $rslt_assist_str = "rslt_assist_group_$i";
            $arr_rslt_assist_group = explode(",", $$rslt_assist_str);
        }
        ///-----------------------------------------------------------------------------
        //遷移元のデータに変更
        ///-----------------------------------------------------------------------------
        for($k=1;$k<=$day_cnt;$k++) {
            $src_k = $k + $extra_day;
            
            $set_data[$i]["plan_rslt_flg"] = "1";			//１：実績は再取得
            $wk2 = "pattern_id_$i" . "_" . $src_k;
            $wk3 = "atdptn_ptn_id_$i" . "_" . $src_k;
            $wk5 = "comment_$i" . "_" . $src_k;
            $wk6 = "reason_2_$i" . "_" . $src_k;
            
            $set_data[$i]["staff_id"] = $staff_id[$i];					//職員ＩＤ
            
            $set_data[$i]["pattern_id_$k"] = $$wk2;						//出勤グループＩＤ
            
            if ((int)substr($$wk3,0,2) > 0) {
                $set_data[$i]["atdptn_ptn_id_$k"] = (int)substr($$wk3,0,2);	//出勤パターンＩＤ
            }
            if ((int)substr($$wk3,2,2) > 0) {
                $set_data[$i]["reason_$k"] = (int)substr($$wk3,2,2);	//事由
            }
            
            $set_data[$i]["reason_2_$k"] = $$wk6;						//事由（午前休暇／午後休暇）
            $set_data[$i]["comment_$k"] = $$wk5;							//コメント
            //応援情報
            $set_data[$i]["assist_group_$k"] = $arr_assist_group[$src_k - 1];
            if ($show_data_flg == "2") {
                $set_data[$i]["rslt_assist_group_$k"] = $arr_rslt_assist_group[$src_k - 1];
            }
            // 勤務シフト希望のデータ
            $wk7 = "rslt_pattern_id_$i" . "_" . $src_k;
            $wk8 = "rslt_id_$i" . "_" . $src_k;
            $wk9 = "rslt_reason_2_$i" . "_" . $src_k;
            if ($show_data_flg == "2" && $plan_results_flg != "3") { //実績入力画面を除く 20160223
                // 勤務シフト希望のデータ
                $set_data[$i]["rslt_pattern_id_$k"] = $$wk7;						//出勤グループＩＤ
                
                if ((int)substr($$wk8,0,2) > 0) {
                    $set_data[$i]["rslt_id_$k"] = (int)substr($$wk8,0,2);	//出勤パターンＩＤ
                }
                if ((int)substr($$wk8,2,2) > 0) {
                    $set_data[$i]["rslt_reason_$k"] = (int)substr($$wk8,2,2);		//事由
                }
                
                $set_data[$i]["rslt_reason_2_$k"] = $$wk9;						//事由（午前有給／午後有給）
            }
        }
    }
}

//----------------------------------------------
// 当初予定か勤務予定か
//----------------------------------------------
$disp_tosYotei_flg = $wk_group[0]["disp_tosYotei_flg"];

// 取得テーブルを変更する
if ($disp_tosYotei_flg == "t") {
	$sw_sche = "_sche";
} else {
	$sw_sche = "";
}
//----------------------------------------------

$plan_array = $obj->get_duty_shift_plan_array(
		$group_id, $pattern_id,
		$duty_yyyy, $duty_mm, $day_cnt,
		$individual_flg,
		$hope_get_flg,
		$show_data_flg,
		$set_data, $week_array, $calendar_array,
		$data_atdptn, $data_pattern_all,
		$data_st, $data_job, $data_emp, "", $group_array, $sw_sche);
$data_cnt = count($plan_array);
///-----------------------------------------------------------------------------
// 行タイトル（行ごとの集計する勤務パターン）を取得
// 列タイトル（列ごとの集計する勤務パターン）を取得
///-----------------------------------------------------------------------------
$title_gyo_array = $obj->get_total_title_array($pattern_id,	"1", $data_pattern);
$title_retu_array = $obj->get_total_title_array($pattern_id, "2", $data_pattern);
if ((count($title_gyo_array) <= 0) || (count($title_retu_array) <= 0)) {
	$err_msg_2 = "勤務シフトグループ情報が未設定です。管理者に連絡してください。";
}
//繰越公休残日数 start
//繰越公休残用クラス
$obj_hol_remain = new duty_shift_hol_remain_common_class($con, $fname, $group_id, $emp_id, $legal_hol_idx);

if ($obj_hol_remain->transfer_holiday_days_flg == "t" && $show_data_flg == "1") {
    //データをまとめて取得しplan_arrayに設定する。公休残hol_remain、前月残prev_remain、公休使用数hol_cnt、所定公休数std_hol_cnt
    $obj_hol_remain->set_hol_remain_data($plan_array, $group_id, $duty_yyyy, $duty_mm, $start_date, $end_date);
}
//繰越公休残日数 end
///-----------------------------------------------------------------------------
//個人日数合計（行）を算出設定
//個人日数合計（列）を算出設定
///-----------------------------------------------------------------------------
//2段目が実績の場合、システム日以降は、行を集計しない。
if ($show_data_flg == "1") {
	$wk_rs_flg = "2";
} else {
	$wk_rs_flg = "";
}
$gyo_array = $obj->get_total_cnt_array($group_id, $pattern_id,
		"1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
//2段表示、実績、勤務希望の場合、それぞれの内容を列集計する 20160222
if ($show_data_flg == "1" || $show_data_flg == "2") {
	$wk_rs_flg = "1";
} else {
	$wk_rs_flg = "";
}
$retu_array = $obj->get_total_cnt_array($group_id, $pattern_id,
		"2", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);

$title_hol_array = $obj->get_total_title_hol_array($pattern_id, $data_pattern, $data_atdptn);
//print_r($title_hol_array);
//休暇事由情報
$hol_cnt_array = $obj->get_hol_cnt_array($group_id, $pattern_id, "1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn);

$paid_hol_all_array = array();
if ($paid_hol_disp_flg === "t") {
	//年休残
	$paid_hol_all_array = $obj->get_paid_hol_rest_array(
			$plan_array,
			$calendar_array,
			$duty_yyyy,
	        $duty_mm,
			"create");
}
///-----------------------------------------------------------------------------
//最大文字数算出
///-----------------------------------------------------------------------------
$max_font_cnt = 1;
for ($i=0; $i<$data_cnt; $i++) {
	for ($k=1; $k<=$day_cnt; $k++) {
		//予定
		if ($plan_array[$i]["font_name_$k"] != "") {
			if ($max_font_cnt < strlen($plan_array[$i]["font_name_$k"])) {
				if (strlen($plan_array[$i]["font_name_$k"]) <= 2) {
					$max_font_cnt = 1;
				} else 	if (strlen($plan_array[$i]["font_name_$k"]) <= 4) {
					$max_font_cnt = 2;
				}
			}
		}
		//実績
		if ($plan_array[$i]["rslt_name_$k"] != "") {
			if ($max_font_cnt < (int)strlen($plan_array[$i]["rslt_name_$k"])) {
				if (strlen($plan_array[$i]["rslt_name_$k"]) <= 2) {
					$max_font_cnt = 1;
				} else 	if (strlen($plan_array[$i]["rslt_name_$k"]) <= 4) {
					$max_font_cnt = 2;
				}
			}
		}
	}
}

//決裁欄役職名
$st_name_array = $obj->get_duty_shift_group_st_name_array($group_id);

// 決裁欄印字有無を取得する
$approval_column_print_flg = $wk_group[0]["approval_column_print_flg"];

///-----------------------------------------------------------------------------
// ユニット表示仕変
$unit_disp_level = $obj->get_emp_unitlevel($group_id);
///-----------------------------------------------------------------------------

// ユニット（３階層名）表示のため追加
$arr_class_name = get_class_name_array($con, $fname);
$unit_name = $arr_class_name[intval($unit_disp_level) - 1];

///-----------------------------------------------------------------------------
//ファイル名
///-----------------------------------------------------------------------------
if ($excel_week4start != "") {
    $sheetname = 'shift_'.$start_date.'_'.$end_date;
}
else {
    $sheetname = 'shift_'.$duty_yyyy.sprintf("%02d",$duty_mm);
}
$filename = $sheetname.'.xls';
if (preg_match(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
} else {
	$encoding = mb_http_output();   // Firefox
}
$filename = mb_convert_encoding(
		$filename,
		$encoding,
		mb_internal_encoding());

///-----------------------------------------------------------------------------
// Excel シート名
///----------------------------------------------------------------------------
$excelObj->SetSheetName($sheetname);

///-----------------------------------------------------------------------------
// 色
///-----------------------------------------------------------------------------
$week_color = $_REQUEST['excel_weekcolor'];
$hope_color = $_REQUEST['excel_hopecolor'];
$match_color = $conf->get('shift.hope.match.color');
if (empty($match_color)) {
	$match_color = '#000000';
}
$match_bgcolor = $conf->get('shift.hope.match.bgcolor');
if (empty($match_bgcolor)) {
	$match_bgcolor = '#00ff00';
}
$unmatch_color = $conf->get('shift.hope.unmatch.color');
if (empty($unmatch_color)) {
	$unmatch_color = '#000000';
}
$unmatch_bgcolor = $conf->get('shift.hope.unmatch.bgcolor');
if (empty($unmatch_bgcolor)) {
	$unmatch_bgcolor = '#ffff00';
}
$plan_individual = $obj->get_plan_individual($group_id, $pattern_id, $start_date, $end_date);

///*****************************************************************************
//ＨＴＭＬ作成
///*****************************************************************************
showData($err_msg_2,
		$group_name,
		$duty_yyyy,
		$duty_mm,
		$print_title,
		$now_yyyy,			//作成年
		$now_mm,			//作成月
		$now_dd,			//作成日
		$day_cnt,
		$st_name_array,
		$excelColumnName,
		$excelObj,
		$show_data_flg,
        $approval_column_print_flg,
        $disp_tosYotei_flg,
        $excel_week4start,//4週単位開始 20140508
        $end_date, //4週単位終了 20140508
        $obj,
        $group_id
        );

// $download_data_0 = $wk_array["data"];


// まずは見出しまで出してみるため、以下をコメント


$total_print_flg = "1"; // 集計は常に出力する PHP5.1.6対応
$hol_dt_flg = '0'; // 休暇詳細は当面は出さない PHP5.1.6対応

//日・曜日
showTitleWeek(
        $obj,
        $duty_yyyy,
        $duty_mm,
        $day_cnt,
        $plan_array,
        $week_array,
        $title_gyo_array,
        $calendar_array,
        $total_print_flg,
        $show_data_flg,
        $hol_dt_flg,
        $title_hol_array,
        $excelColumnName,
        $excelObj,
        $excel_blancrows,
        $paid_hol_disp_flg,
        $week_color,
        //繰越公休残日数 start
        $obj_hol_remain,
        //繰越公休残日数 end
		$wk_group[0],         		// チーム表示フラグ等
		$unit_name          	// ユニット名
		);

//勤務シフトデータ（職員（列）年月日（行）
$excelCellRowPosition = showList(
        $obj,
        $obj_duty,
        $pattern_id,
        $plan_array,
        $title_gyo_array,
        $gyo_array,
        0,
        $data_cnt,
        $day_cnt,
        $max_font_cnt,
        $total_print_flg,
        $show_data_flg,
        $group_id,
        $reason_setting_flg,
        $hol_dt_flg,
        $title_hol_array,
        $hol_cnt_array,
        $paid_hol_all_array,
        $calendar_array,
        $week_array,
        $excelColumnName,
        $excelObj,
        $excel_blancrows,
        $paid_hol_disp_flg,
		$week_color,
		$hope_color,
        //繰越公休残日数 start
        $obj_hol_remain,
        //繰越公休残日数 end
		$wk_group[0]				// チーム表示フラグ等
		);

//集計行列の出力をする場合
if ($total_print_flg == "1") {
    //有給休暇残日数
    if ($paid_hol_disp_flg === "t") {
        $paid_hol_array[] = $paid_hol_all_array["plan"];
        if ($show_data_flg == "1") {    // 勤務実績
            $paid_hol_array[] = $paid_hol_all_array["result"];
        } elseif ($show_data_flg == "2") {    // 勤務希望
            $paid_hol_array[] = $paid_hol_all_array["hope"];
        }
    }

    //2段目の集計有無
	if ($show_data_flg == "1" || $show_data_flg == "2") {
		$wk_total_gyo_flg = "2";
	} else {
		$wk_total_gyo_flg = "1";
	}


	showTitleRetu(
	        $obj,
	        $day_cnt,
	        $plan_array,
	        $title_gyo_array,
	        $gyo_array,
	        $title_retu_array,
	        $retu_array,
	        $wk_total_gyo_flg,
	        $total_print_flg,
	        $hol_dt_flg,
	        $title_hol_array,
	        $hol_cnt_array,
	        $paid_hol_array,
	        $calendar_array,
	        $week_array,
	        $excelColumnName,
	        $excelObj,
	        $excelCellRowPosition,
	        $show_data_flg,
	        $paid_hol_disp_flg,
	        $week_color,
            //繰越公休残日数 start
            $obj_hol_remain,
            //繰越公休残日数 end
			$wk_group[0]				// チーム表示フラグ等
	        );

}


///-----------------------------------------------------------------------------
//データEXCEL出力
///-----------------------------------------------------------------------------

ob_clean();

$excelObj->PageSetUp('Horizon'); // 向き。横固定
$excelObj->PaperSize( $excel_paper_size ); // 用紙サイズ
// 倍率と自動或いは利用者指定
$excelObj->PageRatio( $excel_setfitpage , $excel_ratio );

// 余白、左右上下頭足
$excelObj->SetMargin($excel_mleft,$excel_mright,$excel_mtop,$excel_mbottom,$excel_mhead,$excel_mfoot);

// ダウンロード
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');
$excelObj->OutPut();

//設定保存
$arr_key = $obj->get_x5para_conf_key();
foreach ($arr_key as $key => $reqkey) {
    $val = $_REQUEST["$reqkey"];
    $obj->conf_set($group_id, $key, $val);
}

ob_end_flush();

pg_close($con);

/*************************************************************************/
//
// シート初期値、承認印の欄
//
/*************************************************************************/

function showData($err_msg,
	$group_name,
	$duty_yyyy,
	$duty_mm,
	$print_title,
	$create_yyyy,			//作成年
	$create_mm,				//作成月
	$create_dd,				//作成日
	$day_cnt,
	$st_name_array,$excelColumnName,$excelObj,$show_data_flg,
    $approval_column_print_flg, $disp_tosYotei_flg,
    $excel_week4start, $end_date, //4週単位対応 20140508
    $obj,
    $group_id
	){

//	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
//	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
//	$data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";
//	$data .= "<tr height=\"30\">\n";
	$wk_colspan = $day_cnt - 3; //決裁欄位置を当初より8列左とする 20140512
//	$data .= "<td colspan=\"$wk_colspan\">";
//	$data .= "</td>";

//	$data .= "<td rowspan=\"2\" align=\"center\" style=\"border:solid 1px\">";
//	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">決<br>裁</font>";
//	$data .= "</td>";

	// 行の高さ
	$excelObj->SetRowDim('1' , 22.5);
	$excelObj->SetRowDim('2' , 45);
	$excelObj->SetRowDim('3' , 13.5);
	$excelObj->SetRowDim('4' , 16.5);
	$excelObj->SetRowDim('5' , 16.5);
	// 以後は行高さを設定しない。既定値

    if ($approval_column_print_flg == 't') {	// 決裁欄の印字は指定による（初期＝印字する）20131119 START

		// 「決裁」
		$excelCellPosition = $excelColumnName[$wk_colspan] . '1';
		$excelObj->SetArea($excelCellPosition); // セル位置
		$excelObj->SetValue(mb_convert_encoding('決','UTF-8','EUC-JP'));
		$excelObj->SetPosH('CENTER'); // ←中央→
		$excelObj->SetPosV('CENTER'); // ↑↓中央
		$excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),12);
		$excelBorderBeginPos=$excelCellPosition;

		$excelCellPosition = $excelColumnName[$wk_colspan] . '2';
		$excelObj->SetValueWithAttrib($excelCellPosition , mb_convert_encoding('裁','UTF-8','EUC-JP'));
		$excelBorderEndPos=$excelCellPosition;

		// 罫線
		$excelObj->SetArea($excelBorderBeginPos.':'.$excelBorderEndPos); // セル位置
		$excelObj->SetBorder( 'THIN' , 'FF000000' , 'outline' ); // 細い罫線 20150305

		// 管理職認印欄
//		for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
		$merge_idx = 1;

		for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
			// 認印の見出し。職名
			$excelCellPositionL = $excelColumnName[$wk_colspan+$merge_idx] . "1";
			$excelCellPositionR = $excelColumnName[$wk_colspan+$merge_idx+1] . "1";
			$excelObj->SetArea( $excelCellPositionL );
//			$excelObj->SetValue(mb_convert_encoding($st_name_array[$st_idx]["st_name"],'UTF-8','EUC-JP'));
			$excelObj->SetValueJP($st_name_array[$st_idx]["st_name"]);

			$excelObj->CellMerge($excelCellPositionL.":".$excelCellPositionR); // セル結合
			$excelObj->SetArea( $excelCellPositionL.":".$excelCellPositionR );
			$excelObj->SetBorder('THIN','FF000000','outline'); // 細い罫線 20150305
            $excelObj->SetShrinkToFit(); //20140512
            
			$excelObj->SetArea($excelCellPositionL);
			$excelObj->SetPosH('CENTER'); // ←中央→
			$excelObj->SetPosV('CENTER'); // ↑↓中央

			// 印章を押すところ。空欄
			$excelCellPositionL = $excelColumnName[$wk_colspan+$merge_idx] . "2";
			$excelCellPositionR = $excelColumnName[$wk_colspan+$merge_idx+1] . "2";
			$excelObj->SetArea($excelCellPositionL.':'.$excelCellPositionR); // 範囲指定
			$excelObj->SetBorder('THIN','FF000000','outline'); // 細い罫線 20150305
			$merge_idx = $merge_idx + 2;
		}

//		$data .= "</tr>\n";
//		$data .= "<tr height=\"60\">\n";

		if ($err_msg != "") {
//			$data .= "<td>";
//			$data .= $err_msg;
//			$data .= "</td></tr></table>\n";

			$excelObj->SetArea('A3');
			$excelObj->SetPosH('CENTER'); // ←中央→
			$excelObj->SetPosV('CENTER'); // ↑↓中央
			$excelObj->SetCharColor('FF0000'); // 赤文字
//			$excelObj->SetValue(mb_convert_encoding($err_msg,'UTF-8','EUC-JP'));
			$excelObj->SetValueJP($err_msg);
//			$wk_array = array();
//			$wk_array["data"] = $data;

			return $wk_array;
		}

    }
    // 決裁欄の印字は指定による（初期＝印字する）END
    
    // 作成者 20150305
	$createEmpDispFlg = $obj->conf_get($group_id, "createEmpDispFlg");
	if ($createEmpDispFlg == "t") {
	    $excelCellPosition = $excelColumnName[$wk_colspan] . '3';
		$excelObj->SetArea($excelCellPosition); // セル位置
		$createEmpStat = $obj->conf_get($group_id, "createEmpStat");
		$createEmpName = $obj->conf_get($group_id, "createEmpName");
		$createEmpStr = '作成者：';
		if ($createEmpStat != "") {
			$createEmpStr .= '役職　'.$createEmpStat.'　　';
		}
		$createEmpStr .= '氏名　'.$createEmpName.'　　　印';
		$excelObj->SetValue(mb_convert_encoding($createEmpStr,'UTF-8','EUC-JP'));
	}


	$wk_title = "<u>$group_name</u>  ";
	$wk_title .= $duty_yyyy."年";
	$wk_title .= $duty_mm."月　　";

	switch ($show_data_flg) {
		case "1":
			$tmp_title = " / 実績";
			break;
		case "2":
			$tmp_title = " / 希望";
			break;
		case "3":
			$tmp_title = " / $obj->duty_or_oncall_str";
			break;
		case "4":
			$tmp_title = " / コメント";
			break;
		default:
			$tmp_title = "";
	}

	$excelObj->SetArea('A2'); // セル位置
    
    //見出し
    //4週単位以外
    if ($excel_week4start == "") {
        $wk_title = $duty_yyyy."年".intval($duty_mm, 10)."月　　";
    }
    //4週単位の場合
    else {
        $wk_title = substr($excel_week4start, 0, 4)."年".
            intval(substr($excel_week4start, 4, 2), 10)."月".
            intval(substr($excel_week4start, 6, 2), 10)."日〜".
            substr($end_date, 0, 4)."年".
            intval(substr($end_date, 4, 2), 10)."月".
            intval(substr($end_date, 6, 2), 10)."日　";
    }
    $excelObj->SetValueJP($group_name." ".$wk_title.$print_title . $tmp_title);

	$excelCellPositionR = $excelColumnName[$wk_colspan - 1] . '2';	// colspan数の代わりにセルの左端から右までの位置
	$excelObj->CellMerge('A2:'.$excelCellPositionR);		// セル結合
	$excelObj->SetPosH('LEFT'); // ←左→
	$excelObj->SetPosV('CENTER'); // ↑↓中央

	//当初予定印字の場合
	if ($disp_tosYotei_flg == "t") {
		$excelObj->SetArea('A3'); // セル位置
		$excelObj->SetValueJP("※予定は当初予定です");
	}

	return ;
}

// 以下の関数はduty_shift_print_common_class.phpからの流用のため、修正がある場合は同時に行う
// 事由の出力について、印刷とは異なる
/*************************************************************************/
//
// 見出し（日段）
//
/*************************************************************************/

function showTitleWeek(
	$obj,
	$duty_yyyy,			//勤務年
	$duty_mm,			//勤務月
	$day_cnt,			//日数
	$plan_array,		//勤務シフト情報
	$week_array,		//週情報
	$title_gyo_array,	//行合計タイトル情報
	$calendar_array,	//カレンダー情報
	$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
	$show_data_flg,		//２行目表示データフラグ（１：勤務実績、２：勤務希望、３：当直、４：コメント）
	$hol_dt_flg,		//休暇日数詳細 1:表示 "":非表示
	$title_hol_array,	//休暇タイトル情報
	$excelColumnName,		//エクセル列名
	$excelObj,		//ExcelWorkShop
	$excel_blancrows,    // 曜日の下に空行をいれるかどうかのスイッチ 2012.8.22
    $paid_hol_disp_flg,    // 有給休暇残日数表示フラグ
    $week_color,
    //繰越公休残日数 start
    $obj_hol_remain,
    //繰越公休残日数 end
	$wk_group,         		// フラグ等引継ぎの為に追加
	$unit_name				//ユニット名
    )
{
	$reason_2_array = $obj->get_reason_2();

	//-------------------------------------------------------------------------------
	//日段
	//-------------------------------------------------------------------------------
	// 列幅は標準値(MS-Pゴシック12p)で設定する。
	// 指定単位はpoint数。1point=1/72インチ。
	// 以後、データ列の幅は日段見出しの処理で設定
	// 行集計の列幅は集計見出しのところで設定

	$excelTitleColPos = 0;

	// 曜日の下に空行をいれるかどうか
	if ( $excel_blancrows == 0 ){
		$excelMergeBottom = 5;
	}else{
		$excelMergeBottom = 6;
	}

	$excelMergeStart = 4;	// row指定のハードコーディングを変更

	$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
	$excelObj->SetValue(mb_convert_encoding('番号','UTF-8','EUC-JP'));
	$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);
	$excelObj->SetColDim($excelColumnName[$excelTitleColPos],4.88);
	$excelTitleColPos ++;

	// 表示／非表示による振り分け
	if ($wk_group["team_disp_flg"] == 't') {
		$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);
		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
		$excelObj->SetValue(mb_convert_encoding('チーム','UTF-8','EUC-JP'));
		$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
		$excelTitleColPos ++;
	}

	// 表示／非表示による振り分け
	if ($wk_group["unit_disp_flg"] == 't') {
		$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);
		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
		$excelObj->SetValue(mb_convert_encoding($unit_name,'UTF-8','EUC-JP'));
		$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
		$excelTitleColPos ++;
	}

	$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);
	$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
	$excelObj->SetValue(mb_convert_encoding('氏名','UTF-8','EUC-JP'));
	$excelObj->SetColDim($excelColumnName[$excelTitleColPos],18);	// 3cmにする 18 = 30mm/((25.4/72*12point))*2.54
	$excelTitleColPos ++;

	if ( $show_data_flg == "" ) {
		// 表示／非表示による振り分け
		if ($wk_group["job_disp_flg"] == 't') {
			$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);
			$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
			$excelObj->SetValue(mb_convert_encoding('職種','UTF-8','EUC-JP'));
			$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
			$excelTitleColPos ++;
		}

		// 表示／非表示による振り分け
		if ($wk_group["st_disp_flg"] == 't') {
			$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);
			$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
			$excelObj->SetValue(mb_convert_encoding('役職','UTF-8','EUC-JP'));
			$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
			$excelTitleColPos ++;
		}
	} else {
		// 2行表示の時、職種・役職が片側表示ならセルをマージする
		if (($wk_group["job_disp_flg"]=='t' && $wk_group["st_disp_flg"]!=='t') || ($wk_group["job_disp_flg"]!=='t' && $wk_group["st_disp_flg"]=='t')) {
			$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);

			// 表示／非表示による振り分け
			if ($wk_group["job_disp_flg"] == 't') {
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
				$excelObj->SetValue(mb_convert_encoding('職種','UTF-8','EUC-JP'));
				$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
				$excelTitleColPos ++;
			}
			// 表示／非表示による振り分け
			if ($wk_group["st_disp_flg"] == 't') {
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
				$excelObj->SetValue(mb_convert_encoding('役職','UTF-8','EUC-JP'));
				$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
				$excelTitleColPos ++;
			}
		} else {
			// 表示／非表示による振り分け
			if ($wk_group["job_disp_flg"] == 't') {
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
				$excelObj->SetValue(mb_convert_encoding('職種','UTF-8','EUC-JP'));
				$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
			}
			// 表示／非表示による振り分け
			if ($wk_group["st_disp_flg"] == 't') {
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].(string)($excelMergeStart + 1));
				$excelObj->SetValue(mb_convert_encoding('役職','UTF-8','EUC-JP'));
				$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 6.63);
			}
			if (($wk_group["job_disp_flg"] == 't') && ($wk_group["st_disp_flg"] == 't')) {
				// 表示の時だけ
				$excelTitleColPos ++;
			}
		}
	}

	//雇用・勤務形態 20130219
	// 表示／非表示による振り分け
	if ($wk_group["es_disp_flg"] == 't') {
		$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);
		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeStart);
		$excelObj->SetValue(mb_convert_encoding('雇用・勤務形態','UTF-8','EUC-JP'));
		$excelObj->SetColDim($excelColumnName[$excelTitleColPos], 18);	// 3cmにする 18 = 30mm/((25.4/72*12point))*2.54
		$excelTitleColPos ++;
	}
	$excelObj->SetColDim($excelColumnName[$excelTitleColPos],4.88); // (空欄)

	$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeStart.":".$excelColumnName[$excelTitleColPos].$excelMergeBottom);

	$excelObj->SetArea("A4:".$excelColumnName[$excelTitleColPos].$excelMergeStart + 1);

	//勤務状況（日）
	for ($k=1; $k<=$day_cnt; $k++) {

		$wk_date = $calendar_array[$k-1]["date"];
		$wk_dd = (int)substr($wk_date,6,2);

		$wk_back_color = "FFFFFF";
		if ($week_color) {
            if ($calendar_array[$k-1]["type"] == "6") {
                $wk_back_color = "C8FFD5";
            }
            else if ($week_array[$k]["name"] == "土" or $calendar_array[$k-1]["type"] == "5"){
			$wk_back_color = "E0FFFF";
            }
            else if ($week_array[$k]["name"] == "日" or $calendar_array[$k-1]["type"] == "4") {
			$wk_back_color = "FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
			//			$wk_back_color = "#FFF0F5";
			}
		}
		$excelObj->SetColDim( $excelColumnName[$k+$excelTitleColPos] , 2.88 ); // 列幅
		$excelObj->SetArea( $excelColumnName[$k+$excelTitleColPos].$excelMergeStart );
		$excelObj->SetValue( $wk_dd );
		$excelObj->SetBackColor( $wk_back_color );
	}

	$rightPosition = $k + $excelTitleColPos;
	$total_print_flg = "1"; // 集計行列は常に出力する // PHP5.1.6対応
	//集計行列の表示をする場合

	if ($total_print_flg == "1") {
		//個人日数
		for ($k=0; $k<count($title_gyo_array); $k++) {
			$excelColPos = $k + $rightPosition;
		    $wk = $title_gyo_array[$k]["name"];
			$excelObj->SetColDim($excelColumnName[$excelColPos],6.66); // 集計の列幅 1.1cm
			if ($title_gyo_array[$k]["id"] == "10") {
				//公休、有給表示
				$excelObj->SetArea($excelColumnName[$excelColPos].$excelMergeStart);
				$excelObj->SetValue( mb_convert_encoding( '公休' , 'UTF-8','EUC-JP') );
				$excelObj->CellMerge( $excelColumnName[$excelColPos].$excelMergeStart.":".$excelColumnName[$excelColPos].$excelMergeBottom );
				$excelObj->SetColDim($excelColumnName[$excelColPos],4.00); // 集計の列幅

				$excelColPos ++;
				$excelObj->SetArea($excelColumnName[$excelColPos].$excelMergeStart);
				$excelObj->SetValue( mb_convert_encoding( '年休有休' , 'UTF-8','EUC-JP'));
				$excelObj->CellMerge( $excelColumnName[$excelColPos].$excelMergeStart.":".$excelColumnName[$excelColPos].$excelMergeBottom );
				$excelObj->SetColDim($excelColumnName[$excelColPos],4.00); // 集計の列幅

				$excelColPos ++;
				$excelObj->SetArea($excelColumnName[$excelColPos].$excelMergeStart);
				$excelObj->SetValueJP( $wk.'計' );
				$excelObj->CellMerge( $excelColumnName[$excelColPos].$excelMergeStart.":".$excelColumnName[$excelColPos].$excelMergeBottom );
				$excelObj->SetColDim($excelColumnName[$excelColPos],4.00); // 「計」の列幅 1.1cm
			} else {
			    $excelObj->SetArea($excelColumnName[$excelColPos].$excelMergeStart );
			    $excelObj->SetValueJP( $wk );
			    $excelObj->CellMerge( $excelColumnName[$excelColPos].$excelMergeStart.":".$excelColumnName[$excelColPos].$excelMergeBottom );
			    $excelObj->SetColDim($excelColumnName[$excelColPos],4.00); // 集計の列幅 1.1cm
				$excelObj->SetWrapText();	// 折り返しを追加 20130106
			}
		}

		//有休残日数
		if ($paid_hol_disp_flg === "t") {
		    //$excelColPos = $rightPosition + count($title_gyo_array);
			$excelColPos ++;
		    $excelObj->SetArea($excelColumnName[$excelColPos].$excelMergeStart);
		    $excelObj->SetValue( mb_convert_encoding('有残', 'UTF-8','EUC-JP'));
		    $excelObj->CellMerge($excelColumnName[$excelColPos].$excelMergeStart.":".$excelColumnName[$excelColPos].$excelMergeBottom);
		    $excelObj->SetColDim($excelColumnName[$excelColPos], 4.00); // 集計の列幅 1.1cm
		    $rightPosition++;
		}
        //繰越公休残日数 start
        if ($obj_hol_remain->transfer_holiday_days_flg == "t" && $show_data_flg == "1") {
			$excelColPos ++;
            $excelObj->SetArea($excelColumnName[$excelColPos].'4');
            $excelObj->SetValue( mb_convert_encoding('繰越公休残', 'UTF-8','EUC-JP'));
            $excelObj->CellMerge($excelColumnName[$excelColPos].'4:'.$excelColumnName[$excelColPos].$excelMergeBottom);
            $excelObj->SetColDim($excelColumnName[$excelColPos], 4.00); // 集計の列幅 1.1cm
            $rightPosition++;
        }
        //繰越公休残日数 end
	}

	$excelObj->SetArea($excelColumnName[$rightPosition].$excelMergeStart.":".$excelColumnName[$k+$rightPosition+1].(string)($excelMergeStart + 1));
	$excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),10);
	$excelObj->SetWrapText();
	$rightPosition += $k ;

	//休暇詳細
	$hol_dt_flg = '0'; // 休暇詳細は当面は出さない PHP5.1.6対応

	if ($hol_dt_flg == "1") {
		for ($k=0; $k<count($title_hol_array); $k++) {
			//公休・年休非表示
			if ($title_hol_array[$k]["reason"] == "1" ||
					$title_hol_array[$k]["reason"] == "24") {
				continue;
			}
			$wk = $title_hol_array[$k]["name"];
		}
	}
	//-------------------------------------------------------------------------------
	//曜日段
	//-------------------------------------------------------------------------------
	//勤務状況（曜日）
	for ($k=1; $k<=$day_cnt; $k++) {
		//土、日、祝日の文字色＆背景色変更
		$wk_color = "";
		$wk_back_color = "FFFFFF";
		if ($week_color) {
            if ($calendar_array[$k-1]["type"] == "6") {
                $wk_color = "ff0000";
                $wk_back_color = "C8FFD5";
            }
            else if ($week_array[$k]["name"] == "土" or $calendar_array[$k-1]["type"] == "5"){
			$wk_color = "0000ff";
			$wk_back_color = "E0FFFF";
            }
            else if ($week_array[$k]["name"] == "日"or $calendar_array[$k-1]["type"] == "4") {
			$wk_color = "ff0000";
			$wk_back_color = "FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
			}
		}
		//表示
		$wk_name = $week_array[$k]["name"];
		$excelObj->SetArea( $excelColumnName[$k+$excelTitleColPos]."5" );
		$excelObj->SetValueJP( $wk_name );
		$excelObj->SetBackColor( $wk_back_color );
		if ( $excel_blancrows != 0 ){
			$excelObj->SetArea( $excelColumnName[$k+$excelTitleColPos]."6" );
			$excelObj->SetBackColor( $wk_back_color );
		}
	}
		// 2012.2.2 「集計方法変更」課題対応
	if( $title_gyo_array[0]["change_switch"]=="1"){
		$rightPosition -= 2;
	}
	$excelObj->SetArea("A4:". $excelColumnName[$rightPosition+1]."5" );
	$excelObj->SetBorder( "THIN" , "000000" , 'all' );
	$excelObj->SetPosH('CENTER');
	$excelObj->SetPosV('CENTER');

	return ;
}
/*************************************************************************/
//
// 表データ
//
/*************************************************************************/

function showList(
	$obj,
	$obj_duty,
	$pattern_id,			//出勤パターンＩＤ
	$plan_array,			//勤務シフト情報
	$title_gyo_array,		//行合計タイトル情報
	$gyo_array,				//行合計情報
	$start_cnt,				//行表示開始位置
	$end_cnt,				//行表示終了位置
	$day_cnt,				//日数
	$max_font_cnt,			//最大文字数
	$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
	$show_data_flg,			//２行目表示データフラグ（""：予定のみ、１：勤務実績、２：勤務希望、３：当直、４：コメント）
	$group_id,
	$reason_setting_flg,
	$hol_dt_flg,			//休暇日数詳細 1:表示 "":非表示
	$title_hol_array,		//休暇タイトル情報
	$hol_cnt_array,			//休暇日数
	$paid_hol_all_array,	//年休情報
	$calendar_array,
	$week_array,
	$excelColumnName,		//エクセル列名
	$excelObj,		//ExcelWorkShop
	$excel_blancrows,	// 曜日の下のブランク行有無のスイッチ
    $paid_hol_disp_flg,    // 有給休暇残日数表示フラグ
	$week_color,
	$hope_color,
    //繰越公休残日数 start
    $obj_hol_remain,
    //繰越公休残日数 end
	$wk_group         		// フラグ等引継ぎの為に追加
	){
	// 当日

	$today = Date("Ymd");

	//		$wk_height = 22 * (int)$max_font_cnt;
	// 高さを22固定とする2009/01/06
//	$wk_height = 22;
	//		$wk_height = 66;
	// 高さ22ピクセルはエクセルの16.5ポイント 2012/1/16
	$wk_height = 16.5;

	$reason_2_array = $obj->get_reason_2();

	//当直パターン取得
	$duty_pattern =	$obj_duty->get_duty_pattern_array();

	$data = "";

	// 表内の規定値
	$wk_color = "FFFFFF"; // セル色の規定値は白
	$excelObj->SetBackColorDef( $wk_color );

	$excelCellRowPosition = 6; // 6行から
	if ( $excel_blancrows != 0 ){
		$excelCellRowPosition = 7; // 曜日の下のブランク行があるときは1行下から
	}

//行事予定 20150709
	$event_start_row = $excelCellRowPosition;
	global $start_date, $end_date;
	$arr_event_data = $obj->get_event_data($group_id, $start_date, $end_date);

$event_flg = $_REQUEST['excel_event'];
	if ($event_flg == "1") {
	
		for ($i=1; $i<=4; $i++) {

			$excelTitleColPos = 0;
			//*******************************************************************************
			// 予定データ
			//*******************************************************************************
			$excelObj->SetRowDim($excelCellRowPosition , 16.5); // 高さ22pixcel=16.5point 2012.1.16
			$excelTitleColPos++; // A列→B列
			// チーム
			if ($wk_group["team_disp_flg"] == 't') {
					$excelTitleColPos++; // B列→C列
			}
			// ユニット
			if ($wk_group["unit_disp_flg"] == 't') {
					$excelTitleColPos++; // B列→C列
			}
			// 氏名
			$excelTitleColPos++; // C列→D列
			// 職種 / 役職
			if ( $span == 2 ) {
				// 2行表示 職種・役職片側表示
				if (($wk_group["job_disp_flg"]=='t' && $wk_group["st_disp_flg"]!=='t') || ($wk_group["job_disp_flg"]!=='t' && $wk_group["st_disp_flg"]=='t')) {
					$excelTitleColPos ++;

				} else { // 職種・役職両方表示又は両方非表示
					if (($wk_group["job_disp_flg"] == 't') && ($wk_group["st_disp_flg"] == 't')) {
						// 表示の時だけ
						$excelTitleColPos ++;
					}
				}
			} else {
				// 表示／非表示による振り分け
				if ($wk_group["job_disp_flg"] == 't') {
					$excelTitleColPos++;
				}
				// 表示／非表示による振り分け
				if ($wk_group["st_disp_flg"] == 't') {
					$excelTitleColPos++;
				}
			}
			// 雇用・勤務形態
			if ($wk_group["es_disp_flg"] == 't') {
				$excelTitleColPos++; // 列を進める
			}
			// 行事予定見出し
			$event_start_col = $excelTitleColPos;
			if ($i == 1)  {
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
				$excelObj->SetValueJP( "行\n事\n予\n定" );
			}
			// 行事予定
			for ($k=1; $k<=$day_cnt; $k++) {
				$wk_date = $calendar_array[$k-1]["date"];

				$excelObj->SetArea($excelColumnName[$k+$excelTitleColPos].$excelCellRowPosition);

				$wk = mb_substr($arr_event_data["$wk_date"]["event".$i], 0, 2);
//$wk = "$wk_date";
				$excelObj->SetValueJP( $wk );
				$excelObj->SetShrinkToFit();

			}
			
			//集計行列の表示をする場合

			$excelCellRowPosition++;
		}
	}

	// 役職・職種・雇用勤務形態の履歴情報 20130219
//	$wk_group = array();									// 表示フラグは上位から引き継ぐ
//	$wk_group[0] = array("job_disp_flg"=>"t", "st_disp_flg"=>"t", "es_disp_flg"=>"t");

	$history_info = $obj->get_history_info($calendar_array, $wk_group);

	for ($i=$start_cnt; $i<$end_cnt; $i++) {

		$excelTitleColPos = 0;
		//*******************************************************************************
		// 予定データ
		//*******************************************************************************
		$excelObj->SetRowDim($excelCellRowPosition , 16.5); // 高さ22pixcel=16.5point 2012.1.16

		if ($show_data_flg != "") {
			$span = "2";
			// セル結合の準備、行指定
			$excelMergeBEGIN = $excelCellRowPosition ;
			$excelMergeEND = $excelCellRowPosition + 1;
		} else {
			$span = "1";
		}

		//-------------------------------------------------------------------------------
		// 表示順
		//-------------------------------------------------------------------------------
		$wk = $plan_array[$i]["no"];

		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
		$excelObj->SetValueJP( $wk ); // 値を設定
		if ( $span == "2" ){											// 2段表示のとき
			$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeBEGIN.':'.$excelColumnName[$excelTitleColPos].$excelMergeEND);	// 結合範囲指定
			$excelObj->CellMergeReady();									// 結合
		}
		$excelTitleColPos++; // A列→B列
		//-------------------------------------------------------------------------------
		// チーム
		//-------------------------------------------------------------------------------
		// 表示／非表示による振り分け
		if ($wk_group["team_disp_flg"] == 't') {
				// チームの背景色
				$wk_color = "FFFFFF";
				if ($plan_array[$i]["other_staff_flg"] == "1") {
					//他病棟スタッフ
					$wk_color = "dce8bb";
					$wk_color = "FF00FF";
				}
				$wk = $plan_array[$i]["team_name"];
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
				$excelObj->SetValueJP( $wk );
				$excelObj->SetBackColor( $wk_color );
				if ( $span == "2" ){											// 2段表示のとき
					$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeBEGIN.":".$excelColumnName[$excelTitleColPos].$excelMergeEND);	// 結合範囲指定
					$excelObj->CellMergeReady();									// 結合
				}
				$excelTitleColPos++; // B列→C列
		}
		//-------------------------------------------------------------------------------
		// ユニット
		//-------------------------------------------------------------------------------
		// 表示／非表示による振り分け
		if ($wk_group["unit_disp_flg"] == 't') {
				$staff_id = $plan_array[$i]["staff_id"];
				$wk = $obj->get_emp_unitname($plan_array[$i]["staff_id"], $group_id);
				$wk_color = "000000";
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
				$excelObj->SetValueJP( $wk );
				$excelObj->SetCharColor( $wk_color );
				$excelObj->SetShrinkToFit();
				if ( $span == "2" ){											// 2段表示のとき
					$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeBEGIN.":".$excelColumnName[$excelTitleColPos].$excelMergeEND);	// 結合範囲指定
					$excelObj->CellMergeReady();									// 結合
				}
				$excelTitleColPos++; // B列→C列
		}
		//-------------------------------------------------------------------------------
		// 氏名
		//-------------------------------------------------------------------------------
		// 氏名の背景色
		$wk_color = "FFFFFF";
		if ($plan_array[$i]["other_staff_flg"] == "1") {
			$wk_color = "dce8bb"; //他病棟スタッフ
		}
		$excelObj->SetBackColorDef( $wk_color );
		// 氏名
		$wk = $plan_array[$i]["staff_name"];
		$color = ( $plan_array[$i]["sex"] == 1 ) ? 'blue' : 'black';
		$color = str_replace("#", "", $color); // #を取る
		if ( $color == "" ){
			$color = "FFFFFF";
		}
		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
		$excelObj->SetValueJP( $wk );
		$excelObj->SetCharColor( "000000" ); // 性別で色違いをやめる $color -> "000000" 2012.1.20
		$excelObj->SetBackColor( $wk_color );
		$excelObj->SetShrinkToFit();
		if ( $span == "2" ){											// 2段表示のとき
			$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeBEGIN.":".$excelColumnName[$excelTitleColPos].$excelMergeEND);	// 結合範囲指定
			$excelObj->CellMergeReady();									// 結合
		}
		$excelTitleColPos++; // C列→D列

		//-------------------------------------------------------------------------------
		// 職種 / 役職
		//-------------------------------------------------------------------------------
		if ( $span == 2 ) {
			// 2行表示 職種・役職片側表示
			if (($wk_group["job_disp_flg"]=='t' && $wk_group["st_disp_flg"]!=='t') || ($wk_group["job_disp_flg"]!=='t' && $wk_group["st_disp_flg"]=='t')) {
				// 片側表示の時、マージ
				$excelObj->CellMerge($excelColumnName[$excelTitleColPos].$excelMergeBEGIN.":".$excelColumnName[$excelTitleColPos].$excelMergeEND);
				$excelObj->CellMergeReady();									// 結合

				// 表示／非表示による振り分け
				if ($wk_group["job_disp_flg"] == 't') {
					$staff_id = $plan_array[$i]["staff_id"];
					//職種履歴を考慮する 20130219
					$arr_name_color = $obj->get_show_name($history_info["job_id_info"][$staff_id], $history_info["job_id_name"], "job_id", $history_info["start_date"], $history_info["end_date"]);
					$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["job_name"];
					$wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "000000";
					$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
					$excelObj->SetValueJP( $wk );
					$excelObj->SetCharColor( $wk_color );
					$excelObj->SetShrinkToFit();
				}
				// 表示／非表示による振り分け
				if ($wk_group["st_disp_flg"] == 't') {
					$staff_id = $plan_array[$i]["staff_id"];
					//役職履歴を考慮する 20130219
					$arr_name_color = $obj->get_show_name($history_info["st_id_info"][$staff_id], $history_info["st_id_name"], "st_id", $history_info["start_date"], $history_info["end_date"]);
					$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["status"];
					$wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "000000";
					$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
					$excelObj->SetValueJP( $wk );
					$excelObj->SetCharColor( $wk_color );
				}
				$excelTitleColPos ++;

			} else { // 職種・役職両方表示又は両方非表示
				// 表示／非表示による振り分け
				if ($wk_group["job_disp_flg"] == 't') {
					$staff_id = $plan_array[$i]["staff_id"];
					//職種履歴を考慮する 20130219
					$arr_name_color = $obj->get_show_name($history_info["job_id_info"][$staff_id], $history_info["job_id_name"], "job_id", $history_info["start_date"], $history_info["end_date"]);
					$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["job_name"];
					$wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "000000";
					$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
					$excelObj->SetValueJP( $wk );
					$excelObj->SetCharColor( $wk_color );
					$excelObj->SetShrinkToFit();
				}
				// 表示／非表示による振り分け
				if ($wk_group["st_disp_flg"] == 't') {
					$staff_id = $plan_array[$i]["staff_id"];
					//役職履歴を考慮する 20130219
					$arr_name_color = $obj->get_show_name($history_info["st_id_info"][$staff_id], $history_info["st_id_name"], "st_id", $history_info["start_date"], $history_info["end_date"]);
					$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["status"];
					$wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "000000";
					$excelObj->SetArea($excelColumnName[$excelTitleColPos].(string)($excelCellRowPosition + 1));
					$excelObj->SetValueJP( $wk );
					$excelObj->SetCharColor( $wk_color );
				}
				if (($wk_group["job_disp_flg"] == 't') && ($wk_group["st_disp_flg"] == 't')) {
					// 表示の時だけ
					$excelTitleColPos ++;
				}
			}
		} else {
			// 表示／非表示による振り分け
			if ($wk_group["job_disp_flg"] == 't') {
				$staff_id = $plan_array[$i]["staff_id"];
				//職種履歴を考慮する 20130219
				$arr_name_color = $obj->get_show_name($history_info["job_id_info"][$staff_id], $history_info["job_id_name"], "job_id", $history_info["start_date"], $history_info["end_date"]);
				$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["job_name"];
				$wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "000000";
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
				$excelObj->SetValueJP( $wk );
				$excelObj->SetCharColor( $wk_color );
				$excelObj->SetShrinkToFit();
				$excelTitleColPos++;
			}
			// 表示／非表示による振り分け
			if ($wk_group["st_disp_flg"] == 't') {
				$staff_id = $plan_array[$i]["staff_id"];
				//役職履歴を考慮する 20130219
				$arr_name_color = $obj->get_show_name($history_info["st_id_info"][$staff_id], $history_info["st_id_name"], "st_id", $history_info["start_date"], $history_info["end_date"]);
				$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["status"];
				$wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "000000";
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
				$excelObj->SetValueJP( $wk );
				$excelObj->SetCharColor( $wk_color );
				$excelTitleColPos++;
			}
		}

		// 表示／非表示による振り分け
		if ($wk_group["es_disp_flg"] == 't') {
				//雇用・勤務形態 20130219
				$arr_wk = array("1"=>"常勤","2"=>"非常勤","3"=>"短時間正職員");
				$buf_wk = $plan_array[$i]["duty_form"];
				$arr_name_color = $obj->get_show_name($history_info["duty_form_info"][$staff_id], $arr_wk, "duty_form", $history_info["start_date"], $history_info["end_date"]);
				$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $arr_wk[$buf_wk];
				$wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "000000";
				$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
				$excelObj->SetValueJP( $wk );
				$excelObj->SetCharColor( $wk_color );
				$excelObj->SetShrinkToFit();
				if ( $span == "2" ){											// 2段表示のとき
					$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelMergeBEGIN.":".$excelColumnName[$excelTitleColPos].$excelMergeEND);	// 結合範囲指定
					$excelObj->CellMergeReady();										// 結合
				}
				$excelTitleColPos++; // 列を進める
		}
		//-------------------------------------------------------------------------------
		// みだし
		//-------------------------------------------------------------------------------
		//$excelObj->SetCharColor( '000000' );
		if ( $span == "2" ){											// 2段表示のとき
			;
		}
		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition);
		$excelObj->SetValue( mb_convert_encoding( '予定' , 'UTF-8','EUC-JP'));


		//-------------------------------------------------------------------------------
		// 勤務状況
		//-------------------------------------------------------------------------------
		// PHP Excel Native改造 色指定を #FFFFFF→FFFFFFへ。#をとる

		$excelObj->SetCharColorDef( '000000' );
		for ($k=1; $k<=$day_cnt; $k++) {

			//勤務シフト予定（表示）
			if ($plan_array[$i]["pattern_id_$k"] == $pattern_id &&
					($plan_array[$i]["assist_group_$k"] == "" ||
						$plan_array[$i]["assist_group_$k"] == $group_id)) {
				$wk_font_color = $plan_array[$i]["font_color_$k"];
				$wk_back_color = $plan_array[$i]["back_color_$k"];

			} else {
				$wk_font_color = "#C0C0C0";		//灰色
				$wk_back_color = '#FFFFFF';
			}

			//*曜日で背景色設定
			if ($week_color and ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF")){
                if ($calendar_array[$k-1]["type"] == "6") {
                    $wk_back_color = "#C8FFD5";
                }
				else if ($week_array[$k]["name"] == "土" or $calendar_array[$k-1]["type"] == "5"){
					$wk_back_color = "#E0FFFF";
				}
                else if ($week_array[$k]["name"] == "日" or $calendar_array[$k-1]["type"] == "4") {
					$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
				}
			}

            // 希望色
            if ($hope_color) {
                global $plan_individual, $match_bgcolor, $match_color, $unmatch_bgcolor, $unmatch_color;
                $wk_hope_ptn = $plan_individual["e{$plan_array[$i]["staff_id"]}"]["d{$k}"]["id"];
                $wk_plan_ptn = $plan_array[$i]["atdptn_ptn_id_$k"];
                //両方設定されている場合
                if ($wk_hope_ptn != "" && $wk_plan_ptn != "") {
                    //一致の場合
                    if ($wk_hope_ptn == $wk_plan_ptn) {
                        if ($match_bgcolor !== 'noset') {
                            $wk_back_color = $match_bgcolor;
                        }
                        if ($match_color !== 'noset') {
                            $wk_font_color = $match_color;
                        }
                    }
                    //不一致の場合
                    else {
                        if ($unmatch_bgcolor !== 'noset') {
                            $wk_back_color = $unmatch_bgcolor;
                        }
                        if ($unmatch_color !== 'noset') {
                            $wk_font_color = $unmatch_color;
                        }
					}
				}
			}

			//名称
			$wk = $plan_array[$i]["font_name_$k"];
			//2008/11/27 改修
			if ($plan_array[$i]["reason_2_$k"] != "" &&
					$reason_setting_flg == "t") {
				for ($m=0; $m<count($reason_2_array); $m++) {
					if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"]) {
						$wk_reason_name = $reason_2_array[$m]["font_name"];
						$wk .= $wk_reason_name;
						break;
					}
				}
			}

			$wk_font_color = str_replace("#", "", $wk_font_color); // #を取る
			if ( $wk_font_color == "" ){
				$wk_font_color = "FFFFFF";
			}
			$wk_back_color = str_replace("#", "", $wk_back_color); // #を取る
			if ( $wk_back_color == "" ){
				$wk_back_color = "FFFFFF";
			}
			$wk = str_replace("&nbsp;", "", $wk ); // HTMLの空白をNull化

			$excelObj->SetArea($excelColumnName[$k+$excelTitleColPos].$excelCellRowPosition);
			$excelObj->SetValueJP( $wk );
			$excelObj->SetBackColor( $wk_back_color );
			$excelObj->SetCharColor( $wk_font_color );
			$excelObj->SetShrinkToFit();

		}

		$excelClumnNum = $k + $excelTitleColPos;
		$excelDataPosEnd = $k + $excelTitleColPos - 1;
		//集計行列の表示をする場合

		if ($total_print_flg == "1") {
			if ($show_data_flg == "3" || $show_data_flg == "4") { // 3:当直、4:コメント
				$span = "2";
				$excelMergeBEGIN = $excelCellRowPosition ;
				$excelMergeEND = $excelCellRowPosition + 1;
			} else {
				$span = "1";
			}
			//-------------------------------------------------------------------------------
			//個人日数
			//-------------------------------------------------------------------------------
			//公休・年休表示
			$wk_koukyu_cnt = 0;
			$wk_nenkyu_cnt = 0;
			for ($k=0; $k<count($title_hol_array); $k++) {
				if ($title_hol_array[$k]["reason"] == "1") {
					$wk_nenkyu_cnt = $hol_cnt_array[$i][$k];
				}
				if ($title_hol_array[$k]["reason"] == "24") {
					$wk_koukyu_cnt = $hol_cnt_array[$i][$k];
				}
			}
			$excelObj->SetCharColorDef( '000000' );
			for ($k=0; $k<count($title_gyo_array); $k++) {
				$wk = $gyo_array[$i][$k];
				if ($title_gyo_array[$k]["id"] == "10") {
					//公休、有休表示
					$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
					$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
					$excelObj->SetValueJP( $wk_koukyu_cnt );
					$excelObj->SetShrinkToFit();
					if ( $span == "2" ){											// 2段表示のとき
						$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定
						$excelObj->CellMergeReady();									// 結合
					}
					$excelClumnNum ++ ;
					$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
					$excelObj->SetValueJP( $wk_nenkyu_cnt );
					$excelObj->SetShrinkToFit();
					if ( $span == "2" ){											// 2段表示のとき
						$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定
						$excelObj->CellMergeReady();									// 結合
					}
					$excelClumnNum ++ ;
				}

				$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
				$excelObj->SetValueJP( $wk );
				$excelObj->SetShrinkToFit();
				if ( $span == "2" ){											// 2段表示のとき
					$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定
					$excelObj->CellMergeReady();									// 結合
				}
				$excelClumnNum ++ ;
			}
			//有休残日数
			if ($paid_hol_disp_flg === "t") {
			    $wk = $paid_hol_all_array["plan"][$i];
			    $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
			    $excelObj->SetValueJP($wk);
			    $excelObj->SetShrinkToFit();
			    if ($span == "2"){											// 2段表示のとき
			        $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定
			        $excelObj->CellMergeReady();									// 結合
			    }
			    $excelClumnNum++;
			}
            //繰越公休残日数 start
            if ($obj_hol_remain->transfer_holiday_days_flg == "t" && $show_data_flg == "1") {
                $wk = ""; //予定側
                $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
                $excelObj->SetValueJP($wk);
                $excelObj->SetShrinkToFit();
                if ($span == "2"){											// 2段表示のとき
                    $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定
                    $excelObj->CellMergeReady();									// 結合
                }
                $excelClumnNum++;
            }
            //繰越公休残日数 end
		}

		//休暇日数詳細
		if ($hol_dt_flg == "1") {
			if ($show_data_flg == "3" || $show_data_flg == "4") {
				$span = "2";
				$excelMergeBEGIN = $excelCellRowPosition ;
				$excelMergeEND = $excelCellRowPosition + 1;
			} else {
				$span = "1";
			}
			$excelObj->SetCharColorDef( '000000' );
			for ($k=0; $k<count($title_hol_array); $k++) {
				//公休・年休非表示
				if ($title_hol_array[$k]["reason"] == "1" ||
						$title_hol_array[$k]["reason"] == "24") {
					continue;
				}
				$wk = $hol_cnt_array[$i][$k];
				$excelObj->SetValueWithAttrib( $excelColumnName[$excelClumnNum].$excelCellRowPosition , mb_convert_encoding( $wk , 'UTF-8','EUC-JP') );
				if ( $span == "2" ){											// 2段表示のとき
					$excelObj->SetArea($excelColumnName[excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[excelClumnNum].$excelMergeEND);	// 結合範囲指定
					$excelObj->CellMergeReady();									// 結合
				}
				$excelObj->SetBorder( 'THIN' , '000000' , 'outline' );
				$excelClumnNum ++ ;
			}
		}
		$excelCellAreaEND = $excelClumnNum - 1;
		$excelClumnNum = $excelTitleColPos;

		//*******************************************************************************
		// 実績データ
		//*******************************************************************************
		if ($show_data_flg != "") {
			$excelCellRowPosition ++;
			switch ($show_data_flg) {
				case "0": // 2012.2.3 追加
					$tmp_title = "";
					break;
				case "1":
					$tmp_title = "実績";
					break;
				case "2":
					$tmp_title = "希望";
					break;
				case "3":
					$tmp_title = $obj->duty_or_oncall_str;
					break;
				case "4":
					$tmp_title = "コメント";
					break;
				default:
					$tmp_title = "";
			}
			$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
			$excelObj->SetValueJP( $tmp_title );
			$excelClumnNum ++ ;
			//勤務状況（実績）
			for ($k=1; $k<=$day_cnt; $k++) {

				//コメントの場合
				if ($show_data_flg == "4") { // コメント表示あり
					$wk = $plan_array[$i]["comment_$k"];
					$wk_font_color = "#000000";
					$wk_back_color = "#ffffff";
				} else
					if ($show_data_flg != "3" && $show_data_flg != "0") { // 実績表示、勤務希望表示あり（当直表示以外）
						// 当日以前の場合に表示、希望は無条件に表示
						$duty_ymd = $calendar_array[$k-1]["date"]; //前月から当月が作成期間の場合、出力されない不具合対応 20111027
						if ($duty_ymd <= $today || $show_data_flg == "2") {

							//文字、背景色
							if ($plan_array[$i]["rslt_pattern_id_$k"] == $pattern_id) {
								$wk_font_color = $plan_array[$i]["rslt_font_color_$k"];
								$wk_back_color = $plan_array[$i]["rslt_back_color_$k"];
							} else {
								$wk_font_color = "#C0C0C0";		//灰色
								$wk_back_color = "#ffffff";
							}
							//勤務実績
							$wk = $plan_array[$i]["rslt_name_$k"];
							//2008/11/27 改修
							if ($plan_array[$i]["rslt_reason_2_$k"] != "" &&
									$reason_setting_flg == "t") {
								for ($m=0; $m<count($reason_2_array); $m++) {
									if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"]) {
										$wk_reason_name = $reason_2_array[$m]["font_name"];
										$wk .= $wk_reason_name;
										break;
									}
								}
							}
						} else {
							$wk_font_color = "";
							$wk_back_color = "";
							$wk = ""; // "_" -> " "
						}
					} else {
						$wk = ""; // "_" -> " "
						$wk_font_color = "";
						$wk_back_color = "";
						for ($m=0; $m<count($duty_pattern); $m++) {
							if ($plan_array[$i]["night_duty_$k"] == $duty_pattern[$m]["id"]) {
								$wk = $duty_pattern[$m]["font_name"];	//表示文字名
								$wk_font_color = $duty_pattern[$m]["font_color"];
								$wk_back_color = $duty_pattern[$m]["back_color"];
							}
						}
					}

					//*曜日で背景色設定
					if ($week_color and ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF")){
                        if ($calendar_array[$k-1]["type"] == "6") {
                            $wk_back_color = "#C8FFD5";
                        }
						else if ($week_array[$k]["name"] == "土" or $calendar_array[$k-1]["type"] == "5"){
							$wk_back_color = "#E0FFFF";
						}
                        else if ($week_array[$k]["name"] == "日" or $calendar_array[$k-1]["type"] == "4") {
							//*					$wk_back_color = "#FFF0F5";
							$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
						}
					}

				$wk_width = 20;
				$wk_font_color = str_replace("#", "", $wk_font_color); // #を取る
				if ( $wk_font_color == "" ){
					$wk_font_color = "FFFFFF";
				}
				$wk_back_color = str_replace("#", "", $wk_back_color); // #を取る
				if ( $wk_back_color == "" ){
					$wk_back_color = "FFFFFF";
				}
				$wk = str_replace("&nbsp;", "", $wk ); // 空白の処理
				$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition );
				$excelObj->SetValueJP( $wk );
				$excelObj->SetBackColor( $wk_back_color );
				$excelObj->SetCharColor( $wk_font_color );
				$excelObj->SetShrinkToFit();					// 縮小表示抜け 20140106
				$excelClumnNum ++ ;

			} // for

			//２段目の集計
			if ($total_print_flg == "1" && ($show_data_flg == "1" || $show_data_flg == "2")) {
				$gyo_array_cnt = count($title_gyo_array);
				$hol_array_cnt = count($title_hol_array);

				//公休・年休表示
				$wk_koukyu_cnt = 0;
				$wk_nenkyu_cnt = 0;
				for ($k=0; $k<count($title_hol_array); $k++) {
					//公休・年休表示
					if ($title_hol_array[$k]["reason"] == "1") {
						$wk_nenkyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						$wk_koukyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
					}
				}
				$excelObj->SetCharColorDef( '000000' );
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = $gyo_array[$i][$gyo_array_cnt + $k];
					if ($title_gyo_array[$k]["id"] == "10") {
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;

						$excelObj->SetArea( $excelColumnName[$excelClumnNum].$excelCellRowPosition );
						$excelObj->SetValueJP( $wk_koukyu_cnt );
						if ( $span == "2" ){											// 2段表示のとき
                            $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定 20131119   excelClumnNum=>$excelClumnNum
							$excelObj->CellMergeReady();	// 結合
						}
						$excelClumnNum ++ ;

						$excelObj->SetArea( $excelColumnName[$excelClumnNum].$excelCellRowPosition );
						$excelObj->SetValueJP( $wk_nenkyu_cnt );
						if ( $span == "2" ){											// 2段表示のとき
                            $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定 20131119   excelClumnNum=>$excelClumnNum
							$excelObj->CellMergeReady();	// 結合
						}
						$excelClumnNum ++ ;
					}
					$wk = str_replace("&nbsp;", "", $wk ); // 空白の処理
					$excelObj->SetArea( $excelColumnName[$excelClumnNum].$excelCellRowPosition );
					$excelObj->SetValueJP( $wk );
					$excelClumnNum ++ ;
				}
				// 有休残日数
				if ($paid_hol_disp_flg === "t") {
				    if ($show_data_flg == "1") {
				        $wk = $paid_hol_all_array["result"][$i];
				    } elseif ($show_data_flg == "2") {
				        $wk = $paid_hol_all_array["hope"][$i];
				    }
				    $wk = str_replace("&nbsp;", "", $wk); // 空白の処理
				    $excelObj->SetValueWithAttrib( $excelColumnName[$excelClumnNum].$excelCellRowPosition , mb_convert_encoding( $wk , 'UTF-8','EUC-JP') );
				    if ($span == "2"){											// 2段表示のとき
                        $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定 20131119   excelClumnNum=>$excelClumnNum
				        $excelObj->CellMergeReady();	// 結合
				    }
				    $excelObj->SetBorder('THIN' , '000000' , 'outline');
                    $excelObj->SetBackColor( 'FFFFFF' ); //20131119最終日が日曜の時背景色が変わる
                    $excelClumnNum++;
				}
                //繰越公休残日数 start
                if ($obj_hol_remain->transfer_holiday_days_flg == "t" && $show_data_flg == "1") {
                    $wk = $plan_array[$i]["hol_remain"];
                    $hol_bgcolor = "FFFFFF";
                    if ($wk == "") {//公休残がなければ計算
                        //前月がなければ計算せず、背景を赤とする
                        if ($plan_array[$i]["prev_remain"] == "") {
                            $hol_bgcolor = "FF0000";
                        }
                        else {
                            //前月残＋所定公休数−当月実績公休数
                            $wk = $plan_array[$i]["prev_remain"] + $plan_array[$i]["std_hol_cnt"] - $plan_array[$i]["hol_cnt"];
                        }
                    }
                    $wk = str_replace("&nbsp;", "", $wk); // 空白の処理
                    $excelObj->SetValueWithAttrib( $excelColumnName[$excelClumnNum].$excelCellRowPosition , mb_convert_encoding( $wk , 'UTF-8','EUC-JP') );
                    if ($span == "2"){											// 2段表示のとき
                        $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定
                        $excelObj->CellMergeReady();	// 結合
                    }
                    $excelObj->SetBorder('THIN' , '000000' , 'outline');
                    $excelObj->SetBackColor( $hol_bgcolor );

                    $excelClumnNum++;
                }
                //繰越公休残日数 end
            }

		    //休暇日数詳細 * 未対応 *
			if ($hol_dt_flg == "1" && ($show_data_flg == "1" || $show_data_flg == "2")) {
				if ($show_data_flg == "3" || $show_data_flg == "4") {
					$span = "2";
					$excelMergeBEGIN = $excelCellRowPosition ;
					$excelMergeEND = $excelCellRowPosition + 1;
				} else {
					$span = "1";
				}
				$gyo_array_cnt = count($title_hol_array);
				$excelObj->SetCharColorDef( '000000' );
				for ($k=0; $k<count($title_hol_array); $k++) {
					//公休・年休非表示
					if ($title_hol_array[$k]["reason"] == "1" ||
							$title_hol_array[$k]["reason"] == "24") {
						continue;
					}
					$wk = $hol_cnt_array[$i][$gyo_array_cnt + $k];
					$wk = str_replace("&nbsp;", "", $wk ); // 空白の処理
					$excelObj->SetValueWithAttrib( $excelColumnName[$excelClumnNum].$excelCellRowPosition , mb_convert_encoding( $wk , 'UTF-8','EUC-JP') );
					if ( $span == "2" ){											// 2段表示のとき
                        $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelMergeBEGIN.":".$excelColumnName[$excelClumnNum].$excelMergeEND);	// 結合範囲指定 20131119   excelClumnNum=>$excelClumnNum
						$excelObj->CellMergeReady();	// 結合
					}
					$excelObj->SetBorder( 'THIN' , '000000' , 'outline' );
					$excelClumnNum ++ ;
				}
			}

		}

		$excelCellRowPosition ++;

	}
	$excelClumnNum--;
	$excelRowEND = $excelCellRowPosition + 1;

	// 罫線
	$excelObj->SetArea('A6:'.$excelColumnName[$excelCellAreaEND].$excelRowEND);
	$excelObj->SetPosH('CENTER'); // ←中央→
	$excelObj->SetPosV('CENTER'); // ↑↓中央
	$excelObj->SetBorder( 'THIN' , '000000' , 'all' );
	$excelObj->SetArea('G6:'.$excelColumnName[$excelCellAreaEND].$excelRowEND); // 集計行のフォント指定は別途設定するので行指定が被ってもよい
	$excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),10);

//行事予定 20150709
//
	if ($event_flg == "1") {
		$event_end_row = $event_start_row + 3;
        $excelObj->SetArea($excelColumnName[$event_start_col].$event_start_row.":".$excelColumnName[$event_start_col].$event_end_row);	// 結合範囲指定 
		$excelObj->CellMergeReady();	// 結合
		$excelObj->SetWrapText();

	}
	// 縮小表示抜け 20140106
	$excelRowEND = $excelCellRowPosition + 2;
	$excelObj->SetArea($excelColumnName[$excelTitleColPos]."6:".$excelColumnName[$excelCellAreaEND].$excelRowEND);
	$excelObj->SetPosH('CENTER'); // ←中央→
	$excelObj->SetPosV('CENTER'); // ↑↓中央
	$excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),10);
	$excelObj->SetShrinkToFit();


	return $excelCellRowPosition;
}

/*************************************************************************/
//
// 列計
//
/*************************************************************************/
function showTitleRetu(
	$obj,
	$day_cnt,				//日数
	$plan_array,			//勤務シフト情報
	$title_gyo_array,		//行合計タイトル情報
	$gyo_array,				//行合計情報
	$title_retu_array,		//列合計タイトル情報
	$retu_array,			//列合計情報
	$total_gyo_flg="1",		// 2:2段 2以外:1段
	$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
	$hol_dt_flg,			//休暇日数詳細 1:表示 "":非表示
	$title_hol_array,		//休暇タイトル情報
	$hol_cnt_array,			//休暇日数
	$paid_hol_array,	    //年休情報
	$calendar_array,
	$week_array,
	$excelColumnName,		// エクセル列名
	$excelObj,		// ExcelWorkShop
	$excelCellRowPosition,  // 集計行の開始行位置
	$show_data_flg,		// 2012.2.3追加
	$paid_hol_disp_flg,    // 有給休暇残日数表示フラグ
	$week_color,
    //繰越公休残日数 start
    $obj_hol_remain,
    //繰越公休残日数 end
	$wk_group				// チーム表示フラグ等
	){

    $len4_count_value_flg = $obj->is_exist_len4_sign_count_value($wk_group["pattern_id"]);

	$reason_2_array = $obj->get_reason_2();

	$data = "";
	$data_cnt = count($plan_array);

	$excelCellAreaRowBEGIN = $excelCellRowPosition ; // 個人日数欄の空白域処理用

	for ( $i=0; $i<count($title_retu_array); $i++) {

//		$excelTitleColPos=0;
		$excelTitleColPos = 1; //B列:番号（固定位置）の次
		//-------------------------------------------------------------------------------
		// 名称
		//-------------------------------------------------------------------------------
		$wk = $title_retu_array[$i]["name"];

		// 表示フラグを考慮する
		if ($wk_group["team_disp_flg"] =='t'){
			$excelTitleColPos ++;
		}
		if ($wk_group["unit_disp_flg"] =='t'){
			$excelTitleColPos ++;
		}

//		$excelObj->SetArea('C'.$excelCellRowPosition );
		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition );

		$excelObj->SetValueJP( $wk );	//見出し表示
		$excelTitleColPos ++;


		//-------------------------------------------------------------------------------
		// 総合計
		//-------------------------------------------------------------------------------
		$wk_cnt = 0;
		for ($k=1; $k<=$day_cnt; $k++) {
			$wk_cnt += $retu_array[$k][$i];
		}

		// 表示フラグを考慮する
//		if( $show_data_flg == "" ){ // 明細行2段表示 2012.2.3追加
//			$excelTitleColPos = 6; // F列
//		}else{
//			$excelTitleColPos = 5; // E列
//		}
		if( $show_data_flg == "" ) {
			if ($wk_group["job_disp_flg"] == 't') {	// 職種表示を考慮
				$excelTitleColPos ++;
			}
			if ($wk_group["st_disp_flg"] == 't') {	// 役職表示を考慮
				$excelTitleColPos ++;
			}
		} else {
			if (($wk_group["job_disp_flg"] == 't') || ($wk_group["st_disp_flg"] == 't')) {	// 職種役職表示を考慮
				$excelTitleColPos ++;
			}
		}

		if ($wk_group["es_disp_flg"] =='t') {			// 雇用・勤務形態表示を考慮
			$excelTitleColPos ++;
		}

// 何故2行？
		$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition );
		$excelObj->SetValueJP( $wk_cnt );
		//$excelObj->SetArea($excelColumnName[$excelTitleColPos].$excelCellRowPosition );
		//$excelObj->SetValueJP( $wk_cnt );


		//-------------------------------------------------------------------------------
		// 日ごとの合計
		//-------------------------------------------------------------------------------
		$excelObj->SetFontDef(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),10);
		$excelCellAreaBEGIN = $excelTitleColPos + 1; // まとめて罫線を引く開始位置の列名
		for ($k=1; $k<=$day_cnt; $k++) {
			$wk = $retu_array[$k][$i];
			$wk_width = 20;
			for ($m=0; $m<count($reason_2_array); $m++) {
				if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
					$wk_width = 30;
					break;
				}
				if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
					$wk_width = 30;
					break;
				}
			}

			//*曜日で背景色設定
			$wk_back_color = "";
			if ($week_color) {
                if ($calendar_array[$k-1]["type"] == "6") {
                    $wk_back_color = "#C8FFD5"; // 祝日
                }
                else if ($week_array[$k]["name"] == "土" or $calendar_array[$k-1]["type"] == "5"){
				$wk_back_color = "#E0FFFF";
                }
                else if ($week_array[$k]["name"] == "日" or $calendar_array[$k-1]["type"] == "4") {
				$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
				}
			}

			$wk_back_color = str_replace("#", "", $wk_back_color); // #を取る
			if ( $wk_back_color == "" ){
				$wk_back_color = "FFFFFF";
			}
			$excelObj->SetArea( $excelColumnName[$k + $excelTitleColPos].$excelCellRowPosition );
			$excelObj->SetValueJP( $wk );
			$excelObj->SetBackColor( $wk_back_color );
			$excelObj->SetShrinkToFit();
            //0.25、0.75等対応 20140507
            if (!$len4_count_value_flg) {
                $excelObj->SetFormatStyle( '0.0' );
            }
		}
		// 罫線まとめて引く
		$excelObj->SetArea( $excelColumnName[$excelCellAreaBEGIN].$excelCellRowPosition.':'.$excelColumnName[$k + $excelTitleColPos - 1].$excelCellRowPosition ); // $kのカウンタをひとつ戻す
		$excelObj->SetBorder( 'THIN' , '000000' , 'all' );

		$excelClumnNum = $k + $excelTitleColPos;
		$excelObj->SetFontDef(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),12);
		//-------------------------------------------------------------------------------
		// 個人日数
		//-------------------------------------------------------------------------------
		if ($total_print_flg == "1") {
			if (($total_gyo_flg != "2" && $i == 0) || ($total_gyo_flg == "2" && $i <= 1)) {
				$wk_nenkyu_cnt = 0;
				$wk_koukyu_cnt = 0;
				//集計
				for ($k=0; $k<count($title_hol_array); $k++) {
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_hol_array) + $k;
					}

					if ($title_hol_array[$k]["reason"] == "1") {
						for ($m=0; $m<$data_cnt; $m++) {
							$wk_nenkyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						for ($m=0; $m<$data_cnt; $m++) {
							$wk_koukyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
				}

				// 個人日数の列合計
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = 0;
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_gyo_array) + $k;
					}
					for ($m=0; $m<count($gyo_array); $m++) {
						$wk = $wk + $gyo_array[$m][$k2];
					}
					//公休、有休表示
					if ($title_gyo_array[$k]["id"] == "10") {
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
						$excelObj->SetArea( $excelColumnName[$excelClumnNum].$excelCellRowPosition );
						$excelObj->SetValueJP( $wk_koukyu_cnt );
						$excelClumnNum++;
						$excelObj->SetArea( $excelColumnName[$excelClumnNum].$excelCellRowPosition );
						$excelObj->SetValueJP( $wk_nenkyu_cnt );
						$excelClumnNum++;
					}
					$excelObj->SetArea( $excelColumnName[$excelClumnNum].$excelCellRowPosition );
					$excelObj->SetValueJP(  $wk );
					$excelClumnNum++;
				}
				//有給休暇残日数
				if ($paid_hol_disp_flg === "t") {
				    $wk = 0;
				    foreach ($paid_hol_array[$i] as $value) {
				        $wk += $value;
				    }
				    $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
				    $excelObj->SetValueJP($wk);
				    $excelClumnNum++;
				}
                //繰越公休残日数 start
                if ($obj_hol_remain->transfer_holiday_days_flg == "t" && $show_data_flg == "1") {
                    if ($i == 1) {
                        $wk = $obj_hol_remain->hol_remain_total;
                    }
                    else {
                        $wk = "";
                    }

                    $excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
                    $excelObj->SetValueJP($wk);
                    $excelClumnNum++;
                }
                //繰越公休残日数 end

				//休暇詳細
				if ($hol_dt_flg == "1") {
					for ($k=0; $k<count($title_hol_array); $k++) {
						//公休・年休非表示
						if ($title_hol_array[$k]["reason"] == "1" ||
								$title_hol_array[$k]["reason"] == "24") {
							continue;
						}
						$wk = 0;
						if ($i == 0) {
							$k2 = $k;
						} else {
							$k2 = count($title_hol_array) + $k;
						}
						for ($m=0; $m<$data_cnt; $m++) {
							$wk = $wk + $hol_cnt_array[$m][$k2];
						}
						$excelObj->SetValueWithAttrib( $excelColumnName[$excelClumnNum].$excelCellRowPosition , mb_convert_encoding( $wk , 'UTF-8','EUC-JP') );
						$excelObj->SetBorder( 'THIN' , '000000' , 'outline' );
						$excelClumnNum++;
					}
					$data .= "<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n";
					$excelObj->SetArea($excelColumnName[$excelClumnNum].$excelCellRowPosition);
					$excelObj->SetBorder( 'THIN' , '000000' , 'outline' );
					$excelClumnNum++;
				}

			}
		}
		$excelCellRowPosition++;
	}

	$excelCellRowPosition--; // 行カウンタをひとつ戻す

	// 罫線、位置など
	$excelObj->SetArea('A'.$excelCellAreaRowBEGIN.':'.$excelColumnName[$day_cnt+$excelTitleColPo].$excelCellRowPosition);
	$excelObj->SetBorder( 'THIN' , '000000' , 'all' );
	$excelObj->SetPosH('CENTER'); // ←中央→
	$excelObj->SetPosV('CENTER'); // ↑↓中央

	// ブランク(個人日数用の空白域を罫線で囲む処理)
	// HTMLのTDタグは不足分が自動的に空白になるがNativeなexcelセル指定ではセルの範囲指定が必要
	// 左上は$excelColumnName[$day_cnt+5+1].$excelCellAreaRowBEGIN
	// 右下は以下のループで列位置を数える
	$excelCellAreaEND=0;
	for ($k=0; $k<count($title_gyo_array); $k++) {
		if ($title_gyo_array[$k]["id"] == "10") { // PHP5.1.6対応
			$excelCellAreaEND+=2;
		}
		$excelCellAreaEND++;
	}
	if ($paid_hol_disp_flg === "t") {
	    $excelCellAreaEND++;
	}
    //繰越公休残日数 start
    if ($obj_hol_remain->transfer_holiday_days_flg == "t" && $show_data_flg == "1") {
        $excelCellAreaEND++;
    }
    //繰越公休残日数 end
    $excelBorderStartCol = $day_cnt+$excelTitleColPos+1;
	$excelBorderEndCol = $day_cnt+$excelTitleColPos+$excelCellAreaEND;
	$excelObj->SetArea( $excelColumnName[ $excelBorderStartCol ].$excelCellAreaRowBEGIN.':'.$excelColumnName[ $excelBorderEndCol ].$excelCellRowPosition );
	$excelObj->SetBorder( 'THIN' , '000000' , 'outline' );

	return ;
}

?>

