<?
require_once("about_comedix.php");
require_once("fplus_common_class.php");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session=="0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 79, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);


if($mode == "select")
{
// ワークフロー情報取得
$arr_wkfwmst = $obj->get_wkfwmst($wkfw_id);
$wkfw_title = $arr_wkfwmst[0]["wkfw_title"];

$wkfw_title = h($wkfw_title, ENT_QUOTES);
$wkfw_title = str_replace("&#039;", "\'", $wkfw_title);
$wkfw_title = str_replace("&quot;", "\"", $wkfw_title);

?>
<script type="text/javascript">

// 登録済みワークフローチェック
self_flg = false;
obj_wkfw_id = opener.document.getElementById('wkfw_id');
if(obj_wkfw_id)
{
	if(obj_wkfw_id.value == '<? echo($wkfw_id); ?>')
	{
		self_flg = true;
	}
}
// 重複チェック
dpl_flg = false;
sel_precond_wkfw_id = opener.document.getElementById('sel_precond_wkfw_id').value;
arr_sel_precond_wkfw_id = sel_precond_wkfw_id.split(",");
for(i=0; i<arr_sel_precond_wkfw_id.length;i++)
{
	if(arr_sel_precond_wkfw_id[i] == '<? echo($wkfw_id); ?>')
	{
		dpl_flg = true;
	}
}



if(!self_flg && !dpl_flg)
{
	var wkfw_title = '<? echo($wkfw_title); ?>';

	var a = opener.document.createElement('span');
	a.appendChild(opener.document.createTextNode(wkfw_title));

	var inputA = opener.document.createElement('input');
	inputA.type = 'button';
	inputA.name = 'btn_precond_<? echo($wkfw_id); ?>';
	inputA.value = '削除';
	inputA.id = 'btn_precond_<? echo($wkfw_id); ?>';
	if (inputA.onclick === null) {  // IE, Safari
		inputA.setAttribute('onclick', 'delPrecond();');
	}
	if (typeof inputA.onclick == 'string') {  // IE
		inputA.setAttribute('onclick', null);
	}
	if (inputA.onclick == null) {  // IE, FireFox
		inputA.onclick = opener.delPrecond;
	}

	var inputB = opener.document.createElement('input');
	inputB.type = 'hidden';
	inputB.name = 'precond_wkfw_title[]';
	inputB.value = wkfw_title;

	var inputC = opener.document.createElement('input');
	inputC.type = 'hidden';
	inputC.name = 'precond_wkfw_id[]';
	inputC.value = '<? echo($wkfw_id); ?>';

	var p = opener.document.createElement('p');
	p.id = 'p_precond_<? echo($wkfw_id); ?>';
	p.appendChild(a);
	p.appendChild(opener.document.createTextNode(' '));
	p.appendChild(inputA);
	p.appendChild(inputB);
	p.appendChild(inputC);

	var div = opener.document.getElementById('precond_wkfw');
	div.appendChild(p);


	sel_precond_wkfw_id_obj = opener.document.getElementById('sel_precond_wkfw_id');
	if(sel_precond_wkfw_id_obj.value != "")
	{
		sel_precond_wkfw_id_obj.value += ',';
	}
	sel_precond_wkfw_id_obj.value += '<? echo($wkfw_id); ?>';
}
</script>
<?
}





// ワークフローカテゴリ/フォルダ情報取得
$tree_list = $obj->get_workflow_folder_list_for_apply("", $wkfw_counts);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | 報告書様式選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">
<style type="text/css">
.tree-line1 { padding-left: 35px; background: url(img/tree_file_1.gif) 0px no-repeat;}
.tree-line2 { padding-left: 35px; background: url(img/tree_file_2.gif) 0px no-repeat;}
.selectedLabel {font-weight:bold; color:black; padding:1px;}
.ygtvlabel {padding-right:6px;}
</style>


<script type="text/javascript">

var tree = null;
function initPage()
{
<?
if(count($tree_list) > 0)
{
?>
	// ツリービュー初期化
	tree = new YAHOO.widget.TreeView("tree1");

	var root = tree.getRoot();
	<?
    // 申請書様式
	if(count($tree_list) > 0)
	{
		show_wkfw_folder_js($tree_list, $session, $fname, $wkfw_type, $wkfw_id, $wkfw_counts);
	}
	?>
	tree.draw();
<?
}
?>
}

function expandAncestor(node)
{
	if (node.parent) {
		node.parent.expand();
		expandAncestor(node.parent);
	}
}

function expandAll()
{
	if (tree) {
		tree.expandAll();
		document.getElementById('expand').style.display = 'none';
		document.getElementById('collapse').style.display = '';
	}
}

function collapseAll()
{
	if (tree) {
		tree.collapseAll();
		document.getElementById('collapse').style.display = 'none';
		document.getElementById('expand').style.display = '';
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#E6B3D4">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>報告書様式選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
</tr>
<tr height="2">
<td></td>
</tr>
<tr valign="top">
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#FFFBFF">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書様式</font></td>
<td align="right">
<span id="expand"><input type="button" value="全て開く" onclick="expandAll();"></span>
<span id="collapse" style="display:none;"><input type="button" value="全て閉じる" onclick="collapseAll();"></span>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td valign="top" height="400"><div id="tree1"></div></td>
</tr>
</table>
</td>
</tr>
<tr height="2">
<td></td>
</tr>
</table>
</center>
</body>
</html>
<?
// 申請書様式フォルダ表示
function show_wkfw_folder_js($tree_list, $session, $fname, $selected_wkfw_type, $selected_wkfw_id, $wkfw_counts)
{
	$is_safari = (strpos($_SERVER["HTTP_USER_AGENT"], "Safari") !== false);

	// ツリーの画像表示で使用
	$idx = 0;

	foreach ($tree_list as $tree)
	{
		$idx++;
		$type    = $tree["type"];
		$folders = $tree["folders"];

		// カテゴリ出力
		if($type == "category")
		{
			$wkfw_nm = h($tree["wkfw_nm"], ENT_QUOTES);
			$wkfw_type = $tree["wkfw_type"];

			if (is_array($wkfw_counts))
			{
				$wkfw_cnt = "(" . intval($wkfw_counts[$wkfw_type]["-"]) . ")";
			} else {
				$wkfw_cnt = "";
			}

			$node_name = "folder$wkfw_type";
			if ($is_safari) {
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wkfw_nm$wkfw_cnt</font><span style=\"display:none;\"></span>', root, false);\n");
			} else {
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wkfw_nm<font color=\"#444\">$wkfw_cnt</font></font><span style=\"display:none;\"></span>', root, false);\n");
			}
		}
		// フォルダ出力
		else if($type == "folder")
		{
			$tmp_id         = $tree["id"];
			$tmp_cate_id    = $tree["cate"];
			$tmp_parent_id  = $tree["parent_id"];
			$tmp_parent_elm = ($tmp_parent_id == "") ? "folder$tmp_cate_id" : "folder{$tmp_cate_id}_{$tmp_parent_id}";
			$name           = h($tree["name"], ENT_QUOTES);

			if (is_array($wkfw_counts))
			{
				$wkfw_cnt = "(" . intval($wkfw_counts[$tmp_cate_id][$tmp_id]) . ")";
			} else {
				$wkfw_cnt = "";
			}

			$node_name      = "folder{$tmp_cate_id}_{$tmp_id}";
			if ($is_safari) {
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$name$wkfw_cnt</font><span style=\"display:none;\"></span>', $tmp_parent_elm, false);\n");
			} else {
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$name<font color=\"#444\">$wkfw_cnt</font></font><span style=\"display:none;\"></span>', $tmp_parent_elm, false);\n");
			}
		}
		// ファイル出力
		else if($type == "file")
		{
			$wkfw_id        = $tree["id"];
			$wkfw_type      = $tree["cate"];
			$name           = h($tree["name"], ENT_QUOTES);
			$tmp_parent_id  = $tree["parent_id"];

			$tmp_parent_elm = ($tmp_parent_id == "") ? "folder$wkfw_type" : "folder{$wkfw_type}_{$tmp_parent_id}";

			$node_name = "file{$wkfw_type}_{$wkfw_id}";

//			if($selected_wkfw_type == $wkfw_type && $selected_wkfw_id == $wkfw_id)
			if($selected_wkfw_id == $wkfw_id)
			{
				echo("var $node_name = new YAHOO.widget.HTMLNode('<a href=\'$fname?session=$session&mode=select&wkfw_id=$wkfw_id&wkfw_type=$wkfw_type\'><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><font class=\"selectedLabel\">$name</font></font></a><span style=\"display:none;\"></span>', $tmp_parent_elm, false);\n");
				echo("expandAncestor($node_name);\n");
			}
			else
			{
				echo("var $node_name = new YAHOO.widget.HTMLNode('<a href=\'$fname?session=$session&mode=select&wkfw_id=$wkfw_id&wkfw_type=$wkfw_type\'><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$name</font></a><span style=\"display:none;\"></span>', $tmp_parent_elm, false);\n");
			}

			if(count($tree_list) == $idx)
			{
				echo("$node_name.contentStyle = 'tree-line1';\n");
			}
			else
			{
				echo("$node_name.contentStyle = 'tree-line2';\n");
			}

		}
		show_wkfw_folder_js($folders, $session, $fname, $selected_wkfw_type, $selected_wkfw_id, $wkfw_counts);

	}
}



// データベース切断
pg_close($con);
?>
