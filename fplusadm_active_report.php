<?
$fname = $PHP_SELF;
ob_start();
require_once("about_comedix.php");
require_once("show_fplusadm_lib_list.ini");
require_once("fplusadm_common.ini");
require_once("summary_tmplitemrireki.ini"); // 兼用
ob_end_clean();

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$checkauth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$checkauth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}



if($reg_flg == "reg")
{
	//登録処理


	if($report_epinet == NULL)
	{
		$up_epi="false";
	}
	else
	{
		$up_epi="true";
	}
	
	if($report_001 == NULL)
	{
		$up_001="false";
	}
	else
	{
		$up_001="true";
	}
	

	$sql = "update fplus_active_report set";
	$set = array("report_epinet", "report_001");
	$setvalue = array(
		$up_epi,
		$up_001
		);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}



//==============================
//表示する報告書のチェック一覧を取得
//==============================
$item_list = "";
$sql = "select * from fplus_active_report";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_list = pg_fetch_all($sel);


//エピネット報告書の出力チェック
if($item_list[0]["report_epinet"] == "t")
{
	$ck_epinet = "checked";
}
else
{
	$ck_epinet = "";
}


//MRSA等耐性性菌検出報告書の出力チェック
if($item_list[0]["report_001"] == "t")
{
	$ck_001 = "checked";
}
else
{
	$ck_001 = "";
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | 集計報告書選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function registReport()
{

	ret_mes = confirm("表示状態で登録しますが、よろしいですか？");

	if(ret_mes == false)
	{
		return false;
	}

	document.regReport.reg_flg.value = "reg";
	document.regReport.submit();
}
</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}

.list2 {border-collapse:collapse;}
.list2 td {border:#E6B3D4 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#E6B3D4 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="fplusadm_active_report.php?session=<? echo($session); ?>"><b>集計報告書選択</b></a></font></td>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
				</tr>
			</table>


		</td>

			<table style="width:100%; margin-top:4px">
				<tr>
					<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
					<td style="vertical-align:top">

						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
							<tr>
								<td width="3" align="right"><img src="img/menu-fplus-left-on.gif" alt="" width="3" height="22"></td>
							<td width="120" align="center" bgcolor="#FF99CC"><a href="fplusadm_active_report.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr>集計報告書選択</nobr></font></a></td>
							<td width="3" align="left"><img src="img/menu-fplus-right-on.gif" alt="" width="3" height="22"></td>
							<td>&nbsp;</td>
							</tr>
						</table>

						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:6px">
							<tr>
								<td width="30%" valign="top">

									<form name="regReport" action="fplusadm_active_report.php" method="post">
								



										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="1">
														<tr bgcolor="#feeae9">
															<td class="spacing" height="24"><?=$font?>集計報告書選択</font></td>
															<td align="right">
																<input type="button" value="登録" onclick="registReport();">
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td valign="top">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="report_epinet[]" id="report_epinet[]" value="01" <?=$ck_epinet?> >エピネット</font>
												</td>
											</tr>
											<tr>
												<td valign="top">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="report_001[]" id="report_001[]" value="01" <?=$ck_001?> >MRSA等耐性性菌検出報告書</font>
												</td>
											</tr>
										</table>

										<input type="hidden" name="session" value="<? echo($session); ?>">
										<input type="hidden" name="reg_flg" id="reg_flg" value="">
									</form>
								
								</td>
							</tr>
						</table>
				</td>
			</tr>
		</table>
	</tr>
</table>

</body>
</html>


