<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 遅刻早退理由</title>
<?
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");

//-----------------------------------------------------------------------------------------------------
// 新規作成　遅刻早退理由
// timecard_overtime_reason.php より作成
//-----------------------------------------------------------------------------------------------------

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 残業理由一覧を取得
$sql = "select reason_id, reason, ireg_no_deduction_flg, group_name from iregrsn rsn left outer join wktmgrp grp on rsn.tmcd_group_id = grp.group_id";
$cond = "where del_flg = 'f' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
// opt以下のphpを表示するかどうか(申請用追加機能有無判定)
$opt_display_flag = file_exists("opt/flag");
if ($opt_display_flag) {
    // ----- メディアテック Start -----
    // 勤務グループ一覧を取得
    $get_group_sql = "select * from wktmgrp order by group_id";
    $grp_list = select_from_table($con, $get_group_sql, null, $fname);
    if ($grp_list == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    // ----- メディアテック End -----
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteReason() {
	if (confirm('削除します。よろしいですか？')) {
		document.delform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<!-- atdbk_timecard.phpを atdbk_timecard_shift.phpに変更 20130129 -->
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<!--  遅刻早退テンプレート -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
&nbsp;&nbsp;<a href="timecard_overtime_reason.php?session=<? echo($session); ?>"></>残業理由</a>
&nbsp;&nbsp;<a href="timecard_modify_reason.php?session=<? echo($session); ?>">勤務時間修正理由</a>
&nbsp;&nbsp;<a href="timecard_return_reason.php?session=<? echo($session); ?>">呼出勤務理由</a>
&nbsp;&nbsp;<b>遅刻早退理由</b>
&nbsp;&nbsp;</font>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="15"><br>
<!--  遅刻早退テンプレート -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="regform" action="timecard_irregular_reason_insert.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻早退理由</font></td>
<td><input type="text" name="reason" value="<? echo($reason); ?>" size="50" maxlength="100" style="ime-mode:active;">
<label><input type="checkbox" name="ireg_no_deduction_flg" id="ireg_no_deduction_flg" value="t"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻控除しない</font></label>
</td>
</tr>
<?php
if ($opt_display_flag) {
?>
<!-- メディアテック Start -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務グループ</font></td>
<td>
	<select name="wktmgrp">
		<option value="" label="">
    <?php
    while ($row = pg_fetch_array($grp_list)) {
        $id = $row["group_id"];
        $name = h($row["group_name"]);
        echo "<option value=\"$id\">$name</option>";
    }
		?>
	</select>
</td>
</tr>
<!-- メディアテック End -->
<? } ?>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="delform" action="timecard_irregular_reason_delete.php" method="post" style="border-top:#5279a5 solid 1px;padding-top:5px;">
<?
if (pg_num_rows($sel) > 0) {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\"><input type=\"button\" value=\"削除\" onclick=\"deleteReason();\"></td>\n");
	echo("</tr>\n");
	echo("</table>\n");

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
	echo("<td colspan=1 ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">遅刻早退理由</font></td>\n");
	if ($opt_display_flag) {

        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務グループ</font></td>\n"); // メディアテック
    }
    echo("</tr>\n");
	while ($row = pg_fetch_array($sel)) {
		$tmp_reason_id = $row["reason_id"];
		$tmp_reason = trim($row["reason"]);
		$tmp_ireg_no_deduction_flg = $row["ireg_no_deduction_flg"];

		echo("<tr>\n");
		echo("<td width=\"30\" align=\"center\"><input type=\"checkbox\" name=\"reason_ids[]\" value=\"$tmp_reason_id\"></td>\n");
		echo("<td><a href=\"timecard_irregular_reason_update.php?session=$session&reason_id=$tmp_reason_id&ireg_no_deduction_flg=$tmp_ireg_no_deduction_flg\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_reason</a>");

		//遅刻控除しない場合
		if ($tmp_ireg_no_deduction_flg == "t") {
			echo("　（遅刻控除しない）");
		}
		echo("</font>");
		echo("</td>\n");
        if ($opt_display_flag) {

            // ----- メディアテック Start -----
            echo "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
            echo h($row["group_name"]);
            echo "</font></td>";
            // ----- メディアテック End -----
        }
        echo("</tr>\n");
	}
	echo("</table>\n");
} else {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">理由は登録されていません。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

echo ("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">※この設定は決裁申請の遅刻早退届からのみ有効です。</font><br>");
echo ("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">※常勤の場合の遅刻は３０分単位で遅刻控除として集計されます。</font>\n");
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
