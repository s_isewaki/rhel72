<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// お知らせ参照情報の登録
$sql = "insert into inci_news_ref (news_id, emp_id, ref_time) values (";
$content = array($news_id, $emp_id, date("YmdHi"));
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 元画面を再表示
echo("<script type=\"text/javascript\">location.href = 'hiyari_news_detail_from_top.php?session=$session&news_id=$news_id';</script>");
?>
