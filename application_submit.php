<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="application_menu.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?php
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}


//for ($i = 1; $i <= $approve_num; $i++) {
//	$varname = "approve_name$i";
//	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
//}

?>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="wkfw_id" value="<?php echo($wkfw_id); ?>">
<input type="hidden" name="wkfw_type" value="<?php echo($wkfw_type); ?>">
<input type="hidden" name="approve_num" value="<?php echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<?php echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="apply_title_disp_flg" value="<?=$apply_title_disp_flg?>">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">
<input type="hidden" name="wkfwfile_history_no" value="<?=$wkfwfile_history_no?>">
<input type="hidden" name="council_emp_id" id="council_emp_id" value="<?=$council_emp_id?>">
<input type="hidden" name="council_emp_nm" id="council_emp_nm" value="<?=$council_emp_nm?>">
</form>
<?php

$fname=$PHP_SELF;

require_once("./conf/sql.inf");
require_once("./about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("Cmx.php");
require_once("aclg_set.php");

$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (mb_strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('{$title_label}が長すぎます。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 添付ファイルの確認
if (!is_dir("apply")) {
	mkdir("apply", 0755);
}
if (!is_dir("apply/tmp")) {
	mkdir("apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
//		echo("<script language=\"javascript\">document.items.submit();</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$obj = new application_workflow_common_class($con, $fname);

// 申請の場合、未指定の承認者を許可する
if ($draft != "on") {
	$tmp_arr_apv = array();
	$tmp_apv_order = 0;
	$tmp_pre_apv_order = 0;
	for ($i = 1; $i <= $approve_num; $i++) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";
		$tmp_updateable_var = "updateable$i";

		$tmp_org_apv_order = $$tmp_apv_order_var;
		$tmp_org_pst_approve_num_var = "pst_approve_num$tmp_org_apv_order";
		$tmp_radio_emp_id_var = "radio_emp_id$tmp_org_apv_order";

		if ($$tmp_regist_emp_id_var != "" || ($$tmp_org_pst_approve_num_var != "" && $$tmp_radio_emp_id_var != "")) {
			if ($$tmp_apv_order_var != $tmp_pre_apv_order) {
				$tmp_apv_order++;
				$tmp_apv_sub_order = 1;
				$tmp_pre_apv_order = $$tmp_apv_order_var;
			} else {
				$tmp_apv_sub_order += 1;
			}

			if ($tmp_apv_sub_order == 2) {
				$tmp_arr_apv[count($tmp_arr_apv) - 1]["apv_sub_order"] = 1;
			}

			$tmp_arr_apv[] = array(
				"regist_emp_id" => $$tmp_regist_emp_id_var,
				"st_div" => $$tmp_st_div_var,
				"parent_pjt_id" => $$tmp_parent_pjt_id_var,
				"child_pjt_id" => $$tmp_child_pjt_id_var,
				"apv_order" => $tmp_apv_order,
				"apv_sub_order" => (($tmp_apv_sub_order == 1) ? null : $tmp_apv_sub_order),
				"multi_apv_flg" => $$tmp_multi_apv_flg_var,
				"next_notice_div" => $$tmp_next_notice_div_var,
				"updateable" => $$tmp_updateable_var,
				"org_apv_order" => $tmp_org_apv_order
			);
		}

		if ($$tmp_org_pst_approve_num_var != "" && $$tmp_radio_emp_id_var != "" && $tmp_apv_order != $tmp_org_apv_order) {
			$tmp_pst_approve_num_var = "pst_approve_num$tmp_apv_order";
			$$tmp_pst_approve_num_var = $$tmp_org_pst_approve_num_var;

			$base_varnames = array("pst_emp_id", "pst_st_div", "pst_parent_pjt_id", "pst_child_pjt_id", "pst_class_id", "pst_atrb_id", "pst_dept_id", "pst_room_id", "pst_st_id");
			for ($j = 1; $j <= $$tmp_pst_approve_num_var; $j++) {
				foreach ($base_varnames as $base_varname) {
					$org_varname = "$base_varname{$tmp_org_apv_order}_{$j}";
					$new_varname = "$base_varname{$tmp_apv_order}_{$j}";
					$$new_varname = $$org_varname;
					unset($$org_varname);
				}
			}

			unset($$tmp_org_pst_approve_num_var);
		}

		unset($$tmp_regist_emp_id_var);
		unset($$tmp_st_div_var);
		unset($$tmp_parent_pjt_id_var);
		unset($$tmp_child_pjt_id_var);
		unset($$tmp_apv_order_var);
		unset($$tmp_apv_sub_order_var);
		unset($$tmp_multi_apv_flg_var);
		unset($$tmp_next_notice_div_var);
		unset($$tmp_updateable_var);
	}

	$approve_num = count($tmp_arr_apv);
	$i = 1;
	foreach ($tmp_arr_apv as $tmp_apv) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";
		$tmp_updateable_var = "updateable$i";
		$tmp_org_apv_order_var = "org_apv_order$i";

		$$tmp_regist_emp_id_var = $tmp_apv["regist_emp_id"];
		$$tmp_st_div_var = $tmp_apv["st_div"];
		$$tmp_parent_pjt_id_var = $tmp_apv["parent_pjt_id"];
		$$tmp_child_pjt_id_var = $tmp_apv["child_pjt_id"];
		$$tmp_apv_order_var = $tmp_apv["apv_order"];
		$$tmp_apv_sub_order_var = $tmp_apv["apv_sub_order"];
		$$tmp_multi_apv_flg_var = $tmp_apv["multi_apv_flg"];
		$$tmp_next_notice_div_var = $tmp_apv["next_notice_div"];
		$$tmp_updateable_var = $tmp_apv["updateable"];
		$$tmp_org_apv_order_var = $tmp_apv["org_apv_order"];

		$i++;
	}
	unset($tmp_arr_apv);
}

// トランザクション開始
pg_query($con, "begin");

// 職員情報取得
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];
$emp_class     = $arr_empmst[0]["emp_class"];
$emp_attribute = $arr_empmst[0]["emp_attribute"];
$emp_dept      = $arr_empmst[0]["emp_dept"];
$emp_room      = $arr_empmst[0]["emp_room"];

// 新規申請(apply_id)採番
$cond = "";
$apply_max_sel = select_from_table($con,$SQL203,$cond,$fname);
$max = pg_result($apply_max_sel,0,"max");
if($max == ""){
	$apply_id = "1";
}else{
	$apply_id = $max + 1;
}

// 新規申請番号(apply_no)採番
$apply_no = null;
if($draft != "on")
{
	$date = date("Ymd");
	$year = substr($date, 0, 4);
	$md   = substr($date, 4, 4);

	if($md >= "0101" and $md <= "0331")
	{
		$year = $year - 1;
	}
	$max_cnt = $obj->get_apply_cnt_per_year($year);
	$apply_no = $max_cnt + 1;
}

// テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "workflow/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}

	// ワークフロー情報取得
//20070918
//	$sel_wkfwmst = search_wkfwmst($con, $fname, $wkfw_id);
//	$wkfw_content = pg_fetch_result($sel_wkfwmst, 0, "wkfw_content");

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、申請してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、申請してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );


}



// 申請登録
$arr = array(
				"apply_id" => $apply_id,
				"wkfw_id" => $wkfw_id,
				"apply_content" => $content,
				"emp_id" => $emp_id,
				"apply_stat" => "0",
				"apply_date" => date("YmdHi"),
				"delete_flg" => "f",
				"apply_title" => $apply_title,
				"re_apply_id" => null,
				"apv_fix_show_flg" => "t",
				"apv_bak_show_flg" => "t",
				"emp_class" => $emp_class,
				"emp_attribute" => $emp_attribute,
				"emp_dept" => $emp_dept,
				"apv_ng_show_flg" => "t",
				"emp_room" => $emp_room,
				"draft_flg" => ($draft == "on") ? "t" : "f",
				"wkfw_appr" => $wkfw_appr,
				"wkfw_content_type" => $wkfw_content_type,
				"apply_title_disp_flg" => $apply_title_disp_flg,
				"apply_no" => $apply_no,
				"notice_sel_flg" => $rslt_ntc_div2_flg,
				"wkfw_history_no" => ($wkfw_history_no == "") ? null : $wkfw_history_no,
				"wkfwfile_history_no" => ($wkfwfile_history_no == "") ? null : $wkfwfile_history_no
			 );

$obj->regist_apply($arr);
//----- メディアテック Start -----
if (phpversion() >= '5.1') {
    $template_type = $_POST["kintai_template_type"];
    if (!empty($template_type) && $draft != "on") {
        // MDB初期化
        require_once 'kintai/common/mdb_wrapper.php';
        require_once 'kintai/common/mdb.php';
        MDBWrapper::init();
        $mdb = new MDB();
        $mdb->beginTransaction();

        $class = null;
        switch ($template_type) {
            case "ovtm": // 時間外勤務命令の場合
                if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                    $class = WORKFLOW_OVTM_CLASS;
                }
                break;
            case "hol": // 休暇申請の場合
                // 総合届け使用フラグ
                $general_flag = file_exists("opt/general_flag");
                if ($general_flag) {
                    if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                        $class = WORKFLOW_HOL_CLASS;
                    }
                }
                //休暇申請
                else {
                    if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                        $class = WORKFLOW_HOL2_CLASS;
                    }
                }
                break;
            case "travel": // 旅行命令
                if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                    $class = WORKFLOW_TRVL_CLASS;
                }
                break;
            case "late": // 遅刻早退の場合
            	if ($_POST["late_early_type"] === "new") {
            		if (module_check(WORKFLOW_LATE2_MODULE, WORKFLOW_LATE2_CLASS)) {
            			$class = WORKFLOW_LATE2_CLASS;
            		}
            	}else{
	                if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
	                    $class = WORKFLOW_LATE_CLASS;
	                }
            	}
                break;
        }
        if (!empty($class)) {
            $module = new $class;
            $module->regist_apply($apply_id);
        }
    }
}
// モジュール・クラスの存在チェック
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}

//----- メディアテック End   -----

// 下書きでない場合、メール送信準備
$to_addresses = array();
if ($draft != "on") {
	$wkfw_send_mail_flg = get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
} else {
	$wkfw_send_mail_flg = "f";
}

// 承認登録
$arr_wkfwapvmng = $obj->get_wkfwapvmng($wkfw_id);
for($i=1; $i<=$approve_num; $i++)
{
	$varname = "regist_emp_id$i";
	$regist_emp_id = ($$varname == "") ? null : $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "apv_order$i";
	$apv_order = ($$varname == "") ? null : $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = ($$varname == "") ? null : $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

	$varname = "updateable$i";
	$updateable = ($$varname == "") ? null : $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;


	// 所属、役職も登録する
	$infos = get_empmst($con, $regist_emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];
	$emp_email     = $infos[31];

	//// 役職指定なら部署と役職を上書き
	$varname = "org_apv_order$i";
	$org_apv_order = ($$varname == "") ? $apv_order : $$varname;
	foreach ($arr_wkfwapvmng as $apvmng) {
		if ($apvmng["apv_order"] != $org_apv_order) {
			continue;
		}

		$matched = $obj->get_matched_post_sect($apvmng, $emp_id, $regist_emp_id);
		if ($matched) {
			$emp_class     = $matched["class"];
			$emp_attribute = $matched["attribute"];
			$emp_dept      = $matched["dept"];
			$emp_room      = $matched["room"];
			$emp_st        = $matched["st"];
			break;
		}
	}

	$arr = array(
				"wkfw_id" => $wkfw_id,
				"apply_id" => $apply_id,
				"apv_order" => $apv_order,
				"wkfw_apv_order" => $org_apv_order,
				"emp_id" => $regist_emp_id,
				"apv_stat" => "0",
				"apv_date" => "",
				"delete_flg" => "f",
				"apv_comment" => "",
				"st_div" => ($st_div == "") ? null : $st_div,
				"deci_flg" => "t",
				"emp_class" => $emp_class,
				"emp_attribute" => $emp_attribute,
				"emp_dept" => $emp_dept,
				"emp_st" => $emp_st,
				"apv_fix_show_flg" => "t",
				"emp_room" => $emp_room,
				"apv_sub_order" => $apv_sub_order,
				"multi_apv_flg" => $multi_apv_flg,
				"next_notice_div" => $next_notice_div,
				"updateable" => $updateable,
				"parent_pjt_id" => $parent_pjt_id,
				"child_pjt_id" => $child_pjt_id,
				"other_apv_flg" =>  "f"
				);

    $obj->regist_applyapv($arr);

	if (must_send_mail($wkfw_send_mail_flg, $emp_email, $wkfw_appr, $apv_order)) {
		$to_addresses[] = $emp_email;
	}
}

// 承認者候補登録
for($i=1; $i <= $approve_num; $i++)
{
	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "pst_approve_num$apv_order";
	$pst_approve_num = $$varname;

	for($j=1; $j<=$pst_approve_num; $j++)
	{
		$varname = "pst_emp_id{$apv_order}_{$j}";
		$pst_emp_id = $$varname;

		$varname = "pst_st_div{$apv_order}_{$j}";
		$pst_st_div = $$varname;

		$varname = "pst_parent_pjt_id{$apv_order}_{$j}";
		$pst_parent_pjt_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_child_pjt_id{$apv_order}_{$j}";
		$pst_child_pjt_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_class_id{$apv_order}_{$j}";
		$pst_class_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_atrb_id{$apv_order}_{$j}";
		$pst_atrb_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_dept_id{$apv_order}_{$j}";
		$pst_dept_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_room_id{$apv_order}_{$j}";
		$pst_room_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_st_id{$apv_order}_{$j}";
		$pst_st_id = ($$varname == "") ? null : $$varname;

		$obj->regist_applyapvemp($apply_id, $apv_order, $j, $pst_emp_id, $pst_st_div, $pst_parent_pjt_id, $pst_child_pjt_id, $pst_class_id, $pst_atrb_id, $pst_dept_id, $pst_room_id, $pst_st_id);
	}
}



// 添付ファイル登録
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$no++;
}


// 非同期・同期受信登録
if($wkfw_appr == "2")
{
	$previous_apv_order = "";
	$arr_apv = array();
	$arr_apv_sub_order = array();
	for($i=1; $i <= $approve_num; $i++)
	{
		$varname = "apv_order$i";
		$apv_order = $$varname;

		$varname = "apv_sub_order$i";
		$apv_sub_order = ($$varname == "") ? null : $$varname;

		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;

		if($previous_apv_order != $apv_order)
		{
			$arr_apv_sub_order = array();
		}
		$arr_apv_sub_order[] = $apv_sub_order;
		$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);

		$previous_apv_order = $apv_order;
	}

	$arr_send_apv_sub_order = array();
	foreach($arr_apv as $apv_order => $apv_info)
	{
		$multi_apv_flg = $apv_info["multi_apv_flg"];
		$next_notice_div = $apv_info["next_notice_div"];
		$arr_apv_sub_order = $apv_info["apv_sub_order"];

		foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
		{
			// 非同期通知
			if($arr_send_apv_sub != null)
			{
				foreach($arr_send_apv_sub as $send_apv_sub_order)
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						$obj->regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order);
					}
				}
			}
			// 同期通知
			else
			{
				foreach($arr_apv_sub_order as $recv_apv_sub_order)
				{
					$obj->regist_applyasyncrecv($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order);
				}
			}

		}
		$arr_send_apv_sub_order = array();

		// 非同期通知の場合
		if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
		}
		// 同期通知または権限並列通知の場合
		else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = null;
		}
	}
}

// 申請結果通知登録
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

// 前提とする申請書(申請用)登録
for($i=0; $i<$precond_num; $i++)
{
	$order = $i + 1;
	$varname = "precond_wkfw_id$order";
	$precond_wkfw_id = $$varname;

	$varname = "precond_apply_id$order";
	$precond_apply_id = $$varname;

	$obj->regist_applyprecond($apply_id, $precond_wkfw_id, $order, $precond_apply_id);
}

// メール送信に必要な情報を取得
if (count($to_addresses) > 0) {
	$emp_detail = $obj->get_empmst_detail($emp_id);
	$emp_nm = format_emp_nm($emp_detail[0]);
	$emp_mail_header = format_emp_mail_header($emp_detail[0]);
	$emp_mail = format_emp_mail($emp_detail[0]);
	$emp_pos = format_emp_pos($emp_detail[0]);

	$wkfwmst = $obj->get_wkfwmst($wkfw_id);
	$approve_label = ($wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
}

// 合議者リスト
if ( $council_emp_id != "" ){
	$arr_council_emp_id = split(",", $council_emp_id);
	for($i=0; $i<count($arr_council_emp_id); $i++){
		$obj->regist_apply_council($apply_id, $arr_council_emp_id[$i]);
	}
}
//----- メディアテック Start -----
if (phpversion() >= '5.1') {
    if (!empty($template_type) && $draft != "on") {
        // 申請日を更新
        require_once 'kintai/common/workflow_common_util.php';
        WorkflowCommonUtil::apply_date_update($con, $apply_id);

        $mdb->endTransaction();
    }
}
//----- メディアテック End   -----
// トランザクションをコミット
pg_query($con, "commit");

// メール送信
if (count($to_addresses) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] {$approve_label}依頼のお知らせ";
	$mail_content = format_mail_content($con, $wkfw_content_type, $content, $fname);
	$mail_separator = str_repeat("-", 60) . "\n";
	$additional_headers = "From: $emp_mail_header";
	$additional_parameter = "-f$emp_mail";

	foreach ($to_addresses as $to_address) {
		$mail_body = "以下の{$approve_label}依頼がありました。\n\n";
		$mail_body .= "申請者：{$emp_nm}\n";
		$mail_body .= "所属：{$emp_pos}\n";
		$mail_body .= "{$title_label}：{$apply_title}\n";
		if ($mail_content != "") {
			$mail_body .= $mail_separator;
			$mail_body .= "{$mail_content}\n";
		}

		mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
	}
}

// データベース接続を閉じる
pg_close($con);

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}


if($draft == "on")
{
	echo("<script type=\"text/javascript\">location.href='application_menu.php?session=$session&apply_id=$apply_id';</script>");
}
else
{
	echo("<script type=\"text/javascript\">location.href='application_apply_list.php?session=$session&mode=search&category=$wkfw_type&workflow=$wkfw_id';</script>");
}

?>
</body>
