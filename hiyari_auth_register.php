<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 更新処理
if($mode == 'update') {

	// トランザクションの開始
	pg_query($con, "begin transaction");

	$arr_auth = split(",", $auths);
	for($i=0; $i<count($arr_auth); $i++) {

		$nm = $arr_auth[$i]."_NM";
		$s_nm = $arr_auth[$i]."_S_NM";
		$rank = $arr_auth[$i]."_RANK";
		$explanation = $arr_auth[$i]."_EXPLAIN";
		$use_flg = $arr_auth[$i]."_USE_FLG";

		update_inci_auth_mst($con, $fname, $arr_auth[$i], $$nm, $$s_nm, $$rank, $$explanation, $$use_flg);
	}
	
	set_sm_disp_flg($con,$fname,$sm_disp_flg);
	
	// トランザクションをコミット
	pg_query($con, "commit");

}

if($mode == 'rank_change')
{
	//ポストバックデータを表示
	$arr_inci = array();
	$arr_auth = split(",", $auths);
	for($i1=0; $i1<count($arr_auth); $i1++) {
		for($i2=0; $i2<count($arr_auth); $i2++) {
	
			$nm = $arr_auth[$i2]."_NM";
			$s_nm = $arr_auth[$i2]."_S_NM";
			$rank = $arr_auth[$i2]."_RANK";
			$explanation = $arr_auth[$i2]."_EXPLAIN";
			$use_flg = $arr_auth[$i2]."_USE_FLG";

			if($$rank == $i1)
			{
				$arr_inci[$arr_auth[$i2]] = array('auth_name' => $$nm, 'auth_short_name' => $$s_nm, 'auth_rank' => $$rank, 'explanation' => $$explanation, 'use_flg' => $$use_flg);
			}
		}
	}
}
else if($mode == 'back')
{

	//インシデント権限マスタデフォルト取得
	$arr_inci = search_inci_auth_mst_default($con, $fname);

	$tmp_inci_info = array();
	foreach($arr_inci as $key => $val)
	{
		$input_name = $key."_USE_FLG";	
		$use_flg = $$input_name;

		$tmp_arr = array("auth_name" => $val["auth_name"], "auth_short_name" => $val["auth_short_name"], "auth_rank" => $val["auth_rank"], "explanation" => $val["explanation"], "use_flg" => $use_flg);
		$tmp_inci_info[$key] = $tmp_arr;

	}
	$arr_inci = $tmp_inci_info;
}
else
{
	//インシデント権限マスタ取得
	$arr_inci = search_inci_auth_mst($con, $fname);
	//SM表示フラグ取得
	$sm_disp_flg = get_sm_disp_flg($fname, $con);
}


//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | 担当者名称設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--

function initPage() {

	val = '';
<?
// キー取得
$arr_key = array_keys($arr_inci);
for($i=0; $i<count($arr_key); $i++) {
?>
	if(val != "") {
		val = val + ',';
	}
	val = val + '<?=$arr_key[$i]?>';
	
<?
}
?>
	document.mainform.elements['auths'].value = val;
}

function update() {

	auth = document.mainform.elements['auths'].value;
	auth_keys = auth.split(",");

	if(chkNon(auth_keys) && chkDupl(auth_keys)) {
//		document.mainform.action="hiyari_auth_register.php?session=<?=$session?>&mode=update";
		mainform.mode.value = "update";
		document.mainform.submit();
	}

}

function back_auth_info() {
	if(confirm('「担当者名称」・「略称」・「職務」・「表示順番」の設定値を初期値に戻します。よろしいですか？'))
	{
		mainform.mode.value = "back";
		document.mainform.submit();
	}
}

function chkNon(auth_keys) {

	chk_name_flg = false;
	chk_s_name_flg = false;

	// 未入力チェック
	non_chk_msg = '';
	for(i=0; i<auth_keys.length; i++) {
		name = auth_keys[i] + '_NM';
		s_name = auth_keys[i] + '_S_NM';

		name_val = document.mainform.elements[name].value
		s_name_val = document.mainform.elements[s_name].value

		if(name_val == "") {
			chk_name_flg = true;
		}

		if(s_name_val == "") {
			chk_s_name_flg = true;
		}
	}

	if(chk_name_flg) {
		non_chk_msg = '「担当者名称」を入力してください。';
	}
	if(chk_s_name_flg) {
		if(non_chk_msg != "") {
			non_chk_msg = non_chk_msg + "\n";
		}
		non_chk_msg = non_chk_msg + '「略称」を入力してください。';
	}

	if(non_chk_msg != "") {
		alert(non_chk_msg);
		return false;
	}
	return true;
} 

function chkDupl(auth_keys) {

	chk_name_flg = false;
	chk_s_name_flg = false;

	// 重複チェック
	dupl_chk_msg = '';
	for(i=0; i<auth_keys.length; i++) {
		name_i = auth_keys[i] + '_NM';
		s_name_i = auth_keys[i] + '_S_NM';

		name_val_i = document.mainform.elements[name_i].value;
		s_name_val_i = document.mainform.elements[s_name_i].value;

		for(j=i+1; j<auth_keys.length; j++) {

			name_j = auth_keys[j] + '_NM';
			s_name_j = auth_keys[j] + '_S_NM';

			name_val_j = document.mainform.elements[name_j].value;
			s_name_val_j = document.mainform.elements[s_name_j].value;

			if(name_val_i == name_val_j) {
				chk_name_flg = true;
			}

			if(s_name_val_i == s_name_val_j) {
				chk_s_name_flg = true;
			}
		}
	}

	if(chk_name_flg) {
		dupl_chk_msg = '「担当者名称」の入力内容が重複しています。';
	}
	if(chk_s_name_flg) {
		if(dupl_chk_msg != "") {
			dupl_chk_msg = dupl_chk_msg + "\n";
		}
		dupl_chk_msg = dupl_chk_msg + '「略称」の入力内容が重複しています。';
	}

	if(dupl_chk_msg != "") {
		alert(dupl_chk_msg);
		return false;
	}
	return true;
}



//注意:add_valueは1or-1にのみ対応。
function change_lank(rank_obj_name,add_value)
{
	var rank_obj = document.getElementsByName(rank_obj_name)[0];
	var target_value = rank_obj.value;
	var change_flg = false;
	
	var all_inputs = document.getElementsByTagName("input");
	for(var i = 0; i < all_inputs.length; i++)
	{
		var input_obj = all_inputs[i];
 		if(input_obj.name != "" && input_obj.name.indexOf("_RANK") != -1)
 		{
			if((input_obj.value-0) == (target_value-0) + (add_value-0) )
			{
				input_obj.value = target_value;
				change_flg = true;
				break;
			}
 		}
	}
	
	if(!change_flg)
	{
		return;
	}
	
	rank_obj.value = (target_value-0) + (add_value-0);
	
	mainform.mode.value = "rank_change";
	mainform.submit();
}




// -->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<form name="mainform" action="hiyari_auth_register.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- ここから -->

<table width="100%" border="0" cellspacing="0" cellpadding="3"  class="list">
<tr>
<td align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者名称</font></td>
<td width="70" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">略称</font></td>
<td align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職務</font></td>
<td width="110" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順番</font></td>
<td width="130" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></td>
</tr>

<?
$i = 0;
foreach($arr_inci as $auth => $arr) {
?>
<tr>
<td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="<?=$auth?>_NM" size="40" maxlength="15" value="<?=h($arr['auth_name'])?>"></font></td>
<td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="<?=$auth?>_S_NM" size="10" maxlength="3" value="<?=h($arr['auth_short_name'])?>"></font></td>
<td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="<?=$auth?>_EXPLAIN" size="45" value="<?=h($arr['explanation'])?>"></font></td>
<td bgcolor="#FFFFFF" align="center" nowrap>
	<input type="hidden" name="<?=$auth?>_RANK" value="<?=h($arr['auth_rank'])?>">
	<?
	if($i != 0)
	{
	?>
		<img src="img/up.gif" onclick="change_lank('<?=$auth?>_RANK',-1);" style="cursor: pointer;">
	<?
	}
	else
	{
	?>
		<img src="img/spacer.gif" width="17" height="17" alt="">
	<?
	}

	if($i != count($arr_inci)-1)
	{
	?>
		<img src="img/down.gif" onclick="change_lank('<?=$auth?>_RANK',1);" style="cursor: pointer;">
	<?
	}
	else
	{
	?>
		<img src="img/spacer.gif" width="17" height="17" alt="">
	<?
	}
	?>
</td>
<td bgcolor="#FFFFFF" align="center" nowrap>
<?if($auth == "SM") {?>
<input type="hidden" name="<?=$auth?>_USE_FLG" value="t">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="sm_disp_flg" value="t" <?if($sm_disp_flg == 't'){echo(" checked");}?>>する<input type="radio" name="sm_disp_flg" value="f" <?if($sm_disp_flg == 'f'){echo(" checked");}?>>しない</font>
<?} elseif($auth == "RM") {?>
<input type="hidden" name="<?=$auth?>_USE_FLG" value="t">
<?} else {?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<?=$auth?>_USE_FLG" value="t" <?if($arr['use_flg'] == 't'){echo(" checked");}?>>する<input type="radio" name="<?=$auth?>_USE_FLG" value="f" <?if($arr['use_flg'] == 'f'){echo(" checked");}?>>しない</font>
<?}?>
</td>
</tr>
<?
$i++;
}
?>
</table>
<input type="hidden" name="auths" value="">

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="left">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">
&lt;注意事項&gt;<BR>
・各担当者の職務と権限は、初期値に基づいて想定されておりますので、名称変更の際は注意してください。<BR>
・<?=$arr_inci['SM']['auth_name']?>の表示設定(表示しない)については他と異なり、以下の表示変更のみとなります。<BR>
&nbsp;&nbsp;&nbsp;&nbsp;１．お知らせ画面の「<?=$arr_inci['SM']['auth_name']?>からのお知らせ」が「お知らせ」に変わります。<BR>
&nbsp;&nbsp;&nbsp;&nbsp;２．受送信トレイ、及び報告ファイルの進捗表示にて<?=$arr_inci['SM']['auth_short_name']?>の項が非表示となります。
</font>
</td>
<td align="right" valign="top">
<input type="button" value="初期値に戻す" onclick="back_auth_info();">
<input type="button" value="更新" onclick="update();">
</td>
</tr>
</table>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>


</td>
</tr>
</table>
</form>
</body>
</html>

<?
//==============================
//インシデント権限マスタ取得
//==============================
function search_inci_auth_mst($con, $fname) {

	$arr_icni = get_inci_mst($con, $fname);
	return $arr_icni;
}

//==============================
//インシデント権限マスタ更新
//==============================
function update_inci_auth_mst($con, $fname, $auth, $auth_name, $auth_short_name, $auth_rank, $explanation, $use_flg) {

	$sql = "update inci_auth_mst set";
	$set = array("auth_name", "auth_short_name", "auth_rank", "explanation", "use_flg");
	$setvalue = array(pg_escape_string($auth_name), pg_escape_string($auth_short_name), pg_escape_string($auth_rank), pg_escape_string($explanation), $use_flg);
	$cond = "where auth = '$auth'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);

	if ($upd == 0) {
        pg_exec($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//==============================
//権限マスタデフォルト取得
//==============================
function search_inci_auth_mst_default($con, $fname) {

	$arr = array();

	$sql  = "select * from inci_auth_mst_default";
	$cond = "order by auth_rank";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
    {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel))
	{
		$tmp_auth = $row["auth"];
		$tmp_auth_nm = $row["auth_name"];
		$tmp_auth_s_nm = $row["auth_short_name"];
		$tmp_auth_rank = $row["auth_rank"];	
		$tmp_explanation = $row["explanation"];

		$tmp_arr = array("auth_name" => $tmp_auth_nm, "auth_short_name" => $tmp_auth_s_nm, "auth_rank" => $tmp_auth_rank, "explanation" => $tmp_explanation);
		$arr[$tmp_auth] = $tmp_arr;
	}

	return $arr;
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);

?>
