<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | メニュー一括設定</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// メニューグループの一覧を配列に格納する
$sql = "select group_id, group_nm from menugroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu_groups = array();
while ($row = pg_fetch_array($sel)) {
	$menu_groups[$row["group_id"]] = $row["group_nm"];
}

// デフォルトの設定内容を「メニューグループで設定」にする
if ($action_mode == "") {
	$action_mode = "1";
}

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
	$lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}

// デフォルト値の設定
if ($back != "t") {
	$sidemenu_show_flg = "t";
	$sidemenu_position = "1";
	$menu_ids = array();
	$menu_ids[0] = "15";  // オプション設定
	$menu_ids[1] = "98";  // open/close
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
function reverseAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = !box.options[i].selected;
	}
}

function setRowsDisplay() {
	var group_mode = document.employee.action_mode[0].checked;
	$('tr.for_group').css('display', (group_mode) ? '' : 'none');
	$('tr.for_user').css('display', (group_mode) ? 'none' : '');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setRowsDisplay();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>">権限</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_display_bulk_setting.php?session=<? echo($session); ?>">表示</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー</font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_condition_bulk_setting.php?session=<? echo($session); ?>">勤務条件</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<? if ($result == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示を一括設定しました。</font></td>
<? } else { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※設定内容は登録済みの職員にのみ反映されます。職員の追加登録時には再度一括設定して下さい。職員自身が「オプション設定」で変更することも可能です</font></td>
<? } ?>
</tr>
</table>
<form name="employee" action="employee_menu_bulk_setting_confirm.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="19%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td width="35%">
<select name="tgt_job[]" size="6" multiple>
<? show_options($sel_job, "job_id", "job_nm", $tgt_job); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_job[]']);">
</td>
<td width="11%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td width="35%">
<select name="tgt_st[]" size="6" multiple>
<? show_options($sel_st, "st_id", "st_nm", $tgt_st); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_st[]']);">
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定内容</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="action_mode" value="1" onclick="setRowsDisplay();"<? if ($action_mode == "1") {echo(" checked");} ?>>メニューグループで設定
<input type="radio" name="action_mode" value="2" onclick="setRowsDisplay();"<? if ($action_mode == "2") {echo(" checked");} ?>>個別に設定
</font></td>
</tr>
<tr class="for_group" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="menu_group_id">
<option value="">　　　　　</option>
<?
foreach ($menu_groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $menu_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select>
</font></td>
</tr>
<tr class="for_user" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ヘッダーメニュー</font></td>
<td colspan="3"><? show_header_menu_list($con, $lcs_func, $menu_ids, $fname); ?></td>
</tr>
<tr class="for_user" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイドメニューを表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sidemenu_show_flg" value="t"<? if ($sidemenu_show_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="sidemenu_show_flg" value="f"<? if ($sidemenu_show_flg == "f") {echo(" checked");} ?>>しない
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示サイド</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sidemenu_position" value="1"<? if ($sidemenu_position == "1") {echo(" checked");} ?>>右
<input type="radio" name="sidemenu_position" value="2"<? if ($sidemenu_position == "2") {echo(" checked");} ?>>左
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_options($sel, $id_col, $nm_col, $tgt_ids) {
	while ($row = pg_fetch_array($sel)) {
		$id = $row[$id_col];
		$nm = $row[$nm_col];
		echo("<option value=\"$id\"");
		if (in_array($id, $tgt_ids)) {
			echo(" selected");
		}
		echo(">$nm\n");
	}
}

function show_header_menu_list($con, $lcs_func, $menu_ids, $fname) {
	echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	for ($i = 1; $i <= 11; $i++) {
		echo("<tr>\n");
		echo("<td width=\"20\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$i.</font></td>\n");
		echo("<td>");
		show_header_menu_listbox($con, $lcs_func, $menu_ids[$i - 1], $fname);
		echo("</td>\n");
		echo("</tr>\n");
	}
	echo("<tr>\n");
	echo("<td width=\"20\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">12.</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">ログアウト</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

function show_header_menu_listbox($con, $lcs_func, $selected_menu_id, $fname) {
	global $_label_by_profile;
	global $profile_type;

	require_once("get_menu_label.ini");
	$report_menu_label = get_report_menu_label($con, $fname);
	$shift_menu_label = get_shift_menu_label($con, $fname);

	echo("<select name=\"menu_ids[]\">");
	echo("<option value=\"-\">");
	echo("<option value=\"00\"");
	if ($selected_menu_id == "00") {echo(" selected");}
	echo(">マイページ");
	echo("<option value=\"01\"");
	if ($selected_menu_id == "01") {echo(" selected");}
	echo(">ウェブメール");
	echo("<option value=\"02\"");
	if ($selected_menu_id == "02") {echo(" selected");}
	echo(">スケジュール");
	echo("<option value=\"03\"");
	if ($selected_menu_id == "03") {echo(" selected");}
	echo(">委員会・WG");
	echo("<option value=\"35\"");
	if ($selected_menu_id == "35") {echo(" selected");}
	echo(">お知らせ・回覧板");
	echo("<option value=\"04\"");
	if ($selected_menu_id == "04") {echo(" selected");}
	echo(">タスク");
	echo("<option value=\"05\"");
	if ($selected_menu_id == "05") {echo(" selected");}
	echo(">伝言メモ");
	echo("<option value=\"06\"");
	if ($selected_menu_id == "06") {echo(" selected");}
	echo(">設備予約");
	echo("<option value=\"07\"");
	if ($selected_menu_id == "07") {echo(" selected");}
	echo(">出勤表");
	echo("<option value=\"08\"");
	if ($selected_menu_id == "08") {echo(" selected");}
	echo(">決裁・申請");
	echo("<option value=\"09\"");
	if ($selected_menu_id == "09") {echo(" selected");}
	echo(">ネットカンファレンス");
	echo("<option value=\"10\"");
	if ($selected_menu_id == "10") {echo(" selected");}
	echo(">掲示板");
	echo("<option value=\"11\"");
	if ($selected_menu_id == "11") {echo(" selected");}
	echo(">Q&amp;A");
	echo("<option value=\"12\"");
	if ($selected_menu_id == "12") {echo(" selected");}
	echo(">文書管理");
	echo("<option value=\"13\"");
	if ($selected_menu_id == "13") {echo(" selected");}
	echo(">アドレス帳");
	echo("<option value=\"36\"");
	if ($selected_menu_id == "36") {echo(" selected");}
	echo(">内線電話帳");
	echo("<option value=\"40\"");
	if ($selected_menu_id == "40") {echo(" selected");}
	echo(">リンクライブラリ");
	echo("<option value=\"14\"");
	if ($selected_menu_id == "14") {echo(" selected");}
	echo(">パスワード・本人情報");
	echo("<option value=\"15\"");
	if ($selected_menu_id == "15") {echo(" selected");}
	echo(">オプション設定");
//XX	echo("<option value=\"25\"");
//XX	if ($selected_menu_id == "25") {echo(" selected");}
//XX	echo(">コミュニティサイト");
	if ($lcs_func["4"] == "t") {
		echo("<option value=\"38\"");
		if ($selected_menu_id == "38") {echo(" selected");}
		echo(">イントラネット");
	}
	if ($lcs_func["7"] == "t") {
		echo("<option value=\"41\"");
		if ($selected_menu_id == "41") {echo(" selected");}
		echo(">検索ちゃん");
	}
	if ($lcs_func["1"] == "t") {
		echo("<option value=\"16\"");
		if ($selected_menu_id == "16") {echo(" selected");}
		// 患者管理/利用者管理
		echo(">".$_label_by_profile["PATIENT_MANAGE"][$profile_type]);
	}
	if ($lcs_func["15"] == "t") {
		echo("<option value=\"49\"");
		if ($selected_menu_id == "49") {echo(" selected");}
		echo(">看護支援");
	}
	if ($lcs_func["2"] == "t") {
		echo("<option value=\"17\"");
		if ($selected_menu_id == "17") {echo(" selected");}
		// 病床管理/居室管理
		echo(">".$_label_by_profile["BED_MANAGE"][$profile_type]);
	}
	if ($lcs_func["10"] == "t") {
		echo("<option value=\"44\"");
		if ($selected_menu_id == "44") {echo(" selected");}
		echo(">看護観察記録");
	}
	if ($lcs_func["5"] == "t") {
		echo("<option value=\"20\"");
		if ($selected_menu_id == "20") {echo(" selected");}
		echo(">ファントルくん");
	}
	if ($lcs_func["13"] == "t") {
		echo("<option value=\"47\"");
		if ($selected_menu_id == "47") {echo(" selected");}
		echo(">ファントルくん＋");
	}
	if ($lcs_func["12"] == "t") {
		echo("<option value=\"45\"");
		if ($selected_menu_id == "45") {echo(" selected");}
		echo(">バリテス");
	}
	if ($lcs_func["6"] == "t") {
		echo("<option value=\"39\"");
		if ($selected_menu_id == "39") {echo(" selected");}
		// メドレポート/ＰＯレポート(Problem Oriented)
		echo(">$report_menu_label");
	}
	if ($lcs_func["11"] == "t") {
		echo("<option value=\"46\"");
		if ($selected_menu_id == "46") {echo(" selected");}
		echo(">カルテビューワー");
	}
	if ($lcs_func["9"] == "t") {
		echo("<option value=\"43\"");
		if ($selected_menu_id == "43") {echo(" selected");}
		// 勤務シフト作成
		echo(">$shift_menu_label");
	}
	if ($lcs_func["8"] == "t") {
		echo("<option value=\"42\"");
		if ($selected_menu_id == "42") {echo(" selected");}
		echo(">" . get_cas_title_name());
	}
	if ($lcs_func["16"] == "t") {
		echo("<option value=\"50\"");
		if ($selected_menu_id == "50") {echo(" selected");}
		echo(">人事管理");
	}
	if ($lcs_func["17"] == "t") {
		echo("<option value=\"51\"");
		if ($selected_menu_id == "51") {echo(" selected");}
		echo(">クリニカルラダー");
	}
	if ($lcs_func["19"] == "t") {
		echo("<option value=\"53\"");
		if ($selected_menu_id == "53") {echo(" selected");}
		echo(">キャリア開発ラダー");
	}
	if ($lcs_func["18"] == "t") {
		echo("<option value=\"52\"");
		if ($selected_menu_id == "52") {echo(" selected");}
		echo(">勤務表");
	}
	if ($lcs_func["14"] == "t") {
		echo("<option value=\"48\"");
		if ($selected_menu_id == "48") {echo(" selected");}
		echo(">日報・月報");
	}
	if ($lcs_func["3"] == "t") {
		echo("<option value=\"18\"");
		if ($selected_menu_id == "18") {echo(" selected");}
		echo(">経営支援");
	}
	if ($lcs_func["31"] == "t") {
		echo("<option value=\"54\"");
		if ($selected_menu_id == "54") {echo(" selected");}
		echo(">スタッフ・ポートフォリオ");
	}
	echo("<option value=\"98\"");
	if ($selected_menu_id == "98") {echo(" selected");}
	echo(">open/close");
}
