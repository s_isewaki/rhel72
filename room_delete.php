<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$check_auth = check_authority($session, 21, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// チェックされた病室をループ
if (is_array($del_room)) {
	foreach ($del_room as $rm_no) {

		// 当該病室の入院履歴情報を取得
		$sql = "select ptif_id, inpt_in_dt, inpt_in_tm, inpt_out_dt, inpt_out_tm from inpthist";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 入院履歴一覧をループ
		while ($row = pg_fetch_array($sel)) {
			$ptif_id = $row["ptif_id"];
			$inpt_in_dt = $row["inpt_in_dt"];
			$inpt_in_tm = $row["inpt_in_tm"];
			$inpt_out_dt = $row["inpt_out_dt"];
			$inpt_out_tm = $row["inpt_out_tm"];

			// 退院情報の削除
			$sql = "delete from inpthist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// チェックリスト情報の削除
			$sql = "delete from inptqhist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 栄養問診表情報の削除
			$sql = "delete from inptnuthist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 入院状況情報の削除
			$sql = "delete from inptcond";
			$cond = "where ptif_id = '$ptif_id' and to_date(date, 'YYYYMMDD') between to_date('$inpt_in_dt', 'YYYYMMDD') and to_date('$inpt_out_dt', 'YYYYMMDD') and hist_flg = 't'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 移転情報の削除
			$sql = "delete from inptmove";
			$cond = "where ptif_id = '$ptif_id' and to_timestamp(move_dt || move_tm, 'YYYYMMDDHH24MI') between to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI') and to_timestamp('$inpt_out_dt$inpt_out_tm', 'YYYYMMDDHH24MI')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 外出情報の削除
			$sql = "delete from inptgoout";
			$cond = "where ptif_id = '$ptif_id' and out_type = '1' and to_timestamp(out_date || out_time, 'YYYYMMDDHH24MI') between to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI') and to_timestamp('$inpt_out_dt$inpt_out_tm', 'YYYYMMDDHH24MI')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 外泊情報の削除
			$sql = "delete from inptgoout";
			$cond = "where ptif_id = '$ptif_id' and out_type = '2' and to_date(out_date, 'YYYYMMDD') between to_date('$inpt_in_dt', 'YYYYMMDD') and to_date('$inpt_out_dt', 'YYYYMMDD')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 担当者情報の削除
			$sql = "delete from inptophist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		// 当該病室の入院情報を取得
		$sql = "select ptif_id, inpt_in_dt, inpt_in_tm from inptmst";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no' and not (ptif_id = '' or ptif_id is null)";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 入院情報一覧をループ
		while ($row = pg_fetch_array($sel)) {
			$ptif_id = $row["ptif_id"];
			$inpt_in_dt = $row["inpt_in_dt"];
			$inpt_in_tm = $row["inpt_in_tm"];

			// チェックリスト情報の削除
			$sql = "delete from inptq";
			$cond = "where ptif_id = '$ptif_id'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 栄養問診表情報の削除
			$sql = "delete from inptnut";
			$cond = "where ptif_id = '$ptif_id'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 入院状況情報の削除
			$sql = "delete from inptcond";
			$cond = "where ptif_id = '$ptif_id' and hist_flg = 'f'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 移転情報の削除
			$sql = "delete from inptmove";
			$cond = "where ptif_id = '$ptif_id' and to_timestamp(move_dt || move_tm, 'YYYYMMDDHH24MI') >= to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 外出情報の削除
			$sql = "delete from inptgoout";
			$cond = "where ptif_id = '$ptif_id' and out_type = '1' and to_timestamp(out_date || out_time, 'YYYYMMDDHH24MI') >= to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 外泊情報の削除
			$sql = "delete from inptgoout";
			$cond = "where ptif_id = '$ptif_id' and out_type = '2' and to_date(out_date, 'YYYYMMDD') >= to_date('$inpt_in_dt', 'YYYYMMDD')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 担当者情報の削除
			$sql = "delete from inptop";
			$cond = "where ptif_id = '$ptif_id'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		// 病床情報の削除
		$sql = "delete from inptmst";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 仮抑え情報の削除
		$sql = "delete from bedtemp";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病室の入院予定患者の担当者情報を削除
		$sql = "delete from inptopres";
		$cond = "where exists (select * from inptres where inptres.ptif_id = inptopres.ptif_id and inptres.bldg_cd = '$bldg_cd' and inptres.ward_cd = '$ward_cd' and inptres.ptrm_room_no = '$rm_no')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病室の入院予定情報を削除
		$sql = "delete from inptres";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病室の入院予定キャンセル患者の担当者情報を削除
		$sql = "delete from inptopcancel";
		$cond = "where exists (select * from inptcancel where inptcancel.inpt_res_ccl_id = inptopcancel.inpt_res_ccl_id and inptcancel.bldg_cd = '$bldg_cd' and inptcancel.ward_cd = '$ward_cd' and inptcancel.ptrm_room_no = '$rm_no')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病室の入院予定キャンセル情報を削除
		$sql = "delete from inptcancel";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病室の設備情報を削除
		$sql = "delete from rmeqp";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 病室情報の論理削除
		$sql = "update ptrmmst set";
		$set = array("ptrm_del_flg");
		$setvalue = array("t");
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 病室一覧画面を再表示
echo("<script language=\"javascript\">location.href = 'room_list.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd'</script>");
?>
