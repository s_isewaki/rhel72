<?php
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if ($day1_hour == "--" && $day1_min == "--") {
	echo("<script type=\"text/javascript\">alert('所定労働時間は必ず指定してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
    
if (($day1_hour == "--" && $day1_min != "--") || ($day1_hour != "--" && $day1_min == "--") || ($day4_hour == "--" && $day4_min != "--") || ($day4_hour != "--" && $day4_min == "--") || ($day5_hour == "--" && $day5_min != "--") || ($day5_hour != "--" && $day5_min == "--") || ($am_hour == "--" && $am_min != "--") || ($am_hour != "--" && $am_min == "--") || ($pm_hour == "--" && $pm_min != "--") || ($pm_hour != "--" && $pm_min == "--")) {
	echo("<script type=\"text/javascript\">alert('時間と分の一方のみは登録できません。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
if (!preg_match("/^\d\d\d\d\-\d\d\-\d\d \d\d:\d\d:\d\d$/", $_POST["histdate"])) {
    echo("<script type=\"text/javascript\">alert('日付が正しく入力されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

// 登録値の編集
if ($am_hour == "--") {
	$am_hour = "";
}
if ($am_min == "--") {
	$am_min = "";
}
if ($pm_hour == "--") {
	$pm_hour = "";
}
if ($pm_min == "--") {
	$pm_min = "";
}
$day1_time = "$day1_hour$day1_min";
$am_time = "$am_hour$am_min";
$pm_time = "$pm_hour$pm_min";
// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// UPDATE
if (!empty($_POST["calendarname_time_history_id"])) {
    $sql = "UPDATE calendarname_time_history SET ";
    $cond = "WHERE calendarname_time_history_id={$_POST["calendarname_time_history_id"]}";
    $upd = update_set_table($con, $sql, array('day1_time', 'am_time', 'pm_time', 'histdate'), array($day1_time, $am_time, $pm_time, $_POST["histdate"]), $cond, $fname);
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
// INSERT
else {
    $sql = "insert into calendarname_time_history (day1_time, am_time, pm_time, histdate) values(";
    $content = array($day1_time, $am_time, $pm_time, $_POST["histdate"]);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
location.href = 'calendar_name.php?session=<? echo($session); ?>';
</script>
