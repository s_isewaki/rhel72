<?
class timecard_bean{

	var $closing;
	var $fraction1;
	var $fraction1_min;
	var $fraction2;
	var $fraction2_min;
	var $fraction3;
	var $fraction4;
	var $fraction4_min;
	var $fraction5;
	var $fraction5_min;
	var $others1;
	var $others2;
	var $others3;
	var $others4;
	var $irrg_time_w;
	var $irrg_time_wd;
	var $irrg_time_m;
	var $rest1;
	var $rest2;
	var $rest3;
	var $modify_flg;
	var $ovtmscr_tm;
	var $show_time_flg;
	var $return_flg;
	var $others5;
	var $closing_parttime;
	var $night_start_time;
	var $night_end_time;
	var $night_workday_count;
	var $night_start_time_sat;
	var $night_end_time_sat;
	var $night_workday_count_sat;
	var $night_start_time_hol;
	var $night_end_time_hol;
	var $night_workday_count_hol;
	var $night_start_next_day_flag;
	var $night_end_next_day_flag;
	var $night_end_time2;
	var $night_workday_count2;
	var $night_end_next_day_flag2;
	var $night_start_next_day_flag_sat;
	var $night_end_next_day_flag_sat;
	var $night_end_time2_sat;
	var $night_workday_count2_sat;
	var $night_end_next_day_flag2_sat;
	var $night_start_next_day_flag_hol;
	var $night_end_next_day_flag_hol;
	var $night_end_time2_hol;
	var $night_workday_count2_hol;
	var $night_end_next_day_flag2_hol;
	var $over_time_apply_type;
	var $time_move_flag;
	var $type_display_flag;
	var $group_display_flag;
	var $zen_display_flag;
	var $yoku_display_flag;
	var $ret_display_flag;
	var $time_big_font_flag;
	var $closing_paid_holiday;
	var $night_allowance;
	var $night_allowance_sat;
	var $night_allowance_hol;
	var $csv_header_flg;
	var $delay_overtime_flg;
	var $overtime_40_flg;
	var $early_leave_time_flg;
	var $timecard_title;
	var $timecard_appr_disp_flg;
	var $class_disp_flg;
	var $atrb_disp_flg;
	var $dept_disp_flg;
	var $room_disp_flg;
	var $csv_ext_name;
	var $matching_key;
	var $over_apply_disp_flg;
	var $return_icon_flg;
	var $closing_month_flg;
	var $over_disp_split_flg;
	var $all_apply_disp_flg;
	var $all_duty_disp_flg;
	var $all_legal_over_disp_flg;
	var $over_yet_apply_disp_min;
	var $criteria_months;
	var $fraction_over_unit;
	var $rest4;
	var $rest5;
	var $rest6;
	var $rest7;
	var $fraction_out_unit;
	var $fraction_ret_unit;
	var $sortkey1;
	var $sortkey2;
	var $sortkey3;
	var $sortkey4;
	var $sortkey5;
	var $pdf_total_print_flg;
	var $pdf_count_print_flg;
	var $workflow_menu_flg;
	var $shift_menu_flg;
	var $shift_refer_menu_flg;
	var $shift_list_menu_flg;
	var $shift_duty_menu_flg;
	var $menu_view1;
	var $menu_view2;
	var $menu_view3;
	var $menu_view4;
	var $menu_view5;
	var $menu_view6;
	var $menu_view_default;
	var $timecard_title2;
	var $over_disp_sixty_flg;
	var $over_disp_forty_flg;
	var $over_disp_forty_week;
	var $over_forty_persons_full;
	var $over_forty_persons_short;
	var $over_forty_persons_part;
	var $year_paid_hol_menu_flg;
	var $modify_plan_flg;
    var $no_hol_overtime_flg;
    var $arr_hol_ovtm_except_reason; //休日残業として計算しない休暇事由（公休以外）20140815 
	
	function select($con, $fname){
		//1レコード取得
		$sql = "select * from timecard";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$row = pg_fetch_all($sel);
		if(count($row) > 0){
			$this->set_select_value($row[0]);
		}
        //休日残業として計算しない休暇事由（公休以外）20140815 
        $sql = "select reason_id from timecard_hol_ovtm_except_reason order by no";
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $this->arr_hol_ovtm_except_reason = array();
        while ($row = pg_fetch_assoc($sel)) {
            $this->arr_hol_ovtm_except_reason[] = $row["reason_id"];
        }
	}

	function set_select_value($row){

		//キー（カラム名取得）
		$arr_key = (array_keys($row));

		foreach ($arr_key as $key) {
			//カラム名の変数に値を保持
			$colmun_name = $key;
			$value       = $row[$key];
			$this->$colmun_name = $value;
//			}
//			global $$colmun_name;
//			$$colmun_name = $value;
		}

	}

}
?>