<?
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
    //初期値設定
    $emp_id = "";
    $emp_flg = "1";     //１：責任者、２：代行者

    ///-----------------------------------------------------------------------------
    //ＤＢ(empmst)より職員情報を取得
    //ＤＢ(wktmgrp)より勤務パターン情報を取得
    ///-----------------------------------------------------------------------------
    $data_emp = $obj->get_empmst_array("");
    $data_wktmgrp = $obj->get_wktmgrp_array();
    if (count($data_wktmgrp) <= 0) {
        $err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
    }
    ///-----------------------------------------------------------------------------
    // 勤務シフトグループ情報を取得
    ///-----------------------------------------------------------------------------
    if ($reload_flg == "") {
        $group_array = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
        for ($i=0;$i<count($group_array);$i++) {
            if ($group_array[$i]["group_id"] == $group_id) {
                $group_name = $group_array[$i]["group_name"];
                $standard_id = $group_array[$i]["standard_id"];
                $dr_emp_id_1 = $group_array[$i]["person_charge_id"];
                $dr_emp_id_2 = $group_array[$i]["caretaker_id"];
                $dr_emp_id_3 = $group_array[$i]["caretaker2_id"];
                $dr_emp_id_4 = $group_array[$i]["caretaker3_id"];
                $dr_emp_id_5 = $group_array[$i]["caretaker4_id"];
                $dr_emp_id_6 = $group_array[$i]["caretaker5_id"];
                $dr_emp_id_7 = $group_array[$i]["caretaker6_id"];
                $dr_emp_id_8 = $group_array[$i]["caretaker7_id"];
                $dr_emp_id_9 = $group_array[$i]["caretaker8_id"];
                $dr_emp_id_10 = $group_array[$i]["caretaker9_id"];
                $dr_emp_id_11 = $group_array[$i]["caretaker10_id"];
                $dr_nm_1 = $group_array[$i]["person_charge_name"];
                $dr_nm_2 = $group_array[$i]["caretaker_name"];
                $dr_nm_3 = $group_array[$i]["caretaker2_name"];
                $dr_nm_4 = $group_array[$i]["caretaker3_name"];
                $dr_nm_5 = $group_array[$i]["caretaker4_name"];
                $dr_nm_6 = $group_array[$i]["caretaker5_name"];
                $dr_nm_7 = $group_array[$i]["caretaker6_name"];
                $dr_nm_8 = $group_array[$i]["caretaker7_name"];
                $dr_nm_9 = $group_array[$i]["caretaker8_name"];
                $dr_nm_10 = $group_array[$i]["caretaker9_name"];
                $dr_nm_11 = $group_array[$i]["caretaker10_name"];
                $pattern_id = $group_array[$i]["pattern_id"];
                $cause_pattern_id = $group_array[$i]["pattern_id"];
                //SFC連携用病棟コード追加 20100709 
                $sfc_group_code = $group_array[$i]["sfc_group_code"];
                $timecard_auto_flg = $group_array[$i]["timecard_auto_flg"]; //勤務管理連動フラグ 20120305
                $hosp_patient_cnt = $group_array[$i]["hosp_patient_cnt"];  //１日平均入院患者数(病棟別) 20130423
                break;
            }
        }
    }
//勤務管理連動のデフォルトはしない
if ($timecard_auto_flg == "") {
    $timecard_auto_flg = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜管理 |シフトグループ登録</title>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">

    var childwin = null;

    ///-----------------------------------------------------------------------------
    // openEmployeeList
    ///-----------------------------------------------------------------------------
    function openEmployeeList(item_id, emp_flg) {

        $emp_flg = emp_flg;

        //職員選択画面表示
        dx = screen.width;
        dy = screen.top;
        base = 0;
        wx = 720;
        wy = 600;
        var url = './emplist_popup.php';
        url += '?session=<?=$session?>';
        url += '&emp_id=<?=$emp_id?>';
        url += '&mode=19';
        url += '&item_id='+item_id;
        childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

        childwin.focus();
    }
    ///-----------------------------------------------------------------------------
    // closeEmployeeList
    ///-----------------------------------------------------------------------------
    function closeEmployeeList() {
        if (childwin != null && !childwin.closed) {
            childwin.close();
        }
        childwin = null;
    }
    ///-----------------------------------------------------------------------------
    // add_target_list
    ///-----------------------------------------------------------------------------
    function add_target_list(item_id, emp_id, emp_name)
    {
        var emp_ids = emp_id.split(", ");
        var emp_names = emp_name.split(", ");

        //追加
        for(i=0;i<emp_ids.length;i++){
            emp_id = emp_ids[i];
            emp_name = emp_names[i];

            if ($emp_flg == "1") {
                document.mainform.dr_emp_id_1.value = emp_id;
                document.mainform.dr_nm_1.value = emp_name;
            } else
            if ($emp_flg == "2") {
                document.mainform.dr_emp_id_2.value = emp_id;
                document.mainform.dr_nm_2.value = emp_name;
            } else
            if ($emp_flg == "3") {
                document.mainform.dr_emp_id_3.value = emp_id;
                document.mainform.dr_nm_3.value = emp_name;
            } else
            if ($emp_flg == "4") {
                document.mainform.dr_emp_id_4.value = emp_id;
                document.mainform.dr_nm_4.value = emp_name;
            } else
            if ($emp_flg == "5") {
                document.mainform.dr_emp_id_5.value = emp_id;
                document.mainform.dr_nm_5.value = emp_name;
            } else
            if ($emp_flg == "6") {
                document.mainform.dr_emp_id_6.value = emp_id;
                document.mainform.dr_nm_6.value = emp_name;
            } else
            if ($emp_flg == "7") {
                document.mainform.dr_emp_id_7.value = emp_id;
                document.mainform.dr_nm_7.value = emp_name;
            } else
            if ($emp_flg == "8") {
                document.mainform.dr_emp_id_8.value = emp_id;
                document.mainform.dr_nm_8.value = emp_name;
            } else
            if ($emp_flg == "9") {
                document.mainform.dr_emp_id_9.value = emp_id;
                document.mainform.dr_nm_9.value = emp_name;
            } else
            if ($emp_flg == "10") {
                document.mainform.dr_emp_id_10.value = emp_id;
                document.mainform.dr_nm_10.value = emp_name;
            } else
            if ($emp_flg == "11") {
                document.mainform.dr_emp_id_11.value = emp_id;
                document.mainform.dr_nm_11.value = emp_name;
            }
            break;
        }
    }
    ///-----------------------------------------------------------------------------
    // 登録
    ///-----------------------------------------------------------------------------
    function dataInsert()
    {
        // 勤務パターン変更時チェック
        var wk1 = document.mainform.pattern_id.value;
        var wk2 = document.mainform.cause_pattern_id.value;
        if ((wk1 != "") && (wk2 != "")) {
            if (wk1 != wk2) {
                if (!confirm("出勤パターンを変更した場合、このシフトグループ(病棟)で既に登録済みの職員と下書きデータは削除されます。よろしいですか？")) {
                    return;
                }
            }
        }
<? //SFC連携用病棟コード追加開始 20100709 ?>
        // 連携用病棟コード
        wk2 = document.mainform.sfc_group_code.value;
        if (wk2 != '') {
            if (!wk2.match(/^[0-9a-zA-Z]{1,6}$/)) {
                alert("連携用病棟コードは半角英数字6桁以内で入力してください。");
                document.mainform.sfc_group_code.focus();
                return;
            }
        }
<? //SFC連携用病棟コード追加終了 20100709 ?>
        //病棟別１日平均入院患者数
        wk2 = document.mainform.hosp_patient_cnt.value;
        if (wk2 != '') {
            if (wk2.match(/[^0-9]/g)) {
                alert('病棟別１日平均入院患者数は、半角数字で入力してください。');
                document.mainform.hosp_patient_cnt.focus();
                return;
            }
        }
        
        //登録
        document.mainform.action="duty_shift_entity_group_update_exe.php";
        document.mainform.submit();
    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list tr {font-family:ＭＳ Ｐゴシック, Osaka;}
table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
table.block_in td td {border-width:0;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 画面遷移／タブ -->
    <!-- ------------------------------------------------------------------------ -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
    <?
        // 画面遷移
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_title($session, $section_admin_auth);           //duty_shift_common.ini
        echo("</table>\n");

        // タブ
        $arr_option = "";
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_tab_menuitem($session, $fname, $arr_option);    //duty_shift_manage_tab_common.ini
        echo("</table>\n");

        // 下線
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
    ?>
    </td></tr></table>
    <!-- ------------------------------------------------------------------------ -->
    <!-- データ不備時 -->
    <!-- ------------------------------------------------------------------------ -->
    <?
        if ($err_msg_1 != "") {
            echo($err_msg_1);
            echo("<br>\n");
        } else {
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 入力エリア -->
    <!-- ------------------------------------------------------------------------ -->
    <form name="mainform" method="post">
        <!-- ------------------------------------------------------------------------ -->
        <!-- 登録ボタン -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="600" border="0" cellspacing="0" cellpadding="2">
        <tr height="22">
        <td align="right"><input type="button" value="登録" onclick="dataInsert();"></td>
        </tr>
        </table>
        <!-- ------------------------------------------------------------------------ -->
        <!-- シフトグループ（病棟）名、施設基準、 -->
        <!-- シフト管理者（責任者、代行者） -->
        <!-- 勤務パターングループ -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
            <!-- ------------------------------------------------------------------------ -->
            <!-- シフトグループ（病棟）名 -->
            <!-- ------------------------------------------------------------------------ -->
            <tr height="22" class="j12">
            <td width="30%" bgcolor="#f6f9ff" align="right">シフトグループ（病棟）名</td>
            <td width="70%" colspan="3">
                <input name="group_name" type="text" value="<? echo(h($group_name)); ?>" size="50" maxlength="25" style="ime-mode:active;">
            </td>
            </tr>
            <!-- ------------------------------------------------------------------------ -->
            <!-- 施設基準 -->
            <!-- ------------------------------------------------------------------------ -->
            <tr height="22" class="j12">
            <td width="30%" bgcolor="#f6f9ff" align="right">施設基準</td>
            <td width="70%" colspan="3" valign="middle">
            <select name="standard_id">
            <?
                echo("<option value=\"\">適用なし");
                for($i=1;$i<=20;$i++) { //10件から20件へ変更 20120209
                    $wk_name = "施設基準" . $i;
                    echo("<option value=\"$i\"");
                    if ($standard_id == $i) {
                        echo(" selected");
                    }
                    echo(">$wk_name\n");
                }
            ?>
            </select>
            <span style="margin-left: 40px;">病棟別１日平均入院患者数</span>
            <input type="text" name="hosp_patient_cnt" value="<? echo($hosp_patient_cnt); ?>" style="width: 75px; text-align: right; ime-mode:disabled;">
            </td>
            <!-- ------------------------------------------------------------------------ -->
            <!-- シフト管理者（責任者） -->
            <!-- ------------------------------------------------------------------------ -->
            <tr height="22">
            <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフト管理者（責任者）</font></td>
            <td width="70%" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="text" name="dr_nm_1" value="<? echo($dr_nm_1); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_1" value="<? echo($dr_emp_personal_id_1); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_1" value="<? echo($dr_emp_id_1); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','1');">
            </font></td>
            </tr>
            <!-- ------------------------------------------------------------------------ -->
            <!-- シフト管理者（代行者） -->
            <!-- ------------------------------------------------------------------------ -->
            <tr height="22">
            <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフト管理者（代行者）</font></td>
            <td width="70%" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="text" name="dr_nm_2" value="<? echo($dr_nm_2); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_2" value="<? echo($dr_emp_personal_id_2); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_2" value="<? echo($dr_emp_id_2); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','2');">
                <br>
                <input type="text" name="dr_nm_3" value="<? echo($dr_nm_3); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_3" value="<? echo($dr_emp_personal_id_3); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_3" value="<? echo($dr_emp_id_3); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','3');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_3.value = '';document.mainform.dr_nm_3.value = '';">
                <br>
                <input type="text" name="dr_nm_4" value="<? echo($dr_nm_4); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_4" value="<? echo($dr_emp_personal_id_4); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_4" value="<? echo($dr_emp_id_4); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','4');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_4.value = '';document.mainform.dr_nm_4.value = '';">
                <br>
                <input type="text" name="dr_nm_5" value="<? echo($dr_nm_5); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_5" value="<? echo($dr_emp_personal_id_5); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_5" value="<? echo($dr_emp_id_5); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','5');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_5.value = '';document.mainform.dr_nm_5.value = '';">
                <br>
                <input type="text" name="dr_nm_6" value="<? echo($dr_nm_6); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_6" value="<? echo($dr_emp_personal_id_6); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_6" value="<? echo($dr_emp_id_6); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','6');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_6.value = '';document.mainform.dr_nm_6.value = '';">
                <br>
                <input type="text" name="dr_nm_7" value="<? echo($dr_nm_7); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_7" value="<? echo($dr_emp_personal_id_7); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_7" value="<? echo($dr_emp_id_7); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','7');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_7.value = '';document.mainform.dr_nm_7.value = '';">
                <br>
                <input type="text" name="dr_nm_8" value="<? echo($dr_nm_8); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_8" value="<? echo($dr_emp_personal_id_8); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_8" value="<? echo($dr_emp_id_8); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','8');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_8.value = '';document.mainform.dr_nm_8.value = '';">
                <br>
                <input type="text" name="dr_nm_9" value="<? echo($dr_nm_9); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_9" value="<? echo($dr_emp_personal_id_9); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_9" value="<? echo($dr_emp_id_9); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','9');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_9.value = '';document.mainform.dr_nm_9.value = '';">
                <br>
                <input type="text" name="dr_nm_10" value="<? echo($dr_nm_10); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_10" value="<? echo($dr_emp_personal_id_10); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_10" value="<? echo($dr_emp_id_10); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','10');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_10.value = '';document.mainform.dr_nm_10.value = '';">
                <br>
                <input type="text" name="dr_nm_11" value="<? echo($dr_nm_11); ?>" readOnly>
                <input type="hidden" name="dr_emp_personal_id_11" value="<? echo($dr_emp_personal_id_11); ?>" readOnly>
                <input type="hidden" name="dr_emp_id_11" value="<? echo($dr_emp_id_11); ?>" readOnly>
                <input type="button" name="emp1" value="職員名簿" onclick="openEmployeeList('1','11');">
                <input type="button" value="クリア" onclick="document.mainform.dr_emp_id_11.value = '';document.mainform.dr_nm_11.value = '';">
            </font>
            </td>
            </tr>
            <!-- ------------------------------------------------------------------------ -->
            <!-- 勤務パターングループ -->
            <!-- ------------------------------------------------------------------------ -->
            <tr height="22">
            <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務パターングループ名</font></td>
            <td width="70%" colspan="3">
            <?
                echo("<select name=\"pattern_id\" onchange=\"this.form.action = '$fname'; this.form.submit();\"> \n");
                for($i=0;$i<count($data_wktmgrp);$i++) {
                    $wk_id= $data_wktmgrp[$i]["id"];
                    $wk_name = $data_wktmgrp[$i]["name"];
                    echo("<option value=\"$wk_id\"");
                    if ($pattern_id == $wk_id) {
                        echo(" selected");
                    }
                    echo(">$wk_name\n");
                }
                echo("</select> \n");
            ?>
            </td>
            </tr>
        <? /* SFC連携用病棟コード追加開始 20100709 */ ?>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 連携用病棟コード -->
        <!-- ------------------------------------------------------------------------ -->
            <!-- ------------------------------------------------------------------------ -->
            <!-- 見出し -->
            <!-- ------------------------------------------------------------------------ -->
            <tr height="22">
            <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携用病棟コード</font></td>
            <td width="70%" colspan="2">
            <input type="text" id="sfc_group_code" name="sfc_group_code" size="6" maxlength="6" value="<?=$sfc_group_code?>" style="ime-mode:disabled;"><br>
            </tr>
        <? /* SFC連携用病棟コード追加終了 20100709 */ ?>
        <? /* 勤務管理連動フラグ追加開始 20120305 */ ?>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 勤務管理連動 -->
        <!-- ------------------------------------------------------------------------ -->
            <!-- ------------------------------------------------------------------------ -->
            <!-- 見出し -->
            <!-- ------------------------------------------------------------------------ -->
            <tr height="22">
            <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務管理連動</font></td>
            <td width="70%" colspan="2">
            <label>
    <input type="radio" name="timecard_auto_flg" value="t"<? if ($timecard_auto_flg == "t") {echo(" checked ");} ?>>    
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
    <label>
    <input type="radio" name="timecard_auto_flg" value="f"<? if ($timecard_auto_flg == "f") {echo(" checked ");} ?>>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
            </tr>
        </table>
        <? /* 勤務管理連動フラグ追加終了 20120305 */ ?>
        <!-- ------------------------------------------------------------------------ -->
        <!-- 登録ボタン -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="600" border="0" cellspacing="0" cellpadding="2">
        <tr height="22">
        <td align="right"><input type="button" value="登録" onclick="dataInsert();"></td>
        </tr>
        </table>
        <!-- ------------------------------------------------------------------------ -->
        <!-- ＨＩＤＤＥＮ -->
        <!-- ------------------------------------------------------------------------ -->
        <input type="hidden" name="session" value="<? echo($session); ?>">
        <input type="hidden" name="reload_flg" value="1">
        <input type="hidden" name="group_id" value="<? echo($group_id); ?>">
        <input type="hidden" name="cause_pattern_id" value="<? echo($cause_pattern_id); ?>">
    </form>
    <?
        }
    ?>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>