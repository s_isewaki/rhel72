<?
require_once("about_comedix.php");
require_once("get_values.php");
require_once("fplus_common_class.php");

//==============================
//初期処理
//==============================
//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix  報告書印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">

function load_action() 
{
	//印刷して閉じる
	self.print();
//	self.close();
	setTimeout('self.close()', 1000);
}	

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}

</style>

</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">
<form name="mainform" action="hiyari_rp_classification_folder_input.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="<?=$mode?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="folder_id" value="<?=$folder_id?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">


<!-- 本体 START -->
<!--
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5">
-->
<?

$obj = new fplus_common_class($con, $fname);


// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);

$wkfw_folder_id = $arr_apply_wkfwmst[0]["wkfw_folder_id"];
$emp_class_nm      = $arr_apply_wkfwmst[0]["apply_class_nm"];
$emp_attribute_nm  = $arr_apply_wkfwmst[0]["apply_atrb_nm"];
$emp_dept_nm       = $arr_apply_wkfwmst[0]["apply_dept_nm"];
$emp_room_nm       = $arr_apply_wkfwmst[0]["apply_room_nm"];
$class_nm  = $emp_class_nm;
$class_nm .= " > ";
$class_nm .= $emp_attribute_nm;
$class_nm .= " > ";
$class_nm .= $emp_dept_nm;

if ($emp_room_nm != "")
{
	$class_nm .= " > ";
	$class_nm .= $emp_room_nm;
}
$apply_lt_name  = $arr_apply_wkfwmst[0]["emp_lt_nm"];
$apply_ft_name  = $arr_apply_wkfwmst[0]["emp_ft_nm"];
$apply_full_nm  = "$apply_lt_name $apply_ft_name";
$wkfw_nm        = $arr_apply_wkfwmst[0]["wkfw_nm"];
$wkfw_title     = $arr_apply_wkfwmst[0]["wkfw_title"];
$apply_date       = $arr_apply_wkfwmst[0]["apply_date"];
$year = substr($apply_date, 0, 4);
$apply_date     = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $apply_date);

$apply_no         = $arr_apply_wkfwmst[0]["apply_no"];
$short_wkfw_name  = $arr_apply_wkfwmst[0]["short_wkfw_name"];

// カテゴリ名(フォルダパス)取得
$folder_path = $wkfw_nm;
if($wkfw_folder_id != "")
{
	// フォルダ名
	$folder_list = $obj->get_folder_path($wkfw_folder_id);
	foreach($folder_list as $folder)
	{
		if($folder_path != "")
		{
			$folder_path .= " > ";
		}
		$folder_path .= $folder["name"];
	}
}


$apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);


//$patient_non_print2 = $patient_non_print == "true";
//$report_html = get_report_html_for_print($con,$fname,$report_id,$eis_id,$patient_non_print2,$session);//$eis_idは省略可能。
?>
	<tr>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書様式</font>
		</td>
		<td width="30%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($wkfw_title)?></font>
		</td>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font>
		</td>
		<td width="30%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font>
		</td>
	</tr>

	<tr>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書番号</font>
		</td>
		<td width="30%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($apply_no)?></font>
		</td>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font>
		</td>
		<td width="30%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($folder_path)?></font>
		</td>
	</tr>

	<tr>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($apply_full_nm)?></font>
		</td>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font>
		</td>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($class_nm)?></font>
		</td>
	</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<?
if($MRSA_REPORT =="MRSA")
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■検体採取日</font></td>
		<td colspan="3"><font class="j12"><?=$data_1_14_pick_day?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■患者ID</font></td>
		<td><font class="j12"><?=$data_1_1_pid?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">■患者氏名</font></td>
		<td><font class="j12"><?=$data_1_3_name?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■生年月日/年齢/性別</font></td>
		<td><font class="j12"><?=$data_1_2_birth?> / <?=$data_1_4_years?>歳 / <?=$data_1_51_sex?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">■入院日</font></td>
		<td><font class="j12"><?=$data_1_8_hospital?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■診療科</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A409' and item_cd='$data_1_6_depart'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td><font class="j12"><?=$get_data?></font></td>
		
		<td bgcolor="E3E3FF"><font class="j12">■病棟</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A404' and item_cd='$data_1_7_ward'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■基礎疾患</font></td>
<?
$sql = "select record_caption from fplus_001_master where field_name='data_1_11_base_disease' and record_id='$data_1_9_base_disease'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><br>(疾患名)<?=$data_1_10_disease_detail?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■検出菌名/微生物名</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A405' and item_cd='$data_1_13_find_germ'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td colspan="3"><font class="j12"><?=$get_data?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■検出検体</font></td>
<?
$sql = "select record_caption from fplus_001_master where field_name='data_2_1_specimen' and record_id='$data_2_1_specimen'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><br>(疾患名)<?=$data_2_2_etc_detail?></font></td>
	</tr>

<?
$str_chk_name = "";
$cond_cnt = 0;
$cond_sql = "";
if($data_3_1_pcg[0] == "01")
{
	$cond_sql = " field_name = 'data_3_1_pcg' "; 
	$cond_cnt++;
}

if($data_3_2_mpipc[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_2_mpipc' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_2_mpipc' ";}
	$cond_cnt++;
}

if($data_3_3_cez[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_3_cez' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_3_cez' ";}
	$cond_cnt++;
}

if($data_3_4_ctm[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_4_ctm' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_4_ctm' ";}
	$cond_cnt++;
}

if($data_3_5_gm[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_5_gm' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_5_gm' ";}
	$cond_cnt++;
}

if($data_3_6_em[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_6_em' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_6_em' ";}
	$cond_cnt++;
}

if($data_3_7_mino[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_7_mino' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_7_mino' ";}
	$cond_cnt++;
}

if($data_3_8_ipm[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_8_ipm' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_8_ipm' ";}
	$cond_cnt++;
}

if($data_3_9_cldm[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_9_cldm' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_9_cldm' ";}
	$cond_cnt++;
}

if($data_3_10_vcm[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_10_vcm' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_10_vcm' ";}
	$cond_cnt++;
}

if($data_3_11_lvfx[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_11_lvfx' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_11_lvfx' ";}
	$cond_cnt++;
}

if($data_3_12_teic[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_12_teic' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_12_teic' ";}
	$cond_cnt++;
}

if($data_3_13_etc[0] == "01")
{
	if($cond_cnt < 1)
		{$cond_sql = " field_name = 'data_3_13_etc' ";}
	else
		{$cond_sql .= " or field_name = 'data_3_13_etc' ";}
	$cond_cnt++;
}

if($cond_sql != "")
{
	$sql = "select record_caption,record_sub_caption from fplus_001_master where ".$cond_sql;
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	
	$str_chk_name = "";
	while($row = pg_fetch_array($sel))
	{
		$str_chk_name .= "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
	}
}
?>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■薬剤感受性検査結果（Rのついている薬剤にチェックをつけて下さい）</font></td>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">その他記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_3_14_etc_detail?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■病原体の型(MRSAの場合)</font></td>
		<td colspan="3"><font class="j12"><?=$data_4_1_mrsa?></font></td>
	</tr>
<?
$sql = "select record_caption,record_sub_caption from fplus_001_master where field_name='data_4_2_infectious' and record_id='".$data_4_2_infectious."'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}

$str_chk_name = "";
while($row = pg_fetch_array($sel))
{
	$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
}
?>
<?
$sql = "select item_name from tmplitem where mst_cd='A407' and item_cd='$data_4_3_diagnosis'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■感染症/保菌状態</font></td>
		<td colspan="3"><font class="j12"><?=$str_chk_name?><br>(診断名)<?=$get_data?><br>(その他詳細)<?=$data_4_4_diagnosis_detail?></font></td>
	</tr>

<?
$sql = "select record_caption,record_sub_caption from fplus_001_master where field_name='data_4_5_root' and record_id='".$data_4_5_root."'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}

$str_chk_name = "";
while($row = pg_fetch_array($sel))
{
	$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
}
?>
	<tr align="left">
		<td colspan="4" bgcolor="E3E3FF"><font class="j12">■感染経路</font></td>
	</tr>
	<tr align="left">
		<td colspan="4"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>
	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_4_6_reason)?></font></td>
	</tr>

<?
$sql = "select record_caption,record_sub_caption from fplus_001_master where field_name='data_4_7_antimicrobial' and record_id='".$data_4_7_antimicrobial."'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}

$str_chk_name = "";
while($row = pg_fetch_array($sel))
{
	$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
}
?>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■一か月以内投薬抗菌剤</font></td>
		<td colspan="3"><font class="j12"><?=$str_chk_name?><br>(薬剤名)<?=$data_4_8_antimicrobial_medicine?></font></td>
	</tr>

<?
$sql = "select record_caption,record_sub_caption from fplus_001_master where field_name='data_4_9_immunity' and record_id='".$data_4_9_immunity."'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}

$str_chk_name = "";
while($row = pg_fetch_array($sel))
{
	$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
}
?>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■患者の免疫能</font></td>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>

<?
$sql = "select record_caption,record_sub_caption from fplus_001_master where field_name='data_4_10_control' and record_id='".$data_4_10_control."'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}

$str_chk_name = "";
while($row = pg_fetch_array($sel))
{
	$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
}
?>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■免疫抑制剤使用の有無</font></td>
		<td colspan="3"><font class="j12"><?=$str_chk_name?><br>(薬剤名)<?=$data_4_11_control_medicine?></font></td>
	</tr>

</table>

<?
}
else if($MRSA_REPORT =="enter")
{
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■検体採取日</font></td>
		<td colspan="3"><font class="j12"><?=$data_1_14_pick_day?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■患者ID</font></td>
		<td><font class="j12"><?=$data_1_1_pid?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">■患者氏名</font></td>
		<td><font class="j12"><?=$data_1_3_name?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■生年月日/年齢/性別</font></td>
		<td><font class="j12"><?=$data_1_2_birth?> / <?=$data_1_4_years?>歳 / <?=$data_1_51_sex?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">■入院日</font></td>
		<td><font class="j12"><?=$data_1_8_hospital?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■診療科</font></td>
	<?
	$sql = "select item_name from tmplitem where mst_cd='A409' and item_cd='$data_1_6_depart'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td><font class="j12"><?=$get_data?></font></td>
		
		<td bgcolor="E3E3FF"><font class="j12">■病棟</font></td>
	<?
	$sql = "select item_name from tmplitem where mst_cd='A404' and item_cd='$data_1_7_ward'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■基礎疾患</font></td>
	<?
	$sql = "select record_caption from fplus_001_master where field_name='data_1_11_base_disease' and record_id='$data_1_9_base_disease'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><br>(疾患名)<?=$data_1_10_disease_detail?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■検出菌名/微生物名</font></td>
	<?
	$sql = "select item_name from tmplitem where mst_cd='A406' and item_cd='$data_1_11_find_germ'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td colspan="3"><font class="j12"><?=$get_data?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■感染症名</font></td>
	<?
	$sql = "select item_name from tmplitem where mst_cd='A408' and item_cd='$data_1_15_diagnosis'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td colspan="3"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■診断日</font></td>
		<td colspan="3"><font class="j12"><?=$data_1_16_diagnosis_day?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■感染症発生原因、経路</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_17_root)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■実施した感染対策</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_18_plan)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■接触患者の有無と対応</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_19_contact_patient)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■接触職員の有無と対応</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_20_contact_member)?></font></td>
	</tr>
</table>
<?
}
else if($MRSA_REPORT =="member")
{
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■職員氏名</font></td>
		<td><font class="j12"><?=$data_1_1_name?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">■年齢/性別</font></td>
		
	<?
	$sql = "select record_caption from fplus_epi_master where field_name='data_1_7_sex' and record_id='$data_1_3_sex'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
?>
		
		<td><font class="j12"><?=$data_1_2_years?>歳  / <?=$get_data?></font></td>
	</tr>


	<?
	if($data_1_8_etc[0] == "01")
	{
		$str_belong = $data_1_9_etc_detail;
	}
	else
	{
		$str_belong = $data_1_4_cls." ".$data_1_5_atrb." ".$data_1_6_dept." ".$data_1_7_room;
	}
?>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■所属</font></td>
		<td colspan="3"><font class="j12"><?=$str_belong?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■感染症名</font></td>
	<?
	$sql = "select item_name from tmplitem where mst_cd='A408' and item_cd='$data_1_11_diagnosis'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td><font class="j12"><?=$get_data?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">■診断日</font></td>
		<td><font class="j12"><?=$data_1_14_pick_day?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■感染症発生原因、経路</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_15_root)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■実施した感染対策</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_16_plan)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■接触患者の有無と対応</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_17_contact_patient)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■接触職員の有無と対応</font></td>
		<td colspan="3"><font class="j12"><?=nl2br($data_1_18_contact_member)?></font></td>
	</tr>



</table>


<?
}
?>

<!-- BODY全体 END -->

</form>
</body>
</html>
<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

