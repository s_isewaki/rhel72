<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("./conf/sql.inf");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$auth_check = check_authority($session, 21, $fname);
if ($auth_check == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// チェックされた設備をループ
if (is_array($del_equip)) {
	foreach ($del_equip as $rmeqp_id) {

		// 設備を論理削除
		$cond = "where rmeqp_id = '$rmeqp_id'";
		$set = array("rmeqp_del_flg");
		$setvalue = array("t");
		$up = update_set_table($con, $SQL76, $set, $setvalue, $cond, $fname);
		if ($up == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 設備登録画面を再表示
echo("<script language=\"javascript\">location.href = 'room_equipment_register.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd';</script>\n");
?>
