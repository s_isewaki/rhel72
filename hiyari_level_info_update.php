<?
require_once("Cmx.php");
require_once("aclg_set.php");

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//レベル情報の取得
//==============================

//インシレベルに対する説明文を取得
$rep_obj = new hiyari_report_class($con, $fname);
$level_infos = $rep_obj->get_inci_level_infos();


//==============================
//更新処理
//==============================
if($mode == "update")
{
	//==============================
	//インシレベル使用チェック
	//==============================
//	for($i=0; $i<count($level_infos); $i++)
//	{
//		$easy_code = $level_infos[$i]["easy_code"];
//		$input_name = "use_flg_".$easy_code;
//		$use_flg = ($$input_name != "");
		
		//選択不可能にする場合
//		if(!$use_flg)
//		{
			
//			$sql = "select count(*) as cnt from inci_easyinput_data where grp_code = 90 and easy_item_code = 10 and input_item = '$easy_code'";
//			$sel = select_from_table($con,$sql,"",$fname);
//			if($sel == 0){
//				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//				echo("<script language='javascript'>showErrorPage(window);</script>");
//				pg_close($con);
//				exit;
//			}
//			$num = pg_fetch_result($sel,0,"cnt");
//			if($num > 0)
//			{
//				echo("<script language='javascript'>alert(\"使用済みの患者影響レベルを選択不可能にすることはできません。\");</script>");
//				echo("<script language='javascript'>history.back();</script>");
//				pg_close($con);
//				exit;
//			}
//		}
//	}

	//==============================
	//レベル情報の更新
	//==============================
	
	// トランザクションの開始
	pg_query($con, "begin");
	
	for($i=0; $i<count($level_infos); $i++)
	{
		$easy_code = $level_infos[$order_no[$i]]["easy_code"];
		$input_name = "message_".$easy_code;
		$message   = $$input_name;
		$input_name = "use_flg_".$easy_code;
		$use_flg = ($$input_name != "");
		$input_name = "easy_name_".$easy_code;
		$easy_name = $$input_name;
		$input_name = "short_name_".$easy_code;
		$short_name = $$input_name;
		//$$"id名"を入力することでタグテーブルの中身を持ってくることが出来る。
		if($shoki_flg == "t"){}
		$input_name = "order_no_".$easy_code;
		$easy_num = $$input_name;		
	    $rep_obj->set_level_info($easy_code,pg_escape_string($short_name),pg_escape_string($message),$use_flg);
		$rep_obj->set_inci_easy_name($easy_code,pg_escape_string($easy_name),$i);
	}
	
	// トランザクションのコミット
	pg_query($con, "commit");
	
	//==============================
	//レベル情報の再取得
	//==============================
	$level_infos = $rep_obj->get_inci_level_infos();
}
else if($mode == "back")
{
	//インシレベルマスタ情報取得
	$level_mst_infos = $rep_obj->get_inci_level_mst();

	$tmp_level_mst_infos = array();
	for($i=0; $i<count($level_mst_infos); $i++)
	{
		$easy_code = $level_mst_infos[$i]["easy_code"];
		$input_name = "use_flg_".$easy_code;
		$use_flg = ($$input_name != "");
		$use_flg = $$input_name;

		$tmp_arr = array("easy_code" => $level_mst_infos[$i]["easy_code"], "easy_name" => $level_mst_infos[$i]["easy_name"], "short_name" => $level_mst_infos[$i]["short_name"], "message" => $level_mst_infos[$i]["message"], "use_flg" => $use_flg);
		array_push($tmp_level_mst_infos, $tmp_arr);
	}
	$level_infos = $tmp_level_mst_infos;
	//初期値に戻すをクリックしたことを記録する。
	$shoki_flg = "t";

}

//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | レベル説明文</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tablednd.js"></script>
<script type="text/javascript">

//影響レベルの並び替えを行うスクリプト
$(function() {
	$('#eislist tbody').tableDnD({
		dragHandle: "dragHandle"
	});

	$('#eislist tbody tr').hover(function() {
		$(this.cells).css("cursor","move");//表の行に乗った時に、カーソルのアイコンをむーぶに変更する。
		$(this.cells[0]).addClass('showDragHandle');
		$(this.cells).css('background-color', 'gold');//カーソルを載った場所の色を変更する
	},

	function() {//カーソルが行から移動したときに色を変更する
		$(this.cells[0]).removeClass('showDragHandle');//
		$(this.cells).css('background-color', 'white');
	});
});

function check_submit()
{
	var obj = "";
	var msg = "";
	var use_flg_no_checked = true;
	var easy_name_checked = false;
	var short_name_checked = false;
	var message_checked = false;

<?
for($i=0; $i<count($level_infos); $i++)
{
?>

	//インシレベル未入力チェック
	obj = document.getElementById("easy_name_<?=$level_infos[$i]["easy_code"]?>");
	if(!easy_name_checked)
	{
		if(obj.value == "")
		{
			msg = msg + "患者影響レベルが未入力です。\n";
			easy_name_checked = true;
		}
	}

	//略称未入力チェック
	obj = document.getElementById("short_name_<?=$level_infos[$i]["easy_code"]?>");
	if(!short_name_checked)
	{
		if(obj.value == "")
		{
			msg = msg + "略称が未入力です。\n";
			short_name_checked = true;
		}
	}

	//メッセージ未入力チェック
	obj = document.getElementById("message_<?=$level_infos[$i]["easy_code"]?>");
	if(!message_checked)
	{
		if(obj.value == "")
		{
			msg = msg + "患者影響レベルの説明文が未入力です。\n";
			message_checked = true;
		}
	}

	//選択可能フラグ全未指定チェック
	obj = document.getElementById("use_flg_<?=$level_infos[$i]["easy_code"]?>");
	if(obj.checked)
	{
		use_flg_no_checked = false;
	}
<?
}
?>
	if(use_flg_no_checked)
	{
		msg = msg + "選択可能な患者影響レベルがありません。\n";
	}
	if(msg == "")
	{
		return true;
	}
	else
	{
		alert(msg);
		return false;
	}
}

// 更新ボタン
function upd_level_info()
{
	if(check_submit())
	{
		document.form1.action="hiyari_level_info_update.php?session=<?=$session?>&mode=update";
		document.form1.submit();
	}
}

// 初期値に戻すボタン
function bak_level_info()
{
	if(confirm('「患者影響レベル」・「略称」・「説明」の設定値を初期値に戻します。よろしいですか？'))
	{
		document.form1.action="hiyari_level_info_update.php?session=<?=$session?>&mode=back";
		document.form1.submit();
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form id="form1" name="form1" action="hiyari_level_info_update.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
$is_kanri_gamen = true;
show_hiyari_header($session,$fname,$is_kanri_gamen);
?>
<!-- ヘッダー END -->


<!-- メイン START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>

<!-- 入力表の枠 START -->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- 入力表 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" id="eislist">
<thead>
<tr height="22">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">※ ドラッグ アンド ドロップで並び替えを行えます。並び替え後は「更新」ボタンをクリックしてください。</font>
<td align="center" bgcolor="#DFFFDC" width="10"style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
<td align="center" bgcolor="#DFFFDC" width="120"style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">患者影響レベル</font></td>
<td align="center" bgcolor="#DFFFDC" width="40"style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">略称</font></td>
<td align="center"  bgcolor="#DFFFDC" width="100%"style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">説明</font></td>
<td align="center"  bgcolor="#DFFFDC" width="80"style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">選択可能</font></td>
</tr>
</thead>
<tbody>
<?
for($i=0; $i<count($level_infos); $i++)
{
	if($level_infos[$i]["use_flg"])
	{
		$use_flg_checked = "checked";
	}
	else
	{
		$use_flg_checked = "";
	}
?>
<tr height="22">
<td class="dragHandle" align="left" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input type="hidden" name="order_no_<?=$level_infos[$i]["easy_code"]?>" value="<?php echo($i); ?>"></font></td>
<input type="hidden" name="order_no[]" value = "<?php echo($i);?>">
<td align="left" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input type="text" id="easy_name_<?=$level_infos[$i]["easy_code"]?>" name="easy_name_<?=$level_infos[$i]["easy_code"]?>" value="<?=h($level_infos[$i]["easy_name"])?>" maxlength="20"></font></td>
<td align="left" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input type="text" id="short_name_<?=$level_infos[$i]["easy_code"]?>" name="short_name_<?=$level_infos[$i]["easy_code"]?>" value="<?=h($level_infos[$i]["short_name"])?>" size="3" maxlength="2"></font></td>
<td align="left"  bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input type="text" id="message_<?=$level_infos[$i]["easy_code"]?>" name="message_<?=$level_infos[$i]["easy_code"]?>" value="<?=h($level_infos[$i]["message"])?>" style="width:100%"></font></td>
<td align="center"  bgcolor="#FFFFFF"><input type="checkbox" id="use_flg_<?=$level_infos[$i]["easy_code"]?>" name="use_flg_<?=$level_infos[$i]["easy_code"]?>" value="true" <?=$use_flg_checked?>></td>
</tr>
<?
}
?>
</tbody>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<input type="button" name="shoki" value="文言を初期値に戻す" onclick="bak_level_info();">
<input type="button" name="koushin" value="更新" onclick="upd_level_info();">
</font>
</td>
</tr>
</table>
<!-- 入力表 END -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 入力表の枠 END -->

</td>
</tr>
</table>
<!-- メイン END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,$is_kanri_gamen);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

<?php if($mode==back){?>
	<input type="hidden" name="shoki_flg" value="f">
<?php }?>
</form>
</body>
</html>
<?



//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>
