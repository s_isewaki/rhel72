<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG | スケジュール（月）</title>
<?
$fname = $PHP_SELF;

require("about_session.php");
require("about_authority.php");
require("time_check3.ini");
require("show_date_navigation_month.ini");
require("holiday.php");
require("show_schedule_common.ini");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付の設定
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
	time_update($con, $emp_id, $timegd, $fname);
} else if ($timegd == "") {
	$timegd = time_check($con, $emp_id, $fname);
}

// オプション設定情報を取得
$sql = "select calendar_start2 from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start2");

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

// 委員会・WG名を取得
$sql = "select pjt_name from project";
$cond = "where pjt_id = $pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$project_name = pg_result($sel, 0, "pjt_name");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function showGuide() {
	var timegd = (document.timegd.timeguide.checked) ? 'on' : 'off';
	location.href = 'project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>&date=<? echo($date); ?>&timegd=' + timegd;
}

function openPrint() {
	window.open('project_schedule_print_month_menu.php?&session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>', 'newwin', 'width=800,height=600,scrollbars=yes');
}

function changeDate(dt) {
	location.href = 'project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>&date=' + dt;
}

function popupScheduleDetail(title, place, date, time, type, detail, regname, e) {
	popupDetailBlue(
		new Array(
			'タイトル', title,
			'行先', place,
			'日付', date,
			'時刻', time,
			'種別', type,
			'詳細', detail,
			'登録者', regname
		), 400, 80, e
	);
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
	popupDetailBlue(
		new Array(
			'施設', fcl_name,
			'日付', evt_date,
			'時刻', evt_time,
			'行事名', evt_name,
			'詳細', evt_content,
			'連絡先', evt_contact
		), 400, 80, e
	);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>

<? if ($pjt_admin_auth == "1") { ?>

	<? if($adm_flg=="t") {?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? }else{ ?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? } ?>


<? }else{ ?>
	<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>

<? } ?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? }else{ ?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } ?>

<td width="5"></td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_schedule_month_menu.php?session=<? echo($session); ?>&date=<? echo($date); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>スケジュール</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5" colspan="10"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($project_name); ?></b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>月</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_year_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($year); ?>&s_date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="project_schedule_list_upcoming.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="project_schedule_option.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onclick="openPrint();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($last_month); ?>&adm_flg=<?=$adm_flg?>">&lt;前月</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_m($date); ?></select>&nbsp;<a href="project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($next_month); ?>&adm_flg=<?=$adm_flg?>">翌月&gt;</a></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? 
// 院内行事/所内行事
echo ($_label_by_profile["EVENT"][$profile_type]); ?></font><input type="checkbox" name="timeguide" onclick="showGuide();"<? if ($timegd == "on") {echo(" checked");} ?>></font></td>
</tr>
</table>
</form>
<? show_menu_schedule_list_m($date, $pjt_id, $emp_id, $session, $fname, $start_wd, $con,$adm_flg); ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 月間スケジュールを表示
function show_menu_schedule_list_m($date, $pjt_id, $emp_id, $session, $fname, $start_wd, $con,$adm_flg) {


	//スケジュール登録制御用に自分が指定されている委員会・ＷＧのメンバーかどうかチェックする
	$sql="select count(pjt_id) as member_cnt from promember where pjt_id=$pjt_id and emp_id='$emp_id' 
			union all
			select count(pjt_id) as member_cnt from project where pjt_id=$pjt_id and pjt_delete_flag = 'f'  
			and (pjt_response='$emp_id' or pjt_reg_emp_id='$emp_id')";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$pjt_member_flg = 0;
	if ($adm_flg == 't') {
		// 管理者権限ありの場合は変更可能
		$pjt_member_flg = 1;
	}

	while ($row = pg_fetch_array($sel)) {
		//委員会・ＷＧのメンバーであったらカウントされる
		if($row["member_cnt"] > 0)
		{
			$pjt_member_flg = 1;
		}
	}


	// 曜日行の表示
	echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">");
	echo("<tr height=\"22\">\n");
	echo("<td width=\"9%\" bgcolor=\"#f6f9ff\">&nbsp;</td>\n");
	for ($i = 0; $i < 7; $i++) {
		$tmp_wd = $start_wd + $i;
		if ($tmp_wd >= 7) {
			$tmp_wd -= 7;
		}
		switch ($tmp_wd) {
		case 0:
			$show_wd = "日";
			$bgcolor = "#fadede";
			break;
		case 1:
			$show_wd = "月";
			$bgcolor = "#fefcdf";
			break;
		case 2:
			$show_wd = "火";
			$bgcolor = "#fefcdf";
			break;
		case 3:
			$show_wd = "水";
			$bgcolor = "#fefcdf";
			break;
		case 4:
			$show_wd = "木";
			$bgcolor = "#fefcdf";
			break;
		case 5:
			$show_wd = "金";
			$bgcolor = "#fefcdf";
			break;
		case 6:
			$show_wd = "土";
			$bgcolor = "#defafa";
			break;
		}
		echo("<td width=\"13%\" align=\"center\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$show_wd</font></td>\n");
	}
	echo("</tr>\n");

	// 当月1日〜末日の配列を作成
	$year = date("Y", $date);
	$month = date("m", $date);
	$arr_date = array();
	for ($i = 1; $i <= 31; $i++) {
		$day = substr("0" . $i, -2);
		if (checkdate($month, $day, $year)) {
			array_push($arr_date, "$year$month$day");
		} else {
			break;
		}
	}

	// 空白セル分の日付を配列の先頭に追加
	$first_day = mktime(0, 0, 0, $month, "01", $year);
	$empty = date("w", $first_day) - $start_wd;
	if ($empty < 0) {
		$empty += 7;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
	}

	// 空白セル分の日付を配列の末尾に追加
	$day = substr($arr_date[count($arr_date) - 1], -2);
	$end_day = mktime(0, 0, 0, $month, $day, $year);
	$empty = 7 - (count($arr_date) % 7);
	if ($empty == 7) {
		$empty = 0;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
	}

	// 配列を週単位に分割
	$arr_week = array_chunk($arr_date, 7);

	// 「週表示」リンク出力用インデックスを求める
	$index = date("w", $date) - $start_wd;
	if ($index < 0) {
		$index += 7;
	}

	$start_date = "$year{$month}01";
	$end_date = "$year{$month}31";
	// カレンダーのメモを取得
	$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);

	// 予定行を表示
	foreach ($arr_week as $arr_date) {
		echo("<tr height=\"80\">\n");

		// ＜週間＞セルを出力（リンク先は処理日と同じ曜日）
		$tmp_year = substr($arr_date[$index], 0, 4);
		$tmp_month = substr($arr_date[$index], 4, 2);
		$tmp_day = substr($arr_date[$index], 6, 2);
		$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
		echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"project_schedule_week_menu.php?session=$session&pjt_id=$pjt_id&date=$tmp_date&adm_flg=$adm_flg\">&lt;週間&gt;</a></font></td>\n");

		// 日付セルを出力
		foreach ($arr_date as $tmp_ymd) {
			$tmp_year = substr($tmp_ymd, 0, 4);
			$tmp_month = substr($tmp_ymd, 4, 2);
			$tmp_day = substr($tmp_ymd, 6, 2);
			$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
			$wd = date("w", $tmp_date);

			if ($tmp_ymd == date("Ymd")) {
				$bgcolor = "#ccffcc";
			} else {
				if ($wd == 6) {
					$bgcolor = "#defafa";
				} else if ($wd == 0) {
					$bgcolor = "#fadede";
				} else {
					$bgcolor = "#fefcdf";
				}
			}

			// 他の月の場合は空表示
			if ($tmp_month != $month) {
				echo("<td bgcolor=\"$bgcolor\">&nbsp;</td>\n");

			// 当月の場合は詳細表示
			} else {
				$holiday_name = ktHolidayName($tmp_date);
				if ($tmp_ymd == date("Ymd")) {
					$bgcolor = "#ccffcc";
				} else if ($holiday_name != "") {
					$bgcolor = "#fadede";
				} else if ($arr_calendar_memo["{$tmp_ymd}_type"] == "5") {
					$bgcolor = "#defafa";
				}
				$tmp_day = intval($tmp_day);

				echo("<td bgcolor=\"$bgcolor\" valign=\"top\">\n");

				echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
				echo("<tr>\n");
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b><a href=\"project_schedule_menu.php?session=$session&pjt_id=$pjt_id&date=$tmp_date&adm_flg=$adm_flg\">$tmp_day</a></b></font><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_schd_type_name</font></td>\n");
				
				if($pjt_member_flg > 0)
				{
					//委員会・ＷＧのメンバーなのでリンクを表示する→メンバーでなければ画像のみを表示する
					echo("<td align=\"right\"><img src=\"img/pencil.gif\" alt=\"\" width=\"13\" height=\"13\" onclick=\"window.open('project_schedule_registration.php?session=$session&pjt_id=$pjt_id&date=$tmp_date', 'newwin', 'width=640,height=700,scrollbars=yes');\" style=\"cursor:pointer;\"></td>\n");
				}
				else
				{
					echo("<td align=\"right\"></td>\n");
				}
				echo("</tr>\n");
				echo("</table>\n");

				// カレンダーのメモがある場合は設定する
				if ($arr_calendar_memo["$tmp_ymd"] != "") {
					if ($holiday_name == "") {
						$holiday_name = $arr_calendar_memo["$tmp_ymd"];
					} else {
						$holiday_name .= "<br>".$arr_calendar_memo["$tmp_ymd"];
					}
				}

				if ($holiday_name != "") {
					echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
					echo("<tr>\n");
					echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">$holiday_name</font></td>\n");
					echo("</tr>\n");
					echo("</table>\n");
				}

				display_schedule_list($con, $pjt_id, $emp_id, $tmp_ymd, $session, $fname);

				echo("</td>\n");
			}
		}
		echo("</tr>\n");
	}
	echo("</table>\n");
}

// スケジュール一覧を出力
function display_schedule_list($con, $pjt_id, $emp_id, $ymd, $session, $fname) {

	// スケジュール情報を配列で取得
	$schedules = get_schedules_array($con, $pjt_id, $emp_id, $ymd, $fname);
	$schedule_count = count($schedules);
	if (count($schedule_count) == 0) {
		return;
	}

	$year = substr($ymd, 0, 4);
	$month = substr($ymd, 4, 2);
	$day = substr($ymd, 6, 2);
	$date = mktime(0, 0, 0, $month, $day, $year);
	$formatted_date = "$year/$month/$day";

	// スケジュールを表示
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	for ($i = 0; $i < $schedule_count; $i++) {
		$tmp_object_type = $schedules[$i]["object_type"];
		$tmp_timeless = $schedules[$i]["timeless"];
		$tmp_schedule_id = $schedules[$i]["schedule_id"];
		$tmp_title = $schedules[$i]["title"];
		$tmp_place = $schedules[$i]["place"];
		$tmp_place_detail = $schedules[$i]["place_detail"];
		$tmp_facility = $schedules[$i]["facility"];
		$tmp_fcl_detail = $schedules[$i]["fcl_detail"];
		$tmp_start_time = substr($schedules[$i]["start_time"], 0, 5);
		$tmp_end_time = substr($schedules[$i]["end_time"], 0, 5);
		$tmp_schedule_type = $schedules[$i]["schedule_type"];
		$tmp_detail = preg_replace("/\r?\n/", "<br>", $schedules[$i]["detail"]);
		$tmp_project_id = $schedules[$i]["project_id"];
		$tmp_contact = preg_replace("/\r?\n/", "<br>", $schedules[$i]["contact"]);
		$tmp_place_id = $schedules[$i]["schd_place_id"];
		$tmp_regname = $schedules[$i]["regname"];
		
		if ($tmp_place_id == "0") {
			$tmp_place = "その他";
		}
		if ($tmp_place_detail != "") {
			$tmp_place .= " $tmp_place_detail";
		}
		if ($tmp_facility == "") {
			$tmp_facility = "全体";
		}
		if ($tmp_fcl_detail != "") {
			$tmp_facility .= " $tmp_fcl_detail";
		}

		// 時間の設定
		if ($tmp_timeless) {
			$timeless = "t";
			$formatted_time1 = "<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
			$formatted_time2 = "指定なし";
		} else {
			$timeless = "";
			$formatted_time1 = "{$tmp_start_time}-{$tmp_end_time}<br>";
			$formatted_time2 = "$tmp_start_time 〜 $tmp_end_time";
		}

		// ボーダーの設定
		if ($i < $schedule_count - 1) {
			$style = "padding:1px;border-bottom:silver dotted 1px;";
		} else {
			$style = "padding:1px;";
		}

		echo("<tr>\n<td style=\"$style\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$formatted_time1");
		switch ($tmp_object_type) {

		case "project":  // 委員会・WGスケジュール
		
			echo("<a href=\"javascript:void(0);\" 
			onclick=\"window.open('project_schedule_update.php?session=$session&pjt_schd_id=$tmp_schedule_id&pjt_id=$tmp_project_id&date=$date&time=$tmp_start_time&timeless=$timeless', 'newwin', 'width=640,height=480,scrollbars=yes');\" 
			onmousemove=\"popupScheduleDetail('$tmp_title', '$tmp_place', '$formatted_date', '$formatted_time2', '$tmp_schedule_type', '$tmp_detail', '$tmp_regname', event);\" 
			onmouseout=\"closeDetail();\">$tmp_title</a>");
			break;

		case "timeguide":  // 行事
			echo("<font color=\"red\">行事：</font><a href=\"javascript:void(0);\" onclick=\"window.open('time_guide_open_detail.php?session=$session&e_id=$tmp_schedule_id', 'newwin', 'width=640,height=480,scrollbars=yes');\" onmousemove=\"popupEventDetail('$tmp_facility', '$formatted_date', '$formatted_time2', '$tmp_title', '$tmp_detail', '$tmp_contact', event);\" onmouseout=\"closeDetail();\">$tmp_title</a>");
			break;

		}
		echo("</font></td>\n</tr>\n");
	}
	echo("</table>\n");
}

// スケジュール情報を配列で取得
function get_schedules_array($con, $pjt_id, $emp_id, $ymd, $fname) {
	$timeless_project_schedules = get_timeless_project_schedule_array($con, $pjt_id, $ymd, $fname);
	$timeless_events = get_timeless_event_array($con, $emp_id, $ymd, $fname);
	$normal_project_schedules = get_normal_project_schedule_array($con, $pjt_id, $ymd, $fname);
	$normal_events = get_normal_event_array($con, $emp_id, $ymd, $fname);

	$schedules = array_merge($timeless_project_schedules, $timeless_events, $normal_project_schedules, $normal_events);
	usort($schedules, "sort_scehdules");

	return $schedules;
}

// 時間指定のない委員会・WGスケジュールを配列で取得
function get_timeless_project_schedule_array($con, $pjt_id, $ymd, $fname) {
	$sql = "select proschd2.pjt_schd_id, empmst.emp_lt_nm, empmst.emp_ft_nm, proschd2.pjt_schd_title, scheduleplace.place_name, proschd2.pjt_schd_plc, proschdtype.type_name, proschd2.pjt_schd_detail, proschd2.pjt_id, proschd2.pjt_schd_place_id 
	from (proschd2 inner join proschdtype on proschdtype.type_id = proschd2.pjt_schd_type) 
	left join scheduleplace on scheduleplace.place_id = proschd2.pjt_schd_place_id
	inner join empmst on proschd2.emp_id = empmst.emp_id ";
	$cond = "where proschd2.pjt_id = $pjt_id and to_char(proschd2.pjt_schd_start_date, 'YYYYMMDD') = '$ymd' order by proschd2.pjt_schd_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$ret = array();
	while ($row = pg_fetch_array($sel)) {
		$schedule = array();
		$schedule["object_type"] = "project";
		$schedule["timeless"] = true;
		$schedule["schedule_id"] = $row["pjt_schd_id"];
		$schedule["title"] = $row["pjt_schd_title"];
		$schedule["place"] = $row["place_name"];
		$schedule["place_detail"] = $row["pjt_schd_plc"];
		$schedule["schedule_type"] = $row["type_name"];
		$schedule["detail"] = $row["pjt_schd_detail"];
		$schedule["project_id"] = $row["pjt_id"];
		$schedule["schd_place_id"] = $row["pjt_schd_place_id"];
		$schedule["regname"] = $row["emp_lt_nm"].$row["emp_ft_nm"];
		$ret[] = $schedule;
	}
	return $ret;
}

// 時間指定のない行事を配列で取得
function get_timeless_event_array($con, $emp_id, $ymd, $fname) {
	$sql = "select timegd.event_id, timegd.event_name, timegdfcl.name as fcl_name, timegd.event_content, timegd.fcl_detail, timegd.contact from timegd left join timegdfcl on timegdfcl.fcl_id = timegd.fcl_id";
	$cond = "where exists (select * from timecheck where emp_id = '$emp_id' and ownother = '1' and timecheck = true) and to_char(timegd.event_date, 'YYYYMMDD') = '$ymd' and timegd.time_flg = false order by timegd.event_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$ret = array();
	while ($row = pg_fetch_array($sel)) {
		$schedule = array();
		$schedule["object_type"] = "timeguide";
		$schedule["timeless"] = true;
		$schedule["schedule_id"] = $row["event_id"];
		$schedule["title"] = $row["event_name"];
		$schedule["facility"] = $row["fcl_name"];
		$schedule["fcl_detail"] = $row["fcl_detail"];
		$schedule["detail"] = $row["event_content"];
		$schedule["contact"] = $row["contact"];
		$ret[] = $schedule;
	}
	return $ret;
}

// 時間指定のある委員会・WGスケジュールを配列で取得
function get_normal_project_schedule_array($con, $pjt_id, $ymd, $fname) {
	$sql = "select proschd.pjt_schd_id, empmst.emp_lt_nm, empmst.emp_ft_nm, proschd.pjt_schd_title, scheduleplace.place_name, 
			proschd.pjt_schd_plc, proschd.pjt_schd_start_time, proschd.pjt_schd_dur, proschdtype.type_name, proschd.pjt_schd_detail, 
			proschd.pjt_id, proschd.pjt_schd_start_time_v, proschd.pjt_schd_dur_v, proschd.pjt_schd_place_id 
			from (proschd inner join proschdtype on proschdtype.type_id = proschd.pjt_schd_type) 
			left join scheduleplace on scheduleplace.place_id = proschd.pjt_schd_place_id
			inner join empmst on proschd.emp_id = empmst.emp_id ";
	$cond = "where proschd.pjt_id = $pjt_id and to_char(proschd.pjt_schd_start_date, 'YYYYMMDD') = '$ymd' order by proschd.pjt_schd_start_time_v, proschd.pjt_schd_dur_v, proschd.pjt_schd_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$ret = array();
	while ($row = pg_fetch_array($sel)) {
		$schedule = array();
		$schedule["object_type"] = "project";
		$schedule["timeless"] = false;
		$schedule["schedule_id"] = $row["pjt_schd_id"];
		$schedule["title"] = $row["pjt_schd_title"];
		$schedule["place"] = $row["place_name"];
		$schedule["place_detail"] = $row["pjt_schd_plc"];
		$schedule["start_time"] = substr($row["pjt_schd_start_time_v"],0,2).":".substr($row["pjt_schd_start_time_v"],2,2);
		$schedule["end_time"] = substr($row["pjt_schd_dur_v"],0,2).":".substr($row["pjt_schd_dur_v"],2,2);
		$schedule["schedule_type"] = $row["type_name"];
		$schedule["detail"] = $row["pjt_schd_detail"];
		$schedule["project_id"] = $row["pjt_id"];
		$schedule["schd_place_id"] = $row["pjt_schd_place_id"];
		$schedule["regname"] = $row["emp_lt_nm"].$row["emp_ft_nm"];
		$ret[] = $schedule;
	}
	return $ret;
}

// 時間指定のある行事を配列で取得
function get_normal_event_array($con, $emp_id, $ymd, $fname) {
	$sql = "select timegd.event_id, timegd.event_name, timegdfcl.name as fcl_name, timegd.event_time, timegd.event_dur, timegd.event_content, timegd.fcl_detail, timegd.contact from timegd left join timegdfcl on timegdfcl.fcl_id = timegd.fcl_id";
	$cond = "where exists (select * from timecheck where emp_id = '$emp_id' and ownother = '1' and timecheck = true) and to_char(timegd.event_date, 'YYYYMMDD') = '$ymd' and timegd.time_flg = true order by timegd.event_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$ret = array();
	while ($row = pg_fetch_array($sel)) {
		$schedule = array();
		$schedule["object_type"] = "timeguide";
		$schedule["timeless"] = false;
		$schedule["schedule_id"] = $row["event_id"];
		$schedule["title"] = $row["event_name"];
		$schedule["facility"] = $row["fcl_name"];
		$schedule["fcl_detail"] = $row["fcl_detail"];
		$schedule["start_time"] = substr($row["event_time"], 0, 5);
		$schedule["end_time"] = substr($row["event_dur"], 0, 5);
		$schedule["detail"] = $row["event_content"];
		$schedule["contact"] = $row["contact"];
		$ret[] = $schedule;
	}
	return $ret;
}

// スケジュールを表示順に並べ替え
function sort_scehdules($schedule1, $schedule2) {
	if ($schedule1["start_time"] != $schedule2["start_time"]) {
		return strcmp($schedule1["start_time"], $schedule2["start_time"]);
	}

	if ($schedule1["object_type"] != $schedule2["object_type"]) {
		return strcmp($schedule1["object_type"], $schedule2["object_type"]);
	}

	if ($schedule1["end_time"] != $schedule2["end_time"]) {
		return strcmp($schedule1["end_time"], $schedule2["end_time"]);
	}

	return strcmp($schedule1["schedule_id"], $schedule2["schedule_id"]);
}

// 1週間分の日付をタイムスタンプで配列に格納
function get_date_array($date, $start_wd) {
	$diff = date("w", $date) - $start_wd;
	if ($diff < 0) {
		$diff += 7;
	}
	$start_date = strtotime("-$diff days", $date);

	$arr_result = array();
	for ($i = 0; $i < 7; $i++) {
		array_push($arr_result, strtotime("+$i days", $start_date));
	}
	return $arr_result;
}
?>
