<?
ob_start();
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("aclg_set.php");
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_db_class.php");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');

require_once("hiyari_auth_class.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//テスト用。
//TODO リリース時はコメントアウト必須。
//ini_set("display_errors","1");

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);

$is_excel = false;
if($_POST['is_postback'] == 'true') {
    switch ($mode) {
    case 'delete' :
        // 論理削除を行う
        foreach($delete_key as $value) {
            $sql = "UPDATE inci_analysis_regist SET ";
            $set = array('available');
            $dat = array('f');
            $cond = "WHERE analysis_id = {$value}";
            $db->update($sql, $set, $dat, $cond);
        }
        break;
    case 'excel':
        $is_excel = true;        
        break;
    default:
        break;
    }


}

//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if($search_date_year == "")
{
    $search_date_year = date("Y");
}
//日付検索(月)初期値は今月
if($search_date_month == "")
{
    $search_date_month = date("m");
}

// データ取得
$sql = "SELECT * FROM inci_analysis_regist WHERE available = 't'";

//日付による絞込み
if($search_date_mode == "month")
{
    $search_date_1 = $search_date_year."-".$search_date_month;
    $where .= " and substr(registed_date::text,0,11) like '$search_date_1%' ";
}
elseif($search_date_mode == "ymd_st_ed")
{
    if($search_date_ymd_st != "")
    {
        //$search_date_ymd_st = str_replace("/", "-", $search_date_ymd_st);
        $where .= " and substr(registed_date::text,0,11) >= '".str_replace("/", "-", $search_date_ymd_st)."' ";
    }
    if($search_date_ymd_ed != "")
    {
        //$search_date_ymd_ed = str_replace("/", "-", $search_date_ymd_ed);
        $where .= " and substr(registed_date::text,0,11) <= '".str_replace("/", "-", $search_date_ymd_ed)."' ";
    }
}
elseif($search_date_mode == "non")
{
    //絞り込み条件なし。
}

/******************************
 * 検索条件開始
 ******************************/
// 分析番号
if($search_analysis_no != "") {
    $search_analysis_no = $db->escape($search_analysis_no);
    $where .= " and analysis_no = '{$search_analysis_no}' ";
}

// 事案番号
if($search_problem_no != "") {
    $search_problem_no = $db->escape($search_problem_no);
    $where .= " and analysis_problem = '{$search_problem_no}'";
}

// タイトル
if($search_analysis_title != "") {
    $search_analysis_title = $db->escape($search_analysis_title);
    $title_keyword = mb_ereg_replace("　"," ",$search_analysis_title); //全角スペース除去
    $title_keyword = trim($title_keyword); //前後スペース除去
    $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

    $wk_str = "";
    //マルチワードについては常にOR結合
    for ($i=0; $i<count($title_keyword_list); $i++) {
        if ($i > 0) {
            if($title_and_or_div == 1 || $title_and_or_div == "") {
                $wk_str .= " and ";
            } else {
                $wk_str .= " or ";
            }
        }
        $wk_str .= " analysis_title like '%{$title_keyword_list[$i]}%' ";
    }
    $where .= " and ($wk_str) ";
}

// 概要
if($search_analysis_summary != "") {
    $search_analysis_summary = $db->escape($search_analysis_summary);
    $title_keyword = mb_ereg_replace("　"," ",$search_analysis_summary); //全角スペース除去
    $title_keyword = trim($title_keyword); //前後スペース除去
    $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

    $wk_str = "";
    //マルチワードについては常にOR結合
    for ($i=0; $i<count($title_keyword_list); $i++) {
        if ($i > 0) {
            if($title_and_or_div == 1 || $title_and_or_div == "") {
                $wk_str .= " and ";
            } else {
                $wk_str .= " or ";
            }
        }
        $wk_str .= " analysis_summary like '%{$title_keyword_list[$i]}%' ";
    }
    $where .= " and ($wk_str) ";
}

// 分析手法
if($search_analysis_method != "") {
    $search_analysis_method = $db->escape($search_analysis_method);
    $where .= " and analysis_method = '{$search_analysis_method}' ";
}

// 更新年月日
if($renewal_date_start_input != "") {
    $start_date = str_replace("/", "-", $renewal_date_start_input);
    $where .= " and renewal_date >= '{$start_date}' ";
}
if($renewal_date_end_input != "") {
    $end_date = str_replace("/", "-", $renewal_date_end_input);
    $where .= " and renewal_date <= '{$end_date}' ";
}

// 評価予定期日
if($assessment_date_start_input != "") {
    $start_date = str_replace("/", "-", $assessment_date_start_input);
    $where .= " and progress_date >= '{$start_date}' ";
}
if($assessment_date_end_input != "") {
    $end_date = str_replace("/", "-", $assessment_date_end_input);
    $where .= " and progress_date <= '{$end_date}' ";
}

// 進捗
if($search_analysis_progress != "") {
    $search_analysis_progress = $db->escape($search_analysis_progress);
    $where .= " and analysis_progress = '{$search_analysis_progress}' ";
}

$order_by = " ORDER BY analysis_id DESC";

//==============================
//SEHL分析データ
//==============================
$has_shel = false;
if ($search_analysis_method == "" || $search_analysis_method == "SHEL") {
    $shel_sql = 'SELECT r.report_title, a.analysis_problem, a.analysis_summary, s.* ';
    $shel_sql.= 'FROM inci_analysis_shel s ';
    $shel_sql.= 'LEFT JOIN inci_analysis_regist a on a.analysis_id = s.analysis_id ';
    $shel_sql.= 'LEFT JOIN inci_report r on r.report_id = cast(a.analysis_problem_id as integer) ';
    $shel_sql.= "WHERE available = 't'";
    if($search_analysis_method == "") {
        $shel_sql.= " and analysis_method = 'SHEL'";
    }
    $db->query($shel_sql.$where.$order_by);
    $array = $db->getAll();
    if (!empty($array)){
        //出力できるSHEL分析がある
        //一覧にSHEL分析があっても、内容が未登録（inci_analysis_shelにレコードがない状態）の場合、出力するものがないのでtrueにならない
        $has_shel = true;

        //出力実行
        if ($is_excel){
            shel_data_excel($array);
            exit;
        }
    }
}

//１ページ分のデータを取得
if($page == ""){
    $page = 1;
}
$disp_count_max_in_page = 20;
$offset = $disp_count_max_in_page * ($page - 1);
$offset_limit = " offset $offset limit $disp_count_max_in_page";

$db->query($sql.$where.$order_by.$offset_limit);
$analysis_data = $db->getAll();

// 参照権限 20141023
$auth_list = array();
$auth_obj = new hiyari_auth_class($con, $fname, $session);
$auth_list[$auth_obj->_auth] = $auth_obj->_auth; 
foreach ($auth_obj->_my_inci_auth_list as $auth_item) {			
	$auth_list[$auth_item["auth"]] = $auth_item["auth"];
}

$con_id = implode("','", $auth_list);
$auth_sql  = "SELECT analysis_auth FROM inci_analysis_auth ";
$auth_sql .= "where analysis_auth in ('$con_id') and analysis_read_flg = 't' and analysis_access_flg = 't' ";
$db->query($auth_sql);
$auth_info = $db->getAll();

$sm_flg = is_sm_emp($session,$fname,$con);


//総件数取得
$sql = str_replace('SELECT *', 'SELECT count(*)', $sql);
$db->query($sql.$where);

$rec_count = $db->getOne();

//最大ページ数
if($rec_count == 0){
    $page_max  = 1;
}
else{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("file", $file);

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("page_title", "出来事分析");
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//------------------------------
//カレンダー
//------------------------------
$smarty->assign("search_date_mode", $search_date_mode);

//年月指定
$smarty->assign("search_date_year", $search_date_year);
$smarty->assign("search_date_month", $search_date_month);

//開始日・終了日指定
$smarty->assign("search_date_ymd_st", $search_date_ymd_st);
$smarty->assign("search_date_ymd_ed", $search_date_ymd_ed);

//------------------------------
//検索パラメータ
//------------------------------
$smarty->assign("search_toggle", $search_toggle);

$smarty->assign("search_analysis_no", h($search_analysis_no));
$smarty->assign("search_problem_no", h($search_problem_no));
$smarty->assign("search_analysis_title", h($search_analysis_title));
$smarty->assign("search_analysis_summary", h($search_analysis_summary));
$smarty->assign("search_analysis_method", $search_analysis_method);
$smarty->assign("renewal_date_start_input", str_replace("-", "/", $renewal_date_start_input));
$smarty->assign("renewal_date_end_input", str_replace("-", "/", $renewal_date_end_input));
$smarty->assign("assessment_date_start_input", str_replace("-", "/", $assessment_date_start_input));
$smarty->assign("assessment_date_end_input", str_replace("-", "/", $assessment_date_end_input));
$smarty->assign("search_analysis_progress", $search_analysis_progress);

//------------------------------
//実行アイコン
//------------------------------
$smarty->assign("is_analysis_update_usable", is_analysis_update_usable($session, $fname, $con));
$smarty->assign("has_shel", $has_shel);

//------------------------------
//ログインユーザ情報
//------------------------------
$login_user_id = get_emp_id($con,$session,$fname);
$smarty->assign("login_user_id", $login_user_id);
$smarty->assign("is_sm", is_inci_auth_emp_of_auth($session,$fname,$con,"SM"));

//------------------------------
//一覧の内容
//------------------------------
$list = array();
foreach($analysis_data as $i => $analysis_datum) {
	
	// 作成者、SM担当者、分析メンバー、参照権限者 20141023 
	$access_authority_flg = true;
	$analysis_member_id = explode(",", $analysis_datum["analysis_member_id"]);
	$member_flg = in_array($login_user_id, $analysis_member_id);
	$readable_flg = $auth_obj->get_analysis_disp_auth($analysis_datum["analysis_problem_id"]);
	if (!empty($auth_info)){
		if ($login_user_id == $analysis_datum['registed_user_id'] || $sm_flg || $member_flg || $readable_flg) {
			$access_authority_flg = true;
		}else{
			$access_authority_flg = false;
		}
	}
	
 	if ($access_authority_flg){ 
		$list[$i]['registed_user_id'] = $analysis_datum['registed_user_id'];
	    $list[$i]['id'] = $analysis_datum['analysis_id'];
	    $list[$i]['no'] = $analysis_datum['analysis_no'];
	    $list[$i]['problem'] = $analysis_datum['analysis_problem'];
	    $list[$i]['method'] = trim($analysis_datum['analysis_method']);
	    $list[$i]['title'] = $analysis_datum['analysis_title'];
	    $list[$i]['renewal_date'] = str_replace("-", "/", $analysis_datum['renewal_date']);
	    $list[$i]['progress_date'] = str_replace("-", "/", $analysis_datum['progress_date']);
	    switch($analysis_datum['analysis_progress']){
	        case '1':
	            $list[$i]['progress'] = "分析中";
	            break;
	        case '2':
	            $list[$i]['progress'] = "分析済";
	            break;
	        case '3':
	            $list[$i]['progress'] = "評価中";
	            break;
	        case '4':
	            $list[$i]['progress'] = "評価済";
	            break;
	    }
	    if (trim($analysis_datum['analysis_method'])=='general') {
	        $list[$i]['method_name'] = "汎用";
	    }
	    else {
	        $list[$i]['method_name'] = $analysis_datum['analysis_method'];
	    }
	}
}
$smarty->assign("list", $list);

//------------------------------
//ソート
//------------------------------
$smarty->assign("sort_item", $sort_item);
$smarty->assign("sort_div", $sort_div);

//------------------------------
//ページング用データ
//------------------------------
$smarty->assign("page", $page);
$smarty->assign("page_max", $page_max);
$page_list = array();
$page_stt = max(1, $page - 3);
$page_end = min($page_stt + 6, $page_max);
$page_stt = max(1, $page_end - 6);
for ($i=$page_stt; $i<=$page_end; $i++){
    $page_list[] = $i;
}
$smarty->assign("page_list", $page_list);

if ($design_mode == 1){
    $smarty->display("hiyari_analysis1.tpl");
}
else{
    $smarty->display("hiyari_analysis2.tpl");
}

//======================================================================================================================================================
//内部関数
//======================================================================================================================================================
/**
 * SHEL分析データをHTML形式に変換し、Excel出力する
 *
 * @param array $shel_data SHEL分析データ配列
 */
function shel_data_excel($shel_data)
{
    //==============================
    // HTML作成
    //==============================
    $style = 'style="border:#c8c8c8 solid 1px;"';
    $style_c = 'style="border:#c8c8c8 solid 1px; background-color:rgb(255,255,0);"';

    $message = array();

    // metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
    $message[] = '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';

    $message[] = '<table width="100%">';

    //ヘッダー
    $message[] = '<tr>';
    $message[] = '<th '.$style.'>分類</th>';
    $message[] = '<th '.$style_c.'>事案番号</th>';
    $message[] = '<th '.$style.'>概要</th>';
    $message[] = '<th '.$style.'>Software</th>';
    $message[] = '<th '.$style.'>Hardware</th>';
    $message[] = '<th '.$style.'>Environment</th>';
    $message[] = '<th '.$style.'>Liveware(関与者)</th>';
    $message[] = '<th '.$style.'>Liveware(当事者)</th>';
    $message[] = '<th '.$style.'>対策</th>';
    $message[] = '<th '.$style.'>評価</th>';
    $message[] = '</tr>';

    //SHEL分析データ
    $br = "<br style='mso-data-placement:same-cell'>";
    foreach ($shel_data as $row) {
        $data = array();
        foreach ($row as $key=>$col){
            $data[$key] = str_replace(array("\r\n","\r","\n"), $br, $col);
        }

        $message[] = '<tr>';
        $message[] = '<td '.$style.'>'.$data['report_title'].'</td>';
        $message[] = '<td '.$style_c.'>'.$data['analysis_problem'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_summary'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_ls_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_lh_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_le_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_ll_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_free_factor'].'</td>';
        $step = $data['analysis_ls_step'].$br;
        $step.= $data['analysis_lh_step'].$br;
        $step.= $data['analysis_le_step'].$br;
        $step.= $data['analysis_ll_step'].$br;
        $step.= $data['analysis_free_step'];
        $message[] = '<td '.$style.'>'.$step.'</td>';
        $message[] = '<td '.$style.'>&nbsp;</td>';
        $message[] = '</tr>';
    }

    $message[] ="</table>";

    $download_data =  implode($message, "");

    //==============================
    //EXCEL出力
    //==============================
    
    //ファイル名
    $filename = 'fantol_analysis_list_shel.xls';
    if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
        $encoding = 'sjis';
    }
    else {
        $encoding = mb_http_output();   // Firefox
    }
    $filename = mb_convert_encoding($filename,$encoding,mb_internal_encoding());

    //出力
    ob_clean();
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename=' . $filename);
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
    header('Pragma: public');
    echo mb_convert_encoding(nl2br($download_data), 'sjis', mb_internal_encoding());
    ob_end_flush();
}
