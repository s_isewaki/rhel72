<?

// 出張旅費
// 報告書PDF印刷
ini_set("max_execution_time", 0);
require_once("about_comedix.php");
ob_start();
require_once("workflow_trip_common.php");
require_once("application_workflow_common_class.php");
require_once("application_imprint_common.ini");
require_once('fpdf153/mbfpdf.php');
ob_end_clean();

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);
if ($con == "0") {
    js_login_exit();
}

// パラメータ
$apply_id = @$_REQUEST["apply_id"];

// 交通手段
$array_transport = get_trip_transport();

// obj
$obj = new application_workflow_common_class($con, $fname);

//-----------------------------------------------
// 報告書
//-----------------------------------------------
$sql = <<<__SQL_END__
SELECT
    a.*,
    w.short_wkfw_name,
    w.wkfw_title,
    w.approve_label,
    e.emp_personal_id,
    e.emp_lt_nm,
    e.emp_ft_nm,
    b1.class_nm,
    b2.atrb_nm,
    b3.dept_nm,
    b4.room_nm
FROM apply a
    JOIN empmst e USING (emp_id)
    JOIN wkfwmst w USING (wkfw_id)
    LEFT JOIN classmst b1 ON a.emp_class=b1.class_id
    LEFT JOIN atrbmst b2 ON a.emp_attribute=b2.atrb_id
    LEFT JOIN deptmst b3 ON a.emp_dept=b3.dept_id
    LEFT JOIN classroom b4 ON a.emp_room=b4.room_id
WHERE a.delete_flg='f'
  AND a.apply_id = '{$apply_id}'
__SQL_END__;

$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    js_error_exit();
}
$trip2 = pg_fetch_assoc($sel);
$trip2_xml = get_array_apply_content($trip2["apply_content"]);
$trip2_apv = $obj->get_applyapv($apply_id);

//-----------------------------------------------
// 出張届
//-----------------------------------------------
$sql = <<<__SQL_END__
SELECT
    a.*,
    w.short_wkfw_name,
    w.wkfw_title,
    w.approve_label
FROM apply a
    JOIN empmst e USING (emp_id)
    JOIN wkfwmst w USING (wkfw_id)
WHERE a.delete_flg='f'
  AND a.apply_id = '{$trip2_xml['trip1']}'
__SQL_END__;

$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    js_error_exit();
}
$trip1 = pg_fetch_assoc($sel);
$trip1_xml = get_array_apply_content($trip1["apply_content"]);
$trip1_apv = $obj->get_applyapv($trip2_xml['trip1']);

//-----------------------------------------------
// 申請番号
//-----------------------------------------------
$y = substr($trip2["apply_date"], 0, 4);
$md = substr($trip2["apply_date"], 4, 4);
if ($md >= "0101" && $md <= "0331") {
    $y = $y - 1;
}
$apply_number = sprintf("%s-%s%04d", $trip2["short_wkfw_name"], $y, $trip2["apply_no"]);

//-----------------------------------------------
// PDF
//-----------------------------------------------
$base_row = "6.0";
$tt_row = "5.0";

class CustomMBFPDF extends MBFPDF {

    var $now;
    var $apply_number;

    function SetHeader1Caption($apply_number) {
        $this->now = date('Y/m/d H:i');
        $this->apply_number = $apply_number;
    }

    function Header() {
        $this->SetFont(PGOTHIC, '', 6);
        $this->Cell(0, 4, "KD総務-AD00213(00)", 0, 1, "R");
        $this->SetFont(PMINCHO, '', 7);
        $this->Cell(20, 4, "出力日時：" . $this->now, 0, 0);
        $this->Cell(0, 4, "申請番号：" . $this->apply_number, 0, 1, "R");
    }

    function Footer() {
        $this->SetFont(PMINCHO, 'B', 6);
        $this->SetY(-15); //下から1.5センチのところ
        $this->Cell(0, 10, $this->PageNo() . '', 0, 0, 'C');
    }

}

$pdf = new CustomMBFPDF();
$pdf->AddMBFont(PMINCHO, 'EUC-JP');
$pdf->AddMBFont(PGOTHIC, 'EUC-JP');
$pdf->SetHeader1Caption($apply_number);
$pdf->Open();
$pdf->AddPage();
$pdf->SetFillColor(200);

// タイトル
$pdf->SetFont(PGOTHIC, 'B', 14);
$pdf->Cell(0, 8, "出 張 報 告 書", 0, 1, 'C');

// 報告者情報
$pdf->SetFont(PMINCHO, '', 7);
$pdf->Cell(180, $base_row,
           "所属：{$trip2["class_nm"]}＞{$trip2["atrb_nm"]}＞{$trip2["dept_nm"]}  職員番号：{$trip2["emp_personal_id"]}  氏名：{$trip2["emp_lt_nm"]} {$trip2["emp_ft_nm"]}   印",
           0, 1, "R");

// 承認印
$pdf->SetFont(PMINCHO, '', 6);
$apv_cnt = count($trip2_apv);
$line_cnt = $apv_cnt < 10 ? $apv_cnt : 10;
$mark_x = 190 - (15 * $line_cnt) + 10;
$mark_y = $pdf->GetY();
$mark_cnt = 0;
foreach (array_reverse($trip2_apv) as $apv) {
    $pdf->setXY($mark_x, $mark_y);
    $pdf->Cell(15, 3, $apv["st_nm"], 1, 2, "C", 1);
    $pdf->Rect($mark_x, $mark_y + 3, 15.0, 12.0, '');

    $pdf->setXY($mark_x, $mark_y + 3);
    switch ($apv["apv_stat"]) {
        case 1:
            $imprint_flg = get_imprint_flg($con, $apv["emp_id"], $fname);
            if ($imprint_flg == 't') {
                if (is_file("workflow/imprint/{$apv["emp_id"]}.jpg")) {
                    $pdf->Image("workflow/imprint/{$apv["emp_id"]}.jpg", $mark_x + 2.5, $mark_y + 4,
                                10.0, 10.0);
                }
                else {
                    $pdf->Cell(15, 12, "承認済み", 0, 0, "C");
                }
            }
            else {
                $pdf->Cell(15, 12, "承認済み", 0, 0, "C");
            }
            break;
        case 2:
            $pdf->Cell(15, 12, "否認", 0, 0, "C");
            break;
        case 3:
            $pdf->Cell(15, 12, "差戻し", 0, 0, "C");
            break;
    }

    $pdf->setXY($mark_x, $mark_y + 15);
    $pdf->Cell(15, 3, $apv["emp_lt_nm"] . " " . $apv["emp_ft_nm"], 1, 1, "C");

    $mark_x += 15;
    $mark_cnt++;
    if ($mark_cnt >= 10) {
        $apv_cnt -= 10;
        $line_cnt = $apv_cnt < 10 ? $apv_cnt : 10;
        $mark_x = 190 - (15 * $line_cnt) + 10;
        $mark_y += 18;
        $mark_cnt = 0;
    }
}
$pdf->Cell(0, 1, 0, 0, 1);

// 種別
$pdf->SetFont(PMINCHO, '', 7);
$pdf->SetFont(PMINCHO, '', 8);
$pdf->Cell(20, $base_row, "種 別", 1, 0, "C", 1);
$pdf->Cell(0, $base_row, $array_kubun[$trip1_xml["kubun"]], 1, 1);

// 期間
$pdf->Cell(20, $base_row, "期 間", 1, 0, "C", 1);
$pdf->Cell(0, $base_row,
           sprintf("%s年%s月%s日 〜 %s年%s月%s日", $trip1_xml["date_y1"], $trip1_xml["date_m1"],
                   $trip1_xml["date_d1"], $trip1_xml["date_y2"], $trip1_xml["date_m2"],
                   $trip1_xml["date_d2"]), 1, 1);

// 研修先
$pdf->Cell(20, $base_row, "出張先", 1, 0, "C", 1);
$pdf->Cell(50, $base_row, "{$array_pref[$trip1_xml["pref"]]} {$trip1_xml["city"]}", 1);
$pdf->Cell(30, $base_row, "出張場所(会場名)", 1, 0, "C", 1);
$pdf->Cell(0, $base_row, "{$trip1_xml["place"]}", 1, 1);

// 要件
if ($trip1_xml["youken"] == 1) {
    $pdf->SetFont(PMINCHO, '', 7);
    // 学会名
    $pdf->Cell(20, $base_row, "", "LTR", 0, "C", 1);
    $pdf->Cell(22, $base_row, "名称：", 0, 0, "R");
    $pdf->Cell(0, $base_row, $trip1_xml["gakkai_name"], "R", 1);

    // 主催者
    $pdf->Cell(20, $base_row, "", "LR", 0, "C", 1);
    $pdf->Cell(22, $base_row, "主催者：", 0, 0, "R");
    $pdf->Cell(0, $base_row, $trip1_xml["gakkai_organizer"], "R", 1);

    // 学会期間
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "要 件", "LR", 0, "C", 1);
    $pdf->SetFont(PMINCHO, '', 7);
    $pdf->Cell(22, $base_row, "期間：", 0, 0, "R");
    $pdf->Cell(0, $base_row,
               sprintf("%s年%s月%s日 〜 %s年%s月%s日", $trip1_xml["date_y3"], $trip1_xml["date_m3"],
                       $trip1_xml["date_d3"], $trip1_xml["date_y4"], $trip1_xml["date_m4"],
                       $trip1_xml["date_d4"]), "R", 1);

    // 演題発表
    $presentation = $array_presentation[$trip1_xml["presentation"]];
    if ($trip1_xml["presentation"] == 1) {
        $presentation .= "({$array_act[$trip1_xml["act"]]})";
    }
    $pdf->Cell(20, $base_row, "", "LR", 0, "C", 1);
    $pdf->Cell(22, $base_row, "演題発表：", 0, 0, "R");
    $pdf->Cell(0, $base_row, $presentation, "R", 1);

    // 演題名
    $pdf->Cell(20, $base_row, "", "LRB", 0, "C", 1);
    $pdf->Cell(22, $base_row, "演題名：", "B", 0, "R");
    $pdf->Cell(0, $base_row, $trip1_xml["presentation_titie"], "RB", 1);
}
else {
    list($x1, $y1) = array($pdf->GetX(), $pdf->GetY());
    $pdf->Cell(20, 0, "", 0, 0);
    $pdf->SetFont(PMINCHO, '', 7);
    $pdf->MultiCell(0, $base_row, $trip1_xml["gakkai_igai"], 1);
    list($x2, $y2) = array($pdf->GetX(), $pdf->GetY());
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->SetXY($x1, $y1);
    $pdf->Cell(20, $y2 - $y1, "要 件", 1, 1, "C", 1);
}

// 謝礼
$pdf->SetFont(PMINCHO, '', 8);
$pdf->Cell(20, $base_row, "謝 礼", 1, 0, "C", 1);
$pdf->Cell(40, $base_row, $array_sharei[$trip1_xml["sharei"]], 1, 0);
$pdf->Cell(20, $base_row, "前渡金", 1, 0, "C", 1);
$pdf->Cell(50, $base_row, "{$array_prepayment[$trip1_xml["prepayment"]]}", 1);
$pdf->Cell(20, $base_row, "前渡チケット", 1, 0, "C", 1);
$pdf->Cell(0, $base_row, $array_preticket[$trip1_xml["preticket"]], 1, 1);

// 空白行
$pdf->Cell(0, 2.0, "", 0, 1);

// 報告
list($x1, $y1) = array($pdf->GetX(), $pdf->GetY());
$pdf->Cell(20, 0, "", 0, 0);
$pdf->SetFont(PMINCHO, '', 7);
$pdf->MultiCell(0, $base_row, $trip2_xml["report"], 1);
list($x2, $y2) = array($pdf->GetX(), $pdf->GetY());
$pdf->SetFont(PMINCHO, '', 8);
$pdf->SetXY($x1, $y1);
$pdf->Cell(20, $y2 - $y1, "報 告", 1, 1, "C", 1);

// 期間
if ($trip2_xml["kikan2"] > 0) {
    $kikan = sprintf('%s日間（A）  %s日間（B : %s以降）', $trip2_xml["kikan"], $trip2_xml["kikan2"],
                     $trip2_xml["kikan2_date"]);
}
else {
    $kikan = sprintf('%s日間', $trip2_xml["kikan"]);
}

// 発着時間
$pdf->SetFont(PMINCHO, '', 8);
$pdf->Cell(20, $base_row, "発着時間", 1, 0, "C", 1);
$pdf->Cell(0, $base_row,
           sprintf("%s年%s月%s日 %s:%s 〜 %s年%s月%s日 %s:%s  :  %s", $trip2_xml["date_y1"],
                   $trip2_xml["date_m1"], $trip2_xml["date_d1"], $trip2_xml["date_h1"],
                   $trip2_xml["date_i1"], $trip2_xml["date_y2"], $trip2_xml["date_m2"],
                   $trip2_xml["date_d2"], $trip2_xml["date_h2"], $trip2_xml["date_i2"], $kikan), 1,
                   1);

// 宿泊地
$stay = "";
if (@$trip2_xml["stay"]) {
    foreach ($trip2_xml["stay"] as $s) {
        if ($s == 1) {
            $stay1_2 = "";
            if ($trip2_xml["stay1_days2"] > 0) {
                $stay1_2 = sprintf('(A) %s泊(B)', $trip2_xml["stay1_days2"]);
            }
            $stay .= sprintf("東京23区内 %s泊%s     ", $trip2_xml["stay1_days"], $stay1_2);
        }
        if ($s == 2) {
            $stay2_2 = "";
            if ($trip2_xml["stay2_days2"] > 0) {
                $stay2_2 = sprintf('(A) %s泊(B)', $trip2_xml["stay2_days2"]);
            }
            $stay .= sprintf("東京23区以外 (%s) %s泊%s ", $trip2_xml["stay2_city"],
                             $trip2_xml["stay2_days"], $stay2_2);
        }
    }
}
$pdf->Cell(20, $base_row, "宿泊地", 1, 0, "C", 1);
$pdf->Cell(0, $base_row, $stay, 1, 1);

// 空白行
$pdf->Cell(0, 1.0, "", 0, 1);

// 交通費内訳
$count_tt = 0;
if (!empty($trip2_xml["tt1_data"])) {
    $tt1 = cmx_json_decode($trip2_xml["tt1_data"], true);
    $count_tt += count($tt1) ? count($tt1) + 3 : 0;
}
if (!empty($trip2_xml["tt2_data"])) {
    $tt2 = cmx_json_decode($trip2_xml["tt2_data"], true);
    $count_tt += count($tt2) ? count($tt2) + 3 : 0;
}
if (!empty($trip2_xml["tt3_data"])) {
    $tt3 = cmx_json_decode($trip2_xml["tt3_data"], true);
    $count_tt += count($tt3) ? count($tt3) + 3 : 0;
}

// 改ページチェック
$next_page = false;
if ($pdf->GetY() > 200) {
    $next_page = true;
}
if (260 - $count_tt * $tt_row <= 140) {
    $next_page = true;
}
if (!$next_page) {
    tt_pdf($tt1, $tt2, $tt3);
}

// 空白行
$pdf->Cell(0, 1.0, "", 0, 1);

// 金額PDFリスト
$right_pdf = array('transit_pdf', 'entrance_pdf', 'prepayment_pdf', 'total_pdf');
$left_pdf = array('payment_pdf');
if ($trip2_xml["kikan2"] > 0) {
    $left_pdf[] = 'payment_pdf_b';
}
$left_pdf[] = 'tokyo_pdf';
if ($trip2_xml["stay1_days2"] > 0) {
    $left_pdf[] = 'tokyo_pdf_b';
}
$left_pdf[] = 'hotel_pdf';
if ($trip2_xml["stay2_days2"] > 0) {
    $left_pdf[] = 'hotel_pdf_b';
}
$pdf_cnt = count($left_pdf) > 4 ? count($left_pdf) : 4;

for ($i = 0; $i < $pdf_cnt; $i++) {
    // LEFT
    $l_pdf = array_shift($left_pdf);
    if ($l_pdf) {
        $l_pdf();
    }
    else {
        fill_left_pdf();
    }

    // RIGHT
    $r_pdf = array_shift($right_pdf);
    if ($r_pdf) {
        $r_pdf();
    }
    else {
        fill_right_pdf();
    }
}

// 次ページ
if ($next_page) {
    $pdf->AddPage();
    tt_pdf($tt1, $tt2, $tt3);
}

$pdf->Output();

pg_close($con);
die;

// 交通費
function transit_pdf() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "交通費合計", 1, 0, "C", 1);
    if ($trip1_xml["sharei"] == 2) {
        $pdf->Cell(55, $base_row, "謝礼本人受領", 1, 0, "C");
        $pdf->Cell(0, $base_row, "-", 1, 1, "C");
    }
    elseif (@$trip2_xml["transit_flg"] and $trip2_xml["transit_flg"][0] == 1) {
        $pdf->Cell(55, $base_row, "交通費不要", 1, 0, "C");
        $pdf->Cell(0, $base_row, "-", 1, 1, "C");
    }
    else {
        $pdf->Cell(55, $base_row, "", 1, 0);
        $pdf->Cell(0, $base_row,
                   number_format($trip2_xml["tt1_fee_total"] + $trip2_xml["tt2_fee_total"] + $trip2_xml["tt3_fee_total"]) . "円",
                                 1, 1, "R");
    }
}

// 参加費
function entrance_pdf() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "参加費", 1, 0, "C", 1);
    $pdf->Cell(55, $base_row, "", 1, 0);
    $pdf->Cell(20, $base_row, number_format($trip2_xml["entrance_fee"]) . "円", 1, 1, "R");
}

// 前渡金
function prepayment_pdf() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "前渡金", 1, 0, "C", 1);
    if ($trip1_xml["prepayment"] == 1) {
        $pdf->Cell(55, $base_row, "申請無し", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 1, "C");
    }
    else {
        $pdf->Cell(55, $base_row, "", 1, 0);
        $pdf->Cell(20, $base_row, number_format($trip2_xml["prepayment"]) . "円", 1, 1, "R");
    }
}

// 精算額
function total_pdf() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "精算額", 1, 0, "C", 1);
    $pdf->Cell(55, $base_row, "", 1, 0);
    $pdf->Cell(20, $base_row, number_format($trip2_xml["total_fee"]) . "円", 1, 1, "R");
}

// 空pdf
function fill_left_pdf() {
    global $pdf, $base_row;
    $pdf->Cell(95, $base_row, "", 0, 0);
}

function fill_right_pdf() {
    global $pdf, $base_row;
    $pdf->Cell(0, $base_row, "", 0, 1);
}

// 日当
function payment_pdf() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "日 当", 1, 0, "C", 1);
    if ($trip1_xml["kubun"] >= 6) {
        $pdf->Cell(55, $base_row, "自己研修", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    elseif ($trip1_xml["sharei"] == 2) {
        $pdf->Cell(55, $base_row, "謝礼本人受領", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    else {
        $pdf->Cell(4, $base_row, "", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "＠", "TB", 0, "R");
        $pdf->Cell(13, $base_row, number_format($trip2_xml["daily_fee"]) . "円", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
        $pdf->Cell(10, $base_row, $trip2_xml["daily_fee_days"] . "日", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
        $pdf->Cell(12, $base_row, $trip2_xml["daily_fee_percentage"] . "％", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "", "TB", 0, "R");
        $pdf->Cell(20, $base_row, number_format($trip2_xml["daily_fee_total"]) . "円", 1, 0, "R");
    }
}

// 日当(B)
function payment_pdf_b() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    if ($trip1_xml["kubun"] >= 6) {
        return;
    }
    elseif ($trip1_xml["sharei"] == 2) {
        return;
    }
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "日 当 (B)", 1, 0, "C", 1);
    $pdf->Cell(4, $base_row, "", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "＠", "TB", 0, "R");
    $pdf->Cell(13, $base_row, number_format($trip2_xml["daily_fee2"]) . "円", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
    $pdf->Cell(10, $base_row, $trip2_xml["daily_fee_days2"] . "日", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
    $pdf->Cell(12, $base_row, $trip2_xml["daily_fee_percentage2"] . "％", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "", "TB", 0, "R");
    $pdf->Cell(20, $base_row, number_format($trip2_xml["daily_fee_total2"]) . "円", 1, 0, "R");
}

// 宿泊料(23区)
function tokyo_pdf() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "宿泊料(23区)", 1, 0, "C", 1);
    if ($trip1_xml["sharei"] == 2) {
        $pdf->Cell(55, $base_row, "謝礼本人受領", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    elseif ($trip1_xml["kubun"] == 1 or $trip1_xml["kubun"] == 4) {
        $pdf->Cell(55, $base_row, "日帰り", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    elseif ($trip1_xml["kubun"] >= 6) {
        $pdf->Cell(55, $base_row, "自己研修", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    elseif (@$trip2_xml["stay_flg"] and $trip2_xml["stay_flg"][0] == 1) {
        $pdf->Cell(55, $base_row, "宿泊料不要", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    else {
        $pdf->Cell(4, $base_row, "", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "＠", "TB", 0, "R");
        $pdf->Cell(13, $base_row, number_format($trip2_xml["tokyo_fee"]) . "円", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
        $pdf->Cell(10, $base_row, $trip2_xml["tokyo_fee_days"] . "日", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
        $pdf->Cell(12, $base_row, $trip2_xml["tokyo_fee_percentage"] . "％", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "", "TB", 0, "R");
        $pdf->Cell(20, $base_row, number_format($trip2_xml["tokyo_fee_total"]) . "円", 1, 0, "R");
    }
}

// 宿泊料(23区) (B)
function tokyo_pdf_b() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    if ($trip1_xml["sharei"] == 2) {
        return;
    }
    elseif ($trip1_xml["kubun"] == 1 or $trip1_xml["kubun"] == 4) {
        return;
    }
    elseif ($trip1_xml["kubun"] >= 6) {
        return;
    }
    elseif (@$trip2_xml["stay_flg"] and $trip2_xml["stay_flg"][0] == 1) {
        return;
    }
    $pdf->SetFont(PMINCHO, '', 7);
    $pdf->Cell(20, $base_row, "宿泊料(23区)(B)", 1, 0, "C", 1);
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(4, $base_row, "", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "＠", "TB", 0, "R");
    $pdf->Cell(13, $base_row, number_format($trip2_xml["tokyo_fee2"]) . "円", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
    $pdf->Cell(10, $base_row, $trip2_xml["tokyo_fee_days2"] . "日", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
    $pdf->Cell(12, $base_row, $trip2_xml["tokyo_fee_percentage2"] . "％", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "", "TB", 0, "R");
    $pdf->Cell(20, $base_row, number_format($trip2_xml["tokyo_fee_total2"]) . "円", 1, 0, "R");
}

// 宿泊料
function hotel_pdf() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "宿泊料", 1, 0, "C", 1);
    if ($trip1_xml["sharei"] == 2) {
        $pdf->Cell(55, $base_row, "謝礼本人受領", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    elseif ($trip1_xml["kubun"] == 1 or $trip1_xml["kubun"] == 4) {
        $pdf->Cell(55, $base_row, "日帰り", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    elseif ($trip1_xml["kubun"] >= 6) {
        $pdf->Cell(55, $base_row, "自己研修", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    elseif (@$trip2_xml["stay_flg"] and $trip2_xml["stay_flg"][0] == 1) {
        $pdf->Cell(55, $base_row, "宿泊料不要", 1, 0, "C");
        $pdf->Cell(20, $base_row, "-", 1, 0, "C");
    }
    else {
        $pdf->Cell(4, $base_row, "", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "＠", "TB", 0, "R");
        $pdf->Cell(13, $base_row, number_format($trip2_xml["hotel_fee"]) . "円", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
        $pdf->Cell(10, $base_row, $trip2_xml["hotel_fee_days"] . "日", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
        $pdf->Cell(12, $base_row, $trip2_xml["hotel_fee_percentage"] . "％", "TB", 0, "R");
        $pdf->Cell(4, $base_row, "", "TB", 0, "R");
        $pdf->Cell(20, $base_row, number_format($trip2_xml["hotel_fee_total"]) . "円", 1, 0, "R");
    }
}

// 宿泊料(B)
function hotel_pdf_b() {
    global $pdf, $base_row, $trip1_xml, $trip2_xml;
    if ($trip1_xml["sharei"] == 2) {
        return;
    }
    elseif ($trip1_xml["kubun"] == 1 or $trip1_xml["kubun"] == 4) {
        return;
    }
    elseif ($trip1_xml["kubun"] >= 6) {
        return;
    }
    elseif (@$trip2_xml["stay_flg"] and $trip2_xml["stay_flg"][0] == 1) {
        return;
    }
    $pdf->SetFont(PMINCHO, '', 8);
    $pdf->Cell(20, $base_row, "宿泊料(B)", 1, 0, "C", 1);
    $pdf->Cell(4, $base_row, "", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "＠", "TB", 0, "R");
    $pdf->Cell(13, $base_row, number_format($trip2_xml["hotel_fee2"]) . "円", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
    $pdf->Cell(10, $base_row, $trip2_xml["hotel_fee_days2"] . "日", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "×", "TB", 0, "R");
    $pdf->Cell(12, $base_row, $trip2_xml["hotel_fee_percentage2"] . "％", "TB", 0, "R");
    $pdf->Cell(4, $base_row, "", "TB", 0, "R");
    $pdf->Cell(20, $base_row, number_format($trip2_xml["hotel_fee_total2"]) . "円", 1, 0, "R");
}

// 交通費内訳
function tt_pdf($tt1, $tt2, $tt3) {
    global $pdf, $tt_row, $trip2_xml, $array_transport, $array_oufuku, $array_tt2type, $array_tt3type, $array_tt3car;

    // tt1
    if ($tt1) {
        $pdf->SetFont(PMINCHO, '', 7);
        $pdf->Cell(0, $tt_row, "■公共交通機関 (タクシー、航空機：領収書添付必要)", 0, 1);
        $pdf->Cell(13, $tt_row, "日付", 1, 0, "C", 1);
        $pdf->Cell(13, $tt_row, "交通機関", 1, 0, "C", 1);
        $pdf->Cell(8, $tt_row, "区分", 1, 0, "C", 1);
        $pdf->Cell(28, $tt_row, "出発", 1, 0, "C", 1);
        $pdf->Cell(28, $tt_row, "到着", 1, 0, "C", 1);
        $pdf->Cell(82, $tt_row, "備考", 1, 0, "C", 1);
        $pdf->Cell(18, $tt_row, "金額", 1, 1, "C", 1);

        $pdf->SetFont(PMINCHO, '', 6);
        foreach ($tt1 as $t) {
            list($x1, $y1) = array($pdf->GetX(), $pdf->GetY());
            $pdf->Cell(90, 0, "", 0, 0);
            $pdf->MultiCell(82, $tt_row, $t["note"], 1);
            list($x2, $y2) = array($pdf->GetX(), $pdf->GetY());
            $pdf->SetXY($x1, $y1);
            $pdf->Cell(13, $y2 - $y1, $t["date"], 1, 0, "C");
            $pdf->Cell(13, $y2 - $y1, $array_transport[$t["transport"]], 1, 0, "C");
            $pdf->Cell(8, $y2 - $y1, $array_oufuku[$t["oufuku"]], 1, 0, "C");
            $pdf->Cell(28, $y2 - $y1, $t["from"], 1, 0, "C");
            $pdf->Cell(28, $y2 - $y1, $t["to"], 1, 0, "C");
            $pdf->SetX(182);
            $pdf->Cell(18, $y2 - $y1, number_format($t["fee"]) . "円", 1, 1, "R");
        }
        $pdf->SetFont(PMINCHO, '', 7);
        $pdf->Cell(152, $tt_row, "", 0, 0);
        $pdf->Cell(20, $tt_row, "合計金額", 1, 0, "C", 1);
        $pdf->SetFont(PMINCHO, '', 6);
        $pdf->Cell(18, $tt_row, number_format($trip2_xml["tt1_fee_total"]) . "円", 1, 1, "R");

        // 空白行
        $pdf->Cell(0, 1.0, "", 0, 1);
    }

    // tt2
    if ($tt2) {
        $pdf->SetFont(PMINCHO, '', 7);
        $pdf->Cell(0, $tt_row, "■宿泊、交通費などが組み込まれたビジネスパック、出張パック (領収書添付が必要)", 0, 1);
        $pdf->Cell(13, $tt_row, "日付", 1, 0, "C", 1);
        $pdf->Cell(40, $tt_row, "内容", 1, 0, "C", 1);
        $pdf->Cell(119, $tt_row, "備考", 1, 0, "C", 1);
        $pdf->Cell(18, $tt_row, "金額", 1, 1, "C", 1);

        $pdf->SetFont(PMINCHO, '', 6);
        foreach ($tt2 as $t) {
            list($x1, $y1) = array($pdf->GetX(), $pdf->GetY());
            $pdf->Cell(53, 0, "", 0, 0);
            $pdf->MultiCell(119, $tt_row, $t["note"], 1);
            list($x2, $y2) = array($pdf->GetX(), $pdf->GetY());
            $pdf->SetXY($x1, $y1);
            $pdf->Cell(13, $tt_row, $t["date"], 1, 0, "C");
            $pdf->Cell(40, $tt_row, $array_tt2type[$t["type"]], 1, 0, "C");
            $pdf->SetX(182);
            $pdf->Cell(18, $tt_row, number_format($t["fee"]) . "円", 1, 1, "R");
        }
        $pdf->SetFont(PMINCHO, '', 7);
        $pdf->Cell(152, $tt_row, "", 0, 0);
        $pdf->Cell(20, $tt_row, "合計金額", 1, 0, "C", 1);
        $pdf->SetFont(PMINCHO, '', 6);
        $pdf->Cell(18, $tt_row, number_format($trip2_xml["tt2_fee_total"]) . "円", 1, 1, "R");

        // 空白行
        $pdf->Cell(0, 1.0, "", 0, 1);
    }

    // tt3
    if ($tt3) {
        $pdf->SetFont(PMINCHO, '', 7);
        $pdf->Cell(0, $tt_row, "■私有車利用 (領収書添付が必要)", 0, 1);
        $pdf->Cell(13, $tt_row, "日付", 1, 0, "C", 1);
        $pdf->Cell(14, $tt_row, "区分", 1, 0, "C", 1);
        $pdf->Cell(15, $tt_row, "車種", 1, 0, "C", 1);
        $pdf->Cell(28, $tt_row, "出発", 1, 0, "C", 1);
        $pdf->Cell(28, $tt_row, "経由1", 1, 0, "C", 1);
        $pdf->Cell(28, $tt_row, "経由2", 1, 0, "C", 1);
        $pdf->Cell(28, $tt_row, "到着", 1, 0, "C", 1);
        $pdf->Cell(18, $tt_row, "走行距離", 1, 0, "C", 1);
        $pdf->Cell(18, $tt_row, "金額", 1, 1, "C", 1);

        foreach ($tt3 as $t) {
            $pdf->SetFont(PMINCHO, '', 6);
            $pdf->Cell(13, $tt_row, $t["date"], 1, 0, "C");
            $pdf->Cell(14, $tt_row, $array_tt3type[$t["type"]], 1, 0, "C");
            $pdf->Cell(15, $tt_row, $t["type"] != 3 ? $array_tt3car[$t["car"]] : "", 1, 0, "C");
            $pdf->Cell(28, $tt_row, $t["type"] != 3 ? $t["from"] : "", 1, 0, "C");
            $pdf->Cell(28, $tt_row, $t["type"] != 3 ? $t["via1"] : "", 1, 0, "C");
            $pdf->Cell(28, $tt_row, $t["type"] != 3 ? $t["via2"] : "", 1, 0, "C");
            $pdf->Cell(28, $tt_row, $t["type"] != 3 ? $t["to"] : "", 1, 0, "C");
            $pdf->Cell(18, $tt_row, $t["type"] == 1 ? $t["dist"] . "km" : "", 1, 0, "R");
            $pdf->Cell(18, $tt_row, number_format($t["fee"]) . "円", 1, 1, "R");
        }
        $pdf->SetFont(PMINCHO, '', 7);
        $pdf->Cell(152, $tt_row, "", 0, 0);
        $pdf->Cell(20, $tt_row, "合計金額", 1, 0, "C", 1);
        $pdf->SetFont(PMINCHO, '', 6);
        $pdf->Cell(18, $tt_row, number_format($trip2_xml["tt3_fee_total"]) . "円", 1, 1, "R");
        $pdf->Cell(0, $tt_row, "■私有車利用がやむをえないとする理由", 0, 1);
        $pdf->MultiCell(0, $tt_row, $trip2_xml["tt3_reason"], 1, 1);

        // 空白行
        $pdf->Cell(0, 1.0, "", 0, 1);
    }
}
