<?
require("about_session.php");
require("get_values.ini");
require_once("about_postgres.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($reason_id == "other") {
	if ($reason == "") {
		echo("<script type=\"text/javascript\">alert('理由が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($reason) > 200) {
		echo("<script type=\"text/javascript\">alert('理由が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$reason_id = null;
} else {
	$reason = "";
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 申請情報を更新
$sql = "update rtnapply set";
$set = array("reason_id", "reason", "apply_time", "comment");
$setvalue = array($reason_id, $reason, date("YmdHis"), $comment);
$cond = "where apply_id = $apply_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 承認更新
if($rtncfm != ""){
	
	for($i=1; $i<=$approve_num; $i++)
	{
		$varname = "regist_emp_id$i";
		$aprv_emp_id = ($$varname == "") ? null : $$varname;
	
		$varname = "apv_order$i";
		$apv_order = ($$varname == "") ? null : $$varname;
	
		$varname = "apv_sub_order$i";
		$apv_sub_order = ($$varname == "") ? null : $$varname;
	
		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
	
		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;

        if($aprv_emp_id != "")
        {
			// 役職も登録する
			$sql = "select emp_st from empmst ";
			$cond = "where emp_id = '$aprv_emp_id'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$emp_st = pg_fetch_result($sel, 0, 0);
		
			$sql = "update rtnaprv set";
			$set = array("aprv_emp_id", "emp_st");
			$setvalue = array($aprv_emp_id, $emp_st);
			$cond = "where apply_id = $apply_id and aprv_no = $apv_order ";
			if($apv_sub_order != "")
			{
				$cond .= "and aprv_sub_no = $apv_sub_order"; 
			}
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
        } 
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 自画面を閉じる
echo("<script type=\"text/javascript\">alert('更新しました。');</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
?>
