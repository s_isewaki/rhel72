<?

//====================================================================
//事故件数検索
//====================================================================
//レベル別
function getAccidentCount4Level($con,$fname,$level_ary,$date_this_y,$date_last_y){
	$sql = " SELECT ".
		" 	oc.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, oc.occurrences_year ".
		" 	, history.hospital_bed ".
		" 	, ( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " ) as total_report_count ".
		" 	, (case history.hospital_bed ".
		" 		WHEN '0' THEN 0 ".
		" 		ELSE ( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " 			COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " 			) / CAST(history.hospital_bed AS float) ".
		" 	end ".
		" 	) as report_per_hospital_bed ".
		" 	, COALESCE(level_0,0) as level0 ".
		" 	, COALESCE(level_1,0) as level1 ".
		" 	, COALESCE(level_2,0) as level2 ".
		" 	, COALESCE(level_3a,0) as level3a ".
		" 	, COALESCE(level_3b,0) as level3b ".
		" 	, COALESCE(level_4,0) as level4 ".
		" 	, COALESCE(level_5,0) as level5 ".
		" 	, '1' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_level oc ".
		" 	, fplusapply app ".
		" 	, fplus_hospital_master hos ".
		" 	, (SELECT ".
		" 			his.hospital_id ".
		" 			, his.hospital_bed ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_this_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 				, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" WHERE ".
		" 	oc.occurrences_year = '$date_this_y' ".
		" 	AND oc.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND oc.hospital_id = hos.hospital_id ".
		" 	AND oc.hospital_id = history.hospital_id ".
		" UNION ".
		" SELECT ".
		" 	oc.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, oc.occurrences_year ".
		" 	, history.hospital_bed ".
		" 	, ( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " ) as total_report_count ".
		" 	, (case history.hospital_bed ".
		" 		WHEN '0' THEN 0 ".
		" 		ELSE ( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " 			COALESCE(level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " 			) / CAST(history.hospital_bed AS float) ".
		" 	end ".
		" 	) as report_per_hospital_bed ".
		" 	, COALESCE(level_0,0) as level0 ".
		" 	, COALESCE(level_1,0) as level1 ".
		" 	, COALESCE(level_2,0) as level2 ".
		" 	, COALESCE(level_3a,0) as level3a ".
		" 	, COALESCE(level_3b,0) as level3b ".
		" 	, COALESCE(level_4,0) as level4 ".
		" 	, COALESCE(level_5,0) as level5 ".
		" 	, '2' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_level oc ".
		" 	, fplusapply app ".
		" 	, fplus_hospital_master hos ".
		" 	, (SELECT ".
		" 			his.hospital_id ".
		" 			, his.hospital_bed ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_last_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 				, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" WHERE ".
		" 	oc.occurrences_year = '$date_last_y' ".
		" 	AND oc.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND oc.hospital_id = hos.hospital_id ".
		" 	AND oc.hospital_id = history.hospital_id ".
		" ORDER BY ".
		" 	hospital_id ".
		" 	, kbn ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;

}

function getAccidentCount4Job($con,$fname,$level_ary,$job,$date_this_y,$date_last_y){
	$sql = " SELECT ".
		" 	oc.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, oc.occurrences_year ".
		" 	, history.hospital_bed ".
		" 	, ( ";
		for($i = 0;$i < count($job);$i++){
			for($j=0;$j < count($level_ary);$j++){
				if($j != 0){
					$sql .= " + COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
				}else{
					if($i == 0){
						$sql .= " COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
					}else{
						$sql .= " + COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
					}
				}
			}
		}
		$sql .= " ) as total_report_count";
		for($i = 0;$i < count($job);$i++){
			$sql .= " 	, ( ";
			for($j=0;$j < count($level_ary);$j++){
				if($j != 0){
					$sql .= " + COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
				}else{
					$sql .= " COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
				}
			}
			$sql .= " 	) as jobcategory".$job[$i]['def_code']."sum";
		}
		$sql .= " 	, ( ";
		for($i = 0;$i < count($job);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level_0_jobcategory_".$job[$i]['def_code'].",0) ";
			}else{
				$sql .= " COALESCE(level_0_jobcategory_".$job[$i]['def_code'].",0) ";
			}
		}
		$sql .= " ) as total_report_count_level0";
		for($i = 0;$i < count($job);$i++){
			$sql .= " 	, COALESCE(level_0_jobcategory_".$job[$i]['def_code'].",0) as level_0_jobcategory".$job[$i]['def_code'];
		}
		$sql .= " 	, '1' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences oc ".
		" 	, fplusapply app ".
		" 	, fplus_hospital_master hos ".
		" 	, (SELECT ".
		" 			his.hospital_id ".
		" 			, his.hospital_bed ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_this_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 			, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" WHERE ".
		" 	oc.occurrences_year = '$date_this_y' ".
		" 	AND oc.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND oc.hospital_id = hos.hospital_id ".
		" 	AND oc.hospital_id = history.hospital_id ".
		" UNION ".
		" SELECT ".
		" 	oc.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, oc.occurrences_year ".
		" 	, history.hospital_bed ".
		" 	, ( ";
		for($i = 0;$i < count($job);$i++){
			for($j=0;$j < count($level_ary);$j++){
				if($j != 0){
					$sql .= " + COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
				}else{
					if($i == 0){
						$sql .= " COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
					}else{
						$sql .= " + COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
					}
				}
			}
		}
		$sql .= " ) as total_report_count";
		for($i = 0;$i < count($job);$i++){
			$sql .= " 	, ( ";
			for($j=0;$j < count($level_ary);$j++){
				if($j != 0){
					$sql .= " + COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
				}else{
					$sql .= " COALESCE(level_".$level_ary[$j]['short_name']."_jobcategory_".$job[$i]['def_code'].",0) ";
				}
			}
			$sql .= " 	) as jobcategory".$job[$i]['def_code']."sum";
		}
		$sql .= " 	, ( ";
		for($i = 0;$i < count($job);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level_0_jobcategory_".$job[$i]['def_code'].",0) ";
			}else{
				$sql .= " COALESCE(level_0_jobcategory_".$job[$i]['def_code'].",0) ";
			}
		}
		$sql .= " ) as total_report_count_level0";
		for($i = 0;$i < count($job);$i++){
			$sql .= " 	, COALESCE(level_0_jobcategory_".$job[$i]['def_code'].",0) as level_0_jobcategory".$job[$i]['def_code'];
		}
		$sql .= " 	, '2' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences oc ".
		" 	, fplusapply app ".
		" 	, fplus_hospital_master hos ".
		" 	, (SELECT ".
		" 			his.hospital_id ".
		" 			, his.hospital_bed ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_last_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 			, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" WHERE ".
		" 	oc.occurrences_year = '$date_last_y' ".
		" 	AND oc.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND oc.hospital_id = hos.hospital_id ".
		" 	AND oc.hospital_id = history.hospital_id ".
		" ORDER BY ".
		" 	hospital_id ".
		" 	, kbn ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;
}

//転倒・転落事故
function getFallAccidentCount($con,$fname,$date_this_y,$date_last_y){
	$sql = " SELECT ".
		" 	fall.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, fall.occurrences_year ".
		" 	, COALESCE(fall.level_3b,0) as level3b ".
		" 	, COALESCE(fall.level_4,0) as level4 ".
		" 	, COALESCE(fall.level_5,0) as level5 ".
		" 	, '1' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_fall_accident fall ".
		" 	, fplusapply app ".
		" 	, fplus_hospital_master hos ".
		" WHERE ".
		" 	fall.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND fall.hospital_id = hos.hospital_id ".
		" 	AND fall.occurrences_year = '$date_this_y' ".
		" UNION ".
		" SELECT ".
		" 	fall.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, fall.occurrences_year ".
		" 	, COALESCE(fall.level_3b,0) as level3b ".
		" 	, COALESCE(fall.level_4,0) as level4 ".
		" 	, COALESCE(fall.level_5,0) as level5 ".
		" 	, '2' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_fall_accident fall ".
		" 	, fplusapply app ".
		" 	, fplus_hospital_master hos ".
		" WHERE ".
		" 	fall.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND fall.hospital_id = hos.hospital_id ".
		" 	AND fall.occurrences_year = '$date_last_y' ".
		" ORDER BY ".
		" 	hospital_id ".
		" 	,kbn ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;
}

//病床規模別
function getAccidentCount4bed($con,$fname,$level_ary,$date_this_y,$date_last_y){
	$sql = " SELECT ".
		" 	(CASE ".
		" 		WHEN history.hospital_bed >= 400 THEN 1 ".
		" 		WHEN (history.hospital_bed < 400 AND history.hospital_bed >= 250) THEN 2 ".
		" 		ELSE 3 ".
		" 	END) as bed_kbn ".
		" 	, level.occurrences_year ".
		" 	, SUM( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level.level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " COALESCE(level.level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " ) as total_report_count ";
		for($i=0;$i<count($level_ary);$i++){
			$sql .= " 	, SUM(COALESCE(level.level_".$level_ary[$i]['short_name'].",0)) as level".$level_ary[$i]['short_name']."sum ";
		}
		$sql .= " 	, '1' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_level level ".
		" 	, fplusapply app ".
		" 	, (SELECT ".
		" 		his.hospital_id ".
		" 		, his.hospital_bed ".
		" 	   FROM ".
		" 		(SELECT ".
		" 			his.hospital_id ".
		" 			, max(his.fiscal_year) as latest_fiscal_year ".
		" 		FROM ".
		" 			fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.del_flg = 'f' ".
		" 			AND his.closing_flg = 'f' ".
		" 			AND his.fiscal_year <= '$date_this_y' ".
		" 		GROUP BY ".
		" 			his.hospital_id) history_year ".
		" 		, fplus_hospital_history his ".
		" 	   WHERE ".
		" 		his.fiscal_year = history_year.latest_fiscal_year ".
		" 		AND his.del_flg = 'f' ".
		" 		AND his.hospital_id = history_year.hospital_id) history ".
		"  ".
		" WHERE ".
		" 	level.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND level.hospital_id = history.hospital_id ".
		" 	AND level.occurrences_year = '$date_this_y' ".
		" GROUP BY ".
		" 	bed_kbn ".
		" 	, level.occurrences_year ".
		" UNION ".
		" SELECT ".
		" 	(CASE ".
		" 		WHEN history.hospital_bed >= 400 THEN 1 ".
		" 		WHEN (history.hospital_bed < 400 AND history.hospital_bed >= 250) THEN 2 ".
		" 		ELSE 3 ".
		" 	END) as bed_kbn ".
		" 	, level.occurrences_year ".
		" 	, SUM( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(level.level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " COALESCE(level.level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " ) as total_report_count ";
		for($i=0;$i<count($level_ary);$i++){
			$sql .= " 	, SUM(COALESCE(level.level_".$level_ary[$i]['short_name'].",0)) as level".$level_ary[$i]['short_name']."sum ";
		}
		$sql .= " 	, '2' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_level level ".
		" 	, fplusapply app ".
		" 	, (SELECT ".
		" 		his.hospital_id ".
		" 		, his.hospital_bed ".
		" 	   FROM ".
		" 		(SELECT ".
		" 			his.hospital_id ".
		" 			, max(his.fiscal_year) as latest_fiscal_year ".
		" 		FROM ".
		" 			fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.del_flg = 'f' ".
		" 			AND his.closing_flg = 'f' ".
		" 			AND his.fiscal_year <= '$date_last_y' ".
		" 		GROUP BY ".
		" 			his.hospital_id) history_year ".
		" 		, fplus_hospital_history his ".
		" 	   WHERE ".
		" 		his.fiscal_year = history_year.latest_fiscal_year ".
		" 		AND his.del_flg = 'f' ".
		" 		AND his.hospital_id = history_year.hospital_id) history ".
		"  ".
		" WHERE ".
		" 	level.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND level.hospital_id = history.hospital_id ".
		" 	AND level.occurrences_year = '$date_last_y' ".
		" GROUP BY ".
		" 	bed_kbn ".
		" 	, level.occurrences_year ".
		" ORDER BY ".
		" 	bed_kbn ".
		" 	,kbn ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;
}

//入院看護基準別
function getAccidentCount4nursing($con,$fname,$level_ary,$date_this_y,$date_last_y){
	$sql = " SELECT ".
		" 	nurs.nursing_standard_id ".
		" 	, nurs.nursing_standard ".
		" 	, oc.occurrences_year ".
		" 	, SUM( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(oc.level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " COALESCE(oc.level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " ) as total_report_count ";
		for($i=0;$i<count($level_ary);$i++){
			$sql .= " 	, SUM(COALESCE(oc.level_".$level_ary[$i]['short_name'].",0)) as level".$level_ary[$i]['short_name']."sum ";
		}
		$sql .= " 	, '1' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_level oc ".
		" 	, fplusapply app ".
		" 	, (SELECT ".
		" 			his.nursing_standard as nursing_standard_id ".
		" 			,his.hospital_id ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_this_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 			, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" 	, fplus_nursing_standard nurs ".
		" WHERE ".
		" 	oc.occurrences_year = '$date_this_y' ".
		" 	AND oc.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND oc.hospital_id = history.hospital_id ".
		" 	AND history.nursing_standard_id = nurs.nursing_standard_id ".
		" GROUP BY ".
		" 	nurs.nursing_standard_id ".
		" 	, nurs.nursing_standard ".
		" 	, oc.occurrences_year ".
		" UNION ".
		" SELECT ".
		" 	nurs.nursing_standard_id ".
		" 	, nurs.nursing_standard ".
		" 	, oc.occurrences_year ".
		" 	, SUM( ";
		for($i=0;$i<count($level_ary);$i++){
			if($i != 0){
				$sql .= " + COALESCE(oc.level_".$level_ary[$i]['short_name'].",0) ";
			}else{
				$sql .= " COALESCE(oc.level_".$level_ary[$i]['short_name'].",0) ";
			}
		}
		$sql .= " ) as total_report_count ";
		for($i=0;$i<count($level_ary);$i++){
			$sql .= " 	, SUM(COALESCE(oc.level_".$level_ary[$i]['short_name'].",0)) as level".$level_ary[$i]['short_name']."sum ";
		}
		$sql .= " 	, '2' as kbn ".
		" FROM ".
		" 	fplus_number_of_occurrences_level oc ".
		" 	, fplusapply app ".
		" 	, (SELECT ".
		" 			his.nursing_standard as nursing_standard_id ".
		" 			,his.hospital_id ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_last_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 			, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" 	, fplus_nursing_standard nurs ".
		" WHERE ".
		" 	oc.occurrences_year = '$date_last_y' ".
		" 	AND oc.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND app.apply_stat = '1' ".
		" 	AND oc.hospital_id = history.hospital_id ".
		" 	AND history.nursing_standard_id = nurs.nursing_standard_id ".
		" GROUP BY ".
		" 	nurs.nursing_standard_id ".
		" 	, nurs.nursing_standard ".
		" 	, oc.occurrences_year ".
		" ORDER BY ".
		" 	nursing_standard_id ".
		" 	,kbn ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;

}

//事故報告件数（要件別）
function getAccidentCount4requirement($con,$fname,$date_this_y,$date_last_y){
	$date_next_y = (string)((int)$date_this_y + 1);

	$sql = " SELECT ".
		" 	dat.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, dat.occurrences_year ".
		" 	, history.hospital_bed ".
		" 	, COALESCE(lev.sum_over_3b,0) as sum_over_3b ".
		" 	, dat.total_report_count ".
		" 	, dat.requirements1sum ".
		" 	, dat.requirements2sum ".
		" 	, dat.requirements3sum ".
		" 	, dat.outline1sum ".
		" 	, dat.outline2sum ".
		" 	, dat.outline3sum ".
		" 	, dat.outline4sum ".
		" 	, dat.outline5sum ".
		" 	, dat.outline6sum ".
		" 	, dat.outline7sum ".
		" 	, dat.kbn ".
		" FROM ".
		" 	(SELECT ".
		" 		list.hospital_id ".
		" 		, list.occurrences_year ".
		" 		, SUM(list.requirements1 + list.requirements2 + list.requirements3) AS total_report_count ".
		" 		, SUM(list.requirements1) AS requirements1sum ".
		" 		, SUM(list.requirements2) AS requirements2sum ".
		" 		, SUM(list.requirements3) AS requirements3sum ".
		" 		, SUM(list.outline1) AS outline1sum ".
		" 		, SUM(list.outline2) AS outline2sum ".
		" 		, SUM(list.outline3) AS outline3sum ".
		" 		, SUM(list.outline4) AS outline4sum ".
		" 		, SUM(list.outline5) AS outline5sum ".
		" 		, SUM(list.outline6) AS outline6sum ".
		" 		, SUM(list.outline7) AS outline7sum ".
		" 		, varchar(1) '1' AS kbn ".
		" 	FROM ".
		" 		(SELECT ".
		" 			repo.hospital_id ".
		" 			, varchar(4) '$date_this_y' AS occurrences_year ".
		" 			, CASE repo.requirements1 ".
		" 				WHEN 't' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS requirements1 ".
		" 			, CASE repo.requirements2 ".
		" 				WHEN 't' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS requirements2 ".
		" 			, CASE repo.requirements3 ".
		" 				WHEN 't' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS requirements3 ".
		" 			, CASE repo.outline ".
		" 				WHEN '1' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline1 ".
		" 			, CASE repo.outline ".
		" 				WHEN '2' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline2 ".
		" 			, CASE repo.outline ".
		" 				WHEN '3' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline3 ".
		" 			, CASE repo.outline ".
		" 				WHEN '4' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline4 ".
		" 			, CASE repo.outline ".
		" 				WHEN '5' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline5 ".
		" 			, CASE repo.outline ".
		" 				WHEN '6' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline6 ".
		" 			, CASE repo.outline ".
		" 				WHEN '7' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline7 ".
		" 		FROM ".
		" 			(SELECT ".
		" 				CASE ".
		" 					WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 					ELSE s.hospital_id ".
		" 				END as hospital_id ".
		" 				, CASE ".
		" 					WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 					ELSE s.occurrences_date ".
		" 				END as occurrences_date ".
		" 				, CASE ".
		" 					WHEN s.requirements1 IS NULL THEN f.requirements1 ".
		" 					ELSE s.requirements1 ".
		" 				END as requirements1 ".
		" 				, CASE ".
		" 					WHEN s.requirements2 IS NULL THEN f.requirements2 ".
		" 					ELSE s.requirements2 ".
		" 				END as requirements2 ".
		" 				, CASE ".
		" 					WHEN s.requirements3 IS NULL THEN f.requirements3 ".
		" 					ELSE s.requirements3 ".
		" 				END as requirements3 ".
		" 				, CASE ".
		" 					WHEN s.outline IS NULL THEN f.outline ".
		" 					ELSE s.outline ".
		" 				END as outline ".
		" 			FROM ".
		" 				((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 				  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 			WHERE ".
		" 				(f.apply_id is not null ".
		" 				OR s.apply_id is not null) ".
		" 				AND am.del_flg <> 't') repo ".
		" 		WHERE ".
		" 			repo.occurrences_date BETWEEN '$date_this_y/04/01' AND '$date_next_y/03/31') list ".
		" 	GROUP BY ".
		" 		list.hospital_id ".
		" 		, list.occurrences_year) dat left join (SELECT ".
		" 								lev.hospital_id ".
		" 								, (level_3b + level_4 + level_5) AS sum_over_3b  ".
		" 							FROM ".
		" 								fplus_number_of_occurrences_level lev ".
		" 								, fplusapply app ".
		" 							WHERE ".
		" 								lev.apply_id = app.apply_id ".
		" 								AND lev.occurrences_year = '$date_this_y' ".
		" 								AND app.delete_flg = 'f' ".
		" 								AND app.apply_stat = '1') lev on dat.hospital_id = lev.hospital_id ".
		" 	, (SELECT ".
		" 			his.hospital_bed ".
		" 			,his.hospital_id ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_this_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 			, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" 	, fplus_hospital_master hos ".
		" WHERE ".
		" 	dat.hospital_id = history.hospital_id ".
		" 	AND dat.hospital_id = hos.hospital_id ".
		" UNION ".
		" SELECT ".
		" 	dat.hospital_id ".
		" 	, hos.hospital_name ".
		" 	, dat.occurrences_year ".
		" 	, history.hospital_bed ".
		" 	, COALESCE(lev.sum_over_3b,0) as sum_over_3b".
		" 	, dat.total_report_count ".
		" 	, dat.requirements1sum ".
		" 	, dat.requirements2sum ".
		" 	, dat.requirements3sum ".
		" 	, dat.outline1sum ".
		" 	, dat.outline2sum ".
		" 	, dat.outline3sum ".
		" 	, dat.outline4sum ".
		" 	, dat.outline5sum ".
		" 	, dat.outline6sum ".
		" 	, dat.outline7sum ".
		" 	, dat.kbn ".
		" FROM ".
		" 	(SELECT ".
		" 		list.hospital_id ".
		" 		, list.occurrences_year ".
		" 		, SUM(list.requirements1 + list.requirements2 + list.requirements3) AS total_report_count ".
		" 		, SUM(list.requirements1) AS requirements1sum ".
		" 		, SUM(list.requirements2) AS requirements2sum ".
		" 		, SUM(list.requirements3) AS requirements3sum ".
		" 		, SUM(list.outline1) AS outline1sum ".
		" 		, SUM(list.outline2) AS outline2sum ".
		" 		, SUM(list.outline3) AS outline3sum ".
		" 		, SUM(list.outline4) AS outline4sum ".
		" 		, SUM(list.outline5) AS outline5sum ".
		" 		, SUM(list.outline6) AS outline6sum ".
		" 		, SUM(list.outline7) AS outline7sum ".
		" 		, varchar(1) '2' AS kbn ".
		" 	FROM ".
		" 		(SELECT ".
		" 			repo.hospital_id ".
		" 			, varchar(4) '$date_last_y' AS occurrences_year ".
		" 			, CASE repo.requirements1 ".
		" 				WHEN 't' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS requirements1 ".
		" 			, CASE repo.requirements2 ".
		" 				WHEN 't' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS requirements2 ".
		" 			, CASE repo.requirements3 ".
		" 				WHEN 't' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS requirements3 ".
		" 			, CASE repo.outline ".
		" 				WHEN '1' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline1 ".
		" 			, CASE repo.outline ".
		" 				WHEN '2' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline2 ".
		" 			, CASE repo.outline ".
		" 				WHEN '3' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline3 ".
		" 			, CASE repo.outline ".
		" 				WHEN '4' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline4 ".
		" 			, CASE repo.outline ".
		" 				WHEN '5' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline5 ".
		" 			, CASE repo.outline ".
		" 				WHEN '6' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline6 ".
		" 			, CASE repo.outline ".
		" 				WHEN '7' THEN 1 ".
		" 				ELSE 0 ".
		" 			END AS outline7 ".
		" 		FROM ".
		" 			(SELECT ".
		" 				CASE ".
		" 					WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 					ELSE s.hospital_id ".
		" 				END as hospital_id ".
		" 				, CASE ".
		" 					WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 					ELSE s.occurrences_date ".
		" 				END as occurrences_date ".
		" 				, CASE ".
		" 					WHEN s.requirements1 IS NULL THEN f.requirements1 ".
		" 					ELSE s.requirements1 ".
		" 				END as requirements1 ".
		" 				, CASE ".
		" 					WHEN s.requirements2 IS NULL THEN f.requirements2 ".
		" 					ELSE s.requirements2 ".
		" 				END as requirements2 ".
		" 				, CASE ".
		" 					WHEN s.requirements3 IS NULL THEN f.requirements3 ".
		" 					ELSE s.requirements3 ".
		" 				END as requirements3 ".
		" 				, CASE ".
		" 					WHEN s.outline IS NULL THEN f.outline ".
		" 					ELSE s.outline ".
		" 				END as outline ".
		" 			FROM ".
		" 				((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 				  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 			WHERE ".
		" 				(f.apply_id is not null ".
		" 				OR s.apply_id is not null) ".
		" 				AND am.del_flg <> 't') repo ".
		" 		WHERE ".
		" 			repo.occurrences_date BETWEEN '$date_last_y/04/01' AND '$date_this_y/03/31') list ".
		" 	GROUP BY ".
		" 		list.hospital_id ".
		" 		, list.occurrences_year) dat left join (SELECT ".
		" 								lev.hospital_id ".
		" 								, (level_3b + level_4 + level_5) AS sum_over_3b  ".
		" 							FROM ".
		" 								fplus_number_of_occurrences_level lev ".
		" 								, fplusapply app ".
		" 							WHERE ".
		" 								lev.apply_id = app.apply_id ".
		" 								AND lev.occurrences_year = '$date_this_y' ".
		" 								AND app.delete_flg = 'f' ".
		" 								AND app.apply_stat = '1') lev on dat.hospital_id = lev.hospital_id ".
		" 	, (SELECT ".
		" 			his.hospital_bed ".
		" 			,his.hospital_id ".
		" 		FROM ".
		" 			(SELECT ".
		" 				his.hospital_id ".
		" 				, max(his.fiscal_year) as latest_fiscal_year ".
		" 			FROM ".
		" 				fplus_hospital_history his ".
		" 			WHERE ".
		" 				his.del_flg = 'f' ".
		" 				AND his.closing_flg = 'f' ".
		" 				AND his.fiscal_year <= '$date_last_y' ".
		" 			GROUP BY ".
		" 				his.hospital_id) history_year ".
		" 			, fplus_hospital_history his ".
		" 		WHERE ".
		" 			his.fiscal_year = history_year.latest_fiscal_year ".
		" 			AND his.del_flg = 'f' ".
		" 			AND his.hospital_id = history_year.hospital_id) history ".
		" 	, fplus_hospital_master hos ".
		" WHERE ".
		" 	dat.hospital_id = history.hospital_id ".
		" 	AND dat.hospital_id = hos.hospital_id ".
		" ORDER BY ".
		" 	hospital_id ".
		" 	, kbn ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

	return $result;

}

// 概要別事故
function getTotalReportCountOutline($con, $fname, $search) {
	// 集計期間の年度から、年月(範囲)を取得
	$start = $search . "-04";
	$end = ((int) $search) + 1 . "-03";
	
	$sql = "select";
		$sql .= " *";
	$sql .= " from";
		$sql .= " (";
			$sql .= "select";
				$sql .= " e.medical_accident_id";
				$sql .= ", case when e.second_outline <> '' then e.second_date else e.first_date end as report_date";
				$sql .= ", case when e.second_outline <> '' then e.second_require1 else e.first_require1 end as require1";
				$sql .= ", case when e.second_outline <> '' then e.second_require2 else e.first_require2 end as require2";
				$sql .= ", case when e.second_outline <> '' then e.second_require3 else e.first_require3 end as require3";
				$sql .= ", case when e.second_outline <> '' then e.second_outline else e.first_outline end as outline";
			$sql .= " from";
				$sql .= " (";
					$sql .= "select";
						$sql .= " c.medical_accident_id";
						$sql .= ", first_accident_id";
						$sql .= ", first_date";
						$sql .= ", first_require1";
						$sql .= ", first_require2";
						$sql .= ", first_require3";
						$sql .= ", first_outline";
						$sql .= ", d.medical_accident_id as second_accident_id";
						$sql .= ", d.report_date as second_date";
						$sql .= ", d.requirements1 as second_require1";
						$sql .= ", d.requirements2 as second_require2";
						$sql .= ", d.requirements3 as second_require3";
						$sql .= ", d.outline as second_outline";
					$sql .= " from";
						$sql .= " (";
							$sql .= "select";
								$sql .= " a.medical_accident_id";
								$sql .= ", b.medical_accident_id as first_accident_id";
								$sql .= ", b.report_date as first_date";
								$sql .= ", b.requirements1 as first_require1";
								$sql .= ", b.requirements2 as first_require2";
								$sql .= ", b.requirements3 as first_require3";
								$sql .= ", b.outline as first_outline";
							$sql .= " from";
								$sql .= " fplus_medical_accident_report_management a";
								$sql .= " left join";
								$sql .= " (";
									$sql .= "select";
										$sql .= " ba.medical_accident_id";
										$sql .= ", ba.report_date";
										$sql .= ", ba.requirements1";
										$sql .= ", ba.requirements2";
										$sql .= ", ba.requirements3";
										$sql .= ", ba.outline";
									$sql .= " from";
										$sql .= " fplus_first_report ba";
										$sql .= " inner join";
										$sql .= " fplusapply bb";
										$sql .= " on";
										$sql .= " ba.apply_id = bb.apply_id";
										$sql .= " and bb.apply_stat = '1'";
										$sql .= " and bb.delete_flg = 'f'";
								$sql .= ") b";
								$sql .= " on";
								$sql .= " a.medical_accident_id = b.medical_accident_id";
						$sql .= ") c";
						$sql .= " left join";
						$sql .= " (";
							$sql .= "select";
								$sql .= " da.medical_accident_id";
								$sql .= ", da.report_date";
								$sql .= ", da.requirements1";
								$sql .= ", da.requirements2";
								$sql .= ", da.requirements3";
								$sql .= ", da.outline";
							$sql .= " from";
								$sql .= " fplus_second_report da";
								$sql .= " inner join";
								$sql .= " fplusapply db";
								$sql .= " on";
								$sql .= " da.apply_id = db.apply_id";
								$sql .= " and db.apply_stat = '1'";
								$sql .= " and db.delete_flg = 'f'";
						$sql .= ") d";
						$sql .= " on";
						$sql .= " c.medical_accident_id = d.medical_accident_id";
				$sql .= ") e";
		$sql .= ") f";
	$sql .= " where";
		$sql .= " f.outline <> ''";
		$sql .= " and substr(report_date::text, 1, 7) between '" . $start . "' and '" . $end . "'";
	
	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);
	
	return $result;
}

//事故報告病院数
function getReportHospitalCount($con,$fname,$date_next_y,$date_this_y,$date_last_y,$date_2y_befor,$date_3y_befor,$date_4y_befor){

	$sql = " SELECT ".
		" 	count(hosp.hospital_id) as hospital_count ".
		" 	, varchar(4) '{$date_this_y}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		repo.hospital_id ".
		" 	FROM ".
		" 		(SELECT ".
		" 			CASE ".
		" 				WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 				ELSE s.hospital_id ".
		" 			END as hospital_id ".
		" 			, CASE ".
		" 				WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 				ELSE s.occurrences_date ".
		" 			END as occurrences_date ".
		" 		FROM ".
		" 			((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 			  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 		WHERE ".
		" 			(f.apply_id is not null ".
		" 			OR s.apply_id is not null) ".
		" 			AND am.del_flg <> 't') repo ".
		" 	WHERE ".
		" 		repo.occurrences_date between '{$date_this_y}/04/01' AND '{$date_next_y}/03/31' ".
		" 	GROUP BY ".
		" 		repo.hospital_id) hosp ".
		" UNION ".
		" SELECT ".
		" 	count(hosp.hospital_id) as hospital_count ".
		" 	, varchar(4) '{$date_last_y}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		repo.hospital_id ".
		" 	FROM ".
		" 		(SELECT ".
		" 			CASE ".
		" 				WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 				ELSE s.hospital_id ".
		" 			END as hospital_id ".
		" 			, CASE ".
		" 				WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 				ELSE s.occurrences_date ".
		" 			END as occurrences_date ".
		" 		FROM ".
		" 			((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 			  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 		WHERE ".
		" 			(f.apply_id is not null ".
		" 			OR s.apply_id is not null) ".
		" 			AND am.del_flg <> 't') repo ".
		" 	WHERE ".
		" 		repo.occurrences_date between '{$date_last_y}/04/01' AND '{$date_this_y}/03/31' ".
		" 	GROUP BY ".
		" 		repo.hospital_id) hosp ".
		" UNION ".
		" SELECT ".
		" 	count(hosp.hospital_id) as hospital_count ".
		" 	, varchar(4) '{$date_2y_befor}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		repo.hospital_id ".
		" 	FROM ".
		" 		(SELECT ".
		" 			CASE ".
		" 				WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 				ELSE s.hospital_id ".
		" 			END as hospital_id ".
		" 			, CASE ".
		" 				WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 				ELSE s.occurrences_date ".
		" 			END as occurrences_date ".
		" 		FROM ".
		" 			((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 			  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 		WHERE ".
		" 			(f.apply_id is not null ".
		" 			OR s.apply_id is not null) ".
		" 			AND am.del_flg <> 't') repo ".
		" 	WHERE ".
		" 		repo.occurrences_date between '{$date_2y_befor}/04/01' AND '{$date_last_y}/03/31' ".
		" 	GROUP BY ".
		" 		repo.hospital_id) hosp ".
		" UNION ".
		" SELECT ".
		" 	count(hosp.hospital_id) as hospital_count ".
		" 	, varchar(4) '{$date_3y_befor}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		repo.hospital_id ".
		" 	FROM ".
		" 		(SELECT ".
		" 			CASE ".
		" 				WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 				ELSE s.hospital_id ".
		" 			END as hospital_id ".
		" 			, CASE ".
		" 				WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 				ELSE s.occurrences_date ".
		" 			END as occurrences_date ".
		" 		FROM ".
		" 			((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 			  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 		WHERE ".
		" 			(f.apply_id is not null ".
		" 			OR s.apply_id is not null) ".
		" 			AND am.del_flg <> 't') repo ".
		" 	WHERE ".
		" 		repo.occurrences_date between '{$date_3y_befor}/04/01' AND '{$date_2y_befor}/03/31' ".
		" 	GROUP BY ".
		" 		repo.hospital_id) hosp ".
		" UNION ".
		" SELECT ".
		" 	count(hosp.hospital_id) as hospital_count ".
		" 	, varchar(4) '{$date_4y_befor}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		repo.hospital_id ".
		" 	FROM ".
		" 		(SELECT ".
		" 			CASE ".
		" 				WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 				ELSE s.hospital_id ".
		" 			END as hospital_id ".
		" 			, CASE ".
		" 				WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 				ELSE s.occurrences_date ".
		" 			END as occurrences_date ".
		" 		FROM ".
		" 			((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 			  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 		WHERE ".
		" 			(f.apply_id is not null ".
		" 			OR s.apply_id is not null) ".
		" 			AND am.del_flg <> 't') repo ".
		" 	WHERE ".
		" 		repo.occurrences_date between '{$date_4y_befor}/04/01' AND '{$date_3y_befor}/03/31' ".
		" 	GROUP BY ".
		" 		repo.hospital_id) hosp ".
		" ORDER BY ".
		" 	occurrences_year ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);
	
	return $result;

}

function getReportRequirementCount($con,$fname,$date_next_y,$date_this_y,$date_last_y,$date_2y_befor,$date_3y_befor,$date_4y_befor){

	$sql = " SELECT ".
		" 	COALESCE(SUM(case repo.requirements1 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements1_count ".
		" 	,COALESCE(SUM(case repo.requirements2 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements2_count ".
		" 	,COALESCE(SUM(case repo.requirements3 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements3_count ".
		" 	,varchar(4) '{$date_this_y}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		CASE ".
		" 			WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 			ELSE s.hospital_id ".
		" 		END as hospital_id ".
		" 		, CASE ".
		" 			WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 			ELSE s.occurrences_date ".
		" 		END as occurrences_date ".
		" 		, CASE ".
		" 			WHEN s.requirements1 IS NULL THEN f.requirements1 ".
		" 			ELSE s.requirements1 ".
		" 		END as requirements1 ".
		" 		, CASE ".
		" 			WHEN s.requirements2 IS NULL THEN f.requirements2 ".
		" 			ELSE s.requirements2 ".
		" 		END as requirements2 ".
		" 		, CASE ".
		" 			WHEN s.requirements3 IS NULL THEN f.requirements3 ".
		" 			ELSE s.requirements3 ".
		" 		END as requirements3 ".
		" 	FROM ".
		" 		((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 		  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 	WHERE ".
		" 		(f.apply_id is not null ".
		" 		OR s.apply_id is not null) ".
		" 		AND am.del_flg <> 't') repo ".
		" WHERE ".
		" 	repo.occurrences_date BETWEEN '{$date_this_y}/04/01' AND '{$date_next_y}/03/31' ".
		" UNION ".
		" SELECT ".
		" 	COALESCE(SUM(case repo.requirements1 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements1_count ".
		" 	,COALESCE(SUM(case repo.requirements2 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements2_count ".
		" 	,COALESCE(SUM(case repo.requirements3 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements3_count ".
		" 	,varchar(4) '{$date_last_y}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		CASE ".
		" 			WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 			ELSE s.hospital_id ".
		" 		END as hospital_id ".
		" 		, CASE ".
		" 			WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 			ELSE s.occurrences_date ".
		" 		END as occurrences_date ".
		" 		, CASE ".
		" 			WHEN s.requirements1 IS NULL THEN f.requirements1 ".
		" 			ELSE s.requirements1 ".
		" 		END as requirements1 ".
		" 		, CASE ".
		" 			WHEN s.requirements2 IS NULL THEN f.requirements2 ".
		" 			ELSE s.requirements2 ".
		" 		END as requirements2 ".
		" 		, CASE ".
		" 			WHEN s.requirements3 IS NULL THEN f.requirements3 ".
		" 			ELSE s.requirements3 ".
		" 		END as requirements3 ".
		" 	FROM ".
		" 		((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 		  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 	WHERE ".
		" 		(f.apply_id is not null ".
		" 		OR s.apply_id is not null) ".
		" 		AND am.del_flg <> 't') repo ".
		" WHERE ".
		" 	repo.occurrences_date BETWEEN '{$date_last_y}/04/01' AND '{$date_this_y}/03/31' ".
		" UNION ".
		" SELECT ".
		" 	COALESCE(SUM(case repo.requirements1 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements1_count ".
		" 	,COALESCE(SUM(case repo.requirements2 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements2_count ".
		" 	,COALESCE(SUM(case repo.requirements3 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements3_count ".
		" 	,varchar(4) '{$date_2y_befor}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		CASE ".
		" 			WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 			ELSE s.hospital_id ".
		" 		END as hospital_id ".
		" 		, CASE ".
		" 			WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 			ELSE s.occurrences_date ".
		" 		END as occurrences_date ".
		" 		, CASE ".
		" 			WHEN s.requirements1 IS NULL THEN f.requirements1 ".
		" 			ELSE s.requirements1 ".
		" 		END as requirements1 ".
		" 		, CASE ".
		" 			WHEN s.requirements2 IS NULL THEN f.requirements2 ".
		" 			ELSE s.requirements2 ".
		" 		END as requirements2 ".
		" 		, CASE ".
		" 			WHEN s.requirements3 IS NULL THEN f.requirements3 ".
		" 			ELSE s.requirements3 ".
		" 		END as requirements3 ".
		" 	FROM ".
		" 		((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 		  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 	WHERE ".
		" 		(f.apply_id is not null ".
		" 		OR s.apply_id is not null) ".
		" 		AND am.del_flg <> 't') repo ".
		" WHERE ".
		" 	repo.occurrences_date BETWEEN '{$date_2y_befor}/04/01' AND '{$date_last_y}/03/31' ".
		" UNION ".
		" SELECT ".
		" 	COALESCE(SUM(case repo.requirements1 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements1_count ".
		" 	,COALESCE(SUM(case repo.requirements2 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements2_count ".
		" 	,COALESCE(SUM(case repo.requirements3 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements3_count ".
		" 	,varchar(4) '{$date_3y_befor}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		CASE ".
		" 			WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 			ELSE s.hospital_id ".
		" 		END as hospital_id ".
		" 		, CASE ".
		" 			WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 			ELSE s.occurrences_date ".
		" 		END as occurrences_date ".
		" 		, CASE ".
		" 			WHEN s.requirements1 IS NULL THEN f.requirements1 ".
		" 			ELSE s.requirements1 ".
		" 		END as requirements1 ".
		" 		, CASE ".
		" 			WHEN s.requirements2 IS NULL THEN f.requirements2 ".
		" 			ELSE s.requirements2 ".
		" 		END as requirements2 ".
		" 		, CASE ".
		" 			WHEN s.requirements3 IS NULL THEN f.requirements3 ".
		" 			ELSE s.requirements3 ".
		" 		END as requirements3 ".
		" 	FROM ".
		" 		((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 		  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 	WHERE ".
		" 		(f.apply_id is not null ".
		" 		OR s.apply_id is not null) ".
		" 		AND am.del_flg <> 't') repo ".
		" WHERE ".
		" 	repo.occurrences_date BETWEEN '{$date_3y_befor}/04/01' AND '{$date_2y_befor}/03/31' ".
		" UNION ".
		" SELECT ".
		" 	COALESCE(SUM(case repo.requirements1 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements1_count ".
		" 	,COALESCE(SUM(case repo.requirements2 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements2_count ".
		" 	,COALESCE(SUM(case repo.requirements3 ".
		" 		WHEN 't' THEN 1 ".
		" 		ELSE 0 ".
		" 	END),0) as total_requirements3_count ".
		" 	,varchar(4) '{$date_4y_befor}' as occurrences_year ".
		" FROM ".
		" 	(SELECT ".
		" 		CASE ".
		" 			WHEN s.hospital_id IS NULL THEN f.hospital_id ".
		" 			ELSE s.hospital_id ".
		" 		END as hospital_id ".
		" 		, CASE ".
		" 			WHEN s.occurrences_date IS NULL THEN f.occurrences_date ".
		" 			ELSE s.occurrences_date ".
		" 		END as occurrences_date ".
		" 		, CASE ".
		" 			WHEN s.requirements1 IS NULL THEN f.requirements1 ".
		" 			ELSE s.requirements1 ".
		" 		END as requirements1 ".
		" 		, CASE ".
		" 			WHEN s.requirements2 IS NULL THEN f.requirements2 ".
		" 			ELSE s.requirements2 ".
		" 		END as requirements2 ".
		" 		, CASE ".
		" 			WHEN s.requirements3 IS NULL THEN f.requirements3 ".
		" 			ELSE s.requirements3 ".
		" 		END as requirements3 ".
		" 	FROM ".
		" 		((fplus_medical_accident_report_management am left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 		  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 	WHERE ".
		" 		(f.apply_id is not null ".
		" 		OR s.apply_id is not null) ".
		" 		AND am.del_flg <> 't') repo ".
		" WHERE ".
		" 	repo.occurrences_date BETWEEN '{$date_4y_befor}/04/01' AND '{$date_3y_befor}/03/31' ";
		" ORDER BY ".
		" 	occurrences_year ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);
	
	return $result;

}

//====================================================================
//報告総件数取得(職種別用)
//====================================================================
function getReportPerHospitalBed4Job($result){
	for($i = 0;$i < count($result);$i++){
		$report_per_hospital_bed[$i] = $result[$i]["total_report_count"] / (float)$result[$i]["hospital_bed"];
	}
	return $report_per_hospital_bed;
}

//====================================================================
// 病床数取得
//====================================================================
function getHospitalbedList($con,$fname,$result,$date_this_y,$date_last_y){
	$target_hospital_id = "";
	for($i = 0;$i < count($result);$i++){
		if($i == 0){
			$target_hospital_id .= $result[$i]["hospital_id"];
		}else{
			$target_hospital_id .= ",".$result[$i]["hospital_id"];
		}
	}
	//病院病床数検索
	$sql = " SELECT ".
		" 	his.hospital_id ".
		" 	, his.hospital_bed ".
		" 	, varchar(4) '$date_this_y' as nendo ".
		" FROM ".
		" 	(SELECT   ".
		" 		his.hospital_id ".
		" 		, max(his.fiscal_year) as latest_fiscal_year ".
		" 	FROM   ".
		" 		fplus_hospital_history his ".
		" 	WHERE ".
		" 		his.del_flg = 'f' ".
		" 		AND his.closing_flg = 'f' ".
		" 		AND his.fiscal_year <= '$date_this_y' ".
		" 	GROUP BY ".
		" 		his.hospital_id) history_year ".
		" 	, fplus_hospital_history his ".
		" WHERE   ".
		" 	his.fiscal_year = history_year.latest_fiscal_year ".
		" 	AND his.del_flg = 'f' ".
		" 	AND his.hospital_id = history_year.hospital_id ".
		" 	AND his.hospital_id in ($target_hospital_id) ".
		" UNION ".
		" SELECT ".
		" 	his.hospital_id ".
		" 	, his.hospital_bed ".
		" 	, varchar(4) '$date_last_y' as nendo ".
		" FROM ".
		" 	(SELECT   ".
		" 		his.hospital_id ".
		" 		, max(his.fiscal_year) as latest_fiscal_year ".
		" 	FROM   ".
		" 		fplus_hospital_history his ".
		" 	WHERE ".
		" 		his.del_flg = 'f' ".
		" 		AND his.closing_flg = 'f' ".
		" 		AND his.fiscal_year <= '$date_last_y' ".
		" 	GROUP BY ".
		" 		his.hospital_id) history_year ".
		" 	, fplus_hospital_history his ".
		" WHERE   ".
		" 	his.fiscal_year = history_year.latest_fiscal_year ".
		" 	AND his.del_flg = 'f' ".
		" 	AND his.hospital_id = history_year.hospital_id ".
		" 	AND his.hospital_id in ($target_hospital_id) ".
		" ORDER BY ".
		" 	hospital_id ".
		" 	, nendo ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hospital_bed_list = pg_fetch_all($sel);

	return $hospital_bed_list;
}

//====================================================================
// データマトリクスを作成する
//====================================================================
//レベル別
function getDatalist_a($result,$horz_field_definision,$hospital_bed_list,$date_this_y,$date_last_y){
	$data_list = array();

	// 行データを描画
	$row_kbn_befor = "";
	$hospital_id_befor = "";
	$row_idx = 0;
	for($i = 0;$i < count($result);$i++){
		$hospital_id_now = $result[$i]["hospital_id"];

		if($i == 0){
			if($result[$i]["kbn"] == "1"){
				//当年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j < 2){
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}else{
						if($j == 3){
							$data_list[$row_idx][$j] = round($result[$i][$horz_field_definision[$j]["dbkey"]],2);
						}else{
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
					}
					
				}
				$row_idx += 1;
				$row_kbn_befor = "1";

			}else{
				//当年データ描画（空データ）
				//病床数取得
				foreach($hospital_bed_list as $idx => $row){
					if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
						$hospital_bed = $row["hospital_bed"];
						break;
					}
				}

				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}elseif($j == 1){
						$data_list[$row_idx][$j] = $date_this_y;
					}elseif($j == 2){
						$data_list[$row_idx][$j] = $hospital_bed;
					}else{
						$data_list[$row_idx][$j] = 0;
					}
				}
				$row_idx += 1;

				//前年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						if($j == 3){
							$data_list[$row_idx][$j] = round($result[$i][$horz_field_definision[$j]["dbkey"]],2);
						}else{
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j]);
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";
			}
		}else{
			if($hospital_id_now == $hospital_id_befor){
				// 昨年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						if($j == 3){
							$data_list[$row_idx][$j] = round($result[$i][$horz_field_definision[$j]["dbkey"]],2);
						}else{
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";


			}else{ //病院が変わった時
				if($row_kbn_befor == "1"){
					//前年データ（空データ）描画
					//病床数取得
					foreach($hospital_bed_list as $idx => $row){
						if($row["hospital_id"] == $hospital_id_befor && $row["nendo"] == "$date_last_y"){
							$hospital_bed = $row["hospital_bed"];
							break;
						}
					}

					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = $date_last_y;
						}elseif($j == 2){
							$data_list[$row_idx][$j] = $hospital_bed;
						}else{
							$data_list[$row_idx][$j] = 0;
						}
					}
					$row_idx += 1;

					//増減
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j <= 3){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = "";
							}
						}else{
							$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
						}
					}
					$row_idx += 1;

					//増減率
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j <= 3){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = "";
							}
						}else{
							$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
						}
					}
					$row_idx += 1;

					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 3){
								$data_list[$row_idx][$j] = round($result[$i][$horz_field_definision[$j]["dbkey"]],2);
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						//病床数取得
						foreach($hospital_bed_list as $idx => $row){
							if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
								$hospital_bed = $row["hospital_bed"];
								break;
							}
						}

						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}elseif($j == 2){
								$data_list[$row_idx][$j] = $hospital_bed;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								if($j == 3){
									$data_list[$row_idx][$j] = round($result[$i][$horz_field_definision[$j]["dbkey"]],2);
								}else{
									$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
								}
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;


						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減率";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}else{
					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 3){
								$data_list[$row_idx][$j] = round($result[$i][$horz_field_definision[$j]["dbkey"]],2);
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "1";
					}else{
						// 当年データ描画（空データ）
						//病床数取得
						foreach($hospital_bed_list as $idx => $row){
							if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
								$hospital_bed = $row["hospital_bed"];
								break;
							}
						}

						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}elseif($j == 2){
								$data_list[$row_idx][$j] = $hospital_bed;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								if($j == 3){
									$data_list[$row_idx][$j] = round($result[$i][$horz_field_definision[$j]["dbkey"]],2);
								}else{
									$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
								}
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;

						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減率";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}
			}
		}
		$hospital_id_befor = $result[$i]["hospital_id"];
	}

	//レコードのラストが当年データの場合
	if($row_kbn_befor == "1"){
		//前年データ（空データ）描画
		//病床数取得
		foreach($hospital_bed_list as $idx => $row){
			if($row["hospital_id"] == $hospital_id_befor && $row["nendo"] == "$date_last_y"){
				$hospital_bed = $row["hospital_bed"];
				break;
			}
		}
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = $date_last_y;
			}elseif($j == 2){
				$data_list[$row_idx][$j] = $hospital_bed;
			}else{
				$data_list[$row_idx][$j] = 0;
			}
		}
		$row_idx += 1;

		//増減
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j <= 3){
				if($j == 0){
					$data_list[$row_idx][$j] = "";
				}elseif($j == 1){
					$data_list[$row_idx][$j] = "増減";
				}else{
					$data_list[$row_idx][$j] = "";
				}
			}else{
				$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
			}
		}
		$row_idx += 1;

		//増減率
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j <= 3){
				if($j == 0){
					$data_list[$row_idx][$j] = "";
				}elseif($j == 1){
					$data_list[$row_idx][$j] = "増減率";
				}else{
					$data_list[$row_idx][$j] = "";
				}
			}else{
				$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
			}
		}
		$row_idx += 1;
	}
	return $data_list;
}

//転倒・転落
function getDatalist_b($result,$horz_field_definision,$date_this_y,$date_last_y){
	$data_list = array();

	// 行データを描画
	$row_kbn_befor = "";
	$hospital_id_befor = "";
	$row_idx = 0;
	for($i = 0;$i < count($result);$i++){
		$hospital_id_now = $result[$i]["hospital_id"];

		if($i == 0){
			if($result[$i]["kbn"] == "1"){
				//当年データ格納
				for ($j = 0; $j < count($horz_field_definision); $j++){
					$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
				}
				$row_idx += 1;
				$row_kbn_befor = "1";

			}else{
				//当年データ描画（空データ）
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}elseif($j == 1){
						$data_list[$row_idx][$j] = $date_this_y;
					}else{
						$data_list[$row_idx][$j] = 0;
					}
				}
				$row_idx += 1;

				//前年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減";
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減率";
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";
			}
		}else{
			if($hospital_id_now == $hospital_id_befor){
				// 昨年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減";
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減率";
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}

				}
				$row_idx += 1;
				$row_kbn_befor = "2";


			}else{ //病院が変わった時
				if($row_kbn_befor == "1"){
					//前年データ（空データ）描画
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = $date_last_y;
						}else{
							$data_list[$row_idx][$j] = 0;
						}
					}
					$row_idx += 1;

					//増減
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
						}
					}
					$row_idx += 1;

					//増減率
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
						}
					}
					$row_idx += 1;

					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;


						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}else{
					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;

						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}
			}
		}
		$hospital_id_befor = $result[$i]["hospital_id"];
	}

	//レコードのラストが当年データの場合
	if($row_kbn_befor == "1"){
		//前年データ（空データ）描画
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = $date_last_y;
			}else{
				$data_list[$row_idx][$j] = 0;
			}
		}
		$row_idx += 1;

		//増減
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = "増減";
			}else{
				$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
			}
		}
		$row_idx += 1;

		//増減率
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = "増減率";
			}else{
				$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
			}
		}
		$row_idx += 1;
	}
	return $data_list;
}

//職種別
function getDatalist_c($result,$horz_field_definision,$report_per_hospital_bed,$hospital_bed_list,$date_this_y,$date_last_y){
	$data_list = array();

	// 行データを描画
	$row_kbn_befor = "";
	$hospital_id_befor = "";
	$row_idx = 0;
	for($i = 0;$i < count($result);$i++){
		$hospital_id_now = $result[$i]["hospital_id"];

		if($i == 0){
			if($result[$i]["kbn"] == "1"){
				//当年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j < 2){
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}else{
						if($j == 3){
							$data_list[$row_idx][$j] = round($report_per_hospital_bed[$i],2);
						}else{
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
					}
					
				}
				$row_idx += 1;
				$row_kbn_befor = "1";

			}else{
				//当年データ描画（空データ）
				//病床数取得
				foreach($hospital_bed_list as $idx => $row){
					if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
						$hospital_bed = $row["hospital_bed"];
						break;
					}
				}

				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}elseif($j == 1){
						$data_list[$row_idx][$j] = $date_this_y;
					}elseif($j == 2){
						$data_list[$row_idx][$j] = $hospital_bed;
					}else{
						$data_list[$row_idx][$j] = 0;
					}
				}
				$row_idx += 1;

				//前年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						if($j == 3){
							$data_list[$row_idx][$j] = round($report_per_hospital_bed[$i],2);
						}else{
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j]);
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";
			}
		}else{
			if($hospital_id_now == $hospital_id_befor){
				// 昨年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						if($j == 3){
							$data_list[$row_idx][$j] = round($report_per_hospital_bed[$i],2);
						}else{
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 3){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";


			}else{ //病院が変わった時
				if($row_kbn_befor == "1"){
					//前年データ（空データ）描画
					//病床数取得
					foreach($hospital_bed_list as $idx => $row){
						if($row["hospital_id"] == $hospital_id_befor && $row["nendo"] == "$date_last_y"){
							$hospital_bed = $row["hospital_bed"];
							break;
						}
					}

					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = $date_last_y;
						}elseif($j == 2){
							$data_list[$row_idx][$j] = $hospital_bed;
						}else{
							$data_list[$row_idx][$j] = 0;
						}
					}
					$row_idx += 1;

					//増減
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j <= 3){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = "";
							}
						}else{
							$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
						}
					}
					$row_idx += 1;

					//増減率
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j <= 3){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = "";
							}
						}else{
							$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
						}
					}
					$row_idx += 1;

					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 3){
								$data_list[$row_idx][$j] = round($report_per_hospital_bed[$i],2);
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						//病床数取得
						foreach($hospital_bed_list as $idx => $row){
							if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
								$hospital_bed = $row["hospital_bed"];
								break;
							}
						}

						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}elseif($j == 2){
								$data_list[$row_idx][$j] = $hospital_bed;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								if($j == 3){
									$data_list[$row_idx][$j] = round($report_per_hospital_bed[$i],2);
								}else{
									$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
								}
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;


						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減率";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}else{
					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 3){
								$data_list[$row_idx][$j] = round($report_per_hospital_bed[$i],2);
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;


						$row_kbn_befor = "1";
					}else{
						// 当年データ描画（空データ）
						//病床数取得
						foreach($hospital_bed_list as $idx => $row){
							if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
								$hospital_bed = $row["hospital_bed"];
								break;
							}
						}

						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}elseif($j == 2){
								$data_list[$row_idx][$j] = $hospital_bed;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								if($j == 3){
									$data_list[$row_idx][$j] = round($report_per_hospital_bed[$i],2);
								}else{
									$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
								}
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;

						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 3){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減率";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}
			}
		}
		$hospital_id_befor = $result[$i]["hospital_id"];
	}

	//レコードのラストが当年データの場合
	if($row_kbn_befor == "1"){
		//前年データ（空データ）描画
		//病床数取得
		foreach($hospital_bed_list as $idx => $row){
			if($row["hospital_id"] == $hospital_id_befor && $row["nendo"] == "$date_last_y"){
				$hospital_bed = $row["hospital_bed"];
				break;
			}
		}
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = $date_last_y;
			}elseif($j == 2){
				$data_list[$row_idx][$j] = $hospital_bed;
			}else{
				$data_list[$row_idx][$j] = 0;
			}
		}
		$row_idx += 1;

		//増減
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j <= 3){
				if($j == 0){
					$data_list[$row_idx][$j] = "";
				}elseif($j == 1){
					$data_list[$row_idx][$j] = "増減";
				}else{
					$data_list[$row_idx][$j] = "";
				}
			}else{
				$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
			}
		}
		$row_idx += 1;

		//増減率
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j <= 3){
				if($j == 0){
					$data_list[$row_idx][$j] = "";
				}elseif($j == 1){
					$data_list[$row_idx][$j] = "増減率";
				}else{
					$data_list[$row_idx][$j] = "";
				}
			}else{
				$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
			}
		}
		$row_idx += 1;
	}
	return $data_list;
}

//病床規模別
function getDatalist_d($result,$horz_field_definision,$date_this_y,$date_last_y){
	$data_list = array();

	// 行データを描画
	$row_kbn_befor = "";
	$bed_kbn_befor = "";
	$row_idx = 0;
	for($i = 0;$i < count($result);$i++){
		$bed_kbn_now = $result[$i]["bed_kbn"];

		if($i == 0){
			if($result[$i]["kbn"] == "1"){
				//当年データ格納
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						if($bed_kbn_now == "1"){
							$data_list[$row_idx][$j] = "400床以上";
						}elseif($bed_kbn_now == "2"){
							$data_list[$row_idx][$j] = "250〜399床";
						}else{
							$data_list[$row_idx][$j] = "249床以下";
						}
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "1";

			}else{
				//当年データ描画（空データ）
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						if($bed_kbn_now == "1"){
							$data_list[$row_idx][$j] = "400床以上";
						}elseif($bed_kbn_now == "2"){
							$data_list[$row_idx][$j] = "250〜399床";
						}else{
							$data_list[$row_idx][$j] = "249床以下";
						}
					}elseif($j == 1){
						$data_list[$row_idx][$j] = $date_this_y;
					}else{
						$data_list[$row_idx][$j] = 0;
					}
				}
				$row_idx += 1;

				//前年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減";
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減率";
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";
			}
		}else{
			if($bed_kbn_now == $bed_kbn_befor){
				// 昨年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減";
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減率";
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}

				}
				$row_idx += 1;
				$row_kbn_befor = "2";


			}else{ //病床規模が変わった時
				if($row_kbn_befor == "1"){
					//前年データ（空データ）描画
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = $date_last_y;
						}else{
							$data_list[$row_idx][$j] = 0;
						}
					}
					$row_idx += 1;

					//増減
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
						}
					}
					$row_idx += 1;

					//増減率
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
						}
					}
					$row_idx += 1;

					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								if($bed_kbn_now == "1"){
									$data_list[$row_idx][$j] = "400床以上";
								}elseif($bed_kbn_now == "2"){
									$data_list[$row_idx][$j] = "250〜399床";
								}else{
									$data_list[$row_idx][$j] = "249床以下";
								}
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								if($bed_kbn_now == "1"){
									$data_list[$row_idx][$j] = "400床以上";
								}elseif($bed_kbn_now == "2"){
									$data_list[$row_idx][$j] = "250〜399床";
								}else{
									$data_list[$row_idx][$j] = "249床以下";
								}
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;


						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}else{
					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								if($bed_kbn_now == "1"){
									$data_list[$row_idx][$j] = "400床以上";
								}elseif($bed_kbn_now == "2"){
									$data_list[$row_idx][$j] = "250〜399床";
								}else{
									$data_list[$row_idx][$j] = "249床以下";
								}
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								if($bed_kbn_now == "1"){
									$data_list[$row_idx][$j] = "400床以上";
								}elseif($bed_kbn_now == "2"){
									$data_list[$row_idx][$j] = "250〜399床";
								}else{
									$data_list[$row_idx][$j] = "249床以下";
								}
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;

						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}
			}
		}
		$bed_kbn_befor = $result[$i]["bed_kbn"];
	}

	//レコードのラストが当年データの場合
	if($row_kbn_befor == "1"){
		//前年データ（空データ）描画
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = $date_last_y;
			}else{
				$data_list[$row_idx][$j] = 0;
			}
		}
		$row_idx += 1;

		//増減
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = "増減";
			}else{
				$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
			}
		}
		$row_idx += 1;

		//増減率
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = "増減率";
			}else{
				$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
			}
		}
		$row_idx += 1;
	}
	return $data_list;
}

//入院看護基準別
function getDatalist_e($result,$horz_field_definision,$date_this_y,$date_last_y){
	$data_list = array();

	// 行データを描画
	$row_kbn_befor = "";
	$nursing_standard_id_befor = "";
	$row_idx = 0;
	for($i = 0;$i < count($result);$i++){
		$nursing_standard_id_now = $result[$i]["nursing_standard_id"];

		if($i == 0){
			if($result[$i]["kbn"] == "1"){
				//当年データ格納
				for ($j = 0; $j < count($horz_field_definision); $j++){
					$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
				}
				$row_idx += 1;
				$row_kbn_befor = "1";

			}else{
				//当年データ描画（空データ）
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}elseif($j == 1){
						$data_list[$row_idx][$j] = $date_this_y;
					}else{
						$data_list[$row_idx][$j] = 0;
					}
				}
				$row_idx += 1;

				//前年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減";
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減率";
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";
			}
		}else{
			if($nursing_standard_id_now == $nursing_standard_id_befor){
				// 昨年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減";
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}elseif($j == 1){
						$data_list[$row_idx][$j] = "増減率";
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}

				}
				$row_idx += 1;
				$row_kbn_befor = "2";


			}else{ //病床規模が変わった時
				if($row_kbn_befor == "1"){
					//前年データ（空データ）描画
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = $date_last_y;
						}else{
							$data_list[$row_idx][$j] = 0;
						}
					}
					$row_idx += 1;

					//増減
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
						}
					}
					$row_idx += 1;

					//増減率
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
						}
					}
					$row_idx += 1;

					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;


						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}else{
					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;

						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}
			}
		}
		$nursing_standard_id_befor = $result[$i]["nursing_standard_id"];
	}

	//レコードのラストが当年データの場合
	if($row_kbn_befor == "1"){
		//前年データ（空データ）描画
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = $date_last_y;
			}else{
				$data_list[$row_idx][$j] = 0;
			}
		}
		$row_idx += 1;

		//増減
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = "増減";
			}else{
				$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
			}
		}
		$row_idx += 1;

		//増減率
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = "増減率";
			}else{
				$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
			}
		}
		$row_idx += 1;
	}
	return $data_list;
}

//事故報告件数（要件別）
function getDatalist_f($result,$horz_field_definision,$hospital_bed_list,$date_this_y,$date_last_y){
	$data_list = array();

	// 行データを描画
	$row_kbn_befor = "";
	$hospital_id_befor = "";
	$row_idx = 0;
	for($i = 0;$i < count($result);$i++){
		$hospital_id_now = $result[$i]["hospital_id"];

		if($i == 0){
			if($result[$i]["kbn"] == "1"){
				//当年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
				}
				$row_idx += 1;
				$row_kbn_befor = "1";

			}else{
				//当年データ描画（空データ）
				//病床数取得
				foreach($hospital_bed_list as $idx => $row){
					if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
						$hospital_bed = $row["hospital_bed"];
						break;
					}
				}

				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}elseif($j == 1){
						$data_list[$row_idx][$j] = $date_this_y;
					}elseif($j == 2){
						$data_list[$row_idx][$j] = $hospital_bed;
					}else{
						$data_list[$row_idx][$j] = 0;
					}
				}
				$row_idx += 1;

				//前年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 2){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j]);
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 2){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";
			}
		}else{
			if($hospital_id_now == $hospital_id_befor){
				// 昨年データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j == 0){
						$data_list[$row_idx][$j] = "";
					}else{
						$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
					}
				}
				$row_idx += 1;

				// 増減データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 2){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
					}
				}
				$row_idx += 1;

				// 増減率データ描画
				for ($j = 0; $j < count($horz_field_definision); $j++){
					if($j <= 2){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = "増減率";
						}else{
							$data_list[$row_idx][$j] = "";
						}
					}else{
						$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
					}
				}
				$row_idx += 1;
				$row_kbn_befor = "2";


			}else{ //病院が変わった時
				if($row_kbn_befor == "1"){
					//前年データ（空データ）描画
					//病床数取得
					foreach($hospital_bed_list as $idx => $row){
						if($row["hospital_id"] == $hospital_id_befor && $row["nendo"] == "$date_last_y"){
							$hospital_bed = $row["hospital_bed"];
							break;
						}
					}

					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j == 0){
							$data_list[$row_idx][$j] = "";
						}elseif($j == 1){
							$data_list[$row_idx][$j] = $date_last_y;
						}elseif($j == 2){
							$data_list[$row_idx][$j] = $hospital_bed;
						}else{
							$data_list[$row_idx][$j] = 0;
						}
					}
					$row_idx += 1;

					//増減
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j <= 2){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減";
							}else{
								$data_list[$row_idx][$j] = "";
							}
						}else{
							$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
						}
					}
					$row_idx += 1;

					//増減率
					for ($j = 0; $j < count($horz_field_definision); $j++){
						if($j <= 2){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}elseif($j == 1){
								$data_list[$row_idx][$j] = "増減率";
							}else{
								$data_list[$row_idx][$j] = "";
							}
						}else{
							$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
						}
					}
					$row_idx += 1;

					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
						$row_idx += 1;
						$row_kbn_befor = "1";

					}else{
						// 当年データ描画（空データ）
						//病床数取得
						foreach($hospital_bed_list as $idx => $row){
							if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
								$hospital_bed = $row["hospital_bed"];
								break;
							}
						}

						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}elseif($j == 2){
								$data_list[$row_idx][$j] = $hospital_bed;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 2){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;


						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 2){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減率";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}else{
					if($result[$i]["kbn"] == 1){
						// 当年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
						}
						$row_idx += 1;

						$row_kbn_befor = "1";
					}else{
						// 当年データ描画（空データ）
						//病床数取得
						foreach($hospital_bed_list as $idx => $row){
							if($row["hospital_id"] == $hospital_id_now && $row["nendo"] == "$date_this_y"){
								$hospital_bed = $row["hospital_bed"];
								break;
							}
						}

						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}elseif($j == 1){
								$data_list[$row_idx][$j] = $date_this_y;
							}elseif($j == 2){
								$data_list[$row_idx][$j] = $hospital_bed;
							}else{
								$data_list[$row_idx][$j] = 0;
							}
						}
						$row_idx += 1;

						// 昨年データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j == 0){
								$data_list[$row_idx][$j] = "";
							}else{
								$data_list[$row_idx][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
							}
						}
						$row_idx += 1;

						// 増減データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 2){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
							}
						}
						$row_idx += 1;

						// 増減率データ描画
						for ($j = 0; $j < count($horz_field_definision); $j++){
							if($j <= 2){
								if($j == 0){
									$data_list[$row_idx][$j] = "";
								}elseif($j == 1){
									$data_list[$row_idx][$j] = "増減率";
								}else{
									$data_list[$row_idx][$j] = "";
								}
							}else{
								$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
							}
						}
						$row_idx += 1;

						$row_kbn_befor = "2";
					}
				}
			}
		}
		$hospital_id_befor = $result[$i]["hospital_id"];
	}

	//レコードのラストが当年データの場合
	if($row_kbn_befor == "1"){
		//前年データ（空データ）描画
		//病床数取得
		foreach($hospital_bed_list as $idx => $row){
			if($row["hospital_id"] == $hospital_id_befor && $row["nendo"] == "$date_last_y"){
				$hospital_bed = $row["hospital_bed"];
				break;
			}
		}
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j == 0){
				$data_list[$row_idx][$j] = "";
			}elseif($j == 1){
				$data_list[$row_idx][$j] = $date_last_y;
			}elseif($j == 2){
				$data_list[$row_idx][$j] = $hospital_bed;
			}else{
				$data_list[$row_idx][$j] = 0;
			}
		}
		$row_idx += 1;

		//増減
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j <= 2){
				if($j == 0){
					$data_list[$row_idx][$j] = "";
				}elseif($j == 1){
					$data_list[$row_idx][$j] = "増減";
				}else{
					$data_list[$row_idx][$j] = "";
				}
			}else{
				$data_list[$row_idx][$j] = $data_list[$row_idx - 2][$j] - $data_list[$row_idx - 1][$j];
			}
		}
		$row_idx += 1;

		//増減率
		for ($j = 0; $j < count($horz_field_definision); $j++){
			if($j <= 2){
				if($j == 0){
					$data_list[$row_idx][$j] = "";
				}elseif($j == 1){
					$data_list[$row_idx][$j] = "増減率";
				}else{
					$data_list[$row_idx][$j] = "";
				}
			}else{
				$data_list[$row_idx][$j] = ($data_list[$row_idx - 2][$j] != 0 ? number_format(round($data_list[$row_idx - 3][$j] / $data_list[$row_idx - 2][$j]*100,2),2)."%" : "-");
			}
		}
		$row_idx += 1;
	}
	return $data_list;
}

function getDatalist_g($result, $vert_fields_definision, $horz_field_definision)
{
	// 生成するデータリスト
	$data_list = array();
	
	// 列合計
	$total_column_arr = array();
	
	// 総合計
	$total_num = 0;
	
	// 行
	for ($i = 0; $i < count($vert_fields_definision); $i++) {
		// 行合計
		$total_row_num = 0;
		
		$row_code = $vert_fields_definision[$i]["code"];
		$row_str = $vert_fields_definision[$i]["name"];
		
		// 列
		for ($j = 0; $j < count($horz_field_definision); $j++) {
			if ($j == 0) {
				// 行のタイトル(左)
				$data_list[$i][$j] = $row_str;
				continue;
			} else if ($i == (count($vert_fields_definision) - 1) && $j == (count($horz_field_definision) - 1)) {
				// 総合計
				$data_list[$i][$j] = $total_num;
			} else if ($j == (count($horz_field_definision) - 1)) {
				// 合計(右)
				$data_list[$i][$j] = $total_row_num;
			} else if ($i == (count($vert_fields_definision) - 1)) {
				// 合計(下)
				$data_list[$i][$j] = $total_column_arr[$j];
			} else {
				// 初期値設定
				$data_list[$i][$j] = 0;
				if ($i == 0) {
					$total_column_arr[$j] = 0;
				}
				
				foreach ($result as $tmp) {
					$tmp_require = $tmp["require" . $j];
					$tmp_outline = $tmp["outline"];
					
					if ($row_code == $tmp_outline && $tmp_require == "t") {
						$data_list[$i][$j]++;
						$total_row_num++;
						$total_column_arr[$j]++;
						$total_num++;
					}
				}
			}
		}
	}
	
	return $data_list;
}

function getDatalist_h($result){
	// 生成するデータリスト
	$data_list = array();
	$chk_cnt = 0;

	$data_list[0][0] = "報告病院数";
	for($i=0;$i<count($result);$i++){
		$data_list[0][($i+1)] = $result[$i]["hospital_count"];
		if($result[$i]["hospital_count"]){
			$chk_cnt += (int)$data_list[0][($i+1)];
		}
	}

	if($chk_cnt == 0){
		return false;
	}else{
		return $data_list;
	}
}

function getDatalist_h_2($result,$vert_fields_definision){
	// 生成するデータリスト
	$data_list = array();
	$chk_cnt = 0;

	for($i=0;$i<count($result);$i++){
		for($j=0;$j<4;$j++){
			if($j == 0){
				$data_list[$i][$j] = $result[$i][$vert_fields_definision[$j]["code"]]."年度";
			}else{
				$data_list[$i][$j] = $result[$i][$vert_fields_definision[$j]["code"]];
				$chk_cnt += (int)$data_list[$i][$j];
			}
		}
	}

	if($chk_cnt == 0){
		return false;
	}else{
		return $data_list;
	}

}

?>