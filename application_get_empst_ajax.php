<?
ob_start();
require("about_authority.php");
require("about_session.php");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	ajax_server_erorr();
	exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, 7, $fname);
if ($check_auth == "0") {
	pg_close($con);
	ajax_server_erorr();
	exit;
}


// データベースに接続
$con = connect2db($fname);
if ($apply_id != "") {
	update_applyapv($con, $fname, $emp_id, $apply_id, $apv_order, $apv_sub_order, $kind_flag);
}

$st_nm = get_empst_ajax($con, $fname, $emp_id);

//レスポンスデータ作成
if($st_nm == "")
{
	//検索にヒットしない。
	$response_data = "ajax=0hit\n";
}
else
{

	$response_data = "ajax=success\n";
		
	$p_key = "st_nm";
	$p_value = $st_nm;
	$response_data .= "$p_key=$p_value\n";
	
	$p_key = "item_id";
	$p_value = $item_id;
	$response_data .= "$p_key=$p_value\n";
}	
	
print $response_data;

// データベース切断
pg_close($con);



function get_empst_ajax($con, $fname, $emp_id)
{
	$sql  = "select stmst.st_nm from empmst ";
	$sql .= "inner join stmst on empmst.emp_st = stmst.st_id ";
	$cond = "where empmst.emp_id = '$emp_id' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		ajax_server_erorr();
		exit;
	}

	$st_nm = pg_fetch_result($sel, 0, "st_nm");

	return $st_nm;
}	


function ajax_server_erorr()
{
	print "ajax=error\n";
	exit;
}

function update_applyapv($con, $fname, $emp_id, $apply_id, $apv_order, $apv_sub_order, $kind_flag) {

	pg_query($con, "begin transaction");

	$sql  = "select emp_st, emp_class, emp_attribute, emp_dept from empmst ";
	$cond = "where empmst.emp_id = '$emp_id' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		ajax_server_erorr();
		exit;
	}

	$st_id = pg_fetch_result($sel, 0, "emp_st");
	$emp_class = pg_fetch_result($sel, 0, "emp_class");
	$emp_attribute = pg_fetch_result($sel, 0, "emp_attribute");
	$emp_dept = pg_fetch_result($sel, 0, "emp_dept");

	if ($kind_flag == "1" ) {
		$sql = "update applyapv set";
		$set = array("emp_id", "emp_st", "emp_class", "emp_attribute", "emp_dept");
		$setvalue = array($emp_id, $st_id, $emp_class, $emp_attribute, $emp_dept);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order ";
		if ($apv_sub_order > 0) {
			$cond .= " and apv_sub_order = $apv_sub_order";
		}
	}
	else {
		if ($kind_flag == "2") {
			$tablename = "ovtmaprv";
		}
		else {
			$tablename = "tmmdaprv";
		}
		$sql = "update $tablename set";
		$set = array("aprv_emp_id", "emp_st");
		$setvalue = array($emp_id, $st_id);
		$cond = "where apply_id = $apply_id and aprv_no = $apv_order ";
		if ($apv_sub_order > 0) {
			$cond .= " and aprv_sub_no = $apv_sub_order";
		}
	}
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		ajax_server_erorr();
		exit;
	}
	//delete apvemp
	if ($kind_flag == "1" ) {
		$tablename = "applyapvemp";
		$cond = "where apply_id = $apply_id and apv_order = $apv_order ";
	}
	elseif ($kind_flag == "2" ) {
		$tablename = "ovtmaprvemp";
		$cond = "where apply_id = $apply_id and aprv_no = $apv_order ";
	}
	else {
		$tablename = "tmmdaprvemp";
		$cond = "where apply_id = $apply_id and aprv_no = $apv_order ";
	}
	$sql = "delete from $tablename";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		ajax_server_erorr();
		exit;
	}
	pg_query("commit");

}

?>
