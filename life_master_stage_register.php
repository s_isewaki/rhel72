<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 54, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// ライフステージ一覧を取得
$sql = "select name from lifestage";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$stages = array();
while ($row = pg_fetch_array($sel)) {
	$stages[] = $row["name"];
}
$stage_count = count($stages);

// 表示順が指定されていなければ末尾とする
if ($order_no == "") {
	$order_no = $stage_count + 1;
}

// イントラメニュー名を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu3 = pg_fetch_result($sel, 0, "menu3");
$intra_menu3_1 = pg_fetch_result($sel, 0, "menu3_1");
?>
<title>マスターメンテナンス | <? echo($intra_menu3); ?>設定 | ライフステージ登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setOrderNo(order_no) {
	document.mainform.order_no.value = order_no;
	document.mainform.action = 'life_master_stage_register.php';
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_life.php?session=<? echo($session); ?>"><b><? echo($intra_menu3); ?></b></a> &gt; <a href="life_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_life.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="life_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="life_master_stage_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライフステージ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#5279a5"><a href="life_master_stage_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ライフステージ登録</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="life_master_stage_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライフステージ</font></td>
</tr>
<?
for ($i = 1; $i < $order_no; $i++) {
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$i</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$stages[$i - 1]}</font></td>\n");
	echo("</tr>\n");
}
?>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($order_no); ?> <input type="button" value="↑" onclick="setOrderNo(<? echo($order_no - 1); ?>);"<? if ($order_no == "1") {echo(" disabled");} ?>> <input type="button" value="↓" onclick="setOrderNo(<? echo($order_no + 1); ?>);"<? if ($order_no == $stage_count + 1) {echo(" disabled");} ?>></font></td>
<td><input type="text" name="name" value="<? echo($name); ?>" size="40" maxlength="50" style="ime-mode:active;"></td>
</tr>
<?
for ($i = $order_no + 1; $i <= $stage_count + 1; $i++) {
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$i</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$stages[$i - 2]}</font></td>\n");
	echo("</tr>\n");
}
?>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="order_no" value="<? echo($order_no); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
