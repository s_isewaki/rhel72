<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 有給休暇付与日数表（４月１日基準、入職年度用）</title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_timecard_common.ini");
require_once("atdbk_common_class.php");
require_once("atdbk_menu_common.ini");
require_once("timecard_hol_menu_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);
$arr_kinzoku_su = array("", "");
//設定保存
if ($postback == "t") {
	//数値チェック
	for ($i=1; $i<=8; $i++) {
		for ($j=1; $j<=12; $j++) {
			$varname = "add_day_{$i}_{$j}";
			$wk_str = $$varname;
            //追加分（週５日、定時、パート）は未設定でもエラーとしない 20120330
            if (($i >= 6 && $i <= 8) && $wk_str == "") {
                ;
            }
            else if (preg_match("/^\d{1,3}$/", $wk_str) == 0) {
				pg_close($con);

				if ($j <= 9) {
					$wk_mon_str = ($j+3)."月入職";
				} else {
					$wk_mon_str = ($j-9)."月入職";
				}
				//名称変更 常勤 週ｎ日 20101117
				$wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($i);
				echo("<script type=\"text/javascript\">alert('{$wk_tbl_id_name}、{$wk_mon_str}の日数は半角数字1〜3桁で入力してください。');</script>");
				echo("<script type=\"text/javascript\">history.back();</script>");
				exit;
			}
		}
	}
	pg_query($con, "begin transation");
	//削除
	$sql = "delete from timecard_paid_hol2";
	$cond = "";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	//登録
	for ($i=0; $i<8; $i++) {
		$add_day_str = "";
		for ($j=1; $j<=12; $j++) {
			$add_day_str .= ", add_day$j";
		}
		$sql = "insert into timecard_paid_hol2 (paid_hol_tbl_id, order_no $add_day_str) values (";
		$wk_id = $i+1;

		$content = array($wk_id);
        //ソート順
        $order_no = "order_id_{$wk_id}";
        $wk_str = $$order_no;
        if ($wk_str == "") {
            $wk_str = null;
        }
        array_push($content, $wk_str);

		for ($j=1; $j<=12; $j++) {
			$varname = "add_day_{$wk_id}_{$j}";
			$wk_str = $$varname;
			if ($wk_str == "") {
				$wk_str = null;
			}
			array_push($content, $wk_str);
		}
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	pg_query("commit");
}
//タイムカード設定から基準月数を取得
$sql = "select criteria_months from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$criteria_months = pg_fetch_result($sel, 0, "criteria_months");
}
if ($criteria_months == "") {
	$criteria_months = "2";
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#bdd1e7 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block th {background: #bdd1e7; border:#5279a5 solid 1px; font:"ＭＳ Ｐゴシック"; }

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol2 order by order_no";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//休暇種別メニュータブ表示
show_timecard_hol_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<form name="update" action="timecard_paid_hol_tbl2.php" method="post">

<table>
	<tr>

		<td>
			<table width="660" border="0" cellspacing="0" cellpadding="1"  class="list">
				<tr bgcolor="#f6f9ff">
					<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表名称</font></td>
					<? for ($i=1; $i<=12; $i++) { ?>
					<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?
    if ($i<=9) {
		$wk_mon_str = $i+3;
	} else {
		$wk_mon_str = $i-9;
	}
					echo($wk_mon_str);
					?>月<br>入職
					</font></td>
					<? } ?>
				</tr>
<?
while($row = pg_fetch_array($sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
	for ($i=1; $i<=12; $i++) {
		$varname = "add_day".$i;
		$$varname       = $row["$varname"];
	}
?>
				<tr>
					<td nowrap bgcolor="#f6f9ff" align="center">
						<font size="2" face="ＭＳ Ｐゴシック, Osaka">
	<?
	//名称変更 常勤 週n日 20101117
	$wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($paid_hol_tbl_id);
	echo($wk_tbl_id_name);
	?>
						</font>
					</td>
	<?
	for ($i=1; $i<=12; $i++) {
		$varname = "add_day".$i;
		echo("<td align=\"center\">");
		echo("<input type=\"text\" name=\"add_day_{$paid_hol_tbl_id}_{$i}\" value=\"{$$varname}\" size=\"3\" maxlength=\"3\" style=\"ime-mode: inactive;text-align: right;\">");
		echo("</td>");
	}
	echo("</tr>");
    echo("<input type=\"hidden\" name=\"order_id_{$paid_hol_tbl_id}\" value=\"{$row["order_no"]}\">");
}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="submit" value="登録"></td>
	</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="postback" value="t">
</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
