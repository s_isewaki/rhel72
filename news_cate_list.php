<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | カテゴリ一覧</title>
<?php
//ini_set("display_errors","1");
require("show_news_cate_list.ini");
require("referer_common.ini");
require("news_common.ini");
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// ページ番号の設定
if ($page == "") {$page = 1;}

// データベースに接続
$con = connect2db($con);

// 遷移元の取得
$referer = get_referer($con, $session, "news", $fname);

// カテゴリをDBから取得
$category_list = get_category_list_from_db($con, $fname, 0, null, false);

// お知らせ集計機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_aggregate = $conf->get('setting_aggregate');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if($setting_aggregate != "t"){
    $setting_aggregate = "f";
}
?>
<?php
include_js("js/fontsize.js");
include_js("js/jquery/jquery-1.9.1.min.js");
include_js("js/jquery/jquery-ui-1.10.3.min.js");
?>
<script type="text/javascript">
function deleteRecords() {
    if (confirm('削除してよろしいですか？')) {
        document.delform.submit();
    }
}

$(function() {
    $('#check').click(function() {
        if($(this).get(0).checked) {
            $('input.delete').prop('checked', true);
        }
        else {
            $('input.delete').prop('checked', false);
        }
    });

    $('#sortable > tbody').sortable({
        items: '> tr.sortable',
        handle: '.handle',
        axis: 'y',
        opacity: 0.4
    });

    $('#btn_delete').click(function() {
        if ($('input.delete:checked').length <= 0) {
            alert('カテゴリがチェックされていません');
            return;
        }

        if (confirm('削除してよろしいですか？')) {
            $('#mode').val('delete');
            $('form[name=delform]').submit();
        }
    });

    $('#btn_sort').click(function() {
        $('#mode').val('sort');
        //$('#mode').val('test');
        $('form[name=delform]').submit();
    });
});
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<?php if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<?php echo($session); ?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>
<?php } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<?php echo($session); ?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<?php echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<?php echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<?php } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="90" height="22" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<?php echo($session); ?>&page=<?php echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="news_cate_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>カテゴリ一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="5">&nbsp;</td>
    <?php if($setting_aggregate == 't'){?>
    <td width="70" align="center" bgcolor="#bdd1e7">
        <a href="news_aggregate.php?session=<?php echo($session); ?>">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font>
        </a>
    </td>
    <?php } ?><td width="">&nbsp;</td><td align="right">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="updform" action="news_cate_list_update_exe.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="130"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">標準カテゴリ</font></td>
<td><input type="submit" value="更新"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ１</font></td>
<td width="15%"><input type="text" name="cate1" value="<?=$category_list['1']['newscate_name']?>" size="20"></td>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全てのユーザに通知されます。</font></td>
    <td><input type="checkbox" name="no_disp_flg1" value="t" <?php if ($category_list['1']['no_disp_flg'] == 't') { echo " checked"; } ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ２</font></td>
<td width="15%"><input type="text" name="cate2" value="<?=$category_list['2']['newscate_name']?>" size="20"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定した組織に所属するユーザに通知されます。</font></td>
    <td><input type="checkbox" name="no_disp_flg2" value="t" <?php if ($category_list['2']['no_disp_flg'] == 't') { echo " checked"; } ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ３</font></td>
<td width="15%"><input type="text" name="cate3" value="<?=$category_list['3']['newscate_name']?>" size="20"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定した職種のユーザに通知されます。</font></td>
    <td><input type="checkbox" name="no_disp_flg3" value="t" <?php if ($category_list['3']['no_disp_flg'] == 't') { echo " checked"; } ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ４</font></td>
<td width="15%"><input type="text" name="cate4" value="<?=$category_list['10000']['newscate_name']?>" size="20"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名簿から、職員氏名を指定して通知します。受信した各職員は、コメントを付与することができ、他者のコメントも参照することができます。</font></td>
    <td><input type="checkbox" name="no_disp_flg4" value="t" <?php if ($category_list['10000']['no_disp_flg'] == 't') { echo " checked"; } ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="page" value="<?php echo($page); ?>">
</form>
<img src="img/spacer.gif" alt="" width="1" height="2"><br><br><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="130"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">追加カテゴリ</font></td>
<td>
    <p>
        <button type="button" id="btn_delete" onclick="/* deleteRecords(); */">削除</button>
        <button type="button" id="btn_sort">並び順を更新する</button>
    </p>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="delform" action="news_cate_delete.php" method="post">
<table id="sortable" border="0" cellspacing="0" cellpadding="2" class="list">
    <thead>
        <tr height="22" bgcolor="#f6f9ff">
        <td width="30">&nbsp;</td>
        <td width="30">&nbsp;</td>
        <td width="400"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ名称</font></td>
        </tr>
    </thead>
    <tbody>
        <?php show_news_cate_list($con, $page, $session, $fname); ?>
    </tbody>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="page" value="<?php echo($page); ?>">
<input type="hidden" id="mode" name="mode" value="">
</form>
<?php //show_news_cate_page_list($con, $page, $session, $fname); ?>
</td>
</tr>
</table>
</body>
</html>
<?php pg_close($con);