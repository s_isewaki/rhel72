<?
ob_start();
require_once("cl_application_workflow_common_class.php");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;


// データベースに接続
$con = connect2db($fname);

$obj = new cl_application_workflow_common_class($con, $fname);

$emp_id = get_emp_id($con, $fname, $session);
$cnt = 0;
$arr_ward_apply = $obj->get_ward_apply($short_wkfw_name, $emp_id);
foreach($arr_ward_apply as $ward_apply)
{
    $arr = $obj->get_ward_eval_ym($ward_apply["apply_content"]);
    if($arr["eval_year"] == $eval_year && $arr["eval_month"] == $eval_month)
    {
        $cnt++;
        break;
    }
}

//レスポンスデータ作成
$response_data = "ajax=success\n";
$p_key = "cnt";
$p_value = $cnt;
$response_data .= "$p_key=$p_value\n";

print $response_data;

function get_emp_id($con, $fname, $session)
{
	$sql  = "select * from empmst";
	$cond = "where emp_id in (select emp_id from session where session_id='$session')";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
	    pg_close($con);
	    ajax_server_erorr();
	    exit;
    }
	return pg_fetch_result($sel, 0, "emp_id");
}






// データベース切断
pg_close($con);
?>
