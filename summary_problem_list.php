<?
ob_start();
require_once("about_comedix.php");
ob_end_clean();
/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID
	$type
		出力種別
*/

/**
 * プロブレムの一覧を表示します。
 * @param $mode 表示モード
 *               "active":アクティブ/インアクティブを表示
 *              ,"zizoku":持続性を表示
 *              ,"itizi":一時的を表示
 *              ,"all":全て表示
 */
function show_problem_list($con,$pt_id,$fname,$mode) {
	//==============================
	//パラメータ整理
	//==============================
	if($mode != "active" && $mode != "zizoku" && $mode != "itizi") {
		$mode = "all";
	}

	//==============================
	//DBより一覧データを取得
	//==============================
    $problem_list = getSummaryProblemList($con, $pt_id, $fname, $mode);
	if ($problem_list === false) {
		pg_close($con);
		summary_common_show_error_page_and_die();
	}
	//==============================
	//一覧表 出力
	//==============================
	?>
	<table class="listp_table" cellspacing="1">
		<tr>
			<th style="text-align:center; padding:2px; width:9%;"><nobr>登録年月日</nobr></th>
			<th style="text-align:center; padding:2px; width:4%;"><nobr>番号</nobr></th>
			<? if($mode == "active") { ?>
			<th style="text-align:center; padding:2px;"><nobr>アクティブ</nobr><br><nobr>プロブレム</nobr></th>
			<th style="text-align:center; padding:2px;"><nobr>インアクティブ</nobr><br/><nobr>プロブレム</nobr></th>
			<? } else { ?>
			<th style="text-align:center; padding:2px;"><nobr>プロブレム</nobr><br/><nobr>(問題)</nobr></th>
			<? } ?>
			<th style="text-align:center; padding:2px; width:12%;"><nobr>記載者職種<br/>記載者氏名</nobr></th>
			<th style="text-align:center; padding:2px; width:17%;"><nobr>問題分類</nobr></th>
			<th style="text-align:center; padding:2px; width:2%;">病<br/>名</th>
			<th style="text-align:center; padding:2px; width:9%;"><nobr>発生日<br/>解決日</th>
			<th style="text-align:center; padding:2px; width:230px;"><nobr>コメント</nobr></th>
		</tr>
		<?

		$list_disp_num = count($problem_list);
		for($i=0;$i<$list_disp_num;$i++) {
			$problem_id = $problem_list[$i]["problem_id"];
			$problem_class = $problem_list[$i]["problem_class"];
			$start_date = stringToDateFormat($problem_list[$i]["start_date"]);
			$end_date = stringToDateFormat($problem_list[$i]["end_date"]);
			$problem_html = h($problem_list[$i]["problem"]);
?>
		<tr>
			<!-- 登録年月日 -->
			<? $create_date = $problem_list[$i]["create_date"]; ?>
			<? $create_date_disp = substr($create_date, 0, 4)."/".substr($create_date, 4, 2)."/".substr($create_date, 6, 2); ?>
			<td align='center' rowspan="2">
				<nobr><a href="javascript:showProblemRirekiWindow(<? echo $problem_id; ?>);"><? echo $create_date_disp; ?></a></nobr>
			</td>

			<!-- 番号 -->
			<td align='center' rowspan="2">
				<nobr><? echo $problem_list[$i]["problem_list_no"]; ?></nobr>
			</td>

			<? if($mode == "active") { ?>
				<!-- アクティブプロブレム -->
				<td align='center' rowspan="2">
					<nobr><a href="javascript:showUpdateWindow(<? echo $problem_id; ?>);"><? echo ($problem_class=="アクティブ") ? $problem_html : ""; ?></a></nobr>
				</td>

				<!-- インアクティブプロブレム -->
				<td align='center' rowspan="2">
					<nobr><a href="javascript:showUpdateWindow(<? echo $problem_id; ?>);"><? echo ($problem_class=="インアクティブ") ? $problem_html : ""; ?></a></nobr>
				</td>
			<? } else { ?>
				<!-- プロブレム -->
				<td align='center' rowspan="2">
					<nobr><a href="javascript:showUpdateWindow(<? echo $problem_id; ?>);"><? echo $problem_html; ?></a></nobr>
				</td>
			<? } ?>

			<!-- 記載者 職種 -->
			<td align='center'>
				<nobr><? echo h($problem_list[$i]["job_nm"]); ?></nobr>
			</td>

			<!-- 問題分類 -->
			<td align='center' rowspan="2">
				<nobr><? echo $problem_list[$i]["problem_kind"]; ?></nobr>
			</td>

			<!-- 病名 -->
			<td align='center' rowspan="2">
				<nobr><? echo $problem_list[$i]["disease_name_flg"]=="t" ? "<span style='color: red;'>●</span>" : "○"; ?></nobr>
			</td>

			<!-- 発生日 -->
			<td align='center'>
				<nobr><? echo $start_date; ?></nobr>
			</td>

			<!-- コメント -->
			<td align='center' rowspan="2" style="width:230px;">
				<? echo h($problem_list[$i]["memo"]); ?>
			</td>

		</tr>
		<tr>
			<!-- 記載者 氏名 -->
			<td align='center'>
				<nobr><? echo h(trim($problem_list[$i]["emp_lt_nm"]." ".$problem_list[$i]["emp_ft_nm"])); ?></nobr>
			</td>

			<!-- 解決日 -->
			<td align='center'>
				<nobr><? echo $end_date; ?></nobr>
			</td>
		</tr>
<? } ?>
	</table>

<div class="search_button_div">
	<input type="button" onclick="showInsertWindow('<? echo $mode; ?>');" value="追加"/>
</div>
<?
}

function show_inpt_disease($con,$pt_id,$fname) {
?>
  
<table class="listp_table" cellspacing="1">
<tr>
  <th style="text-align:center; padding:2px; width:12%">入院日時</th>
  <th style="text-align:center; padding:2px; width:12%">退院日時</th>
  <th style="text-align:center; padding:2px; width:18%">診療科</th>
  <th style="text-align:center; padding:2px; width:26%">診断名(主病名)</th>
  <th style="text-align:center; padding:2px; width:12%">発症年月日</th>
  <th style="text-align:center; padding:2px; width:20%">担当医</th>
</tr>

<?
  // 入院履歴情報を取得
    $inpthist = getInpthistData($con, $pt_id, $fname);
    if ($inpthist === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }

    $idx = 0 ;
    foreach ($inpthist as $row) {
        $in_dt      = $row["inpt_in_dt"];
        $in_tm      = $row["inpt_in_tm"];
        $out_dt     = $row["inpt_out_dt"];
        $out_tm     = $row["inpt_out_tm"];
        $disease    = $row["inpt_disease"];
        $patho_from = $row["inpt_patho_from"];
        $patho_to   = $row["inpt_patho_to"];
        $sect_id    = $row["sect_id"];
        $sect_nm    = $row["sect_nm"];
        $dr_id      = $row["dr_id"];
        $dr_nm      = $row["dr_nm"];
        $tbl_nm     = $row["tbl_nm"];

        $in_dt_fm      = format_date($in_dt);
        $in_tm_fm      = format_time($in_tm);
        $out_dt_fm     = format_date($out_dt);
        $out_tm_fm     = format_time($out_tm);
        $patho_from_fm = format_date($patho_from);

        $in_dttm  = "$in_dt_fm $in_tm_fm";
        $out_dttm = "$out_dt_fm $out_tm_fm";

        $disease_enc = urlencode($disease) ;
?>
<tr>
  <td><a href="javascript:showUpdateDiseaseWindow(<? echo $idx; ?>);"><? echo $in_dttm; ?></a></td>
  <td><? echo $out_dttm; ?></td>
  <td><? echo $sect_nm; ?></td>
  <td><? echo $disease; ?></td>
  <td><? echo $patho_from_fm; ?></td>
  <td><? echo $dr_nm; ?>
    <input type="hidden" id="in_dt_<? echo $idx; ?>"      value="<? echo $in_dt; ?>">
    <input type="hidden" id="in_tm_<? echo $idx; ?>"      value="<? echo $in_tm; ?>">
    <input type="hidden" id="disease_<? echo $idx; ?>"    value="<? echo $disease_enc; ?>">
    <input type="hidden" id="patho_from_<? echo $idx; ?>" value="<? echo $patho_from; ?>">
    <input type="hidden" id="patho_to_<? echo $idx; ?>"   value="<? echo $patho_to; ?>">
    <input type="hidden" id="sect_id_<? echo $idx; ?>"    value="<? echo $sect_id; ?>">
    <input type="hidden" id="dr_id_<? echo $idx; ?>"      value="<? echo $dr_id; ?>">
    <input type="hidden" id="tbl_nm_<? echo $idx; ?>"     value="<? echo $tbl_nm; ?>">
    </td>
</tr>
<?
        $idx++ ;
    }
?>
</table>
<?
}

/**
 * 主目標一覧を作成する
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param string   $fname ページ名
 * 
 * @return void
 */
function show_maingoal_list($con, $pt_id, $fname)
{
    //==============================
    //DBより一覧データを取得
    //==============================
    $maingoal_list = getSumMainGoalList($con, $pt_id, $fname);
    if ($maingoal_list === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }
    
    //==============================
    //一覧表 出力
    //==============================
?>
    <table class="listp_table" cellspacing="1">
      <tr>
        <th style="text-align:center; padding:2px; width:85%;"><nobr>内容</nobr></th>
        <th style="text-align:center; padding:2px; width:15%;"><nobr>登録年月日</nobr></th>
      </tr>
<? foreach ($maingoal_list as $value) { ?>
      <tr>
        <td>
          <nobr><a href="javascript:showMainGoalUpdateWindow('<? echo $value['goal_id']; ?>');"><? echo h($value['main_goal']); ?></a></nobr>
        </td>
        <td style="text-align:center"><? echo date('Y/m/d', strtotime($value['reg_date'])); ?></td>
      </tr>
<? } ?>
    </table>

<div class="search_button_div">
<input type="button" onclick="showMainGoalInsertWindow();" value="追加"/>
</div>
<?
}

function format_date($dt) {
  $pattern = '/^(\d{4})(\d{2})(\d{2})$/';
  $replacement = '$1/$2/$3';
  return preg_replace($pattern, $replacement, $dt);
}

function format_time($tm) {
  $pattern = '/^(\d{2})(\d{2})$/';
  $replacement = '$1:$2';
  return preg_replace($pattern, $replacement, $tm);
}

/**
 * 入院履歴情報を取得
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param string   $fname ページ名
 * 
 * @return mixed 成功時:入院履歴情報/失敗時:false
 */
function getInpthistData($con, $pt_id, $fname)
{
    // 入院履歴情報を取得
//    $sql = "select i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.inpt_disease, i.inpt_patho_from, (select s.sect_nm from sectmst s where s.enti_id = i.inpt_enti_id and s.sect_id = i.inpt_sect_id and s.sect_del_flg = 'f') as sect_nm, (select d.dr_nm from drmst d where d.enti_id = i.inpt_enti_id and d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id and d.dr_del_flg = 'f') as dr_nm, (select e.enti_nm from entimst e where e.enti_id = i.inpt_enti_id and e.enti_del_flg = 'f') as enti_nm from inpthist i where i.ptif_id = '$pt_id' union select i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.inpt_disease, i.inpt_patho_from, (select s.sect_nm from sectmst s where s.enti_id = i.inpt_enti_id and s.sect_id = i.inpt_sect_id and s.sect_del_flg = 'f') as sect_nm, (select d.dr_nm from drmst d where d.enti_id = i.inpt_enti_id and d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id and d.dr_del_flg = 'f') as dr_nm, (select e.enti_nm from entimst e where e.enti_id = i.inpt_enti_id and e.enti_del_flg = 'f') as enti_nm from inptmst i where i.ptif_id = '$pt_id' and i.inpt_res_flg = 'f'";
    $sql =
    " select" .
    "   ih.inpt_in_dt" .
    "  ,ih.inpt_in_tm" .
    "  ,ih.inpt_out_dt" .
    "  ,ih.inpt_out_tm" .
    "  ,ih.inpt_disease" .
    "  ,ih.inpt_patho_from" .
    "  ,ih.inpt_patho_to" .
    "  ,sm.sect_id" .
    "  ,sm.sect_nm" .
    "  ,dm.dr_id" .
    "  ,dm.dr_nm" .
    "  ,'inpthist' as tbl_nm" .
    " from" .
    "   inpthist ih" .
    "   left join sectmst sm on (sm.enti_id = ih.inpt_enti_id and sm.sect_id = ih.inpt_sect_id and sm.sect_del_flg = 'f')" .
    "   left join drmst   dm on (dm.enti_id = ih.inpt_enti_id and dm.sect_id = ih.inpt_sect_id and dm.dr_id = ih.dr_id and dm.dr_del_flg = 'f')" .
    " where" .
    "   ih.ptif_id = '" . $pt_id . "'" .
    " union" .
    " select" .
    "   im.inpt_in_dt" .
    "  ,im.inpt_in_tm" .
    "  ,im.inpt_out_dt" .
    "  ,im.inpt_out_tm" .
    "  ,im.inpt_disease" .
    "  ,im.inpt_patho_from" .
    "  ,im.inpt_patho_to" .
    "  ,sm.sect_id" .
    "  ,sm.sect_nm" .
    "  ,dm.dr_id" .
    "  ,dm.dr_nm" .
    "  ,'inptmst' as tbl_nm" .
    " from" .
    "   inptmst im" .
    "   left join sectmst sm on (sm.enti_id = im.inpt_enti_id and sm.sect_id = im.inpt_sect_id and sm.sect_del_flg = 'f')" .
    "   left join drmst   dm on (dm.enti_id = im.inpt_enti_id and dm.sect_id = im.inpt_sect_id and dm.dr_id = im.dr_id and dm.dr_del_flg = 'f')" .
    " where" .
    "      im.ptif_id      = '" . $pt_id . "'" .
    "  and im.inpt_res_flg = 'f'" ;
    $cond = "order by inpt_in_dt desc, inpt_in_tm desc";
    $sel_inpthist = select_from_table($con, $sql, $cond, $fname);
    if ($sel_inpthist == 0) {
        return false;
    }
    $inpthist = pg_fetch_all($sel_inpthist);
    if ($inpthist === false) {
        $inpthist = array();
    }
    
    return $inpthist;
}

/**
 * プロブレム一覧データを取得
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param string   $fname ページ名
 * @param string   $mode  表示モード
 *                        "active":アクティブ/インアクティブを表示
 *                        "zizoku":持続性を表示
 *                        "itizi":一時的を表示
 *                        "all":全て表示
 * 
 * @return mixed 成功時:プロブレム一覧データ/失敗時:false
 */
function getSummaryProblemList($con, $pt_id, $fname, $mode)
{
    $sql = "select summary_problem_list.*,empmst.emp_lt_nm,empmst.emp_ft_nm,empmst.emp_job,jobmst.job_nm from summary_problem_list ";
    $sql .= "left join empmst on empmst.emp_id = summary_problem_list.emp_id ";
    $sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";
    $cond = "where summary_problem_list.ptif_id = '$pt_id'";
    if($mode == "active") {
        $cond .= " and (summary_problem_list.problem_class = 'アクティブ' or summary_problem_list.problem_class = 'インアクティブ')";
    }
    elseif($mode == "zizoku") {
        $cond .= " and summary_problem_list.problem_class = '持続性'";
    }
    elseif($mode == "itizi") {
        $cond .= " and summary_problem_list.problem_class = '一時的'";
    }
    $cond .= " order by summary_problem_list.problem_list_no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    $problem_list = pg_fetch_all($sel);
    if ($problem_list === false) {
        $problem_list = array();
    }
    return $problem_list;
}

/**
 * 主治医を取得する
 *
 * @param resource $con        データベース接続リソース
 * @param array    $nyuin_info 入院情報
 * @param string   $fname      ページ名
 *
 * @return mixed 成功時:主治医名/失敗時:false
 */
function getAttendingDoctorName($con, $nyuin_info, $fname)
{
    $sql = "SELECT dr_emp_id, dr_nm from drmst " .
            "WHERE enti_id = {$nyuin_info["inpt_enti_id"]} " .
            "AND sect_id = {$nyuin_info["inpt_sect_id"]} " .
            "AND dr_id = {$nyuin_info["dr_id"]}";
    $sel = select_from_table($con, $sql, '', $fname) ;
    if ($sel == 0) {
        return false;
    }
    $attdr_name = pg_fetch_result($sel, 0, 'dr_nm');
    if ($attdr_name === false) {
        $attdr_name = '';
    }
    return $attdr_name;
}

/**
 * 担当者を取得
 *
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param string   $fname ページ名
 *
 * @return mixed 成功時:担当者リスト/失敗時:false
 */
function getPersonInCharge($con, $pt_id, $fname)
{
    $sql = "SELECT iop.emp_id, emp.emp_lt_nm, emp.emp_ft_nm, emp.emp_job, job.job_nm FROM inptop iop " .
            "LEFT OUTER JOIN empmst emp ON (emp.emp_id = iop.emp_id) " .
            "LEFT OUTER JOIN jobmst job ON (job.job_id = emp.emp_job) " .
            "WHERE ptif_id = '{$pt_id}' ORDER BY iop.order_no";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        return false;
    }
    $person_in_charge = pg_fetch_all($sel);
    if ($person_in_charge === false) {
        $person_in_charge = array();
    }
    
    return $person_in_charge;
}

/**
 * 主目標一覧データを取得
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param string   $fname ページ名
 * 
 * @return mixed 成功時:プロブレム一覧データ/失敗時:false
 */
function getSumMainGoalList($con, $pt_id, $fname)
{
    $sql = "SELECT * FROM sum_main_goal";
    $cond = "WHERE ptif_id = '{$pt_id}' ORDER BY reg_date";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    $maingoal_list = pg_fetch_all($sel);
    if ($maingoal_list === false) {
        $maingoal_list = array();
    }
    return $maingoal_list;
}

/**
 * PDF作成
 *
 * @param array  $nyuin_info       入院情報
 * @param string $attdr_name       主治医
 * @param array  $person_in_charge 担当者
 * @param array  $inpthist         入院履歴情報
 * @param array  $problem_list     プロブレムリストデータ
 *
 * @return void
 */
function createPDF($nyuin_info, $attdr_name, $person_in_charge, $inpthist, $problem_list, $maingoal_list)
{
    $pdf = new CustomMBFPDF($nyuin_info['ptif_id']);
    $pdf->AddMBFont(GOTHIC, 'SJIS');
    $pdf->AddMBFont(PMINCHO, 'SJIS');
    $pdf->Open();
    $pdf->AddPage();
    
    $total_w = 190.0;    // プロブレムリスト全体の幅
    
    //==============================
    // タイトル
    //==============================
    $pdf->SetFont(GOTHIC, '', 22);
    $pdf->Cell(0, 12, 'プロブレムリスト', 0, 1, 'C');

    //==============================
    // 患者情報
    //==============================
    $pdf->SetFontSize(9.5);
    $pdf->Cell(60, 5, sprintf('患者ID:%s', $nyuin_info['ptif_id']), 0, 0);
    $pdf->Cell(100, 5, sprintf('病棟:%s 病室:%s', $nyuin_info['ward_name'], $nyuin_info['ptrm_name']), 0, 1);
    $pdf->SetFontSize(10);
    $pdf->Cell(50, 10, sprintf('患者名:%s %s', $nyuin_info['inpt_lt_kj_nm'], $nyuin_info['inpt_ft_kj_nm']), 1, 0);

    //==============================
    // 担当者(最大9名迄表示可能。主治医を含め、10名迄表示する。)
    //==============================
    $xpos = $pdf->GetX();
    $pdf->SetFontSize(6.5);
    $pdf->Cell(28, 5, sprintf('主治医:%s', $attdr_name), 1, 0);
    $count = count($person_in_charge);
    $person_in_charge_max = 9;

    if ($count < $person_in_charge_max) {    // 担当者が9名に満たない場合は、最大表示人数迄、空のデータを追加する
        for ($i = $count; $i <= $person_in_charge_max; $i++) {
            $person_in_charge[] = array('emp_id' => '', 'job_nm' => '', 'emp_lt_nm' => '', 'emp_ft_nm' => '');
        }
    }

    for ($i = 0; $i < $person_in_charge_max; $i++) {
        if ($i == $person_in_charge_max) {
            break;
        }
        if ($i == 4) {
            $pdf->Ln();
            $pdf->SetX($xpos);
        }
        if ($person_in_charge[$i]['emp_id']) {
            $pdf->Cell(28, 5, sprintf('%s:%s %s', $person_in_charge[$i]['job_nm'], $person_in_charge[$i]['emp_lt_nm'], $person_in_charge[$i]['emp_ft_nm']), 1, 0);
        } else {
            $pdf->Cell(28, 5, '', 1, 0);
        }
    }
    $pdf->Ln();

    //==============================
    // 直近の病名(入院時診断名)
    //==============================
    if ($inpthist) {
        $disease_name = $inpthist[0]['inpt_disease'];
    } else {
        $disease_name = '';
    }
    $pdf->SetFontSize(9);
    $ypos = $pdf->GetY();
    $pdf->SetY($ypos + 2.0);
    $pdf->MultiCell(24, 3.5, "入院時診断名\n(主病名)");
    $pdf->SetY($ypos);
    $pdf->Cell(24.0, 10.0, '', 1, 0);
    $pdf->SetFontSize(10);
    $pdf->Cell($total_w - 24.0, 10, $disease_name, 1, 1);

    //==============================
    // 主目標
    //==============================
    $maingoal_caption_w = 24.0;
    $maingoal_text_w = $total_w - $maingoal_caption_w;
    $maingoal_h = 5.0;
    $ypos_maingoal = $pdf->GetY();
    if ($maingoal_list) {    // データが存在する場合
        $pdf->Cell($maingoal_caption_w, $maingoal_h, '主目標', 0, 0);
        $pdf->Cell($maingoal_text_w, $maingoal_h, '', 0, 1);
    } else {                 // データが存在しない場合
        $pdf->Cell($maingoal_caption_w, $maingoal_h * 2, '主目標', 0, 0);
        $pdf->Cell($maingoal_text_w, $maingoal_h * 2, '', 0, 1);
        $ypos = $pdf->GetY();
    }
    $pdf->SetFontSize(9);
    foreach ($maingoal_list as $value) {
        $pdf->Cell($maingoal_caption_w, $maingoal_h, date('Y年n月j日', strtotime($value['reg_date'])), 0, 0);
        $pdf->Cell($maingoal_text_w, $maingoal_h, $value['main_goal'], 0, 1);
        $ypos = $pdf->GetY();
    }

    // 枠線を表示する
    $pdf->SetY($ypos_maingoal);
    $height = $ypos - $ypos_maingoal;
    $pdf->Cell($maingoal_caption_w, $height, '', 1, 0);
    $pdf->Cell($maingoal_text_w, $height, '', 1, 0);

    $pdf->SetY($ypos);
    $pdf->Ln(5);

    //==============================
    // プロブレム一覧
    //==============================
    $pdf->SetFontSize(10);
    $pdf->Cell(16, 8, '番号', 1, 0, 'C');
    $pdf->Cell(67, 8, '問題', 1, 0, 'C');
    $pdf->Cell(20, 8, '発生日', 1, 0, 'C');
    $pdf->Cell(20, 8, '解決日', 1, 0, 'C');
    $pdf->Cell(67, 8, 'コメント', 1, 1, 'C');
    $pdf->SetFontSize(9);
    foreach ($problem_list as $value) {
        $pdf->Cell(16, 8, sprintf('#%s', $value['problem_list_no']), 1, 0, 'R');
        $pdf->Cell(67, 8, $value['problem'], 1, 0);
        $pdf->Cell(20, 8, stringToDateFormat($value["start_date"]), 1, 0, 'C');
        $pdf->Cell(20, 8, stringToDateFormat($value["end_date"]), 1, 0, 'C');
        $xpos = $pdf->GetX();
        $ypos = $pdf->GetY();
        $pdf->Cell(67, 8, '', 1, 1);    // 枠線
        $pdf->SetXY($xpos, $ypos);
        $pdf->MultiCell(67, 4, $value['memo']);
        $pdf->SetY($ypos + 8.0);
    }

    $pdf->Output();
}

/**
 * 文字列をyyyy/mm/ddの形式に変換する
 * 
 * @param string $str 変換対象の文字列
 * 
 * @return string 変換後の文字列
 */
function stringToDateFormat($str)
{
    $str = trim($str);
    $formatted = trim(substr($str, 0, 4) . "/" . str_replace('--', '00/00', substr($str, 4, 2)) . "/" . str_replace('-', '00', substr($str, 6, 2)), "/");
    
    return $formatted;
}

//==========================================================================================
//実処理開始
//==========================================================================================
?>
<?
ob_start();
require_once("show_kanja_joho.ini");
require_once("label_by_profile_type.ini");
require_once("summary_common.ini");
require_once("CustomMBFPDF.php");
ob_end_clean();

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
// DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// ログ
//==============================
summary_common_write_operate_log("access", $con, $fname, $session, "", "", $pt_id);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

//==========================================================================================
//PDF出力
//==========================================================================================
if ($type == 'pdf') {
    // 患者情報を取得
    $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, date('Ymd'));
    if ($nyuin_info === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }

    // 主治医を取得
    $attdr_name = getAttendingDoctorName($con, $nyuin_info, $fname);
    if ($attdr_name === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }

    // 担当者を取得
    $person_in_charge = getPersonInCharge($con, $pt_id, $fname);
    if ($person_in_charge === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }

    // 入院履歴情報を取得
    $inpthist = getInpthistData($con, $pt_id, $fname);
    if ($inpthist === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }

    // プロブレム一覧データを取得(全データ)
    $problem_list_all = getSummaryProblemList($con, $pt_id, $fname, 'all');
    if ($problem_list_all === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }
    
    // 主目標一覧データを取得
    $maingoal_list = getSumMainGoalList($con, $pt_id, $fname);
    if ($maingoal_list === false) {
        pg_close($con);
        summary_common_show_error_page_and_die();
    }
    pg_close($con);    // DB切断
    
    createPDF($nyuin_info, $attdr_name, $person_in_charge, $inpthist, $problem_list_all, $maingoal_list);
    exit;
}

//==========================================================================================
//HTML出力
//==========================================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜プロブレムリスト</title>
<script type="text/javascript">

var problem_input_window_open_option  = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=700,height=500";
var update_disease_window_open_option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=700,height=200";

//プロブレム追加画面を表示します。
function showInsertWindow(mode) {
	var insert_default_class = encodeURIComponent("アクティブ");
	if(mode == "zizoku") insert_default_class = "持続性";
	if(mode == "itizi") insert_default_class = "一時的";
	var url = "summary_problem_input.php?session=<? echo $session; ?>&pt_id=<? echo $pt_id; ?>&mode=insert&default_class=" + insert_default_class;
	window.open(url, 'problem_input_window',problem_input_window_open_option);
}

//プロブレム更新画面を表示します。
function showUpdateWindow(problem_id) {
	var url = "summary_problem_input.php?session=<? echo $session; ?>&pt_id=<? echo $pt_id; ?>&mode=update&problem_id=" + problem_id;
	window.open(url, 'problem_input_window',problem_input_window_open_option);
}

var problem_rireki_list_window_open_option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=800,height=500";

//プロブレム更新履歴画面を表示します。
function showProblemRirekiWindow(problem_id) {
	var url = "summary_problem_rireki_list.php?session=<? echo $session; ?>&pt_id=<? echo $pt_id; ?>&problem_id=" + problem_id;
	window.open(url, 'problem_rireki_list_window',problem_rireki_list_window_open_option);
}

function showUpdateDiseaseWindow(idx)
{
  var disease    = document.getElementById("disease_"    + idx).value ;
  var in_dt      = document.getElementById("in_dt_"      + idx).value ;
  var in_tm      = document.getElementById("in_tm_"      + idx).value ;
  var patho_from = document.getElementById("patho_from_" + idx).value ;
  var patho_to   = document.getElementById("patho_to_"   + idx).value ;
  var sect_id    = document.getElementById("sect_id_"    + idx).value ;
  var dr_id      = document.getElementById("dr_id_"      + idx).value ;
  var tbl_nm     = document.getElementById("tbl_nm_"     + idx).value ;
  var url = "summary_disease_input.php?session=<? echo $session; ?>&pt_id=<? echo $pt_id; ?>&disease=" + disease + "&in_dt=" + in_dt + "&in_tm=" + in_tm + "&patho_from=" + patho_from + "&patho_to=" + patho_to + "&id_sect=" + sect_id + "&dr_id=" + dr_id + "&tbl_nm=" + tbl_nm ;
  window.open(url, 'disease_input_window',update_disease_window_open_option);
}

// 主目標追加画面を表示
function showMainGoalInsertWindow()
{
  var url = "summary_maingoal_input.php?session=<? echo $session; ?>&mode=insert&pt_id=<? echo $pt_id; ?>";
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=550,height=270";
  window.open(url, 'maingoal_input_window', option);
}

//主目標更新画面を表示
function showMainGoalUpdateWindow(goal_id)
{
  var url = "summary_maingoal_input.php?session=<? echo $session; ?>&mode=update&pt_id=<? echo $pt_id; ?>&goal_id=" + goal_id;
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=550,height=270";
  window.open(url, 'maingoal_input_window', option);
}

// PDFを出力
function printPDF()
{
  var option = "directories=no,location=no,menubar=1,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=1,left=0,top=0,width=730,height=800";
  window.open('summary_problem_list.php?session=<? echo $session; ?>&pt_id=<? echo $pt_id; ?>&type=pdf', 'stdPlanLibraryWin', option);
}
</script>
</head>
<body>

<? $pager_param_addition = "&page=".@$page."&key1=".@$key1."&key2=".@$key2; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "プロブレムリスト", "", $pt_id); ?>

<? summary_common_show_report_tab_menu($con, $fname, "プロブレムリスト", 0, $pt_id, $pager_param_addition); ?>

<div class="search_button_div"><input type="button" value="印刷" onclick="printPDF();"></div>
<? //患者情報ヘッダ ?>
<? show_kanja_joho($con,$pt_id,$fname); ?>

<? //入院時診断名(主病名) ?>
<div style="margin-top:12px">
<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin-top:16px; margin-bottom:2px; letter-spacing:1px; width:150px">入院時診断名(主病名)</div>
<? show_inpt_disease($con,$pt_id,$fname); ?>

<? //主目標 ?>
<div style="margin-top:24px">
<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin:2px 0px; letter-spacing:1px; width:150px">主目標</div>
<? show_maingoal_list($con, $pt_id, $fname); ?>

<? //アクティブ/インアクティブ一覧 出力 ?>
<div style="margin-top:24px">
<? show_problem_list($con,$pt_id,$fname,"active"); ?>
</div>

<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin:2px 0px; letter-spacing:1px; width:150px">持続性プロブレム</div>
<? show_problem_list($con,$pt_id,$fname,"zizoku"); ?>

<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin:2px 0px; letter-spacing:1px; width:150px">一時的プロブレム</div>
<? show_problem_list($con,$pt_id,$fname,"itizi"); ?>

</body>
</html>
<? pg_close($con); ?>
