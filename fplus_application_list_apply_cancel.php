<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="fplus_apply_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<? echo($category); ?>">
<input type="hidden" name="workflow" value="<? echo($workflow); ?>">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="approve_emp_nm" value="<? echo($approve_emp_nm); ?>">
<input type="hidden" name="date_y1" value="<? echo($date_y1); ?>">
<input type="hidden" name="date_m1" value="<? echo($date_m1); ?>">
<input type="hidden" name="date_d1" value="<? echo($date_d1); ?>">
<input type="hidden" name="date_y2" value="<? echo($date_y2); ?>">
<input type="hidden" name="date_m2" value="<? echo($date_m2); ?>">
<input type="hidden" name="date_d2" value="<? echo($date_d2); ?>">
<input type="hidden" name="apply_stat" value="<? echo($apply_stat); ?>">
</form>
<?
require_once("about_comedix.php");
require_once("fplus_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

$obj = new fplus_common_class($con, $fname);

for ($i = 0, $j = count($apply_cancel_chk); $i < $j; $i++) {
	if ($apply_cancel_chk[$i] != "") {
		$apply_id = $apply_cancel_chk[$i];

		// 承認状況チェック
		$apv_cnt = $obj->get_applyapv_cnt($apply_id);
		if ($apv_cnt > 0) {
			$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
			pg_query($con, "rollback");
			pg_close($con);
			$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
			echo("<script type=\"text/javascript\">alert('他の受付状況が変更されたため、報告取消ができません。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}

		// 申請論理削除
		$obj->update_delflg_all_apply($apply_id, "t");
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
