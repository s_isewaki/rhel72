<?php
require_once("about_session.php");
require_once("about_authority.php");

require_once './common_log_class.ini';
require_once './kintai/common/mdb_wrapper.php'; // MDB本体をWrap
require_once './kintai/common/mdb.php'; 		// DBアクセスユーティリティ
require_once './kintai/common/user_info.php';	// ユーザ情報
require_once './kintai/common/master_util.php';	// マスタ
require_once './kintai/common/exception.php';
require_once './kintai/common/date_util.php';
require_once './kintai/common/code_util.php';

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//フォームデータ取得
$emp_id 	= $_REQUEST["emp_id"];
$yyyymm		= $_REQUEST["yyyymm"];

//年月
$yyyy = substr($yyyymm, 0, 4);
$mm   = intval(substr($yyyymm, 4, 2));


//データアクセス
try {

	MDBWrapper::init(); // おまじない
	$log = new common_log_class(basename(__FILE__));
	$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

	$db->beginTransaction(); // トランザクション開始
	
	//部署階層数
	$class_cnt = MasterUtil::get_organization_level_count();
	
	//職員ID・氏名を取得
	$user_info  = UserInfo::get($emp_id, "u");
	$base_info  = $user_info->get_base_info();
	$job_info   = $user_info->get_job_info();
	$class_info  = $user_info->get_class_info();
	$attr_info   = $user_info->get_attr_info();
	$dept_info   = $user_info->get_dept_info();
	$croom_info  = $user_info->get_class_room_info();
	$cond_info   = $user_info->get_emp_condition();

	//所属
	$shozoku = $class_info->class_nm." ".$attr_info->atrb_nm." ".$dept_info->dept_nm;
	if($class_cnt == 4){
		if($croom_info->room_nm != ""){
			$shozoku .= " ".$croom_info->room_nm;
		}
	}

	//締め日情報を取得
	$closing_info  = MasterUtil::get_closing();
	if(is_null($closing_info)){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if($cond_info->duty_form == "2"){
		if($closing_info["closing_parttime"] != ""){
			$closing = $closing_info["closing_parttime"];
		}else{
			$closing = $closing_info["closing"];
		}
	}else{
		$closing = $closing_info["closing"];
	}
	
	//期間取得（月度考慮）
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);
	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);
	$prdArr = DateUtil::get_cutoff_period($cond_info->duty_form, $year, $month);
	
	// 勤務実績を取得
	$sql  = "select ";
	$sql .= " atdbkrslt.emp_id, ";
	$sql .= " atdbkrslt.date, ";
	$sql .= " atdbkrslt.pattern, ";
	$sql .= " atdbkrslt.reason, ";
	$sql .= " atdbkrslt.tmcd_group_id, ";
	$sql .= " atdptn.atdptn_nm, ";
	$sql .= " case when atdbk_reason_mst.display_name = '' then atdbk_reason_mst.default_name else display_name end as reason_name ";
	$sql .= "FROM ";
	$sql .= " atdbkrslt ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdptn ";
	$sql .= "ON ";
	$sql .= " CAST(atdptn.atdptn_id AS varchar) = atdbkrslt.pattern AND ";
	$sql .= " atdptn.group_id  = atdbkrslt.tmcd_group_id ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdbk_reason_mst ";
	$sql .= "ON ";
	$sql .= " atdbk_reason_mst.reason_id = atdbkrslt.reason ";
	$sql .= "WHERE ";
	$sql .= " atdbkrslt.emp_id = '".$base_info->emp_id."' AND ";
	$sql .= " (atdbkrslt.date >= '".$prdArr["start"]."' AND atdbkrslt.date <= '".$prdArr["end"]."') ";
	$atdbk_rslt = $db->find($sql);
	
	// リストの編集
	$display_list = array();
	
	// 区分マスタを取得
	$ovtmkbn_mst = MasterUtil::get_ovtm_div_all();
	$total_time = 0;
	// 集計用連想配列初期化
	$ovtmkbn_ttl_list = array();
	$ovtmkbn_mst_list = array();
	
	foreach ($ovtmkbn_mst as $item) {
		$ovtmkbn_ttl_list[$item["ovtmkbn_kind"]] = 0; // 区分毎集計初期化
		$ovtmkbn_mst_list[$item["ovtmkbn_id"]] = $item["ovtmkbn_kind"]; // コード変換用
	}

	foreach ($atdbk_rslt as $rslt) {
		$display_line = array();
		$date_map = DateUtil::split_date($rslt["date"]);
		
		// 曜日
		$display_line["bgcolor"] = getBgColor($rslt["date"]);
		$display_line["date"] = intval($date_map["m"])."月".intval($date_map["d"])."日";
		$display_line["day_of_week"] = CodeUtil::get_day_of_week($date_map["w"]);
		$display_line["atdptn_name"] = $rslt["atdptn_nm"];
		$display_line["reason_name"] = $rslt["reason_name"];
		
		// 区分カラムの初期化
		$line_kbn_columns = array(); // 表示用
		$line_kbn_total = array(); // 行の集計用
		foreach ($ovtmkbn_mst as $item) {
			$line_kbn_columns[$item["ovtmkbn_kind"]] = "";
			$line_kbn_total[$item["ovtmkbn_kind"]] = "0";
		}
		
		// ここから集計処理
		$ovtm_get_sql = "select ";
		$ovtm_get_sql.= "kintai_ovtm_seq.minute, kintai_ovtm_seq.ovtmkbn_id from kintai_ovtm_seq ";
		$ovtm_get_sql.= "join ";
		$ovtm_get_sql.= "kintai_ovtm on kintai_ovtm_seq.emp_id = kintai_ovtm.emp_id and kintai_ovtm_seq.date = kintai_ovtm.date ";
		$ovtm_get_sql.= "where ";
		$ovtm_get_sql.= "kintai_ovtm_seq.emp_id = '".$rslt["emp_id"]."' and kintai_ovtm_seq.date = '".$rslt["date"]."' and kintai_ovtm.apply_stat = '1' ";
		$ovtm_get_sql.= "order by kintai_ovtm_seq.no";
		$ovtm_times = $db->find($ovtm_get_sql);
		
		foreach ($ovtm_times as $time) {
			$minute = $time["minute"];
			$kbn_kind = $ovtmkbn_mst_list[$time["ovtmkbn_id"]]; // ovtnkbn_kindを取得
			
			if ($total_time + $minute > 3600) { // 60h over
				$under = 0;
				$over  = 0;
				if ($total_time < 3600) {
					$under = 3600 - $total_time;
					$over  = $minute - $under;
				} else {
					$over = $minute;
				}
				
				switch($kbn_kind) {
					case "kbn_125": // 125 → 150
						$line_kbn_total["kbn_125"] += $under;
						$line_kbn_total["kbn_150"] += $over;
						break;
					case "kbn_135": // 135 → 150
						$line_kbn_total["kbn_135"] += $under;
						$line_kbn_total["kbn_150"] += $over;
						break;
					case "kbn_150": // 150 → 175
						$line_kbn_total["kbn_150"] += $under;
						$line_kbn_total["kbn_175_none"] += $over;
						break;
					case "kbn_160": // 160 → 175
						$line_kbn_total["kbn_160"] += $under;
						$line_kbn_total["kbn_175_none"] += $over;
						break;
                    case "kbn_135_hol": // 135休  → 150
						$line_kbn_total["kbn_135_hol"] += $under;
						$line_kbn_total["kbn_150"] += $over;
						break;
                    default:
						// そのまま加算
						$line_kbn_total[$kbn_kind] += $minute;
						break;
				}
			} else {
				// そのまま加算
				$line_kbn_total[$kbn_kind] += $minute;
			}
            $total_time += $minute;
        }
		
		$line_total = 0;
		// 行の計算が終わったら区分毎合計に加算、表示用に編集
		foreach($line_kbn_total as $kind => $value) {
			// 区分合計に加算
			$ovtmkbn_ttl_list[$kind] += $value;
			$line_total += $value;
			// 表示用
			if ($value > 0) {
				$line_kbn_columns[$kind] = intval($value / 60).":".sprintf('%02d', $value % 60);
			}
		}
		
		$display_line["kbn_list"] = $line_kbn_columns;
		if ($line_total > 0) {
			$display_line["line_total"] = intval($line_total / 60).":".sprintf('%02d', $line_total % 60);
		}
		array_push($display_list, $display_line);
	}
	
	// 合計行の編集
	$display_ttl_list = array("label" => "合計(月次)");
	foreach ($ovtmkbn_ttl_list as $key => $value) {
		$display_ttl_list[$key] = $value > 0 ? intval($value / 60).":".sprintf('%02d', $value % 60):"";
	}
	$display_ttl_list["total"] = $total_time > 0 ? intval($total_time / 60).":".sprintf('%02d', $total_time % 60):"";

} catch (BusinessException $bex) {
	$message = $bex->getMessage();
	echo("<script type='text/javascript'>alert('".$message."');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} catch (FatalException $fex) { 
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

function getBgColor($date) {
	if(is_cal_holiday($date) == true){
		return "FADEDE";
	}else{
		return "FFFFFF";
	}

}
function is_cal_holiday($date) {
	$sql = "select type from calendar where date = :date";
	$value = get_mdb()->one(
		$sql, array("text"), array("date" => $date));

	if (in_array($value, array("6", "7"))) {
		return true;
	}
	return false;
}
function get_mdb() {
	$log = new common_log_class(basename(__FILE__));
	return new MDB($log);
}

//====================================
//	エクセル出力クラス
//====================================
require_once("./kintai/common/kintai_print_excel_class.php");
// Excelオブジェクト生成
$excelObj = new ExcelWorkShop();
// Excel列名。列名をloopで連続するときに使うと便利です。
$excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
						 'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
						 'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
						 'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
						 'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ');
// 用紙の向き。横方向の例
$excelObj->PageSetUp("Horizon");
// 用紙サイズ
$excelObj->PaperSize( "A4" );
// 倍率と自動或いは利用者指定。利用者が指定して95%で印刷する場合
//$excelObj->PageRatio( "USER" , "70" );
$excelObj->PageRatio( "AUTO" , "" );
// 余白指定、左,右,上,下,ヘッダ,フッタ
$excelObj->SetMargin( "0.5" , "0.5", "1.5" , "1.5" , "0.5" , "0.5" );
// ヘッダー情報
$HeaderMessage = '&"ＭＳ ゴシック"&11&B超勤区分内訳';
$HeaderMessage = $HeaderMessage . "&R".date("Y.n.j");
$excelObj->SetPrintHeader( $HeaderMessage );
//フッター
$strFooter  = '&C&P / &N';
$excelObj->SetPrintFooter($strFooter);
// シート名
$excelObj->SetSheetName(mb2("超勤区分内訳"));
// 列幅
for($i=0;$i<count($excelColumnName);$i++){
	$excelObj->SetColDim($excelColumnName[$i], 2.2);
}
//デフォルトフォント
$excelObj->setDefaultFont("ＭＳ ゴシック", 9, false);

//基本■■■■■■■■■■■■■■■■■■
//開始位置
$ROW_ST = 3;
//行のタイトル
$excelObj->setRowTitle(1, $ROW_ST);
//ウィンド枠固定
$excelObj->setPane(0,4);

//期間
$excelObj->CellMerge2("A1:E1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A1", "対象期間：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("F1:M1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("F1", $yyyy."年".$mm."月度", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("N1:AD1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("N1", "開始日 ".date("n月j日", strtotime($prdArr["start"]))." 〜 締め日 ".date("n月j日", strtotime($prdArr["end"]))."", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("AE1:AG1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("AE1", "所属：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->SetArea("AH1"); 
$excelObj->SetFontBold();
$excelObj->SetValueJP2("AH1", $shozoku, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);


//職員
$excelObj->CellMerge2("A2:E2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A2", "職員ID：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("F2:M2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("F2", $base_info->emp_personal_id, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("N2:P2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("N2", "氏名：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->SetArea("Q2"); 
$excelObj->SetFontBold();
$excelObj->SetValueJP2("Q2", $base_info->emp_lt_nm." ".$base_info->emp_ft_nm."（".$job_info->job_nm." ".CodeUtil::get_duty_form($cond_info->duty_form)." ".CodeUtil::get_salary_type($cond_info->wage)."）", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

//現在行位置
$y = $ROW_ST;

// フォーマット定義
// header + data
$arrName = array("日付", "曜日", "勤務実績", "事由");
$arrLen  = array(4, 2, 5, 5);
$arrHPos = array("HCENTER", "HCENTER", "HCENTER", "HCENTER");
$arrFld  = array("date", "day_of_week", "atdptn_name", "reason_name");
foreach($ovtmkbn_mst as $column) {
	array_push($arrName, $column["ovtmkbn_name"]);
	array_push($arrLen, 3);
	array_push($arrHPos, "HRIGHT");
	array_push($arrFld, $column["ovtmkbn_kind"]);
}
array_push($arrName, "合計(日次)");
array_push($arrLen, 4);
array_push($arrHPos, "HRIGHT");
array_push($arrFld, "line_total");

// ヘッダ
$col = 0;
for($i=0;$i<count($arrName);$i++){
	$col = getRange($arrLen[$i], $col, $y, $retArr);
	$excelObj->CellMerge2($retArr["range"], "", "", "");
	$excelObj->SetValueJP2($retArr["start"], $arrName[$i], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
}


//データ
for($i=0;$i<count($display_list);$i++){
	$y++;
	$col = 0;
	$display_line = array_merge($display_list[$i], $display_list[$i]["kbn_list"]);
	for($j=0;$j<count($arrName);$j++){
		$col = getRange($arrLen[$j], $col, $y, $retArr);
		$excelObj->CellMerge2($retArr["range"], "", "", $display_line[$i]["bgcolor"]);
		$excelObj->SetValueJP2($retArr["start"], $display_line[$arrFld[$j]], "VCENTER ".$arrHPos[$j], mb1("ＭＳ ゴシック"), 9);
	}
}

// total
$arrLenTotal = array(16);
$arrHPosTotal = array("HRIGHT");
$arrFldTotal  = array("label");
foreach($ovtmkbn_mst as $column) {
	array_push($arrLenTotal, 3);
	array_push($arrHPosTotal, "HRIGHT");
	array_push($arrFldTotal, $column["ovtmkbn_kind"]);
}
array_push($arrLenTotal,  4);
array_push($arrHPosTotal, "HRIGHT");
array_push($arrFldTotal,  "total");

$y++;
$col = 0;
for($i=0;$i<count($arrFldTotal);$i++){
	$col = getRange($arrLenTotal[$i], $col, $y, $retArr);
	$excelObj->CellMerge2($retArr["range"], "", "", "");
	$excelObj->SetValueJP2($retArr["start"], $display_ttl_list[$arrFldTotal[$i]], "VCENTER ".$arrHPosTotal[$i], mb1("ＭＳ ゴシック"), 9);
}
//罫線
$end_column = split(":", $retArr["range"]);
$excelObj->SetArea("A".$ROW_ST.":".$end_column[1]); 
$excelObj->SetBorder("THIN", "", "all");
$excelObj->SetArea("A".$y.":".$end_column[1]); 
$excelObj->setLineBottomTOP();
$excelObj->SetArea("A1"); 

// 出力ファイル名
$filename = "List_of_overtimekbn.xls";
// ダウンロード形式
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');
$excelObj->OutPut();

function mb1($str){
	return mb_convert_encoding($str, "UTF-8", "EUC-JP");
}
function mb2($str){
	return mb_convert_encoding(mb_convert_encoding($str, "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
}
function getRange($len, $now, $y, &$retArr){
	$colArr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
					'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
					'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
					'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
					'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ');
	$retArr = array();
	//開始位置
	$retArr["start"] = $colArr[$now].$y;
	//範囲
	$retArr["range"] = $colArr[$now].$y.":".$colArr[$now+$len-1].$y;

	return $now + $len;
}
?>
