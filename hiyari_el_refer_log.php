<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
    showLoginPage();
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//==============================
// デフォルト値の設定
//==============================
if ($sort == "") {$sort = "1";}  // 並び順：「累積参照数の多い順」
if ($page == "") {$page = 1;}  // ページ数：1


//====================================
//表示
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", "参照履歴");
$smarty->assign("session", $session);
$smarty->assign("fname", $fname);

//------------------------------------
//ソート
//------------------------------------
$smarty->assign("sort", $sort);

$sort_type = array();
$temp['title'] = '累積参照数の多い順';
$temp['is_selected'] = ($sort == '1');
$temp['sort_id'] = 1;
$sort_type[0][] = $temp;

$temp['title'] = '直近3ヶ月で参照数の多い順';
$temp['is_selected'] = ($sort == '2');
$temp['sort_id'] = 2;
$sort_type[0][] = $temp;

$temp['title'] = '直近1ヶ月で参照数の多い順';
$temp['is_selected'] = ($sort == '3');
$temp['sort_id'] = 3;
$sort_type[0][] = $temp;

$temp['title'] = '累積参照数の少ない順';
$temp['is_selected'] = ($sort == '4');
$temp['sort_id'] = 4;
$sort_type[1][] = $temp;

$temp['title'] = '直近3ヶ月で参照数の少ない順';
$temp['is_selected'] = ($sort == '5');
$temp['sort_id'] = 5;
$sort_type[1][] = $temp;

$temp['title'] = '直近1ヶ月で参照数の少ない順';
$temp['is_selected'] = ($sort == '6');
$temp['sort_id'] = 6;
$sort_type[1][] = $temp;

$smarty->assign("sort_type", $sort_type);

//------------------------------------
//一覧
//------------------------------------
$limit = 15;

// コンテンツ数を取得（ページング処理のため）
$sql_sel_doc = get_select_documents_sql($con, $session, $fname);
$sql = "select count(*) from ($sql_sel_doc) el_info";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0){
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lib_count = intval(pg_fetch_result($sel, 0, 0));
if ($lib_count > 0){
    // コンテンツ情報の取得
    $sql_common = "el_info.*, (select emp_lt_nm || ' ' || emp_ft_nm from empmst where empmst.emp_id = el_info.emp_id) as emp_nm from ($sql_sel_doc) el_info";

    if ($sort == "1" || $sort == "4"){
        // 期間指定なし
        $sql = "select $sql_common";
    }
    else{
        // 期間指定あり
        if ($sort == "2" || $sort == "5"){
            // 直近3ヶ月
            $comp_time = date("YmdHi", strtotime("-3 months"));
        }
        else{
            $comp_time = date("YmdHi", strtotime("-1 month"));
        }
        $sql = "select el_reflog.ref_count2, $sql_common left join (select lib_id, count(*) as ref_count2 from el_reflog where ref_date >= '$comp_time' group by lib_id) el_reflog on el_info.lib_id = el_reflog.lib_id";
    }
    switch ($sort){
        case "1":  // 累積参照数の多い順
            $cond = "order by el_info.ref_count desc, el_info.lib_up_date desc";
            break;
        case "2":  // 直近3ヶ月で参照数の多い順
            $cond = "order by coalesce(el_reflog.ref_count2, 0) desc, el_info.lib_up_date desc";
            break;
        case "3":  // 直近1ヶ月で参照数の多い順
            $cond = "order by coalesce(el_reflog.ref_count2, 0) desc, el_info.lib_up_date desc";
            break;
        case "4":  // 累積参照数の少ない順
            $cond = "order by el_info.ref_count, el_info.lib_up_date";
            break;
        case "5":  // 直近3ヶ月で参照数の少ない順
            $cond = "order by coalesce(el_reflog.ref_count2, 0), el_info.lib_up_date";
            break;
        case "6":  // 直近1ヶ月で参照数の少ない順
            $cond = "order by coalesce(el_reflog.ref_count2, 0), el_info.lib_up_date";
            break;
    }
    $offset = $limit * ($page - 1);
    $cond .= " offset $offset limit $limit";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // ページング
    if ($lib_count > $limit){
        if ($design_mode == 1){
            $smarty->assign("page_count", ceil($lib_count / $limit));
        }
        else{
            $page_max = ceil($lib_count / $limit);
            $smarty->assign("page_max", $page_max);
            $page_list = array();
            $page_stt = max(1, $page - 3);
            $page_end = min($page_stt + 6, $page_max);
            $page_stt = max(1, $page_end - 6);
            for ($i=$page_stt; $i<=$page_end; $i++){
                $page_list[] = $i;
            }
            $smarty->assign("page_list", $page_list);
        }
    }
    
    // コンテンツ一覧
    $list = array();
    while ($row = pg_fetch_array($sel)){
        $tmp_lib_id = $row["lib_id"];
        $tmp_lib_nm = $row["lib_nm"];
        if ($sort == "1" || $sort == "4"){
            $tmp_ref_count =  intval($row["ref_count"]);
        }
        else{
            $tmp_ref_count =  intval($row["ref_count2"]);
        }
        $tmp_lib_up_date = format_datetime($row["lib_up_date"]);
        $tmp_emp_nm = $row["emp_nm"];
        $tmp_lib_cate_nm = $row["lib_cate_nm"];
        $tmp_folder_id = $row["folder_id"];
        $tmp_lib_url = get_lib_url($row["lib_cate_id"], $tmp_lib_id, $row["lib_extension"], $session);

        $row = array();
        $row['lib_nm'] = $tmp_lib_nm;
        $row['ref_count'] = $tmp_ref_count;
        $row['lib_up_date'] = $tmp_lib_up_date;
        $row['emp_nm'] = $tmp_emp_nm;
        $row['lib_cate_nm'] = $tmp_lib_cate_nm;

        //パス情報
        $tmp_folder_path = lib_get_folder_path($con, $tmp_folder_id, $fname);
        if (count($tmp_folder_path) == 0){
            $row['path'] = '';
        }
        else{
            //フォルダ名配列
            $folder_names = array();
            foreach ($tmp_folder_path as $tmp_folder){
                $folder_names[] = $tmp_folder["name"];
            }

            //「>」区切りの文字列にする
            $row['path'] = " &gt; " . implode(" &gt; ", $folder_names);
        }

        $row['url'] = $tmp_lib_url;

        $list[] = $row;
    }
    $smarty->assign("list", $list);
}

$smarty->assign("lib_get_filename_flg", lib_get_filename_flg());
$smarty->assign("page", $page);

if ($design_mode == 1){
    $smarty->display("hiyari_el_refer_log1.tpl");
}
else{
    $smarty->display("hiyari_el_refer_log2.tpl");
}

pg_close($con);

//========================================================================================================================================================================================================
// 内部関数
//========================================================================================================================================================================================================
function format_datetime($datetime)
{
    if (strlen($datetime) == 14)
    {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3　$4:$5:$6", $datetime);
    }
    else
    {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $datetime);
    }
}

/**
 * 参照可能なコンテンツ一覧を取得するSQLを返す
 * カテゴリとフォルダ別に権限を確認して取得するSQLをunionでつなげる
 *
 * @param object $con DBコネクション
 * @param string $session セッションID
 * @param string $fname 画面名
 * @return string SLQ文
 */
function get_select_documents_sql($con, $session, $fname)
{

    //==============================
    // ログインユーザの職員情報を取得
    //==============================
    $sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
    $cond = "where emp_id = (select emp_id from session where session_id = '$session')";
    $sel_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_emp == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_id = pg_fetch_result($sel_emp, 0, "emp_id");
    $emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
    $emp_atrb = pg_fetch_result($sel_emp, 0, "emp_attribute");
    $emp_dept = pg_fetch_result($sel_emp, 0, "emp_dept");
    $emp_st = pg_fetch_result($sel_emp, 0, "emp_st");


    //==============================
    // カテゴリ直下のファイル取得用SQL
    //==============================
    $sql1   = " select el_info.*, el_cate.lib_cate_nm from el_info inner join (select * from el_cate where el_cate.libcate_del_flg = 'f') el_cate on el_info.lib_cate_id = el_cate.lib_cate_id";
    $cond1  = " where el_info.lib_delete_flag = 'f' ";
    $cond1 .= " and el_info.folder_id is null ";



    //==============================
    // フォルダ配下のファイル取得用SQL
    //==============================
    $sql2   = " select el_info.*, el_cate.lib_cate_nm from el_info inner join (select * from el_cate where el_cate.libcate_del_flg = 'f') el_cate on el_info.lib_cate_id = el_cate.lib_cate_id";
    $sql2  .= " left join el_folder on el_info.folder_id = el_folder.folder_id ";
    $cond2  = " where el_info.lib_delete_flag = 'f' ";
    $cond2 .= " and el_info.folder_id is not null ";



    //==============================
    // コンテンツの権限判定
    //==============================
    $cond_info  = " and";
    $cond_info .= " (";
        // 参照：所属
        $cond_info .= "(";
            $cond_info .= "(el_info.ref_dept_flg = '1' or (el_info.ref_dept_flg = '2' and exists (select * from el_ref_dept where el_ref_dept.lib_id = el_info.lib_id and el_ref_dept.class_id = $emp_class and el_ref_dept.atrb_id = $emp_atrb and el_ref_dept.dept_id = $emp_dept)))";
            $cond_info .= " and ";
            $cond_info .= "(el_info.ref_st_flg = '1' or (el_info.ref_st_flg = '2' and exists (select * from el_ref_st where el_ref_st.lib_id = el_info.lib_id and el_ref_st.st_id = $emp_st)))";
        $cond_info .= ")";
        // 参照：兼務所属
        $cond_info .= " or ";
        $cond_info .= "(";
            $cond_info .= "(el_info.ref_dept_flg = '1' or (el_info.ref_dept_flg = '2' and exists (select * from concurrent, el_ref_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_ref_dept.class_id and concurrent.emp_attribute = el_ref_dept.atrb_id and concurrent.emp_dept = el_ref_dept.dept_id and el_ref_dept.lib_id = el_info.lib_id)))";
            $cond_info .= " and ";
            $cond_info .= "(el_info.ref_st_flg = '1' or (el_info.ref_st_flg = '2' and exists (select * from concurrent, el_ref_st where concurrent.emp_id = '$emp_id' and el_ref_st.st_id = concurrent.emp_st and el_ref_st.lib_id = el_info.lib_id)))";
        $cond_info .= ")";
        // 参照：職員
        $cond_info .= " or ";
        $cond_info .= "exists (select * from el_ref_emp where el_ref_emp.lib_id = el_info.lib_id and el_ref_emp.emp_id = '$emp_id')";
        // 更新：所属
        $cond_info .= " or ";
        $cond_info .= "(";
            $cond_info .= "(el_info.upd_dept_flg = '1' or (el_info.upd_dept_flg = '2' and exists (select * from el_upd_dept where el_upd_dept.lib_id = el_info.lib_id and el_upd_dept.class_id = $emp_class and el_upd_dept.atrb_id = $emp_atrb and el_upd_dept.dept_id = $emp_dept)))";
            $cond_info .= " and ";
            $cond_info .= "(el_info.upd_st_flg = '1' or (el_info.upd_st_flg = '2' and exists (select * from el_upd_st where el_upd_st.lib_id = el_info.lib_id and el_upd_st.st_id = $emp_st)))";
        $cond_info .= ")";
        // 更新：兼務所属
        $cond_info .= " or ";
        $cond_info .= "(";
            $cond_info .= "(el_info.upd_dept_flg = '1' or (el_info.upd_dept_flg = '2' and exists (select * from concurrent, el_upd_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_upd_dept.class_id and concurrent.emp_attribute = el_upd_dept.atrb_id and concurrent.emp_dept = el_upd_dept.dept_id and el_upd_dept.lib_id = el_info.lib_id)))";
            $cond_info .= " and ";
            $cond_info .= "(el_info.upd_st_flg = '1' or (el_info.upd_st_flg = '2' and exists (select * from concurrent, el_upd_st where concurrent.emp_id = '$emp_id' and el_upd_st.st_id = concurrent.emp_st and el_upd_st.lib_id = el_info.lib_id)))";
        $cond_info .= ")";
        // 更新：職員
        $cond_info .= " or ";
        $cond_info .= "exists (select * from el_upd_emp where el_upd_emp.lib_id = el_info.lib_id and el_upd_emp.emp_id = '$emp_id')";
    $cond_info .= " )";

    //バージョン最新判定
    $cond_info .= " and exists (select * from el_edition where exists (select * from (select el_edition.base_lib_id, max(el_edition.edition_no) as edition_no from el_edition group by el_edition.base_lib_id) latest_edition where latest_edition.base_lib_id = el_edition.base_lib_id and latest_edition.edition_no = el_edition.edition_no) and el_edition.lib_id = el_info.lib_id)";



    //==============================
    // カテゴリの権限判定
    //==============================
    $cond_cate  = " and";
    $cond_cate .= " (";
        // 参照：所属
        $cond_cate .= " (";
            $cond_cate .= " (el_cate.ref_dept_flg = '1' or (el_cate.ref_dept_flg = '2' and exists (select * from el_cate_ref_dept where el_cate_ref_dept.lib_cate_id = el_cate.lib_cate_id and el_cate_ref_dept.class_id = $emp_class and el_cate_ref_dept.atrb_id = $emp_atrb and el_cate_ref_dept.dept_id = $emp_dept)))";
            $cond_cate .= " and ";
            $cond_cate .= " (el_cate.ref_st_flg = '1' or (el_cate.ref_st_flg = '2' and exists (select * from el_cate_ref_st where el_cate_ref_st.lib_cate_id = el_cate.lib_cate_id and el_cate_ref_st.st_id = $emp_st)))";
        $cond_cate .= " )";
        // 参照：兼務所属
        $cond_cate .= " or ";
        $cond_cate .= " (";
            $cond_cate .= " (el_cate.ref_dept_flg = '1' or (el_cate.ref_dept_flg = '2' and exists (select * from concurrent, el_cate_ref_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_cate_ref_dept.class_id and concurrent.emp_attribute = el_cate_ref_dept.atrb_id and concurrent.emp_dept = el_cate_ref_dept.dept_id and el_cate_ref_dept.lib_cate_id = el_cate.lib_cate_id)))";
            $cond_cate .= " and ";
            $cond_cate .= " (el_cate.ref_st_flg = '1' or (el_cate.ref_st_flg = '2' and exists (select * from concurrent, el_cate_ref_st where concurrent.emp_id = '$emp_id' and el_cate_ref_st.st_id = concurrent.emp_st and el_cate_ref_st.lib_cate_id = el_cate.lib_cate_id)))";
        $cond_cate .= " )";
        // 参照：職員
        $cond_cate .= " or";
        $cond_cate .= " exists (select * from el_cate_ref_emp where el_cate_ref_emp.lib_cate_id = el_cate.lib_cate_id and el_cate_ref_emp.emp_id = '$emp_id')";
        // 更新：所属
        $cond_cate .= " or (";
            $cond_cate .= " (el_cate.upd_dept_flg = '1' or (el_cate.upd_dept_flg = '2' and exists (select * from el_cate_upd_dept where el_cate_upd_dept.lib_cate_id = el_cate.lib_cate_id and el_cate_upd_dept.class_id = $emp_class and el_cate_upd_dept.atrb_id = $emp_atrb and el_cate_upd_dept.dept_id = $emp_dept)))";
            $cond_cate .= " and ";
            $cond_cate .= " (el_cate.upd_st_flg = '1' or (el_cate.upd_st_flg = '2' and exists (select * from el_cate_upd_st where el_cate_upd_st.lib_cate_id = el_cate.lib_cate_id and el_cate_upd_st.st_id = $emp_st)))";
        $cond_cate .= " )";
        // 更新：兼務所属
        $cond_cate .= " or ";
        $cond_cate .= " (";
            $cond_cate .= " (el_cate.upd_dept_flg = '1' or (el_cate.upd_dept_flg = '2' and exists (select * from concurrent, el_cate_upd_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_cate_upd_dept.class_id and concurrent.emp_attribute = el_cate_upd_dept.atrb_id and concurrent.emp_dept = el_cate_upd_dept.dept_id and el_cate_upd_dept.lib_cate_id = el_cate.lib_cate_id)))";
            $cond_cate .= " and ";
            $cond_cate .= " (el_cate.upd_st_flg = '1' or (el_cate.upd_st_flg = '2' and exists (select * from concurrent, el_cate_upd_st where concurrent.emp_id = '$emp_id' and el_cate_upd_st.st_id = concurrent.emp_st and el_cate_upd_st.lib_cate_id = el_cate.lib_cate_id)))";
        $cond_cate .= " )";
        // 更新：職員
        $cond_cate .= " or";
        $cond_cate .= " exists (select * from el_cate_upd_emp where el_cate_upd_emp.lib_cate_id = el_cate.lib_cate_id and el_cate_upd_emp.emp_id = '$emp_id')";
    $cond_cate .= " )";



    //==============================
    //フォルダの権限判定
    //==============================
    $cond_folder  = " and";
    $cond_folder .= " (";
        // 参照：所属
        $cond_folder .= " ( ";
            $cond_folder .= " (el_folder.ref_dept_flg = '1' or (el_folder.ref_dept_flg = '2' and exists (select * from el_folder_ref_dept where el_folder_ref_dept.folder_id = el_folder.folder_id and el_folder_ref_dept.class_id = $emp_class and el_folder_ref_dept.atrb_id = $emp_atrb and el_folder_ref_dept.dept_id = $emp_dept))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (el_folder.ref_st_flg = '1' or (el_folder.ref_st_flg = '2' and exists (select * from el_folder_ref_st where el_folder_ref_st.folder_id = el_folder.folder_id and el_folder_ref_st.st_id = $emp_st))) ";
        $cond_folder .= " )";
        // 参照：兼務所属
        $cond_folder .= " or ";
        $cond_folder .= " (";
            $cond_folder .= " (el_folder.ref_dept_flg = '1' or (el_folder.ref_dept_flg = '2' and exists (select * from concurrent, el_folder_ref_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_folder_ref_dept.class_id and concurrent.emp_attribute = el_folder_ref_dept.atrb_id and concurrent.emp_dept = el_folder_ref_dept.dept_id and el_folder_ref_dept.folder_id = el_folder.folder_id))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (el_folder.ref_st_flg = '1' or (el_folder.ref_st_flg = '2' and exists (select * from concurrent, el_folder_ref_st where concurrent.emp_id = '$emp_id' and el_folder_ref_st.st_id = concurrent.emp_st and el_folder_ref_st.folder_id = el_folder.folder_id))) ";
        $cond_folder .= " )";
        // 参照：職員
        $cond_folder .= " or";
        $cond_folder .= " exists (select * from el_folder_ref_emp where el_folder_ref_emp.folder_id = el_folder.folder_id and el_folder_ref_emp.emp_id = '$emp_id') ";
        // 更新：所属
        $cond_folder .= " or ";
        $cond_folder .= " (";
            $cond_folder .= "  (el_folder.upd_dept_flg = '1' or (el_folder.upd_dept_flg = '2' and exists (select * from el_folder_upd_dept where el_folder_upd_dept.folder_id = el_folder.folder_id and el_folder_upd_dept.class_id = $emp_class and el_folder_upd_dept.atrb_id = $emp_atrb and el_folder_upd_dept.dept_id = $emp_dept))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (el_folder.upd_st_flg = '1' or (el_folder.upd_st_flg = '2' and exists (select * from el_folder_upd_st where el_folder_upd_st.folder_id = el_folder.folder_id and el_folder_upd_st.st_id = $emp_st))) ";
        $cond_folder .= " )";
        // 更新：兼務所属
        $cond_folder .= " or ";
        $cond_folder .= " (";
            $cond_folder .= " (el_folder.upd_dept_flg = '1' or (el_folder.upd_dept_flg = '2' and exists (select * from concurrent, el_folder_upd_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_folder_upd_dept.class_id and concurrent.emp_attribute = el_folder_upd_dept.atrb_id and concurrent.emp_dept = el_folder_upd_dept.dept_id and el_folder_upd_dept.folder_id = el_folder.folder_id))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (el_folder.upd_st_flg = '1' or (el_folder.upd_st_flg = '2' and exists (select * from concurrent, el_folder_upd_st where concurrent.emp_id = '$emp_id' and el_folder_upd_st.st_id = concurrent.emp_st and el_folder_upd_st.folder_id = el_folder.folder_id))) ";
        $cond_folder .= " )";
        // 更新：職員
        $cond_folder .= " or";
        $cond_folder .= " exists (select * from el_folder_upd_emp where el_folder_upd_emp.folder_id = el_folder.folder_id and el_folder_upd_emp.emp_id = '$emp_id') ";
    $cond_folder .= " )";



    //==============================
    //SQLを組み立てて返却
    //==============================
    return "$sql1 $cond1 $cond_info $cond_cate union $sql2 $cond2 $cond_info $cond_folder ";
}
