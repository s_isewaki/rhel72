<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_workflow_common.ini");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

$cl_title = cl_title_name();

/**
 * アプリケーションメニューHTML取得
 */
$wkfwctn_mnitm_str=get_wkfw_menuitem($session,$fname);

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベース接続オブジェクト取得
 */
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

/**
 * ログインユーザ情報取得
 */
require_once(dirname(__FILE__) . "/cl/model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);
$arr_login = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_login["emp_id"];



if ($_POST['regist_flg'] == 'true') {

	/**
	 * トランザクション開始
	 */
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	/**
	 * 看護副部長モデル呼出
	 */
	require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_nurse_deputy_manager_model.php");
	$cl_mst_nurse_deputy_manager_model = new cl_mst_nurse_deputy_manager_model($mdb2, $login_emp_id);
	$log->debug("看護副部長モデル呼出",__FILE__,__LINE__);

	/**
	 * 看護副部長削除(一括)
	 */
	$cl_mst_nurse_deputy_manager_model->physical_delete();
	$log->debug("看護副部長削除",__FILE__,__LINE__);

	/**
	 * 看護副部長ＩＤ取得用モデル呼出
	 */
	require_once(dirname(__FILE__) . "/cl/model/sequence/cl_nurse_deputy_manager_id_seq_model.php");
	$nurse_deputy_manager_id_model = new cl_nurse_deputy_manager_id_seq_model($mdb2, $login_emp_id);
	$log->debug("看護副部長ＩＤ取得用モデル呼出",__FILE__,__LINE__);

	define("EMP_ID_PREFIX","emp_id");

	// 画面ＩＤを取得
	$arr_field_name = array_keys($_POST);

	// 画面項目分ループ
	foreach($arr_field_name as $field_name){

		// 項目が職員ＩＤの場合
		$pos = strpos($field_name,EMP_ID_PREFIX);
		if ($pos === 0) {

			// 職員ＩＤが設定されていない場合、次の行へ
			$arr_emp_ids = $_POST[$field_name];
			if ($arr_emp_ids == '') {
				continue;
			}

			// 所属先を取得
			$affiliation_id = substr($field_name,strlen(EMP_ID_PREFIX));
			$arr_affiliation_id = explode('-' ,$affiliation_id);
			$class_division = 0;
			foreach($arr_affiliation_id as $affiliation_id) {
				if ($affiliation_id == '') {
					break;
				} else {
					$class_division++;
				}
			}

			// 所属先に選択されている顧客分ループ
			$arr_emp_id = explode(',' ,$arr_emp_ids);
			foreach($arr_emp_id as $emp_id) {

				// 看護副部長ＩＤ取得
				$nurse_deputy_manager_id = $nurse_deputy_manager_id_model->getId();

				// パラメータ設定
				$param = array(
					'nurse_deputy_manager_id'	=> $nurse_deputy_manager_id	,
					'class_division'			=> $class_division			,
					'class_id'					=> $arr_affiliation_id[0]	,
					'atrb_id'					=> $arr_affiliation_id[1]	,
					'dept_id'					=> $arr_affiliation_id[2]	,
					'room_id' 					=> $arr_affiliation_id[3]	,
					'emp_id'					=> $emp_id
				);

				// 看護副部長登録
				$cl_mst_nurse_deputy_manager_model->insert($param);
			}
		}
	}


	/**
	 * コミット処理
	 */
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

/**
 * 登録フラグを初期化
 */
$regist_flg = '';

/**
 * 看護副部長取得
 */
require_once(dirname(__FILE__) . "/cl/model/search/cl_nurse_deputy_manager_select_list_model.php");
$nurse_deputy_manager_select_list_model = new cl_nurse_deputy_manager_select_list_model($mdb2, $login_emp_id);
$data = $nurse_deputy_manager_select_list_model->get_nurse_deputy_manager_list();

// 先頭行の値を保持しておく
$key = $data[0]['row_id'];
$data[0]['view_flg'] = true;
$emp_ids = $data[0]['emp_id'];
$emp_nms = $data[0]['emp_lt_nm'].' '.$data[0]['emp_ft_nm'];
$emp_nms_view = '';
if ($data[0]['emp_id'] != '') {
	$emp_nms_view = write_emp_delete($data[0]['row_id'], $data[0]['emp_id'], $data[0]['emp_lt_nm'], $data[0]['emp_ft_nm']);
}

$row_count = count($data);

for($i = 1; $i < $row_count; $i++){

	// 表示フラグをONにする
	$data[$i]['view_flg'] = true;

	if ($key == $data[$i]['row_id']){
		// 職員ID、職員名
		$emp_ids .= ','.$data[$i]['emp_id'];
		$emp_nms .= ','.$data[$i]['emp_lt_nm'].' '.$data[$i]['emp_ft_nm'];

		if ($data[$i]['emp_id'] != '') {
			$emp_nms_view .= ",";
			$emp_nms_view .= write_emp_delete($data[$i]['row_id'], $data[$i]['emp_id'], $data[$i]['emp_lt_nm'], $data[$i]['emp_ft_nm']);
		}

		// 重複所属の表示フラグをOFFにする
		$data[$i - 1]['view_flg'] = false;

	} else {
		// 職員ID、職員名をセット
		$data[$i - 1]['emp_ids'] = $emp_ids;
		$data[$i - 1]['emp_nms'] = $emp_nms;
		$data[$i - 1]['emp_nms_view'] = $emp_nms_view;

		// 職員ID、職員名
		$emp_ids = $data[$i]['emp_id'];
		$emp_nms = $data[$i]['emp_lt_nm'].' '.$data[$i]['emp_ft_nm'];
		$emp_nms_view = '';

		if ($data[$i]['emp_id'] != '') {
			$emp_nms_view = write_emp_delete($data[$i]['row_id'], $data[$i]['emp_id'], $data[$i]['emp_lt_nm'], $data[$i]['emp_ft_nm']);
		}

		// キー入れ替え
		$key = $data[$i]['row_id'];

	}
}

// 最終行の職員ID、職員名をセット
$data[$i]['emp_ids'] = $emp_ids;
$data[$i]['emp_nms'] = $emp_nms;
$data[$i]['emp_nms_view'] = $emp_nms_view;

// DB切断
$mdb2->disconnect();

/**
 * テンプレートマッピング
 */
$log->debug("テンプレートマッピング開始",__FILE__,__LINE__);

$smarty->assign( 'cl_title'				, $cl_title						);
$smarty->assign( 'session'				, $session						);
$smarty->assign( 'regist_flg'			, $regist_flg					);
$smarty->assign( 'wkfwctn_mnitm_str'	, $wkfwctn_mnitm_str			);
$smarty->assign( 'js_str'				, $js_str						);
$smarty->assign( 'data'					, $data							);

/*
$smarty->assign( 'level_cnt'			, $arr_classname['class_cnt']	);
$smarty->assign( 'class_nm'				, $arr_classname['class_nm'	]	);
$smarty->assign( 'atrb_nm'				, $arr_classname['atrb_nm'	]	);
$smarty->assign( 'dept_nm'				, $arr_classname['dept_nm'	]	);
$smarty->assign( 'room_nm'				, $arr_classname['room_nm'	]	);
$smarty->assign( 'target_class'			, $target_class					);
$smarty->assign( 'target_atrb'			, $target_atrb					);
$smarty->assign( 'target_dept'			, $target_dept					);
$smarty->assign( 'target_room'			, $target_room					);
$smarty->assign( 'target_none'			, $target_none					);

*/

$log->debug("テンプレートマッピング終了",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート出力開始",__FILE__,__LINE__);

$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

$log->debug("テンプレート出力終了",__FILE__,__LINE__);

/**
 * アプリケーションメニューHTML取得
 */
function get_wkfw_menuitem($session,$fname)
{
	ob_start();
	show_wkfw_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 削除アンカー生成
 */
function write_emp_delete($row_id, $emp_id, $emp_lt_nm, $emp_ft_nm){

	$build  = "<a href=\"javascript:delete_emp('";
	$build .= $row_id;
	$build .= "', '";
	$build .= $emp_id;
	$build .= "', '";
	$build .= $emp_lt_nm;
	$build .= ' ';
	$build .= $emp_ft_nm;
	$build .= "');\">";
	$build .= $emp_lt_nm;
	$build .= ' ';
	$build .= $emp_ft_nm;
	$build .= "</a>";
	return $build;
}
$log->info(basename(__FILE__)." END");
$log->shutdown();
