<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ネットカンファレンス | ネットカンファレンス一覧</title>
<?
//------------------------------------------------------------------------------
// Title     :  ネットカンファレンス一覧画面
// File Name :  net_conference_menu.php
// History   :  2004/07/16 - リリース（岩本隆史）
// Copyright :
//------------------------------------------------------------------------------
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;  // スクリプトパス

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$auth_nc = check_authority($session, 10, $fname);
if ($auth_nc == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkValue() {
	if (confirm('削除してよろしいですか？')) {
		document.net_conference.action = 'net_conference_delete.php';
		document.net_conference.submit();
	}
}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="net_conference_menu.php?session=<? echo($session); ?>"><img src="img/icon/b09.gif" width="32" height="32" border="0" alt="ネットカンファレンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="net_conference_menu.php?session=<? echo($session); ?>"><b>ネットカンファレンス</b></a></font></td>
</tr>
</table>
<form name="net_conference" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="160" height="22" align="center" bgcolor="#5279a5"><a href="net_conference_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ネットカンファレンス一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="net_conference_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カンファレンス登録</font></a></td>
<td width="">&nbsp;</td>
<td width="100" align="center"><input type="button" value="削除" onclick="checkValue()"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? show_net_conference_list($session, $fname); ?>
<?
function show_net_conference_list($session, $fname) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?
$con = connect2db($fname);

	// ネットカンファレンス一覧取得SQLの実行
	$now = date("YmdHi");
	$sql = "select nmrm_id, nmrm_nm, nmrm_thm, (select count(*) from nmmemst where nmmemst.nmrm_id = nmrmmst.nmrm_id) as mem_cnt, nmrm_start, nmrm_end from nmrmmst";
	$cond = "where nmrm_end >= '$now' and nmrm_end_flg = 'f' and nmrm_del_flg = 'f' order by nmrm_start, nmrm_end";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// データベース接続をクローズ
	pg_close($con);

	// ネットカンファレンス一覧の出力
	while ($row = pg_fetch_array($sel)) {
		$nmrm_id = $row["nmrm_id"];
		$nmrm_nm = htmlspecialchars($row["nmrm_nm"]);
		$nmrm_thm = htmlspecialchars($row["nmrm_thm"]);
		$mem_cnt = $row["mem_cnt"];
		$nmrm_start = format_time($row["nmrm_start"]);
		$nmrm_end = format_time($row["nmrm_end"]);
		$nmrm_start=substr($nmrm_start,5);
		$nmrm_end=substr($nmrm_end,5);
?>
<tr>
<td width="30" height="22" align="center"><input name="trashbox[]" type="checkbox" value="<? echo($nmrm_id); ?>"></td>
<td width="180"><a href="net_conference_update.php?session=<? echo($session); ?>&nmrm_id=<? echo($nmrm_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $nmrm_nm; ?></font></a></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $nmrm_thm; ?></font></td>
<td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $mem_cnt; ?>人</font></td>
<td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $nmrm_start; ?>〜<? echo $nmrm_end; ?></font></td>
<td width="50" align="center"><a href="net_conference.php?session=<? echo($session); ?>&nmrm_id=<? echo($nmrm_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入室</font></a></td>
</tr>
<?
	}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
<?
}

//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み日時取得
// Name      :  format_time
// Arguments :  time - 日時（「YYYYMMDDHHMM」形式）
// Return    :  日時（「YYYY-MM-DD HH:MM」形式）
//------------------------------------------------------------------------------
function format_time($time) {
	$pattern = '/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/';
	$replacement = '$1-$2-$3 $4:$5';
	return preg_replace($pattern, $replacement, $time);
}

?>
</body>
</html>
