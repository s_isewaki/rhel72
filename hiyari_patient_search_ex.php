<?
//デバッグ出力禁止
ini_set("display_errors","0"); //"0":メッセージ出力なし "1":メッセージ出力あり

//Comedixで使用。DB、システムに応じてデータ取得処理は変更してください。
require("about_postgres.php");

//画面名
$fname = $PHP_SELF;

//システム名
$INCIDENT_TITLE = "ファントルくん";


//画面パラメータ
$param_names = array("caller","target_date","patient_id","patient_name","page", "callbacker_url", "is_postback");
foreach($param_names as $param_name)
{
	$$param_name = ($_POST[$param_name] == "") ? $_GET[$param_name] : $_POST[$param_name];
}

//画面パラメータ精査
$patient_id = str_replace(" ", "", $patient_id);
$patient_id = str_replace("　", "", $patient_id);
$patient_name = str_replace(" ", "", $patient_name);
$patient_name = str_replace("　", "", $patient_name);
if ($page == "")
{
	$page = 1;
}



//====================================================================================================
//HTML出力
//====================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?echo $INCIDENT_TITLE ?> | 患者検索</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

//起動時の処理を行います。
function loadaction()
{
	<?
	//初期起動時の場合
	if($is_postback != "true")
	{
	?>
		//検索結果1件の実行関数があれば実行
		if(window.single_selected)
		{
			single_selected();
		}
	<?
	}
	?>
}


//患者氏名が選択されたときの処理を行います。
function patient_selected(patient_id, patient_name, sex, age_year, age_month, patient_class1, patient_class2, inpt_date)
{
	document.callbacker_form.patient_id.value = patient_id;
	document.callbacker_form.patient_name.value = patient_name;
	document.callbacker_form.sex.value = sex;
	document.callbacker_form.age_year.value = age_year;
	document.callbacker_form.age_month.value = age_month;
	document.callbacker_form.patient_class1.value = patient_class1;
	document.callbacker_form.patient_class2.value = patient_class2;
	document.callbacker_form.inpt_date.value = inpt_date;
	document.callbacker_form.submit();

}

</script>
<style type="text/css">
form {margin:0;}
table.list {border-collapse:collapse;}
table.list td {border:solid #35B341 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadaction()">

<center>

<? show_hiyari_header_for_sub_window("患者検索"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<form name="main_form" action="hiyari_patient_search_ex.php" method="post">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="caller" value="<?=$caller?>">
<input type="hidden" name="target_date" value="<?=$target_date?>">
<input type="hidden" name="callbacker_url" value="<?=$callbacker_url?>">
<input type="hidden" name="page" value="<?=$page?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="70" align="right" bgcolor="DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td><input type="text" name="patient_id" value="<? echo($patient_id); ?>" size="20" style="ime-mode:inactive;" maxlength="10"></td>
<td width="70" align="right" bgcolor="DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td><input type="text" name="patient_name" value="<? echo($patient_name); ?>" size="20" style="ime-mode:active;"></td>
<td align="center" bgcolor="#F5FFE5" style="border-style:none;"><input type="submit" value="検索" onclick="document.main_form.page.value=1;"></td>
</tr>
</table>

<?
show_patient_list($patient_id, $patient_name, $target_date, $page, $fname);
?>

</form>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
</center>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<img src="img/spacer.gif" width="1" height="1" alt="">
</td>
</tr>
</table>

<?
if($target_date == "")
{
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
	<td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">
	※発生年月日が未入力の場合、年齢・入院等の情報はコピーされません。
	</font>
	</td>
	</tr>
	</table>
<?
}
?>

<!-- 結果通知フォームSTART -->
<form name="callbacker_form" action="<?=$callbacker_url?>" method="post">
<input type="hidden" name="target_date" value="<?=$target_date?>">
<input type="hidden" id="patient_id" name="patient_id">
<input type="hidden" id="patient_name" name="patient_name">
<input type="hidden" id="sex" name="sex">
<input type="hidden" id="age_year" name="age_year">
<input type="hidden" id="age_month" name="age_month">
<input type="hidden" id="patient_class1" name="patient_class1">
<input type="hidden" id="patient_class2" name="patient_class2">
<input type="hidden" id="inpt_date" name="inpt_date">
</form>
<!-- 結果通知フォームEND -->

</body>
</html>
<?


//====================================================================================================
//内部関数
//====================================================================================================



function show_patient_list($patient_id, $patient_name, $target_date, $page, $fname)
{

	//==============================
	//検索条件未指定の場合は非表示
	//==============================
	if ($patient_id == "" && $patient_name == "")
	{
		return;
	}
	
	//==============================
	//検索実行
	//==============================
	$data = exec_search($patient_id, $patient_name, $target_date, $fname);
	
	//==============================
	//ページング情報
	//==============================
	
	//表示最大レコード数
	define("ROW_LIMIT", 10);
	
	//全データ件数
	if($data == "")
	{
		$patient_count = 0;
	}
	else
	{
		$patient_count = count($data);
	}
	
	//最大ページ数
	$page_max = ceil($patient_count / ROW_LIMIT);
	if($page_max == 0)
	{
		$page_max = 1;
	}
	
	//==============================
	//ページングデータ処理
	//==============================
	if($page_max != 1)
	{
		//表示開始レコード番号
		$disp_start_row_index = ($page-1)*ROW_LIMIT;
		
		//表示終了レコード番号
		$disp_end_row_index = ($page)*ROW_LIMIT;
		if($disp_end_row_index > $patient_count)
		{
			$disp_end_row_index = $patient_count;
		}
		
		//データカットアウト
		$data2 = null;
		for ($i = $disp_start_row_index; $i < $disp_end_row_index; $i++)
		{
			$data2[] = $data[$i];
		}
		$data = $data2;
	}
	
	//==============================
	//一覧HTML出力
	//==============================
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="22" bgcolor="#F5FFE5">
			<td><img src="img/spacer.gif" alt="" width="1" height="3"></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22" bgcolor="#DFFFDC">
			<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
			<td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
			<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font></td>
			<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院年月日</font></td>
		</tr>
	<?
	foreach($data as $row)
	{
		//データ
		$tmp_patient_id   = $row['pt_id'];
		$tmp_patient_name = $row['pt_name'];
		$tmp_sex          = $row['sex'];
		$tmp_birthday     = $row['birthday'];
		$tmp_inpt_date    = $row['inpt_date'];
		
		//他システムと異なる場合は、性別値変換する
		//1:男性,2:女性,90:不明
		switch ($tmp_sex)
		{
			case "1":
				$tmp_sex = "1";
				$tmp_sex_string = "男性";
				break;
			case "2":
				$tmp_sex = "2";
				$tmp_sex_string = "女性";
				break;
			default:
				$tmp_sex = "90";
				$tmp_sex_string = "不明";
				break;
		}
		
		//年齢算出
		if($target_date != "" && $tmp_birthday != "")
		{
			//誕生日/対象日付を分解
			list($tmp_birth_year, $tmp_birth_month, $tmp_birth_day) = split("/", $tmp_birthday);
			list($tmp_today_year, $tmp_today_month, $tmp_today_day) = split("/", $target_date);
			
			//年齢(年)算出
			$tmp_age_year = ($tmp_today_year - $tmp_birth_year);
			if ("$tmp_today_month/$tmp_today_day" < "$tmp_birth_month/$tmp_birth_day")
			{
				$tmp_age_year--;
			}
			$tmp_age_year = sprintf("%03d", $tmp_age_year);
			
			//年齢(月)算出
			$tmp_age_month = ($tmp_today_month - $tmp_birth_month);
			if ("$tmp_today_day" < "$tmp_birth_day")
			{
				$tmp_age_month--;
			}
			if ($tmp_age_month < 0)
			{
				$tmp_age_month += 12;
			}
			$tmp_age_month = sprintf("%02d", $tmp_age_month);
		}
		else
		{
			$tmp_age_year = "";
			$tmp_age_month = "";
		}
		
		//患者区分判定
		if($target_date != "" && $tmp_inpt_date != "")
		{
			//患者区分1:入院中
			$tmp_patient_class1 = "1";
			
			//入院31日目の日付
			list($tmp_inpt_date_year, $tmp_inpt_date_month, $tmp_inpt_date_day) = split("/", $tmp_inpt_date);
			$tmp_inpt_31_date_time = mktime(0, 0, 0, $tmp_inpt_date_month, $tmp_inpt_date_day+30, $tmp_inpt_date_year);
			$tmp_inpt_31_date = date("Y/m/d",$tmp_inpt_31_date_time);
			
			//患者区分2:入院日数
			if($target_date <= $tmp_inpt_31_date)
			{
				//入院(0-31日)
				$tmp_patient_class2 = "1";
			}
			else
			{
				//入院(32日-)
				$tmp_patient_class2 = "2";
			}
		}
		else
		{
			$tmp_patient_class1 = "";
			$tmp_patient_class2 = "";
		}
		
		?>
		<tr height="22">
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($tmp_patient_id)?></font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a name="selected_link" href="javascript:patient_selected('<?=$tmp_patient_id?>', '<?=$tmp_patient_name?>', '<?=$tmp_sex?>', '<?=$tmp_age_year?>', '<?=$tmp_age_month?>', '<?=$tmp_patient_class1?>', '<?=$tmp_patient_class2?>', '<?=$tmp_inpt_date?>');"><?=h($tmp_patient_name)?></a></font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($tmp_sex_string)?></font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($tmp_birthday)?></font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($tmp_inpt_date)?></font></td>
		</tr>
		<?
		
		//検索結果が1件のみの場合は選択実行javaScript関数を出力
		if($patient_count == 1)
		{
			?>
			<script type="text/javascript">
			//起動時の処理を行います。
			function single_selected()
			{
				patient_selected('<?=$tmp_patient_id?>', '<?=$tmp_patient_name?>', '<?=$tmp_sex?>', '<?=$tmp_age_year?>', '<?=$tmp_age_month?>', '<?=$tmp_patient_class1?>', '<?=$tmp_patient_class2?>', '<?=$tmp_inpt_date?>');
			}
			</script>
			<?
		}
	}
	?>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
	<td><img src="img/spacer.gif" alt="" width="10" height="1"></td>
	</tr>
	</table>
	<?
	
	//==============================
	//ページングHTML出力
	//==============================
	if ($page_max > 1)
	{
		?>
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr height="22" bgcolor="#F5FFE5">
		<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<?
		for ($i = 1; $i <= $page_max; $i++)
		{
			if ($i == $page)
			{
				?>
				<?=$i?>
				<?
			}
			else
			{
				?>
				<a href="javascript:document.main_form.page.value=<?=$i?>;document.main_form.submit();"><?=$i?></a>
				<?
			}
		}
		?>
		</font></td>
		</tr>
		</table>
		<?
	}
	
	return $patient_count;
}




/**
 * 患者検索を実行します。
 *
 * @param string $pt_id 患者ID(検索条件)
 * @param string $pt_name 患者名(検索条件)
 * @param string $target_date 対象日付(検索/計算条件)
 * @param string $fname 画面名
 * @return array 検索結果情報配列
 */
function exec_search($pt_id,$pt_name,$target_date,$fname)
{
	$result = null;

	// データベースに接続
	$con = connect2db($fname);

	$sql = "select a.ptif_id, a.ptif_lt_kaj_nm, a.ptif_ft_kaj_nm, a.ptif_sex, a.ptif_birth, b.inpt_in_dt from ptifmst a ";
	//入院日の取得、取得方法はデータの持ち方に応じ変更する
	//Comedixでは、最新の入院情報と、履歴は別テーブルのため、単純な条件である。履歴も同じテーブルにあるような場合は、入院中の期間と発生日を確認する必要がある
	$sql .= " left join inptmst b on b.ptif_id = a.ptif_id and b.inpt_in_dt is not null ";
	$cond = get_search_condition($pt_id, $pt_name);
	$cond .= " order by a.ptif_id ";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = null;
		$data['pt_id'] = $row["ptif_id"];
		$data['pt_name'] = $row[ptif_lt_kaj_nm] . " " . $row[ptif_ft_kaj_nm];
		$data['birthday'] = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $row["ptif_birth"]);
		$data['sex'] = $row["ptif_sex"];
		$data['inpt_date'] = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $row["inpt_in_dt"]);
		$result[] = $data;

	}
	pg_close($con);

	return $result;
}

/**
 * インシデントレポートサブウィンドウ用のヘッダーを表示します。
 * 
 * @param string $title タイトル
 */
function show_hiyari_header_for_sub_window($title)
{
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#35B341">
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$title?></b></font></td>
	<td>&nbsp</td>
	<td width="10">&nbsp</td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	</table>
	<img src="img/spacer.gif" width="10" height="10" alt=""><br>
	<?
}

/**
 * 患者検索の条件を返す
 * 
 * @param string $patient_id 患者ID
 * @param string $patient_name 患者氏名
 * @return string 条件文字列
 */
function get_search_condition($patient_id, $patient_name) {
	$cond = "where a.ptif_del_flg = 'f'";
	if ($patient_id != "") {
		//患者コードの条件は利用者の要望に応じて変更
		$cond .= " and a.ptif_id = '$patient_id'"; //完全一致
//		$cond .= " and a.ptif_id like '%$patient_id%'"; //部分一致
	}
	if ($patient_name != "") {
		$trimmed_patient_name = str_replace(" ", "", $patient_name);
		$trimmed_patient_name = str_replace("　", "", $trimmed_patient_name);
		$cond .= " and (a.ptif_lt_kaj_nm || a.ptif_ft_kaj_nm like '%$trimmed_patient_name%' or a.ptif_lt_kana_nm || a.ptif_ft_kana_nm like '%$trimmed_patient_name%')";
	}
	return $cond;
}


?>
