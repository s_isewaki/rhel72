<?
$session = $_REQUEST["session"];
$file_id = $_REQUEST["file_id"];
$view_type = $_REQUEST["view_type"];
$file_flg = $_REQUEST["file_flg"]; // 最新ではview_typeが渡るようになっているため、この値は存在しないかもしれない

$script1 = '<html><head><script type="text/javascript">alert("';
$script2 = '"); window.close();</script></head></html>';

if (!$session) { echo $script1."ファイルの呼出しが不正です。(session)".$script2; die; }
if (!(int)$file_id) { echo $script1."ファイルの呼出しが不正です。(file_id)".$script2; die; }
if ($file_flg!="1" && $file_flg!="2" && $view_type!="doc" && $view_type!="fcl") {
    echo $script1."ファイルの呼出しが不正です。(view_type/".$view_type.")".$script2;
    die;
}

// セッションチェックのみ行いたい。
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) { echo $script1."ダウンロード権限がありません。".$script2; die; }


// ファイル存在確認
$contentRoot = dirname(dirname(__FILE__)); // /var/www/html/comedix になる
$file_path = "";
if ($file_flg=="1" || $view_type=="doc") {
    $paths = glob($contentRoot.'/extension/'.$file_id.'.*');
    if (count($paths) > 0) $file_path = $paths[0];
}
if ($file_flg=="2" || $view_type=="fcl") {
    $paths = glob($contentRoot.'/extension/fcl/'.$file_id.'.*');
    if (count($paths) > 0) $file_path = $paths[0];
}
if (!$file_path) { echo $script1."ファイルが存在しませんでした。".$script2; die; }


// ダウンロード
header("Content-Disposition: attachment");
header("Content-Type: application/".substr(strrchr($paths[0],"."),1));
header("Content-Length: " . filesize($file_path));
readfile($file_path);
die;

