<?
define("C2_LOGIN_FREE", 1); // ログインチェック不要
require_once("common.php"); // 本来は読込みと同時にログイン判別するが、ログインチェックはスルーされる

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

$page = max(1, (int)$_REQUEST["page"]); // ページ、１以上
$head_emp_id = $_REQUEST["head_emp_id"];
$class_id = (int)$_REQUEST["class_id"];
$atrb_id = (int)$_REQUEST["atrb_id"];
$dept_id = (int)$_REQUEST["dept_id"];


$emp_where = " where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
if ($class_id) {
    $emp_where .=
    " and ((empmst.emp_class = ".$class_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = ".$class_id."))";
}
if ($atrb_id) {
    $emp_where .=
    " and ((empmst.emp_attribute = ".$atrb_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = ".$atrb_id."))";
}
if ($dept_id) {
    $emp_where .= " and ((empmst.emp_dept = ".$dept_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = ".$dept_id."))";
}



//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 部門職員一覧</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body style="margin:5px"><div id="CLIENT_FRAME_CONTENT">



<table cellspacing="0" cellpadding="0" class="dialog_title"><tr>
    <th>部門職員一覧</td>
    <td class="closebtn"><a href="javascript:void(0);" onclick="window.close();"></a></td>
</tr></table>



<?
    // 部門情報を取得
    $title = c2dbGetOne("select class_nm from classmst where class_id = ".$class_id);
    if ($atrb_id) $title .= "＞". c2dbGetOne("select atrb_nm from atrbmst where atrb_id = ".$atrb_id);
    if ($dept_id) $title .= "＞". c2dbGetOne("select dept_nm from deptmst where dept_id = ".$dept_id);
?>
<div style="padding-top:10px">
    <table width="800" cellspacing="0" cellpadding="2" class="list prop_list">
        <tr>
            <th width="12%">部門</th>
            <td width="32%"><nobr><?=hh($title)?></nobr></td>
            <td style="border-style:none;"></td>
        </tr>
    </table>
</div>




<div style="padding-top:10px"><b>所属長</b></div>



<?
    // 所属長の情報を取得
    $sql =
    " select".
    " empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_phs, empmst.emp_ext, empmst.emp_mobile1, empmst.emp_mobile2, empmst.emp_mobile3".
    ",classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, jobmst.job_nm, stmst.st_nm".
    " from empmst".
    " inner join classmst on ( empmst.emp_class = classmst.class_id )".
    " inner join atrbmst on ( empmst.emp_attribute = atrbmst.atrb_id )".
    " inner join deptmst on ( empmst.emp_dept = deptmst.dept_id )".
    " inner join jobmst on ( empmst.emp_job = jobmst.job_id )".
    " inner join stmst on ( empmst.emp_st = stmst.st_id )".
    " where empmst.emp_id = ".c2dbStr($head_emp_id);
    $row = c2dbGetTopRow($sql);
?>
<div style="padding-top:2px">
    <table width="100%" cellspacing="0" cellpadding="2" class="list">
        <tr height="22" bgcolor="#f6f9ff">
            <td width="12%">職員氏名</td>
            <td width="32%">所属<?=hh($classnameRow["class_nm"])?></td>
            <td width="12%">職種</td>
            <td width="10%">役職</td>
            <td width="11%"><?=getPhsName()?></td>
            <td width="9%">内線番号</td>
            <td width="14%">携帯端末・外線</td>
        </tr>
        <tr height="22" valign="top">
            <td><?=hh($row["emp_lt_nm"]." ".$row["emp_ft_nm"])?></td>
            <td><?=hh($row["class_nm"])?>＞<?=hh($row["atrb_nm"])?>＞<?=hh($row["dept_nm"])?></td>
            <td><?=hh($row["job_nm"])?></td>
            <td><?=hh($row["st_nm"])?></td>
            <td><?=hh($row["emp_phs"])?></td>
            <td><?=hh($row["emp_ext"])?></td>
            <td><?=concatTel($row["emp_mobile1"], $row["emp_mobile2"], $row["emp_mobile3"])?></td>
        </tr>
    </table>
</div>



<div style="padding-top:10px">
    <table width="100%" border="0" cellspacing="0" cellpadding="2"><tr>
        <td><b>所属職員</b></td>
        <td align="right">
        <? //**************************************************************************************************************** ?>
        <? // ぱんくず                                                                                                        ?>
        <? //**************************************************************************************************************** ?>
        <?
            $ttl_count = (int)c2dbGetOne("select count(*) from empmst ".$emp_where." and emp_id <> ".c2dbStr($head_emp_id));
            if ($ttl_count <= ROW_LIMIT10) $page = 1;
            if ($ttl_count > ROW_LIMIT10) {
                echo("ページ：\n");
                $max_page = ceil($ttl_count / ROW_LIMIT10);
                for ($p=1; $p<=$max_page; $p++) {
                    if ($p == $page) {
                        echo $p."&nbsp;";
                    } else {
                        echo '<a href="ext_class_list_from_login.php?&head_emp_id='.$head_emp_id;
                        echo '&class_id='.$class_id.'&atrb_id='.$atrb_id.'&dept_id='.$dept_id.'&page='.$p.'">'.$p.'</a>&nbsp;';
                    }
                }
            }
        ?>
        </td>
    </tr></table>
</div>



<table width="100%" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" valign="top">
    <td width="12%">職員氏名</td>
    <td width="32%">所属<?=$classnameRow["class_nm"]?></td>
    <td width="12%">職種</td>
    <td width="10%">役職</td>
    <td width="11%"><?=getPhsName()?></td>
    <td width="9%">内線番号</td>
    <td width="14%">携帯端末・外線</td>
</tr>
<?
    $sql =
    " select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm".
    ",classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, jobmst.job_nm, stmst.st_nm".
    ",empmst.emp_phs, empmst.emp_ext, empmst.emp_mobile1, empmst.emp_mobile2, empmst.emp_mobile3".
    " from empmst".
    " inner join classmst on ( empmst.emp_class = classmst.class_id )".
    " inner join atrbmst on ( empmst.emp_attribute = atrbmst.atrb_id )".
    " inner join deptmst on ( empmst.emp_dept = deptmst.dept_id )".
    " inner join jobmst on ( empmst.emp_job = jobmst.job_id )".
    " inner join stmst on ( empmst.emp_st = stmst.st_id )".
    " ".$emp_where.
    " and empmst.emp_id <> ".c2dbStr($head_emp_id).
    " order by empmst.emp_keywd, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_id".
    " offset ".(($page-1)*ROW_LIMIT10)." limit ".ROW_LIMIT10;
    $rows = c2dbGetRows($sql);
    foreach ($rows as $row) {
        echo '<tr height="22" valign="top">';
        echo '<td>'.hh($row["emp_lt_nm"] . " " . $row["emp_ft_nm"]).'</td>';
        echo '<td>'.hh($row["class_nm"]."＞".$row["atrb_nm"]."＞".$row["dept_nm"]).'</td>';
        echo '<td>'.hh($row["job_nm"]).'</td>';
        echo '<td>'.hh($row["st_nm"]).'</td>';
        echo '<td>'.hh($row["emp_phs"]).'</td>';
        echo '<td>'.hh($row["emp_ext"]).'</td>';
        echo '<td>'.hh(concatTel($row["emp_mobile1"], $row["emp_mobile2"], $row["emp_mobile3"])).'</td>';
        echo '</tr>'."\n";
    }
?>
</table>


</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>