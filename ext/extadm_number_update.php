<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$emp_id = $_REQUEST["emp_id"];
$view_type = $_REQUEST["view_type"]; // extadm_menuでのモード

// 選択された職員の連絡先情報を取得
$sql = "select emp_lt_nm, emp_ft_nm, emp_phs, emp_ext, emp_mobile1, emp_mobile2, emp_mobile3 from empmst where emp_id = ".c2dbStr($emp_id);
$empmstRow = c2dbGetTopRow($sql);

// 画面に渡された検索条件
$self_url_params =
"srch_emp_nm=".urlencode($_REQUEST["srch_emp_nm"]).
"&class_id=".urlencode($_REQUEST["class_id"]).
"&atrb_id=".urlencode($_REQUEST["atrb_id"]).
"&dept_id=".urlencode($_REQUEST["dept_id"]).
"&view_type=".urlencode($_REQUEST["view_type"]).
"&akasatana=".urlencode($_REQUEST["akasatana"]).
"&photo_flg=".urlencode($_REQUEST["photo_flg"]).
"&page=".urlencode($_REQUEST["page"]);

//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
    $empmstRow = $_REQUEST;

    // アクセスログ
    require_once("aclg_set.php");
    aclg_regist($session, "extension_admin_number_update_exe.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

    // 入力チェック
    if (strlen($empmstRow["emp_phs"]) > 6) $errmsg[]= getPhsName()."が長すぎます。"; // 院内PHS/所内PHS
    if (strlen($empmstRow["emp_ext"]) > 10) $errmsg[]= '内線番号が長すぎます。';
    if (strlen($empmstRow["emp_mobile1"]) > 6) $errmsg[]= '携帯端末（1）が長すぎます。';
    if (strlen($empmstRow["emp_mobile2"]) > 6) $errmsg[]= '携帯端末（2）が長すぎます。';
    if (strlen($empmstRow["emp_mobile3"]) > 6) $errmsg[]= '携帯端末（3）が長すぎます。';
    if (!allOrNothing($empmstRow["emp_mobile1"], $empmstRow["emp_mobile2"], $empmstRow["emp_mobile3"])) $errmsg[]= '携帯端末を正しく入力してください。';

    // 職員情報を更新
    if (!count($errmsg)) {
        $sql =
        " update empmst set".
        " emp_phs = ".c2dbStr($empmstRow["emp_phs"]).
        ",emp_ext = ".c2dbStr($empmstRow["emp_ext"]).
        ",emp_mobile1 = ".c2dbStr($empmstRow["emp_mobile1"]).
        ",emp_mobile2 = ".c2dbStr($empmstRow["emp_mobile2"]).
        ",emp_mobile3 = ".c2dbStr($empmstRow["emp_mobile3"]).
        " where emp_id = ".c2dbStr($emp_id);
        c2dbExec($sql);

        // 一覧／検索画面に遷移
        $dest_url = "extadm_search.php?".$self_url_params."&action=search";
        if ($view_type=="emptree" || $view_type=="akasatana") {
            $dest_url = "extadm_menu.php?".$self_url_params;
        }
	    ob_clean();
        header("Location: ".$dest_url);
        die;
    }
}
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 | 番号変更</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<?=$admin_pankuzu?>


<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="extadm_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="extadm_search.php">番号検索</a></th>
    <th class="current"><a href="extadm_number_update.php?emp_id=<?=$emp_id?>&<?=$self_url_params?>">番号変更</a></th>
    <th class="selectable w120"><a href="extadm_facility_register.php"><nobr>施設連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_class_register.php"><nobr>部門連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_document_register.php"><nobr>番号リスト登録</nobr></a></th>
    <th class="selectable"><a href="extadm_option.php">オプション</a></th>
    </tr></table>
</div>




<form action="extadm_number_update.php" method="post">
<input type="hidden" name="try_update" value="1">


<div style="padding-top:5px; width:600px">

<table width="100%" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="140">職員氏名</th>
        <td><?=hh($empmstRow["emp_lt_nm"]." ".$empmstRow["emp_ft_nm"])?></td>
    </tr>
    <tr>
        <th><?=getPhsName()?></th>
        <td><input type="text" name="emp_phs" value="<?=hh($empmstRow["emp_phs"])?>" size="10" maxlength="6" style="ime-mode:inactive;"></td>
    </tr>
    <tr>
        <th>内線番号</th>
        <td><input type="text" name="emp_ext" value="<?=hh($empmstRow["emp_ext"])?>" size="10" maxlength="10" style="ime-mode:inactive;"></td>
    </tr>
    <tr>
        <th>携帯端末・外線</th>
        <td>
            <input type="text" name="emp_mobile1" value="<?=($empmstRow["emp_mobile1"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="emp_mobile2" value="<?=($empmstRow["emp_mobile2"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="emp_mobile3" value="<?=($empmstRow["emp_mobile3"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
        </td>
    </tr>
</table>


<div style="text-align:right; padding-top:2px"><input type="submit" value="更新"></div>


</div>

<input type="hidden" name="emp_id" value="<?=hh($emp_id)?>">
<input type="hidden" name="srch_emp_nm" value="<?=hh($_REQUEST["srch_emp_nm"])?>"><? // 番号検索画面での検索条件 ?>
<input type="hidden" name="class_id" value="<?=hh($_REQUEST["class_id"])?>"><? // 番号検索画面での検索条件または、番号一覧の職員ツリー ?>
<input type="hidden" name="atrb_id" value="<?=hh($_REQUEST["atrb_id"])?>"><? // 番号検索画面での検索条件または、番号一覧の職員ツリー ?>
<input type="hidden" name="dept_id" value="<?=hh($_REQUEST["dept_id"])?>"><? // 番号検索画面での検索条件または、番号一覧の職員ツリー ?>
<input type="hidden" name="view_type" value="<?=hh($_REQUEST["view_type"])?>"><? // 番号一覧のモード ?>
<input type="hidden" name="page" value="<?=hh($_REQUEST["page"])?>">
<input type="hidden" name="photo_flg" value="<?=hh($_REQUEST["photo_flg"])?>">
<input type="hidden" name="akasatana" value="<?=hh($_REQUEST["akasatana"])?>">
</form>


</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>
