<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$cadNames = array();
$stmstNames = array();
$empmstNames = array();
$sql =
" select am.class_id, dm.atrb_id, dm.dept_id, cm.class_nm, am.atrb_nm, dm.dept_nm".
" from deptmst dm".
" inner join atrbmst am on (am.atrb_id = dm.atrb_id)".
" inner join classmst cm on (cm.class_id = am.class_id)".
" where dept_del_flg = 'f' order by am.order_no, dm.order_no";
$_rows = c2dbGetRows($sql);// 科一覧
foreach ($_rows as $idx => $row) {
    $cadNames[$row["class_id"]."_".$row["atrb_id"]."_".$row["dept_id"]] = $row["class_nm"]." ＞ ".$row["atrb_nm"]." ＞ ".$row["dept_nm"];
}
$_rows = c2dbGetRows("select st_id, st_nm from stmst where st_del_flg = 'f' order by st_id");// 役職一覧
foreach ($_rows as $idx => $row) {
    $stmstNames[$row["st_id"]] = $row["st_nm"];
}
$_rows = c2dbGetRows("select emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_nm from empmst");// 職員一覧
foreach ($_rows as $idx => $row) {
    $empmstNames[$row["emp_id"]] = $row["emp_nm"];
}

    $sql = "select field_id, field_label from spfm_field_struct where table_id = 'spft_empmst' and field_id like 'empmst_ext%'";
    $_rows = c2dbGetRows($sql);
    $fieldLabels = array();
    foreach ($_rows as $row) {
        $fieldLabels[$row["field_id"]] = $row["field_label"];
    }


// 表示列。そもそもシステム的に利用するフィールドかを確認
$sql =
" select table_id, field_id from spfm_field_struct_auth".
" where target_place = 'all'".
" and disp_emp = 'all_ok'";
$_rows = c2dbGetRows($sql);
$fieldStructAuthRows = array();
foreach ($_rows as $row) {
    $fieldStructAuthRows[$row["field_id"]]["field_label"] = $fieldLabels[$row["field_id"]];
}


// 表示列。内線電話帳的に参照するフィールドかを確認
$_rows = c2dbGetRows("select table_id, field_id, disp_emp, disp_st, disp_cad from spfm_field_struct_auth where target_place = 'extension_viewing'");
$extStructAuthRows = array();
foreach ($_rows as $row) {
    $extStructAuthRows[$row["field_id"]] = $row;
}


$lgnList = explode(",", c2dbGetSystemConfig("ext.searchFromLoginDispCols"));
$showPhotoProfile = c2dbGetSystemConfig("ext.showPhotoProfile");

//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
if ($_REQUEST["try_update"]) {
    $initial_flg = $_REQUEST["initial_flg"];
    $facility_flg = $_REQUEST["facility_flg"];
    $class_flg = $_REQUEST["class_flg"];
    $emptree_flg = $_REQUEST["emptree_flg"];
    $showPhotoProfile = $_REQUEST["showPhotoProfile"];

    $lgnList = array();
    $ary = array("class_nm","st_nm", "job_nm", "emp_ext", "emp_phs", "emp_mobile");
    for ($idx=1; $idx<=5; $idx++) {
        if (!$fieldStructAuthRows["empmst_ext".$idx]) continue;
        $ary[]= "empmst_ext".$idx;
    }
    foreach ($ary as $_field_id) {
        if ($_REQUEST["lgn_".$_field_id]) $lgnList[]= $_field_id;
    }

    $extadmoptRow["newcomer_flg"] = $_REQUEST["newcomer_flg"];

    foreach ($extStructAuthRows as $field_id => $row) {
        $data = explode("\t", $_REQUEST["hdn_".$field_id]);
        $row = array();
        foreach ($data as $param) {
            list($k, $v) = split("=",$param);
            if (!$k) continue;
            $row[$k] = $v;
        }
        $extStructAuthRows[$field_id] = $row;
    }

    // アクセスログ
    require_once("aclg_set.php");
    aclg_regist($session, "extension_admin_option_update_exe.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

    $sql = "select field_id, field_label from spfm_field_struct where table_id = 'spft_empmst' and field_id like 'empmst_ext%'";
    $_rows = c2dbGetRows($sql);
    $fieldLabels = array();
    foreach ($_rows as $row) $fieldLabels[$row["field_id"]] = $row["field_label"];


    // トランザクションを開始
//    c2dbBeginTrans();
    foreach ($extStructAuthRows as $field_id => $row) {
        if (substr($field_id, 0, 10)=="empmst_ext" && !$fieldStructAuthRows[$field_id]) continue;
        $updSql = array();
        foreach ($row as $k => $v) {
            $updSql[]= $k."=".c2dbStr($v);
        }
        $sql =
        " update spfm_field_struct_auth set".
        " disp_emp = ".c2dbStr($row["disp_emp"]).
        ",disp_st = ".c2dbStr($row["disp_st"]).
        ",disp_cad = ".c2dbStr($row["disp_cad"]).
        " where field_id = ".c2dbStr($field_id).
        " and table_id = ".c2dbStr($row["table_id"]).
        " and target_place = 'extension_viewing'";
        c2dbExec($sql);
    }

    if ($initial_flg != "" || $facility_flg != "" || $class_flg != "" || $emptree_flg != "") {
        // オプション情報の無い職員のレコードを作成、各フラグはとりあえずfalseで登録
        $sql =
        " insert into extopt (emp_id, initial_flg, facility_flg, class_flg, emptree_flg)".
        " select tbl.emp_id, 'f', 'f', 'f', 'f'".
        " from (".
        "     select empmst.emp_id".
        "     from empmst".
        "     left outer join extopt on (extopt.emp_id = empmst.emp_id)".
        "     where extopt.emp_id is null".
        " ) tbl";
        c2dbExec($sql);

        // 全職員を更新
        $upd = array();
        if ($initial_flg  != "") $upd[]= "initial_flg = ".c2dbBool($initial_flg);
        if ($facility_flg != "") $upd[]= "facility_flg = ".c2dbBool($facility_flg);
        if ($class_flg != "") $upd[]= "class_flg = ".c2dbStr($class_flg);
        if ($emptree_flg != "") $upd[]= "emptree_flg = ".c2dbStr($emptree_flg);
        if ($upd) c2dbExec("update extopt set ".implode(", ",$upd));
    }
    c2dbSetSystemConfig("ext.searchFromLoginDispCols", implode(",", $lgnList), 1);

    c2dbSetSystemConfig("ext.showPhotoProfile", $showPhotoProfile, 1);

    // 共通のオプション情報をDELETE〜INSERT
    c2dbExec("delete from extadmopt");
    c2dbExec("insert into extadmopt (newcomer_flg) values (".c2dbBool($extadmoptRow["newcomer_flg"]).")");
//    c2dbCommit();

    // 画面を再表示
    ob_clean();
    header("Location: extadm_option.php");
    die;
}
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 |オプション</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<style type="text/css">
    table.inner_list td { padding:3px }
</style>
<script type="text/javascript">
var ee = function(id) { return document.getElementById(id); };

var cadNames = <?=c2ToJson($cadNames)?>;
var stmstNames = <?=c2ToJson($stmstNames)?>;
var empmstNames = <?=c2ToJson($empmstNames)?>;



var extStructAuthRows = <?=c2ToJson($extStructAuthRows)?>;
    var curCol = "";
    var extAdminAuthWin = 0;
    function popupAuthSelector(col) {
        curCol = col;
        extAdminAuthWin = window.open("extadm_auth_sel_dlg.php?callback_func=callbackAuthSelector&onload_func=initAuthSelector", "extadm_auth", "width=960,height=530");
    }
    function initAuthSelector() {
        return appendFormat(extStructAuthRows[curCol]);
    }
    function callbackAuthSelector(ret) {
        var obj = extStructAuthRows[curCol];

        // まず全職員許可ラジオ判定。全職員許可ならall_ok。それ以外は全職員不許可（all_ng）とみなす。
        obj.disp_emp =  (ret.rdo_all=="all_ok" ? "all_ok" : "");
        obj.disp_st =   (ret.rdo_all=="all_ok" ? "all_ok" : "");
        obj.disp_cad = (ret.rdo_all=="all_ok" ? "all_ok" : "");
        // 全職員許可でも全職員不許可でもない場合（refer_listである場合）
        // 部署役職については、さらに部署役職それぞれのall_okかどうかを判定する。
        // 部署役職のall_okでない場合と、職員については、リストをとってくる。
        if (ret.rdo_all=="ref_list") {
            if (ret.ary_emp.length) obj.disp_emp = "[" + ret.ary_emp.join("],[") + "]";

            if (ret.rdo_st=="all_ok") obj.disp_st = "all_ok";
            else if (ret.ary_st.length) obj.disp_st = "[" + ret.ary_st.join("],[") + "]";

            if (ret.rdo_cad=="all_ok") obj.disp_cad = "all_ok";
            else if (ret.ary_cad.length) obj.disp_cad = "[" + ret.ary_cad.join("],[") + "]";
        }

        extStructAuthRows[curCol] = obj;
        createAuthHtml(curCol);
        if (extAdminAuthWin) extAdminAuthWin.close();
        extAdminAuthWin = 0;
        document.getElementById("auth_change_msg").style.display = "";
    }
    function appendFormat(obj) {
        obj.ary_st = obj.disp_st.replace(/\[/g, "").replace(/\]/g, "").split(",");
        obj.ary_cad = obj.disp_cad.replace(/\[/g, "").replace(/\]/g, "").split(",");
        obj.ary_emp = obj.disp_emp.replace(/\[/g, "").replace(/\]/g, "").split(",");
        if (obj.ary_st[0]  =="all_ok" || !obj.ary_st[0])   obj.ary_st = [];
        if (obj.ary_cad[0]=="all_ok" || !obj.ary_cad[0]) obj.ary_cad = [];
        if (obj.ary_emp[0] =="all_ok" || !obj.ary_emp[0])  obj.ary_emp = [];

        obj.rdo_all ="all_ok";
        if (obj.disp_emp!="all_ok" || obj.disp_st!="all_ok" || obj.disp_cad!="all_ok") obj.rdo_all = "ref_list";

        obj.rdo_sub = "ref_both";
        if (obj.rdo_all=="ref_list") {
            if ((obj.ary_st.length || obj.ary_cad.length) && !obj.ary_emp.length) obj.rdo_sub = "ref_cad_st";
            if (!obj.ary_st.length && !obj.ary_cad.length && obj.ary_emp_length)   obj.rdo_sub = "ref_emp";
        }
        obj.rdo_cad = (obj.disp_cad=="all_ok" ? "all_ok" : "ref_list");
        obj.rdo_st = (obj.disp_st=="all_ok" ? "all_ok" : "ref_list");

        if (obj.rdo_all!="all_ok" && !obj.ary_cad.length && !obj.ary_st.length && !obj.ary_emp.length) obj.rdo_all ="all_ng";
        return obj;
    }
    function createAuthHtml(col) {
        var obj = appendFormat(extStructAuthRows[col]);
        var _cadNames = [];
        var _stNames = [];
        var _empNames = [];
        if (obj.rdo_all!="all_ok") {
            if (obj.rdo_sub=="ref_cad_st" || obj.rdo_sub=="ref_both") {
                if (obj.rdo_cad=="all_ok") _cadNames.push("全部署");
                else {
                    for (var idx=0; idx<obj.ary_cad.length; idx++) _cadNames.push(cadNames[obj.ary_cad[idx]]);
                }
                if (obj.rdo_st=="all_ok") _stNames.push("全役職");
                else {
                    for (var idx=0; idx<obj.ary_st.length; idx++) _stNames.push(stmstNames[obj.ary_st[idx]]);
                }
            }
            if (obj.rdo_sub=="ref_emp" || obj.rdo_sub=="ref_both") {
                for (var idx=0; idx<obj.ary_emp.length; idx++) _empNames.push(empmstNames[obj.ary_emp[idx]]);
            }
        }

        if (obj.rdo_all=="all_ok") {
            ee("div_"+col).innerHTML = "全職員許可";
        } else if (obj.rdo_all=="all_ng") {
            ee("div_"+col).innerHTML = "全職員不許可";
        } else {
            ee("div_"+col).innerHTML = "" +
            (_cadNames.length ? "<b>【部署】</b><br>" + _cadNames.join("<br>")+"<br>" : "") +
            (_stNames.length ? "<b>【役職】</b><br>" + _stNames.join(",&nbsp;")+"<br>" : "") +
            (_empNames.length ? "<b>【職員】</b><br>" + _empNames+"<br>" : "");
        }
        ee("hdn_"+col).value = "table_id="+obj.table_id+"\tdisp_emp="+obj.disp_emp+"\t"+"disp_st="+obj.disp_st+"\t"+"disp_cad="+obj.disp_cad;
    }
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<?=$admin_pankuzu?>



<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="extadm_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="extadm_search.php">番号検索</a></th>
    <th class="selectable w120"><a href="extadm_facility_register.php"><nobr>施設連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_class_register.php"><nobr>部門連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_document_register.php"><nobr>番号リスト登録</nobr></a></th>
    <th class="current"><a href="extadm_option.php">オプション</a></th>
    </tr></table>
</div>




<div style="width:800px">
<form action="extadm_option.php" method="post">
<input type="hidden" name="try_update" value="1">


<? //---------------------------------------------------------------------------------------------------------------- ?>
<div style="padding-top:7px"><b>番号一覧タブ画面（ユーザ画面/管理画面）全職員分を一括設定</b></div>
<? //---------------------------------------------------------------------------------------------------------------- ?>


<div style="padding-top:2px">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="200">頭文字（あ行〜わ行）を表示</th>
        <td>
            <label><input type="radio" name="initial_flg" value="" checked>変更しない</label>
            <label><input type="radio" name="initial_flg" value="t">表示する</label>
            <label><input type="radio" name="initial_flg" value="f">表示しない</label>
        </td>
    </tr>

    <tr>
        <th>施設を表示</th>
        <td>
            <label><input type="radio" name="facility_flg" value="" checked>変更しない</label>
            <label><input type="radio" name="facility_flg" value="t">表示する</label>
            <label><input type="radio" name="facility_flg" value="f">表示しない</label>
        </td>
    </tr>

    <tr>
        <th>部門を表示</th>
        <td>
            <label><input type="radio" name="class_flg" value="" checked>変更しない</label>
            <label><input type="radio" name="class_flg" value="t">表示する</label>
            <label><input type="radio" name="class_flg" value="f">表示しない</label>
        </td>
    </tr>

    <tr>
        <th>部門（職員）を表示</th>
        <td>
            <label><input type="radio" name="emptree_flg" value="" checked>変更しない</label>
            <label><input type="radio" name="emptree_flg" value="t">表示する</label>
            <label><input type="radio" name="emptree_flg" value="f">表示しない</label>
        </td>
    </tr>
</table>

</div>







<? //---------------------------------------------------------------------------------------------------------------- ?>
<div style="padding-top:12px"><b>「今月の新人」タブ画面（ユーザ画面）</b></div>
<? //---------------------------------------------------------------------------------------------------------------- ?>

<div style="padding-top:2px">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="200">「今月の新人」機能を使用</th>
        <td>
            <label><input type="radio" name="newcomer_flg" value="t"<?=($extadmoptRow["newcomer_flg"]=="t"?" checked":"")?>>する</label>
            <label><input type="radio" name="newcomer_flg" value="f"<?=($extadmoptRow["newcomer_flg"]=="f"?" checked":"")?>>しない</label>
        </td>
    </tr>
</table>

</div>


<? //---------------------------------------------------------------------------------------------------------------- ?>
<div style="padding-top:12px"><b>番号一覧タブ画面/番号検索タブ画面（ユーザ画面）</b></div>
<? //---------------------------------------------------------------------------------------------------------------- ?>
<div id="auth_change_msg" style="display:none; color:#ff0000">
	権限の変更はまだ保存されていません。「更新」ボタンを押して保存してください。
</div>


<div style="padding-top:2px">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="200" style="border-right:0; line-height:1.5">職員情報 表示列閲覧権限</th>
        <td style="padding:0; border:0">
            <table cellspacing="0" cellpadding="0" class="prop_list inner_list" width="100%" style="border-collapse:collapse">
            <tr>
                <td style="width:80px"><a href="javascript:void(0)" onclick="popupAuthSelector('photo');"><nobr>・写真</nobr></a></td>
                <td>
                    <div id="div_photo"></div>
                    <input type="hidden" name="hdn_photo" id="hdn_photo" />
                    <script type="text/javascript">createAuthHtml("photo");</script>
                </td>
            </tr>

            <tr style="border-top:1px solid #5279a5">
                <td><a href="javascript:void(0)" onclick="popupAuthSelector('class_nm');"><nobr>・所属</nobr></a></td>
                <td>
                    <div id="div_class_nm"></div>
                    <input type="hidden" name="hdn_class_nm" id="hdn_class_nm" />
                    <script type="text/javascript">createAuthHtml("class_nm");</script>
                </td>
            </tr>

            <tr>
                <td><a href="javascript:void(0)" onclick="popupAuthSelector('st_nm');"><nobr>・役職</nobr></a></td>
                <td>
                    <div id="div_st_nm"></div>
                    <input type="hidden" name="hdn_st_nm" id="hdn_st_nm" />
                    <script type="text/javascript">createAuthHtml("st_nm");</script>
                </td>
            </tr>

            <tr>
                <td><a href="javascript:void(0)" onclick="popupAuthSelector('job_nm');"><nobr>・職種</nobr></a></td>
                <td>
                    <div id="div_job_nm"></div>
                    <input type="hidden" name="hdn_job_nm" id="hdn_job_nm" />
                    <script type="text/javascript">createAuthHtml("job_nm");</script>
                </td>
            </tr>

            <tr>
                <td><a href="javascript:void(0)" onclick="popupAuthSelector('emp_ext');"><nobr>・内線番号</nobr></a></td>
                <td>
                    <div id="div_emp_ext"></div>
                    <input type="hidden" name="hdn_emp_ext" id="hdn_emp_ext" />
                    <script type="text/javascript">createAuthHtml("emp_ext");</script>
                </td>
            </tr>

            <tr>
                <td><a href="javascript:void(0)" onclick="popupAuthSelector('emp_phs');"><nobr>・<?=getPhsName()?></nobr></a></td>
                <td>
                    <div id="div_emp_phs"></div>
                    <input type="hidden" name="hdn_emp_phs" id="hdn_emp_phs" />
                    <script type="text/javascript">createAuthHtml("emp_phs");</script>
                </td>
            </tr>

            <tr>
                <td><a href="javascript:void(0)" onclick="popupAuthSelector('emp_mobile');"><nobr>・携帯端末・外線</nobr></a></td>
                <td>
                    <div id="div_emp_mobile"></div>
                    <input type="hidden" name="hdn_emp_mobile" id="hdn_emp_mobile" />
                    <script type="text/javascript">createAuthHtml("emp_mobile");</script>
                </td>
            </tr>

            <? for ($idx=1; $idx<=5; $idx++) { ?>
            <?     $row = $fieldStructAuthRows["empmst_ext".$idx]; ?>
            <?     if (!$row) continue; ?>
            <tr>
                <td><a href="javascript:void(0)" onclick="popupAuthSelector('empmst_ext<?=$idx?>');"><nobr>・<?=hh($row["field_label"])?></nobr></a></td>
                <td>
                    <div id="div_empmst_ext<?=$idx?>"></div>
                    <input type="hidden" name="hdn_empmst_ext<?=$idx?>" id="hdn_empmst_ext<?=$idx?>" />
                    <script type="text/javascript">createAuthHtml("empmst_ext<?=$idx?>");</script>
                </td>
            </tr>
            <? } ?>

            </table>
        </td>
    </tr>
</table>

</div>


<? //---------------------------------------------------------------------------------------------------------------- ?>
<div style="padding-top:12px"><b>番号検索画面（ログイン前）</b></div>
<? //---------------------------------------------------------------------------------------------------------------- ?>

<div style="padding-top:2px">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="200">職員情報 表示列閲覧権限</th>
        <td>
            <label><input type="checkbox" name="lgn_class_nm" value="1"<?=(in_array("class_nm", $lgnList)?" checked":"")?>>所属</label>
            <label><input type="checkbox" name="lgn_st_nm" value="1"<?=(in_array("st_nm", $lgnList)?" checked":"")?>>役職</label>
            <label><input type="checkbox" name="lgn_job_nm" value="1"<?=(in_array("job_nm", $lgnList)?" checked":"")?>>職種</label><br>
            <label><input type="checkbox" name="lgn_emp_ext" value="1"<?=(in_array("emp_ext", $lgnList)?" checked":"")?>>内線番号</label>
            <label><input type="checkbox" name="lgn_emp_phs" value="1"<?=(in_array("emp_phs", $lgnList)?" checked":"")?>><?=getPhsName()?></label>
            <label><input type="checkbox" name="lgn_emp_mobile" value="1"<?=(in_array("emp_mobile", $lgnList)?" checked":"")?>>携帯端末・外線</label><br>
            <? for ($idx=1; $idx<=5; $idx++) { ?>
            <?     $row = $fieldStructAuthRows["empmst_ext".$idx]; ?>
            <?     if (!$row) continue; ?>
            <label><input type="checkbox" name="lgn_empmst_ext<?=$idx?>" value="1"<?=(in_array("empmst_ext".$idx, $lgnList)?" checked":"")?>><?=hh($row["field_label"])?></label>
            <? } ?>
        </td>
    </tr>
</table>

</div>



<? //---------------------------------------------------------------------------------------------------------------- ?>
<div style="padding-top:12px"><b>全般</b></div>
<? //---------------------------------------------------------------------------------------------------------------- ?>

<div style="padding-top:2px">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="200">写真ポップアップ時</th>
        <td>
            <label><input type="checkbox" name="showPhotoProfile" value="1"<?=($showPhotoProfile?" checked":"")?>>写真下部にプロフィールを公開表示する</label>
        </td>
    </tr>
</table>

</div>






<div style="text-align:right; padding-top:2px"><input type="submit" value="更新"></div>





</form>
</div>



</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>
