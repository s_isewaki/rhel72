<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$doc_nm = $_REQUEST["doc_nm"];

//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
    // 入力チェック
    if ($doc_nm == "") $errmsg[]= '文書名が入力されていません。';
    else if (strlen($doc_nm) > 100) $errmsg[]= '文書名が長すぎます。';
    $filename = $_FILES["file"]["name"];
    if ($filename == "") $errmsg[]= '登録するファイルを選択してください。';
    else {
        $ferr = $_FILES["file"]["error"];
        if ($ferr==1 || $ferr==2) $errmsg[]= 'ファイルサイズが大きすぎます。';
        if ($ferr==3 || $ferr==4) $errmsg[]= 'アップロードに失敗しました。再度実行してください。';
    }

    if (!count($errmsg)) {
        // アクセスログ
        require_once("aclg_set.php");
        aclg_regist($session, "extension_admin_document_insert.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

        // 拡張子を取得しているが、どうにも怪しい雰囲気である。
        $doc_ext = (strpos($filename, ".")) ? preg_replace("/^.*\.([^.]*)$/", "$1", $filename) : "txt";

        // 同じ文書名で登録があれば番号リストIDを取得。拡張子の違いは無視
        $extdoc_id = (int)c2dbGetOne("select extdoc_id from extdoc where doc_nm = ".c2dbStr($doc_nm));

        // IDが無い場合は未登録。採番してインサート
        if (!$extdoc_id) {
            $extdoc_id = 1 + (int)c2dbGetOne("select max(extdoc_id) from extdoc");
            c2dbExec(
                " insert into extdoc (extdoc_id, doc_nm, doc_ext, update_time) values (".
                " ".$extdoc_id.", ".c2dbStr($doc_nm).", ".c2dbStr($doc_ext).", '".date("YmdHis")."')"
            );
        }
        // IDがある。アップデート
        else {
            c2dbExec("update extdoc set doc_ext = ".c2dbStr($doc_ext).", update_time = '".date("YmdHis")."' where extdoc_id = ".$extdoc_id);
        }

        // ファイルを保存
        if (!is_dir("extension")) mkdir("extension", 0755);
        exec("rm -f " . dirname(__FILE__) . "/extension/$extdoc_id.*");
        copy($_FILES["file"]["tmp_name"], "extension/$extdoc_id.$doc_ext");

        // 一覧画面に遷移
	    ob_clean();
        header("Location: extadm_menu.php?view_type=doc");
        die;
    }
}



//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 | 番号リスト登録</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
function setDocName(path) {
    var fname;
    if (path.indexOf('\\') != -1) {
        fname = path.replace(/^.*\\/, '');
    } else if (path.indexOf('/') != -1) {
        fname = path.replace(/^.*\//, '');
    } else {
        fname = path;
    }
    document.uploadform.doc_nm.value = fname.replace(/\.[^.]*$/, '');
}
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<?=$admin_pankuzu?>



<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="extadm_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="extadm_search.php">番号検索</a></th>
    <th class="selectable w120"><a href="extadm_facility_register.php"><nobr>施設連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_class_register.php"><nobr>部門連絡先登録</nobr></a></th>
    <th class="current w120"><a href="extadm_document_register.php"><nobr>番号リスト登録</nobr></a></th>
    <th class="selectable"><a href="extadm_option.php">オプション</a></th>
    </tr></table>
</div>



<div style="padding-top:5px; width:600px">



<form name="uploadform" action="extadm_document_register.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="try_update" value="1">



<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="140">ファイル</th>
        <td><input type="file" name="file" value="" size="50" onchange="setDocName(this.value);"></td>
    </tr>
    <tr>
        <th>文書名</th>
        <td><input type="text" name="doc_nm" value="<?=hh($doc_nm)?>" size="50" maxlength="100"><br>
            <span style="color:red">※同じ文書名のファイルが既にある場合、今回のファイルで上書きされます。</span>
        </td>
    </tr>
</table>



<div style="text-align:right; padding-top:2px">
    <span style="color:gray; padding-right:20px">※アップロードファイルは「番号一覧」画面の「内線電話・PHSリスト」に公開されます。</span>
    <input type="submit" value="登録">
</div>



</form>

</div>
</div><!-- // CLIENT_FRAME_CONTENT --></body>
</html>
