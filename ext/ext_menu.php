<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(49)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

$page = max(1, (int)$_REQUEST["page"]); // ページ、１以上
$view_type = $_REQUEST["view_type"]; // doc or fcl or akasatana  or cls or emptree
$akasatana = min(10, max(1, (int)$_REQUEST["akasatana"])); // 1から10、不正値なら1か10

$photo_flg = ($_REQUEST["photo_flg"]=="t"?"t":""); // この画面で「写真表示」ボタンを押すと「t」が渡ってくる。t以外なら空値に。あいうえおの時のみ

$dispInfo = getEmpmstExtUserColumns("");
$colspan = count($dispInfo);
$last_id = "";
foreach ($dispInfo as $field_id => $fieldInfo) $last_id = $field_id;

// アクセスログ
require_once("aclg_set.php");
aclg_regist($session, "extension_menu.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

if ($view_type=="") $view_type = "doc"; // 内線電話・PHSリストを初期表示

$cadEmpmstRows = array();
$cadCounts = array();
if ($view_type=="emptree") {
    $sql =
    " select".
    " empmst.emp_class, empmst.emp_attribute, empmst.emp_dept".
    ",empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_phs, empmst.emp_ext".
    ",empmst.emp_mobile1, empmst.emp_mobile2, empmst.emp_mobile3".
    ",spft_empmst.empmst_ext1, spft_empmst.empmst_ext2, spft_empmst.empmst_ext3, spft_empmst.empmst_ext4, spft_empmst.empmst_ext5".
    " from empmst".
    " left outer join spft_empmst on (spft_empmst.emp_id = empmst.emp_id)".
    " where exists (".
    "     select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f'".
    " )".
    " order by empmst.emp_keywd, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_id";
    $_rows = c2dbGetRows($sql);
    foreach ($_rows as $row) {
        $ec = (int)$row["emp_class"];
        $ea = (int)$row["emp_attribute"];
        $ed = (int)$row["emp_dept"];
        $cad = $ec."_".$ea."_".$ed;
        $cadEmpmstRows[$cad][] = $row;
        $cadCounts[$ec] = (int)$cadCounts[$ec] + 1;
        $cadCounts[$ec."_".$ea] = (int)$cadCounts[$ec."_".$ea] + 1;
        $cadCounts[$cad] = (int)$cadCounts[$cad] + 1;
    }
}
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 番号一覧</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
var photo_flg = "<?=$photo_flg?>";
function changePhotoDisp() {
    photo_flg = (photo_flg=="t" ? "" : "t");
    var spanAry = document.getElementsByTagName('span');
    for (var i=0; i < spanAry.length; i++) {
        var span = spanAry[i];
        var cn = span.className;
        if (cn=="photo_img") span.style.display = (photo_flg=="t" ?  "" : "none");
        else if (cn=="photo_atag") span.style.display = (photo_flg=="t" ?  "none" : "");
    }
    resizeIframeEx();
}
function changeDivision(division) {
    var cells = document.getElementsByTagName('td');
    for (var i = 0, j = cells.length; i < j; i++) {
        var cn = cells[i].className;
        if (cn!="class_col" && cn!="atrb_col" && cn!="dept_col" && cn!="kojin_col") continue;
        var display = (cells[i].className == division+'_col' ? '' : 'none');
        cells[i].style.display = display;
    }
    document.getElementById('class_anchor').className = (division=="class"?"selected":"");
    document.getElementById('atrb_anchor').className =  (division=="atrb"?"selected":"");
    document.getElementById('dept_anchor').className =  (division=="dept"?"selected":"");
    document.getElementById('kojin_anchor').className = (division=="kojin"?"selected":"");
}
function treeClicked(btn, parentPrefix, parentHier) {
    var dest = (btn.className=="plus" ? "minus" : "plus");
    btn.className = dest;
    var tbody = document.getElementById("main_tbody");
    for (var idx=0; idx<tbody.childNodes.length; idx++) {
        var tr = tbody.childNodes[idx];
        var cls = tr.className;
        if (!cls) continue;
        if (tr.id.indexOf(parentPrefix)==0) {
            var aCls = cls.split(" ");
            aCls[parentHier-1] = (dest=="plus" ? "tr_hide" : "tr_show");
            tr.className = aCls.join(" ");
        }
    }
    resizeIframeEx();
}
function showEmployeeList(head_emp_id, class_id, atrb_id, dept_id) {
    window.open('ext_class_list_from_login.php?head_emp_id='+head_emp_id+'&class_id='+class_id+'&atrb_id='+atrb_id+'&dept_id='+dept_id, 'emplist', 'width=840,height=520,scrollbars=yes');
}
function resizeIframeEx() {
    if (!parent || !parent.document || !parent.document.getElementById('floatingpage')) return;
    var cont = document.getElementById("CLIENT_FRAME_CONTENT");
    var hh = 600;
    if (cont && 600 < cont.offsetHeight) hh = cont.offsetHeight + 20;
    parent.document.getElementById('floatingpage').style.height = hh + 'px';
}
</script>
</head>
<body onload="resizeIframeEx()"><div id="CLIENT_FRAME_CONTENT">


<div class="window_title">
    <table cellspacing="0" cellpadding="0"><tr>
    <th>
        <?=$intra_pankuzu_html?>
        <a class="pankuzu2" href="ext_menu.php">番号一覧</a>
    </th>
    <? if ($EAL[SELF_ADMIN_AUTH_FLG] == "1") { ?>
        <td><a href="extadm_menu.php">管理画面へ</a></td>
    <? } ?>
    </tr></table>
</div>


<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="current"><a href="ext_menu.php"><nobr>番号一覧</nobr></a></th>
    <th class="selectable"><a href="ext_search.php"><nobr>番号検索</nobr></a></th>
    <th class="selectable"><a href="ext_number_update.php"><nobr>番号変更</nobr></a></th>
    <? if ($extadmoptRow["newcomer_flg"]!="f") { ?>
    <th class="selectable"><a href="ext_newcomer.php"><nobr>今月の新人</nobr></a></th>
    <? } ?>
    <th class="selectable"><a href="ext_option.php"><nobr>オプション</nobr></a></th>
    </tr></table>
</div>




<? //**************************************************************************************************************** ?>
<? // 一覧タイプのセレクタリンク                                                                                      ?>
<? //**************************************************************************************************************** ?>
<div style="padding-top:2px">
    <table width="100%" cellspacing="0" cellpadding="2">
    <tr height="26">
    <td><nobr>
<?

    // ログインユーザのオプション情報を取得
    $extoptRow = c2dbGetTopRow("select initial_flg, facility_flg, class_flg, emptree_flg from extopt where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID));

    //----------------------------------
    // 左側１：アップロードした文書の一覧のためのリンク
    //----------------------------------
    if ($view_type!="doc") echo '<a href="ext_menu.php?view_type=doc">';
    echo '<span title="ここに表示されるリストは管理者により登録されます。">内線電話・PHSリスト</span>';
    if ($view_type!="doc") echo '</a>';

    //----------------------------------
    // 左側２：あかさたなリンク
    //----------------------------------
    if (c2Bool($extoptRow["initial_flg"])) { // 未設定ならfalse判定となる
        $ary = array("", "あ", "か", "さ", "た", "な", "は", "ま", "や", "ら", "わ");
        for ($idx=1; $idx<=10; $idx++) {
            echo ' | ';
            if ($view_type=="akasatana" && $akasatana==$idx) {
                echo $ary[$idx]."行";
            } else {
                echo '<a href="ext_menu.php?view_type=akasatana&akasatana='.$idx.'&photo_flg='.$photo_flg.'">'.$ary[$idx].'行</a>';
            }
        }
    }

    //----------------------------------
    // 左側３：施設一覧のためのリンク
    //----------------------------------
    if (c2Bool($extoptRow["facility_flg"])) { // 未設定ならfalse判定となる
        echo ' | ';
        if ($view_type!="fcl") echo '<a href="ext_menu.php?view_type=fcl">';
        echo "施設";
        if ($view_type!="fcl") echo '</a>';
    }

    //----------------------------------
    // 左側４：部門ツリーのためのリンク
    //----------------------------------
    if (c2Bool($extoptRow["class_flg"])) { // 未設定ならfalse判定となる
        echo ' | ';
        if ($view_type!="cls") echo '<a href="ext_menu.php?view_type=cls">';
        echo "部門";
        if ($view_type!="cls") echo '</a>';
    }
    //----------------------------------
    // 左側５：部門（職員）ツリーのためのリンク
    //----------------------------------
    if (c2Bool($extoptRow["emptree_flg"])) { // 未設定ならfalse判定となる
        echo ' | ';
        if ($view_type!="emptree") echo '<a href="ext_menu.php?view_type=emptree">';
        echo "部門（職員）";
        if ($view_type!="emptree") echo '</a>';
    }

    echo '</nobr></td>';


    //----------------------------------
    // 右側１a：写真表示切替ボタン：あかさたなの場合のみ
    //----------------------------------
    if ($view_type=="akasatana" && $dispInfo["photo"]) {
        echo '<td>';
        echo '<button type="button"';
        echo ' onclick="location.href=\'ext_menu.php?view_type='.$view_type.'&akasatana='.$akasatana;
        echo '&photo_flg='.($photo_flg=="t"?"":"t").'&page='.$page.'\'"><nobr>'.($photo_flg=="t"?"写真非表示":"写真表示").'</nobr></button>';
        echo("</td>");
    }
    //----------------------------------
    // 右側１b：写真表示切替ボタン：部門（職員）の場合
    //----------------------------------
    if ($view_type=="emptree" && $dispInfo["photo"]) {
        echo '<td>';
        echo '<button type="button" onclick="changePhotoDisp();"><nobr>'.($photo_flg=="t"?"写真非表示":"写真表示").'</nobr></button>';
        echo("</td>");
    }
    //----------------------------------
    // 右側２：部署切替ぱんくず：これも「あかさたな」の場合のみ
    //----------------------------------
    if ($view_type=="akasatana") {
        echo '<td align="right"><nobr>';
        echo '<a id="class_anchor" href="javascript:void(0);" onclick="changeDivision(\'class\');">'.hh($classnameRow["class_nm"]).'</a> &gt; ';
        echo '<a id="atrb_anchor" href="javascript:void(0);" onclick="changeDivision(\'atrb\');">'.hh($classnameRow["atrb_nm"]).'</a> &gt; ';
        echo '<a id="dept_anchor" href="javascript:void(0);" onclick="changeDivision(\'dept\');">'.hh($classnameRow["dept_nm"]).'</a> &gt; ';
        echo '<a id="kojin_anchor" href="javascript:void(0);" onclick="changeDivision(\'kojin\');" class="selected">本人</a>';
        echo "</nobr></td>";
    }
?>
    </tr></table>
</div>


<? //**************************************************************************************************************** ?>
<? // 一覧メイン（一覧ヘッダ）                                                                                        ?>
<? //**************************************************************************************************************** ?>
<div style="padding-top:2px;">
<table width="100%" cellspacing="0" cellpadding="2" class="list">
    <tr height="22" bgcolor="#f6f9ff">
    <? if ($view_type=="fcl") { ?>

        <td width="46%">施設名</td>
        <td width="20%">電話番号（代表）</td>
        <td width="20%">FAX</td>
        <td width="9%" align="center">番号リスト</td>
    <? }  else if ($view_type=="doc") { ?>

        <td width="44%">文書名</td>
        <td width="34%">更新日時</td>
        <td width="7%" align="center">番号リスト</td>
    <? }  else if ($view_type=="cls") { ?>
        <td width="24%">部門名</td>
        <td width="8%">内線番号</td>
        <td width="17%">外線番号</td>
        <td width="17%">FAX</td>
        <td width="13%">所属長</td>
        <td width="21%">機能</td>
    <? }  else if ($view_type=="emptree") { ?>
        <td><nobr>部署＞職員氏名</nobr></td>
        <?
        if (!count($dispInfo)) echo '<td width="auto"></td>';
        foreach ($dispInfo as $field_id => $fieldInfo) {
            if ($field_id=="photo") echo '<td width="'.($field_id!=$last_id?'60px':'auto').'"><nobr>写真</nobr></td>';
            if ($field_id=="emp_phs") echo '<td width="'.($field_id!=$last_id?'60px':'auto').'"><nobr>'.getPhsName().'</nobr></td>';
            if ($field_id=="emp_ext") echo '<td width="'.($field_id!=$last_id?'60px':'auto').'"><nobr>内線番号</nobr></td>';
            if ($field_id=="emp_mobile") echo '<td width="'.($field_id!=$last_id?'100px':'auto').'"><nobr>携帯端末・外線</nobr></td>';
            if ($field_id=="empmst_ext1") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext2") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext3") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext4") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext5") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
        }
        ?>
    <? } else { ?>
        <td width="150px"><nobr>職員氏名</nobr></td>
        <?
        foreach ($dispInfo as $field_id => $fieldInfo) {
            if ($field_id=="photo") echo '<td width="'.($field_id!=$last_id?'60px':'auto').'"><nobr>写真</nobr></td>';
            if ($field_id=="class_nm") echo '<td width="'.($field_id!=$last_id?'100px':'auto').'"><nobr>所属'.hh($classnameRow["class_nm"]).'</nobr></td>';
            if ($field_id=="job_nm") echo '<td width="'.($field_id!=$last_id?'80px':'auto').'"><nobr>職種</nobr></td>';
            if ($field_id=="st_nm") echo '<td width="'.($field_id!=$last_id?'80px':'auto').'"><nobr>役職</nobr></td>';
            if ($field_id=="emp_phs") echo '<td width="'.($field_id!=$last_id?'60px':'auto').'"><nobr>'.getPhsName().'</nobr></td>';
            if ($field_id=="emp_ext") echo '<td width="'.($field_id!=$last_id?'60px':'auto').'" class="kojin_col"><nobr>内線番号</nobr></td>';
            if ($field_id=="emp_mobile") echo '<td width="'.($field_id!=$last_id?'100px':'auto').'" class="kojin_col"><nobr>携帯端末・外線</nobr></td>';
            if ($field_id=="empmst_ext1") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext2") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext3") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext4") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext5") echo '<td width="auto"><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
        }
        echo '<td class="class_col" style="display:none;"><nobr>内線番号</nobr></td>';
        echo '<td class="class_col" style="display:none;"><nobr>外線番号</nobr></td>';
        echo '<td class="atrb_col" style="display:none;"><nobr>内線番号</nobr></td>';
        echo '<td class="atrb_col" style="display:none;"><nobr>外線番号</nobr></td>';
        echo '<td class="dept_col" style="display:none;"><nobr>内線番号</nobr></td>';
        echo '<td class="dept_col" style="display:none;"><nobr>外線番号</nobr></td>';
        ?>
    <? } ?>
    </tr>
    <tbody id="main_tbody">

<? //**************************************************************************************************************** ?>
<? // 一覧メイン（SQL作成）                                                                                           ?>
<? //**************************************************************************************************************** ?>
<?
    $offset_limit = " offset ".(($page-1)*ROW_LIMIT)." limit ".ROW_LIMIT;
    if ($view_type=="fcl") {
        $sql = "select extfcl_id, fcl_nm, fcl_tel1, fcl_tel2, fcl_tel3, fcl_fax1, fcl_fax2, fcl_fax3 from extfcl order by extfcl_id".$offset_limit;
    } else if ($view_type=="doc") {
        $sql = "select extdoc_id, doc_nm, doc_ext, update_time from extdoc order by extdoc_id".$offset_limit;
    } else if ($view_type=="cls") {
        $sql =
        " select main.*".
        ",empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as head_emp_nm".
        " from (".
        "     select classmst.class_id, 0 as atrb_id, 0 as dept_id, classmst.class_nm as master_nm".
        "    ,1 as hier, classmst.order_no as class_order, 0 as atrb_order, 0 as dept_order".
        "    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
        "     from classmst".
        "     left join extcls on ( extcls.class_id = classmst.class_id )".
        "     where extcls.atrb_id is null".
        "     and extcls.dept_id is null".
        "     and classmst.class_del_flg = 'f'".
        " union".
        "     select atrbmst.class_id, atrbmst.atrb_id, 0 as dept_id, atrbmst.atrb_nm as master_nm".
        "    ,2 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, 0 as dept_order".
        "    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
        "     from atrbmst".
        "     inner join classmst on (classmst.class_id = atrbmst.class_id )".
        "     left join extcls on ( extcls.class_id = atrbmst.class_id and extcls.atrb_id = atrbmst.atrb_id )".
        "     where extcls.dept_id is null".
        "     and atrbmst.atrb_del_flg = 'f'".
        " union".
        "     select atrbmst.class_id, deptmst.atrb_id, deptmst.dept_id, deptmst.dept_nm as master_nm".
        "    ,3 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, deptmst.order_no as dept_order".
        "    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
        "     from deptmst".
        "     inner join atrbmst on (atrbmst.atrb_id = deptmst.atrb_id )".
        "     inner join classmst on (classmst.class_id = atrbmst.class_id )".
        "         left join extcls on ( extcls.atrb_id = deptmst.atrb_id and extcls.dept_id = deptmst.dept_id )".
        "     where deptmst.dept_del_flg = 'f'".
        " ) main".
        " left join (".
        "     select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm from empmst".
        "     inner join authmst on (authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')".
        " ) empmst on ( empmst.emp_id = main.head_emp_id )".
        " order by class_order, atrb_order, dept_order";
    } else if ($view_type=="emptree") {
        $sql =
        " select main.*".
        ",empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as head_emp_nm".
        " from (".
        "     select classmst.class_id, 0 as atrb_id, 0 as dept_id, classmst.class_nm as master_nm".
        "    ,1 as hier, classmst.order_no as class_order, 0 as atrb_order, 0 as dept_order".
        "    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
        "     from classmst".
        "     left join extcls on ( extcls.class_id = classmst.class_id )".
        "     where extcls.atrb_id is null".
        "     and extcls.dept_id is null".
        "     and classmst.class_del_flg = 'f'".
        " union".
        "     select atrbmst.class_id, atrbmst.atrb_id, 0 as dept_id, atrbmst.atrb_nm as master_nm".
        "    ,2 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, 0 as dept_order".
        "    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
        "     from atrbmst".
        "     inner join classmst on (classmst.class_id = atrbmst.class_id )".
        "     left join extcls on ( extcls.class_id = atrbmst.class_id and extcls.atrb_id = atrbmst.atrb_id )".
        "     where extcls.dept_id is null".
        "     and atrbmst.atrb_del_flg = 'f'".
        " union".
        "     select atrbmst.class_id, deptmst.atrb_id, deptmst.dept_id, deptmst.dept_nm as master_nm".
        "    ,3 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, deptmst.order_no as dept_order".
        "    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
        "     from deptmst".
        "     inner join atrbmst on (atrbmst.atrb_id = deptmst.atrb_id )".
        "     inner join classmst on (classmst.class_id = atrbmst.class_id )".
        "         left join extcls on ( extcls.atrb_id = deptmst.atrb_id and extcls.dept_id = deptmst.dept_id )".
        "     where deptmst.dept_del_flg = 'f'".
        " ) main".
        " left join (".
        "     select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm from empmst".
        "     inner join authmst on (authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')".
        " ) empmst on ( empmst.emp_id = main.head_emp_id )".
        " order by class_order, atrb_order, dept_order";
    } else { // akasatana
        $sql =
        " select".
        " empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_phs, empmst.emp_ext".
        ",empmst.emp_mobile1, empmst.emp_mobile2, empmst.emp_mobile3".
        ",classmst.class_nm, jobmst.job_nm, stmst.st_nm".
        ",extclass.extension as class_ext, extclass.tel1 as class_tel1, extclass.tel2 as class_tel2, extclass.tel3 as class_tel3".
        ",extatrb.extension as atrb_ext, extatrb.tel1 as atrb_tel1, extatrb.tel2 as atrb_tel2, extatrb.tel3 as atrb_tel3".
        ",extdept.extension as dept_ext, extdept.tel1 as dept_tel1, extdept.tel2 as dept_tel2, extdept.tel3 as dept_tel3".
        ",spft_empmst.empmst_ext1, spft_empmst.empmst_ext2, spft_empmst.empmst_ext3, spft_empmst.empmst_ext4, spft_empmst.empmst_ext5".
        " from empmst".
        " inner join classmst on ( empmst.emp_class = classmst.class_id )".
        " inner join jobmst on ( empmst.emp_job = jobmst.job_id )".
        " inner join stmst on ( empmst.emp_st = stmst.st_id )".
        " left outer join spft_empmst on ( spft_empmst.emp_id = empmst.emp_id )".
        " left join (".
        "     select * from extcls where atrb_id is null and dept_id is null".
        " ) extclass on ( empmst.emp_class = extclass.class_id )".
        " left join (".
        "     select * from extcls where atrb_id is not null and dept_id is null".
        " ) extatrb on ( empmst.emp_class = extatrb.class_id and empmst.emp_attribute = extatrb.atrb_id )".
        " left join (".
        "     select * from extcls where atrb_id is not null and dept_id is not null".
        " ) extdept on ( empmst.emp_class = extdept.class_id and empmst.emp_attribute = extdept.atrb_id and empmst.emp_dept = extdept.dept_id )".
        " where exists (".
        "     select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f'".
        " )".
        and_EmpKeywdSql($akasatana).
        " order by empmst.emp_keywd, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_id".$offset_limit;
    }
    $rows = c2dbGetRows($sql);
?>



<? //**************************************************************************************************************** ?>
<? // 一覧メイン（データ出力）                                                                                        ?>
<? //**************************************************************************************************************** ?>
<?
    $last_class_id = "";
    $last_atrb_id = "";
    $last_dept_id = "";
    $last_hier = 0;
    foreach ($rows as $row) {
        if ($view_type=="fcl") {
            echo '<tr height="26">';
            $extfcl_id = $row["extfcl_id"];
            $paths = glob('extension/fcl/'.$extfcl_id.'.*');
            $path = (count($paths) > 0) ? strrchr($paths[0], "/") : "";

            echo '<td>'.hh($row["fcl_nm"]).'</td>';
            echo '<td>'.hh(concatTel($row["fcl_tel1"], $row["fcl_tel2"], $row["fcl_tel3"])).'</td>';
            echo '<td>'.hh(concatTel($row["fcl_fax1"], $row["fcl_fax2"], $row["fcl_fax3"])).'</td>';
            echo '<td align="center">';
            if ($path!="") {
                if ($is_realname_download) $path = "/".$row["fcl_nm"].strrchr($paths[0],".");
                echo '<a href="./download/'.$extfcl_id.'/fcl/'.C2_SESSION_ID.$path.'">参照</a>';
            }
            echo '</td>';
            echo "</tr>\n";
        }
        else if ($view_type=="doc") {
            $paths = glob('extension/'.$row["extdoc_id"].'.*');
            $path = (count($paths) > 0) ? strrchr($paths[0],"/") : "";
            echo '<tr height="26">';

            echo '<td>'.hh($row["doc_nm"]).'</td>';
            echo '<td>'.preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $row["update_time"]).'</td>';
            echo '<td align="center">';
            if ($path!="") {
                if ($is_realname_download) $path = "/".$row["doc_nm"].strrchr($path, ".");
                echo '<a href="./download/'.$row["extdoc_id"].'/doc/'.C2_SESSION_ID.$path.'">参照</a>';
            }
            echo '</td>';
            echo "</tr>\n";
        }
        else if ($view_type=="cls") {
            $hier = $row["hier"];
            if ($hier==$last_hier) {
                if ($hier==1) {
                    echo '<tr id="tr'.$last_class_id.'_0" class="tr_hide">';
                    echo '<td colspan="6" style="padding-left:45px;">（登録なし）</td>';
                    echo "</tr>\n";
                }
                if ($hier==2) {
                    echo '<tr id="tr'.$last_class_id.'_'.$last_atrb_id.'_0" class="tr_hide tr_hide">';
                    echo '<td colspan="6" style="padding-left:70px;">（登録なし）</td>';
                    echo "</tr>\n";
                }
            }

            $pid = "";
            if ($hier==1) echo '<tr>';
            if ($hier==2) { $pid = $last_class_id.'_'.$row["atrb_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide">'; }
            if ($hier==3) { $pid = $last_class_id.'_'.$last_atrb_id."_".$row["dept_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide tr_hide">'; }

            echo '<td class="hier'.$hier.'"><nobr>';
            if ($hier==1) echo '<button type="button" class="plus" onclick="treeClicked(this, \'tr'.$row["class_id"].'_\', '.$hier.')"></button>';
            if ($hier==2) echo '<button type="button" class="plus" onclick="treeClicked(this, \'tr'.$last_class_id."_".$row["atrb_id"].'_\', '.$hier.')"></button>';
            echo hh($row["master_nm"]).'</nobr></td>';

            echo '<td>'.hh($row["extension"]).'</td>';
            echo '<td>'.concatTel($row["tel1"], $row["tel2"], $row["tel3"]).'</td>';
            echo '<td>'.concatTel($row["fax1"], $row["fax2"], $row["fax3"]).'</td>';
            echo '<td><a href="javascript:void(0);" onclick="showEmployeeList(\''.$row["head_emp_id"].'\', '.$row["class_id"].', '.$row["atrb_id"].', '.$row["dept_id"].');">';
            echo hh($row["head_emp_nm"]).'</a></td>';
            echo '<td>'.hh($row["note"]).'</td>';
            echo "</tr>\n";
            $last_hier = $hier;
            if ($hier==1) $last_class_id = $row["class_id"];
            if ($hier==2) $last_atrb_id = $row["atrb_id"];
        }
        else if ($view_type=="emptree") {
            $hier = $row["hier"];
            $pid = $row["class_id"];
            if ($hier==1) echo '<tr>';
            if ($hier==2) { $pid = $last_class_id.'_'.$row["atrb_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide">'; }
            if ($hier==3) { $pid = $last_class_id.'_'.$last_atrb_id."_".$row["dept_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide tr_hide">'; }

            echo '<td class="hier'.$hier.'" style="width:5%"><nobr>';
            if ($hier==1) echo '<button type="button" class="plus" onclick="treeClicked(this, \'tr'.$row["class_id"].'_\', '.$hier.')"></button>';
            if ($hier==2) echo '<button type="button" class="plus" onclick="treeClicked(this, \'tr'.$last_class_id."_".$row["atrb_id"].'_\', '.$hier.')"></button>';
            if ($hier==3) echo '<button type="button" class="plus" onclick="treeClicked(this, \'tr'.$last_class_id."_".$last_atrb_id."_".$row["dept_id"].'_\', '.$hier.')"></button>';
            echo hh($row["master_nm"]).'（'.(int)$cadCounts[$pid].'）</nobr></td>';

            echo '<td colspan="'.$colspan.'" class="busyo">';
            if ($row["extension"]) echo '<span><b>内</b>'.hh($row["extension"]).'</span>';
            $tel = concatTel($row["tel1"], $row["tel2"], $row["tel3"]);
            if ($tel) echo '<span><b>Tel</b>'.$tel.'</span>';
            $fax = concatTel($row["fax1"], $row["fax2"], $row["fax3"]);
            if ($fax) echo '<span><b>Fax</b>'.$fax.'</span>';
            $nm = $row["head_emp_nm"];
            if ($nm) {
                echo '<span><b>長</b><a href="javascript:void(0);"';
                echo ' onclick="showEmployeeList(\''.$row["head_emp_id"].'\', '.$row["class_id"].', '.$row["atrb_id"].', '.$row["dept_id"].');">';
                echo hh($nm).'</a></span>';
            }
            if ($row["note"]) echo '<span><b>機能</b>'.hh($row["note"]).'</span>';
            echo "</tr>\n";
            $last_hier = $hier;
            if ($hier==1) $last_class_id = $row["class_id"];
            if ($hier==2) $last_atrb_id = $row["atrb_id"];
            if ($hier==3) $last_dept_id = $row["dept_id"];

            $cad = (int)$last_class_id."_".(int)$last_atrb_id."_".(int)$last_dept_id;
            $empmstRows = $cadEmpmstRows[$cad];
            if (!is_array($empmstRows)) $empmstRows = array();
            foreach ($empmstRows as $eRow) {
                $photoInfo = getPhotoInfo($eRow["emp_id"]);
                echo '<tr id="tr'.$cad."_".$eRow["emp_id"].'" class="tr_hide tr_hide tr_hide">';
                echo '<td style="padding-left:72px;"><nobr>'.hh($eRow["emp_lt_nm"]." ".$eRow["emp_ft_nm"]).'</nobr></td>';
                if (!count($dispInfo)) echo '<td></td>';
                foreach ($dispInfo as $field_id => $fieldInfo) { // field_labelは拡張empmst項目のみ
                    if ($field_id=="photo") {
                        echo '<td title="写真">';
                        echo '<span class="photo_img"'.($photo_on?'':' style="display:none"').'>'.$photoInfo["image_viewer_imgtag"].'</span>';
                        echo '<span class="photo_atag"'.($photo_on?' style="display:none"':'').'>'.$photoInfo["image_viewer_atag"].'</span>';
                        echo '</td>';
                    }
                    if ($field_id=="emp_phs") echo '<td title="院内PHS">'.hh($eRow["emp_phs"]).'</td>';
                    if ($field_id=="emp_ext") echo '<td title="内線番号" class="kojin_col">'.hh($eRow["emp_ext"]).'</td>';
                    if ($field_id=="emp_mobile") echo '<td title="携帯端末・外線" class="kojin_col">'.hh(concatTel($eRow["emp_mobile1"], $eRow["emp_mobile2"], $eRow["emp_mobile3"])).'</td>';
                    if ($field_id=="empmst_ext1") echo '<td title="'.hh($fieldInfo["label"]).'">'.hh($eRow["empmst_ext1"]).'</td>';
                    if ($field_id=="empmst_ext2") echo '<td title="'.hh($fieldInfo["label"]).'">'.hh($eRow["empmst_ext2"]).'</td>';
                    if ($field_id=="empmst_ext3") echo '<td title="'.hh($fieldInfo["label"]).'">'.hh($eRow["empmst_ext3"]).'</td>';
                    if ($field_id=="empmst_ext4") echo '<td title="'.hh($fieldInfo["label"]).'">'.hh($eRow["empmst_ext4"]).'</td>';
                    if ($field_id=="empmst_ext5") echo '<td title="'.hh($fieldInfo["label"]).'">'.hh($eRow["empmst_ext5"]).'</td>';
                }
                echo "</tr>\n";
            }
        }
        else {
            $photoInfo = getPhotoInfo($row["emp_id"]);
            echo '<tr height="22">';

            echo '<td><nobr>'.hh($row["emp_lt_nm"]." ".$row["emp_ft_nm"]).'</nobr></td>';
            foreach ($dispInfo as $field_id => $field_label) { // field_labelは拡張empmst項目のみ
                if ($field_id=="photo") echo '<td>'.($photo_flg=="t" ? $photoInfo["image_viewer_imgtag"] : $photoInfo["image_viewer_atag"]).'</td>';
                if ($field_id=="class_nm") echo '<td><nobr>'.hh($row["class_nm"]).'</nobr></td>';
                if ($field_id=="job_nm") echo '<td><nobr>'.hh($row["job_nm"]).'</nobr></td>';
                if ($field_id=="st_nm") echo '<td><nobr>'.hh($row["st_nm"]).'</nobr></td>';
                if ($field_id=="emp_phs") echo '<td>'.hh($row["emp_phs"]).'</td>';
                if ($field_id=="emp_ext") echo '<td class="kojin_col">'.hh($row["emp_ext"]).'</td>';
                if ($field_id=="emp_mobile") echo '<td class="kojin_col">'.hh(concatTel($row["emp_mobile1"], $row["emp_mobile2"], $row["emp_mobile3"])).'</td>';
                if ($field_id=="empmst_ext1") echo '<td>'.hh($row["empmst_ext1"]).'</td>';
                if ($field_id=="empmst_ext2") echo '<td>'.hh($row["empmst_ext2"]).'</td>';
                if ($field_id=="empmst_ext3") echo '<td>'.hh($row["empmst_ext3"]).'</td>';
                if ($field_id=="empmst_ext4") echo '<td>'.hh($row["empmst_ext4"]).'</td>';
                if ($field_id=="empmst_ext5") echo '<td>'.hh($row["empmst_ext5"]).'</td>';
            }
            echo '<td class="class_col" style="display:none;">'.hh($row["class_ext"]).'</td>';
            echo '<td class="class_col" style="display:none;">'.hh(concatTel($row["class_tel1"], $row["class_tel2"], $row["class_tel3"])).'</td>';
            echo '<td class="atrb_col" style="display:none;">'.hh($row["atrb_ext"]).'</td>';
            echo '<td class="atrb_col" style="display:none;">'.hh(concatTel($row["atrb_tel1"], $row["atrb_tel2"], $row["atrb_tel3"])).'</td>';
            echo '<td class="dept_col" style="display:none;">'.hh($row["dept_ext"]).'</td>';
            echo '<td class="dept_col" style="display:none;">'.hh(concatTel($row["dept_tel1"], $row["dept_tel2"], $row["dept_tel3"])).'</td>';
            echo "</tr>\n";
        }
    }
?>

</tbody>
</table>
</div>




<? //**************************************************************************************************************** ?>
<? // 画面下ぱんくず                                                                                                  ?>
<? //**************************************************************************************************************** ?>
<?
    $sql = "";
    if ($view_type=="fcl") $sql = "select count(*) from extfcl";
    if ($view_type=="doc") $sql = "select count(*) from extdoc";
    if ($view_type=="akasatana") {
        $sql =
        " select count(*) from empmst".
        " where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')".
        " ".and_EmpKeywdSql($akasatana);
    }
    if ($sql) { // cls(部署ツリー)の場合はページャなし
        $ttl_count = (int)c2dbGetOne($sql);
        if ($ttl_count <= ROW_LIMIT) $page = 1; // 行が少ないのでページング不要
        if ($ttl_count > ROW_LIMIT) {
            echo '<div style="text-align:right; padding-top:2px">ページ：';
            for ($p = 1, $max = ceil($ttl_count / ROW_LIMIT); $p <= $max; $p++) {
                if ($p == $page) {
                    echo($p."&nbsp;");
                } else {
                    echo '<a href="ext_menu.php?view_type='.$view_type.'&akasatana='.$akasatana;
                    echo '&page='.$p.'&photo_flg='.$photo_flg.'">'.$p.'</a>&nbsp;';
                }
            }
            echo '</div>';
        }
    }
?>




</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>
