<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$req = $_REQUEST; // 長いので短い変数にしたい


//―――――――――――――――――――――――――――――――――――――――――――――――――
// 新規登録の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
//    $extfclRow = $_REQUEST;

    // アクセスログ
    require_once("aclg_set.php");
    aclg_regist($session, "extension_admin_facility_insert.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_FILES);

    // 入力チェック
    if ($req["fcl_nm"] == "") $errmsg[]= '施設名が入力されていません。';
    if (strlen($req["fcl_nm"]) > 60) $errmsg[]= '施設名が長すぎます。';
    if (strlen($req["fcl_tel1"]) > 6) $errmsg[]= '電話番号（代表）（1）が長すぎます。';
    if (strlen($req["fcl_tel2"]) > 6) $errmsg[]= '電話番号（代表）（2）が長すぎます。';
    if (strlen($req["fcl_tel3"]) > 6) $errmsg[]= '電話番号（代表）（3）が長すぎます。';
    if (!allOrNothing($req["fcl_tel1"], $req["fcl_tel2"], $req["fcl_tel3"])) $errmsg[]= '電話番号（代表）を正しく入力してください。';
    if (strlen($req["fcl_fax1"]) > 6) $errmsg[]= 'FAX（1）が長すぎます。';
    if (strlen($req["fcl_fax2"]) > 6) $errmsg[]= 'FAX（2）が長すぎます。';
    if (strlen($req["fcl_fax3"]) > 6) $errmsg[]= 'FAX（3）が長すぎます。';
    if (!allOrNothing($req["fcl_fax1"], $req["fcl_fax2"], $req["fcl_fax3"])) $errmsg[]= 'FAXを正しく入力してください。';

    $filename = $_FILES["file"]["name"];
    if ($filename != "") {
        $ferr = $_FILES["file"]["error"];
        if ($ferr==1 || $ferr==2) $errmsg[]= 'ファイルサイズが大きすぎます。';
        if ($ferr==3 || $ferr==4) $errmsg[]= 'アップロードに失敗しました。再度実行してください。';
    }

    if (!count($errmsg)) {
        // 施設連絡先IDを採番
        $extfcl_id = 1 + (int)c2dbGetOne("select max(extfcl_id) from extfcl");

        // 施設連絡先情報を登録
        $sql =
        " insert into extfcl (".
        "     extfcl_id, fcl_nm, fcl_tel1, fcl_tel2, fcl_tel3, fcl_fax1, fcl_fax2, fcl_fax3".
        " ) values (".
        " ".$extfcl_id.
        ",".c2dbStr($req["fcl_nm"]).
        ",".c2dbStr($req["fcl_tel1"]).
        ",".c2dbStr($req["fcl_tel2"]).
        ",".c2dbStr($req["fcl_tel3"]).
        ",".c2dbStr($req["fcl_fax1"]).
        ",".c2dbStr($req["fcl_fax2"]).
        ",".c2dbStr($req["fcl_fax3"]).
        ")";
        c2dbExec($sql);

        // ファイルを保存
        if ($filename != "") {
            $doc_ext = (strpos($filename, ".")) ? preg_replace("/^.*\.([^.]*)$/", "$1", $filename) : "txt"; // 拡張子の設定
            if (!is_dir("extension")) mkdir("extension", 0755);
            if (!is_dir("extension/fcl")) mkdir("extension/fcl", 0755);
            exec("rm -f " . dirname(__FILE__) . "/extension/fcl/$extfcl_id.*");
            copy($_FILES["file"]["tmp_name"], "extension/fcl/$extfcl_id.$doc_ext");
        }

        // 一覧画面に遷移
	    ob_clean();
        header("Location: extadm_menu.php?view_type=fcl");
        die;
    }
}

//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 | 施設連絡先登録</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<?=$admin_pankuzu?>



<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="extadm_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="extadm_search.php">番号検索</a></th>
    <th class="current w120"><a href="extadm_facility_register.php"><nobr>施設連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_class_register.php"><nobr>部門連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_document_register.php"><nobr>番号リスト登録</nobr></a></th>
    <th class="selectable"><a href="extadm_option.php">オプション</a></th>
    </tr></table>
</div>



<div style="padding-top:5px; width:600px">



<form method="post" enctype="multipart/form-data" action="extadm_facility_register.php">
<input type="hidden" name="try_update" value="1">


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="140">施設名</th>
        <td><input type="text" name="fcl_nm" value="<?=hh($req["fcl_nm"])?>" size="40" maxlength="60" style="ime-mode:active;"></td>
    </tr>
    <tr>
        <th>電話番号（代表）</th>
        <td>
            <input type="text" name="fcl_tel1" value="<?=hh($req["fcl_tel1"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_tel2" value="<?=hh($req["fcl_tel2"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_tel3" value="<?=hh($req["fcl_tel3"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
        </td>
    </tr>
    <tr>
        <th>FAX</th>
        <td>
            <input type="text" name="fcl_fax1" value="<?=hh($req["fcl_fax1"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_fax2" value="<?=hh($req["fcl_fax2"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_fax3" value="<?=hh($req["fcl_fax3"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
        </td>
    </tr>
    <tr>
        <th>番号リスト</th>
        <td><input type="file" name="file" value="" size="50"></td>
    </tr>
</table>

<div style="text-align:right; padding-top:2px">
    <span style="color:gray; padding-right:20px">※アップロードファイルは「番号一覧」画面の「施設」リンクを押した画面に公開されます。</span>
    <input type="submit" value="登録">
</div>


</form>


</div>
</div><!-- // CLIENT_FRAME_CONTENT --></body>
</html>
