<?
//--------------------------------------------------------------------------------------------------
// 職員プロファイル画像のポップアップ表示ダイアログ画面
//--------------------------------------------------------------------------------------------------
require_once(dirname(dirname(__FILE__))."/class/Cmx/Core2/C2FW.php"); // 読込みと同時にセッションチェック

$emp_id = $_REQUEST["emp_id"];
$isize = $_REQUEST["isize"];

$emp_nm = "";
$emp_profile = "";
$opt_style = "";
if (c2dbGetSystemConfig("ext.showPhotoProfile")) {
	$emp_nm = c2dbGetOne("select emp_lt_nm || ' ' || emp_ft_nm as emp_nm from empmst where emp_id = ".c2dbStr($emp_id));
	$emp_profile = c2dbGetOne("select emp_profile as emp_nm from empmst where emp_id = ".c2dbStr($emp_id));
	if ($emp_profile) {
		$opt_style = 'style="height:100%; background:url(../spf/img/bk_emp.gif) repeat-x 0 bottom"';
	}
}

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML", $opt_style);
c2env::echoStaticHtmlTag("HTML5_HEAD_META");
?>
<title>CoMedix 画像参照</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="margin:5px; padding:0; font-family:メイリオ">

<div style="text-align:right"><input type="button" value="閉じる" onclick="window.close();"></div>

<? if ($emp_nm) { ?>
	<div style="text-align:center; font-size:20px">
		<?=hh($emp_nm)?><span style="font-size:18px"> さん</span>
	</div>
<? } ?>


<div style="text-align:center">
	<img id="mainimg" src="../profile/<?=$emp_id?>.jpg" alt="" style="padding:0; margin:0; <?=$isize;?>" />

	<?if ($emp_profile){?>
		<div style="font-size:14px; width:80%; text-align:center; margin:0 auto; padding-top:10px">
			<?=hh($emp_profile)?>
		</div>
	<? } ?>

</div>



</body>
</html>
