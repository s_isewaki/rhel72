<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$class_id = (int)$_REQUEST["class_id"];
$element_id = $_REQUEST["element_id"];

$classmstRows = c2dbGetRows("select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no");

// イニシャルのデフォルトは「あ行」
$akasatana = min(10, max(1, (int)$_REQUEST["akasatana"]));

//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 所属長設定</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script language="javascript">
    function classOnChange(class_id) {
        location.href = 'extadm_class_head.php?class_id='.concat(class_id).concat('&element_id=<? echo($element_id); ?>');
    }
    function setEmployeeData(emp_id, emp_nm) {
        opener.document.mainform.elements['lbl_head_nm_<?=$element_id?>'].value = emp_nm;
        opener.document.mainform.elements['head_<?=$element_id?>'].value = emp_id;
        opener.document.mainform.elements['head_nm_<?=$element_id?>'].value = emp_nm;
        window.close();
    }
</script>
</head>
<body style="margin:5px;"><div id="CLIENT_FRAME_CONTENT">


<table class="dialog_title" cellspacing="0" cellpadding="0">
    <tr><th>所属長設定</th><td class="closebtn"><a href="javascript:void(0);" onclick="window.close();"></a></td></tr>
</table>

<form name="frm" method="get" action="extadm_class_head.php">
<input type="hidden" name="akasatana" value="<?=$akasatana?>" />
<input type="hidden" name="element_id" value="<?=$element_id?>" />






<div style="padding-top:2px">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td style="padding:0">
            <select name="class_id" onchange="document.frm.submit();">
            <? foreach ($classmstRows as $row) { ?>
                <option value="<?=$row["class_id"]?>"<?=($row["class_id"]==$class_id ? " selected":"")?>><?=hh($row["class_nm"])?></option>
            <? } ?>
            </select>
        </td>
        <td style="padding:0" align="right"><input type="button" value="クリア" onclick="setEmployeeData('', '');"></td>
    </tr>
    </table>
</div>





<div style="padding-top:2px">
<?
    $ary = array("", "あ", "か", "さ", "た", "な", "は", "ま", "や", "ら", "わ");
    for ($idx=1; $idx<=10; $idx++) {
        if ($akasatana != $idx) {
            echo '<a href="javascript:void(0)" onclick="document.frm.akasatana.value='.$idx.';document.frm.submit(); return false;">'.$ary[$idx]."行</a>\n";
        } else {
            echo $ary[$idx]."行\n";
        }
    }
?>
</div>


</form>




<div style="padding-top:10px">
<?
    $sql =
    " select emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_nm".
    " from empmst".
    " where (".
    "     (emp_class = $class_id)".
    "     or (".
    "         exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $class_id)".
    "     )".
    " ) ".
    " and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')".
    and_EmpKeywdSql($akasatana).
    " order by emp_keywd, emp_kn_lt_nm, emp_kn_ft_nm, emp_id";
    $rows = c2dbGetRows($sql);
    foreach ($rows as $row) {
        echo '<a href="javascript:void(0);"';
        echo ' onclick="setEmployeeData(\''.$row["emp_id"].'\', \''.c2Js($row["emp_nm"]).'\');">';
        echo hh($row["emp_nm"])."</a><br>\n";
    }
?>
</div>


</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>
