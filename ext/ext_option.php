<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(49)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


// ログインユーザのオプション情報を取得
$extoptRow = c2dbGetTopRow("select initial_flg, facility_flg, class_flg, emptree_flg from extopt where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID));


//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
    $extoptRow = $_REQUEST;
    c2dbExec("delete from extopt where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID)); // いったんDELETE
    c2dbExec( // インサート
	    " insert into extopt (emp_id, initial_flg, facility_flg, class_flg, emptree_flg) values (".
	    " ".c2dbStr(C2_LOGIN_EMP_ID).
	    ",".c2dbBool($extoptRow["initial_flg"]).
	    ",".c2dbBool($extoptRow["facility_flg"]).
	    ",".c2dbBool($extoptRow["class_flg"]).
	    ",".c2dbBool($extoptRow["emptree_flg"]).
	    ")"
    );
    header("Location: ext_menu.php");// 番号一覧画面に遷移
    die;
}
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | オプション</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">




<div class="window_title">
    <table cellspacing="0" cellpadding="0">
    <tr>
    <th>
        <?=$intra_pankuzu_html?>
        <a class="pankuzu2" href="ext_option.php">オプション</a>
    </th>
    <? if ($EAL[SELF_ADMIN_AUTH_FLG] == "1") { ?>
     <td><a href="extadm_menu.php">管理画面へ</a></td>
    <? } ?>
    </tr>
    </table>
</div>


<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="ext_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="ext_search.php">番号検索</a></th>
    <th class="selectable"><a href="ext_number_update.php">番号変更</a></th>
    <? if ($extadmoptRow["newcomer_flg"]!="f") { ?>
    <th class="selectable"><a href="ext_newcomer.php"><nobr>今月の新人</nobr></a></th>
    <? } ?>
    <th class="current"><a href="ext_option.php">オプション</a></th>
    </tr></table>
</div>






<form action="ext_option.php" method="post">
<input type="hidden" name="try_update" value="1" />


<div style="padding-top:5px; width:600px">


<div style="padding:2px 0"><b>番号一覧画面</b></div>


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <td width="200">頭文字（あ行〜わ行）を表示</td>
        <td>
            <label><input type="radio" name="initial_flg" value="1"<?=(c2Bool($extoptRow["initial_flg"])?" checked":"")?>>する</label>
            <label><input type="radio" name="initial_flg" value=""<?=(!c2Bool($extoptRow["initial_flg"])?" checked":"")?>>しない</label>
        </td>
    </tr>
    <tr>
        <td>施設を表示</td>
        <td>
            <label><input type="radio" name="facility_flg" value="1"<?=(c2Bool($extoptRow["facility_flg"])?" checked":"")?>>する</label>
            <label><input type="radio" name="facility_flg" value=""<?=(!c2Bool($extoptRow["facility_flg"])?" checked":"")?>>しない</label>
        </td>
    </tr>
    <tr>
        <td>部門を表示</td>
        <td>
            <label><input type="radio" name="class_flg" value="1"<?=(c2Bool($extoptRow["class_flg"])?" checked":"")?>>する</label>
            <label><input type="radio" name="class_flg" value=""<?=(!c2Bool($extoptRow["class_flg"])?" checked":"")?>>しない</label>
        </td>
    </tr>
    <tr>
        <td>部門（職員）を表示</td>
        <td>
            <label><input type="radio" name="emptree_flg" value="1"<?=(c2Bool($extoptRow["emptree_flg"])?" checked":"")?>>する</label>
            <label><input type="radio" name="emptree_flg" value=""<?=(!c2Bool($extoptRow["emptree_flg"])?" checked":"")?>>しない</label>
        </td>
    </tr>
</table>


<div style="text-align:right; padding-top:2px"><input type="submit" value="更新"></div>


</div>

</form>
</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>
