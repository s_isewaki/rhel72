<?
define("SELF_APPID",   "ext");
define("SELF_USER_AUTH_FLG", "emp_ext_flg");
define("SELF_ADMIN_AUTH_FLG", "emp_extadm_flg");

if ($_REQUEST["c2trace"]=="direct" || $_REQUEST["c2trace"]=="intelligent") {
    $_COOKIE["C2TRACE"] = $_REQUEST["c2trace"];
}
if ($_REQUEST["c2trace"]=="off" || $_REQUEST["c2trace"]=="exit") {
    $_COOKIE["C2TRACE"] = "";
}

define("C2_SESSION_GROUP_NAME", "ext");

// このファイルのひとつ上の階層に上ってから、Core2呼び出し
require_once(dirname(dirname(__FILE__))."/class/Cmx/Core2/C2FW.php");

// アプリ名
$c2app = c2GetC2App();
//define("SELF_APPNAME",   $c2app->getApplicationName("ext"));
define("SELF_APPNAME",   "内線電話帳");
// クッキーのセッション値からログイン者判別
// ログイン前の画面から来た場合は、無視する。
$loginEmpmstRow = array();
$EAL = array();
if (!defined("C2_LOGIN_FREE")) {
	$loginEmpmstRow = c2login::validateSession();
	$EAL = $c2app->getEmpAuthority(C2_LOGIN_EMP_ID); // ログイン者の機能権限有無リスト
}

if ($spf_exec_log_start) c2dbExecLogStart("ext"); // DBにspfという名義で更新系SQLログを保存させるように指定

require_once(dirname(dirname(__FILE__))."/about_postgres.php");
require_once(dirname(dirname(__FILE__))."/referer_common.ini");

// 遷移元の設定。イントラ呼出しかの判定で利用
$referer = "";
if (!defined("C2_LOGIN_FREE")) {
	if ($_REQUEST["extension"] != "") {
	    set_referer($con, C2_SESSION_ID, "extension", $_REQUEST["extension"], $fname);
	    $referer = $_REQUEST["extension"];
	} else {
	    $referer = get_referer($con, C2_SESSION_ID, "extension", $fname);
	}
}

// メインメニューやぱんくずは、一部作成しておく
$intra_menu_html = "";
$intra_pankuzu_html = '<a class="pankuzu" href="ext_menu.php">内線電話帳</a> &gt;';

$c2app = c2GetC2App();
$intramenuRow = $c2app->getIntramenuRow();

if ($referer == "2") {
    $intra_pankuzu_html =
    '<a class="pankuzu" href="../intra_menu.php?session='.C2_SESSION_ID.'">イントラネット</a> &gt; '.
    '<a class="pankuzu2" href="../intra_info.php?session='.C2_SESSION_ID.'">'.$intramenuRow["menu2"].'</b></a> &gt; '. // 役立つ情報
    '<a class="pankuzu2" href="ext_menu.php">'.$intramenuRow["menu2_2"].'</a> &gt;'; // 内線電話・PHS

    $intra_menu_html =
    '<th class="selectable"><a href="../intra_menu.php?session='.C2_SESSION_ID.'"><nobr>メインメニュー</nobr></a></th>'.
    '<th class="selectable"><a href="../intra_info.php?session='.C2_SESSION_ID.'"><nobr>'.$intramenuRow["menu2"].'</nobr></a></th>'; // 役立つ情報
}
// 管理用のぱんくずは、これで固定
$admin_pankuzu =
'<div class="window_title">'.
'<table cellspacing="0" cellpadding="0"><tr>'.
'<th>'.$intra_pankuzu_html.' <a class="pankuzu2" href="extadm_menu.php">管理画面</a></th>'.
'<td><a href="ext_menu.php">ユーザ画面へ</a></td>'.
'</tr></table></div>';

// ページングありの一覧の、１ページあたりの行数
define("ROW_LIMIT", 20);
define("ROW_LIMIT10", 10);


$fname = $PHP_SELF;
$con = connect2db($fname); // DB接続


define("SHOW_PHOTO_PROFILE", c2dbGetSystemConfig("ext.showPhotoProfile"));

function getPhsName() {
    global $c2app;
    $LBP = $c2app->getLabelByProfile();
    return $LBP["PHS"][$c2app->getPrfType()];
}

$classnameRow = c2dbGetTopRow("select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname");

// 「今月の新人」機能利用フラグ デフォルトはtrueとしたい
$extadmoptRow = c2dbGetTopRow("select * from extadmopt"); // このテーブルにはnewcomer_flgしか無い

// ダウンロード時は本当の名前でダウンロードする場合true。そうでなければシステムIDがファイル名となる。文書管理のフラグを利用。
$is_realname_download = c2Bool(c2dbGetOne("select real_name_flg from libconfig"));


// $akasatanaは画面指定した「あかさたなはまやらわ」。
// empmst.emp_keywdは職員名カナを数字にしたもの。このemp_keywdって、存在の意味なくね？
function and_EmpKeywdSql($akasatana) {
    $akasatana = (int)$akasatana;
    $keys = array();
    if ($akasatana==1) $keys = array("01", "02", "03", "04", "05"); // あいうえお
    if ($akasatana==2) $keys = array("06", "07", "08", "09", "10", "11", "12", "13", "14", "15"); // かきくけこ
    if ($akasatana==3) $keys = array("16", "17", "18", "19", "20", "21", "22", "23", "24", "25"); // さしすせそ
    if ($akasatana==4) $keys = array("26", "27", "28", "29", "30", "31", "32", "33", "34", "35"); // たちつてと、、、以下同文
    if ($akasatana==5) $keys = array("36", "37", "38", "39", "40");
    if ($akasatana==6) $keys = array("41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55");
    if ($akasatana==7) $keys = array("56", "57", "58", "59", "60");
    if ($akasatana==8) $keys = array("61", "62", "63");
    if ($akasatana==9) $keys = array("64", "65", "66", "67", "68");
    if ($akasatana==10) $keys = array("69", "70", "71", "72", "73", "99"); // 最後に99（その他）を追加
    if (!count($keys)) return "";
    return "and (empmst.emp_keywd like '" . implode("%' or empmst.emp_keywd like '", $keys). "%')";
}
function concatTel($t1, $t2, $t3) {
    if ($t1=="") return "";
    return $t1."-".$t2."-".$t3;
}

function allOrNothing($tel1, $tel2, $tel3) {
    if ($tel1.$tel2.$tel3) {
        if ($tel1=="" || $tel2=="" || $tel3=="") return false; // 一部無い。エラーfalse
        //if (!preg_match("/^[0-9]+$/", $tel1)) return false; // 数値でない。エラーfalse 既に数値以外が登録済？このチェックの追加は見合わせ
        //if (!preg_match("/^[0-9]+$/", $tel2)) return false; // 数値でない。エラーfalse 既に数値以外が登録済？このチェックの追加は見合わせ
        //if (!preg_match("/^[0-9]+$/", $tel3)) return false; // 数値でない。エラーfalse 既に数値以外が登録済？このチェックの追加は見合わせ
    }
    return true; // 全部カラか全部入力あり。成功true
}

function getPhotoInfo($emp_id) {
    $os_rel_path = "profile/".$emp_id.".jpg"; // C2フレームワークにより、cwdは「/comedix/」直下であるため、profileディレクトリが見れる。
    $photo_url = "../".$os_rel_path; // URL上では、/comedix/ext/の下であるから、ひとつ上にのぼらなければならない。
    $photo_html = "";
    $wheight = (SHOW_PHOTO_PROFILE=="1"? 600 : 480);
    $is_file_exists = file_exists($os_rel_path);
    $s1w = 30;  $s1h = 40;
    $s2w = 114; $s2h = 152;
    $r1w = 0;   $r1h = 0;
    $r2w = $s2w; $r2h = $s2h;
    $iv_size = "";
    if ($is_file_exists) {
        list($rw, $rh) = @(array)getimagesize($os_rel_path);
        if ($rw && $rh) {
            $base_rate = 3 / 4; // 表示枠の、縦に対する横の大きさ。横が大きいほど数値が大きい。
            $real_rate = $rw / $rh; // 実画像の、縦に対する横の大きさ。横が大きいほど数値が大きい。
            if ($real_rate > $base_rate) { // 実画像は表示枠より横が大きめ⇒横を120に縮小した場合の縦サイズを求める
                $r1h = (int)($rh * ($s1w / $rw));
                $r1w = $s1w;
                $r2h = (int)($rh * ($s2w / $rw));
                $r2w = $s2w;
                $r2h = (int)($rh * ($s2w / $rw));
                $r2w = $s2w;
            } else { // 実画像は表示枠より縦が大きめか同じ⇒縦を160に縮小した場合の横サイズを求める
                $r1w = (int)($rw * ($s1h / $rh));
                $r1h = $s1h;
                $r2w = (int)($rw * ($s2h / $rh));
                $r2h = $s2h;
                $r2w = (int)($rw * ($s2h / $rh));
                $r2h = $s2h;
            }
            // ポップアップ画像上は、縦横360px以内であること
            if ($rw > $rh ) $iv_size = '&isize='.urlencode('width:'.min(400, $rw)).'px';
            else $iv_size = '&isize='.urlencode('height:'.min(400, $rh)).'px'; // 横が大きい。横を強制する。
        }
    }
    $js = "window.open('ext_image_viewer.php?emp_id=".$emp_id.$iv_size."', 'empphoto', 'width=480,height=".$wheight.",scrollbars=yes');";

    // シャドウは余白が8px四方、影が9px四方である。結果、実画像より34px大きくなる。
    $imgtag2 =
    '<div style="width:'.($r2w+34).'px; height:'.($r2h+34).'px; margin:0 0 0 auto; background:url(../spf/shadow.php?iw='.$r2w.'&ih='.$r2h.') no-repeat">'.
    '<img src="'.($is_file_exists ? $photo_url : "../spf/img/avator.gif").'" alt="" height="'.$r2h.'" width="'.$r2w.'" style="padding:17px; margin:0" />'.
    '</div>';
    return array(
        "os_rel_path" => $os_rel_path,
        "photo_url" => $photo_url,
        "is_file_exists" => $is_file_exists,
        "image_viewer_atag" => ($is_file_exists ? '<a href="javascript:void(0)" onclick="'.$js.'">写真</a>' : ""),
        "image_viewer_imgtag" => ($is_file_exists ? '<img src="'.$photo_url.'" width="'.$r1w.'" height="'.$r1h.'" style="cursor:pointer;" onclick="'.$js.'">' : ""),
        "image_viewer_imgtag2" => $imgtag2
    );
}

function getEmpmstExtLabels() {
    $sql = "select field_id, field_label from spfm_field_struct where table_id = 'spft_empmst' and field_id like 'empmst_ext%'";
    $_rows = c2dbGetRows($sql);
    $labels = array();
    foreach ($_rows as $row) {
        $labels[$row["field_id"]] = $row["field_label"];
    }
    return $labels;
}

// 拡張empmst項目1から5の表示可能判定と編集可能判定を取得
// ・利用判定：disp_empがall_okか、そうでないかを判定
// ・編集判定：edit_empがall_okか、そうでないかを判定
function getEmpmstExtUsing() {
    $fieldLabels = array();
    $labels = getEmpmstExtLabels();
    $sql =
    " select field_id from spfm_field_struct_auth".
    " where table_id = 'spft_empmst'".
    " and field_id like 'empmst_ext%'".
    " and target_place = 'all'".
    " and disp_emp = 'all_ok'".
    " order by order_no";
    $rows = c2dbGetRows($sql);
    $using = array();
    foreach ($rows as $row) {
        $using[$row["field_id"]] = array("label"=>$labels[$row["field_id"]]);
    }
    return $using;
}


// 番号一覧、番号検索での列定義。
// 写真から拡張フィールドまで。
// ユーザ画面側で利用。管理側は全表示するため、この関数を見ていない。
// 表示対象のものは、配列に加えられる。
function getEmpmstExtUserColumns($disp_for) {
    $using = getEmpmstExtUsing();
    $labels = getEmpmstExtLabels();

    // ログイン画面向け（ext_search_from_login.php）
    $lgnList = explode(",", c2dbGetSystemConfig("ext.searchFromLoginDispCols"));
    if ($disp_for=="login" || $disp_for=="admin") {
        $sql =
        " select fsa.* from spfm_field_struct_auth fsa".
        " where fsa.target_place = 'extension_viewing'".
        " order by fsa.order_no";
    }
    else {
        $sql =
        " select fsa.* from spfm_field_struct_auth fsa".
        " where fsa.target_place = 'extension_viewing'".
        " and exists (".
        "     select emp_id from concurrent_view cv".
        "     where cv.emp_id =".c2dbStr(C2_LOGIN_EMP_ID). // ログイン者について抽出する。
        "     and".
        "     (".
        "         ( fsa.disp_emp = 'all_ok' or fsa.disp_emp like '%[' || cv.emp_id || ']%' )". // 職員全許可か、職員管理IDがが許可リストに含まれればOK
        "         or (". // それか、
        "             (fsa.disp_st = 'all_ok' or fsa.disp_st like '%[' || cv.emp_st || ']%')". // 部署全許可か、職員の部署が許可リストに含まれていて、
        "             and". // かつ
        "             (fsa.disp_cad = 'all_ok' or fsa.disp_cad like '%[' || cv.cad_id || ']%')". // 役職が全許可か、職員の役職が許可リストに含まれる
        "         )".
        "     )".
        ")".
        " order by fsa.order_no";
    }
    $rows = c2dbGetRows($sql);
    $ret = array();
    foreach ($rows as $row) {
        if ($disp_for=="login" && !in_array($row["field_id"], $lgnList)) continue;
        if (substr($row["field_id"],0,10)=="empmst_ext" && !$using[$row["field_id"]]) continue; // システム的に使っていない。（職員登録側で制御）スルー
        $ret[$row["field_id"]] = array("label"=>$labels[$row["field_id"]]); // ラベル値があるのはempmst_extxxのみ
    }
    return $ret;
}
