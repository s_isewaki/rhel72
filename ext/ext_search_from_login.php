<?
define("C2_LOGIN_FREE", 1); // ログインチェック不要
require_once("common.php"); // 読込みと同時にログイン判別だが、ログインはスルーされる


c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");



$page = max(1, (int)$_REQUEST["page"]); // ページ、１以上
$class_id = (int)$_REQUEST["class_id"];
$atrb_id = (int)$_REQUEST["atrb_id"];
$dept_id = (int)$_REQUEST["dept_id"];
$srch_emp_nm = $_REQUEST["srch_emp_nm"];
$_srch_emp_nm = str_replace("　", "", str_replace(" ", "", $srch_emp_nm));
$urlenc_srch_emp_nm = urlencode($srch_emp_nm);

$classmstRows = c2dbGetRows("select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no");// 部門一覧
$atrbmstRows = c2dbGetRows("select class_id as pid, atrb_id as cid, atrb_nm as nm from atrbmst where atrb_del_flg = 'f' order by class_id, order_no");// 課一覧
$deptmstRows = c2dbGetRows("select atrb_id as pid, dept_id as cid, dept_nm as nm from deptmst where dept_del_flg = 'f' order by atrb_id, order_no");// 科一覧



$emp_where = " where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
if ($class_id) {
    $emp_where .=
    " and ((empmst.emp_class = ".$class_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = ".$class_id."))";
}
if ($atrb_id) {
    $emp_where .=
    " and ((empmst.emp_attribute = ".$atrb_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = ".$atrb_id."))";
}
if ($dept_id) {
    $emp_where .= " and ((empmst.emp_dept = ".$dept_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = ".$dept_id."))";
}

$dispInfo = getEmpmstExtUserColumns("login");
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 一発電話検索</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<style type="text/css">
.current_page {
    background-color:#e0e0e0;
    font-weight:bold;
}
.current_page, .page {
    border:#e0e0e0 solid 1px;
    display:block;
    float:left;
    padding:0.2em 0.5em;
    margin-right:5px;
}
a.page:hover {
    background-color:navy;
    color:white;
}
</style>
<script type="text/javascript">
var busyoObj = {};
busyoObj["atrb_id"] = <?=c2ToJson($atrbmstRows)?>;
busyoObj["dept_id"] = <?=c2ToJson($deptmstRows)?>;

function busyoChanged(childId, selectValue) {
    var cmb = document.getElementById(childId);
    cmb.options.length = 1;
    for ( var idx=0; idx<busyoObj[childId].length; idx++) {
        var obj = busyoObj[childId][idx];
        if (obj.pid!=selectValue) continue;
        cmb.options[cmb.options.length] = new Option(obj.nm, obj.cid);
    }
    if (childId=="emp_attribute") document.getElementById("emp_dept").options.length = 1;
}
function changeDivision(division) {
    var cells = document.getElementsByTagName('td');
    for (var i = 0, j = cells.length; i < j; i++) {
        var cn = cells[i].className;
        if (cn!="class_col" && cn!="atrb_col" && cn!="dept_col" && cn!="kojin_col") continue;
        var display = (cells[i].className == division+'_col') ? '' : 'none';
        cells[i].style.display = display;
    }
    document.getElementById('class_anchor').className = (division=="class"?"selected":"");
    document.getElementById('atrb_anchor').className =  (division=="atrb"?"selected":"");
    document.getElementById('dept_anchor').className =  (division=="dept"?"selected":"");
    document.getElementById('kojin_anchor').className = (division=="kojin"?"selected":"");
}
</script>
</head>
<body style="margin:5px" onload="window.focus()"><div id="CLIENT_FRAME_CONTENT">


<table cellspacing="0" cellpadding="0" class="dialog_title"><tr>
        <th>一発電話検索</th>
        <td class="closebtn"><a href="javascript:void(0);" onclick="window.close();"></a></td>
</tr></table>



<form name="mainform" method="post" action="ext_search_from_login.php">
<div style="padding-top:5px; width:600px">



<table width="100%" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="120">検索文字入力</th>
        <td><input type="text" name="srch_emp_nm" value="<? echo($srch_emp_nm); ?>" size="20" style="ime-mode:active;">（氏名）</td>
    </tr>
    <tr>
        <th><?=hh($classnameRow["class_nm"])?></th>
        <td>
            <select name="class_id" onchange="busyoChanged('atrb_id', this.value);">
                <option value="-">----------</option>
                <? foreach($classmstRows as $row) {?>
                    <option value="<?=$row["class_id"]?>"<?=($row["class_id"]==$class_id?" selected":"")?>><?=hh($row["class_nm"])?></option>
                <? } ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?=hh($classnameRow["atrb_nm"])?></th>
        <td>
            <select name="atrb_id" id="atrb_id" onchange="busyoChanged('dept_id', this.value);">
                <option value="-">----------</option>
                <? foreach ($atrbmstRows as $row) { ?>
                    <? if ($class_id!=$row["pid"]) continue; ?>
                    <option value="<?=$row["cid"]?>"<?=($row["cid"]==$atrb_id?" selected":"")?>><?=hh($row["nm"])?></option>
                <? } ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?=hh($classnameRow["dept_nm"])?></th>
        <td>
            <select name="dept_id" id="dept_id">
                <option value="-">----------</option>
                <? foreach ($deptmstRows as $row) { ?>
                    <? if ($atrb_id!=$row["pid"]) continue; ?>
                    <option value="<?=$row["cid"]?>"<?=($row["cid"]==$dept_id?" selected":"")?>><?=hh($row["nm"])?></option>
                <? } ?>
            </select>
        </td>
    </tr>
</table>

<div style="text-align:right; padding-top:2px"><input type="submit" value="検索"></div>

</div>



</form>





<table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr height="22">
        <td align="right">
            <a id="class_anchor" href="javascript:void(0);" onclick="changeDivision('class');"><?=hh($classnameRow["class_nm"])?></a> &gt;
            <a id="atrb_anchor" href="javascript:void(0);" onclick="changeDivision('atrb');"><?=hh($classnameRow["atrb_nm"])?></a> &gt;
            <a id="dept_anchor" href="javascript:void(0);" onclick="changeDivision('dept');"><?=hh($classnameRow["dept_nm"])?></a> &gt;
            <a id="kojin_anchor" href="javascript:void(0);" onclick="changeDivision('kojin');" class="selected">本人</a>
        </td>
    </tr>
</table>



<div>

<table width="100%" cellspacing="0" cellpadding="2" class="list">
    <tr height="22" bgcolor="#f6f9ff" valign="top">
        <td><nobr>職員氏名</nobr></td>
        <?
        foreach ($dispInfo as $field_id => $fieldInfo) {
            if ($field_id=="class_nm") echo '<td><nobr>所属'.hh($classnameRow["class_id"]).'</nobr></td>';
            if ($field_id=="job_nm") echo '<td><nobr>職種</nobr></td>';
            if ($field_id=="st_nm") echo '<td><nobr>役職</nobr></td>';
            if ($field_id=="emp_phs") echo '<td><nobr>'.getPhsName().'</nobr></td>';
            if ($field_id=="emp_ext") echo '<td class="kojin"><nobr>内線番号</nobr></td>';
            if ($field_id=="emp_mobile") echo '<td class="kojin"><nobr>携帯端末・外線</nobr></td>';
            if ($field_id=="empmst_ext1") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext2") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext3") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext4") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext5") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
        }
        ?>
        <td class="class_col" style="display:none;"><nobr>内線番号</nobr></td>
        <td class="class_col" style="display:none;"><nobr>外線番号</nobr></td>
        <td class="atrb_col" style="display:none;"><nobr>内線番号</nobr></td>
        <td class="atrb_col" style="display:none;"><nobr>外線番号</nobr></td>
        <td class="dept_col" style="display:none;"><nobr>内線番号</nobr></td>
        <td class="dept_col" style="display:none;"><nobr>外線番号</nobr></td>
    </tr>
<?
    $sql =
    " select count(*) from empmst".
    " ". $emp_where.
    " and (empmst.emp_lt_nm || empmst.emp_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").
    " or empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").")";
    $ttl_count = c2dbGetOne($sql);
    if ($ttl_count <= ROW_LIMIT10) $page = 1;
    if ($ttl_count < ROW_LIMIT10*$page) $page = (int)($ttl_count / ROW_LIMIT10) + (($ttl_coun+1)%ROW_LIMIT ? 1 : 0);

    $sql =
    " select".
    " empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm".
    ",classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, jobmst.job_nm, stmst.st_nm".
    ",empmst.emp_phs, empmst.emp_ext, empmst.emp_mobile1, empmst.emp_mobile2, empmst.emp_mobile3".
    ",extclass.extension as class_ext, extclass.tel1 as class_tel1, extclass.tel2 as class_tel2, extclass.tel3 as class_tel3".
    ",extatrb.extension as atrb_ext, extatrb.tel1 as atrb_tel1, extatrb.tel2 as atrb_tel2, extatrb.tel3 as atrb_tel3".
    ",extdept.extension as dept_ext, extdept.tel1 as dept_tel1, extdept.tel2 as dept_tel2, extdept.tel3 as dept_tel3".
    ",spft_empmst.empmst_ext1, spft_empmst.empmst_ext2, spft_empmst.empmst_ext3, spft_empmst.empmst_ext4, spft_empmst.empmst_ext5".
    " from empmst".
    " left outer join spft_empmst on (spft_empmst.emp_id = empmst.emp_id)".
    " inner join classmst on empmst.emp_class = classmst.class_id".
    " inner join atrbmst on empmst.emp_attribute = atrbmst.atrb_id".
    " inner join deptmst on empmst.emp_dept = deptmst.dept_id".
    " inner join jobmst on empmst.emp_job = jobmst.job_id".
    " left outer join stmst on empmst.emp_st = stmst.st_id".
    " left join (".
    "     select * from extcls where atrb_id is null and dept_id is null".
    " ) extclass on ( empmst.emp_class = extclass.class_id )".
    " left join (".
    "     select * from extcls where atrb_id is not null and dept_id is null".
    " ) extatrb on ( empmst.emp_class = extatrb.class_id and empmst.emp_attribute = extatrb.atrb_id )".
    " left join (".
    "     select * from extcls where atrb_id is not null and dept_id is not null".
    " ) extdept on ( empmst.emp_class = extdept.class_id and empmst.emp_attribute = extdept.atrb_id and empmst.emp_dept = extdept.dept_id )".
    " ". $emp_where.
    " and (empmst.emp_lt_nm || empmst.emp_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").
    " or empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").")".
    " order by empmst.emp_keywd, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_id".
    " offset ".(($page-1)*ROW_LIMIT10)." limit ".ROW_LIMIT10;
    $rows = c2dbGetRows($sql);
    foreach ($rows as $row) {
        echo("<tr height=\"22\">\n");
        echo '<td>'.hh($row["emp_lt_nm"]." ".$row["emp_ft_nm"]).'</td>';

        foreach ($dispInfo as $field_id => $fieldInfo) {
            if ($field_id=="class_nm") echo '<td>'.hh($row["class_nm"]."＞".$row["atrb_nm"]."＞".$row["dept_nm"]).'</td>';
            if ($field_id=="job_nm") echo '<td>'.hh($row["job_nm"]).'</td>';
            if ($field_id=="st_nm") echo '<td>'.hh($row["st_nm"]).'</td>';
            if ($field_id=="emp_phs") echo '<td>'.hh($row["emp_phs"]).'</td>';
            if ($field_id=="emp_ext") echo '<td>'.hh($row["emp_ext"]).'</td>';
            if ($field_id=="emp_mobile") echo '<td>'.hh(concatTel($row["emp_mobile1"], $row["emp_mobile2"], $row["emp_mobile3"])).'</td>';
            if ($field_id=="empmst_ext1") echo '<td>'.hh($row["empmst_ext1"]).'</td>';
            if ($field_id=="empmst_ext2") echo '<td>'.hh($row["empmst_ext2"]).'</td>';
            if ($field_id=="empmst_ext3") echo '<td>'.hh($row["empmst_ext3"]).'</td>';
            if ($field_id=="empmst_ext4") echo '<td>'.hh($row["empmst_ext4"]).'</td>';
            if ($field_id=="empmst_ext5") echo '<td>'.hh($row["empmst_ext5"]).'</td>';
        }
        echo '<td class="class_col" style="display:none;">'.hh($row["class_ext"]).'</td>';
        echo '<td class="class_col" style="display:none;">'.hh(concatTel($row["class_tel1"], $row["class_tel2"], $row["class_tel3"])).'</td>';
        echo '<td class="atrb_col" style="display:none;">'.hh($row["atrb_ext"]).'</td>';
        echo '<td class="atrb_col" style="display:none;">'.hh(concatTel($row["atrb_tel1"], $row["atrb_tel2"], $row["atrb_tel3"])).'</td>';
        echo '<td class="dept_col" style="display:none;">'.hh($row["dept_ext"]).'</td>';
        echo '<td class="dept_col" style="display:none;">'.hh(concatTel($row["dept_tel1"], $row["dept_tel2"], $row["dept_tel3"])).'</td>';
        echo '</tr>'."\n";
    }
?>

</table>
</div>


<?
    if ($ttl_count > ROW_LIMIT10) {
        echo '<table align="center"><tr><td style="padding:1em 0 0.5em; font-size:14px">';
        if ($page > 1) {
            $pre = $page - 1;
            echo '<a class="page" href="ext_search_from_login.php';
            echo '?srch_emp_nm='.$urlenc_srch_emp_nm.'&class_id='.$class_id.'&atrb_id='.$atrb_id.'&dept_id='.$dept_id.'&page='.$pre.'">';
            echo '<b>前へ</b><img src="../img/frame7black17x17.gif" alt="" width="17" height="17"';
            echo ' style="border-style:none;vertical-align:middle;margin-left:6px;"></a>';
        }

        $min = 1;
        $max = ceil($ttl_count / ROW_LIMIT10);
        $show_min = $min;
        $show_max = $max;

        while ($show_max - $show_min >= 10) {
            if ($show_max - $page >= $page - $show_min) $show_max--;
            else $show_min++;
        }

        for ($p = $show_min; $p <= $show_max; $p++) {
            if ($p == $page) {
                echo '<span class="current_page">';
                echo '<img src="../img/spacer.gif" alt="" width="1" height="17" style="border-style:none;vertical-align:middle;">';
                echo $p.'<img src="../img/spacer.gif" alt="" width="1" height="17" style="border-style:none;vertical-align:middle;"></span>';
            } else {
                echo '<a class="page" href="ext_search_from_login.php';
                echo '?srch_emp_nm='.$urlenc_srch_emp_nm.'&class_id='.$class_id.'&atrb_id='.$atrb_id.'&dept_id='.$dept_id.'&page='.$p.'">';
                echo '<img src="../img/spacer.gif" alt="" width="1" height="17"';
                echo ' style="border-style:none;vertical-align:middle;">';
                echo $p.'<img src="../img/spacer.gif" alt="" width="1" height="17" style="border-style:none;vertical-align:middle;"></a>';
            }
        }
        if ($page < $max) {
            $next = $page + 1;
            echo '<a class="page" href="ext_search_from_login.php';
            echo '?srch_emp_nm='.$urlenc_srch_emp_nm.'&class_id='.$class_id.'&atrb_id='.$atrb_id.'&dept_id='.$dept_id.'&page='.$next.'">';
            echo '<img src="../img/frame3black17x17.gif" alt="" width="17" height="17"';
            echo ' style="border-style:none;vertical-align:middle;margin-right:6px;"><b>次へ</b></a>';
        }
        echo '<br style="clear:left;">';
        echo '</td></tr></table>';
    }
?>


</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>