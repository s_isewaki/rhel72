<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$sql =
" select main.*".
",empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as head_emp_nm".
" from (".
"     select classmst.class_id, 0 as atrb_id, 0 as dept_id, classmst.class_nm, '' as atrb_nm, '' as dept_nm".
"    ,1 as hier, classmst.order_no as class_order, 0 as atrb_order, 0 as dept_order".
"    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
"     from classmst".
"     left join extcls on ( extcls.class_id = classmst.class_id )".
"     where extcls.atrb_id is null".
"     and extcls.dept_id is null".
"     and classmst.class_del_flg = 'f'".
"     and classmst.class_show_flg = 't'". // なぜかユーザ側と異なる？
" union".
"     select atrbmst.class_id, atrbmst.atrb_id, 0 as dept_id, classmst.class_nm, atrbmst.atrb_nm, '' as dept_nm".
"    ,2 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, 0 as dept_order".
"    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
"     from atrbmst".
"     inner join classmst on (classmst.class_id = atrbmst.class_id )".
"     left join extcls on ( extcls.class_id = atrbmst.class_id and extcls.atrb_id = atrbmst.atrb_id )".
"     where extcls.dept_id is null".
"     and atrbmst.atrb_del_flg = 'f'".
" union".
"     select atrbmst.class_id, deptmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm".
"    ,3 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, deptmst.order_no as dept_order".
"    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
"     from deptmst".
"     inner join atrbmst on (atrbmst.atrb_id = deptmst.atrb_id )".
"     inner join classmst on (classmst.class_id = atrbmst.class_id )".
"         left join extcls on ( extcls.atrb_id = deptmst.atrb_id and extcls.dept_id = deptmst.dept_id )".
"     where deptmst.dept_del_flg = 'f'".
" ) main".
" left join (".
"     select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm from empmst".
"     inner join authmst on (authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')".
" ) empmst on ( empmst.emp_id = main.head_emp_id )".
" order by class_order, atrb_order, dept_order";
$rows = c2dbGetRows($sql);

//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {

    // 入力チェック
    $postfix = array("extension", "tel1", "tel2", "tel3", "fax1", "fax2", "fax3", "head", "note");
    foreach ($rows as $idx => $row) {
        $uid = $row["class_id"];
        $unm = $row["class_nm"];
        if ($row["hier"]>=2) { $uid .= "-".$row["atrb_id"]; $unm .= "＞".$row["atrb_nm"]; }
        if ($row["hier"]>=3) { $uid .= "-".$row["dept_id"]; $unm .= "＞".$row["dept_nm"]; }

        foreach ($postfix as $field) {
            $row[$field] = $_REQUEST[$field."_".$uid];
        }
        $rows[$idx] = $row;
        if (count($errmsg)) continue;

        if (strlen($row["ext"]) > 6) { $errmsg[]= '内線番号が長すぎます。（'.$unm.'）'; }
        if (strlen($row["tel1"]) > 6) { $errmsg[]= '外線番号1が長すぎます。('.$unm.')'; }
        if (strlen($row["tel2"]) > 6) { $errmsg[]= '外線番号2が長すぎます。('.$unm.')'; }
        if (strlen($row["tel3"]) > 6) { $errmsg[]= '外線番号3が長すぎます。('.$unm.')'; }
        if (!allOrNothing($row["tel1"], $row["tel2"], $row["tel3"])) { $errmsg[]= '外線番号を正しく入力してください。（'.$unm.'）'; }
        if (strlen($row["fax1"]) > 6) { $errmsg[]= 'FAX1が長すぎます。（'.$unm.'）'; }
        if (strlen($row["fax2"]) > 6) { $errmsg[]= 'FAX2が長すぎます。（'.$unm.'）'; }
        if (strlen($row["fax3"]) > 6) { $errmsg[]= 'FAX3が長すぎます。（'.$unm.'）'; }
        if (!allOrNothing($row["fax1"], $row["fax2"], $row["fax3"])) { $errmsg[]= 'FAXを正しく入力してください。（'.$unm.'）'; }
    }

    if (!count($errmsg)) {
        // アクセスログ
        require_once("aclg_set.php");
        aclg_regist($session, "extension_admin_class_insert.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//        c2dbBeginTrans();

        // 部門連絡先データを全削除
        c2dbExec("delete from extcls");
        // 部門連絡先データを登録
        foreach ($rows as $row) {
            if (!$row["class_id"]) continue;

            // 原因不明の重複登録を防ぐ
            $atrb_sql = ((int)$row["atrb_id"] ? " = ".(int)$row["atrb_id"] : " is null");
            $dept_sql = ((int)$row["dept_id"] ? " = ".(int)$row["dept_id"] : " is null");
            c2dbExec("delete from extcls where class_id = ".$row["class_id"]." and atrb_id".$atrb_sql." and dept_id".$dept_sql);

            $atrb_sql = ((int)$row["atrb_id"] ? (int)$row["atrb_id"] : "null");
            $dept_sql = ((int)$row["dept_id"] ? (int)$row["dept_id"] : "null");
            $sql =
            " insert into extcls (class_id, atrb_id, dept_id, tel1, tel2, tel3, fax1, fax2, fax3, head_emp_id, note, extension".
            " ) values (".
            " ".$row["class_id"].", ".$atrb_sql.", ".$dept_sql.
            ",".c2dbStr($row["tel1"]).", ".c2dbStr($row["tel2"]).", ".c2dbStr($row["tel3"]).
            ",".c2dbStr($row["fax1"]).", ".c2dbStr($row["fax2"]).", ".c2dbStr($row["fax3"]).
            ",".c2dbStr($row["head"]).", ".c2dbStr($row["note"]).", ".c2dbStr($row["extension"]).
            " )";
            c2dbExec($sql);
        }
//        c2dbCommit();

        // 一覧画面に遷移
	    ob_clean();
        header("Location: extadm_class_register.php?");
        die;
    }
}
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 | 部門連絡先登録</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
function initPage() {
<?
if (!is_array($opened_ids)) $opened_ids = array();
foreach ($opened_ids as $opened_id) {
    echo("changeDisplay('$opened_id');\n");
}
?>
}
function changeDisplay(btn, parentPrefix, parentHier) {
    var dest = (btn.className=="plus" ? "minus" : "plus");
    btn.className = dest;
    var tbody = document.getElementById("main_tbody");
    for (var idx=0; idx<tbody.childNodes.length; idx++) {
        var tr = tbody.childNodes[idx];
        var cls = tr.className;
        if (!cls) continue;
        if (tr.id.indexOf(parentPrefix)==0) {
            var aCls = cls.split(" ");
            aCls[parentHier-1] = (dest=="plus" ? "tr_hide" : "tr_show");
            tr.className = aCls.join(" ");
        }
    }
}
function setOpendIds() {
    var images = document.getElementsByTagName('img');
    var form = document.getElementById('mainform');
    for (var i = 0, j = images.length; i < j; i++) {
        if (images[i].className == 'open') {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'opened_ids[]';
            input.value = images[i].id.replace('class', '');
            form.appendChild(input);
        }
    }
}

function selectHead(class_id, element_id) {
    window.open('extadm_class_head.php?class_id='.concat(class_id).concat('&element_id=').concat(element_id), 'newwin', 'width=640,height=480,scrollbars=yes');
}
</script>
</head>
<body onload="initPage();"><div id="CLIENT_FRAME_CONTENT">



<div class="window_title">
    <table cellspacing="0" cellpadding="0">
    <tr>
    <th>
        <?=$intra_pankuzu_html?>
        <a class="pankuzu2" href="extadm_menu.php">管理画面</a>
    </th>
    <td><a href="ext_menu.php">ユーザ画面へ</a></td>
    </tr>
    </table>
</div>



<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="extadm_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="extadm_search.php">番号検索</a></th>
    <th class="selectable w120"><a href="extadm_facility_register.php"><nobr>施設連絡先登録</nobr></a></th>
    <th class="current w120"><a href="extadm_class_register.php"><nobr>部門連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_document_register.php"><nobr>番号リスト登録</nobr></a></th>
    <th class="selectable"><a href="extadm_option.php">オプション</a></th>
    </tr></table>
</div>



<div style="padding-top:5px;">








<form id="mainform" name="mainform" method="post" action="extadm_class_register.php" onsubmit="setOpendIds();">
<input type="hidden" name="try_update" value="1" />
<table width="100%" cellspacing="0" cellpadding="2" class="list">
<tr height="26" bgcolor="#f6f9ff">
    <td width="24%"><nobr>部門名</nobr></td>
    <td width="8%"><nobr>内線番号</nobr></td>
    <td width="17%"><nobr>外線番号</nobr></td>
    <td width="17%">FAX</td>
    <td width="16%"><nobr>所属長</nobr></td>
    <td width="18%"><nobr>機能</nobr></td>
</tr>
<tbody id="main_tbody">


<?
    $last_class_id = "";
    $last_atrb_id = "";
    $last_hier = 0;
    foreach ($rows as $row) {
        $hier = $row["hier"];
        if ($hier==$last_hier) {
            if ($hier==1 || $hier==2) {
                if ($hier==1) {
                    echo '<tr class="p1_'.$row["class_id"].' tr_hide">';
                    echo '<td colspan="6" style="padding-left:45px;">（登録なし）</td>';
                }
                if ($hier==2) {
                    echo '<tr class="p2_'.$row["atrb_id"].' tr_hide tr_hide">';
                    echo '<td colspan="6" style="padding-left:70px;">（登録なし）</td>';
                }
                echo '</tr>';
            }
        }
        $uid = $row["class_id"];
        if ($hier>=2) $uid .= "-".$row["atrb_id"];
        if ($hier>=3) $uid .= "-".$row["dept_id"];

        $pid = "";
        if ($hier==1) echo '<tr>';
        if ($hier==2) { $pid = $last_class_id.'_'.$row["atrb_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide">'; }
        if ($hier==3) { $pid = $last_class_id.'_'.$last_atrb_id."_".$row["dept_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide tr_hide">'; }

        echo '<td class="hier'.$hier.'"><nobr>';
        if ($hier==1) echo '<button type="button" class="plus" onclick="changeDisplay(this, \'tr'.$row["class_id"].'_\', '.$hier.')"></button>';
        if ($hier==2) echo '<button type="button" class="plus" onclick="changeDisplay(this, \'tr'.$last_class_id."_".$row["atrb_id"].'_\', '.$hier.')"></button>';
        if ($hier==1) echo hh($row["class_nm"]).'</nobr></td>';
        if ($hier==2) echo hh($row["atrb_nm"]).'</nobr></td>';
        if ($hier==3) echo hh($row["dept_nm"]).'</nobr></td>';

        echo '<td><input type="text" name="extension_'.$uid.'" value="'.hh($row["extension"]).'" size="5" maxlength="6" style="ime-mode:inactive;" /></td>';
        echo '<td>';
        echo '<input type="text" name="tel1_'.$uid.'" value="'.hh($row["tel1"]).'" size="5" maxlength="6" style="ime-mode:inactive;">-';
        echo '<input type="text" name="tel2_'.$uid.'" value="'.hh($row["tel2"]).'" size="5" maxlength="6" style="ime-mode:inactive;">-';
        echo '<input type="text" name="tel3_'.$uid.'" value="'.hh($row["tel3"]).'" size="5" maxlength="6" style="ime-mode:inactive;">';
        echo '</td>';
        echo '<td>';
        echo '<input type="text" name="fax1_'.$uid.'" value="'.hh($row["fax1"]).'" size="5" maxlength="6" style="ime-mode:inactive;">-';
        echo '<input type="text" name="fax2_'.$uid.'" value="'.hh($row["fax2"]).'" size="5" maxlength="6" style="ime-mode:inactive;">-';
        echo '<input type="text" name="fax3_'.$uid.'" value="'.hh($row["fax3"]).'" size="5" maxlength="6" style="ime-mode:inactive;">';
        echo '</td>';

        echo '<td>';
        echo '<input type="text" name="lbl_head_nm_'.$uid.'" value="'.hh($row["head_emp_nm"]).'" size="12" disabled>';
        echo '<input type="button" value="設定" onclick="selectHead('.$row["class_id"].', \''.$uid.'\');">';
        echo '<input type="hidden" name="head_'.$uid.'" value="'.$row["head_emp_id"].'">';
        echo '<input type="hidden" name="head_nm_'.$uid.'" value="'.hh($row["head_emp_nm"]).'">';
        echo '</td>';

        echo '<td><input type="text" name="note_'.$uid.'" value="'.hh($row["note"]).'" size="24" style="ime-mode:active;"></td>';
        $last_hier = $hier;
        if ($hier==1) $last_class_id = $row["class_id"];
        if ($hier==2) $last_atrb_id = $row["atrb_id"];
        echo '</tr>';
    }
?>
</table>


<div style="text-align:right; padding-top:2px"><input type="submit" value="登録"></div>



</form>
</div>
</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>
