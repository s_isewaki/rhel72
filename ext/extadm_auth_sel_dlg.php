<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

$callback_func = $_REQUEST["callback_func"];
$onload_func = $_REQUEST["onload_func"];


c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$classmstRows = c2dbGetRows("select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no");// 部門一覧
$atrbmstRows = c2dbGetRows("select class_id, atrb_id, atrb_nm from atrbmst where atrb_del_flg = 'f' order by class_id, order_no");// 課一覧
$sql =
" select am.class_id, dm.atrb_id, dm.dept_id, cm.class_nm, am.atrb_nm, dm.dept_nm".
" from deptmst dm".
" inner join atrbmst am on (am.atrb_id = dm.atrb_id)".
" inner join classmst cm on (cm.class_id = am.class_id)".
" where dept_del_flg = 'f' order by am.order_no, dm.order_no";
$deptmstRows = c2dbGetRows($sql);// 科一覧
$deptmstIndexes = array();
foreach ($deptmstRows as $idx => $row) {
    $deptmstIndexes[$row["class_id"]."_".$row["atrb_id"]."_".$row["dept_id"]] = $idx;
}
$stmstNames = array();
$_rows = c2dbGetRows("select st_id, st_nm from stmst where st_del_flg = 'f' order by st_id");// 役職一覧
foreach ($_rows as $idx => $row) {
    $stmstNames[$row["st_id"]] = $row["st_nm"];
}
$empmstNames = array();
$_rows = c2dbGetRows("select emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_nm from empmst");// 職員一覧
foreach ($_rows as $idx => $row) {
    $empmstNames[$row["emp_id"]] = $row["emp_nm"];
}

//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 | </title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
var ee = function(id) { return document.getElementById(id); }
var atrbObj = <?=c2ToJson($atrbmstRows)?>;
var deptObj = <?=c2ToJson($deptmstRows)?>;
var deptIndexes = <?=c2ToJson($deptmstIndexes)?>;
var empmstNames = <?=c2ToJson($empmstNames)?>;
function classChanged() {
    var class_id = ee("ref_class_id").options[ee("ref_class_id").selectedIndex].value;
    var cmb = document.getElementById("ref_atrb_id");
    cmb.options.length = 1;
    for ( var idx=0; idx<atrbObj.length; idx++) {
        var obj = atrbObj[idx];
        if (obj.class_id!=class_id) continue;
        cmb.options[cmb.options.length] = new Option(obj.atrb_nm, obj.atrb_id);
    }
    atrbChanged("");
}
function atrbChanged() {
    var class_id = ee("ref_class_id").options[ee("ref_class_id").selectedIndex].value;
    var atrb_id = ee("ref_atrb_id").options[ee("ref_atrb_id").selectedIndex].value;
    var cmb = document.getElementById("ref_dept_id");
    cmb.options.length = 0;
    if (!class_id) return;
    for ( var idx=0; idx<deptObj.length; idx++) {
        var obj = deptObj[idx];
        if (class_id!=obj.class_id) continue;
        if (atrb_id && atrb_id!=obj.atrb_id) continue;
        cmb.options[cmb.options.length] = new Option(obj.dept_nm, obj.class_id+"_"+obj.atrb_id+"_"+obj.dept_id);
    }
}
var dept_win = null;
function tryApply() {
    var flg = getFlags();
    var ret = {};

    ret.rdo_all = "ref_list";
    if (flg.rdo_flg_all_ok) ret.rdo_all = "all_ok";
    if (flg.rdo_flg_all_ng) ret.rdo_all = "all_ng";

    ret.rdo_sub = "ref_both";
    if (!flg.rdo_ref_list_both && flg.rdo_ref_list_by) ret.rdo_sub = "ref_cad_st";
    if (!flg.rdo_ref_list_both && flg.rdo_ref_list_emp) ret.rdo_sub = "ref_emp";

    ret.rdo_cad = "all_ok";
    if (flg.rdo_cad_ref_list) ret.rdo_cad = "ref_list";

    ret.rdo_st = "all_ok";
    if (flg.rdo_st_ref_list) ret.rdo_st = "ref_list";

    ret.ary_cad = [];
    var cmb = ee("cmb_cad");
    if (ret.rdo_all=="ref_list" && (ret.rdo_sub=="ref_both" || ret.rdo_sub=="ref_cad_st") && ret.rdo_cad=="ref_list") {
        for (var idx=0; idx<cmb.options.length; idx++) {
            if (cmb.options[idx].value) ret.ary_cad.push(cmb.options[idx].value);
        }
        if (!ret.ary_cad.length) return alert("部署を指定してください。\n部署を指定しない場合は、「全部署」を選択してください。");
    }

    ret.ary_st = [];
    var cmb = ee("cmb_st");
    if (ret.rdo_all=="ref_list" && (ret.rdo_sub=="ref_both" || ret.rdo_sub=="ref_cad_st") && ret.rdo_st=="ref_list") {
        for (var idx=0; idx<cmb.options.length; idx++) {
            if (cmb.options[idx].selected) ret.ary_st.push(cmb.options[idx].value);
        }
        if (!ret.ary_st.length) return alert("役職を指定してください。\n役職を指定しない場合は、「全役職」を選択してください。");
    }

    ret.ary_emp = [];
    if (ret.rdo_all=="ref_list" && (ret.rdo_sub=="ref_both" || ret.rdo_sub=="ref_emp")) {
        var ary = ee("target_id_list1").value.split(",");
        for (var idx=0; idx<ary.length; idx++) {
            if (ary[idx]=="") continue;
            ret.ary_emp.push(ary[idx]);
        }
        if (!ret.ary_emp.length) return alert("職員を指定してください。\n職員を指定しない場合は、「部署役職指定」を選択してください。");
    }

    closeEmployeeList();
    <? if ($callback_func) echo "opener.".$callback_func."(ret);" ?>

}
function getFlags(elem) {
    var flg = {};
    flg.rdo_flg_all_ok = ee("rdo_all_all_ok").checked;
    flg.rdo_flg_all_ng = ee("rdo_all_all_ng").checked;
    flg.rdo_all_ref_list = ee("rdo_all_ref_list").checked; //詳細指定
    flg.rdo_ref_list_by = ee("rdo_ref_list_by").checked; // 部署役職指定
    flg.rdo_ref_list_emp = ee("rdo_ref_list_emp").checked; // 職員指定
    flg.rdo_ref_list_both = ee("rdo_ref_list_both").checked; // 部署役職＋職員指定
    flg.rdo_cad_ref_list = ee("rdo_cad_ref_list").checked; // 部署指定する
    flg.rdo_st_ref_list = ee("rdo_st_ref_list").checked; // 役職すべて
    if (elem && elem.id=="rdo_st_all_ok"   && !flg.rdo_cad_ref_list) { ee("rdo_cad_ref_list").checked = true; flg.rdo_cad_ref_list = true; }
    if (elem && elem.id=="rdo_cad_all_ok" && !flg.rdo_st_ref_list)   { ee("rdo_st_ref_list").checked = true;   flg.rdo_st_ref_list = true; }
    if (!flg.rdo_cad_ref_list && !flg.rdo_st_ref_list) {
        flg.rdo_cad_ref_list = true;
        flg.rdo_st_ref_list = true;
        ee("rdo_cad_ref_list").checked = true;
        ee("rdo_st_ref_list").checked = true;
    }

    if (!flg.rdo_all_ref_list) { flg.rdo_ref_list_by = false; flg.rdo_ref_list_emp = false; flg.rdo_ref_list_both = false; }
    if (flg.rdo_ref_list_both) { flg.rdo_ref_list_by = true;  flg.rdo_ref_list_emp = true; }
    return flg;
}

function setDisabled(elem) {
    var flg = getFlags(elem);
    ee("span_by_emp_ref_list").style.display = (flg.rdo_all_ref_list ? "" : "none");
    ee("span_cad_detail").style.display =       (flg.rdo_ref_list_by ? "" : "none");
    ee("span_st_detail").style.display =         (flg.rdo_ref_list_by ? "" : "none");
    ee("div_dept").style.display =               (flg.rdo_ref_list_by && flg.rdo_cad_ref_list ? "" : "none");
    ee("div_st").style.display =                 (flg.rdo_ref_list_by && flg.rdo_st_ref_list ? "" : "none");
    ee("span_emp_detail").style.display =        (flg.rdo_ref_list_emp && flg.rdo_ref_list_emp ? "" : "none");
    ee("target_disp_area1").style.display =      (flg.rdo_ref_list_emp && flg.rdo_ref_list_emp ? "" : "none");
}

function cadAdd(isAll) {
    var ref_dept_id = ee("ref_dept_id");
    var cmb_cad = ee("cmb_cad");

    var current = {};
    for (var idx=0; idx<cmb_cad.options.length; idx++) {
        current[cmb_cad.options[idx].value] = 1;
    }
    var adds = [];
    for (var idx=0; idx<ref_dept_id.options.length; idx++) {
        var opt = ref_dept_id.options[idx];
        if (current[opt.value]) continue; // 追加済。スルー
        if (!isAll && !opt.selected) continue;
        adds.push(opt.value);
    }
    for (var idx=0; idx<adds.length; idx++) {
        var cad_id = adds[idx];
        var index = deptIndexes[cad_id];
        var obj = deptObj[index];
        var disp = obj.class_nm+"＞"+obj.atrb_nm+"＞"+obj.dept_nm;
        cmb_cad.options[cmb_cad.options.length] = new Option(disp, adds[idx]);
    }
}

function cadDel(isAll) {
    var cmb_cad = ee("cmb_cad");
    var len = cmb_cad.options.length;
    for (var idx=len-1; idx>=0; idx--) {
        var opt = cmb_cad.options[idx];
        if (!isAll && !opt.selected) continue;
        cmb_cad.removeChild(opt);
    }
}


var childwin = null;
function openEmployeeList() {
    dx = screen.availWidth - 10;
    wx = 720;
    var url = '../emplist_popup.php?session=<?=C2_SESSION_ID?>&emp_id=<?=C2_LOGIN_EMP_ID?>&mode=1&item_id=1';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top=0,width=720,height=600,scrollbars=yes,resizable=yes');
    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) childwin.close();
    childwin = null;
}

// クリア
function clear_target() {
    if(!confirm("登録対象者を削除します。よろしいですか？")) return;
    var is_exist = false;
    for(var i=0;i<m_target_list[1].length;i++) {
        if("<?=C2_LOGIN_EMP_ID?>" == m_target_list[1][i].emp_id) { is_exist = true; break; }
    }
    m_target_list[1] = new Array();
    if (is_exist) m_target_list[1] = array_add(m_target_list[1], new user_info(emp_id,'<?=hh(C2_LOGIN_EMP_LT_NAME." ".C2_LOGIN_EMP_FT_NM)?>'));
    update_target_html(1);
}
function loadInit() {
    <? if ($onload_func) { ?>
    var init = opener.<?=$onload_func?>();

    if (init.rdo_all=="all_ok") ee("rdo_all_all_ok").checked = true;
    else if (init.rdo_all=="all_ng") ee("rdo_all_all_ng").checked = true;
    else ee("rdo_all_ref_list").checked = true;

    if (init.rdo_sub=="ref_cad_st") ee("rdo_ref_list_by").checked = true;
    else if (init.rdo_sub=="ref_emp") ee("rdo_ref_list_emp").checked = true;
    else ee("rdo_ref_list_both").checked = true;

    if (init.rdo_cad=="all_ok") ee("rdo_cad_all_ok").checked = true;
    else ee("rdo_cad_ref_list").checked = true;

    if (init.rdo_st=="all_ok") ee("rdo_st_all_ok").checked = true;
    else ee("rdo_st_ref_list").checked = true;

    if (init.ary_cad) {
        var cmb = ee("cmb_cad");
        for (var idx=0; idx<init.ary_cad.length; idx++) {
            var cad_id = init.ary_cad[idx];
            if (!cad_id) continue;
            var deptIdx = deptIndexes[cad_id];
            var obj = deptObj[deptIdx];
            cmb.options[cmb.options.length] = new Option(obj.class_nm+"＞"+obj.atrb_nm+"＞"+obj.dept_nm, cad_id);
        }
    }

    if (init.ary_st) {
        var cmb = ee("cmb_st");
        for (var idx=0; idx<init.ary_st.length; idx++) {
            var st_id = init.ary_st[idx];
            if (!st_id) continue;
            for (var idx2=0; idx2<cmb.options.length; idx2++) {
                if (cmb.options[idx2].value==st_id) { cmb.options[idx2].selected = true; break; }
            }
        }
    }

    if (init.ary_emp) {
        for (var idx=0; idx<init.ary_emp.length; idx++) {
            add_target_list("1", init.ary_emp[idx], empmstNames[init.ary_emp[idx]]);
        }
    }
    <? } ?>
    setDisabled();
}

</script>

<?
require("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
<? if (!$callback_func) echo 'alert("画面呼出しが不正です。");'?>
</script>
</head>
<body style="padding:0; margin:5px" onload="loadInit()"><div id="CLIENT_FRAME_CONTENT">


<table class="dialog_title" cellspacing="0" cellpadding="0">
    <tr><th>閲覧権限者指定</th><td class="closebtn"><a href="javascript:void(0);" onclick="window.close();"></a></td></tr>
</table>



<form name="frm" method="post" action="extadm_auth_sel_dlg.php">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="" />
<input type="hidden" id="target_name_list1" name="target_name_list1" value="" />


<div style="padding:5px 0 0 0">
<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse"><tr>
<td>
    <label><input type="radio" name="rdo_all" id="rdo_all_all_ok" onclick="setDisabled()" checked>全職員許可</label>
    <label><input type="radio" name="rdo_all" id="rdo_all_all_ng" onclick="setDisabled()">全職員不許可</label>
    <label><input type="radio" name="rdo_all" id="rdo_all_ref_list" onclick="setDisabled()">詳細指定</label>
    <span style="display:" id="span_by_emp_ref_list">⇒
        （<label><input type="radio" name="rdo_ref_list" id="rdo_ref_list_by" onclick="setDisabled()">部署役職指定</label>
        <label><input type="radio" name="rdo_ref_list" id="rdo_ref_list_emp" onclick="setDisabled()">職員指定</label>
        <label><input type="radio" name="rdo_ref_list" id="rdo_ref_list_both" onclick="setDisabled()" checked>部署役職＋職員指定</label>
        ）
    </span>
</td>
<td style="text-align:right; vertical-align:middle">
<button type="button" onclick="tryApply()">ＯＫ</button>
</td>
</tr></table>
</div>


<div style="margin-top:10px">

<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse">

    <tr>
        <td style="background-color:#eeeeee; border:1px solid #cccccc; vertical-align:top; padding:5px; height:250px">

            <div style="padding:0 5px 5px 5px; line-height:22px"><nobr>
                <span style="padding-right:20px"><b>■ 許可する部署</b></span>
                <span id="span_cad_detail">
                <label><input type="radio" name="rdo_cad" id="rdo_cad_all_ok" <? if ($ref_dept_flg!="all_ok") {echo(" checked");} ?>
                    onclick="setDisabled(this);">全部署</label>
                <label><input type="radio" name="rdo_cad" id="rdo_cad_ref_list" <? if ($ref_dept_flg=="ref_list") {echo(" checked");} ?>
                    onclick="setDisabled();" checked>指定する</label></nobr>
                </span>
            </div>

            <div id="div_dept">
            <table cellspacing="2" cellpadding="0">
                <tr>
                    <td valign="bottom" style="position:relative;top:3px;">≪可能とする<?=hh($classnameRow["class_nm"])?>≫</td>
                    <td></td>
                    <td>
                        <select id="ref_class_id" onchange="classChanged();">
                            <option value="">----------</option>
                            <? foreach ($classmstRows as $row) { ?>
                                <option value="<?=hh($row["class_id"])?>"><?=hh($row["class_nm"])?></option>
                            <? } ?>
                        </select><?=hh($classnameRow["class_nm"])?><br>
                        <div style="padding:2px 0">
                        <select id="ref_atrb_id" onchange="atrbChanged();">
                            <option value="">----------</option>
                            <? foreach ($atrbmstRows as $row) { ?>
                                <? if (true) continue; ?>
                                <option value="<?=hh($row["atrb_id"])?>"><?=hh($row["atrb_nm"])?></option>
                            <? } ?>
                        </select><?=hh($classnameRow["atrb_nm"])?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?//--------------------------------------------------------- ?>
                        <?// 可能とする施設                                           ?>
                        <?//--------------------------------------------------------- ?>
                        <select id="cmb_cad" size="10" multiple style="width:350px;">
                        </select>
                    </td>

                    <?//--------------------------------------------------------- ?>
                    <?// 部署の候補リスト                                         ?>
                    <?//--------------------------------------------------------- ?>
                    <td align="center">
                        <button type="button" onclick="cadAdd();"> &lt; </button><br>
                        <br>
                        <button type="button" onclick="cadDel()"> &gt; </button>
                    </td>
                    <td>
                        <select id="ref_dept_id" size="10" multiple style="width:170px;">
                            <?  foreach ($deptmstRows as $row) { ?>
                                <? if (true) continue; ?>
                                <option value="<?=hh($row["dept_id"])?>"><?=hh($row["dept_nm"])?></option>
                            <? } ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td><button type="button" name="delete_all_ref_dept" onclick="cadDel(1);">全て消去</button></td>
                    <td></td>
                    <td><button type="button"
                        onclick="var opts=ee('ref_dept_id').options; for(var idx=0; idx<opts.length; idx++) opts[idx].selected=true;">全て選択</button></td>
                </tr>
            </table>
            </div>
        </td>

        <td style="width:40px; padding:5px;text-align:center"><nobr><b>かつ</b></nobr></td>


        <td style="background-color:#eeeeee; border:1px solid #cccccc; vertical-align:top; width:290px; padding:5px">

            <div style="padding:0 5px 5px 5px; line-height:22px"><nobr>
                <span style="padding-right:20px"><b>■ 許可する役職</b></span>
                <span id="span_st_detail">
                <label><input type="radio" name="rdo_st" id="rdo_st_all_ok"<? if ($ref_st_flg!="ref_list") {echo(" checked");} ?>
                    onclick="setDisabled(this);">全役職</label>
                <label><input type="radio" name="rdo_st" id="rdo_st_ref_list"<? if ($ref_st_flg=="ref_list") {echo(" checked");} ?>
                    onclick="setDisabled();" checked>指定する</label></nobr>
                </span>
            </div>

            <div id="div_st">
                <select id="cmb_st" size="14" multiple>
                <? foreach ($stmstNames as $st_id => $st_nm) { ?>
                    <option value="<?=$st_id?>"><?=hh($st_nm)?></option>
                <? } ?>
                </select>
            </div>
        </td>
    </tr>
</table>
</div>



<div style="text-align:center; color:#aaaaaa; margin-top:10px; height:20px">
    <div>
    <nobr>
        <?=str_repeat("- ", 30)?>
        <span style="color:#000000; font-weight:bold">または</span>
        <?=str_repeat("- ", 30)?>
    </nobr>
    </div>
</div>



<div style="margin-top:10px">
    <table width="100%" cellspacing="0" cellpadding="0"><tr>
    <td style="background-color:#eeeeee; border:1px solid #cccccc; vertical-align:top; padding:5px; height:125px">

        <div style="padding:0 5px 5px 5px; line-height:24px">
            <span style="padding-right:20px"><b>■ 許可する職員</b></span>
            <span id="span_emp_detail">
            <button type="button" name="emplist1" style="width:80px;" onclick="openEmployeeList();">職員名簿</button>
            <button type="button" name="emplist1_clear" style="width:80px;" onclick="clear_target();">クリア</button>
            </span>
        </div>

        <div style="border:1px solid #777777; background-color:#ffffff; height:80px; padding:5px" id="target_disp_area1"></div>
    </td></tr></table>
</div>




</form>

</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>