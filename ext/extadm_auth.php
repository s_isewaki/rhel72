<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("library_common.php");

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$efRows = getEmpmstExtFieldRows();

$classmstRows = c2dbGetRows("select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no");// 部門一覧
$atrbmstRows = c2dbGetRows("select class_id as pid, atrb_id as cid, atrb_nm as nm from atrbmst where atrb_del_flg = 'f' order by class_id, order_no");// 課一覧
$deptmstRows = c2dbGetRows("select atrb_id as pid, dept_id as cid, dept_nm as nm from deptmst where dept_del_flg = 'f' order by atrb_id, order_no");// 科一覧


//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 | </title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body><div id="CLIENT_FRAME_CONTENT">




<?=$admin_pankuzu?>





<?
// ログインユーザの職員ID・所属部署IDを取得
$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_class = pg_fetch_result($sel, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel, 0, "emp_dept");
$emp_st = pg_fetch_result($sel, 0, "emp_st");
$emp_name = pg_fetch_result($sel, 0, "emp_name");

// 部門一覧を取得
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
$classes = array();
$classes["-"] = "----------";
while ($row = pg_fetch_array($sel_class)) {
    $tmp_class_id = $row["class_id"];
    $tmp_class_nm = $row["class_nm"];
    $classes[$tmp_class_id] = $tmp_class_nm;
}
pg_result_seek($sel_class, 0);



// 課一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.order_no, atrbmst.order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);

// 科一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.order_no, atrbmst.order_no, deptmst.order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept)) {
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_dept_id = $row["dept_id"];
    $tmp_dept_nm = "{$row['class_nm']}＞{$row['atrb_nm']}＞{$row['dept_nm']}";
    $dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// 初期表示時の設定
if (!is_array($ref_dept)) {$ref_dept = array();}
if (!is_array($ref_st)) {$ref_st = array();}
if (!is_array($upd_dept)) {$upd_dept = array();}
if (!is_array($upd_st)) {$upd_st = array();}


// 書庫名を取得
$archive_nm = lib_get_archive_name($arch_id);

// カテゴリ名を取得
if ($back != "t") $parent_cate_id = $cate_id;
$parent_cate_nm = lib_get_category_name($con, $parent_cate_id, $fname);

// 親フォルダパスを配列に格納
if ($back != "t" && $folder_id != "") {
    $sql = "select parent_id from libtree";
    $cond = "where child_id = $folder_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if (pg_num_rows($sel) > 0) {
        $parent_folder_id = pg_fetch_result($sel, 0, "parent_id");
    }
}
if ($parent_folder_id != "") {
    $parent_path = lib_get_folder_path($con, $parent_folder_id, $fname);
} else {
    $parent_path = array();
}

// リンクフォルダのパスを取得
$link_archive_nm = lib_get_archive_name($link_arch_id);
$link_cate_nm = lib_get_category_name($con, $link_cate_id, $fname);
$link_folder_path = lib_get_folder_path($con, $link_id, $fname);

// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname);

// 文書数をカウントする
$count_flg = lib_get_count_flg($con, $fname);
if ($count_flg == "t") {
    $folder_id_array = array();
    $folder_id_array = array("folder_id" => $folder_id);
    if ($path == "3") {
        // 管理者権限の場合は、すべての文書をカウントする。
        $emp_id = "";
    }
    $doc_count = lib_get_doc_count($con, $fname, $emp_id, $arch_id, $cate_id, $folder_id_array);
}

if ($back != "t") {
    // フォルダが指定されている場合
    if ($folder_id != "") {
        // フォルダ情報を取得
        $sql = "select libfolder.*, libcate.lib_link_id from libfolder inner join libcate on libcate.lib_cate_id = libfolder.lib_cate_id";
        $cond = "where folder_id = $folder_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        $folder_name = pg_fetch_result($sel, 0, "folder_name");
        $folder_no = pg_fetch_result($sel, 0, "folder_no");
        $lib_link_id = pg_fetch_result($sel, 0, "lib_link_id");

    } else {
    // カテゴリの場合
        $sql = "select * from libcate";
        $cond = "where lib_cate_id = $cate_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        $folder_name = pg_fetch_result($sel, 0, "lib_cate_nm");
        $folder_no = pg_fetch_result($sel, 0, "lib_cate_no");
        $lib_link_id = pg_fetch_result($sel, 0, "lib_link_id");
    }

    // リンクフォルダのパスを取得
    if ($folder_id != "") {
        list($link_arch_id, $link_cate_id, $link_id) = lib_get_real_folder_info($con, $folder_id, $fname);
        if (!empty($link_id)) {
            $folder_type = 1;
            $link_archive_nm = lib_get_archive_name($link_arch_id);
            $link_cate_nm = lib_get_category_name($con, $link_cate_id, $fname);
            $link_folder_path = lib_get_folder_path($con, $link_id, $fname);
        }
    }

    // 権限情報を取得
    $private_flg = pg_fetch_result($sel, 0, "private_flg");
    $ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
    $ref_dept_flg = pg_fetch_result($sel, 0, "ref_dept_flg");
    $ref_st_flg = pg_fetch_result($sel, 0, "ref_st_flg");
    $upd_dept_st_flg = pg_fetch_result($sel, 0, "upd_dept_st_flg");
    $upd_dept_flg = pg_fetch_result($sel, 0, "upd_dept_flg");
    $upd_st_flg = pg_fetch_result($sel, 0, "upd_st_flg");

    switch ($arch_id) {
    case "1":  // 個人ファイル
    case "2":  // 全体共有
        $private_flg1 = "";
        $private_flg2 = "";
        break;
    case "3":  // 部署共有
        $private_flg1 = $private_flg;
        $private_flg2 = "";
        break;
    case "4":  // 委員会・WG共有
        $private_flg1 = "";
        $private_flg2 = $private_flg;
        break;
    }

    // フォルダが指定されている場合
    if ($folder_id != "") {
        $refdept_tblname = "libfolderrefdept";
        $refst_tblname = "libfolderrefst";
        $refemp_tblname = "libfolderrefemp";
        $upddept_tblname = "libfolderupddept";
        $updst_tblname = "libfolderupdst";
        $updemp_tblname = "libfolderupdemp";
        $id_name = "folder_id";
        $lib_id = $folder_id;
    } else {
        $refdept_tblname = "libcaterefdept";
        $refst_tblname = "libcaterefst";
        $refemp_tblname = "libcaterefemp";
        $upddept_tblname = "libcateupddept";
        $updst_tblname = "libcateupdst";
        $updemp_tblname = "libcateupdemp";
        $id_name = "lib_cate_id";
        $lib_id = $cate_id;
    }

    $sql = "select l.class_id, l.atrb_id, l.dept_id from $refdept_tblname l left join classmst c on l.class_id = c.class_id left join atrbmst a on l.atrb_id = a.atrb_id left join deptmst d on l.dept_id = d.dept_id";
    $cond = "where $id_name = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_ref_dept = select_from_table($con, $sql, $cond, $fname);
    while ($row = pg_fetch_array($sel_ref_dept)) {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $ref_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from $refst_tblname";
    $cond = "where $id_name = $lib_id order by st_id";
    $sel_ref_st = select_from_table($con, $sql, $cond, $fname);
    while ($row = pg_fetch_array($sel_ref_st)) {
        $tmp_st_id = $row["st_id"];
        $ref_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from $refemp_tblname";
    $cond = "where $id_name = $lib_id order by emp_id";
    $sel_ref_emp = select_from_table($con, $sql, $cond, $fname);
    $target_id_list1 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_ref_emp)) {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg) {
            $is_first_flg = false;
        } else {
            $target_id_list1 .= ",";
        }
        $target_id_list1 .= $tmp_emp_id;
    }

    $sql = "select l.class_id, l.atrb_id, l.dept_id from $upddept_tblname l left join classmst c on l.class_id = c.class_id left join atrbmst a on l.atrb_id = a.atrb_id left join deptmst d on l.dept_id = d.dept_id";
    $cond = "where $id_name = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_upd_dept = select_from_table($con, $sql, $cond, $fname);
    while ($row = pg_fetch_array($sel_upd_dept)) {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $upd_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from $updst_tblname";
    $cond = "where $id_name = $lib_id order by st_id";
    $sel_upd_st = select_from_table($con, $sql, $cond, $fname);
    while ($row = pg_fetch_array($sel_upd_st)) {
        $tmp_st_id = $row["st_id"];
        $upd_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from $updemp_tblname";
    $cond = "where $id_name = $lib_id order by emp_id";
    $sel_upd_emp = select_from_table($con, $sql, $cond, $fname);
    $target_id_list2 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_upd_emp)) {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg) {
            $is_first_flg = false;
        } else {
            $target_id_list2 .= ",";
        }
        $target_id_list2 .= $tmp_emp_id;
    }
}

// メンバー情報を配列に格納
$arr_target['1'] = array();
if ($target_id_list1 != "") {
    $arr_target_id = split(",", $target_id_list1);
    for ($i = 0; $i < count($arr_target_id); $i++) {
        $tmp_emp_id = $arr_target_id[$i];
        $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
        }
        $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
        array_push($arr_target['1'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
    }
}

$arr_target['2'] = array();
if ($target_id_list2 != "") {
    $arr_target_id = split(",", $target_id_list2);
    for ($i = 0; $i < count($arr_target_id); $i++) {
        $tmp_emp_id = $arr_target_id[$i];
        $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
        }
        $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
        array_push($arr_target['2'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
    }
}
// 更新権限確認
if ($path == "3" || $arch_id == "1") { // 管理画面か個人の場合
    $edit_button_disabled = false;
} else {
    $upd_flg = get_upd_flg($con, $fname, $arch_id, $cate_id, $folder_id, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);
    $edit_button_disabled = !$upd_flg;
}

// 部署のIDを登録されたIDとする
if ($arch_id == "3") {
    $emp_class = $lib_link_id;
}

// 部署、委員会で、上位カテゴリの場合は名称変更不可
$readonly = "";
if (($arch_id == "3" || $arch_id == "4") && ($folder_id == "")) {
    $readonly = " readOnly";
}

if (!isset($update_auth_recursively)) {
    $update_auth_recursively = "f";
}
?>
<?
require("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$emp_id?>';
    url += '&mode='+item_id;
    url += '&item_id='+item_id;
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
    for ($i=1; $i<=2; $i++) {
        $script = "m_target_list['$i'] = new Array(";
        $is_first = true;
        foreach($arr_target["$i"] as $row)
        {
            if($is_first)
            {
                $is_first = false;
            }
            else
            {
                $script .= ",";
            }
            $tmp_emp_id = $row["id"];
            $tmp_emp_name = $row["name"];
            $script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
        }
        $script .= ");\n";
        print $script;
    }
?>

// クリア
function clear_target(item_id, emp_id,emp_name) {
    if(confirm("登録対象者を削除します。よろしいですか？"))
    {
        var is_exist_flg = false;
        for(var i=0;i<m_target_list[item_id].length;i++)
        {
            if(emp_id == m_target_list[item_id][i].emp_id)
            {
                is_exist_flg = true;
                break;
            }
        }
        m_target_list[item_id] = new Array();
        if (is_exist_flg == true) {
            m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
        }
        update_target_html(item_id);
    }
}

var classes = [];
<?
while ($row = pg_fetch_array($sel_class)) {
    $tmp_class_id = $row["class_id"];
    $tmp_class_nm = $row["class_nm"];
    echo("classes.push({id: $tmp_class_id, name: '$tmp_class_nm'});\n");
}
pg_result_seek($sel_class, 0);
?>

var atrbs = {};
<?
$pre_class_id = "";
while ($row = pg_fetch_array($sel_atrb)) {
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_class_nm = $row["class_nm"];
    $tmp_atrb_nm = $row["atrb_nm"];

    if ($tmp_class_id != $pre_class_id) {
        echo("atrbs[$tmp_class_id] = [];\n");
    }

    echo("atrbs[$tmp_class_id].push({id: $tmp_atrb_id, name: '$tmp_atrb_nm'});\n");

    $pre_class_id = $tmp_class_id;
}
pg_result_seek($sel_atrb, 0);
?>

var depts = {};
<?
$pre_class_id = "";
$pre_atrb_id = "";
while ($row = pg_fetch_array($sel_dept)) {
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_dept_id = $row["dept_id"];
    $tmp_class_nm = $row["class_nm"];
    $tmp_atrb_nm = $row["atrb_nm"];
    $tmp_dept_nm = $row["dept_nm"];

    if ($tmp_class_id != $pre_class_id) {
        echo("depts[$tmp_class_id] = {};\n");
    }

    if ($tmp_class_id != $pre_class_id || $tmp_atrb_id != $pre_atrb_id) {
        echo("depts[$tmp_class_id][$tmp_atrb_id] = [];\n");
    }

    echo("depts[$tmp_class_id][$tmp_atrb_id].push({id: $tmp_dept_id, hierarchy: '{$tmp_class_nm}＞{$tmp_atrb_nm}＞', name: '$tmp_dept_nm'});\n");

    $pre_class_id = $tmp_class_id;
    $pre_atrb_id = $tmp_atrb_id;
}
pg_result_seek($sel_dept, 0);
?>

//----------

var ARCHIVE_ALL = '2';
var ARCHIVE_SECTION = '3';
var ARCHIVE_PROJECT = '4';
var ARCHIVE_PRIVATE = '1';

var dept_win = null;

function submitForm() {
    if (document.mainform.update_auth_recursively[1].checked) {
        if (!confirm('下位フォルダの権限も変更されます。本当によろしいですか？')) {
            return;
        }
    }

    var ref_dept_box = document.mainform.ref_dept;
    if (!ref_dept_box.disabled) {
        for (var i = 0, j = ref_dept_box.length; i < j; i++) {
            addHiddenElement(document.mainform, 'hid_ref_dept[]', ref_dept_box.options[i].value);
        }
    }

    var upd_dept_box = document.mainform.upd_dept;
    if (!upd_dept_box.disabled) {
        for (var i = 0, j = upd_dept_box.length; i < j; i++) {
            addHiddenElement(document.mainform, 'hid_upd_dept[]', upd_dept_box.options[i].value);
        }
    }

    document.mainform.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;
    document.mainform.upd_toggle_mode.value = document.getElementById('upd_toggle').innerHTML;

    closeEmployeeList();

    document.mainform.submit();
}

function selectAllOptions(box) {
    for (var i = 0, j = box.length; i < j; i++) {
        box.options[i].selected = true;
    }
}

function deleteSelectedOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        if (box.options[i].selected) {
            box.options[i] = null;
        }
    }
    if (!(box.options[0])) {
        box.style.width = '100%';
    } else {
        box.style.width = '';
    }
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
    if (box.name == 'upd_dept' || box.name == 'ref_dept') {
        box.style.width = '100%';
    }
}

function addHiddenElement(frm, name, value) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    frm.appendChild(input);
}

function closeDeptWin() {
    if (dept_win) {
        if (!dept_win.closed) {
            dept_win.close();
        }
        dept_win = null;
    }
}

function showDeptAll(div) {
    var emp_class = '';
    if (document.mainform.archive.value == ARCHIVE_SECTION) {
        if (div == 'upd' || document.mainform.private_flg1.checked) {
            emp_class = '<? echo($emp_class); ?>';
        }
    }

    dept_win = window.open('select_dept_all.php?session=<? echo($session); ?>&module=library&emp_class='.concat(emp_class).concat('&div=').concat(div), 'deptall', 'width=800,height=500,scrollbars=yes');
}

function selectLinkFolder() {
    var a = document.mainform.link_arch_id.value;
    var c = document.mainform.link_cate_id.value;
    var f = document.mainform.link_id.value;
    window.open('library_folder_select.php?type=link&session=<? echo($session); ?>&a=' + a + '&c=' + c + '&f=' + f + '&path=<? echo($path); ?>', 'newwin', 'width=640,height=700,scrollbars=yes');
}

function clearAllTargets(idx) {
    document.getElementById('target_disp_area'.concat(idx)).innerHTML = '';
    document.mainform.elements['target_id_list'.concat(idx)].value = '';
    document.mainform.elements['target_name_list'.concat(idx)].value = '';
}

function setProjectMembers() {
    var url = 'library_project_member_xml.php?session=<? echo($session); ?>&category='.concat(document.mainform.category.value);
    var callback = {
        success: function (o) {
            var ids = o.responseXML.getElementsByTagName('ID');
            var names = o.responseXML.getElementsByTagName('Name');
            for (var i = 0, j = ids.length; i < j; i++) {
                add_target_list('1', ids[i].firstChild.nodeValue, names[i].firstChild.nodeValue);
            }
        },
        failure: function () {alert('データの取得に失敗しました。');}
    };
    YAHOO.util.Connect.asyncRequest('GET', url, callback);

}

</script>




<form name="mainform" action="library_folder_update_exe.php" method="post">



<table width="100%" cellspacing="0" cellpadding="2" style="border-collapse:collapse">

	<tr height="22">
		<td class="with_border with_bg" rowspan="2" align="center">部署・役職<br>
			<label><input type="checkbox" name="ref_dept_st_flg" value="t"<? if ($ref_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</label>
		</td>
		<td class="with_border with_bg">
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td>部署</td>
					<td align="right"><input type="button" name="ref_dept_all" value="全画面" onclick="showDeptAll('ref');"></td>
				</tr>
			</table>
		</td>
		<td class="with_border with_bg">役職</td>
	</tr>

	<tr>
		<td class="with_border">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<td style="padding-right:20px;">
						<label><input type="radio" name="ref_dept_flg" value="1"<? if ($ref_dept_flg != "2") {echo(" checked");} ?> onclick="setDisabled();">すべて</label><br>
						<label><input type="radio" name="ref_dept_flg" value="2"<? if ($ref_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する</label>
					</td>
					<td>
						<table cellspacing="2" cellpadding="0">
							<tr>
								<td valign="bottom" style="position:relative;top:3px;">可能とする<? echo($arr_class_name[2]); ?></td>
								<td></td>
								<td>
									<select name="ref_class_src" onchange="setRefAtrbSrcOptions();">
									</select><? echo($arr_class_name[0]); ?><br>

									<select name="ref_atrb_src" onchange="setRefDeptSrcOptions();">
									</select><? echo($arr_class_name[1]); ?>
								</td>
							</tr>

							<tr>
								<td>
									<select name="ref_dept" size="6" multiple style="width:120px;">
									<?
									foreach ($ref_dept as $tmp_dept_id) {
									    echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
									}
									?>
									</select>
								</td>

								<td align="center">
									<input type="button" name="add_ref_dept" value=" &lt; "
										onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br>
										<br>
										<input type="button" name="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);">
								</td>
								<td>
									<select name="ref_dept_src" size="6" multiple style="width:120px;">
									</select>
								</td>
							</tr>

							<tr>
								<td><input type="button" name="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);"></td>
								<td></td>
								<td><input type="button" name="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>

		<td colspan="2" class="with_border">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<td style="padding-right:20px;">
						<label><input type="radio" name="ref_st_flg" value="1"<? if ($ref_st_flg != "2") {echo(" checked");} ?> onclick="setDisabled();">すべて</label><br>
						<label><input type="radio" name="ref_st_flg" value="2"<? if ($ref_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する</label>
					</td>

					<td>
						<select name="ref_st[]" size="10" multiple>
						<?
						while ($row = pg_fetch_array($sel_st)) {
						    $tmp_st_id = $row["st_id"];
						    $tmp_st_nm = $row["st_nm"];
						    echo("<option value=\"$tmp_st_id\"");
						    if (in_array($tmp_st_id, $ref_st)) {
						        echo(" selected");
						    }
						    echo(">$tmp_st_nm");
						}
						pg_result_seek($sel_st, 0);
						?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr height="22">
		<th align="center" class="with_border with_bg">職員<br>
			<input type="button" name="emplist1" value="職員名簿" style="width:5.5em;" onclick="openEmployeeList('1');"><br>
			<input type="button" name="emplist1_clear" value="クリア" style="width:5.5em;" onclick="clear_target('1','<?=$emp_id?>','<?=$emp_name?>');">
		</th>
		<td colspan="2" class="with_border">
			<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="non_in_list">
				<tr>
					<td width="350" height="60" style="border:#5279a5 solid 1px;">
						<span id="target_disp_area1"></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="archive" value="<? echo($arch_id); ?>">
<input type="hidden" name="arch_id" value="<? echo($arch_id); ?>">
<input type="hidden" name="category" value="<? echo($cate_id); ?>">
<input type="hidden" name="cate_id" value="<? echo($cate_id); ?>">
<input type="hidden" name="folder_id" value="<? echo($folder_id); ?>">
<input type="hidden" name="o" value="<? echo($o); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="ref_toggle_mode" value="">
<input type="hidden" name="upd_toggle_mode" value="">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
<input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
<input type="hidden" id="target_name_list2" name="target_name_list2" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>