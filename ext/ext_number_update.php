<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にext権限(49)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

// ログインユーザの連絡先情報を取得
$empmstRow = c2dbGetTopRow("select emp_phs, emp_ext, emp_mobile1, emp_mobile2, emp_mobile3 from empmst where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID));



//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
    $empmstRow = $_REQUEST;
    // アクセスログ
    require_once("aclg_set.php");
    aclg_regist($session, "extension_number_update_exe.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

    // 入力チェック
    if (strlen($empmstRow["emp_phs"]) > 6) $errmsg[]= getPhsName()."が長すぎます。";// 院内PHS/所内PHS
    if (strlen($empmstRow["emp_ext"]) > 10) $errmsg[]= '内線番号が長すぎます。';
    if (strlen($empmstRow["emp_mobile1"]) > 6) $errmsg[]= '携帯端末（1）が長すぎます。';
    if (strlen($empmstRow["emp_mobile2"]) > 6) $errmsg[]= '携帯端末（2）が長すぎます。';
    if (strlen($empmstRow["emp_mobile3"]) > 6) $errmsg[]= '携帯端末（3）が長すぎます。';
    if (!allOrNothing($empmstRow["emp_mobile1"], $empmstRow["emp_mobile2"], $empmstRow["emp_mobile3"])) $errmsg[]= '携帯端末を正しく入力してください。';

    // エラーがなければ職員情報を更新
    if (!count($errmsg)) {
        $sql =
        " update empmst set".
        " emp_phs = ".c2dbStr($empmstRow["emp_phs"]).
        ",emp_ext = ".c2dbStr($empmstRow["emp_ext"]).
        ",emp_mobile1 = ".c2dbStr($empmstRow["emp_mobile1"]).
        ",emp_mobile2 = ".c2dbStr($empmstRow["emp_mobile2"]).
        ",emp_mobile3 = ".c2dbStr($empmstRow["emp_mobile3"]).
        " where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID);
        c2dbExec($sql);
	    ob_clean();
        header("Location: ext_number_update.php");
        die;
    }
}



//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 番号変更</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">




<div class="window_title">
    <table cellspacing="0" cellpadding="0">
    <tr>
    <th>
        <?=$intra_pankuzu_html?>
        <a class="pankuzu2" href="ext_number_update.php">番号変更</a>
    </th>
    <? if ($EAL[SELF_ADMIN_AUTH_FLG] == "1") { ?>
     <td><a href="extadm_menu.php">管理画面へ</a></td>
    <? } ?>
    </tr>
    </table>
</div>


<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="ext_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="ext_search.php">番号検索</a></th>
    <th class="current"><a href="ext_number_update.php">番号変更</a></th>
    <? if ($extadmoptRow["newcomer_flg"]!="f") { ?>
    <th class="selectable"><a href="ext_newcomer.php"><nobr>今月の新人</nobr></a></th>
    <? } ?>
    <th class="selectable"><a href="ext_option.php">オプション</a></th>
    </tr></table>
</div>




<form action="ext_number_update.php" method="post">
<input type="hidden" name="try_update" value="1" />
<div style="padding-top:5px; width:600px">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="140"><?=getPhsName()?></th>
        <td><input type="text" name="emp_phs" value="<?=$empmstRow["emp_phs"]?>" size="10" maxlength="6" style="ime-mode:inactive;"></td>
    </tr>

    <tr>
        <th>内線番号</th>
        <td><input type="text" name="emp_ext" value="<?=$empmstRow["emp_ext"]?>" size="10" maxlength="10" style="ime-mode:inactive;"></td>
    </tr>

    <tr>
        <th>携帯端末・外線</th>
        <td>
            <input type="text" name="emp_mobile1" value="<?=$empmstRow["emp_mobile1"]?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="emp_mobile2" value="<?=$empmstRow["emp_mobile2"]?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="emp_mobile3" value="<?=$empmstRow["emp_mobile3"]?>" size="5" maxlength="6" style="ime-mode:inactive;">
        </td>
    </tr>
</table>

<div style="text-align:right; padding-top:2px"><input type="submit" value="更新"></div>

</div>

</form>



</div><!-- // CLIENT_FRAME_CONTENT --></body>
</html>
