<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(49)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$year  = str_pad((int)$_REQUEST["year"],  4, "0", STR_PAD_LEFT);
$month = str_pad((int)$_REQUEST["month"], 2, "0", STR_PAD_LEFT);
if ($year  == "0000") $year = date("Y");
if ($month == "00")   $month = date("m"); // ゼロ埋め2ケタ


// 入職日が当該年月の職員一覧を取得
$sql =
" select".
" empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_profile".
",jobmst.job_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm".
" from empmst".
" inner join jobmst on jobmst.job_id = empmst.emp_job".
" inner join classmst on classmst.class_id = empmst.emp_class".
" inner join atrbmst on atrbmst.atrb_id = empmst.emp_attribute".
" inner join deptmst on deptmst.dept_id = empmst.emp_dept".
" left join classroom on classroom.room_id = empmst.emp_room".
" where empmst.emp_join like '".$year.$month."__'".
" and exists (select * from authmst where authmst.emp_id = empmst.emp_id and emp_del_flg = 'f')".
" order by empmst.emp_keywd, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_id";
$rows = c2dbGetRows($sql);
$empmstRows_div4 = array_chunk($rows, 4);


//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 今月の新人</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<div class="window_title">
    <table cellspacing="0" cellpadding="0">
    <tr>
    <th>
        <?=$intra_pankuzu_html?>
        <a class="pankuzu2" href="ext_newcomer.php">今月の新人</a>
    </th>
    <? if ($EAL[SELF_ADMIN_AUTH_FLG] == "1") { ?>
     <td><a href="extadm_menu.php">管理画面へ</a></td>
    <? } ?>
    </tr>
    </table>
</div>


<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="ext_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="ext_search.php">番号検索</a></th>
    <th class="selectable"><a href="ext_number_update.php">番号変更</a></th>
    <th class="current"><a href="ext_newcomer.php"><nobr>今月の新人</nobr></a></th>
    <th class="selectable"><a href="ext_option.php">オプション</a></th>
    </tr></table>
</div>





<div style="padding-top:5px">




<table border="0" cellspacing="0" cellpadding="2">
    <tr height="22">
        <td>
        <a href="ext_newcomer.php?year=<?=($year-1)?>&month=<?=$month?>" style="margin-right:6px;">←前年</a>
        <strong><?=$year?>年</strong>
        <a href="ext_newcomer.php?year=<?=($year+1)?>&month=<?=$month?>" style="margin-left:6px;">翌年→</a>
        </td>
    </tr>

    <tr height="22">
        <td>
        <?
        for ($i = 1; $i <= 12; $i++) {
            if ($i == intval($month)) {
                echo '<span style="margin-right:6px;"><strong>'.$i.'月</strong></span>'."\n";
            } else {
                echo '<a href="ext_newcomer.php?year='.$year.'&month='.sprintf("%02d", $i).'" style="margin-right:6px;">'.$i.'月</a>'."\n";
            }
        }
        ?>
        </td>
    </tr>
</table>


<table border="0" cellspacing="0" cellpadding="0">
<? foreach ($empmstRows_div4 as $empmstRows) { ?>
    <tr valign="top">
    <? $col = 0; ?>
    <? foreach ($empmstRows as $row) { ?>
        <? $col++; if ($col==5) $col = 1; ?>
        <td width="25%" style="padding:20px 20px 0 0;">
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr valign="top">
                    <td colspan="2">
                        <div style="width:150px; height:186px; padding-bottom:3px; position:relative">
                        <?
                            $photoInfo = getPhotoInfo($row["emp_id"]);
                            echo $photoInfo["image_viewer_imgtag2"];
                        ?>
                        </div>
                    </td>
                </tr>
                <tr valign="top">
                    <td width="36">氏名</td>
                    <td><?=hh($row["emp_lt_nm"] . " " . $row["emp_ft_nm"])?></td>
                </tr>
                <tr valign="top">
                    <td>職種</td>
                    <td><?=hh($row["job_nm"])?></td>
                </tr>
                <tr valign="top">
                    <td>所属</td>
                    <td style="height:30px">
                        <nobr><?=hh($row["class_nm"])?></nobr>
                        ＞
                        <nobr><?=hh($row["atrb_nm"])?></nobr>
                        ＞
                        <nobr><?=hh($row["dept_nm"])?></nobr>
                        <? if ($row["room_nm"]) { ?>
                        ＞
                        <? } ?>
                        <nobr><?=hh($row["room_nm"])?></nobr>
                    </td>
                </tr>
                <tr valign="top">
                    <td colspan="2" style="padding-top:8px">プロフィール</td>
                </tr>
                <tr valign="top" style="background:url(../spf/img/bk_emp.gif) repeat-x">
                    <td colspan="2" style="padding:4px; height:50px; color:#666666; line-height:1.4">
                        <?=str_replace("\n", "<br>", $row["emp_profile"])?>
                    </td>
                </tr>
            </table>
        </td>
    <? } ?>
    <? for ($idx=$col+1; $idx<=4; $idx++) { ?>
    <td width="25%" style="padding:20px 20px 0 0;"></td>
    <? } ?>
    </tr>
<? } ?>
</table>




</div>
</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>
