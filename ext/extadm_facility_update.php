<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(50)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");



$extfcl_id  = (int)$_REQUEST["extfcl_id"]; // カラかもしれない
$filemode = $_REQUEST["filemode"];

// 施設連絡先情報を取得
$extfclRow = c2dbGetTopRow("select fcl_nm, fcl_tel1, fcl_tel2, fcl_tel3, fcl_fax1, fcl_fax2, fcl_fax3 from extfcl where extfcl_id = ".(int)$extfcl_id);

$paths = glob("./extension/fcl/".$extfcl_id.".*");
$path = (count($paths) > 0) ? $paths[0] : "";



//―――――――――――――――――――――――――――――――――――――――――――――――――
// 更新の場合
//―――――――――――――――――――――――――――――――――――――――――――――――――
$errmsg = array();
if ($_REQUEST["try_update"]) {
    $extfclRow = $_REQUEST;
    // アクセスログ
    require_once("aclg_set.php");
    aclg_regist($session, "extension_admin_facility_update_exe.php", $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_FILES);

    //=============================
    // 入力チェック
    //=============================
    if ($extfclRow["fcl_nm"] == "") $errmsg[]= '施設名が入力されていません。';
    if (strlen($extfclRow["fcl_nm"]) > 60) $errmsg[]= '施設名が長すぎます。';
    if (strlen($extfclRow["fcl_tel1"]) > 6) $errmsg[]= '電話番号（代表）（1）が長すぎます。';
    if (strlen($extfclRow["fcl_tel2"]) > 6) $errmsg[]= '電話番号（代表）（2）が長すぎます。';
    if (strlen($extfclRow["fcl_tel3"]) > 6) $errmsg[]= '電話番号（代表）（3）が長すぎます。';
    if (!allOrNothing($extfclRow["fcl_tel1"], $extfclRow["fcl_tel2"], $extfclRow["fcl_tel3"])) $errmsg[]= '電話番号（代表）を正しく入力してください。';
    if (strlen($extfclRow["fcl_fax1"]) > 6) $errmsg[]= 'FAX（1）が長すぎます。';
    if (strlen($extfclRow["fcl_fax2"]) > 6) $errmsg[]= 'FAX（2）が長すぎます。';
    if (strlen($extfclRow["fcl_fax3"]) > 6) $errmsg[]= 'FAX（3）が長すぎます。';
    if (!allOrNothing($extfclRow["fcl_fax1"], $extfclRow["fcl_fax2"], $extfclRow["fcl_fax3"])) $errmsg[]= 'FAXを正しく入力してください。';

    //=============================
    // ファイル名チェック
    //=============================
    $filename = $_FILES["file"]["name"];
    if ($filemode == "" && $filename != "") $filemode = "renew";
    if ($filemode == "renew") {
        if ($filename == "") $errmsg[]= 'ファイルが選択されていません。'; // リストを更新する場合
        else {
            $ferr = $_FILES["file"]["error"];
            if ($ferr==1 || $ferr==2) $errmsg[]= 'ファイルサイズが大きすぎます。';
            if ($ferr==3 || $ferr==4) $errmsg[]= 'アップロードに失敗しました。再度実行してください。';
        }
    }

    //=============================
    // チェックＯＫなら、施設連絡先情報を更新
    //=============================
    if (!count($errmsg)) {
        $sql =
        " update extfcl set".
        " fcl_nm = ".c2dbStr($extfclRow["fcl_nm"]).
        ",fcl_tel1 = ".c2dbStr($extfclRow["fcl_tel1"]).
        ",fcl_tel2 = ".c2dbStr($extfclRow["fcl_tel2"]).
        ",fcl_tel3 = ".c2dbStr($extfclRow["fcl_tel3"]).
        ",fcl_fax1 = ".c2dbStr($extfclRow["fcl_fax1"]).
        ",fcl_fax2 = ".c2dbStr($extfclRow["fcl_fax2"]).
        ",fcl_fax3 = ".c2dbStr($extfclRow["fcl_fax3"]).
        " where extfcl_id = ".(int)$extfcl_id;
        c2dbExec($sql);

        $doc_ext = (strpos($filename, ".")) ? preg_replace("/^.*\.([^.]*)$/", "$1", $filename) : "txt"; // 拡張子の設定
        // 番号リストの処理
        if (!is_dir("extension")) mkdir("extension", 0755);
        if (!is_dir("extension/fcl")) mkdir("extension/fcl", 0755);
        if ($filemode == "renew") {  // リストを更新する場合
            exec("rm -f " . dirname(dirname(__FILE__)) . "/extension/fcl/$extfcl_id.*");
            copy($_FILES["file"]["tmp_name"], "extension/fcl/$extfcl_id.$doc_ext");
        } else if ($filemode == "dispose") {  // リストを削除する場合
            exec("rm -f " . dirname(dirname(__FILE__)) . "/extension/fcl/".$extfcl_id.".*");
        }

        // 一覧画面に遷移
	    ob_clean();
        header("Location: extadm_menu.php?view_type=fcl&page=".$page);
        die;
    }
}
if ($filemode == "") $filemode = "stay";

//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 管理画面 | 施設連絡先更新</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
function setFileMode() {
    if (document.mainform.filemode) {
        document.mainform.file.disabled = !document.mainform.filemode[1].checked;
    }
}
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT" onload="setFileMode();">




<?=$admin_pankuzu?>



<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="extadm_menu.php">番号一覧</a></th>
    <th class="selectable"><a href="extadm_search.php">番号検索</a></th>
    <th class="current w120"><a href="extadm_facility_update.php?&extfcl_id=<?=$extfcl_id?>&page=<?=$page?>"><nobr>施設連絡先更新</nobr></a></th>
    <th class="selectable w120"><a href="extadm_class_register.php"><nobr>部門連絡先登録</nobr></a></th>
    <th class="selectable w120"><a href="extadm_document_register.php"><nobr>番号リスト登録</nobr></a></th>
    <th class="selectable"><a href="extadm_option.php">オプション</a></th>
    </tr></table>
</div>





<form name="mainform" action="extadm_facility_update.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="extfcl_id" value="<?=$extfcl_id?>">
<input type="hidden" name="page" value="<?=$page?>">
<input type="hidden" name="try_update" value="1">


<div style="padding-top:5px; width:600px">


<table width="100%" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="140">施設名</th>
        <td><input type="text" name="fcl_nm" value="<?=hh($extfclRow["fcl_nm"])?>" size="40" maxlength="60" style="ime-mode:active;"></td>
    </tr>
    <tr>
        <th>電話番号（代表）</th>
        <td>
            <input type="text" name="fcl_tel1" value="<?=hh($extfclRow["fcl_tel1"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_tel2" value="<?=($extfclRow["fcl_tel2"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_tel3" value="<?=hh($extfclRow["fcl_tel3"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
        </td>
    </tr>
    <tr>
        <th>FAX</th>
        <td>
            <input type="text" name="fcl_fax1" value="<?=hh($extfclRow["fcl_fax1"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_fax2" value="<?=hh($extfclRow["fcl_fax2"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
            -
            <input type="text" name="fcl_fax3" value="<?=hh($extfclRow["fcl_fax3"])?>" size="5" maxlength="6" style="ime-mode:inactive;">
        </td>
    </tr>
    <? if ($path=="") { ?>
    <tr>
        <th>
            番号リスト<br>
            <span style="color:gray">（現在は登録なし）</span>
        </th>
        <td><input type="file" name="file" value="" size="50"></td>
    </tr>
    <? } else { ?>
    <tr>
        <th>
            番号リスト<br>
            <input type="button" value="参照" onclick="window.open('../<? echo($path); ?>');">
        </th>
        <td>
            <div style="height:30px; line-height:30px">
                <label><input type="radio" name="filemode" value="stay"<?=($filemode=="stay"?" checked":"")?> onclick="setFileMode();">更新しない</label>
            </div>
            <div style="height:30px; line-height:30px">
                <label><input type="radio" name="filemode" value="renew"<?=($filemode=="renew"?" checked":"")?> onclick="setFileMode();">更新する </label>
                <input type="file" name="file" value="" size="50" disabled>
            </div>
            <div style="height:30px; line-height:30px">
                <label><input type="radio" name="filemode" value="dispose"<?=($filemode=="dispose"?" checked":"")?> onclick="setFileMode();">削除する</label>
            </div>
        </td>
    </tr>
    <? } ?>
</table>



<div style="text-align:right; padding-top:2px"><input type="submit" value="更新"></div>



</div>

</form>



</div><!-- // CLIENT_FRAME_CONTENT --></body>
</html>
