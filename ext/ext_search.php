<?
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にextadm権限(49)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");

$action = $_REQUEST["action"];
$photo_flg = $_REQUEST["photo_flg"];
$page = max(1, (int)$_REQUEST["page"]); // ページ、１以上
$class_id = (int)$_REQUEST["class_id"];
$atrb_id = (int)$_REQUEST["atrb_id"];
$dept_id = (int)$_REQUEST["dept_id"];
$srch_emp_nm = $_REQUEST["srch_emp_nm"];
$_srch_emp_nm = str_replace("　", "", str_replace(" ", "", $srch_emp_nm));
$urlenc_srch_emp_nm = urlencode($srch_emp_nm);

$classmstRows = c2dbGetRows("select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no");// 部門一覧
$atrbmstRows = c2dbGetRows("select class_id as pid, atrb_id as cid, atrb_nm as nm from atrbmst where atrb_del_flg = 'f' order by class_id, order_no");// 課一覧
$deptmstRows = c2dbGetRows("select atrb_id as pid, dept_id as cid, dept_nm as nm from deptmst where dept_del_flg = 'f' order by atrb_id, order_no");// 科一覧



$emp_where = " where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
if ($class_id) {
    $emp_where .=
    " and ((empmst.emp_class = ".$class_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = ".$class_id."))";
}
if ($atrb_id) {
    $emp_where .=
    " and ((empmst.emp_attribute = ".$atrb_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = ".$atrb_id."))";
}
if ($dept_id) {
    $emp_where .= " and ((empmst.emp_dept = ".$dept_id.")".
    " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = ".$dept_id."))";
}

$dispInfo = getEmpmstExtUserColumns("");
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 番号検索</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">

















<script type="text/javascript">
var busyoObj = {};
busyoObj["atrb_id"] = <?=c2ToJson($atrbmstRows)?>;
busyoObj["dept_id"] = <?=c2ToJson($deptmstRows)?>;

function busyoChanged(childId, selectValue) {
    var cmb = document.getElementById(childId);
    cmb.options.length = 1;
    for ( var idx=0; idx<busyoObj[childId].length; idx++) {
        var obj = busyoObj[childId][idx];
        if (obj.pid!=selectValue) continue;
        cmb.options[cmb.options.length] = new Option(obj.nm, obj.cid);
    }
    if (childId=="emp_attribute") document.getElementById("emp_dept").options.length = 1;
}
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT">



<div class="window_title">
    <table cellspacing="0" cellpadding="0">
    <tr>
    <th>
        <?=$intra_pankuzu_html?>
        <a class="pankuzu2" href="ext_search.php">番号検索</a>
    </th>
    <? if ($EAL[SELF_ADMIN_AUTH_FLG] == "1") { ?>
    <td><a href="extadm_menu.php">管理画面へ</a></td>
    <? } ?>
    </tr>
    </table>
</div>


<div class="basic_tab_menu">
    <table cellspacing="0" cellpadding="0"><tr>
    <?=$intra_menu_html?>
    <th class="selectable"><a href="ext_menu.php"><nobr>番号一覧</nobr></a></th>
    <th class="current"><a href="ext_search.php"><nobr>番号検索</nobr></a></th>
    <th class="selectable"><a href="ext_number_update.php"><nobr>番号変更</nobr></a></th>
    <? if ($extadmoptRow["newcomer_flg"]!="f") { ?>
    <th class="selectable"><a href="ext_newcomer.php"><nobr>今月の新人</nobr></a></th>
    <? } ?>
    <th class="selectable"><a href="ext_option.php"><nobr>オプション</nobr></a></th>
    </tr></table>
</div>







<form name="mainform" method="post" action="ext_search.php">
<input type="hidden" name="action" value="search">


<div style="padding-top:5px; width:600px">



<table width="100%" cellspacing="0" cellpadding="2" class="list prop_list">
    <tr>
        <th width="160">職員氏名</th>
        <td><input type="text" name="srch_emp_nm" value="<?=hh($srch_emp_nm)?>" size="30" style="ime-mode:active;"></td>
    </tr>
    <tr>
        <th><?=hh($classnameRow["class_nm"])?></th>
        <td>
            <select name="class_id" onchange="busyoChanged('atrb_id', this.value);">
                <option value="-">----------</option>
                <? foreach($classmstRows as $row) {?>
                    <option value="<?=$row["class_id"]?>"<?=($row["class_id"]==$class_id?" selected":"")?>><?=hh($row["class_nm"])?></option>
                <? } ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?=hh($classnameRow["atrb_nm"])?></th>
        <td>
            <select name="atrb_id" id="atrb_id" onchange="busyoChanged('dept_id', this.value);">
                <option value="-">----------</option>
                <? foreach ($atrbmstRows as $row) { ?>
                    <? if ($class_id!=$row["pid"]) continue; ?>
                    <option value="<?=$row["cid"]?>"<?=($row["cid"]==$atrb_id?" selected":"")?>><?=hh($row["nm"])?></option>
                <? } ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?=hh($classnameRow["dept_nm"])?></th>
        <td>
            <select name="dept_id" id="dept_id">
                <option value="-">----------</option>
                <? foreach ($deptmstRows as $row) { ?>
                    <? if ($atrb_id!=$row["pid"]) continue; ?>
                    <option value="<?=$row["cid"]?>"<?=($row["cid"]==$dept_id?" selected":"")?>><?=hh($row["nm"])?></option>
                <? } ?>
            </select>
        </td>
    </tr>
</table>

<div style="text-align:right; padding-top:2px"><input type="submit" value="検索"></div>

</div>



</form>


<div sytle="padding-top:5px">
<? if ($action == "search") { ?>
    <table width="100%" cellspacing="0" cellpadding="2">
    <tr height="22">
        <?
            $sql =
            " select count(*) from empmst ".$emp_where.
            " and (empmst.emp_lt_nm || empmst.emp_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").
            " or empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").")";
            $ttl_count = c2dbGetOne($sql);
            if ($ttl_count <= ROW_LIMIT) $page = 1;
            $loc =
            'ext_search.php?srch_emp_nm='.$urlenc_srch_emp_nm.'&class_id='.$class_id.'&atrb_id='.$atrb_id.'&dept_id='.$dept_id.
            '&action=search&page='.$page.($photo_flg!='t'?'&photo_flg=t':'');
        ?>
        <td>
            <input type="button" value="<?=($photo_flg=="t"?"写真非表示":"写真表示")?>" onclick="location.href='<?=$loc?>';">
        </td>
        <?
            if ($ttl_count > ROW_LIMIT) {
                echo("<td align=\"right\">ページ：\n");
                $page_max = ceil($ttl_count / ROW_LIMIT);
                for ($p=1; $p<=$page_max; $p++) {
                    if ($p==$page) echo $p."&nbsp;";
                    else {
                        echo '<a href="ext_search.php?srch_emp_nm='.$urlenc_srch_emp_nm.'&action=search&page='.$p;
                        echo '&class_id='.$class_id.'&atrb_id='.$atrb_id.'&dept_id='.$dept_id.'&photo_flg='.$photo_flg.'">'.$p.'</a>&nbsp;';
                    }
                }
                echo("</td>\n");
            }
        ?>
    </tr>
    </table>
<? } ?>
</div>





<? if ($action == "search") { ?>
<div sytle="padding-top:5px">
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22" bgcolor="#f6f9ff" valign="top">
        <td><nobr>職員氏名</nobr></td>
        <?
        foreach ($dispInfo as $field_id => $fieldInfo) {
            if ($field_id=="photo") echo '<td><nobr>写真</nobr></td>';
            if ($field_id=="class_nm") echo '<td><nobr>所属'.hh($classnameRow["class_id"]).'</nobr></td>';
            if ($field_id=="job_nm") echo '<td><nobr>職種</nobr></td>';
            if ($field_id=="st_nm") echo '<td><nobr>役職</nobr></td>';
            if ($field_id=="emp_phs") echo '<td><nobr>'.getPhsName().'</nobr></td>';
            if ($field_id=="emp_ext") echo '<td><nobr>内線番号</nobr></td>';
            if ($field_id=="emp_mobile") echo '<td><nobr>携帯端末・外線</nobr></td>';
            if ($field_id=="empmst_ext1") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext2") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext3") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext4") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
            if ($field_id=="empmst_ext5") echo '<td><nobr>'.hh($fieldInfo["label"]).'</nobr></td>';
        }
        ?>
    </tr>
    <?
    $sql =
    " select".
    " empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_phs, empmst.emp_ext, empmst.emp_mobile1, empmst.emp_mobile2, empmst.emp_mobile3".
    ",classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, jobmst.job_nm, stmst.st_nm".
    ",spft_empmst.empmst_ext1, spft_empmst.empmst_ext2, spft_empmst.empmst_ext3, spft_empmst.empmst_ext4, spft_empmst.empmst_ext5".
    " from empmst".
    " left outer join spft_empmst on (spft_empmst.emp_id = empmst.emp_id)".
    " inner join classmst on ( empmst.emp_class = classmst.class_id )".
    " inner join atrbmst on ( empmst.emp_attribute = atrbmst.atrb_id )".
    " inner join deptmst on ( empmst.emp_dept = deptmst.dept_id )".
    " left outer join jobmst on ( empmst.emp_job = jobmst.job_id )".
    " left outer join stmst on ( empmst.emp_st = stmst.st_id )".
    " ". $emp_where.
    " and (empmst.emp_lt_nm || empmst.emp_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").
    " or empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like ".c2dbStr("%".$_srch_emp_nm."%").")".
    " order by empmst.emp_keywd, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_id".
    " offset ".(($page-1)*ROW_LIMIT)." limit ".ROW_LIMIT;
    $rows = c2dbGetRows($sql);
    foreach ($rows as $row) {
        $photoInfo = getPhotoInfo($row["emp_id"]);
        echo '<tr valign="top">';
        echo '<td>'.hh($row["emp_lt_nm"]." ".$row["emp_ft_nm"]).'</td>';


        foreach ($dispInfo as $field_id => $fieldInfo) {
            if ($field_id=="photo") echo '<td>'.($photo_flg=="t" ? $photoInfo["image_viewer_imgtag"] : $photoInfo["image_viewer_atag"]).'</td>';
            if ($field_id=="class_nm") echo '<td>'.hh($row["class_nm"]."＞".$row["atrb_nm"]."＞".$row["dept_nm"]).'</td>';
            if ($field_id=="job_nm") echo '<td>'.hh($row["job_nm"]).'</td>';
            if ($field_id=="st_nm") echo '<td>'.hh($row["st_nm"]).'</td>';
            if ($field_id=="emp_phs") echo '<td>'.hh($row["emp_phs"]).'</td>';
            if ($field_id=="emp_ext") echo '<td>'.hh($row["emp_ext"]).'</td>';
            if ($field_id=="emp_mobile") echo '<td>'.hh(concatTel($row["emp_mobile1"], $row["emp_mobile2"], $row["emp_mobile3"])).'</td>';
            if ($field_id=="empmst_ext1") echo '<td>'.hh($row["empmst_ext1"]).'</td>';
            if ($field_id=="empmst_ext2") echo '<td>'.hh($row["empmst_ext2"]).'</td>';
            if ($field_id=="empmst_ext3") echo '<td>'.hh($row["empmst_ext3"]).'</td>';
            if ($field_id=="empmst_ext4") echo '<td>'.hh($row["empmst_ext4"]).'</td>';
            if ($field_id=="empmst_ext5") echo '<td>'.hh($row["empmst_ext5"]).'</td>';
        }
        echo '</tr>'."\n";
    }
    ?>
    </table>
</div>
<? } ?>



</div><!-- // CLIENT_FRAME_CONTENT --></body>
</html>