<?
define("C2_LOGIN_FREE", 1); // ログインチェック不要
require_once("common.php"); // 本来は読込みと同時にログイン判別するが、ログインチェックはスルーされる

c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");


$sql =
" select main.*".
",empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as head_emp_nm".
" from (".
"     select classmst.class_id, 0 as atrb_id, 0 as dept_id, classmst.class_nm as master_nm".
"    ,1 as hier, classmst.order_no as class_order, 0 as atrb_order, 0 as dept_order".
"    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
"     from classmst".
"     left join extcls on ( extcls.class_id = classmst.class_id )".
"     where extcls.atrb_id is null".
"     and extcls.dept_id is null".
"     and classmst.class_del_flg = 'f'".
"     and classmst.class_show_flg = 't'". //
" union".
"     select atrbmst.class_id, atrbmst.atrb_id, 0 as dept_id, atrbmst.atrb_nm as master_nm".
"    ,2 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, 0 as dept_order".
"    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
"     from atrbmst".
"     inner join classmst on (classmst.class_id = atrbmst.class_id )".
"     left join extcls on ( extcls.class_id = atrbmst.class_id and extcls.atrb_id = atrbmst.atrb_id )".
"     where extcls.dept_id is null".
"     and atrbmst.atrb_del_flg = 'f'".
" union".
"     select atrbmst.class_id, deptmst.atrb_id, deptmst.dept_id, deptmst.dept_nm as master_nm".
"    ,3 as hier, classmst.order_no as class_order, atrbmst.order_no as atrb_order, deptmst.order_no as dept_order".
"    ,extcls.extension, extcls.tel1, extcls.tel2, extcls.tel3, extcls.fax1, extcls.fax2, extcls.fax3, extcls.note, extcls.head_emp_id".
"     from deptmst".
"     inner join atrbmst on (atrbmst.atrb_id = deptmst.atrb_id )".
"     inner join classmst on (classmst.class_id = atrbmst.class_id )".
"         left join extcls on ( extcls.atrb_id = deptmst.atrb_id and extcls.dept_id = deptmst.dept_id )".
"     where deptmst.dept_del_flg = 'f'".
" ) main".
" left join (".
"     select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm from empmst".
"     inner join authmst on (authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')".
" ) empmst on ( empmst.emp_id = main.head_emp_id )".
" order by class_order, atrb_order, dept_order";
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix <?=SELF_APPNAME?> | 部門階層表示</title>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript">
function changeDisplay(btn, parentPrefix, parentHier) {
    var dest = (btn.className=="plus" ? "minus" : "plus");
    btn.className = dest;
    var tbody = document.getElementById("main_tbody");
    for (var idx=0; idx<tbody.childNodes.length; idx++) {
        var tr = tbody.childNodes[idx];
        var cls = tr.className;
        if (!cls) continue;
        if (tr.id.indexOf(parentPrefix)==0) {
            var aCls = cls.split(" ");
            aCls[parentHier-1] = (dest=="plus" ? "tr_hide" : "tr_show");
            tr.className = aCls.join(" ");
        }
    }
}
function showEmployeeList(head_emp_id, class_id, atrb_id, dept_id) {
    window.open('ext_class_list_from_login.php?head_emp_id='.concat(head_emp_id).concat('&class_id=').concat(class_id).concat('&atrb_id=').concat(atrb_id).concat('&dept_id=').concat(dept_id), 'emplist', 'width=840,height=520,scrollbars=yes');
}
</script>
</head>
<body style="margin:5px"><div id="CLIENT_FRAME_CONTENT">





<table cellspacing="0" cellpadding="0" class="dialog_title"><tr>
        <th>部門階層表示</th>
        <td class="closebtn"><a href="javascript:void(0);" onclick="window.close();"></a></td>
</tr></table>



<div style="padding:10px 0 0 0">

<table width="100%" cellspacing="0" cellpadding="2" class="list">
<tr height="26" bgcolor="#f6f9ff">
    <td width="24%"><nobr>部門名</nobr></td>
    <td width="8%"><nobr>内線番号</nobr></td>
    <td width="17%"><nobr>外線番号</nobr></td>
    <td width="17%">FAX</td>
    <td width="13%"><nobr>所属長</nobr></td>
    <td width="21%"><nobr>機能</nobr></td>
</tr>
<tbody id="main_tbody">




<?
    $rows = c2dbGetRows($sql);
    $last_class_id = "";
    $last_atrb_id = "";
    $last_hier = 0;
    foreach ($rows as $row) {
        $hier = $row["hier"];
        if ($hier==$last_hier) {
            if ($hier==1 || $hier==2) {
                if ($hier==1) {
                    echo '<tr class="p1_'.$row["class_id"].' tr_hide">';
                    echo '<td colspan="6" style="padding-left:45px;">（登録なし）</td>';
                }
                if ($hier==2) {
                    echo '<tr class="p2_'.$row["atrb_id"].' tr_hide tr_hide">';
                    echo '<td colspan="6" style="padding-left:70px;">（登録なし）</td>';
                }
                echo '</tr>';
            }
        }

        $pid = "";
        if ($hier==1) echo '<tr>';
        if ($hier==2) { $pid = $last_class_id.'_'.$row["atrb_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide">'; }
        if ($hier==3) { $pid = $last_class_id.'_'.$last_atrb_id."_".$row["dept_id"]; echo '<tr id="tr'.$pid.'" class="tr_hide tr_hide">'; }

        echo '<td class="hier'.$hier.'"><nobr>';
        if ($hier==1) echo '<button class="plus" onclick="changeDisplay(this, \'tr'.$row["class_id"].'_\', '.$hier.')"></button>';
        if ($hier==2) echo '<button class="plus" onclick="changeDisplay(this, \'tr'.$last_class_id."_".$row["atrb_id"].'_\', '.$hier.')"></button>';
        echo hh($row["master_nm"]).'</nobr></td>';

        echo '<td>'.hh($row["extension"]).'</td>';
        echo '<td>'.concatTel($row["tel1"], $row["tel2"], $row["tel3"]).'</td>';
        echo '<td>'.concatTel($row["fax1"], $row["fax2"], $row["fax3"]).'</td>';
        echo '<td><a href="javascript:void(0);" onclick="showEmployeeList(\''.$row["head_emp_id"].'\', '.$row["class_id"].', '.$row["atrb_id"].', '.$row["dept_id"].');">';
        echo hh($row["head_emp_nm"]).'</a></td>';
        echo '<td>'.hh($row["note"]).'</td>';
        $last_hier = $hier;
        if ($hier==1) $last_class_id = $row["class_id"];
        if ($hier==2) $last_atrb_id = $row["atrb_id"];
        echo '</tr>';
    }
?>
</tbody>
</table>
</div>



</div><!-- // CLIENT_FRAME_CONTENT --></body>

</html>