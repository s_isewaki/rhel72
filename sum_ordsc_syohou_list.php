<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require_once("get_values.php");
require_once("show_sinryo_top_header.ini");
require_once("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

$c_ordsc_util = new ordsc_util_class();    // 処方箋ユーティリティオブジェクト

//==========================================================================
// 登録処理
//==========================================================================
$mode = isset($_POST["mode"]) ? $_POST["mode"] : "";
// 処方箋機能コントロールマスタテーブルに登録されているレコード数を取得する
$count = $c_ordsc_util->countSumMedSyohouMst($con, $fname);
if ($count === false) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    die;
}
if ($mode == "regist") {
    $params = getParams();
    if ($count > 0) {
        // 更新
        $result = updateSumMedSyohouMst($con, $params, $fname);
    } else {
        // 新規登録
        $result = insertSumMedSyohouMst($con, $params, $fname);
    }
    if ($result === false) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        die;
    }
    header("Location: sum_ordsc_syohou_list.php?session=" . $session);
    die;
}

//==========================================================================
// 以下、通常処理
//==========================================================================
// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

//==========================================================================
// 表示データを取得
//==========================================================================
if ($count > 0) {
    $data = $c_ordsc_util->getSumMedSyohouMst($con, $fname);
} else {
    // 初期値をセット
    $data = $c_ordsc_util->initSumMedSyohouMstData();
}

/**
 * パラメータを取得する
 *
 * @return array パラメータ
 */
function getParams()
{
    // POSTパラメータがセットされていなかった場合の値をセット
    $params = array(
            "search_all_medicine"            => "0",
            "default_tab"                    => "",
            "last_do_target"                 => "",
            "send_info_button"               => "0",
            "print_label_button"             => "0",
            "print_sub_button"               => "0",
            "print_direction_button"         => "0",
            "print_direction_touyaku_button" => "0",
            "print_direction_cyusya_button"  => "0",
            "print_button"                   => "0",
            "dosing_timing"                  => "0",
            "meal_timing_before"             => "0",
            "meal_timing_just_before"        => "0",
            "meal_timing_just_after"         => "0",
            "meal_timing_after"              => "0",
            "meal_timing_during"             => "0",
            "meal_timing_between"            => "0",
            "hour_distance"                  => "0",
            "doctor_direction"               => "0",
            "time_assign_num"                => "0",
            "dosing_period_word"             => "",
            "fixed_time_continuance"         => "0",
            "enable_dispensing_medicine"     => "0",
            "dispensing_medicine_crush"      => "0",
            "dispensing_medicine_off"        => "0",
            "dispensing_tablet"              => "0",
            "dispensing_suspension"          => "0",
            "dispensing_crush"               => "0",
            "dispensing_package"             => "0",
            "dispensing_off"                 => "0",
            "input_dispensing"               => "0",
            "print_rp_line"                  => "0",
            "print_dosing_period"            => "0",
            "print_injected_record"          => "0",
            "input_injection_content"        => "0",
            "print_dialog_auto"              => "0"
    );

    foreach ($params as $key => $value) {
        if (isset($_POST[$key])) {
            $params[$key] = $_POST[$key];
        }
    }

    return $params;
}


/**
 * 処方箋機能コントロールマスタテーブルにデータを登録する
 *
 * @param resource $con    データベース接続リソース
 * @param array    $params パラメータ
 * @param string   $fname  ページ名
 *
 * @return boolean 成功時:true/失敗時:false
 */
function insertSumMedSyohouMst($con, $params, $fname)
{
    $sql = "INSERT INTO sum_med_syohou_mst (" .
             "search_all_medicine," .
             "default_tab," .
             "last_do_target," .
             "send_info_button," .
             "print_label_button," .
             "print_sub_button," .
             "print_direction_button," .
             "print_direction_touyaku_button," .
             "print_direction_cyusya_button," .
             "print_button," .
             "dosing_timing," .
             "meal_timing_before," .
             "meal_timing_just_before," .
             "meal_timing_just_after," .
             "meal_timing_after," .
             "meal_timing_during," .
             "meal_timing_between," .
             "hour_distance," .
             "doctor_direction," .
             "time_assign_num," .
             "dosing_period_word," .
             "fixed_time_continuance," .
             "enable_dispensing_medicine," .
             "dispensing_medicine_crush," .
             "dispensing_medicine_off," .
             "dispensing_tablet," .
             "dispensing_suspension," .
             "dispensing_crush," .
             "dispensing_package," .
             "dispensing_off," .
             "input_dispensing," .
             "print_rp_line," .
             "print_dosing_period," .
             "print_injected_record," .
             "input_injection_content," .
             "print_dialog_auto" .
             ") VALUES (" .
             "'" . pg_escape_string($params["search_all_medicine"])             . "'," .
             "'" . pg_escape_string($params["default_tab"])                     . "'," .
             "'" . pg_escape_string($params["last_do_target"])                  . "'," .
             "'" . pg_escape_string($params["send_info_button"])                . "'," .
             "'" . pg_escape_string($params["print_label_button"])              . "'," .
             "'" . pg_escape_string($params["print_sub_button"])                . "'," .
             "'" . pg_escape_string($params["print_direction_button"])          . "'," .
             "'" . pg_escape_string($params["print_direction_touyaku_button"])  . "'," .
             "'" . pg_escape_string($params["print_direction_cyusya_button"])   . "'," .
             "'" . pg_escape_string($params["print_button"])                    . "'," .
             "'" . pg_escape_string($params["dosing_timing"])                   . "'," .
             "'" . pg_escape_string($params["meal_timing_before"])              . "'," .
             "'" . pg_escape_string($params["meal_timing_just_before"])         . "'," .
             "'" . pg_escape_string($params["meal_timing_just_after"])          . "'," .
             "'" . pg_escape_string($params["meal_timing_after"])               . "'," .
             "'" . pg_escape_string($params["meal_timing_during"])              . "'," .
             "'" . pg_escape_string($params["meal_timing_between"])             . "'," .
             "'" . pg_escape_string($params["hour_distance"])                   . "'," .
             "'" . pg_escape_string($params["doctor_direction"])                . "'," .
             $params["time_assign_num"]                                         . "," .
             "'" . pg_escape_string($params["dosing_period_word"])              . "'," .
             "'" . pg_escape_string($params["fixed_time_continuance"])          . "'," .
             "'" . pg_escape_string($params["enable_dispensing_medicine"])      . "'," .
             "'" . pg_escape_string($params["dispensing_medicine_crush"])       . "'," .
             "'" . pg_escape_string($params["dispensing_medicine_off"])         . "'," .
             "'" . pg_escape_string($params["dispensing_tablet"])               . "'," .
             "'" . pg_escape_string($params["dispensing_suspension"])           . "'," .
             "'" . pg_escape_string($params["dispensing_crush"])                . "'," .
             "'" . pg_escape_string($params["dispensing_package"])              . "'," .
             "'" . pg_escape_string($params["dispensing_off"])                  . "'," .
             "'" . pg_escape_string($params["input_dispensing"])                . "'," .
             "'" . pg_escape_string($params["print_rp_line"])                   . "'," .
             "'" . pg_escape_string($params["print_dosing_period"])             . "'," .
             "'" . pg_escape_string($params["print_injected_record"])           . "'," .
             "'" . pg_escape_string($params["input_injection_content"])         . "'," .
             "'" . pg_escape_string($params["print_dialog_auto"])               . "'" .
             ")";

    $result = insert_into_table_no_content($con, $sql, $fname);
    if ($result === 0) {
        return false;
    } else {
        return true;
    }
}

/**
 * 処方箋機能コントロールマスタテーブルのデータを更新する
 *
 * @param resource $con    データベース接続リソース
 * @param array    $params パラメータ
 * @param string   $fname  ページ名
 *
 * @return boolean 成功時:true/失敗時:false
 */
function updateSumMedSyohouMst($con, $params, $fname)
{
    $sql = "UPDATE sum_med_syohou_mst SET " .
             "search_all_medicine = '"            . pg_escape_string($params["search_all_medicine"])            . "', " .
             "default_tab = '"                    . pg_escape_string($params["default_tab"])                    . "', " .
             "last_do_target = '"                 . pg_escape_string($params["last_do_target"])                 . "', " .
             "send_info_button = '"               . pg_escape_string($params["send_info_button"])               . "', " .
             "print_label_button = '"             . pg_escape_string($params["print_label_button"])             . "', " .
             "print_sub_button = '"               . pg_escape_string($params["print_sub_button"])               . "', " .
             "print_direction_button = '"         . pg_escape_string($params["print_direction_button"])         . "', " .
             "print_direction_touyaku_button = '" . pg_escape_string($params["print_direction_touyaku_button"]) . "', " .
             "print_direction_cyusya_button = '"  . pg_escape_string($params["print_direction_cyusya_button"])  . "', " .
             "print_button = '"                   . pg_escape_string($params["print_button"])                   . "', " .
             "dosing_timing = '"                  . pg_escape_string($params["dosing_timing"])                  . "', " .
             "meal_timing_before = '"             . pg_escape_string($params["meal_timing_before"])             . "', " .
             "meal_timing_just_before = '"        . pg_escape_string($params["meal_timing_just_before"])        . "', " .
             "meal_timing_just_after = '"         . pg_escape_string($params["meal_timing_just_after"])         . "', " .
             "meal_timing_after = '"              . pg_escape_string($params["meal_timing_after"])              . "', " .
             "meal_timing_during = '"             . pg_escape_string($params["meal_timing_during"])             . "', " .
             "meal_timing_between = '"            . pg_escape_string($params["meal_timing_between"])            . "', " .
             "hour_distance = '"                  . pg_escape_string($params["hour_distance"])                  . "', " .
             "doctor_direction = '"               . pg_escape_string($params["doctor_direction"])               . "', " .
             "time_assign_num = "                 . $params["time_assign_num"]                                  . "," .
             "dosing_period_word = '"             . pg_escape_string($params["dosing_period_word"])             . "', " .
             "fixed_time_continuance = '"         . pg_escape_string($params["fixed_time_continuance"])         . "', " .
             "enable_dispensing_medicine = '"     . pg_escape_string($params["enable_dispensing_medicine"])     . "', " .
             "dispensing_medicine_crush = '"      . pg_escape_string($params["dispensing_medicine_crush"])      . "', " .
             "dispensing_medicine_off = '"        . pg_escape_string($params["dispensing_medicine_off"])        . "', " .
             "dispensing_tablet = '"              . pg_escape_string($params["dispensing_tablet"])              . "', " .
             "dispensing_suspension = '"          . pg_escape_string($params["dispensing_suspension"])          . "', " .
             "dispensing_crush = '"               . pg_escape_string($params["dispensing_crush"])               . "', " .
             "dispensing_package = '"             . pg_escape_string($params["dispensing_package"])             . "', " .
             "dispensing_off = '"                 . pg_escape_string($params["dispensing_off"])                 . "', " .
             "input_dispensing = '"               . pg_escape_string($params["input_dispensing"])               . "', " .
             "print_rp_line = '"                  . pg_escape_string($params["print_rp_line"])                  . "', " .
             "print_dosing_period = '"            . pg_escape_string($params["print_dosing_period"])            . "', " .
             "print_injected_record = '"          . pg_escape_string($params["print_injected_record"])          . "', " .
             "input_injection_content = '"        . pg_escape_string($params["input_injection_content"])        . "', " .
             "print_dialog_auto = '"              . pg_escape_string($params["print_dialog_auto"])              . "'";

    $result = update_set_table($con, $sql, array(), null, "", $fname);
    if ($result === 0) {
        return false;
    } else {
        return true;
    }
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | 処方・注射オーダ</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="./js/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">
$(function() {
    changeDispensingMedicine();

    // 薬剤単位の調剤指示
    $("input[name='enable_dispensing_medicine']:radio").change(function() {
        changeDispensingMedicine();
    });
});

// 薬剤単位の調剤指示の表示切替
function changeDispensingMedicine() {
    if ($("[name='enable_dispensing_medicine']:checked").val() == 1) {
        $("input[name='dispensing_medicine_crush']").attr("disabled", false);
        $("input[name='dispensing_medicine_off']").attr("disabled", false);
    } else {
        $("input[name='dispensing_medicine_crush']").attr("disabled", true);
        $("input[name='dispensing_medicine_off']").attr("disabled", true);
        $("input[name='dispensing_medicine_crush']").attr("checked", false);
        $("input[name='dispensing_medicine_off']").attr("checked", false);
    }
}
</script>
<style type="text/css">
div.box_item {
  float: left;
  width: 180px;
  height: 23px;
  padding-top: 1px;
}
div.box_select {
  float: left;
  height: 23px;
  width: 500px;
}
div.container {
  margin-top: 15px;
  margin-left: 10px;
  width: 680px;
  font-family: ＭＳ Ｐゴシック, Osaka;
  font-size: 14px;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session,$fname,"KANRI_ORDSC"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<? echo(summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title)); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<?
//==========================================================================
// 処方・注射メニュー
//==========================================================================
echo(summary_common_show_ordsc_right_menu($session, "処方箋"));
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>
<form action="sum_ordsc_syohou_list.php" method="post" name="frm">
    <input type="hidden" name="session" value="<? echo($session); ?>">
    <input type="hidden" name="mode" value="regist">
<div class="container">
<!-- 画面設定start -->
  <div class="box_item">＜画面設定＞</div>
  <br style="clear:both">
  <div class="box_item">全薬品からの検索機能</div>
  <div class="box_select">
    <label><input type="radio" name="search_all_medicine" value="1" <? echo ($data["search_all_medicine"] === "1") ? "checked" : ""; ?>>表示する</label>&nbsp;
    <label><input type="radio" name="search_all_medicine" value="0" <? echo ($data["search_all_medicine"] === "0") ? "checked" : ""; ?>>表示しない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">初期表示タブ</div>
  <div class="box_select">
    <label><input type="radio" name="default_tab" value="P" <? echo ($data["default_tab"] === "P") ? "checked" : ""; ?>>処方薬</label>&nbsp;
    <label><input type="radio" name="default_tab" value="C" <? echo ($data["default_tab"] === "C") ? "checked" : ""; ?>>採用薬</label>
  </div>
  <br style="clear:both">
  <div class="box_item">前回DOの表示対象</div>
  <div class="box_select">
    <label><input type="radio" name="last_do_target" value="K" <? echo ($data["last_do_target"] === "K") ? "checked" : ""; ?>>同じ種類の履歴を表示</label>&nbsp;
    <label><input type="radio" name="last_do_target" value="P" <? echo ($data["last_do_target"] === "P") ? "checked" : ""; ?>>同じ処方箋の履歴のみ表示</label>
  </div>
  <br style="clear:both">
  <div class="box_item">ボタン表示</div>
  <div class="box_select">
    <label><input type="checkbox" name="send_info_button" value="1" <? echo ($data["send_info_button"] === "1") ? "checked" : ""; ?>>薬情送信</label>&nbsp;
    <label><input type="checkbox" name="print_label_button" value="1" <? echo ($data["print_label_button"] === "1") ? "checked" : ""; ?>>印刷（ラベル）</label>&nbsp;
    <label><input type="checkbox" name="print_sub_button" value="1" <? echo ($data["print_sub_button"] === "1") ? "checked" : ""; ?>>印刷（控え）</label>&nbsp;
    <label><input type="checkbox" name="print_direction_button" value="1" <? echo ($data["print_direction_button"] === "1") ? "checked" : ""; ?>>印刷（指示書）</label>
  </div>
  <br style="clear:both">
  <div class="box_item">&nbsp;</div>
  <div class="box_select">
    <label><input type="checkbox" name="print_direction_touyaku_button" value="1" <? echo ($data["print_direction_touyaku_button"] === "1") ? "checked" : ""; ?>>印刷（投薬指示書）</label>&nbsp;
    <label><input type="checkbox" name="print_direction_cyusya_button" value="1" <? echo ($data["print_direction_cyusya_button"] === "1") ? "checked" : ""; ?>>印刷（注射指示書）</label>&nbsp;
    <label><input type="checkbox" name="print_button" value="1" <? echo ($data["print_button"] === "1") ? "checked" : ""; ?>>印刷</label>
  </div>
  <br style="clear:both">
  <div class="box_item">タイミング表示</div>
  <div class="box_select">
    <label><input type="radio" name="dosing_timing" value="1" <? echo ($data["dosing_timing"] === "1") ? "checked" : ""; ?>>全て</label>&nbsp;
    <label><input type="radio" name="dosing_timing" value="0" <? echo ($data["dosing_timing"] === "0") ? "checked" : ""; ?>>中間を表示しない（朝〜昼、昼〜夕、夕〜眠前は表示しない）</label>
  </div>
  <br style="clear:both">
  <div class="box_item">食事タイミング表示</div>
  <div class="box_select">
    <label><input type="checkbox" name="meal_timing_before" value="1" <? echo ($data["meal_timing_before"] === "1") ? "checked" : ""; ?>>食前</label>&nbsp;
    <label><input type="checkbox" name="meal_timing_just_before" value="1" <? echo ($data["meal_timing_just_before"] === "1") ? "checked" : ""; ?>>食直前</label>&nbsp;
    <label><input type="checkbox" name="meal_timing_just_after" value="1" <? echo ($data["meal_timing_just_after"] === "1") ? "checked" : ""; ?>>食直後</label>&nbsp;
    <label><input type="checkbox" name="meal_timing_after" value="1" <? echo ($data["meal_timing_after"] === "1") ? "checked" : ""; ?>>食後</label>&nbsp;
    <label><input type="checkbox" name="meal_timing_during" value="1" <? echo ($data["meal_timing_during"] === "1") ? "checked" : ""; ?>>食中</label>&nbsp;
    <label><input type="checkbox" name="meal_timing_between" value="1" <? echo ($data["meal_timing_between"] === "1") ? "checked" : ""; ?>>食間</label>
  </div>
  <br style="clear:both">
  <div class="box_item">時間間隔</div>
  <div class="box_select">
    <label><input type="radio" name="hour_distance" value="1" <? echo ($data["hour_distance"] === "1") ? "checked" : ""; ?>>表示する</label>&nbsp;
    <label><input type="radio" name="hour_distance" value="0" <? echo ($data["hour_distance"] === "0") ? "checked" : ""; ?>>表示しない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">医師の指示通り</div>
  <div class="box_select">
    <label><input type="radio" name="doctor_direction" value="1" <? echo ($data["doctor_direction"] === "1") ? "checked" : ""; ?>>表示する</label>&nbsp;
    <label><input type="radio" name="doctor_direction" value="0" <? echo ($data["doctor_direction"] === "0") ? "checked" : ""; ?>>表示しない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">時刻指定枠数</div>
  <div class="box_select">
    <label><input type="radio" name="time_assign_num" value="4" <? echo ($data["time_assign_num"] === "4") ? "checked" : ""; ?>>４枠</label>&nbsp;
    <label><input type="radio" name="time_assign_num" value="5" <? echo ($data["time_assign_num"] === "5") ? "checked" : ""; ?>>５枠</label>&nbsp;
    <label><input type="radio" name="time_assign_num" value="6" <? echo ($data["time_assign_num"] === "6") ? "checked" : ""; ?>>６枠</label>
  </div>
  <br style="clear:both">
  <div class="box_item">投与期間文言変更</div>
  <div class="box_select">
    <label><input type="radio" name="dosing_period_word" value="投与期間" <? echo ($data["dosing_period_word"] === "投与期間") ? "checked" : ""; ?>>投与期間</label>&nbsp;
    <label><input type="radio" name="dosing_period_word" value="投与開始日" <? echo ($data["dosing_period_word"] === "投与開始日") ? "checked" : ""; ?>>投与開始日</label>
  </div>
  <br style="clear:both">
  <div class="box_item">定時つなぎ表示</div>
  <div class="box_select">
    <label><input type="radio" name="fixed_time_continuance" value="1" <? echo ($data["fixed_time_continuance"] === "1") ? "checked" : ""; ?>>表示する</label>&nbsp;
    <label><input type="radio" name="fixed_time_continuance" value="0" <? echo ($data["fixed_time_continuance"] === "0") ? "checked" : ""; ?>>表示しない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">薬剤単位の調剤指示</div>
  <div class="box_select">
    <label><input type="radio" name="enable_dispensing_medicine" value="1" <? echo ($data["enable_dispensing_medicine"] === "1") ? "checked" : ""; ?>>可</label>&nbsp;
      （<label><input type="checkbox" name="dispensing_medicine_crush" value="1" <? echo ($data["dispensing_medicine_crush"] === "1") ? "checked" : ""; ?>>粉砕</label>&nbsp;
      <label><input type="checkbox" name="dispensing_medicine_off" value="1" <? echo ($data["dispensing_medicine_off"] === "1") ? "checked" : ""; ?>>はずし</label>
    ）&nbsp;
    <label><input type="radio" name="enable_dispensing_medicine" value="0" <? echo ($data["enable_dispensing_medicine"] === "0") ? "checked" : ""; ?>>不可</label>
  </div>
  <br style="clear:both">
  <div class="box_item">調剤指示</div>
  <div class="box_select">
    <label><input type="checkbox" name="dispensing_tablet" value="1" <? echo ($data["dispensing_tablet"] === "1") ? "checked" : ""; ?>>錠剤</label>&nbsp;
    <label><input type="checkbox" name="dispensing_suspension" value="1" <? echo ($data["dispensing_suspension"] === "1") ? "checked" : ""; ?>>懸濁</label>&nbsp;
    <label><input type="checkbox" name="dispensing_crush" value="1" <? echo ($data["dispensing_crush"] === "1") ? "checked" : ""; ?>>粉砕</label>&nbsp;
    <label><input type="checkbox" name="dispensing_package" value="1" <? echo ($data["dispensing_package"] === "1") ? "checked" : ""; ?>>一包化</label>&nbsp;
    <label><input type="checkbox" name="dispensing_off" value="1" <? echo ($data["dispensing_off"] === "1") ? "checked" : ""; ?>>はずし</label>&nbsp;
  </div>
  <br style="clear:both">
  <div class="box_item">調剤指示の入力</div>
  <div class="box_select">
    <label><input type="radio" name="input_dispensing" value="1" <? echo ($data["input_dispensing"] === "1") ? "checked" : ""; ?>>必須</label>&nbsp;
    <label><input type="radio" name="input_dispensing" value="0" <? echo ($data["input_dispensing"] === "0") ? "checked" : ""; ?>>必須でない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">注射箋用法</div>
  <div class="box_select">
    <label><input type="radio" name="input_injection_content" value="0" <? echo ($data["input_injection_content"] === "0") ? "checked" : ""; ?>>内容入力なし</label>&nbsp;
    <label><input type="radio" name="input_injection_content" value="1" <? echo ($data["input_injection_content"] === "1") ? "checked" : ""; ?>>内容入力あり</label>
  </div>
  <br style="clear:both">
  <br>
<!-- 画面設定end -->
<!-- 印刷設定start -->
  <div class="box_item">＜印刷設定＞</div>
  <br style="clear:both">
  <div class="box_item">RP間の罫線</div>
  <div class="box_select">
    <label><input type="radio" name="print_rp_line" value="1" <? echo ($data["print_rp_line"] === "1") ? "checked" : ""; ?>>印字する</label>&nbsp;
    <label><input type="radio" name="print_rp_line" value="0" <? echo ($data["print_rp_line"] === "0") ? "checked" : ""; ?>>印字しない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">投与期間印字</div>
  <div class="box_select">
    <label><input type="radio" name="print_dosing_period" value="1" <? echo ($data["print_dosing_period"] === "1") ? "checked" : ""; ?>>行を分ける</label>&nbsp;
    <label><input type="radio" name="print_dosing_period" value="0" <? echo ($data["print_dosing_period"] === "0") ? "checked" : ""; ?>>行を分けない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">注射箋の実施欄</div>
  <div class="box_select">
    <label><input type="radio" name="print_injected_record" value="1" <? echo ($data["print_injected_record"] === "1") ? "checked" : ""; ?>>印字する</label>&nbsp;
    <label><input type="radio" name="print_injected_record" value="0" <? echo ($data["print_injected_record"] === "0") ? "checked" : ""; ?>>印字しない</label>
  </div>
  <br style="clear:both">
  <div class="box_item">印刷ダイアログ</div>
  <div class="box_select">
    <label><input type="radio" name="print_dialog_auto" value="1" <? echo ($data["print_dialog_auto"] === "1") ? "checked" : ""; ?>>自動で表示する</label>&nbsp;
    <label><input type="radio" name="print_dialog_auto" value="0" <? echo ($data["print_dialog_auto"] === "0") ? "checked" : ""; ?>>自動で表示しない</label>
  </div>
  <br style="clear:both">
<!-- 印刷設定end -->
  </div>
  <div style="margin-top:10px; text-align:right; width:300px">
    <input type="button" onclick="document.frm.submit();" value="更新" />
  </div>
</form>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
