<?php
require_once 'jnl_indicator_display.php';
require_once 'jnl_indicator_nurse_home_display.php';
require_once 'jnl_indicator_facility_excel.php';
/**
 * 施設別画面表示出力
 */
class IndicatorFacilityDisplay extends IndicatorDisplay
{
    /**
     * @var IndicatorFacilityExcel
     */
    var $indicatorFacilityExcel;

    /**
     * @var IndicatorNurseHomeDisplay
     */
    var $indicatorNurseHomeDisplay;

    /**
     * コンストラクタ
     * @param $arr
     */
    function IndicatorFacilityDisplay($arr)
    {
        parent::IndicatorDisplay($arr);
    }

    /**
     * @return void
     */
    function setIndicatorFacilityExcel()
    {
        $this->indicatorFacilityExcel = new IndicatorFacilityExcel(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorFacilityExcel
     */
    function getIndicatorFacilityExcel()
    {
        if (!is_object($this->indicatorFacilityExcel)) { $this->setIndicatorFacilityExcel(); }
        return $this->indicatorFacilityExcel;
    }

    /**
     * @return void
     */
    function setIndicatorNurseHomeDisplay()
    {
        $this->indicatorNurseHomeDisplay = new IndicatorNurseHomeDisplay(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorNurseHomeDisplay
     */
    function getIndicatorNurseHomeDisplay()
    {
        if (!is_object($this->indicatorNurseHomeDisplay)) { $this->setIndicatorNurseHomeDisplay(); }
        return $this->indicatorNurseHomeDisplay;
    }

    /**
     * 各画面に応じた表示データを出力する
     * @param  integer $no
     * @param  array   $dataArr
     * @param  array   $plantArr
     * @param  array   $arr
     * @param  array   $dateArr
     * @return string  [html table]
     */
    function getDataDisplayByNo($no, $dataArr, $plantArr, $arr, $optionArr)
    {
        $displayData = false;
        switch ($no) {
            case  '74': # [病院] 病院別月間報告書患者数総括
                $displayData = $this->getSummaryOfNumberOfMonthlyReportPatientsAccordingToHospital($dataArr, $plantArr, $arr, $optionArr);
                break;

            case  '75': # [病院] 病院月間報告書(行為件数)
                $displayData = $this->getHospitalMonthlyReport($dataArr, $plantArr, $arr, $optionArr);
                break;

            case  '76': # [病院] 外来診療行為別明細
                $displayData = $this->getOutpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr);
                break;

            case  '77': # [病院] 外来日当点
                $displayData = $this->getOutpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr);
                break;

            case  '78': # [病院] 入院診療行為別明細
                $displayData = $this->getInpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr);
                break;

            case  '79': # [病院] 入院日当点
                $displayData = $this->getInpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr);
                break;

            case  '80': # [病院] 診療報酬総括表[医科]
            case  '81': # [病院] 診療報酬総括表[歯科]
                $displayData = $this->getMedicalTreatmentFeeGeneralizationTable($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '103': # [老健] 患者月報(日報・日毎)
                $displayData = $this->getMonthlyPerDate($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '124': # [老健] 老健月間加算項目・算定実績一覧
                $displayData = $this->getCalculationResults($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '125': # [老健] 療養費明細書
                $displayData = $this->getMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '126': # [老健] 予防給付療養費明細書
                $displayData = $this->getPreventiveSupplyMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr);
                break;
        }

        return $displayData;
    }

    /**
     * 各画面に応じたエクセルファイルを出力する
     * (non-PHPdoc)
     * @see IndicatorDisplay#outputExcelData($no, $dataArr, $plantArr, $arr, $optionArr)
     */
    function outputExcelData($no, $dataArr, $plantArr, $arr, $optionArr=null)
    {
        $excelData = null;
        /* @var $indicatorFacilityExcel IndicatorFacilityExcel */
        $indicatorFacilityExcel = $this->getIndicatorFacilityExcel();

        switch ($no) {
            case '74': # [病院] 病院別月間報告書患者数総括
                $excelData = $indicatorFacilityExcel->outputExcelSummaryOfNumberOfMonthlyReportPatientsAccordingToHospital($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '75': # [病院] 病院月間報告書(行為件数)
                $excelData = $indicatorFacilityExcel->outputExcelHospitalMonthlyReport($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '76': # [病院] 外来診療行為別明細
                $excelData = $indicatorFacilityExcel->outputExcelOutpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '77': # [病院] 外来日当点
                $excelData = $indicatorFacilityExcel->outputExcelOutpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '78': # [病院] 入院診療行為別明細
                $excelData = $indicatorFacilityExcel->outputExcelInpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '79'; # [病院] 入院日当点
                $excelData = $indicatorFacilityExcel->outputInpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '80': # [病院] 診療報酬総括表[医科]
            case '81': # [病院] 診療報酬総括表[歯科]
                $excelData = $indicatorFacilityExcel->outputExcelMedicalTreatmentFeeGeneralizationTable($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '103': # [老健] 患者月報(日報・日毎)
                $excelData = $indicatorFacilityExcel->outputExcelMonthlyPerDate($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '124': # [老健] 老健月間加算項目・算定実績一覧
                $excelData = $indicatorFacilityExcel->outputExcelCalculationResults($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '125': # [老健] 療養費明細書
                $excelData = $indicatorFacilityExcel->outputExcelMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr);
                break;

            case '126': # [老健] 予防給付療養費明細書
                $excelData = $indicatorFacilityExcel->outputExcelPreventiveSupplyMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr);
                break;

            default:
                $excelData = parent::outputExcelData($no, $dataArr, $plantArr, $arr, $optionArr);
                break;
        }

        return $excelData;
    }

    /**
     * セルのデータの状態を取得します。
     * @see IndicatorDisplay#checkCellDataStatus($type, $arr)
     */
    function checkCellDataStatus($type, $arr)
    {
        $rtn        = 3;
        $startMonth = $this->getFiscalYearStartMonth();
        $thisYear   = date('Y');
        $thisMonth  = date('m');
        $today      = date('d');
        $lastYear   = date('Y', mktime(0, 0, 0, $thisMonth, $today, $thisYear-1));

        switch ($type) {
            case 'comp':
                foreach ($arr as $val) { if (!empty($val) && $rtn > $val) { $rtn = $val; } }
                break;

            case 'patient_daily_facilities': # No.103 [老健] 患者月報(日報・日毎)
                $year   = $this->getArrValueByKey('year',       $arr);
                $month  = $this->getArrValueByKey('month',      $arr);
                $day    = $this->getArrValueByKey('day',        $arr);
                $status = $this->getArrValueByKey('jnl_status', $arr);
                if (!is_null($status) && $status === '0') { $rtn = 2; }
                else { $rtn = 1; }

                if ($year < $thisYear) { # 閲覧年以前の場合
                    if ($thisMonth < $startMonth) { if ($month < $startMonth) { $rtn = 3; } }
                    else { $rtn = 3; }
                    if ($year < $lastYear) { $rtn = 3; }
                }
                if ($year == $thisYear) { if ($thisMonth >= $startMonth) { if ($month < $startMonth) { $rtn = 3; } } } # 閲覧年の場合
                if ( ($year == $thisYear && $month >= $thisMonth) || $year > $thisYear) { # 翌月以降の場合
                    if ($month == $thisMonth && $day >= $today &&  $rtn == 1) { $rtn = 3; }
                    else if ($month > $thisMonth || $year > $thisYear) { $rtn = 3; }
                }
                break;

            case 'calculation_results': # No.124 [老健] 老健月間加算項目・算定実績
                /* @var $indicatorNurseHomeDisplay IndicatorNurseHomeDisplay */
                $indicatorNurseHomeDisplay = $this->getIndicatorNurseHomeDisplay();
                $rtn = $indicatorNurseHomeDisplay->checkCellDataStatus($type, $arr);
                break;

            case 'summaryOfNumberOfMonthlyReportPatientsAccordingToHospital': # No.74 [病院] 病院別月間報告書患者数総括
                $year     = $this->getArrValueByKey('year',      $arr);
                $month    = $this->getArrValueByKey('month',     $arr);
                $dayCount = $this->getArrValueByKey('day_count', $arr);
                $valid    = $this->getArrValueByKey('valid',     $arr);
                $status   = $this->getArrValueByKey('status',    $arr);
                $lastDay  = $this->getLastDay($year, $month);
                $mainData = null;
                if (is_null($dayCount)) { # 月報またはデータがない場合
                    $mainData = $this->getArrValueByKey('main_data', $arr);
                    $status   = $this->getArrValueByKey('status',    $mainData);
                    if (!is_null($status) && $status==='0') { $rtn = 2; } # 月報の場合
                    else { $rtn = 1; } # データがない場合
                }
                else { # 日報の1月分のデータ
                    if ($valid === '0' || (is_null($valid) && is_null($status))) { $rtn = 1; }
                    else if ($status === '0') { $rtn = 2; }
                }
                if ($year < $thisYear) { # 閲覧年以前の場合
                    if ($thisMonth < $startMonth) { if ($month < $startMonth) { $rtn = 3; } }
                    else { $rtn = 3; }
                    if ($year < $lastYear) { $rtn = 3; }
                }
                if ($year == $thisYear) { if ($thisMonth >= $startMonth) { if ($month < $startMonth) { $rtn = 3; } } } # 閲覧年の場合
                if ( ($year == $thisYear && $month >= $thisMonth) || $year > $thisYear) { # 翌月以降の場合
                    if ($month == $thisMonth && $rtn == 1) { $rtn = 3; }
                    else if ($month > $thisMonth || $year > $thisYear) { $rtn = 3; }
                }
                break;

            case 'hospital_monthly_report':          # No.75 [病院] 病院月刊報告書(行為件数)
            case "outpatientCareActDetails":         # No.76 [病院] 外来診療行為別明細
            case 'outpatientDailyAllowancePoint': # No.77 [病院] 外来日当点
            case 'inpatientCareActDetails':          # No.78 [病院] 入院診療行為別明細
            case 'inpatientDailyAllowancePoint':     # No.79 [病院] 入院日当点
                $year   = $this->getArrValueByKey('year',   $arr);
                $month  = $this->getArrValueByKey('month',  $arr);
                $status = $this->getArrValueByKey('status', $arr);
                if (!is_null($status) && $status === '0') { $rtn = 2; }
                else { $rtn = 1; }
                if ($year < $thisYear) { # 閲覧年以前の場合
                    if ($thisMonth < $startMonth) { if ($month < $startMonth) { $rtn = 3; } }
                    else { $rtn = 3; }
                    if ($year < $lastYear) { $rtn = 3; }
                }
                if ($year == $thisYear) { if ($thisMonth >= $startMonth) { if ($month <= $startMonth) { $rtn = 3; } } } # 閲覧年の場合
                if ( ($year == $thisYear && $month >= $thisMonth) || $year > $thisYear) { # 翌月以降の場合
                    if ($month == $thisMonth && $rtn == 1) { $rtn = 3; }
                    else if ($month > $thisMonth || $year > $thisYear) { $rtn = 3; }
                }
                break;

            default:
                $rtn = parent::checkCellDataStatus($type, $arr);
                break;
        }
        return $rtn;
    }

    /**
     * No.74 [病院] 病院別月間報告書患者数総括のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $dateArr
     * @return string    [html table]
     */
    function getSummaryOfNumberOfMonthlyReportPatientsAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr  = '';
        $fontTag = $this->getFontTag();

        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();

        # attr
        $rowspan_2         = array('name' => 'rowspan',          'value' =>  2);
        $rowspan_4         = array('name' => 'rowspan',          'value' =>  4);
        $rowspan_6         = array('name' => 'rowspan',          'value' =>  6);
        $rowspan_8         = array('name' => 'rowspan',          'value' =>  8);
        $rowspan_9         = array('name' => 'rowspan',          'value' =>  9);
        $rowspan_10        = array('name' => 'rowspan',          'value' => 10);
        $rowspan_13        = array('name' => 'rowspan',          'value' => 13);
        $rowspan_14        = array('name' => 'rowspan',          'value' => 14);
        $rowspan_19        = array('name' => 'rowspan',          'value' => 19);
        $rowspan_24        = array('name' => 'rowspan',          'value' => 24);
        $rowspan_28        = array('name' => 'rowspan',          'value' => 28);
        $rowspan_65        = array('name' => 'rowspan',          'value' => 65);
        $colspan_2         = array('name' => 'colspan',          'value' =>  2);
        $colspan_3         = array('name' => 'colspan',          'value' =>  3);
        $colspan_4         = array('name' => 'colspan',          'value' =>  4);
        $colspan_5         = array('name' => 'colspan',          'value' =>  5);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $facilityId   = $this->getArrValueByKey('facility_id', $dateArr);
        $targetYear   = $this->getArrValueByKey('year',        $dateArr);
        $displayMonth = $this->getDisplayMonth();
        $startMonth   = $this->getFiscalYearStartMonth();

        $leftSide = array();
        $mainData = array();

        $rtnStr .= '<table class="list" width="100%" ><tr>';

        $yearMonthArr[] = array('data' => '&nbsp;', 'attr' => array($colspan_5));
        for ($m=0; $m < 12; $m++) {
            $month = ($startMonth + $m < 13)? $startMonth + $m : $startMonth + $m - 12;
            $yearMonthArr[] = array('data' => $month.'月');
        }

        $yearMonthArr[] = array('data' => '合計');
        $rtnStr .= $this->convertCellData($yearMonthArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)).'</tr>';

        # 外来
        $leftSide[1] = array(0 => array('data' => '外来',     'attr' => array($rowspan_2))
                           , 1 => array('data' => '外来合計', 'attr' => array($colspan_4))
        );
        $leftSide[2] = array(0 => array('data' => '1日平均',  'attr' => array($colspan_4)));

        # 入院
        $leftSide[3] = array(0 => array('data' => '入院',           'attr' => array($rowspan_65))
                           , 1 => array('data' => '全病棟',         'attr' => array($rowspan_6))
                           , 2 => array('data' => '入院合計',       'attr' => array($colspan_3))
        );
        $leftSide[4] = array(0 => array('data' => '1日平均',        'attr' => array($colspan_3)));
        $leftSide[5] = array(0 => array('data' => '定床数',         'attr' => array($colspan_3)));
        $leftSide[6] = array(0 => array('data' => '空床数',         'attr' => array($colspan_3)));
        $leftSide[7] = array(0 => array('data' => '稼働病床稼働率', 'attr' => array($colspan_3)));
        $leftSide[8] = array(0 => array('data' => '許可病床稼働率', 'attr' => array($colspan_3)));

        # 入院 一般病棟
        $leftSide[9] = array(0 => array('data' => '一般病棟',                      'attr' => array($rowspan_13))
                           , 1 => array('data' => '延べ患者数',                    'attr' => array($colspan_3))
        );
        $leftSide[10] = array(0 => array('data' => '定床数',                       'attr' => array($colspan_3)));
        $leftSide[11] = array(0 => array('data' => '空床数',                       'attr' => array($colspan_3)));
        $leftSide[12] = array(0 => array('data' => '稼働率',                       'attr' => array($colspan_3)));
        $leftSide[13] = array(0 => array('data' => '平均在院日数(直近3ヶ月)',      'attr' => array($colspan_3)));
        $leftSide[14] = array(0 => array('data' => '新入院患者数',                 'attr' => array($colspan_3)));
        $leftSide[15] = array(0 => array('data' => 'DPC対象延べ患者数',            'attr' => array($colspan_3)));
        $leftSide[16] = array(0 => array('data' => '重症度・看護必要度割合',       'attr' => array($colspan_3)));
        $leftSide[17] = array(0 => array('data' => '1日〜14日延べ患者数',          'attr' => array($colspan_3)));
        $leftSide[18] = array(0 => array('data' => '15日〜30日延べ患者数',         'attr' => array($colspan_3)));
        $leftSide[19] = array(0 => array('data' => '救急医療管理加算算定延べ数',   'attr' => array($colspan_3)));
        $leftSide[20] = array(0 => array('data' => '90日超延べ患者数',             'attr' => array($colspan_3)));
        $leftSide[21] = array(0 => array('data' => '上記内特定入院対象延べ患者数', 'attr' => array($colspan_3)));

        # 入院 障害者病棟
        $leftSide[22] = array(0 => array('data' => '障害者病棟',                     'attr' => array($rowspan_8))
                            , 1 => array('data' => '延べ患者数',                     'attr' => array($colspan_3)),
        );
        $leftSide[23] = array(0 => array('data' => '定床数',                         'attr' => array($colspan_3)));
        $leftSide[24] = array(0 => array('data' => '空床数',                         'attr' => array($colspan_3)));
        $leftSide[25] = array(0 => array('data' => '稼働率',                         'attr' => array($colspan_3)));
        $leftSide[26] = array(0 => array('data' => '1日〜14日延べ患者数',            'attr' => array($colspan_3)));
        $leftSide[27] = array(0 => array('data' => '15日〜30日延べ患者数',           'attr' => array($colspan_3)));
        $leftSide[28] = array(0 => array('data' => '90日超延べ患者数',               'attr' => array($colspan_3)));
        $leftSide[29] = array(0 => array('data' => '上記の内特定入院対象延べ患者数', 'attr' => array($colspan_3)));

        # 入院 精神一般病棟
        $leftSide[30] = array(0 => array('data' => '精神一般病棟', 'attr' => array($rowspan_4))
                            , 1 => array('data' => '延べ患者数',   'attr' => array($colspan_3)),
        );
        $leftSide[31] = array(0 => array('data' => '定床数',       'attr' => array($colspan_3)));
        $leftSide[32] = array(0 => array('data' => '空床数',       'attr' => array($colspan_3)));
        $leftSide[33] = array(0 => array('data' => '稼働率',       'attr' => array($colspan_3)));

        # 入院 療養病棟 医療
        $leftSide[34] = array(0 => array('data' => '療養病棟',            'attr' => array($rowspan_28))
                            , 1 => array('data' => '医療',                'attr' => array($rowspan_14))
                            , 2 => array('data' => '延べ患者数',          'attr' => array($colspan_2)),
        );
        $leftSide[35] = array(0 => array('data' => '定床数',              'attr' => array($colspan_2)));
        $leftSide[36] = array(0 => array('data' => '1日平均',             'attr' => array($colspan_2)));
        $leftSide[37] = array(0 => array('data' => '稼働率',              'attr' => array($colspan_2)));
        $leftSide[38] = array(0 => array('data' => '入院基本料',          'attr' => array($rowspan_9))
                            , 1 => array('data' => 'A'));
        $leftSide[39] = array(0 => array('data' => 'B'));
        $leftSide[40] = array(0 => array('data' => 'C'));
        $leftSide[41] = array(0 => array('data' => 'D'));
        $leftSide[42] = array(0 => array('data' => 'E'));
        $leftSide[43] = array(0 => array('data' => 'F'));
        $leftSide[44] = array(0 => array('data' => 'G'));
        $leftSide[45] = array(0 => array('data' => 'H'));
        $leftSide[46] = array(0 => array('data' => 'I'));
        $leftSide[47] = array(0 => array('data' => '1日〜14日延べ患者数', 'attr' => array($colspan_2)));

        # 入院 療養病棟 介護
        $leftSide[48] = array(0 => array('data' => '介護',       'attr' => array($rowspan_10))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2)),
        );
        $leftSide[49] = array(0 => array('data' => '定床数',     'attr' => array($colspan_2)));
        $leftSide[50] = array(0 => array('data' => '1日平均',    'attr' => array($colspan_2)));
        $leftSide[51] = array(0 => array('data' => '稼働率',     'attr' => array($colspan_2)));
        $leftSide[52] = array(0 => array('data' => '要介護5',    'attr' => array($colspan_2)));
        $leftSide[53] = array(0 => array('data' => '要介護4',    'attr' => array($colspan_2)));
        $leftSide[54] = array(0 => array('data' => '要介護3',    'attr' => array($colspan_2)));
        $leftSide[55] = array(0 => array('data' => '要介護2',    'attr' => array($colspan_2)));
        $leftSide[56] = array(0 => array('data' => '要介護1',    'attr' => array($colspan_2)));
        $leftSide[57] = array(0 => array('data' => '平均介護度', 'attr' => array($colspan_2)));

        # 入院 療養病棟 精神療養
        $leftSide[58] = array(0 => array('data' => '精神療養',   'attr' => array($rowspan_4))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2))
        );
        $leftSide[59] = array(0 => array('data' => '定床数', 'attr' => array($colspan_2)));
        $leftSide[60] = array(0 => array('data' => '空床数', 'attr' => array($colspan_2)));
        $leftSide[61] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_2)));

        # 入院 特定病床
        $leftSide[62] = array(0 => array('data' => '特定病棟',       'attr' => array($rowspan_6))
                            , 1 => array('data' => '延べ患者数',     'attr' => array($colspan_3))
        );
        $leftSide[63] = array(0 => array('data' => '定床数',         'attr' => array($colspan_3)));
        $leftSide[64] = array(0 => array('data' => '空床数',         'attr' => array($colspan_3)));
        $leftSide[65] = array(0 => array('data' => '稼働率',         'attr' => array($colspan_3)));
        $leftSide[66] = array(0 => array('data' => '回復期リハ',     'attr' => array($rowspan_2))
                            , 1 => array('data' => '重症患者の割合', 'attr' => array($colspan_2))
        );
        $leftSide[67] = array(0 => array('data' => '在宅復帰率',     'attr' => array($colspan_2)));

        # 重点項目
        $leftSide[68] = array(0 => array('data' => '重点項目', 'attr' => array($rowspan_19))
                            , 1 => array('data' => $indicatorData->sc(1))
                            , 2 => array('data' => '新患者数', 'attr' => array($colspan_3)),
        );
        $leftSide[69] = array(0 => array('data' => $indicatorData->sc(2))
                            , 1 => array('data' => '初診患者数', 'attr' => array($colspan_3)),
        );
        $leftSide[70] = array(0 => array('data' => $indicatorData->sc(3))
                            , 1 => array('data' => '紹介患者数', 'attr' => array($colspan_3)),
        );
        $leftSide[71] = array(0 => array('data' => $indicatorData->sc(4))
                            , 1 => array('data' => '救急依頼件数', 'attr' => array($colspan_3)),
        );
        $leftSide[72] = array(0 => array('data' => $indicatorData->sc(5))
                            , 1 => array('data' => $indicatorData->sc(4).'の内、救急受入れ患者数', 'attr' => array($colspan_3)),
        );
        $leftSide[73] = array(0 => array('data' => $indicatorData->sc(6))
                            , 1 => array('data' => $indicatorData->sc(5).'の内、救急入院患者数', 'attr' => array($colspan_3)),
        );
        $leftSide[74] = array(0 => array('data' => $indicatorData->sc(7))
                            , 1 => array('data' => '診療時間外患者数', 'attr' => array($colspan_3)),
        );
        $leftSide[75] = array(0 => array('data' => $indicatorData->sc(8))
                            , 1 => array('data' => $indicatorData->sc(7).'の内、時間外入院患者数', 'attr' => array($colspan_3)),
        );
        $leftSide[76] = array(0 => array('data' => '紹介率', 'attr' => array($colspan_4)));
        $leftSide[77] = array(0 => array('data' => '救急受入率', 'attr' => array($colspan_4)));
        $leftSide[78] = array(0 => array('data' => '救急入院率', 'attr' => array($colspan_4)));
        $leftSide[79] = array(0 => array('data' => '時間外入院率', 'attr' => array($colspan_4)));
        $leftSide[80] = array(0 => array('data' => $indicatorData->sc(9))
                            , 1 => array('data' => 'リハビリ平均提供単位数(一般)', 'attr' => array($colspan_3)));
        $leftSide[81] = array(0 => array('data' => $indicatorData->sc(10))
                            , 1 => array('data' => 'リハビリ平均提供単位数(療養・精神)', 'attr' => array($colspan_3)));
        $leftSide[82] = array(0 => array('data' => $indicatorData->sc(11))
                            , 1 => array('data' => 'リハビリ平均提供単位数(回復期リハ)', 'attr' => array($colspan_3)));
        $leftSide[83] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4)));
        $leftSide[84] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4)));
        $leftSide[85] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4)));
        $leftSide[86] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4)));

        $rowCount = 86;

        $dailyData   = $this->getArrValueByKey('daily_data',   $dataArr);
        $monthlyData = $this->getArrValueByKey('monthly_data', $dataArr);
		
        for ($m=0; $m < 12; $m++) {
            $month         = ($startMonth + $m < 13)? $startMonth + $m : $startMonth + $m - 12;
            $year          = ($month < $startMonth)? $targetYear + 1 : $targetYear;
			
			//表示月が現在月より過去の場合はMAXの日曜・祝日日数を設定する
			//表示月と現在月が同じ場合は現在日までの日曜・祝日日数を設定する
			$holi_cnt = $indicatorData->getHolidayCount($year,$month);
			
			
            $dailyStatus   = $this->checkCellDataStatus('summaryOfNumberOfMonthlyReportPatientsAccordingToHospital', array('year' => $year, 'month' => $month));
            $monthlyStatus = $this->checkCellDataStatus('summaryOfNumberOfMonthlyReportPatientsAccordingToHospital', array('year' => $year, 'month' => $month));

            $gokeiGa      = null;
            $dayCount     = null;
            $gokeiIp      = null;
            $gokeiShog    = null;
            $gokeiSeip    = null;
            $gokeiIr      = null;
            $gokeiKaig    = null;
            $gokeiKaif    = null;
            $gokeiAk      = null;
            $gokeiKan     = null;
            $gokeiIcu     = null;
            $gokeiShoni   = null;
            $gokeiSeir    = null;
            $gokeiTok     = null;
            $gokeiNin     = null;
            $actCount     = null;
            $pmtCount     = null;
            $ipAct        = null;
            $shogAct      = null;
            $seipAct      = null;
            $irAct        = null;
            $kaigAct      = null;
            $kaifAct      = null;
            $akAct        = null;
            $kanAct       = null;
            $icuAct       = null;
            $shoniAct     = null;
            $seirAct      = null;
            $tokAct       = null;
            $ninAct       = null;
            $sumGokei     = null;
            $gokeiTokutei = null;
            $tokuteiAct   = null;

            $dispNum = array();

            # このへんでデータ格納
            if (is_array($dailyData)) {
                foreach ($dailyData as $dailyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dailyVal) && sprintf('%02d', $month) == $this->getArrValueByKey('month', $dailyVal)) {
                        $dailyStatus  = $this->checkCellDataStatus('summaryOfNumberOfMonthlyReportPatientsAccordingToHospital', $dailyVal);
                        $gokeiGa      = $this->getArrValueByKey('gokei_ga',    $dailyVal);
                        $dayCount     = $this->getArrValueByKey('day_count',   $dailyVal);
                        $gokeiIp      = $this->getArrValueByKey('gokei_ip',    $dailyVal);
                        $gokeiShog    = $this->getArrValueByKey('gokei_shog',  $dailyVal);
                        $gokeiSeip    = $this->getArrValueByKey('gokei_seip',  $dailyVal);
                        $gokeiIr      = $this->getArrValueByKey('gokei_ir',    $dailyVal);
                        $gokeiKaig    = $this->getArrValueByKey('gokei_kaig',  $dailyVal);
                        $gokeiKaif    = $this->getArrValueByKey('gokei_kaif',  $dailyVal);
                        $gokeiAk      = $this->getArrValueByKey('gokei_ak',    $dailyVal);
                        $gokeiKan     = $this->getArrValueByKey('gokei_kan',   $dailyVal);
                        $gokeiIcu     = $this->getArrValueByKey('gokei_icu',   $dailyVal);
                        $gokeiShoni   = $this->getArrValueByKey('gokei_shoni', $dailyVal);
                        $gokeiSeir    = $this->getArrValueByKey('gokei_seir',  $dailyVal);
                        $gokeiTok     = $this->getArrValueByKey('gokei_tok',   $dailyVal);
                        $gokeiNin     = $this->getArrValueByKey('gokei_nin',   $dailyVal);
                        $actCount     = $this->getArrValueByKey('act_count',   $dailyVal);
                        $pmtCount     = $this->getArrValueByKey('pmt_count',   $dailyVal);
                        $ipAct        = $this->getArrValueByKey('ip_act',      $dailyVal);
                        $shogAct      = $this->getArrValueByKey('shog_act',    $dailyVal);
                        $seipAct      = $this->getArrValueByKey('seip_act',    $dailyVal);
                        $irAct        = $this->getArrValueByKey('ir_act',      $dailyVal);
                        $kaigAct      = $this->getArrValueByKey('kaig_act',    $dailyVal);
                        $kaifAct      = $this->getArrValueByKey('kaif_act',    $dailyVal);
                        $akAct        = $this->getArrValueByKey('ak_act',      $dailyVal);
                        $kanAct       = $this->getArrValueByKey('kan_act',     $dailyVal);
                        $icuAct       = $this->getArrValueByKey('icu_act',     $dailyVal);
                        $shoniAct     = $this->getArrValueByKey('shoni_act',   $dailyVal);
                        $seirAct      = $this->getArrValueByKey('seir_act',    $dailyVal);
                        $tokAct       = $this->getArrValueByKey('tok_act',     $dailyVal);
                        $ninAct       = $this->getArrValueByKey('nin_act',     $dailyVal);
                        $sumGokei     = $gokeiIp + $gokeiShog + $gokeiSeip + $gokeiIr + $gokeiKaig + $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiSeir + $gokeiTok + $gokeiNin;
                        $gokeiTokutei = $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiTok + $gokeiNin;
                        $tokuteiAct   = $kaifAct + $akAct + $kanAct + $icuAct + $shoniAct + $tokAct + $ninAct;
                    }
                }
            }

            if (is_array($monthlyData)) {
                foreach ($monthlyData as $monthlyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $monthlyVal) && $month == $this->getArrValueByKey('month', $monthlyVal)) {
                        $monthlyStatus = $this->checkCellDataStatus('summaryOfNumberOfMonthlyReportPatientsAccordingToHospital', array('main_data' => $monthlyVal, 'year' => $year, 'month' => $month));
                        for ($i=297; $i <= 341; $i++) { $dispNum[$i] = $monthlyVal['dispnum_'.$i]; }
                    }
                }
            }

            # 外来
            $mainData[1][$m] = array('data' => $gokeiGa, 'status' => $dailyStatus);
			$mainData[2][$m] = array('data' => empty($dayCount)? '' : round($gokeiGa / ($dayCount - $holi_cnt), 1), 'gokei_ga' => $gokeiGa, 'day_count' => $dayCount, 'status' => $dailyStatus, 'disp_type' => 'float');

            # 入院 全病棟
            $mainData[3][$m] = array('data' => $sumGokei, 'status' => $dailyStatus);
            $mainData[4][$m] = array('data' => empty($dayCount)? '' : round($sumGokei / $dayCount, 1), 'sum_gokei' => $sumGokei, 'day_count' => $dayCount, 'status' => $dailyStatus, 'disp_type' => 'float');
            $mainData[5][$m] = array('data' => $actCount, 'status' => $dailyStatus);
            $mainData[6][$m] = array('data' => empty($actCount)? '' : $this->getNumberOfEmptyFloors($sumGokei, $actCount, 1), 'sum_gokei' => $sumGokei, 'act_count' => $actCount, 'status' => $dailyStatus);
            $mainData[7][$m] = array('data' => empty($actCount)? '' : $this->getUtilizationRates($sumGokei, $actCount, 1), 'act_count' => $actCount, 'sum_gokei' => $sumGokei, 'status' => $dailyStatus, 'disp_type' => 'float_percent');
            $mainData[8][$m] = array('data' => empty($pmtCount)? '' : $this->getUtilizationRates($sumGokei, $pmtCount, 1), 'pmt_count' => $pmtCount, 'sum_gokei' => $sumGokei, 'status' => $dailyStatus, 'disp_type' => 'float_percent');

            # 入院 一般病棟
            $mainData[9][$m]  = array('data' => $gokeiIp,        'status' => $dailyStatus);
            $mainData[10][$m] = array('data' => $ipAct,          'status' => $dailyStatus);
            $mainData[11][$m] = array('data' => empty($ipAct)? '' : $this->getNumberOfEmptyFloors($gokeiIp, $ipAct, 1), 'ip_act' => $ipAct, 'gokei_ip' => $gokeiIp, 'status' => $dailyStatus);
            $mainData[12][$m] = array('data' => empty($ipAct)? '' : $this->getUtilizationRates($gokeiIp, $ipAct, 1), 'ip_act' => $ipAct, 'gokei_ip' => $gokeiIp, 'status' => $dailyStatus, 'disp_type' => 'float_percent');
            $mainData[13][$m] = array('data' => $dispNum['297'], 'status' => $monthlyStatus ,'disp_type' => 'float');
//			$mainData[13][$m] = array('data' => $dispNum['297'], 'status' => $monthlyStatus ,'disp_type' => 'float_percent');
			$mainData[14][$m] = array('data' => $dispNum['298'], 'status' => $monthlyStatus);
            $mainData[15][$m] = array('data' => $dispNum['299'], 'status' => $monthlyStatus);
//			$mainData[16][$m] = array('data' => $dispNum['300'], 'status' => $monthlyStatus);
			$mainData[16][$m] = array('data' => $dispNum['300'], 'status' => $monthlyStatus, 'disp_type' => 'float_percent');
			$mainData[17][$m] = array('data' => $dispNum['301'], 'status' => $monthlyStatus);
            $mainData[18][$m] = array('data' => $dispNum['302'], 'status' => $monthlyStatus);
            $mainData[19][$m] = array('data' => $dispNum['303'], 'status' => $monthlyStatus);
            $mainData[20][$m] = array('data' => $dispNum['304'], 'status' => $monthlyStatus);
            $mainData[21][$m] = array('data' => $dispNum['305'], 'status' => $monthlyStatus, 'shog_act' => $shogAct, 'gokei_shog' => $gokeiShog);

            # 入院 障害者病棟
            $mainData[22][$m] = array('data' => $gokeiShog,      'status' => $dailyStatus);
            $mainData[23][$m] = array('data' => $shogAct,        'status' => $dailyStatus);
            $mainData[24][$m] = array('data' => empty($shogAct)? '' : $this->getNumberOfEmptyFloors($gokeiShog, $shogAct, 1), 'shog_act' => $shogAct, 'gokei_shog' => $gokeiShog, 'status' => $dailyStatus);
            $mainData[25][$m] = array('data' => empty($shogAct)? '' : $this->getUtilizationRates($gokeiShog, $shogAct, 1), 'shog_act' => $shogAct, 'gokei_shog' => $gokeiShog, 'status' => $dailyStatus, 'disp_type' => 'float_percent');
            $mainData[26][$m] = array('data' => $dispNum['306'], 'status' => $monthlyStatus);
            $mainData[27][$m] = array('data' => $dispNum['307'], 'status' => $monthlyStatus);
            $mainData[28][$m] = array('data' => $dispNum['308'], 'status' => $monthlyStatus);
            $mainData[29][$m] = array('data' => $dispNum['309'], 'status' => $monthlyStatus, 'seip_act' => $seipAct, 'gokei_seip' => $gokeiSeip);

            # 入院 精神一般
            $mainData[30][$m] = array('data' => $gokeiSeip, 'gokei_seip' => $gokeiSeip, 'status' => $dailyStatus);
            $mainData[31][$m] = array('data' => $seipAct, 'seip_act' => $seipAct, 'status' => $dailyStatus);
            $mainData[32][$m] = array('data' => empty($seipAct)? '' : $this->getNumberOfEmptyFloors($gokeiSeip, $seipAct, 1), 'seip_act' => $seipAct, 'gokei_seip' => $gokeiSeip, 'status' => $dailyStatus);
            $mainData[33][$m] = array('data' => empty($seipAct)? '' : $this->getUtilizationRates($gokeiSeip, $seipAct, 1), 'seip_act' => $seipAct, 'gokei_seip' => $gokeiSeip, 'status' => $dailyStatus, 'disp_type' => 'float_percent');

            # 入院 療養病棟 医療
            $mainData[34][$m] = array('data' => $gokeiIr, 'gokei_ir' => $gokeiIr, 'status' => $dailyStatus);
            $mainData[35][$m] = array('data' => $irAct, 'ir_act' => $irAct, 'status' => $dailyStatus);
            $mainData[36][$m] = array('data' => empty($dayCount)? '' : round($gokeiIr / $dayCount,  1), 'day_count' => $dayCount, 'gokei_ir' => $gokeiIr, 'status' => $dailyStatus, 'disp_type' => 'float');
            $mainData[37][$m] = array('data' => empty($irAct)? '' : $this->getUtilizationRates($gokeiIr, $irAct, 1), 'ir_act' => $irAct, 'gokei_ir' => $gokeiIr, 'status' => $dailyStatus, 'disp_type' => 'float_percent');
            $mainData[38][$m] = array('data' => $dispNum['312'], 'status' => $monthlyStatus);
            $mainData[39][$m] = array('data' => $dispNum['313'], 'status' => $monthlyStatus);
            $mainData[40][$m] = array('data' => $dispNum['314'], 'status' => $monthlyStatus);
            $mainData[41][$m] = array('data' => $dispNum['315'], 'status' => $monthlyStatus);
            $mainData[42][$m] = array('data' => $dispNum['316'], 'status' => $monthlyStatus);
            $mainData[43][$m] = array('data' => $dispNum['317'], 'status' => $monthlyStatus);
            $mainData[44][$m] = array('data' => $dispNum['318'], 'status' => $monthlyStatus);
            $mainData[45][$m] = array('data' => $dispNum['319'], 'status' => $monthlyStatus);
            $mainData[46][$m] = array('data' => $dispNum['320'], 'status' => $monthlyStatus);
            $mainData[47][$m] = array('data' => $dispNum['321'], 'status' => $monthlyStatus);

            # 入院 療養病棟 介護
            $mainData[48][$m] = array('data' => $gokeiKaig, 'status' => $dailyStatus);
            $mainData[49][$m] = array('data' => $kaigAct, 'status' => $dailyStatus);
            $mainData[50][$m] = array('data' => empty($dayCount)? '' : round($gokeiKaig / $dayCount,  1), 'day_count' => $dayCount, 'gokei_kaig' => $gokeiKaig, 'status' => $dailyStatus, 'disp_type' => 'float');
            $mainData[51][$m] = array('data' => empty($kaigAct)? '' : $this->getUtilizationRates($gokeiKaig, $kaigAct, 1), 'kaig_act' => $kaigAct, 'gokei_kaig' => $gokeiKaig, 'status' => $dailyStatus, 'disp_type' => 'float_percent');
            $mainData[52][$m] = array('data' => $dispNum['322'], 'status' => $monthlyStatus);
            $mainData[53][$m] = array('data' => $dispNum['323'], 'status' => $monthlyStatus);
            $mainData[54][$m] = array('data' => $dispNum['324'], 'status' => $monthlyStatus);
            $mainData[55][$m] = array('data' => $dispNum['325'], 'status' => $monthlyStatus);
            $mainData[56][$m] = array('data' => $dispNum['326'], 'status' => $monthlyStatus);
            $tmpRowData57 = $dispNum['322']+$dispNum['323']+$dispNum['324']+$dispNum['325']+$dispNum['326'];
            $mainData[57][$m] = array(
                'data' => empty($tmpRowData57)? '' : round((($dispNum['322']*5 + $dispNum['323']*4 + $dispNum['324']*3 + $dispNum['325']*2 + $dispNum['326']*1) / ($tmpRowData57)), 1),
                'dn322' => $dispNum['322'],
                'dn323' => $dispNum['323'],
                'dn324' => $dispNum['324'],
                'dn325' => $dispNum['325'],
                'dn326' => $dispNum['326'],
                'tmpRowData57' => $tmpRowData57,
                'status' => $monthlyStatus
				, 'disp_type' => 'float'
            );

            # 入院 療養病棟 精神療養
            $mainData[58][$m] = array('data' => $gokeiSeir, 'status' => $dailyStatus);
            $mainData[59][$m] = array('data' => $seirAct, 'status' => $dailyStatus);
            $mainData[60][$m] = array('data' => empty($seirAct)? '' : $this->getNumberOfEmptyFloors($gokeiSeir, $seirAct, 1), 'gokei_seir' => $gokeiSeir, 'seir_act' => $seirAct, 'status' => $dailyStatus);
            $mainData[61][$m] = array('data' => empty($seirAct)? '' : $this->getUtilizationRates($gokeiSeir, $seirAct, 1), 'gokei_seir' => $gokeiSeir, 'seir_act' => $seirAct, 'status' => $dailyStatus, 'disp_type' => 'float_percent');

            # 入院 特定病棟
            $mainData[62][$m] = array('data' => $gokeiTokutei, 'status' => $dailyStatus);
            $mainData[63][$m] = array('data' => $tokuteiAct, 'status' => $dailyStatus);
            $mainData[64][$m] = array('data' => empty($tokuteiAct)? '' : $this->getNumberOfEmptyFloors($gokeiTokutei, $tokuteiAct, 1), 'tokutei_act' => $tokuteiAct, 'gokei_tokutei' => $gokeiTokutei, 'status' => $dailyStatus);
            $mainData[65][$m] = array('data' => empty($tokuteiAct)? '' : $this->getUtilizationRates($gokeiTokutei, $tokuteiAct, 1), 'tokutei_act' => $tokuteiAct, 'gokei_tokutei' => $gokeiTokutei, 'status' => $dailyStatus, 'disp_type' => 'float_percent');

            # 重点項目
			$mainData[66][$m] = array('data' => $dispNum['310'], 'status' => $monthlyStatus, 'disp_type' => 'float');
			$mainData[67][$m] = array('data' => $dispNum['311'], 'status' => $monthlyStatus, 'disp_type' => 'float');
            $mainData[68][$m] = array('data' => $dispNum['327'], 'status' => $monthlyStatus);
            $mainData[69][$m] = array('data' => $dispNum['328'], 'status' => $monthlyStatus);
            $mainData[70][$m] = array('data' => $dispNum['329'], 'status' => $monthlyStatus);
            $mainData[71][$m] = array('data' => $dispNum['330'], 'status' => $monthlyStatus);
            $mainData[72][$m] = array('data' => $dispNum['331'], 'status' => $monthlyStatus);
            $mainData[73][$m] = array('data' => $dispNum['332'], 'status' => $monthlyStatus);
            $mainData[74][$m] = array('data' => $dispNum['333'], 'status' => $monthlyStatus);
            $mainData[75][$m] = array('data' => $dispNum['334'], 'status' => $monthlyStatus);
            $mainData[76][$m] = array('data' => empty($dispNum['328'])? '' : $this->getUtilizationRates(($dispNum['329']+$dispNum['331']), $dispNum['328'], 1), 'dn328' => $dispNum['328'], 'dn329' => $dispNum['329'], 'dn331' => $dispNum['331'], 'status' => $monthlyStatus, 'disp_type' => 'float_percent');
            $mainData[77][$m] = array('data' => empty($dispNum['330'])? '' : $this->getUtilizationRates($dispNum['331'], $dispNum['330'], 1), 'dn330' => $dispNum['330'], 'dn331' => $dispNum['331'], 'status' => $monthlyStatus, 'disp_type' => 'float_percent');
            $mainData[78][$m] = array('data' => empty($dispNum['331'])? '' : $this->getUtilizationRates($dispNum['332'], $dispNum['331'], 1), 'dn331' => $dispNum['331'], 'dn332' => $dispNum['332'], 'status' => $monthlyStatus, 'disp_type' => 'float_percent');
            $mainData[79][$m] = array('data' => empty($dispNum['333'])? '' : $this->getUtilizationRates($dispNum['334'], $dispNum['333'], 1), 'dn333' => $dispNum['333'], 'dn334' => $dispNum['334'], 'status' => $monthlyStatus, 'disp_type' => 'float_percent');
            $mainData[80][$m] = array('data' => $dispNum['335'], 'status' => $monthlyStatus, 'disp_type' => 'float_unit');
            $mainData[81][$m] = array('data' => $dispNum['336'], 'status' => $monthlyStatus, 'disp_type' => 'float_unit');
            $mainData[82][$m] = array('data' => $dispNum['337'], 'status' => $monthlyStatus, 'disp_type' => 'float_unit');
            $mainData[83][$m] = array('data' => $dispNum['338'], 'status' => $monthlyStatus);
            $mainData[84][$m] = array('data' => $dispNum['339'], 'status' => $monthlyStatus);
            $mainData[85][$m] = array('data' => $dispNum['340'], 'status' => $monthlyStatus);
            $mainData[86][$m] = array('data' => $dispNum['341'], 'status' => $monthlyStatus);

            for ($t=1; $t <= $rowCount; $t++) {
                $totalData[$t][0]['status']  = $this->checkCellDataStatus('comp', array($totalData[$t][0]['status'], $mainData[$t][$m]['status']));
                switch ($t) {
                    case '2': # 外来 1日平均
//                        $totalData[$t][0]['gokei_ga']  = $totalData[$t][0]['gokei_ga']  + $mainData[$t][$m]['gokei_ga'];
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
//                        $totalData[$t][0]['data']      = round($totalData[$t][0]['gokei_ga'] / $totalData[$t][0]['day_count'], 1);
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
                        break;

                    case '4': # 入院 全病棟 1日平均
//                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$m]['sum_gokei'];
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
//                        $totalData[$t][0]['data']      = round($totalData[$t][0]['sum_gokei'] / $totalData[$t][0]['day_count'], 1);
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
                        break;

                    case '6': # 入院 全病棟 空床数
                        $totalData[$t][0]['act_count'] = $totalData[$t][0]['act_count'] + $mainData[$t][$m]['act_count'];
                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$m]['sum_gokei'];
                        $totalData[$t][0]['data']      = $this->getNumberOfEmptyFloors($totalData[$t][0]['sum_gokei'], $totalData[$t][0]['act_count'], 1);
                        break;

                    case '7': # 入院 全病棟 稼働病床稼働率
                        $totalData[$t][0]['act_count'] = $totalData[$t][0]['act_count'] + $mainData[$t][$m]['act_count'];
                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$m]['sum_gokei'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['sum_gokei'], $totalData[$t][0]['act_count'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '8': # 入院 全病棟 許可病床稼働率
                        $totalData[$t][0]['pmt_count'] = $totalData[$t][0]['pmt_count'] + $mainData[$t][$m]['pmt_count'];
                        $totalData[$t][0]['sum_gokei'] = $totalData[$t][0]['sum_gokei'] + $mainData[$t][$m]['sum_gokei'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['sum_gokei'], $totalData[$t][0]['pmt_count'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '11': # 入院 一般病棟 空床数
                        $totalData[$t][0]['ip_act']   = $totalData[$t][0]['ip_act'] + $mainData[$t][$m]['ip_act'];
                        $totalData[$t][0]['gokei_ip'] = $totalData[$t][0]['gokei_ip'] + $mainData[$t][$m]['gokei_ip'];
                        $totalData[$t][0]['data']     = $this->getNumberOfEmptyFloors($totalData[$t][0]['gokei_ip'], $totalData[$t][0]['ip_act'], 1);
                        break;

                    case '12': # 入院 一般病棟 稼働率
                        $totalData[$t][0]['ip_act']    = $totalData[$t][0]['ip_act'] + $mainData[$t][$m]['ip_act'];
                        $totalData[$t][0]['gokei_ip']  = $totalData[$t][0]['gokei_ip'] + $mainData[$t][$m]['gokei_ip'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['gokei_ip'], $totalData[$t][0]['ip_act'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

					case '13': # 平均在院日数(直近3ヶ月)
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
						break;
					
					case '16': # 重症度・看護必要度割合
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
						$totalData[$t][0]['disp_type'] = 'float_percent';
						break;
					

                    case '24': # 入院 障害者病棟 空床数
                        $totalData[$t][0]['shog_act']   = $totalData[$t][0]['shog_act'] + $mainData[$t][$m]['shog_act'];
                        $totalData[$t][0]['gokei_shog'] = $totalData[$t][0]['gokei_shog'] + $mainData[$t][$m]['gokei_shog'];
                        $totalData[$t][0]['data']       = $this->getNumberOfEmptyFloors($totalData[$t][0]['gokei_shog'], $totalData[$t][0]['shog_act'], 1);
                        break;

                    case '25': # 入院 障害者病棟 稼働率
                        $totalData[$t][0]['shog_act']   = $totalData[$t][0]['shog_act'] + $mainData[$t][$m]['shog_act'];
                        $totalData[$t][0]['gokei_shog'] = $totalData[$t][0]['gokei_shog'] + $mainData[$t][$m]['gokei_shog'];
                        $totalData[$t][0]['data']       = $this->getUtilizationRates($totalData[$t][0]['gokei_shog'], $totalData[$t][0]['shog_act'], 1);
                        $totalData[$t][0]['disp_type']  = 'float_percent';
                        break;

                    case '32': # 入院 精神一般病棟 空床数
                        $totalData[$t][0]['seip_act']   = $totalData[$t][0]['seip_act'] + $mainData[$t][$m]['seip_act'];
                        $totalData[$t][0]['gokei_seip'] = $totalData[$t][0]['gokei_seip'] + $mainData[$t][$m]['gokei_seip'];
                        $totalData[$t][0]['data']       = $this->getNumberOfEmptyFloors($totalData[$t][0]['gokei_seip'], $totalData[$t][0]['seip_act'], 1);
                        break;

                    case '33': # 入院 精神一般病棟 稼働率
                        $totalData[$t][0]['seip_act']   = $totalData[$t][0]['seip_act'] + $mainData[$t][$m]['seip_act'];
                        $totalData[$t][0]['gokei_seip'] = $totalData[$t][0]['gokei_seip'] + $mainData[$t][$m]['gokei_seip'];
                        $totalData[$t][0]['data']       = $this->getUtilizationRates($totalData[$t][0]['gokei_seip'], $totalData[$t][0]['seip_act'], 1);
                        $totalData[$t][0]['disp_type']  = 'float_percent';
                        break;

                    case '36': # 入院 療養病棟 医療適用 1日平均
                        //$totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
                        //$totalData[$t][0]['gokei_ir']  = $totalData[$t][0]['gokei_ir'] + $mainData[$t][$m]['gokei_ir'];
                        //$totalData[$t][0]['data']      = round($totalData[$t][0]['gokei_ir'] / $totalData[$t][0]['day_count'], 1);
                        //$totalData[$t][0]['disp_type'] = 'float';

						//平均値の合計数を月数で割った平均値を表示するように変更します。2011/05/16
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
						
                        break;

                    case '37': # 入院 療養病棟 医療適用 稼働率
                        $totalData[$t][0]['ir_act']    = $totalData[$t][0]['ir_act'] + $mainData[$t][$m]['ir_act'];
                        $totalData[$t][0]['gokei_ir']  = $totalData[$t][0]['gokei_ir'] + $mainData[$t][$m]['gokei_ir'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['gokei_ir'], $totalData[$t][0]['ir_act'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '50': # 入院 療養病棟 介護適用 1日平均
                        //$totalData[$t][0]['day_count']  = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
                        //$totalData[$t][0]['gokei_kaig'] = $totalData[$t][0]['gokei_kaig'] + $mainData[$t][$m]['gokei_kaig'];
                        //$totalData[$t][0]['data']       = round($totalData[$t][0]['gokei_kaig'] / $totalData[$t][0]['day_count'], 1);
                        //$totalData[$t][0]['disp_type']  = 'float';

						//平均値の合計数を月数で割った平均値を表示するように変更します。2011/05/16
						$totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
						break;

                    case '51': # 入院 療養病棟 介護適用 稼働率
                        $totalData[$t][0]['kaig_act']   = $totalData[$t][0]['kaig_act']   + $mainData[$t][$m]['kaig_act'];
                        $totalData[$t][0]['gokei_kaig'] = $totalData[$t][0]['gokei_kaig'] + $mainData[$t][$m]['gokei_kaig'];
                        $totalData[$t][0]['data']       = $this->getUtilizationRates($totalData[$t][0]['gokei_kaig'], $totalData[$t][0]['kaig_act'], 1);
                        $totalData[$t][0]['disp_type']  = 'float_percent';
                        break;

                    case '57': # 入院 療養病棟 介護適用 平均介護度
                        $totalData[$t][0]['dn322'] = $totalData[$t][0]['dn322'] + $mainData[$t][$m]['dn322'];
                        $totalData[$t][0]['dn323'] = $totalData[$t][0]['dn323'] + $mainData[$t][$m]['dn323'];
                        $totalData[$t][0]['dn324'] = $totalData[$t][0]['dn324'] + $mainData[$t][$m]['dn324'];
                        $totalData[$t][0]['dn325'] = $totalData[$t][0]['dn325'] + $mainData[$t][$m]['dn325'];
                        $totalData[$t][0]['dn326'] = $totalData[$t][0]['dn326'] + $mainData[$t][$m]['dn326'];
                        $tmpSum = ($totalData[$t][0]['dn322']+$totalData[$t][0]['dn323']+$totalData[$t][0]['dn324']+$totalData[$t][0]['dn325']+$totalData[$t][0]['dn326']);
                        $totalData[$t][0]['data'] = empty($tmpSum)? '' : round((($totalData[$t][0]['dn322']*5 + $totalData[$t][0]['dn323']*4 + $totalData[$t][0]['dn324']*3 + $totalData[$t][0]['dn325']*2 + $totalData[$t][0]['dn326']*1) / $tmpSum), 1);
						$totalData[$t][0]['disp_type']  = 'float';
						break;

                    case '60': # 入院 療養病棟 精神療養 空床数
                        $totalData[$t][0]['seir_act']   = $totalData[$t][0]['seir_act'] + $mainData[$t][$m]['seir_act'];
                        $totalData[$t][0]['gokei_seir'] = $totalData[$t][0]['gokei_seir'] + $mainData[$t][$m]['gokei_seir'];
                        $totalData[$t][0]['data']       = $this->getNumberOfEmptyFloors($totalData[$t][0]['gokei_seir'], $totalData[$t][0]['seir_act'], 1);
                        break;

                    case '61': # 入院 療養病棟 精神療養 稼働率
                        $totalData[$t][0]['seir_act']   = $totalData[$t][0]['seir_act'] + $mainData[$t][$m]['seir_act'];
                        $totalData[$t][0]['gokei_seir'] = $totalData[$t][0]['gokei_seir'] + $mainData[$t][$m]['gokei_seir'];
                        $totalData[$t][0]['data']       = $this->getUtilizationRates($totalData[$t][0]['gokei_seir'], $totalData[$t][0]['seir_act'], 1);
                        $totalData[$t][0]['disp_type']  = 'float_percent';
                        break;

                    case '64': # 入院 特定病棟 空床数
                        $totalData[$t][0]['tokutei_act']   = $totalData[$t][0]['tokutei_act'] + $mainData[$t][$m]['tokutei_act'];
                        $totalData[$t][0]['gokei_tokutei'] = $totalData[$t][0]['gokei_tokutei'] + $mainData[$t][$m]['gokei_tokutei'];
                        $totalData[$t][0]['data']          = $this->getNumberOfEmptyFloors($totalData[$t][0]['gokei_tokutei'], $totalData[$t][0]['tokutei_act'], 1);
                        break;

                    case '65': # 入院 特定病棟 稼働率
                        $totalData[$t][0]['tokutei_act']   = $totalData[$t][0]['tokutei_act'] + $mainData[$t][$m]['tokutei_act'];
                        $totalData[$t][0]['gokei_tokutei'] = $totalData[$t][0]['gokei_tokutei'] + $mainData[$t][$m]['gokei_tokutei'];
                        $totalData[$t][0]['data']          = $this->getUtilizationRates($totalData[$t][0]['gokei_tokutei'], $totalData[$t][0]['tokutei_act'], 1);
                        $totalData[$t][0]['disp_type']     = 'float_percent';
                        break;

                    case '76': # その他 紹介率
                        $totalData[$t][0]['dn328']     = $totalData[$t][0]['dn328'] + $mainData[$t][$m]['dn328'];
                        $totalData[$t][0]['dn329']     = $totalData[$t][0]['dn329'] + $mainData[$t][$m]['dn329'];
                        $totalData[$t][0]['dn331']     = $totalData[$t][0]['dn331'] + $mainData[$t][$m]['dn331'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates( ($totalData[$t][0]['dn329'] + $totalData[$t][0]['dn331']) , $totalData[$t][0]['dn328'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '77': # その他 救急受入率
                        $totalData[$t][0]['dn330']     = $totalData[$t][0]['dn330'] + $mainData[$t][$m]['dn330'];
                        $totalData[$t][0]['dn331']     = $totalData[$t][0]['dn331'] + $mainData[$t][$m]['dn331'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['dn331'], $totalData[$t][0]['dn330'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '78': # その他 救急入院率
                        $totalData[$t][0]['dn331']     = $totalData[$t][0]['dn331'] + $mainData[$t][$m]['dn331'];
                        $totalData[$t][0]['dn332']     = $totalData[$t][0]['dn332'] + $mainData[$t][$m]['dn332'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['dn332'], $totalData[$t][0]['dn331'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '79': # その他 時間外入院率
                        $totalData[$t][0]['dn333']     = $totalData[$t][0]['dn333'] + $mainData[$t][$m]['dn333'];
                        $totalData[$t][0]['dn334']     = $totalData[$t][0]['dn334'] + $mainData[$t][$m]['dn334'];
                        $totalData[$t][0]['data']      = $this->getUtilizationRates($totalData[$t][0]['dn334'], $totalData[$t][0]['dn333'], 1);
                        $totalData[$t][0]['disp_type'] = 'float_percent';
                        break;

                    case '80': case '81': case '82':
                        $totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
                        $totalData[$t][0]['disp_type'] = 'float_unit';
                        break;

					case '66': case '67': 
						$totalData[$t][0]['data']      = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
						$totalData[$t][0]['disp_type'] = 'float';
						break;
					
					default:
                        $totalData[$t][0]['data'] = $totalData[$t][0]['data'] + $mainData[$t][$m]['data'];
                        break;
                }
            }
        }

		//月数分の平均値を計算
		//「外来1日平均」、「全病棟1日平均」、「平均在院日数（直近3カ月）」、「重症度、看護必要度割合」、「入院 療養病棟 医療適用 1日平均」、
		//「療養病棟介護1日平均」、「平均介護度」、「重症患者の割合」、「在宅復帰率」、「ﾘﾊﾋﾞﾘ平均提供単位数（一般）」、「ﾘﾊﾋﾞﾘ平均提供単位数（療養、精神）」、「ﾘﾊﾋﾞﾘ平均提供単位数（回復期リハ）」
		$totalData["2"][0]['data'] =  ($totalData["2"][0]['data'] / count($monthlyData));
		$totalData["4"][0]['data'] =  ($totalData["4"][0]['data'] / count($monthlyData));
		$totalData["13"][0]['data'] =  ($totalData["13"][0]['data'] / count($monthlyData));
		$totalData["16"][0]['data'] =  ($totalData["16"][0]['data'] / count($monthlyData));
		$totalData["36"][0]['data'] =  ($totalData["36"][0]['data'] / count($monthlyData));
		$totalData["50"][0]['data'] =  ($totalData["50"][0]['data'] / count($monthlyData));
		$totalData["66"][0]['data'] =  ($totalData["66"][0]['data'] / count($monthlyData));
		$totalData["67"][0]['data'] =  ($totalData["67"][0]['data'] / count($monthlyData));
		$totalData["80"][0]['data'] =  ($totalData["80"][0]['data'] / count($monthlyData));
		$totalData["81"][0]['data'] =  ($totalData["81"][0]['data'] / count($monthlyData));
		$totalData["82"][0]['data'] =  ($totalData["82"][0]['data'] / count($monthlyData));
		

        for ($i=1; $i <= $rowCount; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center))
                          .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }

        $rtnStr .= '</table>';

        return $rtnStr;
    }

    /**
     * No.75 [病院] 病院月間報告書(行為件数)のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $dateArr
     * @return string
     */
    function getHospitalMonthlyReport($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr     = '';
        $fontTag    = $this->getFontTag();
        $cYear      = $this->getArrValueByKey('year', $dateArr);
        $startMonth = $this->getFiscalYearStartMonth();

        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();

        # attr
        $rowspan_2         = array('name' => 'rowspan',          'value' =>  2);
        $rowspan_3         = array('name' => 'rowspan',          'value' =>  3);
        $rowspan_4         = array('name' => 'rowspan',          'value' =>  4);
        $rowspan_5         = array('name' => 'rowspan',          'value' =>  5);
        $rowspan_9         = array('name' => 'rowspan',          'value' =>  9);
        $rowspan_17        = array('name' => 'rowspan',          'value' => 17);
        $colspan_2         = array('name' => 'colspan',          'value' =>  2);

        # style
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $rtnStr .= '<table class="list" width="100%" >';

        $leftSide              = array();
        $mainData              = array();
        $totalData             = array();
        $plantNameArr          = array();
        $plantNameAnderDisplay = array();

        $leftSide[0]  = array(0 => array('data' => '&nbsp;', 'attr' => array($rowspan_2, $colspan_2)));
        $leftSide[2]  = array(0 => array('data' => '手術件数', 'attr' => array($rowspan_5))
                            , 1 => array('data' => '全身麻酔による手術')
        );
        $leftSide[3]  = array(0 => array('data' => '硬膜外・脊椎麻酔による手術'));
        $leftSide[4]  = array(0 => array('data' => '上・下肢伝達麻酔による手術'));
        $leftSide[5]  = array(0 => array('data' => '内視鏡的手術'));
        $leftSide[6]  = array(0 => array('data' => '白内障手術'));
        $leftSide[7]  = array(0 => array('data' => '内視鏡', 'attr' => array($rowspan_3))
                            , 1 => array('data' => '食道・胃・十二指腸ファイバー')
        );
        $leftSide[8]  = array(0 => array('data' => '大腸・直腸ファイバー'));
        $leftSide[9]  = array(0 => array('data' => 'その他'));
        $leftSide[10] = array(0 => array('data' => '検査', 'attr' => array($rowspan_9))
                            , 1 => array('data' => '心電図')
        );
        $leftSide[11] = array(0 => array('data' => 'ホルター型心電図'));
        $leftSide[12] = array(0 => array('data' => 'トレッドミル'));
        $leftSide[13] = array(0 => array('data' => '心臓超音波'));
        $leftSide[14] = array(0 => array('data' => '胸腹部超音波'));
        $leftSide[15] = array(0 => array('data' => 'その他超音波(頭頸部・四肢等)'));
        $leftSide[16] = array(0 => array('data' => '肺機能'));
        $leftSide[17] = array(0 => array('data' => '骨塩定量'));
        $leftSide[18] = array(0 => array('data' => '血液ガス分析'));
        $leftSide[19] = array(0 => array('data' => '放射線', 'attr' => array($rowspan_17))
                            , 1 => array('data' => '一般撮影')
        );
        $leftSide[20] = array(0 => array('data' => 'マンモグラフィー'));
        $leftSide[21] = array(0 => array('data' => 'MDL(UGI)'));
        $leftSide[22] = array(0 => array('data' => 'BEM'));
        $leftSide[23] = array(0 => array('data' => 'DIP'));
        $leftSide[24] = array(0 => array('data' => 'DIC'));
        $leftSide[25] = array(0 => array('data' => '特殊'));
        $leftSide[26] = array(0 => array('data' => 'その他'));
        $leftSide[27] = array(0 => array('data' => 'CT(BRAIN)'));
        $leftSide[28] = array(0 => array('data' => 'CT(BODY)'));
        $leftSide[29] = array(0 => array('data' => 'CT(その他)'));
        $leftSide[30] = array(0 => array('data' => 'CT造影剤加算'));
        $leftSide[31] = array(0 => array('data' => 'MRI(BRAIN)'));
        $leftSide[32] = array(0 => array('data' => 'MRI(BODY)'));
        $leftSide[33] = array(0 => array('data' => 'MRI(その他)'));
        $leftSide[34] = array(0 => array('data' => 'MRI造影剤加算'));
        $leftSide[35] = array(0 => array('data' => 'Angio'));
        $leftSide[36] = array(0 => array('data' => '重点項目', 'attr' => array($rowspan_17))
                            , 1 => array('data' => '超重症児(者)入院診療加算(イ・ロ)')
        );
        $leftSide[37] = array(0 => array('data' => '栄養サポートチーム加算'));
        $leftSide[38] = array(0 => array('data' => '呼吸ケアチーム加算'));
        $leftSide[39] = array(0 => array('data' => '医療安全対策加算(1・2)'));
        $leftSide[40] = array(0 => array('data' => '感染防止対策加算'));
        $leftSide[41] = array(0 => array('data' => '医療機器安全管理料(1・2)'));
        $leftSide[42] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_1').')'));
        $leftSide[43] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_2').')'));
        $leftSide[44] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_3').')'));
        $leftSide[45] = array(0 => array('data' => '医薬品安全性情報等管理体制加算'));
        $leftSide[46] = array(0 => array('data' => '&nbsp;'));
        $leftSide[47] = array(0 => array('data' => '&nbsp;'));
        $leftSide[48] = array(0 => array('data' => '&nbsp;'));
        $leftSide[49] = array(0 => array('data' => '&nbsp;'));
        $leftSide[50] = array(0 => array('data' => '&nbsp;'));
        $leftSide[51] = array(0 => array('data' => '&nbsp;'));
        $leftSide[52] = array(0 => array('data' => '&nbsp;'));

        $rowCnt = count($leftSide);
        for ($m = 0; $m < 12; $m++) {
            $year  = ($startMonth+$m>12)? $cYear + 1 : $cYear;
            $month = ($startMonth+$m>12)? $startMonth + $m - 12 : $startMonth + $m;

            $plantNameArr[$m]          = array('data' => sprintf('%d月', $month), 'style' => array('bgColor' => $bgColor_blue));
//            $plantNameAnderDisplay[$m] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));

            $dispNum = array();
            for ($i = 342; $i <= 392; $i++) { $dispNum[$i] = null; }
            $status = $this->checkCellDataStatus('hospital_monthly_report', array('year' => $year, 'month' => $month));

            foreach ($dataArr as $dataKey => $dataVal) {
                if ($year == $dataVal['year'] && sprintf('%02d', $month) == $dataVal['month']) {
                    $status = $this->checkCellDataStatus('hospital_monthly_report', $dataVal);
                    for ($i = 342; $i <= 392; $i++) { $dispNum[$i] = $this->getArrValueByKey('dispnum_'.$i, $dataVal); }
                }
            }

            $startNum = 2;
            for ($i = 342; $i <= 392; $i++) {
                $mainData[$startNum][$m] = array('data' => $dispNum[$i], 'status' => $status);
                $startNum++;
            }

            for ($i = 2; $i <= $rowCnt; $i++) {
                $totalData[$i][0] = array(
                    'data' => $totalData[$i][0]['data'] + $mainData[$i][$m]['data'],
                    'status' => $this->checkCellDataStatus('comp', array($totalData[$i][0]['status'], $mainData[$i][$m]['status']))
                );
            }
        }

        $plantNameArr[]          = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue));
//        $plantNameAnderDisplay[] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));

        $mainData[0] = $plantNameArr;
        $mainData[1] = $plantNameAnderDisplay;

        $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSide[0], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
                      .$this->convertCellData($mainData[0], $fontTag, array('textAlign' => $textAlign_center))
                  .'</tr>';
//                  .'<tr>'
//                      .$this->convertCellData($mainData[1], $fontTag, array('textAlign' => $textAlign_center))
//                  .'</tr>';

//        for ($i = 2; $i <= $rowCnt; $i++) {
        for ($i = 1; $i <= $rowCnt; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSide[$i],  $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_center))
                          .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_center))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.76 [病院] 外来診療行為別明細のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $dateArr
     * @return string
     */
    function getOutpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = '';

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $bgColor_header    = array('name' => 'background-color', 'value' => '#ffc8e9');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $topHeaderArr = array(
             0 => array('data' => '診療科'),  1 => array('data' => '件数'),    2 => array('data' => '日数'),      3 => array('data' => '初診料')
           , 4 => array('data' => '再診料'),  5 => array('data' => '指導料'),  6 => array('data' => '在宅医療'),  7 => array('data' => '院内処方')
           , 8 => array('data' => '投薬料'),  9 => array('data' => '注射料'), 10 => array('data' => '処置料'),   11 => array('data' => '手術・麻酔料')
          , 12 => array('data' => '検査料'), 13 => array('data' => 'X線料'),  14 => array('data' => 'リハビリ'), 15 => array('data' => '処方箋料')
          , 16 => array('data' => 'その他'), 17 => array('data' => '合計')
        );

        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '内科')),        1 => array(0 => array('data' => '神経内科')),  2 => array(0 => array('data' => '呼吸器科'))
           , 3 => array(0 => array('data' => '消化器科')),    4 => array(0 => array('data' => '循環器科')),  5 => array(0 => array('data' => '小児科'))
           , 6 => array(0 => array('data' => '外科')),        7 => array(0 => array('data' => '整形外科')),  8 => array(0 => array('data' => '形成(美)外科'))
           , 9 => array(0 => array('data' => '脳神経外科')), 10 => array(0 => array('data' => '皮膚科')),   11 => array(0 => array('data' => '泌尿器科'))
          , 12 => array(0 => array('data' => '産婦人科')),   13 => array(0 => array('data' => '眼科')),     14 => array(0 => array('data' => '耳鼻咽喉科'))
          , 15 => array(0 => array('data' => '透析科')),     16 => array(0 => array('data' => '精神科')),   17 => array(0 => array('data' => '歯科'))
          , 18 => array(0 => array('data' => '放射線科')),   19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '総合計', 'style' => array($textAlign_center)))
        );

        $cellNameArr = array(
             0 => 'a',  1 => 'b',  2 => 'c',  3 => 'd',  4 => 'e',  5 => 'f',  6 => 'g',  7 => 'h',  8 => 'i',  9 => 'j'
          , 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p', 16 => 'q', 17 => 'r', 18 => 's', 19 => 't'
        );

        $dispData = array();
        for ($r=0; $r<=19; $r++) {
            for ($c=0; $c<=15; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $mainDataArr     = array();
        $totalRowDataArr = array();
        $totalColDataArr = array();

        $status = $this->checkCellDataStatus('outpatientCareActDetails', array('status' => $dispData['jnl_status'], 'year' => $displayYear, 'month' => $displayMonth));

        for ($r=0; $r<=19; $r++) {
            for ($c=0; $c<=16; $c++) {
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        break;

                    case '1':
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_days'], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_days'], 'status' => $status);
                        break;

                    case '16':
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_side_total'], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_side_total'], 'status' => $status);
                        break;

                    default:
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_'.($c-1)], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_'.($c-1)], 'status' => $status);
                        break;
                }
            }
        }

        $rtnStr .= '<table width="100%" border="0" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => $bgColor_header)).'</tr>';

        # メインデータ行
        for ($i=0; $i<=19; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        }

        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[20], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[20],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.77 [病院] 外来日当点のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string
     */
    function getOutpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = '';

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $bgColor_header    = array('name' => 'background-color', 'value' => '#ffc8e9');

        $topHeaderArr = array(
             0 => array('data' => '診療科'),  1 => array('data' => '件数'),    2 => array('data' => '日数'),       3 => array('data' => '初診料')
           , 4 => array('data' => '再診料'),  5 => array('data' => '指導料'),  6 => array('data' => '在宅医療'),  7 => array('data' => '院内処方')
           , 8 => array('data' => '投薬料'),  9 => array('data' => '注射料'), 10 => array('data' => '処置料'),   11 => array('data' => '手術・麻酔料')
          , 12 => array('data' => '検査料'), 13 => array('data' => 'X線料'),  14 => array('data' => 'リハビリ'), 15 => array('data' => '処方箋料')
          , 16 => array('data' => 'その他'), 17 => array('data' => '合計')
        );

        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '内科')),        1 => array(0 => array('data' => '神経内科')),  2 => array(0 => array('data' => '呼吸器科'))
           , 3 => array(0 => array('data' => '消化器科')),    4 => array(0 => array('data' => '循環器科')),  5 => array(0 => array('data' => '小児科'))
           , 6 => array(0 => array('data' => '外科')),        7 => array(0 => array('data' => '整形外科')),  8 => array(0 => array('data' => '形成(美)外科'))
           , 9 => array(0 => array('data' => '脳神経外科')), 10 => array(0 => array('data' => '皮膚科')),   11 => array(0 => array('data' => '泌尿器科'))
          , 12 => array(0 => array('data' => '産婦人科')),   13 => array(0 => array('data' => '眼科')),     14 => array(0 => array('data' => '耳鼻咽喉科'))
          , 15 => array(0 => array('data' => '透析科')),     16 => array(0 => array('data' => '精神科')),   17 => array(0 => array('data' => '歯科'))
          , 18 => array(0 => array('data' => '放射線科')),   19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '総合計', 'style' => array($textAlign_center)))
        );

        $cellNameArr = array(
             0 => 'a',  1 => 'b',  2 => 'c',  3 => 'd',  4 => 'e',  5 => 'f',  6 => 'g',  7 => 'h',  8 => 'i',  9 => 'j'
          , 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p', 16 => 'q', 17 => 'r', 18 => 's', 19 => 't'
        );

        $dispData = array();
        for ($r=0; $r<=19; $r++) {
            for ($c=0; $c<=15; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # このあたりで、データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $mainDataArr     = array();
        $totalRowDataArr = array();
        $totalColDataArr = array();

        $status = $this->checkCellDataStatus('outpatientDailyAllowancePoint', array('status' => $dispData['jnl_status'], 'year' => $displayYear, 'month' => $displayMonth));

        for ($r=0; $r<=19; $r++) {
            $dayCnt = $dispData[$cellNameArr[$r].'_days'];
            for ($c=0; $c<=15; $c++) {
                $mainDataArr[$r][$c]['status'] = $status;
                $totalRowDataArr[$c]['status'] = $status;
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        break;

                    case '1':
                        $mainDataArr[$r][$c] = array('data' => $dayCnt, 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dayCnt, 'status' => $status);
                        break;

                    default:
                        $mainDataArr[$r][$c] = array(
                            'data'        => empty($dayCnt)? 0 : round($dispData[$cellNameArr[$r].'_'.($c-1)] / $dayCnt, 1)
                          , 'disp_type'   => 'float'
                          , 'numerator'   => $dispData[$cellNameArr[$r].'_'.($c-1)]
                          , 'denominator' => $dayCnt
                          , 'status'      => $status
                        );

                        # 合計列
                        $totalColDataArr[$r][0]['numerator']   = $totalColDataArr[$r][0]['numerator'] + $mainDataArr[$r][$c]['numerator'];
                        $totalColDataArr[$r][0]['denominator'] = $dayCnt;
                        if ($c == 15) {
                            $totalColDataArr[$r][0]['data']   = empty($totalColDataArr[$r][0]['denominator'])? 0 : round($totalColDataArr[$r][0]['numerator'] / $totalColDataArr[$r][0]['denominator'], 1);
                            $totalColDataArr[$r][0]['status'] = $status;
							$totalColDataArr[$r][0]['disp_type'] = 'float';
						}

                        # 総合計行
                        $totalRowDataArr[$c]['numerator']   = $totalRowDataArr[$c]['numerator']   + $mainDataArr[$r][$c]['numerator'];
                        $totalRowDataArr[$c]['denominator'] = $totalRowDataArr[$c]['denominator'] + $mainDataArr[$r][$c]['denominator'];
                        $totalRowDataArr[$c]['data']        = empty($totalRowDataArr[$c]['denominator'])? 0 : round($totalRowDataArr[$c]['numerator'] / $totalRowDataArr[$c]['denominator'], 1);
                        $totalRowDataArr[$c]['disp_type']   = 'float';
                        break;
                }
            }
        }

        $average     = 0;
        $numerator   = 0;
        $denominator = 0;

        foreach ($totalRowDataArr as $key => $val) {
            switch ($key) {
                case '0': case '1': # 件数と日数は合計しない
                    break;

                default:
                    $numerator   = $numerator   + $val['numerator'];
                    $denominator = $totalRowDataArr[1]['data'];
                    if ($key == 15) { $average = empty($denominator)? 0 : round($numerator / $denominator, 1); }
                    break;
            }
        }
		$totalColDataArr[20][0] = array('data' => $average, 'status' => $status, 'disp_type' => 'float');

        $rtnStr .= '<table width="100%" border="0" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => $bgColor_header)).'</tr>';

        # メインデータ行
        for ($i=0; $i<=19; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[$i],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        }

        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[20], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[20],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.78 [病院] 入院診療行為別明細のデータ出力表示
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string
     */
    function getInpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = '';

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $colspan_2         = array('name' => 'colspan',          'value' => 2);
        $rowspan_20        = array('name' => 'rowspan',          'value' => 20);

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));

        $topHeaderArr = array(
              0 => array('data' => '病床種別/診療科', 'attr' => array($colspan_2)),     1 => array('data' => '件数'),    2 => array('data' => '日数')
			, 3 => array('data' => '初診料'),    4 => array('data' => '指導料'),        5 => array('data' => '投薬料'),  6 => array('data' => '注射料')
            , 7 => array('data' => '処置料'),    8 => array('data' => '手術・麻酔料'),  9 => array('data' => '検査料'), 10 => array('data' => 'X線料')
           , 11 => array('data' => 'リハビリ'), 12 => array('data' => 'その他'),       13 => array('data' => '入院料'), 14 => array('data' => 'DPC包括評価')
           , 15 => array('data' => '食事療養'), 16 => array('data' => '合計')
        );

        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '一般病棟', 'attr' => array($rowspan_20)), 1 => array('data' => '内科')), 1 => array(0 => array('data' => '神経内科'))
           , 2 => array(0 => array('data' => '呼吸器科')),     3 => array(0 => array('data' => '消化器科')),           4 => array(0 => array('data' => '循環器科'))
           , 5 => array(0 => array('data' => '小児科')),       6 => array(0 => array('data' => '外科')),               7 => array(0 => array('data' => '整形外科'))
           , 8 => array(0 => array('data' => '形成(美)外科')), 9 => array(0 => array('data' => '脳神経外科')),        10 => array(0 => array('data' => '皮膚科'))
          , 11 => array(0 => array('data' => '泌尿器科')),    12 => array(0 => array('data' => '産婦人科')),          13 => array(0 => array('data' => '眼科'))
          , 14 => array(0 => array('data' => '耳鼻咽喉科')),  15 => array(0 => array('data' => '透析科')),            16 => array(0 => array('data' => '精神科'))
          , 17 => array(0 => array('data' => '歯科')),        18 => array(0 => array('data' => '放射線科')),          19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '一般病棟合計',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)))
          , 21 => array(0 => array('data' => '障害者病棟',       'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#cccccc'))))
          , 22 => array(0 => array('data' => '回復期リハ病床',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffa4b8'))))
          , 23 => array(0 => array('data' => '亜急性期病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 24 => array(0 => array('data' => 'ICU及びハイケア',  'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffe8b7'))))
          , 25 => array(0 => array('data' => '小児・入院管理料', 'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#d5ffc8'))))
          , 26 => array(0 => array('data' => '療養病棟(医療)',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#aaddaa'))))
          , 27 => array(0 => array('data' => '介護病棟',         'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#b7c7ff'))))
          , 28 => array(0 => array('data' => '精神一般病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#c8ddff'))))
          , 29 => array(0 => array('data' => '精神療養病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#a4e9ff'))))
          , 30 => array(0 => array('data' => '特殊疾患病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#dbb7ff'))))
          , 31 => array(0 => array('data' => '認知症治療病棟',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 32 => array(0 => array('data' => '緩和ケア病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffddc8'))))
          , 33 => array(0 => array('data' => '総合計',           'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center, 'bgColor' => array('name' => 'background-color', 'value' => '#ccddbb'))))
        );

        $cellNameArr = array(
             0 =>  'a',  1 =>  'b',  2 =>  'c',  3 =>  'd',  4 =>  'e',  5 =>  'f',  6 =>  'g',  7 =>  'h',  8 =>  'i',  9 =>  'j', 10 =>  'k'
          , 11 =>  'l', 12 =>  'm', 13 =>  'n', 14 =>  'o', 15 =>  'p', 16 =>  'q', 17 =>  'r', 18 =>  's', 19 =>  't', 20 =>  'u', 21 =>  'v'
          , 22 =>  'w', 23 =>  'x', 24 =>  'y', 25 =>  'z', 26 => 'ab', 27 => 'ac', 28 => 'ad', 29 => 'ae', 30 => 'af', 31 => 'ag'
        );

        $dispData = array();
        for ($r=0; $r<=32; $r++) {
            for ($c=0; $c<=14; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # このあたりで、データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $status = $this->checkCellDataStatus('inpatientCareActDetails', array('status' => $dispData['jnl_status'], 'year' => $displayYear, 'month' => $displayMonth));

        $mainDataArr                = array();
        $generalWardTotalRowDataArr = array();
        $totalRowDataArr            = array();
        $totalColDataArr            = array();

        for ($r=0; $r<=31; $r++) {
            for ($c=0; $c<=14; $c++) {
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        if ($r < 20) { $generalWardTotalRowDataArr[$c] = array('data' => $generalWardTotalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_account'], 'status' => $status); }
                        break;

                    case '1':
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_days'], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_days'], 'status' => $status);
                        if ($r < 20) { $generalWardTotalRowDataArr[$c] = array('data' => $generalWardTotalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_days'], 'status' => $status); }
                        break;

                    default:
                        # DPC包括評価は途中からデータ無し
                        if ($r >= 20 && $c == 13) {
                            $mainDataArr[$r][$c]['data'] = null;
                            $totalRowDataArr[$c]['data'] = $totalRowDataArr[$c]['data'] + null;
                        }
                        else {
                            $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_'.($c-1)], 'status' => $status);
                            $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_'.($c-1)], 'status' => $status);
                        }

                        if ($r < 20) { $generalWardTotalRowDataArr[$c] = array('data' => $generalWardTotalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_'.($c-1)], 'status' => $status); }
                        $totalColDataArr[$r][0] = array('data' => $totalColDataArr[$r][0]['data'] + $dispData[$cellNameArr[$r].'_'.($c-1)], 'status' => $status);
                        break;
                }
            }
        }

        $generalWardTotal = 0;
        $total = 0;
        foreach ($totalRowDataArr as $key => $val) {
            switch ($key) {
                case '0': case '1': # 件数と日数は合計しない
                    break;

                default:
                    $total = $total + $val['data'];
                    break;
            }
        }
        foreach ($generalWardTotalRowDataArr as $key => $val) {
            switch ($key) {
                case '0': case '1': # 件数と日数は合計しない
                    break;

                default:
                    $generalWardTotal = $generalWardTotal + $val['data'];
                    break;
            }
        }
        $generalWardTotalRowDataArr[15] = array('data' => $generalWardTotal, 'status' => $status);
        $totalColDataArr[33][0]         = array('data' => $total,            'status' => $status);

        # ここから表示データ
        $rtnStr .= '<table width="100%" border="0" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8'))).'</tr>';

        # メインデータ行
        for ($i=0; $i<20; $i++) {

            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[$i],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';

        }
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSideHeaderArr[20],      $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                          .$this->convertCellData($generalWardTotalRowDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                          .'</tr>';

        for ($i=21; $i<33; $i++) {

            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left))
                      .$this->convertCellData($mainDataArr[$i-1],     $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[$i-1], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';

        }


        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[33], $fontTag, array('whiteSpace' => $whiteSpace_nowrap))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[33],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.79 [病院] 入院日当点のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string
     */
    function getInpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = '';

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $colspan_2         = array('name' => 'colspan',          'value' => 2);
        $rowspan_20        = array('name' => 'rowspan',          'value' => 20);

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $bgColor_header    = array('name' => 'background-color', 'value' => '#8effa8');
        #

        $topHeaderArr = array(
              0 => array('data' => '病床種別/診療科', 'attr' => array($colspan_2)),  1 => array('data' => '件数'),         2 => array('data' => '日数')
            , 3 => array('data' => '初診料'),       4 => array('data' => '指導料'),  5 => array('data' => '投薬料'),       6 => array('data' => '注射料'),       7 => array('data' => '処置料')
            , 8 => array('data' => '手術・麻酔料'), 9 => array('data' => '検査料'), 10 => array('data' => 'X線料'),       11 => array('data' => 'リハビリ')
           , 12 => array('data' => 'その他'),      13 => array('data' => '入院料'), 14 => array('data' => 'DPC包括評価'), 15 => array('data' => '食事療養')
           , 16 => array('data' => '合計')
        );

        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '一般病棟', 'attr' => array($rowspan_20)), 1 => array('data' => '内科')), 1 => array(0 => array('data' => '神経内科'))
           , 2 => array(0 => array('data' => '呼吸器科')),     3 => array(0 => array('data' => '消化器科')),           4 => array(0 => array('data' => '循環器科'))
           , 5 => array(0 => array('data' => '小児科')),       6 => array(0 => array('data' => '外科')),               7 => array(0 => array('data' => '整形外科'))
           , 8 => array(0 => array('data' => '形成(美)外科')), 9 => array(0 => array('data' => '脳神経外科')),        10 => array(0 => array('data' => '皮膚科'))
          , 11 => array(0 => array('data' => '泌尿器科')),    12 => array(0 => array('data' => '産婦人科')),          13 => array(0 => array('data' => '眼科'))
          , 14 => array(0 => array('data' => '耳鼻咽喉科')),  15 => array(0 => array('data' => '透析科')),            16 => array(0 => array('data' => '精神科'))
          , 17 => array(0 => array('data' => '歯科')),        18 => array(0 => array('data' => '放射線科')),          19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '一般病棟合計',     'attr' => array($colspan_2), 'style' => array($textAlign_center)))
          , 21 => array(0 => array('data' => '障害者病棟',       'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#cccccc'))))
          , 22 => array(0 => array('data' => '回復期リハ病床',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffa4b8'))))
          , 23 => array(0 => array('data' => '亜急性期病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 24 => array(0 => array('data' => 'ICU及びハイケア',  'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffe8b7'))))
          , 25 => array(0 => array('data' => '小児・入院管理料', 'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#d5ffc8'))))
          , 26 => array(0 => array('data' => '療養病棟(医療)',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#aaddaa'))))
          , 27 => array(0 => array('data' => '介護病棟',         'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#b7c7ff'))))
          , 28 => array(0 => array('data' => '精神一般病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#c8ddff'))))
          , 29 => array(0 => array('data' => '精神療養病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#a4e9ff'))))
          , 30 => array(0 => array('data' => '特殊疾患病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#dbb7ff'))))
          , 31 => array(0 => array('data' => '認知症治療病棟',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 32 => array(0 => array('data' => '緩和ケア病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffddc8'))))
          , 33 => array(0 => array('data' => '総合計',           'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ccddbb'), 'textAlign' => $textAlign_center)))
        );

        $cellNameArr = array(
             0 =>  'a',  1 =>  'b',  2 =>  'c',  3 =>  'd',  4 =>  'e',  5 =>  'f',  6 =>  'g',  7 =>  'h',  8 =>  'i',  9 =>  'j', 10 =>  'k'
          , 11 =>  'l', 12 =>  'm', 13 =>  'n', 14 =>  'o', 15 =>  'p', 16 =>  'q', 17 =>  'r', 18 =>  's', 19 =>  't', 20 =>  'u', 21 =>  'v'
          , 22 =>  'w', 23 =>  'x', 24 =>  'y', 25 =>  'z', 26 => 'ab', 27 => 'ac', 28 => 'ad', 29 => 'ae', 30 => 'af', 31 => 'ag'
        );

        $dispData = array();
        for ($r=0; $r<=32; $r++) {
            for ($c=0; $c<=13; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # このあたりで、データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $status = $this->checkCellDataStatus('inpatientDailyAllowancePoint', array('status' => $dispData['jnl_status'], 'year' => $displayYear, 'month' => $displayMonth));

        $mainDataArr     = array();
        $generalWardTotalRowDataArr = array();
        $totalRowDataArr = array();
        $totalColDataArr = array();

        for ($r=0; $r<=32; $r++) {
            for ($c=0; $c<=14; $c++) {
                $dayCnt = $dispData[$cellNameArr[$r].'_days'];
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c] = array('data' => $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_account'], 'status' => $status);
                        if ($r < 20) { $generalWardTotalRowDataArr[$c] = array('data' => $generalWardTotalRowDataArr[$c]['data'] + $dispData[$cellNameArr[$r].'_account'], 'status' => $status); }
                        break;

                    case '1':
                        $mainDataArr[$r][$c] = array('data' => $dayCnt, 'status' => $status);
                        $totalRowDataArr[$c] = array('data' => $totalRowDataArr[$c]['data'] + $dayCnt, 'status' => $status);
                        if ($r < 20) { $generalWardTotalRowDataArr[$c] = array('data' => $generalWardTotalRowDataArr[$c]['data'] + $dayCnt, 'status' => $status); }
                        break;

                    default:
                        # DPC包括評価は途中からデータ無し
                        if ($r >= 20 && $c == 13) {
                            $mainDataArr[$r][$c]['data']        = null;
                            $totalRowDataArr[$c]['numerator']   = $totalRowDataArr[$c]['numerator'] + null;
                            $totalRowDataArr[$c]['denominator'] = $totalRowDataArr[$c]['denominator'] + null;
                            $totalRowDataArr[$c]['data']        = empty($totalRowDataArr[$c]['denominator'])? 0 : round($totalRowDataArr[$c]['numerator'] / $totalRowDataArr[$c]['denominator'], 1);
                            $totalRowDataArr[$c]['disp_type']   = 'float';
                            $totalRowDataArr[$c]['status']      = $status;
                        }
                        else {
                            $mainDataArr[$r][$c]['numerator']   = $mainDataArr[$r][$c]['numerator'] + $dispData[$cellNameArr[$r].'_'.($c-1)];
                            $mainDataArr[$r][$c]['denominator'] = $dayCnt;
                            $mainDataArr[$r][$c]['data']        = empty($mainDataArr[$r][1]['data'])? 0 : round($mainDataArr[$r][$c]['numerator'] / $mainDataArr[$r][1]['data'], 1);
                            $mainDataArr[$r][$c]['disp_type']   = 'float';
                            $mainDataArr[$r][$c]['status']      = $status;
                            $totalRowDataArr[$c]['numerator']   = $totalRowDataArr[$c]['numerator'] + $dispData[$cellNameArr[$r].'_'.($c-1)];
                            $totalRowDataArr[$c]['denominator'] = $mainDataArr[$r][1]['data'];
                            $totalRowDataArr[$c]['disp_type']   = 'float';
                            $totalRowDataArr[$c]['status']      = $status;
                        }

                        if ($r < 20) {
                            $generalWardTotalRowDataArr[$c]['numerator']   = $generalWardTotalRowDataArr[$c]['numerator'] + $dispData[$cellNameArr[$r].'_'.($c-1)];
                            $generalWardTotalRowDataArr[$c]['denominator'] = $generalWardTotalRowDataArr[1]['data'];
                            $generalWardTotalRowDataArr[$c]['data']        = empty($generalWardTotalRowDataArr[$c]['denominator'])? 0 : round($generalWardTotalRowDataArr[$c]['numerator'] / $generalWardTotalRowDataArr[$c]['denominator'], 1);
                            $generalWardTotalRowDataArr[$c]['disp_type']   = 'float';
                            $generalWardTotalRowDataArr[$c]['status']      = $status;
                        }
                        $totalColDataArr[$r][0]['numerator']   = $totalColDataArr[$r][0]['numerator'] + $mainDataArr[$r][$c]['numerator'];
                        $totalColDataArr[$r][0]['denominator'] = $mainDataArr[$r][1]['data'];
                        $totalColDataArr[$r][0]['data']        = empty($totalColDataArr[$r][0]['denominator'])? 0 : round($totalColDataArr[$r][0]['numerator'] / $totalColDataArr[$r][0]['denominator'], 1);
                        $totalColDataArr[$r][0]['disp_type']   = 'float';
                        $totalColDataArr[$r][0]['status']      = $status;
                        break;
                }
            }
        }

        $generalWardNumerator   = 0;
        $generalWardDenominator = 0;
        $generalWardTotal       = 0;
        $numerator              = 0;
        $denominator            = 0;
        $total                  = 0;

        $totalNumerator         = 0;

        foreach ($totalRowDataArr as $key => $val) {
            switch ($key) {
                case '0': case '1': # 件数と日数は合計しない
                    break;

                default:
                    $numerator   = $val['numerator'];
                    $totalNumerator = $totalNumerator + $numerator;
                    $denominator = $totalRowDataArr[1]['data'];
                    $denominator = ($key==13)? $generalWardTotalRowDataArr[13]['denominator'] : $denominator;
                    $total       = empty($denominator)? 0 : round($numerator / $denominator, 1);
                    $totalRowDataArr[$key]['data'] = $total;
                    break;
            }
        }

        foreach ($generalWardTotalRowDataArr as $key => $val) {
            switch ($key) {
                case '0': case '1': # 件数と日数は合計しない
                    break;

                default:
                    $generalWardNumerator   = $generalWardNumerator   + $val['numerator'];
                    $generalWardDenominator = $generalWardTotalRowDataArr[1]['data'];
                    $generalWardTotal       = empty($generalWardDenominator)? 0 : round($generalWardNumerator / $generalWardDenominator, 1);
                    break;
            }
        }
        $generalWardTotalRowDataArr[15] = array('data' => $generalWardTotal, 'disp_type' => 'float', 'status' => $status);

        $totalSideTotalData = empty($totalRowDataArr[1]['data'])? 0 : round($totalNumerator / $totalRowDataArr[1]['data'], 1);
        $totalColDataArr[33][0] = array('data' => $totalSideTotalData, 'disp_type' => 'float', 'status' => $status);

        # ここから表示データ
        $rtnStr .= '<table width="100%" border="0" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8'))).'</tr>';

        # メインデータ行
        for ($i=0; $i<20; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[$i],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        }
         $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[20],      $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                      .$this->convertCellData($generalWardTotalRowDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        for ($i=21; $i<33; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left))
                      .$this->convertCellData($mainDataArr[$i-1],     $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[$i-1], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        }

        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[33], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .$this->convertCellData($totalColDataArr[33],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.80 [病院] 診療報酬総括表[医科]
     * No.81 [病院] 診療報酬総括表[歯科]のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string
     */
    function getMedicalTreatmentFeeGeneralizationTable($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = '';

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # style
        $textAlign_center   = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left     = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right    = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap  = array('name' => 'white-space',      'value' => 'nowrap');

        $bgColor_salmon     = array('name' => 'background-color', 'value' => $this->getRgbColor('salmon'));
        $bgColor_cadetBlue  = array('name' => 'background-color', 'value' => $this->getRgbColor('cadet_blue'));
        $bgColor_orange     = array('name' => 'background-color', 'value' => $this->getRgbColor('orange'));
        $bgColor_purple     = array('name' => 'background-color', 'value' => $this->getRgbColor('purple'));
        $bgColor_lightGreen = array('name' => 'background-color', 'value' => $this->getRgbColor('light_green'));
        $bgColor_cadetGreen = array('name' => 'background-color', 'value' => $this->getRgbColor('cadet_green'));
        $bgColor_lightBrown = array('name' => 'background-color', 'value' => $this->getRgbColor('light_brown'));
        $bgColor_pink       = array('name' => 'background-color', 'value' => $this->getRgbColor('pink'));
        $bgColor_yellow     = array('name' => 'background-color', 'value' => "#FFF975");

        # attr
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_15 = array('name' => 'colspan', 'value' => 15);
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_12 = array('name' => 'rowspan', 'value' => 12);

        # top header
        $topHeaderArr = array(
            0 => array(
                0 => array('data' => '&nbsp;', 'attr' => array($colspan_3, $rowspan_2))
              , 1 => array('data' => '入院',   'attr' => array($colspan_4))
              , 2 => array('data' => '外来',   'attr' => array($colspan_4))
              , 3 => array('data' => '合計',   'attr' => array($colspan_4))
            )
          , 1 => array(
                 0 => array('data' => '件数')
               , 1 => array('data' => '日数', 'style' => array('bgColor' => $bgColor_orange))
               , 2 => array('data' => '点数', 'style' => array('bgColor' => $bgColor_purple))
               , 3 => array('data' => '金額', 'style' => array('bgColor' => $bgColor_lightGreen))
               , 4 => array('data' => '件数')
               , 5 => array('data' => '日数', 'style' => array('bgColor' => $bgColor_orange))
               , 6 => array('data' => '点数', 'style' => array('bgColor' => $bgColor_purple))
               , 7 => array('data' => '金額', 'style' => array('bgColor' => $bgColor_lightGreen))
               , 8 => array('data' => '件数')
               , 9 => array('data' => '日数', 'style' => array('bgColor' => $bgColor_orange))
              , 10 => array('data' => '点数', 'style' => array('bgColor' => $bgColor_purple))
              , 11 => array('data' => '金額', 'style' => array('bgColor' => $bgColor_lightGreen))
            )
        );

        # side header
        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '社保', 'attr' => array($rowspan_5))
                      , 1 => array('data' => '基金', 'attr' => array($rowspan_5))
                      , 2 => array('data' => '単独')
             )
           , 1 => array(0 => array('data' => '生保'))
           , 2 => array(0 => array('data' => '(旧)27老人'))
           , 3 => array(0 => array('data' => '公費併用'))
           , 4 => array(0 => array('data' => '小計1', 'style' => array($bgColor_yellow)))
           , 5 => array(0 => array('data' => '国保', 'attr' => array($rowspan_6))
                      , 1 => array('data' => '基金', 'attr' => array($rowspan_6))
                      , 2 => array('data' => '一般')
             )
           , 6 => array(0 => array('data' => '退職'))
           , 7 => array(0 => array('data' => '39長寿'))
           , 8 => array(0 => array('data' => '(旧)27老人'))
           , 9 => array(0 => array('data' => '公費併用'))
          , 10 => array(0 => array('data' => '小計2', 'style' => array($bgColor_yellow)))
          , 11 => array(0 => array('data' => '労災', 'attr' => array($colspan_2))
                      , 1 => array('data' => '小計3')
            )
          , 12 => array(0 => array('data' => '自賠', 'attr' => array($colspan_2))
                      , 1 => array('data' => '小計4')
            )
          , 13 => array(0 => array('data' => '合計 A', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          # 空白行
          , 14 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_15)))
          , 15 => array(0 => array('data' => "返戻<br />再請求分", 'attr' => array($rowspan_12))
                      , 1 => array('data' => '社保', 'attr' => array($rowspan_5))
                      , 2 => array('data' => '単独')
            )
          , 16 => array(0 => array('data' => '生保'))
          , 17 => array(0 => array('data' => '(旧)27老人'))
          , 18 => array(0 => array('data' => '公費併用'))
          , 19 => array(0 => array('data' => '小計1', 'style' => array($bgColor_yellow)))
          , 20 => array(0 => array('data' => '国保', 'attr' => array($rowspan_6))
                      , 1 => array('data' => '一般')
            )
          , 21 => array(0 => array('data' => '退職'))
          , 22 => array(0 => array('data' => '39長寿'))
          , 23 => array(0 => array('data' => '(旧)27老人'))
          , 24 => array(0 => array('data' => '公費併用'))
          , 25 => array(0 => array('data' => '小計2', 'style' => array($bgColor_yellow)))
          , 26 => array(0 => array('data' => '労災')
                      , 1 => array('data' => '小計3')
            )
          , 27 => array(0 => array('data' => '合計 B', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          # 空白行
          , 28 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_15)))
          , 29 => array(0 => array('data' => "査定･過誤<br />再審査請求分", 'attr' => array($rowspan_12))
                      , 1 => array('data' => '社保', 'attr' => array($rowspan_5))
                      , 2 => array('data' => '一般')
            )
          , 30 => array(0 => array('data' => '生保'))
          , 31 => array(0 => array('data' => '(旧)27老人'))
          , 32 => array(0 => array('data' => '公費併用'))
          , 33 => array(0 => array('data' => '小計1', 'style' => array($bgColor_yellow)))
          , 34 => array(0 => array('data' => '国保', 'attr' => array($rowspan_6))
                      , 1 => array('data' => '一般')
            )
          , 35 => array(0 => array('data' => '退職'))
          , 36 => array(0 => array('data' => '39長寿'))
          , 37 => array(0 => array('data' => '(旧)27老人'))
          , 38 => array(0 => array('data' => '公費併用'))
          , 39 => array(0 => array('data' => '小計2', 'style' => array($bgColor_yellow)))
          , 40 => array(0 => array('data' => '労災')
                      , 1 => array('data' => '小計3')
            )
          , 41 => array(0 => array('data' => '合計 C', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          , 42 => array(0 => array('data' => '当月請求 A - (B + C)', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          # 空白行
          , 43 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_15)))
          , 44 => array(0 => array('data' => '前年分', 'attr' => array($rowspan_3))
                      , 1 => array('data' => '当月請求分', 'attr' => array($colspan_2))
            )
          , 45 => array(0 => array('data' => '前年同月請求分', 'attr' => array($colspan_2)))
          , 46 => array(0 => array('data' => '差額', 'attr' => array($colspan_2), 'style' => array($bgColor_yellow)))
        );

        $cellDataNameArr = array(
             0 => 'a',  1 => 'b',  2 => 'c',  3 => 'd',  5 => 'e',  6 => 'f',  7 =>  'g',  8 =>  'h',  9 => 'i', 15 => 'j'
          , 16 => 'k', 17 => 'l', 18 => 'm', 20 => 'n', 21 => 'o', 22 => 'p', 23 =>  'q', 24 =>  'r', 29 => 's', 30 => 't'
          , 31 => 'u', 32 => 'v', 34 => 'w', 35 => 'x', 36 => 'y', 37 => 'z', 38 => 'ab', 39 => 'ac'
        );

        $mainData    = array();
        $mainDataArr = array();

        foreach ($cellDataNameArr as $key => $val) {
            for ($l=1; $l<=8; $l++) {
                $mainData[$val.'_'.$l] = null;
                if ($l < 5) { $mainData[$val.'_'.$l.'_'.($l + 4)] = null; }
            }
        }
        $kanaArr = array(
            0 =>  'a', 1 =>   'i',  2 =>  'u',  3 =>  'e',  4 => 'ka',  5 =>  'ki',  6 => 'ku',  7 => 'ke'
         ,  8 => 'sa', 9 => 'shi', 10 => 'su', 11 => 'se', 12 => 'ta', 13 => 'chi', 14 => 'te', 15 => 'to'
        );
        for ($k=0; $k<count($kanaArr); $k++) {
            $mainData['kana_'.$kanaArr[$k].'_1']   = null;
            $mainData['kana_'.$kanaArr[$k].'_2']   = null;
            $mainData['kana_'.$kanaArr[$k].'_1_2'] = null;
        }

        # データを $mainData に入力する
        if (is_array($dataArr) && count($dataArr)>0) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $mainData[$dataArrKey] = $dataArrVal; } }

        # main data
        for ($i=0; $i<count($leftSideHeaderArr); $i++) {
            $mainDataArr[$i] = array();
            $tmpArr          = array();
            switch ($i) {
                case '0': case '1': case '2': case '5': case '6': case '7': case '8': case '15': case '16': case '17': case '20': case '21': case '22': case '23':
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)]);
                        $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)]);
                        $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1).'_'.($l+5)]);
                    }
                    break;

                case '3': case '9': case '18': case '24': case '29': case '30': case '31': case '32': case '34': case '35': case '36': case '37': case '38':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)],            'style' => array($bgColor_salmon));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)],            'style' => array($bgColor_salmon));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_salmon));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)]);
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)]);
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1).'_'.($l+5)]);
                                break;
                        }
                    }
                    break;

                # 小計 1
                case '4': case '19':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    break;

                # 小計 2
                case '10': case '25':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i-5].$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i-5].$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i-5].$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i-5].$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i-5].$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i-5].$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    break;

                # 小計 3
                case '11':
                    $kanaAArr = array(0 => 'a', 1 => 'i', 2 => 'u', 3 => 'e');
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]    = array('data' => $mainData['kana_'.$kanaAArr[$l].'_1']);
                        $tmpArr[$l+4]  = array('data' => $mainData['kana_'.$kanaAArr[$l].'_2']);
                        $tmpArr[$l+8]  = array('data' => $mainData['kana_'.$kanaAArr[$l].'_1_2']);
                    }
                    break;

                # 小計 4
                case '12':
                    $kanaKaArr = array(0 => 'ka', 1 => 'ki', 2 => 'ku', 3 => 'ke');
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaKaArr[$l].'_1']);
                        $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaKaArr[$l].'_2']);
                        $tmpArr[$l+8] = array('data' => $mainData['kana_'.$kanaKaArr[$l].'_1_2']);
                    }
                    break;

                # 合計 A
                case '13':
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData['total_a_'.($l+1)], 'style' => array($bgColor_yellow));
                        $tmpArr[$l+4] = array('data' => $mainData['total_a_'.($l+5)], 'style' => array($bgColor_yellow));
                        $tmpArr[$l+8] = array('data' => $mainData['total_a_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                    }
                    break;

                # 小計 3
                case '26':
                    $kanaSaArr = array(0 => 'sa', 1 => 'shi', 2 => 'su', 3 => 'se');
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaSaArr[$l].'_1']);
                        $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaSaArr[$l].'_2']);
                        $tmpArr[$l+8] = array('data' => $mainData['kana_'.$kanaSaArr[$l].'_1_2']);
                    }
                    break;

                # 合計 B
                case '27':
                    for ($l=0; $l<8; $l++) { $tmpArr[$l] = array('data' => $mainData['total_b_'.($l+1)], 'style' => array($bgColor_yellow)); }
                    $tmpArr[8]  = array('data' => $mainData['total_b_1_5'], 'style' => array($bgColor_yellow));
                    $tmpArr[9]  = array('data' => $mainData['total_b_2_6'], 'style' => array($bgColor_yellow));
                    $tmpArr[10] = array('data' => $mainData['total_b_3_7'], 'style' => array($bgColor_yellow));
                    $tmpArr[11] = array('data' => $mainData['total_b_4_8'], 'style' => array($bgColor_yellow));
                    break;

                # 小計 1
                case '33':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1)],            'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+5)],            'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i-4].$cellDataNameArr[$i-3].$cellDataNameArr[$i-2].$cellDataNameArr[$i-1].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    break;

                # 小計 2
                case '39':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    break;

                # 小計 3
                case '40':
                    $kanaTaArr = array(0 => 'ta', 1 => 'chi', 2 => 'te', 3 => 'to');
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_1'],   'style' => array($bgColor_salmon));
                                $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_2'],   'style' => array($bgColor_salmon));
                                $tmpArr[$l+8] = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_1_2'], 'style' => array($bgColor_salmon));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_1']);
                                $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_2']);
                                $tmpArr[$l+8] = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_1_2']);
                                break;
                        }
                    }
                    break;

                # 合計 C
                case '41':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData['total_c_'.($l+1)],            'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData['total_c_'.($l+5)],            'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData['total_c_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData['total_c_'.($l+1)],            'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData['total_c_'.($l+5)],            'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => $mainData['total_c_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    break;

                # 当月請求 A - ( B + C )
                case '42':
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData['total_abc_'.($l+1)], 'style' => array($bgColor_yellow));
                        $tmpArr[$l+4] = array('data' => $mainData['total_abc_'.($l+5)], 'style' => array($bgColor_yellow));
                        $tmpArr[$l+8] = array('data' => $mainData['total_abc_'.($l+1).'_'.($l+5)], 'style' => array($bgColor_yellow));
                    }
                    break;

                case '44':
                    for ($l=1; $l<=12; $l++) { $tmpArr[$l] = array('data' => $mainData['claim_this_mon_'.$l]); }
                    break;

                case '45':
                    for ($l=1; $l<=12; $l++) { $tmpArr[$l] = array('data' => $mainData['claim_prev_year_'.$l]); }
                    break;

                case '46':
                    for ($l=1; $l<=12; $l++) { $tmpArr[$l] = array('data' => $mainData['difference_'.$l], 'style' => array($bgColor_yellow)); }
                    break;

                # 空白行は何もしない
                case '14': case '28': case '43':
                    break;

                default:
                    for ($l=0; $l<12; $l++) { $tmpArr[$l] = array('data' => '&nbsp;'); }
                    break;
            }
            $mainDataArr[$i] = $tmpArr;
        }

        # ここから表示データ
        $rtnStr .= '<table width="100%" border="0" >'
                      .'<tr>'
                      .$this->convertCellData(
                          array(0 => array('data' => $facilityName, 'style' => array('textAlign' => $textAlign_left))
                              , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          )
                        , $fontTag
                        , array()
                      )
                      .'</tr>';
        $rtnStr .= '</table>';

        $rtnStr .= '<table class="list" width="100%" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_cadetBlue, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap)).'</tr>';

        for ($i=0; $i <= 14; $i++) {
            switch ($i) {
                case '14':
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array($textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap))
                              .'</tr>';
                    break;

                default:
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_cadetBlue, $textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .=     '<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_cadetGreen, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap)).'</tr>';

        for ($i=15; $i <= 28; $i++) {
            switch ($i) {
                case '28':
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array($textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap))
                              .'</tr>';
                    break;

                default:
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_cadetGreen, $textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .=     '<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_lightBrown, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap)).'</tr>';

        for ($i=29; $i <= 43; $i++) {
            switch ($i) {
                case '43':
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array($textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap))
                              .'</tr>';
                    break;

                default:
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_lightBrown, $textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .=     '<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_pink, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap)).'</tr>';

        for ($i=44; $i <= 46; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_pink, $textAlign_center, $whiteSpace_nowrap))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.103 [老健] 患者月報(日報・日毎)のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string    HTML table
     */
    function getMonthlyPerDate($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = '';
        $fontTag = $this->getFontTag();

        $displayYear  = $this->getArrValueByKey('year',  $optionArr);
        $displayMonth = $this->getArrValueByKey('month', $optionArr);

        # attr
        $rowspan_2         = array('name' => 'rowspan',          'value' =>  2);
        $rowspan_3         = array('name' => 'rowspan',          'value' =>  3);
        $rowspan_4         = array('name' => 'rowspan',          'value' =>  4);
        $colspan_2         = array('name' => 'colspan',          'value' =>  2);
        $colspan_3         = array('name' => 'colspan',          'value' =>  3);
        $colspan_4         = array('name' => 'colspan',          'value' =>  4);
        $colspan_7         = array('name' => 'colspan',          'value' =>  7);
        $colspan_8         = array('name' => 'colspan',          'value' =>  8);
        $colspan_12        = array('name' => 'colspan',          'value' => 12);
        $colspan_13        = array('name' => 'colspan',          'value' => 13);
        $colspan_14        = array('name' => 'colspan',          'value' => 14);
        $colspan_15        = array('name' => 'colspan',          'value' => 15);
        $colspan_16        = array('name' => 'colspan',          'value' => 16);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        $rtnStr .= '<table class="list" width="100%" >'
                     .'<tr>'
                        .$this->convertCellData(
                            array(0 => array('data' => '&nbsp;',                       'attr' => array($rowspan_3))
                                , 1 => array('data' => '通所',                         'attr' => array($colspan_12))
                                , 2 => array('data' => '入所',                         'attr' => array($colspan_15))
                                , 3 => array('data' => '訪問リハビリ<br />(単位)',       'attr' => array($colspan_3, $rowspan_2))
                                , 4 => array('data' => '判定件数<br />(本入所のみ)',   'attr' => array($colspan_2, $rowspan_2))
                                , 5 => array('data' => '営業件数<br />(訪問件数のみ)', 'attr' => array($rowspan_3)),
                            ),
                            $fontTag,
                            array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                        )
                     .'</tr>'
                     .'<tr>'
                        .$this->convertCellData(
                            array(0 => array('data' => '定員数',                                 'attr' => array($rowspan_2))
                                , 1 => array('data' => '通所リハビリ',                           'attr' => array($colspan_8))
                                , 2 => array('data' => '予防通所リハビリ',                       'attr' => array($rowspan_2))
                                , 3 => array('data' => '合計',                                   'attr' => array($rowspan_2))
                                , 4 => array('data' => '稼働率',                                 'attr' => array($rowspan_2))
                                , 5 => array('data' => '定員数',                                 'attr' => array($rowspan_2))
                                , 6 => array('data' => '一般',                                   'attr' => array($rowspan_2))
                                , 7 => array('data' => '一般<br />ショートステイ',               'attr' => array($rowspan_2))
                                , 8 => array('data' => '一般<br />予防ショート',                 'attr' => array($rowspan_2))
                                , 9 => array('data' => '認知',                                   'attr' => array($rowspan_2))
                               , 10 => array('data' => '認知<br />ショートステイ',               'attr' => array($rowspan_2))
                               , 11 => array('data' => '認知<br />予防ショート',                 'attr' => array($rowspan_2))
                               , 12 => array('data' => '小計',                                   'attr' => array($rowspan_2))
                               , 13 => array('data' => '入所数',                                 'attr' => array($rowspan_2))
                               , 14 => array('data' => '退所数',                                 'attr' => array($rowspan_2))
                               , 15 => array('data' => 'ショート<br />予防ショート<br />入所数', 'attr' => array($rowspan_2))
                               , 16 => array('data' => 'ショート<br />予防ショート<br />退所数', 'attr' => array($rowspan_2))
                               , 17 => array('data' => '空床数',                                 'attr' => array($rowspan_2))
                               , 18 => array('data' => '稼働率',                                 'attr' => array($rowspan_2, $colspan_2))
                            ),
                            $fontTag,
                            array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                        )
                     .'</tr>'
                     .'<tr>'
                        .$this->convertCellData(
                            array(0 => array('data' => '1時間以上<br />2時間未満')
                                , 1 => array('data' => '2時間以上<br />3時間未満')
                                , 2 => array('data' => '3時間以上<br />4時間未満')
                                , 3 => array('data' => '4時間以上<br />6時間未満')
                                , 4 => array('data' => '6時間以上<br />8時間未満')
                                , 5 => array('data' => '8時間以上<br />9時間未満')
                                , 6 => array('data' => '9時間以上<br />10時間未満')
                                , 7 => array('data' => '小計')
                                , 8 => array('data' => '訪問リハビリ')
                                , 9 => array('data' => '介護予防<br />訪問リハビリ')
                               , 10 => array('data' => '合計')
                               , 11 => array('data' => '可')
                               , 12 => array('data' => '不可')
                            ),
                            $fontTag,
                            array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                        )
                     .'</tr>';

        $totalData = array();
        if (is_array($plantArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $facilityId   = $this->getArrValueByKey('facility_id',   $plantVal);
                $facilityName = $this->getArrValueByKey('facility_name', $plantVal);

                $lastDay = $this->getLastDay($displayYear, $displayMonth);
                for ($d=1; $d <= $lastDay; $d++) {
                    $displayDate = sprintf('%02d月%02d日(%s)', $displayMonth, $d, $this->getDayOfTheWeekForDate($displayYear, $displayMonth, $d));
                    $status      = $this->checkCellDataStatus('patient_daily_facilities', array('year' => $displayYear, 'month' => $displayMonth, 'day' => $d));
                    $mainData    = array();
                    $dailyLimit  = null;
                    $dispNum_7   = null;
                    $dispNum_14  = null;
                    $dispNum_21  = null;
                    $dispNum_28  = null;
                    $dispNum_35  = null;
                    $dispNum_42  = null;
                    $dispNum_49  = null;
                    $dispNum_53  = null;
                    $enterLimit  = null;
                    $dispNum_60  = null;
                    $dispNum_74  = null;
                    $dispNum_85  = null;
                    $dispNum_67  = null;
                    $dispNum_81  = null;
                    $dispNum_89  = null;
                    $dispNum_101 = null;
                    $dispNum_102 = null;
                    $dispNum_104 = null;
                    $dispNum_105 = null;
                    $dispNum_96  = null;
                    $dispNum_100 = null;
                    $dispNum_106 = null;
                    $dispNum_107 = null;
                    $dispNum_108 = null;

                    if (is_array($dataArr)) {
                        foreach ($dataArr as $dataVal) {
                            if ($d == $this->getArrValueByKey('day', $dataVal)) {
                                $status       = $this->checkCellDataStatus('patient_daily_facilities', $dataVal);
                                $dailyLimit   = $this->getArrValueByKey('daily_limit', $dataVal);
                                $dispNum_7    = $this->getArrValueByKey('dispnum_7',   $dataVal);
                                $dispNum_14   = $this->getArrValueByKey('dispnum_14',  $dataVal);
                                $dispNum_21   = $this->getArrValueByKey('dispnum_21',  $dataVal);
                                $dispNum_28   = $this->getArrValueByKey('dispnum_28',  $dataVal);
                                $dispNum_35   = $this->getArrValueByKey('dispnum_35',  $dataVal);
                                $dispNum_42   = $this->getArrValueByKey('dispnum_42',  $dataVal);
                                $dispNum_49   = $this->getArrValueByKey('dispnum_49',  $dataVal);
                                $dispNum_53   = $this->getArrValueByKey('dispnum_53',  $dataVal);
                                $enterLimit   = $this->getArrValueByKey('enter_limit', $dataVal);
                                $dispNum_60   = $this->getArrValueByKey('dispnum_60',  $dataVal);
                                $dispNum_74   = $this->getArrValueByKey('dispnum_74',  $dataVal);
                                $dispNum_85   = $this->getArrValueByKey('dispnum_85',  $dataVal);
                                $dispNum_67   = $this->getArrValueByKey('dispnum_67',  $dataVal);
                                $dispNum_81   = $this->getArrValueByKey('dispnum_81',  $dataVal);
                                $dispNum_89   = $this->getArrValueByKey('dispnum_89',  $dataVal);
                                $dispNum_101  = $this->getArrValueByKey('dispnum_101', $dataVal);
                                $dispNum_102  = $this->getArrValueByKey('dispnum_102', $dataVal);
                                $dispNum_104  = $this->getArrValueByKey('dispnum_104', $dataVal);
                                $dispNum_105  = $this->getArrValueByKey('dispnum_105', $dataVal);
                                $dispNum_96   = $this->getArrValueByKey('dispnum_96',  $dataVal);
                                $dispNum_100  = $this->getArrValueByKey('dispnum_100', $dataVal);
                                $dispNum_106  = $this->getArrValueByKey('dispnum_106', $dataVal);
                                $dispNum_107  = $this->getArrValueByKey('dispnum_107', $dataVal);
                                $dispNum_108  = $this->getArrValueByKey('dispnum_108', $dataVal);
                            }
                        }
                    }

                    # 通所
                    $mainData[0]  = array('data' => $displayDate, 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));
                    $mainData[1]  = array('status' => $status, 'data' => $dailyLimit);
                    $mainData[2]  = array('status' => $status, 'data' => $dispNum_7);
                    $mainData[3]  = array('status' => $status, 'data' => $dispNum_14);
                    $mainData[4]  = array('status' => $status, 'data' => $dispNum_21);
                    $mainData[5]  = array('status' => $status, 'data' => $dispNum_28);
                    $mainData[6]  = array('status' => $status, 'data' => $dispNum_35);
                    $mainData[7]  = array('status' => $status, 'data' => $dispNum_42);
                    $mainData[8]  = array('status' => $status, 'data' => $dispNum_49);
                    $mainData[9]  = array('status' => $status, 'data' => $mainData[2]['data'] + $mainData[3]['data'] + $mainData[4]['data'] + $mainData[5]['data'] + $mainData[6]['data'] + $mainData[7]['data'] + $mainData[8]['data']);
                    $mainData[10] = array('status' => $status, 'data' => $dispNum_53);
                    $mainData[11] = array('status' => $status, 'data' => $mainData[9]['data'] + $mainData[10]['data']);
                    $mainData[12] = array('status' => $status, 'data' => is_null($mainData[1]['data'])? null : $this->getUtilizationRates($mainData[11]['data'], $mainData[1]['data'], 1), 'disp_type' => 'float_percent');

                    # 入所 定員数
                    $mainData[13] = array('status' => $status, 'data' => $enterLimit);
                    $mainData[14] = array('status' => $status, 'data' => $dispNum_60);
                    $mainData[15] = array('status' => $status, 'data' => $dispNum_74);
                    $mainData[16] = array('status' => $status, 'data' => $dispNum_85);
                    $mainData[17] = array('status' => $status, 'data' => $dispNum_67);
                    $mainData[18] = array('status' => $status, 'data' => $dispNum_81);
                    $mainData[19] = array('status' => $status, 'data' => $dispNum_89);

                    # 入所 小計
                    $mainData[20] = array('status' => $status, 'data' => $mainData[14]['data'] + $mainData[15]['data'] + $mainData[16]['data'] + $mainData[17]['data'] + $mainData[18]['data'] + $mainData[19]['data']);
                    $mainData[21] = array('status' => $status, 'data' => $dispNum_101);
                    $mainData[22] = array('status' => $status, 'data' => $dispNum_102);
                    $mainData[23] = array('status' => $status, 'data' => $dispNum_104);
                    $mainData[24] = array('status' => $status, 'data' => $dispNum_105);

                    # 入所 空床数
                    $mainData[25] = array('status' => $status, 'data' => $this->getNumberOfEmptyFloors($mainData[20]['data'], $mainData[13]['data']));
                    $mainData[27] = array('status' => $status, 'data' => is_null($mainData[20]['data'])? null : $this->getUtilizationRates($mainData[20]['data'], $mainData[13]['data'], 1), 'disp_type' => 'float_percent');
                    $mainData[26] = array('status' => $status, 'data' => $this->getUtilizationRatesTrigona($mainData[27]['data'], 95));

                    # 訪問リハビリ
                    $mainData[28] = array('status' => $status, 'data' => $dispNum_96);
                    $mainData[29] = array('status' => $status, 'data' => $dispNum_100);
                    $mainData[30] = array('status' => $status, 'data' => $dispNum_96 + $dispNum_100);

                    # 判定件数
                    $mainData[31] = array('status' => $status, 'data' => $dispNum_106);
                    $mainData[32] = array('status' => $status, 'data' => $dispNum_107);

                    # 営業件数
                    $mainData[33] = array('status' => $status, 'data' => $dispNum_108);

                    for ($i=0; $i <= 33; $i++) {
                        $totalData[$i]['status'] = $this->checkCellDataStatus('comp', array($totalData[$i]['status'], $status));
                        switch ($i) {
                            case '0':
                                $totalData[$i] = array('data' => '合計', 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));
                                break;

                            case '9':
                                $totalData[$i]['data'] = $totalData[2]['data'] + $totalData[3]['data'] + $totalData[4]['data'] + $totalData[5]['data'] + $totalData[6]['data'] + $totalData[7]['data'] + $totalData[8]['data'];
                                break;

                            case '11':
                                $totalData[$i]['data'] = $totalData[9]['data'] + $totalData[10]['data'];
                                break;

                            case '26':
                                break;

                            case '12': case '27':
                                $leftKey  = ($i==12)? 11 : 20;
                                $rightKey = ($i==12)?  1 : 13;
                                $totalData[$i]['data'] = $this->getUtilizationRates($totalData[$leftKey]['data'], $totalData[$rightKey]['data'], 1);
                                $totalData[$i]['disp_type'] = 'float_percent';
                                if ($i == 27) { $totalData[$i-1]['data'] = $this->getUtilizationRatesTrigona($totalData[$i]['data'], 95); }
                                break;

                            case '25':
                                $totalData[$i]['data'] = $this->getNumberOfEmptyFloors($totalData[20]['data'], $totalData[13]['data']);
                                break;

                            default:
                                $totalData[$i]['data'] = $totalData[$i]['data'] + $mainData[$i]['data'];
                                break;
                        }
                    }
                    $rtnStr .= '<tr>'.$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                }
            }
            $rtnStr .= '<tr>'.$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.124 [老健] 老健月間加算項目・算定実績のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string
     */
    function getCalculationResults($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr     = '';
        $fontTag    = $this->getFontTag();
        $facilityId = $this->getArrValueByKey('facility_id', $optionArr);
        $targetYear = $this->getArrValueByKey('year',        $optionArr);

        # 特殊文字
        $roman1 = $this->sc('roman_1');
        $roman2 = $this->sc('roman_2');
        $roman3 = $this->sc('roman_3');

        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();

        # attr
        $calspan_16        = array('name' => 'colspan',          'value' => 16);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_white     = array('name' => 'background-color', 'value' => '#ffffff');
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');

        # 入所のヘッダー
        $enterHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数'))
           , 1 => array(0 => array('data' => '1.夜間職員配置加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '24単位'))
           , 2 => array(0 => array('data' => '2.短期集中ﾘﾊﾋﾞﾘﾃｰｼｮﾝ実施加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位'))
           , 3 => array(0 => array('data' => '3.認知症短期集中ﾘﾊﾋﾞﾘﾃｰｼｮﾝ実施加算(週3日を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '240単位'))
           , 4 => array(0 => array('data' => '4.認知症ケア加算(ユニットは算定不可)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '76単位'))
           , 5 => array(0 => array('data' => '5.若年性認知症入所者受入加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '120単位'))
           , 6 => array(0 => array('data' => '6.ターミナル加算(1)死亡日以前15日以上30日以下', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位'))
           , 7 => array(0 => array('data' => '7.ターミナル加算(1)死亡日以前15日以上31日以下', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '315単位'))
           , 8 => array(0 => array('data' => '8.療養体制維持特別加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '27単位'))
           , 9 => array(0 => array('data' => '9.初期加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '30単位'))
          , 10 => array(0 => array('data' => '10.退所前後訪問指導加算(入所中1回又は2回、退所後1回を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '460単位'))
          , 11 => array(0 => array('data' => '11.退所時指導加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '400単位'))
          , 12 => array(0 => array('data' => '12.退所時情報提供加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位'))
          , 13 => array(0 => array('data' => '13.退所前連携加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位'))
          , 14 => array(0 => array('data' => '14.老人訪問看護指示加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '300単位'))
          , 15 => array(0 => array('data' => '15.栄養マネジメント加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '14単位'))
          , 16 => array(0 => array('data' => '16.経口移行加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '28単位'))
          , 17 => array(0 => array('data' => '17.経口維持加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '28単位'))
          , 18 => array(0 => array('data' => '18.経口維持加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '5単位'))
          , 19 => array(0 => array('data' => '19.口腔機能維持管理加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '30単位'))
          , 20 => array(0 => array('data' => '20.療養食加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '23単位'))
          , 21 => array(0 => array('data' => '21.在宅復帰支援機能加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '15単位'))
          , 22 => array(0 => array('data' => '22.在宅復帰支援機能加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '5単位'))
          , 23 => array(0 => array('data' => '23.緊急時施設療養費(1)緊急時治療管理(1月に1回3日を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位'))
          , 24 => array(0 => array('data' => '24.認知症専門ケア加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '3単位'))
          , 25 => array(0 => array('data' => '25.認知症専門ケア加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '4単位'))
          , 26 => array(0 => array('data' => '26.認知症情報提供加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '350単位'))
          , 27 => array(0 => array('data' => '27.サービス提供体制強化加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '12単位'))
          , 28 => array(0 => array('data' => '28.サービス提供体制強化加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位'))
          , 29 => array(0 => array('data' => '29.サービス提供体制強化加算('.$roman3.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位'))
        );

        # ショートのヘッダー
        $shortHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数'))
           , 1 => array(0 => array('data' => '1.夜勤職員配置加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '24単位'))
           , 2 => array(0 => array('data' => '2.リハビリ機能強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '30単位'))
           , 3 => array(0 => array('data' => '3.個別リハビリテーション実施加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位'))
           , 4 => array(0 => array('data' => '4.認知症ケア加算(ユニットは算定不可)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '76単位'))
           , 5 => array(0 => array('data' => '5.認知症行動・心理症状緊急対応加算(7日間を限度)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位'))
           , 6 => array(0 => array('data' => '6.若年性認知症入所者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '120単位'))
           , 7 => array(0 => array('data' => '7.送迎加算(片道につき)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '184単位'))
           , 8 => array(0 => array('data' => '8.療養体制維持特別加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '27単位'))
           , 9 => array(0 => array('data' => '9.療養食加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '23単位'))
          , 10 => array(0 => array('data' => '10.緊急短期入所ネットワーク加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '50単位'))
          , 11 => array(0 => array('data' => '11.緊急時施設療養費(１)緊急時治療管理(1月に1回3日を限度)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位'))
          , 12 => array(0 => array('data' => '12.サービス提供体制強化加算('.$roman1.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '12単位'))
          , 13 => array(0 => array('data' => '13.サービス提供体制強化加算('.$roman2.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位'))
          , 14 => array(0 => array('data' => '14.サービス提供体制強化加算('.$roman3.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位'))
        );

        # 通所のヘッダー
        $communteHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数'))
           , 1 => array(0 => array('data' => '1.連続介護加算(ｱ･8h-9h)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '50単位'))
           , 2 => array(0 => array('data' => '2.連続介護加算(ｲ･9ｈ-10h)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '100単位'))
           , 3 => array(0 => array('data' => '3.入浴介助加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '50単位'))
           , 4 => array(0 => array('data' => '4.リハ計画作成加算（月1回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '550単位'))
           , 5 => array(0 => array('data' => '5.リハビリテーションマネジメント加算（月1回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '230単位'))
           , 6 => array(0 => array('data' => '6.短期集中リハ実施加算（１月以内）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '280単位'))
           , 7 => array(0 => array('data' => '7.短期集中リハ実施加算（１月超３月以内）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '140単位'))
           , 8 => array(0 => array('data' => '8.個別リハ実施加算（３月超）（月13回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '80単位'))
           , 9 => array(0 => array('data' => '9.認知症短期集中リハ実施加算（週2日）を限度', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位'))
          , 10 => array(0 => array('data' => '10.若年性認知症利用者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '60単位'))
          , 11 => array(0 => array('data' => '11.栄養改善加算（月2回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '150単位'))
          , 12 => array(0 => array('data' => '12.口腔機能向上加算（月2回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '150単位'))
          , 13 => array(0 => array('data' => '13.サービス提供体制強化加算（'.$roman1.'）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '12単位'))
          , 14 => array(0 => array('data' => '14.サービス提供体制強化加算（'.$roman2.'）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位'))
        );

        # 介護予防通所のヘッダー
        $communtePreHeaderArr = array(
            0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数'))
          , 1 => array(0 => array('data' => '1．若年性認知症利用者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '240単位'))
          , 2 => array(0 => array('data' => '2.運動機能加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '225単位'))
          , 3 => array(0 => array('data' => '3.栄養改善加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '150単位'))
          , 4 => array(0 => array('data' => '4.口腔機能向上加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '150単位'))
          , 5 => array(0 => array('data' => '5.事業所評価加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '100単位'))
          , 6 => array(0 => array('data' => '6.サービス提供体制強化加算（1）要支援１', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '48単位'))
          , 7 => array(0 => array('data' => '7.サービス提供体制強化加算（1）要支援２', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '96単位'))
          , 8 => array(0 => array('data' => '8.サービス提供体制強化加算（2）要支援１', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '24単位'))
          , 9 => array(0 => array('data' => '9.サービス提供体制強化加算（2）要支援２', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '48単位'))
        );

        # 訪問リハのヘッダー
        $visitHeaderArr = array(
            0 => array(0 => array('data' => '加算項目', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数'))
          , 1 => array(0 => array('data' => '1.短期集中リハビリテーション実施加算(1月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '340単位'))
          , 2 => array(0 => array('data' => '2.短期集中リハビリテーション実施加算(1月超3月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位'))
          , 3 => array(0 => array('data' => '3.サービス提供体制強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位'))
        );

        # 介護予防訪問リハのヘッダー
        $visitPreHeaderArr = array(
            0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数'))
          , 1 => array(0 => array('data' => '1.短期集中リハビリテーション実施加算(3月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位'))
          , 2 => array(0 => array('data' => '2.サービス提供体制強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位'))
        );

        $plantCnt = count($plantArr);

        $enterMainArr        = array();
        $shortMainArr        = array();
        $communteMainArr     = array();
        $communtePreMainArr  = array();
        $visitMainArr        = array();
        $visitPreMainArr     = array();
        $enterTotalArr       = array();
        $shortTotalArr       = array();
        $communteTotalArr    = array();
        $communtePreTotalArr = array();
        $visitTotalArr       = array();
        $visitPreTotalArr    = array();

        $totalStatus = 3;
        $startMonth  = $this->getFiscalYearStartMonth();

        for ($m=0; $m<=11; $m++) {
            $currentYear = $targetYear;
            $month       = $startMonth + $m;
            $currentYear = ($month >= 13)? $currentYear + 1 : $currentYear;
            $month       = ($month >= 13)? $month-12 : $month;
            $monthName   = $month.'月';

            # 施設名
            $enterMainArr[0][]       = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $shortMainArr[0][]       = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $communteMainArr[0][]    = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $communtePreMainArr[0][] = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $visitMainArr[0][]       = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $visitPreMainArr[0][]    = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));

            $status = $this->checkCellDataStatus('calculation_results', array('year' => $currentYear, 'month' => $month));

            # 初期化
            for ($i = 109; $i <= 179; $i++) { $dispNum[$i] = 0; }
            if (is_array($dataArr) && count($dataArr)!=0) {
                foreach ($dataArr as $dataVal) {
                    if ($facilityId==$this->getArrValueByKey('jnl_facility_id', $dataVal) && $currentYear == $this->getArrValueByKey('year', $dataVal) && sprintf('%02d', $month) == $this->getArrValueByKey('month', $dataVal)) {
                        for ($i = 109; $i <= 179; $i++) { $dispNum[$i] = $this->getArrValueByKey('dispnum_'.$i, $dataVal); }
                        $status = $this->checkCellDataStatus('calculation_results', $dataVal);
                    }
                }
            }

            for ($i = 1; $i <= 29; $i++) {
                $totalStatus = $this->checkCellDataStatus('comp', array($totalStatus, $status));
                $enterMainArr[$i][]   = array('data' => $dispNum[108+$i], 'status' => $status);
                $enterTotalArr[$i][0] = array('data' => $enterTotalArr[$i][0]['data'] + $dispNum[108+$i], 'status' => $totalStatus);

                if ($i <= 14) {
                    $shortMainArr[$i][]      = array('data' => $dispNum[137+$i], 'status' => $status);
                    $shortTotalArr[$i][0]    = array('data' => $shortTotalArr[$i][0]['data'] + $dispNum[137+$i], 'status' => $totalStatus);
                    $communteMainArr[$i][]   = array('data' => $dispNum[151+$i], 'status' => $status);
                    $communteTotalArr[$i][0] = array('data' => $communteTotalArr[$i][0]['data'] + $dispNum[151+$i], 'status' => $totalStatus);

                    if ($i <= 9) {
                        $communtePreMainArr[$i][]   = array('data' => $dispNum[165+$i], 'status' => $status);
                        $communtePreTotalArr[$i][0] = array('data' => $communtePreTotalArr[$i][0]['data'] + $dispNum[165+$i], 'status' => $totalStatus);

                        if ($i <= 3) {
                            $visitMainArr[$i][]   = array('data' => $dispNum[174+$i], 'status' => $status);
                            $visitTotalArr[$i][0] = array('data' => $visitTotalArr[$i][0]['data'] + $dispNum[174+$i], 'status' => $totalStatus);

                            if ($i <= 2) {
                                $visitPreMainArr[$i][]   = array('data' => $dispNum[177+$i], 'status' => $status);
                                $visitPreTotalArr[$i][0] = array('data' => $visitPreTotalArr[$i][0]['data'] + $dispNum[177+$i], 'status' => $totalStatus);
                            }
                        }
                    }
                }
            }
        }

        $enterMainArr[0][12]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $shortMainArr[0][12]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $communteMainArr[0][12]    = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $communtePreMainArr[0][12] = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $visitMainArr[0][12]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $visitPreMainArr[0][12]    = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));

        # 出力データ表示
        $rtnStr .= '<table class="list" width="100%" >'
                      .'<tr>'.$this->convertCellData(array(0 => array('data'=> '入所', 'attr' => array($calspan_16), 'style' => array($textAlign_left))), $fontTag, array()).'</tr>';
        for ($i = 0; $i <= 29; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($enterHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($enterMainArr[$i],   $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($enterTotalArr[$i],  $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .=    '<tr>'.$this->convertCellData(array(0 => array('data'=> 'ショート(介護・予防を含む)', 'attr' => array($calspan_16), 'style' => array($textAlign_left))), $fontTag, array()).'</tr>'
                      .'<tr>';
        for ($i = 0; $i <= 14; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($shortHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($shortMainArr[$i],   $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($shortTotalArr[$i],  $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .=    '<tr>'.$this->convertCellData(array(0 => array('data'=> '通所リハビリテーション', 'attr' => array($calspan_16), 'style' => array($textAlign_left))), $fontTag, array()).'</tr>';
        for ($i = 0; $i <= 14; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($communteHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($communteMainArr[$i],   $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($communteTotalArr[$i],  $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .=    '<tr>'.$this->convertCellData(array(0 => array('data'=> '介護予防通所リハビリテーション', 'attr' => array($calspan_16), 'style' => array($textAlign_left))), $fontTag, array()).'</tr>'
                      .'<tr>';
        for ($i = 0; $i <= 9; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($communtePreHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($communtePreMainArr[$i],   $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($communtePreTotalArr[$i],  $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .=    '<tr>'.$this->convertCellData(array(0 => array('data'=> '訪問リハビリテーション', 'attr' => array($calspan_16), 'style' => array($textAlign_left))), $fontTag, array()).'</tr>';
        for ($i = 0; $i <= 3; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($visitHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($visitMainArr[$i],   $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($visitTotalArr[$i],  $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .=    '<tr>'.$this->convertCellData(array(0 => array('data'=> '介護予防訪問リハビリテーション', 'attr' => array($calspan_16), 'style' => array($textAlign_left))), $fontTag, array()).'</tr>';
        for ($i = 0; $i <= 2; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($visitPreHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($visitPreMainArr[$i],   $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($visitPreTotalArr[$i],  $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .=    '</table>';
        return $rtnStr;
    }

    /**
     * No.125 [老健] 療養費明細書のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string
     */
    function getMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = '';

        $fontTag      = $this->getFontTag();
        $facilityName = $plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_13 = array('name' => 'rowspan', 'value' => 13);
        $rowspan_24 = array('name' => 'rowspan', 'value' => 24);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_6  = array('name' => 'colspan', 'value' =>  6);
        $colspan_7  = array('name' => 'colspan', 'value' =>  7);
        $colspan_10 = array('name' => 'colspan', 'value' => 10);

        # style
        $bgColor_gray        = array('name' => 'background-color', 'value' => '#c0c0c0');
        $bgColor_white       = array('name' => 'background-color', 'value' => '#ffffff');
        $bgColor_lightBlue   = array('name' => 'background-color', 'value' => '#b7dbff');
        $bgColor_lightRed    = array('name' => 'background-color', 'value' => '#ffb7b7');
        $bgColor_lightGreen  = array('name' => 'background-color', 'value' => '#a4ffa4');
        $bgColor_lightYellow = array('name' => 'background-color', 'value' => '#ffe8b7');
        $bgColor_lightOrange = array('name' => 'background-color', 'value' => '#ffc6a4');
        $bgColor_purple      = array('name' => 'background-color', 'value' => '#dbb7ff');
        $bgColor_blue        = array('name' => 'background-color', 'value' => '#75a9ff');
        $textAlign_right     = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left      = array('name' => 'text-align',       'value' => 'left');
        $textAlign_center    = array('name' => 'text-align',       'value' => 'center');
        $whiteSpace_nowrap   = array('name' => 'white-space',      'value' => 'nowrap');

        # 以下、表示データ
        $rtnStr .= '<table width="100%" border="0" ><tr>'
                       .$this->convertCellData(
                           array(0 => array('data'  => '施設名：'.$facilityName)
                               , 1 => array('data'  => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array('textAlign' => $textAlign_right))
                           )
                         , $fontTag
                         , array()
                       )
                       .'</tr>'
                   .'</table>';

        ### header ###
        $firstTypeHeaderArr = array(
            3 => array('data' => '保険種別'),    4 => array('data' => '公費法別'),    5 => array('data' => '件数(人数)')
          , 6 => array('data' => '日数'),        7 => array('data' => '単位'),        8 => array('data' => '保険請求額')
          , 9 => array('data' => '公費請求額'), 10 => array('data' => '利用者負担'), 11 => array('data' => '費用合計額')
        );

        $firstHeaderArr = array(
             0 => array(0 => array('data' => '保険請求分', 'attr' => array($rowspan_2, $colspan_7), 'style' => array('textAlign' => $textAlign_left))
                      , 1 => array('data' => '総請求額', 'attr' => array($colspan_2), 'style' => array($bgColor_lightYellow))
             )
           , 1 => array(0 => array('data' => '保険分請求総額', 'attr' => array($colspan_2), 'style' => array($bgColor_lightYellow)))
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 26 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 27 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 28 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 29 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 32 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 33 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 34 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 35 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 38 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 39 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 40 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 41 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 44 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 45 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 46 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 47 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 50 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 51 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 52 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 53 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 56 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 57 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 58 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 59 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 62 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 63 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 64 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 65 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 68 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 69 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 70 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 71 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 74 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 75 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 76 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 77 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 80 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 81 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 82 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 83 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 86 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 87 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 88 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 89 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 92 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 93 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 94 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 95 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $secondLeftHeaderArr = array(
             0 => array(0 => array('data' => '特定入所者介護サービス費等', 'attr' => array($rowspan_2, $colspan_10), 'style' => array('textAlign' => $textAlign_left)))
           , 2 => array(0 => array('data' => '保険請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
          , 11 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 12 => array(0 => array('data' => '食費'))
          , 13 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 14 => array(0 => array('data' => '食費'))
          , 15 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 16 => array(0 => array('data' => '食費'))
          , 17 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 18 => array(0 => array('data' => '食費'))
          , 19 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 20 => array(0 => array('data' => '食費'))
          , 21 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 22 => array(0 => array('data' => '食費'))
          , 23 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 24 => array(0 => array('data' => '食費'))
          , 25 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 26 => array(0 => array('data' => '食費'))
        );
        $secondRightHeaderArr = array(
             1 => array(1 => array('data' => '特定入所者分請求額', 'attr' => array($colspan_2), 'style' => array($bgColor_lightYellow)))
           , 2 => array(0 => array('data' => '公費請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
          , 11 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 12 => array(0 => array('data' => '食費'))
          , 13 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 14 => array(0 => array('data' => '食費'))
          , 15 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 16 => array(0 => array('data' => '食費'))
          , 17 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 18 => array(0 => array('data' => '食費'))
          , 19 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 20 => array(0 => array('data' => '食費'))
          , 21 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 22 => array(0 => array('data' => '食費'))
          , 23 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 24 => array(0 => array('data' => '食費'))
          , 25 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 26 => array(0 => array('data' => '食費'))
        );

        $thirdHeaderArr = array(
            0 => array(0 => array('data' => '居宅介護支援(ケアプラン作成料)', 'attr' => array($colspan_4), 'style' => array($textAlign_left)))
          , 1 => array(0 => array('data' => '&nbsp;'), 1 => array('data' => '件数'), 2 => array('data' => '単位'), 3 => array('data' => '金額'))
          , 2 => array(0 => array('data' => '合計'))
          , 3 => array(0 => array('data' => '当月'))
          , 4 => array(0 => array('data' => '月遅れ'))
          , 5 => array(0 => array('data' => '返戻再請求'))
        );

        $fourthHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)), 1 => array('data' => '低所得者件数'), 2 => array('data' => '低所得者日数')
                     , 3 => array('data' => '入所者全日数'), 4 => array('data' => '比率'))
          , 1 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_3)), 1 => array('data' => '合計'))
          , 2 => array(0 => array('data' => '入所'))
          , 3 => array(0 => array('data' => 'ショート'))
        );

        $fifthHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数'), 3 => array('data' => '金額'))
          , 1 => array(0 => array('data' => '通所'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
          , 2 => array(0 => array('data' => 'ショート', 'attr' => array($rowspan_13)), 1 => array('data' => '4段階', 'attr' => array($rowspan_2)), 2 => array('data' => '滞在費(個室室)'))
          , 3 => array(0 => array('data' => '滞在費(多床室)'))
          , 4 => array(0 => array('data' => '2･3段階', 'attr' => array($rowspan_2)), 1 => array('data' => '滞在費(低所得者負担金あり 多床室)'))
          , 5 => array(0 => array('data' => '滞在費(低所得 者負担金あり 個室)'))
          , 6 => array(0 => array('data' => '1段階'), 1 => array('data' => '滞在費(低所得者負担金なし)'))
          , 7 => array(0 => array('data' => '4段階', 'attr' => array($rowspan_4)), 1 => array('data' => '食費(1日)'))
          , 8 => array(0 => array('data' => '食費(朝)'))
          , 9 => array(0 => array('data' => '食費(昼)'))
         , 10 => array(0 => array('data' => '食費(夕)'))
         , 11 => array(0 => array('data' => '3段階'), 1 => array('data' => '食費'))
         , 12 => array(0 => array('data' => '2段階'), 1 => array('data' => '食費'))
         , 13 => array(0 => array('data' => '1段階'), 1 => array('data' => '食費'))
         , 14 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
         , 15 => array(0 => array('data' => '入所', 'attr' => array($rowspan_13)), 1 => array('data' => '4段階', 'attr' => array($rowspan_2)), 2 => array('data' => '滞在費(個室室)'))
         , 16 => array(0 => array('data' => '滞在費(多床室)'))
         , 17 => array(0 => array('data' => '2･3段階', 'attr' => array($rowspan_2)), 1 => array('data' => '滞在費(低所得者負担金あり 多床室)'))
         , 18 => array(0 => array('data' => '滞在費(低所得者負担金あり 個室)'))
         , 19 => array(0 => array('data' => '1段階'), 1 => array('data' => '滞在費(低所得者負担金なし)'))
         , 20 => array(0 => array('data' => '4段階', 'attr' => array($rowspan_4)), 1 => array('data' => '食費(1日)'))
         , 21 => array(0 => array('data' => '食費(朝)'))
         , 22 => array(0 => array('data' => '食費(昼)'))
         , 23 => array(0 => array('data' => '食費(夕)'))
         , 24 => array(0 => array('data' => '3段階'), 1 => array('data' => '食費'))
         , 25 => array(0 => array('data' => '2段階'), 1 => array('data' => '食費'))
         , 26 => array(0 => array('data' => '1段階'), 1 => array('data' => '食費'))
         , 27 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
         , 28 => array(0 => array('data' => '特別な室料', 'attr' => array($colspan_3)))
         , 29 => array(0 => array('data' => 'その他の自費', 'attr' => array($colspan_3)))
         , 30 => array(0 => array('data' => '総合計', 'attr' => array($colspan_3)))
        );

        $sixthHeaderArr = array(
             0 => array(0 => array('data' => '訪問リハビリ', 'attr' => array($rowspan_2, $colspan_6), 'style' => array('textAlign' => $textAlign_left)))
           , 1 => array(0 => array('data' => '保険分請求総額', 'attr' => array($colspan_2), 'style' => array($bgColor_lightYellow)))
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $seventhHeaderArr = array(0 => array('data' => '請求額'));

        ### data ###
        $dispData = array();
        for ($i=1; $i<=570; $i++) {
            $dispData['orange_'.$i] = null;
            if ($i<=359) {
                $dispData['red_'.$i] = null;
                if ($i<=80) { $dispData['green_'.$i] = null; }
            }
        }

        # ここでデータを配列に･･･
        if (is_array($dataArr) && count($dataArr)>0 ) { foreach ($dataArr[0] as $key => $val) { $dispData[$key] = $val; } }

        $firstOrangeNumArr = array(
              3 =>   1,   9 =>  32,  15 =>  63,  27 =>  94,  33 => 125,  39 => 156,  51 => 187,  57 => 218,  63 => 249
        );
        $firstRedNumArr    = array(
              0 =>   1,   1 =>   2,  21 =>   3,  45 =>  34,  69 =>  65,  75 =>  96,  81 => 127,  87 => 158,  93 => 189
        );
        $firstGreenNumArr  = array(
              3 =>   1,   9 =>   5,  15 =>   9,  21 =>  13,  27 =>  17,  33 =>  21,  39 =>  25,  45 =>  29,  51 =>  33
          ,  57 =>  37,  63 =>  41,  69 =>  45,  75 =>  49,  81 =>  53,  87 =>  57,  93 =>  61
        );

        $firstMainDataArr = array();
        for ($i = 0; $i <= 97; $i++) {
            $oNum = null;
            $rNum = null;
            $gNum = null;

            switch ($i) {
                case '0': case '1':
                    $rNum = $firstRedNumArr[$i];
                    $firstMainDataArr[$i] = array(0 => array('data' => $dispData['red_'.$rNum], 'attr' => array($colspan_2)));
                    break;

                case '3': case '9': case '15': case '27': case '33': case '39'; case '51': case '57': case '63':
                    $oNum = $firstOrangeNumArr[$i];
                    $gNum = $firstGreenNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $firstMainDataArr[$i][$l]['data'] = $dispData['orange_'.($oNum + $l)]; }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+ 7)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+13)];
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+6)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+12)];
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)], 'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+17)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+23)];
                        }
                        else if ($l==5) {}
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+16)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+22)];
                        }
                    }
                    break;

                case '21': case '45': case '69': case '75': case '81': case '87': case '93':
                    $gNum = $firstGreenNumArr[$i];
                    $rNum = $firstRedNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $firstMainDataArr[$i][$l]['data'] = $dispData['red_'.($rNum + $l)]; }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['red_'.($rNum+$l+7)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['red_'.($rNum+$l+13)];
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['red_'.($rNum+$l+6)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['red_'.($rNum+$l+12)];
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)], 'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['red_'.($rNum+$l+17)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['red_'.($rNum+$l+23)];
                        }
                        else if ($l==5) {}
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['red_'.($rNum+$l+16)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['red_'.($rNum+$l+22)];
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $secondRightOrangeArr = array(3 => 281, 9 => 297, 15 => 313);
        $secondLeftOrangeArr  = array(3 => 329, 9 => 345, 15 => 361);
        $secondRightRedArr    = array(3 => 221, 9 => 229, 15 => 237, 21 => 245);
        $secondLeftRedArr     = array(3 => 269, 9 => 277, 15 => 285, 21 => 293);

        for ($i=0; $i<=26; $i++) {
            $orNum = null;
            $olNum = null;
            $rrNum = null;
            $rlNum = null;
            switch ($i) {
                case '1':
                    $secondRightMainData[$i][0] = array('data' => $dispData['red_220'], 'attr' => array($colspan_2));
                    break;

                case '3': case '9': case '15':
                    $orNum = $secondRightOrangeArr[$i];
                    $olNum = $secondLeftOrangeArr[$i];
                    $rrNum = $secondRightRedArr[$i];
                    $rlNum = $secondLeftRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        $secondLeftMainData[$i][$l]    = array('data' => $dispData['orange_'.($orNum+$l)]);
                        $secondLeftMainData[$i+1][$l]  = array('data' => $dispData['orange_'.($orNum+$l+4)]);
                        $secondLeftMainData[$i+2][$l]  = array('data' => $dispData['orange_'.($orNum+$l+8)]);
                        $secondLeftMainData[$i+3][$l]  = array('data' => $dispData['orange_'.($orNum+$l+12)]);
                        $secondLeftMainData[$i+4][$l]  = array('data' => $dispData['red_'.($rrNum+$l)]);
                        $secondLeftMainData[$i+5][$l]  = array('data' => $dispData['red_'.($rrNum+$l+4)]);

                        $secondRightMainData[$i][$l]   = array('data' => $dispData['orange_'.($olNum+$l)]);
                        $secondRightMainData[$i+1][$l] = array('data' => $dispData['orange_'.($olNum+$l+4)]);
                        $secondRightMainData[$i+2][$l] = array('data' => $dispData['orange_'.($olNum+$l+8)]);
                        $secondRightMainData[$i+3][$l] = array('data' => $dispData['orange_'.($olNum+$l+12)]);
                        $secondRightMainData[$i+4][$l] = array('data' => $dispData['red_'.($rlNum+$l)]);
                        $secondRightMainData[$i+5][$l] = array('data' => $dispData['red_'.($rlNum+$l+4)]);
                    }
                    break;

                case '21':
                    $orNum = $secondRightOrangeArr[$i];
                    $olNum = $secondLeftOrangeArr[$i];
                    $rrNum = $secondRightRedArr[$i];
                    $rlNum = $secondLeftRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        for ($r=0; $r<=5; $r++) {
                            $secondLeftMainData[$i+$r][$l]  = array('data' => $dispData['red_'.($rrNum+$l+($r*4))]);
                            $secondRightMainData[$i+$r][$l] = array('data' => $dispData['red_'.($rlNum+$l+($r*4))]);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $thirdRedArr    = array(2 => 317);
        $thirdOrangeArr = array(3 => 377, 4 => 380, 5 => 383);

        for ($i=2; $i<=5; $i++) {
            $rNum = $thirdRedArr[$i];
            $oNum = $thirdOrangeArr[$i];
            if ($i==2) { for ($l=0; $l<=2; $l++) { $thirdMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum+$l)]); } }
            else { for ($l=0; $l<=2; $l++) { $thirdMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); } }
        }

        $fourthOrangeArr = array(1 => 386, 2 => 389, 3 => 392);
        $fourthRedArr    = array(1 => 320, 2 => 321, 3 => 322);

        for ($i=1; $i<=3; $i++) {
            $oNum = $fourthOrangeArr[$i];
            $rNum = $fourthRedArr[$i];
            for ($l=0; $l<=3; $l++) {
				if ($l!=3) { $fourthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)] ); }
				else { $fourthMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum)] , 'disp_type' => 'float' ); }
            }
        }

        $fifthOrangeArr = array(
             1 => 395,  2 => 398,  3 => 401,  4 => 404,  5 => 407,  6 => 410,  7 => 413,  8 => 416,  9 => 419, 10 => 422
          , 11 => 425, 12 => 428, 13 => 431, 15 => 434, 16 => 437, 17 => 440, 18 => 443, 19 => 446, 20 => 449, 21 => 452
          , 22 => 455, 23 => 458, 24 => 461, 25 => 464, 26 => 467, 28 => 470, 29 => 471
        );
        $fifthRedArr    = array(14 => 323, 27 => 324, 30 => 325);
        for ($i=1; $i<=30; $i++) {
            $oNum = $fifthOrangeArr[$i];
            $rNum = $fifthRedArr[$i];
            switch ($i) {
                default:
                    for ($l=0; $l<=2; $l++) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                    break;

                case '14':
                    for ($l=0; $l<=2; $l++) {
                        if ($l==0) { $fifthMainDataArr[$i][$l] = array('data' => '&nbsp;', 'attr' => array($colspan_2), 'style' => array($bgColor_white)); }
                        else if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum)]); }
                    }
                    break;

                case '27':
                    for ($l=0; $l<=2; $l++) {
                        if ($l==0) { $fifthMainDataArr[$i][$l] = array('data' => '&nbsp;', 'attr' => array($colspan_2, $rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum)]); }
                    }
                    break;

                case '30':
                    for ($l=0; $l<=2; $l++) { if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum)]); } }
                    break;

                case '28': case '29':
                    for ($l=0; $l<=2; $l++) { if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum)]); } }
                    break;
            }
        }

        $sixthOrangeArr = array(3 => 472, 9 => 505, 15 => 538);
        $sixthGreenArr  = array(3 =>  65, 9 =>  69, 15 =>  73,  21 => 77);
        $sixthRedArr    = array(21 => 327);
        for ($i=1; $i<=25; $i++) {
            $oNum = null;
            $gNum = null;
            switch ($i) {
                case '1':
                    $sixthMainDataArr[$i] = array(0 => array('data' => $dispData['red_326'], 'attr' => array($colspan_2), 'style' => array($textAlign_right)));
                    break;

                case '3': case '9': case '15':
                    $oNum = $sixthOrangeArr[$i];
                    $gNum = $sixthGreenArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $sixthMainDataArr[$i][$l]   = array('data' => $dispData['orange_'.($oNum+$l)]); }
                        # 2, 3
                        if ($l<=2) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+7)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+13)]);
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+6)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+12)]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)], 'style' => array($bgColor_white));
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['orange_'.($oNum+$l+17)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['orange_'.($oNum+$l+24)]);
                        }
                    }
                    break;

                case '21':
                    $gNum = $sixthGreenArr[$i];
                    $rNum = $sixthRedArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $sixthMainDataArr[$i][$l]   = array('data' => $dispData['red_'.($rNum+$l)]); }
                        # 2, 3
                        if ($l<=2) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['red_'.($rNum+$l+7)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['red_'.($rNum+$l+13)]);
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['red_'.($rNum+$l+6)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['red_'.($rNum+$l+12)]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)], 'style' => array($bgColor_white));
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['red_'.($rNum+$l+17)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['red_'.($rNum+$l+24)]);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $sevenMainDataArr = array(0 => array('data' => $dispData['orange_280'], 'style' => array('textAlign' => $textAlign_right)));

        ### first table ###
        $rtnStr .= '<table class="list" >';
        for ($i=0; $i <= 97; $i++) {
            if ($i <= 1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 19) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 25) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue)).'</tr>';
            }
            else if ($i <= 43) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 49) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed)).'</tr>';
            }
            else if ($i <= 67) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 73) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen)).'</tr>';
            }
            else if ($i <= 91) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 97) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
            else {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
        }
        $rtnStr .= '</table><br />';

        ### second table ###
        $rtnStr .= '<table class="list" >';
        for ($i=0; $i <= 26; $i++) {
            if ($i <= 1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                           .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                           .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                           .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 2) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightOrange))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 6) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 8) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue)).'</tr>';
            }
            else if ($i <= 12) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 14) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed)).'</tr>';
            }
            else if ($i <= 18) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 20) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen)).'</tr>';
            }
            else if ($i <= 24) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 26) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
            else {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
        }
        $rtnStr .= '</table><br />';

        ### third table ###
        $rtnStr .=             '<table class="list" >';
        for ($i=0; $i <= 5; $i++) {
            switch ($i) {
                case '0':
                    $rtnStr .=    '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                                  .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '1':
                    $rtnStr .=    '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                                  .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '2':
                    $rtnStr .=    '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                                  .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '3':
                    $rtnStr .=    '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                                  .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '4':
                    $rtnStr .=    '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                                  .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '5':
                    $rtnStr .=    '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                                  .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;
            }
        }
        $rtnStr .=             '</table><br />';

        ### fourth table ###
        $rtnStr .=             '<table class="list" >'.'<tr><td colspan="6">'.sprintf($fontTag, '負担限度額認定者比率(日数比率)').'</td></tr>';

        for ($i=0; $i <= 3; $i++) {
            $rtnStr .=             '<tr>'.$this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
                                  .$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
        }
        $rtnStr .=             '</table><br />';

        ### fifth table ###
        $rtnStr .= '<table class="list">'
                      .'<tr><td colspan="6">'.sprintf($fontTag, '自己負担分明細書').'</td></tr>';
        for ($i=0; $i <=30; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($fifthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($fifthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .= '</table><br />';

        ### sixth table ###
        $rtnStr .= '<table class="list" >';
        for ($i=0; $i < 26; $i++) {
            if ($i <= 1) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 7) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 13) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 19) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i <= 25) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }

        }
        $rtnStr .= '</table><br />';

        $fontArr = $this->getFont();
        $fontArr['color'] = 'red';
        $fontTag_red = $this->getFontTag($fontArr);

        ### seventh table ###
        $rtnStr .= '<table class="list" >'
                      .'<tr>'
                          .'<td colspan="2">'.sprintf($fontTag_red, '介護職員処遇改善交付金').'</td>'
                      .'</tr><tr>'
                          .$this->convertCellData($seventhHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
                          .$this->convertCellData($sevenMainDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>'
                  .'</table>';

        return $rtnStr;
    }

    /**
     * No.126 [老健] 予防給付療養費明細書のデータ表示出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string    $rtnStr
     */
    function getPreventiveSupplyMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = '';

        $fontTag      = $this->getFontTag();
        $facilityName = $plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_13 = array('name' => 'rowspan', 'value' => 13);
        $rowspan_18 = array('name' => 'rowspan', 'value' => 18);
        $rowspan_24 = array('name' => 'rowspan', 'value' => 24);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_6  = array('name' => 'colspan', 'value' =>  6);
        $colspan_7  = array('name' => 'colspan', 'value' =>  7);
        $colspan_10 = array('name' => 'colspan', 'value' => 10);

        # style
        $bgColor_gray        = array('name' => 'background-color', 'value' => '#c0c0c0');
        $bgColor_white       = array('name' => 'background-color', 'value' => '#ffffff');
        $bgColor_lightBlue   = array('name' => 'background-color', 'value' => '#b7dbff');
        $bgColor_lightRed    = array('name' => 'background-color', 'value' => '#ffb7b7');
        $bgColor_lightGreen  = array('name' => 'background-color', 'value' => '#a4ffa4');
        $bgColor_lightYellow = array('name' => 'background-color', 'value' => '#ffe8b7');
        $bgColor_lightOrange = array('name' => 'background-color', 'value' => '#ffc6a4');
        $bgColor_purple      = array('name' => 'background-color', 'value' => '#dbb7ff');
        $bgColor_blue        = array('name' => 'background-color', 'value' => '#75a9ff');
        $textAlign_right     = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left      = array('name' => 'text-align',       'value' => 'left');
        $textAlign_center    = array('name' => 'text-align',       'value' => 'center');
        $whiteSpace_nowrap   = array('name' => 'white-space',      'value' => 'nowrap');

        # 以下、表示データ
        $rtnStr .= '<table width="100%" border="0" ><tr>'
                       .$this->convertCellData(
                           array(0 => array('data'  => '施設名：'.$facilityName)
                               , 1 => array('data'  => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array('textAlign' => $textAlign_right))
                           )
                         , $fontTag
                         , array()
                       )
                       .'</tr>'
                   .'</table>';

        ### header ###
        $firstTypeHeaderArr = array(
            3 => array('data' => '保険種別'),    4 => array('data' => '公費法別'),    5 => array('data' => '件数(人数)')
          , 6 => array('data' => '日数'),        7 => array('data' => '単位'),        8 => array('data' => '保険請求額')
          , 9 => array('data' => '公費請求額'), 10 => array('data' => '利用者負担'), 11 => array('data' => '費用合計額')
        );

        $firstHeaderArr = array(
             0 => array(0 => array('data' => '保険請求分', 'attr' => array($rowspan_2, $colspan_7), 'style' => array('textAlign' => $textAlign_left))
                      , 1 => array('data' => '総請求額', 'attr' => array($colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow))
             )
           , 1 => array(0 => array('data' => '保険分請求総額', 'attr' => array($colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow)))
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11=> array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 26 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 27 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 28 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 29 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 32 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 33 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 34 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 35 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 38 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 39 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 40 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 41 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 44 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 45 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 46 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 47 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 50 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 51 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 52 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 53 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 56 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 57 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 58 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 59 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 62 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 63 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 64 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 65 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 68 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 69 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 70 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 71 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $secondLeftHeaderArr = array(
             0 => array(0 => array('data' => '特定入所者介護サービス費等', 'attr' => array($rowspan_2, $colspan_10), 'style' => array('textAlign' => $textAlign_left)))
           , 2 => array(0 => array('data' => '保険請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_2)), 1 => array('data' => '合計',     'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
        );
        $secondRightHeaderArr = array(
             1 => array(1 => array('data' => '特定入所者分請求額', 'attr' => array($colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow)))
           , 2 => array(0 => array('data' => '公費請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_2)), 1 => array('data' => '合計',     'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
        );

        $thirdHeaderArr = array(
            0 => array(0 => array('data' => '居宅介護支援(ケアプラン作成料)', 'attr' => array($colspan_4)))
          , 1 => array(0 => array('data' => '&nbsp;'), 1 => array('data' => '件数'), 2 => array('data' => '単位'), 3 => array('data' => '金額'))
          , 2 => array(0 => array('data' => '合計'))
          , 3 => array(0 => array('data' => '当月'))
          , 4 => array(0 => array('data' => '月遅れ'))
          , 5 => array(0 => array('data' => '返戻再請求'))
        );

        $fourthHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)), 1 => array('data' => '低所得者件数'), 2 => array('data' => '低所得者日数')
                     , 3 => array('data' => '入所者全日数'), 4 => array('data' => '比率'))
          , 1 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_2)), 1 => array('data' => '合計'))
          , 2 => array(0 => array('data' => 'ショート'))
        );

        $fifthHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数'), 3 => array('data' => '金額'))
          , 1 => array(0 => array('data' => '通所'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
          , 2 => array(0 => array('data' => 'ショート', 'attr' => array($rowspan_13)), 1 => array('data' => '4段階', 'attr' => array($rowspan_2)), 2 => array('data' => '滞在費(個室室)'))
          , 3 => array(0 => array('data' => '滞在費(多床室)'))
          , 4 => array(0 => array('data' => '2･3段階', 'attr' => array($rowspan_2)), 1 => array('data' => '滞在費(低所得者負担金あり 多床室)'))
          , 5 => array(0 => array('data' => '滞在費(低所得 者負担金あり 個室)'))
          , 6 => array(0 => array('data' => '1段階'), 1 => array('data' => '滞在費(低所得者負担金なし)'))
          , 7 => array(0 => array('data' => '4段階', 'attr' => array($rowspan_4)), 1 => array('data' => '食費(1日)'))
          , 8 => array(0 => array('data' => '食費(朝)'))
          , 9 => array(0 => array('data' => '食費(昼)'))
         , 10 => array(0 => array('data' => '食費(夕)'))
         , 11 => array(0 => array('data' => '3段階'), 1 => array('data' => '食費'))
         , 12 => array(0 => array('data' => '2段階'), 1 => array('data' => '食費'))
         , 13 => array(0 => array('data' => '1段階'), 1 => array('data' => '食費'))
         , 14 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
         , 15 => array(0 => array('data' => '特別な室料', 'attr' => array($colspan_3)))
         , 16 => array(0 => array('data' => 'その他の自費', 'attr' => array($colspan_3)))
         , 17 => array(0 => array('data' => '総合計', 'attr' => array($colspan_3)))
        );

        $sixthHeaderArr = array(
             0 => array(0 => array('data' => '訪問リハビリ', 'attr' => array($rowspan_2, $colspan_6), 'style' => array('textAlign' => $textAlign_left)))
           , 1 => array(0 => array('data' => '保険分請求総額', 'attr' => array($colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow)))
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $seventhHeaderArr = array(0 => array('data' => '請求額'));

        ### data ###
        $dispData = array();
        for($i=81; $i<=144; $i++) { $dispData['green_'.$i] = null; }
        for ($i=360; $i<=960; $i++) {
            if ($i<=605) { $dispData['red_'.$i] = null; }
            if ($i>=571) { $dispData['orange_'.$i] = null; }
        }

        # ここでデータを配列に･･･
        if (is_array($dataArr) && count($dataArr) > 0 ) { foreach ($dataArr[0] as $key => $val) { $dispData[$key] = $val; } }

        $firstOrangeNumArr = array(3 => 571,   9 => 602,  21 => 633,  27 => 664,  39 => 695,  45 => 726);
        $firstRedNumArr    = array(0 => 360,   1 => 361,  15 => 362,  33 => 393,  51 => 424,  57 => 455,  63 => 486,  69 => 517);
        $firstGreenNumArr  = array(3 =>  81,   9 =>  85,  15 =>  89,  21 =>  93,  27 =>  97,  33 => 101,  39 => 105,  45 => 109,  51 => 113,  57 => 117,  63 => 121,  69 => 125);

        $firstMainDataArr = array();
        for ($i = 0; $i <= 73; $i++) {
            $oNum = null;
            $rNum = null;
            $gNum = null;

            switch ($i) {
                case '0': case '1':
                    $rNum = $firstRedNumArr[$i];
                    $firstMainDataArr[$i] = array(0 => array('data' => $dispData['red_'.$rNum], 'attr' => array($colspan_2)));
                    break;

                case '3': case '9': case '21': case '27': case '39': case '45';
                    $oNum = $firstOrangeNumArr[$i];
                    $gNum = $firstGreenNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $firstMainDataArr[$i][$l]['data'] = $dispData['orange_'.($oNum + $l)]; }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+ 7)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+13)];
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+6)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+12)];
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+17)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+23)];
                        }
                        else if ($l==5) {
                            # なにもしない
                        }
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+16)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+22)];
                        }
                    }
                    break;

                case '15': case '33': case '51': case '57': case '63': case '69':
                    $gNum = $firstGreenNumArr[$i];
                    $rNum = $firstRedNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $firstMainDataArr[$i][$l]['data'] = $dispData['red_'.($rNum + $l)]; }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['red_'.($rNum+$l+7)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['red_'.($rNum+$l+13)];
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['red_'.($rNum+$l+6)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['red_'.($rNum+$l+12)];
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['red_'.($rNum+$l+17)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['red_'.($rNum+$l+23)];
                        }
                        else if ($l==5) {
                            # なにもしない
                        }
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['red_'.($rNum+$l+16)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['red_'.($rNum+$l+22)];
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $secondLeftMainData   = array();
        $secondRightMainData  = array();
        $secondLeftOrangeArr  = array(3 => 758, 5 => 766, 7 => 774);
        $secondRightOrangeArr = array(3 => 782, 5 => 790, 7 => 798);
        $secondLeftRedArr     = array(9 => 549);
        $secondRightRedArr    = array(9 => 557);

        for ($i=0; $i<=10; $i++) {
            $orNum = null;
            $olNum = null;
            $rrNum = null;
            $rlNum = null;

            switch ($i) {
                case '1':
                    $secondRightMainData[$i][0] = array('data' => $dispData['red_548'], 'attr' => array($colspan_2));
                    break;

                case '3': case '5': case '7': case '11':
                    $olNum = $secondLeftOrangeArr[$i];
                    $orNum = $secondRightOrangeArr[$i];
                    $rlNum = $secondLeftRedArr[$i];
                    $rrNum = $secondRightRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        $secondLeftMainData[$i][$l]    = array('data' => $dispData['orange_'.($olNum+$l)]);
                        $secondLeftMainData[$i+1][$l]  = array('data' => $dispData['orange_'.($olNum+$l+4)]);
                        $secondRightMainData[$i][$l]   = array('data' => $dispData['orange_'.($orNum+$l)]);
                        $secondRightMainData[$i+1][$l] = array('data' => $dispData['orange_'.($orNum+$l+4)]);
                    }
                    break;

                case '9':
                    $rlNum = $secondLeftRedArr[$i];
                    $rrNum = $secondRightRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        for ($r=0; $r<=5; $r++) {
                            $secondLeftMainData[$i+$r][$l]  = array('data' => $dispData['red_'.($rlNum+$l+($r*4))]);
                            $secondRightMainData[$i+$r][$l] = array('data' => $dispData['red_'.($rrNum+$l+($r*4))]);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $thirdMainDataArr = array();
        $thirdRedArr      = array(2 => 565);
        $thirdOrangeArr   = array(3 => 806, 4 => 809, 5 => 812);

        for ($i=2; $i<=5; $i++) {
            $rNum = $thirdRedArr[$i];
            $oNum = $thirdOrangeArr[$i];
            if ($i==2) { for ($l=0; $l<=2; $l++) { $thirdMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum+$l)]); } }
            else { for ($l=0; $l<=2; $l++) { $thirdMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); } }
        }

        $fourthOrangeArr = array(1 => 815, 2 => 818);
        $fourthRedArr    = array(1 => 568, 2 => 569);

        for ($i=1; $i<=2; $i++) {
            $oNum = $fourthOrangeArr[$i];
            $rNum = $fourthRedArr[$i];
            for ($l=0; $l<=3; $l++) {
				if ($l!=3) { $fourthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                else {       $fourthMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum)] , 'disp_type' => 'float' ); }
            }
        }

        $fifthMainDataArr = array();
        $fifthOrangeArr   = array( 1 => 821,  2 => 824,  3 => 827,  4 => 830,  5 => 833,  6 => 836,  7 => 839,  8 => 842,  9 => 845, 10 => 848, 11 => 851, 12 => 854, 13 => 857, 15 => 860, 16 => 861);
        $fifthRedArr      = array(14 => 570, 17 => 571);
        for ($i=1; $i<=17; $i++) {
            $oNum = $fifthOrangeArr[$i];
            $rNum = $fifthRedArr[$i];
            switch ($i) {
                default:
                    for ($l=0; $l<=2; $l++) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                    break;

                case '14': case '17':
                    for ($l=0; $l<=2; $l++) {
                        if ($l==0 && $i==14) { $fifthMainDataArr[$i][$l] = array('data' => '&nbsp;', 'attr' => array($colspan_2, $rowspan_4), 'style' => array($bgColor_white)); }
                        else if($l==2) {       $fifthMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum)]); }
                    }
                    break;

                case '15': case '16':
                    for ($l=0; $l<=2; $l++) { if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum)]); } }
                    break;
            }
        }

        $sixthOrangeArr = array( 3 => 862, 9 => 895, 15 => 928);
        $sixthGreenArr  = array( 3 => 129, 9 => 133, 15 => 137,  21 => 141);
        $sixthRedArr    = array(21 => 573);
        for ($i=1; $i<=25; $i++) {
            $oNum = null;
            $gNum = null;
            switch ($i) {
                case '1':
                    $sixthMainDataArr[$i] = array(0 => array('data' => $dispData['red_572'], 'attr' => array($colspan_2), 'style' => array($textAlign_right)));
                    break;

                case '3': case '9': case '15':
                    $oNum = $sixthOrangeArr[$i];
                    $gNum = $sixthGreenArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $sixthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                        # 2, 3
                        if ($l<=2) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+7)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+13)]);
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+6)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+12)]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['orange_'.($oNum+$l+17)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['orange_'.($oNum+$l+24)]);
                        }
                    }
                    break;

                case '21':
                    $gNum = $sixthGreenArr[$i];
                    $rNum = $sixthRedArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $sixthMainDataArr[$i][$l] = array('data' => $dispData['red_'.($rNum+$l)]); }
                        # 2, 3
                        if ($l<=2) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['red_'.($rNum+$l+7)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['red_'.($rNum+$l+13)]);
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['red_'.($rNum+$l+6)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['red_'.($rNum+$l+12)]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['red_'.($rNum+$l+17)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['red_'.($rNum+$l+24)]);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $sevenMainDataArr = array(0 => array('data' => $dispData['orange_757'], 'style' => array($textAlign_right)));

        ### first table ###
        $rtnStr .= '<table class="list" >';
        for ($i=0; $i <= 73; $i++) {
            if ($i<=1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=13) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=19) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue)).'</tr>';
            }
            else if ($i<=31) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=37) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed)).'</tr>';
            }
            else if ($i<=49) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=55) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen)).'</tr>';
            }
            else if ($i<=67) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=73) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
        }
        $rtnStr .= '</table><br />';

        ### second table ###
        $rtnStr .= '<table class="list" >';
        for ($i=0; $i <= 10; $i++) {
            if ($i<=1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=2) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightOrange))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=4) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=6) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=8) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=10) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
        }
        $rtnStr .= '</table><br>';

        ### third table ###
        $rtnStr .= '<table class="list" >';
        for ($i=0; $i <= 5; $i++) {
            switch ($i) {
                case '0':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_left, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '1':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '2':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '3':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '4':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;

                case '5':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
                    break;
            }

        }
        $rtnStr .= '</table><br />';

        ### fourth table ###
        $rtnStr .= '<table class="list" ><tr>'.$this->convertCellData(array(0 => array('data' => '負担限度額認定者比率(日数比率)', 'attr' => array($colspan_6))), $fontTag, array());
        for ($i=0; $i <= 3; $i++) {
            $rtnStr .= '<tr>'.$this->convertCellData($fourthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
                      .$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
        }
        $rtnStr .= '</table><br />';

        ### fifth table ###
        $rtnStr .= '<table class="list" ><tr><td colspan="6">'.sprintf($fontTag, '自己負担分明細書').'</td></tr>';
        for ($i=0; $i <=17; $i++) {
            $rtnStr .= '<tr>'.$this->convertCellData($fifthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                      .$this->convertCellData($fifthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
        }
        $rtnStr .= '</table><br />';

        ### sixth table ###
        $rtnStr .= '<table class="list" >';
        for ($i=0; $i < 26; $i++) {
            if ($i<=1) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=7) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=13) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=19) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap)).'</tr>';
            }
            else if ($i<=25) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }

        }
        $rtnStr .= '</table><br />';

        $fontArr = $this->getFont();
        $fontArr['color'] = 'red';
        $fontTag_red = $this->getFontTag($fontArr);

        ### seventh table ###
        $rtnStr .= '<table class="list" >'
                      .'<tr>'.$this->convertCellData(array(0 => array('data' => '介護職員処遇改善交付金', 'attr' => array($colspan_2))), $fontTag_red, array()).'</td></tr>'
                      .'<tr>'.$this->convertCellData($seventhHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
                      .$this->convertCellData($sevenMainDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                  .'</table>';
        return $rtnStr;
    }

}
