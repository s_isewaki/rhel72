<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>カルテビューワー</title>
<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 73, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// カルテビューワー管理権限の取得
$admin_auth = check_authority($session, 74, $fname);

// データベースに接続
$con = connect2db($fname);

// カルテビューワーURLを取得
$sql = "select kview_url from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$kview_url = pg_fetch_result($sel, 0, "kview_url");
if ($kview_url != "" && $admin_auth == 1) {
	$separator = (strpos($kview_url, "?") === false) ? "?" : "&";
	$kview_url .= $separator . "mode=admin";
}

// 職員IDを取得
$sql = "select emp_id, emp_personal_id from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");

// ログイン情報を取得
$sql = "select emp_login_id, emp_login_pass from login";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_login_id = pg_fetch_result($sel, 0, "emp_login_id");
$emp_login_pass = pg_fetch_result($sel, 0, "emp_login_pass");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
<? if ($kview_url != "") { ?>
	document.mainform.submit();
<? } ?>
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<? if ($kview_url == "") { ?>
<p><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カルテビューワーURLが設定されていません。管理者にお問い合わせください。</font></p>
<? } else { ?>
<form name="mainform" action="<? echo($kview_url); ?>" method="post">
<input type="hidden" name="emp_id" value="<? echo($emp_personal_id); ?>">
<input type="hidden" name="login_id" value="<? echo($emp_login_id); ?>">
<input type="hidden" name="pass" value="<? echo($emp_login_pass); ?>">
</form>
<? } ?>
</body>
<? pg_close($con); ?>
</html>
