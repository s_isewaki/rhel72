<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜管理指標</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");

require_once("jnl_application_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
  echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
  echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
  exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);

require_once('jnl_indicator_facility_data.php');
/* @var $indicatorData IndicatorFacilityData */
$indicatorData = new IndicatorFacilityData(array('fname' => $fname, 'con' => $con));
$noArr = $indicatorData->getNoForNo($no);
?>
<script type="text/javascript">
<!--
function selectFiscalYear(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm        = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type        = document.getElementById('type');
    category    = document.getElementById('category');
    plant       = document.getElementById('_plant');
    plant.name  = 'plant';
    subCategory = document.getElementById('sub_category');
    fiscalYear  = document.getElementById('fiscal_year');
//    displayYear  = document.getElementById('display_year');
//    displayMonth = document.getElementById('display_month');
//    displayDay   = document.getElementById('display_day');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType     = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value     = 'plant';
        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
//        alert(category.value);
//        alert(subCategory.value);
//        alert(no.value);
    }
    else if (subCategory==null) {
//        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    fiscalYear.value  = e.value;
//    displayYear.value  = e.value;
    wkfwForm.submit();
}


function selectYear(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm        = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type         = document.getElementById('type');
    category     = document.getElementById('category');
    plant        = document.getElementById('plant');
    subCategory  = document.getElementById('sub_category');
    displayYear  = document.getElementById('display_year');
    displayMonth = document.getElementById('display_month');
    displayDay   = document.getElementById('display_day');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType     = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value     = 'plant';
        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
//        alert(category.value);
//        alert(subCategory.value);
//        alert(no.value);
    }
    else if (subCategory==null) {
//        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    displayYear.value  = e.value;
    wkfwForm.submit();
}

function selectMonth(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm        = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type         = document.getElementById('type');
    category     = document.getElementById('category');
    plant        = document.getElementById('plant');
    subCategory  = document.getElementById('sub_category');
    displayYear  = document.getElementById('display_year');
    displayMonth = document.getElementById('display_month');
    displayDay   = document.getElementById('display_day');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType     = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value     = 'plant';
        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
//        alert(category.value);
//        alert(subCategory.value);
//        alert(no.value);
    }
    else if (subCategory==null) {
//        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    displayMonth.value  = e.value;
    wkfwForm.submit();
}

function selectDay(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm        = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type         = document.getElementById('type');
    category     = document.getElementById('category');
    plant        = document.getElementById('plant');
    subCategory  = document.getElementById('sub_category');
    displayYear  = document.getElementById('display_year');
    displayMonth = document.getElementById('display_month');
    displayDay   = document.getElementById('display_day');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType     = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value     = 'plant';
        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
//        alert(category.value);
//        alert(subCategory.value);
//        alert(no.value);
    }
    else if (subCategory==null) {
//        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    displayDay.value  = e.value;
    wkfwForm.submit();
}

function clickDownLoad() {
  wkfwForm        = document.getElementById("wkfw");
  wkfwForm.action = 'jnl_application_indicator_facility_excel.php';
  wkfwForm.method = 'get';
  wkfwForm.submit();
}

//-->
</script>
<script type="text/javascript" src="js/jnl/jnl_indicator_menu_list.js"></script>
<script type="text/javascript" src="js/jnl/jnl_indicator_ajax.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/jnl/tategaki.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
<style type="text/css">
.this_table {border: medium solid 1px #5279A5;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="決裁・管理指標"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_indicator_menu.php?session=<? echo($session); ?>"><b>管理指標</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form id="wkfw" name="wkfw" action="jnl_application_imprint.php" method="post" enctype="multipart/form-data">
    <input type="hidden" id="category" name="category" value="<?= $category?>" />
    <input type="hidden" id="sub_category" name="sub_category" value="<?= $sub_category?>" />
<?php
if (!empty($plant)) {
?>
    <input type="hidden" id="plant" name="plant" value="<?= $plant?>" />
    <input type="hidden" id="type" name="type" value="<?= $type?>" />
    <input type="hidden" id="hidden_type" name="hidden_type" value="<?= $category?>" />
<?php
}
?>
    <input type="hidden" id="no" name="no" value="<?= $no?>" />
<?php
require_once('jnl_indicator_facility_display.php');
/* @var $indicatorDisplay IndicatorFacilityDisplay */
$indicatorDisplay = new IndicatorFacilityDisplay(array('no' => $no, 'last_no' => $last_no, 'con' => $con, 'session' => $session, 'current_year' => $fiscal_year, 'display_month' => $display_month, 'type' => 'fiscal'));
$display_year  = $indicatorDisplay->getDisplayYear();
$display_month = $indicatorDisplay->getDisplayMonth();

/* menu list */
echo $indicatorDisplay->getMenuList(array('category' => $category, 'sub_category' => $sub_category, 'no' => $no, 'plant' => $plant, 'type' => $type), null, null, $emp_id);

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td>
<?php
# 表示年度指定用 select box
$fiscal_year = $indicatorDisplay->getCurrentYear();
$display_year = $indicatorDisplay->getCurrentYear();
echo $indicatorDisplay->getSelectFiscalDislpayYear(array(
    'fiscal_year'   => $fiscal_year,
    'category'      => $category,
    'sub_category'  => $sub_category,
    'no'            => $no,
    'type'          => $type,
    'plant'         => $plant,
)).'年度';

$fontArr = $indicatorDisplay->getFont();
$fontArr['color'] = $indicatorDisplay->getRgbColor('ash');
$fontTag = $indicatorDisplay->getFontTag($fontArr);
$dataList = $indicatorData->getDisplayDataList(
    $no,
    $indicatorDisplay->getDateToLastYearStartMonth(),
    $indicatorDisplay->getDateToCurrentYearLastMonth(),
    array('year' => $fiscal_year, 'facility_id' => $plant)
);
?>
                <b><span style="<?= $indicatorDisplay->getRgbColor('yellow', 'background-color') ?>" ><?= sprintf($fontTag, '申請中') ?></span></b>
                <b><span style="<?= $indicatorDisplay->getRgbColor('red', 'background-color') ?>" ><?= sprintf($fontTag, '日報未入力') ?></span></b>
            </td>
            <td style="text-align: right;">
<? if ($dataList !== false) { ?>
                <input type="button" value="Excel出力" onclick="clickDownLoad();">
<? }?>
            </td>
        </tr>
    </tbody>
</table>
<?php
if ($dataList!==false) {
    $dataList = ($dataList===false)? array() : $dataList;
    echo $indicatorDisplay->getDataDisplayByNo(
        $no,
        $dataList,
        $indicatorData->getHospitalList($plant),
        $noArr,
        array('year' => $fiscal_year, 'facility_id' => $plant)
    );
}
?>
<input type="hidden" name="display_year" value="<? echo($display_year); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">

</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>
