<?php
require_once 'jnl_indicator_data.php';
require_once 'jnl_indicator_nurse_home.php';
class IndicatorNurseHomeData extends IndicatorData
{
    var $indicatorNurseHome;

    /**
     * コンストラクタ
     * @param  array $arr
     * @return void
     */
    function IndicatorNurseHomeData($arr)
    {
        parent::IndicatorData($arr);
    }

    /**
     * @return void
     */
    function setIndicatorNurseHome() {
        $this->indicatorNurseHome = new IndicatorNurseHome(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorNurseHome
     */
    function getIndicatorNurseHome() {
        if (!is_object($this->indicatorNurseHome)) { $this->setIndicatorNurseHome(); }
        return $this->indicatorNurseHome;
    }

    /**
     * (non-PHPdoc)
     * @see Indicator#getNoForNo($no)
     */
    function getNoForNo($no)
    {
        $indicatorNurseHome = $this->getIndicatorNurseHome();
        $noArr = $indicatorNurseHome->noArr;
        $data  = $this->getArrValueByKey($no, $noArr);
        if (is_null($data)) { $data = parent::getNoForNo($no); }
        return $data;
    }

    /**
     * (non-PHPdoc)
     * @see IndicatorData#getDisplayDataList($no, $lastYearMonth, $currentYearMonth, $optionArr)
     */
    function getDisplayDataList($no, $lastYearMonth, $currentYearMonth, $optionArr)
    {
        $data = false;
        switch ($no) {
            case '100': # [老健] 施設別患者日報
            case '101': # [老健] 施設別患者日報(シンプル版)
                $data = $this->getPatientDailyFacilities($optionArr);
                break;

            case '102'; # [老健] 施設別患者日報(累積統計表)
                $data = $this->getDailyReportStats($optionArr);
                break;

            case '105': # [老健] 施設別患者月報
            case '107': # [老健] 施設別患者月報(詳細版)
                $data = $this->getPatientMonReportDetail($optionArr);
                break;

            case '106': # [老健] 施設別患者月報(前年比較)
                $data = $this->getPatientMonReportCmp($optionArr);
                break;

            case '108': # [老健] 老健月間加算項目・算定実績一覧
                $data = $this->getCalculationResults($optionArr);
                break;

            case '110': # [老健] 老人保健施設通所稼働率
            case '114': # [老健] 老人保健施設入所稼働率
                $data = $this->getFacilityRate($no, $optionArr);
                break;

            case '111': # [老健] 老人保健施設通所(合計)延数
            case '112': # [老健] 老人保健施設通所(介護)延数
            case '113': # [老健] 老人保健施設通所(予防)延数
            case '115': # [老健] 老人保健施設入所(合計)延数
            case '116': # [老健] 老人保健施設入所(一般)延数
            case '117': # [老健] 老人保健施設入所(認知)延数
            case '118': # [老健] 老人保健施設入所(一般ショート)延数
            case '119': # [老健] 老人保健施設入所(認知ショート)延数
            case '120': # [老健] 老人保健施設入所(予防ショート)延数
            case '121':
            case '122':
            case '123':
                $data = $this->getNumberTotalFacility($no, $optionArr);
                break;

            default:
                $data = parent::getDisplayDataList($no, $lastYearMonth, $currentYearMonth, $optionArr);
        }

        return $data;
    }

    /**
     * No.100 [老健] 施設別患者日報
     * No.101 [老健] 施設別患者日報(シンプル版)
     * No.103 [施設別:老健] 患者月報(日報・日毎) の データ取得
     * @param  array $optionArr
     * @return array
     */
    function getPatientDailyFacilities($optionArr)
    {
        $jnlNurseHome = $this->getJnlNurseHomeDay();
        $rtnData = $jnlNurseHome->getNurseHomeData(
            $this->getArrValueByKey('facility_id', $optionArr),
            $this->getArrValueByKey('year',        $optionArr),
            $this->getArrValueByKey('month',       $optionArr),
            $this->getArrValueByKey('day',         $optionArr)
        );
        $this->setSqlArr($jnlNurseHome->getSqlArr());
        return $rtnData;
    }

    /**
     * No.102 [老健] 施設別患者日報(累積統計表) の データ取得
     * @param  array $optionArr
     * @return array
     */
    function getDailyReportStats($optionArr) {
        $jnlNurseHome = $this->getJnlNurseHomeDay();
        $rtnData = $jnlNurseHome->getNurseHomeDataOfStats(
            null,
            $this->getArrValueByKey('year',  $optionArr),
            $this->getArrValueByKey('month', $optionArr),
            $this->getArrValueByKey('day',   $optionArr)
        );
        $this->setSqlArr($jnlNurseHome->getSqlArr());
        return $rtnData;
    }

    /**
     * No.105 [老健] 施設別患者月報
     * No.107 [老健] 施設別患者月報(詳細版) の データ取得
     * @param  array $optionArr
     * @return array
     */
    function getPatientMonReportDetail($optionArr)
    {
        $jnlNurseHome = $this->getJnlNurseHomeDay();
        $rtnData = $jnlNurseHome->getNurseHomeDataOfMonth(
            null,
            $this->getArrValueByKey('year',  $optionArr),
            $this->getArrValueByKey('month', $optionArr)
        );
        $this->setSqlArr($jnlNurseHome->getSqlArr());
        return $rtnData;
    }

    /**
     * No.106 [老健] 施設別患者月報(前年比較)のデータ取得
     * @param  array $optionArr
     * @return array
     */
    function getPatientMonReportCmp($optionArr)
    {
        $rtnArr       = array();
        $currentYear  = $this->getArrValueByKey('year',  $optionArr);
        $lastYear     = $currentYear - 1;
        $month        = $this->getArrValueByKey('month', $optionArr);
        $jnlNurseHome = $this->getJnlNurseHomeDay();

        $rtnArr['current'] = $jnlNurseHome->getNurseHomeDataOfMonth(null, $currentYear, $month);
        $rtnArr['last']    = $jnlNurseHome->getNurseHomeDataOfMonth(null, $lastYear,    $month);
        $this->setSqlArr($jnlNurseHome->setSqlArr());

        return $rtnArr;
    }

    /**
     * No.110 [老健] 老人保健施設通所稼働率
     * No.114 [老健] 老人保健施設入所稼働率
     * @param  string $no
     * @param  array  $optionArr
     * @return array
     */
    function getFacilityRate($no, $optionArr)
    {
        $flgArr = null;
        switch ($no) {
            case '110': # [老健] 老人保健施設通所稼働率
                $flgArr = array('visit' => true);
                break;

            case '114': # [老健] 老人保健施設入所稼働率
                $flgArr = array('enter' => true, 'short' => true);
                break;

            default: # 一応登録･･･ここを通ると全部のクエリが走るが、重い。
                $flgArr = array('visit' => true, 'enter' => true, 'short' => true, 'rehab' => true, 'other' => true);
                break;
        }

        /* @var $jnlNurseHome JnlNurseHomeDay  */
        $jnlNurseHome = $this->getJnlNurseHomeDay();
        $rtnArr       = array();
        $year         = $optionArr['year'];
        for ($y=$year-1; $y<=$year+1; $y++) {
            $tempArr = $jnlNurseHome->getNurseHomeDataOfMonth(null, $y, null, $flgArr);
            foreach ($tempArr as $tempVal) { $rtnArr[] = $tempVal; }
        }

        foreach ($rtnArr as $rtnKey => $rtnVal) {
            # 通所
            $dispNum_7  = $rtnVal['dispnum_7'];
            $dispNum_14 = $rtnVal['dispnum_14'];
            $dispNum_21 = $rtnVal['dispnum_21'];
            $dispNum_28 = $rtnVal['dispnum_28'];
            $dispNum_35 = $rtnVal['dispnum_35'];
            $dispNum_42 = $rtnVal['dispnum_42'];
            $dispNum_49 = $rtnVal['dispnum_49'];
            $dispNum_53 = $rtnVal['dispnum_53'];
            $dailyLimit = $rtnVal['daily_limit'];
            $visit      = round( ($dispNum_7+$dispNum_14+$dispNum_21+$dispNum_28+$dispNum_35+$dispNum_42+$dispNum_49+$dispNum_53) / $dailyLimit * 100, 1);

            # 入所
            $dispNum_60 = $rtnVal['dispnum_60'];
            $dispNum_74 = $rtnVal['dispnum_74'];
            $dispNum_85 = $rtnVal['dispnum_85'];
            $dispNum_67 = $rtnVal['dispnum_67'];
            $dispNum_81 = $rtnVal['dispnum_81'];
            $dispNum_89 = $rtnVal['dispnum_89'];
            $enterLimit = $rtnVal['enter_limit'];
            $enter      = round( ($dispNum_60+$dispNum_74+$dispNum_85+$dispNum_67+$dispNum_81+$dispNum_89) / $enterLimit * 100, 1);

            switch ($no) {
                case '110': # [老健] 老人保健施設通所稼働率
                    $rtnArr[$rtnKey]['target_data'] = $visit;
                    break;

                case '114': # [老健] 老人保健施設入所稼働率
                    $rtnArr[$rtnKey]['target_data'] = $enter;
                    break;
            }
        }

        $this->setSqlArr($jnlNurseHome->getSqlArr());
        return $rtnArr;
    }

    /**
     * No.111 [老健] 老人保健施設通所(合計)延数
     * No.112 [老健] 老人保健施設通所(介護)延数
     * No.113 [老健] 老人保健施設通所(予防)延数
     * No.115 [老健] 老人保健施設入所(合計)延数
     * No.116 [老健] 老人保健施設入所(一般)延数
     * No.117 [老健] 老人保健施設入所(認知)延数
     * No.118 [老健] 老人保健施設入所(一般ショート)延数
     * No.119 [老健] 老人保健施設入所(認知ショート)延数
     * No.120 [老健] 老人保健施設入所(予防ショート)延数
     * No.121 [老健] 在宅復帰率
     * No.122 [老健] 判定件数
     * No.123 [老健] 営業件数
     * @param  string $no
     * @param  array  $optionArr
     * @return array
     */
    function getNumberTotalFacility($no, $optionArr)
    {
        $flgArr = null;
        switch ($no) {
            case '111': case '112': case '113':
                $flgArr = array('visit' => true);
                break;

            case '115': case '116': case '117':
                $flgArr = array('enter' => true, 'short' => true);
                break;

            case '118': case '119': case '120':
                $flgArr = array('short' => true);
                break;

            case '121': case '122': case '123':
                $flgArr = array('other' => true);
                break;

            default: # 一応登録･･･ここを通ると全部のクエリが走るが、重い。
                $flgArr = array('visit' => true, 'enter' => true, 'short' => true, 'rehab' => true, 'other' => true);
                break;
        }

        $jnlNurseHome = $this->getJnlNurseHomeDay();
        $rtnArr = array();
        $year = $optionArr['year'];
        for ($y=$year-1; $y<=$year+1; $y++) {
            $tempArr = $jnlNurseHome->getNurseHomeDataOfMonth(null, $y, null, $flgArr);
            foreach ($tempArr as $tempVal) { $rtnArr[] = $tempVal; }
        }

        foreach ($rtnArr as $rtnKey => $rtnVal) {
            $dispNum_7   = $this->getArrValueByKey('dispnum_7',   $rtnVal);
            $dispNum_14  = $this->getArrValueByKey('dispnum_14',  $rtnVal);
            $dispNum_21  = $this->getArrValueByKey('dispnum_21',  $rtnVal);
            $dispNum_28  = $this->getArrValueByKey('dispnum_28',  $rtnVal);
            $dispNum_35  = $this->getArrValueByKey('dispnum_35',  $rtnVal);
            $dispNum_42  = $this->getArrValueByKey('dispnum_42',  $rtnVal);
            $dispNum_49  = $this->getArrValueByKey('dispnum_49',  $rtnVal);
            $dispNum_53  = $this->getArrValueByKey('dispnum_53',  $rtnVal);
            $dispNum_60  = $this->getArrValueByKey('dispnum_60',  $rtnVal);
            $dispNum_67  = $this->getArrValueByKey('dispnum_67',  $rtnVal);
            $dispNum_74  = $this->getArrValueByKey('dispnum_74',  $rtnVal);
            $dispNum_81  = $this->getArrValueByKey('dispnum_81',  $rtnVal);
            $dispNum_85  = $this->getArrValueByKey('dispnum_85',  $rtnVal);
            $dispNum_89  = $this->getArrValueByKey('dispnum_89',  $rtnVal);
            $dispNum_102 = $this->getArrValueByKey('dispnum_102', $rtnVal);
            $dispNum_103 = $this->getArrValueByKey('dispnum_103', $rtnVal);
            $dispNum_106 = $this->getArrValueByKey('dispnum_106', $rtnVal);
            $dispNum_107 = $this->getArrValueByKey('dispnum_107', $rtnVal);
            $dispNum_108 = $this->getArrValueByKey('dispnum_108', $rtnVal);
            $dailyLimit  = $this->getArrValueByKey('daily_limit', $rtnVal);
            $enterLimit  = $this->getArrValueByKey('enter_limit', $rtnVal);

            $visitTotal           = (int)$dispNum_7 + (int)$dispNum_14 + (int)$dispNum_21 + (int)$dispNum_28 + (int)$dispNum_35 + (int)$dispNum_42
                                    + (int)$dispNum_49 + (int)$dispNum_53; # 通所 合計
            $visitCare            = ( (int)$dispNum_7 + (int)$dispNum_14 + (int)$dispNum_21 + (int)$dispNum_28 + (int)$dispNum_35 + (int)$dispNum_42
                                    + (int)$dispNum_49); # 通所 介護
            $visitPrevention      = (int)$dispNum_53; # 通所 予防

            # 入所 合計
            $enterTotal           = ( (int)$dispNum_60 + (int)$dispNum_74 + (int)$dispNum_85 + (int)$dispNum_67 + (int)$dispNum_81 + (int)$dispNum_89);
            $enterPublic          = (int)$dispNum_60; # 入所 一般
            $enterPerception      = (int)$dispNum_67; # 入所 認知
            $enterPublicShort     = (int)$dispNum_74; # short 一般ショート
            $enterPerceptionShort = (int)$dispNum_81; # short 認知ショート
            $enterPreventionShort = (int)$dispNum_85 + (int)$dispNum_89; # short 予防ショート
			$homeReturnRate       =  ((int)$dispNum_103 / (int)$dispNum_102) * 100 ; # 在宅復帰率
            $judgmentNumber       = array('pass' => (int)$dispNum_106, 'disapprove' => (int)$dispNum_107); # 判定件数
            $businessNumber       = (int)$dispNum_108; # 営業件数

            switch ($no) {
                case '111':
                    $rtnArr[$rtnKey]['target_data'] = $visitTotal;
                    break;

                case '112':
                    $rtnArr[$rtnKey]['target_data'] = $visitCare;
                    break;

                case '113':
                    $rtnArr[$rtnKey]['target_data'] = $visitPrevention;
                    break;

                case '115':
                    $rtnArr[$rtnKey]['target_data'] = $enterTotal;
                    break;

                case '116':
                    $rtnArr[$rtnKey]['target_data'] = $enterPublic;
                    break;

                case '117':
                    $rtnArr[$rtnKey]['target_data'] = $enterPerception;
                    break;

                case '118':
                    $rtnArr[$rtnKey]['target_data'] = $enterPublicShort;
                    break;

                case '119':
                    $rtnArr[$rtnKey]['target_data'] = $enterPerceptionShort;
                    break;

                case '120':
                    $rtnArr[$rtnKey]['target_data'] = $enterPreventionShort;
                    break;

                case '121':
                    $rtnArr[$rtnKey]['target_data'] = $homeReturnRate;
                    break;

                case '122':
                    $rtnArr[$rtnKey]['target_data'] = $judgmentNumber;
                    break;

                case '123':
                    $rtnArr[$rtnKey]['target_data'] = $businessNumber;
                    break;
            }
        }
        $this->setSqlArr($jnlNurseHome->getSqlArr());
        return $rtnArr;
    }

    /**
     * No.108 [老健] 老健月間加算項目・算定実績一覧
     * @param  array $optionArr
     * @return array
     */
    function getCalculationResults($optionArr)
    {
        /* @var $jnlNurseHomeMonth JnlNurseHomeMonth */
        $jnlNurseHomeMonth = $this->getJnlNurseHomeMonth();
        return $jnlNurseHomeMonth->getNurseHomeMonth(null, $this->getArrValueByKey('year', $optionArr), $this->getArrValueByKey('month', $optionArr));
    }

    function getCalculationResultsForFiscalYear($optionArr)
    {
        /* @var $jnlNurseHomeMonth JnlNurseHomeMonth */
        $jnlNurseHomeMonth = $this->getJnlNurseHomeMonth();
        return $jnlNurseHomeMonth->getNurseHomeMonthForFiscalYear($this->getArrValueByKey('facility_id', $optionArr), $this->getArrValueByKey('year', $optionArr));
    }

    /**
     * @var JnlNurseHomeDay
     */
    var $jnlNurseHomeDay;

    /**
     * @var JnlNurseHomeMonth
     */
    var $jnlNurseHomeMonth;

    /**
     * @return JnlNurseHomeDay
     */
    function getJnlNurseHomeDay()
    {
        if (!is_object($this->jnlNurseHomeDay)) { $this->setJnlNurseHomeDay(); }
        return $this->jnlNurseHomeDay;
    }

    /**
     * @return void
     */
    function setJnlNurseHomeDay()
    {
        $this->jnlNurseHomeDay = new JnlNurseHomeDay(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return JnlNurseHomeMonth
     */
    function getJnlNurseHomeMonth()
    {
        if (!is_object($this->jnlNurseHomeMonth)) { $this->setJnlNurseHomeMonth(); }
        return $this->jnlNurseHomeMonth;
    }

    /**
     * @return void
     */
    function setJnlNurseHomeMonth()
    {
        $this->jnlNurseHomeMonth = new JnlNurseHomeMonth(array('fname' => $this->fname, 'con' => $this->con));
    }

}

/**
 *
 */

class JnlNurseHomeDay extends IndicatorData
{
    /**
     * コンストラクタ
     * @param $arr
     * @return unknown_type
     */
    function JnlNurseHomeDay($arr)
    {
        parent::IndicatorData($arr);
    }

    /**
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getComplement($jnlFacilityId, $year, $month)
    {
        $lastDay   = $this->getLastDay($year, $month);
        $targetDay = sprintf('%04d%02d%02d', $year, $month, $lastDay);

        $sql = "SELECT main.jnl_facility_id, main.assigned_date, main.wk_limit, main.st_limit, main.sn_limit, main.enter_limit, main.wk_regard, main.st_regard"
                   .", main.sn_regard, main.ann_regard, SUBSTR(main.assigned_date, 0, 5) AS year, SUBSTR(main.assigned_date, 5, 2) AS month"
                   .", SUBSTR(main.assigned_date, 7, 2) AS day"
               ." FROM jnl_nurse_home_bed main"
               ." LEFT JOIN (SELECT MAX(assigned_date) as assigned_date"
                            ." FROM jnl_nurse_home_bed"
                           ." WHERE".sprintf(" assigned_date <= '%08d'", $targetDay)
                           .sprintf(" AND jnl_facility_id = (%d)", $jnlFacilityId)
                          .") sub"
                      ." ON main.assigned_date = sub.assigned_date"
              ." WHERE".sprintf(" jnl_facility_id = (%d)", $jnlFacilityId)
              ." ORDER BY main.assigned_date ";

        $rs      = $this->getDateList($sql);
        $dateArr = $this->getDayCountOfMonth($year, $month);
        foreach ($dateArr['day_array'] as $dataKey => $dataVal) {
            foreach ($rs as $rsKey => $rsVal) {
                if ($this->getTimestamp($dataVal['date']) >= $this->getTimestamp($rsVal['assigned_date'])) {
                    $checkType = null;
                    switch ($dataVal['day_type']) {
                        case 'weekday':
                            $checkType = $rsVal['wk_regard'];
                            break;

                        case 'sat':
                            $checkType = $rsVal['st_regard'];
                            break;

                        case 'sun':
                            $checkType = $rsVal['sn_regard'];
                            break;

                        case 'holiday':
                            $checkType = $rsVal['ann_regard'];
                            break;
                    }

                    switch ($checkType) {
                        case '0':
                            $dateArr['day_array'][$dataKey]['daily_limit']   = $rsVal['wk_limit'];
                            $dateArr['day_array'][$dataKey]['weekday_limit'] = $rsVal['wk_limit'];
                            break;

                        case '1':
                            $dateArr['day_array'][$dataKey]['daily_limit']    = $rsVal['st_limit'];
                            $dateArr['day_array'][$dataKey]['saturday_limit'] = $rsVal['st_limit'];
                            break;

                        case '2':
                            $dateArr['day_array'][$dataKey]['daily_limit']   = $rsVal['sn_limit'];
                            $dateArr['day_array'][$dataKey]['holiday_limit'] = $rsVal['sn_limit'];
                            break;
                    }
                    $dateArr['day_array'][$dataKey]['enter_limit'] = $rsVal['enter_limit'];
                    $dateArr['day_array'][$dataKey]['day_type']    = $dataVal['day_type'];
                }
            }
        }

        return $dateArr;
    }

    function getComplementOfDay($jnlFaclityId, $year, $month, $day)
    {
        $dateArr = $this->getComplement($jnlFaclityId, $year, $month);
        foreach ($dateArr['day_array'] as $dateVal) {
            $yyyymmdd = $this->getArrValueByKey('date', $dateVal);
            if (sprintf('%02d', $day) == substr($yyyymmdd, -2)) { return $dateVal; }
        }
    }

    /**
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $flgArr
     * @return array
     */
    function getNurseHomeDataOfMonth($facilityId=null, $year=null, $month=null, $flgArr=null)
    {
        $rtnData   = array();
        $firstData = $this->getNurseHomeData($facilityId, $year, $month, null, $flgArr);
        foreach ($firstData as $secondKey => $secondData) {
            $secondData['day_cnt'] = 1;
            $facilityId   = $secondData['jnl_facility_id'];
            $targetYear   = $secondData['year'];
            $targetMonth  = $secondData['month'];
            $tmpData      = array();
            $duplicateKey = null;

            foreach ($rtnData as $rtnKey => $rtnVal) {
                if ($facilityId  == $rtnVal['jnl_facility_id'] && $targetYear  == $rtnVal['year'] && $targetMonth == $rtnVal['month']) {
                    $tmpData      = $rtnVal;
                    $duplicateKey = $rtnKey;
                }
            }

            foreach ($secondData as $thirdKey => $thirdData) {
                switch ($thirdKey) {
                    default:
                        $tmpData[$thirdKey] = $tmpData[$thirdKey] + $thirdData;
                        break;

                    case 'jnl_facility_id': case 'year': case 'month':
                        $tmpData[$thirdKey] = $thirdData;
                        break;

                    case 'jnl_status':
                        if (!is_null($tmpData[$thirdKey])) { $tmpData[$thirdKey] = ($tmpData[$thirdKey] < $thirdData)? $tmpData[$thirdKey] : $thirdData; }
                        else { $tmpData[$thirdKey] = $thirdData; }
                        break;

                    case 'newest_apply_id': case 'regist_date':
                        break;
                }
            }

            if ($duplicateKey === null) { $rtnData[] = $tmpData; }
            else { $rtnData[$duplicateKey] = $tmpData; }
        }

        foreach ($rtnData as $rtnKey => $rtnVal) {
            $rtnData[$rtnKey]['daily_limit_avrg'] = round($rtnVal['daily_limit'] / $rtnVal['day_cnt'], 1);
            $rtnData[$rtnKey]['enter_limit_avrg'] = round($rtnVal['enter_limit'] / $rtnVal['day_cnt'], 1);
        }

        return $rtnData;
    }

	/** 施設別患者日報(累積統計表)のデータ取得
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $day
     * @return array
     */
    function getNurseHomeDataOfStats($facilityId=null, $year=null, $month=null, $day=null)
    {
        $rtnData   = array();
        $firstData = $this->getNurseHomeData($facilityId, $year, $month);

        foreach ($firstData as $secondKey => $secondData) {
            if ($day >= $secondData['day']) {
                $secondData['day_cnt'] = 1;
                $facilityId   = $secondData['jnl_facility_id'];
                $targetYear   = $secondData['year'];
                $targetMonth  = $secondData['month'];
                $tmpData      = array();
                $duplicateKey = null;

                foreach ($rtnData as $rtnKey => $rtnVal) {
                    if ($facilityId  == $rtnVal['jnl_facility_id'] && $targetYear == $rtnVal['year'] && $targetMonth == $rtnVal['month']) {
                        $tmpData      = $rtnVal;
                        $duplicateKey = $rtnKey;
                    }
                }

                foreach ($secondData as $thirdKey => $thirdData) {
                    switch ($thirdKey) {
                        default:
                            $tmpData['sum_'.$thirdKey] = $tmpData['sum_'.$thirdKey] + $thirdData;
                            if ($day == $secondData['day']) { $tmpData[$thirdKey] = $thirdData; }
                            break;

                        case 'day':
                            if ($thirdData == $day) { $tmpData[$thirdKey] = $thirdData; }
                            else if ($thirdData == $day-1) { if ($tmpData[$thirdKey] < $day) { $tmpData[$thirdKey] = $thirdData; } }
                            break;

                        case 'jnl_facility_id': case 'year': case 'month':
                            $tmpData[$thirdKey] = $thirdData;
                            break;

                        case 'jnl_status':
                            if (!is_null($tmpData['sum_'.$thirdKey])) { $tmpData['sum_'.$thirdKey] = ($tmpData[$thirdKey] < $thirdData)? $tmpData[$thirdKey] : $thirdData; }
                            else { $tmpData['sum_'.$thirdKey] = $thirdData; }
                            if ($day == $secondData['day']) { $tmpData[$thirdKey] = $thirdData; }
                            else if ( ($day-1) == $secondData['day'] && !$tmpData['day'] ) { $tmpData[$thirdKey] = $thirdData; }
                            break;

                        case 'day_cnt':
                            if (empty($tmpData[$thirdKey])) { $tmpData[$thirdKey] = 1; }
                            else { $tmpData[$thirdKey] = $tmpData[$thirdKey] + $thirdData; }
                            break;

                        case 'day_type':
                            switch ($thirdData) {
								case 'weekday':case 'holiday'://施設別患者日報(累積統計表)では祝日は平日としてカウントする
                                    $tmpData['weekday_cnt'] = $tmpData['weekday_cnt'] + 1;
                                    break;

                                case 'sat':
                                    $tmpData['saturday_cnt'] = $tmpData['saturday_cnt'] + 1;
                                    break;

                                case 'sun': 
                                    $tmpData['holiday_cnt'] = $tmpData['holiday_cnt'] + 1;
                                    break;
                            }
                            break;

                        case 'newest_apply_id': case 'regist_date':
                            break;
                    }
                }

                if ($duplicateKey === null) { $rtnData[] = $tmpData; }
                else { $rtnData[$duplicateKey] = $tmpData; }
            }
        }

        foreach ($rtnData as $rtnKey => $rtnVal) {
            $rtnData[$rtnKey]['daily_limit_avrg'] = $rtnVal['daily_limit'] / $rtnVal['day_cnt'];
            $rtnData[$rtnKey]['enter_limit_avrg'] = $rtnVal['enter_limit'] / $rtnVal['day_cnt'];
        }

        return $rtnData;
    }

    /**
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $day
     * @param  array  $flgArr
     * @return array
     */
    function getNurseHomeData($facilityId=null, $year=null, $month=null, $day=null, $flgArr=null)
    {
        $rtnArr = array();

        $visitFlg = false;
        $enterFlg = false;
        $shortFlg = false;
        $rehabFlg = false;
        $otherFlg = false;

        if (is_array($flgArr)) {
            $visitFlg = $this->getArrValueByKey('visit', $flgArr);
            $enterFlg = $this->getArrValueByKey('enter', $flgArr);
            $shortFlg = $this->getArrValueByKey('short', $flgArr);
            $rehabFlg = $this->getArrValueByKey('rehab', $flgArr);
            $otherFlg = $this->getArrValueByKey('other', $flgArr);
        }
        else if (is_null($flgArr)) {
            $visitFlg = true;
            $enterFlg = true;
            $shortFlg = true;
            $rehabFlg = true;
            $otherFlg = true;
        }

        if ($visitFlg===true) {
            $visit = $this->getNurseHomeDataVisit($facilityId, $year, $month, $day);
            $rtnArr = $this->arrayMerge($visit, $rtnArr);
        }
        if ($enterFlg===true) {
            $enter = $this->getNurseHomeDataEnter($facilityId, $year, $month, $day);
            $rtnArr = $this->arrayMerge($enter, $rtnArr);
        }
        if ($shortFlg===true) {
            $short = $this->getNurseHomeDataShort($facilityId, $year, $month, $day);
            $rtnArr = $this->arrayMerge($short, $rtnArr);
        }
        if ($rehabFlg===true) {
            $rehab = $this->getNurseHomeDataRehab($facilityId, $year, $month, $day);
            $rtnArr = $this->arrayMerge($rehab, $rtnArr);
        }
        if ($otherFlg===true) {
            $other = $this->getNurseHomeDataOther($facilityId, $year, $month, $day);
            $rtnArr = $this->arrayMerge($other, $rtnArr);
        }

        foreach ($rtnArr as $rtnKey => $rtnVal) {
            $data = $this->getComplementOfDay($rtnVal['jnl_facility_id'], $rtnVal['year'], $rtnVal['month'], $rtnVal['day']);

            $rtnArr[$rtnKey]['daily_limit']    = $data['daily_limit'];
            $rtnArr[$rtnKey]['weekday_limit']  = $data['weekday_limit'];
            $rtnArr[$rtnKey]['saturday_limit'] = $data['saturday_limit'];
            $rtnArr[$rtnKey]['holiday_limit']  = $data['holiday_limit'];
            $rtnArr[$rtnKey]['enter_limit']    = $data['enter_limit'];
            $rtnArr[$rtnKey]['day_type']       = $data['day_type'];
        }

        return $rtnArr;
    }

    /**
     * 1潤ｵ53
     * @access private
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $day
     * @return array
     */
    function getNurseHomeDataVisit($facilityId=null, $year=null, $month=null, $day=null)
    {
        $sql = "SELECT main.newest_apply_id, main.jnl_facility_id, main.jnl_status, main.regist_date, SUBSTR(main.regist_date, 0, 5) AS year"
                   .", SUBSTR(main.regist_date, 5, 2) AS month, SUBSTR(main.regist_date, 7, 2) AS day, a.dispNum_1, a.dispNum_2, a.dispNum_3, a.dispNum_4"
                   .", dispNum_5, a.dispNum_6, a.dispNum_7, b.dispNum_8, b.dispNum_9, b.dispNum_10, b.dispNum_11, b.dispNum_12, b.dispNum_13, b.dispNum_14"
                   .", c.dispNum_15, c.dispNum_16, c.dispNum_17, c.dispNum_18, c.dispNum_19, c.dispNum_20, c.dispNum_21, d.dispNum_22, d.dispNum_23"
                   .", d.dispNum_24, d.dispNum_25, d.dispNum_26, d.dispNum_27, d.dispNum_28, e.dispNum_29, e.dispNum_30, e.dispNum_31, e.dispNum_32"
                   .", e.dispNum_33, e.dispNum_34, e.dispNum_35, f.dispNum_36, f.dispNum_37, f.dispNum_38, f.dispNum_39, f.dispNum_40, f.dispNum_41"
                   .", f.dispNum_42, g.dispNum_43, g.dispNum_44, g.dispNum_45, g.dispNum_46, g.dispNum_47, g.dispNum_48, g.dispNum_49, h.dispNum_50"
                   .", h.dispNum_51, h.dispNum_52, h.dispNum_53"
               ." FROM jnl_nurse_home_main main"
              ." INNER JOIN (SELECT a.newest_apply_id, a.care_five AS dispNum_1, a.care_four AS dispNum_2, a.care_three AS dispNum_3"
                                .", a.care_two AS dispNum_4, a.care_one AS dispNum_5, a.care_etc AS dispNum_6, a.dispNum_7"
                            ." FROM (SELECT newest_apply_id, time_area, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_7"
                                    ." FROM jnl_nurse_home_day_visit WHERE time_area = '1' AND del_flg = 'f') a ) AS a"
                      ." ON main.newest_apply_id = a.newest_apply_id"
              ." INNER JOIN (SELECT b.newest_apply_id, b.care_five AS dispNum_8, b.care_four AS dispNum_9, b.care_three AS dispNum_10"
                                .", b.care_two AS dispNum_11, b.care_one AS dispNum_12, b.care_etc AS dispNum_13, b.dispNum_14"
                            ." FROM (SELECT newest_apply_id, time_area, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_14"
                                    ." FROM jnl_nurse_home_day_visit WHERE time_area = '2' AND del_flg = 'f') b ) AS b"
                      ." ON main.newest_apply_id = b.newest_apply_id"
              ." INNER JOIN (SELECT c.newest_apply_id, c.care_five AS dispNum_15, c.care_four AS dispNum_16, c.care_three AS dispNum_17"
                                .", c.care_two AS dispNum_18, c.care_one AS dispNum_19, c.care_etc AS dispNum_20, c.dispNum_21"
                            ." FROM (SELECT newest_apply_id, time_area, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_21"
                                    ." FROM jnl_nurse_home_day_visit"
                                   ." WHERE time_area = '3' AND del_flg = 'f') c ) AS c"
                      ." ON main.newest_apply_id = c.newest_apply_id"
              ." INNER JOIN (SELECT d.newest_apply_id, d.care_five AS dispNum_22, d.care_four AS dispNum_23, d.care_three AS dispNum_24"
                                .", d.care_two AS dispNum_25, d.care_one AS dispNum_26, d.care_etc AS dispNum_27, d.dispNum_28"
                            ." FROM (SELECT newest_apply_id, time_area, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_28"
                                    ." FROM jnl_nurse_home_day_visit"
                                   ." WHERE time_area = '4' AND del_flg = 'f') d ) AS d"
                      ." ON main.newest_apply_id = d.newest_apply_id"
              ." INNER JOIN (SELECT e.newest_apply_id, e.care_five AS dispNum_29, e.care_four AS dispNum_30, e.care_three AS dispNum_31"
                                .", e.care_two AS dispNum_32, e.care_one AS dispNum_33, e.care_etc AS dispNum_34, e.dispNum_35"
                            ." FROM (SELECT newest_apply_id, time_area, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_35"
                                    ." FROM jnl_nurse_home_day_visit WHERE time_area = '5' AND del_flg = 'f') e ) AS e"
                      ." ON main.newest_apply_id = e.newest_apply_id"
              ." INNER JOIN (SELECT f.newest_apply_id, f.care_five AS dispNum_36, f.care_four AS dispNum_37, f.care_three AS dispNum_38"
                                .", f.care_two AS dispNum_39, f.care_one AS dispNum_40, f.care_etc AS dispNum_41, f.dispNum_42"
                            ." FROM (SELECT newest_apply_id, time_area, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_42"
                                    ." FROM jnl_nurse_home_day_visit WHERE time_area = '6' AND del_flg = 'f') f ) AS f"
                      ." ON main.newest_apply_id = f.newest_apply_id"
              ." INNER JOIN (SELECT g.newest_apply_id, g.care_five AS dispNum_43, g.care_four AS dispNum_44, g.care_three AS dispNum_45"
                                .", g.care_two AS dispNum_46, g.care_one AS dispNum_47, g.care_etc AS dispNum_48, g.dispNum_49"
                            ." FROM (SELECT newest_apply_id, time_area, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_49"
                                    ." FROM jnl_nurse_home_day_visit WHERE time_area = '7' AND del_flg = 'f') g ) AS g"
                      ." ON main.newest_apply_id = g.newest_apply_id"
              ." INNER JOIN (SELECT h.newest_apply_id, h.support_two AS dispNum_50, h.support_one AS dispNum_51"
                                .", h.support_etc AS dispNum_52, h.dispNum_53"
                            ." FROM (SELECT newest_apply_id, support_two, support_one, support_etc, del_flg"
                                        .", (COALESCE(support_two, 0) + COALESCE(support_one, 0) + COALESCE(support_etc, 0)) AS dispNum_53"
                                    ." FROM jnl_nurse_home_day_visit_prevention WHERE del_flg = 'f') h ) h"
                      ." ON main.newest_apply_id = h.newest_apply_id"
              ." WHERE main.del_flg = 'f' AND main.jnl_status < 2 AND main.jnl_report_flg = 1";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND main.jnl_facility_id = '%d'", $facilityId);        }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(main.regist_date, 0, 5) = '%04d'", $year);  }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(main.regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($day))        { $sql .= sprintf(" AND SUBSTR(main.regist_date, 7, 2) = '%02d'", $day);   }
        return $this->getDateList($sql);
    }

    /**
     * 54潤ｵ67
     * @access private
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $day
     * @return array
     */
    function getNurseHomeDataEnter($facilityId=null, $year=null, $month=null, $day=null)
    {
        $sql = "SELECT main.newest_apply_id, main.jnl_facility_id, main.jnl_status, main.regist_date, SUBSTR(main.regist_date, 0, 5) AS year"
                   .", SUBSTR(main.regist_date, 5, 2) AS month, SUBSTR(main.regist_date, 7, 2) AS day, i.dispNum_54, i.dispNum_55, i.dispNum_56"
                   .", i.dispNum_57, i.dispNum_58, i.dispNum_59, i.dispNum_60, j.dispNum_61, j.dispNum_62, j.dispNum_63, j.dispNum_64, j.dispNum_65"
                   .", j.dispNum_66, j.dispNum_67"
               ." FROM jnl_nurse_home_main main"
              ." INNER JOIN (SELECT i.newest_apply_id, i.care_five AS dispNum_54, i.care_four AS dispNum_55, i.care_three AS dispNum_56"
                                .", i.care_two AS dispNum_57, i.care_one AS dispNum_58, i.care_etc AS dispNum_59, i.dispNum_60"
                            ." FROM (SELECT newest_apply_id, enter_level, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_60"
                                    ." FROM jnl_nurse_home_day_enter WHERE enter_level = '1') i ) i"
                      ." ON main.newest_apply_id = i.newest_apply_id"
              ." INNER JOIN (SELECT j.newest_apply_id, j.care_five AS dispNum_61, j.care_four AS dispNum_62, j.care_three AS dispNum_63"
                                .", j.care_two AS dispNum_64, j.care_one AS dispNum_65, j.care_etc AS dispNum_66, j.dispNum_67"
                            ." FROM (SELECT newest_apply_id, enter_level, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_67"
                                    ." FROM jnl_nurse_home_day_enter WHERE enter_level = '2') j ) j"
                      ." ON main.newest_apply_id = j.newest_apply_id "
              ." WHERE main.del_flg = 'f' AND main.jnl_status < 2 AND main.jnl_report_flg = 1";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND main.jnl_facility_id = '%d'", $facilityId);        }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(main.regist_date, 0, 5) = '%04d'", $year);  }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(main.regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($day))        { $sql .= sprintf(" AND SUBSTR(main.regist_date, 7, 2) = '%02d'", $day);   }
        return $this->getDateList($sql);
    }

    /**
     * 68潤ｵ89
     * @access private
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $day
     * @return array
     */
    function getNurseHomeDataShort($facilityId=null, $year=null, $month=null, $day=null)
    {
        $sql = "SELECT main.newest_apply_id, main.jnl_facility_id, main.jnl_status, main.regist_date, SUBSTR(main.regist_date, 0, 5) AS year"
                   .", SUBSTR(main.regist_date, 5, 2) AS month, SUBSTR(main.regist_date, 7, 2) AS day, k.dispNum_68, k.dispNum_69, k.dispNum_70, k.dispNum_71"
                   .", k.dispNum_72, k.dispNum_73, k.dispNum_74, l.dispNum_75, l.dispNum_76, l.dispNum_77, l.dispNum_78, l.dispNum_79, l.dispNum_80"
                   .", l.dispNum_81, m.dispNum_82, m.dispNum_83, m.dispNum_84, m.dispNum_85, o.dispNum_86, o.dispNum_87, o.dispNum_88, o.dispNum_89"
               ." FROM jnl_nurse_home_main main"
              ." INNER JOIN (SELECT k.newest_apply_id, k.care_five AS dispNum_68, k.care_four AS dispNum_69, k.care_three AS dispNum_70"
                                .", k.care_two AS dispNum_71, k.care_one AS dispNum_72, k.care_etc AS dispNum_73, k.dispNum_74"
                            ." FROM (SELECT newest_apply_id, short_level, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_74"
                                    ." FROM jnl_nurse_home_day_short WHERE short_level = '1' and del_flg = 'f') k ) k"
                      ." ON main.newest_apply_id = k.newest_apply_id"
              ." INNER JOIN (SELECT l.newest_apply_id, l.care_five AS dispNum_75, l.care_four AS dispNum_76, l.care_three  AS dispNum_77"
                                .", l.care_two AS dispNum_78, l.care_one AS dispNum_79, l.care_etc AS dispNum_80, l.dispNum_81"
                            ." FROM (SELECT newest_apply_id, short_level, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_81"
                                    ." FROM jnl_nurse_home_day_short WHERE short_level = '2' and del_flg = 'f') l ) l"
                      ." ON main.newest_apply_id = l.newest_apply_id"
              ." INNER JOIN (SELECT m.newest_apply_id, m.support_two AS dispNum_82, m.support_one AS dispNum_83, m.support_etc AS dispNum_84, m.dispNum_85"
                            ." FROM (SELECT newest_apply_id, prevention_level, support_two, support_one, support_etc, del_flg"
                                        .", (COALESCE(support_two, 0) + COALESCE(support_one, 0) + COALESCE(support_etc, 0)) AS dispNum_85"
                                    ." FROM jnl_nurse_home_day_short_prevention WHERE prevention_level = '1' and del_flg = 'f') m ) m"
                      ." ON main.newest_apply_id = m.newest_apply_id"
              ." INNER JOIN (SELECT o.newest_apply_id, o.support_two AS dispNum_86, o.support_one AS dispNum_87, o.support_etc AS dispNum_88, o.dispNum_89"
                            ." FROM (SELECT newest_apply_id, prevention_level, support_two, support_one, support_etc, del_flg"
                                        .", (COALESCE(support_two, 0) + COALESCE(support_one, 0) + COALESCE(support_etc, 0)) AS dispNum_89"
                                    ." FROM jnl_nurse_home_day_short_prevention WHERE prevention_level = '2' and del_flg = 'f') o ) o"
                      ." ON main.newest_apply_id = o.newest_apply_id "
              ." WHERE main.del_flg = 'f' AND main.jnl_status < 2 AND main.jnl_report_flg = 1";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND main.jnl_facility_id = '%d'", $facilityId);        }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(main.regist_date, 0, 5) = '%04d'", $year);  }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(main.regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($day))        { $sql .= sprintf(" AND SUBSTR(main.regist_date, 7, 2) = '%02d'", $day);   }
        return $this->getDateList($sql);
    }

    /**
     * 90潤ｵ100
     * @access private
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $day
     * @return array
     */
    function getNurseHomeDataRehab($facilityId=null, $year=null, $month=null, $day=null)
    {
        $sql = "SELECT main.newest_apply_id, main.jnl_facility_id, main.jnl_status, main.regist_date, SUBSTR(main.regist_date, 0, 5) AS year"
                   .", SUBSTR(main.regist_date, 5, 2) AS month, SUBSTR(main.regist_date, 7, 2) AS day, p.dispNum_90, p.dispNum_91, p.dispNum_92, p.dispNum_93"
                   .", p.dispNum_94, p.dispNum_95, p.dispNum_96, q.dispNum_97, q.dispNum_98, q.dispNum_99, q.dispNum_100"
               ." FROM jnl_nurse_home_main main"
              ." INNER JOIN (SELECT p.newest_apply_id, p.care_five AS dispNum_90, p.care_four AS dispNum_91, p.care_three AS dispNum_92"
                                .", p.care_two AS dispNum_93, p.care_one AS dispNum_94, p.care_etc AS dispNum_95, p.dispNum_96"
                            ." FROM (SELECT newest_apply_id, care_five, care_four, care_three, care_two, care_one, care_etc, del_flg"
                                        .", (COALESCE(care_five, 0) + COALESCE(care_four, 0) + COALESCE(care_three, 0) + COALESCE(care_two, 0) + COALESCE(care_one, 0) + COALESCE(care_etc, 0)) AS dispNum_96"
                                    ." FROM jnl_nurse_home_day_rehab WHERE del_flg = 'f') p ) p"
                      ." ON main.newest_apply_id = p.newest_apply_id"
              ." INNER JOIN (SELECT q.newest_apply_id, q.support_two AS dispNum_97, q.support_one AS dispNum_98, q.support_etc AS dispNum_99, q.dispNum_100"
                            ." FROM (SELECT newest_apply_id, support_two, support_one, support_etc, del_flg"
                                        .", (COALESCE(support_two, 0) + COALESCE(support_one, 0) + COALESCE(support_etc, 0)) AS dispNum_100"
                                    ." FROM jnl_nurse_home_day_rehab_prevention WHERE del_flg = 'f') q ) q"
                      ." ON main.newest_apply_id = q.newest_apply_id "
              ." WHERE main.del_flg = 'f' AND main.jnl_status < 2 AND main.jnl_report_flg = 1";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND main.jnl_facility_id = '%d'", $facilityId);        }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(main.regist_date, 0, 5) = '%04d'", $year);  }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(main.regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($day))        { $sql .= sprintf(" AND SUBSTR(main.regist_date, 7, 2) = '%02d'", $day);   }
        return $this->getDateList($sql);
    }

    /**
     * 101潤ｵ108
     * @access private
     * @param  string $facilityId
     * @param  string $year
     * @param  string $month
     * @param  string $day
     * @return array
     */
    function getNurseHomeDataOther($facilityId=null, $year=null, $month=null, $day=null)
    {
        $sql = "SELECT main.newest_apply_id, main.jnl_facility_id, main.jnl_status, main.regist_date, SUBSTR(main.regist_date, 0, 5) AS year"
                   .", SUBSTR(main.regist_date, 5, 2) AS month, SUBSTR(main.regist_date, 7, 2) AS day, r.dispNum_101, r.dispNum_102, r.dispNum_103"
                   .", r.dispNum_104, r.dispNum_105, r.dispNum_106, r.dispNum_107, r.dispNum_108"
               ." FROM jnl_nurse_home_main main"
              ." INNER JOIN (SELECT r.newest_apply_id, r.cnt_in AS dispNum_101, r.cnt_out AS dispNum_102, r.out_return AS dispNum_103, r.short_in AS dispNum_104"
                                .", r.short_out AS dispNum_105, r.judge_ok AS dispNum_106, r.judge_ng AS dispNum_107, r.cnt_visit AS dispNum_108"
                            ." FROM (SELECT newest_apply_id, cnt_in, cnt_out, out_return, short_in, short_out, judge_ok, judge_ng, cnt_visit, del_flg"
                                    ." FROM jnl_nurse_home_day_cnt WHERE del_flg = 'f' ) r ) r"
                      ." ON main.newest_apply_id = r.newest_apply_id"
              ." WHERE main.del_flg = 'f' AND main.jnl_status < 2 AND main.jnl_report_flg = 1";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND main.jnl_facility_id = '%d'", $facilityId);        }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(main.regist_date, 0, 5) = '%04d'", $year);  }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(main.regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($day))        { $sql .= sprintf(" AND SUBSTR(main.regist_date, 7, 2) = '%02d'", $day);   }
        return $this->getDateList($sql);
    }
}


/**
 *
 */
class JnlNurseHomeMonth extends IndicatorData
{
    /**
     * コンストラクタ
     * @param  array $arr
     * @return void
     */
    function JnlNurseHomeMonth($arr)
    {
        parent::IndicatorData($arr);
    }

    /**
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getNurseHomeMonth($jnlFaclityId=null, $year=null, $month=null)
    {
        $rtnArr = array();

        $enter      = $this->getNurseHomeMonthEnter($jnlFaclityId,      $year, $month);
        $short      = $this->getNurseHomeMonthShort($jnlFaclityId,      $year, $month);
        $commute    = $this->getNurseHomeMonthCommute($jnlFaclityId,    $year, $month);
        $commutePre = $this->getNurseHomeMonthCommutePre($jnlFaclityId, $year, $month);
        $visit      = $this->getNurseHomeMonthVisit($jnlFaclityId,      $year, $month);
        $visitPre   = $this->getNurseHomeMonthVisitPre($jnlFaclityId,   $year, $month);

        $rtnArr = $this->arrayMerge($enter,      $rtnArr);
        $rtnArr = $this->arrayMerge($short,      $rtnArr);
        $rtnArr = $this->arrayMerge($commute,    $rtnArr);
        $rtnArr = $this->arrayMerge($commutePre, $rtnArr);
        $rtnArr = $this->arrayMerge($visit,      $rtnArr);
        $rtnArr = $this->arrayMerge($visitPre,   $rtnArr);

        return $rtnArr;
    }

    /**
     * @param  string $jnlFaclityId
     * @param  string $year
     * @return array
     */
    function getNurseHomeMonthForFiscalYear($jnlFaclityId=null, $year=null)
    {
        if (is_null($year)) { return false; }
        $currentYearData = $this->getNurseHomeMonth($jnlFaclityId, $year);
        $nextYearData    = $this->getNurseHomeMonth($jnlFaclityId, $year+1);
        $rtnArr          = array();
        if (is_array($currentYearData)) { foreach ($currentYearData as $currentYearDataVal) { $rtnArr[] = $currentYearDataVal; } }
        if (is_array($nextYearData))    { foreach ($nextYearData as $nextYearDataVal)       { $rtnArr[] = $nextYearDataVal; } }
        return $rtnArr;
    }

    /**
     * 老健月報・入所
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getNurseHomeMonthEnter($jnlFaclityId=null, $year=null, $month=null)
    {
        $sql = "SELECT jnhm.jnl_facility_id, jnhm.jnl_status, jnhm.regist_date, jnhm.year, jnhm.month, jnhme.newest_apply_id, jnhme.dispnum_109"
                   .", jnhme.dispnum_110, jnhme.dispnum_111, jnhme.dispnum_112, jnhme.dispnum_113, jnhme.dispnum_114, jnhme.dispnum_115, jnhme.dispnum_116"
                   .", jnhme.dispnum_117, jnhme.dispnum_118, jnhme.dispnum_119, jnhme.dispnum_120, jnhme.dispnum_121, jnhme.dispnum_122, jnhme.dispnum_123"
                   .", jnhme.dispnum_124, jnhme.dispnum_125, jnhme.dispnum_126, jnhme.dispnum_127, jnhme.dispnum_128, jnhme.dispnum_129, jnhme.dispnum_130"
                   .", jnhme.dispnum_131, jnhme.dispnum_132, jnhme.dispnum_133, jnhme.dispnum_134, jnhme.dispnum_135, jnhme.dispnum_136, jnhme.dispnum_137"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, jnl_report_flg, regist_date, del_flg, SUBSTR(regist_date, 0, 5) AS year"
                           .", SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_nurse_home_main WHERE jnl_report_flg = 2 AND del_flg = 'f'"
                     .") AS jnhm"
              ." INNER JOIN (SELECT newest_apply_id, night_assigned AS dispnum_109, short_term_rehab AS dispnum_110, sterm_dementia AS dispnum_111"
                                .", care_dementia AS dispnum_112, young_dementia AS dispnum_113, terminal_thirty AS dispnum_114"
                                .", terminal_thirty_one AS dispnum_115, keep_treat AS dispnum_116, init_add AS dispnum_117, out_preguidance  AS dispnum_118"
                                .", out_guidance AS dispnum_119, out_information AS dispnum_120, out_presession AS dispnum_121, visit_care AS dispnum_122"
                                .", management_nutrition AS dispnum_123, move_oral AS dispnum_124, keep_oral_one AS dispnum_125, keep_oral_two AS dispnum_126"
                                .", keep_mouth_func AS dispnum_127, treat_food AS dispnum_128, support_return_home_one AS dispnum_129"
                                .", support_return_home_two AS dispnum_130, urgent_treat AS dispnum_131, care_dementia_one AS dispnum_132"
                                .", care_dementia_two AS dispnum_133, dementia_information AS dispnum_134, service_one AS dispnum_135"
                                .", service_two AS dispnum_136, service_three AS dispnum_137"
                            ." FROM jnl_nurse_home_mon_enter WHERE del_flg = 'f'"
                          .") AS jnhme"
                      ." ON jnhm.newest_apply_id = jnhme.newest_apply_id"
              ." WHERE del_flg = 'f'";

        if (!is_null($jnlFaclityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $jnlFaclityId); }
        if (!is_null($year))         { $sql .= sprintf(" AND year = '%04d'", $year); }
        if (!is_null($month))        { $sql .= sprintf(" AND month = '%02d'", $month); }

        return $this->getDateList($sql);
    }

    /**
     * 老健月報・ショート(介護・予防含む)
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getNurseHomeMonthShort($jnlFaclityId=null, $year=null, $month=null)
    {
        $sql = "SELECT jnhm.jnl_facility_id, jnhm.jnl_status, jnhm.regist_date, jnhm.year, jnhm.month, jnhms.newest_apply_id, jnhms.dispnum_138"
                   .", jnhms.dispnum_139, jnhms.dispnum_140, jnhms.dispnum_141, jnhms.dispnum_142, jnhms.dispnum_143, jnhms.dispnum_144, jnhms.dispnum_145"
                   .", jnhms.dispnum_146, jnhms.dispnum_147, jnhms.dispnum_148, jnhms.dispnum_149, jnhms.dispnum_150, jnhms.dispnum_151"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, jnl_report_flg, regist_date, del_flg, SUBSTR(regist_date, 0, 5) AS year"
                           .", SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_nurse_home_main WHERE jnl_report_flg = 2 AND del_flg = 'f'"
                     .") AS jnhm"
              ." INNER JOIN (SELECT newest_apply_id, night_assigned AS dispnum_138, enforce_rehab AS dispnum_139, personal_rehab AS dispnum_140"
                                .", care_dementia AS dispnum_141, urgent_support_dementia AS dispnum_142, young_dementia AS dispnum_143"
                                .", send_return AS dispnum_144, keep_treat AS dispnum_145, treat_food AS dispnum_146, urgent_network AS dispnum_147"
                                .", urgent_treat AS dispnum_148, service_one AS dispnum_149, service_two AS dispnum_150, service_three AS dispnum_151"
                            ." FROM jnl_nurse_home_mon_short WHERE del_flg = 'f'"
                          .") AS jnhms"
                      ." ON jnhm.newest_apply_id = jnhms.newest_apply_id"
              ." WHERE del_flg = 'f'";

        if (!is_null($jnlFaclityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $jnlFaclityId); }
        if (!is_null($year))         { $sql .= sprintf(" AND year = '%04d'", $year); }
        if (!is_null($month))        { $sql .= sprintf(" AND month = '%02d'", $month); }

        return $this->getDateList($sql);
    }

    /**
     * 老健月報・通所リハビリテーション
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getNurseHomeMonthCommute($jnlFaclityId=null, $year=null, $month=null)
    {
        $sql = "SELECT jnhm.jnl_facility_id, jnhm.jnl_status, jnhm.regist_date, jnhm.year, jnhm.month, jnhmc.newest_apply_id, jnhmc.dispnum_152"
                   .", jnhmc.dispnum_153, jnhmc.dispnum_154, jnhmc.dispnum_155, jnhmc.dispnum_156, jnhmc.dispnum_157, jnhmc.dispnum_158, jnhmc.dispnum_159"
                   .", jnhmc.dispnum_160, jnhmc.dispnum_161, jnhmc.dispnum_162, jnhmc.dispnum_163, jnhmc.dispnum_164, jnhmc.dispnum_165"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, jnl_report_flg, regist_date, del_flg, SUBSTR(regist_date, 0, 5) AS year"
                           .", SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_nurse_home_main WHERE jnl_report_flg = 2 AND del_flg = 'f'"
                     .") AS jnhm"
              ." INNER JOIN (SELECT newest_apply_id, serial_care_a AS dispnum_152, serial_care_b AS dispnum_153, bathe_care AS dispnum_154"
                                .", plan_rehab AS dispnum_155, management_rehab AS dispnum_156, sterm_rehab_one AS dispnum_157, sterm_rehab_three AS dispnum_158"
                                .", personal_rehab AS dispnum_159, sterm_dementia AS dispnum_160, young_dementia AS dispnum_161, improve_food AS dispnum_162"
                                .", keep_mouth_func AS dispnum_163, service_one AS dispnum_164, service_two AS dispnum_165"
                            ." FROM jnl_nurse_home_mon_commute WHERE del_flg = 'f'"
                          .") AS jnhmc"
                      ." ON jnhm.newest_apply_id = jnhmc.newest_apply_id"
              ." WHERE del_flg = 'f'";

        if (!is_null($jnlFaclityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $jnlFaclityId); }
        if (!is_null($year))         { $sql .= sprintf(" AND year = '%04d'", $year); }
        if (!is_null($month))        { $sql .= sprintf(" AND month = '%02d'", $month); }

        return $this->getDateList($sql);
    }

    /**
     * 老健月報・介護予防通所リハビリテーション
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getNurseHomeMonthCommutePre($jnlFaclityId=null, $year=null, $month=null)
    {
        $sql = "SELECT jnhm.jnl_facility_id, jnhm.jnl_status, jnhm.regist_date, jnhm.year, jnhm.month, jnhmcp.newest_apply_id, jnhmcp.dispnum_166, jnhmcp.dispnum_167"
                   .", jnhmcp.dispnum_168, jnhmcp.dispnum_169, jnhmcp.dispnum_170, jnhmcp.dispnum_171, jnhmcp.dispnum_172, jnhmcp.dispnum_173, jnhmcp.dispnum_174"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, jnl_report_flg, regist_date, del_flg, SUBSTR(regist_date, 0, 5) AS year"
                           .", SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_nurse_home_main WHERE jnl_report_flg = 2 AND del_flg = 'f'"
                     .") AS jnhm"
              ." INNER JOIN (SELECT newest_apply_id, young_dementia AS dispnum_166, improve_motion_func AS dispnum_167, improve_food AS dispnum_168"
                                .", improve_mouth_func AS dispnum_169, value_office AS dispnum_170, service_one_one AS dispnum_171"
                                .", service_one_two AS dispnum_172, service_two_one AS dispnum_173, service_two_two AS dispnum_174"
                            ." FROM jnl_nurse_home_mon_commute_pre WHERE del_flg = 'f'"
                          .") AS jnhmcp"
                      ." ON jnhm.newest_apply_id = jnhmcp.newest_apply_id"
              ." WHERE del_flg = 'f'";

        if (!is_null($jnlFaclityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $jnlFaclityId); }
        if (!is_null($year))         { $sql .= sprintf(" AND year = '%04d'", $year); }
        if (!is_null($month))        { $sql .= sprintf(" AND month = '%02d'", $month); }

        return $this->getDateList($sql);
    }

    /**
     * 老健月報・訪問リハビリテーション
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getNurseHomeMonthVisit($jnlFaclityId=null, $year=null, $month=null)
    {
        $sql = "SELECT jnhm.jnl_facility_id, jnhm.jnl_status, jnhm.regist_date, jnhm.year, jnhm.month, jnhmv.newest_apply_id, jnhmv.dispnum_175"
                   .", jnhmv.dispnum_176, jnhmv.dispnum_177"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, jnl_report_flg, regist_date, del_flg, SUBSTR(regist_date, 0, 5) AS year"
                           .", SUBSTR(regist_date, 5, 2) AS month"
                      ." FROM jnl_nurse_home_main WHERE jnl_report_flg = 2 AND del_flg = 'f'"
                     .") AS jnhm"
              ." INNER JOIN (SELECT newest_apply_id, short_term_rehab_one AS dispnum_175, short_term_rehab_three AS dispnum_176, service AS dispnum_177"
                            ." FROM jnl_nurse_home_mon_visit WHERE del_flg = 'f'"
                          .") AS jnhmv"
                      ." ON jnhm.newest_apply_id = jnhmv.newest_apply_id"
              ." WHERE del_flg = 'f'";

        if (!is_null($jnlFaclityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $jnlFaclityId); }
        if (!is_null($year))         { $sql .= sprintf(" AND year = '%04d'", $year); }
        if (!is_null($month))        { $sql .= sprintf(" AND month = '%02d'", $month); }

        return $this->getDateList($sql);
    }

    /**
     * 老健月報・介護予防訪問リハビリテーション
     * @param  string $jnlFaclityId
     * @param  string $year
     * @param  string $month
     * @return array
     */
    function getNurseHomeMonthVisitPre($jnlFaclityId=null, $year=null, $month=null)
    {
        $sql = "SELECT jnhm.jnl_facility_id, jnhm.jnl_status, jnhm.regist_date, jnhm.year, jnhm.month, jnhmvp.newest_apply_id, jnhmvp.dispnum_178"
                   .", jnhmvp.dispnum_179"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, jnl_report_flg, regist_date, del_flg, SUBSTR(regist_date, 0, 5) AS year"
                           .", SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_nurse_home_main WHERE jnl_report_flg = 2 AND del_flg = 'f'"
                     .") AS jnhm"
              ." INNER JOIN (SELECT newest_apply_id, short_term_rehab_three AS dispnum_178, service AS dispnum_179"
                            ." FROM jnl_nurse_home_mon_visit_pre WHERE del_flg = 'f'"
                          .") AS jnhmvp"
                      ." ON jnhm.newest_apply_id = jnhmvp.newest_apply_id"
              ." WHERE del_flg = 'f'";

        if (!is_null($jnlFaclityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $jnlFaclityId); }
        if (!is_null($year))         { $sql .= sprintf(" AND year = '%04d'", $year); }
        if (!is_null($month))        { $sql .= sprintf(" AND month = '%02d'", $month); }

        return $this->getDateList($sql);
    }
}