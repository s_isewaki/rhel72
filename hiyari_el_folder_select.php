<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
$a = "2";//書庫：医療安全固定



//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// ログインユーザの職員IDを取得
//==============================
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");


//==============================
// ツリー情報を取得
//==============================

//管理画面の場合
if ($path == "3")
{
	$tree = lib_get_category_tree($con, $a, null, $fname, "admin");
}
//ユーザー画面の場合
else
{
	$tree = lib_get_category_tree($con, $a, $emp_id, $fname, "updatable");
}





//画面名
$PAGE_TITLE = "保存先選択";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">
<script type="text/javascript">
function initPage()
{
	drawTree();
}

var tree = null;
function drawTree()
{
	<?
	if (count($tree) == 0)
	{
		echo("var div = document.getElementById('folders');\n");
		echo("div.style.padding = '2px';\n");
		echo("div.innerHTML = '<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">フォルダは登録されていません。</font>';\n");
		echo("return;\n");
	}
	else
	{
		echo("tree = new YAHOO.widget.TreeView('folders');\n");
		echo("var root = tree.getRoot();\n");
		lib_write_tree_js($tree, $a, $c, $f, $path, $session);
		echo("tree.draw();\n");
	}
	?>
}

function expandAncestor(node)
{
	if (node.parent)
	{
		node.parent.expand();
		expandAncestor(node.parent);
	}
}

function expandAll()
{
	if (tree)
	{
		tree.expandAll();
		document.getElementById('expand').style.display = 'none';
		document.getElementById('collapse').style.display = '';
	}
}

function collapseAll()
{
	if (tree)
	{
		tree.collapseAll();
		document.getElementById('collapse').style.display = 'none';
		document.getElementById('expand').style.display = '';
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.selectedLabel {font-weight:bold; color:black; padding:1px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->



</td></tr><tr><td>




<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5">



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr height="22" bgcolor="#DFFFDC">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
		<td align="right">
		<span id="expand"><input type="button" value="全て開く" onclick="expandAll();"></span>
		<span id="collapse" style="display:none;"><input type="button" value="全て閉じる" onclick="collapseAll();"></span>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	<tr>
	<td valign="top" height="400" bgcolor="#FFFFFF"><div id="folders"></div></td>
	</tr>
	</table>
</td>
</tr>
<tr height="2">
<td></td>
</tr>
</table>



		</td>
		<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/r_3.gif" width="10" height="10"></td>
		<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td><img src="img/r_4.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- 本体 END -->


</td>
</tr>
</table>


</body>
</html>
<? pg_close($con); ?>
<?


// フォルダツリー表示JavaScriptを出力
function lib_write_tree_js($tree, $a, $c, $f, $path, $session)
{
	foreach ($tree as $tmp_folder)
	{
		$tmp_type = $tmp_folder["type"];
		$tmp_id = $tmp_folder["id"];
		$tmp_nm = h(str_replace("'", "\\'",$tmp_folder["name"]));
		
		//カテゴリの場合
		if ($tmp_type == "category")
		{
			$tmp_elm = "folder$tmp_id";

			echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm</font>', root, false);\n");
			echo("$tmp_elm.onLabelClick = function () {location.href = 'hiyari_el_folder_select_exe.php?session=$session&a=$a&c=$tmp_id&path=$path'; return false;}\n");
			if ($c == $tmp_id && $f == "") {
				echo("$tmp_elm.labelStyle = 'ygtvlabel selectedLabel';\n");
			}
		}
		//フォルダの場合
		else if ($tmp_type == "folder")
		{
			$tmp_cate_id = $tmp_folder["cate_id"];
			$tmp_parent_id = $tmp_folder["parent_id"];
			$tmp_parent_elm = ($tmp_parent_id == "") ? "folder$tmp_cate_id" : "folder{$tmp_cate_id}_{$tmp_parent_id}";
			$tmp_elm = "folder{$tmp_cate_id}_{$tmp_id}";

			echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm</font>', $tmp_parent_elm, false);\n");
			echo("$tmp_elm.onLabelClick = function () {location.href = 'hiyari_el_folder_select_exe.php?session=$session&a=$a&c=$tmp_cate_id&f=$tmp_id&path=$path'; return false;}\n");
			if ($f == $tmp_id)
			{
				echo("$tmp_elm.labelStyle = 'ygtvlabel selectedLabel';\n");
				echo("expandAncestor($tmp_elm);\n");
			}
		}
		
		//配下フォルダに対して再起処理
		lib_write_tree_js($tmp_folder, $a, $c, $f, $path, $session);
	}
}
?>
