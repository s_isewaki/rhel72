<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | 役職一覧</title>
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("show_status_list.ini");
require_once("maintenance_menu_list.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 27, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 順番変更処理
if ($action == "up" || $action == "down") {
	pg_query($con, "begin");

	// 選択された役職の表示順を取得
	$sql = "select order_no from stmst";
	$cond = "where st_id = $st_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$order_no = pg_fetch_result($sel, 0, "order_no");

	// 隣の役職を上げ下げ
	$diff = ($action == "up") ? -1 : 1;
	$sql = "update stmst set";
	$set = array("order_no");
	$setvalue = array($order_no);
	$cond = "where order_no = " . ($order_no + $diff);
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 選択役職を上げ下げ
	$sql = "update stmst set";
	$set = array("order_no");
	$setvalue = array($order_no + $diff);
	$cond = "where st_id = $st_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	pg_query($con, "commit");
}

// 役職一覧を取得
$sql = "select st_id, st_nm, order_no, link_key from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 役職一覧を配列に格納
$statuses = array();
$actual_order_no = 1;
while ($row = pg_fetch_array($sel)) {
	$statuses[] = array(
		"id" => $row["st_id"],
		"name" => $row["st_nm"],
		"actual_order_no" => $actual_order_no,
		"order_no" => $row["order_no"],
		"link_key" => $row["link_key"]        
	);
	$actual_order_no++;
}

// トランザクションを開始
pg_query($con, "begin");

// 並び順の再設定
foreach ($statuses as $tmp_status) {
	if ($tmp_status["actual_order_no"] == $tmp_status["order_no"]) {
		continue;
	}

	$sql = "update stmst set";
	$set = array("order_no");
	$setvalue = array($tmp_status["actual_order_no"]);
	$cond = "where st_id = {$tmp_status["id"]}";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");
?>
<script type="text/javascript">
function deleteStatus() {
	if (confirm("削除してよろしいですか？")) {
		document.mainform.submit();
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="status_menu.php?session=<? echo($session); ?>"><b>役職</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="status_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>役職一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="status_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteStatus();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform" action="status_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#f6f9ff">
<td width="28">&nbsp;</td>
<td width="40">&nbsp;</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職名</font></td>
</tr>
<?
for ($i = 0, $j = count($statuses) - 1; $i <= $j; $i++) {
	$tmp_status = $statuses[$i];
	$tmp_status_id = $tmp_status["id"];
	$tmp_status_name = $tmp_status["name"].($tmp_status["link_key"] ? " ({$tmp_status["link_key"]})" : "");

	if ($i == 0) {
		$tmp_img_html = "<img src=\"img/up-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
	} else {
		$tmp_img_html = "<a href=\"status_menu.php?session=$session&action=up&st_id=$tmp_status_id\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\"></a>";
	}
	if ($i == $j) {
		$tmp_img_html .= "<img src=\"img/down-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
	} else {
		$tmp_img_html .= "<a href=\"status_menu.php?session=$session&action=down&st_id=$tmp_status_id\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\"></a>";
	}

	echo("<tr height=\"22\">\n");
	echo("<td align=\"center\"><input type=\"checkbox\" name=\"trashbox[]\" value=\"$tmp_status_id\"></td>\n");
	echo("<td>$tmp_img_html</td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"status_update.php?session=$session&st_id=$tmp_status_id\">$tmp_status_name</a></font></td>\n");
    echo("</tr>\n");
}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
