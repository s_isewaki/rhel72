<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療区分登録権限チェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

$div_name = trim(mb_ereg_replace("　"," ",$div_name));

// 診療区分未入力チェック
if($div_name ==""){
	echo("<script language=\"javascript\">alert(\"{$summary_kubun_title}名を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

//既にその診療区分が使用されているかのチェック
//※summaryテーブルとは診療区分名でリンクしている為、使用されている場合は診療項目名を変更できない。
$tbl = "select count(*) from summary left join divmst on divmst.diag_div = summary.diag_div ";
$cond = "where divmst.div_id = '$div_id'";
$sel = select_from_table($con,$tbl,$cond,$fname);
if($sel == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	pg_close($con);
	exit;
}
$count = pg_result($sel,0,"count");
if($count > 0){
	$sel = select_from_table($con,"select diag_div from divmst where div_id = ".(int)$div_id,"",$fname);
	$diag_div_org = pg_fetch_result($sel, 0, "diag_div");
	if ($diag_div_org != $div_name){
		echo("<script language=\"javascript\">alert(\"この{$summary_kubun_title}は既に使用されているため、名称の更新はできません。\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		pg_close($con);
		exit;
	}
}


//同一名の診療区分が既に登録されているかチェック
$sql = "select count(div_id) from divmst where diag_div like '".pg_escape_string($div_name)."' and div_id <> ".(int)$div_id;
$sel = select_from_table($con,$sql,"",$fname);
if($sel == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$count = pg_result($sel,0,"count");
if($count > 0){
	echo("<script language=\"javascript\">alert(\"その{$summary_kubun_title}名は既に登録されています\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 画像登録
$icon_file_name = $_FILES["div_icon_file"]["name"];
if (@$icon_file_name) {
  // ファイルアップロードチェック
  define(UPLOAD_ERR_OK, 0);
  define(UPLOAD_ERR_INI_SIZE, 1);
  define(UPLOAD_ERR_FORM_SIZE, 2);
  define(UPLOAD_ERR_PARTIAL, 3);
  define(UPLOAD_ERR_NO_FILE, 4);

  // gif, jpgチェック
  $pos = strrpos($icon_file_name, ".");
  $ext = "";
  if ($pos > 0 ) {
    $ext = substr($icon_file_name, $pos+1, 3);
    $ext = strtolower($ext);
  }

  if ($pos === false || ($ext != "gif" && $ext != "jpg" && $ext != "png")) {
    echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  }

  switch ($_FILES["div_icon_file"]["error"]) {
  case UPLOAD_ERR_INI_SIZE:
  case UPLOAD_ERR_FORM_SIZE:
    echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  case UPLOAD_ERR_PARTIAL:
  case UPLOAD_ERR_NO_FILE:
    echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  }

  // ファイル保存用ディレクトリがなければ作成
  if (!is_dir("summary")) mkdir("summary", 0755);
  if (!is_dir("summary/divicon")) mkdir("summary/divicon", 0755);

  $icon_file_name1 = "summary/divicon/".$div_id.".gif";
  $icon_file_name2 = "summary/divicon/".$div_id.".jpg";
  $icon_file_name3 = "summary/divicon/".$div_id.".png";

  // 画像がある場合は削除
  if (is_file($icon_file_name1)) unlink($icon_file_name1);
  if (is_file($icon_file_name2)) unlink($icon_file_name2);
  if (is_file($icon_file_name3)) unlink($icon_file_name3);

  // アップロードされたファイルを保存
  $savefilename = "summary/divicon/".$div_id.".".$ext;
  $ret = copy($_FILES["div_icon_file"]["tmp_name"], $savefilename);

  if ($ret == false) {
    echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  }
}

// 診療区分情報を更新
$sql =
	" update divmst set".
	" diag_div = '" . pg_escape_string($div_name) . "'".
	",div_display_auth_flg = " . (int)@$_REQUEST["div_display_auth_flg"].
	",div_modify_auth_flg = " . (int)@$_REQUEST["div_modify_auth_flg"].
	",div_display_auth_stid_list = '" . pg_escape_string(@$_REQUEST["div_display_auth_stid_list"]) ."'".
	",div_modify_auth_stid_list = '" . pg_escape_string(@$_REQUEST["div_modify_auth_stid_list"]) ."'".
	",div_show_link_flowsheet_button = " . (int)@$_REQUEST["div_show_link_flowsheet_button"].
	",div_show_link_ondoban_button = " . (int)@$_REQUEST["div_show_link_ondoban_button"].
	",div_show_icon = " . (int)@$_REQUEST["div_show_icon"].
	" where div_id = ". (int)$div_id;
$upd = update_set_table($con, $sql, array(), array(), "", $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// データベース接続を切断
pg_close($con);

// 診療区分一覧画面に遷移
echo("<script language=\"javascript\">location.href = 'summary_kanri_kubun.php?session=$session';</script>");
?>
