<?
//ini_set( 'display_errors', 1 );
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_common_apply.inc");
require_once(dirname(__FILE__) . "/Cmx.php");
require_once('MDB2.php');

require_once(dirname(__FILE__) . "/cl_common_log_class.inc");

$log = new cl_common_log_class(basename(__FILE__,'.php'));
$log->info(basename(__FILE__)." START");

$fname=$PHP_SELF;

//------------------------------------------------------------------------------
// セッションのチェック
//------------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//------------------------------------------------------------------------------
// 決裁・申請権限のチェック
//------------------------------------------------------------------------------
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}



//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->debug("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

$con = connect2db($fname);

$obj = new cl_application_workflow_common_class($con, $fname);

//------------------------------------------------------------------------------
// ログインユーザの職員IDを保持
//------------------------------------------------------------------------------
$arr_empmst = $obj->get_empmst($session);
$login_emp_id        = $arr_empmst[0]["emp_id"];

//------------------------------------------------------------------------------
// トランザクションを開始
//------------------------------------------------------------------------------
$log->debug("トランザクション開始(mdb2) START",__FILE__,__LINE__);
$mdb2->beginTransaction();
$log->debug("トランザクション開始(mdb2) END",__FILE__,__LINE__);

pg_query($con, "begin");


//------------------------------------------------------------------------------
// 申請論理削除
//------------------------------------------------------------------------------
$obj->update_delflg_all_apply($apply_id, "t");

//------------------------------------------------------------------------------
// テンプレートの項目のみをパラメータに設定
//------------------------------------------------------------------------------
$param=template_parameter_pre_proccess($_POST);

//------------------------------------------------------------------------------
// パラメータに申請ＩＤを追加
//------------------------------------------------------------------------------
$param['apply_id'] = $apply_id;
$param['emp_id'] = $login_emp_id;

//------------------------------------------------------------------------------
// テンプレート独自の申請処理を呼び出す
//------------------------------------------------------------------------------
//ハンドラーモジュールロード
$log->debug('ハンドラーモジュールロード　開始　管理番号：'.$short_wkfw_name,__FILE__,__LINE__);
$handler_script=dirname(__FILE__) . "/cl/handler/".$short_wkfw_name."_handler.php";
$log->debug('$handler_script：'.$handler_script,__FILE__,__LINE__);
require_once($handler_script);
$log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

$log->debug('★cl_application_draft_delete.php ●$draft:'.$draft.' ●$mode:'.$mode.' ●$wkfw_id:'.$wkfw_id.' ●$apply_id:'.$apply_id,__FILE__,__LINE__);

// 2012/04/21 Yamagawa add(s)
// ハンドラー　クラス化対応
$log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
$handler_name = $short_wkfw_name."_handler";
$handler = new $handler_name();
$log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);
// 2012/04/21 Yamagawa add(e)

//------------------------------------------------------------------------------
// 新規申請（一時保存から）　削除の場合
//------------------------------------------------------------------------------
if( $mode == "draft_delete"){

	$log->debug('■ハンドラー新規申請（下書保存から）　削除　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//DraftApl_Delete($mdb2, $login_emp_id, $param);
	$handler->DraftApl_Delete($mdb2, $login_emp_id, $param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー新規申請（下書保存から）　削除　終了',__FILE__,__LINE__);

}
//------------------------------------------------------------------------------
// その他
//------------------------------------------------------------------------------
else{

}

//------------------------------------------------------------------------------
// トランザクションをコミット
//------------------------------------------------------------------------------
pg_query($con, "commit");

$log->debug("トランザクション確定 START",__FILE__,__LINE__);
$mdb2->commit();
$log->debug("トランザクション確定 END",__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベース接続を切断
//------------------------------------------------------------------------------
pg_close($con);

$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

// 添付ファイルの移動
foreach (glob("cl/apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

foreach (glob("cl/apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href='ladder_menu.php?session=$session';</script>");

$log->info(basename(__FILE__)." END");

?>