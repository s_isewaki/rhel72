<?php
//**********このファイルはエラーメッセージについてのファイルです**********

//*****エラーメッセージの書き込み*****
function write_error($fname,$ERRORLOG, $ERROR){				   								//引数としてエラー番号を受け取る　エラー番号はerror.infに記述

	if (is_file("./log/atmodule_err.log")){
		if (filesize("./log/atmodule_err.log") < 1024*1024) { // 1Mbまで
			$errorDate = date("D M j G:i:s T Y");                								// 例　Sat Mar 10 15:16:08 MST 2001
			@error_log($errorDate ."--> ".$fname.":". $ERROR . "\n",3,"./log/atmodule_err.log");		//エラーメッセージの書き込み処理
		}
	}

//	if($ERRORLOG == "on"){								  										//エラーログの書き込み有無チェック
		$errorDate = date("D M j G:i:s T Y");                								// 例　Sat Mar 10 15:16:08 MST 2001
		error_log($errorDate ."--> ".$fname.":". $ERROR . "\n",3,"./log/error.log");		//エラーメッセージの書き込み処理
		return 1;
//	}
	return 0;
}
?>