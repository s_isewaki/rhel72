<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>

<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require_once("summary_common.ini"); ?>
<? require_once("get_menu_label.ini"); ?>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// メドレポート権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
?>

<title><?=get_report_menu_label($con, $fname)?>｜ファイル添付</title>
</head>
<body>
<center>
<div style="padding:4px">


<!-- ヘッダ -->
<?= summary_common_show_dialog_header("ファイル添付"); ?>


<form name="frm"action="sum_apply_attach_save.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="session" value="<? echo($session); ?>">
<table width="100%" class="list_table" style="margin-top:4px">
	<tr>
		<th width="100" style="text-align:center">添付ファイル</th>
		<td>
			<input type="file" name="file" size="60"><span style="color:#da7954; padding-left:10px">※<?=ini_get("upload_max_filesize") ?>まで</span>
		</td>
	</tr>
</table>

<div class="search_button_div">
	<input type="button" onclick="document.frm.submit();" value="添付"/>
</div>

</form>

</div>
</center>
</body>
</html>
