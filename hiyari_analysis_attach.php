<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$PAGE_TITLE = "ファイル添付";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>



<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>




<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form action="hiyari_analysis_attach_save.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="session" value="<?=$session?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5">




		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
			<td height="22" width="160" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル：</font></td>
			<td width="440">
			<input type="file" name="file" size="50">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※<? echo(ini_get("upload_max_filesize")); ?>まで</font>
			</td>
		</tr>
		<tr>
			<td height="22" align="right" colspan="2"><input type="submit" value="添付"></td>
		</tr>
		</table>



		</td>
		<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/r_3.gif" width="10" height="10"></td>
		<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td><img src="img/r_4.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- 本体 END -->


</td>
</tr>
<tr>
<td>
<div id="attach">
</div>
</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
