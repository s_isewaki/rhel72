<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>

<form name='errorForm' action='atdbk_timecard_others_register.php' method='post'>
	<input type='hidden' name='back'        value='t'>
	<input type='hidden' name='session'     value='<?= $session ?>'>

	<input type="hidden" name="emp_id" value="<?=$emp_id?>">
	<input type="hidden" name="date" value="<? echo($date); ?>">
	<input type="hidden" name="tmcd_group_id" value="<?=$tmcd_group_id?>">
	<input type="hidden" name="pattern" value="<?=$pattern?>">
	<input type="hidden" name="reason" value="<?=$reason?>">
	<input type="hidden" name="night_duty" value="<?=$night_duty?>">
	<input type="hidden" name="allow_ids[]" value="<?=$allow_ids[0]?>">
	<input type="hidden" name="start_hour" value="<?=$start_hour?>">
	<input type="hidden" name="start_min" value="<?=$start_min?>">
	<input type="hidden" name="out_hour" value="<?=$out_hour?>">
	<input type="hidden" name="out_min" value="<?=$out_min?>">
	<input type="hidden" name="ret_hour" value="<?=$ret_hour?>">
	<input type="hidden" name="ret_min" value="<?=$ret_min?>">
	<input type="hidden" name="end_hour" value="<?=$end_hour?>">
	<input type="hidden" name="end_min" value="<?=$end_min?>">
	<input type="hidden" name="meeting_time_hh" value="<?=$meeting_time_hh?>">
	<input type="hidden" name="meeting_time_mm" value="<?=$meeting_time_mm?>">
	<input type='hidden' name='pnt_url'     value='<?= $pnt_url ?>'>
	<input type='hidden' name='previous_day_flag' value='<?= $previous_day_flag ?>'>
	<input type='hidden' name='next_day_flag' value='<?= $next_day_flag ?>'>
<?
	$break_flg = false;
	for ($i = 1; $i <= 10; $i++) {
		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";

		if ($$start_hour_var == "") {
			break;
		}

		echo("<input type=\"hidden\" name=\"".$start_hour_var."\"  value=\"".$$start_hour_var."\">\n");
		echo("<input type=\"hidden\" name=\"".$start_min_var."\"  value=\"".$$start_min_var."\">\n");
		echo("<input type=\"hidden\" name=\"".$end_hour_var."\"  value=\"".$$end_hour_var."\">\n");
		echo("<input type=\"hidden\" name=\"".$end_min_var."\"  value=\"".$$end_min_var."\">\n");
	}
?>

	<input type='hidden' name='status'      value='<?= $status ?>'>

	<input type='hidden' name='meeting_time_hh' value='<?= $meeting_time_hh ?>'>
	<input type='hidden' name='meeting_time_mm' value='<?= $meeting_time_mm ?>'>
	<input type="hidden" name="start_btn_time" value="<? echo($start_btn_time); ?>">
	<input type="hidden" name="end_btn_time" value="<? echo($end_btn_time); ?>">
	<input type="hidden" name="meeting_start_hour" value="<? echo($meeting_start_hour); ?>">
	<input type="hidden" name="meeting_start_min" value="<? echo($meeting_start_min); ?>">
	<input type="hidden" name="meeting_end_hour" value="<? echo($meeting_end_hour); ?>">
	<input type="hidden" name="meeting_end_min" value="<? echo($meeting_end_min); ?>">
<? /* 手当回数、残業時刻追加 */ ?>
	<input type='hidden' name='allow_counts[]'         value='<?= $allow_counts[0] ?>'>
	<input type="hidden" name="over_start_hour" value="<? echo($over_start_hour); ?>">
	<input type="hidden" name="over_start_min" value="<? echo($over_start_min); ?>">
	<input type="hidden" name="over_end_hour" value="<? echo($over_end_hour); ?>">
	<input type="hidden" name="over_end_min" value="<? echo($over_end_min); ?>">
	<input type='hidden' name='over_start_next_day_flag' value='<?= $over_start_next_day_flag ?>'>
	<input type='hidden' name='over_end_next_day_flag' value='<?= $over_end_next_day_flag ?>'>
<? /* 残業時刻2追加 20110621 */ ?>
	<input type="hidden" name="over_start_hour2" value="<? echo($over_start_hour2); ?>">
	<input type="hidden" name="over_start_min2" value="<? echo($over_start_min2); ?>">
	<input type="hidden" name="over_end_hour2" value="<? echo($over_end_hour2); ?>">
	<input type="hidden" name="over_end_min2" value="<? echo($over_end_min2); ?>">
	<input type='hidden' name='over_start_next_day_flag2' value='<?= $over_start_next_day_flag2 ?>'>
	<input type='hidden' name='over_end_next_day_flag2' value='<?= $over_end_next_day_flag2 ?>'>
	<? /* 残業理由 */ ?>
	<input type='hidden' name='ovtm_reason_id' value='<? echo($ovtm_reason_id); ?>'>
	<input type='hidden' name='ovtm_reason' value='<? echo($ovtm_reason); ?>'>
	<? //時間有休追加 20111207 ?>
	<input type="hidden" name="paid_hol_start_hour" value="<? echo($paid_hol_start_hour); ?>">
	<input type="hidden" name="paid_hol_start_min" value="<? echo($paid_hol_start_min); ?>">
	<input type="hidden" name="paid_hol_hour" value="<? echo($paid_hol_hour); ?>">
	<input type="hidden" name="paid_hol_min" value="<? echo($paid_hol_min); ?>">

</form>

<?
require_once("about_session.php");
require_once("about_postgres.php");
require_once("get_values.ini");
require_once("timecard_common_class.php");
require_once("timecard_bean.php");
require_once("atdbk_workflow_common_class.php");
require_once("show_clock_in_common.ini");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date);
//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

if ($apply_count == 0) {

	// 時刻のチェック
	if (($start_hour == "--" && $start_min != "--") || ($start_hour != "--" && $start_min == "--") || ($end_hour != "--" && $end_min == "--") || ($end_hour == "--" && $end_min != "--") || ($out_hour == "--" && $out_min != "--") || ($out_hour != "--" && $out_min == "--") || ($ret_hour == "--" && $ret_min != "--") || ($ret_hour != "--" && $ret_min == "--")) {
		echo("<script type=\"text/javascript\">alert('時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
	if (($over_start_hour == "--" && $over_start_min != "--") || ($over_start_hour != "--" && $over_start_min == "--") || ($over_end_hour != "--" && $over_end_min == "--") || ($over_end_hour == "--" && $over_end_min != "--")) {
		echo("<script type=\"text/javascript\">alert('残業時刻1の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
	if (($over_start_hour2 == "--" && $over_start_min2 != "--") || ($over_start_hour2 != "--" && $over_start_min2 == "--") || ($over_end_hour2 != "--" && $over_end_min2 == "--") || ($over_end_hour2 == "--" && $over_end_min2 != "--")) {
		echo("<script type=\"text/javascript\">alert('残業時刻2の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
    // 勤務条件テーブルより残業管理を取得
    $sql = "select no_overtime, specified_time from empcond";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    //残業管理するフラグ
    $no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
    $paid_specified_time = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "specified_time") : "";
    
    //時間有休追加 20111207 
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//1項目でも入力
		if ($paid_hol_start_hour != "--" || $paid_hol_start_min != "--" || $paid_hol_hour != "--" || $paid_hol_min != "--") {
			//休暇の場合はエラーとする
			if ($pattern == "10") {
				echo("<script type=\"text/javascript\">alert('休暇の場合には時間有休を指定できません。');</script>");
				echo("<script language='javascript'>document.errorForm.submit();</script>");
				exit;
				
			}
			//全項目が入力されていない場合
			if (!($paid_hol_start_hour != "--" && $paid_hol_start_min != "--" && $paid_hol_hour != "--" && $paid_hol_min != "--")) {
				echo("<script type=\"text/javascript\">alert('時間有休の開始時刻、時間、分を全て指定してください。');</script>");
				echo("<script language='javascript'>document.errorForm.submit();</script>");
				exit;
			}
		}
		$paid_hol_hour_start_time = $paid_hol_start_hour.$paid_hol_start_min;
		
		if ($obj_hol_hour->paid_hol_hour_max_flag == "t") {
			
			//時間有休使用時間数、限度数確認
			$year = substr($yyyymm, 0, 4);
			$month = substr($yyyymm, 4, 2);
			
			//カレンダーの所定労働時間を取得
			//$day1_time = $timecard_common_class->get_calendarname_day1_time();
            //所定労働時間履歴対応 20121129
            if ($paid_specified_time != "") {
                $day1_time = $paid_specified_time;
            }
            else {
                $arr_timehist = $calendar_name_class->get_calendarname_time($date);
                $day1_time = $arr_timehist["day1_time"];
            }
            
			$arr_ret = $obj_hol_hour->paid_hol_hour_max_check($year, $month, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $date, $paid_hol_hour, $paid_hol_min, $day1_time);
			if ($arr_ret["err_flg"] == "1") {
				echo("<script type=\"text/javascript\">alert('{$arr_ret["err_msg"]}');</script>");
				echo("<script language='javascript'>document.errorForm.submit();</script>");
				exit;
				
			}
		}
	}
	$empty_flg = false;
	for ($i = 1; $i <= 10; $i++) {
		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";

		if ($$start_hour_var == "") {$$start_hour_var = "--";}
		if ($$start_min_var == "") {$$start_min_var = "--";}
		if ($$end_hour_var == "") {$$end_hour_var = "--";}
		if ($$end_min_var == "") {$$end_min_var = "--";}

		if (($$start_hour_var == "--" && $$start_min_var != "--") || ($$start_hour_var != "--" && $$start_min_var == "--") || ($$end_hour_var == "--" && $$end_min_var != "--") || ($$end_hour_var != "--" && $$end_min_var == "--")) {
			echo("<script type=\"text/javascript\">alert('{$ret_str}時刻の時と分の一方のみは登録できません。');</script>");
			echo("<script language='javascript'>document.errorForm.submit();</script>");
			exit;
		}
		if ($empty_flg && ($$start_hour_var != "--" || $$end_hour_var != "--")) {
			echo("<script type=\"text/javascript\">alert('{$ret_str}時刻が不正です。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		if ($$start_hour_var == "--" && $$end_hour_var != "--") {
			echo("<script type=\"text/javascript\">alert('{$ret_str}時刻が不正です。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		if ($$start_hour_var == "--" || $$end_hour_var == "--") {
			$empty_flg = true;
		}
	}
}
if (($meeting_start_hour == "--" && $meeting_start_min != "--") || ($meeting_start_hour != "--" && $meeting_start_min == "--") || ($meeting_end_hour != "--" && $meeting_end_min == "--") || ($meeting_end_hour == "--" && $meeting_end_min != "--")) {
	echo("<script type=\"text/javascript\">alert('会議・研修・病棟外勤務の時と分の一方のみは登録できません。');</script>");
	echo("<script language='javascript'>document.errorForm.submit();</script>");
	exit;
}
if ($ovtm_reason_id == "other") {
	//残業時刻が指定された時、残業理由の未入力チェック 20120125
	if (($over_start_hour != "--" || $over_start_min != "--" || $over_end_hour != "--" || $over_end_min != "--" ||
				$over_start_hour2 != "--" || $over_start_min2 != "--" || $over_end_hour2 != "--" || $over_end_min2 != "--")
			&& $timecard_bean->over_time_apply_type != "0"
			&& $no_overtime != "t" // 残業管理をする場合
		){
		
		if ($ovtm_reason == "") {
			echo("<script type=\"text/javascript\">alert('残業理由が入力されていません。');</script>");
			echo("<script language='javascript'>document.errorForm.submit();</script>");
			exit;
		}
	}
	if (strlen($ovtm_reason) > 200) {
		echo("<script type=\"text/javascript\">alert('残業理由が長すぎます。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
	$ovtm_reason_id = null;
} else {
	$ovtm_reason = "";
}

// データベース登録値の編集
if ($pattern == "--") {$pattern = "";}
if ($reason == "--") {$reason = "";}
$start_time = ($start_hour == "--") ? "" : "$start_hour$start_min";
$end_time = ($end_hour == "--") ? "" : "$end_hour$end_min";
$out_time = ($out_hour == "--") ? "" : "$out_hour$out_min";
$ret_time = ($ret_hour == "--") ? "" : "$ret_hour$ret_min";
$meeting_start_time = ($meeting_start_hour == "--") ? "" : "$meeting_start_hour$meeting_start_min";
$meeting_end_time = ($meeting_end_hour == "--") ? "" : "$meeting_end_hour$meeting_end_min";
// 残業時刻追加
$over_start_time = ($over_start_hour == "--") ? "" : "$over_start_hour$over_start_min";
$over_end_time = ($over_end_hour == "--") ? "" : "$over_end_hour$over_end_min";
// 残業時刻2追加 20110622
$over_start_time2 = ($over_start_hour2 == "--") ? "" : "$over_start_hour2$over_start_min2";
$over_end_time2 = ($over_end_hour2 == "--") ? "" : "$over_end_hour2$over_end_min2";
for ($i = 1; $i <= 10; $i++) {
	$start_time_var = "o_start_time$i";
	$start_hour_var = "o_start_hour$i";
	$start_min_var = "o_start_min$i";
	$end_time_var = "o_end_time$i";
	$end_hour_var = "o_end_hour$i";
	$end_min_var = "o_end_min$i";
	$$start_time_var = ($$start_hour_var == "--") ? "" : "{$$start_hour_var}{$$start_min_var}";
	$$end_time_var = ($$end_hour_var == "--") ? "" : "{$$end_hour_var}{$$end_min_var}";

}

$allow_id = ($allow_ids[0] == "--" || $allow_ids[0] == "") ? NULL : $allow_ids[0];

//会議研修時間の作成
$meeting_time = str_replace("-", "0", $meeting_time_hh.$meeting_time_mm);
if ($meeting_time == "0000"){
	$meeting_time = "";
}

// 月給者は出勤予定なしでの打刻を不可とする
if (is_monthly_emp($con, $emp_id, $fname) && $pattern == "") {
	if ($start_time != "" || $end_time != "" || $out_time != "" || $ret_time != "" || $o_start_time1 != "" || $o_end_time1 != "" || $o_start_time2 != "" || $o_end_time2 != "" || $o_start_time3 != "" || $o_end_time3 != "" || $o_start_time4 != "" || $o_end_time4 != "" || $o_start_time5 != "" || $o_end_time5 != "" || $o_start_time6 != "" || $o_end_time6 != "" || $o_start_time7 != "" || $o_end_time7 != "" || $o_start_time8 != "" || $o_end_time8 != "" || $o_start_time9 != "" || $o_end_time9 != "" || $o_start_time10 != "" || $o_end_time10) {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('出勤予定を選択してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// トランザクションを開始
pg_query($con, "begin");

// 処理日付の勤務実績レコード数を取得
$sql = "select * from atdbkrslt";
$cond = "where emp_id = '$emp_id' and date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$rec_count = pg_num_rows($sel);
if ($rec_count > 0){
	//前回登録情報取得
	$pre_end_time        = pg_fetch_result($sel, 0, "end_time");
	$pre_next_day_flag   = pg_fetch_result($sel, 0, "next_day_flag");
	$pre_night_duty_flag = pg_fetch_result($sel, 0, "night_duty_flag");
	$pre_o_start_time1   = pg_fetch_result($sel, 0, "o_start_time1");
	$pre_o_start_time1   = pg_fetch_result($sel, 0, "o_start_time1");
	$pre_o_start_time3   = pg_fetch_result($sel, 0, "o_start_time3");
	$pre_o_start_time4   = pg_fetch_result($sel, 0, "o_start_time4");
	$pre_o_start_time5   = pg_fetch_result($sel, 0, "o_start_time5");
	$pre_o_start_time6   = pg_fetch_result($sel, 0, "o_start_time6");
	$pre_o_start_time7   = pg_fetch_result($sel, 0, "o_start_time7");
	$pre_o_start_time8   = pg_fetch_result($sel, 0, "o_start_time8");
	$pre_o_start_time9   = pg_fetch_result($sel, 0, "o_start_time9");
	$pre_o_start_time10  = pg_fetch_result($sel, 0, "o_start_time10");
	// 残業時刻追加
	$pre_start_time        = pg_fetch_result($sel, 0, "start_time");
	$pre_over_start_time        = pg_fetch_result($sel, 0, "over_start_time");
	$pre_over_end_time        = pg_fetch_result($sel, 0, "over_end_time");
	$pre_over_start_time2        = pg_fetch_result($sel, 0, "over_start_time2");
	$pre_over_end_time2        = pg_fetch_result($sel, 0, "over_end_time2");

	$pre_over_start_next_day_flag        = pg_fetch_result($sel, 0, "over_start_next_day_flag");
	$pre_over_end_next_day_flag        = pg_fetch_result($sel, 0, "over_end_next_day_flag");
	$pre_over_start_next_day_flag2        = pg_fetch_result($sel, 0, "over_start_next_day_flag2");
	$pre_over_end_next_day_flag2        = pg_fetch_result($sel, 0, "over_end_next_day_flag2");
}
else{
	$pre_end_time        = "";
	$pre_next_day_flag   = "";
	$pre_night_duty_flag = "";
	$pre_o_start_time1   = "";
	$pre_o_start_time2   = "";
	$pre_o_start_time3   = "";
	$pre_o_start_time4   = "";
	$pre_o_start_time5   = "";
	$pre_o_start_time6   = "";
	$pre_o_start_time7   = "";
	$pre_o_start_time8   = "";
	$pre_o_start_time9   = "";
	$pre_o_start_time10  = "";
	$pre_start_time       = "";
	$pre_over_start_time = "";
	$pre_over_end_time   = "";
	$pre_over_start_time2 = "";
	$pre_over_end_time2   = "";
	
	$pre_over_start_next_day_flag        = "";
	$pre_over_end_next_day_flag        = "";
	$pre_over_start_next_day_flag2        = "";
	$pre_over_end_next_day_flag2        = "";
}

//前日フラグが未設定、出勤時刻が入力されていない場合、前日フラグを初期化(0にする)
if ($start_time == "" || $start_time == null || $previous_day_flag == "" || $previous_day_flag == null){
	$previous_day_flag = 0;
}

//翌日フラグが未設定、退勤時刻が入力されていない場合、翌日フラグを初期化(0にする)
if ($end_time == "" || $end_time == null || $next_day_flag == "" || $next_day_flag == null){
	$next_day_flag = 0;
}

// 残業時刻追加
//残業開始翌日フラグを初期化(0にする)
if ($over_start_time == "" || $over_start_time == null || $over_start_next_day_flag == "" || $over_start_next_day_flag == null){
	$over_start_next_day_flag = 0;
}
//残業終了翌日フラグを初期化(0にする)
if ($over_end_time == "" || $over_end_time == null || $over_end_next_day_flag == "" || $over_end_next_day_flag == null){
	$over_end_next_day_flag = 0;
}
//残業開始2翌日フラグを初期化(0にする)
if ($over_start_time2 == "" || $over_start_time2 == null || $over_start_next_day_flag2 == "" || $over_start_next_day_flag2 == null){
	$over_start_next_day_flag2 = 0;
}
//残業終了2翌日フラグを初期化(0にする)
if ($over_end_time2 == "" || $over_end_time2 == null || $over_end_next_day_flag2 == "" || $over_end_next_day_flag2 == null){
	$over_end_next_day_flag2 = 0;
}


// 勤務実績レコードがなければ作成、あれば更新
if ($rec_count == 0) {
	$sql = "insert into atdbkrslt (emp_id, date, tmcd_group_id, pattern, reason, night_duty, allow_id, start_time, end_time, out_time, ret_time, o_start_time1, o_end_time1, o_start_time2, o_end_time2, o_start_time3, o_end_time3, o_start_time4, o_end_time4, o_start_time5, o_end_time5, o_start_time6, o_end_time6, o_start_time7, o_end_time7, o_start_time8, o_end_time8, o_start_time9, o_end_time9, o_start_time10, o_end_time10, status, meeting_time, previous_day_flag, next_day_flag, reg_prg_flg, meeting_start_time, meeting_end_time, allow_count, over_start_time, over_end_time, over_start_next_day_flag, over_end_next_day_flag, over_start_time2, over_end_time2, over_start_next_day_flag2, over_end_next_day_flag2) values (";
	$content = array($emp_id, $date, $tmcd_group_id, $pattern, $reason, $night_duty, $allow_id, $start_time, $end_time, $out_time, $ret_time, $o_start_time1, $o_end_time1, $o_start_time2, $o_end_time2, $o_start_time3, $o_end_time3, $o_start_time4, $o_end_time4, $o_start_time5, $o_end_time5, $o_start_time6, $o_end_time6, $o_start_time7, $o_end_time7, $o_start_time8, $o_end_time8, $o_start_time9, $o_end_time9, $o_start_time10, $o_end_time10, $status, $meeting_time, $previous_day_flag, $next_day_flag, "1", $meeting_start_time, $meeting_end_time, $allow_counts[0], $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
} else {
	$sql = "update atdbkrslt set";
	$set = array("tmcd_group_id", "pattern", "reason", "night_duty", "allow_id", "start_time", "end_time", "out_time", "ret_time", "o_start_time1", "o_end_time1", "o_start_time2", "o_end_time2", "o_start_time3", "o_end_time3", "o_start_time4", "o_end_time4", "o_start_time5", "o_end_time5", "o_start_time6", "o_end_time6", "o_start_time7", "o_end_time7", "o_start_time8", "o_end_time8", "o_start_time9", "o_end_time9", "o_start_time10", "o_end_time10", "status", "meeting_time", "previous_day_flag", "next_day_flag", "reg_prg_flg", "meeting_start_time", "meeting_end_time", "allow_count", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
	$setvalue = array($tmcd_group_id, $pattern, $reason, $night_duty, $allow_id, $start_time, $end_time, $out_time, $ret_time, $o_start_time1, $o_end_time1, $o_start_time2, $o_end_time2, $o_start_time3, $o_end_time3, $o_start_time4, $o_end_time4, $o_start_time5, $o_end_time5, $o_start_time6, $o_end_time6, $o_start_time7, $o_end_time7, $o_start_time8, $o_end_time8, $o_start_time9, $o_end_time9, $o_start_time10, $o_end_time10, $status, $meeting_time, $previous_day_flag, $next_day_flag, "1", $meeting_start_time, $meeting_end_time, $allow_counts[0], $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
	$cond = "where emp_id = '$emp_id' and date = '$date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
//時間有休追加 20111207 
if ($obj_hol_hour->paid_hol_hour_flag == "t") {
	$obj_hol_hour->update_paid_hol_hour($emp_id, $date, $paid_hol_start_hour, $paid_hol_start_min, $paid_hol_hour, $paid_hol_min);
}

//ワークフロー関連共通クラス
$obj = new atdbk_workflow_common_class($con, $fname);

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date);
// 翌日以降の休暇を実績へ登録
$timecard_common_class->next_hol_set($date, $emp_id);

//残業の有無
$ovtm_time_flg = false;
//残業時刻未入力
if ($no_overtime == "t" || ($over_start_time == "" && $over_end_time == "" &&
			$over_start_time2 == "" && $over_end_time2 == "")) {
	$ovtm_time_flg = false;
}
else {
	//事由に関わらず休暇で残業時刻があり
	//明けを条件に追加
	$after_night_duty_flag = "";
	if ($pattern != "" && $pattern != "10" && (($over_start_time != "" && $over_end_time != "") || ($over_start_time2 != "" && $over_end_time2 != ""))) {
		$sql = "select after_night_duty_flag from atdptn";
		$cond = "where group_id = $tmcd_group_id and atdptn_id = $pattern";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		if (pg_num_rows($sel) > 0) {
			$after_night_duty_flag = pg_fetch_result($sel, 0, "after_night_duty_flag");
		}
	}
	//休暇、明け、事後申請
	if (($pattern == "10" || $after_night_duty_flag == "1" || $timecard_bean->over_time_apply_type == "2") && (($over_start_time != "" && $over_end_time != "") || ($over_start_time2 != "" && $over_end_time2 != ""))) {
		$ovtm_time_flg = true;
	} else {
		//残業有無確認用の翌日フラグ
		if ($over_end_time != "") {
			$tmp_chk_next_day_flag = $over_end_next_day_flag;
		}
		else {
			$tmp_chk_next_day_flag = $next_day_flag;
		}
		$arr_result = get_atdbkrslt($con, $emp_id, $date, $fname);
		//残業有無
		$ovtm_time_flg = $timecard_common_class->is_overtime_apply($date, $start_time, $previous_day_flag, $end_time, $tmp_chk_next_day_flag, $night_duty, $arr_result["office_start_time"], $arr_result["office_end_time"], $link_type_flg, $arr_result["tmcd_group_id"], $over_start_time, $over_end_time, "", $arr_result["over_24hour_flag"], $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
	}
}


//理由詳細、現在入力なし
$reason_detail = null;

// 残業時刻が変わった場合
if ($over_start_time != $pre_over_start_time || $over_end_time != $pre_over_end_time || $over_start_next_day_flag != $pre_over_start_next_day_flag || $over_end_next_day_flag != $pre_over_end_next_day_flag || $over_start_time2 != $pre_over_start_time2 || $over_end_time2 != $pre_over_end_time2 || $over_start_next_day_flag2 != $pre_over_start_next_day_flag2 || $over_end_next_day_flag2 != $pre_over_end_next_day_flag2) {

	$sql = "select apply_id from ovtmapply ";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' order by apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while($row = pg_fetch_array($sel))
	{
        $ovtm_apply_id = $row["apply_id"];

		// 残業申請承認論理削除
		$obj->delete_ovtm_apply_id($ovtm_apply_id);
    }
	//残業時間がある場合
	if ($ovtm_time_flg) {
		
		$obj->insert_ovtm_apply	($emp_id, $date, $ovtm_reason_id, $ovtm_reason, $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, "0", $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $reason_detail);
		
		
	}
	
}
//変更なし、残業入力あり
else {
	//残業時間がある場合
	if ($ovtm_time_flg) {
		//既存データ確認、削除以外
		$sql = "select apply_id, apply_status from ovtmapply ";
		$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sinsei_flg = false;
		$num = pg_num_rows($sel);
		if ($num > 0) {
			$apply_status = pg_fetch_result($sel, 0, "apply_status");
			$apply_id = pg_fetch_result($sel, 0, "apply_id");
			//申請中、承認済み、そのまま変更しない
			if ($apply_status == "0" || $apply_status == "1") {
				$sinsei_flg = false;
				//出勤時刻等を更新
				$sql = "update ovtmapply set";
				$set = array("start_time", "end_time", "previous_day_flag", "next_day_flag", "reason_id", "reason");
				$setvalue = array($start_time, $end_time, $previous_day_flag, $next_day_flag, $ovtm_reason_id, $ovtm_reason);
				$cond = "where apply_id = $apply_id ";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
			}
			//上記以外は申請
			else {
				$sinsei_flg = true;
			}
		} else {
			$sinsei_flg = true;
		}
		if ($sinsei_flg) {
			$obj->insert_ovtm_apply	($emp_id, $date, $ovtm_reason_id, $ovtm_reason, $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, "0", $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $reason_detail);
		}
	}	
}

// 退勤後復帰時刻が変わった場合
if ($o_start_time1 != $pre_o_start_time1 || $o_start_time2 != $pre_o_start_time2 || $o_start_time3 != $pre_o_start_time3 || $o_start_time4 != $pre_o_start_time4 || $o_start_time5 != $pre_o_start_time5 || $o_start_time6 != $pre_o_start_time6 || $o_start_time7 != $pre_o_start_time7 || $o_start_time8 != $pre_o_start_time8 || $o_start_time9 != $pre_o_start_time9 || $o_start_time10 != $pre_o_start_time10) {

	$sql = "select apply_id from rtnapply ";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' order by apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while($row = pg_fetch_array($sel))
	{
	    $rtn_apply_id = $row["apply_id"];
	
		// 退勤後復帰申請承認論理削除
		$sql = "update rtnaprv set";
		$set = array("delete_flg");
		$setvalue = array("t");
		$cond = "where apply_id = $rtn_apply_id";
		
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 退勤後復帰申請論理削除
		$sql = "update rtnapply set";
		$set = array("delete_flg");
		$setvalue = array("t");
		$cond = "where apply_id = $rtn_apply_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 承認候補を論理削除
		$sql = "update rtnaprvemp set";
		$set = array("delete_flg");
		$setvalue = array("t");
		$cond = "where apply_id = $rtn_apply_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 同期・非同期情報を論理削除
		$sql = "update rtn_async_recv set";
		$set = array("delete_flg");
		$setvalue = array("t");
		$cond = "where apply_id = $rtn_apply_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 結果表示
echo("<script type=\"text/javascript\">location.href = 'atdbk_timecard_others_register.php?session=$session&emp_id=$emp_id&date=$date&success=t';</script>");
?>
</body>