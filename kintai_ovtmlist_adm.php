<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | タイムカード入力</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("atdbk_menu_common.ini");

require_once './kintai/common/mdb_wrapper.php';
require_once './kintai/common/user_info.php';
require_once './kintai/common/master_util.php';
require_once './kintai/common/code_util.php';
require_once './kintai/common/date_util.php';

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
//出張有無使用フラグ
$opt_trip_flag = file_exists("opt/trip_flag");

//フォームデータ
$yyyymm            = $_REQUEST["yyyymm"];
$emp_id            = $_REQUEST["emp_id"];
$view              = $_REQUEST["view"];
$srch_id           = $_REQUEST["srch_id"];
$srch_name         = $_REQUEST["srch_name"];
$cls               = $_REQUEST["cls"];
$atrb              = $_REQUEST["atrb"];
$dept              = $_REQUEST["dept"];
$group_id          = $_REQUEST["group_id"];
$page              = $_REQUEST["page"];
$shift_group_id    = $_REQUEST["shift_group_id"];
$csv_layout_id     = $_REQUEST["csv_layout_id"];
$uty_form_jokin    = $_REQUEST["duty_form_jokin"];
$duty_form_hijokin = $_REQUEST["duty_form_hijokin"];
$staff_ids         = $_REQUEST["staff_ids"];
$data_cnt          = $_REQUEST["data_cnt"];
$check_flg         = $_REQUEST["check_flg"];
$emp_move_flg      = $_REQUEST["emp_move_flg"];
$emp_personal_id   = $_REQUEST["emp_personal_id"];
$sus_flg           = $_REQUEST["sus_flg"];

$url_srch_name = urlencode($srch_name);

// 勤務管理権限の取得
$work_admin_auth = check_authority($session, 42, $fname);

//データアクセス
try {

	MDBWrapper::init(); // おまじない
	$log = new common_log_class(basename(__FILE__));
	$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

	$db->beginTransaction(); // トランザクション開始

	// データベースに接続
	$con = connect2db($fname);

	//次の職員、前の職員
	require_once("work_admin_timecard_common.php");
	$err_flg = "";
	//$emp_personal_id = $base_info->emp_personal_id;

	if ($emp_move_flg != "") {
		//勤務シフト作成からの場合
		if ($wherefrom >= "7") {
			$arr_staff_id = split(",", $staff_ids);
			$staff_idx = "";
			$data_cnt = count($arr_staff_id);
			for ($i=0; $i<$data_cnt; $i++) {
				if ($emp_id == $arr_staff_id[$i]) {
					$staff_idx = $i;
					break;
				}
			}
			if (($emp_move_flg == "prev" && $staff_idx == 0) || //先頭で前の職員
				($emp_move_flg == "next" && $staff_idx == $data_cnt - 1)) { //最後で次の職員
				$err_flg = "1";
			} else {
				$move_idx = ($emp_move_flg == "next") ? $staff_idx + 1 : $staff_idx - 1;
				$move_emp_id = $arr_staff_id[$move_idx];
			}
		} else {
		
			$move_emp_id = get_move_emp_id($con, $fname, $emp_move_flg, $emp_personal_id, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id, $sus_flg);

		}
		if ($move_emp_id == "") {
			$err_flg = "1";
		} else {
			$emp_id = $move_emp_id;
		}
	}
	
	//GETパラメータ
	$getPrm  = "&session=".$session;
	$getPrm .= "&view=".$view;
	$getPrm .= "&emp_id=".$emp_id;
	$getPrm .= "&srch_name=".$srch_name;
	$getPrm .= "&cls=".$cls;
	$getPrm .= "&atrb=".$atrb;
	$getPrm .= "&dept=".$dept;
	$getPrm .= "&group_id=".$group_id;
	$getPrm .= "&page=".$page;
	$getPrm .= "&shift_group_id=".$shift_group_id;
	$getPrm .= "&csv_layout_id=".$csv_layout_id;
	$getPrm .= "&duty_form_jokin=".$duty_form_jokin;
	$getPrm .= "&duty_form_hijokin=".$duty_form_hijokin;
    $getPrm .= "&srch_id=".$srch_id;
    $getPrm .= "&sus_flg=".$sus_flg;
    
	//職員情報
	$ukey  = $emp_id;
	$uType = "u";
	$user_info   = UserInfo::get($ukey, $uType);
	$base_info   = $user_info->get_base_info();
	$job_info    = $user_info->get_job_info();
	$class_info  = $user_info->get_class_info();
	$attr_info   = $user_info->get_attr_info();
	$dept_info   = $user_info->get_dept_info();
	$croom_info  = $user_info->get_class_room_info();
	$cond_info   = $user_info->get_emp_condition();
	
	
	//組織階層
	$class_cnt  = MasterUtil::get_organization_level_count();
	
	//所属
	$shozoku = $class_info->class_nm."＞".$attr_info->atrb_nm."＞".$dept_info->dept_nm;
	if($class_cnt == 4){
		if($croom_info->room_nm != ""){
			$shozoku .= "＞".$croom_info->room_nm;
		}
	}
	

	//締め日情報を取得
	$closing_info  = MasterUtil::get_closing();
	if(is_null($closing_info)){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if($cond_info->duty_form == ""){
		$closing = "";
	}else if($cond_info->duty_form == "2"){
		if($closing_info["closing_parttime"] != ""){
			$closing = $closing_info["closing_parttime"];
		}else{
			$closing = $closing_info["closing"];
		}
	}else{
		$closing = $closing_info["closing"];
	}
	
	//期間取得（月度考慮）
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);
	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);
	$prdArr = DateUtil::get_cutoff_period($cond_info->duty_form, $year, $month);
	//kintai_ovtmkbn_mst
	$sql  = "";
	$sql .= "SELECT * FROM kintai_ovtmkbn_mst ORDER BY order_no";
	$retKbn = $db->find($sql);
	//集計用配列初期化
	for($k=0;$k<count($retKbn);$k++){
		$ttlKbn[$k] = 0;
	}
	
	//データ取得
	$sql  = "";
	$sql .= "SELECT ";
	$sql .= " atdbkrslt.emp_id, ";
	$sql .= " atdbkrslt.date, ";
	$sql .= " atdbkrslt.pattern, ";
	$sql .= " atdbkrslt.reason, ";
	$sql .= " atdbkrslt.tmcd_group_id, ";
	$sql .= " atdbkrslt.meeting_start_time, ";
	$sql .= " atdbkrslt.meeting_end_time, ";
    if ($opt_trip_flag) {
        $sql .= " atdbk_trip.trip_flag, ";
    }
    $sql .= " atdptn.atdptn_nm, ";
	$sql .= " atdptn.workday_count, ";
	$sql .= " atdptn.atdptn_order_no, ";
	$sql .= " atdbk_reason_mst.default_name, ";
	$sql .= " atdbk_reason_mst.display_name, ";
	$sql .= " timecard_paid_hol_hour.calc_minute, ";
	$sql .= " wktmgrp.group_name, ";
	$sql .= " kintai_ovtm.apply_date, ";
	$sql .= " kintai_ovtm.apply_stat, ";
	$sql .= " kintai_ovtm.cmmd_start_time1, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag1, ";
	$sql .= " kintai_ovtm.cmmd_end_time1, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag1, ";
	$sql .= " kintai_ovtm.cmmd_rest_time1, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.cmmd_start_time2, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag2, ";
	$sql .= " kintai_ovtm.cmmd_end_time2, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag2, ";
	$sql .= " kintai_ovtm.cmmd_rest_time2, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.cmmd_start_time3, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag3, ";
	$sql .= " kintai_ovtm.cmmd_end_time3, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag3, ";
	$sql .= " kintai_ovtm.cmmd_rest_time3, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.rslt_start_time1, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag1, ";
	$sql .= " kintai_ovtm.rslt_end_time1, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag1, ";
	$sql .= " kintai_ovtm.rslt_rest_time1, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.rslt_start_time2, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag2, ";
	$sql .= " kintai_ovtm.rslt_end_time2, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag2, ";
	$sql .= " kintai_ovtm.rslt_rest_time2, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.rslt_start_time3, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag3, ";
	$sql .= " kintai_ovtm.rslt_end_time3, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag3, ";
	$sql .= " kintai_ovtm.rslt_rest_time3, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.mdfy_start_time1, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag1, ";
	$sql .= " kintai_ovtm.mdfy_end_time1, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag1, ";
	$sql .= " kintai_ovtm.mdfy_rest_time1, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag1, ";
	$sql .= " kintai_ovtm.mdfy_start_time2, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag2, ";
	$sql .= " kintai_ovtm.mdfy_end_time2, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag2, ";
	$sql .= " kintai_ovtm.mdfy_rest_time2, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag2, ";
	$sql .= " kintai_ovtm.mdfy_start_time3, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag3, ";
	$sql .= " kintai_ovtm.mdfy_end_time3, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag3, ";
	$sql .= " kintai_ovtm.mdfy_rest_time3, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag3 ";
	$sql .= "FROM ";
	$sql .= " atdbkrslt ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdptn ";
	$sql .= "ON ";
	$sql .= " CAST(atdptn.atdptn_id AS varchar) = atdbkrslt.pattern AND ";
	$sql .= " atdptn.group_id  = atdbkrslt.tmcd_group_id ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdbk_reason_mst ";
	$sql .= "ON ";
	$sql .= " atdbk_reason_mst.reason_id = atdbkrslt.reason ";
	$sql .= "LEFT JOIN ";
	$sql .= " timecard_paid_hol_hour ";
	$sql .= "ON ";
	$sql .= " timecard_paid_hol_hour.emp_id       = atdbkrslt.emp_id AND ";
	$sql .= " timecard_paid_hol_hour.start_date   = atdbkrslt.date ";
	$sql .= "LEFT JOIN ";
	$sql .= " kintai_ovtm ";
	$sql .= "ON ";
	$sql .= " kintai_ovtm.emp_id = atdbkrslt.emp_id AND ";
	$sql .= " kintai_ovtm.date   = atdbkrslt.date ";
    if ($opt_trip_flag) {
        $sql .= "LEFT JOIN ";
        $sql .= " atdbk_trip ";
        $sql .= "ON ";
        $sql .= " atdbkrslt.emp_id = atdbk_trip.emp_id AND ";
        $sql .= " atdbkrslt.date   = atdbk_trip.date ";
    }
    $sql .= "LEFT JOIN ";
	$sql .= " wktmgrp ";
	$sql .= "ON ";
	$sql .= " wktmgrp.group_id   = atdbkrslt.tmcd_group_id ";
	$sql .= "WHERE ";
	$sql .= " atdbkrslt.emp_id = '".$base_info->emp_id."' AND ";
	$sql .= " (atdbkrslt.date >= '".$prdArr["start"]."' AND atdbkrslt.date <= '".$prdArr["end"]."') ";
	$retRec = $db->find($sql);
	$rData = array();	//データ
	//表示日数
	$dcnt = DateUtil::date_diff($prdArr["start"], $prdArr["end"]);
	for($i=0;$i<$dcnt;$i++){
		$arcnt = date("Ymd", strtotime($prdArr["start"]." ".$i." day")); 
		$rData[$i]["Ymd"] = $arcnt;
		//背景色
		$rData[$i]["bgcolor"] = getBgColor($arcnt);
/*
		if($check_flg == 1){
			if($rData[$i]["Ymd"] <= date("Ymd")){
				$rData[$i]["bgcolor"] = "#FFFF66";
			}
		}
*/
	}

	$ttlType = array();	//集計データ
    $ovtm_ttl = 0; // 月次超勤時間
    $ttlMeeting = 0;	//会議集計
	$ttlTimehol = 0;	//時間休集計
	for($out=0;$out<count($rData);$out++){
		//日付
		$rData[$out]["date"] = intval(substr($rData[$out]["Ymd"], 4, 2))."月".intval(substr($rData[$out]["Ymd"], 6, 2))."日";
		//曜日
		$rData[$out]["wday"] = DateUtil::day_of_week($rData[$out]["Ymd"]);

		for($i=0;$i<count($retRec);$i++){
			if($rData[$out]["Ymd"] == $retRec[$i]["date"]){
				//勤務実績
				$rData[$out]["atdptn_nm"] = $retRec[$i]["atdptn_nm"];
				//勤務実績集計
				$g = $retRec[$i]["tmcd_group_id"];
				//$p = $retRec[$i]["pattern"];
				$o = $retRec[$i]["atdptn_order_no"];	//表示順考慮
				//$ttlType[$g][$p]["name"] = $retRec[$i]["atdptn_nm"];
				$ttlType[$g][$o]["group"] = $retRec[$i]["group_name"];
				$ttlType[$g][$o]["name"]  = $retRec[$i]["atdptn_nm"];
				$ttlType[$g][$o]["gid"]   = $retRec[$i]["tmcd_group_id"];
				$ttlType[$g][$o]["pid"]   = $retRec[$i]["pattern"];
				$ttlType[$g][$o]["order"] = $retRec[$i]["atdptn_order_no"];
				$ttlType[$g][$o]["cnt"]   = floatval($ttlType[$g][$o]["cnt"]) + floatval($retRec[$i]["workday_count"]);
				
				//事由
				if($retRec[$i]["display_name"] != ""){
					$rData[$out]["reason_nm"] = $retRec[$i]["display_name"];
				}else{
					$rData[$out]["reason_nm"] = $retRec[$i]["default_name"];
				}
                // 出張
                $rData[$out]["trip_flag"] = $retRec[$i]["trip_flag"];
                //申請状態 0 1 2 3
				if(/*$retRec[$i]["apply_stat"] == 0 || $retRec[$i]["apply_stat"] == 1*/ in_array($retRec[$i]["apply_stat"], array(0,1,2,3))){
                    //申請状態
					$rData[$out]["apply_stat"] = CodeUtil::get_apply_status($retRec[$i]["apply_stat"]);
					for($j=1;$j<=3;$j++){
						if($retRec[$i]["mdfy_cancel_flag".$j] == 1){
							$rData[$out]["ovtm_t".$j] = "";
							$rData[$out]["ovtm_r".$j] = "";
							$rData[$out]["ovtm_b".$j] = "";
						}else{
							$fldName = "";
							if($retRec[$i]["mdfy_start_time".$j] != ""){
								$fldName = "mdfy";
							}else if($retRec[$i]["rslt_start_time".$j] != ""){
								$fldName = "rslt";
							}else if($retRec[$i]["cmmd_start_time1".$j] != ""){
								$fldName = "cmmd";
							}else{
								$fldName = "";
								$rData[$out]["ovtm_t".$j] = "";
								$rData[$out]["ovtm_r".$j] = "";
								$rData[$out]["ovtm_b".$j] = "";
							}
							if($fldName != ""){
								//時間外
								$rData[$out]["ovtm_t".$j] = "";
								if($retRec[$i][$fldName."_start_next_flag".$j] == 1){
									$rData[$out]["ovtm_t".$j] .= "翌";
								}
								$rData[$out]["ovtm_t".$j] .= intval(substr($retRec[$i][$fldName."_start_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_start_time".$j], 2, 2);
								$rData[$out]["ovtm_t".$j] .= "〜";
								if($retRec[$i][$fldName."_end_next_flag".$j] == 1){
									$rData[$out]["ovtm_t".$j] .= "翌";
								}
								$rData[$out]["ovtm_t".$j] .= intval(substr($retRec[$i][$fldName."_end_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_end_time".$j], 2, 2);
								//理由
								if(intval($retRec[$i][$fldName."_ovtmrsn_id".$j]) > 0){
									$sql  = "";
									$sql .= "SELECT ";
									$sql .= " reason ";
									$sql .= "FROM ";
									$sql .= " ovtmrsn ";
									$sql .= "WHERE ";
									$sql .= " reason_id = ".$retRec[$i][$fldName."_ovtmrsn_id".$j];
									$retRsn = $db->find($sql);
									$rData[$out]["ovtm_r".$j] = $retRsn[0]["reason"];
									
								}else{
									$rData[$out]["ovtm_r".$j] = "";
								}
								//休憩
								if($retRec[$i][$fldName."_rest_time".$j] != "" && $retRec[$i][$fldName."_rest_time".$j] != "0000"){
									$rData[$out]["ovtm_b".$j] = intval(substr($retRec[$i][$fldName."_rest_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_rest_time".$j], 2, 2);
								}else{
									$rData[$out]["ovtm_b".$j] = "";
								}
							}
						}
		
					}
					
					//時間外
					$sql = "";
					$sql .= "SELECT ";
					$sql .= " sum(minute) ";
					$sql .= "FROM ";
					$sql .= " kintai_ovtm_sum ";
					$sql .= "WHERE ";
					$sql .= " emp_id = '".$base_info->emp_id."' AND ";
					$sql .= " date = '".$retRec[$i]["date"]."'";
					$retOvtm = $db->one($sql);
						
					if(count($retOvtm) > 0 && $retRec[$i]["apply_stat"] == 1){
						$rData[$out]["ovtm_time"] = DateUtil::convert_time($retOvtm);
						$ovtm_ttl += $retOvtm;
					}else{
						$rData[$out]["ovtm_time"] = "";
					}
                    
                    //チェック
                    if($check_flg == 1){
                        //休暇で事由なし
                        if($retRec[$i]["pattern"] == "10" && $retRec[$i]["reason"] == ""){
                            $rData[$out]["bgcolor"] = "#FFFF66";
                        }
                        //申請中・差戻し
                        if($retRec[$i]["apply_stat"] == "0" || $retRec[$i]["apply_stat"] == "3"){
							$rData[$out]["bgcolor"] = "#FFFF66";
						}
					}
				}else{
					//申請状態
					$rData[$out]["apply_stat"] = "";
					//時間外、理由、休憩1〜3
					for($j=1;$j<=3;$j++){
						$rData[$out]["ovtm_t".$j] = "";
						$rData[$out]["ovtm_r".$j] = "";
						$rData[$out]["ovtm_b".$j] = "";
					}
		
				}
				//会議研修
				$rData[$out]["meeting"] = DateUtil::diff_time($retRec[$i]["meeting_start_time"], $retRec[$i]["meeting_end_time"]);
				$ttlMeeting += DateUtil::convert_min($rData[$out]["meeting"]);
				//時間有給
				$rData[$out]["hol_min"] = DateUtil::convert_time($retRec[$i]["calc_minute"]);
				$ttlTimehol += DateUtil::convert_min($rData[$out]["hol_min"]);
			}

		}
	
		//未入力チェック
		if($check_flg == 1){
			if($rData[$out]["Ymd"] <= date("Ymd") && $rData[$out]["atdptn_nm"] == ""){
				$rData[$out]["bgcolor"] = "#FFFF66";
			}
		}
	}

	
	//チェックフラグ
	$sql  = "";
	$sql .= "SELECT ";
	$sql .= " check_flag ";
	$sql .= "FROM duty_shift_plan_staff ";
	$sql .= " duty_shift_plan_staff ";
	$sql .= "WHERE ";
	$sql .= " emp_id = '".$base_info->emp_id."' AND ";
	$sql .= " duty_yyyy = ".$year." AND ";
	$sql .= " duty_mm = ".$month." ";
	$retCHK = $db->find($sql);
	$chkRecCnt = 0;
	$checked_flg = "";
	if(count($retCHK) > 0){
		$chkRecCnt = count($retCHK);
		for($i=0;$i<count($retCHK);$i++){
			if($retCHK[$i]["check_flag"] == "t"){
				$checked_flg = "checked";
			}
		}
	}

	$db->endTransaction(); // トランザクション終了
} catch (BusinessException $bex) {
	$message = $bex->getMessage();
	echo("<script type='text/javascript'>alert('".$message."');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} catch (FatalException $fex) { 
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

function getBgColor($date) {
	if(is_cal_holiday($date) == true){
		return "#FADEDE";
	}else{
		return "#FFFFFF";
	}

}
function is_cal_holiday($date) {
	$sql = "select type from calendar where date = :date";
	$value = get_mdb()->one(
		$sql, array("text"), array("date" => $date));

	if (in_array($value, array("6", "7"))) {
		return true;
	}
	return false;
}
function get_mdb() {
	$log = new common_log_class(basename(__FILE__));
	return new MDB($log);
}

// データベースに接続
$con = connect2db($fname);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./kintai/js/tmpl_common.js?<?php echo time(); ?>"></script>
<script type="text/javascript">
<!--
<?php if ($opt_trip_flag) { ?>
j$(document).ready(function() {
	j$("[id^=trip_]").change(function() {
		var session = j$("#session").val();
		var date = j$(this).attr("id").split('_')[1];
		var flag = j$(this).val();
		var emp_id = j$("#emp_id").val();
		update_trip(session, date, emp_id, flag, 5);
	});
});
<? } ?>

//一括修正画面
function openTimecardAll() {
	base_left = 0;
	base = 0;
	wx = window.screen.availWidth;
	wy = window.screen.availHeight;
	url = 'atdbk_timecard_all.php?session=<?=$session?>&yyyymm=<? echo($yyyymm); ?>&view=<? echo($view); ?>&check_flg=<? echo($check_flg); ?>';
	window.open(url, 'TimecardAll', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
}

//前の職員、次の職員
function emp_move(move_flg) {
	document.frmKintai.emp_move_flg.value = move_flg;
//	view = (document.frmKintai.view.value == '1') ? '2' : '1';
//	document.frmKintai.view.value = view;
<?
if ($wherefrom == "7") {
	//勤務シフトからの場合
?>
	//職員情報が未設定の場合に呼出元画面から取得
	if (document.frmKintai.staff_ids.value == '') {
		if(window.opener && !window.opener.closed && window.opener.document.mainform.data_cnt) {
			data_cnt = window.opener.document.mainform.data_cnt.value;
			staff_ids = '';
			for (i=0; i<data_cnt; i++) {
				wk1 = 'staff_id[' + i + ']';
				staff_id = window.opener.document.mainform.elements[wk1].value;
				if (i > 0) {
					staff_ids += ',';
				}
				staff_ids += staff_id;
			}
			
			document.frmKintai.staff_ids.value = staff_ids;
			document.frmKintai.data_cnt.value = data_cnt;
		} else {
			alert('職員情報を取得できません。もう一度開き直してください。');
			return false;
		}
	}
	<?
}
?>

	document.frmKintai.submit();

}

//チェック
function inputcheck() {
	document.frmKintai.check_flg.value = '1';
//	view = (document.frmKintai.view.value == '1') ? '2' : '1';
//	document.frmKintai.view.value = view;
	document.frmKintai.submit();

}

//チェック済み
function update_checked(dtcnt, mon) {
	
	//勤務シフト未登録	
	if(dtcnt == 0){
		alert(mon + "月度の勤務予定が未登録です。");
		return false;
	}

	document.frmKintai.action = "./kintai_ovtmlist_update.php";
	document.frmKintai.method="POST";
	document.frmKintai.submit();
	
}

function print_xls(){

	if(document.frmKintai.emp_id.value == ""){
		alert("職員が選択されていません。");
		return false;
	}

	document.frmKintai.action = "./kintai_ovtmlist_xls.php";
	document.frmKintai.method = "POST";
	document.frmKintai.submit();

}


//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
td.lbl {padding-right:1em;}
td.txt {padding-right:0.5em;}
td.tm {width:2.8em;text-align:right;}
td.mark {width:2.8em;text-align:center;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="frmKintai" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
<? /* 実績入力画面からの場合 20120116 */
if ($wherefrom == "7") { ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#5279a5">
					<td width="100%" height="28" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>タイムカード修正</b></font></td>
					<td width="28" bgcolor="#5279a5" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
				</tr>
			</table>
<?
}else{
	if ($referer == "2") { 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
				</tr>
			</table>
<? 
	} else { 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
				</tr>
			</table>
<? 
	}
	require_once("work_admin_menu_common.ini");
	// メニュー表示
    $option = array("emp_id" => $emp_id,
            "srch_name" => $url_srch_name,
            "cls" => $cls,
            "atrb" => $atrb,
            "dept" => $dept,
            "page" => $page,
            "group_id" => $group_id,
            "shift_group_id" => $shift_group_id,
            "csv_layout_id" => $csv_layout_id,
            "duty_form_jokin" => $duty_form_jokin,
            "duty_form_hijokin" => $duty_form_hijokin,
            "srch_id" => $srch_id,
            "sus_flg" => $sus_flg);
	show_work_admin_menuitem($session, $fname, $option);
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
<?php
}
if ($closing == "") {  // 締め日未登録の場合 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。管理者に連絡してください。</font></td>
				</tr>
			</table>
<?php
} else { 
?>
			<table  width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td height="22" >
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?php
    $wherefrom = ($wherefrom >= "7") ? "7" : $wherefrom;
    $url_shift = "work_admin_timecard_shift.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
?>
									<a href="<?php echo($url_shift); ?>">出勤簿</a>&nbsp;
    <?php
    $url = "work_admin_timecard.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";	
?>
									<a href="<?php echo($url); ?>">日別修正</a>&nbsp;
<?php
    $url = "work_admin_timecard_a4.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
?>
									<a href="<?php echo($url); ?>">日別修正（A4横）</a>&nbsp;
									
<?
	if ($wherefrom != "7") {
?>
									<a href="javascript:void(0);" onclick="openTimecardAll();">
<?
	}else{ 
        $url = "work_admin_timecard_all.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&check_flg=$check_flg&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
?>
									<a href="<? echo($url); ?>">
<?
	}
?>
									一括修正</a>
									&nbsp;
									<b>出勤簿（超勤区分）</b>
    <?php
    $url = "kintai_ovtmlist_detail_adm.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&check_flg=$check_flg&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";	
    echo "<a href='".$url."'>超勤区分内訳</a>";
?>
									</font>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td height="22">
								
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="kintai_ovtmlist_adm.php?yyyymm=<?php echo($last_yyyymm.$getPrm); ?>">&lt;前月</a>&nbsp;<?php echo("{$year}年{$month}月度"); ?>&nbsp;<a href="kintai_ovtmlist_adm.php?yyyymm=<?php echo($next_yyyymm.$getPrm); ?>">翌月&gt;</a></font>&nbsp;
									&nbsp;
									<input type="button" value="<前の職員" onclick="emp_move('prev');">
									<input type="button" value="次の職員>" onclick="emp_move('next');">
									&nbsp;
									<input type="button" value="チェック" onclick="inputcheck();">
									&nbsp;
									<input type="button" value="印刷" onclick="return print_xls();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td height="22" width="">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									職員ID：<?php echo $base_info->emp_personal_id; ?>&nbsp;&nbsp;
									職員氏名：<?php echo $base_info->emp_lt_nm."&nbsp;".$base_info->emp_ft_nm; ?>&nbsp;&nbsp;
									所属：<?php echo $shozoku; ?>&nbsp;&nbsp;
									（
										<?php echo $job_info->job_nm; ?>&nbsp;
										<?php echo CodeUtil::get_duty_form($cond_info->duty_form); ?>&nbsp;
										<?php echo CodeUtil::get_salary_type($cond_info->wage); ?>
									）
									</font>
								</td>
								<td align="right" width="">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<!--りすと-->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
			<table border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="center" nowrap width="56" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
					<td align="center" nowrap width="18"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜<br>日</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請状態</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議<br>研修</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間<br>有休</font></td>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出張<br>有無</font></td>
<? } ?>
				</tr>
<?php
for($i=0;$i<count($rData);$i++){
	$strDate = "";
	if($rData[$i]["atdptn_nm"] != ""){
		$openURL  = "kintai_ovtmlist_input.php";
		$openURL .= "?date=".$rData[$i]["Ymd"];
		$openURL .= "&wherefrom=".$wherefrom;
		$openURL .= $getPrm;
		$strDate = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$openURL."', 'newwin', 'width=640,height=720,scrollbars=yes');\">".$rData[$i]["date"]."</a>";
	}else{
		$strDate = $rData[$i]["date"];
	}
	
?>				
				<tr bgcolor="<?echo $rData[$i]["bgcolor"]; ?>">
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap height="30">
<? } else { ?>
					<td align="center" nowrap height="24">
<? } ?>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $strDate; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["wday"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["atdptn_nm"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["reason_nm"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["apply_stat"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["meeting"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["hol_min"]; ?></font></td>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap>
						<select name="trip_<?php echo $rData[$i]["Ymd"]; ?>" id="trip_<?php echo $rData[$i]["Ymd"]; ?>">
							<option value=""></option>
							<?php $selected = $rData[$i]["trip_flag"] === "1" ? "selected":""; ?>
							<option value="1" <?php echo $selected; ?> >有</option>
						</select>
					</td>
<? } ?>
				</tr>
<?php
}
?>				
				<tr>
					<td align="center" nowrap width="56" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
					<td align="center" nowrap width="18"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜<br>日</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
					<td align="center" nowrap width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請状態</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議<br>研修</font></td>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間<br>有休</font></td>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出張<br>有無</font></td>
<? } ?>
				</tr>
				<tr>
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap height="3" colspan="8"></td>
<? } else { ?>
					<td align="center" nowrap height="3" colspan="7"></td>
<? } ?>
				</tr>
				<tr>
					<td align="right" nowrap height="22" colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo DateUtil::convert_time($ttlMeeting); ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo DateUtil::convert_time($ttlTimehol); ?></font></td>
<?php if ($opt_trip_flag) { ?>
					<td/>
<? } ?>
				</tr>
				
			</table>
		</td>
		<td valign="top">
<div style="overflow-x:scroll; overflow-y:hidden; overflow:auto; border:#5279a5 solid 0px; width:520px">
			<table border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="center" nowrap width="90" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外1</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由1</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩1</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外2</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由2</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩2</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外3</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計<br>(日次)</font></td>
				</tr>
    <?php
    for($i=0;$i<count($rData);$i++){
?>				
				<tr bgcolor="<?echo $rData[$i]["bgcolor"]; ?>">
<?php if ($opt_trip_flag) { ?>
					<td align="center" nowrap height="30">
<? } else { ?>
					<td align="center" nowrap height="24">
<? } ?>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_t1"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_r1"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_b1"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_t2"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_r2"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_b2"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_t3"]; ?></font></td>
					<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_r3"]; ?></font></td>
					<td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_b3"]; ?></font></td>
					<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ovtm_time"]; ?></font></td>
				</tr>
        <?php
    }
?>				
				<tr>
					<td align="center" nowrap width="90" height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外1</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由1</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩1</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外2</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由2</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩2</font></td>
					<td align="center" nowrap width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間外3</font></td>
					<td align="center" nowrap width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩3</font></td>
					<td align="center" nowrap width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計<br>(日次)</font></td>
				</tr>
				<tr>
					<td align="center" nowrap height="3" colspan="<?php echo count($retKbn) + 1 + 9; ?>"></td>
				</tr>
				<tr>
					<td align="right" nowrap height="22" colspan="9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>
					<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo DateUtil::convert_time($ovtm_ttl); ?></font></td>
				</tr>
			</table>
</div>			
		</td>
	</tr>
</table>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績集計</font>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?php
//ふくすうしふと
for($i=0;$i<=max(array_keys($ttlType));$i++){
	$group = "";
	$total = "";
	for($j=0;$j<=max(array_keys($ttlType[$i]));$j++){
		if($ttlType[$i][$j]["cnt"] > 0){
			if(count($ttlType) > 1){
				$group  = $ttlType[$i][$j]["group"]."<br>";
			}
			$total .= $ttlType[$i][$j]["name"]."：".$ttlType[$i][$j]["cnt"]."&nbsp;";
		}
	}
	if($total != ""){
		echo $group.$total."<br>";
	}
}
?>
<br>
<input type="checkbox" name="checked_flg" value="t" <?php echo $checked_flg; ?>>チェック済み&nbsp;&nbsp;&nbsp;<input type="button" value="登録" onclick="return update_checked(<?php echo $chkRecCnt; ?>, <?php echo $month; ?>);">

</font>
<?php
}
?>
<input type="hidden" id="session" name="session"   value="<?php echo $session; ?>">
<input type="hidden" id="emp_id" name="emp_id"    value="<?php echo $emp_id; ?>">

<input type="hidden" name="view"      value="<?php echo $view; ?>">

<input type="hidden" name="yyyymm"    value="<?php echo $yyyymm; ?>">
<input type="hidden" name="srch_id" value="<?php echo $srch_id; ?>">
<input type="hidden" name="srch_name" value="<?php echo $srch_name; ?>">
<input type="hidden" name="cls"       value="<?php echo $cls; ?>">
<input type="hidden" name="atrb"      value="<?php echo $atrb; ?>">
<input type="hidden" name="dept"      value="<?php echo $dept; ?>">
<input type="hidden" name="page"      value="<?php echo $page; ?>">
<input type="hidden" name="group_id"  value="<?php echo $group_id; ?>">

<input type="hidden" name="year"     value="<?php echo $year; ?>">
<input type="hidden" name="month"    value="<?php echo $month; ?>">


<input type="hidden" name="shift_group_id"    value="<?php echo $shift_group_id; ?>">
<input type="hidden" name="csv_layout_id"     value="<?php echo $csv_layout_id; ?>">
<input type="hidden" name="duty_form_jokin"   value="<?php echo $duty_form_jokin; ?>">
<input type="hidden" name="duty_form_hijokin" value="<?php echo $duty_form_hijokin; ?>">
<input type="hidden" name="sus_flg" value="<?php echo $sus_flg; ?>">

<input type="hidden" name="staff_ids" value="<? echo($staff_ids); ?>">
<input type="hidden" name="data_cnt"  value="<? echo($data_cnt); ?>">

<input type="hidden" name="check_flg" value="">
<input type="hidden" name="emp_move_flg"    value="">
<input type="hidden" name="emp_personal_id" value="<? echo($base_info->emp_personal_id); ?>">

<input type="hidden" name="up_mode" value="chk_update">
<input type="hidden" name="wherefrom"  value="<? echo($wherefrom); ?>">

</form>
<?
if ($err_flg == "1") {
	if ($emp_move_flg == "next") {
		$err_msg = "最後の職員です。";
	} else {
		$err_msg = "先頭の職員です。";
	}
	echo("<script type=\"text/javascript\">alert('$err_msg');</script>");
}
?>
</body>
</html>
<? pg_close($con); ?>
