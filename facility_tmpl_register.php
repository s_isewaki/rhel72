<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>マスターメンテナンス | 施設・設備｜テンプレート登録</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("./conf/sql.inf");
require("show_facility.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];

// 職員ID取得 
$emp_id=get_emp_id($con,$session,$fname);

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
	$can_regist_flg = false;
}

?>
<script type="text/javascript">

function submitForm() {
	document.tmpl.submit();
}

function referTemplate() {
	window.open('facility_tmpl_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function submitPreviewForm() {
	document.tmpl.action = "facility_tmpl_register.php";
	document.tmpl.preview_flg.value = "1";
	document.tmpl.submit();
}

function show_preview_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=650,height=700";
	window.open(url, 'preview_window',option);
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a> &gt; <a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">

<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="facility_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_category_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_master_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_master_type.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="facility_tmpl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>テンプレート登録</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="tmpl" action="facility_tmpl_insert.php" method="post">
<table width="580" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート名称</font></td>
<td><input name="tmpl_title" type="text" size="50" maxlength="80" value="<? echo($tmpl_title); ?>" style="ime-mode: active;"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<table width="580" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="left" width="100">

<table width="100" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td align="left" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本文</font>
&nbsp;
<input type="button" name="referbtn" value="参照" onclick="referTemplate();">
</td>
</tr>
</table>

</td>
<td align="right">
<input type="button" name="prevbtn" value="プレビュー" onclick="submitPreviewForm();">　
<input type="button" name="regist" value="登録" onclick="submitForm();"></td>
</tr>
<tr>
<?
// ファイルから本文を読み込む
$savefilename = "fcl/tmp/{$session}_t.php";
if ($back == "t") {
	$fp = fopen($savefilename, "r");
	if ($fp != false){
		$content = fread($fp, filesize($savefilename));
		fclose($fp);
	}
}
// テキストエリア中に/textareaがあると表示が崩れるため変換する
$content = eregi_replace("/(textarea)", "_\\1", $content);

?>
<td colspan="3"><textarea name="content" rows="20" cols="80"><? echo($content); ?></textarea></td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="preview_flg" value="">
<input type="hidden" name="back" value="">
</form>
</td>
</tr>
</table>
<script type="text/javascript">
<?
// プレビュー押下時
if ($preview_flg == "1") {

	if (!is_dir("fcl")) {
		mkdir("fcl", 0755);
	}
	if (!is_dir("fcl/tmp")) {
		mkdir("fcl/tmp", 0755);
	}
	// content 保存
	$ext = ".php";
	$savefilename = "fcl/tmp/{$session}_t{$ext}";

	// 内容書き込み
	if ($content == "") {
		$content = " ";
	}

	$fp = fopen($savefilename, "w");
	if (!$fp) {
		echo("alert('一時ファイルがオープンできません。再度、登録してください。$savefilename');");
		echo("history.back();</script>");
		exit;
	} else {

		if(!fwrite($fp, $content, 2000000)) {
			fclose($fp);
			echo("alert('一時ファイルに書込みできません。再度、登録してください。');");
			echo("history.back();</script>");
			exit;
		}
	}

?>
	var url = 'facility_tmpl_preview.php'
		+ '?session=<? echo($session); ?>';
	show_preview_window(url);
<?
}
?>

<?
if ($can_regist_flg == false) {
?>
document.tmpl.regist.disabled = true;
<? } ?>

</script>
</body>
<? pg_close($con); ?>
</html>
