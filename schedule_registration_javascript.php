<?php
function insert_javascript() {
  global $session;
    ?>
	<script src="js/prototype/dist/prototype.js"></script>
    <SCRIPT LANGUAGE="Javascript"><!--

var m_target_list = new Array();
m_target_list["ID"] = new Array();

//--------------------------------------------------
//登録対象者情報
//--------------------------------------------------
function user_info(emp_id,emp_name)
{
	this.emp_id = emp_id;
	this.emp_name = emp_name;
}

//--------------------------------------------------
//登録対象者追加 引数：", "で区切られたID、名前
//--------------------------------------------------
//※この関数は他の画面からも呼ばれます。
function add_target_list(emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");

	//追加
	for(i=0;i<emp_ids.length;i++){
		m_target_list["ID"] = array_add(m_target_list["ID"],new user_info(emp_ids[i],emp_names[i]));

	}

	//登録対象者一覧の重複除去
	single_target_list();
	//HTML反映処理
	update_target_html();
	if (window.setMailDisabled) {
		setMailDisabled();
	}
}

//--------------------------------------------------
//登録対象者を削除します。
//--------------------------------------------------
function delete_target(emp_id,emp_name)
{
	if(confirm("「" + emp_name + "」を登録対象者から削除します。よろしいですか？"))
	{
		delete_target_list(emp_id);
		
		//HTML反映処理
		update_target_html();
		if (window.setMailDisabled) {
			setMailDisabled();
		}
	}
}

//--------------------------------------------------
//m_target_listより登録対象者を削除します。
//--------------------------------------------------
//※ここではHTML反映は行わないため、update_target_html()を実行する必要あり。
function delete_target_list(emp_id)
{
	var new_array = new Array();
	for(var i=0;i<m_target_list["ID"].length;i++)
	{
		if(emp_id != m_target_list["ID"][i].emp_id)
		{
			new_array = array_add(new_array,m_target_list["ID"][i]);
		}
	}
	m_target_list["ID"] = new_array;
}

//--------------------------------------------------
//m_target_listの登録対象者の重複をなくします。
//--------------------------------------------------
function single_target_list()
{
	var new_target_list = new Array();
	new_target_list["ID"] = new Array();
	var emp_id_csv = "";
	for(var i = 0; i < m_target_list["ID"].length; i++)
	{
		var emp_id = m_target_list["ID"][i].emp_id;
		if(emp_id_csv.indexOf(emp_id) == -1)
		{
			//重複していない。
			emp_id_csv = emp_id_csv + "," + emp_id;
			new_target_list["ID"] = array_add(new_target_list["ID"],m_target_list["ID"][i]);
		}
	}
	m_target_list = new_target_list;
}

//--------------------------------------------------
//m_target_listより登録対象者のHTMLを再生成します。
//--------------------------------------------------
function update_target_html()
{
	//登録対象者HTMLを更新
	_update_target_html2(document.getElementById("target_disp_area"),document.getElementById("target_id_list"),document.getElementById("target_name_list"));
}

function _update_target_html2(disp_obj,target_hidden_id,target_hidden_name)
{
	if(m_target_list["ID"].length == 0)
	{
		disp_obj.innerHTML = "";
		target_hidden_id.value = "";
		target_hidden_name.value = "";
	}
	else
	{
		var disp_obj_html = "";
		var target_hidden_id_value = "";
		var target_hidden_name_value = "";
		for(var i=0;i<m_target_list["ID"].length;i++)
		{
			var emp_id   = m_target_list["ID"][i].emp_id;
			var emp_name = m_target_list["ID"][i].emp_name;
			if(i!=0)
			{
				disp_obj_html = disp_obj_html + ",";
				target_hidden_id_value = target_hidden_id_value + ",";
				target_hidden_name_value = target_hidden_name_value + ",";
			}
			
//			disp_obj_html = disp_obj_html + "<a href=\"javascript:delete_target('" + emp_id + "','" + emp_name + "')\">" + emp_name + "</a>";
			disp_obj_html = disp_obj_html + "<a href=\"javascript:void(0);\" onClick=\"delete_target('" + emp_id + "','" + emp_name + "')\">" + emp_name + "</a>";
			
			target_hidden_id_value = target_hidden_id_value + emp_id;
			target_hidden_name_value = target_hidden_name_value + emp_name;
		}
		disp_obj.innerHTML = disp_obj_html;
		target_hidden_id.value = target_hidden_id_value;
		target_hidden_name.value = target_hidden_name_value;
	}
}


//--------------------------------------------------
//配列追加
//--------------------------------------------------
function array_add(array_obj,add_obj)
{
	return array_obj.concat( new Array( add_obj ) );
}

// クリア
function clear_target(emp_id,emp_name) {
	if(confirm("自分以外の登録対象者を削除します。よろしいですか？"))
	{
		m_target_list["ID"] = new Array();
		m_target_list["ID"] = array_add(m_target_list["ID"],new user_info(emp_id,emp_name));
		update_target_html();
		if (window.setMailDisabled) {
			setMailDisabled();
		}
	}
}

function clear_target_all() {
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		m_target_list["ID"] = new Array();
		update_target_html();
		if (window.setMailDisabled) {
			setMailDisabled();
		}
	}
}

function selectAllEmp() {
	for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
		document.mainform.emplist.options[i].selected = true;
	}
}


//--------------------------------------------------
//m_target_list形式データをデバッグします。
//--------------------------------------------------
function debug_target_list(target_list)
{
	var str = "[ID]\n";
	if(target_list["ID"].length != 0)
	{
		for(var i=0;i<target_list["ID"].length;i++)
		{
			str = str + target_list["ID"][i].emp_id + "(" + target_list["ID"][i].emp_name + ")" + "\n";
		}
	}
	alert(str);
}

	function set_wm_counter(ids)
	{
		
		var url = 'emplist_address_counter.php';
		var params = $H({'session':'<?=$session?>','emp_ids':ids}).toQueryString();
		var myAjax = new Ajax.Request(
			url, 
			{
				method: 'post',
				postBody: params
			});
	}

// --></SCRIPT>
<?php
} /* End of included JavaScript */

?>
