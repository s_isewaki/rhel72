<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix Excel印刷オプション</TITLE>

<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

$arr_val = array();
$arr_val = $obj->conf_get_group($group_id);

// 月曜日のリストを作る 2012.8.23
//保存された設定の日にちが同じならselectedとする
$select_date = "";
if ($arr_val["selectweek"] != "") {
    $select_date = $arr_val["selectweek"];
}
$monday_list = "";
$duty_list=explode(",",$duty_list);
$day=$duty_start;
foreach($duty_list as $num=>$monday){
    $selected = ($day == $select_date) ? " selected" : "";
	$monday_list.="<OPTION VALUE=\"".$day."\" $selected>".$monday."(月)</OPTION>\n";
	$day+=7;
}

//4週単位未指定の場合。呼出画面の年月を使用したいため月単位リスト処理より先に行う
if ($arr_val["week4start"] == "") {
    $year = $duty_yyyy;
    $month = $duty_mm;
    $day = '01';
}
else {
    $year = substr($arr_val["week4start"], 0, 4);
    $month = substr($arr_val["week4start"], 4, 2);
    $day = substr($arr_val["week4start"], 6, 4);
}

// 月単位のリストを作る 20130219
if ($arr_val["selectmonth"] != "") {
    list($duty_yyyy, $duty_mm) = explode("-", $arr_val["selectmonth"]);
}
$month_list = "";
for($i = -5; $i < 8; $i++)
{
	$buf = date("Y-m",strtotime("$i month" ,strtotime($duty_yyyy. "/". $duty_mm. "/1")));
	if ($i == 0)
	{
		$month_list .= "<OPTION VALUE=\"".$buf."\" selected>". $buf. "</OPTION>\n";
	}else{
		$month_list .= "<OPTION VALUE=\"".$buf."\">". $buf. "</OPTION>\n";
	}
}

?>
<style type="text/css">
    td {
        font-size: 13px;
    }
</style>
<script type="text/javascript">
    resizeTo(550, 750);

    function initPage() {
        setDayOptions('<? echo($day); ?>');
<?
//1段の場合、無効化
if ($arr_val["setRowCount"] != 2) {
?>
	    document.getElementById("050").setAttribute("disabled","enable");
	    document.getElementById("051").setAttribute("disabled","enable");
	    document.getElementById("052").setAttribute("disabled","enable");
	    document.getElementById("053").setAttribute("disabled","enable");
	    document.getElementById("054").setAttribute("disabled","enable");
<? } ?>
<?
//週単位未指定の場合、無効化
if ($arr_val["selectweek"] == "") {
?>
	    document.getElementById("062").setAttribute("disabled","enable");
    <?
}
else {
?>
	    document.getElementById("101").setAttribute("disabled","enable");
	    document.getElementById("111").setAttribute("disabled","enable");
    <?
}
 ?>
<?
//月単位未指定の場合、無効化
if ($arr_val["selectmonth"] == "") {
?>
	    document.getElementById("102").setAttribute("disabled","enable");
<?
}
else {
?>
	    document.getElementById("061").setAttribute("disabled","enable");
	    document.getElementById("111").setAttribute("disabled","enable");
    <?
}
 ?>
<?
//4週単位未指定の場合、無効化
if ($arr_val["week4start"] == "") {
        ?>
	    document.getElementById("112").setAttribute("disabled","enable");
	    document.getElementById("113").setAttribute("disabled","enable");
	    document.getElementById("114").setAttribute("disabled","enable");
<? }
else {
?>
	    document.getElementById("061").setAttribute("disabled","enable");
	    document.getElementById("101").setAttribute("disabled","enable");
    <?
}

 ?>
    }
	// 0.1単位で加減したいが
	// jsも小数点の連続演算は10進で正しくないため
	// 10倍して加減後に10で割る。
	function AddVal(_value){
		_value = _value * 10;
		_value = _value + 1;
		_value = _value / 10;
		return (_value);
	}
	function SubVal(_value){
		_value = _value * 10;
		_value = _value - 1;
		_value = _value / 10;
		if( _value <= 0 ){_value = 0;}
		return (_value);
	}
	function OkBtn(){
		var paper_size = 'A4';
		var setfitpage = 'AUTO';
		var ratio,mtop,mleft,mright,mbottom,mhead,mfoot,blancrows,weekcolor,hopecolor;
		var disp;
		if( document.excelparam.setPapaerSize[0].checked ){
			paper_size = 'A3';
		}
		if( document.excelparam.setPapaerSize[1].checked ){
			paper_size = 'A4';
		}
		if( document.excelparam.setPapaerSize[2].checked ){
			paper_size = 'B4';
		}
		if( document.excelparam.setFitPage[0].checked ){
			setfitpage = 'USER';
		}

		if( document.excelparam.setRowCount[0].checked ){
			set_row_count = '1';
		}
		if( document.excelparam.setRowCount[1].checked ){
			set_row_count = '2';
		}

		if( document.excelparam.rowType[0].checked ){
			set_row_type = '0';
		}
		if( document.excelparam.rowType[1].checked ){
			set_row_type = '1';
		}
		if( document.excelparam.rowType[2].checked ){
			set_row_type = '2';
		}
		if( document.excelparam.rowType[3].checked ){
			set_row_type = '3';
		}
		if( document.excelparam.rowType[4].checked ){
			set_row_type = '4';
		}

		if( document.excelparam.pageSet[0].checked ){
			ratio=document.excelparam.ratio.value;
			mtop=document.excelparam.marginTop.value;
			mleft=document.excelparam.marginLeft.value;
			mright=document.excelparam.marginRight.value;
			mbottom=document.excelparam.marginBottom.value;
			mhead=document.excelparam.marginHead.value;
			mfoot=document.excelparam.marginFoot.value;
			if ( document.excelparam.BlancSet[0].checked ){
				blancrows = 1;
			}else{
				blancrows = 0;
			}
			if ( document.excelparam.weekColor[0].checked ){
				weekcolor = 1;
			}else{
				weekcolor = 0;
			}
			if ( document.excelparam.hopeColor[0].checked ){
				hopecolor = 1;
			}else{
				hopecolor = 0;
			}
			if ( document.excelparam.event[0].checked ){
				event_flg = 1;
			}else{
				event_flg = 0;
			}
			if( document.excelparam.setWeek.checked){
				var selectweek = document.excelparam.selectweek.value;
				parent.opener.makeExcel5_1_6week(ratio,paper_size,setfitpage,mtop,mbottom,mleft,mright,mhead,mfoot,set_row_count,set_row_type,blancrows,selectweek,weekcolor,hopecolor,event_flg);
			}else{
				var selectmonth =  (document.excelparam.setMonth.checked) ? document.excelparam.selectmonth.value : "";
                var week4start = (document.excelparam.setWeek4.checked) ? document.excelparam.selectyear_w4.value + document.excelparam.selectmonth_w4.value  + document.excelparam.selectday_w4.value : "";
				parent.opener.makeExcel5_1_6(ratio,paper_size,setfitpage,mtop,mbottom,mleft,mright,mhead,mfoot,set_row_count,set_row_type,blancrows,weekcolor,hopecolor,selectmonth,week4start,event_flg);
			}

		}else{
			parent.opener.makeExcel4();
		}
		window.close();
	}
	function radioChange() {
		elements = new Array();
		elements[14] = document.getElementById("050");
		elements[15] = document.getElementById("051");
		elements[16] = document.getElementById("052");
		elements[17] = document.getElementById("053");
		elements[18] = document.getElementById("054");
		if(document.excelparam.setRowCount[0].checked) {
			for(i=14; i<19; i++){
				elements[i].setAttribute("disabled","disabled");
			}
		}
		if(document.excelparam.setRowCount[1].checked) {
			for(i=14; i<19; i++){
				elements[i].removeAttribute("disabled");
			}
		}
	}
	function pageChange() {
		var i=0;
		elements=new Array();
		elements[i]=document.getElementById("010");i++;
		elements[i]=document.getElementById("011");i++;
		elements[i]=document.getElementById("012");i++;
		elements[i]=document.getElementById("020");i++;
		elements[i]=document.getElementById("021");i++;
		elements[i]=document.getElementById("022");i++;
		elements[i]=document.getElementById("030");i++;
		elements[i]=document.getElementById("031");i++;
		elements[i]=document.getElementById("032");i++;
		elements[i]=document.getElementById("033");i++;
		elements[i]=document.getElementById("034");i++;
		elements[i]=document.getElementById("035");i++;
		elements[i]=document.getElementById("040");i++;
		elements[i]=document.getElementById("041");i++;
		elements[i]=document.getElementById("050");i++;
		elements[i]=document.getElementById("051");i++;
		elements[i]=document.getElementById("052");i++;
		elements[i]=document.getElementById("053");i++;
		elements[i]=document.getElementById("054");i++;
		elements[i]=document.getElementById("061");i++;
		elements[i]=document.getElementById("070");i++;
		elements[i]=document.getElementById("071");i++;
		elements[i]=document.getElementById("080");i++;
		elements[i]=document.getElementById("081");i++;
		elements[i]=document.getElementById("090");i++;
		elements[i]=document.getElementById("091");i++;
		elements[i]=document.getElementById("101");i++;
		elements[i]=document.getElementById("111");i++;
		elements[i]=document.getElementById("121");i++;
		var element_lengrh = elements.length;
		if(document.excelparam.pageSet[0].checked) {
			for(i=0; i<element_lengrh; i++){
				elements[i].removeAttribute("disabled");
			}
		}
		if(document.excelparam.pageSet[1].checked) {
			for(i=0; i<element_lengrh; i++){
				elements[i].setAttribute("disabled","disabled");
			}
		}
		if(document.excelparam.setRowCount[0].checked) {
			for(i=14; i<19; i++){
				elements[i].setAttribute("disabled","disabled");
			}
		}
	}
	function weekChange() {
		if( document.excelparam.setWeek.checked){
			document.getElementById("062").removeAttribute("disabled");
			document.getElementById("101").setAttribute("disabled","enable");
			document.getElementById("111").setAttribute("disabled","enable");
		}
		if( !document.excelparam.setWeek.checked){
			document.getElementById("062").setAttribute("disabled","enable");
			document.getElementById("101").removeAttribute("disabled");;
			document.getElementById("111").removeAttribute("disabled");;
			document.getElementById("112").setAttribute("disabled","enable");
			document.getElementById("113").setAttribute("disabled","enable");
			document.getElementById("114").setAttribute("disabled","enable");
		}
	}
	function monthChange() {
		if( document.excelparam.setMonth.checked){
			document.getElementById("102").removeAttribute("disabled");
			document.getElementById("061").setAttribute("disabled","enable");
			document.getElementById("111").setAttribute("disabled","enable");
		}
		if( !document.excelparam.setMonth.checked){
			document.getElementById("102").setAttribute("disabled","enable");
			document.getElementById("061").removeAttribute("disabled");;
			document.getElementById("111").removeAttribute("disabled");;
			document.getElementById("112").setAttribute("disabled","enable");
			document.getElementById("113").setAttribute("disabled","enable");
			document.getElementById("114").setAttribute("disabled","enable");
		}
	}
	function week4Change() {
		if( document.excelparam.setWeek4.checked){
			document.getElementById("061").setAttribute("disabled","enable");
			document.getElementById("101").setAttribute("disabled","enable");
			document.getElementById("112").removeAttribute("disabled");
			document.getElementById("113").removeAttribute("disabled");
			document.getElementById("114").removeAttribute("disabled");
		}
		if( !document.excelparam.setWeek4.checked){
			document.getElementById("061").removeAttribute("disabled");
			document.getElementById("101").removeAttribute("disabled");
			document.getElementById("112").setAttribute("disabled","enable");
			document.getElementById("113").setAttribute("disabled","enable");
			document.getElementById("114").setAttribute("disabled","enable");
		}
	}

function setDayOptions(selectedDay) {
    var dayPulldown = document.excelparam.selectday_w4;
    if (!selectedDay) {
        selectedDay = dayPulldown.value;
    }
    deleteAllOptions(dayPulldown);

    var year = parseInt(document.excelparam.selectyear_w4.value, 10);
    var month = parseInt(document.excelparam.selectmonth_w4.value, 10);
    var daysInMonth = new Date(year, month, 0).getDate();

    for (var d = 1; d <= daysInMonth; d++) {
        var tmpDayValue = d.toString();
        if (tmpDayValue.length == 1) {
            tmpDayValue = '0'.concat(tmpDayValue);
        }

        var tmpDayText = d.toString();

        var tmpDate = new Date(year, month - 1, d);
        var tmpWeekDay = tmpDate.getDay();
        switch (tmpWeekDay) {
        case 0:
            tmpDayText = tmpDayText.concat('（日）');
            break;
        case 1:
            tmpDayText = tmpDayText.concat('（月）');
            break;
        case 2:
            tmpDayText = tmpDayText.concat('（火）');
            break;
        case 3:
            tmpDayText = tmpDayText.concat('（水）');
            break;
        case 4:
            tmpDayText = tmpDayText.concat('（木）');
            break;
        case 5:
            tmpDayText = tmpDayText.concat('（金）');
            break;
        case 6:
            tmpDayText = tmpDayText.concat('（土）');
            break;
        }

        addOption(dayPulldown, tmpDayValue, tmpDayText);
    }

    while (parseInt(selectedDay, 10) > daysInMonth) {
        selectedDay = (parseInt(selectedDay, 10) - 1).toString();
    }
    dayPulldown.value = selectedDay;
}
function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}
function getSelectedValue(sel) {
    return sel.options[sel.selectedIndex].value;
}
</script>
</head>
<BODY onload="initPage();">
<SPAN ID="DEBUG"></SPAN><? echo $DEBUG; ?>
<CENTER>
<FORM ACTION="javascript:OkBtn();" NAME="excelparam" METHOD="POST">

<TABLE width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<TR BGCOLOR="#5279a5" height="32"><TD class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>ＥＸＣＥＬ印刷パラメータ指定</b></font></TD>
<TD width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></TD><TR>
</TABLE>
<TABLE BORDER="0" cellspacing="1">
<TR><TD COLSPAN="6">
<BR>
<label><INPUT TYPE="radio" NAME="pageSet" VALUE="Y" onclick="pageChange()" checked >ページ設定有効</label>
<label><INPUT TYPE="radio" NAME="pageSet" VALUE="N" onclick="pageChange()">ページ設定無効</label>
</TD></TR>

<TR><TD COLSPAN="6" style="padding-top:8px;">

曜日の下の空行<BR>
<label><INPUT TYPE="radio" NAME="BlancSet" VALUE="Y" ID="070" <? if ($arr_val["BlancSet"] == 1) { echo "checked"; } ?>>挿入する</label>
<label><INPUT TYPE="radio" NAME="BlancSet" VALUE="N" ID="071" <? if ($arr_val["BlancSet"] != 1) { echo "checked"; } ?>>無し</label>
</TD></TR>

<TR><TD COLSPAN="6" style="padding-top:8px;">
曜日の背景色<BR>
<label><INPUT TYPE="radio" NAME="weekColor" VALUE="Y" ID="080" <? if ($arr_val["weekColor"] != "0") { echo "checked"; } ?>>表示する</label>
<label><INPUT TYPE="radio" NAME="weekColor" VALUE="N" ID="081" <? if ($arr_val["weekColor"] == "0") { echo "checked"; } ?>>表示しない</label>
</TD></TR>

<TR><TD COLSPAN="6" style="padding-top:8px;">
勤務希望の色<BR>
<label><INPUT TYPE="radio" NAME="hopeColor" VALUE="Y" ID="090" <? if ($arr_val["hopeColor"] == 1) { echo "checked"; } ?>>表示する</label>
<label><INPUT TYPE="radio" NAME="hopeColor" VALUE="N" ID="091" <? if ($arr_val["hopeColor"] != 1) { echo "checked"; } ?>>表示しない</label>
</TD></TR>

<TR><TD COLSPAN="6" style="padding-top:8px;">
行事予定<BR>
<label><INPUT TYPE="radio" NAME="event" VALUE="Y" ID="120" <? if ($arr_val["event"] == 1) { echo "checked"; } ?>>表示する</label>
<label><INPUT TYPE="radio" NAME="event" VALUE="N" ID="121" <? if ($arr_val["event"] != 1) { echo "checked"; } ?>>表示しない</label>
</TD></TR>

<TR><TD COLSPAN="6" style="padding-top:8px;">縮小拡大</TD></TR>
<TR><TD COLSPAN=6><label><INPUT TYPE="radio" NAME="setFitPage" VALUE="USER" ID="010" <? if ($arr_val["setFitPage"] == "USER") { echo "checked"; } ?>>拡大/縮小</label>　
<input type=text size=3 name=ratio value="<? if ($arr_val["ratio"] != "") { echo $arr_val["ratio"]; } else { echo "100"; } ?>"  ID="011">%
    <input type=button value="+" onclick="ratio.value=eval(ratio.value)+5">
    <input type=button value="-" onclick="ratio.value=eval(ratio.value)-5">
</TD></TR>
<TR><TD COLSPAN=6><label><INPUT TYPE="radio" NAME="setFitPage" VALUE="AUTO" ID="012" <? if ($arr_val["setFitPage"] != "USER") { echo "checked"; } ?>>ページに合わせて自動拡大/縮小</label>
</TD></TR>
<TR><TD COLSPAN=6><BR>用紙サイズ</TD></TR>
<TR>
<TD COLSPAN=6>
<label><INPUT TYPE="radio" NAME="setPapaerSize" VALUE="A3" ID="020" <? if ($arr_val["setPapaerSize"] == "A3" || $arr_val["setPapaerSize"] == "") { echo "checked"; } ?>>A3</label>
<label><INPUT TYPE="radio" NAME="setPapaerSize" VALUE="A4" ID="021" <? if ($arr_val["setPapaerSize"] == "A4") { echo "checked"; } ?>>A4</label>
<label><INPUT TYPE="radio" NAME="setPapaerSize" VALUE="B4" ID="022" <? if ($arr_val["setPapaerSize"] == "B4") { echo "checked"; } ?>>B4</label>
</TD></TR>
<TR><TD COLSPAN="6" style="padding-top:8px;">
余白<BR>
</TD></TR>
<TR><TD>上</TD><TD>
<input ID="030" type=text size=3 name="marginTop" value="<? if ($arr_val["marginTop"] != "") { echo $arr_val["marginTop"]; } else { echo "2.5"; } ?>">
    <input type=button value="+" onclick="marginTop.value=AddVal(marginTop.value)">
    <input type=button value="-" onclick="marginTop.value=SubVal(marginTop.value)">

</TD>
<TD>&nbsp;&nbsp;左</TD><TD>
<input ID="031" type=text size=3 name=marginLeft value="<? if ($arr_val["marginLeft"] != "") { echo $arr_val["marginLeft"]; } else { echo "2.5"; } ?>">
    <input type=button value="+" onclick="marginLeft.value=AddVal(marginLeft.value)">
    <input type=button value="-" onclick="marginLeft.value=SubVal(marginLeft.value)">

</TD>
<TD>&nbsp;&nbsp;ヘッダ</TD><TD>
<input ID="032" type=text size=3 name=marginHead value="<? if ($arr_val["marginHead"] != "") { echo $arr_val["marginHead"]; } else { echo "2.5"; } ?>">
    <input type=button value="+" onclick="marginHead.value=AddVal(marginHead.value)">
    <input type=button value="-" onclick="marginHead.value=SubVal(marginHead.value)">

</TD>
</TR>
<TR>
<TD>下</TD><TD>
<input ID="033" type=text size=3 name=marginBottom value="<? if ($arr_val["marginBottom"] != "") { echo $arr_val["marginBottom"]; } else { echo "2.5"; } ?>">
    <input type=button value="+" onclick="marginBottom.value=AddVal(marginBottom.value)">
    <input type=button value="-" onclick="marginBottom.value=SubVal(marginBottom.value)">

</TD>
<TD>&nbsp;&nbsp;右</TD><TD>
<input ID="034" type=text size=3 name=marginRight value="<? if ($arr_val["marginRight"] != "") { echo $arr_val["marginRight"]; } else { echo "2.5"; } ?>">
    <input type=button value="+" onclick="marginRight.value=AddVal(marginRight.value)">
    <input type=button value="-" onclick="marginRight.value=SubVal(marginRight.value)">

</TD>
<TD>&nbsp;&nbsp;フッタ</TD><TD>
<input ID="035" type=text size=3 name=marginFoot value="<? if ($arr_val["marginFoot"] != "") { echo $arr_val["marginFoot"]; } else { echo "2.5"; } ?>">
    <input type=button value="+" onclick="marginFoot.value=AddVal(marginFoot.value)">
    <input type=button value="-" onclick="marginFoot.value=SubVal(marginFoot.value)">

</TD>
</TR>

<TR><TD COLSPAN="6" style="padding-top:8px;">
１職員に対する表示<BR>
</TD></TR>
<TR><TD COLSPAN=6>
<label><INPUT TYPE="radio" NAME="setRowCount" VALUE="0" onclick="radioChange()" ID="040" <? if ($arr_val["setRowCount"] != 2) { echo "checked"; } ?>>１段表示</label>
<label><INPUT TYPE="radio" NAME="setRowCount" VALUE="1" onclick="radioChange()" ID="041" <? if ($arr_val["setRowCount"] == 2) { echo "checked"; } ?>>２段表示</label>
<BR>
<label><INPUT TYPE="radio" NAME="rowType" VALUE="0" ID="050" <? if ($arr_val["rowType"] == "0" || $arr_val["rowType"] == "") { echo "checked"; } ?>>空行</label>
<label><INPUT TYPE="radio" NAME="rowType" VALUE="1" ID="051" <? if ($arr_val["rowType"] == "1") { echo "checked"; } ?>>実績</label>
<label><INPUT TYPE="radio" NAME="rowType" VALUE="2" ID="052" <? if ($arr_val["rowType"] == "2") { echo "checked"; } ?>>勤務希望</label>
<label><INPUT TYPE="radio" NAME="rowType" VALUE="3" ID="053" <? if ($arr_val["rowType"] == "3") { echo "checked"; } ?>><? echo($obj->duty_or_oncall_str); ?></label>
<label><INPUT TYPE="radio" NAME="rowType" VALUE="4" ID="054" <? if ($arr_val["rowType"] == "4") { echo "checked"; } ?>>コメント</label>
</TD></TR>

<TR><TD COLSPAN="6" style="padding-top:8px;">
<label><INPUT TYPE="CHECKBOX" NAME="setWeek" onclick="weekChange()" ID="061" <? if ($arr_val["selectweek"] != "") { echo "checked"; } ?>>週単位で出力する</label>&nbsp;&nbsp;&nbsp;
 <SELECT NAME="selectweek" ID="062"><? echo $monday_list; ?></select>から１週間
</TD></TR>

<!-- 月単位対応 20130219 -->
<TR><TD COLSPAN="6" style="padding-top:8px;">
<label><INPUT TYPE="CHECKBOX" NAME="setMonth" onclick="monthChange()" ID="101" <? if ($arr_val["selectmonth"] != "") { echo "checked"; } ?>>月単位で出力する</label>&nbsp;&nbsp;&nbsp;
 <SELECT NAME="selectmonth" ID="102"><? echo $month_list; ?></select>
</TD></TR>

<? /* エクセルで4週分出力  */ ?>
<TR><TD COLSPAN="6" style="padding-top:8px;">
<label><INPUT TYPE="CHECKBOX" NAME="setWeek4" onclick="week4Change()" ID="111" <? if ($arr_val["week4start"] != "") { echo "checked"; } ?>>４週単位で出力する</label>&nbsp;
 <SELECT NAME="selectyear_w4" ID="112" onchange="setDayOptions();"><? show_select_years_span($year-5, date("Y") + 1, $year); ?></select>-
 <SELECT NAME="selectmonth_w4" ID="113" onchange="setDayOptions();"><? show_select_months($month); ?></select>-
 <SELECT NAME="selectday_w4" ID="114"></select>から４週間
</TD></TR>

</TABLE>
<BR>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" align="center">
<INPUT TYPE="submit" VALUE="　　ＯＫ　　" >
<INPUT TYPE="button" VALUE="キャンセル" onclick="window.close();">
</td>
</tr>
</table>

</FORM>
</CENTER>

</BODY>
</HTML>
