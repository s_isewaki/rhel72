<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜登録内容選択</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("show_select_years_wareki.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 患者検索URLを取得
$sql = "select linkage_url from ptadm";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$linkage_url = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "linkage_url") : "";

// 病棟名を取得
$sql = "select ward_name from wdmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ward_name = pg_fetch_result($sel, 0, "ward_name");

// 病室名を取得
$sql = "select ptrm_name from ptrmmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ptrm_name = pg_fetch_result($sel, 0, "ptrm_name");

// 選択病床への入院予定を取得
$sql = "select ptif_id, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_in_res_dt, inpt_in_res_tm from inptres";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no and inpt_bed_no = '$bed_no' and inpt_in_res_dt_flg = 't'";
$sel_in = select_from_table($con, $sql, $cond, $fname);
if ($sel_in == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 選択病床への移転予定を取得
$sql = "select inptmove.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, inptmove.move_dt, inptmove.move_tm from inptmove inner join ptifmst on inptmove.ptif_id = ptifmst.ptif_id";
$cond = "where inptmove.to_bldg_cd = $bldg_cd and inptmove.to_ward_cd = $ward_cd and inptmove.to_ptrm_room_no = $room_no and inptmove.to_bed_no = '$bed_no' and inptmove.move_cfm_flg = 'f' and inptmove.move_del_flg = 'f'";
$sel_mv = select_from_table($con, $sql, $cond, $fname);
if ($sel_mv == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 予定情報をマージして日時でソート
$reserve_list = array();
while ($row = pg_fetch_array($sel_in)) {
	$reserve_info = array();
	$reserve_info["ptif_id"] = $row["ptif_id"];
	$reserve_info["pt_nm"] = $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"];
	$reserve_info["type"] = "入院予定";
	$reserve_info["datetime"] = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["inpt_in_res_dt"]) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["inpt_in_res_tm"]);
	$reserve_info["button"] = "入院";
	$reserve_info["js"] = "registerIn";
	$reserve_list[] = $reserve_info;
}
while ($row = pg_fetch_array($sel_mv)) {
	$reserve_info = array();
	$reserve_info["ptif_id"] = $row["ptif_id"];
	$reserve_info["pt_nm"] = $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"];
	$reserve_info["type"] = "転床予定";
	$reserve_info["datetime"] = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["move_dt"]) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["move_tm"]);
	$reserve_info["button"] = "転床";
	$reserve_info["js"] = "registerMove";
	$reserve_list[] = $reserve_info;
}
usort($reserve_list, "sort_reserve_list");

function sort_reserve_list($reserve_info1, $reserve_info2) {
	return strcmp($reserve_info1["datetime"], $reserve_info2["datetime"]);
}

// 初期表示時は和暦
if ($back != "t") {
	$wareki_flg = "t";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registerIn(ptif_id) {
	opener.location.href = 'inpatient_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id + '&ward=<? echo("{$bldg_cd}-{$ward_cd}"); ?>&ptrm=<? echo($room_no); ?>&bed=<? echo($bed_no); ?>&bedundec1=f&bedundec2=f&bedundec3=f&path=3';
	window.close();
}

function registerMove(ptif_id) {
	location.href = 'inpatient_move_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id;
}

function wareki_flg_onchange() {
	if (document.patient.wareki_flg.checked == true) {
		birth_yr_onchange();
		document.getElementById('year_div').style.display = "none";
		document.getElementById('year_wareki_div').style.display = "";
	} else {
		document.getElementById('year_div').style.display = "";
		document.getElementById('year_wareki_div').style.display = "none";
	}
}

<?
// 改元データを取得
$era_data = get_era_data();
$era_ymd_str = "";
$gengo_str = "";
for ($i = 1; $i < count($era_data); $i++) {
	if ($i > 1) {
		$era_ymd_str .= ",";
		$gengo_str .= ",";
	}
	$split_data = split(",", $era_data[$i]);
	$era_ymd_str .= "'".$split_data[0]."'";
	$gengo_str .= "'".$split_data[1]."'";
}
?>
var ymd_arr = new Array(<?=$era_ymd_str?>);
var gengo_arr = new Array(<?=$gengo_str?>);

function birth_yr_onchange() {
	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		if (year == ymd_arr[i].substring(0,4)) {
			find_idx = i;
			break;
		}
	}

	document.patient.birth_yr2.value = year;
	if (find_idx > -1 && ymd < ymd_arr[find_idx]) {
		document.patient.birth_yr2.selectedIndex += 1;
	}
}

function birth_yr2_onchange() {
	document.patient.birth_yr.value = document.patient.birth_yr2.value;
}

function check_wareki() {

	if (document.patient.wareki_flg.checked == false) {
		return true;
	}

	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		ymp_tmp = ymd_arr[i].substring(0,4);
		if (year == ymp_tmp) {
			find_idx = i;
			break;
		}
	}

	if (find_idx == -1) {
		return true;
	}

	if (ymd < ymd_arr[find_idx]) {
		gengo = gengo_arr[find_idx + 1];
	} else {
		gengo = gengo_arr[find_idx];
	}

	obj = document.getElementById("birth_yr2");
	var wareki_tmp = obj.options[obj.selectedIndex].text;
	select_gengo = wareki_tmp.substring(0,2);
	if (select_gengo != gengo) {
		alert('年月日を正しく指定してください');
		return false;
	}
	return true;
}

function change_age() {
	var birth_year = document.patient.birth_yr.value;
	var birth_month = document.patient.birth_mon.value;
	var birth_day = document.patient.birth_day.value;

	var today = new Date();
	var this_year = today.getFullYear();
	var this_month = today.getMonth() + 1;
	var this_day = today.getDate();

	var age = this_year - birth_year;
	if (this_month < birth_month || (this_month == birth_month && this_day < birth_day)) {
		age--;
	}

	document.getElementById('age_label').innerHTML = age + '歳';
}

function insertPatient(path) {
	document.patient.path.value = path;
	document.patient.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="wareki_flg_onchange();change_age();">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>登録内容選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病棟</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($ward_name); ?></font></td>
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病室</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($ptrm_name); ?></font></td>
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">ベットNo</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($bed_no); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<? if (count($reserve_list) > 0) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者氏名</font></td>
<td width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">種別</font></td>
<td width="40%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">予定日時</font></td>
<td width="10%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録</font></td>
</tr>
<? foreach ($reserve_list as $reserve_info) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($reserve_info["pt_nm"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($reserve_info["type"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($reserve_info["datetime"]); ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><input type="button" value="<? echo($reserve_info["button"]); ?>" onclick="<? echo($reserve_info["js"]); ?>('<? echo($reserve_info["ptif_id"]); ?>');"></font></td>
</tr>
<? } ?>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<? } ?>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<? if ($linkage_url != "") { ?>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="inpatient_sub_menu2_linkage.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">入院予定・入院</font></a></td>
<? } else { ?>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="inpatient_sub_menu2.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">入院予定・入院</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#5279a5"><a href="inpatient_sub_menu2_patient.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18" color="#ffffff"><b>新規登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_sub_menu2_temp.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">仮抑え</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="patient" action="patient_insert.php" method="post" onsubmit="return check_wareki();">
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者ID</font></td>
<td colspan="3"><input name="pt_id" type="text" value="<? echo $pt_id; ?>" maxlength="20" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">姓（漢字）</font></td>
<td width="42%"><input name="lt_nm" type="text" value="<? echo $lt_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">名（漢字）</font></td>
<td width="26%"><input name="ft_nm" type="text" value="<? echo $ft_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">姓（かな）</font></td>
<td><input name="lt_kana_nm" type="text" value="<? echo $lt_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">名（かな）</font></td>
<td><input name="ft_kana_nm" type="text" value="<? echo $ft_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">生年月日</font></td>
<td>
<span id="year_div">
<select name="birth_yr" onchange="birth_yr_onchange();change_age();">
<? show_select_years(120, $birth_yr);?>
</select></span><span id="year_wareki_div">
<select name="birth_yr2" onchange="birth_yr2_onchange();change_age();">
<? show_select_years_jp(120, $birth_yr2, "", ""); ?>
</select></span><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">/</font><select name="birth_mon" onchange="change_age();"><? show_select_months($birth_mon); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">/</font><select name="birth_day" onchange="change_age();"><? show_select_days($birth_day); ?></select>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<input type="checkbox" name="wareki_flg" value="t" <? if ($wareki_flg == "t") echo(" checked"); ?> onclick="wareki_flg_onchange();">和暦</font>
</td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">年齢</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><span id="age_label"></span></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">性別</font></td>
<td><select name="sex">
<option value="0"<? if ($sex == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($sex == "1") {echo " selected";} ?>>男性
<option value="2"<? if ($sex == "2") {echo " selected";} ?>>女性
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">職業</font></td>
<td><input type="text" name="job" value="<? echo $job; ?>" style="ime-mode:active;"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="4">
<tr height="22">
<td align="right">
<input type="button" value="入院予定登録" onclick="insertPatient('R');">
<input type="button" value="入院登録" onclick="insertPatient('I');">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
<input type="hidden" name="room_no" value="<? echo($room_no); ?>">
<input type="hidden" name="bed_no" value="<? echo($bed_no); ?>">
<input type="hidden" name="path" value="">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
