<?
ob_start();
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_db_class.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//テスト用。
//TODO リリース時はコメントアウト必須。
//ini_set("display_errors","1");

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);

$is_excel = false;
if($_POST['is_postback'] == 'true') {
    switch ($mode) {
    case 'delete' :
        // 論理削除を行う
        foreach($delete_key as $value) {
            $sql = "UPDATE inci_analysis_regist SET ";
            $set = array('available');
            $dat = array('f');
            $cond = "WHERE analysis_id = {$value}";
            $db->update($sql, $set, $dat, $cond);
        }
        break;

    case 'excel':
        $is_excel = true;        
        break;

    default:
        break;
    }
}




//==============================
//ログインしているユーザの職員IDを取得
//==============================
$login_user_id  = get_emp_id($con,$session,$fname);

//SMかチェックする。
//true:ログインしているユーザがSM  false:ログインしているユーザがSMではない
$is_sm = is_inci_auth_emp_of_auth($session,$fname,$con,"SM");




//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if($search_date_year == "")
{
    $search_date_year = date("Y");
}
//日付検索(月)初期値は今月
if($search_date_month == "")
{
    $search_date_month = date("m");
}

//日付最近検索の初期値は２週間
if($search_date_mode == "" && $search_date_late_mode == "")
{
    $search_date_late_mode = "2week";
}

// データ取得
$sql = "SELECT * FROM inci_analysis_regist WHERE available = 't'";

//日付による絞込み
if($search_date_mode == "month")
{
    $search_date_1 = $search_date_year."-".$search_date_month;
    $where .= " and substr(registed_date::text,0,11) like '$search_date_1%' ";
}
elseif($search_date_mode == "ymd_st_ed")
{
    if($search_date_ymd_st != "")
    {
        //$search_date_ymd_st = str_replace("/", "-", $search_date_ymd_st);
        $where .= " and substr(registed_date::text,0,11) >= '".str_replace("/", "-", $search_date_ymd_st)."' ";
    }
    if($search_date_ymd_ed != "")
    {
        //$search_date_ymd_ed = str_replace("/", "-", $search_date_ymd_ed);
        $where .= " and substr(registed_date::text,0,11) <= '".str_replace("/", "-", $search_date_ymd_ed)."' ";
    }
}
elseif($search_date_mode == "non")
{
    //絞り込み条件なし。
}
else
{
    if($search_date_late_mode == "1month")
    {
        //最近１ヶ月(先月の今日以降)
        $search_date_y = date("Y");
        $search_date_m = date("n");
        $search_date_d = date("d");

        $search_date_m--;
        if($search_date_m <= 0)
        {
            $search_date_y--;
            $search_date_m = 12;
        }
        if($search_date_m <= 9)
        {
            $search_date_m = "0".$search_date_m;
        }
    }
    elseif($search_date_late_mode == "2week")
    {
        //最近２週間(先々週の今日以降)
        $date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
        $date_2week = $date_today - 1209600;//2w * (7d/w) * (24h/d) * (60m/h) * (60s/m) = 1209600s
        $search_date_y = date('Y', $date_2week);
        $search_date_m = date('n', $date_2week);
        $search_date_d = date('d', $date_2week);
        if($search_date_m <= 9)
        {
            $search_date_m = "0".$search_date_m;
        }
    }
    else//all
    {
        //過去全件の場合:何もしない。(絞り込まない)
    }

    if($search_date_late_mode != "all")
    {
        $search_date_1 = $search_date_y."-".$search_date_m."-".$search_date_d;
        $where .= " and substr(registed_date::text,0,11) > '$search_date_1' ";
    }
}


/******************************
 * 検索条件開始
 ******************************/
// 分析番号
if($search_analysis_no != "") {
    $search_analysis_no = $db->escape($search_analysis_no);
    $where .= " and analysis_no = '{$search_analysis_no}' ";
}

// 事案番号
if($search_problem_no != "") {
    $search_problem_no = $db->escape($search_problem_no);
    $where .= " and analysis_problem = '{$search_problem_no}'";
}

// タイトル
if($search_analysis_title != "") {
    $search_analysis_title = $db->escape($search_analysis_title);
    $title_keyword = mb_ereg_replace("　"," ",$search_analysis_title); //全角スペース除去
    $title_keyword = trim($title_keyword); //前後スペース除去
    $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

    $wk_str = "";
    //マルチワードについては常にOR結合
    for ($i=0; $i<count($title_keyword_list); $i++) {
        if ($i > 0) {
            if($title_and_or_div == 1 || $title_and_or_div == "") {
                $wk_str .= " and ";
            } else {
                $wk_str .= " or ";
            }
        }
        $wk_str .= " analysis_title like '%{$title_keyword_list[$i]}%' ";
    }
    $where .= " and ($wk_str) ";
}

// 概要
if($search_analysis_summary != "") {
    $search_analysis_summary = $db->escape($search_analysis_summary);
    $title_keyword = mb_ereg_replace("　"," ",$search_analysis_summary); //全角スペース除去
    $title_keyword = trim($title_keyword); //前後スペース除去
    $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

    $wk_str = "";
    //マルチワードについては常にOR結合
    for ($i=0; $i<count($title_keyword_list); $i++) {
        if ($i > 0) {
            if($title_and_or_div == 1 || $title_and_or_div == "") {
                $wk_str .= " and ";
            } else {
                $wk_str .= " or ";
            }
        }
        $wk_str .= " analysis_summary like '%{$title_keyword_list[$i]}%' ";
    }
    $where .= " and ($wk_str) ";
}

// 分析手法
if($search_analysis_method != "") {
    $search_analysis_method = $db->escape($search_analysis_method);
    $where .= " and analysis_method = '{$search_analysis_method}' ";
}

// 更新年月日
if($renewal_date_start_input != "") {
    $start_date = str_replace("/", "-", $renewal_date_start_input);
    $where .= " and renewal_date >= '{$start_date}' ";
}
if($renewal_date_end_input != "") {
    $end_date = str_replace("/", "-", $renewal_date_end_input);
    $where .= " and renewal_date <= '{$end_date}' ";
}

// 評価予定期日
if($assessment_date_start_input != "") {
    $start_date = str_replace("/", "-", $assessment_date_start_input);
    $where .= " and progress_date >= '{$start_date}' ";
}
if($assessment_date_end_input != "") {
    $end_date = str_replace("/", "-", $assessment_date_end_input);
    $where .= " and progress_date <= '{$end_date}' ";
}

// 進捗
if($search_analysis_progress != "") {
    $search_analysis_progress = $db->escape($search_analysis_progress);
    $where .= " and analysis_progress = '{$search_analysis_progress}' ";
}


$order_by = " ORDER BY analysis_id DESC";

//==============================
//SEHL分析データ
//==============================
$has_shel = false;
if ($search_analysis_method == "" || $search_analysis_method == "SHEL") {
    //EXCEL実行の場合、SEHL分析データを出力
    if ($is_excel){
        $shel_sql = 'SELECT r.report_title, a.analysis_problem, a.analysis_summary, s.* ';
        $shel_sql.= 'FROM inci_analysis_shel s ';
        $shel_sql.= 'LEFT JOIN inci_analysis_regist a on a.analysis_id = s.analysis_id ';
        $shel_sql.= 'LEFT JOIN inci_report r on r.report_id = cast(a.analysis_problem_id as integer) ';
        $shel_sql.= "WHERE available = 't'";
        if($search_analysis_method == "") {
            $shel_sql.= " and analysis_method = 'SHEL'";
        }
        $db->query($shel_sql.$where.$order_by);
        $array = $db->getAll();
        if (!empty($array)){
            $has_shel = true;
            shel_data_excel($array);
            exit;
        }
    }

    //EXCEL実行以外の場合、SHEL分析の有無を取得
    else{
        $shel_sql = "SELECT count(*) FROM inci_analysis_regist ";
        $shel_sql.= "WHERE available = 't'";
        if($search_analysis_method == "") {
            $shel_sql.= " and analysis_method = 'SHEL'";
        }
        $db->query($shel_sql.$where);
        $cnt = $db->getOne();
        if ($cnt > 0) {
            $has_shel = true;
        }
    }
}

$db->query($sql.$where.$order_by);
$analysis_data = $db->getAll();

//==============================
//ページングに関する情報
//==============================

//pate省略時は先頭ページを対象とする。
if($page == "")
{
    $page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($analysis_data);
if($rec_count == 1 && $analysis_data[0] == "")
{
    $rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
    $page_max  = 1;
}
else
{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//ページングによる絞込み
//==============================

$analysis_data_work = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++)
{
    if($i >= $rec_count)
    {
        break;
    }
    $analysis_data_work[] = $analysis_data[$i];
}
$analysis_data = $analysis_data_work;

//======================================================================================================================================================
//内部関数
//======================================================================================================================================================
/**
 * SHEL分析データをHTML形式に変換し、Excel出力する
 *
 * @param array $shel_data SHEL分析データ配列
 */
function shel_data_excel($shel_data)
{
    //==============================
    // HTML作成
    //==============================
    $style = 'style="border:#c8c8c8 solid 1px;"';
    $style_c = 'style="border:#c8c8c8 solid 1px; background-color:rgb(255,255,0);"';

    $message = array();

    // metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
    $message[] = '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';

    $message[] = '<table width="100%">';

    //ヘッダー
    $message[] = '<tr>';
    $message[] = '<th '.$style.'>分類</th>';
    $message[] = '<th '.$style_c.'>事案番号</th>';
    $message[] = '<th '.$style.'>概要</th>';
    $message[] = '<th '.$style.'>Software</th>';
    $message[] = '<th '.$style.'>Hardware</th>';
    $message[] = '<th '.$style.'>Environment</th>';
    $message[] = '<th '.$style.'>Liveware(関与者)</th>';
    $message[] = '<th '.$style.'>Liveware(当事者)</th>';
    $message[] = '<th '.$style.'>対策</th>';
    $message[] = '<th '.$style.'>評価</th>';
    $message[] = '</tr>';

    //SHEL分析データ
    $br = "<br style='mso-data-placement:same-cell'>";
    foreach ($shel_data as $row) {
        $data = array();
        foreach ($row as $key=>$col){
            $data[$key] = str_replace(array("\r\n","\r","\n"), $br, $col);
        }

        $message[] = '<tr>';
        $message[] = '<td '.$style.'>'.$data['report_title'].'</td>';
        $message[] = '<td '.$style_c.'>'.$data['analysis_problem'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_summary'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_ls_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_lh_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_le_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_ll_factor'].'</td>';
        $message[] = '<td '.$style.'>'.$data['analysis_free_factor'].'</td>';
        $step = $data['analysis_ls_step'].$br;
        $step.= $data['analysis_lh_step'].$br;
        $step.= $data['analysis_le_step'].$br;
        $step.= $data['analysis_ll_step'].$br;
        $step.= $data['analysis_free_step'];
        $message[] = '<td '.$style.'>'.$step.'</td>';
        $message[] = '<td '.$style.'>&nbsp;</td>';
        $message[] = '</tr>';
    }

    $message[] ="</table>";

    $download_data =  implode($message, "");

    //==============================
    //EXCEL出力
    //==============================
    
    //ファイル名
    $filename = 'fantol_analysis_list_shel.xls';
    if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
        $encoding = 'sjis';
    }
    else {
        $encoding = mb_http_output();   // Firefox
    }
    $filename = mb_convert_encoding($filename,$encoding,mb_internal_encoding());

    //出力
    ob_clean();
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename=' . $filename);
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
    header('Pragma: public');
    echo mb_convert_encoding(nl2br($download_data), 'sjis', mb_internal_encoding());
    ob_end_flush();
}

//======================================================================================================================================================
//HTML出力
//======================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 出来事分析</title>
<?
show_rp_torey_common_header_html($session,$fname,$con);

// ポップアップ表示用JavaScript
$mouseover_popup_flg = is_anonymous_readable($session, $con, $fname);
show_anonymous_emp_name_popup_js();

?>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<script>
function analysis_new(ID) {
    if(ID) {
        var url = "hiyari_analysis_regist.php?session=<?=$session?>&mode=update&a_id="+ID;
    } else {
        var url = "hiyari_analysis_regist.php?session=<?=$session?>";
    }
    var h = 400;//window.screen.availHeight;
    var w = 500;//window.screen.availWidth;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

    window.open(url, '_blank',option);
}

function analysis_method_new(ID, METHOD, ANALYSIS) {
    if(ANALYSIS == '') {
        alert('分析事案を指定してから、再度アクセスしてください');
    } else {
        if(METHOD == '4M4E') {
            var url = "hiyari_analysis_4m4e.php?session=<?=$session?>&a_id="+ID;
        } else if(METHOD == 'SHEL') {
            var url = "hiyari_analysis_shel.php?session=<?=$session?>&a_id="+ID;
        } else if(METHOD == 'RCA') {
            var url = "hiyari_analysis_rca.php?session=<?=$session?>&a_id="+ID;
        } else {
            var url = "hiyari_analysis_general.php?session=<?=$session?>&a_id="+ID;
        }

        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,loaction=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

        window.open(url, '_blank', option);
    }
}

function reload_page() {
    document.list_form.submit();
}

function analysis_action_delete() {
    var ids = get_selected_classification_folder_ids();
    if(! check_multi_select(ids))
    {
        return;
    }

    if(! confirm("削除してもよろしいですか？")) {
        return;
    }

    document.list_form.mode.value = 'delete';
    document.list_form.submit();

}

//選択されているリストID配列を謫ｾします。
function get_selected_classification_folder_ids()
{
    var result = new Array();
    var r_i = 0;

    var list_select_objs = document.getElementsByName("delete_key[]");
    for(var i = 0; i < list_select_objs.length ; i++)
    {
        list_select_obj = list_select_objs.item(i);
        if(list_select_obj.checked)
        {
            result[r_i] = list_select_obj.value;
            r_i++;
        }
    }

    return result;
}


//複数選択チェック
function check_multi_select(ids)
{
    if(ids.length < 1)
    {
        alert("選択されていません。");
        return false;
    }
    return true;
}

function show_search_input_page()
{

    var display = document.getElementById('search_toggle').style.display;
    if(display == '')
    {
        display = 'none';
        document.list_form.search_toggle_div.value = '';
    }
    else
    {
        display = '';
        document.list_form.search_toggle_div.value = 'open';
    }
    document.getElementById('search_toggle').style.display = display;

    // デフォルト設定
    if (document.list_form.search_default_date_start) {
        if (document.getElementById('search_torey_date_in_search_area_start').value == '') {

            document.getElementById('search_torey_date_in_search_area_start').value = document.list_form.search_default_date_start.value;
        }
        if (document.getElementById('search_torey_date_in_search_area_end').value == '') {
            document.getElementById('search_torey_date_in_search_area_end').value = document.list_form.search_default_date_end.value;
        }
    }
}

function output_excel()
{
    <?php if ($has_shel): ?>
        document.list_form.mode.value = 'excel';
        document.list_form.submit();
        document.list_form.mode.value = 'search';
    <?php else: ?>
        alert('SHEL分析がありません。');
    <?php endif; ?>    
}

function search_torey()
{
    //入力チェック
    var st = document.getElementById('renewal_date_start_input').value;
    var ed = document.getElementById('renewal_date_end_input').value;
    if( (st != "" && !is_date_str(st)) || (ed != "" && !is_date_str(ed)) )
    {
        alert("更新年月日が正しくありません。");
        return;
    }

    var st = document.getElementById('assessment_date_start_input').value;
    var ed = document.getElementById('assessment_date_end_input').value;
    if( (st != "" && !is_date_str(st)) || (ed != "" && !is_date_str(ed)) )
    {
        alert("評価予定期日が正しくありません。");
        return;
    }

    /*
    //検索実行

    // 部門
    document.list_form.search_emp_class.value            =   document.getElementById('search_emp_class').value;

    // 課
    document.list_form.search_emp_attribute.value        =   document.getElementById('search_emp_attribute').value;

    // 科
    document.list_form.search_emp_dept.value             =   document.getElementById('search_emp_dept').value;

            // 室
    document.list_form.search_emp_room.value             =   document.getElementById('search_emp_room').value;

    //報告者
    document.list_form.search_emp_job.value              =   document.getElementById('search_emp_job').value;
    document.list_form.search_emp_name.value             =   document.getElementById('search_emp_name').value;

            //患者
    document.list_form.search_patient_id.value           =   document.getElementById('search_patient_id').value;
    document.list_form.search_patient_name.value         =   document.getElementById('search_patient_name').value;
    document.list_form.search_patient_year_from.value    =   document.getElementById('search_patient_year_from').value;
    document.list_form.search_patient_year_to.value      =   document.getElementById('search_patient_year_to').value;

    //事案番号
    document.list_form.search_report_no.value            =   document.getElementById('search_report_no').value;

    // キーワード
    document.list_form.search_title.value = document.getElementById('search_title').value;
    document.list_form.search_incident.value = document.getElementById('search_incident').value;
    var title_and_or_div = document.getElementsByName('title_and_or_div');
    for(var i=0; title_and_or_div.length > i; i++) {
        if(title_and_or_div[i].checked == true) {
            document.list_form.title_and_or_div.value = title_and_or_div[i].value;
            break;
        }
    }
    var incident_and_or_div = document.getElementsByName('incident_and_or_div');
    for(var i=0; incident_and_or_div.length > i; i++) {
        if(incident_and_or_div[i].checked == true) {
            document.list_form.incident_and_or_div.value = incident_and_or_div[i].value;
            break;
        }
    }



    //送信日/報告日/作成日
    document.list_form.search_torey_date_in_search_area_start.value =   document.getElementById('search_torey_date_in_search_area_start').value;
    document.list_form.search_torey_date_in_search_area_end.value   =   document.getElementById('search_torey_date_in_search_area_end').value;

            //発生日
    document.list_form.search_incident_date_start.value  =   document.getElementById('search_incident_date_start').value;
    document.list_form.search_incident_date_end.value    =   document.getElementById('search_incident_date_end').value;



    //検索時には日付検索領域は使用不可となる。
    document.list_form.search_date_mode.value = "non";

    */
    document.list_form.page.value=1;
    document.list_form.mode.value="search";
    document.list_form.submit();
}

//YYYY/MM/DD形式の日付文字列であるか判定します。(100年)
function is_date_str(datestr)
{
    if(!datestr.match(/^\d{4}\/\d{2}\/\d{2}$/)){
        return false;
    }
    var vYear = datestr.substr(0, 4) - 0;
    var vMonth = datestr.substr(5, 2) - 1; // Javascriptは、0-11で表現
    var vDay = datestr.substr(8, 2) - 0;
    if(vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31){
        var vDt = new Date(vYear, vMonth, vDay); //※0100/01/01 より前だと1900年代と解釈される。
        vDt.setFullYear(datestr.substr(0, 4));
        vDt.setMonth(vMonth);
        vDt.setDate(vDay);
        if(isNaN(vDt)){
            return false;
        }else if(vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

</script>



<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,false);
?>
<!-- ヘッダー END -->

<!-- レポーティング本体 START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="200" valign="top" bgcolor="#D5F4D8">

<?
// 左の年月日選択
$param_list = array();
$param_list["search_date_target"] = $search_date_target;
$param_list["search_date_mode"] = $search_date_mode;
$param_list["search_date_late_mode"] = $search_date_late_mode;
$param_list["search_date_year"] = $search_date_year;
$param_list["search_date_month"] = $search_date_month;
$param_list["search_date_ymd_st"] = $search_date_ymd_st;
$param_list["search_date_ymd_ed"] = $search_date_ymd_ed;
show_rp_torey_select_cal_area($session,$fname,$con,$param_list);
?>

<!-- 一覧 START -->
<form name="list_form" method="post" action="hiyari_analysis_disp.php">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="search">

<input type="hidden" name="search_toggle_div" value="<?=$search_toggle_div?>">

<input type="hidden" name="page" value="<?=$page?>">
<input type="hidden" name="sort_item" value="<?=$sort_item?>">
<input type="hidden" name="sort_div" value="<?=$sort_div?>">

<input type="hidden" name="search_date_target" value="<?=$search_date_target?>">
<input type="hidden" name="search_date_mode" value="<?=$search_date_mode?>">
<input type="hidden" name="search_date_late_mode" value="<?=$search_date_late_mode?>">
<input type="hidden" name="search_date_year" value="<?=$search_date_year?>">
<input type="hidden" name="search_date_month" value="<?=$search_date_month?>">
<input type="hidden" name="search_date_ymd_st" value="<?=$search_date_ymd_st?>">
<input type="hidden" name="search_date_ymd_ed" value="<?=$search_date_ymd_ed?>">

</td>
<td width="10"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>

    <table border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
        <tr>
<?if (is_analysis_update_usable($session, $fname, $con)) { ?>
            <td align="center"><img src="img/hiyari_rp_new.gif" width="36" height="30" style="cursor: pointer;" onclick="analysis_new();"></td>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><img src="img/hiyari_rp_delete.gif" style="cursor: pointer;" onclick="analysis_action_delete();"></td>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
<? } ?>
            <td align="center"><img src="img/hiyari_rp_search.gif" style="cursor: pointer;" onclick="show_search_input_page();"></td>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><img src="img/hiyari_rp_excel.gif" style="cursor: pointer;" onclick="output_excel();"></td>
        </tr>
        <tr>
<?if (is_analysis_update_usable($session, $fname, $con)) { ?>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">作成</font></td>
            <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">削除</font></td>
            <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
<? } ?>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">検索</font></td>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">出力（SHEL）</font></td>
        </tr>
    </table>
</td>
</tr>
</table>

<!--// 検索領域開始 -->
<?
    //$search_toggle_div = $param_list["search_toggle_div"];
?>
    <table border="0" cellspacing="0" cellpadding="0"  id="search_toggle" style="display:none;">
    <img src="img/spacer.gif" width="1" height="10" alt=""><br>
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">







    <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr>
    <td rowspan="2">

    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

    <tr>
    <td width="80" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分析番号</font></td>
    <td width="150" bgcolor="#FFFFFF"><input type="text" name="search_analysis_no" id="search_analysis_no" size="40" value="<?=h($search_analysis_no);?>" maxlength="40" style="ime-mode:disable"></td>
    <td width="80" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font></td>
    <td width="*" bgcolor="#FFFFFF"><input type="text" name="search_problem_no" id="search_problem_no" size="40" value="<?=h($search_problem_no);?>" maxlength="40" style="ime-mode:disable"></td>
    </tr>

    <tr>
    <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
    <td width="*" bgcolor="#FFFFFF" colspan="3"><input type="text" name="search_analysis_title" id="search_analysis_title" size="80" value="<?=h($search_analysis_title);?>" maxlength="40" style="ime-mode:active"></td>
    </tr>

    <tr>
    <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font></td>
    <td width="*" bgcolor="#FFFFFF" colspan="3"><input type="text" name="search_analysis_summary" id="search_analysis_summary" size="80" value="<?=h($search_analysis_summary);?>" maxlength="40" style="ime-mode:active"></td>
    </tr>

    <tr>
    <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分析手法</font></td>
    <td width="*" bgcolor="#FFFFFF" colspan="3">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="radio" name="search_analysis_method" value="" <?if($search_analysis_method==""){ echo 'checked'; }?>>全て&nbsp;&nbsp;&nbsp;
        <input type="radio" name="search_analysis_method" value="general" <?if($search_analysis_method=="general"){ echo 'checked'; }?>>汎用&nbsp;&nbsp;&nbsp;
        <input type="radio" name="search_analysis_method" value="4M4E" <?if($search_analysis_method=="4M4E"){ echo 'checked'; }?>>４Ｍ４Ｅ&nbsp;&nbsp;&nbsp;
        <input type="radio" name="search_analysis_method" value="SHEL" <?if($search_analysis_method=="SHEL"){ echo 'checked'; }?>>ＳＨＥＬ&nbsp;&nbsp;&nbsp;
        <input type="radio" name="search_analysis_method" value="RCA" <?if($search_analysis_method=="RCA"){ echo 'checked'; }?>>ＲＣＡ&nbsp;&nbsp;&nbsp;
    </font>
    </td>
    </tr>

    <tr>
    <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新年月日</font></td>
    <td width="*" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始年月日</font><input type="text" id="renewal_date_start_input" name="renewal_date_start_input" value="<?=str_replace("-", "/", $renewal_date_start_input)?>" style="width:80px" maxlength="10">
    <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('renewal_search_st')"/></td>
    <td width="*" bgcolor="#FFFFFF" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了年月日</font><input type="text" id="renewal_date_end_input" name="renewal_date_end_input" value="<?=str_replace("-", "/", $renewal_date_end_input)?>" style="width:80px" maxlength="10">
    <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('renewal_search_ed')"/></td>
    </tr>

    <tr>
    <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価予定期日</font></td>
    <td width="*" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始年月日</font><input type="text" id="assessment_date_start_input" name="assessment_date_start_input" value="<?=str_replace("-", "/", $assessment_date_start_input)?>" style="width:80px" value="" maxlength="10">
    <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('assessment_search_st')"/></td>
    <td width="*" bgcolor="#FFFFFF" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了年月日</font><input type="text" id="assessment_date_end_input" name="assessment_date_end_input" value="<?=str_replace("-", "/", $assessment_date_end_input)?>" style="width:80px" value="" maxlength="10">
    <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('assessment_search_ed')"/></td>
    </tr>

    <tr>
    <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">進捗</font></td>
    <td width="*" bgcolor="#FFFFFF" colspan="3">
        <select name="search_analysis_progress">
            <option value="" <?if($search_analysis_progress=="") { echo 'selected';}?>>全て</option>
            <option value="1" <?if($search_analysis_progress=="1"){echo 'selected';}?>>分析中</option>
            <option value="2" <?if($search_analysis_progress=="2"){echo 'selected';}?>>分析済</option>
            <option value="3" <?if($search_analysis_progress=="3"){echo 'selected';}?>>評価中</option>
            <option value="4" <?if($search_analysis_progress=="4"){echo 'selected';}?>>評価済</option>
        </select>
    </td>
    </tr>






    </table>

    </td>
    <td valign="top">
    <a href="javascript:show_search_input_page();"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">閉じる</font></a>
    </td>
    </tr>

    <tr>
    <td valign="bottom">
    <input type="button" value="検索" onclick="search_torey();">
    </td>
    </tr>
    </table>









        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
    </table>
    <img src="img/spacer.gif" width="1" height="10" alt=""><br>

    <script type="text/javascript">
    if('<?=$search_toggle_div?>' == "open")
    {
        // 表示
        document.getElementById('search_toggle').style.display = '';
    }
    else
    {
        // 非表示
        document.getElementById('search_toggle').style.display = 'none';
    }
    </script>




<!--// 検索領域終了 -->

<table border="0" cellspacing="0" cellpadding="2" width="100%" class="list">
    <tr>
        <td bgcolor="#DFFFDC"></td>
        <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分析番号</font></td>
        <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font></td>
        <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
        <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新年月日</font></td>
        <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価期日</font></td>
        <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">進捗</font></td>
        <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手法</font></td>
    </tr>
<?
foreach($analysis_data as $analysis_datum) {
	echo "<tr>";
	//ログインしているユーザが作成者もしくはSMならチェックボックスを表示。それ以外は非表示にする。
	if($login_user_id == $analysis_datum['registed_user_id'] || $is_sm == true) {
		echo"<td align=\"center\" width=\"10px\"><input type=\"checkbox\" name=\"delete_key[]\" value=". $analysis_datum['analysis_id'] ."></td>";
	}
	else {
		echo"<td align=\"center\" width=\"10px\"><input type=\"checkbox\" name=\"delete_key[]\" value='". $analysis_datum['analysis_id'] ."' disabled ></td>";
	}
?>
        <td align="center" width="120px">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <?if(is_analysis_update_usable($session, $fname, $con)) { ?>
                    <a href="JavaScript:analysis_new(<?=$analysis_datum['analysis_id']?>);"><?=$analysis_datum['analysis_no']?></a>
                <?} else {?>
                    <?=$analysis_datum['analysis_no']?>
                <?}?>
            </font>
        </td>
        <td align="center" width="100px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$analysis_datum['analysis_problem']?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="JavaScript:analysis_method_new(<?=$analysis_datum['analysis_id']?>, '<?=trim($analysis_datum['analysis_method'])?>', '<?=$analysis_datum['analysis_problem']?>')"><?=$analysis_datum['analysis_title']?></a></font></td>
        <td align="center" width="100px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=str_replace("-", "/", $analysis_datum['renewal_date'])?></font></td>
        <td align="center" width="100px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=str_replace("-", "/", $analysis_datum['progress_date'])?></font></td>
        <td align="center" width="60px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?if($analysis_datum['analysis_progress']=='1') { ?>分析中<?} else if($analysis_datum['analysis_progress']=='2') { ?>分析済<?} else if($analysis_datum['analysis_progress']=='3') { ?>評価中<?} else if($analysis_datum['analysis_progress']=='4') {?>評価済<?}?></font></td>
        <td align="center" width="60px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?if(trim($analysis_datum['analysis_method'])=='general') { ?>汎用<? } else { echo $analysis_datum['analysis_method']; }?></font></td>
    </tr>
<?
}
?>
</table>
<table>
    <tr>
        <td>
<!-- ページ選択 START -->
<?
show_rp_page_select_area($session,$fname,$con,$page_max,$page)
?>
<!-- ページ選択 END -->
        </td>
    </tr>
</table>

<!--//
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="analysis_mode" value="">
<input type="hidden" name="is_postback" value="true">
-->
</form>










