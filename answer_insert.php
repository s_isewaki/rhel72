<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("about_postgres.php"); ?>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<? require_once("Cmx.php"); ?>
<? require_once("aclg_set.php"); ?>
<?
$fname = $PHP_SELF;

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$ward = check_authority($session,6,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
?>
<?
$answer = trim($answer);

if(!$answer){
echo("<script language='javascript'>alert(\"回答を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($answer) > 500){
echo("<script language='javascript'>alert(\"回答を250文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$select_id = "select emp_id from login where emp_id in (select emp_id from session where session_id = '$session')";
$result_select = pg_exec($con, $select_id);
if($result_select == false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_result($result_select,0,"emp_id");

$date = date(YmdHi);

$select_max = "select max(answer_id) from answer";
$result_max = pg_exec($con,$select_max);
$count = pg_numrows($result_max);
if($count > 0){
	$answer_id = pg_result($result_max,0,"max");
	$answer_id = $answer_id + 1;
}else{
	$answer_id = "1";
}

$insert_answer = "insert into answer (emp_id, answer_id, date, qa_category_id, question_id, answer, a_del_flg) values ('$emp_id', '$answer_id', '$date', '$qa_category_id', '$question_id', '$answer', 'f')";
//echo($insert_answer);
$result_insert = pg_exec($con,$insert_answer);
if($result_insert == false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
echo("<script language='javascript'>location.href='qa_list.php?session=$session&qa_category_id=$qa_category_id&question_id=$question_id';</script>");
exit;
?>