<?
require_once("about_comedix.php");
require_once("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");


$sql = "select apply_id, a.emp_id, a.target_date from ovtmapply a ";
$cond = "where exists (select * from ovtmapply b where a.emp_id = b.emp_id and a.target_date = b.target_date and apply_id = $apply_id)";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

while($row = pg_fetch_array($sel))
{
    $ovtm_apply_id = $row["apply_id"];
    $emp_id = $row["emp_id"];
    $target_date = $row["target_date"];
    
	// 承認情報を論理削除
	$sql = "update ovtmaprv set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $ovtm_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 申請情報を論理削除
	$sql = "update ovtmapply set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $ovtm_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 承認候補を論理削除
	$sql = "update ovtmaprvemp set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $ovtm_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 同期・非同期情報を論理削除
	$sql = "update ovtm_async_recv set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $ovtm_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

    //勤務実績の残業時刻をクリアする 20140814
    $sql = "update atdbkrslt set";
    $set = array("over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag");
    $setvalue = array("", "", 0 , 0);
    //追加
    for ($i=2; $i<=5; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
        array_push($set, $s_t);
        array_push($set, $e_t);
        array_push($set, $s_f);
        array_push($set, $e_f);
        array_push($setvalue, "");
        array_push($setvalue, "");
        array_push($setvalue, 0);
        array_push($setvalue, 0);
    }
    //休憩    
    for ($i=1; $i<=5; $i++) {
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        array_push($set, $s_t);
        array_push($set, $e_t);
        array_push($set, $s_f);
        array_push($set, $e_f);
        array_push($setvalue, "");
        array_push($setvalue, "");
        array_push($setvalue, 0);
        array_push($setvalue, 0);
    }    
    $cond = "where emp_id = '$emp_id' and date = '$target_date'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //
	// 勤務時間修正申請が出されている場合
	$sql = "select apply_id from tmmdapply";
    $cond = "where emp_id = '$emp_id' and target_date = '$target_date' and not delete_flg and re_apply_id is null";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0)
    {
        $tmmd_apply_id = pg_fetch_result($sel, 0, "apply_id");
        
        // 勤務時間修正申請の残業時刻を更新
        $sql = "update tmmdapply set";
        $set = array("a_over_start_time", "a_over_end_time", "a_over_start_next_day_flag", "a_over_end_next_day_flag");
        $setvalue = array("", "", 0, 0);
        //追加
        for ($i=2; $i<=5; $i++) {
            $s_t = "a_over_start_time".$i;
            $e_t = "a_over_end_time".$i;
            $s_f = "a_over_start_next_day_flag".$i;
            $e_f = "a_over_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, "");
            array_push($setvalue, "");
            array_push($setvalue, 0);
            array_push($setvalue, 0);
        }    
        for ($i=1; $i<=5; $i++) {
            $s_t = "a_rest_start_time".$i;
            $e_t = "a_rest_end_time".$i;
            $s_f = "a_rest_start_next_day_flag".$i;
            $e_f = "a_rest_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, "");
            array_push($setvalue, "");
            array_push($setvalue, 0);
            array_push($setvalue, 0);
        }    
        $cond = "where apply_id = '$tmmd_apply_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);


// 自画面を閉じる
if (strpos($pnt_url, "atdbk_timecard.php") !== false || strpos($pnt_url, "atdbk_timecard_shift.php") !== false) {
	echo("<script type=\"text/javascript\">opener.location.reload();self.close();</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
	echo("<script language=\"javascript\">window.close();</script>\n");
}

?>
