<?php
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
//require_once("show_select_values.ini");
require_once("date_utils.php");
require_once("show_timecard_common.ini");
require_once("timecard_paid_hol_hour_class.php");

require_once './common_log_class.ini';
require_once './kintai/common/mdb_wrapper.php'; // MDB本体をWrap
require_once './kintai/common/mdb.php'; 		// DBアクセスユーティリティ
require_once './kintai/common/user_info.php';	// ユーザ情報
require_once './kintai/common/master_util.php';	// マスタ
require_once './kintai/common/exception.php';
require_once './kintai/common/date_util.php';
require_once './kintai/common/code_util.php';

require_once("settings_common.php"); // 年次有給休暇簿 20131218
$wk_this_title = get_settings_value("kintai_vacation_paid_hol_title", "年次休暇簿");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// DBコネクション作成（既存用）
$con = connect2db($fname);

if($umode == "usr" || $mmode == "usr"){
    // 権限のチェック
    $checkauth = check_authority($session, 5, $fname);
    if ($checkauth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}else if($umode == "adm" || $umode == "adm2"){
    // 勤務管理権限チェック
    $checkauth = check_authority($session, 42, $fname);
    if ($checkauth == "0") {
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
}
elseif ($umode == "duty") {
    //シフト
    require_once("duty_shift_common_class.php");
    $obj = new duty_shift_common_class($con, $fname);
    //ユーザ画面用
    $chk_flg = $obj->check_authority_user($session, $fname);
    if ($chk_flg == "") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
}
elseif ($umode == "apply" || $umode == "duty2") {
    //決裁・申請
    ;
}
else{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$work_admin_auth = check_authority($session, 42, $fname);

if($flg == ""){
    $flg = "1";
}

//フォームデータ取得
$p_emp_id 	= $_REQUEST["p_emp_id"];
$date_y1	= $_REQUEST["date_y1"];
$date_m1	= $_REQUEST["date_m1"];
$date_d1	= $_REQUEST["date_d1"];
$date_y2	= $_REQUEST["date_y2"];
$date_m2	= $_REQUEST["date_m2"];
$date_d2	= $_REQUEST["date_d2"];

//データアクセス
try {

    MDBWrapper::init(); // おまじない
    $log = new common_log_class(basename(__FILE__));
    $db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

    $db->beginTransaction(); // トランザクション開始

    //部署階層数
    $class_cnt = MasterUtil::get_organization_level_count();

    //職員ID・氏名を取得
    $ukey  = "";
    $uType = "";
    if($umode == "usr"){
        $ukey  = $session;
        $uType = "s";
    }else if($umode == "adm" || $umode == "duty" || $umode == "apply" || $umode == "adm2" || $umode == "duty2"){
        if($selEmpID != ""){
            $ukey  = $selEmpID;
            $uType = "u";
        }
    }
    $user_info  = UserInfo::get($ukey, $uType);
    $base_info  = $user_info->get_base_info();
    $job_info   = $user_info->get_job_info();
    $dept_info  = $user_info->get_dept_info();
    $croom_info = $user_info->get_class_room_info();
    $cond_info = $user_info->get_emp_condition();

    //所属
    if($class_cnt == 4 && $croom_info->room_nm != ""){
        $shozoku = $croom_info->room_nm;
    }else{
        $shozoku = $dept_info->dept_nm;
    }

    $db->endTransaction(); // トランザクション終了
} catch (BusinessException $bex) {
    $message = $bex->getMessage();
    echo("<script type='text/javascript'>alert('".$message."');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
} catch (FatalException $fex) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

// DBコネクション作成（既存用）
$con = connect2db($fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();


$arr_data = array();
$wk_carry = "";
$wk_add = "";


if ($paid_hol_add_date != "") {
    $arr_data = $obj_hol_hour->get_paid_hol_list($base_info->emp_id, $paid_hol_add_date, $end_date);

    $date_y1 = substr($paid_hol_add_date, 0, 4);
    $date_m1 = substr($paid_hol_add_date, 4, 2);
    $date_d1 = substr($paid_hol_add_date, 6, 2);
    $date_y2 = substr($end_date, 0, 4);
    $date_m2 = substr($end_date, 4, 2);
    $date_d2 = substr($end_date, 6, 2);

    $arr_emppaid = get_emppaid($con, $fname, $p_emp_id, $paid_hol_add_date);

    //繰越
    $wk_carry = $arr_emppaid["days1"];
    //有休付与
    $wk_add = $arr_emppaid["days2"];

    //調整日数
    $wk_adjust_day = $arr_emppaid["adjust_day"];

    $wk_total = $wk_carry + $wk_add + $wk_adjust_day;
    //繰越時刻
    $wk_carry_time_minute = $arr_emppaid["carry_time_minute"];
    $wk_carry_time = minute_to_hmm($wk_carry_time_minute);
    //if ($wk_carry_time == "") {
    //    $wk_carry_time = "0:00";
    //}

}


//所定時間
$day1_time = $obj_hol_hour->get_specified_time_calendar();
$wk_minute = $obj_hol_hour->get_specified_time($base_info->emp_id, $day1_time);
$specified_time = minute_to_hmm($wk_minute);

$zan_day = $wk_total;
$zan_minute = 0;
$zan_minute += $wk_carry_time_minute;
$zan_time = minute_to_hmm($zan_minute);

$total_minute = 0;
$total_day = 0; // 日数累計 20130913
$data = array();
for ($i=0; $i<count($arr_data); $i++) {

    if ($arr_data[$i]["date"] == "") {
        continue;
    }
    $wk_date = intval(substr($arr_data[$i]["date"], 4, 2))."月".intval(substr($arr_data[$i]["date"], 6, 2))."日";
    if ($arr_data[$i]["curr_use"] != "" ||
            $arr_data[$i]["curr_use_hour"] != ""
        ) {
        $wk_date .= "移行";
    }

    $data[$i]["date"] = $wk_date;
    $data[$i]["wday"] = get_weekday(date_utils::to_timestamp_from_ymd($arr_data[$i]["date"]));
    if ($arr_data[$i]["apply_stat"] == "0") {
        $data[$i]["status"] = "申請中";
    }
    elseif ($arr_data[$i]["apply_stat"] == "1") {
        $data[$i]["status"] = "承認済み";
    }
    $data[$i]["apply_date"] = ($arr_data[$i]["apply_date"] != "") ?intval(substr($arr_data[$i]["apply_date"], 4, 2))."月".intval(substr($arr_data[$i]["apply_date"], 6, 2))."日" : "";
    //移行データの確認
    if ($arr_data[$i]["curr_use"] != "") {
        $wk_hol_day = $arr_data[$i]["curr_use"];
        $data[$i]["curr_use"] = $arr_data[$i]["curr_use"];
    }
    //
    else
        if ($arr_data[$i]["reason"] != "") {
            $wk_hol_day = $arr_data[$i]["hol_day"];
        }
        else {
            $wk_hol_day = "";
        }
    //$data[$i]["hol_day"] = $wk_hol_day;
    $data[$i]["hol_day"] = ($wk_hol_day != "" ) ? sprintf("%02.1f日", $wk_hol_day) : "";
    //日累計 20130913
    if ($wk_hol_day != "") {
        $total_day += $wk_hol_day;
        //$data[$i]["total_day"] = $total_day;
        $data[$i]["total_day"] = sprintf("%02.1f日", $total_day);
    }
    //移行データがある場合
    if ($arr_data[$i]["curr_use_hour"] != "") {
        $data[$i]["hol_hour"] = "";
        $use_minute_calc = $obj_hol_hour->hh_or_hhmm_to_minute($arr_data[$i]["curr_use_hour"]);//20141014 HH:MM形式対応
        $wk_hmm = minute_to_hmm($use_minute_calc);
        $data[$i]["hol_hour_use"] = $wk_hmm;//20141014 HH:MM形式対応
        $data[$i]["hol_hour_total"] = $wk_hmm;//20141014 HH:MM形式対応
        $total_minute += $use_minute_calc;
        $data[$i]["curr_use_hour"] = $arr_data[$i]["curr_use_hour"];
    }
    else
        if ($arr_data[$i]["start_time"] != "") {
            $wk_start_time = intval(substr($arr_data[$i]["start_time"], 0, 2)).":".substr($arr_data[$i]["start_time"], 2, 2);
            $wk_start_time_minute = date_utils::hi_to_minute($arr_data[$i]["start_time"], 0, 2);
            $wk_end_time_minute = $wk_start_time_minute + $arr_data[$i]["use_hour"] * 60 + intval($arr_data[$i]["use_minute"]);
            $data[$i]["hol_hour"] = "$wk_start_time 〜 ".minute_to_hmm($wk_end_time_minute);
            $data[$i]["hol_hour_use"] = $arr_data[$i]["use_hour"].":".$arr_data[$i]["use_minute"];
            $use_minute_calc = $arr_data[$i]["use_hour"] * 60 + intval($arr_data[$i]["use_minute"]);
            $total_minute += $use_minute_calc;
            $data[$i]["hol_hour_total"] = minute_to_hmm($total_minute);
        }
        else {
            $data[$i]["hol_hour"] = "";
            $data[$i]["hol_hour_use"] = "";
            $data[$i]["hol_hour_total"] = "";
            $use_minute_calc = 0;
        }
    //残数計算
    //上限有無確認 上限なしの場合は、日から繰り下げて減算
    //上限ありの場合
    //※1日未満分の時間を表示のため、コメント化
    //if ($obj_hol_hour->paid_hol_hour_max_flag == "t") {
    //    if ($wk_hol_day != "") {
    //        $zan_day -= $wk_hol_day;
    //    }
    //    $zan_minute -= $use_minute_calc;
    //}
    //else {
    //繰り下げ確認
    if ($use_minute_calc > 0) {
        if ($zan_minute >= $use_minute_calc) {
            $zan_minute -= $use_minute_calc;
        }
        //残が少ない場合、日から繰り下げ
        else {
            //残が半日で引ける場合
            if ($zan_day == 0.5 && ($zan_minute + ($wk_minute / 2) >= $use_minute_calc )) {
                $zan_day -= 0.5; //日から引く
                $zan_minute += ($wk_minute / 2); //所定時間
            }
            else {
                $zan_day--; //日から引く
                $zan_minute += $wk_minute; //所定時間
            }
            $zan_minute -= $use_minute_calc;
        }
    }
    //時間を合わせて1日を引ける場合
    if ($arr_data[$i]["hol_day"] == 1 && $zan_day == 0.5 && $zan_minute >= ($wk_minute / 2)) {
        $zan_day -= 0.5; //日から引く
        $zan_minute -= floor($wk_minute / 2);
        $wk_hol_day = "";
    }

    //時間から0.5日を引ける場合
    if ($arr_data[$i]["hol_day"] == 0.5 && $zan_day == 0 && $zan_minute >= ($wk_minute / 2)) {
        $zan_minute -= floor($wk_minute / 2);
        $wk_hol_day = "";
    }
    if ($wk_hol_day != "") {
        $zan_day -= $wk_hol_day;
    }
    //}
    $zan_day = sprintf("%02.1f", $zan_day);
    $data[$i]["remain_day"] = $zan_day."日";
    $remain_hour = minute_to_hmm($zan_minute);
    if ($remain_hour == "") {
        $remain_hour = "0:00";
    }
    $data[$i]["remain_hour"] = $remain_hour;
}

//====================================
//	エクセル出力クラス
//====================================
require_once("./kintai/common/kintai_print_excel_class.php");
// Excelオブジェクト生成
$excelObj = new ExcelWorkShop();
// Excel列名。列名をloopで連続するときに使うと便利です。
$excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN');
// 用紙の向き。横方向の例
$excelObj->PageSetUp("virtical");
// 用紙サイズ
$excelObj->PaperSize( "A4" );
// 倍率と自動或いは利用者指定。利用者が指定して95%で印刷する場合
$excelObj->PageRatio( "USER" , "95" );
// 余白指定、左,右,上,下,ヘッダ,フッタ
$excelObj->SetMargin( "1.0" , "1,0", "1.5" , "1.5" , "0.5" , "0.5" );
// ヘッダー情報
$HeaderMessage = '&"ＭＳ ゴシック"&11&B'.$wk_this_title;
$HeaderMessage = $HeaderMessage . "&R".date("Y.n.j");
$excelObj->SetPrintHeader( $HeaderMessage );
//フッター
$strFooter  = '&C&P / &N';
$excelObj->SetPrintFooter($strFooter);
// シート名
$excelObj->SetSheetName(mb2($wk_this_title));
// 列幅
$colWdArr = array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5);
for($i=0;$i<count($colWdArr);$i++){
    $excelObj->SetColDim($excelColumnName[$i], $colWdArr[$i]);
}
//デフォルトフォント
$excelObj->setDefaultFont("ＭＳ ゴシック", 9, true);

//基本■■■■■■■■■■■■■■■■■■
//開始位置
$ROW_ST = 5;
//行のタイトル
$excelObj->setRowTitle(1, $ROW_ST);
//ウィンド枠固定
$excelObj->setPane(0,$ROW_ST+2); //見出し2段分

//期間
$excelObj->CellMerge2("A1:C1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A1", "対象期間：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("D1:I1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("D1", $date_y1."年".intval($date_m1)."月".intval($date_d1)."日〜".$date_y2."年".intval($date_m2)."月".intval($date_d2)."日", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
//職員
$excelObj->CellMerge2("J1:K1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("J1", "所属：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("L1:U1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("L1", $shozoku, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("A2:C2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A2", "職員ID：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("D2:F2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("D2", $base_info->emp_personal_id, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("J2:K2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("J2", "氏名：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("L2:U2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("L2", $base_info->emp_lt_nm." ".$base_info->emp_ft_nm."（".$job_info->job_nm."）", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

//2段目
$excelObj->CellMerge2("A3:C3", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A3", "所定労働時間：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("D3:E3", "", "", "");
$excelObj->SetFontBold();
$wk_specified_time = ($specified_time != "") ? $specified_time : "";
$excelObj->SetValueJP2("D3", $wk_specified_time, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("F3:K3", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("F3", "時間単位で取得できる限度：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
//$wk_minute = date_utils::hi_to_minute($specified_time);
//上限ありの場合
if ($obj_hol_hour->paid_hol_hour_max_flag == "t") {
    $wk_hour  = minute_to_hmm($wk_minute * $obj_hol_hour->paid_hol_hour_max_day);
    $wk_limit = "{$obj_hol_hour->paid_hol_hour_max_day}日{$wk_hour}";//（）時間
}
else {
    $wk_limit = "上限なし";
}
$excelObj->CellMerge2("L3:P3", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("L3", $wk_limit, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

$excelObj->CellMerge2("Q3:R3", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("Q3", "採用日：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("S3:U3", "", "", "");
$excelObj->SetFontBold();
$wk_emp_join = ($base_info->emp_join != "") ? substr($base_info->emp_join, 0, 4)."年".intval(substr($base_info->emp_join, 4, 2))."月".intval(substr($base_info->emp_join, 6, 2))."日" : "";
$excelObj->SetValueJP2("S3", $wk_emp_join, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

//3段目
$excelObj->CellMerge2("A4:C4", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A4", "前年繰越：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);

$wk_carry_str = $wk_carry."日".$wk_carry_time;
$excelObj->CellMerge2("D4:F4", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("D4", $wk_carry_str, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

$excelObj->CellMerge2("G4:K4", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("G4", "当年付与：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);

$wk_add_str = $wk_add."日(".$date_y1."年".intval($date_m1)."月".intval($date_d1)."日）";
$excelObj->CellMerge2("L4:P4", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("L4", $wk_add_str, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

$excelObj->CellMerge2("Q4:R4", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("Q4", "合計：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);

$wk_zan_str = $wk_total."日".$zan_time;
$excelObj->CellMerge2("S4:U4", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("S4", $wk_zan_str, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

//現在行位置
$y = $ROW_ST;
$y2 = $y+1; //見出し2段目

//タイトル行
$excelObj->CellMerge2("A".$y.":B".$y2, "", "", "");
$excelObj->SetValueJP2("A".$y, "日付", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("C".$y.":C".$y2, "", "", "");
$excelObj->SetValueJP2("C".$y, "曜日", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("D".$y.":E".$y2, "", "", "");
$excelObj->SetValueJP2("D".$y, "承認", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("F".$y.":I".$y, "", "", ""); // レイアウト修正 20130913
$excelObj->SetValueJP2("F".$y, "日単位有休", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("F".$y2.":G".$y2, "", "", "");
$excelObj->SetValueJP2("F".$y2, "取得", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("H".$y2.":I".$y2, "", "", "");
$excelObj->SetValueJP2("H".$y2, "累計", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("J".$y.":P".$y, "", "", "");
$excelObj->SetValueJP2("J".$y, "時間単位有休", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("J".$y2.":L".$y2, "", "", "");
$excelObj->SetValueJP2("J".$y2, "時間帯", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("M".$y2.":N".$y2, "", "", "");
$excelObj->SetValueJP2("M".$y2, "取得", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("O".$y2.":P".$y2, "", "", "");
$excelObj->SetValueJP2("O".$y2, "累計", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("Q".$y.":S".$y2, "", "", "");
$excelObj->SetValueJP2("Q".$y, "有給休暇残日数", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
//$excelObj->CellMerge2("P".$y2.":Q".$y2, "", "", "");
//$excelObj->SetValueJP2("P".$y2, "日数", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
//$excelObj->CellMerge2("R".$y2.":S".$y2, "", "", "");
//$excelObj->SetValueJP2("R".$y2, "時間数", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("T".$y.":U".$y2, "", "", "");
$excelObj->SetValueJP2("T".$y, "申請日", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
$y++;
//リスト
for($i=0;$i<count($data);$i++){
    $y++;
    $excelObj->CellMerge2("A".$y.":B".$y, "", "", "");
    $excelObj->SetValueJP2("A".$y, $data[$i]["date"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->SetArea("C".$y);
    $excelObj->SetValueJP2("C".$y, $data[$i]["wday"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->CellMerge2("D".$y.":E".$y, "", "", "");
    $excelObj->SetValueJP2("D".$y, $data[$i]["status"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->CellMerge2("F".$y.":G".$y, "", "", "");
    $excelObj->SetValueJP2("F".$y, $data[$i]["hol_day"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->CellMerge2("H".$y.":I".$y, "", "", "");
    $excelObj->SetValueJP2("H".$y, $data[$i]["total_day"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);

    $excelObj->CellMerge2("J".$y.":L".$y, "", "", "");
    $excelObj->SetValueJP2("J".$y, $data[$i]["hol_hour"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->CellMerge2("M".$y.":N".$y, "", "", "");

    $excelObj->SetValueJP2("M".$y, $data[$i]["hol_hour_use"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->CellMerge2("O".$y.":P".$y, "", "", "");
    $excelObj->SetValueJP2("O".$y, $data[$i]["hol_hour_total"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->CellMerge2("Q".$y.":S".$y, "", "", "");
    $excelObj->SetValueJP2("Q".$y, $data[$i]["remain_day"].$data[$i]["remain_hour"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
//    $excelObj->CellMerge2("R".$y.":S".$y, "", "", "");
//    $excelObj->SetValueJP2("R".$y, $data[$i]["remain_hour"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    $excelObj->CellMerge2("T".$y.":U".$y, "", "", "");
    $excelObj->SetValueJP2("T".$y, $data[$i]["apply_date"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
    }
    //罫線
    $excelObj->SetArea("A".$ROW_ST.":U".$y);
    $excelObj->SetBorder("THIN", "", "all");




$excelObj->SetArea("A1");
// 出力ファイル名
$filename = "List_of_paid_holiday.xls";
ob_clean();
// ダウンロード形式
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');
$excelObj->OutPut();

function mb1($str){
    return mb_convert_encoding($str, "UTF-8", "EUC-JP");
}
function mb2($str){
    return mb_convert_encoding(mb_convert_encoding($str, "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
}
?>
<?
//add_date 付与日
function get_emppaid($con, $fname, $emp_id, $add_date) {

    $wk_year = substr($add_date, 0, 4);
    $wk_mmdd = substr($add_date, 4, 4);

    //検索
    $sql = "select * from emppaid ";
    $cond = "where emp_id = '$emp_id' and year = '$wk_year' and paid_hol_add_mmdd = '$wk_mmdd' ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_data = array();
    if (pg_num_rows($sel) > 0) {
        $arr_data = pg_fetch_array($sel);
    }

    return $arr_data;
}
