<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>掲示板 | 投稿更新</title>
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($path == "2") ? 62 : 1;
$check_auth = check_authority($session, $auth_id, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 掲示板管理権限を取得
$bbs_admin_auth = check_authority($session, 62, $fname);

// データベースに接続
$con = connect2db($fname);

if ($back != "t") {
	// 投稿データを取得
	if ($post_id == 0) {
		$sql = "select bbsthread.bbsthread_title as title, bbsthread.bbsthread as post, empmst.emp_lt_nm, empmst.emp_ft_nm, bbsthread.date, bbsthread.emp_id, bbsthread.emp_id as thread_emp_id from bbsthread inner join empmst on bbsthread.emp_id = empmst.emp_id";
		$cond = "where bbsthread.bbsthread_id = $theme_id";
	} else {
		$sql = "select bbs.bbs_title as title, bbs.bbs as post, empmst.emp_lt_nm, empmst.emp_ft_nm, bbs.date, bbs.emp_id, bbsthread.emp_id as thread_emp_id from (bbs inner join empmst on bbs.emp_id = empmst.emp_id) inner join bbsthread on bbs.bbsthread_id = bbsthread.bbsthread_id";
		$cond = "where bbs.bbsthread_id = $theme_id and bbs.bbs_id = $post_id";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$bbs_title = pg_fetch_result($sel, 0, "title");
	$bbs = pg_fetch_result($sel, 0, "post");
	$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	$date = pg_fetch_result($sel, 0, "date");
	$reg_emp_id = pg_fetch_result($sel, 0, "emp_id");
	$thread_emp_id = pg_fetch_result($sel, 0, "thread_emp_id");
}

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// ログインユーザの部署情報を取得
$sql = "select emp_class, emp_attribute, emp_dept, emp_st from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_class = pg_fetch_result($sel, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel, 0, "emp_dept");
$emp_st = pg_fetch_result($sel, 0, "emp_st");

// 投稿権限のあるか確認
$sql = "select count(*) from bbsthread";
$cond = "where bbsthread_del_flg = 'f' and bbsthread_id = $theme_id";
$cond .= " and (";
	$cond .= "(";
		$cond .= "(upd_dept_flg = '1' or (upd_dept_flg = '2' and exists (select * from bbsupddept where bbsupddept.bbsthread_id = bbsthread.bbsthread_id and bbsupddept.class_id = $emp_class and bbsupddept.atrb_id = $emp_atrb and bbsupddept.dept_id = $emp_dept)))";
		$cond .= " and ";
		$cond .= "(upd_st_flg = '1' or (upd_st_flg = '2' and exists (select * from bbsupdst where bbsupdst.bbsthread_id = bbsthread.bbsthread_id and bbsupdst.st_id = $emp_st)))";
	$cond .= ")";
	$cond .= " or ";
	$cond .= "exists (select * from bbsupdemp where bbsupdemp.bbsthread_id = bbsthread.bbsthread_id and bbsupdemp.emp_id = '$emp_id')";
$cond .= ")";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$post_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

// 表示値の編集
//$post = str_replace("\n", "<br>", $post);
$date_str = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $date);

if ($back != "t") {
	// 添付ファイル情報を取得
	$sql = "select bbsfile_no, bbsfile_name from bbsfile";
	$cond = "where bbsthread_id = $theme_id and bbs_id = $post_id order by bbsfile_no";
	$sel_file = select_from_table($con, $sql, $cond, $fname);
	if ($sel_file == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$file_id = array();
	$filename = array();
	while ($row = pg_fetch_array($sel_file)) {
		$tmp_file_id = $row["bbsfile_no"];
		$tmp_filename = $row["bbsfile_name"];

		array_push($file_id, $tmp_file_id);
		array_push($filename, $tmp_filename);
		// 一時フォルダにコピー
		$ext = strrchr($tmp_filename, ".");
		copy("bbs/{$theme_id}_{$post_id}_{$tmp_file_id}{$ext}", "bbs/tmp/{$session}_{$tmp_file_id}{$ext}");
	}
}

// 30分以上前に保存されたファイルを削除
if (!is_dir("bbs")) {
	mkdir("bbs", 0755);
}
if (!is_dir("bbs/tmp")) {
	mkdir("bbs/tmp", 0755);
}
foreach (glob("bbs/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
if(!tinyMCE.isOpera)
{
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "preview,table,emotions,fullscreen,layer,paste",
		//language : "ja_euc-jp",
		language : "ja",
		width : "100%",
		height : "300",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid,pasteword",
		content_css : "tinymce/tinymce_content.css",
		theme_advanced_statusbar_location : "none",
		force_br_newlines : true,
		forced_root_block : '',
		force_p_newlines : false
	});
}

function reply() {
	document.mainform.action = 'bbs_register.php';
	document.mainform.submit();
}

function deletePost() {
<?
$confirm = ($post_id == 0) ? "テーマごと削除されます。よろしいですか？" : "削除します。よろしいですか？";
?>
	if (confirm('<? echo($confirm); ?>')) {
		document.mainform.action = 'bbs_delete.php';
		document.mainform.submit();
	}
}

function attachFile() {
	window.open('bbs_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function updatePost() {
	document.mainform.action = 'bbs_post_update_exe.php';
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p.attach {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="method" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($path == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bbsthread_menu.php?session=<? echo($session); ?>"><img src="img/icon/b10.gif" width="32" height="32" border="0" alt="掲示板・電子会議室"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bbsthread_menu.php?session=<? echo($session); ?>"><b>掲示板・電子会議室</b></a> &gt; <a href="bbs_admin_menu.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bbsthread_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bbs_admin_menu.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#5279a5"><a href="bbs_post_update.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&post_id=<? echo($post_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>&path=2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>投稿更新</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bbsthread_menu.php?session=<? echo($session); ?>"><img src="img/icon/b10.gif" width="32" height="32" border="0" alt="掲示板・電子会議室"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bbsthread_menu.php?session=<? echo($session); ?>"><b>掲示板・電子会議室</b></a> &gt; <a href="bbs_post_update.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&post_id=<? echo($post_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><b>投稿更新</b></font></td>
<? if ($bbs_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bbs_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bbsthread_menu.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#5279a5"><a href="bbs_post_update.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&post_id=<? echo($post_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>投稿更新</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="bbs_option.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bbs_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td width="480"><input name="bbs_title" type="text" size="50" maxlength="100" value="<? echo($bbs_title); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td>
<textarea name="bbs" rows="15" cols="50" style="ime-mode:active;"><? echo($bbs); ?></textarea>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="attach">
<?
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$ext = strrchr($tmp_filename, ".");

	echo("<p id=\"p_{$tmp_file_id}\" class=\"attach\">\n");
	if ($back != "t") {
		echo("<a href=\"bbs/{$theme_id}_{$post_id}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}</a>\n");
	} else {
		echo("<a href=\"bbs/tmp/{$session}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}</a>\n");
	}
	echo("<input type=\"button\" id=\"btn_{$tmp_file_id}\" name=\"btn_{$tmp_file_id}\" value=\"削除\" onclick=\"detachFile(event);\">\n");
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
	echo("</p>\n");
}
?>
</div>
<input type="button" value="追加" onclick="attachFile();">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿者名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($date_str); ?></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="button" value="前画面に戻る" onclick="history.back();">
<input type="button" value="返信" onclick="reply();"<? if (!$post_flg && $path != "2") {echo(" disabled");} ?>>
<input type="button" value="更新" onclick="updatePost();">
<input type="button" value="削除" onclick="deletePost();">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="theme_id" value="<? echo($theme_id); ?>">
<input type="hidden" name="post_id" value="<? echo($post_id); ?>">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="show_content" value="<? echo($show_content); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="emp_nm" value="<? echo($emp_nm); ?>">
<input type="hidden" name="reg_emp_id" value="<? echo($reg_emp_id); ?>">
<input type="hidden" name="thread_emp_id" value="<? echo($thread_emp_id); ?>">
</td>
</tr>
</table>
</form>
</body>
</html>
