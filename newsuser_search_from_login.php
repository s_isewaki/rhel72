<?php
require("Cmx.php");
require("about_comedix.php");
require("show_select_values.ini");
require("show_class_name.ini");
require("news_common.ini");
require("get_values.ini");
require("show_newsuser_search_from_login.ini");

$fname = $PHP_SELF;

// �ڡ����ֹ������
if ($page == "") {
    $page = 1;
}

// �ǡ����١�������³
$con = connect2db($fname);

// �ȿ�̾�����
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// ���ƥ����������� 3:�����İʳ�
//$category_list = get_category_list_from_db($con, $fname, 3);
$category_list_db = get_category_list_from_db($con, $fname, 3);
$category_list = array();
foreach ($category_list_db as $category_id => $category_name) {
    $category_list[$category_id] = $category_name["newscate_name"];
}

// ������������
$class_list = get_class_list($con, $fname);
$atrb_list = get_atrb_list($con, $fname);
$dept_list = get_dept_list($con, $fname);
$room_list = get_room_list($con, $fname);

// ��ã��ʬ���������
$notice_list = get_notice_list($con, $fname);
$notice2_list = get_notice2_list($con, $fname);

// ������������
$job_list = get_job_list($con, $fname);

// ��Ͽ��̾�����
$emp_name = ($srch_emp_id != "") ? get_emp_kanji_name($con, $srch_emp_id, $fname) : "";

// ȯ����̾�����
$src_emp_nm = ($src_emp_id != "") ? get_emp_kanji_name($con, $src_emp_id, $fname) : "";

// ���̥桼����Ͽ���ĥե饰�����
$everyone_flg = get_everyone_flg($con, $fname);

// �򿦰��������
$st_list = get_st_list($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ���Τ餻 | ����</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setSrcAtrbOptions('<? echo($srch_src_atrb_id); ?>', '<? echo($srch_src_dept_id); ?>', '<? echo($srch_src_room_id); ?>');
	setDestAtrbOptions('<? echo($srch_dest_atrb_id); ?>', '<? echo($srch_dest_dept_id); ?>', '<? echo($srch_dest_room_id); ?>');
	changeCategory();
}

function setSrcAtrbOptions(atrb_id, dept_id, room_id) {
	var atrb_elm = document.mainform.srch_src_atrb_id;
	deleteAllOptions(atrb_elm);
	addOption(atrb_elm, '0', '������', atrb_id);

	var class_id = document.mainform.srch_src_class_id.value;
	switch (class_id) {
<?
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
	echo("\tcase '$tmp_class_id':\n");
	foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
		echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setSrcDeptOptions(dept_id, room_id);
}

function setSrcDeptOptions(dept_id, room_id) {
	var dept_elm = document.mainform.srch_src_dept_id;
	deleteAllOptions(dept_elm);
	addOption(dept_elm, '0', '������', dept_id);

	var atrb_id = document.mainform.srch_src_atrb_id.value;
	switch (atrb_id) {
<?
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
	echo("\tcase '$tmp_atrb_id':\n");
	foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
		echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setSrcRoomOptions(room_id);
}

function setSrcRoomOptions(room_id) {
<? if ($arr_class_name['class_cnt'] != 4) { ?>
	return;
<? } ?>

	var room_elm = document.mainform.srch_src_room_id;
	deleteAllOptions(room_elm);
	addOption(room_elm, '0', '������', room_id);

	var dept_id = document.mainform.srch_src_dept_id.value;
	switch (dept_id) {
<?
foreach ($room_list as $tmp_dept_id => $rooms_in_dept) {
	echo("\tcase '$tmp_dept_id':\n");
	foreach ($rooms_in_dept as $tmp_room_id => $tmp_room_nm) {
		echo("\t\taddOption(room_elm, '$tmp_room_id', '$tmp_room_nm', room_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}
}

function setDestAtrbOptions(atrb_id, dept_id, room_id) {
	var atrb_elm = document.mainform.srch_dest_atrb_id;
	deleteAllOptions(atrb_elm);
	addOption(atrb_elm, '0', '������', atrb_id);

	var class_id = document.mainform.srch_dest_class_id.value;
	switch (class_id) {
<?
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
	echo("\tcase '$tmp_class_id':\n");
	foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
		echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setDestDeptOptions(dept_id, room_id);
}

function setDestDeptOptions(dept_id, room_id) {
	var dept_elm = document.mainform.srch_dest_dept_id;
	deleteAllOptions(dept_elm);
	addOption(dept_elm, '0', '������', dept_id);

	var atrb_id = document.mainform.srch_dest_atrb_id.value;
	switch (atrb_id) {
<?
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
	echo("\tcase '$tmp_atrb_id':\n");
	foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
		echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setDestRoomOptions(room_id);
}

function setDestRoomOptions(room_id) {
<? if ($arr_class_name['class_cnt'] != 4) { ?>
	return;
<? } ?>

	var room_elm = document.mainform.srch_dest_room_id;
	deleteAllOptions(room_elm);
	addOption(room_elm, '0', '������', room_id);

	var dept_id = document.mainform.srch_dest_dept_id.value;
	switch (dept_id) {
<?
foreach ($room_list as $tmp_dept_id => $rooms_in_dept) {
	echo("\tcase '$tmp_dept_id':\n");
	foreach ($rooms_in_dept as $tmp_room_id => $tmp_room_nm) {
		echo("\t\taddOption(room_elm, '$tmp_room_id', '$tmp_room_nm', room_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}
}

function selectEmployee() {
	window.open('news_employee_select_from_login.php?session=<? echo($session); ?>&class_id='.concat(document.mainform.srch_src_class_id.value), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	if (selected == value) {
		box.options[box.length - 1].selected = true;
	}
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function changeCategory() {
	var id = document.mainform.srch_news_category.value;

	var tr_srch_dest_class = document.getElementById('srch_dest_class');
	var tr_srch_job = document.getElementById('srch_job');

	if (id == "0") {
		tr_srch_dest_class.style.display = '';
		tr_srch_job.style.display = '';
	} else if (id == "1") {
		tr_srch_dest_class.style.display = 'none';
		tr_srch_job.style.display = 'none';
	} else if (id == "2") {
		tr_srch_dest_class.style.display = '';
		tr_srch_job.style.display = 'none';
	} else if (id == "3") {
		tr_srch_dest_class.style.display = 'none';
		tr_srch_job.style.display = '';
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="2" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff">&nbsp;<b>���Τ餻����</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="newsuser_search_from_login.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="25%" align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ȥ�</font></td>
<td><input type="text" name="srch_news_title" value="<? echo($srch_news_title); ?>" size="50" style="ime-mode:active;"></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td><input type="text" name="srch_news" value="<? echo($srch_news); ?>" size="50" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�оݥ��ƥ���</font></td>
<td><select name="srch_news_category" onchange="changeCategory();"><?php show_options($category_list, $srch_news_category, true); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�о���</font></td>
<td><select name="srch_st_id"><? show_options($st_list, $srch_st_id, true); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ã��ʬ</font></td>
<td><select name="srch_notice_id"><? show_options($notice_list, $srch_notice_id, true); ?></select></td>
</tr>
<? if (!empty($notice2_list)) {?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���Τ餻��ʬ</font></td>
<td colspan="3"><select name="srch_notice2_id"><? show_options($notice2_list, $srch_notice2_id, true); ?></select></td>
</tr>
<? }?>
<tr id="srch_dest_class" height="22" style="display:">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ۿ�������</font></td>
<td><select name="srch_dest_class_id" onchange="setDestAtrbOptions();"><? show_options($class_list, $srch_dest_class_id, true); ?></select><select name="srch_dest_atrb_id" onchange="setDestDeptOptions();"></select><select name="srch_dest_dept_id" onchange="setDestRoomOptions();"></select><div id="dest_room" style="display:none"><? if ($arr_class_name['class_cnt'] == 4) { ?><select name="srch_dest_room_id"></select><? } ?></div></td>
</tr>
<tr id="srch_job" height="22" style="display:">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td><select name="srch_job_id"><? show_options($job_list, $srch_job_id, true); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ȯ������</font></td>
<td><select name="srch_src_class_id" onchange="setSrcAtrbOptions();"><? show_options($class_list, $srch_src_class_id, true); ?></select><select name="srch_src_atrb_id" onchange="setSrcDeptOptions();"></select><select name="srch_src_dept_id" onchange="setSrcRoomOptions();"></select><? if ($arr_class_name['class_cnt'] == 4) { ?><select name="srch_src_room_id"></select><? } ?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="javascript:void(0);" onclick="selectEmployee();">ȯ����</a></font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
<input type="text" name="src_emp_nm" size="30" value="<? echo($src_emp_nm); ?>" disabled>
<a href="javascript:void(0);" onclick="document.mainform.src_emp_nm.value = ''; document.mainform.src_emp_id.value = ''; document.mainform.hid_src_emp_nm.value = '';">���ꥢ</a>
<input type="hidden" name="src_emp_id" value="<? echo($src_emp_id); ?>">
<input type="hidden" name="hid_src_emp_nm" value="<? echo($src_emp_nm); ?>">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�Ǻ���</font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><select name="srch_news_date_from1"><? show_select_years(10, $srch_news_date_from1, true); ?></select>/<select name="srch_news_date_from2"><? show_select_months($srch_news_date_from2, true); ?></select>/<select name="srch_news_date_from3"><? show_select_days($srch_news_date_from3, true); ?></select> �� <select name="srch_news_date_to1"><? show_select_years(10, $srch_news_date_to1, true); ?></select>/<select name="srch_news_date_to2"><? show_select_months($srch_news_date_to2, true); ?></select>/<select name="srch_news_date_to3"><? show_select_days($srch_news_date_to3, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('news_member_list_from_login.php?from_page_id=2', 'newwin', 'width=640,height=480,scrollbars=yes')">��Ͽ��</a></font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
<input type="text" name="emp_name" size="30" value="<? echo($emp_name); ?>" disabled>
<a href="javascript:void(0);" onclick="document.mainform.emp_name.value = ''; document.mainform.srch_emp_id.value = '';">���ꥢ</a>
<input type="hidden" name="srch_emp_id" value="<? echo($srch_emp_id); ?>">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��Ͽ��</font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><select name="srch_news_date1"><? show_select_years(10, $srch_news_date1, true); ?></select>/<select name="srch_news_date2"><? show_select_months($srch_news_date2, true); ?></select>/<select name="srch_news_date3"><? show_select_days($srch_news_date3, true); ?></select></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="����"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="t">
</form>
<? if ($srch_flg == "t") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td align="center" width="80"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��Ͽ��</font></td>
<td align="center" width="100"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�оݥ��ƥ���</font></td>
<td align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ȥ�</font></td>
<td align="center" width="180"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�Ǻܴ���</font></td>
</tr>
<? show_news_search_result_list($con, $page, $class_called, $session, $fname); ?>
</table>
<? show_news_search_page_list($con, $page, $session, $fname); ?>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
