<?

// 合議者コメント更新
// application_approve_regist.php を流用

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");

$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
/*$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
*/
// データベースに接続
$con = connect2db($fname);

$obj = new application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// $apply = get_apply_by_apply_id($con, $apply_id, $fname);

// テンプレートの場合XML形式のテキスト$contentを作成
/*
$wkfw_content_type = $apply["wkfw_content_type"];
if ($wkfw_content_type == "2") {
	$savexmlfilename = "workflow/tmp/{$session}_x.php";

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($apply["wkfw_id"], $apply["wkfw_history_no"]);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);

	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if (!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include($savexmlfilename);
}

// 申請内容更新ボタンが押された場合、内容のみを更新し、画面を再表示する
if ($update_action == "t") {
	$obj->update_apply($apply_id, $content, $apply["apply_title"]);
	pg_query($con, "commit");
	pg_close($con);
	echo("<script type='text/javascript'>location.href = '$location_href';</script>");
	exit;
}

// 申請結果通知削除・登録
$obj->delete_applynotice($apply_id);
if($notice_emp_id != "")
{
    $arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
    for($i=0; $i<count($arr_notice_emp_id); $i++)
    {
        $obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
    }
}

// 承認処理
$obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, "DETAIL");

// メールを送るかどうか判断、送る場合は宛先を配列に追加
$to_addresses = array();
if (must_send_mail_to_next_approvers($con, $obj, $apply_id, $wkfw_appr, $apv_order, $approve, $next_notice_div, $fname)) {

	// 次の階層で未承認の承認者（全員のはず）のメールアドレスを取得
	$next_apv_order = $apv_order + 1;
	$sql = "select e.emp_email2 from empmst e";
	$cond = "where exists (select * from applyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id and a.apv_order = $next_apv_order and a.apv_stat = '0')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		if ($row["emp_email2"] != "") {
			$to_addresses[] = $row["emp_email2"];
		}
	}
}

// メール送信に必要な情報を取得
if (count($to_addresses) > 0) {
	$apply_title = $apply["apply_title"];
	$apply_emp_id = $apply["emp_id"];

	$emp_detail = $obj->get_empmst_detail($apply_emp_id);
	$emp_nm = format_emp_nm($emp_detail[0]);
	$emp_mail = format_emp_mail($emp_detail[0]);
	$emp_pos = format_emp_pos($emp_detail[0]);

	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
	$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
}

// 文書登録処理
$files = register_library($con, $approve, $obj, $apply_id, $fname);
*/
if( $council_comment == "" || empty($council_comment) ){
	$council_comment="合議";
}
$sql = "UPDATE apply_council_comment SET";
$set = array("apply_council_comment","apply_check_flag");
$setvalue = array($council_comment,'1');
$cond = "WHERE emp_id = '$emp_id' AND apply_id=$apply_id";

$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if($up == 0)
{
    pg_query($con,"rollback");
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

// トランザクションをコミット
pg_query($con, "commit");
/*
// メール送信
if (count($to_addresses) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] {$approve_label}依頼のお知らせ";
	$mail_content = format_mail_content($con, $wkfw_content_type, $content, $fname);
	$mail_separator = str_repeat("-", 60) . "\n";
	$additional_headers = "From: $emp_mail";
	$additional_parameter = "-f$emp_mail";

	foreach ($to_addresses as $to_address) {
		$mail_body = "以下の{$approve_label}依頼がありました。\n\n";
		$mail_body .= "申請者：{$emp_nm}\n";
		$mail_body .= "所属：{$emp_pos}\n";
		$mail_body .= "表題：{$apply_title}\n";
		if ($mail_content != "") {
			$mail_body .= $mail_separator;
			$mail_body .= "{$mail_content}\n";
		}

		mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
	}
}
*/
// データベース接続を切断
pg_close($con);

// ファイルコピー
foreach ($files as $tmp_file) {
	copy_apply_file_to_library($tmp_file);
}

// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");
?>
