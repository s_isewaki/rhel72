<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG｜承認者選択</title>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 委員会メンバー配列を初期化
$project_members = array();

// 委員会責任者情報を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select pjt_response from project where pjt_id = $pjt_id)";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$project_members[] = array(
	"emp_id" => pg_fetch_result($sel, 0, "emp_id"),
	"name" => pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm"),
	"response" => true
);

// 委員会メンバー情報を取得
$sql = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm from empmst inner join promember on empmst.emp_id = promember.emp_id";
$cond = "where promember.pjt_id = $pjt_id and exists (select * from authmst where emp_del_flg = 'f' and emp_id = empmst.emp_id) order by promember.pjt_member_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$project_members[] = array(
		"emp_id" => $row["emp_id"],
		"name" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"],
		"response" => false
	);
}

// イニシャル別に職員を取得して配列に格納
$employees = array();
for ($i = 1; $i <= 10; $i++) {
	$employees[$i] = get_employees_by_initial($con, $i, $fname);
}

// イニシャルのデフォルトは「あ行」
if ($in_id == "") {$in_id = 1;}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/tabview/tabview-min.js"></script>
<script type="text/javascript">
function initPage() {
	new YAHOO.widget.TabView('maintab');
	new YAHOO.widget.TabView('initial');
}

function setAprvData(emp_id, emp_nm){
	var used = false;
	var aprv_cnt = opener.document.prcd.elements['approve_id[]'].length;
	if (aprv_cnt == undefined) {
		used = (opener.document.prcd.elements['approve_id[]'].value == emp_id);
	} else {
		for (var i = 0; i < aprv_cnt; i++) {
			if (opener.document.prcd.elements['approve_id[]'][i].value == emp_id) {
				used = true;
				break;
			}
		}
	}
	if (used) {
		alert('この承認者は選択ずみです。');
		return;
	}

	if (aprv_cnt == undefined) {
		opener.document.prcd.elements['approve_name[]'].value = emp_nm;
		opener.document.prcd.elements['approve[]'].value = emp_nm;
		opener.document.prcd.elements['approve_id[]'].value = emp_id;
//		opener.document.prcd.elements['deci_flg[]'].value = emp_id;
	} else {
		opener.document.prcd.elements['approve_name[]'][<? echo($approve); ?>].value = emp_nm;
		opener.document.prcd.elements['approve[]'][<? echo($approve); ?>].value = emp_nm;
		opener.document.prcd.elements['approve_id[]'][<? echo($approve); ?>].value = emp_id;
	}
	self.close();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="js/yui_0.12.2/build/tabview/assets/border_tabs.css">
<link rel="stylesheet" type="text/css" href="js/yui_0.12.2/build/tabview/assets/tabview.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.yui-navset ul.yui-nav {
	text-align:left;
}

.yui-navset .yui-nav li a, .yui-navset .yui-content {
    border:1px solid #5279a5;  /* label and content borders */
}

.yui-navset .yui-nav .selected a, .yui-navset .yui-nav a:hover, .yui-navset .yui-content {
    background-color:#ffffff; /* active tab, tab hover, and content bgcolor */
}

.yui-navset .yui-nav li a {
    background-color:#dddddd;
	color:blue;
}

.yui-navset .yui-content {
	padding:5px;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>承認者選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<div id="maintab" class="yui-navset" style="width:600px;">
<ul class="yui-nav">
<li class="selected"><a href="#member"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>委員会・WGメンバー</em></font></a></span></li>
<li><a href="#emplist"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>職員名簿</em></font></a></li>
</ul>
<div class="yui-content">
<div id="member" style="height:370px;overflow:auto;">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<!--
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
</tr>
-->
<? foreach ($project_members as $project_member) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setAprvData('<? echo($project_member["emp_id"]); ?>', '<? echo($project_member["name"]); ?>');"><? echo($project_member["name"]); ?></a><? if ($project_member["response"]) {echo("（責任者）");} ?></font></td>
</tr>
<? } ?>
</table>
</div>
<div id="emplist" style="display:none;">
	<div id="initial" class="yui-navset">
	<ul class="yui-nav">
	<li class="selected"><a href="#ini1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>あ行</em></font></a></span></li>
	<li><a href="#ini2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>か行</em></font></a></span></li>
	<li><a href="#ini3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>さ行</em></font></a></span></li>
	<li><a href="#ini4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>た行</em></font></a></span></li>
	<li><a href="#ini5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>な行</em></font></a></span></li>
	<li><a href="#ini6"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>は行</em></font></a></span></li>
	<li><a href="#ini7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>ま行</em></font></a></span></li>
	<li><a href="#ini8"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>や行</em></font></a></span></li>
	<li><a href="#ini9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>ら行</em></font></a></span></li>
	<li><a href="#ini10"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>わ行</em></font></a></span></li>
	</ul>
	<div class="yui-content">
<? for ($i = 1; $i <= 10; $i++) { ?>
	<div id="ini<? echo($i); ?>" style="height:340px;overflow:auto;">
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
<!--
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22" bgcolor="#f6f9ff">
	<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
	</tr>
-->
<? foreach ($employees[$i] as $employee) { ?>
	<tr height="22">
	<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setAprvData('<? echo($employee["emp_id"]); ?>', '<? echo($employee["name"]); ?>');"><? echo($employee["name"]); ?></font></td>
	</tr>
<? } ?>
	</table>
	</div>
<? } ?>
	</div>
</div>
</div>
</div>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function get_employees_by_initial($con, $in_id, $fname) {
	switch ($in_id) {
	case "1":
		$keys = array("01", "02", "03", "04", "05");
		break;
	case "2":
		$keys = array("06", "07", "08", "09", "10", "11", "12", "13", "14", "15");
		break;
	case "3":
		$keys = array("16", "17", "18", "19", "20", "21", "22", "23", "24", "25");
		break;
	case "4":
		$keys = array("26", "27", "28", "29", "30", "31", "32", "33", "34", "35");
		break;
	case "5":
		$keys = array("36", "37", "38", "39", "40");
		break;
	case "6":
		$keys = array("41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55");
		break;
	case "7":
		$keys = array("56", "57", "58", "59", "60");
		break;
	case "8":
		$keys = array("61", "62", "63");
		break;
	case "9":
		$keys = array("64", "65", "66", "67", "68");
		break;
	case "10":
		$keys = array("69", "70", "71", "72", "73", "99");
		break;
	}

	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_keywd like '" . join("%' or emp_keywd like '", $keys) . "%' and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') order by emp_keywd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$employees = array();
	while ($row = pg_fetch_array($sel)) {
		$employees[] = array(
			"emp_id" => $row["emp_id"],
			"name" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"]
		);
	}
	return $employees;
}
?>
