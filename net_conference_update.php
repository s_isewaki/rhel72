<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ネットカンファレンス | 詳細</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");

//==============================================================================
// 共通処理
//==============================================================================

$fname = $PHP_SELF;  // スクリプトパス

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ネットカンファレンス登録権限のチェック
$auth_nc_reg = check_authority($session, 10, $fname);
if ($auth_nc_reg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================================================================
// 登録内容の取得
//==============================================================================

// データベースに接続
$con = connect2db($fname);

// ネットカンファレンス情報取得SQLの実行
$sql = "select nmrm_nm, nmrm_thm, nmrm_start, nmrm_end, nmrm_lmt_flg from nmrmmst";
$cond = "where nmrm_id = '$nmrm_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// データベース接続をクローズ
pg_close($con);

$nmrm_nm = pg_fetch_result($sel, 0, "nmrm_nm");
$nmrm_thm = pg_fetch_result($sel, 0, "nmrm_thm");
$nmrm_start = pg_fetch_result($sel, 0, "nmrm_start");
$nmrm_end = pg_fetch_result($sel, 0, "nmrm_end");
$nmrm_lmt_flg = pg_fetch_result($sel, 0, "nmrm_lmt_flg");

//==============================================================================
// 表示値の編集
//==============================================================================

// 年
$nmrm_start_year = substr($nmrm_start, 0, 4);
$nmrm_end_year = substr($nmrm_end, 0, 4);

// 月
$nmrm_start_month = substr($nmrm_start, 4, 2);
$nmrm_end_month = substr($nmrm_end, 4, 2);

// 日
$nmrm_start_day = substr($nmrm_start, 6, 2);
$nmrm_end_day = substr($nmrm_end, 6, 2);

// 時
$nmrm_start_hour = substr($nmrm_start, 8, 2);
$nmrm_end_hour = substr($nmrm_end, 8, 2);

// 分
$nmrm_start_minute = substr($nmrm_start, 10, 2);
$nmrm_end_minute = substr($nmrm_end, 10, 2);

//==============================================================================
// HTML
//==============================================================================
?>
<script type="text/javascript">
<?
//------------------------------------------------------------------------------
// Title     :  ネットカンファレンスメンバー一括選択
// Name      :  setMember
// Arguments :  authority - ネットカンファレンス権限保持メンバーセレクトボックス
//              member    - ネットカンファレンスメンバーセレクトボックス
// Return    :  なし
//------------------------------------------------------------------------------
?>
function setMember(authority, member) {

	deleteAllOptions(member);

	// authorityにて選択されているOPTION要素をmemberにコピー
	for (var i = 0, j = authority.length; i < j; i++) {
		var authopt = authority.options[i];
		if (authopt.selected) {
			var memopt = document.createElement("option");
			memopt.value = authopt.value;
			memopt.text = authopt.text;
			member.options[member.length] = memopt;
			try {member.style.fontSize = 'auto';} catch (e) {}
			member.style.overflow = 'auto';
			memopt = null;
		}
	}

}

<?
//------------------------------------------------------------------------------
// Title     :  OPTION要素全削除
// Name      :  deleteAllOptions
// Arguments :  box - セレクトボックス
// Return    :  なし
//------------------------------------------------------------------------------
?>
function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}

}

<?
//------------------------------------------------------------------------------
// Title     :  OPTION要素全選択
// Name      :  selectAllOptions
// Arguments :  box - セレクトボックス
// Return    :  なし
//------------------------------------------------------------------------------
?>
function selectAllOptions(box) {

	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = true;
	}

}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="net_conference_menu.php?session=<? echo($session); ?>"><img src="img/icon/b09.gif" width="32" height="32" border="0" alt="ネットカンファレンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="net_conference_menu.php?session=<? echo($session); ?>"><b>ネットカンファレンス</b></a></font></td>
</tr>
</table>
<form action="net_conference_update_confirm.php" method="post" onsubmit="selectAllOptions(this.elements['member[]']);">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="160" height="22" align="center" bgcolor="#bdd1e7"><a href="net_conference_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ネットカンファレンス一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="net_conference_update.php?session=<? echo($session); ?>&nmrm_id=<? echo($nmrm_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>詳細</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カンファレンス名：</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $nmrm_nm; ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テーマ：</font></td>
<td><input type="text" name="nmrm_thm" value="<? echo $nmrm_thm; ?>" size="30" style="ime-mode: active;"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日時：</font></td>
<td><select name="nmrm_start_year"><? show_select_years_nc($nmrm_start_year); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="nmrm_start_month"><? show_select_months($nmrm_start_month); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="nmrm_start_day"><? show_select_days($nmrm_start_day); ?></select>
<select name="nmrm_start_hour"><? show_select_hrs_6_23($nmrm_start_hour); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">：</font><select name="nmrm_start_minute"><option value="00"<? if ($nmrm_start_minute == "00") {echo " selected";} ?>>00</option><option value="30"<? if ($nmrm_start_minute == "30") {echo " selected";} ?>>30</option></select></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了日時：</font></td>
<td><select name="nmrm_end_year"><? show_select_years_nc($nmrm_end_year); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="nmrm_end_month"><? show_select_months($nmrm_end_month); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="nmrm_end_day"><? show_select_days($nmrm_end_day); ?></select>
<select name="nmrm_end_hour">
<? show_select_hrs_6_23($nmrm_end_hour); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">：</font><select name="nmrm_end_minute"><option value="00"<? if ($nmrm_end_minute == "00") {echo " selected";} ?>>00</option><option value="30"<? if ($nmrm_end_minute == "30") {echo " selected";} ?>>30</option></select></td>
</tr>
<tr>
<td height="22" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メンバー以外の入室を制限をしますか？：&nbsp;</font><input type="checkbox" name="nmrm_lmt_flg" value="on"<? if ($nmrm_lmt_flg == 't') {echo " checked";} ?>></td>
</tr>
<tr>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メンバー：</font></td>
<td><select name="authority" size="15" multiple style="width:120px;"><? show_net_conference_authority_list($session, $fname); ?></select>&nbsp;<input type="button" value="->" onclick="setMember(this.form.elements['authority'], this.form.elements['member[]']);">&nbsp;<select name="member[]" size="15" multiple style="width:120px;"><? show_net_conference_member_list($nmrm_id, $fname); ?></select></td>
</tr>
<tr>
<td height="22" colspan="2" align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="nmrm_id" value="<? echo $nmrm_id; ?>">
</form>
</td>
</tr>
</table>
</body>
</html>
<?
//==============================================================================
// 関数
//==============================================================================

//------------------------------------------------------------------------------
// Title     :  年選択OPTION要素表示（ネットカンファレンス用）
// Name      :  show_select_years_nc
// Arguments :  selected_year - 選択年
// Return    :  なし
//------------------------------------------------------------------------------
function show_select_years_nc($selected_year) {

	// 表示開始年の決定（選択年・現在年のうち早い方）
	$this_year = date("Y");
	$start_year = min($selected_year, $this_year);

	// 表示終了年の決定（来年）
	$end_year = $this_year + 1;

	for ($year = $start_year; $year <= $end_year; $year++) {
		echo("<option value=\"$year\"");
		if ($year == $selected_year) {
			echo(" selected");
		}
		echo(">$year</option>\n");
	}

}

//------------------------------------------------------------------------------
// Title     :  ネットカンファレンス権限保持メンバー一覧表示
// Name      :  show_net_conference_authority_list
// Arguments :  fname - スクリプトパス
// Return    :  なし
//------------------------------------------------------------------------------
function show_net_conference_authority_list($fname) {

	// データベースに接続
	$con = connect2db($fname);

	// ネットカンファレンス権限保持メンバー一覧取得SQLの実行
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_conf_flg = 't' and authmst.emp_del_flg = 'f') order by emp_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// データベース接続をクローズ
	pg_close($con);

	// ネットカンファレンス権限保持メンバー一覧の出力
	while ($row = pg_fetch_array($sel)) {
		$emp_id = $row["emp_id"];
		$emp_lt_nm = htmlspecialchars($row["emp_lt_nm"]);
		$emp_ft_nm = htmlspecialchars($row["emp_ft_nm"]);
?>
	<option value="<?echo $emp_id; ?>"><?echo $emp_lt_nm; ?>　<?echo $emp_ft_nm; ?></option>
<?
	}
}

//------------------------------------------------------------------------------
// Title     :  ネットカンファレンスメンバー一覧表示
// Name      :  show_net_conference_member_list
// Arguments :  nmrm_id - ネットカンファレンスID
//              fname   - スクリプトパス
// Return    :  なし
//------------------------------------------------------------------------------
function show_net_conference_member_list($nmrm_id, $fname) {

	// データベースに接続
	$con = connect2db($fname);

	// ネットカンファレンスメンバー一覧取得SQLの実行
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where exists (select * from nmmemst where nmmemst.emp_id = empmst.emp_id and nmmemst.nmrm_id = '$nmrm_id') order by emp_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// データベース接続をクローズ
	pg_close($con);

	// ネットカンファレンスメンバー一覧の出力
	while ($row = pg_fetch_array($sel)) {
		$emp_id = $row["emp_id"];
		$emp_lt_nm = htmlspecialchars($row["emp_lt_nm"]);
		$emp_ft_nm = htmlspecialchars($row["emp_ft_nm"]);
?>
	<option value="<?echo $emp_id; ?>"><?echo $emp_lt_nm; ?>　<?echo $emp_ft_nm; ?></option>
<?
	}
}
?>
