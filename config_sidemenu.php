<?php
require_once("about_comedix.php");

$fname = $_SERVER['PHP_SELF'];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 環境設定情報を取得
$sel_config = select_from_table($con, "select * from config", "", $fname);
if ($sel_config == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$timecard_menu_flg  = pg_fetch_result($sel_config, 0, "timecard_menu_flg");
$calendar_menu_flg  = pg_fetch_result($sel_config, 0, "calendar_menu_flg");
$quick_menu_flg     = pg_fetch_result($sel_config, 0, "quick_menu_flg");
$basic_menu_flg     = pg_fetch_result($sel_config, 0, "basic_menu_flg");
$community_menu_flg = pg_fetch_result($sel_config, 0, "community_menu_flg");
$business_menu_flg  = pg_fetch_result($sel_config, 0, "business_menu_flg");
$license_menu_flg   = pg_fetch_result($sel_config, 0, "license_menu_flg");

// メニュー情報を取得
$sel_menu = select_from_table($con, "select report from menuname", "", $fname);
if ($sel_menu == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$report = pg_fetch_result($sel_menu, 0, "report");

// ヘッダメニュー用system_config
$conf = new Cmx_SystemConfig();
$LogOutMenu = $conf->get("headmenu.logout");

if($LogOutMenu == "")
{
	$LogOutMenu ="t";
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix 環境設定｜サイドメニュー</title>
        <script type="text/javascript" src="js/fontsize.js"></script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <style type="text/css">
            .list {border-collapse:collapse;}
            .list td {border:#5279a5 solid 1px;}
            .list td td {border-style:none;}
            .list td .inner {border-collapse:collapse;}
            .list td .inner td {border:#5279a5 solid 1px;}
        </style>
    </head>
    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#f6f9ff">
                            <td width="32" height="32" class="spacing"><a href="config_register.php?session=<?php echo($session); ?>"><img src="img/icon/b28.gif" width="32" height="32" border="0" alt="環境設定"></a></td>
                            <td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="config_register.php?session=<?php echo($session); ?>"><b>環境設定</b></a></font></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr height="22">
                            <td width="110" align="center" bgcolor="#bdd1e7"><a href="config_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン画面</font></a></td>
                            <td width="5">&nbsp;</td>
                            <td width="90" align="center" bgcolor="#5279a5"><a href="config_sidemenu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>メニュー</b></font></a></td>
                            <td width="5">&nbsp;</td>
                            <td width="90" align="center" bgcolor="#bdd1e7"><a href="config_siteid.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイトID</font></a></td>
                            <td width="5">&nbsp;</td>
                            <td width="90" align="center" bgcolor="#bdd1e7"><a href="config_log.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログ</font></a></td>
                            <td width="5">&nbsp;</td>
                            <td width="110" align="center" bgcolor="#bdd1e7"><a href="config_server_information.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サーバ情報</font></a></td>
                            <td width="5">&nbsp;</td>
                            <td width="120" align="center" bgcolor="#bdd1e7"><a href="config_client_auth.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証履歴</font></a></td>
                            <td width="5">&nbsp;</td>
                            <td width="90" align="center" bgcolor="#bdd1e7"><a href="config_other.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                        </tr>
                    </table>
                    <img src="img/spacer.gif" alt="" width="1" height="4"><br>
                    <form name="mainform" action="config_sidemenu_update_exe.php" method="post">
                        <table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
                            <tr>
                                <td width="24%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示するサイドメニュー</font></td>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr height="22">
                                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    <input type="checkbox" name="timecard_menu_flg" value="t"<?php if ($timecard_menu_flg == "t") {echo(" checked");} ?>>タイムカード
                                                </font></td>
                                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    <input type="checkbox" name="calendar_menu_flg" value="t"<?php if ($calendar_menu_flg == "t") {echo(" checked");} ?>>カレンダー
                                                </font></td>
                                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    <input type="checkbox" name="quick_menu_flg" value="t"<?php if ($quick_menu_flg == "t") {echo(" checked");} ?>>クイック登録
                                                </font></td>
                                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    <input type="checkbox" name="basic_menu_flg" value="t"<?php if ($basic_menu_flg == "t") {echo(" checked");} ?>>基本メニュー
                                                </font></td>
                                        </tr>
                                        <tr height="22">
                                            <!-- td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    <input type="checkbox" name="community_menu_flg" value="t"<?php if ($community_menu_flg == "t") {echo(" checked");} ?>>コミュニティサイト
                                                </font></td -->
                                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    <input type="checkbox" name="business_menu_flg" value="t"<?php if ($business_menu_flg == "t") {echo(" checked");} ?>>業務メニュー
                                                </font></td>
                                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    <input type="checkbox" name="license_menu_flg" value="t"<?php if ($license_menu_flg == "t") {echo(" checked");} ?>>ライセンス
                                                </font></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
							<tr>
								<td width="24%" align="right" bgcolor="#f6f9ff">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示するヘッダメニュー<br>(アイコン)</font>
								</td>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr height="22">
											<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
												<input type="checkbox" name="LogOutMenu" value="t"<? if ($LogOutMenu == "t") {echo(" checked");} ?>>ログアウト
												</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
                        </table>
                        <table width="800" border="0" cellspacing="0" cellpadding="2" style="margin-top:5px;">
                            <tr height="22">
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>メニューカスタマイズ</b></font></td>
                            </tr>
                        </table>
                        <table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
                            <tr height="22">
                                <td colspan="2" bgcolor="#f6f9ff"></td>
                                <td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示メニュー名（変更しない場合は空にしてください）</font></td>
                            </tr>
                            <tr height="22">
                                <td width="24%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">業務メニュー</font></td>
                                <td width="16%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メドレポート</font></td>
                                <td><input type="text" name="report" value="<?php echo(h($report)); ?>" size="30" maxlength="20" style="ime-mode:active;"></td>
                            </tr>
                        </table>
                        <table width="800" border="0" cellspacing="0" cellpadding="2">
                            <tr>
                                <td align="right"><input type="submit" value="更新"></td>
                            </tr>
                        </table>
                        <input type="hidden" name="session" value="<?php echo($session); ?>">
                    </form>
                </td>
            </tr>
        </table>
    </body>
    <?php pg_close($con); ?>
</html>