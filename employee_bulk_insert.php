<?
ini_set("max_execution_time", 0);

require_once("about_session.php");
require_once("about_authority.php");
require_once("passwd_change_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}
if (!$uploaded) {
	echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
	echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
	exit;
}

// 文字コードの設定
switch ($encoding) {
case "1":
	$file_encoding = "SJIS";
	break;
case "2":
	$file_encoding = "EUC-JP";
	break;
case "3":
	$file_encoding = "UTF-8";
	break;
default:
	exit;
}

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// サイトIDを取得
$sql = "select site_id, use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

// パスワードの長さ
$pass_length = get_pass_length($con, $fname);

// カラム数
$column_count = 10;
if ($site_id != "") {
	$column_count++;
}

// CSVデータを配列に格納
$employees = array();
$employee_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

	// 文字コードを変換
	$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

	// EOFを削除
	$line = str_replace(chr(0x1A), "", $line);

	// 空行は無視
	if ($line == "") {
		continue;
	}

	// カラム数チェック
	$employee_data = split(",", $line);
	if (count($employee_data) != $column_count) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$employees[$employee_no] = $employee_data;
	$employee_no++;
}

// 職員データを登録
foreach ($employees as $employee_no => $employee_data) {
	insert_employee_data($con, $employee_no, $employee_data, $site_id, $session, $fname);
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=t&encoding=$encoding';</script>");

// 職員データを登録
function insert_employee_data($con, $employee_no, $employee_data, $site_id, $session, $fname) {
	global $pass_length;
	$i = 0;
	$personal_id = trim($employee_data[$i++]);
	$lt_nm = trim($employee_data[$i++]);
	$ft_nm = trim($employee_data[$i++]);
	$lt_kana_nm = mb_convert_kana(trim($employee_data[$i++]), "HVc");
	$ft_kana_nm = mb_convert_kana(trim($employee_data[$i++]), "HVc");
	$id = trim($employee_data[$i++]);
	$pass = trim($employee_data[$i++]);
	if ($site_id != "") {
		$mail_id = trim($employee_data[$i++]);
	}
	$sex = trim($employee_data[$i++]);
	$birth = trim($employee_data[$i++]);
	$entry = trim($employee_data[$i++]);

	// 入力チェック
	if (strlen($personal_id) > 12) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('職員IDが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$sql = "select count(*) from emptmp";
	$cond = "where emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された職員IDは既に使用されています。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($lt_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($lt_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($ft_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($ft_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($lt_kana_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（ひらがな）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($lt_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（ひらがな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($ft_kana_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（ひらがな）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($ft_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（ひらがな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($id == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ログインIDを入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($id) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ログインIDが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($site_id == "") {
		if (preg_match("/[^0-9a-z_-]/", $id) > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('ログインIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	} else {
		if (preg_match("/[^0-9a-zA-Z_-]/", $id) > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('ログインIDに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}
	if (substr($id, 0, 1) == "-") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ログインIDの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($site_id == "") {
		if ($id == "cyrus" || $id == "postmaster" || $id == "root") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$id}」はログインIDとして使用できません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}
	if ($pass == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードを入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($pass) > $pass_length) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (preg_match("/[^0-9a-zA-Z_-]/", $pass) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (substr($pass, 0, 1) == "-") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($site_id != "") {
		if ($mail_id == "") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('メールIDを入力してください。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}

		if (strlen($mail_id) > 20) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('メールIDが長すぎます。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}

		if ($use_cyrus == "t") {
			if (preg_match("/[^0-9a-z_-]/", $mail_id) > 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		} else {
			if (preg_match("/[^0-9a-z_.-]/", $mail_id) > 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」「.」のみです。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		}

		if (substr($mail_id, 0, 1) == "-") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('メールIDの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}
	if ($sex != "1" && $sex != "2") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('性別が不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($birth != "") {
		if (preg_match("/^\d{8}$/", $birth) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		} else {
			$birth_yr = substr($birth, 0, 4);
			$birth_mon = substr($birth, 4, 2);
			$birth_day = substr($birth, 6, 2);
			if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		}
	}
	if ($entry != "") {
		if (preg_match("/^\d{8}$/", $entry) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('入職日が不正です。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		} else {
			$entry_yr = substr($entry, 0, 4);
			$entry_mon = substr($entry, 4, 2);
			$entry_day = substr($entry, 6, 2);
			if (!checkdate($entry_mon, $entry_day, $entry_yr)) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('入職日が不正です。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		}
	}

	$columns = array("emp_personal_id", "emp_lt_nm", "emp_ft_nm", "emp_kn_lt_nm", "emp_kn_ft_nm", "emp_login_id", "emp_login_pass", "emp_sex", "emp_birth", "emp_join");
	$values = array($personal_id, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $id, $pass, $sex, $birth, $entry);
	if ($site_id != "") {
		$columns[] = "emp_login_mail";
		$values[] = $mail_id;
	}
	$sql = "insert into emptmp (" . implode(", ", $columns) . ") values (";
	$ins = insert_into_table($con, $sql, $values, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
