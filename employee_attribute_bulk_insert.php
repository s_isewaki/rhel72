<?
ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("show_class_name.ini");
require("label_by_profile_type.ini");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}
if (!$uploaded) {
	echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
	echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
	exit;
}

// 文字コードの設定
switch ($encoding) {
case "1":
	$file_encoding = "SJIS";
	break;
case "2":
	$file_encoding = "EUC-JP";
	break;
case "3":
	$file_encoding = "UTF-8";
	break;
default:
	exit;
}

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 組織情報を取得
$arr_class_name = get_class_name_array($con, $fname);

$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by class_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$class_tree = array();
while ($row = pg_fetch_array($sel)) {
	$class_tree[$row["class_id"]] = array(
		"name" => $row["class_nm"], "attributes" => array()
	);
}

$sql = "select class_id, atrb_id, atrb_nm from atrbmst";
$cond = "where atrb_del_flg = 'f' order by atrb_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]] = array(
		"name" => $row["atrb_nm"], "depts" => array()
	);
}

$sql = "select atrbmst.class_id, deptmst.atrb_id, deptmst.dept_id, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
$cond = "where deptmst.dept_del_flg = 'f' order by deptmst.dept_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]]["depts"][$row["dept_id"]] = array(
		"name" => $row["dept_nm"], "rooms" => array()
	);
}

if ($arr_class_name[4] == 4) {
	$sql = "select atrbmst.class_id, deptmst.atrb_id, classroom.dept_id, classroom.room_id, classroom.room_nm from classroom inner join deptmst on classroom.dept_id = deptmst.dept_id inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
	$cond = "where classroom.room_del_flg = 'f' order by classroom.room_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rooms = array();
	while ($row = pg_fetch_array($sel)) {
		$class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]]["depts"][$row["dept_id"]]["rooms"][$row["room_id"]] = array("name" => $row["room_nm"]);
	}
}

// 役職情報を配列に格納
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$statuses = array();
while ($row = pg_fetch_array($sel)) {
	$statuses[$row["st_id"]] = $row["st_nm"];
}

// 職種情報を配列に格納
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
	$jobs[$row["job_id"]] = $row["job_nm"];
}

// カラム数
$column_count = ($arr_class_name[4] == 4) ? 16 : 15;

// CSVデータを配列に格納
$employees = array();
$employee_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

	// 文字コードを変換
	$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

	// EOFを削除
	$line = str_replace(chr(0x1A), "", $line);

	// 空行は無視
	if ($line == "") {
		continue;
	}

	// カラム数チェック
	$employee_data = split(",", $line);
	if (count($employee_data) != $column_count) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$employees[$employee_no] = $employee_data;
	$employee_no++;
}

// 職員データを登録
$extra_ids = array();
foreach ($employees as $employee_no => $employee_data) {
	$extra_id = insert_employee_data($con, $employee_no, $employee_data, $arr_class_name[4], $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname);
	if ($extra_id != "") {
		$extra_ids[] = $extra_id;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 存在しない職員IDが指定されていたらalert
if (count($extra_ids) > 0) {
	$tmp_extra_ids = "・" . implode("\\n・", $extra_ids);
	echo("<script type=\"text/javascript\">alert('下記の職員は存在しないため、無視されました。\\n\\n$tmp_extra_ids');</script>\n");
}

// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=t&encoding=$encoding';</script>");

// 職員データを登録
function insert_employee_data($con, $employee_no, $employee_data, $class_cnt, $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname) {
	global $_label_by_profile;
	global $profile_type;

	$i = 0;
	$personal_id = trim($employee_data[$i++]);
	$emp_lt_nm = $employee_data[$i++];
	$emp_ft_nm = $employee_data[$i++];
	$emp_lt_kana_nm = $employee_data[$i++];
	$emp_ft_kana_nm = $employee_data[$i++];
	$class = trim($employee_data[$i++]);
	$atrb = trim($employee_data[$i++]);
	$dept = trim($employee_data[$i++]);
	if ($arr_class_name[4] == "4") {
		$room = trim($employee_data[$i++]);
	} else {
		$room = "";
	}
	$status = trim($employee_data[$i++]);
	$job = trim($employee_data[$i++]);
	$mobile = trim($employee_data[$i++]);
	$extension = trim($employee_data[$i++]);
	$phs = trim($employee_data[$i++]);
	$email = trim($employee_data[$i++]);
	$del_flg = trim($employee_data[$i++]);

	// 当該職員が存在するかチェック
	$sql = "select count(*) from empmst";
	$cond = "where emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
		return $personal_id;
	}

	// 部門IDを取得
	$class_id = "";
	foreach ($class_tree as $tmp_class_id => $tmp_class) {
		if ($tmp_class["name"] == $class) {
			$class_id = $tmp_class_id ;
		}
	}
	if ($class_id == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[0]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 課IDを取得
	$atrb_id = "";
	foreach ($class_tree[$class_id]["attributes"] as $tmp_atrb_id => $tmp_atrb) {
		if ($tmp_atrb["name"] == $atrb) {
			$atrb_id = $tmp_atrb_id ;
		}
	}
	if ($atrb_id == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[1]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 科IDを取得
	$dept_id = "";
	foreach ($class_tree[$class_id]["attributes"][$atrb_id]["depts"] as $tmp_dept_id => $tmp_dept) {
		if ($tmp_dept["name"] == $dept) {
			$dept_id = $tmp_dept_id ;
		}
	}
	if ($dept_id == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[2]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 室IDを取得
	if ($room != "") {
		foreach ($class_tree[$class_id]["attributes"][$atrb_id]["depts"][$dept_id]["rooms"] as $tmp_room_id => $tmp_room) {
			if ($tmp_room["name"] == $room) {
				$room_id = $tmp_room_id ;
			}
		}
		if ($room_id == "") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[3]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}  else {
		$room_id = null;
	}

	// 役職IDを取得
	if ($status != "") {
		$st_id = array_search($status, $statuses);
		if (!$st_id) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定された役職({$status})は登録されていません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	} else {
		$st_id = null;
	}

	// 職種IDを取得
	if ($job != "") {
		$job_id = array_search($job, $jobs);
		if (!$job_id) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定された職種({$job})は登録されていません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	} else {
		$job_id = null;
	}

	// 入力チェック
	if (strlen($emp_lt_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($emp_ft_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($emp_lt_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（かな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($emp_ft_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（かな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($mobile != "") {
		if (preg_match("/^(\d{1,6})-(\d{1,6})-(\d{1,6})$/", $mobile, $matches)) {
			$mobile1 = $matches[1];
			$mobile2 = $matches[2];
			$mobile3 = $matches[3];
		} else {
			$mobile = preg_replace("/\D/", "", $mobile);
			if (strlen($mobile) > 12) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('携帯端末・外線が長すぎます。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
			$mobile1 = substr($mobile, 0, 4);
			$mobile2 = substr($mobile, 4, 4);
			$mobile3 = substr($mobile, 8);
		}
	}
	if (strlen($extension) > 10) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('内線番号が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($phs) > 6) {
		pg_query($con, "rollback");
		pg_close($con);
		// 院内PHS/所内PHS
		$phs_name = $_label_by_profile["PHS"][$profile_type];
		echo("<script type=\"text/javascript\">alert('{$phs_name}が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($email) > 120) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('E-Mailが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($del_flg != "f" && $del_flg != "t") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('利用停止フラグが不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$sql = "update empmst set";
	$set = array("emp_lt_nm", "emp_ft_nm", "emp_kn_lt_nm", "emp_kn_ft_nm", "emp_keywd", "emp_class", "emp_attribute", "emp_dept", "emp_room", "emp_mobile1", "emp_mobile2", "emp_mobile3", "emp_ext", "emp_phs", "emp_email2");
	$setvalue = array($emp_lt_nm, $emp_ft_nm, $emp_lt_kana_nm, $emp_ft_kana_nm, get_keyword($emp_lt_kana_nm), $class_id, $atrb_id, $dept_id, $room_id, $mobile1, $mobile2, $mobile3, $extension, $phs, $email);
	if (!is_null($st_id)) {
		$set[] = "emp_st";
		$setvalue[] = $st_id;
	}
	if (!is_null($job_id)) {
		$set[] = "emp_job";
		$setvalue[] = $job_id;
	}
	$cond = "where emp_personal_id = '$personal_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$sql = "select emp_id from empmst";
	$cond = "where emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");

	$sql = "update authmst set";
	$set = array("emp_del_flg");
	$setvalue = array($del_flg);
	$cond = "where emp_id = '$emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
