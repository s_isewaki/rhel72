<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/EmployeeList.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// 職員名簿
//----------------------------------------------------------
$emp = $session->emp();
$elist = new Cmx_EmployeeList($_POST['mode'], $_POST['webmail']);

//----------------------------------------------------------
// tr_class
//----------------------------------------------------------
$base_tr_class = str_replace('hover', '', $_POST['tr_class']);

//----------------------------------------------------------
// タイプ別
//----------------------------------------------------------
switch ($_POST['type']) {
    // 名前検索
    case 'search':
        $keyword = str_replace("　", "", str_replace(" ", "", from_utf8($_POST['keyword'])));
        if (!empty($keyword)) {
            $lists = $elist->search_lists($keyword);
        }
        $kind = 'emp';
        $header = true;
        break;

    // よく使う人
    case 'used':
        $lists = $elist->often_used_lists($emp->emp_id());
        $kind = 'emp';
        $header = true;
        break;

    // 組織1
    case 'class':
        $lists = $elist->class_lists();
        $kind = 'group';
        $header = true;
        break;

    // 組織2
    case 'atrb':
        $lists = $elist->atrb_lists($_POST['id']);
        $kind = 'subgroup';
        $tr_class = $base_tr_class . ' class-' . $_POST['id'];
        $indent = 25;
        $header = false;
        break;

    // 組織3
    case 'dept':
        $lists = $elist->dept_lists($_POST['id']);
        $kind = 'subgroup';
        $tr_class = $base_tr_class . ' atrb-' . $_POST['id'];
        $indent = 50;
        $header = false;
        break;

    // 組織4
    case 'room':
        $lists = $elist->room_lists($_POST['id']);
        $kind = 'subgroup';
        $header = false;
        break;

    // 職種
    case 'job':
        $lists = $elist->job_lists();
        $kind = 'group';
        $header = true;
        break;

    // 役職
    case 'status':
        $lists = $elist->status_lists();
        $kind = 'group';
        $header = true;
        break;

    // 委員会
    case 'project':
        $lists = $elist->project_lists($emp->emp_id());
        $kind = 'group';
        $header = true;
        break;

    // マイグループ
    case 'mygroup':
        $lists = $elist->mygroup_lists($emp->emp_id());
        $kind = 'group';
        $header = true;
        break;

    // 所属に該当する職員リスト
    case 'empclass':
        $lists = $elist->emp_dept_lists($_POST['id']);
        $kind = 'groupemp';
        $indent = 75;
        $tr_class = $base_tr_class . ' dept-' . $_POST['id'];
        $header = false;
        break;

    // 第4階層に該当する職員リスト
    case 'emproom':
        $lists = $elist->emp_room_lists($_POST['id']);
        $kind = 'groupemp';
        $indent = 100;
        $tr_class = $base_tr_class . ' room-' . $_POST['id'];
        $header = false;
        break;

    // 職種に該当する職員リスト
    case 'empjob':
        $lists = $elist->emp_job_lists($_POST['id']);
        $kind = 'groupemp';
        $indent = 25;
        $tr_class = 'job-' . $_POST['id'];
        $header = false;
        break;

    // 役職に該当する職員リスト
    case 'empstatus':
        $lists = $elist->emp_status_lists($_POST['id']);
        $kind = 'groupemp';
        $indent = 25;
        $tr_class = 'status-' . $_POST['id'];
        $header = false;
        break;

    // 委員会に該当する職員リスト
    case 'empproject':
        $lists = $elist->emp_project_lists($emp->emp_id(), $_POST['id']);
        $kind = 'groupemp';
        $indent = 25;
        $tr_class = 'project-' . $_POST['id'];
        $header = false;
        break;

    // WGに該当する職員リスト
    case 'empwg':
        $lists = $elist->emp_wg_lists($emp->emp_id(), $_POST['id']);
        $kind = 'groupemp';
        $indent = 50;
        $tr_class = $base_tr_class . ' wg-' . $_POST['id'];
        $header = false;
        break;

    // マイグループに該当する職員リスト
    case 'empmygroup':
        $lists = $elist->emp_mygroup_lists($emp->emp_id(), $_POST['id']);
        $kind = 'groupemp';
        $indent = 25;
        $tr_class = 'mygroup-' . $_POST['id'];
        $header = false;
        break;

    // 共通グループに該当する職員リスト
    case 'empcomgroup':
        $lists = $elist->emp_comgroup_lists($_POST['id']);
        $kind = 'groupemp';
        $indent = 25;
        $tr_class = 'comgroup-' . $_POST['id'];
        $header = false;
        break;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('lists', $lists);
$view->assign('mode', $_POST['mode']);
$view->assign('type', $_POST['type']);
$view->assign('webmail', $_POST['webmail']);
$view->assign('kind', $kind);
$view->assign('header', $header);
$view->assign('indent', $indent);
$view->assign('tr_class', $tr_class);
$view->assign('item_id', $_POST['item_id']);
$view->display('emplist/emplist_list.tpl');
