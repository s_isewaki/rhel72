<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/WMCounter.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// カウンタ
//----------------------------------------------------------
$emp = $session->emp();
$counter = new Cmx_WMCounter();
$counter->countup($emp->emp_id(), $_POST['emp_id']);
