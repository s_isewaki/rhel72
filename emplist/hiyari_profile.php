<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/EmployeeList.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// 職員名簿
//----------------------------------------------------------
$emp = $session->emp();
$elist = new Cmx_EmployeeList();

//----------------------------------------------------------
// ファントルくんプロフィール取得
//----------------------------------------------------------
$other_info = $elist->hiyari_profile($_POST['emp_id']);

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
echo cmx_json_encode(
    array(
        'emp_id' => $_POST['emp_id'],
        'name' => $other_info['emp_name'],
        'item_id' => $_POST['item_id'],
        'other_info' => $other_info
    )
);
