<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('mode', $_GET['mode']);
$view->display('emplist/addresslist.tpl');
exit;
