<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/EmployeeList.php');
require_once('Cmx/Model/AddressList.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// 職員名簿
//----------------------------------------------------------
$emp = $session->emp();
if ($_POST['type'] === 'address_group') {
    $alist = new Cmx_AddressList($emp->emp_id());
} else {
    $elist = new Cmx_EmployeeList($_POST['mode'], $_POST['webmail']);
}

//----------------------------------------------------------
// タイプ別
//----------------------------------------------------------
switch ($_POST['type']) {
    // 組織:第1階層に該当する職員リスト
    case 'class':
        $lists = $elist->emp_class_lists($_POST['id']);
        break;

    // 組織:第2階層に該当する職員リスト
    case 'atrb':
        $lists = $elist->emp_atrb_lists($_POST['id']);
        break;

    // 組織:第3階層に該当する職員リスト
    case 'dept':
        $lists = $elist->emp_dept_lists($_POST['id']);
        break;

    // 組織:第4階層に該当する職員リスト
    case 'room':
        $lists = $elist->emp_room_lists($_POST['id']);
        break;

    // 職種に該当する職員リスト
    case 'job':
        $lists = $elist->emp_job_lists($_POST['id']);
        break;

    // 役職に該当する職員リスト
    case 'status':
        $lists = $elist->emp_status_lists($_POST['id']);
        break;

    // 委員会に該当する職員リスト
    case 'project':
        $lists = $elist->emp_project_lists($emp->emp_id(), $_POST['id']);
        break;

    // WGに該当する職員リスト
    case 'wg':
        $lists = $elist->emp_wg_lists($emp->emp_id(), $_POST['id']);
        break;

    // マイグループに該当する職員リスト
    case 'mygroup':
        $lists = $elist->emp_mygroup_lists($emp->emp_id(), $_POST['id']);
        break;

    // 共通グループに該当する職員リスト
    case 'comgroup':
        $lists = $elist->emp_comgroup_lists($_POST['id']);
        break;

    // アドレスグループに該当するアドレスリスト
    case 'address_group':
        $lists = $alist->subgroup_lists($_POST['id']);
        break;
}

//----------------------------------------------------------
// 職員データ生成
//----------------------------------------------------------
$emp_id = array();
$name = array();
$mail = array();
foreach ($lists as $list) {
    // グループデータは除外
    if ($list['kind'] === 'group' or $list['kind'] === 'subgroup') {
        continue;
    }

    // 自分を送信先に含めない
    if ($_POST['myself'] === 'ng' and $emp->emp_id() === $list['emp_id']) {
        continue;
    }

    // 病床管理からの呼び出しの場合、職種を付ける
    if ($_POST['mode'] === '20') {
        $list['name'] .= "({$list['job_nm']})";
    }

    $emp_id[] = $list['emp_id'];
    $name[]   = $list['name'];

    if ($_POST['type'] === 'address_group') {
        $mail[]   = $list['mail'];
    } else {
        if ($_POST['webmail']) {
            $mail[]   = $list['loginid'];
        }
        else {
            $mail[]   = $list['email'];
        }
    }
}

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
echo cmx_json_encode(
    array(
        'emp_id' => join(", ", $emp_id),
        'name' => join(", ", $name),
        'mail' => join(", ", $mail)
    )
);
