<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/AddressList.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// アドレス帳
//----------------------------------------------------------
$emp = $session->emp();
$alist = new Cmx_AddressList($emp->emp_id());

//----------------------------------------------------------
// 検索
//----------------------------------------------------------
$query = $_POST['keyword'];

//----------------------------------------------------------
// 共有
//----------------------------------------------------------
switch ($_POST['type']) {
    case 'personal':
        $share = 'f';
        break;
    case 'share':
        $share = 't';
        break;
    default :
        $share = '';
        break;
}

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
switch ($_POST['type']) {
    // グループ
    case 'group':
        $kind = 'group';
        $lists = $alist->group_lists();
        $header = true;
        $tr_class = '';
        break;
    // グループに該当するアドレスリスト
    case 'subgroup':
        $kind = 'subgroup';
        $lists = $alist->subgroup_lists($_POST['id']);
        $header = false;
        $tr_class = 'group-' . $_POST['id'];
        break;
    default:
        $kind = 'address';
        $lists = $alist->lists($query, $share);
        $header = true;
        $tr_class = '';
        break;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('lists', $lists);
$view->assign('mode', $_POST['mode']);
$view->assign('type', $_POST['type']);
$view->assign('kind', $kind);
$view->assign('header', $header);
$view->assign('tr_class', $tr_class);
$view->display('emplist/addresslist_list.tpl');
