<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Config.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// ウェブメール設定
//----------------------------------------------------------
$mail_option = array();
if ($_GET['mode'] === 'webmail') {
    $conf = new Cmx_Config();
    $mail_option['mygroup'] = $conf->get('webmail_destination_mygroup_myself');
    $mail_option['init_disp'] = $conf->get('webmail_init_disp_address');
} else if ($_GET['mode'] === 'address_group') {
    $conf = new Cmx_Config();
    $mail_option['init_disp'] = $conf->get('webmail_init_disp_address');
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('mode', $_GET['mode']);
$view->assign('item_id', $_GET['item_id']);
$view->assign('callback', $_GET['callback']);
$view->assign('mail_option', $mail_option);
$view->display('emplist/emplist.tpl');
exit;
