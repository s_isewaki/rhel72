<?


//==========================================================
//初期処理と認証
//==========================================================
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("get_values.php");
ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
if(!$session || !$session_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// member_idで編集可能かcheckする
$login_emp_id  = get_emp_id($con,$session,$fname);




//==========================================================
// パラメータ取得
//==========================================================
$analysis_id = (int)@$_REQUEST["a_id"];
$chart_seq = (int)@$_REQUEST["chart_seq"];
$template_command = @$_REQUEST["template_command"];
$template_title = @$_REQUEST["template_title"];
$initial_tab = @$_REQUEST["initial_tab"];
$delete_chart_seq = @$_REQUEST["delete_chart_seq"];
$modify_chart_seq = @$_REQUEST["modify_chart_seq"];
$clear_chart_seq = @$_REQUEST["clear_chart_seq"];




//==========================================================
// 新規テンプレート登録の場合
//==========================================================
if ($template_command=="generate" && $template_title){
    $sql = "select max(analysis_no) from inci_analysis_rca_chart where analysis_no like 'TEMPLATE-%' and delete_flg = 'f' and is_inga_mode = ''";
    $sel = select_from_table($con, $sql, "", $fname);
    $new_analysis_no = pg_fetch_result($sel, 0, 0);
    $new_analysis_no = (!$new_analysis_no) ? "TEMPLATE-0001" : ("TEMPLATE-" . sprintf("%04d", (int)substr($new_analysis_no, 9)+1));

    $sql =
        " insert into inci_analysis_rca_chart (".
        " analysis_no, template_title, update_ymdhms, delete_flg, is_inga_mode".
        ") values (".
        " '".$new_analysis_no."', '".pg_escape_string($template_title)."', '".Date("YmdHis")."', 'f', ''".
        ")";
    update_set_table($con, $sql, array(), null, "", $fname);
}


//==========================================================
// テンプレートタイトル変更の場合
//==========================================================
if ($template_command=="modify"){
    $sql =
        " update inci_analysis_rca_chart set".
        " template_title = '".pg_escape_string($template_title)."'".
        ",update_ymdhms = '".Date("YmdHis")."'".
        " where chart_seq = " . $modify_chart_seq;
    update_set_table($con, $sql, array(), null, "", $fname);
}


//==========================================================
// テンプレート削除の場合（論理削除）
//==========================================================
if ($template_command=="delete" && $delete_chart_seq){
    $sql =
        " update inci_analysis_rca_chart set".
        " delete_flg = 't'".
        ",delete_cause='delete'".
        ",editing_emp_id='".pg_escape_string($login_emp_id)."'".
        ",update_ymdhms = '".Date("YmdHis")."'".
        " where chart_seq = " . $delete_chart_seq;
    update_set_table($con, $sql, array(), null, "", $fname);
}


//==========================================================
// テンプレートクリアの場合（クリア前にバックアップ作成を行う）
//==========================================================
if ($template_command=="clear" && $clear_chart_seq){
    $sql =
        " insert into inci_analysis_rca_chart (".
        "   analysis_id, analysis_no, template_title, chart_data, chart_item_count".
        "  ,cliplist_data, cliplist_item_count, environment_data, update_ymdhms, editing_emp_id, delete_flg, delete_cause, is_inga_mode".
        " )".
        " select".
        " analysis_id, analysis_no, template_title, chart_data, chart_item_count".
        ",cliplist_data, cliplist_item_count, environment_data, '".Date("YmdHis")."', '".pg_escape_string($login_emp_id)."', 't', 'clear', is_inga_mode".
        " from inci_analysis_rca_chart".
        " where chart_seq = " . $clear_chart_seq;
    update_set_table($con, $sql, array(), null, "", $fname);

    $sql =
        " update inci_analysis_rca_chart set".
        " chart_data = ''".
        ",chart_item_count = null".
        ",cliplist_data = ''".
        ",cliplist_item_count = null".
        ",update_ymdhms = '".Date("YmdHis")."'".
        " where chart_seq = " . $clear_chart_seq;
    update_set_table($con, $sql, array(), null, "", $fname);
}


//==========================================================
// 何かを変更した場合はリダイレクト
//==========================================================
if ($template_command=="generate" || $template_command=="delete" || $template_command=="modify" || $template_command=="clear"){
    $param =
        "?session=".$session.
        "&a_id=".$analysis_id.
        "&chart_seq=".$chart_seq.
        "&initial_tab=change";
    header("Location: hiyari_analysis_rca_chart_diversion.php".$param);
    die;
}




//==========================================================
// ヘッダ表示内容、RCAかテンプレートか
//==========================================================
if ($analysis_id){
    $sel = select_from_table($con, "select analysis_no, analysis_title from inci_analysis_regist where analysis_id = ".$analysis_id, "", $fname);
    $analysis_no = @pg_fetch_result($sel, 0, 0);
    $title = @pg_fetch_result($sel, 0, 1);
}
else {
    $sel = select_from_table($con, "select analysis_no, template_title, chart_seq from inci_analysis_rca_chart where chart_seq = ".$chart_seq." and is_inga_mode = ''", "", $fname);
    $analysis_no = @pg_fetch_result($sel, 0, 0);
    $title = @pg_fetch_result($sel, 0, 1);
    $chart_seq = @pg_fetch_result($sel, 0, 2); // 再度取得する
}



//==========================================================
// 一覧表示内容の取得
//==========================================================
$sql =
    " select".
    " a.analysis_title, a.analysis_no, a.analysis_id".
    ",c.chart_seq, c.template_title, c.chart_item_count, c.cliplist_item_count, c.update_ymdhms".
    ",e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name".
    " from inci_analysis_regist a".
    " left outer join inci_analysis_rca_chart c on (c.analysis_id = a.analysis_id and c.delete_flg = 'f')".
    " left outer join empmst e on (e.emp_id = c.editing_emp_id)".
    " where a.analysis_method = 'RCA' and is_inga_mode != 'yes'".
    " order by a.analysis_no";
$sel = select_from_table($con, $sql, "", $fname);
$rs_list_rca_regist = pg_fetch_all($sel);

$sql =
    " select".
    " c.chart_seq, c.analysis_no, c.template_title, c.chart_item_count, c.cliplist_item_count, c.update_ymdhms".
    ",e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name".
    " from inci_analysis_rca_chart c".
    " left outer join empmst e on (e.emp_id = c.editing_emp_id)".
    " where c.analysis_no like 'TEMPLATE-%'".
    " and c.delete_flg = 'f'".
    " and c.is_inga_mode = ''".
    " order by c.chart_seq";
$sel = select_from_table($con, $sql, "", $fname);
$rs_list_rca_template = pg_fetch_all($sel);


function toDispDateTime($ymdhms){
    if (strlen($ymdhms) != 14) return $ymdhms;
    return substr($ymdhms,0,4)."/".((int)substr($ymdhms,4,2))."/".((int)substr($ymdhms,6,2))." ".((int)substr($ymdhms,8,2)).":".substr($ymdhms,10,2);
}



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix RCA分析チャートデータ管理 | ファントルくん | 出来事分析</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">
    form {margin:0;}
    table.list {border-collapse:collapse; border:0;}
    table.list td {border:solid #35B341 1px;}

    .headtab { border-collapse:collapse; }
    .headtab th { padding:0; width:5px; line-height:1; }
    .headtab td { padding:0 6px; text-align:center; white-space:nowrap; line-height:1; }
    .always_green { color:#13781d; }
    .always_green:hover { color:#ff5809; }
    .engawa { width:5px; height:22px; padding:0; }
    .tab_active { background-color:#35b341; color:#fff; padding:0; }
    .tab_deactive { background-color:#aee1b3; padding:0;; }
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="padding:0; margin:0" onload="<? if ($initial_tab=='change'){ ?> changeTab('change'); <?}?> bodyResized();" onresize="bodyResized()">

<script>

function getClientHeight(){
    if (document.body.clientHeight) return document.body.clientHeight;
    if (window.innerHeight) return window.innerHeight;
    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }
    return 0;
}

var TOP_TO_SCROLL_CONTENT_HEIGHT = 212;
var SCROLL_CONTENT_TO_BOTTOM_HEIGHT = 40;
function bodyResized(){
    if (currentTabName!="inyou" && currentTabName!="change") return;
    var h = getClientHeight();
    if (h < 1) return;
    document.getElementById("scroll_div_" + currentTabName).style.height = (h - TOP_TO_SCROLL_CONTENT_HEIGHT - SCROLL_CONTENT_TO_BOTTOM_HEIGHT) + "px";
    var tbl = document.getElementById("list_scroll_table_" + currentTabName);
    document.getElementById("list_header_table_" + currentTabName).style.width = (tbl.offsetWidth || tbl.clientWidth) + "px";
}

var inyou_chart_seq = "";
function rowInyouSelected(chart_seq){
    if (inyou_chart_seq) document.getElementById("tr_inyou_"+inyou_chart_seq).style.backgroundColor = "#fff";
    inyou_chart_seq = chart_seq;
    document.getElementById("btn1").disabled = "";
    document.getElementById("btn2").disabled = "";
    document.getElementById("btn3").disabled = "";
    document.getElementById("tr_inyou_"+inyou_chart_seq).style.backgroundColor = "#fcf78b";
}


function inyou(inyou_pattern){
    if (!window.opener.swapChartData) { alert("値をセットできません。(エラー01)"); return; }
    var msg = "";
    var swap_chart_seq = "";
    var swap_cliplist_seq = "";
    if (inyou_pattern=="chart"){
        swap_chart_seq = inyou_chart_seq;
        msg += "現在編集中のデータをクリアして、指定した分析のチャートデータを読み込みます。\n\nよろしいですか？";
    }
    if (inyou_pattern=="cliplist"){
        swap_cliplist_seq = inyou_chart_seq;
        msg += "指定した分析に登録されているクリップリストデータを読み込みます。\n現在のクリップリストのデータはクリアされます。\n\nよろしいですか？";
    }
    if (inyou_pattern=="both"){
        swap_chart_seq = inyou_chart_seq;
        swap_cliplist_seq = inyou_chart_seq;
        msg += "現在編集中のデータをクリアして、チャートとクリップリストを引用します。\n\nよろしいですか？";
    }
    if (!confirm(msg)) return;
    window.opener.swapChartData(swap_chart_seq, swap_cliplist_seq);
    window.close();
    return;
}

var currentTabName = "inyou";
function changeTab(tabName, chart_seq, analysis_no, template_title){
    currentTabName = tabName;
    document.getElementById("inyou_div").style.display =    (tabName=="inyou"    ? "" : "none");
    document.getElementById("change_div").style.display =   (tabName=="change"   ? "" : "none");
    document.getElementById("template_div").style.display = (tabName=="template" ? "" : "none");
    document.getElementById("modify_div").style.display   = (tabName=="modify" ? "" : "none");

    document.getElementById("maintab_inyou").style.display    = (tabName=="inyou"    ? "" : "none");
    document.getElementById("maintab_change").style.display   = (tabName=="change"   ? "" : "none");
    document.getElementById("maintab_template").style.display = (tabName=="template" ? "" : "none");
    document.getElementById("maintab_modify").style.display   = (tabName=="modify" ? "" : "none");

    if (tabName=="template"){
        document.frm.template_title.focus();
    }

    if (tabName=="modify"){
        document.getElementById("modify_analysis_no").innerHTML = analysis_no + "<br/>";
        document.frm_modify.template_title.value = template_title;
        document.frm_modify.modify_chart_seq.value = chart_seq;
        document.frm_modify.template_title.focus();
        document.frm_modify.template_title.select();
    }

    bodyResized();
}

var change_row = "";
var change_analysis_id = "";
var change_chart_seq = "";
var change_analysis_no = "";
var change_template_title = "";
function rowChangeSelected(row, chart_seq, analysis_id, analysis_no, template_title){
    if (change_row) document.getElementById("tr_change_"+change_row).style.backgroundColor = "#fff";
    change_row = row;
    change_analysis_id = analysis_id;
    change_analysis_no = analysis_no;
    change_template_title = template_title;
    change_chart_seq = chart_seq;
    document.getElementById("btn4").disabled = "";
    document.getElementById("btn5").disabled = analysis_id ? "none" : "";
    document.getElementById("btn6").disabled = analysis_id ? "none" : "";
    document.getElementById("btn7").disabled = analysis_id ? "" : "none";
    document.getElementById("tr_change_"+change_row).style.backgroundColor = "#fcf78b";
}

function change(){
    if (!confirm("現在編集中のデータを閉じて、指定したデータの編集を開始します。\n未保存のデータはクリアされます。\n\nよろしいですか？")) return;
    var url =
        "hiyari_analysis_rca_chart.php"+
        "?session=<?=$session?>"+
        "&a_id=" + change_analysis_id +
        "&chart_seq=" + change_chart_seq;
    window.opener.location.href = url;
    window.close();
    return;
}

function changeTitle(){
    changeTab('modify', change_chart_seq, change_analysis_no, change_template_title);
}

function clearData(){
    if (!confirm("指定したチャートデータをクリアします。\nクリアすると、「ＲＣＡ分析画面」での「事象」内容のみがセットされた状態になります。\n\nクリアしてよろしいですか？")) return;
    if (window.opener.rcaClearNoticeReceiver) window.opener.rcaClearNoticeReceiver(<?=$analysis_id?>);
    var url =
        "hiyari_analysis_rca_chart_diversion.php"+
        "?session=<?=$session?>"+
        "&a_id=<?=$analysis_id?>"+
        "&chart_seq=<?=$chart_seq?>"+
        "&clear_chart_seq=" + change_chart_seq+
        "&template_command=clear";
    window.location.href = url;
    return;
}

function deleteTemplate(){
    if (!confirm("テンプレートを削除します。よろしいですか？")) return;
    if (window.opener.templateDeleteNoticeReceiver) window.opener.templateDeleteNoticeReceiver(change_chart_seq);
    var url =
        "hiyari_analysis_rca_chart_diversion.php"+
        "?session=<?=$session?>"+
        "&a_id=<?=$analysis_id?>"+
        "&chart_seq=<?=$chart_seq?>"+
        "&delete_chart_seq=" + change_chart_seq+
        "&template_command=delete";
    window.location.href = url;
    return;
}


</script>





<? //------------------------------------------------------------------------ ?>
<? // ヘッダー                                                                ?>
<? //------------------------------------------------------------------------ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
    <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>ＲＣＡ分析チャート データ管理</b></font></td>
    <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる"
        width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
</table>




<div style="padding:6px">



<? //------------------------------------------------------------------------ ?>
<? // 現在編集中のデータ                                                      ?>
<? //------------------------------------------------------------------------ ?>
<span style="background-color:#e4756b; padding:2px 4px; color:#fff; letter-spacing:2px"><?=$font?>現在編集中のデータ</font></span>
<table border="0" cellspacing="0" width="100%" class="list" style="margin-top:2px">
    <tr>
        <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>分析番号</font></td>
        <td bgcolor="#efffee" style="padding-left:4px"><?=@$analysis_no?><br/></td>
    </tr>
    <tr>
        <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>タイトル</font></td>
        <td bgcolor="#efffee" style="padding-left:4px"><?=h(@$title)?><br/></td>
    </tr>
</table>





<? //------------------------------------------------------------------------ ?>
<? // メインタブ                                                              ?>
<? //------------------------------------------------------------------------ ?>
<table style="margin-top:20px; border-collapse:collapse; width:100%; border-bottom:3px solid #35b341;"><tr><td style="padding:0">

    <table class="headtab" id="maintab_inyou"><tr>
        <th><img src="img/menu_left.gif" class="engawa"></th>
        <td class="tab_active"><?=$font?>引用・コピー</font></td>
        <th><img src="img/menu_right.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('change');" class="always_green"><?=$font?>編集切替・メンテナンス</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('template');" class="always_green"><?=$font?>テンプレート新規作成</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
    </tr></table>

    <table class="headtab" id="maintab_change" style="display:none"><tr>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('inyou');" class="always_green"><?=$font?>引用・コピー</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left.gif" class="engawa"></th>
        <td class="tab_active"><?=$font?>編集切替・メンテナンス</font></td>
        <th><img src="img/menu_right.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('template');" class="always_green"><?=$font?>テンプレート新規作成</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
    </tr></table>

    <table class="headtab" id="maintab_template" style="display:none"><tr>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('inyou');" class="always_green"><?=$font?>引用・コピー</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('change');" class="always_green"><?=$font?>編集切替・メンテナンス</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left.gif" class="engawa"></th>
        <td class="tab_active"><?=$font?>テンプレート新規作成</font></td>
        <th><img src="img/menu_right.gif" class="engawa"></th>
    </tr></table>

    <table class="headtab" id="maintab_modify" style="display:none"><tr>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('inyou');" class="always_green"><?=$font?>引用・コピー</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('change');" class="always_green"><?=$font?>編集切替・メンテナンス</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left_40.gif" class="engawa"></th>
        <td class="tab_deactive"><a href="javascript:changeTab('template');" class="always_green"><?=$font?>テンプレート新規作成</font></a></td>
        <th><img src="img/menu_right_40.gif" class="engawa"></th>
        <th><img src="img/spacer.gif" class="engawa"></th>
        <th><img src="img/menu_left.gif" class="engawa"></th>
        <td class="tab_active"><?=$font?>テンプレートタイトル変更</font></td>
        <th><img src="img/menu_right.gif" class="engawa"></th>
    </tr></table>

</td></tr></table>






<? //------------------------------------------------------------------------ ?>
<? // テンプレート新規作成タブ内                                              ?>
<? //------------------------------------------------------------------------ ?>
<div id="template_div" style="margin-top:8px; display:none">
    <form name="frm" action="hiyari_analysis_rca_chart_diversion.php" method="post">
    <input type="hidden" name="session" value="<?=$session?>"/>
    <input type="hidden" name="a_id" value="<?=$analysis_id?>"/>
    <input type="hidden" name="chart_seq" value="<?=$chart_seq?>"/>
    <input type="hidden" name="template_command" value="generate"/>
    <span style="background-color:#e4756b; padding:2px 4px; color:#fff; letter-spacing:2px"><?=$font?>テンプレート新規作成</font></span>
    <table border="0" cellspacing="0" width="100%" class="list" style="margin-top:4px">
        <tr>
            <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>分析番号</font></td>
            <td bgcolor="#efffee" style="padding-left:4px; color:#aaa">TEMPLATE-XXXX<?=$font?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（自動設定されます）<br/></font></td>
        </tr>
        <tr>
            <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>タイトル</font></td>
            <td bgcolor="#efffee" style="padding-left:4px"><input type="text" name="template_title" style="width:100%" /></td>
        </tr>
    </table>
    <div style="text-align:center;"><input type="button" value="テンプレートの新規作成" onclick="document.frm.submit();" style="margin-top:20px" /></div>
    </form>
</div>
<!-- テンプレート新規作成機能 ここまで -->







<? //------------------------------------------------------------------------ ?>
<? // テンプレートタイトル変更タブ内                                          ?>
<? //------------------------------------------------------------------------ ?>
<div id="modify_div" style="margin-top:8px; display:none">
    <form name="frm_modify" action="hiyari_analysis_rca_chart_diversion.php" method="post">
    <input type="hidden" name="session" value="<?=$session?>"/>
    <input type="hidden" name="a_id" value="<?=$analysis_id?>"/>
    <input type="hidden" name="chart_seq" value="<?=$chart_seq?>"/>
    <input type="hidden" name="modify_chart_seq" value=""/>
    <input type="hidden" name="template_command" value="modify"/>
    <span style="background-color:#e4756b; padding:2px 4px; color:#fff; letter-spacing:2px"><?=$font?>テンプレートタイトル変更</font></span>
    <table border="0" cellspacing="0" width="100%" class="list" style="margin-top:4px">
        <tr>
            <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>分析番号</font></td>
            <td bgcolor="#efffee" style="padding-left:4px" id="modify_analysis_no"></td>
        </tr>
        <tr>
            <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>タイトル</font></td>
            <td bgcolor="#efffee" style="padding-left:4px"><input type="text" name="template_title" style="width:100%" /></td>
        </tr>
    </table>
    <div style="text-align:center;"><input type="button" value="タイトル変更" onclick="if (window.opener.templateRenameNoticeReceiver) window.opener.templateRenameNoticeReceiver(document.frm_modify.template_title.value); document.frm_modify.submit();" style="margin-top:20px" /></div>
    </form>
</div>
<!-- テンプレートタイトル変更機能 ここから -->







<? //------------------------------------------------------------------------ ?>
<? // 引用・コピータブ内                                                      ?>
<? //------------------------------------------------------------------------ ?>
<div id="inyou_div">
    <div style="margin-top:8px;">
        <span style="background-color:#e4756b; padding:2px 4px; color:#fff; letter-spacing:2px;"><?=$font?>引用・コピー</font></span>
        <?=$font?><span style="color:#888">他の分析記録のチャートデータをコピーして、現在編集中データに適用します。</span></font>
    </div>

    <!-- 引用・コピースクロールヘッダ -->
    <div style="border:#fff solid 1px; margin-top:4px; border-top:0;">
    <table id="list_header_table_inyou" border="0" cellspacing="0" cellpadding="2" class="list" width="100%">
        <tr>
            <td align="center" bgcolor="#DFFFDC" style="width:110px"><?=$font?>分析番号</font></td>
            <td align="center" bgcolor="#DFFFDC" style=""><?=$font?>タイトル</font></td>
            <td align="center" bgcolor="#DFFFDC" style="width:60px"><?=$font?>要素数</font></td>
            <td align="center" bgcolor="#DFFFDC" style="width:60px"><?=$font?>ｸﾘｯﾌﾟ数</font></td>
        </tr>
    </table>
    </div>

    <!-- 引用・コピースクロール領域 -->
    <div id="scroll_div_inyou" style="overflow:scroll; border:#cccccc solid 1px; background-color:#eee">
            <!-- 引用・コピー対象リスト -->
            <table id="list_scroll_table_inyou" border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff; cursor:pointer">

                <? if (!$analysis_id && !$chart_seq) { ?>
                    <tr><td colspan="4" style="text-align:center; border:0; background-color:#eeeeee; padding-top:20px; font-size:14px">現在編集中のデータが指定されていません。<br/>引用を行うには、編集切替タブから、編集対象を指定してください。</td></tr>
                <? } else { ?>

                    <? foreach ($rs_list_rca_regist as $idx => $row){ ?>
                    <? if (!$row["chart_seq"]) continue; ?>
                    <? if ($row["chart_seq"]==$chart_seq) continue; ?>
                    <? if (!$row["chart_item_count"] && !$row["cliplist_item_count"]) continue; ?>
                    <tr id="tr_inyou_<?=$row['chart_seq']?>" onclick="rowInyouSelected(<?=$row['chart_seq']?>)">
                        <td align="center" style="width:110px"><?=$font?><?=$row['analysis_no']?><br/></font></td>
                        <td style="padding-left:3px"><?=$font?><?=$row['analysis_title']?><br/></font></td>
                        <td align="center" style="width:60px"><?=$font?><?=$row['chart_item_count']?><br/></font></td>
                        <td align="center" style="width:60px"><?=$font?><?=$row['cliplist_item_count']?><br/></font></td>
                    </tr>
                    <? } ?>
                    <tr>
                        <td colspan="6" style="border:0; background-color:#eee; height:10px"></td>
                    </td>
                    <? foreach ($rs_list_rca_template as $idx => $row){ ?>
                        <? if ($row["chart_seq"]==$chart_seq) continue; ?>
                        <? if (!$row["chart_item_count"] && !$row["cliplist_item_count"]) continue; ?>
                        <tr id="tr_inyou_<?=$row['chart_seq']?>" onclick="rowInyouSelected(<?=$row['chart_seq']?>)">
                            <td align="center" style="width:110px"><?=$font?><?=$row['analysis_no']?><br/></font></td>
                            <td style="padding-left:3px"><?=$font?><?=$row['template_title'] ?><br/></font></td>
                            <td align="center" style="width:60px"><?=$font?><?=$row['chart_item_count']?><br/></font></td>
                            <td align="center" style="width:60px"><?=$font?><?=$row['cliplist_item_count']?><br/></font></td>
                        </tr>
                    <? } ?>
                <? } ?>
            </table>
    </div>

    <!-- 引用・コピーボタンセット -->
    <div style="text-align:center; margin-top:10px">
        <input type="button" value="チャートデータのみ引用" onclick="inyou('chart');" disabled="disabled" id="btn1" />
        <input type="button" value="クリップリストデータのみ引用" onclick="inyou('cliplist');" disabled="disabled" id="btn2" />
        <input type="button" value="チャート・クリップリストデータ両方を引用" onclick="inyou('both');" disabled="disabled" id="btn3" />
    </div>

</div>







<? //------------------------------------------------------------------------ ?>
<? // 編集切替・メンテナンスタブ内                                                ?>
<? //------------------------------------------------------------------------ ?>
<div id="change_div" style="display:none">

    <div style="margin-top:8px;">
        <span style="background-color:#e4756b; padding:2px 4px; color:#fff; letter-spacing:2px;"><?=$font?>編集切替・メンテナンス</font></span>
    </div>

    <!-- 編集切替・メンテナンススクロールヘッダ -->
    <div style="border:#fff solid 1px; margin-top:4px; border-top:0;">
    <table id="list_header_table_change" border="0" cellspacing="0" cellpadding="2" class="list" width="100%">
        <tr>
            <td align="center" bgcolor="#DFFFDC" style="width:110px"><?=$font?>分析番号</font></td>
            <td align="center" bgcolor="#DFFFDC" style=""><?=$font?>タイトル</font></td>
            <td align="center" bgcolor="#DFFFDC" style="width:120px"><?=$font?>編集者</font></td>
            <td align="center" bgcolor="#DFFFDC" style="width:120px"><?=$font?>最終更新日時</font></td>
            <td align="center" bgcolor="#DFFFDC" style="width:60px"><?=$font?>要素数</font></td>
            <td align="center" bgcolor="#DFFFDC" style="width:60px"><?=$font?>ｸﾘｯﾌﾟ数</font></td>
        </tr>
    </table>
    </div>

    <!-- 編集切替・メンテナンススクロール領域 -->
    <div id="scroll_div_change" style="overflow:scroll; border:#cccccc solid 1px; background-color:#eee">

        <!-- 編集切替・メンテナンス用リスト -->
        <table id="list_scroll_table_change" border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff; cursor:pointer;">
            <? $rowcnt = 0; ?>
            <? foreach ($rs_list_rca_regist as $idx => $row){ ?>
            <? $rowcnt++; ?>
            <tr id="tr_change_<?=$rowcnt?>" onclick="rowChangeSelected(<?=$rowcnt?>, '<?=$row["chart_seq"]?>', '<?=$row["analysis_id"]?>', '<?=$row["analysis_no"]?>', '')">
                <td align="center" style="width:110px"><?=$font?><?=$row['analysis_no']?><br/></font></td>
                <td style="padding-left:3px"><?=$font?><?=$row['analysis_title']?><br/></font></td>
                    <td align="center" style="width:120px"><?=$font?><?=$row['emp_name']?></font><br/></td>
                    <td align="center" style="width:120px"><?=$font?><?=toDispDateTime($row['update_ymdhms'])?></font><br/></td>
                <? if ($row["chart_item_count"]){ ?>
                    <td align="center" style="width:60px"><?=$font?><?=$row['chart_item_count']?><br/></font></td>
                    <td align="center" style="width:60px"><?=$font?><?=$row['cliplist_item_count']?><br/></font></td>
                <? } else { ?>
                    <td align="center" style="width:120px" colspan="2"><?=$font?><span style='color:red'>ﾁｬｰﾄ未登録</span></font><br/></td>
                <? } ?>
            </tr>
            <? } ?>
            <tr>
                <td colspan="6" style="border:0; background-color:#eee; height:10px"></td>
            </td>
            <? foreach ($rs_list_rca_template as $idx => $row){ ?>
            <? $rowcnt++; ?>
            <tr id="tr_change_<?=$rowcnt?>" onclick="rowChangeSelected(<?=$rowcnt?>, '<?=$row["chart_seq"]?>', '', '<?=$row["analysis_no"]?>', '<?=str_replace("'","\'",h($row["template_title"]))?>')">
                <td align="center" style="width:110px"><?=$font?><?=$row['analysis_no']?><br/></font></td>
                <td style="padding-left:3px"><?=$font?><?=h($row['template_title'])?><br/></font></td>
                <td align="center" style="width:120px"><?=$font?><?=$row['emp_name']?></font><br/></td>
                <td align="center" style="width:120px"><?=$font?><?=toDispDateTime($row['update_ymdhms'])?></font><br/></td>
                <? if ($row["chart_item_count"]){ ?>
                    <td align="center" style="width:60px"><?=$font?><?=$row['chart_item_count']?><br/></font></td>
                    <td align="center" style="width:60px"><?=$font?><?=$row['cliplist_item_count']?><br/></font></td>
                <? } else { ?>
                    <td align="center" style="width:120px" colspan="2"><?=$font?><span style='color:red'>ﾁｬｰﾄ未登録</span></font><br/></td>
                <? } ?>
            </tr>
            <? } ?>
        </table>
    </div>

    <!-- 編集切替・メンテナンスボタンセット -->
    <div style="text-align:center; margin-top:10px;">
        <input type="button" value="テンプレートタイトル変更" onclick="changeTitle();" disabled="disabled" id="btn5" />
        <input type="button" value="テンプレート削除" onclick="deleteTemplate();" disabled="disabled" id="btn6" />
        <input type="button" value="チャートデータのクリア" onclick="clearData();" disabled="disabled" id="btn7" style="margin-left:100px" />
        <input type="button" value="チャート編集" onclick="change();" disabled="disabled" id="btn4" style="margin-left:100px" />
    </div>

</div>





</div>
</body>
</html>

