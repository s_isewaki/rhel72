<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 患者登録権限チェック
$check_auth = check_authority($session, 22, $fname);
if ($check_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// チェックされた患者をループ
if (is_array($del_pt)) {
	foreach ($del_pt as $ptif_id) {

		// 処理対象患者の入院予定情報レコード数を取得
		$sql = "select count(*) from inptres";
		$cond = "where ptif_id = '$ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$inpt_count = pg_fetch_result($sel, 0, 0);

		// レコードが存在する場合はエラーとする
		if ($inpt_count > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert('{$ptif_id}は病床管理データが存在するため削除できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// 処理対象患者の入院情報レコード数を取得
		$sql = "select count(*) from inptmst";
		$cond = "where ptif_id = '$ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$inpt_count = pg_fetch_result($sel, 0, 0);

		// レコードが存在する場合はエラーとする
		if ($inpt_count > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert('{$ptif_id}は病床管理データが存在するため削除できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// 処理対象患者の入院履歴情報レコード数を取得
		$sql = "select count(*) from inpthist";
		$cond = "where ptif_id = '$ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$inpt_count = pg_fetch_result($sel, 0, 0);

		// レコードが存在する場合はエラーとする
		if ($inpt_count > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert('{$ptif_id}は病床管理データが存在するため削除できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// 処理対象患者の入院キャンセル情報レコード数を取得
		$sql = "select count(*) from inptcancel";
		$cond = "where ptif_id = '$ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$inpt_count = pg_fetch_result($sel, 0, 0);

		// レコードが存在する場合はエラーとする
		if ($inpt_count > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert('{$ptif_id}は病床管理データが存在するため削除できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// 処理対象患者の待機患者情報レコード数を取得
		$sql = "select count(*) from ptwait";
		$cond = "where ptif_id = '$ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$inpt_count = pg_fetch_result($sel, 0, 0);

		// レコードが存在する場合はエラーとする
		if ($inpt_count > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert('{$ptif_id}は待機患者データが存在するため削除できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// 初期患者IDを取得
		$sql = "select ptif_org_ptif_id from ptifmst";
		$cond = "where ptif_id = '$ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$org_ptif_id = pg_fetch_result($sel, 0, "ptif_org_ptif_id");

		// ID変更履歴を削除
		if ($org_ptif_id != "") {
			$sql = "delete from ptifid";
			$cond = "where org_ptif_id = '$org_ptif_id'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}

		// 患者情報の物理削除
		$sql = "delete from ptifmst";
		$cond = "where ptif_id = '$ptif_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// カルテ情報の物理削除
		$sql = "delete from ptkarte";
		$cond = "where ptif_id = '$ptif_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 患者検索画面を再表示
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script language=\"javascript\">location.href = 'patient_info_menu.php?session=$session&key1=$url_key1&key2=$url_key2'</script>");
?>
