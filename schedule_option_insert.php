<?

require("about_comedix.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
$time_chart_start1 = "$time_chart_start_hour$time_chart_start_min";
$time_chart_end1 = "$time_chart_end_hour$time_chart_end_min";
if ($time_chart_start1 >= $time_chart_end1) {
    echo("<script type=\"text/javascript\">alert('タイムチャート表示時間が不正です。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if ($pub_limit == "d") {
    $pub_limit = "d" . $pub_limit_dept;
}
else if ($pub_limit == "g") {
    $pub_limit = $pub_limit_group_id;
}
if ($pub_limit == "") {
    $pub_limit = null;
}

// DBへのコネクション作成
$con = connect2db($fname);

// オプション情報を更新
$sql = "update option set";
$set = array("schedule1_default", "schedule2_default", "calendar_start1", "time_chart_start1", "time_chart_end1", "schd_pub_limit");
$setvalue = array($schedule1, $schedule2, $calendar_start, $time_chart_start1, $time_chart_end1, $pub_limit);
$cond = "where emp_id = '$emp_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// データベース接続を閉じる
pg_close($con);

// 全画面をリフレッシュ
$f_url = urlencode("schedule_option.php?session=$session&date=$date");
echo("<script type=\"text/javascript\">parent.location.href = 'main_menu.php?session=$session&f_url=$f_url';</script>");
