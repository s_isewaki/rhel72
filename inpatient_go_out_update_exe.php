<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!checkdate($mon, $day, $yr)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if ($from_hr > $to_hr || ($from_hr == $to_hr && $from_min >= $to_min)) {
	echo("<script type=\"text/javascript\">alert('時刻が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($place) > 30) {
	echo("<script type=\"text/javascript\">alert('外出先が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($comment) > 200) {
	echo("<script type=\"text/javascript\">alert('コメントが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 登録値の編集
$new_date = "$yr$mon$day";
$from_time = "$from_hr$from_min";
$to_time = "$to_hr$to_min";
$go_out_fd1 = ($go_out_fd1 == "on") ? "t" : "f";
$go_out_fd2 = ($go_out_fd2 == "on") ? "t" : "f";
$go_out_fd3 = ($go_out_fd3 == "on") ? "t" : "f";

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 更新前データ分の入院状況レコードを更新
update_inpatient_condition($con, $pt_id, $date, null, "8", $session, $fname);

// 入院状況レコードを更新
update_inpatient_condition($con, $pt_id, $new_date, $from_time, "5", $session, $fname);

// 外出情報の登録
$sql = "insert into inptgoout (ptif_id, out_type, out_date, out_time, ret_time, go_out_fd1, go_out_fd2, go_out_fd3, place, comment) values (";
$content = array($pt_id, "1", $new_date, $from_time, $to_time, $go_out_fd1, $go_out_fd2, $go_out_fd3, $place, $comment);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面があればリフレッシュし、自画面を閉じる
?>
<script type="text/javascript">
try {
	opener.location.href = 'inpatient_go_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>';
} catch (e) {
}
self.close();
</script>
