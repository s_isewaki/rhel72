<?php
require_once("about_comedix.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
    showLoginPage();
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

if($postback_mode == "update")
{
    // トランザクションの開始
    pg_query($con, "begin transaction");

    set_anonymous_input_items($con, $fname, $_POST);

    // トランザクションをコミット
    pg_query($con, "commit");
}


$arr_usable_auth = array();
$arr_inci = get_inci_mst($con, $fname);
foreach($arr_inci as $auth => $arr_val)
{
    if(is_usable_auth($auth,$fname,$con))
    {
        $arr_usable_auth[$auth] = $arr_val["auth_name"];
    }
}

$auths = "";
$arr_key = array_keys($arr_usable_auth);
for($i=0; $i<count($arr_key); $i++)
{
    if($auths != "")
    {
        $auths .= ",";
    }
    $auths .= $arr_key[$i];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | 匿名設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function init_page()
{
    anonymous_check_box();
}

function anonymous_check_box()
{

    chk_box = document.mainform.anonymous_use;

    rd_btn = document.mainform.elements['anonymous_div_class'];
    rd_btn_flg = false;
    if(rd_btn.length)
    {
        for (var i = 0; i < rd_btn.length; i++)
        {
            if(rd_btn[i].checked)
            {
                if(rd_btn[i].value == 1)
                {
                    rd_btn_flg = true;
                    break;
                }
            }
        }
    }

    auth_rd_flg = true;
    if ((chk_box.checked && !rd_btn_flg))
    {
        auth_rd_flg = false;
    }


    anonymous_use(chk_box.checked);
    anonymous_auth_radio(auth_rd_flg);
}




//匿名利用によるdisable制御
function anonymous_use(flg)
{
    var rd_btn = document.mainform.elements['anonymous_div_class'];
    if(rd_btn.length) {
        for (var i = 0; i < rd_btn.length; i++) {
            rd_btn[i].disabled = !flg;
        }
    }

    var rd_btn = document.mainform.elements['anonymous_name'];
    if(rd_btn.length) {
        for (var i = 0; i < rd_btn.length; i++) {
            rd_btn[i].disabled = !flg;
        }
    }

    var chk_btn = document.mainform.elements['default_flg'];
    chk_btn.disabled = !flg;

}


//権限のチェックボックスのdisable制御
function anonymous_auth_radio(flg)
{
    var chk_box = "";
    <?
    foreach($arr_key as $key)
    {
    ?>
        <?
        if($key != "SM")
        {
            $key = strtolower($key);
        ?>
        chk_box = document.mainform.<?=$key?>_anonymous;
        chk_box.disabled = flg;
        <?
        }
        ?>
    <?
    }
    ?>
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="#" method="post">
<input type="hidden" name="postback_mode" value="update">
<input type="hidden" name="auths" value="<?=$auths?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j14">

<!-- ここから -->

<?php
$is_anonymous_usable = get_anonymous_mail_use_flg($con,$fname);
if ($is_anonymous_usable) {
    $anonymous_checked = "checked";
}
?>
<label><input type="checkbox" name="anonymous_use" value="t" onclick="anonymous_check_box()" <?php echo $anonymous_checked;?>>匿名送信を許可する</label><br>


<?php
$default_check_flg = get_anonymous_default_flg($con,$fname);
if ($default_check_flg) {
    $default_flg_checked = "checked";
}
?>
<label><input type="checkbox" name="default_flg" value="t" <?=$default_flg_checked?>>報告者はデフォルトで匿名とする</label><br>


<?php
$anonymous_div = get_anonymous_div($con,$fname);
?>
<label><input type="radio" name="anonymous_div_class" value="1" onclick="anonymous_auth_radio(true);" <?if($anonymous_div == '1') {echo(" checked");}?>>完全匿名</label><br>
<label><input type="radio" name="anonymous_div_class" value="2"  <?if($anonymous_div == '2') {echo(" checked");}?> onclick="anonymous_auth_radio(false);">一部匿名</label><br>


<?php
$arr_anonymous_mail_use_auth = get_anonymous_mail_use_auth($con,$fname);
foreach ($arr_usable_auth as $auth => $auth_name) {
    if ($auth != "SM") {
        $anonymous_flg = $arr_anonymous_mail_use_auth[$auth];
        $auth = strtolower($auth);
?>
<label style="margin-left:1em;"><input type="checkbox" name="<?=$auth?>_anonymous" value="t" <?if($anonymous_flg == "t"){ echo(" checked");}?>><?=h($auth_name)?>には匿名</label><br>
<?php
    }
}
?>

<br>

<?php
$anonymous_name = get_anonymous_name($con,$fname);
?>
匿名送信時の報告者の氏名表示<br>
<label style="margin-left:1em;"><input type="radio" name="anonymous_name" value="ファントルくん" <?if($anonymous_name == 'ファントルくん') {echo(" checked");}?>>ファントルくん</label><br>
<label style="margin-left:1em;"><input type="radio" name="anonymous_name" value="匿名"  <?if($anonymous_name == '匿名') {echo(" checked");}?>>匿名</label><br>

<br>

<?php
$excel_output = get_anonymous_excel_output($con,$fname);
if (!$excel_output) {
    $excel_output_checked = "checked";
}
?>
<label><input type="checkbox" name="excel_output" value="f" <?php echo $excel_output_checked;?>>Excel出力で報告者氏名を出力しない</label><br>

<!-- ここまで -->


<div align="right">
<input type="submit" value="更新">
</div>

</font>
</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>

</form>
</body>
</html>
<?

/**
 * 匿名設定情報登録処理
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param object $inputs 画面入力情報
 */
function set_anonymous_input_items($con, $fname, $inputs)
{

    $auths = $inputs["auths"];
    $arr_auth = split(",", $auths);

    // 匿名送信
    $anonymous_use = $inputs["anonymous_use"];
    set_anonymous_mail_use_flg($con,$fname,$anonymous_use != "");

    // 完全匿名 or 一部匿名
    $anonymous_div_class = $inputs["anonymous_div_class"];
    set_anonymous_div($con,$fname,$anonymous_div_class);

    // 一部匿名
    foreach($arr_auth as $val)
    {
        if($val != "SM")
        {
            $anonymous = $inputs[strtolower($val)."_anonymous"];
            if($anonymous == "") {$anonymous = 'f';}
            set_anonymous_mail_use_auth($con,$fname,$val,$anonymous);
        }
    }

    // デフォルト匿名
    $default_flg   = $inputs["default_flg"];
    set_anonymous_default_flg($con,$fname,$default_flg != "");

    //匿名表示氏名
    $anonymous_name = $inputs["anonymous_name"];
    $anonymous_name = ($anonymous_name == "") ?  "ファントルくん": $anonymous_name;
    set_anonymous_name($con,$fname,$anonymous_name);

    // Excel出力
    $excel_output = $inputs["excel_output"];
    set_anonymous_excel_output($con,$fname,$excel_output == "");
}


//==============================
//DBコネクション終了
//==============================
pg_close($con);
