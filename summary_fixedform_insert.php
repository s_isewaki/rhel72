<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");



//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session, ($kind_flg=="1"?57:59) ,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($fixedform_sentence == "") {
	echo("<script type=\"text/javascript\">alert('定型文を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//個人の場合
if ($kind_flg == "1") {
	//利用者IDを取得
	$emp_id = get_emp_id($con, $session, $fname);
} else {
//共用の場合
	$emp_id = null;
}

// 定型文IDを採番
$sql = "select max(fixedform_id) from fixedform";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
$fixedform_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

$sql =
	" insert into fixedform (".
	" fixedform_id, emp_id, fixedform_sentence, sect_id".
	" ) values (".
	" ".$fixedform_id.
	",". ($emp_id ? "'".pg_escape_string($emp_id)."'" : "null").
	",'".pg_escape_string($fixedform_sentence)."'".
	",".((int)@$sel_sect_id ? (int)@$sel_sect_id : "null").
	")";
$ret = update_set_table($con, $sql, array(), null, "", $fname);

// データベース接続を閉じる
pg_close($con);

// 呼び出し元画面を再表示
if ($kind_flg == "1") {
	echo("<script type=\"text/javascript\">opener.location.reload();window.close();</script>");
} else {
	echo("<script type=\"text/javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();} window.close();</script>");
}
?>
