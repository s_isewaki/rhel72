<?php

require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

// 指定テーブルの指定数値カラムの最大値を取得する
function get_col_max($con, $table, $col, $fname) {
    $sql = "SELECT max($col) FROM $table";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }

    $col_max = pg_result($sel, 0, "max"); //max値を取得する
    return $col_max;
}

// 職員プロフィールを取得
function get_profile($con, $emp_id, $fname) {

    $sql = "select empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as emp_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, inci_profile.* ";
    $sql .= "from empmst ";
    $sql .= "left join classmst on empmst.emp_class = classmst.class_id ";
    $sql .= "left join atrbmst on empmst.emp_attribute = atrbmst.atrb_id ";
    $sql .= "left join deptmst on empmst.emp_dept = deptmst.dept_id ";
    $sql .= "left join classroom on empmst.emp_room = classroom.room_id ";
    $sql .= "left join inci_profile on inci_profile.emp_id = empmst.emp_id";
    $cond = "where empmst.emp_id = '$emp_id'";

    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    return pg_fetch_assoc($result);
}

// 「当事者所属部署」にセットする職員部署名称を取得
function get_emp_profile_class_name($profile, $profile_use) {

    $class_nm = "";
    if ($profile_use["class_flg"] == "t") {
        $class_nm .= $profile["class_nm"];
    }

    if ($profile_use["attribute_flg"] == "t") {
        $class_nm .= $profile["atrb_nm"];
    }

    if ($profile_use["dept_flg"] == "t") {
        $class_nm .= $profile["dept_nm"];
    }

    if ($profile_use["room_flg"] == "t") {
        $class_nm .= $profile["room_nm"];
    }

    return $class_nm;
}

// 職員プロフィールから経験年月を計算
function get_experience($profile) {
    $result = array();
    list($result["exp_year"], $result["exp_month"]) = get_passage($profile["exp_year"], $profile["exp_month"]);
    list($result["dept_year"], $result["dept_month"]) = get_passage($profile["dept_year"], $profile["dept_month"]);
    return $result;
}

// 経過年月を取得
function get_passage($year, $month) {
    if ($year == "" || $month == "") {
//         return array(900, 90);
        return array("", "");
    }

    $this_year = date("Y");
    $this_month = date("m");
    if ($year > $this_year || ($year == $this_year && $month > $this_month)) {
        return array(900, 90);
    }

    $passage_year = $this_year - $year;
    $passage_month = $this_month - $month;
    if ($passage_month < 0) {
        $passage_year--;
        $passage_month += 12;
    }
    return array(sprintf("%03d", $passage_year), sprintf("%02d", $passage_month));
}

// 「プロフィールからコピー」ボタン表示セルのrowspanを算出
function get_profile_rowspan($grp_flags) {
    $result = 0;
    if (in_array(3100, $grp_flags)) {
        $result++;
    }
    if (in_array(3150, $grp_flags)) {
        $result++;
    }
    if (in_array(3200, $grp_flags)) {
        $result++;
    }
    if (in_array(3250, $grp_flags)) {
        $result += 2;
    }
    if (in_array(3300, $grp_flags)) {
        $result += 2;
    }
    return $result;
}
