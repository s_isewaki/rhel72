<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | ワークフロー一覧</title>
<?
require_once("about_comedix.php");
require_once("show_fplusadm_category_list.ini");
require_once("fplusadm_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファントルくん＋管理権限のチェック
$checkauth = check_authority($session, 79, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

//ロード時の処理
function init_action()
{
	//ドラッグアンドドロップを初期設定
	constructDragDrop();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_action()">


<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
	</tr>
</table>



<table style="width:100%; margin-top:4px"><tr>
<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
<td style="vertical-align:top">

	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
		<tr><? show_wkfw_menuitem($session, $fname, ""); ?></tr>
	</table>

	<form name="wkfw" action="#" method="post" style="margin-top:6px">
		<? show_workflow_list($con, $session, $fname, $selected_cate, $selected_folder, $selected_parent, $select_box, $page); ?>
		<input type="hidden" name="session" value="<? echo($session); ?>">
	</form>

</td>
</tr></table>

</td></tr></table>



</body>
<?
// エイリアス対応2007/09/25
// ワークフロー一覧画面を初回起動のみフォルダ作成する。
require_once("fplus_common_class.php");
$obj = new fplus_common_class($con, $fname);
$obj->create_wkfwreal_directory();

pg_close($con);
?>
</html>
