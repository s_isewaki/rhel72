<?
//==============================
//本処理
//==============================

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 47, $fname);
if ($hiyari == "0")
{
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="if (typeof(loadaction)=='function') loadaction()">
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="super_item_id" value="<?=$super_item_id?>">

<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?
    $PAGE_TITLE = "項目管理";
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<? // ヘッダータブ表示
show_item_edit_header_tab($session, $fname, $grp_code, $easy_item_code);
?>

<!-- メイン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>

<!--// トップリンク START -->
<a href="hiyari_ic_super_patient.php?session=<?=$session?>">影響・対応区分</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="hiyari_ic_patient.php?session=<?=$session?>&item_code=influence&id=<?= empty($_GET['id']) ? 1 : $_GET['id']?>">患者への影響</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="hiyari_ic_add_patient.php?session=<?=$session?>&item_code=blood">出血量</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="hiyari_ic_add_patient.php?session=<?=$session?>&item_code=parts">部位</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="hiyari_ic_patient.php?session=<?=$session?>&item_code=mental&id=<?= empty($_GET['id']) ? 1 : $_GET['id']?>">患者への精神的影響</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="hiyari_ic_patient.php?session=<?=$session?>&item_code=correspondence&id=<?= empty($_GET['id']) ? 1 : $_GET['id']?>">対応</a>&nbsp;&nbsp;&nbsp;&nbsp;
<!--// トップリンク END -->

<br><img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!--// プルダウンメニューの表示 START -->
<select name="menu" onChange="location.href='hiyari_ic_add_patient.php?session=<?=$session?>&id='+this.form.menu.value+'&item_code=<?=$_GET['item_code']?>'">
<?php
$menu = get_super_item($con, $fname);
foreach($menu as $value) {
?>
    <option value="<?=$value['super_item_id']?>" <?php if($value['super_item_id'] == $_GET['id']) echo 'selected'?>><?=$value['super_item_name']?>
<?php
}
?>
</select>
<!--// プルダウンメニューの表示 END -->

<br><img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="800" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!--// 戻るボタン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
    <input type="button" value="戻る" onclick="history.back();">
</td>
</tr>
</table>
<!--// 戻るボタン END -->

<?
if($_GET['item_code'] == 'blood' && $_GET['id'] == 2) {
?>
<!--// 出血量 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22">
        <td width=250 bgcolor="#DFFFDC">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <span >

                出血量</span>
            </font>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
        </td>
        <td>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            約
            </font>
                <input type="text" id="id_1400_40" name="_1400_40" value="" style="width:50"  >
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            cc
            </font>
        </td>
    </tr>
</table>
<!--// 出血量 END -->

<?php
} else if($_GET['item_code'] == 'parts' && $_GET['id'] == 3) {
?>
<!--// 部位 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22">
        <td   width=250 bgcolor="#DFFFDC">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <span >部位</span>
            </font>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
        </td>
        <td>
            <input type="text" id="id_1400_20" name="_1400_20" value="" style="width:250"  >
        </td>
    </tr>
</table>
<!--// 部位 END -->

<?php
}
?>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->



</td>
</tr>
</table>
<!-- メイン END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?

//==============================
//DBコネクション終了
//==============================
pg_close($con);

function get_super_item($con, $fname) {
    $sql = "SELECT
                super_item_id,
                super_item_num,
                super_item_order,
                super_item_name,
                disp_flg,
                available
            FROM
                inci_ic_super_item
            WHERE
                available = 't'
            ORDER BY
                super_item_order";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_all($sel);
}

?>